object FcorrecaodeTitulos: TFcorrecaodeTitulos
  Left = 182
  Top = 93
  Width = 232
  Height = 153
  Caption = 'Corre'#231#227'o de Valores de T'#237'tulos - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 104
    Height = 14
    Caption = 'Data para Corre'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 43
    Width = 87
    Height = 14
    Caption = 'Taxa Mensal (%)'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 8
    Top = 67
    Width = 81
    Height = 14
    Caption = 'Tipo de Lancto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Btgravar: TBitBtn
    Left = 8
    Top = 86
    Width = 209
    Height = 37
    Caption = '&Processar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BtgravarClick
  end
  object edtdata: TMaskEdit
    Left = 144
    Top = 16
    Width = 73
    Height = 19
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
  end
  object edttaxa: TEdit
    Left = 144
    Top = 40
    Width = 73
    Height = 19
    TabOrder = 1
    OnKeyPress = edttaxaKeyPress
  end
  object edttipolancto: TEdit
    Left = 144
    Top = 64
    Width = 73
    Height = 19
    TabOrder = 2
    OnKeyDown = edttipolanctoKeyDown
  end
end
