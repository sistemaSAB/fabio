unit UHistoricoLancamentos;

interface

uses
  UObjLancamento, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, Grids, StdCtrls,UessencialGlobal, UobjREGISTRORETORNOCOBRANCA,DB,
  ExtCtrls, DBGrids;

type
  TFhistoricoLancamento = class(TForm)
    SGLancto: TStringGrid;
    Panel1: TPanel;
    Label1: TLabel;
    LbPendencia: TLabel;
    btexcluirlancamento: TButton;
    btRecibo: TButton;
    Panel2: TPanel;
    gridLancto: TDBGrid;
    spl1: TSplitter;
    procedure FormActivate(Sender: TObject);
    procedure SGLanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure btexcluirlancamentoClick(Sender: TObject);
    procedure btReciboClick(Sender: TObject);
    procedure SGLanctoSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Function ExcluiLancamentoExterno(pcodigolancamento:string):boolean;

  public
    { Public declarations }
    Objlancamento:TobjLancamento;
    procedure retornaLancamento(Plancamento:string);
  end;

var
  FhistoricoLancamento: TFhistoricoLancamento;

implementation

uses UDataModulo, UobjParametros;



{$R *.DFM}

procedure TFhistoricoLancamento.FormActivate(Sender: TObject);
begin

        SGLancto.ColCount:=6;
        SGLancto.cols[0].clear;
        SGLancto.cols[1].clear;
        SGLancto.cols[2].clear;
        SGLancto.cols[3].clear;
        SGLancto.cols[4].clear;
        SGLancto.cols[5].clear;
        SGLancto.RowCount:=ObjLancamento.Get_QuantRegs(lbpendencia.caption)+1;
        //Objlancamento.Pendencia.Get_QuantRegsLancamento(lbpendencia.caption)+1;
        If (SGLancto.RowCount<=1)
        Then SGLancto.RowCount:=2;

        SGLancto.FixedRows:=1;
        ObjLancamento.get_listacodigo(SGLancto.Cols[1],lbpendencia.caption);
        ObjLancamento.Get_ListaTipoLancto(SGLancto.Cols[2],lbpendencia.caption);
        ObjLancamento.Get_ListaValor(SGLancto.Cols[3],lbpendencia.caption);
        ObjLancamento.Get_ListaHistorico(SGLancto.Cols[4],lbpendencia.caption);
        ObjLancamento.Get_ListaData(SGLancto.Cols[0],lbpendencia.caption);
        Objlancamento.Get_ListaPortadores(SGLancto.Cols[5],lbpendencia.caption);

        AjustaLArguraColunaGrid(SGLancto);


end;

procedure TFhistoricoLancamento.retornaLancamento(Plancamento:string);
begin

end;

procedure TFhistoricoLancamento.SGLanctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if (key=vk_delete)
     Then Begin
               btexcluirlancamentoclick(sender);
     End;

     if (Key=vk_f8) and (SGLancto.Cells[1,SGLancto.Row]<>'')
     Then begin
               Objlancamento.ImprimeReciboPagamento(SGLancto.Cells[1,SGLancto.Row]);
     end;
end;

procedure TFhistoricoLancamento.FormShow(Sender: TObject);
begin

     Uessencialglobal.PegaCorForm(Self);
     gridLancto.DataSource:=Objlancamento.ObjDataSourceLancto;
    

     
end;

procedure TFhistoricoLancamento.btexcluirlancamentoClick(Sender: TObject);
var
      ObjRegistroRetornoCobranca:TObjREGISTRORETORNOCOBRANCA;
      Registroretcob:string;
      pautorizou:String;
begin
    if (SGLancto.Row=0)
    Then exit;

    if (SGLancto.Cells[1,sglancto.row]='')
    Then exit;

    If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
    Then exit;

    // *************************Alterado por F�bio*********
    if (ObjParametroGlobal.ValidaParametro('PEDIR SENHA AO APAGAR LANCAMENTO FINANCEIRO')=true)
    then Begin
             if (ObjParametroGlobal.Get_Valor = 'SIM')
             then Begin
                    if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('APAGAR LAN�AMENTO FINANCEIRO PORTADDOR')=False)
                    Then Begin
                         //senha
                         if (ObjPermissoesUsoGlobal.PedePermissao(ObjPermissoesUsoGlobal.get_codigo,pautorizou,'Senha lan�amento retroativo')=false)
                         Then exit;

                         pautorizou:='AUTORIZADO';
                    End;
             end else pautorizou:='AUTORIZADO';
    end;

    if (pautorizou = '')
    then exit;
    //***********************************************

    //25082008 alterado por celio - verifica se a pendencia foi quitada por baixa autom�tica de boleto
    //esta fun��o retorna o lancamento pai do lancto da pendencia e o codigo do registroretornocobranca a ser alterado
    If(Objlancamento.RetornaLancamentoBaixaBoletoemLote(lbpendencia.Caption,Registroretcob)>0)
    Then Begin
          //se foi baixado automaticamente, para excluir precisa desvincular o lancamento da tabregistroretornocobranca
          //posso passar o processado para "E" de extornado e remover o lancamento
          Try
                ObjRegistroRetornoCobranca:=TObjREGISTRORETORNOCOBRANCA.Create;
          Except
                MensagemErro('Erro na tentativa de criar Objeto Retorno Boletos');
                Exit;
          End;

          ObjRegistroRetornoCobranca.LocalizaCodigo(Registroretcob);
          ObjRegistroRetornoCobranca.TabelaparaObjeto;
          ObjRegistroRetornoCobranca.Submit_Processado('E');
          ObjRegistroRetornoCobranca.Lancamento.ZerarTabela;
          ObjRegistroRetornoCobranca.Status:=dsEdit;
          If(ObjRegistroRetornoCobranca.Salvar(false)=false)
          Then Begin
                Mensagemerro('Erro na tentativa Desvincular o retorno do boleto ao lancamento');
                FDataModulo.IBTransaction.RollbackRetaining;
                exit;
          End;
          ObjRegistroRetornoCobranca.free;
    End;
    //**** celio


    //**** F�bio
    if (Self.excluilancamentoexterno(SGLancto.Cells[1,sglancto.row])=False)
    Then Begin
              Mensagemerro('Erro na tentativa de exclus�o');
              exit;
    End;

    If (ObjLancamento.ApagaLancamento(SGLancto.Cells[1,sglancto.row],False)=false)
    Then Begin
              Messagedlg('Erro Durante a Exclus�o!!',mterror,[mbok],0);
              FDataModulo.IBTransaction.RollbackRetaining;
              exit;
    End;
    FDataModulo.IBTransaction.CommitRetaining;
    Self.FormActivate(self);
end;

function TFhistoricoLancamento.ExcluiLancamentoExterno(pcodigolancamento:string): boolean;
var
pstrsql:TstringList;
cont:integer;
sqlexclusaoexterno:String;
begin
     result:=False;

     sqlexclusaoexterno:='';

     Try
        PstrSql:=TStringList.Create;
     Except
           Mensagemerro('Erro na tentativa de criar  String List de extra��o de sql externo');
           exit;
     End;


     //********************************* Alterado por F�bio **************************
     if (ObjParametroGlobal.ValidaParametro('SQL EXTERNO EM EXCLUSAO DE LANCAMENTO')=true)
     then  sqlexclusaoexterno:=ObjParametroGlobal.Get_Valor;

     if (sqlexclusaoexterno='')
     then Begin
              result:=True;
              exit;
     End;

     Try

       if (ExplodeStr(sqlexclusaoexterno,pstrsql,'|','STRING')=False)
       then Begin
                 mensagemerro('Erro na tentativa de extrair o sql externo');
                 exit;
       End;

      for cont:=0 to Pstrsql.count-1 do
      Begin
          pstrsql[cont]:=StrReplace(UpperCase(pstrsql[cont]),UpperCase(':codigolancamento'),pcodigolancamento);
          if (Self.Objlancamento.ExecutaSQL(Pstrsql[cont])=False)
          Then exit;
      End;

      result:=true;
     Finally
            freeandnil(Pstrsql);
     End;
end;

procedure TFhistoricoLancamento.btReciboClick(Sender: TObject);
begin
      if (SGLancto.Cells[1,SGLancto.Row]='')
      then exit;

      Self.ObjLancamento.ImprimeReciboPagamento(SGLancto.Cells[1,SGLancto.Row]);
end;

procedure TFhistoricoLancamento.SGLanctoSelectCell(Sender: TObject; ACol,ARow: Integer; var CanSelect: Boolean);
begin

  //ShowMessage (SGLancto.Cells[1,ARow]);

  Objlancamento.retornaLancto(SGLancto.Cells[1,ARow]);

  formatadbgrid(gridLancto);


end;

procedure TFhistoricoLancamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  gridLancto.DataSource.DataSet.Close;


end;

end.
