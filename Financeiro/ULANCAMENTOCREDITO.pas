unit ULANCAMENTOCREDITO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjLANCAMENTOCREDITO,
  jpeg;

type
  TFLANCAMENTOCREDITO = class(TForm)
    Guia: TTabbedNotebook;
    Label1: TLabel;
    Edit1: TEdit;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    painel_imagem: TPanel;
    imcesta: TImage;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    ImagemFundo: TImage;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbData: TLabel;
    EdtData: TMaskEdit;
    LbCliente: TLabel;
    EdtCliente: TEdit;
    LbValor: TLabel;
    EdtValor: TEdit;
    LbHistorico: TLabel;
    EdtHistorico: TEdit;
    LbVenda: TLabel;
    EdtVenda: TEdit;
    LbDevolucao: TLabel;
    EdtDevolucao: TEdit;
    LbValores: TLabel;
    EdtValores: TEdit;
    LbCredito: TLabel;
    EdtCredito: TEdit;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjLANCAMENTOCREDITO:TObjLANCAMENTOCREDITO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLANCAMENTOCREDITO: TFLANCAMENTOCREDITO;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFLANCAMENTOCREDITO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjLANCAMENTOCREDITO do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Data(edtData.text);
        Submit_Cliente(edtCliente.text);
        Submit_Valor(edtValor.text);
        Submit_Historico(edtHistorico.text);
        Submit_Venda(edtVenda.text);
        Submit_Devolucao(edtDevolucao.text);
        Submit_Valores(edtValores.text);
        Submit_Credito(edtCredito.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFLANCAMENTOCREDITO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjLANCAMENTOCREDITO do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtData.text:=Get_Data;
        EdtCliente.text:=Get_Cliente;
        EdtValor.text:=Get_Valor;
        EdtHistorico.text:=Get_Historico;
        EdtVenda.text:=Get_Venda;
        EdtDevolucao.text:=Get_Devolucao;
        EdtValores.text:=Get_Valores;
        EdtCredito.text:=Get_Credito;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFLANCAMENTOCREDITO.TabelaParaControles: Boolean;
begin
     If (Self.ObjLANCAMENTOCREDITO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFLANCAMENTOCREDITO.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjLANCAMENTOCREDITO:=TObjLANCAMENTOCREDITO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
end;

procedure TFLANCAMENTOCREDITO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjLANCAMENTOCREDITO=Nil)
     Then exit;

     If (Self.ObjLANCAMENTOCREDITO.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjLANCAMENTOCREDITO.free;
end;

procedure TFLANCAMENTOCREDITO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFLANCAMENTOCREDITO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjLANCAMENTOCREDITO.status:=dsInsert;
     Guia.pageindex:=0;
     edtData.setfocus;

end;


procedure TFLANCAMENTOCREDITO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjLANCAMENTOCREDITO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjLANCAMENTOCREDITO.Status:=dsEdit;
                guia.pageindex:=0;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtData.setfocus;
                
    End;


end;

procedure TFLANCAMENTOCREDITO.btgravarClick(Sender: TObject);
begin

     If Self.ObjLANCAMENTOCREDITO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjLANCAMENTOCREDITO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjLANCAMENTOCREDITO.Get_codigo;
     Self.ObjLANCAMENTOCREDITO.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFLANCAMENTOCREDITO.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjLANCAMENTOCREDITO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjLANCAMENTOCREDITO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjLANCAMENTOCREDITO.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFLANCAMENTOCREDITO.btcancelarClick(Sender: TObject);
begin
     Self.ObjLANCAMENTOCREDITO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFLANCAMENTOCREDITO.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFLANCAMENTOCREDITO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjLANCAMENTOCREDITO.Get_pesquisa,Self.ObjLANCAMENTOCREDITO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjLANCAMENTOCREDITO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjLANCAMENTOCREDITO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjLANCAMENTOCREDITO.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFLANCAMENTOCREDITO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFLANCAMENTOCREDITO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

