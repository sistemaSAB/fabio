unit UObjGerador;
Interface
Uses Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,Classes,UobjContaGer;

Type
   TObjGerador=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar                          :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaGerador(Parametro:string) :boolean;
                Function    Localizanome(Parametro:string):boolean;
                Function    LocalizaCodigoPorObjeto(Parametro:string):Integer;
                Function    LocalizaContaGerencialPorObjeto(Parametro:string):Integer;


                Function    exclui(Pcodigo:string)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaContaGerencial      :string;
                Function    Get_TituloPesquisaContaGerencial:string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;


                Function Get_CODIGO              :string;
                Function Get_Nome                :string;
                Function Get_Tabela              :string;
                Function Get_Objeto              :string;
                Function Get_InstrucaoSQL        :string;
                Function Get_ContaGerencial      :string;

                Procedure Submit_CODIGO              (parametro:string);
                Procedure Submit_Nome                (parametro:string);
                Procedure Submit_Tabela              (parametro:string);
                Procedure Submit_Objeto              (parametro:string);
                Procedure Submit_InstrucaoSQL        (parametro:string);
                Procedure Submit_ContaGerencial      (parametro:string);

                //Retorna o nome dos geradores
                Procedure  Get_listaNomeGeradores(parametro:TStrings);
                Procedure  Get_listaCodigoGeradores(parametro:TStrings);
                function Get_NovoCodigo: string;


                
         Private
               ObjDataset:Tibdataset;

               CODIGO                  :String[09];
               Nome                    :String[50];
               Tabela                  :String[50];
               Objeto                  :String[50];
               InstrucaoSQL            :String[100];
               ContaGerencial          :TobjContaGer;


                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Procedure  TObjGerador.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO        := FieldByName('CODIGO').asstring          ;
        Self.Nome          := FieldByName('Nome').asstring            ;
        Self.Tabela        := FieldByName('Tabela').asstring          ;
        Self.Objeto        := FieldByName('Objeto').asstring          ;
        Self.InstrucaoSQL  := FieldByName('InstrucaoSQL').asstring    ;

        If (Self.ContaGerencial.LocalizaCodigo(FieldByName('Contagerencial').asstring)=False)
        Then Begin
                  Messagedlg('Conta Gerencial N�o Encontrada!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  Exit;
             End

        Else Self.ContaGerencial.TabelaparaObjeto;
        

     End;
end;


Procedure TObjGerador.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
        FieldByName('CODIGO').asstring          := Self.CODIGO                          ;
        FieldByName('Nome').asstring            := Self.Nome                            ;
        FieldByName('Tabela').asstring          := Self.Tabela                          ;
        FieldByName('Objeto').asstring          := Self.Objeto                          ;
        FieldByName('InstrucaoSQL').asstring    := Self.InstrucaoSQL                    ;
        FieldByName('ContaGerencial').asstring  := Self.ContaGerencial.Get_codigo       ;
  End;
End;

//***********************************************************************

function TObjGerador.Salvar: Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;



if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;
 FDataModulo.IBTransaction.CommitRetaining;
 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjGerador.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO        :='';
        Self.Nome          :='';
        Self.Tabela        :='';
        Self.Objeto        :='';
        Self.InstrucaoSQL  :='';
        Self.ContaGerencial.ZerarTabela;
     End;
end;

Function TObjGerador.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Nome='')
       Then Mensagem:=mensagem+'/Nome';

       If (Tabela='')
       Then Mensagem:=mensagem+'/Tabela';

       If (Objeto='')
       Then Mensagem:=mensagem+'/Objeto';

       If (InstrucaoSQL='')
       Then Mensagem:=mensagem+'/Instru��o SQL';

       If (ContaGerencial.Get_codigo='')
       Then Mensagem:=mensagem+'/Conta Gerencial';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjGerador.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     If (Self.ContaGerencial.LocalizaCodigo(Self.ContaGerencial.Get_CODIGO)=False)
     Then Mensagem:=Mensagem+'/Conta Gerencial N�o Encontrada!';
     

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjGerador.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;


     try
        Inteiros:=Strtoint(Self.ContaGerencial.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Conta Gerencial';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjGerador.VerificaData: Boolean;
var
Datas:Tdate;
Mensagem:string;
begin
     mensagem:='';

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjGerador.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjGerador.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,tabela,objeto,instrucaoSQL,contagerencial from TabGerador where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjGerador.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjGerador.Exclui(Pcodigo: string): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 FDataModulo.IBTransaction.CommitRetaining;
             End
                 
        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjGerador.create;
begin

        Self.ContaGerencial:=TObjContaGer.create;
        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,nome,tabela,objeto,instrucaoSQL,contagerencial from TabGerador where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert into TabGerador (codigo,nome,tabela,objeto,instrucaoSQL,contagerencial) values (:codigo,:nome,:tabela,:objeto,:instrucaoSQL,:contagerencial)');

                ModifySQL.clear;
                ModifySQL.add('Update tabGerador set codigo=:codigo,nome=:nome,tabela=:tabela,objeto=:objeto,instrucaoSQL=:instrucaoSQL,contagerencial=:contagerencial where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add('Delete  from tabGerador where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,nome,tabela,objeto,instrucaoSQL,contagerencial from TabGerador where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjGerador.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjGerador.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjGerador.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjGerador.Get_Pesquisa: string;
begin
     Result:=' Select * from TabGerador ';
end;

function TObjGerador.Get_TituloPesquisa: string;
begin
     Result:='Pesquisa de Rela��o de Geradores de T�tulo';
end;

function TObjGerador.Get_InstrucaoSQL: string;
begin
     Result:=Self.InstrucaoSQL;
end;

function TObjGerador.Get_Nome: string;
begin
     Result:=Self.Nome;
end;

function TObjGerador.Get_Objeto: string;
begin
     Result:=Self.Objeto;
end;

function TObjGerador.Get_Tabela: string;
begin
     Result:=Self.Tabela;
end;

procedure TObjGerador.Submit_InstrucaoSQL(parametro: string);
begin
     Self.InstrucaoSql:=parametro;
end;

procedure TObjGerador.Submit_Nome(parametro: string);
begin
     Self.Nome:=parametro;
end;

procedure TObjGerador.Submit_Objeto(parametro: string);
begin
     Self.Objeto:=parametro;
end;

procedure TObjGerador.Submit_Tabela(parametro: string);
begin
     Self.tabela:=parametro;
end;

function TObjGerador.LocalizaGerador(Parametro: string): boolean;
begin
     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add(' Select *  from '+Self.tabela+' where codigo='+parametro);
          open;
          If (RecordCount>0)
          Then result:=true
          Else result:=false;
     End;
end;


function TObjGerador.Localizanome(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,tabela,objeto,instrucaoSQL,contagerencial from TabGerador where nome='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
End;

procedure TObjGerador.Get_listaCodigoGeradores(parametro: TStrings);
begin
     With Self.Objdataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select codigo from TabGerador');
          open;
          first;
          parametro.clear;
          While Not(eof) do
          Begin
               parametro.add(fieldbyname('codigo').asstring);
               next;
          End;
    End;

end;

procedure TObjGerador.Get_listaNomeGeradores(parametro: TStrings);
begin
     With Self.Objdataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select nome from TabGerador');
          open;
          first;
          parametro.clear;
          While Not(eof) do
          Begin
               parametro.add(fieldbyname('Nome').asstring);
               next;
          End;
    End;

end;
//Se Retornar 0 N�o encontrou
//Caso Contrario retorna o C�digo do Registro

function TObjGerador.LocalizaCodigoporObjeto(Parametro: string): Integer;
begin
     With Self.ObjDataset do
     Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo from TabGerador where Objeto='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=FieldByname('codigo').asinteger
           Else Result:=0;
     End;

end;

function TObjGerador.Get_ContaGerencial: string;
begin
     Result:=Self.ContaGerencial.get_codigo;
end;

procedure TObjGerador.Submit_ContaGerencial(parametro: string);
begin
    Self.ContaGerencial.submit_codigo(Parametro);
end;

function TObjGerador.Get_PesquisaContaGerencial: string;
begin
     Result:=Self.ContaGerencial.Get_Pesquisa;
end;

function TObjGerador.Get_TituloPesquisaContaGerencial: string;
begin
    Result:=Self.ContaGerencial.Get_TituloPesquisa;
end;

function TObjGerador.LocalizaContaGerencialPorObjeto(
  Parametro: string): Integer;
begin

     With Self.ObjDataset do
     Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select ContaGerencial from TabGerador where Objeto='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=FieldByname('ContaGerencial').asinteger
           Else Result:=0;
     End;


end;

destructor TObjGerador.Free;
begin
    Self.ContaGerencial.FREE;
    Freeandnil(Self.ObjDataset);

end;

function TObjgerador.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_GERADOR';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
           result:='0';
           exit;

        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;

end.
