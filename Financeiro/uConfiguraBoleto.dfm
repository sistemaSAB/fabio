object FConfiguraBoleto: TFConfiguraBoleto
  Left = 264
  Top = 179
  Width = 708
  Height = 460
  Caption = 'Configura'#231#227'o das Posi'#231#245'es do Boleto Banc'#225'rio'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnMouseMove = LbDataEmissaoMouseMove
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LbParcela: TLabel
    Left = 400
    Top = 15
    Width = 36
    Height = 13
    Caption = 'Parcela'
    ParentShowHint = False
    ShowHint = True
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object LbDataEmissao: TLabel
    Left = 15
    Top = 15
    Width = 39
    Height = 13
    Caption = 'Emissao'
    DragCursor = crSizeAll
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = False
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object LbNumeroDocumento: TLabel
    Left = 110
    Top = 15
    Width = 67
    Height = 13
    Caption = 'N'#186'Documento'
    ParentShowHint = False
    ShowHint = True
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object LbValor: TLabel
    Left = 400
    Top = 60
    Width = 24
    Height = 13
    Caption = 'Valor'
    ParentShowHint = False
    ShowHint = True
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object LbVencimento: TLabel
    Left = 480
    Top = 15
    Width = 56
    Height = 13
    Caption = 'Vencimento'
    ParentShowHint = False
    ShowHint = True
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object LbInstrucoes: TLabel
    Left = 15
    Top = 90
    Width = 70
    Height = 13
    Caption = 'INSTRU'#199#213'ES'
    DragCursor = crSizeAll
    ParentShowHint = False
    ShowHint = True
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object LbSacado: TLabel
    Left = 30
    Top = 160
    Width = 44
    Height = 13
    Caption = 'SACADO'
    DragCursor = crSizeAll
    ParentShowHint = False
    ShowHint = True
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object LbOutroValor: TLabel
    Left = 400
    Top = 84
    Width = 69
    Height = 13
    Caption = 'Outros Valores'
    ParentShowHint = False
    ShowHint = True
    OnClick = LbDataEmissaoClick
    OnMouseMove = LbDataEmissaoMouseMove
  end
  object PanelPosicoes: TPanel
    Left = 0
    Top = 365
    Width = 692
    Height = 57
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenu
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object LBLEFT: TLabel
      Left = 8
      Top = 8
      Width = 61
      Height = 13
      Caption = 'Esquerda'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object LBTOP: TLabel
      Left = 8
      Top = 32
      Width = 32
      Height = 13
      Caption = 'Topo'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object LBQUANTIDADE: TLabel
      Left = 184
      Top = 32
      Width = 74
      Height = 13
      Caption = 'Quantidade'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object edtleft: TMaskEdit
      Left = 72
      Top = 4
      Width = 108
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnKeyPress = edtleftKeyPress
    end
    object edttop: TMaskEdit
      Left = 72
      Top = 28
      Width = 108
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnKeyPress = edttopKeyPress
    end
    object Btsalvar: TBitBtn
      Left = 406
      Top = 3
      Width = 90
      Height = 50
      Caption = 'Gravar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtsalvarClick
    end
    object btimprime: TBitBtn
      Left = 497
      Top = 3
      Width = 90
      Height = 50
      Caption = 'Imprimir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btimprimeClick
    end
    object edtquantidade: TMaskEdit
      Left = 264
      Top = 28
      Width = 108
      Height = 21
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnKeyPress = edtquantidadeKeyPress
    end
    object CheckUSA: TCheckBox
      Left = 184
      Top = 8
      Width = 97
      Height = 17
      Caption = 'Usar'
      Color = clActiveBorder
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 5
      OnClick = CheckUSAClick
    end
    object BitBtn1: TBitBtn
      Left = 588
      Top = 4
      Width = 90
      Height = 50
      Caption = 'Folha Guia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = BitBtn1Click
    end
  end
end
