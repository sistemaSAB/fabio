unit UObjBoletoBancario;
Interface
Uses windows,stdctrls,Classes,Db,UessencialGlobal,Ibcustomdataset
, UobjCONVENIOSBOLETO, UObjArquivoRemessaRetorno, Grids;


Type
   TObjBoletoBancario=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                Convenioboleto:TObjCONVENIOSBOLETO;
                ArquivoRemessaRetorno:TOBJArquivoRemessaRetorno;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean;PultimoNumero:string)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaBoletoConvenio(PNumeroBoleto,PConvenio:string):Boolean;overload;
                function    LocalizaBoletoBanco(PNumeroBoleto: string;PNumeroBanco:string): Boolean;overload;

                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_PesquisaNaoUSados           :TStringList;overload;
                Function    Get_PesquisaNaoUSados(PConvenioboleto:string):TStringList;overload;
                function    Get_PesquisaUSados: TStringList;

                Function    Get_TituloPesquisa              :string;

                Function    TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Situacao(parametro: string);
                Function Get_Situacao: string;
                Procedure Submit_Historico(parametro: string);
                Function Get_Historico: string;
                Function Get_NumeroBoleto:string;
                Procedure Submit_NumeroBoleto(parametro:string);

                Function Get_ObservacoesSuperior:String;
                Procedure Submit_ObservacoesSuperior(parametro:String);

                Function Get_CobreBem:String;
                Procedure Submit_CobreBem(Parametro:String);

                Function Get_Vencimento:string;
                Function Get_DataDoc:string;
                Function Get_DataProc:string;
                Function Get_NumeroDoc :string;
                Function Get_ValorDoc :string;
                Function Get_Desconto  :string;
                Function Get_OutrasDeducoes:string;
                Function Get_Juros  :string;
                Function Get_outrosacrescimos:string;
                Function Get_ValorCobrado:string;
                Function Get_DataPagamento:string;

                Procedure Submit_Vencimento(parametro:string);
                Procedure Submit_DataDoc(parametro:string);
                Procedure Submit_DataProc(parametro:string);
                Procedure Submit_NumeroDoc(parametro:string);
                Procedure Submit_ValorDoc (parametro:string);
                Procedure Submit_Desconto  (parametro:string);
                Procedure Submit_OutrasDeducoes(parametro:string);
                Procedure Submit_Juros  (parametro:string);
                Procedure Submit_outrosacrescimos(parametro:string);
                Procedure Submit_ValorCobrado(parametro:string);
                Procedure Submit_DataPagamento(parametro:string);

                Function Get_NossoNumeroCobreBem:String;
                Procedure Submit_NossoNumeroCobreBem(Parametro:String);


                Function Get_Instrucoes:String;
                Procedure Submit_Instrucoes(parametro:String);

                //CODIFICA DECLARA GETSESUBMITS
                Procedure Resgatanumeroboletos(StrPend,StrBoleto:TStringList;PcodigoConvenio:string);
                Procedure pegaproximosnaousados(PcodigoAtual:string;StrList:TStringList;Quantidade:integer);
                procedure edtconvenioboletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LbNome: TLabel);overload;
                procedure edtconvenioboletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure edtconvenioboletoExit(Sender: TObject; LbNome: TLabel);
                Procedure Opcoes(pcodigo:string);
                //*************************************************************
                function  GeraNUmeroBB: Boolean;
                function  GeraNumeroBradesco: Boolean;
                function  GeraNumeroCaixaEconomica: Boolean;
                function  GeraNumeroUnibanco: Boolean;
                function  GeraNumeroSicred: Boolean;
                function  GeraNumeroHSBC : boolean;
                //************************************************************

                Function  GeranossoNumero:boolean;
                Procedure ImprimeBoletosEmitidos;

                //**** F�bio ***************
                procedure PreencheStrGridBoletosSemRemessaCobreBem(PConvenio: String; Var PStrGrid:TStringGrid);
                procedure PreencheStrGridBoletosCOMRemessaCobreBem(Boleto: String;var PStrGrid: TStringGrid);
                procedure edtBoletoPorConvenioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; PConvenio:String);
                procedure edtBoletoPorConvenioExit(Sender: TObject; LbNome: TLabel; PConvenio:String);
                procedure MostraHistorico(var PMemo: TMemo; PBoletoBancario: String);

         Private
               ObjDataset:Tibdataset;
               CODIGO:string;
               NumeroBoleto:string;
               Situacao:string;
               Historico:string;

               Vencimento:string;
               DataDoc:string;
               DataProc:string;
               NumeroDoc:string;
               ValorDoc :string;
               Desconto  :string;
               OutrasDeducoes:string;
               Juros  :string;
               outrosacrescimos:string;
               ValorCobrado:string;
               DataPagamento:string;
               CobreBem:String;
               Quitado:String;
               NossoNumeroCobreBem:String;

               Instrucoes:String;//1000
               ObservacoesSuperior:String;//2500

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function Get_NovoCodigo: string;

                procedure ExcluiVariosBoletos;
                procedure edtboletoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
                function Modulo11_2_a_9_Sicred_nossoNumero(PValor: String): string;
    function RetornaNomeCredorDevedo(PCodigo: String): String;



   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Ibquery,Controls,uboletobancario,uescolheboletos,
  UCONVENIOSBOLETO, Forms, UReltxtRDPRINT,rdprint;



Function  TObjBoletoBancario.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Situacao:=fieldbyname('Situacao').asstring;
        Self.Historico:=fieldbyname('Historico').asstring;
        Self.NumeroBoleto:=fieldbyname('numeroboleto').asstring;

        Self.Vencimento      :=fieldbyname('Vencimento').asstring;
        Self.DataDoc         :=fieldbyname('DataDoc').asstring;
        Self.DataProc        :=fieldbyname('DataProc').asstring;
        Self.NumeroDoc       :=fieldbyname('NumeroDoc').asstring;
        Self.ValorDoc        :=fieldbyname('ValorDoc').asstring;
        Self.Desconto        :=fieldbyname('Desconto').asstring;
        Self.OutrasDeducoes  :=fieldbyname('OutrasDeducoes').asstring;
        Self.Juros           :=fieldbyname('Juros').asstring;
        Self.outrosacrescimos:=fieldbyname('outrosacrescimos').asstring;
        Self.ValorCobrado    :=fieldbyname('ValorCobrado').asstring;
        Self.DataPagamento   :=fieldbyname('DataPagamento').asstring;
        Self.Instrucoes      :=fieldbyname('Instrucoes').asstring;
        Self.ObservacoesSuperior:=Fieldbyname('ObservacoesSuperior').asstring;

        if (fieldbyname('Convenioboleto').asstring<>'')
        Then Begin
                  if (Self.Convenioboleto.LocalizaCodigo(fieldbyname('Convenioboleto').asstring)=False)
                  Then begin
                            Self.ZerarTabela;
                            Messagedlg('Conv�nio N� '+fieldbyname('Convenioboleto').asstring+' n�o encontrado',mterror,[mbok],0);
                            exit;
                  End
                  Else Self.Convenioboleto.TabelaparaObjeto;
        End;

        if (fieldbyname('ArquivoRemessaRetorno').asstring<>'')
        Then Begin
                  if (Self.ArquivoRemessaRetorno.LocalizaCodigo(fieldbyname('ArquivoRemessaRetorno').asstring)=False)
                  Then begin
                            Self.ZerarTabela;
                            Messagedlg('ArquivoRemessaRetorno N� '+fieldbyname('ArquivoRemessaRetorno').asstring+' n�o encontrado',mterror,[mbok],0);
                            exit;
                  End
                  Else Self.ArquivoRemessaRetorno.TabelaparaObjeto(false);
        End;

        Self.CobreBem:=Fieldbyname('CobreBem').AsString;
        Self.NossoNumeroCobreBem:=fieldbyname('NossoNumeroCobreBem').AsString;
        result:=True;
     End;

end;

Procedure TObjBoletoBancario.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring            :=Self.CODIGO;
        fieldbyname('Situacao').asstring          :=Self.Situacao;
        fieldbyname('Historico').asstring         :=Self.Historico;
        fieldbyname('numeroboleto').asstring      :=Self.NumeroBoleto;
        fieldbyname('ConvenioBoleto').asstring    :=Self.Convenioboleto.Get_CODIGO;
        fieldbyname('Vencimento').asstring        :=Self.Vencimento       ;
        fieldbyname('DataDoc').asstring           :=Self.DataDoc          ;
        fieldbyname('DataProc').asstring          :=Self.DataProc         ;
        fieldbyname('NumeroDoc').asstring         :=Self.NumeroDoc        ;
        fieldbyname('ValorDoc').asstring          :=Self.ValorDoc         ;
        fieldbyname('Desconto').asstring          :=Self.Desconto         ;
        fieldbyname('OutrasDeducoes').asstring    :=Self.OutrasDeducoes   ;
        fieldbyname('Juros').asstring             :=Self.Juros            ;
        fieldbyname('outrosacrescimos').asstring  :=Self.outrosacrescimos ;
        fieldbyname('ValorCobrado').asstring      :=Self.ValorCobrado     ;
        fieldbyname('DataPagamento').asstring     :=Self.DataPagamento    ;
        fieldbyname('Instrucoes').asstring        :=Self.Instrucoes       ;
        fieldbyname('ObservacoesSuperior').asstring:=Self.ObservacoesSuperior;
        fieldbyname('CobreBem').AsString          :=Self.CobreBem;
        fieldbyname('ArquivoRemessaRetorno').AsString:=Self.ArquivoRemessaRetorno.Get_Codigo;
        fieldbyname('NossoNumeroCobreBem').AsString:=Self.NossoNumeroCobreBem;
  End;
End;

//***********************************************************************

function TObjBoletoBancario.Salvar(ComCommit:Boolean;PultimoNumero:string): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
var
diferenca:Int64;
cont:integer;
pzerosaesquerda:String;
begin
  result:=False;
  
  if (Self.VerificaBrancos=True)
  Then Begin
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
                  End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
                  End;
   End;

   if (PultimoNumero='')
   Then diferenca:=0
   Else Begin
             Try
                StrToInt64(PultimoNumero);
                strtoint64(Self.NumeroBoleto);
             except
                on e:exception  do
                begin
                    Messagedlg(E.message,mterror,[mbok],0);
                    exit;
                End;
             end;

             diferenca:=strtoint64(PultimoNumero)-strtoint64(Self.NumeroBoleto);
   End;

   PZerosaEsquerda:='';
   for cont:=1 to length(Self.NumeroBoleto) do
   Begin
        if (Self.NumeroBoleto[cont]='0')
        Then PZerosaEsquerda:=PZerosaEsquerda+'0'
        Else Break;
   end;



  For cont:=0 to diferenca do
  Begin
       if (Cont>0)
       Then Self.NumeroBoleto:=PZerosaEsquerda+IntToStr(StrToInt64(Self.NumeroBoleto)+1);

       if Self.status=dsinsert
       then Begin
                Self.CODIGO:=Self.Get_NovoCodigo;

                if (Self.LocalizaBoletoConvenio(Self.NumeroBoleto,Self.Convenioboleto.get_codigo)=true)
                Then begin
                          Messagedlg('J� existe um boleto com esse n�mero para este conv�nio',mterror,[mbok],0);
                          exit;
                End;

                Self.ObjDataset.Insert;//libera para insercao
       End
       Else Begin
                 if (Self.Status=dsedit)
                 Then Self.ObjDataset.edit//se for edicao libera para tal
                 else Begin
                          mensagemerro('Status Inv�lido na Grava��o');
                          exit;
                 End;
       End;

       Self.ObjetoParaTabela;

       Try
          Self.ObjDataset.Post;
       Except
            on e:Exception do
            Begin
                Messagedlg('Erro ao cadastrar o Boleto  '+Self.codigo+#13+E.message,mterror,[mbok],0);
                If (ComCommit=True)
                Then FDataModulo.IBTransaction.RollbackRetaining;
                result:=False;
                exit;
            End;
       End;
  End;


  If ComCommit=True
  Then FDataModulo.IBTransaction.CommitRetaining;

  Self.status          :=dsInactive;
  result:=True;

end;

procedure TObjBoletoBancario.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        CODIGO:='';
        Situacao:='';
        Historico:='';
        NumeroBoleto:='';
        ConvenioBoleto.ZerarTabela;
        Self.Vencimento      :='';
        Self.DataDoc         :='';
        Self.DataProc        :='';
        Self.NumeroDoc       :='';
        Self.ValorDoc        :='';
        Self.Desconto        :='';
        Self.OutrasDeducoes  :='';
        Self.Juros           :='';
        Self.outrosacrescimos:='';
        Self.ValorCobrado    :='';
        Self.DataPagamento   :='';
        Self.Instrucoes      :='';
        Self.ObservacoesSuperior:='';
        Self.CobreBem:='';
        Self.ArquivoRemessaRetorno.ZerarTabela;
        Self.NossoNumeroCobreBem:='';

     End;
end;

Function TObjBoletoBancario.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      If (Situacao='')
      Then Mensagem:=mensagem+'/Situacao';

      If (Convenioboleto.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Conv�nio Boleto';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjBoletoBancario.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     if (Self.Convenioboleto.Get_CODIGO<>'')
     Then Begin
              if (Self.Convenioboleto.LocalizaCodigo(Self.Convenioboleto.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'\Conv�nio n�o localizado';
     End;

     if (Self.ArquivoRemessaRetorno.Get_Codigo <> '')
     then Begin
              if (Self.ArquivoRemessaRetorno.LocalizaCodigo(Self.ArquivoRemessaRetorno.Get_Codigo)=false)
              then Mensagem:=Mensagem+'\ArquivoRemessaRetorno';
     end;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjBoletoBancario.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;

     try
        If (Self.Convenioboleto.Get_CODIGO<>'')
        Then Strtoint(Self.Convenioboleto.Get_CODIGO);
     Except
           mensagem:=Mensagem+'\Conv�nio';
     End;

     Try
        if (Self.ValorDoc<>'')
        Then Strtofloat(Self.ValorDoc);
     Except
           Mensagem:=mensagem+'\Valor de Documento';
     End;

     Try
        if (Self.Desconto<>'')
        Then strtofloat(Self.Desconto);
     Except
           Mensagem:=Mensagem+'\Desconto';
     End;

     Try
        if (Self.OutrasDeducoes<>'')
        Then strtofloat(Self.OutrasDeducoes);
     Except
           Mensagem:=Mensagem+'\Outras Dedu��es';
     End;

     Try
        if (Self.Juros<>'')
        Then strtofloat(Self.Juros);
     Except
           Mensagem:=Mensagem+'\Juros';
     End;

     Try
        if (Self.outrosacrescimos<>'')
        Then strtofloat(Self.outrosacrescimos);
     Except
           Mensagem:=Mensagem+'\Outros Acr�scimos';
     End;

     Try
        if (Self.ValorCobrado<>'')
        Then strtofloat(Self.ValorCobrado);
     Except
           Mensagem:=Mensagem+'\Valor Cobrado';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
     End;
     result:=true;

end;

function TObjBoletoBancario.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA
     Try
        if (comebarra(trim(Self.Vencimento))<>'')
        Then strtodate(Self.vencimento)
        Else Self.Vencimento:='';
     Except
           Mensagem:=Mensagem+'\Vencimento';
     End;

     Try
        if (comebarra(trim(Self.DataDoc))<>'')
        Then strtodate(Self.DataDoc)
        Else Self.Datadoc:='';
     Except
           Mensagem:=Mensagem+'\Data do Documento';
     End;

     Try
        if (comebarra(trim(Self.Dataproc))<>'')
        Then strtodate(Self.DataProc)
        Else Self.Dataproc:='';
     Except
           Mensagem:=Mensagem+'\Data de Processamento';
     End;

     Try
        if (comebarra(trim(Self.DataPagamento))<>'')
        Then strtodate(Self.Datapagamento)
        Else Self.DataPagamento:='';
     Except
           Mensagem:=Mensagem+'\Data de Pagamento';
     End;
     
     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjBoletoBancario.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
      If not((Self.SITUACAO='I') OR (self.SITUACAO='R') OR (self.SITUACAO='N'))
      Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Situacao';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;


function TObjBoletoBancario.LocalizaBoletoConvenio(PNumeroBoleto,PConvenio: string): Boolean;
Begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,numeroboleto,Situacao,Historico,convenioboleto');
           SelectSQL.ADD(',Vencimento,DataDoc,DataProc,NumeroDoc,ValorDoc,');
           SelectSQL.ADD('Desconto,OutrasDeducoes,Juros,outrosacrescimos,');
           SelectSQL.ADD('ValorCobrado,DataPagamento,instrucoes,ObservacoesSuperior,CobreBem,ArquivoRemessaRetorno,NossoNumeroCobreBem');
           SelectSQL.ADD('from  TABBOLETOBANCARIO');
           SelectSQL.ADD('WHERE numeroboleto='+#39+PNumeroBoleto+#39+' and Convenioboleto='+PConvenio);
          // inputbox('','',SelectSQL.Text);
           Open;
           last;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjBoletoBancario.LocalizaBoletoBanco(PNumeroBoleto: string;PNumeroBanco:string): Boolean;
Begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select TABBOLETOBANCARIO.CODIGO,TABBOLETOBANCARIO.numeroboleto,TABBOLETOBANCARIO.Situacao,');
           SelectSQL.ADD('TABBOLETOBANCARIO.Historico,TABBOLETOBANCARIO.convenioboleto');
           SelectSQL.ADD(',TABBOLETOBANCARIO.Vencimento,TABBOLETOBANCARIO.DataDoc,TABBOLETOBANCARIO.DataProc,TABBOLETOBANCARIO.NumeroDoc,TABBOLETOBANCARIO.ValorDoc,');
           SelectSQL.ADD('TABBOLETOBANCARIO.Desconto,TABBOLETOBANCARIO.OutrasDeducoes,TABBOLETOBANCARIO.Juros,TABBOLETOBANCARIO.outrosacrescimos,');
           SelectSQL.ADD('TABBOLETOBANCARIO.ValorCobrado,TABBOLETOBANCARIO.DataPagamento,TABBOLETOBANCARIO.instrucoes,TABBOLETOBANCARIO.ObservacoesSuperior,');
           SelectSQL.ADD('TABBOLETOBANCARIO.CobreBem, TABBOLETOBANCARIO.ArquivoRemessaRetorno, TabBoletoBancario.NossoNumeroCobreBem');
           SelectSQL.ADD('from  TABBOLETOBANCARIO');
           SelectSQL.ADD('join tabconveniosboleto on tabboletobancario.convenioboleto=tabconveniosboleto.codigo');
           SelectSQL.ADD('WHERE numeroboleto='+#39+PNumeroBoleto+#39);
           SelectSQL.ADD('and Tabconveniosboleto.codigobanco='+#39+Pnumerobanco+#39);

           Open;
           last;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjBoletoBancario.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,numeroboleto,Situacao,Historico,convenioboleto,');
           SelectSQL.ADD('Vencimento,DataDoc,DataProc,NumeroDoc,ValorDoc,');
           SelectSQL.ADD('Desconto,OutrasDeducoes,Juros,outrosacrescimos,');
           SelectSQL.ADD('ValorCobrado,DataPagamento,instrucoes,ObservacoesSuperior,CobreBem,ArquivoRemessaRetorno,NossoNumeroCobreBem');
           SelectSQL.ADD('from TABBOLETOBANCARIO');
           SelectSQL.ADD('where CODIGO='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjBoletoBancario.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjBoletoBancario.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjBoletoBancario.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Convenioboleto:=TObjCONVENIOSBOLETO.create;
        Self.ArquivoRemessaRetorno:=TObjARQUIVOREMESSARETORNO.Create;
        Self.ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select CODIGO,numeroboleto,Situacao,Historico,convenioboleto');
                SelectSQL.ADD(',Vencimento,DataDoc,DataProc,NumeroDoc,ValorDoc,');
                SelectSQL.ADD('Desconto,OutrasDeducoes,Juros,outrosacrescimos,');
                SelectSQL.ADD('ValorCobrado,DataPagamento,instrucoes,ObservacoesSuperior,CobreBem,ArquivoRemessaRetorno,NossoNumeroCobreBem');
                SelectSQL.ADD(' from  TABBOLETOBANCARIO');
                SelectSQL.ADD(' WHERE CODIGO=:codigo');

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TABBOLETOBANCARIO(CODIGO,numeroboleto,Situacao,Historico,convenioboleto');
                InsertSQL.add(',Vencimento,DataDoc,DataProc,NumeroDoc,ValorDoc,');
                InsertSQL.add('Desconto,OutrasDeducoes,Juros,outrosacrescimos,');
                InsertSQL.add('ValorCobrado,DataPagamento,instrucoes,ObservacoesSuperior,CobreBem,ArquivoRemessaRetorno,NossoNumeroCobreBem)');
                InsertSQL.add('values (:CODIGO,:numeroboleto,:Situacao,:Historico,:convenioboleto');
                InsertSQL.add(',:Vencimento,:DataDoc,:DataProc,:NumeroDoc,:ValorDoc,');
                InsertSQL.add(':Desconto,:OutrasDeducoes,:Juros,:outrosacrescimos,');
                InsertSQL.add(':ValorCobrado,:DataPagamento,:instrucoes,:ObservacoesSuperior,:CobreBem,:ArquivoRemessaRetorno,:NossoNumeroCobreBem)');

                ModifySQL.clear;
                ModifySQL.add('Update TABBOLETOBANCARIO set CODIGO=:CODIGO,numeroboleto=:numeroboleto,Situacao=:Situacao');
                ModifySQL.add(',Historico=:Historico,convenioboleto=:convenioboleto');
                ModifySQL.add(',Vencimento=:Vencimento,DataDoc=:DataDoc,DataProc=:DataProc,NumeroDoc=:NumeroDoc,ValorDoc=:ValorDoc,');
                ModifySQL.add('Desconto=:Desconto,OutrasDeducoes=:OutrasDeducoes,Juros=:Juros,outrosacrescimos=:outrosacrescimos,');
                ModifySQL.add('ValorCobrado=:ValorCobrado,DataPagamento=:DataPagamento,instrucoes=:instrucoes,ObservacoesSuperior=:ObservacoesSuperior,');
                ModifySQL.add('CobreBem=:CobreBem,ArquivoRemessaRetorno=:ArquivoRemessaRetorno,NossoNumeroCobreBem=:NossoNumeroCobreBem');
                ModifySQL.add('Where CODIGO=:CODIGO');

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABBOLETOBANCARIO where CODIGO=:CODIGO ');

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,Situacao,Historico,convenioboleto');
                RefreshSQL.ADD(',Vencimento,DataDoc,DataProc,NumeroDoc,ValorDoc,');
                RefreshSQL.ADD('Desconto,OutrasDeducoes,Juros,outrosacrescimos,');
                RefreshSQL.ADD('ValorCobrado,DataPagamento,instrucoes,ObservacoesSuperior,CobreBem,ArquivoRemessaRetorno,NossoNumeroCobreBem');
                RefreshSQL.ADD('from  TABBOLETOBANCARIO');
                RefreshSQL.ADD('WHERE CODIGO=:codigo');

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjBoletoBancario.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;
function TObjBoletoBancario.Get_PesquisaNaoUSados: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabBoletoBancario where Situacao=''N'' ');
     Result:=Self.ParametroPesquisa;

end;

function TObjBoletoBancario.Get_PesquisaUSados: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabBoletoBancario where Situacao=''I'' ');
     Result:=Self.ParametroPesquisa;

end;


function TObjBoletoBancario.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabBoletoBancario');
     Result:=Self.ParametroPesquisa;
end;

function TObjBoletoBancario.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Boleto Banc�rio ';
end;

destructor TObjBoletoBancario.Free;
begin
    Freeandnil(Self.ObjDataset);
    Freeandnil(Self.ParametroPesquisa);
    Self.Convenioboleto.free;
    Self.ArquivoRemessaRetorno.Free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjBoletoBancario.RetornaCampoCodigo: string;
begin
      result:='CODIGO';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjBoletoBancario.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TOBJBOLETOBANCARIO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TOBJBOLETOBANCARIO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TOBJBOLETOBANCARIO.Submit_Situacao(parametro: string);
begin
        Self.Situacao:=Parametro;
end;
function TOBJBOLETOBANCARIO.Get_Situacao: string;
begin
        Result:=Self.Situacao;
end;
procedure TOBJBOLETOBANCARIO.Submit_Historico(parametro: string);
begin
        Self.Historico:=Parametro;
end;
function TOBJBOLETOBANCARIO.Get_Historico: string;
begin
        Result:=Self.Historico;
end;
//CODIFICA GETSESUBMITS

//CODIFICA EXITONKEYDOWN





procedure TObjBoletoBancario.pegaproximosnaousados(PcodigoAtual: string;
  StrList: TStringList; Quantidade: integer);
var
cont:Integer;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select codigo from tabboletobancario where codigo>'+PCodigoAtual);
          SelectSQL.add('and SITUACAO=''N'' order by codigo');
          open;
          first;
          cont:=0;
          while (cont<Quantidade) do
          Begin
               If (not(eof))
               Then Begin
                        StrList.add(fieldbyname('codigo').asstring);
                        next;
               End
               Else StrList.add('');
               inc(cont,1);
          End;
     End;
end;


procedure TObjBoletoBancario.Resgatanumeroboletos(StrPend,StrBoleto: TStringList;PcodigoConvenio:string);
var
   vazio,cont,cont2:integer;
begin
     //inicialmente a StrList vem preenchida com as pendencias
     //que terei que escolher o boleto.
     //tenho que retornar um boleto para cada pend�ncia
     StrBoleto.clear;//desta forma no retorna da funcao, saberei que naum foi escolhido nenhum
     //1. primeiro descobrindo se h� boletos suficiente para todas as pendencias
     With Self.objdataset do
     Begin
          Repeat
                  close;
                  SelectSQL.clear;
                  SelectSQL.add('Select count(codigo) as conta from TabBoletoBancario where situacao=''N'' ');
                  SelectSQL.add('and ConvenioBoleto='+PcodigoConvenio);
                  open;

                  If (Fieldbyname('conta').asinteger<StrPend.count)
                  Then Begin
                            If (Messagedlg('N�o h� boletos para todas as pend�ncias!'+#13+'Deseja cadastrar mais boletos?',mtconfirmation,[mbyes,mbno],0)=Mrno)
                            Then exit
                            Else Fboletobancario.showmodal;
                  End;
          Until(Fieldbyname('conta').asinteger>=StrPend.count);
     End;
     
     //definindo a quantidade de linhas para o grid de escolha de boletos
     FEscolheBoletos.Grid.rowcount:=StrPend.count+1;
     //preenchendo os numeros das pend.
     for cont:=0 to strpend.count-1 do
        FEscolheBoletos.Grid.cells[1,cont+1]:=StrPend[cont];//coluna com a pendencia
     //definindo titulos
     FEscolheBoletos.Grid.Cols[2].clear;//coluna do N� do boleto
     FEscolheBoletos.Grid.Cols[0].clear;//coluna do X
     FEscolheBoletos.Grid.Cells[0,0]:='Marca';
     FEscolheBoletos.Grid.Cells[1,0]:='Pend�ncia';
     FEscolheBoletos.Grid.Cells[2,0]:='Boleto';
     AjustaLArguraColunaGrid(FEscolheBoletos.Grid);
     
     Repeat
             FEscolheBoletos.edtcodigoconvenio.Text:=PcodigoConvenio;
             FEscolheBoletos.ShowModal;
             //de posso dos codigos escolhidos verifico se naum tem nenhum igual
             //ou se faltou algum
             vazio:=0;
             for cont:=1 to FEscolheBoletos.Grid.RowCount-1 do
             Begin
                  If (FEscolheBoletos.Grid.cells[2,cont]='')
                  Then inc(vazio,1);
             End;
             //se a quantidade de vazio for igual ao numero de linhas significa que
             //naum escolheu nenhum
             If (Vazio>=FEscolheBoletos.Grid.RowCount-1)
             Then Begin
                       Messagedlg('N�o foi escolhido nenhum boleto, n�o � poss�vel continuar!',mterror,[mbok],0);
                       exit;
             End
             Else Begin
                   If (Vazio>0)//tem vazio mas nem todos
                   Then Begin
                             Messagedlg('N�o � permitido deixar pend�ncias sem boletos!'+#13+
                                         'Escolha boletos para todas ou zere todas para cancelar a emiss�o dos boletos!',mtinformation,[mbok],0);
                   End
                   Else Begin//vazio=0
                            //verificando se naum tem nenhum igual
                            for cont:=1 to FEscolheBoletos.Grid.RowCount-1 do
                            Begin
                                 for cont2:=cont+1 to FEscolheBoletos.Grid.RowCount-1 do
                                 Begin
                                      If (FescolheBoletos.grid.Cells[2,cont]=Fescolheboletos.grid.cells[2,cont2])//tem iguais
                                      Then Begin
                                                Messagedlg('Existem pend�ncias com o mesmo n� de boleto, isto n�o � permitido!'+#13+
                                                           'Escolha boletos para todas ou zere todas para cancelar a emiss�o dos boletos!',mtinformation,[mbok],0);
                                                vazio:=1;
                                                break;
                                      End;
                                 End;//cont2
                            End;//cont1
                   End;//else 2
             End;//else 1
     Until(vazio=0);

     for cont:=1 to FEscolheBoletos.Grid.RowCount-1 do
     Begin
          IF (FEscolheBoletos.Grid.cells[2,cont]<>'')
          Then StrBoleto.add(FEscolheBoletos.Grid.cells[2,cont]);
     End;
end;



function TObjBoletoBancario.Get_NumeroBoleto: string;
begin
     Result:=Self.NumeroBoleto;
end;

procedure TObjBoletoBancario.Submit_NumeroBoleto(parametro: string);
begin
     Self.NumeroBoleto:=parametro;
end;
procedure TObjBoletoBancario.edtconvenioboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FconveniosBoleto1:TFCONVENIOSBOLETO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FconveniosBoleto1:=TFCONVENIOSBOLETO.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Convenioboleto.Get_Pesquisa,Self.Convenioboleto.Get_TituloPesquisa,FconveniosBoleto1)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FconveniosBoleto1);
     End;
end;

procedure TObjBoletoBancario.edtconvenioboletoKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState;LbNome:TLabel);
var
   FpesquisaLocal:Tfpesquisa;
   FconveniosBoleto1:TFCONVENIOSBOLETO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FconveniosBoleto1:=TFCONVENIOSBOLETO.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Convenioboleto.Get_Pesquisa,Self.Convenioboleto.Get_TituloPesquisa,FconveniosBoleto1)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FconveniosBoleto1);
     End;
end;

procedure TObjBoletoBancario.edtconvenioboletoExit(Sender: TObject;
  LbNome: TLabel);
begin
     LbNome.caption:='';
     if (TEdit(Sender).text='')
     Then exit;

     Try
        strtoint(TEdit(Sender).text);
     Except
           TEdit(Sender).text:='';
     End;

     if (Self.Convenioboleto.LocalizaCodigo(TEdit(Sender).text)=False)
     Then begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Convenioboleto.TabelaparaObjeto;
     LbNome.caption:=Self.Convenioboleto.Get_Nome;
end;
function TObjboletobancario.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENBOLETOBANCARIO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;

function TObjBoletoBancario.Get_DataDoc: string;
begin
     Result:=Self.DataDoc;
end;

function TObjBoletoBancario.Get_DataPagamento: string;
begin
     Result:=Self.DataPagamento;
end;

function TObjBoletoBancario.Get_DataProc: string;
begin
     Result:=Self.DataProc;
end;

function TObjBoletoBancario.Get_Desconto: string;
begin
     Result:=Self.Desconto;
end;

function TObjBoletoBancario.Get_Juros: string;
begin
     Result:=Self.Juros;
end;

function TObjBoletoBancario.Get_NumeroDoc: string;
begin
     Result:=Self.NumeroDoc;
end;

function TObjBoletoBancario.Get_OutrasDeducoes: string;
begin
     Result:=Self.OutrasDeducoes;
end;

function TObjBoletoBancario.Get_outrosacrescimos: string;
begin
     Result:=Self.OutrosAcrescimos;
end;

function TObjBoletoBancario.Get_ValorCobrado: string;
begin
     Result:=Self.ValorCobrado;
end;

function TObjBoletoBancario.Get_ValorDoc: string;
begin
     Result:=Self.ValorDoc;
end;

function TObjBoletoBancario.Get_Vencimento: string;
begin
     Result:=Self.vencimento;
end;

procedure TObjBoletoBancario.Submit_DataDoc(parametro: string);
begin
     Self.DataDoc:=parametro;
end;

procedure TObjBoletoBancario.Submit_DataPagamento(parametro: string);
begin
     Self.DataPagamento:=parametro;
end;

procedure TObjBoletoBancario.Submit_DataProc(parametro: string);
begin
     Self.DataProc:=parametro;
end;

procedure TObjBoletoBancario.Submit_Desconto(parametro: string);
begin
     Self.Desconto:=parametro;
end;

procedure TObjBoletoBancario.Submit_Juros(parametro: string);
begin
     Self.Juros:=parametro;
end;

procedure TObjBoletoBancario.Submit_NumeroDoc(parametro: string);
begin
     Self.NumeroDoc:=parametro;
end;

procedure TObjBoletoBancario.Submit_OutrasDeducoes(parametro: string);
begin
     Self.OutrasDeducoes:=parametro;
end;

procedure TObjBoletoBancario.Submit_outrosacrescimos(parametro: string);
begin
     Self.outrosacrescimos:=parametro;
end;

procedure TObjBoletoBancario.Submit_ValorCobrado(parametro: string);
begin
     Self.ValorCobrado:=parametro;
end;

procedure TObjBoletoBancario.Submit_ValorDoc(parametro: string);
begin
     if (Parametro='')
     Then Parametro:='0';
     Self.ValorDoc:=parametro;

end;

procedure TObjBoletoBancario.Submit_Vencimento(parametro: string);
begin
     Self.Vencimento:=parametro;
end;

function TObjBoletoBancario.Get_Instrucoes: String;
begin
     Result:=Self.instrucoes;
end;

procedure TObjBoletoBancario.Submit_Instrucoes(parametro: String);
begin
     Self.instrucoes:=parametro;
end;


procedure TObjBoletoBancario.Opcoes(pcodigo: string);
begin
     With FOpcaorel do
     begin
          RgOpcoes.items.clear;
          RgOpcoes.items.Add('Excluir v�rios boletos');
          showmodal;

          if (tag=0)
          Then exit;

          Case RgOpcoes.ItemIndex of
                    0 : Self.ExcluiVariosBoletos;
          end;

     End;
end;


Function TobjBoletoBancario.GeraNumeroCaixaEconomica:Boolean;
Var
  TmpQuantConvenio:integer;
Begin
     {No Campo Nosso N�mero Informar:

     82NNNNNNNN-D1 (no caso de Cobran�a sem Registro)
     9NNNNNNNNN-D1 (no cao de cobran�a r�pida)

     ronnei: ou seja 10 digitos - qtde digitos da carteira (9,80,81,82...)
     os 10 digitos tem q ter sido configurados no convenio (qtde digitos nosso numero)

     No Caso do espaco notebook sao 14 digitos, porem no calculo abaixo estou usando o length
     do codigo carteira, portanto configurar 1 a mais pois no codigo carteira ira o 8


     No Caso da FLevino que � SIGB  o c�digo de barras s�o 17 digitos
     02 para carteira e 15 para o cliente usar

     portanto tem que deixar 17 no convenio e preencher o codigo da carteira
     que neste exemplo � 24 (2 = sem registro e 4=emissao pelo cedente)

     }
     Result:=False;
     Self.NumeroBoleto:='';
     Self.NumeroBoleto:=CompletaPalavra_a_Esquerda(Self.Get_CODIGO,strtoint(Self.Convenioboleto.get_QtdeDigitosNossoNumero)-length(Self.Convenioboleto.Get_CodigoCarteira_Caixa),'0');
     result:=True;
End;

Function TobjBoletoBancario.GeraNumeroBradesco:Boolean;
Var
  TmpQuantConvenio:integer;
Begin
     Result:=False;
     Self.NumeroBoleto:='';
     Self.NumeroBoleto:=CompletaPalavra_a_Esquerda(Self.Get_CODIGO,strtoint(Self.Convenioboleto.get_QtdeDigitosNossoNumero),'0');
     result:=True;

End;

Function TobjBoletoBancario.GeraNumeroBB:Boolean;
Var
  TmpQuantConvenio:integer;
Begin
     Result:=False;
     Self.NumeroBoleto:='';

     //aqui depende da quantidade de digitos
     //na carteira 18 do Banco do Brasil, os boletos podem ter 11 ou 17 digitos no campo nosso numero
     //apenas pego o numero de digitos, descontos as posicoes do convenio

     //e gero zero a esquerda pro resto
     {

       03. COMPOSI��O DO NOSSO-N�MERO DOS BLOQUETOS DE COBRAN�A: Composi��o    +
       padr�o para todas as carteiras. Deve ser informado aos clientes     +
       das carteiras 16, 17 e 18, quando a impress�o estiver totalmente a  +
       seu cargo:                                                          +
       a) Conv�nio de quatro posi��es:                                     +
          I      - CCCCNNNNNNN-X                                           +
       b) Conv�nio de seis posi��es:                                       +
          I      - CCCCCCNNNNN-X                                           +
       c) Conv�nio de sete posi��es, numera��o superior a 1.000.000 (um    +
          milh�o):                                                         +
          I      - CCCCCCCNNNNNNNNNN                                       +
                   OBS: N�o existe DV - D�gito Verificador - na            +
                        composi��o do nosso-n�mero para conv�nios de sete  +
                        posi��es                                           +
          OBS:
               "C" - � o numero do conv�nio;
               "N" - � um seq�encial atribu�do pelo cliente;
               "X" - digito verificador /MODULO 11/.
       d) Na Carteira 18 - Tipo 21 - Cobran�a Sem Registro, o              +
          NOSSO-N�MERO poder� ter 17 (dezessete) posi��es livres. Para     +
          tanto, o cliente dever� observar as instru��es constantes do     +
          LIC#17.2.11.9991.                                                +}
     
     //O Numero do Boleto � Dado Pelo Codigo dele
     //No nosso numero usa-se o convenio e o numerodele
     //esse numero � preenchido com zeros a esquerda de acordo com qtde de digitos
     TmpQuantConvenio:=length(Self.Convenioboleto.Get_numeroconvenio);

     if (strtoint(Self.Convenioboleto.get_QtdeDigitosNossoNumero)<>11)
                and (strtoint(Self.Convenioboleto.get_QtdeDigitosNossoNumero)<>17)
     then Begin
               Messagedlg('O Campo Qtde de Digitos para Nosso Numero deve ser 11 ou 17 em Boletos do Banco do Brasil',mterror,[mbok],0);
               exit;
     End;
     Self.NumeroBoleto:=CompletaPalavra_a_Esquerda(Self.Get_CODIGO,strtoint(Self.Convenioboleto.get_QtdeDigitosNossoNumero)-TmpQuantConvenio,'0');
     result:=True;
End;



procedure TObjBoletoBancario.ImprimeBoletosEmitidos;
var
pdata1,pdata2:Tdate;
linha:integer;
Pconvenio:string;
begin
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          LbGrupo01.caption:='Data Proc. Inicial';
          LbGrupo02.caption:='Data Proc. Final';
          LbGrupo03.caption:='Conv�nio';
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo03.OnKeyDown:=Self.edtconvenioboletoKeyDown;

          Showmodal;

          if (Tag=0)
          then exit;

          Try
             Pdata1:=strtodate(edtgrupo01.text);
             Pdata2:=strtodate(edtgrupo02.text);
          Except
                Messagedlg('Datas Inv�lidas',mterror,[mbok],0);
                exit;
          end;

          try
             Pconvenio:='';
             if (edtgrupo03.text<>'')
             Then strtoint(edtgrupo03.text);

             Pconvenio:=edtgrupo03.text;

          Except
                Messagedlg('Conv�nio Inv�lido',mterror,[mbok],0);
                exit;
          end;

     end;

     with Self.ObjDataset do
     begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select tabboletobancario.*,tabconveniosboleto.nome as nomeconvenio from tabboletobancario');
          SelectSQL.add('join tabconveniosboleto on tabboletobancario.convenioboleto=tabconveniosboleto.codigo');
          SelectSQL.add('Where tabboletobancario.DataProc>='+#39+FormatDateTime('mm/dd/yyyy',pdata1)+#39);
          SelectSQL.add('and tabboletobancario.DataProc<='+#39+FormatDateTime('mm/dd/yyyy',pdata2)+#39);
          if (Pconvenio<>'')
          Then SelectSQL.add('and Tabboletobancario.convenioboleto='+Pconvenio);
          open;

          if (Recordcount=0)
          then Begin
                    Messagedlg('Nenhuma Informa��o foi selecionada',mterror,[mbok],0);
                    exit;
          End;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.rdprint.Abrir;

          if (FreltxtRDPRINT.RDprint.Setup=false)
          then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          end;
          linha:=3;

          FreltxtRDPRINT.RDprint.ImpC(linha,45,'BOLETOS EMITIDOS',[negrito]);
          inc(linha,2);
          
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Data de Processamento: '+datetostr(pdata1)+' a '+datetostr(pdata2),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('COD',5,' ')+' '+
                                              CompletaPalavra('NUM.BOL.',12,' ')+' '+
                                              CompletaPalavra('VENCTO',10,' ')+' '+
                                              CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                              CompletaPalavra('CONVENIO',34,' '),[NEGRITO]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);

          While not(eof) do
          begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+' '+
                                                  CompletaPalavra(fieldbyname('numeroboleto').asstring,12,' ')+' '+
                                                  CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valordoc').asstring),12,' ')+' '+
                                                  CompletaPalavra(fieldbyname('convenioboleto').asstring,3,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomeconvenio').asstring,30,' '));
               inc(linha,1);
               next;
          end;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.RDprint.fechar;
     end;
end;

function TObjBoletoBancario.Get_PesquisaNaoUSados(
  PConvenioboleto: string): TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabBoletoBancario where Situacao=''N'' ');
     Self.ParametroPesquisa.add('and ConvenioBoleto='+PconvenioBoleto);
     Result:=Self.ParametroPesquisa;

end;

function TObjBoletoBancario.GeranossoNumero: boolean;
begin
     result:=False;
     //*******GERANDO O NUMERO DO BOLETO DE ACORDO COM O BANCO
     IF (Self.Convenioboleto.Get_CodigoBAnco='001')//banco do brasil
     Then Result:=Self.GeraNumeroBB
     Else
          IF (Self.Convenioboleto.Get_CodigoBanco='237')//bradesco
          Then result:=Self.GeraNumeroBradesco
          Else
              IF (Self.Convenioboleto.Get_CodigoBanco='104')//caixa econ�mica
              Then result:=Self.GeraNumeroCaixaEconomica
              else
                  if (Self.ConvenioBoleto.get_CodigoBanco='409')
                  Then  result:=Self.geraNumeroUnibanco
                  else
                      if (Self.ConvenioBoleto.get_CodigoBanco='748')
                      Then  result:=Self.geraNumeroSicred
                      else
                            if (Self.ConvenioBoleto.get_CodigoBanco='399')
                            Then  result:=Self.geraNumeroHSBC


end;

function TObjBoletoBancario.GeraNumeroUnibanco: Boolean;
Var
  TmpQuantConvenio:integer;
Begin
     {No Campo Nosso N�mero Informar:

     14 digitos do nosso numero
     }
     Result:=False;
     Self.NumeroBoleto:='';
     Self.NumeroBoleto:=CompletaPalavra_a_Esquerda(Self.Get_CODIGO,strtoint(Self.Convenioboleto.get_QtdeDigitosNossoNumero),'0');
     result:=True;
End;

function TObjBoletoBancario.GeraNumeroSicred: Boolean;
Var
  temp:string;
Begin
     Result:=False;
     Self.NumeroBoleto:='';

(*

Gera��o pelo Cedente

-A parte sequencial do nosso n�mero � controlada pelo cedente
- o Sequencial do nosso n�mero n�o poder� ser repetido, para que n�o haja t�tulos com o mesmo nosso n�mero
- o cedente dever� enviar o nosso n�mero calculado, de acordo com a descri��o na pr�xima p�gina, abaixo o
leiaute de como ficar� o nosso n�mero nos bloquetos:

AA/bnnnnn-d
AA    = Ano da gera��o do t�tulo
b     = Gera��o do nosso n�mero: 1 - Cooperativa cedente
                                 2 a 9 - Cedente
Nnnnn = N�mero sequencial por cedente
d     = D�gito verificador, calculado atrav�s do M�dulo 11

*)

//Portanto

    Self.NumeroBoleto:=formatdatetime('YY',RetornaDataServidor)+Self.Convenioboleto.Get_BYTE_NOSSONUMERO_SICRED+CompletaPalavra_a_Esquerda(Self.Get_CODIGO,5,'0');

    //Calculando DV

(*3.3.3.4 F�rmula para c�lculo do d�gito verificador pelo m�dulo 11

  a) Relacionar os c�digos da cooperativa de cr�dito/ag�ncia cedente (aaaa), posto cedente (pp),do cedente(ccccc),ano atual (yy),
indicador de gera��o do nosso n�mero (b) e o n�mero sequencial do cedente (nnnnn):

aaaappcccccyybnnnnn

b) Atribuir os pesos (de 2 a 9) correspondentes para cada d�gito, come�ando da direita para a esquerda,
efetuando cada multiplica��o;

c) somar o resultado de cada multiplica��o
d) dividir o resultado da soma por 11 (onze)
e) identificar o resto da divis�o
f) d�gito verificador ser� o resultado da subta��o = 11 - resto da divis�o.
Se o resultado da subtra��o for 10 ou 11 o d�gito verificador ser� 0 (zero)
*)


temp:=Self.Convenioboleto.get_posicao31_34_sicred;//aaaa
temp:=temp+Self.Convenioboleto.get_posicao35_36_sicred;//pp
temp:=temp+Self.Convenioboleto.get_posicao37_41_sicred;//ccccc
temp:=temp+Self.numeroboleto;//yybnnnnn

Self.NumeroBoleto:=Self.NumeroBoleto+Self.Modulo11_2_a_9_Sicred_nossoNumero(temp);

     result:=True;
End;



function TObjBoletoBancario.Get_ObservacoesSuperior: String;
begin
     Result:=Self.ObservacoesSuperior;
end;

procedure TObjBoletoBancario.Submit_ObservacoesSuperior(parametro: String);
begin
     Self.ObservacoesSuperior:=parametro;
end;

procedure TObjBoletoBancario.ExcluiVariosBoletos;
Var PBoletoInicial, PBoletoFinal:string;
    QueryLocal:TIBQuery;
Begin
    // Feito por F�bio
    // Esse procedimento � pra Resolver um problema do Jucelino ele cadastra um monte de
    // boleto errado e fica pedindo pra mim deletar pelo banco
    with FfiltroImp do
    Begin
         DesativaGrupos;
         Grupo01.Enabled:=true;
         edtgrupo01.EditMask:='';
         edtgrupo01.OnKeyDown:=Self.edtboletoKeyDown;
         LbGrupo01.Caption:='Boleto Inicial';

         Grupo02.Enabled:=true;
         edtgrupo02.EditMask:='';
         edtgrupo02.OnKeyDown:=Self.edtboletoKeyDown;
         LbGrupo02.Caption:='Boleto Final';

         ShowModal;
         if (Tag = 0)
         then exit;

         PBoletoInicial:=edtgrupo01.Text;
         PBoletoFinal:=edtgrupo02.Text;
    end;

    if (Self.LocalizaCodigo(PBoletoInicial)=false)
    then Begin
            MensagemErro('Boleto inicial n�o encontrado');
            exit;
    end;

    if (Self.LocalizaCodigo(PBoletoFinal)=false)
    then Begin
            MensagemErro('Boleto Final n�o encontrado');
            exit;
    end;

    if (MensagemPergunta('Tem certeza que deseja EXCLUIR todos os boletos de '+PBoletoInicial+' A '+PBoletoFinal)=0)
    then exit;

    With Self.ObjDataset do
    Begin
         Close;
         SelectSQL.Clear;
         SelectSQL.Add('Delete from  TabBoletoBancario');
         SelectSQL.Add('Where Codigo >= '+PBoletoInicial);
         SelectSQL.Add('and   Codigo <= '+PBoletoFinal);
         SelectSQL.Add('and   Situacao = ''N''   ');
         try
             ExecSQL;
         except
             MensagemErro('Erro ao tentar excluir os boletos');
             FDataModulo.IBTransaction.RollbackRetaining;
             exit;
         end;
    end;

    FDataModulo.IBTransaction.CommitRetaining;
    MensagemAviso('Boletos exclu�dos com Sucesso!');
end;

procedure TObjBoletoBancario.edtboletoKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa('Select * from TabBoletoBancario',Self.Convenioboleto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;



function TObjBoletoBancario.Modulo11_2_a_9_Sicred_nossoNumero(PValor: String): string;
var
   Soma : integer;
   Contador, Peso, Digito : integer;
begin
   peso:=2;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
        Soma := Soma + (StrToInt(PValor[Contador]) * Peso);
        if Peso =9
        then Peso :=2
        else Peso :=peso+1;
   End;


   peso:=Soma mod 11;//pega o resto

   Digito:=11-Peso;

   if (Digito>9)
   Then result:='0'
   Else result:=Inttostr(digito);
end;

function TObjBoletoBancario.GeraNumeroHSBC: boolean;
Begin
     Result:=False;
     {

     O Nosso Numero do HSBC � gerado com 13 + 3 digitos de verificadores
     e tipo identificador, vou gerar sem esses 3 digitos
     gerarei os mesmos apenas na hora de imprimir o boleto

      13 digitos  N�mero Banc�rio, igual ao C�digo do Documento, sem os
                   d�gitos verificadores e tipo identificador.
     }

     Self.NumeroBoleto:='';
     Self.NumeroBoleto:=CompletaPalavra_a_Esquerda(Self.Get_CODIGO,strtoint(Self.Convenioboleto.get_QtdeDigitosNossoNumero),'0');
     result:=True;
      
end;

function TObjBoletoBancario.Get_CobreBem: String;
begin
    Result:=Self.CobreBem;
end;

procedure TObjBoletoBancario.Submit_CobreBem(Parametro: String);
begin
    Self.CobreBem:=Parametro;
end;

Procedure TObjBoletoBancario.PreencheStrGridBoletosSemRemessaCobreBem(PConvenio:String; Var PStrGrid:TStringGrid);
Var PrimeiraLinha:Boolean;
    Cont:Integer;
Begin
      LimpaStringGrid(PStrGrid);

      With Self.ObjDataset do
      Begin
           Close;
           SelectSQL.Clear;
           SelectSQL.Add('Select Codigo, NumeroBoleto, Vencimento, ValorDoc from TabBoletoBancario');
           SelectSQL.Add('Where ArquivoRemessaRetorno is null and Situacao=''I'' ');
           SelectSQL.Add('and ConvenioBoleto = '+PConvenio);
           Open;

           PStrGrid.ColCount:=4;
           PStrGrid.RowCount:=2;
           PStrGrid.FixedRows:=1;

           PStrGrid.Cells[0,0]:='C�DIGO';
           PStrGrid.Cells[1,0]:='NUMERO';
           PStrGrid.Cells[2,0]:='VENCIMENTO';
           PStrGrid.Cells[3,0]:='VALOR';

           PrimeiraLinha:=true;
           Cont:=1;

           While not (eof) do
           Begin
                 if (PrimeiraLinha <> true)
                 then PStrGrid.RowCount:=PStrGrid.RowCount+1;
                 PrimeiraLinha:=false;

                 PStrGrid.Cells[0,Cont]:=fieldbyname('Codigo').AsString;
                 PStrGrid.Cells[1,Cont]:=fieldbyname('NumeroBoleto').AsString;
                 PStrGrid.Cells[2,Cont]:=fieldbyname('Vencimento').AsString;
                 PStrGrid.Cells[3,Cont]:=formata_valor(fieldbyname('ValorDoc').AsString);

                 Next;
                 inc(Cont,1);
           end;

      end;

      AjustaLArguraColunaGrid(PStrGrid);
end;


Procedure TObjBoletoBancario.PreencheStrGridBoletosCOMRemessaCobreBem(Boleto:String; Var PStrGrid:TStringGrid);
Begin

{      PStrGrid.Cells[0,Cont]:=fieldbyname('Codigo').AsString;
      PStrGrid.Cells[1,Cont]:=fieldbyname('NumeroBoleto').AsString;
      PStrGrid.Cells[2,Cont]:=fieldbyname('Vencimento').AsString;
      PStrGrid.Cells[3,Cont]:=formata_valor(fieldbyname('ValorDoc').AsString);

      AjustaLArguraColunaGrid(PStrGrid);
      }

end;


procedure TObjBoletoBancario.edtBoletoPorConvenioExit(Sender: TObject; LbNome: TLabel; PConvenio: String);
begin
     LbNome.caption:='';
     if (TEdit(Sender).text='')
     Then exit;

     Try
        strtoint(TEdit(Sender).text);
     Except
           TEdit(Sender).text:='';
     End;

     if (Self.LocalizaCodigo(TEdit(Sender).text)=False)
     Then begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TabelaparaObjeto;
     LbNome.caption:=Self.RetornaNomeCredorDevedo(Self.Get_CODIGO)+#13+
                     Self.Get_Vencimento+' | '+formata_valor(Self.Get_ValorDoc);

end;

procedure TObjBoletoBancario.edtBoletoPorConvenioKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState; PConvenio: String);
var
   FpesquisaLocal:Tfpesquisa;
   FBoletoBancario:TFBoletoBancario;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FBoletoBancario:=TFBoletoBancario.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa('Select * from TabBoletoBancario Where ConvenioBoleto = '+PConvenio+' and Situacao = ''I''  and ArquivoRemessaRetorno is not null' ,Self.Get_TituloPesquisa,FBoletoBancario)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FBoletoBancario);
     End;
end;

procedure TObjBoletoBancario.MostraHistorico(VAR PMemo:TMemo; PBoletoBancario:String);
Begin
     With Self.ObjDataset do
     Begin
          Close;
          SelectSQL.Clear;
          SelectSQL.Add('Select TabBoletoBancario.Codigo, TabBoletoBancario.ValorDoc,');
          SelectSQL.Add('TabBoletoBancario.Vencimento,TabInstrucao.Nome as Instrucao,');
          SelectSQL.Add('TabArquivoRemessaRetorno.ArquivoRemessa,TabArquivoRemessaRetorno.ArquivoRetorno,');
          SelectSQL.Add('Coalesce(TabOcorrenciaRetornoCobranca.Nome,''Enviada'') as OCorrencia');
          SelectSQL.Add('from TabBoletoInstrucoes');
          SelectSQL.Add('join TabBoletoBancario on TabBoletoBancario.Codigo = TabBoletoInstrucoes.BoletoBancario');
          SelectSQL.Add('left join TabInstrucao on TabInstrucao.Codigo = TabBoletoInstrucoes.Instrucao');
          SelectSQL.Add('left join TabArquivoRemessaRetorno on TabArquivoRemessaRetorno.Codigo = TabBoletoInstrucoes.ArquivoRemessaRetorno');
          SelectSQL.Add('left join TabOcorrenciaRetornoCobranca on TabOcorrenciaRetornoCobranca.Codigo = TabBoletoInstrucoes.Ocorrencia');

          if (PBoletoBancario <> '')
          then SelectSQL.Add('Where TabBoletobancario.Codigo = '+PBoletoBancario);

          SelectSQL.Add('Order By TabBoletoBancario.Codigo');

          Open;

          PMemo.Clear;
          While not (eof) do
          Begin
              PMemo.Lines.Add('C�DIGO BOLETO...............: '+fieldbyname('Codigo').AsString);
              PMemo.Lines.Add('VALOR.......................: '+Formata_valor(fieldbyname('ValorDoc').AsString));
              PMemo.Lines.Add('VENCIMENTO....,.............: '+fieldbyname('Vencimento').AsString);
              PMemo.Lines.Add('INSTRU��O...................: '+fieldbyname('Instrucao').AsString);
              PMemo.Lines.Add('OCORR�NCIA..................: '+fieldbyname('OCorrencia').AsString);
              PMemo.Lines.Add('ARQUIVO REMESSA.............: '+fieldbyname('ArquivoRemessa').AsString);
              PMemo.Lines.Add('ARQUIVO RETORNO.............: '+fieldbyname('ArquivoRetorno').AsString);
              PMemo.Lines.Add('');
          Next;
          end;                    

     end;

end;

function TObjBoletoBancario.Get_NossoNumeroCobreBem: String;
begin
    Result:=Self.NossoNumeroCobreBem;
end;

procedure TObjBoletoBancario.Submit_NossoNumeroCobreBem(Parametro: String);
begin
    Self.NossoNumeroCobreBem:=Parametro;
end;

Function TObjBoletoBancario.RetornaNomeCredorDevedo(PCodigo:String):String;
Var NomeTabela, CampoNOme, CodigoTabela:String;
Begin
    Result:='';
    With Self.ObjDataSet do
    Begin
         Close;
         SelectSQL.Clear;
         SelectSQL.Add('Select TabCredorDevedor.Tabela, TabCredorDevedor.CampoNome, TabTitulo.CodigoCredorDevedor from TabBoletoBancario');
         SelectSQL.Add('join TabPendencia on TabPendencia.Boletobancario = TabBoletoBancario.Codigo');
         SelectSQL.Add('join TabTitulo on TabTitulo.Codigo = TabPendencia.Titulo');
         SelectSQL.Add('join TabCredorDevedor on TabCredorDevedor.Codigo = TabTitulo.CredorDevedor');
         SelectSQL.Add('Where TabBoletoBancario.Codigo = '+PCodigo);
         Open;

         NomeTabela:=fieldbyname('Tabela').AsString;
         CodigoTabela:=fieldbyname('CodigoCredorDevedor').AsString;
         CampoNOme:=fieldbyname('CampoNome').AsString;

         if (NomeTabela='') or (CodigoTabela = '') or (CampoNOme = '')
         then exit;

         Close;
         SelectSQL.Clear;
         SelectSQL.Add('Select '+CampoNOme+' from '+NomeTabela+' Where Codigo = '+CodigoTabela);
         Open;

         Result:=Fieldbyname(CampoNOme).AsString;
    end;
end;

end.



