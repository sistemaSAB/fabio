unit UObjTitulo;
Interface
Uses StdCtrls,ibquery,Grids,QuickRpt,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,Uobjcontager,Uobjportador,classes,UobjGerador,UobjcredorDevedor,
UobjPrazoPagamento,UConfValorParcelas,UobjExportaContabilidade,UObjplanodecontas,UobjSubContagerencial,Graphics;
Type

   //registro utilizado para lancamento de t�tulos atrav�s de outros objetos
   //o registro com os campos do Titulo s�o preenchidos de acordo com que ter� que ser
   //preenchido, no momento de lan�ar um novo t�tulo verifica-se se algum campo
   //tem que ser preenchido
   TRegLancTituloExterno= Record

                          LancamentoExterno          :BOOLEAN;
                          LE_CODIGO                  :string;
                          LE_HISTORICO               :string;

                          LE_GERADOR                 :string;
                          LE_CODIGOGERADOR           :string;

                          LE_CREDORDEVEDOR           :string;
                          LE_CODIGOCREDORDEVEDOR     :string;

                          LE_EMISSAO                 :string;
                          LE_PRAZO                   :string;

                          LE_PORTADOR                :string;
                          LE_VALOR                   :string;
                          LE_CONTAGERENCIAL          :string;
                          LE_SUBCONTAGERENCIAL          :string;
                          LE_ParcelasIguais          :Boolean;
                      End;

//**************************************************************
   TObjTitulo=class

          Public
                ObjDatasourceMOstraTitulo                   :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ObjQueryMostraTitulo:TIBQuery;
                //Testes
                CREDORDEVEDOR           :TobjCredorDevedor;
                Objplanodecontas        :TobjPLanodeContas;
                CONTAGERENCIAL          :TObjContaGer;
                SubContaGerencial       :TobjSubContagerencial;
                PORTADOR                :TObjPortador;
                PRAZO                   :TObjPrazoPagamento;
                ObjExportaContabilidade:TobjExportaContabilidade;
                NumeroRelatorio:string;

                Constructor Create;
                Destructor Free;

                Function    Salvar(Comcommit:Boolean)       :Boolean;overload;
                Function    Salvar(Comcommit,PLancaContabilidade:Boolean):Boolean;overload;
                Function    Salvar(Comcommit,PLancaContabilidade:boolean;Pvencimentos,Pvalores:TStringList;NaoMostraTelaParcelas:boolean):Boolean;overload;
                Function    Salvar(Comcommit,PLancaContabilidade:boolean;Pvencimentos,Pvalores:TStringList):Boolean;overload;

                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaPendencia(Parametro:string) :boolean;
                Function    LocalizaBoleto(Parametro: string): boolean;
                function    LocalizaDuplicata(Parametro: string): boolean;
                Function    LocalizaLancExterno(ParametroGerador,ParametroCodigoGerador:string):Integer;
                Function    LocalizaGeradorCodigoPorObjeto(Parametro:string):Integer;
                Function    LocalizaCredorDevedorporTabela(Parametro:string):Integer;
                Function    LocalizaGeradorContaGerencialPorObjeto(Parametro:string):Integer;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;overload;
                Function    Get_Pesquisa(PcomSaldo:Boolean):TStringList;overload;
                Function    Get_Pesquisa(PcomSaldo:Boolean;PDebitoCredito:string):TStringList;overload;



                Function    Get_PesquisaGeradorInstrucaoSQL(parametro:string):string;
                Function    Get_PesquisaCredorDevedorInstrucaoSQL(parametro:string):string;
                Function    Get_TituloPesquisa              :string;


                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;
                Procedure   rollback;

                Function Get_CODIGO                  :string;
                Function Get_HISTORICO               :string;
                Function Get_GERADOR                 :string;//vem o nome e eu procuro o codigo
                Function Get_CODIGOGERADOR           :string;
                Function Get_CREDORDEVEDOR           :string;//vem o nome
                Function Get_CREDORDEVEDORCODIGO     :string;//vem o CODIGO PRIMARIO
                Function Get_CODIGOCREDORDEVEDOR     :string;
                Function Get_EMISSAO                 :string;
                Function Get_PRAZO                   :string;

                Function Get_PORTADOR                :string;
                Function Get_VALOR                   :string;
                Function Get_CONTAGERENCIAL          :string;
                Function Get_NUMDCTO                 :string;
                Function Get_NotaFiscal              :string;

                Function Get_GeradoPeloSistema       :string;
                Function Get_ExportaLanctoContaGer   :string;


                Procedure Submit_CODIGO                  (parametro:string);
                Procedure Submit_HISTORICO               (parametro:string);
                Procedure Submit_GERADOR                 (parametro:string);
                Procedure Submit_CODIGOGERADOR           (parametro:string);
                Procedure Submit_CREDORDEVEDOR           (parametro:string);
                Procedure Submit_CODIGOCREDORDEVEDOR     (parametro:string);
                Procedure Submit_EMISSAO                 (parametro:string);
                Procedure Submit_PRAZO                   (parametro:string);
                Procedure Submit_PORTADOR                (parametro:string);
                Procedure Submit_VALOR                   (parametro:string);
                Procedure Submit_CONTAGERENCIAL          (parametro:string);
                Procedure Submit_NUMDCTO                 (parametro:string);
                Procedure Submit_NotaFiscal              (parametro:string);
                Procedure Submit_GeradoPeloSistema       (parametro:string);
                Procedure Submit_ExportaLanctoContaGer   (parametro:string);
                Procedure Submit_observacao(parametro:String);
                function Get_geradortabela: string;

                Function    Get_PesquisaPortador                :string;
                Function    Get_PesquisaPortadorComCheque      :string; //Rodolfo
                Function    Get_TituloPesquisaPortador          :string;
                Function    Get_PesquisaContaGerencial          :string;
                Function    Get_TituloPesquisaContaGerencial    :string;

                Function    Get_TituloPesquisaPrazo: string;
                Function    Get_PesquisaPrazo: string;
                Function    Get_Observacao:String;


                Procedure    Get_ListaGerador(Parametro:TStrings);
                Procedure    Get_ListaGeradorcodigo(Parametro:TStrings);
                Procedure    Get_ListacredorDevedor(Parametro:TStrings);
                Procedure    Get_ListacredorDevedorcodigo(Parametro:TStrings);
                Procedure    Get_listaPrazoPagamentoNome(Parametro:TStrings);
                Procedure    Get_listaPrazoPagamentoCodigo(Parametro:TStrings);

                Procedure    Get_ListaPendenciaCampos(ParametroCodigo,ParametroHistorico,
                                                      ParametroVencimento,ParametroValor,ParametroSaldo,ParametroBoleto,ParametroDuplicata:TStrings;
                                                      Parametrotitulo:string);overload;
                procedure Get_ListaPendenciaCampos(ParametroCodigo, ParametroHistorico,ParametroVencimento, ParametroValor, ParametroSaldo, ParametroBoleto,
                                                   ParametroNomeBoleto, ParametroDuplicata, ParametroBoletoaPagar,ParametroObservacao: TStrings; Parametrotitulo: string);overload;


                Function    Get_NovoCodigo:string;
                Procedure   Submit_ParcelasIguais(Parametro:boolean);
                //Function    Get_QuantRegsPendencias(ParametroTitulo:string):Integer;

                Function  Get_SomaLancExterno(ParametroGerador,ParametroCodigoGerador:string):Currency;


                Function Desconto_Pendencias_Matricula(ParametroData:string;ParametroTitulo:string):Boolean;overload;
                Function Desconto_Pendencias_Matricula(ParametroTitulo:string):Boolean;overload;
                Function Desconto_Pendencias_Matricula_Gerador(PGerador,PCodigoGerador:string):Boolean;overload;
                Function Desconto_Pendencias_Matricula_Gerador(PGerador,PCodigoGerador:string;Pdata:string):Boolean;overload;


                Function Get_NomePortador(Parametro:string):string;
                Function Get_NomeContaGerencial(Parametro:string):string;
                Function Get_QuantidadePendenciaporGerador(PGerador,Pcodigogerador:string;OpcoesSaldo:Integer):integer;
                Function Get_FormularioCredorDevedor(PCodigoCredorDevedor:string):string;
                Function Get_CampoNomeCredorDevedor(PCodigoCredorDevedor:string):string;
                Function Get_NomeCredorDevedor(PCodigoCredorDevedor:string;PCampoPrimario:string):string;
                Procedure Get_PendenciaseTitulo(PAGAR_RECEBER:string;Pvencimento:string;PGRID:tstringgrid;PCredorDevedor,PCodigoCredorDevedor:String;PCodigoBoleto:string;ptipocolunas:Tstrings);overload;
                Procedure Get_PendenciaseTitulo(PAGAR_RECEBER:string;Pvencimento:string;PGRID:tstringgrid;PCredorDevedor,PCodigoCredorDevedor:String;PCodigoBoleto:string;ptipocolunas:Tstrings;PcontaGer,PSubContaGer:String);overload;



                Function RetornaCampoCodigo:string;
                Function RetornaCampoNome:string;
                function VerificaPermissao: Boolean;

                procedure edtcodigocredordevedorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
                procedure edtcodigocredordevedor_SPV_KeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
                function AcertaNotaFiscal(PNumTitulo:string;PNUmDcto: string;PnotaFiscal:string;ComCommit: Boolean): boolean;
                Procedure AcertaHistoricoPendencias;
                function Pendencias_em_Atraso(PCredorDevedor,PcodigoCredorDevedor: string; RetornaDados: Boolean; PTitulo,PPendencia, Pvencimento, Psaldo: TStringList): Boolean;
                Procedure GeratituloSaldoPagar_receber(Parametro: string);
                procedure LancaDuplicatas(parametro:string;LancaIndividualTituloAtual:Boolean);
                Procedure LancapendenciasTitulos;
                Function Apaga_sem_Saldo_Periodo(etapa:integer):Boolean;
                Function  GeraContabilidadeExclusao(ComCommit:boolean):boolean;


                //Relatorios
                Procedure ImprimeTitulosEmAtraso(TiPO_DC:string;Intervalo:Boolean);
                Procedure ImprimeTitulosEmAtraso_somadoporDia(TiPO_DC:string;Intervalo:Boolean;TrocaHistoricoPorCredorDevedor:Boolean);
                Procedure ImprimeTitulosEmAtraso_NomeCidade(TiPO_DC:string;Intervalo:Boolean);
                Procedure ImprimeFatura(Parametro:string);
                Procedure ImprimePrevisaoFinanceira;
                Procedure GeraFormularioPrevisaoFinanceira(PdataInicial,PdataFinal:string;pportador,pexcluircontagerencial,pexcluirsubcontagerencial:String;PCabecalho,Prodape:Tstrings;PStrgGrid:TStringGrid;var PsaldoInicial:currency;pexcluititulo:string;PtipoColunas:TStrings);
                Procedure ImprimeTitulosPorNumero;
                Procedure ImprimetitulosLancados;
                Procedure ImprimeRelacaoDebitoCreditoFornecedor(PGeradoPeloSistema:string);
                Procedure ImprimeTitulosEmAtrasoDOIS(TiPO_DC: string);
                Procedure ImprimeLancamentosTitulo(parametro:string);
                procedure ImprimeResumoContaGerencialLancamentos;
                procedure ImprimeResumoContaGerencialTitulos;
                procedure ImprimetitulosLancados_por_ContaGerencial(ComTitulodeSaldo:boolean);
                Procedure ImprimeTitulosGeradosPeloSaldo;
                Procedure ImprimeFichaFinanceira;
                procedure ImprimeFichaFinanceiraCRCP(CreditoDebito:string);
                procedure ImprimeTitulosEmAtrasoDOISCOMFONES(TiPO_DC: string);
                Procedure ImprimeTotalporContaGerencial(PTipoConta:string);
                Procedure ImprimeSaldoTotalCredorDevedor;
                procedure ImprimeTitulosEmAtraso_COMFORNECEDOR;
                procedure Imprime_Titulos_Em_Atraso_somado_por_Cidade_e_Dia(TiPO_DC: string; Intervalo: Boolean);
                procedure Imprime_Pagar_por_Dia_com_Cheque;
                procedure Imprime_Cheques_Clientes_portadores;
                procedure Imprime_TotalaReceber_Pagar_PorFornecedor(TiPO_DC: string);

                procedure ImprimeTotalporContaGerencialeSubConta(PTipoConta: string);


                procedure ImprimeTotalporContaGerencialeSubContaComDataDeLancamento(PTipoConta: string);

                procedure ImprimeTotalporContaGerencialeSubConta_agrupado_credordevedor(PTipoConta: string);
                procedure ImprimeTotalporContaGerencialeSubConta_Sintetico(PTipoConta: string);

                procedure EdtcontagerencialPAGARKeyDown_spv(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure EdtcontagerencialRECEBERKeyDown_spv(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure EdtcontagerencialRECEBERKeyDown_spv(Sender: TObject; var Key: Word;Shift: TShiftState;LabelNome:TLabel);overload;


                procedure EdtcontagerencialPAGARKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure EdtcontagerencialRECEBERKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure EdtcontagerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure Edtcontagerencial_SPV_KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure EdtcontagerencialExit(Sender: TObject; LabelNome: Tlabel);

                procedure edtportadorKeyDown_PV(Sender: TObject; var Key: Word;Shift: TShiftState);


                procedure EdtSubcontagerencialExit(Sender: TObject; LabelNome: Tlabel);
                procedure edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; lbnome: Tlabel; PContager: string);
                procedure edtsubcontagerencialKeyDown_PV(Sender: TObject;var Key: Word; Shift: TShiftState);

                procedure edttituloKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edttituloKeyDown_PV(Sender: TObject; var Key: Word;Shift: TShiftState);


                //***********************************************************
                Function  LancaNumeroduplicataPendencia(ptitulo:string):Boolean;
                procedure fieldbyname(PCampo, PValor: String);
                //********************************
                procedure Imprime_Contas_a_Receber_e_Cheques_por_portador;
                Function  RetornaSaldoVencido(PQuantDiasAtraso:Integer;PCredorDevedor,Pcliente:string):Currency;
                function RetornaNomesCredoresDevedores(PCredorDevedor: string;Pcodigos: String): String;
                procedure Get_Pendencias_Recibo_cobranca(PvencimentoINicial,PvencimentoFinal: string; PGRID: tstringgrid; PCredorDevedor,PCodigoCredorDevedor: string; ptipocolunas: Tstrings);
                procedure GeraFormularioPrevisaoFinanceira_mensal(PdataInicial,PdataFinal: string; pportador, pexcluircontagerencial,pexcluirsubcontagerencial: String; PCabecalho, Prodape: Tstrings;PStrgGrid: TStringGrid; var PsaldoInicial: currency;PincluiPrevisao:boolean);
                procedure GeraFormularioPrevisaoFinanceira_Realizado(PdataInicial,PdataFinal: string; pportador, pexcluircontagerencial,pexcluirsubcontagerencial: String; PCabecalho, Prodape: Tstrings;PStrgGrid: TStringGrid; var PsaldoInicial: currency);
                procedure MostraLancamentos_mes_subconta(psubconta, pdata: string);
                procedure MostraPendencias_mes_subconta(psubconta, pdata: string);

                function ContaTitulos: integer;

                Function RetornaSaldoPendencias( PCredordevedor : string; PCodigoCredorDevedor : string ) : Currency;
                procedure ImprimeClientesAdimplentes();
 				        function ListaTitulos(Ptipo, PComSemSaldo: string): Boolean;overload;
                function ListaTitulos(Ptipo, PComSemSaldo, PCampo,Pvalor: string): Boolean;overload;
                function RetornaValoresFinanceiros(Ptipo: string; PdataLimite: string):string;

                function ListaTitulos2(Ptipo, PComSemSaldo, PQuant: string): Boolean; //Rodolfo
                //function VerificaTamanhoBase: Integer ;  {Rodolfo}

         Private
                ObjDataset:Tibdataset;
                Objquery:tibquery;



                 CODIGO                  :string;
                 HISTORICO               :string;


                 GERADOR                 :TobjGerador;
                 CODIGOGERADOR           :string;


                 CODIGOCREDORDEVEDOR     :string;


                 EMISSAO                 :string;



                 VALOR                   :string;


                 ParcelasIguais          :Boolean;
                 NumDcto                 :string;
                 NotaFiscal              :string;
                 GeradoPeloSistema       :string;

                 ExportaLanctoContaGer   :string;
                 Observacao:String;


                 ParametroPesquisa:TStringList;



                 //as duas variaveis s�o usadas no relatorio de previsao
                 //devido ao uso de�sobreposicao de funcao onpreivei e outras
                 //do Frelatorio

                 SaldoBancoRel:Currency;


                Function  Grava_Pendencia:Boolean;overload;
                Function  Grava_Pendencia(Pvencimentos,Pvalores:TStringList;NaoMostraTelaParcelas:boolean):boolean;overload;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaGerador:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Function VerificaNumeroDocumento:boolean;
                Procedure ObjetoparaTabela;



                procedure EdtcontagerencialKeyDown_Unico(Sender: TObject; var Key: Word;Shift: TShiftState);

                procedure edtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                Procedure BeforePrintPevisao(Sender: TCustomQuickRep;var PrintReport: Boolean);
                //procedure CampoSaldoBAncoRelPrint(sender: TObject; var Value: String);
                Function  Grava_Exportacao:Boolean;

                procedure EdtcodigoKeyDown(Sender: TObject; var Key: Word;
                      Shift: TShiftState);
                procedure edtExcetocodigocredordevedorKeyDown(Sender: TObject;
                        var Key: Word; Shift: TShiftState);


                procedure EdtTipoLanctoKeyDown(Sender: TObject; var Key: Word;
                          Shift: TShiftState);


                procedure edtcodigocredordevedorKeyDown_Unico(Sender: TObject;
                        var Key: Word; Shift: TShiftState);
               Function  SomaQuitacao(Ptitulo:string):Currency;
               function VerificaseTemLote(Ptitulo: string;Pquery:tibquery): boolean;

               Function RetornaDataUltimaQuitacaodaPendencia(Ppendencia,PData1, PData2:string):string;
               Function RetornaDataUltimaQuitacaodaPendenciaBoleto(Ppendencia,PData1, PData2:string):string;
               procedure InsereSeNaoExisteNaStringList(Var PStrListNomes, PStrListNomeSubConta:TStringList; PNome, PNomeSubConta:String);




   End;

Procedure LimpaRegLancTituloExterno;
var
   RegLancTituloExterno:TRegLancTituloExterno;

implementation
uses rdprint,printers,SysUtils,Dialogs,Controls,UobjPendencia,UopcaoRel,
     UfiltraImp,forms,Urelatorio,UObjGeraLancamento,UobjLancamento,
     Upesquisa,windows,qrctrls,ureltxt,UObjValoresTransferenciaPortador,Uobjtalaodecheques,
     UreltxtrdPrint,UDatamodulo, 
     UMostraBarraProgresso, UObjchequesPortador, DateUtils,
  UmostraStringList, UmostraStringGrid, utitulo_novo;


{ TTabTitulo }



//******PROCEDIMENTOS COM O REGISTRO DE LAN�AMENTO EXTERNO****************
Procedure LimpaRegLancTituloExterno;
Begin
      With ReglancTituloExterno do
      Begin
              
              LE_CODIGO                  :='';
              LE_HISTORICO               :='';

              LE_GERADOR                 :='';
              LE_CODIGOGERADOR           :='';

              LE_CREDORDEVEDOR           :='';
              LE_CODIGOCREDORDEVEDOR     :='';

              LE_EMISSAO                 :='';
              LE_PRAZO                   :='';

              LE_PORTADOR                :='';
              LE_VALOR                   :='';
              LE_CONTAGERENCIAL          :='';

      End;
End;

//********************************************************





Procedure  TObjTitulo.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin

        Self.CODIGO                     :=      fieldbyname('CODIGO').asstring          ;
        Self.HISTORICO                  :=      fieldbyname('HISTORICO').asstring       ;
        Self.CODIGOCREDORDEVEDOR        :=      fieldbyname('codigocredordevedor').asstring;
        Self.CODIGOGERADOR              :=      fieldbyname('codigogerador').asstring;
        Self.EMISSAO                    :=      fieldbyname('EMISSAO').asstring         ;

        Self.VALOR                      :=      fieldbyname('VALOR').asstring           ;
        Self.NumDcto                    :=      fieldbyname('NUMDCTO').asstring  ;
        Self.NotaFiscal                 :=      fieldbyname('NotaFiscal').asstring  ;
        Self.GeradoPeloSistema          :=      fieldbyname('geradopelosistema').asstring;
        Self.ExportaLanctoContaGer      :=      Fieldbyname('ExportaLanctoContaGer').asstring;
        Self.Observacao                 :=      Fieldbyname('observacao').asstring;



        If (Self.CREDORDEVEDOR.LocalizaCodigo(fieldbyname('credordevedor').asstring)=False)
        Then Begin
                  Messagedlg('C�digo de Credor Devedor n�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.CREDORDEVEDOR.TabelaparaObjeto;



        if (fieldbyname('subcontagerencial').asstring<>'')
        Then Begin
                  If (Self.subcontagerencial.LocalizaCodigo(fieldbyname('subcontagerencial').asstring)=False)
                  Then Begin
                            Messagedlg('Sub-conta gerencial n�o encontrada!',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                       End
                  Else Self.subcontagerencial.TabelaparaObjeto;
        End
        else Self.SubContaGerencial.ZerarTabela;


        If (Self.GERADOR.LocalizaCodigo(fieldbyname('gerador').asstring)=False)
        Then Begin
                  Messagedlg('C�digo de Gerador n�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.gerador.TabelaparaObjeto;

        If (Self.Portador.LocalizaCodigo(FieldByName('Portador').asstring)=False)
        Then Begin
                  Messagedlg('Portador N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.Portador.TabelaparaObjeto;

        If (Self.CONTAGERENCIAL.LocalizaCodigo(FieldByName('CONTAGERENCIAL').asstring)=False)
        Then Begin
                  Messagedlg('CONTA GERENCIAL.N�o encontrada!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.CONTAGERENCIAL.TabelaparaObjeto;

        If (FieldByName('prazo').asstring<>'')
        Then Begin
                If (Self.prazo.LocalizaCodigo(FieldByName('prazo').asstring)=False)
                Then Begin
                          Messagedlg('Prazo N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                End
                Else Self.prazo.TabelaparaObjeto;
        End;

     End;
End;


Procedure TObjTitulo.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
       fieldbyname('CODIGO').asstring              :=   Self.CODIGO                     ;
       fieldbyname('HISTORICO').asstring           :=   Self.HISTORICO                  ;
       fieldbyname('GERADOR').asstring             :=   Self.GERADOR.Get_CODIGO         ;
       fieldbyname('CODIGOGERADOR').asstring       :=   Self.CODIGOGERADOR              ;
       fieldbyname('CREDORDEVEDOR').asstring       :=   Self.CREDORDEVEDOR.Get_CODIGO   ;
       fieldbyname('subcontagerencial').asstring       :=   Self.subcontagerencial.Get_CODIGO   ;
       fieldbyname('CODIGOCREDORDEVEDOR').asstring :=   Self.CODIGOCREDORDEVEDOR        ;
       fieldbyname('EMISSAO').asstring             :=   Self.EMISSAO                    ;
       fieldbyname('PRAZO').asstring               :=   Self.PRAZO.Get_CODIGO           ;
       fieldbyname('PORTADOR').asstring            :=   Self.PORTADOR.Get_CODIGO        ;
       fieldbyname('VALOR').asstring               :=   Self.VALOR                      ;
       fieldbyname('CONTAGERENCIAL').asstring      :=   Self.CONTAGERENCIAL.Get_CODIGO  ;
       fieldbyname('NUMDCTO').asstring             :=   Self.NUMDCTO                    ;
       fieldbyname('NotaFiscal').asstring          :=   Self.Notafiscal                    ;
       fieldbyname('geradopelosistema').asstring   :=   Self.GeradoPeloSistema          ;
       fieldbyname('ExportaLanctoContaGer').asstring:=  Self.ExportaLanctoContaGer;
       Fieldbyname('observacao').asstring:=Self.Observacao;    
   End;
End;

Procedure TObjTITULO.fieldbyname(PCampo:String;PValor:String);
Begin                                                          
     PCampo:=Uppercase(pcampo);                                

     If (Pcampo='CODIGO')
     Then Self.Submit_CODIGO(pVALOR);
     If (Pcampo='HISTORICO')
     Then Self.Submit_HISTORICO(pVALOR);
     If (Pcampo='GERADOR')
     Then Self.Submit_GERADOR(pVALOR);
     If (Pcampo='CODIGOGERADOR')
     Then Self.Submit_CODIGOGERADOR(pVALOR);
     If (Pcampo='CREDORDEVEDOR')
     Then Self.Submit_CREDORDEVEDOR(pVALOR);

     If (Pcampo='SUBCONTAGERENCIAL')
     Then Self.SUBCONTAGERENCIAL.Submit_codigo(pVALOR);

     If (Pcampo='CODIGOCREDORDEVEDOR')
     Then Self.Submit_CODIGOCREDORDEVEDOR(pVALOR);
     If (Pcampo='EMISSAO')
     Then Self.Submit_EMISSAO(pVALOR);
     If (Pcampo='PRAZO')
     Then Self.Submit_PRAZO(pVALOR);
     If (Pcampo='PORTADOR')
     Then Self.Submit_PORTADOR(pVALOR);
     If (Pcampo='VALOR')
     Then Self.Submit_VALOR(pVALOR);
     If (Pcampo='CONTAGERENCIAL')
     Then Self.Submit_CONTAGERENCIAL(pVALOR);
     If (Pcampo='NUMDCTO')
     Then Self.Submit_NUMDCTO(pVALOR);
     If (Pcampo='GERADOPELOSISTEMA')
     Then Self.Submit_GERADOPELOSISTEMA(pVALOR);
     If (Pcampo='NOTAFISCAL')
     Then Self.Submit_NOTAFISCAL(pVALOR);
     If (Pcampo='EXPORTALANCTOCONTAGER')
     Then Self.Submit_EXPORTALANCTOCONTAGER(pVALOR);
     if (Pcampo='OBSERVACAO')
     Then Self.Submit_observacao(pvalor);      
end;

//***********************************************************************

function TObjTitulo.Salvar(Comcommit:Boolean): Boolean;//Ok
begin
     //toda vez que usar este procedimento
     //ele gerara a contabilidade
     //quanto eu  naum puder gerar a contabilidade
     //eu uso o outro passando o parametro = false
     Result:=Self.Salvar(comcommit,true,nil,nil,False);
End;

function TObjTitulo.Salvar(Comcommit:Boolean;PLancaContabilidade:boolean): Boolean;//Ok
begin
     result:=Self.Salvar(Comcommit,PLancaContabilidade,nil,nil,False);
end;

function TObjTitulo.Salvar(Comcommit, PLancaContabilidade: boolean;
  Pvencimentos, Pvalores: TStringList): Boolean;
begin
     Result:=Self.Salvar(Comcommit,PLancaContabilidade,Pvencimentos,Pvalores,False);
end;

Function TObjTitulo.Salvar(Comcommit,PLancaContabilidade: boolean; Pvencimentos,
                           Pvalores: TStringList;NaoMostraTelaParcelas:boolean): Boolean;
var
temp,tempdc:string;
Begin
  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
  End;

  if (self.VerificaNumeroDocumento=False)
  Then Begin
            result:=False;
            exit;
  End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
  Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End
             Else BEgin
                       if (Self.ObjDataset.fieldbyname('contagerencial').asstring<>Self.CONTAGERENCIAL.get_Codigo)
                       Then Begin
                                 Temp:=Self.CONTAGERENCIAL.get_Codigo;
                                 TempDC:=Self.CONTAGERENCIAL.Get_Tipo;

                                 Self.CONTAGERENCIAL.LocalizaCodigo(Self.ObjDataset.fieldbyname('contagerencial').asstring);
                                 Self.CONTAGERENCIAL.TabelaparaObjeto;

                                 if (Self.CONTAGERENCIAL.Get_Tipo<>tempdc)
                                 Then Begin
                                           Mensagemerro('N�o � poss�vel alterar a modalidade da Conta Gerencial');
                                           result:=False;
                                           exit;
                                 End;
                                 Self.CONTAGERENCIAL.LocalizaCodigo(temp);
                                 Self.CONTAGERENCIAL.TabelaparaObjeto;
                       End;
             End;
        End;

 If (Self.VerificaGerador=False)
 Then begin
           result:=False;
           exit;
 end;

if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;


 Self.ObjetoParaTabela;

 Try
      Self.ObjDataset.Post;
 Except
     on e:Exception do
     Begin
          MensagemErro(e.Message);
          Result := false;
          Exit;
     End;

 End;

 If (Self.Status=dsinsert)
 Then Begin
        If (Self.Grava_Pendencia(Pvencimentos,Pvalores,NaoMostraTelaParcelas)=False)
        Then Begin
                Messagedlg('A Grava��o das Pend�ncias n�o foi conclu�da!',mterror,[mbok],0);
                result:=False;
                If (RegLancTituloExterno.Lancamentoexterno=False) and (ComCommit=True)
                Then FDataModulo.IBTransaction.RollbackRetaining;
                exit;
        End;

        if (PLancaContabilidade=true)
        Then Begin
              If (Self.Grava_Exportacao=False)//usado para exportar dados para a contabilidade
              Then Begin
                      Messagedlg('Erro na Grava��o dos registros de Exporta��o!',mterror,[mbok],0);
                      result:=False;
                      If (ComCommit=True)
                      Then FDataModulo.IBTransaction.RollbackRetaining;
                      exit;
              End;
        End;
 End
 Else Begin
           //edicao
           Self.ObjExportaContabilidade.ZerarTabela;

          if (Self.ObjExportaContabilidade.LocalizaGerador('OBJTITULO',Self.codigo)=True)
          Then Begin
                    Self.ObjExportaContabilidade.TabelaparaObjeto;
                    Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                    Self.ObjExportaContabilidade.Submit_NumDocto(Self.NumDcto);
                    Self.ObjExportaContabilidade.Status:=dsedit;
                    if (Self.ObjExportaContabilidade.Salvar(false)=False)
                    Then Begin
                              mensagemerro('Erro na tentativa de alterar a contabilidade');
                              exit;
                    End;
          End;
 End;

 If (Comcommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;



procedure TObjTitulo.ZerarTabela;//Ok
Begin
     With Self do
     Begin
          Self.CODIGO                   :='';        
          Self.HISTORICO                :='';
          Self.EMISSAO                  :='';
          Self.PRAZO.ZerarTabela;

          Self.PORTADOR.ZerarTabela;
          Self.VALOR                    :='';
          Self.CONTAGERENCIAL.ZerarTabela;
          Self.NUMDCTO                  :='';
          Self.NOtaFiscal               :='';
          Self.GeradoPeloSistema        :='N';//padrao
          Self.ExportaLanctoContaGer    :='';
          Self.Observacao:='';
     End;
end;                                    

Function TObjTitulo.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (historico='')
       Then Mensagem:=mensagem+'/Hist�rico';

       If (CREDORDEVEDOR.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Credor&Devedor';


       
       If (CODIGOCREDORDEVEDOR='')
       Then mensagem:=mensagem+'/C�digo Credor&Devedor';

       if (gerador.Get_CODIGO='')
       Then mensagem:=mensagem+'/Gerador';

       If (CODIGOGERADOR='')
       Then mensagem:=mensagem+'/Codigo Gerador';

       if (ExportaLanctoContaGer='')
       Then ExportaLanctoContaGer:='N';



       If (Emissao='')
       Then Mensagem:=mensagem+'/Data de Emiss�o do t�tulo';

       If (Prazo.get_codigo='')
       Then Mensagem:=mensagem+'/Prazo';


       If (Portador.get_codigo='')
       Then Mensagem:=mensagem+'/C�digo do Portador';

       If (Valor='')
       Then Mensagem:=mensagem+'/Valor do T�tulo';

       If (ContaGerencial.Get_CODIGO='')
       Then Mensagem:=mensagem+'/C�digo da Conta Gerencial';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjTitulo.VerificaRelacionamentos: Boolean;
var
mensagem:string;
PcontaAtual,PtipoAtual:string;
Begin
     mensagem:='';

     //verificar portador
     //verificar conta gerencial

     If (Self.PORTADOR.LocalizaCodigo(Self.PORTADOR.Get_CODIGO)=False)
     Then mensagem:=mensagem+'*/*Portador n�o encontrado';

     If (Self.prazo.LocalizaCodigo(Self.prazo.Get_CODIGO)=False)
     Then mensagem:=mensagem+'*/*Prazo n�o encontrado';
     If (self.CONTAGERENCIAL.LocalizaCodigo(Self.CONTAGERENCIAL.Get_CODIGO)=False)
     Then mensagem:=mensagem+'*/*Conta Gerencial n�o encontrada!'
     Else Begin
               Pcontaatual:=Self.CONTAGERENCIAL.Get_CODIGO;
               PtipoAtual:=Self.CONTAGERENCIAL.Get_Tipo;
               
               if (Self.Status=dsedit)
               then Begin
                         Self.LocalizaCodigo(Self.Codigo);
                         Self.CONTAGERENCIAL.LocalizaCodigo(Self.ObjDataset.fieldbyname('contagerencial').asstring);
                         Self.CONTAGERENCIAL.TabelaparaObjeto;
                         if (ptipoatual<>Self.CONTAGERENCIAL.Get_Tipo)
                         Then begin
                                   result:=False;
                                   MensagemErro('N�o � poss�vel alterar o tipo da Conta Gerencial de um t�tulo');
                                   exit;
                         End
                         Else Begin
                                   Self.CONTAGERENCIAL.LocalizaCodigo(PcontaAtual);
                                   Self.CONTAGERENCIAL.TabelaparaObjeto;
                         End
               End;

     End;

     If (self.GERADOR.LocalizaCodigo(Self.GERADOR.Get_CODIGO)=False)
     Then mensagem:=mensagem+'*/*Gerador n�o encontrado!'
     Else Begin
               Self.gerador.TabelaparaObjeto;
               If (self.gerador.LocalizaGerador(Self.codigogerador)=False)
               Then Mensagem:=Mensagem+'*/* C�digo de Gerador n�o Encontrado!';
          End;

     If (self.CREDORDEVEDOR.LocalizaCodigo(Self.CREDORDEVEDOR.Get_CODIGO)=False)
     Then mensagem:=mensagem+'*/*Credor&Devedor n�o encontrado!'
     Else Begin
               Self.CREDORDEVEDOR.TabelaparaObjeto;
               If (self.CREDORDEVEDOR.Localizacredordevedor(Self.codigocredordevedor)=False)
               Then Mensagem:=Mensagem+'*/* C�digo de Credor&Devedor n�o Enontrado!';
          End;

     if (mensagem='')
     then Begin
               if (Self.SubContaGerencial.Get_CODIGO<>'')
               then Begin
                         If (self.subcontagerencial.LocalizaCodigo(Self.subcontagerencial.Get_CODIGO)=False)
                         Then mensagem:=mensagem+'*/*Sub-Conta Gerencial n�o encontrada!'
                         Else Begin
                                   self.subcontagerencial.TabelaparaObjeto;
                                   if (Self.SubContaGerencial.ContaGerencial.Get_CODIGO<>Self.CONTAGERENCIAL.Get_CODIGO)
                                   Then mensagem:=mensagem+'\A sub-conta gerencial deve pertencer a conta gerencial';
                         End;
               End;
     End;



     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;


End;


function TObjTitulo.VerificaGerador: Boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add(' Select count(codigo) as CNT from tabtitulo where gerador='+Self.GERADOR.Get_CODIGO+ '  and  CodigoGerador='+Self.CODIGOGERADOR);
           Open;
           If (fieldbyname('CNT').asinteger>0)
           Then Begin//O SEM GERADOR possui CODIGO GERADOR=0
                     If (strtoint(Self.CODIGOGERADOR)<>0)
                     Then Begin
                             If (Messagedlg('J� existe um t�tulo Este Gerador e C�digo de Gerador!'+#13+'Deseja cadastrar outro t�tulo?',mtconfirmation,[mbyes,mbno],0)=mrno)
                             Then Result:=False
                             Else Result:=True;
                          End
                    Else result:=true;
                End
           Else Result:=True;

           Self.LocalizaCodigo(self.codigo);
       End;
end;


function TObjTitulo.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';
     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;


     try
        Inteiros:=Strtoint(Self.prazo.get_codigo);
     Except
           Mensagem:=mensagem+'/Prazo';
     End;



     try
        Reais:=Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     try
        Inteiros:=Strtoint(Self.Portador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Portador';
     End;


     try
        Inteiros:=Strtoint(Self.ContaGerencial.get_codigo);
     Except
           Mensagem:=mensagem+'/Conta Gerencial';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjTitulo.VerificaData: Boolean;
var
Datas:Tdate;
Mensagem:string;
begin                           
     mensagem:='';

     Try
        Datas:=strtodate(Self.Emissao);
     Except
           Mensagem:=mensagem+'/Emiss�o';
     End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjTitulo.VerificaFaixa: boolean;
var
   Mensagem:string;
   valortmp:currency;
begin
      Mensagem:='';

      if (Self.ExportaLanctoContaGer<>'N') and (Self.ExportaLanctoContaGer<>'S')
      Then ExportaLanctoContaGer:='N';

      If mensagem<>''
      Then Begin
          Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
          result:=false;
          exit;
      End;

      result:=true;
end;

function TObjTitulo.LocalizaBoleto(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add(' Select tabtitulo.CODIGO,tabtitulo.HISTORICO,tabtitulo.GERADOR,tabtitulo.CODIGOGERADOR,tabtitulo.CREDORDEVEDOR,tabtitulo.CODIGOCREDORDEVEDOR,');
           SelectSql.add(' tabtitulo.EMISSAO,tabtitulo.PRAZO,tabtitulo.PORTADOR,tabtitulo.VALOR,tabtitulo.CONTAGERENCIAL,tabtitulo.subcontagerencial,tabtitulo.NUMDCTO,');
           SelectSql.add(' tabtitulo.Notafiscal,tabtitulo.geradopelosistema,tabtitulo.ExportaLanctoContaGer,tabtitulo.observacao');
           SelectSql.add(' from tabtitulo join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
           SelectSql.add(' join TabBoletoBancario on Tabpendencia.BoletoBancario=tabBoletobancario.codigo');
           SelectSql.add(' where tabboletobancario.codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjTitulo.LocalizaPendencia(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add(' Select tabtitulo.CODIGO,tabtitulo.HISTORICO,tabtitulo.GERADOR,tabtitulo.CODIGOGERADOR,tabtitulo.CREDORDEVEDOR,tabtitulo.CODIGOCREDORDEVEDOR,');
           SelectSql.add(' tabtitulo.EMISSAO,tabtitulo.PRAZO,tabtitulo.PORTADOR,tabtitulo.VALOR,tabtitulo.CONTAGERENCIAL,tabtitulo.subcontagerencial,tabtitulo.NUMDCTO,');
           SelectSql.add(' Tabtitulo.Notafiscal,tabtitulo.geradopelosistema,tabtitulo.ExportaLanctoContaGer,tabtitulo.observacao');
           SelectSql.add(' from tabtitulo join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
           SelectSql.add(' where tabpendencia.codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjTitulo.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add(' Select CODIGO,HISTORICO,GERADOR,CODIGOGERADOR,CREDORDEVEDOR,CODIGOCREDORDEVEDOR,');
           SelectSql.add(' EMISSAO,PRAZO,PORTADOR,VALOR,CONTAGERENCIAL,subcontagerencial,NUMDCTO,Notafiscal,geradopelosistema,ExportaLanctoContaGer,tabtitulo.observacao');
           SelectSql.add(' from tabtitulo where codigo='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjTitulo.Cancelar;
begin
     //FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjTitulo.Exclui(Pcodigo: string; ComCommit: boolean): Boolean;
begin
     //este procedimento leva em consideracao que as pendencias
     //e seus lancamentos ja foram apagados
     //pedir pra apagar os lanctos em portador cujo o objgerador seja objtitulo e codigo seja esse em parametro
     //isso acontece em casos de troco do frente de caixa, que nao tem como ligar sempre
     //a um lancamento assim ligo apenas ao titulo

     Result:=False;
     Try
        if (ObjlanctoportadorGlobal.ExcluiporObjeto('OBJTITULO',Pcodigo,false)=False)
        Then Begin
                  Messagedlg('Erro na tentativa de excluir o lan�amento em portador ref. ao titulo '+pcodigo,mterror,[mbok],0);
                  exit;
        End;


        if (Self.LocalizaCodigo(pcodigo)=False)
        Then Begin
                  Messagedlg('T�tulo n�o localizado '+pcodigo,mterror,[mbok],0);
                  exit;
        End;

        Self.ObjDataset.delete;

        if (Comcommit=true)
        Then FDataModulo.IBTransaction.CommitRetaining;

        result:=True;
     Except
           result:=False;
           Messagedlg('Erro na tentativa de Excluir o T�tulo '+pcodigo,mterror,[mbok],0);
           exit;
     End;
end;

constructor TObjTitulo.create;
begin   
        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;

        Self.Objquery:=tibquery.create(nil);
        Self.Objquery.database:=FDataModulo.IbDatabase;

        Self.ObjQueryMostraTitulo:=TIBQuery.Create(nil);
        Self.ObjQueryMostraTitulo.Database:=FDataModulo.IBDatabase;
        Self.ObjDatasourceMOstraTitulo:=TDataSource.Create(nil);
        Self.ObjDatasourceMOstraTitulo.DataSet:=Self.ObjQueryMostraTitulo;


        Self.PORTADOR:=TObjPortador.create;
        Self.CONTAGERENCIAL:=TObjContaGer.Create;
        Self.gerador:=Tobjgerador.create;
        Self.CREDORDEVEDOR:=TObjCredorDevedor.create;
        Self.Prazo:=TobjPrazoPagamento.create;
        Self.parametropesquisa:=TStringList.create;
        Self.ObjExportaContabilidade:=TobjExportaContabilidade.create;
        Self.Objplanodecontas       :=TobjPLanodeContas.create;
        Self.SubContaGerencial      :=TobjSubContagerencial.create;


        //Nao faz parte do Objeto porque
        //criaria uma referencia circular
        //pois o Objeto Pendencia possui Titulo
        //ObjPendencia:=tobjPendencia.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
             SelectSQL.clear;
             SelectSql.add(' Select CODIGO,HISTORICO,GERADOR,CODIGOGERADOR,CREDORDEVEDOR,CODIGOCREDORDEVEDOR,');
             SelectSql.add(' EMISSAO,PRAZO,PORTADOR,VALOR,CONTAGERENCIAL,subcontagerencial,NUMDCTO,Notafiscal,geradopelosistema,ExportaLanctoContaGer,tabtitulo.observacao');
             SelectSql.add(' from tabtitulo where codigo=0');

             Self.SqlInicial:=SelectSQL.text;

             InsertSQL.clear;
             InsertSQL.add('Insert into Tabtitulo (CODIGO,HISTORICO,GERADOR,CODIGOGERADOR,CREDORDEVEDOR,CODIGOCREDORDEVEDOR,');
             InsertSQL.add('EMISSAO,PRAZO,');
             InsertSQL.add('PORTADOR,VALOR,CONTAGERENCIAL,subcontagerencial,NUMDCTO,Notafiscal,geradopelosistema,ExportaLanctoContaGer,observacao)');
             InsertSQL.add('values (:CODIGO,:HISTORICO,:GERADOR,:CODIGOGERADOR,:CREDORDEVEDOR,:CODIGOCREDORDEVEDOR,');
             InsertSQL.add(':EMISSAO,:PRAZO,');
             InsertSQL.add(':PORTADOR,:VALOR,:CONTAGERENCIAL,:subcontagerencial,:NUMDCTO,:Notafiscal,:geradopelosistema,:ExportaLanctoContaGer,:observacao)');

             ModifySQL.clear;
             ModifySQL.add('Update TabTitulo set  CODIGO=:CODIGO,HISTORICO=:HISTORICO,GERADOR=:GERADOR,CODIGOGERADOR=:CODIGOGERADOR,CREDORDEVEDOR=:CREDORDEVEDOR,CODIGOCREDORDEVEDOR=:CODIGOCREDORDEVEDOR,');
             ModifySQL.add('EMISSAO=:EMISSAO,PRAZO=:PRAZO,');
             ModifySQL.add('PORTADOR=:PORTADOR,VALOR=:VALOR,CONTAGERENCIAL=:CONTAGERENCIAL,subcontagerencial=:subcontagerencial,NUMDCTO=:NUMDCTO,Notafiscal=:Notafiscal,');
             ModifySQL.add('geradopelosistema=:geradopelosistema,ExportaLanctoContaGer=:ExportaLanctoContaGer,observacao=:observacao  where');
             ModifySQL.add('codigo=:codigo');


             DeleteSQL.clear;
             DeleteSQL.add(' Delete from tabtitulo where codigo=:codigo');

             RefreshSQL.clear;
             RefreshSQL.add('Select CODIGO,HISTORICO,GERADOR,CODIGOGERADOR,CREDORDEVEDOR,CODIGOCREDORDEVEDOR,');
             RefreshSQL.add('EMISSAO,PRAZO,');
             RefreshSQL.add('PORTADOR,VALOR,CONTAGERENCIAL,subcontagerencial,NUMDCTO,Notafiscal,geradopelosistema,ExportaLanctoContaGer,observacao from tabtitulo where');
             RefreshSQL.add('codigo=0');

             open;

             Self.ObjDataset.First ;
             Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjTitulo.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjTitulo.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjTitulo.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjTitulo.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.Clear;
     If (RegLancTituloExterno.LancamentoExterno=True)
     Then Self.ParametroPesquisa.add(' Select * from TabTitulo where gerador='+RegLancTituloExterno.LE_GERADOR+'  and codigogerador='+RegLancTituloExterno.LE_CODIGOGERADOR)
     Else Self.ParametroPesquisa.add(' Select * from TabTitulo ');

     Result:=Self.ParametroPesquisa;
end;

function TObjTitulo.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de T�tulos ';
end;


function TObjTitulo.Get_CONTAGERENCIAL: string;
begin
     result:=Self.CONTAGERENCIAL.Get_CODIGO;
end;



function TObjTitulo.Get_EMISSAO: string;
begin
     result:=Self.emissao;
end;

function TObjTitulo.Get_HISTORICO: string;
begin
     result:=Self.historico;
end;


function TObjTitulo.Get_PORTADOR: string;
begin
     result:=Self.PORTADOR.get_codigo;
end;

function TObjTitulo.Get_PRAZO: string;
begin
     result:=Self.prazo.Get_CODIGO;
end;



function TObjTitulo.Get_VALOR: string;
begin
     result:=Self.valor;
end;


procedure TObjTitulo.Submit_CONTAGERENCIAL(parametro: string);
begin
     Self.CONTAGERENCIAL.Submit_CODIGO(parametro);
end;

procedure TObjTitulo.Submit_EMISSAO(parametro: string);
begin
     Self.emissao:=parametro;
end;

procedure TObjTitulo.Submit_HISTORICO(parametro: string);
begin
     Self.HISTORICO:=parametro;
end;

procedure TObjTitulo.Submit_PORTADOR(parametro: string);
begin
     Self.portador.Submit_CODIGO(parametro);
end;

procedure TObjTitulo.Submit_PRAZO(parametro: string);
begin
     Self.PRAZO.Submit_CODIGO(parametro);
end;

procedure TObjTitulo.Submit_VALOR(parametro: string);
begin
     Self.VALOR:=parametro;
end;

function TObjTitulo.Get_PesquisaContaGerencial: string;
begin
     Result:=Self.CONTAGERENCIAL.Get_Pesquisa;
end;

function TObjTitulo.Get_PesquisaPortador: string;
begin
     Result:=Self.PORTADOR.Get_Pesquisa;
end;

{Rodolfo}
function TObjTitulo.Get_PesquisaPortadorComCheque: string;
begin
     Result:=Self.PORTADOR.Get_PesquisaPortadorComCheque;
end;

function TObjTitulo.Get_TituloPesquisaContaGerencial: string;
begin
     Result:=Self.CONTAGERENCIAL.Get_TituloPesquisa;
end;

function TObjTitulo.Get_TituloPesquisaPortador: string;
begin
     Result:=Self.PORTADOR.Get_TituloPesquisa;
end;

Procedure TObjTitulo.Get_ListaGerador(Parametro: TStrings);
begin
     Self.GERADOR.Get_listaNomeGeradores(parametro);
end;

procedure TObjTitulo.Get_ListacredorDevedor(Parametro: TStrings);
begin
    Self.CREDORDEVEDOR.get_listanomecredordevedor(parametro);
end;

function TObjTitulo.Get_CODIGOCREDORDEVEDOR: string;
begin
     Result:=Self.CODIGOCREDORDEVEDOR;
end;

function TObjTitulo.Get_CODIGOGERADOR: string;
begin
     Result:=Self.CODIGOGERADOR;
end;

function TObjTitulo.Get_CREDORDEVEDOR: string;
begin
     Result:=Self.CREDORDEVEDOR.get_nome;
end;

function TObjTitulo.Get_GERADOR: string;
begin
     Result:=Self.gerador.get_nome;
end;

procedure TObjTitulo.Submit_CODIGOCREDORDEVEDOR(parametro: string);
begin
     Self.CODIGOCREDORDEVEDOR:=parametro;
end;

procedure TObjTitulo.Submit_CODIGOGERADOR(parametro: string);
begin
     Self.CODIGOGERADOR:=parametro;
end;

procedure TObjTitulo.Submit_CREDORDEVEDOR(parametro: string);
begin
     Self.CREDORDEVEDOR.Submit_CODIGO(parametro);
end;

procedure TObjTitulo.Submit_GERADOR(parametro: string);
begin
     Self.GERADOR.Submit_CODIGO(parametro);
end;

procedure TObjTitulo.Get_ListacredorDevedorcodigo(Parametro: TStrings);
begin
     Self.CREDORDEVEDOR.Get_listaCodigoCredorDevedor(parametro);
end;

procedure TObjTitulo.Get_ListaGeradorcodigo(Parametro: TStrings);
begin
     Self.GERADOR.Get_listaCodigoGeradores(parametro);
end;

function TObjTitulo.Get_PesquisaGeradorInstrucaoSQL(parametro:string): string;
begin
     If (Self.gerador.LocalizaCodigo(parametro)=true)
     Then Begin
             Self.gerador.TabelaparaObjeto;
             Result:=Self.gerador.Get_InstrucaoSQL;
          End
     Else result:='';
end;

function TObjTitulo.Get_PesquisaCredorDevedorInstrucaoSQL(
  parametro: string):string;
begin
     If (Self.CREDORDEVEDOR.LocalizaCodigo(parametro)=true)
     Then Begin
             Self.CREDORDEVEDOR.TabelaparaObjeto;
             Result:=Self.CREDORDEVEDOR.Get_InstrucaoSQL;
          End;
end;

function TObjTitulo.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_TITULO';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
           result:='0';
           exit;

        End;
     Finally
            FreeandNil(StrTemp);
     End;
end;

function TObjTitulo.Grava_Pendencia: Boolean;
begin
     Result:=Self.Grava_pendencia(nil,nil,False);
End;

function TObjTitulo.Grava_Pendencia(Pvencimentos,
  Pvalores: TStringList;NaoMostraTelaParcelas:boolean): boolean;
var
cont:Integer;
NumParcelas:Integer;
ValorParcela:Currency;
ValorParcelaStr:String;
ObjPendencia            :TobjPendencia;
DefineData:boolean;
FConfValorParcelas:TFConfValorParcelas;
DataVencFimMes,CodigoNovaPendencia:string;
JurosDescontos:Currency;
valorAcres:Currency;
ValorTotal:Currency;
objlancamento    :TObjLancamento;
query:TIBQuery;
begin

   result:=False;

   Try

            query:= TIBQuery.Create(nil);
            query.Database:= FDataModulo.IBDatabase;


           FConfValorParcelas:=TFConfValorParcelas.create(nil);
           ObjPendencia:=tobjPendencia.create;

            Try
                DefineData:=False;
                Self.Prazo.LocalizaCodigo(Self.Prazo.get_codigo);
                Self.Prazo.tabelaparaobjeto;
                NumParcelas:=Strtoint(Self.PRAZO.get_parcelas);


                If (NumParcelas=-1)
                Then Begin//significa quantidade definida pelo usuario
                          Self.ParcelasIguais:=False;
                          DefineData:=True;
                          NUmParcelas:=1;
                End;

                ValorParcela:=0;
                ValorParcelaStr:='0';
                if (NumParcelas<>0)
                Then Begin
                        ValorParcela:=Strtofloat(Self.Valor)/NumParcelas;
                        ValorParcelaStr:='';
                        ValorParcelaStr:=ArredondaValor(ValorParcela);
                End
                Else Begin
                          //n�o � para gerar pend�ncias
                          result:=False;
                          Messagedlg('N�o � poss�vel gerar um t�tulo sem pend�ncias!',mterror,[mbok],0);
                          exit;
                End;

                IF (ObjParametroGlobal.ValidaParametro('SEMPRE PARCELAS IGUAIS=FALSE')=True)
                Then Begin
                  
                          If (ObjParametroGlobal.Get_Valor='SIM')
                          Then Self.ParcelasIguais:=False;
                End;

                If (Self.ParcelasIguais=False)
                Then Begin
                          //Preciso Preencher a Lista de Vencimentos
                          //do form de Valor  de Parcelas e depois
                          //chama-lo para Configura��o
                          FConfValorParcelas.LBVencimentos.items.clear;
                          FConfValorParcelas.LBValores.items.clear;
                          For cont:=1 to NumParcelas do
                          Begin
                               With FConfValorParcelas do
                               Begin
                                    If (DefineData=True)
                                    Then Begin
                                         // LBVencimentos.items.add(Datetostr(StrTodate(Self.Emissao)+cont-1))
                                        if(Self.PRAZO.Get_ParcelaPorNum(cont) = -5)
                                        then
                                        begin
                                            DataVencFimMes:=DateToStr(EndOfTheMonth(StrToDate(Self.Emissao)));
                                            LBVencimentos.items.add(DataVencFimMes);
                                        end
                                        else
                                        begin
                                            LBVencimentos.items.add(Datetostr(StrTodate(Self.Emissao)+cont-1))
                                        end;
                                    end
                                    Else
                                    begin
                                      // LBVencimentos.items.add(Datetostr(StrTodate(Self.Emissao)+Self.PRAZO.Get_ParcelaPorNum(cont)));
                                      if(Self.PRAZO.Get_ParcelaPorNum(cont) = -5)
                                      then
                                      begin
                                          DataVencFimMes:=DateToStr(EndOfTheMonth(StrToDate(Self.Emissao)));
                                          LBVencimentos.items.add(DataVencFimMes);
                                      end
                                      else
                                      begin
                                          LBVencimentos.items.add(Datetostr(StrTodate(Self.Emissao)+Self.PRAZO.Get_ParcelaPorNum(cont)));
                                      end;
                                    end;
                                    LBValores.items.add(Formata_valor(ValorParcelaStr));

                               End;
                          End;

                          if ((Pvencimentos<>nil) or (Pvalores<>nil))
                          Then Begin
                                    //aqui vou por a lista que ta vindo e nos parametros
                                    If (Pvencimentos<>nil)
                                    Then Begin
                                              FConfValorParcelas.LBVencimentos.items.clear;
                                              for cont:=0 to Pvencimentos.Count-1 do
                                              Begin
                                                   FConfValorParcelas.LBVencimentos.items.add(Pvencimentos[cont]);
                                              End;

                                    End;

                                    IF (Pvalores<>nil)
                                    Then Begin
                                              FConfValorParcelas.LBValores.items.clear;
                                              for cont:=0 to Pvalores.Count-1 do
                                              Begin
                                                   FConfValorParcelas.LBValores.Items.add(Pvalores[cont]);
                                              End;
                                    End;


                          End;



                          //Chamando o Form de Configura��o
                          FConfValorParcelas.LabValorTotal.caption:=formata_valor(Self.VALOR);
                          FConfValorParcelas.labTotalparcelas.caption:=formata_valor(Self.VALOR);
                          FConfValorParcelas.ValorTotal:=strtofloat(Self.valor);
                          //S� Sai do la�o se as parcelas fecharem o valor do Titulo
                          cont:=0;

                          Repeat

                                if (NaoMostraTelaParcelas=False)
                                Then FConfValorParcelas.showmodal;

                                if (FConfValorParcelas.tag=1)
                                Then exit;

                                If (StrTofloat(Tira_ponto(FConfValorParcelas.labTotalparcelas.caption))<>StrTofloat(Self.valor))
                                Then Begin
                                          Messagedlg('A Soma das Parcelas n�o Confere com o Valor do T�tulo!',mterror,[mbok],0);
                                          cont:=0;
                                End
                                Else cont:=1;

                           Until(cont=1);
                           NumParcelas:=FConfValorParcelas.LBValores.Items.count;
                End;

                for cont:=1 to NumParcelas do
                Begin
                     CodigoNovaPendencia := ObjPendencia.Get_NovoCodigo;
                     ObjPendencia.ZerarTabela;
                     ObjPendencia.status:=Dsinsert;
                     ObjPendencia.submit_codigo(CodigoNovaPendencia);
                     ObjPendencia.Submit_Historico(inttostr(cont)+'/'+Inttostr(NumParcelas));
                     ObjPendencia.Submit_Titulo(Self.codigo);
                     //Pega a Emisso e soma com a quantidade de dias daquela parcela
                     //A quantidade � trazida pela Get_+ParcelaPorNum do Prazo
                     If (DefineData=True)
                     Then
                     begin
                         ObjPendencia.Submit_VENCIMENTO(FConfValorParcelas.PegaVencimento(Cont));
                         //showmessage(FConfValorParcelas.PegaVencimento(Cont));
                     end
                     Else
                     begin
                        //menos 5 � um numero padr�o adotado por mim para saber se a forma de pagamento �
                        //Fim do M�s, se sim ajusta parcela(s) para o fim do m�s
                        //altera��o 07/01/2011 -- Jonatan Medina
                          if(Self.PRAZO.Get_ParcelaPorNum(cont) = -5)
                          then
                          begin
                              DataVencFimMes:=DateToStr(EndOfTheMonth(StrToDate(Self.Emissao)));
                              ObjPendencia.Submit_VENCIMENTO(DataVencFimMes) ;
                          end
                          else
                          begin
                              ObjPendencia.Submit_VENCIMENTO(Datetostr(StrTodate(Self.Emissao)+Self.PRAZO.Get_ParcelaPorNum(cont)));
                          end;
                     end;
                     ObjPendencia.Submit_PORTADOR(Self.Portador.Get_CODIGO);

                     If (NumParcelas=1) and (Pvalores=nil)
                     Then Begin
                               ObjPendencia.Submit_VALOR(Tira_ponto(Self.VALOR));
                               ObjPendencia.Submit_Saldo(Tira_Ponto(Self.valor));
                     End
                     Else Begin
                                if (Self.ParcelasIguais=true) then
                                begin



                                        If (cont=Numparcelas)//� a ultima a gravar
                                        then Begin
                                                  //entao arrendondo o valor, por causa das divisoes se perdem alguns centesimos de reais
                                                  ValorParcelaStr:=Floattostr(strtofloat(ValorParcelaStr)+(strtofloat(Self.valor)-(cont*strtofloat(ValorParcelaStr))));

                                             End;
                                        ObjPendencia.Submit_VALOR(ValorParcelaStr);
                                        ObjPendencia.Submit_Saldo(ValorParcelaStr);

                                     End
                                Else Begin
                                         ObjPendencia.Submit_VALOR(Tira_ponto(FConfValorParcelas.PegaValor(Cont)));
                                         ObjPendencia.Submit_Saldo(ObjPendencia.Get_VALOR);
                                End;
                     End;

                      //N�o Posso dar Commit, porque se n�o commita o titulo aqui
                      if (ObjPendencia.salvar(False)=False)
                      Then exit;
                      //Alterado por Jonatan Medina
                      //Agora no cadastro de Prazo de Pagamentos, no campo CREDITO_DEBITO
                      //pode colocar descontos ou juros altomaticos
                      //ex: 10 => 10% de juros
                      //ex:-10 => 10% de descontos

                   try //add jonas, para destrui o objeto TobjLancamento que nao estava sendo destruido, e porq causa desse balaco-baco, tava gerando um pepino do tamanho do meu (grande pa burro)
                      if(ObjParametroGlobal.ValidaParametro('MODO DE ACRESCIMO/DESCONTO UTILIZANDO A FORMA DE PAGAMENTO'))
                      then if((ObjParametroGlobal.Get_Valor='1') OR (ObjParametroGlobal.Get_Valor='')) //1=no titulo
                            then begin

                                  if (self.PRAZO.get_CREDITO_DEBITO <> '') then
                                      valorAcres:=StrToCurr(Self.PRAZO.get_CREDITO_DEBITO)
                                  else
                                      valorAcres := 0;

                                  ValorTotal:=StrToCurr(ObjPendencia.Get_VALOR);
                                  JurosDescontos:=(valorAcres*valortotal)/100;
                                  //se foi descontos o campo CREDITO_DEBITO  vai estar negativo
                                  //� preciso q ap�s os calculos o valor obtido seja passado pra positivo novamente
                                  if(JurosDescontos<0) then
                                  begin
                                      JurosDescontos:=JurosDescontos*(-1);
                                  end;


                                  //if(Self.PRAZO.get_CREDITO_DEBITO <> '') then
                                  if(JurosDescontos > 0) then
                                  begin

                                      with query do
                                      begin
                                          close;

                                          //SQL.Clear;
                                          //sql.Add('select codigo from tabpendencia order by codigo');
                                          //Open;
                                          //Last;

                                          objlancamento:= TObjLancamento.Create;
                                          objlancamento.ZerarTabela;
                                          ObjLancamento.submit_CODIGO(ObjLancamento.Get_NovoCodigo);
                                          //ObjLancamento.Pendencia.submit_codigo(fieldbyname('codigo').AsString);
                                          ObjLancamento.Pendencia.submit_codigo(CodigoNovaPendencia);
                                          ObjLancamento.submit_Data(formatdatetime ('dd/mm/yyyy',now));
                                          ObjLancamento.submit_Valor(CurrToStr(JurosDescontos));
                                          ObjLancamento.submit_LancamentoPai('');
                                          if(StrToCurr(Self.PRAZO.get_CREDITO_DEBITO)>0) then
                                          begin
                                              if (ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
                                              Then Begin
                                                         If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','JUROS A PAGAR')=False)
                                                         Then Begin
                                                                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="JUROS A PAGAR", n�o foi encontrado!',mterror,[mbok],0);
                                                                  exit;
                                                         End;
                                              End
                                              else begin
                                                         If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','JUROS A RECEBER')=False)
                                                         Then Begin
                                                                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="JUROS A RECEBER", n�o foi encontrado!',mterror,[mbok],0);
                                                                  exit;
                                                         End;
                                                         if not(ObjLancamento.VerificaPermissaoDescontoAcrescimo(ObjLancamento,'A'))
                                                         then exit;
                                              end;
                                              ObjLancamento.submit_Historico('Juros pr�-definido no prazo de pagamento');

                                          end;
                                          if(StrToCurr(Self.PRAZO.get_CREDITO_DEBITO)<0) then
                                          begin
                                              if (ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
                                               Then Begin
                                                         If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO A PAGAR')=False)
                                                         Then Begin
                                                                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="DESCONTO A PAGAR", n�o foi encontrado!',mterror,[mbok],0);
                                                                  exit;
                                                         End;
                                               End
                                              Else begin
                                                         If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO A RECEBER')=False)
                                                         Then Begin
                                                                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="DESCONTO A RECEBER", n�o foi encontrado!',mterror,[mbok],0);
                                                                  exit;
                                                         End;
                                                         if not(ObjLancamento.VerificaPermissaoDescontoAcrescimo(ObjLancamento,'D'))
                                                         then exit;
                                               End;
                                               ObjLancamento.submit_Historico('Desconto pr�-definido no prazo de pagamento');
                                          end;
                                          if(StrToCurr(Self.PRAZO.get_CREDITO_DEBITO)<>0)
                                          then begin
                                                ObjLancamento.ObjGeraLancamento.TabelaparaObjeto;
                                                ObjLancamento.TipoLancto.Submit_CODIGO(ObjLancamento.ObjGeraLancamento.TipoLAncto.Get_CODIGO);
                                                ObjLancamento.status:=dsinsert;
                                                //2011.06.09 Antes estava enviando com commit = true  celio
                                                //If (ObjLancamento.salvar(True,True,ObjLancamento.ObjGeraLancamento.Get_CodigoPlanodeContas,true,False)=False)
                                                If (ObjLancamento.salvar(False,True,ObjLancamento.ObjGeraLancamento.Get_CodigoPlanodeContas,true,False)=False)
                                                Then begin
                                                      ObjLancamento.Rolback;
                                                      Exit;
                                                end;
                                          end;

                                      end;
                                  end;
                            end;

                   finally
                        IF(objlancamento<>nil)
                        then objlancamento.Free;
                   end;

                End;

            Except
                   Messagedlg('Erro Durante a Grava��o das Pend�ncias. A Grava��o do T�tulo ser� cancelada!',mterror,[mbok],0);
                   exit;
            End;

             result:=True;

   Finally
       ObjPendencia.free;
       Freeandnil(FConfValorParcelas);
       Freeandnil(query);
   End;

   result:=true;

end;




procedure TObjTitulo.Get_listaPrazoPagamentoCodigo(Parametro: TStrings);
begin
     Self.prazo.get_listaCodigo(parametro);
end;

procedure TObjTitulo.Get_listaPrazoPagamentoNome(Parametro: TStrings);
begin
     Self.Prazo.get_listaNome(parametro);
end;

procedure TObjTitulo.Submit_ParcelasIguais(Parametro: boolean);
begin
     Self.ParcelasIguais:=Parametro;
end;



procedure TObjTitulo.Get_ListaPendenciaCampos(ParametroCodigo,
  ParametroHistorico, ParametroVencimento, ParametroValor,
  ParametroSaldo,ParametroBoleto,ParametroDuplicata: TStrings; Parametrotitulo: string);
Begin
     Self.Get_ListaPendenciaCampos(ParametroCodigo,ParametroHistorico, ParametroVencimento, ParametroValor,
                                   ParametroSaldo,ParametroBoleto,nil,ParametroDuplicata,nil,nil,Parametrotitulo);
end;


procedure TObjTitulo.Get_ListaPendenciaCampos(ParametroCodigo,
  ParametroHistorico, ParametroVencimento, ParametroValor, ParametroSaldo,
  ParametroBoleto, ParametroNomeBoleto, ParametroDuplicata,ParametroBoletoaPagar,ParametroObservacao: TStrings;
  Parametrotitulo: string);
var
ObjPendencia:TobjPendencia;
begin
     Try
        ObjPendencia:=TobjPendencia.create;
        ObjPendencia.Get_ListaCodigo(parametroCodigo,Parametrotitulo,'T');
        ObjPendencia.Get_ListaHistorico(parametroHistorico,Parametrotitulo,'T');
        ObjPendencia.Get_ListaVencimento(parametroVencimento,Parametrotitulo,'T');
        ObjPendencia.Get_ListaValor(parametroValor,Parametrotitulo,'T');
        ObjPendencia.Get_ListaSaldo(parametroSaldo,Parametrotitulo,'T');
        ObjPendencia.Get_ListaBoleto(parametroBoleto,Parametrotitulo,'T');
        ObjPendencia.Get_ListaNomeBoleto(ParametroNomeBoleto,Parametrotitulo,'T');
        ObjPendencia.Get_ListaDuplicata(parametroduplicata,Parametrotitulo,'T');
        ObjPendencia.Get_ListaBoletoaPagar(parametroboletoapagar,Parametrotitulo,'T');
        ObjPendencia.Get_ListaObservacao(parametroobservacao,Parametrotitulo,'T');


     Finally
            ObjPendencia.free;
     End;

end;


procedure TObjTitulo.LancaDuplicatas(parametro:string;LancaIndividualTituloAtual:Boolean);
var
ObjPendencia:TobjPendencia;
begin
     Try

        ObjPendencia:=TobjPendencia.create;
        ObjPendencia.EscolheDuplicatas(Parametro,LancaIndividualTituloAtual);

     Finally
            ObjPendencia.free;
     End;

end;



{function TObjTitulo.Get_QuantRegsPendencias(
  ParametroTitulo: string): Integer;
var
ObjPendencia:TobjPendencia;
begin
     Try
        ObjPendencia:=TobjPendencia.create;
        Result:=ObjPendencia.Get_QuantRegs(ParametroTitulo);
     Finally
            ObjPendencia.free;
     End;
end;}
//retorna 0 se n�o encontrou
//caso contrario retorna o n� do titulo
function TObjTitulo.LocalizaLancExterno(ParametroGerador,
  ParametroCodigoGerador: string): Integer;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add(' Select CODIGO from tabtitulo where gerador='+parametrogerador+ '  and  CodigoGerador='+parametrocodigogerador);
           Open;
           If (recordcount>0)
           Then Result:=FIELDBYNAME('CODIGO').ASINTEGER
           Else Result:=0;
       End;
end;

function TObjTitulo.LocalizaGeradorCodigoPorObjeto(Parametro: string): Integer;
begin
     result:=Self.GERADOR.LocalizaCodigoporObjeto(Parametro);
end;

function TObjTitulo.LocalizaCredorDevedorporTabela(Parametro: string): Integer;
begin
     Result:=Self.CREDORDEVEDOR.LocalizaTabela(Parametro);
end;

function TObjTitulo.LocalizaGeradorContaGerencialPorObjeto(
  Parametro: string): Integer;
begin
      result:=Self.GERADOR.LocalizaContaGerencialporObjeto(Parametro);
end;


function TObjTitulo.Get_SomaLancExterno(ParametroGerador,
  ParametroCodigoGerador: string): currency;
begin
      With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add(' Select sum(valor) as Soma from tabtitulo where gerador='+parametrogerador+ '  and  CodigoGerador='+parametrocodigogerador);
           Open;
           If (recordcount>0)
           Then Result:=FIELDBYNAME('SOMA').ASfloat
           Else Result:=0;
       End;
end;


function TObjTitulo.Get_CREDORDEVEDORCODIGO: string;
begin
     If (Self.CREDORDEVEDOR.LocalizaCodigo(Self.CREDORDEVEDOR.get_codigo)=False)
     Then result:=''
     Else Begin
               Self.CREDORDEVEDOR.TabelaparaObjeto;
               result:=Self.credordevedor.Get_CODIGO;
     End;
end;


procedure TObjTitulo.ImprimetitulosLancados;
var
saida:boolean;
DataLimite,DataLimite2:Tdate;
CodigoCredorDevedorTemp,CredorDevedorTemp:Integer;
ContaGerencialTemp:TStringList;
temp:Integer;
tempstr:string;
SomaCP,SomaCR:Currency;
PAG_REC:string;
cont:integer;
ptipo:string;

PcredorDevedor,PCodigoCredorDevedor:string;

begin
     With FopcaoRel do
     Begin
          With RgOpcoes do
          Begin
                items.clear;
                items.add('A Pagar');//0
                items.add('A Receber');//1
                items.add('Todos');//2
          End;
          showmodal;
          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                  0:PAG_REC:='P';
                  1:PAG_REC:='R';
                  2:PAG_REC:='';
          End;

     End;

     limpaedit(Ffiltroimp);

     Try
        ContaGerencialTemp:=TStringList.create;
     Except
        Messagedlg('Erro na tentativa de criar a StringList "ContaGerencialTemp"',mterror,[mbok],0);
        exit;
     End;
Try

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.enabled:=true;
          Grupo02.enabled:=true;
          Grupo05.enabled:=true;
                    Grupo07.Enabled:=True;

          lbGrupo01.caption:='Data Inicial';
          lbGrupo02.caption:='Data Final';
          edtGrupo01.EditMask:='!99/99/9999;1;_';
          edtGrupo02.EditMask:='!99/99/9999;1;_';

          edtgrupo05.EditMask:='';
          If (PAG_REC='P')
          Then  edtgrupo05.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else If (Pag_REC='R')
               Then  edtgrupo05.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown
               Else edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown;

          LbGrupo05.caption:='Conta Gerencial';

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;
          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;

          edtgrupo05.Color:=$005CADFE;
          edtgrupo07.Color:=$005CADFE;

          showmodal;

          If tag=0
          Then exit;
          //Conferindo o que foi lancado

          Try
                DataLimite:=Strtodate(edtgrupo01.text);
                DataLimite2:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
                exit;
          End;

          Try
                CredorDevedorTemp:=-1;
                if (ComboGrupo07.itemindex>=0)
                Then CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=-1;
             if (EdtGrupo07.text<>'')
             then CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;

          Try
             TempStr:='';

             ContaGerencialTemp.clear;
             for Temp:=1 to length(EdtGrupo05.text) do
             Begin
                   If ((edtgrupo05.text[temp]=';')
                   or (temp=length(EdtGrupo05.text)))
                   Then Begin
                           if ((temp=length(EdtGrupo05.text)) and (edtgrupo05.text[temp]<>';'))
                           Then TempStr:=TempStr+edtgrupo05.text[temp];

                           If (Tempstr<>'')
                           Then Begin
                                   Try
                                           Strtoint(tempstr);
                                           ContaGerencialTemp.add(Tempstr);
                                   Except
                                           ContaGerencialTemp.clear;
                                           Break;
                                   End;
                           End;
                           TempStr:='';
                   End
                   Else TempStr:=TempStr+edtgrupo05.text[temp];
             End;
          Except
                ContaGerencialTemp.clear;
          End;

     End;//with das opcoes


     With Self.ObjDataset do
     Begin
          Frelatorio.DesativaCampos;
          Frelatorio.QR.DataSet:=Self.ObjDataset;

          close;
          SelectSQL.clear;
          SelectSql.add('select tabtitulo.codigo as CODTITULO,');
          SelectSql.add('tabtitulo.Historico,');

          SelectSql.add('tabtitulo.credordevedor,');
          SelectSql.add('tabtitulo.codigocredordevedor,');


          SelectSql.add('TAbtitulo.contagerencial,');
          SelectSql.add('tabcontager.nome as NOMECONTAGERENCIAL,');
          SelectSql.add('TabContaGer.Tipo,');
          
          SelectSql.add('tabpendencia.codigo CODPENDENCIA,');
          SelectSql.add('tabpendencia.vencimento,');
          SelectSql.add('tabpendencia.valor AS VALORPENDENCIA,');
          SelectSql.add('tabpendencia.saldo AS SALDOPENDENCIA');
          
          SelectSql.add('from tabpendencia join tabtitulo on ');
          SelectSql.add('tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add(' where (Tabtitulo.emissao>='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39+' and Tabtitulo.emissao<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite2)+#39+')' );


          //**********************************************************************
          If(ContaGerencialTemp.count<>0)
          Then Begin
                      SelectSql.add(' and (Tabtitulo.ContaGerencial='+ContaGerencialTemp[0]);
                      for temp:=1 to ContaGerencialTemp.count-1 do
                      Begin
                              SelectSql.add(' or Tabtitulo.ContaGerencial='+ContaGerencialTemp[temp]);
                      End;
                      SelectSql.add(' )');
          End//se naum tiver escolhido CG, ele pega conforme o escolhido
          Else Begin
                    If (PAG_REC='P')//a pagar
                    Then SelectSql.add(' and TabContager.Tipo=''D'' ')
                    Else
                        If (PAG_REC='R')//a receber
                        Then SelectSql.add(' and TabContager.Tipo=''C'' ');
          End;
          //**********************************************************************

          If (CredorDevedorTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If (CodigoCredorDevedorTemp<>-1)
          Then SelectSQL.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));

          SelectSQL.add(' order by TabContaGer.Tipo,tabtitulo.credordevedor,Tabtitulo.codigocredordevedor,tabpendencia.vencimento');

          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               LinhaLocal:=3;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               then Begin
                        RDprint.Fechar;
                        exit;
               End;
               RDprint.ImpC(linhalocal,65,Self.NumeroRelatorio+'T�TULOS LAN�ADOS',[negrito]);
               IncrementaLinha(2);

               RDprint.ImpF(linhalocal,1,'Relat�rio de T�tulos com Emiss�o - '+DatetoStr(DataLimite)+' a '+DatetoStr(DataLimite2),[negrito]);
               IncrementaLinha(1);

               If (ContaGerencialTemp.count=1)
               Then Begin
                         RDprint.ImpF(linhalocal,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(ContaGerencialTemp[0]),[negrito]);
                         IncrementaLinha(1);
               End
               Else Begin
                         If (ContaGerencialTemp.count>1)
                         Then Begin
                                   for cont:=0 to ContaGerencialTemp.count-1 do
                                   Begin
                                        VerificaLinha;
                                        RDprint.ImpF(linhalocal,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(ContaGerencialTemp[cont]),[negrito]);
                                        IncrementaLinha(1);
                                   End;
                         End
               End;

               If (CredorDevedorTemp<>-1)
               Then Begin
                         If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                         Then Begin
                                Self.CREDORDEVEDOR.TabelaparaObjeto;

                                IF (CodigoCredorDevedorTemp<>-1)
                                Then RDprint.ImpF(linhalocal,1,Self.CredorDevedor.Get_nome+' = '+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),Inttostr(CodigoCredorDevedorTemp)),[negrito])
                                Else RDprint.ImpF(linhalocal,1,Self.CredorDevedor.Get_nome,[negrito]);
                                IncrementaLinha(1);
                         End;
               End;
               IncrementaLinha(1);

               //titulo das colunas
               Rdprint.Impf(linhalocal,10,
                                         CompletaPalavra('CONTA GERENCIAL',30,' ')+' '+
                                         CompletaPalavra('TITULO',6,' ')+' '+
                                         completapalavra('HIST�RICO',40,' ')+' '+
                                         completapalavra('PEND.',6,' ')+' '+
                                         completapalavra('VENCIM.',8,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('SALDO',10,' '),[negrito]);
               IncrementaLinha(2);

               desenhalinha;

               SomaCP:=0;
               SomaCR:=0;
               Ptipo:=fieldbyname('tipo').asstring;


               if (Ptipo='D')//DEBITO
               THEN BEGIN
                         VerificaLinha;
                         Rdprint.Impf(linhalocal,1,'T�TULOS A PAGAR',[negrito]);
                         IncrementaLinha(2);
               END
               Else Begin
                         VerificaLinha;
                         Rdprint.Impf(linhalocal,1,'T�TULOS A RECEBER',[negrito]);
                         IncrementaLinha(2);
               End;

               PcredorDevedor:=fieldbyname('credordevedor').asstring;
               PCodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;
               VerificaLinha;
               Rdprint.Imp(linhalocal,1,PCodigoCredorDevedor+'-'+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PCodigoCredorDevedor));
               IncrementaLinha(2);


               While not(Self.objdataset.eof) do
               Begin//PERCORRE TODOS os titulos

                    if (Ptipo<>fieldbyname('tipo').asstring)
                    Then Begin
                              //trocou de receber para pagar (ordem alfabetica me garante que primeiro vem receber (CREDITO e DEBITO))
                              IncrementaLinha(1);
                              VerificaLinha;
                              Rdprint.Impf(linhalocal,1,'T�TULOS A RECEBER R$: '+formata_valor(floattostr(somacR)),[negrito]);
                              IncrementaLinha(1);

                              rdprint.Novapagina;
                              LinhaLocal:=3;

                              VerificaLinha;
                              Rdprint.Impf(linhalocal,1,'T�TULOS A PAGAR',[negrito]);
                              IncrementaLinha(1);
                              ptipo:=fieldbyname('tipo').asstring;

                              //toda vez que troca de receber para pagar ele
                              //tem q mostrar de novo o Forneedor ou Cliente
                              PcredorDevedor:='';
                              PCodigoCredorDevedor:='';
                    End;



                    if ((PcredorDevedor<>fieldbyname('credordevedor').asstring)
                     or (PCodigoCredorDevedor<>fieldbyname('codigocredordevedor').asstring))
                    Then begin
                              IncrementaLinha(1);
                              PcredorDevedor:=fieldbyname('credordevedor').asstring;
                              PCodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;

                              VerificaLinha;
                              Rdprint.Imp(linhalocal,1,PCodigoCredorDevedor+'-'+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PCodigoCredorDevedor));
                              IncrementaLinha(2);
                    End;

                    VerificaLinha;
                    Rdprint.Impf(linhalocal,10,
                                             CompletaPalavra(fieldbyname('contagerencial').asstring+'-'+fieldbyname('nomecontagerencial').asstring,30,' ')+' '+
                                             completapalavra(fieldbyname('codtitulo').asstring,6,' ')+' '+
                                             completapalavra(fieldbyname('historico').asstring,40,' ')+' '+
                                             completapalavra(fieldbyname('codpendencia').asstring,6,' ')+' '+
                                             completapalavra(formatdatetime('dd/mm/yy',fieldbyname('vencimento').ASDATETIME)  ,8,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORPENDENCIA').asstring),10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALDOPENDENCIA').asstring),10,' '),[italico]);
                    IncrementaLinha(1);




                    If (FieldByname('Tipo').asstring='D')
                    Then SomaCP:=SomaCP+fieldbyname('valorpendencia').asfloat
                    Else SomaCR:=SomaCR+fieldbyname('valorpendencia').asfloat;

                    Self.objdataset.next;
               End;
               IncrementaLinha(1);
               
               if (Ptipo='D')//DEBITO
               THEN BEGIN
                         VerificaLinha;
                         Rdprint.Impf(linhalocal,1,'TOTAL  A PAGAR    R$ '+formata_valor(somacP),[negrito]);
                         IncrementaLinha(2);
               END
               Else Begin
                         VerificaLinha;
                         Rdprint.Impf(linhalocal,1,'TOTAL  A RECEBER  R$ '+formata_valor(somacR),[negrito]);
                         IncrementaLinha(2);
               End;

               rdprint.Fechar;
               
          End;
     End;
Finally
       FreeAndNil(ContaGerencialTemp);
End;

end;

procedure TObjTitulo.ImprimetitulosLancados_por_ContaGerencial(ComTitulodeSaldo:boolean);
var
saida:boolean;
DataLimite,DataLimite2:Tdate;
CodigoCredorDevedorTemp,CredorDevedorTemp,GeradorTemp:Integer;
GeradorTexto:String;
ContaGerencialTemp:TStringList;
temp:Integer;
tempstr:string;
ValorCG,SomaCP,SomaCR:Currency;
Tipotemp:string;
CGTemp:string;
begin
     limpaedit(Ffiltroimp);

     Try
        ContaGerencialTemp:=TStringList.create;
     Except
        Messagedlg('Erro na tentativa de criar a StringList "ContaGerencialTemp"',mterror,[mbok],0);
        exit;
     End;
Try

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.enabled:=true;
          Grupo02.enabled:=true;
          Grupo05.enabled:=true;
          Grupo06.Enabled:=True;
          Grupo07.Enabled:=True;

          lbGrupo01.caption:='Data Inicial';
          lbGrupo02.caption:='Data Final';
          edtGrupo01.EditMask:='!99/99/9999;1;_';
          edtGrupo02.EditMask:='!99/99/9999;1;_';
          
          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown;
          LbGrupo05.caption:='Conta Gerencial';
          edtgrupo05.Color:=$005CADFE;

          LbGrupo06.Caption:='Geradores';
          ComboGrupo06.items.clear;
          Combo2Grupo06.items.clear;

          Self.Get_ListaGerador(ComboGrupo06.items);
          Self.Get_ListaGeradorCodigo(Combo2Grupo06.items);


          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
          edtgrupo07.Color:=$005CADFE;


          showmodal;

          If tag=0
          Then exit;
          //Conferindo o que foi lancado

          Try
                DataLimite:=Strtodate(edtgrupo01.text);
                DataLimite2:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
                exit;
          End;

          Try
                GeradorTemp:=strtoint(Combo2Grupo06.Items[ComboGrupo06.itemindex]);
                GeradorTExto:='';

                If geradortemp=-1
                Then GeradorTemp:=strtoint('  ')
                Else GeradorTExto:=ComboGrupo06.items[ComboGrupo06.itemindex];

          Except
                GeradorTemp:=-1;
                Messagedlg('N�o foi escolhido nenhum Gerador ser� pesquisado por Todos os Geradores',mtinformation,[mbok],0);
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;

          Try
             TempStr:='';
             ContaGerencialTemp.clear;
             for Temp:=1 to length(EdtGrupo05.text) do
             Begin
                   If ((edtgrupo05.text[temp]=';')
                   or (temp=length(EdtGrupo05.text)))
                   Then Begin
                           if ((temp=length(EdtGrupo05.text)) and (edtgrupo05.text[temp]<>';'))
                           Then TempStr:=TempStr+edtgrupo05.text[temp];

                           If (Tempstr<>'')
                           Then Begin
                                   Try
                                           Strtoint(tempstr);
                                           ContaGerencialTemp.add(Tempstr);
                                   Except
                                           ContaGerencialTemp.clear;
                                           Break;
                                   End;
                           End;
                           TempStr:='';
                   End
                   Else TempStr:=TempStr+edtgrupo05.text[temp];
             End;
          Except
                ContaGerencialTemp.clear;
          End;


     End;//with das opcoes


     With Self.ObjDataset do
     Begin
          Frelatorio.DesativaCampos;
          Frelatorio.QR.DataSet:=Self.ObjDataset;

          close;                                                                                   
          SelectSQL.clear;
          SelectSql.add('select tabtitulo.codigo as CODTITULO,tabtitulo.Historico,tabtitulo.NumDcto,tabtitulo.contagerencial,TabContaGer.Nome as NomeContaGerencial,TabContaGer.Tipo,tabtitulo.valor');
          SelectSql.add('from tabtitulo join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add(' where (Tabtitulo.emissao>='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39+' and Tabtitulo.emissao<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite2)+#39+')' );

          If (GeradorTemp<>-1)
          Then SelectSql.add(' and Tabtitulo.Gerador='+inttostr(geradortemp));

          //**********************************************************************
          If(ContaGerencialTemp.count<>0)
          Then Begin
                      SelectSql.add(' and (Tabtitulo.ContaGerencial='+ContaGerencialTemp[0]);
                      for temp:=1 to ContaGerencialTemp.count-1 do
                      Begin
                              SelectSql.add(' or Tabtitulo.ContaGerencial='+ContaGerencialTemp[temp]);
                      End;
                      SelectSql.add(' )');
          End;
          //**********************************************************************



          If (CredorDevedorTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If (CodigoCredorDevedorTemp<>-1)
          Then SelectSQL.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));

          If (ComTitulodeSaldo=False)
          Then SelectSQL.add(' and tabtitulo.Geradopelosistema=''N'' ');

          SelectSQL.add(' order by TabContaGer.tipo,TabTitulo.ContaGerencial,tabtitulo.codigo');
          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;


          first;
          SomaCP:=0;
          SomaCR:=0;


          FrelTXT.SB.Items.clear;
          //cabe�alho da colunas
          // o '?' � sinal de negrito para o relat�rio, eu que defini isto
          FrelTXT.SB.Items.add('?'+CompletaPalavra('TITULO',6,' ')+'|'+completapalavra('HIST�RICO',60,' ')+'|'+completapalavra('NUMDCTO',18,' ')+'|'+completapalavra('CGER',09,' ')+'|'+CompletaPalavra_a_Esquerda('VALOR',10,' ')+'|');
          FrelTXT.SB.Items.add('?'+CompletaPalavra('',6,'-')+'-'+completapalavra('',60,'-')+'-'+completapalavra('',18,'-')+'-'+completapalavra('',09,'-')+'-'+CompletaPalavra_a_Esquerda('',10,'-')+'-');

          IF FieldByname('Tipo').asstring='D'
          Then FrelTXT.SB.Items.add(Self.NumeroRelatorio+'?T�TULOS A PAGAR')
          Else FrelTXT.SB.Items.add(Self.NumeroRelatorio+'?T�TULOS A RECEBER');
          FrelTXT.SB.Items.add(' ');
          TipoTemp:=FieldByname('Tipo').asstring;
          CGTemp:=FieldByname('ContaGerencial').asstring;
          FrelTXT.SB.Items.add('?'+Fieldbyname('NomeContaGerencial').asstring);
          ValorCG:=0;



          While not(eof) do
          Begin//PERCORRE TODOS os titulos

               If (TipoTemp<>FieldByname('Tipo').asstring)
               Then Begin//trocou de Receber para Pagar
                         FrelTXT.SB.Items.add(' ');
                         IF FieldByname('Tipo').asstring='D'
                         Then FrelTXT.SB.Items.add('?T�TULOS A PAGAR')
                         Else FrelTXT.SB.Items.add('?T�TULOS A RECEBER');
                         FrelTXT.SB.Items.add(' ');
                         TipoTemp:=FieldByname('Tipo').asstring;
               End;

               If (Fieldbyname('Contagerencial').asstring<>CGTemp)
               Then Begin//trocou de conta gerencial
                         //Totalizando a Anterior
                         FrelTXT.SB.Items.add(' ');
                         FrelTXT.SB.Items.add('?Soma Conta Gerencial R$ '+formata_valor(floattostr(valorCg)));
                         FrelTXT.SB.Items.add(' ');
                         //nome da nova conta gerencial
                         FrelTXT.SB.Items.add('?'+Fieldbyname('NomeContaGerencial').asstring);
                         FrelTXT.SB.Items.add(' ');
                         ValorCg:=0;
                         CGTemp:=FieldByname('ContaGerencial').asstring;
               End;
               

               FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codtitulo').asstring,6,' ')+'|'+completapalavra(fieldbyname('historico').asstring,60,' ')+'|'+completapalavra(fieldbyname('NUmdcto').asstring,18,' ')+'|'+completapalavra(fieldbyname('contagerencial').asstring,5,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),10,' '));

               If (FieldByname('Tipo').asstring='D')
               Then SomaCP:=SomaCP+fieldbyname('valor').asfloat
               Else SomaCR:=SomaCR+fieldbyname('valor').asfloat;
               ValorCg:=ValorCg+fieldbyname('valor').asfloat;

               next;
          End;
          //Totalizando a Ultima Conta Gerencial
          FrelTXT.SB.Items.add(' ');
          FrelTXT.SB.Items.add('?Soma Conta Gerencial R$ '+formata_valor(floattostr(valorCg)));
          FrelTXT.SB.Items.add(' ');
                         



          FrelTXT.SB.Items.add(CompletaPalavra('-',6,'-')+'-'+completapalavra('-',80,'-')+'-'+completapalavra('-',5,'-')+'-'+completapalavra('-',5,'-')+'-'+completapalavra('-',8,'-')+'-'+CompletaPalavra_a_Esquerda('-',10,'-')+'-'+CompletaPalavra_a_Esquerda('-',10,'-'));
          FrelTXT.SB.Items.add('? T�TULOS A PAGAR R$: '+formata_valor(floattostr(somacp)));
          FrelTXT.SB.Items.add('? T�TULOS A RECEBER R$ '+formata_valor(floattostr(somacr)));

          //configurando os campos
          With FRELTXT do
          Begin
               LimpaCampos;//LIMPA E DESATIVA
               lbtitulo.Enabled:=True;
               lbtitulo.caption:='Relat�rio de T�tulos com Emiss�o - '+DatetoStr(DataLimite)+' a '+DatetoStr(DataLimite2);

               If GeradorTemp<>-1
               Then lbtitulo.caption:=lbtitulo.caption+' - '+GeradorTexto;

               If (ContaGerencialTemp.count=1)
               Then Begin
                         LbSubtitulo1.Enabled:=True;
                         LbSubtitulo1.caption:='Conta Gerencial->'+Self.Get_NomeContaGerencial(ContaGerencialTemp[0]);
                    End
               Else Begin
                         If (ContaGerencialTemp.count>1)
                         Then Begin
                                LbSubtitulo1.Enabled:=True;
                                LbSubtitulo1.caption:='Conta Gerencial-> V�RIAS';
                         End
               End;

               If (CredorDevedorTemp<>-1)
               Then Begin
                         If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                         Then Begin
                                Self.CREDORDEVEDOR.TabelaparaObjeto;
                                LbSubtitulo2.Enabled:=True;
                                LbSubtitulo2.caption:=Self.CredorDevedor.Get_nome;
                                IF (CodigoCredorDevedorTemp<>-1)
                                Then LbSubtitulo2.caption:=LbSubtitulo2.caption+' = '+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),Inttostr(CodigoCredorDevedorTemp));
                              End;
                    End;
              Qr.Page.Orientation:=poLandscape;
              Qr.preview;
          End;
     End;

Finally
       FreeAndNil(ContaGerencialTemp);
End;

end;

procedure TObjTitulo.ImprimeTitulosEmAtrasoDOIS(TiPO_DC:string);
var
saida:boolean;
DataLimite:Tdate;
ContaGerencialTemp,CredorDevedorTemp,CodigoCredorDevedorTemp:Integer;
GeradorTExto:String;
Qlocal:Tibquery;
Tfornecedor,TcredorDevedor:string;
SomaGeral,Somafornecedor:currency;
linha:integer;
strtemp:string;
Begin
        Try
                Qlocal:=Tibquery.create(nil);
                Qlocal.database:=Self.ObjDataset.database;
        Except
              Messagedlg('Erro na tentativa de cria��o Query!',mterror,[mbok],0);
              exit;
        End;
Try

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo05.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Limite';

          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.Edtcontagerencial_SPV_KeyDown;
          LbGrupo05.caption:='Conta Gerencial';
          edtgrupo05.Color:=$005CADFE;

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedor_SPV_KeyDown;
          edtgrupo07.color:=$005CADFE;

          showmodal;
          If tag=0
          Then exit;
          Try
                DataLimite:=Strtodate(edtgrupo01.text);
          Except
                Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);

          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;

          Try
             ContaGerencialTemp:=Strtoint(edtgrupo05.text);
             If (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False)
             Then ContaGerencialTemp:=-1
             Else Begin
                       Self.CONTAGERENCIAL.TabelaparaObjeto;
                       If (Self.CONTAGERENCIAL.Get_Tipo<>TIPO_DC)
                       Then Begin
                                 Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                                 exit;
                            End;
                  End;
          Except
             ContaGerencialTemp:=-1;
          End;
     End;


     With Qlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('select tabtitulo.codigo,tabpendencia.vencimento,tabpendencia.saldo,tabtitulo.numdcto,tabtitulo.notafiscal');
          SQL.add('from tabpendencia left join tabtitulo');
          SQL.add('on tabpendencia.titulo=tabtitulo.codigo');
          SQL.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');

          SQL.add(' where Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39+'  and Saldo>0 ');
          SQL.add(' and tabcontager.tipo='+#39+TIPO_DC+#39);

          If (ContagerencialTemp<>-1)
          Then SQL.add(' and TabTitulo.ContaGerencial='+INttostr(COntaGerencialTemp));

          If (CredorDevedorTemp<>-1)
          Then SQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp<>-1)
          Then SQL.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));


          sql.add('order by tabtitulo.credordevedor,tabtitulo.codigocredordevedor');

          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          Self.LocalizaCodigo(fieldbyname('codigo').asstring);
          Self.TabelaparaObjeto;

          FreltxtRDPRINT.ConfiguraImpressao;
          linha:=3;
          FreltxtRDPRINT.rdprint.abrir;
          if ((FreltxtRDPRINT.RDprint.Setup)=False)
          Then Begin
                    FreltxtRDPRINT.rdprint.fechar;
                    exit;
          End;

          If (TIPO_DC='D')
          Then FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'Relat�rio de T�tulos a Pagar DATA '+DATETOSTR(DataLimite),[negrito])
          Else FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'Relat�rio de T�tulos a Receber DATA '+DATETOSTR(DataLimite),[negrito]);
          inc(linha,2);

          If (ContaGerencialTemp<>-1)
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)),[negrito]);
                    inc(linha,1);

          End;

          If (CredorDevedorTemp<>-1)
          Then Begin
                    If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                    Then Begin
                              Self.CREDORDEVEDOR.TabelaparaObjeto;
                              strtemp:='';
                              strtemp:=Self.CredorDevedor.Get_nome;
                              IF (CodigoCredorDevedorTemp<>-1)
                              Then strtemp:=strtemp+' = '+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),Inttostr(CodigoCredorDevedorTemp));
                              FreltxtRDPRINT.RDprint.Impf(linha,1,strtemp,[negrito]);
                              inc(linha,1);
                    End;
          End;

          //200808 adicionado por celio - a coluna NF para exibicao das nfs e notas de debito
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('EMISSAO',10,' ')+' '+CompletaPalavra('FORNECEDOR/CLIENTE',30,' ')+' '+CompletaPalavra('TITULO',9,' ')+' '+CompletaPalavra('NF',15,' ')+' '+CompletaPalavra('VENCIMENTO',10,' ')+' '+CompletaPalavra_a_Esquerda('SALDO',15,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'----------------------------------------------------------------------------------------------',[negrito]);
          inc(linha,1);

          //primeiro fornecedor
          Tfornecedor:=Self.codigocredordevedor;
          TcredorDevedor:=Self.CREDORDEVEDOR.get_codigo;
          Somafornecedor:=0;
          SomaGeral:=0;

          While not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;

               If (Tfornecedor<>Self.CODIGOCREDORDEVEDOR)
               or(TcredorDevedor<>Self.CREDORDEVEDOR.Get_CODIGO)
               Then Begin//totalizando por fornecedor
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'Total do Fornecedor: R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);
                         inc(linha,1);
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Imp(linha,1,'----------------------------------------------------------------------------------------------');
                         inc(linha,1);
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);

                         Somafornecedor:=0;
                         Tfornecedor:=Self.CODIGOCREDORDEVEDOR;
                         TcredorDevedor:=Self.CREDORDEVEDOR.Get_CODIGO;
               End;
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(Self.EMISSAO,10,' ')+' '+CompletaPalavra(Self.CREDORDEVEDOR.Get_RazaoSocialCredorDevedor(Self.credordevedor.get_codigo,self.codigocredordevedor),30,' ')+' '+CompletaPalavra(Self.codigo,9,' ')+' '+CompletaPalavra(fieldbyname('notafiscal').asstring,15,' ')+' '+CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('saldo').asstring),15,' '));
               inc(linha,1);
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);

               SomaFornecedor:=Somafornecedor+strtofloat(fieldbyname('saldo').asstring);
               SomaGeral:=SomaGeral+strtofloat(fieldbyname('saldo').asstring);
               next;
          End;

          FreltxtRDPRINT.RDprint.Impf(linha,1,'Total do Fornecedor: R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,'----------------------------------------------------------------------------------------------');
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Total Geral        : R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaGeral)),15,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Fechar;
     End

 Finally
       Freeandnil(Qlocal);
  End;

end;


procedure TObjTitulo.Imprime_TotalaReceber_Pagar_PorFornecedor(TiPO_DC:string);
var
saida:boolean;
DataInicial:string;
DataLimite:Tdate;
ContaGerencialTemp,CredorDevedorTemp,CodigoCredorDevedorTemp:Integer;
GeradorTExto:String;
Qlocal:Tibquery;
Tfornecedor,TcredorDevedor:string;
SomaGeral,Somafornecedor:currency;
TmpQuantCredorDevedor,linha:integer;
strtemp:string;
Begin
        Try
                Qlocal:=Tibquery.create(nil);
                Qlocal.database:=Self.ObjDataset.database;
        Except
              Messagedlg('Erro na tentativa de cria��o Query!',mterror,[mbok],0);
              exit;
        End;
Try

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo05.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Data Inicial';

          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.caption:='Limite';


          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.Edtcontagerencial_SPV_KeyDown;
          LbGrupo05.caption:='Conta Gerencial';
          edtgrupo05.color:=$005CADFE;

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedor_SPV_KeyDown;
          edtgrupo07.color:=$005CADFE;

          showmodal;
          If tag=0
          Then exit;
          
          Try
                Strtodate(edtgrupo01.text);
                DataInicial:=edtgrupo01.text;
          Except
                Datainicial:='';
          End;



          Try
                DataLimite:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);

          Except
                CredorDevedorTemp:=-1;
          End;


          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;

          Try
             ContaGerencialTemp:=Strtoint(edtgrupo05.text);
             If (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False)
             Then ContaGerencialTemp:=-1
             Else Begin
                       Self.CONTAGERENCIAL.TabelaparaObjeto;
                       If (Self.CONTAGERENCIAL.Get_Tipo<>TIPO_DC)
                       Then Begin
                                 Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                                 exit;
                            End;
                  End;
          Except
             ContaGerencialTemp:=-1;
          End;
     End;


     With Qlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('select PROC.codigo as titulo,PROC.emissao,PROC.historico,PROC.vencimentopendencia,PROC.saldopendencia,PROC.credordevedor,PROC.codigocredordevedor,');
          SQL.add('proc.cd_telefone,proc.cd_celular,proc.cd_razaosocial');

          if (datainicial<>'')
          Then SQL.add('from PROCTITULO_VENCIMENTOPENDENCIA('+#39+formatdatetime('mm/dd/yyyy',strtodate(Datainicial))+#39+',')
          Else SQL.add('from PROCTITULO_VENCIMENTOPENDENCIA('+#39+'01/01/1500'+#39+',');
          SQL.add(#39+formatdatetime('mm/dd/yyyy',datalimite)+#39+','+#39+TIPO_DC+#39+') PROC where PROC.codigo <>-5000');


          If (ContagerencialTemp<>-1)
          Then SQL.add(' and PROC.ContaGerencial='+INttostr(COntaGerencialTemp));

          If (CredorDevedorTemp<>-1)
          Then SQL.add(' and PROC.CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp<>-1)
          Then SQL.add(' and PROC.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));

          sql.add('and PROC.saldopendencia>0');

          sql.add('order by PROC.cd_razaosocial');

          open;
          last;
          Self.ZerarTabela;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          InicializaBarradeProgressoRelatorio(recordcount,'Gerando Relat�rio.....');
          first;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;

          linha:=3;
          FreltxtRDPRINT.rdprint.abrir;
          if ((FreltxtRDPRINT.RDprint.Setup)=False)
          Then Begin
                    FreltxtRDPRINT.rdprint.fechar;
                    exit;
          End;

          If (TIPO_DC='D')
          Then FreltxtRDPRINT.RDprint.ImpC(linha,65,Self.NumeroRelatorio+'T�TULOS A PAGAR   -  DATA '+DATETOSTR(DataLimite),[negrito])
          Else FreltxtRDPRINT.RDprint.ImpC(linha,65,Self.NumeroRelatorio+'T�TULOS A RECEBER -  DATA '+DATETOSTR(DataLimite),[negrito]);
          inc(linha,2);

          If (ContaGerencialTemp<>-1)
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,1,'CONTA GERENCIAL->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)),[negrito]);
                    inc(linha,1);

          End;
          
          if (DataInicial<>'')
          Then FreltxtRDPRINT.rdprint.impf(linha,1,'Intervalo de Datas de Vencimento:'+DataInicial+' a '+datetostr(DataLimite),[negrito])
          Else FreltxtRDPRINT.rdprint.impf(linha,1,'Vencimento Anterior ou Igual a '+datetostr(DataLimite),[negrito]);
          inc(linha,1);

          If (CredorDevedorTemp<>-1)
          Then Begin
                    If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                    Then Begin
                              Self.CREDORDEVEDOR.TabelaparaObjeto;
                              strtemp:='';
                              strtemp:=Self.CredorDevedor.Get_nome;
                              IF (CodigoCredorDevedorTemp<>-1)
                              Then strtemp:=strtemp+' = '+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),Inttostr(CodigoCredorDevedorTemp));
                              FreltxtRDPRINT.RDprint.Impf(linha,1,strtemp,[negrito]);
                              inc(linha,1);
                    End;
          End;

          
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('EMISSAO',08,' ')+' '+CompletaPalavra('HIST�RICO',55,' ')+' '+CompletaPalavra('TELEFONE',12,' ')+' '+CompletaPalavra('CELULAR',13,' ')+' '+
                                              CompletaPalavra('TITULO',7,' ')+' '+CompletaPalavra('VENCTO',08,' ')+' '+CompletaPalavra_a_Esquerda('SALDO',15,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,1,completapalavra('-',130,'-'),[negrito]);
          inc(linha,1);

          //primeiro fornecedor
          Tfornecedor:=Fieldbyname('codigocredordevedor').asstring;
          TcredorDevedor:=Fieldbyname('credordevedor').asstring;


          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(Fieldbyname('cd_razaosocial').asstring,90,' '),[negrito]);
          inc(linha,2);

          Somafornecedor:=0;
          SomaGeral:=0;
          TmpQuantCredorDevedor:=1;

          While not(eof) do
          Begin
               IncrementaBarraProgressoRelatorio;
               Application.ProcessMessages;

               If (Tfornecedor<>fieldbyname('CODIGOCREDORDEVEDOR').asstring)
                or(TcredorDevedor<>fieldbyname('CREDORDEVEDOR').asstring)
               Then Begin//totalizando por fornecedor
                         inc(TmpQuantCredorDevedor,1);
                         
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'Total              : R$'+
                                                             CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);
                         inc(linha,1);
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('-',130,'-'));
                         inc(linha,1);


                         Tfornecedor:=Fieldbyname('codigocredordevedor').asstring;
                         TcredorDevedor:=Fieldbyname('credordevedor').asstring;

                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(Fieldbyname('cd_razaosocial').asstring,130,' '),[negrito]);
                         inc(linha,1);

                         Somafornecedor:=0;
               End;

               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(formatdatetime('dd/mm/yy',Fieldbyname('emissao').asdatetime),08,' ')+' '+
                                                  CompletaPalavra(Fieldbyname('historico').asstring,55,' ')+' '+
                                                  CompletaPalavra(Fieldbyname('cd_telefone').asstring,12,' ')+' '+
                                                  CompletaPalavra(Fieldbyname('cd_celular').asstring,13,' ')+' '+
                                                  CompletaPalavra(fieldbyname('titulo').asstring,7,' ')+' '+
                                                  CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('vencimentopendencia').asstring)),08,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('saldopendencia').asstring),15,' '));

               inc(linha,1);
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);

               SomaFornecedor:=Somafornecedor+strtofloat(fieldbyname('saldopendencia').asstring);
               SomaGeral:=SomaGeral+strtofloat(fieldbyname('saldopendencia').asstring);
               next;
          End;

          FreltxtRDPRINT.RDprint.Impf(linha,1,'Total              : R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('-',130,'-'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Total Geral        : R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaGeral)),15,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Quantidade         : '+inttostr(TmpQuantCredorDevedor),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Fechar;
          FechaBarraProgressoRelatorio;
     End;

Finally
       Freeandnil(Qlocal);
End;

end;

procedure TObjTitulo.ImprimeTitulosEmAtrasoDOISCOMFONES(TiPO_DC:string);
var
saida:boolean;
DataInicial:string;
DataLimite:Tdate;
ContaGerencialTemp,CredorDevedorTemp,CodigoCredorDevedorTemp:Integer;
GeradorTExto:String;
Qlocal:Tibquery;
Tfornecedor,TcredorDevedor:string;
SomaGeral,Somafornecedor:currency;
TmpQuantCredorDevedor,linha:integer;
GrupoCliente: string;
CodigoGrupo:string;
strtemp:string;
historicoAux:string;
Begin
        Try
                Qlocal:=Tibquery.create(nil);
                Qlocal.database:=Self.ObjDataset.database;
        Except
              Messagedlg('Erro na tentativa de cria��o Query!',mterror,[mbok],0);
              exit;
        End;
Try

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          GrupoCliente:= '';
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo05.Enabled:=True;
          Grupo07.Enabled:=True;
        //  Grupo15.Enabled:=True;    //Jonatan


          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Data Inicial';

          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.caption:='Limite';


          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.Edtcontagerencial_SPV_KeyDown;
          edtgrupo05.Color:=$005CADFE;
          LbGrupo05.caption:='Conta Gerencial';

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedor_SPV_KeyDown;
          edtgrupo07.Color:=$005CADFE;


         ShowModal;
         if(ComboGrupo07.text = 'CLIENTES')
          then
         begin
              Grupo15.Enabled:=True;
              lbgrupo15.Enabled :=true;
              ComboGrupo15.Enabled :=true;
              lbgrupo15.Caption := 'Mostrar clientes por Grupo';
              With Qlocal do
              Begin
                  Close();
                  SQL.Clear();
                  SQL.Add('select nome');
                  SQL.Add('from tabgrupocliente');
                  Open();
                  First();
                  while not (Eof) do
                  begin

                    ComboGrupo15.Items.Add(fieldbyname('nome').AsString);
                    Next;
                  end  ;

              end;
               showmodal;

          end;

          If tag=0
          Then exit;
          
          Try
                Strtodate(edtgrupo01.text);
                DataInicial:=edtgrupo01.text;
          Except
                Datainicial:='';
          End;

          Try
                DataLimite:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          CredorDevedorTemp := StrToIntDef(Combo2Grupo07.Items[ComboGrupo07.itemindex],-1);
          CodigoCredorDevedorTemp:=StrtointDef(EdtGrupo07.text,-1);


          try
             GrupoCliente := ComboGrupo15.Text
          except
              GrupoCliente:= '';
          end;

         ContaGerencialTemp := StrToIntDef(edtgrupo05.text,-1);

         if (ContaGerencialTemp <> -1) then
         begin

           if (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False) Then
            ContaGerencialTemp := -1
           else
           begin

             Self.CONTAGERENCIAL.TabelaparaObjeto;
             If (Self.CONTAGERENCIAL.Get_Tipo<>TIPO_DC) Then
             begin
              Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
              exit;
             end;
           end;

         end;

     End;

     if(GrupoCliente = '')
     then begin
         With Qlocal do
         Begin
              close;
              SQL.clear;

              SQL.add('select PROC.codigo as titulo,PROC.emissao,PROC.historico,PROC.vencimentopendencia,PROC.saldopendencia,PROC.credordevedor,PROC.codigocredordevedor,');
              SQL.add('proc.cd_telefone,proc.cd_celular,proc.cd_razaosocial,proc.observacao,proc.numdcto');

              if (datainicial<>'')
              Then SQL.add('from PROCTITULO_VENCIMENTOPENDENCIA('+#39+formatdatetime('mm/dd/yyyy',strtodate(Datainicial))+#39+',')
              Else SQL.add('from PROCTITULO_VENCIMENTOPENDENCIA('+#39+'01/01/1500'+#39+',');
              SQL.add(#39+formatdatetime('mm/dd/yyyy',datalimite)+#39+','+#39+TIPO_DC+#39+') PROC where PROC.codigo <>-5000');


              If (ContagerencialTemp<>-1)
              Then SQL.add(' and PROC.ContaGerencial='+#39+TIPO_DC+#39);

              If (CredorDevedorTemp<>-1)
              Then SQL.add(' and PROC.CredorDevedor='+INttostr(CredorDevedorTemp));

              If(CodigoCredorDevedorTemp<>-1)
              Then SQL.add(' and PROC.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));

              sql.add('and PROC.saldopendencia>0');

              sql.add('order by PROC.CredorDevedor,PROC.CodigoCredorDevedor');

              //InputBox('','',sql.Text);
              open;

              Last;
              InicializaBarradeProgressoRelatorio(RecordCount, 'Gerando Relat�rio.....');
              If (RecordCount=0)
              Then Begin
                        Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                        exit;
              End;
              first;
              FreltxtRDPRINT.ConfiguraImpressao;
              linha:=3;
              FreltxtRDPRINT.rdprint.abrir;
              if ((FreltxtRDPRINT.RDprint.Setup)=False)
              Then Begin
                        FreltxtRDPRINT.rdprint.fechar;
                        exit;
              End;

              If (TIPO_DC='D')
              Then FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'Relat�rio de T�tulos a Pagar DATA '+DATETOSTR(DataLimite),[negrito])
              Else FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'Relat�rio de T�tulos a Receber DATA '+DATETOSTR(DataLimite),[negrito]);
              inc(linha,2);

              If (ContaGerencialTemp<>-1)
              Then Begin
                        FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)),[negrito]);
                        inc(linha,1);

              End;

              if (DataInicial<>'')
              Then FreltxtRDPRINT.rdprint.impf(linha,1,'Intervalo de Datas de Vencimento: '+DataInicial+' a '+datetostr(DataLimite),[negrito])
              Else FreltxtRDPRINT.rdprint.impf(linha,1,'Vencimento Anterior ou Igual a '+datetostr(DataLimite),[negrito]);
              inc(linha,1);

              If (CredorDevedorTemp<>-1)
              Then Begin
                        If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                        Then Begin
                                  Self.CREDORDEVEDOR.TabelaparaObjeto;
                                  strtemp:='';
                                  strtemp:=Self.CredorDevedor.Get_nome;
                                  IF (CodigoCredorDevedorTemp<>-1)
                                  Then strtemp:=strtemp+' = '+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),Inttostr(CodigoCredorDevedorTemp));
                                  FreltxtRDPRINT.RDprint.Impf(linha,1,strtemp,[negrito]);
                                  inc(linha,1);
                        End;
              End;

              FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('EMISSAO',08,' ')+' '+CompletaPalavra('FORN/CLIENTE (NUMDOCTO)',35,' ')+' '+CompletaPalavra('TELEFONE',10,' ')+' '+CompletaPalavra('CELULAR',12,' ')+' '+
                                                  CompletaPalavra('TITULO',09,' ')+' '+CompletaPalavra('VENCIM.',08,' ')+' '+CompletaPalavra_a_Esquerda('SALDO',08,' '),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.RDprint.Impf(linha,1,'------------------------------------------------------------------------------------------------',[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(Fieldbyname('cd_razaosocial').asstring,80,' '),[negrito]);
              inc(linha,1);
              //primeiro fornecedor
              Tfornecedor:=fieldbyname('CODIGOCREDORDEVEDOR').asstring;
              TcredorDevedor:=fieldbyname('CREDORDEVEDOR').asstring;
              Somafornecedor:=0;
              SomaGeral:=0;
              TmpQuantCredorDevedor:=1;
              While not(eof) do
              Begin
                   IncrementaBarraProgressoRelatorio;

                   If (Tfornecedor<>fieldbyname('CODIGOCREDORDEVEDOR').asstring)
                    or(TcredorDevedor<>fieldbyname('CREDORDEVEDOR').asstring)
                   Then Begin//totalizando por fornecedor
                             inc(TmpQuantCredorDevedor,1);

                             FreltxtRDPRINT.RDprint.Impf(linha,1,'Total do Devedor: R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);

                             inc(linha,1);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                             FreltxtRDPRINT.RDprint.Impf(linha,1,'------------------------------------------------------------------------------------------------',[negrito]);
                             inc(linha,1);


                             Tfornecedor:=Fieldbyname('codigocredordevedor').asstring;
                             TcredorDevedor:=Fieldbyname('credordevedor').asstring;

                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                             FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(Fieldbyname('cd_razaosocial').asstring,80,' '),[negrito]);
                             inc(linha,1);

                             Somafornecedor:=0;
                   End;

                   if (fieldbyname('numdcto').AsString = '') then
                     historicoAux := Fieldbyname('historico').asstring
                   else
                    historicoAux  := trunca_string(Fieldbyname('historico').asstring,23)+'('+fieldbyname('numdcto').AsString+')';

                   FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(formatdatetime('dd/mm/yy',Fieldbyname('emissao').asdatetime),08,' ')+' '+
                                                      CompletaPalavra(historicoAux,35,' ')+' '+
                                                      CompletaPalavra(Fieldbyname('cd_telefone').asstring,10,' ')+' '+
                                                      CompletaPalavra(Fieldbyname('cd_celular').asstring,12,' ')+' '+
                                                      CompletaPalavra(fieldbyname('titulo').asstring,09,' ')+' '+
                                                      CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('vencimentopendencia').asstring)),08,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('saldopendencia').asstring),08,' '));

                   inc(linha,1);
                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);

                   SomaFornecedor:=Somafornecedor+strtofloat(fieldbyname('saldopendencia').asstring);
                   SomaGeral:=SomaGeral+strtofloat(fieldbyname('saldopendencia').asstring);
                   if(Fieldbyname('observacao').AsString <>'') then
                   begin
                        FreltxtRDPRINT.RDprint.Imp(linha,1,'Observa��o: ' +Fieldbyname('observacao').AsString);
                        inc(linha,1);
                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                   end;

                   next;
              end;
              FreltxtRDPRINT.RDprint.Impf(linha,1,'Total do Devedor: R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
              FreltxtRDPRINT.RDprint.Imp(linha,1,'------------------------------------------------------------------------------------------------');
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
              FreltxtRDPRINT.RDprint.Impf(linha,1,'Total Geral        : R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaGeral)),15,' '),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
              FreltxtRDPRINT.RDprint.Impf(linha,1,'Quantidade de Devedores: '+inttostr(TmpQuantCredorDevedor),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.RDprint.Fechar;
              FechaBarraProgressoRelatorio;
         End;
     end
     else
     begin

         With Qlocal do
         Begin

              if(GrupoCliente <> '')
              then begin
                  try
                      close;
                      SQL.clear;
                      SQL.Add('select codigo');
                      SQL.Add('from tabgrupocliente');
                      SQL.Add('where nome ='+#39+GrupoCliente+#39);
                      Open;
                      Last;
                      CodigoGrupo := fieldbyname('codigo').AsString;
                  except
                      ShowMessage('Erro na pesquisa');
                  end;
              end;
              close;
              SQL.clear;
              SQL.Add('select sum(saldo),tabtitulo.codigo,tabpendencia.vencimento,tabtitulo.codigocredordevedor, tabcliente.nome,');
              SQL.Add('tabpendencia.codigo,tabpendencia.valor,tabcontager.nome, tabcliente.celular, tabcliente.fone,tabtitulo.historico,tabpendencia.datac');
              SQL.Add('from tabpendencia');
              SQL.Add('join tabtitulo on tabpendencia.titulo= tabtitulo.codigo');
              SQL.Add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
              SQL.Add('join TABCREDORDEVEDOR on tabtitulo.credordevedor=tabcredordevedor.codigo');
              SQL.Add('left join tabsubcontagerencial on tabtitulo.subcontagerencial=tabsubcontagerencial.codigo');
              SQL.Add('join tabcliente on tabcliente.codigo = tabtitulo.codigocredordevedor');
              //SQL.Add('left join tabgrupocliente on tabgrupocliente.codigo = tabcliente.grupo');
              SQL.add('where Tabpendencia.vencimento> ''05/01/2010''');
              SQL.Add('and Tabpendencia.vencimento< ''05/30/2010''');
              SQL.Add('and tabpendencia.saldo>''0''');
              SQL.Add('and tabcontager.tipo = ''C''');
              SQL.Add('and tabcliente.grupo = ' +CodigoGrupo);
              SQL.Add(' group by  tabtitulo.codigo,tabpendencia.vencimento,tabtitulo.codigocredordevedor, tabcliente.nome,');
              SQL.Add('tabpendencia.codigo,tabpendencia.valor,tabcontager.nome, tabcliente.celular, tabcliente.fone,tabtitulo.historico,tabpendencia.datac');


              {If (ContagerencialTemp<>-1)
              Then SQL.add(' and PROC.ContaGerencial='+#39+TIPO_DC+#39);

              If (CredorDevedorTemp<>-1)
              Then SQL.add(' and PROC.CredorDevedor='+INttostr(CredorDevedorTemp));

              If(CodigoCredorDevedorTemp<>-1)
              Then SQL.add(' and PROC.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));

              sql.add('and PROC.saldopendencia>0');

              sql.add('order by PROC.CredorDevedor,PROC.CodigoCredorDevedor');  }

              open;

              Last;
              InicializaBarradeProgressoRelatorio(RecordCount, 'Gerando Relat�rio.....');
              If (RecordCount=0)
              Then Begin
                        Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                        exit;
              End;
              first;
              FreltxtRDPRINT.ConfiguraImpressao;
              linha:=3;
              FreltxtRDPRINT.rdprint.abrir;
              if ((FreltxtRDPRINT.RDprint.Setup)=False)
              Then Begin
                        FreltxtRDPRINT.rdprint.fechar;
                        exit;
              End;

              If (TIPO_DC='D')
              Then FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'Relat�rio de T�tulos a Pagar DATA '+DATETOSTR(DataLimite),[negrito])
              Else FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'Relat�rio de T�tulos a Receber DATA '+DATETOSTR(DataLimite),[negrito]);
              inc(linha,2);

              If (ContaGerencialTemp<>-1)
              Then Begin
                        FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta Gerencial->'+fieldbyname('tabcontager.nome').AsString,[negrito]);
                        inc(linha,1);

              End;

              if (DataInicial<>'')
              Then FreltxtRDPRINT.rdprint.impf(linha,1,'Intervalo de Datas de Vencimento: '+DataInicial+' a '+datetostr(DataLimite),[negrito])
              Else FreltxtRDPRINT.rdprint.impf(linha,1,'Vencimento Anterior ou Igual a '+datetostr(DataLimite),[negrito]);
              inc(linha,1);

              If (CredorDevedorTemp<>-1)
              Then Begin
                        If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                        Then Begin
                                  Self.CREDORDEVEDOR.TabelaparaObjeto;
                                  strtemp:='';
                                  strtemp:=Self.CredorDevedor.Get_nome;
                                  IF (CodigoCredorDevedorTemp<>-1)
                                  Then strtemp:=strtemp+' = '+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),Inttostr(CodigoCredorDevedorTemp));
                                  FreltxtRDPRINT.RDprint.Impf(linha,1,strtemp,[negrito]);
                                  inc(linha,1);
                        End;
              End;

              FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('EMISSAO',08,' ')+' '+CompletaPalavra('FORNECEDOR/CLIENTE',24,' ')+' '+CompletaPalavra('TELEFONE',12,' ')+' '+CompletaPalavra('CELULAR',13,' ')+' '+
                                                  CompletaPalavra('TITULO',7,' ')+' '+CompletaPalavra('VENCIM.',08,' ')+' '+CompletaPalavra_a_Esquerda('SALDO',15,' '),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.RDprint.Impf(linha,1,'------------------------------------------------------------------------------------------------',[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(Fieldbyname('nome').asstring,80,' '),[negrito]);
              inc(linha,1);
              //primeiro fornecedor
              Tfornecedor:=fieldbyname('codigocredordevedor').asstring;
              TcredorDevedor:=fieldbyname('nome').asstring;
              Somafornecedor:=0;
              SomaGeral:=0;
              TmpQuantCredorDevedor:=1;
              First;
              While not(eof) do
              Begin
                   IncrementaBarraProgressoRelatorio;


                   If (Tfornecedor<>fieldbyname('codigocredordevedor').asstring)
                   Then Begin//totalizando por fornecedor
                             inc(TmpQuantCredorDevedor,1);


                             FreltxtRDPRINT.RDprint.Impf(linha,1,'Total do Devedor: R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);

                             inc(linha,1);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                             FreltxtRDPRINT.RDprint.Impf(linha,1,'------------------------------------------------------------------------------',[negrito]);
                             inc(linha,1);


                             Tfornecedor:=Fieldbyname('codigocredordevedor').asstring;
                             TcredorDevedor:=Fieldbyname('nome').asstring;

                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                             FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(Fieldbyname('nome').asstring,80,' '),[negrito]);
                             inc(linha,1);

                             Somafornecedor:=0;
                   End;

                   FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('datac').AsDateTime),08,' ')+' '+
                                                      CompletaPalavra(Fieldbyname('historico').asstring,24,' ')+' '+
                                                      CompletaPalavra(Fieldbyname('fone').asstring,12,' ')+' '+
                                                      CompletaPalavra(Fieldbyname('celular').asstring,13,' ')+' '+
                                                      CompletaPalavra(fieldbyname('codigo').asstring,7,' ')+' '+
                                                      CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('vencimento').asstring)),08,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('sum').asstring),15,' '));

                   inc(linha,1);
                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);

                   SomaFornecedor:=Somafornecedor+strtofloat(fieldbyname('sum').asstring);
                   SomaGeral:=SomaGeral+strtofloat(fieldbyname('sum').asstring);
                   next;

              end;
              FreltxtRDPRINT.RDprint.Impf(linha,1,'Total do Devedor: R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somafornecedor)),15,' '),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
              FreltxtRDPRINT.RDprint.Imp(linha,1,'------------------------------------------------------------------------------');
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
              FreltxtRDPRINT.RDprint.Impf(linha,1,'Total Geral        : R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaGeral)),15,' '),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
              FreltxtRDPRINT.RDprint.Impf(linha,1,'Quantidade de Devedores: '+inttostr(TmpQuantCredorDevedor),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.RDprint.Fechar;
              FechaBarraProgressoRelatorio;
         End;
     end;

Finally
       Freeandnil(Qlocal);
End;

end;



//Procedimento que apaga as pendencias de um titulo
//que s�o maiores que determinada data
//usada por exemplo para cancelamento de uma matricula
//que por sua vez n�o ter� mais compromissos com as mensalidades
//que ficaram para frente do cancelamemento
function TObjTitulo.Desconto_Pendencias_Matricula(ParametroData: string;
  ParametroTitulo: string): Boolean;
var
ObjPendencia:Tobjpendencia;
resultado:boolean;
begin
     Try
        ObjPendencia:=Tobjpendencia.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o do Objeto de Pend�ncias ',mterror,[mbok],0);
           result:=False;
           exit;
     End;

     Try
        If (ObjPendencia.Desconta_Pendencias_de_Matricula(parametrotitulo,parametrodata)=False)
        Then Begin
                FDataModulo.IBTransaction.RollbackRetaining;
                result:=False;
             End
        Else Begin
                FDataModulo.IBTransaction.CommitRetaining;
                result:=True;
             End;

     Finally
            Objpendencia.free;
     End;
end;

function TObjTitulo.Desconto_Pendencias_Matricula_Gerador(PGerador,
  PCodigoGerador: string; Pdata: string): Boolean;
var
ObjPendencia:Tobjpendencia;
begin
     result:=false;
     Try
        ObjPendencia:=Tobjpendencia.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o do Objeto de Pend�ncias ',mterror,[mbok],0);
           exit;
     End;

     //primeiro localizo os titulos que tem este tipo de gerador
     //depois vou cancelando um a um

     Try
        With Self.objdataset do
        Begin
             close;
             selectsql.clear;
             selectsql.add('Select codigo from tabtitulo where gerador='+Pgerador+' and codigogerador='+Pcodigogerador);
             open;
             first;
             While not(eof) do
             Begin
                  If (ObjPendencia.Desconta_Pendencias_de_Matricula(Fieldbyname('codigo').asstring,Pdata)=False)
                  Then Begin
                            FDataModulo.IBTransaction.RollbackRetaining;
                            exit;
                       End;
                  next;
              End;
              FDataModulo.IBTransaction.commitretaining;
              result:=True;
        End;
     Finally
            Objpendencia.free;
     End;
End;


function TObjTitulo.Desconto_Pendencias_Matricula_Gerador(PGerador,
  PCodigoGerador: string): Boolean;
var
ObjPendencia:Tobjpendencia;
begin
     result:=false;
     Try
        ObjPendencia:=Tobjpendencia.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o do Objeto de Pend�ncias ',mterror,[mbok],0);
           exit;
     End;

     //primeiro localizo os titulos que tem este tipo de gerador
     //depois vou cancelando um a um

     Try
        With Self.objdataset do
        Begin
             close;
             selectsql.clear;
             selectsql.add('Select codigo from tabtitulo where gerador='+Pgerador+' and codigogerador='+Pcodigogerador);
             open;
             first;
             While not(eof) do
             Begin
                  If (ObjPendencia.Desconta_Pendencias_de_Matricula(Fieldbyname('codigo').asstring)=False)
                  Then Begin
                            FDataModulo.IBTransaction.RollbackRetaining;
                            exit;
                       End;
                  next;
              End;
              FDataModulo.IBTransaction.commitretaining;
              result:=True;
        End;
     Finally
            Objpendencia.free;
     End;
end;

function TObjTitulo.Desconto_Pendencias_Matricula(
  ParametroTitulo: string): Boolean;
var
ObjPendencia:Tobjpendencia;
resultado:boolean;
begin
     Try
        ObjPendencia:=Tobjpendencia.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o do Objeto de Pend�ncias ',mterror,[mbok],0);
           result:=False;
           exit;
     End;

     Try
        If (ObjPendencia.Desconta_Pendencias_de_Matricula(parametrotitulo)=False)
        Then Begin
                FDataModulo.IBTransaction.RollbackRetaining;
                result:=False;
             End
        Else Begin
                FDataModulo.IBTransaction.CommitRetaining;
                result:=True;
             End;

     Finally
            Objpendencia.free;
     End;
end;


function TObjTitulo.Get_PesquisaPrazo: string;
begin

     Result:=Self.prazo.get_pesquisa;
end;

function TObjTitulo.Get_TituloPesquisaPrazo: string;
begin
     Result:=Self.prazo.get_titulopesquisa;
end;


procedure TObjTitulo.ImprimeFatura(Parametro:string);
begin
     If (Self.LocalizaCodigo(parametro)=False)
     Then Begin
               Messagedlg('T�tulo n�o Localizado!',mterror,[mbok],0);
               exit;
          End;
     Self.TabelaparaObjeto;

     //preenchendo os dados da Nota Promiss�ria
     //Fnotapromissoria.btnovo.OnClick(nil);
//     With FNotaPromissoria do
  //   Begin


    // End;
     
    Showmessage('M�dulo em Implementa��o!');

     
     //chamando o Form da Nota Promiss�ria


          


end;

function TObjTitulo.Get_NomePortador(Parametro: string): string;
begin

     Result:='';
     If (Parametro='')
     Then exit;
     IF (Self.PORTADOR.LocalizaCodigo(parametro)=False)
     Then exit;
     Self.Portador.TabelaparaObjeto;
     Result:=Self.PORTADOR.Get_Nome;


end;

function TObjTitulo.Get_NomeContaGerencial(Parametro: string): string;
begin

     Result:='';
     If (Parametro='')
     Then exit;
     IF (Self.CONTAGERENCIAL.LocalizaCodigo(parametro)=False)
     Then exit;
     Self.CONTAGERENCIAL.TabelaparaObjeto;
     Result:=Self.CONTAGERENCIAL.Get_Nome;

end;

procedure TObjTitulo.EdtcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaContaGerencial,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=Tedit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;
//mesmo do contagerencial porem sem o ;
procedure TObjTitulo.Edtcontagerencial_SPV_KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaContaGerencial,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;


procedure TObjTitulo.EdtcodigoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;


procedure TObjTitulo.edtcodigocredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     //usado no filtro de impressao para pesquisar o credor/devedor
     //de acordo com o escolhido em um combo no grupo 07
     If (key <>vk_f9)
     Then exit;

     If (FfiltroImp.ComboGrupo07.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaCredorDevedorinstrucaoSql(FfiltroImp.Combo2Grupo07.text),'Pesquisa de '+FfiltroImp.ComboGrupo07.text,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//********************
//mesmo do credor devedor sem o ponto virgula
procedure TObjTitulo.edtcodigocredordevedor_SPV_KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     //usado no filtro de impressao para pesquisar o credor/devedor
     //de acordo com o escolhido em um combo no grupo 07
     If (key <>vk_f9)
     Then exit;

     If (FfiltroImp.ComboGrupo07.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaCredorDevedorinstrucaoSql(FfiltroImp.Combo2Grupo07.text),'Pesquisa de '+FfiltroImp.ComboGrupo07.text,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//********************

procedure TObjTitulo.edtcodigocredordevedorKeyDown_Unico(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     //usado no filtro de impressao para pesquisar o credor/devedor
     //de acordo com o escolhido em um combo no grupo 07
     If (key <>vk_f9)
     Then exit;

     If (FfiltroImp.ComboGrupo07.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaCredorDevedorinstrucaoSql(FfiltroImp.Combo2Grupo07.text),'Pesquisa de '+FfiltroImp.ComboGrupo07.text,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjTitulo.edtExcetocodigocredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     //usado no filtro de impressao para pesquisar o credor/devedor
     //de acordo com o escolhido em um combo no grupo 07
     If (key <>vk_f9)
     Then exit;

     If (FfiltroImp.ComboGrupo13.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaCredorDevedorinstrucaoSql(FfiltroImp.Combo2Grupo13.text),'Pesquisa de '+FfiltroImp.ComboGrupo13.text,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

//*****************
procedure TObjTitulo.edtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisaportador,self.get_titulopesquisaportador,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjTitulo.edtportadorKeyDown_PV(Sender: TObject;
  var Key: Word; Shift: TShiftState);

var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisaportador,self.get_titulopesquisaportador,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjTitulo.Get_QuantidadePendenciaporGerador(PGerador,
  Pcodigogerador: string;OpcoesSaldo:Integer): integer;
var
quanttmp:integer;
begin
     With Self.objdataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select count(tabpendencia.codigo) as CONTA from tabpendencia');
          SelectSQL.add('left join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSQL.add('where tabtitulo.gerador='+PGerador+' and tabtitulo.codigogerador='+Pcodigogerador);

          //USado de acordo com os parametros do RGOpcoesPendencias
          //no Ualunoturma, usado para indicar se quero pendencias
          //com saldo, sem saldo ou todas
          //0 todas
         // 1 quitadas
         //2 nao quitadas
          Case OpcoesSaldo of

                 0:Begin End;
                  1:SelectSQL.add('and Tabpendencia.saldo=0');
                  2:SelectSQL.add('and Tabpendencia.saldo>0');

          End;

          
          open;
          Try
             quanttmp:=strtoint(fieldbyname('conta').asstring);
          Except
                quanttmp:=0;
          End;
          result:=quanttmp;
     End;


end;






procedure TObjTitulo.ImprimePrevisaoFinanceira;
var
DataLimite:Tdate;
SaldoAtual:Currency;
cont,Linha:integer;
STRcontaGerencial_EXCLUIR:TStringList;
STRLPortador:TStringList;
begin
     Try
        STRcontaGerencial_EXCLUIR:=TStringList.create;
        STRcontaGerencial_EXCLUIR.clear;
        STRLPortador:=TStringList.create;
        STRLPortador.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "STRcontaGerencial"',mterror,[mbok],0);
           exit;
     End;

Try     

     limpaedit(Ffiltroimp);

     With FfiltroImp do
     Begin
          DesativaGrupos;


          Grupo01.enabled:=true;
          Grupo02.enabled:=true;
          lbGrupo01.caption:='Data Limite';
          lbGrupo02.caption:='Portador(es)';
          edtGrupo01.EditMask:='!99/99/9999;1;_';
          edtGrupo02.EditMask:='';
          edtgrupo02.OnKeyDown:=Self.edtportadorKeyDown_PV;
          edtgrupo02.Color:=$005CADFE;

          Grupo05.Enabled:=True;
          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown;
          edtgrupo05.Color:=$005CADFE;
          LbGrupo05.caption:='Excluir Contas Gerenciais';

          showmodal;

          If tag=0
          Then exit;

          Try
                DataLimite:=Strtodate(edtgrupo01.text);
          Except
                Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          STRLPortador.clear;

          if (explodestr(edtgrupo02.text,STRLPortador,';','integer')=False)
          then exit;

          if (STRLPortador.count>0)
          Then Begin
                    for cont:=0 to STRLPortador.Count-1 do
                    Begin
                         if (Self.portador.LocalizaCodigo(STRLPortador[cont])=False)
                         then begin
                                   MensagemErro('Portador '+STRLPortador[cont]+' n�o encontrado');
                                   exit;
                         End;
                    End;
          End
          Else Begin
                    MensagemErro('� necess�rio escolher pelo menos um portador');
                    exit;
          End;


          if (explodestr(edtgrupo05.text,STRcontaGerencial_EXCLUIR,';','integer')=False)
          then exit;

          if (STRcontaGerencial_EXCLUIR.count>0)
          Then Begin
                    for cont:=0 to STRcontaGerencial_EXCLUIR.Count-1 do
                    Begin
                         if (Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[cont])=False)
                         then begin
                                   MensagemErro('Conta gerencial '+STRcontaGerencial_EXCLUIR[cont]+' n�o encontrada');
                                   exit;
                         End;
                    End;
          End;
     End;



     With Self.ObjDataset do
     Begin

          close;
          SelectSQL.clear;
          SelectSql.add('Select tabtitulo.codigo as TITULO,tabtitulo.historico as HISTORICOTITULO,tabpendencia.vencimento,tabcontager.tipo,tabpendencia.saldo');
          SelectSql.add('from tabpendencia left join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add('where tabpendencia.vencimento<='+#39+formatDateTime('mm/dd/yyyy',DataLimite)+#39);
          SelectSQL.add('and Tabpendencia.saldo>0');
          SelectSQL.add('and (tabtitulo.portador='+STRLPortador[0]);
          for cont:=1 to STRLPortador.count-1 do
          Begin
              SelectSQL.add('or tabtitulo.portador='+STRLPortador[cont]);
          End;
          SelectSQL.add(')');
          

          if (STRcontaGerencial_EXCLUIR.count>0)
          then begin
                    for cont:=0 to STRcontaGerencial_EXCLUIR.count-1 do
                    Begin
                        SelectSQL.add(' and TabTitulo.ContaGerencial<>'+STRcontaGerencial_EXCLUIR[cont]);
                    End;
          End;
          SelectSQL.add('order by tabpendencia.vencimento,tabpendencia.saldo');
          open;
          last;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Gerando o Relat�rio';
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          linha:=3;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'PREVIS�O FINANCEIRA PARA '+datetostr(DataLimite),[negrito]);
          inc(linha,2);

          if (STRcontaGerencial_EXCLUIR.Count>0)
          then begin
                   Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[0]);
                   Self.CONTAGERENCIAL.TabelaparaObjeto;

                   FreltxtRDPRINT.RDprint.ImpF(linha,1,'Contas Gerenciais n�o Inclusas: ',[negrito]);
                   FreltxtRDPRINT.RDprint.Imp (linha,33,CompletaPalavra(Self.CONTAGERENCIAL.get_codigo+'-'+Self.CONTAGERENCIAL.Get_Nome,97,' '));
                   inc(linha,1);
                   for cont:=1 to STRcontaGerencial_EXCLUIR.count-1 do
                   begin
                        Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[cont]);
                        Self.CONTAGERENCIAL.TabelaparaObjeto;
                        FreltxtRDPRINT.RDprint.Imp (linha,33,CompletaPalavra(Self.CONTAGERENCIAL.get_codigo+'-'+Self.CONTAGERENCIAL.Get_Nome,97,' '));
                        inc(linha,1);
                   End;
                   inc(linha,1);
          End;

          SaldoAtual:=0;
               
          if (STRLPortador.Count>0)
          then begin
                   Self.PORTADOR.LocalizaCodigo(STRLPORTADOR[0]);
                   Self.portador.TabelaparaObjeto;

                   FreltxtRDPRINT.RDprint.ImpF(linha,1,'Portador: ',[negrito]);
                   FreltxtRDPRINT.RDprint.Imp (linha,10,CompletaPalavra(Self.portador.get_codigo+'-'+Self.portador.Get_Nome,97,' '));
                   inc(linha,1);

                   FreltxtRDPRINT.RDprint.Imp(linha,10,'Saldo '+formata_valor(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo)));
                   inc(linha,1);
                   SaldoAtual:=SaldoAtual+strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo));

                   for cont:=1 to STRLPORTADOR.count-1 do
                   begin
                        Self.portador.LocalizaCodigo(STRLPORTADOR[cont]);
                        Self.portador.TabelaparaObjeto;
                        FreltxtRDPRINT.RDprint.Imp (linha,10,CompletaPalavra(Self.portador.get_codigo+'-'+Self.portador.Get_Nome,97,' '));
                        inc(linha,1);
                        FreltxtRDPRINT.RDprint.Imp(linha,10,'Saldo '+formata_valor(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo)));
                        inc(linha,1);
                        SaldoAtual:=SaldoAtual+strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo));
                        if (Cont=(STRLPortador.count-1))
                        Then Begin
                                  inc(linha,1);
                                  FreltxtRDPRINT.RDprint.Impf(linha,1,'SOMA DOS SALDOS ='+formata_valor(SaldoAtual),[negrito]);
                                  inc(linha,1);
                        End;
                   End;
                   inc(linha,1);
          End;




          FreltxtRDPRINT.RDprint.Impf(linha,1,'Portador    = '+Self.portador.Get_Nome,[negrito]);
          inc(linha,1);





          FreltxtRDPRINT.RDprint.ImpF(linha,1,completapalavra('T�TULO',55,' ')+' '+
                                             completapalavra('VENCIMENTO',10,' ')+' '+
                                             CompletaPalavra_a_Esquerda('SALDO PEND.',12,' ')+' '+
                                             CompletaPalavra_a_Esquerda('SALDO PORT.',12,' '),[NEGRITO]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('_',90,'_'));
          inc(linha,1);

          while not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               Application.ProcessMessages;

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               if (fieldbyname('tipo').asstring='C')
               Then Begin
                        SaldoAtual:=saldoatual+fieldbyname('saldo').asfloat;
                        FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra(fieldbyname('TITULO').asstring+' - '+fieldbyname('HISTORICOTITULO').asstring,55,' ')+' '+
                                                           completapalavra(fieldbyname('VENCIMENTO').asstring,10,' ')+' '+
                                                           CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALDO').asstring)+'+',12,' ')+' '+
                                                           CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAtual)),12,' '));
               End
               Else Begin
                        SaldoAtual:=saldoatual-fieldbyname('saldo').asfloat;
                        FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra(fieldbyname('TITULO').asstring+' - '+fieldbyname('HISTORICOTITULO').asstring,55,' ')+' '+
                                                           completapalavra(fieldbyname('VENCIMENTO').asstring,10,' ')+' '+
                                                           CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALDO').asstring)+'-',12,' ')+' '+
                                                           CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAtual)),12,' '));
               End;
               inc(linha,1);
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.rdprint.ImpF(linha,1,'PREVIS�O DE SALDO R$ '+formata_valor(floattostr(SaldoAtual)),[negrito]);
          inc(linha,1);

          //******************CHEQUES DOS PORTADORES ESCOLHIDOS*****************
          
          close;
          SelectSQL.clear;
          SelectSQL.add(' select cast(tabchequesportador.numcheque as integer) as NUMCHEQUENUMERICO,');
          SelectSQL.add(' tablancamento.codigo AS CODIGOLANCAMENTO,tablancamento.historico AS HISTORICOLANCAMENTO,');
          SelectSQL.add(' tablancamento.data as EMISSAO,');
          SelectSQL.add(' tabchequesportador.Vencimento,tabchequesportador.Valor as VALORCHEQUE');
          SelectSQL.add(' from tablancamento');
          SelectSQL.add(' join TabTransferenciaPortador on TabTransferenciaPortador.codigolancamento=Tablancamento.codigo');
          SelectSQL.add(' join TabValoresTransferenciaPortador');
          SelectSQL.add(' on Tabvalorestransferenciaportador.transferenciaportador=Tabtransferenciaportador.codigo');
          SelectSQL.add(' join tabchequesportador on TabValoresTransferenciaPortador.Valores=TabChequesportador.codigo');
          SelectSQL.add('where (tabchequesportador.portador='+STRLPortador[0]);
          for cont:=1 to STRLPortador.count-1 do
          Begin
              SelectSQL.add('or tabchequesportador.portador='+STRLPortador[cont]);
          End;
          SelectSQL.add(')');
          SelectSQL.add(' and tabchequesportador.chequedoportador=''S'' ');
          SelectSQL.add(' and tabchequesportador.Vencimento<='+#39+formatDateTime('mm/dd/yyyy',DataLimite)+#39);
          SelectSQL.add(' order by tabchequesportador.portador,tabchequesportador.vencimento');
          open;
          first;

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.ImpC(linha,45,'CONCILIA��O DE CHEQUES',[negrito]);
          inc(linha,1);

          While not(eof) do
          Begin
               SaldoAtual:=SALDOatual-fieldbyname('valorcheque').asfloat;
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigolancamento').asstring+'-'+fieldbyname('historicolancamento').asstring,50,' ')+
                          ' '+CompletaPalavra(fieldbyname('NUMCHEQUENUMERICO').asstring,09,' ')+
                          ' '+CompletaPalavra(fieldbyname('VENCIMENTO').asstring,10,' ')+' '+
                          CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORcheque').asstring),10,' ')+' '+
                          CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAtual)),10,' '));
               inc(linha,1);
               next;
          End;

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,COMPLETAPALAVRA('_',95,'_'),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SALDO CONT�BIL: R$ '+formata_valor(floattostr(SALDOATUAL)),[negrito]);
          inc(linha,1);

          //*******************************************************************
          FreltxtRDPRINT.RDprint.Fechar;
     End;

finally
       FMostraBarraProgresso.close;
       Freeandnil(STRcontaGerencial_EXCLUIR);
       Freeandnil(STRLPortador);
End;

end;


Procedure TObjtitulo.GeraFormularioPrevisaoFinanceira(PdataInicial,PdataFinal:string;pportador,pexcluircontagerencial,pexcluirsubcontagerencial:String;PCabecalho,Prodape:Tstrings;PStrgGrid:TStringGrid;var PsaldoInicial:currency;pexcluititulo:string;PtipoColunas:TStrings);
var
DataLimite:tdate;
SaldoAtual:Currency;
cont:integer;
STRcontaGerencial_EXCLUIR:TStringList;
STRSubcontaGerencial_EXCLUIR:TStringList;
STRTitulo_EXCLUIR:TStringList;
STRLPortador:TStringList;
begin
     PCabecalho.Clear;
     Try
        STRcontaGerencial_EXCLUIR:=TStringList.create;
        STRcontaGerencial_EXCLUIR.clear;

        STRSUBcontaGerencial_EXCLUIR:=TStringList.create;
        STRSUBcontaGerencial_EXCLUIR.clear;

        STRtitulo_EXCLUIR:=TStringList.create;
        STRtitulo_EXCLUIR.clear;

        STRLPortador:=TStringList.create;
        STRLPortador.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "STRcontaGerencial"',mterror,[mbok],0);
           exit;
     End;

Try
     if (comebarra(trim(PdataInicial))<>'')
     then Begin
               Try
                  strtodate(Pdatainicial);
               Except
                     Mensagemerro('Data Inicial inv�lida');
                     exit;
               End;
     End
     else Pdatainicial:='';


     Try
           DataLimite:=Strtodate(Pdatafinal);
     Except
           Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
           exit;
     End;

     STRLPortador.clear;

     if (explodestr(pportador,STRLPortador,';','integer')=False)
     then exit;

     if (STRLPortador.count>0)
     Then Begin
               for cont:=0 to STRLPortador.Count-1 do
               Begin
                    if (Self.portador.LocalizaCodigo(STRLPortador[cont])=False)
                    then begin
                              MensagemErro('Portador '+STRLPortador[cont]+' n�o encontrado');
                              exit;
                    End;
               End;
     End
     Else Begin
               MensagemErro('� necess�rio escolher pelo menos um portador');
               exit;
     End;


     if (explodestr(pexcluircontagerencial,STRcontaGerencial_EXCLUIR,';','integer')=False)
     then exit;

     if (STRcontaGerencial_EXCLUIR.count>0)
     Then Begin
               for cont:=0 to STRcontaGerencial_EXCLUIR.Count-1 do
               Begin
                    if (Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[cont])=False)
                    then begin
                              MensagemErro('Conta gerencial '+STRcontaGerencial_EXCLUIR[cont]+' n�o encontrada');
                              exit;
                    End;
               End;
     End;


     if (explodestr(pexcluirsubcontagerencial,STRSubcontaGerencial_EXCLUIR,';','integer')=False)
     then exit;

     if (STRSubcontaGerencial_EXCLUIR.count>0)
     Then Begin
               for cont:=0 to STRSubcontaGerencial_EXCLUIR.Count-1 do
               Begin
                    if (Self.SubCONTAGERENCIAL.LocalizaCodigo(STRSubcontaGerencial_EXCLUIR[cont])=False)
                    then begin
                              MensagemErro('Sub-Conta gerencial '+STRSubcontaGerencial_EXCLUIR[cont]+' n�o encontrada');
                              exit;
                    End;
               End;
     End;

     if (explodestr(pexcluititulo,STRTitulo_EXCLUIR,';','integer')=False)
     then exit;


     With Self.Objquery do
     Begin

          close;
          sql.clear;
          sql.add('Select tabtitulo.codigo as TITULO,tabtitulo.historico as HISTORICOTITULO,tabpendencia.vencimento,tabcontager.tipo,tabpendencia.saldo');
          sql.add('from tabpendencia left join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          sql.add('where tabpendencia.vencimento<='+#39+formatDateTime('mm/dd/yyyy',DataLimite)+#39);
          if (PdataInicial<>'')
          Then sql.add('and tabpendencia.vencimento>='+#39+formatDateTime('mm/dd/yyyy',strtodate(Pdatainicial))+#39);
          
          sql.add('and Tabpendencia.saldo>0');
          sql.add('and (tabtitulo.portador='+STRLPortador[0]);
          for cont:=1 to STRLPortador.count-1 do
          Begin
              sql.add('or tabtitulo.portador='+STRLPortador[cont]);
          End;
          sql.add(')');


          if (STRcontaGerencial_EXCLUIR.count>0)
          then begin
                    for cont:=0 to STRcontaGerencial_EXCLUIR.count-1 do
                    Begin
                        sql.add(' and TabTitulo.ContaGerencial<>'+STRcontaGerencial_EXCLUIR[cont]);
                    End;
          End;

          if (STRSubcontaGerencial_EXCLUIR.count>0)
          then begin
                    for cont:=0 to STRSubcontaGerencial_EXCLUIR.count-1 do
                    Begin
                        sql.add(' and TabTitulo.SubContaGerencial<>'+STRSubcontaGerencial_EXCLUIR[cont]);
                    End;
          End;

          if (STRTitulo_EXCLUIR.count>0)
          then begin

                    for cont:=0 to STRTitulo_EXCLUIR.count-1 do
                    Begin
                         sql.add(' and TabTitulo.codigo<>'+STRTitulo_EXCLUIR[cont]);
                    End;
          End;



          sql.add('order by tabpendencia.vencimento,tabpendencia.saldo');
          open;
          last;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Gerando o Relat�rio';
          first;

          if (STRcontaGerencial_EXCLUIR.Count>0)
          then begin
                   Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[0]);
                   Self.CONTAGERENCIAL.TabelaparaObjeto;

                   PCabecalho.add('Contas Gerenciais n�o Inclusas: ');
                   PCabecalho.add(CompletaPalavra(Self.CONTAGERENCIAL.get_codigo+'-'+Self.CONTAGERENCIAL.Get_Nome,97,' '));
                   for cont:=1 to STRcontaGerencial_EXCLUIR.count-1 do
                   begin
                        Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[cont]);
                        Self.CONTAGERENCIAL.TabelaparaObjeto;
                        PCabecalho.add(CompletaPalavra(Self.CONTAGERENCIAL.get_codigo+'-'+Self.CONTAGERENCIAL.Get_Nome,97,' '));
                   End;
          End;

          
          if (STRSubcontaGerencial_EXCLUIR.Count>0)
          then begin
                   Self.SubCONTAGERENCIAL.LocalizaCodigo(STRSubcontaGerencial_EXCLUIR[0]);
                   Self.SubCONTAGERENCIAL.TabelaparaObjeto;

                   PCabecalho.add('Sub-Contas Gerenciais n�o Inclusas: ');
                   PCabecalho.add(CompletaPalavra(Self.SubCONTAGERENCIAL.get_codigo+'-'+Self.SubCONTAGERENCIAL.Get_Nome,97,' '));
                   for cont:=1 to STRSubcontaGerencial_EXCLUIR.count-1 do
                   begin
                        Self.SubCONTAGERENCIAL.LocalizaCodigo(STRSubcontaGerencial_EXCLUIR[cont]);
                        Self.SubCONTAGERENCIAL.TabelaparaObjeto;
                        PCabecalho.add(CompletaPalavra(Self.SubCONTAGERENCIAL.get_codigo+'-'+Self.SubCONTAGERENCIAL.Get_Nome,97,' '));
                   End;
          End;

          if (STRTitulo_EXCLUIR.Count>0)
          then begin
                   Self.LocalizaCodigo(STRTitulo_EXCLUIR[0]);
                   Self.TabelaparaObjeto;

                   PCabecalho.add('T�tulos n�o Inclusos: ');
                   PCabecalho.add(CompletaPalavra(Self.get_codigo+'-'+Self.Get_HISTORICO,97,' '));
                   for cont:=1 to STRTitulo_EXCLUIR.count-1 do
                   begin
                        Self.LocalizaCodigo(STRTitulo_EXCLUIR[cont]);
                        Self.TabelaparaObjeto;
                        PCabecalho.add(CompletaPalavra(Self.get_codigo+'-'+Self.Get_HISTORICO,97,' '));
                   End;
          End;
          SaldoAtual:=0;
               
          if (STRLPortador.Count>0)
          then begin
                   Self.PORTADOR.LocalizaCodigo(STRLPORTADOR[0]);
                   Self.portador.TabelaparaObjeto;

                   for cont:=0 to STRLPORTADOR.count-1 do
                   begin
                        Self.portador.LocalizaCodigo(STRLPORTADOR[cont]);
                        Self.portador.TabelaparaObjeto;
                        PCabecalho.add('Portador       : '+CompletaPalavra(Self.portador.get_codigo+'-'+Self.portador.Get_Nome,50,' '));
                        PCabecalho.add('Saldo          : '+formata_valor(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo)));
                        SaldoAtual:=SaldoAtual+strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo));
                        if (Cont=(STRLPortador.count-1))
                        and (STRLPortador.count>1)
                        Then PCabecalho.add('Soma dos Saldos: '+formata_valor(SaldoAtual));
                   End;
          End;
          PsaldoInicial:=SaldoAtual;

          //Aqui ja vai para o Grid
          PStrgGrid.Colcount:=5;
          PStrgGrid.rowcount:=1;
          PStrgGrid.cols[0].clear;
          PStrgGrid.cols[1].clear;
          PStrgGrid.cols[2].clear;
          PStrgGrid.cols[3].clear;
          PStrgGrid.cols[4].clear;


          ptipocolunas.clear;
          ptipocolunas.add('string');
          PStrgGrid.Cells[0,0]:='T�TULO';
          ptipocolunas.add('string');
          PStrgGrid.Cells[1,0]:='DOC';
          ptipocolunas.add('date');
          PStrgGrid.Cells[2,0]:='VENCIMENTO';
          ptipocolunas.add('decimal');
          PStrgGrid.Cells[3,0]:='VALOR';
          ptipocolunas.add('decimal');
          PStrgGrid.Cells[4,0]:='SALDO';

          while not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               Application.ProcessMessages;
               //**************************************************************
               PStrgGrid.rowcount:=PStrgGrid.rowcount+1;
               if (fieldbyname('tipo').asstring='C')
               Then Begin
                        SaldoAtual:=saldoatual+fieldbyname('saldo').asfloat;
                        PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:=completapalavra(fieldbyname('TITULO').asstring+' - '+fieldbyname('HISTORICOTITULO').asstring,55,' ');
                        PStrgGrid.Cells[1,PStrgGrid.rowcount-1]:='';
                        PStrgGrid.Cells[2,PStrgGrid.rowcount-1]:=completapalavra(fieldbyname('VENCIMENTO').asstring,10,' ');
                        PStrgGrid.Cells[3,PStrgGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALDO').asstring),12,' ');
                        PStrgGrid.Cells[4,PStrgGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAtual)),12,' ');
               End
               Else Begin
                        SaldoAtual:=saldoatual-fieldbyname('saldo').asfloat;
                        PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:=completapalavra(fieldbyname('TITULO').asstring+' - '+fieldbyname('HISTORICOTITULO').asstring,55,' ');
                        PStrgGrid.Cells[1,PStrgGrid.rowcount-1]:='';
                        PStrgGrid.Cells[2,PStrgGrid.rowcount-1]:=completapalavra(fieldbyname('VENCIMENTO').asstring,10,' ');
                        PStrgGrid.Cells[3,PStrgGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor('-'+fieldbyname('SALDO').asstring),12,' ');
                        PStrgGrid.Cells[4,PStrgGrid.rowcount-1]:=CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAtual)),12,' ');
               End;
               next;
          End;
          PStrgGrid.FixedCols:=0;
          PStrgGrid.FixedRows:=1;

          
          //******************CHEQUES DOS PORTADORES ESCOLHIDOS*****************
          
          close;
          sql.clear;
          sql.add(' select cast(tabchequesportador.numcheque as integer) as NUMCHEQUENUMERICO,');
          sql.add(' tablancamento.codigo AS CODIGOLANCAMENTO,tablancamento.historico AS HISTORICOLANCAMENTO,');
          sql.add(' tablancamento.data as EMISSAO,');
          sql.add(' tabchequesportador.Vencimento,tabchequesportador.Valor as VALORCHEQUE');
          sql.add(' from tablancamento');
          sql.add(' join TabTransferenciaPortador on TabTransferenciaPortador.codigolancamento=Tablancamento.codigo');
          sql.add(' join TabValoresTransferenciaPortador');
          sql.add(' on Tabvalorestransferenciaportador.transferenciaportador=Tabtransferenciaportador.codigo');
          sql.add(' join tabchequesportador on TabValoresTransferenciaPortador.Valores=TabChequesportador.codigo');
          sql.add('where (tabchequesportador.portador='+STRLPortador[0]);
          for cont:=1 to STRLPortador.count-1 do
          Begin
              sql.add('or tabchequesportador.portador='+STRLPortador[cont]);
          End;
          sql.add(')');
          sql.add(' and tabchequesportador.chequedoportador=''S'' ');
          sql.add(' and tabchequesportador.Vencimento<='+#39+formatDateTime('mm/dd/yyyy',DataLimite)+#39);
          sql.add(' order by tabchequesportador.portador,tabchequesportador.vencimento');
          open;
          first;

          While not(eof) do
          Begin
               SaldoAtual:=SALDOatual-fieldbyname('valorcheque').asfloat;
               PStrgGrid.RowCount:=PStrgGrid.RowCount+1;
               PStrgGrid.Cells[0,PStrgGrid.RowCount-1]:=CompletaPalavra(fieldbyname('codigolancamento').asstring+'- CH -'+fieldbyname('historicolancamento').asstring,55,' ');
               PStrgGrid.Cells[1,PStrgGrid.RowCount-1]:=CompletaPalavra(fieldbyname('NUMCHEQUENUMERICO').asstring,10,' ');
               PStrgGrid.Cells[2,PStrgGrid.RowCount-1]:=CompletaPalavra(fieldbyname('VENCIMENTO').asstring,10,' ');
               PStrgGrid.Cells[3,PStrgGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor('-'+fieldbyname('VALORcheque').asstring),12,' ');
               PStrgGrid.Cells[4,PStrgGrid.RowCount-1]:=CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAtual)),12,' ');
               next;
          End;
     End;

finally
       FMostraBarraProgresso.close;
       Freeandnil(STRcontaGerencial_EXCLUIR);
       Freeandnil(STRSubcontaGerencial_EXCLUIR);
       Freeandnil(STRLPortador);
End;

end;

Procedure TObjtitulo.GeraFormularioPrevisaoFinanceira_mensal(PdataInicial,PdataFinal:string;pportador,pexcluircontagerencial,pexcluirsubcontagerencial:String;PCabecalho,Prodape:Tstrings;PStrgGrid:TStringGrid;var PsaldoInicial:currency;PincluiPrevisao:boolean);
var
DataLimite:tdate;
SaldoAtual:Currency;
cont:integer;
STRcontaGerencial_EXCLUIR:TStringList;
STRSubcontaGerencial_EXCLUIR,STRdatas,StrSaldo:TStringList;
STRLPortador:TStringList;
meses:integer;
pdataatual:tdate;
PqueryLocal,PqueryLocal2:TIbquery;

Contagerencialatual:string;
linhacontagerencialatual:integer;
temp,pvalorsomatoria,somacontagerencialatual:currency;

positivo:boolean;

begin
     PCabecalho.Clear;
     Try
        STRcontaGerencial_EXCLUIR:=TStringList.create;
        STRcontaGerencial_EXCLUIR.clear;

        STRSUBcontaGerencial_EXCLUIR:=TStringList.create;
        STRSUBcontaGerencial_EXCLUIR.clear;

        StrDatas:=TStringList.create;
        StrDatas.clear;

        STRLPortador:=TStringList.create;
        STRLPortador.clear;

        StrSaldo:=TstringList.create;
        StrSaldo.clear;

        PqueryLocal:=TIbquery.create(nil);
        PqueryLocal.Database:=fdatamodulo.IBDatabase;

        PqueryLocal2:=TIbquery.create(nil);
        PqueryLocal2.Database:=fdatamodulo.IBDatabase;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "STRcontaGerencial"',mterror,[mbok],0);
           exit;
     End;

Try

     Try
        strtodate(Pdatainicial);
     Except
           Mensagemerro('Data Inicial inv�lida');
           exit;
     End;

     Try
           DataLimite:=Strtodate(Pdatafinal);
     Except
           Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
           exit;
     End;

     Meses:=MonthsBetween(strtodate(pdatainicial),datalimite);

     cont:=1;
     pdataatual:=strtodate(pdatainicial);
     While (formatdatetime('mm/yyyy',pdataatual)<>formatdatetime('mm/yyyy',datalimite)) do
     Begin
          pdataatual:=IncMonth(pdataatual,1);
          cont:=cont+1;
     End;
     meses:=cont;


     STRdatas.Clear;
     StrSaldo.clear;

     for cont:=0 to meses-1 do
     Begin
          Pdataatual:=IncMonth(strtodate(pdatainicial),cont);
          STRdatas.add('01/'+formatdatetime('mm/yyyy',pdataatual));
          StrSaldo.add('0');
     End;


     STRLPortador.clear;

     if (explodestr(pportador,STRLPortador,';','integer')=False)
     then exit;

     if (STRLPortador.count>0)
     Then Begin
               for cont:=0 to STRLPortador.Count-1 do
               Begin
                    if (Self.portador.LocalizaCodigo(STRLPortador[cont])=False)
                    then begin
                              MensagemErro('Portador '+STRLPortador[cont]+' n�o encontrado');
                              exit;
                    End;
               End;
     End
     Else Begin
               MensagemErro('� necess�rio escolher pelo menos um portador');
               exit;
     End;

     if (explodestr(pexcluircontagerencial,STRcontaGerencial_EXCLUIR,';','integer')=False)
     then exit;

     if (STRcontaGerencial_EXCLUIR.count>0)
     Then Begin
               for cont:=0 to STRcontaGerencial_EXCLUIR.Count-1 do
               Begin
                    if (Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[cont])=False)
                    then begin
                              MensagemErro('Conta gerencial '+STRcontaGerencial_EXCLUIR[cont]+' n�o encontrada');
                              exit;
                    End;
               End;
     End;

     if (explodestr(pexcluirsubcontagerencial,STRSubcontaGerencial_EXCLUIR,';','integer')=False)
     then exit;

     if (STRSubcontaGerencial_EXCLUIR.count>0)
     Then Begin
               for cont:=0 to STRSubcontaGerencial_EXCLUIR.Count-1 do
               Begin
                    if (Self.SubCONTAGERENCIAL.LocalizaCodigo(STRSubcontaGerencial_EXCLUIR[cont])=False)
                    then begin
                              MensagemErro('Sub-Conta gerencial '+STRSubcontaGerencial_EXCLUIR[cont]+' n�o encontrada');
                              exit;
                    End;
               End;
     End;




     With Self.ObjDataset do
     Begin

          close;
          SelectSql.clear;
          SelectSql.add('Select tipo,nomecontagerencial,contagerencial from proccontasprevisao('+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatainicial))+#39+
                        ','+#39+formatdatetime('mm/dd/yyyy',datalimite)+#39);
          if (PincluiPrevisao=true)
          Then  SelectSQL.add(',''S'') ')
          else   SelectSQL.add(',''N'') ');

          SelectSQL.add('where (portador='+STRLPortador[0]);

          for cont:=1 to STRLPortador.count-1 do
          Begin
              SelectSQL.add('or portador='+STRLPortador[cont]);
          End;
          SelectSQL.add(')');

          if (STRcontaGerencial_EXCLUIR.count>0)
          then begin
                    for cont:=0 to STRcontaGerencial_EXCLUIR.count-1 do
                    Begin
                        SelectSQL.add(' and ContaGerencial<>'+STRcontaGerencial_EXCLUIR[cont]);
                    End;
          End;

          if (STRSubcontaGerencial_EXCLUIR.count>0)
          then begin
                    for cont:=0 to STRSubcontaGerencial_EXCLUIR.count-1 do
                    Begin
                        SelectSQL.add(' and SubContaGerencial<>'+STRSubcontaGerencial_EXCLUIR[cont]);
                    End;
          End;

          SelectSql.add('group by tipo,nomecontagerencial,contagerencial order by tipo');
          open; 
          last;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Gerando o Relat�rio';
          first;

          
          if (STRcontaGerencial_EXCLUIR.Count>0)
          then begin
                   Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[0]);
                   Self.CONTAGERENCIAL.TabelaparaObjeto;

                   PCabecalho.add('Contas Gerenciais n�o Inclusas: ');
                   PCabecalho.add(CompletaPalavra(Self.CONTAGERENCIAL.get_codigo+'-'+Self.CONTAGERENCIAL.Get_Nome,97,' '));
                   for cont:=1 to STRcontaGerencial_EXCLUIR.count-1 do
                   begin
                        Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[cont]);
                        Self.CONTAGERENCIAL.TabelaparaObjeto;
                        PCabecalho.add(CompletaPalavra(Self.CONTAGERENCIAL.get_codigo+'-'+Self.CONTAGERENCIAL.Get_Nome,97,' '));
                   End;
          End;

          
          if (STRSubcontaGerencial_EXCLUIR.Count>0)
          then begin
                   Self.SubCONTAGERENCIAL.LocalizaCodigo(STRSubcontaGerencial_EXCLUIR[0]);
                   Self.SubCONTAGERENCIAL.TabelaparaObjeto;

                   PCabecalho.add('Sub-Contas Gerenciais n�o Inclusas: ');
                   PCabecalho.add(CompletaPalavra(Self.SubCONTAGERENCIAL.get_codigo+'-'+Self.SubCONTAGERENCIAL.Get_Nome,97,' '));
                   for cont:=1 to STRSubcontaGerencial_EXCLUIR.count-1 do
                   begin
                        Self.SubCONTAGERENCIAL.LocalizaCodigo(STRSubcontaGerencial_EXCLUIR[cont]);
                        Self.SubCONTAGERENCIAL.TabelaparaObjeto;
                        PCabecalho.add(CompletaPalavra(Self.SubCONTAGERENCIAL.get_codigo+'-'+Self.SubCONTAGERENCIAL.Get_Nome,97,' '));
                   End;
          End;


          SaldoAtual:=0;
               
          if (STRLPortador.Count>0)
          then begin
                   Self.PORTADOR.LocalizaCodigo(STRLPORTADOR[0]);
                   Self.portador.TabelaparaObjeto;

                   for cont:=0 to STRLPORTADOR.count-1 do
                   begin
                        Self.portador.LocalizaCodigo(STRLPORTADOR[cont]);
                        Self.portador.TabelaparaObjeto;
                        PCabecalho.add('Portador       : '+CompletaPalavra(Self.portador.get_codigo+'-'+Self.portador.Get_Nome,50,' '));
                        PCabecalho.add('Saldo          : '+formata_valor(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo)));
                        SaldoAtual:=SaldoAtual+strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo));
                        if (Cont=(STRLPortador.count-1))
                        and (STRLPortador.count>1)
                        Then PCabecalho.add('Soma dos Saldos: '+formata_valor(SaldoAtual));
                   End;
          End;
          PsaldoInicial:=SaldoAtual;

          //Aqui ja vai para o Grid
          //04 a Mais = Titulo + Codigo CG +Codigo SUB + Indicacao (Titulo,Previsao,Manual)
          PStrgGrid.Colcount:=STRdatas.Count+4;

          for cont:=0 to PStrgGrid.Colcount-1 do
          Begin
               PStrgGrid.cols[cont].clear;
          End;

          PStrgGrid.Cells[0,0]:='Contas e Sub-Contas';
          //escrevendo as datas no cabecalho das colunas do StrgGrid
          for cont:=1 to STRdatas.count do
          Begin
               PStrgGrid.cols[cont].clear;
               PStrgGrid.Cells[cont,0]:=formatdatetime('mm/yyyy',strtodate(STRdatas[cont-1]));
          End;
          PStrgGrid.rowcount:=1;

          linhacontagerencialatual:=1;
          Contagerencialatual:=fieldbyname('contagerencial').AsString;
          somacontagerencialatual:=0;

          PStrgGrid.rowcount:=PStrgGrid.rowcount+1;
          PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:=fieldbyname('nomecontagerencial').asstring;//primeira coluna � o nome da conta gerencial
          PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='T';//ultima � o TIPO
          PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:='';//Penultima Sub-Conta
          PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:=fieldbyname('contagerencial').asstring;//AntePenultimo � o Codigo da Conta


          if (fieldbyname('tipo').asstring='D')
          Then positivo:=False
          Else positivo:=true;


          //Esse while percorre todas as contas gerencias que serao utilizadas em contas no periodo escolhido
          while not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               Application.ProcessMessages;
               //**************************************************************
               //Mudou a Conta Gerencial
               if (Contagerencialatual<>fieldbyname('contagerencial').asstring)
               Then Begin
                         //antes de trocar a conta gerencial imprimo a previsao
                         //*****************************************************
                         if (PincluiPrevisao=true)
                         Then Begin

                                   //Filtrando todas as previsoes que estao ligadas a conta gerencial
                                   //atual e dentro do periodo data inicial e limite
                                   pquerylocal2.close;
                                   pquerylocal2.sql.clear;
                                   pquerylocal2.sql.add('Select TPF.codigo,TPF.historico,TPF.somatoriaprevisao,TPF.subcontagerencial');
                                   pquerylocal2.sql.add('from TabPrevisaoFinanceira TPF');
                                   pquerylocal2.sql.add('join tabsubcontagerencial TSBC on TPF.subcontagerencial=TSBC.codigo');
                                   pquerylocal2.sql.add('where TPF.contagerencial='+contagerencialatual);
                                   pquerylocal2.sql.add('and((Tpf.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatainicial))+#39);
                                   pquerylocal2.sql.add('and Tpf.data<='+#39+formatdatetime('mm/dd/yyyy',datalimite)+#39+')');
                                   pquerylocal2.sql.add('or (Tpf.data<='+#39+formatdatetime('mm/dd/yyyy',datalimite)+#39+' and considerarmesessubsequentes=''S''))');
                                   pquerylocal2.sql.add('order by TPF.subcontagerencial');
                                   pquerylocal2.open;

                                   While not(pquerylocal2.eof) do
                                   Begin
                                        {Para cada previsao faco um select na data do for}
                                        PStrgGrid.rowcount:=PStrgGrid.rowcount+1;
                                        PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:='    Previs�o '+pquerylocal2.fieldbyname('historico').asstring;
                                        PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='P';
                                        PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:=pquerylocal2.fieldbyname('subcontagerencial').asstring;//Penultima Sub-Conta ou Previsao
                                        PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:='';//AntePenultimo � o Codigo da Conta

                                        pvalorsomatoria:=0;


                                        for cont:=0 to STRdatas.Count-1 do
                                        begin
                                              {*******� selecionado baseado nos seguintes criterios*******
                                                1 - A data � o mes atual e nao considera meses subsequentes
                                                2 - A  data esta dentro desse ano ate o mes atual e
                                                          - A data limite vence desse mes ao fim do ano
                                                          - A data limite vence nos proximos anos
                                                3 - A data � em anos anterios
                                                          - A data limite vence desse mes ao fim do ano
                                                          - A data limite vence nos proximos anos}

                                              pquerylocal.close;
                                              pquerylocal.sql.clear;
                                              pquerylocal.sql.add('Select codigo,historico,valor as SOMA');
                                              pquerylocal.sql.add('from tabprevisaofinanceira');
                                              pquerylocal.sql.add('where codigo='+PqueryLocal2.fieldbyname('codigo').asstring);
                                              pquerylocal.sql.add('and (');
                                              pquerylocal.sql.add('(extract(month from data)='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from data)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''N'')');
                                              pquerylocal.sql.add('or (extract(month from data)<='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from data)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(month from datalimite)>='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from datalimite)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                                              pquerylocal.sql.add('or (extract(month from data)<='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from data)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(year from datalimite)>'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                                              pquerylocal.sql.add('or (extract(year from data)<'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(month from datalimite)>='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from datalimite)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                                              pquerylocal.sql.add('or (extract(year from data)<'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(year from datalimite)>'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                                              pquerylocal.sql.add(')');
                                              PqueryLocal.open;

                                              if (positivo=true)
                                              Then temp:=Pquerylocal.fieldbyname('soma').asfloat
                                              Else temp:=-1*Pquerylocal.fieldbyname('soma').asfloat;

                                              if (pquerylocal2.FieldByName('somatoriaprevisao').asstring='S')
                                              Then pvalorsomatoria:=pvalorsomatoria+temp
                                              Else pvalorsomatoria:=temp;

                                              PStrgGrid.Cells[cont+1,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(pvalorsomatoria),12,' ');
                                              StrSaldo[cont]:=Floattostr(strtofloat(StrSaldo[cont])+pvalorsomatoria);
                                        End;
                                        //pegando a proxima previsao
                                        pquerylocal2.next;

                                   End;
                         End;
                         //*****************************************************

                         //Guardando a soma de cada mes  da conta gerencial anterior

                         for cont:=0 to STRSALDo.Count-1 do
                         begin
                              PStrgGrid.Cells[1+cont,linhacontagerencialatual]:=formata_valor(STRSALDO[CONT]);
                              STRSALDO[CONT]:='0';
                         End;

                         PStrgGrid.rowcount:=PStrgGrid.rowcount+2;

                         PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:=fieldbyname('nomecontagerencial').asstring;//primeira coluna nome da conta
                         PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='T';//ultima titulo
                         PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:='';//Penultima Sub-Conta
                         PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:=fieldbyname('contagerencial').asstring;//AntePenultimo � o Codigo da Conta

                         linhacontagerencialatual:=PStrgGrid.rowcount-1;
                         Contagerencialatual:=fieldbyname('contagerencial').AsString;
                         somacontagerencialatual:=0;

                         if (fieldbyname('tipo').asstring='D')
                         Then positivo:=False
                         Else positivo:=true;
               End;
               //****************************************************************


               //************************SUB-CONTAS*****************************
               Pquerylocal2.close;
               Pquerylocal2.sql.clear;
               Pquerylocal2.sql.add('Select tabtitulo.subcontagerencial,tabsubcontagerencial.nome as nomesubcontagerencial,tabsubcontagerencial.mascara from');
               Pquerylocal2.sql.add('tabpendencia join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
               Pquerylocal2.sql.add('join tabsubcontagerencial on tabtitulo.subcontagerencial=tabsubcontagerencial.codigo');
               Pquerylocal2.sql.add('where tabtitulo.contagerencial='+fieldbyname('contagerencial').asstring);
               Pquerylocal2.sql.add('and vencimento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatainicial))+#39);
               Pquerylocal2.sql.add('and vencimento <='#39+formatdatetime('mm/dd/yyyy',datalimite)+#39);
               PqueryLocal2.sql.add('group by subcontagerencial,tabsubcontagerencial.nome,tabsubcontagerencial.mascara');
               Pquerylocal2.open;
               Pquerylocal2.last;

               FMostraBarraProgresso.BarradeProgresso2.visible:=true;
               FMostraBarraProgresso.Lbmensagem2.visible:=true;
               FMostraBarraProgresso.BarradeProgresso2.Progress:=0;
               FMostraBarraProgresso.BarradeProgresso2.MaxValue:=Pquerylocal2.recordcount;
               FMostraBarraProgresso.Lbmensagem2.caption:='Conta Gerencial '+Fieldbyname('contagerencial').asstring;
               Pquerylocal2.first;
               
               While not(Pquerylocal2.eof) do
               Begin
                      PStrgGrid.rowcount:=PStrgGrid.rowcount+1;

                      PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:='    '+PqueryLocal2.fieldbyname('nomesubcontagerencial').asstring;
                      PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='S';
                      PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:=Pquerylocal2.fieldbyname('subcontagerencial').asstring;//Penultima Sub-Conta
                      PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:='';

       
                      for cont:=0 to STRdatas.Count-1 do
                      begin
                          PqueryLocal.close;
                          PqueryLocal.sql.clear;
                          PqueryLocal.sql.add('Select sum(tabpendencia.saldo) as SOMA');
                          PqueryLocal.sql.add('from tabpendencia');
                          PqueryLocal.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                          PqueryLocal.sql.add('where tabtitulo.contagerencial='+Fieldbyname('contagerencial').asstring);
                          PqueryLocal.sql.add('and tabtitulo.subcontagerencial='+Pquerylocal2.Fieldbyname('subcontagerencial').asstring);
                          PqueryLocal.sql.add('and extract(month from tabpendencia.vencimento)='+FormatDateTime('mm',strtodate(strdatas[cont])));
                          PqueryLocal.sql.add('and extract(year from tabpendencia.vencimento)='+FormatDateTime('yyyy',strtodate(strdatas[cont])));
                          PqueryLocal.open;
       
                          if (fieldbyname('tipo').asstring='D')
                          Then PStrgGrid.Cells[cont+1,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(-1*Pquerylocal.fieldbyname('soma').asfloat),12,' ')
                          Else PStrgGrid.Cells[cont+1,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(Pquerylocal.fieldbyname('soma').asfloat),12,' ');
       
                          if (fieldbyname('tipo').asstring='D')
                          Then StrSaldo[cont]:=Floattostr(strtofloat(StrSaldo[cont])-Pquerylocal.fieldbyname('soma').asfloat)
                          Else StrSaldo[cont]:=Floattostr(strtofloat(StrSaldo[cont])+Pquerylocal.fieldbyname('soma').asfloat);
                      End;

                      Pquerylocal2.next;
               End;
               //****************************************************************
               next;
          End;

          //Acabou as contas imprimindo a previsao e a totalizacao
          //*****************************************************
          if (PincluiPrevisao=true)
          Then Begin
                    //Filtrando todas as previsoes que estao ligadas a conta gerencial
                    //atual e dentro do periodo data inicial e limite
                    pquerylocal2.close;
                    pquerylocal2.sql.clear;
                    pquerylocal2.sql.add('Select TPF.codigo,TPF.historico,TPF.somatoriaprevisao,TPF.subcontagerencial');
                    pquerylocal2.sql.add('from TabPrevisaoFinanceira TPF');
                    pquerylocal2.sql.add('join tabsubcontagerencial TSBC on TPF.subcontagerencial=TSBC.codigo');
                    pquerylocal2.sql.add('where TPF.contagerencial='+contagerencialatual);
                    pquerylocal2.sql.add('and((Tpf.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatainicial))+#39);
                    pquerylocal2.sql.add('and Tpf.data<='+#39+formatdatetime('mm/dd/yyyy',datalimite)+#39+')');
                    pquerylocal2.sql.add('or (Tpf.data<='+#39+formatdatetime('mm/dd/yyyy',datalimite)+#39+' and considerarmesessubsequentes=''S''))');
                    pquerylocal2.sql.add('order by TPF.subcontagerencial');
                    pquerylocal2.open;

                    While not(pquerylocal2.eof) do
                    Begin
                         {Para cada previsao faco um select na data do for}
                         PStrgGrid.rowcount:=PStrgGrid.rowcount+1;
                         PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:='    Previs�o '+pquerylocal2.fieldbyname('historico').asstring;
                         PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='P';
                         PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:=pquerylocal2.fieldbyname('subcontagerencial').asstring;//Penultima Sub-Conta ou Previsao
                         PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:='';

                         pvalorsomatoria:=0;


                         for cont:=0 to STRdatas.Count-1 do
                         begin
                               {*******� selecionado baseado nos seguintes criterios*******
                                 1 - A data � o mes atual e nao considera meses subsequentes
                                 2 - A  data esta dentro desse ano ate o mes atual e
                                           - A data limite vence desse mes ao fim do ano
                                           - A data limite vence nos proximos anos
                                 3 - A data � em anos anterios
                                           - A data limite vence desse mes ao fim do ano
                                           - A data limite vence nos proximos anos}

                               pquerylocal.close;
                               pquerylocal.sql.clear;
                               pquerylocal.sql.add('Select codigo,historico,valor as SOMA');
                               pquerylocal.sql.add('from tabprevisaofinanceira');
                               pquerylocal.sql.add('where codigo='+PqueryLocal2.fieldbyname('codigo').asstring);
                               pquerylocal.sql.add('and (');
                               pquerylocal.sql.add('(extract(month from data)='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from data)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''N'')');
                               pquerylocal.sql.add('or (extract(month from data)<='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from data)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(month from datalimite)>='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from datalimite)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                               pquerylocal.sql.add('or (extract(month from data)<='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from data)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(year from datalimite)>'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                               pquerylocal.sql.add('or (extract(year from data)<'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(month from datalimite)>='+FormatDateTime('mm',strtodate(strdatas[cont]))+' and extract(year from datalimite)='+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                               pquerylocal.sql.add('or (extract(year from data)<'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+' and considerarmesessubsequentes=''S'' and extract(year from datalimite)>'+FormatDateTime('yyyy',strtodate(strdatas[cont]))+')');
                               pquerylocal.sql.add(')');
                               PqueryLocal.open;

                               if (positivo=true)
                               Then temp:=Pquerylocal.fieldbyname('soma').asfloat
                               Else temp:=-1*Pquerylocal.fieldbyname('soma').asfloat;

                               if (pquerylocal2.FieldByName('somatoriaprevisao').asstring='S')
                               Then pvalorsomatoria:=pvalorsomatoria+temp
                               Else pvalorsomatoria:=temp;

                               PStrgGrid.Cells[cont+1,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(pvalorsomatoria),12,' ');
                               StrSaldo[cont]:=Floattostr(strtofloat(StrSaldo[cont])+pvalorsomatoria);
                         End;
                         //pegando a proxima previsao
                         pquerylocal2.next;
                    End;//while Previsao
          End;
          //*****************************************************

          (*
          //Guardando a soma de cada mes  da conta gerencial anterior
          for cont:=0 to STRSALDo.Count-1 do
          begin
               PStrgGrid.Cells[1+cont,linhacontagerencialatual]:=formata_valor(STRSALDO[CONT]);
               STRSALDO[CONT]:='0';
          End;

          PStrgGrid.rowcount:=PStrgGrid.rowcount+2;
          PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:=fieldbyname('nomecontagerencial').asstring;
          PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='T';

          linhacontagerencialatual:=PStrgGrid.rowcount-1;
          Contagerencialatual:=fieldbyname('contagerencial').AsString;
          somacontagerencialatual:=0;

          if (fieldbyname('tipo').asstring='D')
          Then positivo:=False
          Else positivo:=true;
          //*****************************************************
          *)

          
          PStrgGrid.FixedRows:=1;
          PStrgGrid.FixedCols:=1;
          PStrgGrid.rowcount:=PStrgGrid.rowcount+2;
          PStrgGrid.cells[0,PStrgGrid.rowcount-1]:=Formata_valor(SaldoAtual);//primeira coluna recebe o saldo atual

          for cont:=0 to PstrgGrid.RowCount-1 do
          Begin
                PStrgGrid.RowHeights[cont]:=14;
          End;

          for cont:=0 to PstrgGrid.ColCount-1 do
          Begin
                PStrgGrid.ColWidths[cont]:=15;
          End;

          (*for cont:=0 to StrSaldo.Count-1 do
          begin
               SaldoAtual:=SaldoAtual+strtofloat(Strsaldo[cont]);
               PStrgGrid.cells[cont+1,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(Strsaldo[cont],12,' ');//cada coluna recebe seu saldo
          End;*)


     End;

finally
       FMostraBarraProgresso.close;
       Freeandnil(STRcontaGerencial_EXCLUIR);
       Freeandnil(STRSubcontaGerencial_EXCLUIR);
       Freeandnil(STRLPortador);
       Freeandnil(Strdatas);
       FreeAndNil(PqueryLocal);
       FreeAndNil(PqueryLocal2);
       Freeandnil(StrSaldo);
End;

end;



Procedure TObjtitulo.GeraFormularioPrevisaoFinanceira_Realizado(PdataInicial,PdataFinal:string;pportador,pexcluircontagerencial,pexcluirsubcontagerencial:String;PCabecalho,Prodape:Tstrings;PStrgGrid:TStringGrid;var PsaldoInicial:currency);
var
DataLimite:tdate;
SaldoAtual:Currency;
cont:integer;
STRcontaGerencial_EXCLUIR:TStringList;
STRSubcontaGerencial_EXCLUIR,STRdatas,StrSaldo:TStringList;
STRLPortador:TStringList;
meses:integer;
pdataatual:tdate;
PqueryLocal,PqueryLocal2:TIbquery;

Contagerencialatual:string;
pcoluna,linhacontagerencialatual:integer;
temp,pvalorsomatoria,somacontagerencialatual:currency;

positivo:boolean;

begin
     PCabecalho.Clear;
     Try
        STRcontaGerencial_EXCLUIR:=TStringList.create;
        STRcontaGerencial_EXCLUIR.clear;

        STRSUBcontaGerencial_EXCLUIR:=TStringList.create;
        STRSUBcontaGerencial_EXCLUIR.clear;

        StrDatas:=TStringList.create;
        StrDatas.clear;

        STRLPortador:=TStringList.create;
        STRLPortador.clear;

        StrSaldo:=TstringList.create;
        StrSaldo.clear;

        PqueryLocal:=TIbquery.create(nil);
        PqueryLocal.Database:=fdatamodulo.IBDatabase;

        PqueryLocal2:=TIbquery.create(nil);
        PqueryLocal2.Database:=fdatamodulo.IBDatabase;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "STRcontaGerencial"',mterror,[mbok],0);
           exit;
     End;

Try

     Try
        strtodate(Pdatainicial);
     Except
           Mensagemerro('Data Inicial inv�lida');
           exit;
     End;

     Try
           DataLimite:=Strtodate(Pdatafinal);
     Except
           Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
           exit;
     End;

     Meses:=MonthsBetween(strtodate(pdatainicial),datalimite);

     cont:=1;
     pdataatual:=strtodate(pdatainicial);
     While (formatdatetime('mm/yyyy',pdataatual)<>formatdatetime('mm/yyyy',datalimite)) do
     Begin
          pdataatual:=IncMonth(pdataatual,1);
          cont:=cont+1;
     End;
     meses:=cont;


     STRdatas.Clear;
     StrSaldo.clear;

     for cont:=0 to meses-1 do
     Begin
          Pdataatual:=IncMonth(strtodate(pdatainicial),cont);
          STRdatas.add('01/'+formatdatetime('mm/yyyy',pdataatual));
          StrSaldo.add('0');
     End;


     STRLPortador.clear;

     if (explodestr(pportador,STRLPortador,';','integer')=False)
     then exit;

     if (STRLPortador.count>0)
     Then Begin
               for cont:=0 to STRLPortador.Count-1 do
               Begin
                    if (Self.portador.LocalizaCodigo(STRLPortador[cont])=False)
                    then begin
                              MensagemErro('Portador '+STRLPortador[cont]+' n�o encontrado');
                              exit;
                    End;
               End;
     End
     Else Begin
               MensagemErro('� necess�rio escolher pelo menos um portador');
               exit;
     End;

     if (explodestr(pexcluircontagerencial,STRcontaGerencial_EXCLUIR,';','integer')=False)
     then exit;

     if (STRcontaGerencial_EXCLUIR.count>0)
     Then Begin
               for cont:=0 to STRcontaGerencial_EXCLUIR.Count-1 do
               Begin
                    if (Self.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial_EXCLUIR[cont])=False)
                    then begin
                              MensagemErro('Conta gerencial '+STRcontaGerencial_EXCLUIR[cont]+' n�o encontrada');
                              exit;
                    End;
               End;
     End;

     if (explodestr(pexcluirsubcontagerencial,STRSubcontaGerencial_EXCLUIR,';','integer')=False)
     then exit;

     if (STRSubcontaGerencial_EXCLUIR.count>0)
     Then Begin
               for cont:=0 to STRSubcontaGerencial_EXCLUIR.Count-1 do
               Begin
                    if (Self.SubCONTAGERENCIAL.LocalizaCodigo(STRSubcontaGerencial_EXCLUIR[cont])=False)
                    then begin
                              MensagemErro('Sub-Conta gerencial '+STRSubcontaGerencial_EXCLUIR[cont]+' n�o encontrada');
                              exit;
                    End;
               End;
     End;




     With Self.objquery do
     Begin
          //Selecionando todas as contas gerenciais que foram utilizadas em pagamentos ou recebimentos
          //no per�odo escolhido

          close;
          Sql.clear;
          Sql.add('SELECT PROC.TIPO,');
          Sql.add('PROC.CONTAGERENCIAL,TABCONTAGER.NOME AS NOMECONTAGERENCIAL,');
          Sql.add('PROC.SUBCONTAGERENCIAL,TABSUBCONTAGERENCIAL.NOME AS NOMESUBCONTAGERENCIAL');
          Sql.add('FROM PROC_REALIZADO_E_ABERTOS(:Pdatainicial,:DATALIMITE) PROC');
          Sql.add('JOIN TABCONTAGER ON PROC.CONTAGERENCIAL=TABCONTAGER.CODIGO');
          Sql.add('JOIN TABSUBCONTAGERENCIAL ON PROC.SUBCONTAGERENCIAL=TABSUBCONTAGERENCIAL.CODIGO');
          Sql.add('where PROC.TIPO<>''G'' ');

          if (STRcontaGerencial_EXCLUIR.count>0)
          then begin
                    for cont:=0 to STRcontaGerencial_EXCLUIR.count-1 do
                    Begin
                        SQL.add(' and tabcontager.codigo<>'+STRcontaGerencial_EXCLUIR[cont]);
                    End;
          End;

          if (STRSubcontaGerencial_EXCLUIR.count>0)
          then begin
                    for cont:=0 to STRSubcontaGerencial_EXCLUIR.count-1 do
                    Begin
                        SQL.add(' and tabsubcontagerencial.codigo<>'+STRSubcontaGerencial_EXCLUIR[cont]);
                    End;
          End;

          Sql.add('GROUP BY PROC.TIPO,');
          Sql.add('PROC.CONTAGERENCIAL,TABCONTAGER.NOME,');
          Sql.add('PROC.SUBCONTAGERENCIAL,TABSUBCONTAGERENCIAL.NOME');
          Sql.add('ORDER BY PROC.TIPO');

          parambyname('pdatainicial').asstring:=pdatainicial;
          parambyname('datalimite').asstring:=pdatafinal;

          FmostraStringList.Memo.Lines.Text:=sql.Text;
          //FmostraStringList.ShowModal;
          open;
          last;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi Selecionada na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Gerando o Relat�rio';
          first;

          SaldoAtual:=0;

          //Selecionando o Saldo Atual dos portadores escolhidos
          
          if (STRLPortador.Count>0)
          then begin
                   Self.PORTADOR.LocalizaCodigo(STRLPORTADOR[0]);
                   Self.portador.TabelaparaObjeto;

                   for cont:=0 to STRLPORTADOR.count-1 do
                   begin
                        Self.portador.LocalizaCodigo(STRLPORTADOR[cont]);
                        Self.portador.TabelaparaObjeto;
                        SaldoAtual:=SaldoAtual+strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo));
                   End;
          End;
          
          PsaldoInicial:=SaldoAtual;

          //Aqui ja vai para o Grid
          //04 a Mais = Titulo + Codigo CG +Codigo SUB + Indicacao (Titulo,Previsao,Manual)
          //2 X as datas para ter uma coluna de realizado e uma de a realizar
          PStrgGrid.Colcount:=(STRdatas.Count*2)+4;

          for cont:=0 to PStrgGrid.Colcount-1 do
          Begin
               PStrgGrid.cols[cont].clear;
          End;

          PStrgGrid.Cells[0,0]:='Contas e Sub-Contas';

          //escrevendo as datas no cabecalho das colunas do StrgGrid
          pcoluna:=1;
          for cont:=0 to STRdatas.count-1 do
          Begin
               //primeira coluna do REALIZADO
               PStrgGrid.cols[pcoluna].clear;
               PStrgGrid.Cells[pcoluna,0]:=formatdatetime('RZ mm/yyyy',strtodate(STRdatas[cont]));
               //segunda coluna do A REALIZAR
               pcoluna:=pcoluna+1;
               PStrgGrid.cols[pcoluna].clear;
               PStrgGrid.Cells[pcoluna,0]:=formatdatetime('AR mm/yyyy',strtodate(STRdatas[cont]));

               pcoluna:=pcoluna+1;
          End;
          PStrgGrid.rowcount:=1;//somente uma linha

          linhacontagerencialatual:=1;
          Contagerencialatual:=fieldbyname('contagerencial').AsString;
          somacontagerencialatual:=0;

          //Escrevendo o nome da primeira conta gerencial (linha 2)
          PStrgGrid.rowcount:=PStrgGrid.rowcount+1;
          PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:=fieldbyname('nomecontagerencial').asstring;//primeira coluna � o nome da conta gerencial
          PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='T';//ultima � o TIPO
          PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:='';//Penultima Sub-Conta
          PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:=fieldbyname('contagerencial').asstring;//AntePenultimo � o Codigo da Conta
          //***************************************************

          if (fieldbyname('tipo').asstring='D')
          Then positivo:=False
          Else positivo:=true;

          //Esse while percorre todas as contas gerencias/sub contas que foram selecionadas em uma query
          //de contas pagas e recebidas no periodo escolhido
          while not(eof) do
          Begin
               //***************************************************************
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               Application.ProcessMessages;


               //Mudou a Conta Gerencial
               if (Contagerencialatual<>fieldbyname('contagerencial').asstring)
               Then Begin
                         //crio duas linhas para poder separar as contas gerenciais no grid
                         PStrgGrid.rowcount:=PStrgGrid.rowcount+2;

                         PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:=fieldbyname('nomecontagerencial').asstring;//primeira coluna nome da conta
                         PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='T';//ultima titulo
                         PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:='';//Penultima Sub-Conta
                         PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:=fieldbyname('contagerencial').asstring;//AntePenultimo � o Codigo da Conta

                         linhacontagerencialatual:=PStrgGrid.rowcount-1;
                         Contagerencialatual:=fieldbyname('contagerencial').AsString;
                         somacontagerencialatual:=0;

                         if (fieldbyname('tipo').asstring='D')
                         Then positivo:=False
                         Else positivo:=true;
               End;

               //imprimindo o nome da sub-conta gerencial
               PStrgGrid.rowcount:=PStrgGrid.rowcount+1;
               PStrgGrid.Cells[0,PStrgGrid.rowcount-1]:='    '+fieldbyname('nomesubcontagerencial').asstring;
               PStrgGrid.Cells[PStrgGrid.colcount-1,PStrgGrid.rowcount-1]:='S';
               PStrgGrid.Cells[PStrgGrid.colcount-2,PStrgGrid.rowcount-1]:=fieldbyname('subcontagerencial').asstring;//Penultima Sub-Conta
               PStrgGrid.Cells[PStrgGrid.colcount-3,PStrgGrid.rowcount-1]:='';

               //Para cada m�s, seleciono o valor de quitacao e preencho na coluna correta
               pcoluna:=1;

               for cont:=0 to STRdatas.count-1 do
               Begin

                     //Selecionando o realizado para o m�s desejada e na subconta atual
                     PqueryLocal.close;
                     PqueryLocal.sql.clear;
                     PqueryLocal.sql.add('Select sum(tablancamento.valor) as soma');
                     PqueryLocal.sql.add('from tabtitulo');
                     PqueryLocal.sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
                     PqueryLocal.sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
                     PqueryLocal.sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
                     PqueryLocal.sql.add('join tablancamento on tabpendencia.codigo=tablancamento.pendencia');
                     PqueryLocal.Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
                     PqueryLocal.Sql.add('where tabtipolancto.classificacao=''Q'' ');
                     PqueryLocal.sql.add('and tabtitulo.contagerencial='+Fieldbyname('contagerencial').asstring);

                     if (Fieldbyname('subcontagerencial').asstring='')//titulos sem conta gerencial
                     Then PqueryLocal.sql.add('and tabtitulo.subcontagerencial is null')
                     else PqueryLocal.sql.add('and tabtitulo.subcontagerencial='+Fieldbyname('subcontagerencial').asstring);
                     
                     PqueryLocal.sql.add('and extract(month from tablancamento.data)='+FormatDateTime('mm',strtodate(strdatas[cont])));
                     PqueryLocal.sql.add('and extract(year from tablancamento.data)='+FormatDateTime('yyyy',strtodate(strdatas[cont])));
                     PqueryLocal.open;
                     
                     if (fieldbyname('tipo').asstring='D')
                     Then PStrgGrid.Cells[pcoluna,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(-1*Pquerylocal.fieldbyname('soma').asfloat),12,' ')
                     Else PStrgGrid.Cells[pcoluna,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(Pquerylocal.fieldbyname('soma').asfloat),12,' ');
                     
                     pcoluna:=pcoluna+1;
                     
                     //Selecionando o a realizar para o m�s desejado e na subconta atual
                     PqueryLocal.close;
                     PqueryLocal.sql.clear;
                     PqueryLocal.sql.add('Select sum(tabpendencia.saldo) as soma');
                     PqueryLocal.sql.add('from tabtitulo');
                     PqueryLocal.sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
                     PqueryLocal.sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
                     PqueryLocal.sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
                     PqueryLocal.Sql.add('where tabtitulo.contagerencial='+Fieldbyname('contagerencial').asstring);

                     if (Fieldbyname('subcontagerencial').asstring='')//titulos sem conta gerencial
                     Then PqueryLocal.sql.add('and tabtitulo.subcontagerencial is null')
                     else PqueryLocal.sql.add('and tabtitulo.subcontagerencial='+Fieldbyname('subcontagerencial').asstring);

                     if (cont=0)//primeiro mes pego tudo que esteja aberto antes deste mes e lance no mes
                     Then Begin
                               PqueryLocal.sql.add('and extract(month from tabpendencia.vencimento)='+FormatDateTime('mm',strtodate(strdatas[cont])));
                               PqueryLocal.sql.add('and extract(year from tabpendencia.vencimento)='+FormatDateTime('yyyy',strtodate(strdatas[cont])));
                     End
                     Else Begin
                               PqueryLocal.sql.add('and extract(month from tabpendencia.vencimento)='+FormatDateTime('mm',strtodate(strdatas[cont])));
                               PqueryLocal.sql.add('and extract(year from tabpendencia.vencimento)='+FormatDateTime('yyyy',strtodate(strdatas[cont])));
                     End;
                     PqueryLocal.open;

                     if (fieldbyname('tipo').asstring='D')
                     Then PStrgGrid.Cells[pcoluna,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(-1*Pquerylocal.fieldbyname('soma').asfloat),12,' ')
                     Else PStrgGrid.Cells[pcoluna,PStrgGrid.rowcount-1]:=completapalavra_a_esquerda(formata_valor(Pquerylocal.fieldbyname('soma').asfloat),12,' ');
                     pcoluna:=pcoluna+1;
                     
               End;//for

               next;
               //****************************************************************
               
          End;// With Self.objquery do

          PStrgGrid.FixedRows:=1;
          PStrgGrid.FixedCols:=1;
          PStrgGrid.rowcount:=PStrgGrid.rowcount+2;//1 em branco, 1 de total
          PStrgGrid.cells[0,PStrgGrid.rowcount-1]:=Formata_valor(SaldoAtual);//primeira coluna recebe o saldo atual

          for cont:=0 to PstrgGrid.RowCount-1 do
          Begin
                PStrgGrid.RowHeights[cont]:=14;
          End;

          for cont:=0 to PstrgGrid.ColCount-1 do
          Begin
                PStrgGrid.ColWidths[cont]:=15;
          End;
     End;

finally
       FMostraBarraProgresso.close;
       Freeandnil(STRcontaGerencial_EXCLUIR);
       Freeandnil(STRSubcontaGerencial_EXCLUIR);
       Freeandnil(STRLPortador);
       Freeandnil(Strdatas);
       freeandnil(PqueryLocal);
       freeandnil(PqueryLocal2);
       Freeandnil(StrSaldo);
End;

end;

procedure TObjTitulo.BeforePrintPevisao(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
        //o que me interessa aqui � zerar a variavel que ira
        //calcular o saldo do banco linha por linha
        //no inicio e no fim � necess�rio iguala-la ao
        //saldo atual
        Self.SaldoBancoRel:=0;
        Self.SaldoBancoRel:=Strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo));
end;

{procedure TObjTitulo.CampoSaldoBAncoRelPrint(sender: TObject; var Value: String);
var
valorAtualTmp:Currency;
begin
     //este procedimento � disparado no momento que o campo de saldo
     //de relatorio
     try
        valorAtualTmp:=strtofloat(TQRDBTExt(Sender).Dataset.fieldbyname('SALDO').asstring);
     Except
           valorAtualTmp:=0;
     End;
     If ((TQRDBTExt(Sender).Dataset.fieldbyname('DEBITO_CREDITO').asstring)='C')
     Then Self.SaldoBancoRel:=Self.SaldoBancoRel+valorAtualTmp
     Else Self.SaldoBancoRel:=Self.SaldoBancoRel-valorAtualTmp;

     Value:=formata_valor(Floattostr(self.saldobancorel));


end;
}



destructor TObjTitulo.Free;
begin
        Freeandnil(Self.ObjDataset);
        Freeandnil(Self.parametropesquisa);
        Freeandnil(self.Objquery);

        Self.PORTADOR.free;
        Self.CONTAGERENCIAL.free;
        Self.gerador.free;
        Self.CREDORDEVEDOR.free;
        Self.Prazo.free;
        Self.ObjExportaContabilidade.free;
        Self.Objplanodecontas.free;
        Self.SubContaGerencial.free;

        //adicionado celio 301109
        Freeandnil(ObjQueryMostraTitulo);
        FreeAndNil(Self.ObjDatasourceMOstraTitulo);
end;

function TObjTitulo.Get_geradortabela: string;
begin
     Result:=Self.GERADOR.Get_Tabela;
end;
function TObjTitulo.Get_Pesquisa(PComSaldo:Boolean): TStringList;
Begin
     Result:=Self.Get_Pesquisa(PComSaldo,'');
End;

function TObjTitulo.Get_Pesquisa(PComSaldo:Boolean;PDebitoCredito:string): TStringList;
var
PCampos:String;
PProcedimento:String;
begin
     if (ObjParametroGlobal.ValidaParametro('CAMPOS NA PESQUISA DE TITULOS NO FORM DE TITULOS')=True)
     then Begin
               Pcampos:=ObjParametroGlobal.get_valor;
               if (Pcampos='')
               Then Pcampos:='*';
     End
     Else Pcampos:='*';


     if (ObjParametroGlobal.ValidaParametro('PROCEDIMENTO DO INTERBASE A SER USADO NA PESQUISA DE TITULOS NO FORM DE TITULOS')=True)
     then Begin
               PPROCEdIMENTO:=ObjParametroGlobal.get_valor;
               if (PPROCEDIMENTO='')
               Then PPROCEDIMENTO:='PROCTITULOS';
     End
     Else PPROCEDIMENTO:='PROCTITULOS';

     Self.ParametroPesquisa.Clear;
     If ((RegLancTituloExterno.LancamentoExterno=True) and (RegLancTituloExterno.LE_GERADOR<>'') and (RegLancTituloExterno.LE_CODIGOGERADOR<>''))
     Then Self.ParametroPesquisa.add(' Select tAbtitulo.* from TabTitulo where gerador='+RegLancTituloExterno.LE_GERADOR+'  and codigogerador='+RegLancTituloExterno.LE_CODIGOGERADOR)
     Else Begin
               Self.ParametroPesquisa.add('Select '+pcampos+' from '+pprocedimento);


               With FopcaoRel do
               Begin

                    With RgOpcoes do
                    Begin
                        items.clear;
                        items.add('T�tulos a Pagar');//0
                        items.add('T�tulos a Receber');//1

                    End;

                    if (PDebitoCredito='')
                    Then showmodal
                    Else Begin
                              tag:=1;
                              if (pDebitoCredito='D')
                              Then rgopcoes.itemindex:=0
                              Else rgopcoes.itemindex:=1;
                    End;


                    if (tag=0)
                    Then Self.ParametroPesquisa.clear
                    else Begin
                            Case RgOpcoes.ItemIndex of
                              0:Self.ParametroPesquisa.add('where '+pprocedimento+'.Tipo=''D'' ');
                              1:Self.ParametroPesquisa.add('where '+pprocedimento+'.Tipo=''C'' ');
                            End;
                            if (PComSaldo=True)
                            Then Self.ParametroPesquisa.add('and '+pprocedimento+'.saldopendencia>0')
                            Else Self.ParametroPesquisa.add('and '+pprocedimento+'.saldopendencia=0');
                    End;
               End;
     End;

     Result:=Self.ParametroPesquisa;


end;


function TObjTitulo.Get_NUMDCTO: string;
begin
     Result:=Self.NumDcto;
end;

procedure TObjTitulo.Submit_NUMDCTO(parametro: string);
begin
     Self.NumDcto:=Parametro;
end;

function TObjTitulo.Get_FormularioCredorDevedor(PCodigoCredorDevedor:string): string;
begin
     If (Self.CREDORDEVEDOR.LocalizaCodigo(PCodigoCredorDevedor)=true)
     Then Begin
             Self.CREDORDEVEDOR.TabelaparaObjeto;
             Result:=Self.CREDORDEVEDOR.Get_NomeFormulario;
          End
     Else result:='';

end;

function TObjTitulo.Get_CampoNomeCredorDevedor(PCodigoCredorDevedor:string): string;
begin
     If (Self.CREDORDEVEDOR.LocalizaCodigo(PCodigoCredorDevedor)=true)
     Then Begin
             Self.CREDORDEVEDOR.TabelaparaObjeto;
             Result:=Self.CREDORDEVEDOR.Get_CampoNome;
          End
     Else result:='';

end;

function TObjTitulo.Get_NomeCredorDevedor(PCodigoCredorDevedor,
  PCampoPrimario: string): string;
begin
     //Localizo o CredorDevedor na Tabela de CredorDevedor
     //retorno o sql e o campo nome para gerar o c�digo SQL
     //que retornara o nome conforme a tabela
     If (PCodigoCredorDevedor='')
     Then exit;
     
     Result:='';
     If (Self.CREDORDEVEDOR.LocalizaCodigo(PCodigoCredorDevedor)=False)
     Then exit;

     Self.credordevedor.TabelaparaObjeto;

     with Self.ObjDataset do
     Begin
          SelectSQL.clear;
          SelectSQL.add(Self.credorDevedor.Get_InstrucaoSQL+' where codigo='+PCampoPrimario);
          Try
                open;
                Result:=fieldbyname(Self.credordevedor.get_camponome).asstring;
          Except
                exit;
          End;
     End;

end;



//Usado para gravar o registro atual na tabela exporta��o da contabilidade
function TObjTitulo.Grava_Exportacao: Boolean;
var
PCContaGerencial,PCCredorDevedor:string;
begin
     Result:=False;
     
     Self.ObjExportaContabilidade.ZerarTabela;
     Self.ObjExportaContabilidade.Submit_Exportado('N');
     Self.ObjExportaContabilidade.Submit_ObjGerador('OBJTITULO');
     Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
     Self.ObjExportaContabilidade.Submit_NumDocto(Self.NumDcto);


     Self.CONTAGERENCIAL.LocalizaCodigo(self.CONTAGERENCIAL.Get_CODIGO);
     Self.CONTAGERENCIAL.TabelaparaObjeto;
     if (Self.SubContaGerencial.Get_CODIGO<>'')
     then Begin
               Self.SubContaGerencial.LocalizaCodigo(Self.SubContaGerencial.Get_CODIGO);
               Self.SubContaGerencial.TabelaparaObjeto;
     End;
     Self.CREDORDEVEDOR.LocalizaCodigo(CREDORDEVEDOR.Get_CODIGO);
     Self.CREDORDEVEDOR.TabelaparaObjeto;

     //*************************************
     //Encontrando os dados do plano de contas do credor/devedsdor
     PCCredorDevedor:='';

     self.ObjExportaContabilidade.Submit_Exporta('N');

     If (Self.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
     Then PCCredorDevedor:=Self.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(self.credordevedor.get_codigo,self.CODIGOCREDORDEVEDOR);

     If (PCCredorDevedor='')
     Then PCCredorDevedor:='0'
     Else Begin
             If (Self.Objplanodecontas.LocalizaCodigo(PCCredorDevedor)=False)
             Then PCCredorDevedor:='0'
             Else Begin
                       Self.Objplanodecontas.TabelaparaObjeto;
                       self.ObjExportaContabilidade.Submit_Exporta('S');
                  End;
          End;
     //**************************************
     //Encontrando os dados do plano de contas da Conta Gerencial
     //Se tiver subcontagerencial utilize o codigo dela
     
     PCContaGerencial:='';
     if (Self.SubContaGerencial.Get_CODIGO<>'') and (Self.SubContaGerencial.Get_CODIGOPLANODECONTAS<>'')
     then PCContaGerencial:=Self.SubContaGerencial.Get_CodigoPlanodeContas
     else PCContaGerencial:=Self.CONTAGERENCIAL.Get_CodigoPlanodeContas;



     If (PCContaGerencial='')
     Then Begin
                PCContaGerencial:='0';
                self.ObjExportaContabilidade.Submit_Exporta('N');
     End
     Else Begin
                If (Self.Objplanodecontas.LocalizaCodigo(PCContaGerencial)=False)
                Then Begin
                        PCContaGerencial:='0';
                        self.ObjExportaContabilidade.Submit_Exporta('N');
                End
                Else Self.Objplanodecontas.TabelaparaObjeto;
     End;
     //****************************************

     //preciso gravar os dados principais na exporta��o
     //Inserindo os dados
     ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
     ObjExportaContabilidade.Submit_Data(Self.EMISSAO);


     If(Self.CONTAGERENCIAL.Get_Tipo='D')
     Then Begin
               //Credita do Credor/Devedor e Debita da Conta Gerencial
               ObjExportaContabilidade.Submit_ContaCredite(PCCredorDevedor);
               ObjExportaContabilidade.Submit_ContaDebite(PCContaGerencial);
          End
     Else Begin
               //Debita do Credor/Devedor e Credita na ContaGerencial
               ObjExportaContabilidade.Submit_ContaDebite(PCCredorDevedor);
               ObjExportaContabilidade.Submit_ContaCredite(PCContaGerencial);
          End;

     ObjExportaContabilidade.Submit_Valor(Self.VALOR);
     ObjExportaContabilidade.Submit_Historico(Self.HISTORICO+'-'+Self.Get_NUMDCTO);
     ObjExportaContabilidade.Status:=dsinsert;
     Result:=ObjExportaContabilidade.Salvar(False);
     ObjExportaContabilidade.Status:=dsInactive;
end;


function TObjTitulo.GeraContabilidadeExclusao(ComCommit:boolean): boolean;
var
PCContaGerencial,PCCredorDevedor:string;
NomePCContaGerencial,NomePCCredorDevedor:string;
begin
     Result:=False;
     //primeiro verifico se preciso excluir a contabilidade?
     If (ObjParametroGlobal.ValidaParametro('EXPORTA CONTABILIDADE EXCLUS�O DE T�TULO?')=False)
     Then Begin
               Messagedlg('O parametro "EXPORTA CONTABILIDADE EXCLUS�O DE T�TULO?" n�o foi encontrado!',mterror,[mbok],0);
               exit;
     End;

     If (ObjParametroGlobal.Get_Valor='N�O')
     Then Begin
               Result:=True;
               exit;
     End;

     //
     //*******************
     Result:=False;
     objExportaContabilidade.ZerarTabela;
     Self.ObjExportaContabilidade.Submit_Exportado('N');
     Self.ObjExportaContabilidade.Submit_ObjGerador('OBJTITULO');
     Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);

     PCCredorDevedor:='';
     NomePCCredorDevedor:='';

     self.ObjExportaContabilidade.Submit_Exporta('N');

     If (Self.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
     Then PCCredorDevedor:=Self.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(self.credordevedor.get_codigo,self.CODIGOCREDORDEVEDOR);

     If (PCCredorDevedor='')
     Then PCCredorDevedor:='0'
     Else Begin
             If (Self.Objplanodecontas.LocalizaCodigo(PCCredorDevedor)=False)
             Then PCCredorDevedor:='0'
             Else Begin
                       Self.Objplanodecontas.TabelaparaObjeto;
                       NomePCCredorDevedor:=Self.Objplanodecontas.Get_Nome;
                       self.ObjExportaContabilidade.Submit_Exporta('S');
                  End;
          End;
     //**************************************
     //Encontrando os dados do plano de contas da Conta Gerencial
     PCContaGerencial:='';
     NomePCContaGerencial:='';
     PCContaGerencial:=Self.CONTAGERENCIAL.Get_CodigoPlanodeContas;

     If (PCContaGerencial='')
     Then Begin
                PCContaGerencial:='0';
                self.ObjExportaContabilidade.Submit_Exporta('N');
     End
     Else Begin
                If (Self.Objplanodecontas.LocalizaCodigo(PCContaGerencial)=False)
                Then Begin
                        PCContaGerencial:='0';
                        self.ObjExportaContabilidade.Submit_Exporta('N');
                End
                Else Begin
                        Self.Objplanodecontas.TabelaparaObjeto;
                        NomePCContaGerencial:=Self.Objplanodecontas.Get_Nome;
                End;
     End;
     //****************************************

     //preciso gravar os dados principais na exporta��o
     //Inserindo os dados
     ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
     ObjExportaContabilidade.Submit_Data(DATETOSTR(NOW));

     If(Self.CONTAGERENCIAL.Get_Tipo='D')
     Then Begin
          //Debita do Credor/Devedor e Credita na ContaGerencial
          ObjExportaContabilidade.Submit_ContaDebite(PCCredorDevedor);
          ObjExportaContabilidade.Submit_ContaCredite(PCContaGerencial);
     End
     Else Begin
               //Credita do Credor/Devedor e Debita da Conta Gerencial
               ObjExportaContabilidade.Submit_ContaCredite(PCCredorDevedor);
               ObjExportaContabilidade.Submit_ContaDebite(PCContaGerencial);
          End;

     ObjExportaContabilidade.Submit_Valor(Self.VALOR);
     ObjExportaContabilidade.Submit_Historico('EXCLUS�O - '+Self.HISTORICO);



     ObjExportaContabilidade.Status:=dsinsert;
     Result:=ObjExportaContabilidade.Salvar(ComCommit);
     ObjExportaContabilidade.Status:=dsInactive;
end;


procedure TObjTitulo.Get_PendenciaseTitulo(PAGAR_RECEBER: string;
  Pvencimento: string; PGRID: tstringgrid;PCredorDevedor,PCodigoCredorDevedor:String;PCodigoBoleto:string;ptipocolunas:Tstrings);
Begin
     Self.Get_PendenciaseTitulo(PAGAR_RECEBER,Pvencimento,PGRID,PCredorDevedor,PCodigoCredorDevedor,PCodigoBoleto,ptipocolunas,'','');
End;

procedure TObjTitulo.Get_PendenciaseTitulo(PAGAR_RECEBER: string;
  Pvencimento: string; PGRID: tstringgrid;PCredorDevedor,PCodigoCredorDevedor:String;PCodigoBoleto:string;ptipocolunas:Tstrings;PcontaGer,PSubContaGer:String);
VAR
  cont:integer;
  StrCodigos:TstringList;
begin
     Try
          StrCodigos:=TstringList.create;
          StrCodigos.Clear;
     except
           MensagemErro('Erro na tentativa de criar a StringList de C�digos');
           exit;
     End;

Try

     Try
        if (PCredorDevedor<>'')
        Then strtoint(PCredorDevedor);
     Except
           PCredorDevedor:='';
     End;

     Try
        if (PCodigoCredorDevedor<>'')
        Then Begin
                  if (ExplodeStr(PCodigoCredorDevedor,Strcodigos,';','INTEGER')=False)
                  Then exit;
        End;
     Except
           PCodigoCredorDevedor:='';
     End;


     Try
        if (trim(comebarra(pvencimento))<>'')
        Then strtodate(Pvencimento)
        else pvencimento:='';
     Except
           Messagedlg('Data de Vencimento Inv�lida!',mterror,[mbok],0);
           exit;
     End;

     Try
        if (PCodigoBoleto<>'')
        Then strtoint(PCodigoBoleto);
     Except
           Messagedlg('C�digo do Boleto Inv�lido!',mterror,[mbok],0);
           exit;

     End;

     If (PAGAR_RECEBER<>'C') and (PAGAR_RECEBER<>'D')
     Then Begin
               Messagedlg('Escolha se os T�tulos ser�o para Pagar ou Receber!',mterror,[mbok],0);
               exit;
     End;

     PGRID.RowCount:=1;
     PGRID.ColCount:=15;

     ptipocolunas.Clear;
     Pgrid.Cols[0].clear;
     Pgrid.Cols[1].clear;
     Pgrid.Cols[2].clear;
     Pgrid.Cols[3].clear;
     Pgrid.Cols[4].clear;
     Pgrid.Cols[5].clear;
     Pgrid.Cols[6].clear;
     PGRID.Cols[7].clear;
     PGRID.Cols[8].clear;
     PGRID.Cols[7].clear;
     PGRID.Cols[9].clear;
     PGRID.Cols[10].clear;
     PGRID.Cols[11].clear;
     PGRID.Cols[12].clear;
     PGRID.Cols[13].clear;

     ptipocolunas.add('integer');
     PGRID.Cells[00,0]:='COD.TIT.';
     ptipocolunas.add('string');
     PGRID.Cells[01,0]:='HIST. T�TULO';
     ptipocolunas.add('date');
     PGRID.Cells[02,0]:='EMISSAO';
     ptipocolunas.add('integer');
     PGRID.Cells[03,0]:='COD.PEND.';
     ptipocolunas.add('string');
     PGRID.Cells[04,0]:='HIST. PEND�NCIA';
     ptipocolunas.add('date');
     PGRID.Cells[05,0]:='VENCIMENTO';
     ptipocolunas.add('decimal');
     PGRID.Cells[06,0]:='VALOR';
     ptipocolunas.add('decimal');
     PGRID.Cells[07,0]:='SALDO';
     ptipocolunas.add('decimal');
     PGRID.cells[08,0]:='VALOR REC/PAG';
     ptipocolunas.add('string');
     PGRID.cells[09,0]:='NUMDCTO';

     if (PAGAR_RECEBER='C')
     Then Begin
              ptipocolunas.add('integer');
              PGRID.cells[10,0]:='COD.BOLETO';
              ptipocolunas.add('date');
              PGRID.Cells[11,0]:='VENCIMENTO BOLETO';
     End
     Else Begin
              //no contas a pagar usa essa coluna para o Boletoapagar
              ptipocolunas.add('string');
              PGRID.cells[10,0]:='BOLETO A PAGAR';
              ptipocolunas.add('string');
              PGRID.Cells[11,0]:='.';
     End;
     
     ptipocolunas.add('string');
     PGRID.Cells[12,0]:='NOTAFISCAL';

     ptipocolunas.add('string');
     PGRID.Cells[13,0]:='TMP';//usado como campo temporario... para diversas opera��es
     PGRID.Cells[14,0]:='CodigoCredorDevedor';
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select Tabpendencia.Titulo,TAbtitulo.Historico as HistoricoTitulo,TAbtitulo.NUMDCTO,TAbtitulo.emissao,Tabpendencia.Codigo,');
          SelectSQL.add('Tabpendencia.Historico as HistoricoPendencia,');
          SelectSQL.add('Tabpendencia.Vencimento,Tabpendencia.Valor,Tabpendencia.saldo,TAbtitulo.NotaFiscal,TABPENDENCIA.BOLETOBANCARIO,tabboletobancario.vencimento as VENCIMENTOBOLETO,tabpendencia.boletoapagar');
          SelectSQL.add(',tabtitulo.codigocredordevedor from Tabtitulo');
          SelectSQL.add('left join Tabpendencia on Tabtitulo.codigo=Tabpendencia.titulo');
          SelectSQL.add('left join TabContager on TAbtitulo.contagerencial=tabcontager.codigo');
          SelectSQL.add('left join tabboletobancario on TABPENDENCIA.boletobancario=tabboletobancario.codigo');
          SelectSQL.add('where not (tabpendencia.codigo is null)');
          SelectSQL.add('and (tabpendencia.saldo>0) and');

          if (Pvencimento<>'')
          Then SelectSQL.add('(Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pvencimento))+#39+')and');

          SelectSQL.add('(tabcontager.tipo='+#39+PAGAR_RECEBER+#39+')');



          if (StrCodigos.Count>0)
          Then Begin
                    SelectSQL.add('and (TabTitulo.CodigoCredorDevedor='+Strcodigos[0]);
                    for cont:=1 to StrCodigos.Count-1 do
                    Begin
                         SelectSQL.add('or TabTitulo.CodigoCredorDevedor='+Strcodigos[cont]);
                    End;
                    SelectSQL.add(')');
          End;

          if (PCredorDevedor<>'')
          Then SelectSQL.add('and TabTitulo.CredorDevedor='+PCredorDevedor);

          if (PCodigoBoleto<>'')
          Then SelectSql.add('and Tabpendencia.BoletoBancario='+PCodigoBoleto);

          if (PcontaGer<>'')
          Then SelectSql.add('and TabTitulo.ContaGerencial='+Pcontager);

          if (PSubcontaGer<>'')
          Then SelectSql.add('and TabTitulo.SubContaGerencial='+PSubcontager);
          SelectSql.add('order by  Tabpendencia.Vencimento');
         
          open;


          IF (RecordCount=0)
          Then Exit;
          last;
          PGRID.RowCount:=RecordCount+1;
          first;
          cont:=1;
          While not(eof) do
          Begin
               PGRID.Rows[cont].clear;
               PGRID.cells[0,cont]:=FieldByname('TITULO').asstring;
               PGRID.cells[1,cont]:=FieldByname('HISTORICOTITULO').asstring;
               PGRID.cells[2,cont]:=FieldByname('EMISSAO').asstring;
               PGRID.cells[3,cont]:=FieldByname('CODIGO').asstring;
               PGRID.cells[4,cont]:=FieldByname('HISTORICOPENDENCIA').asstring;
               PGRID.cells[5,cont]:=FieldByname('VENCIMENTO').asstring;
               PGRID.cells[6,cont]:=formata_valor(FieldByname('VALOR').asstring);
               PGRID.cells[7,cont]:=formata_valor(FieldByname('SALDO').asstring);
               PGRID.cells[9,cont]:=FieldByname('numdcto').asstring;

               if (PAGAR_RECEBER='C')
               Then Begin
                        PGRID.cells[10,cont]:=FieldByname('BOLETOBANCARIO').asstring;
                        PGRID.cells[11,cont]:=FieldByname('VENCIMENTOBOLETO').asstring;
               End
               Else Begin
                        PGRID.cells[10,cont]:=FieldByname('BOLETOAPAGAR').asstring;
                        PGRID.cells[11,cont]:='';
               End;

               PGRID.cells[12,cont]:=FieldByname('notafiscal').asstring;
               PGRID.cells[14,cont]:=FieldByname('codigocredordevedor').asstring;
               next;
               inc(cont,1);
               
          End;
         
     End;

finally
       Freeandnil(StrCodigos);
End;

end;

procedure TObjTitulo.ImprimeTitulosPorNumero;
var
saida:boolean;
NumInicial,NumFinal:Integer;
begin

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;

          edtgrupo01.EditMask:='';
          edtgrupo01.OnKeyDown:=Self.EdtcodigoKeyDown;
          edtgrupo01.Color:=$005CADFE;
          LbGrupo01.caption:='N� Inicial';

          edtgrupo02.EditMask:='';
          edtgrupo02.OnKeyDown:=Self.EdtcodigoKeyDown;
          edtgrupo02.Color:=$005CADFE ;
          LbGrupo02.caption:='N� Final';

          showmodal;
          If tag=0
          Then exit;
          edtgrupo01.OnKeyDown:=nil;
          edtgrupo02.OnKeyDown:=nil;

          Try
                NUmInicial:=Strtoint(edtgrupo01.text);
                NUmFinal:=Strtoint(edtgrupo02.text);
          Except
                Messagedlg('N�meros Inv�lidos!',mterror,[mbok],0);
                exit;
          End;
     eND;


     With Self.ObjDataset do
     Begin
          Frelatorio.DesativaCampos;
          Frelatorio.QR.DataSet:=Self.ObjDataset;

          close;
          SelectSQL.clear;
          SelectSql.add('select * FROM TABTITULO');
          SelectSql.add(' where Tabtitulo.codigo>='+inttostr(numinicial)+' and Tabtitulo.codigo<='+inttostr(numfinal));
          SelectSql.add(' order by tabtitulo.codigo');

          open;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
               End;

          With Frelatorio do
          Begin
               titulo.caption:=Self.NumeroRelatorio+'Relat�rio de T�tulos DE '+Inttostr(numinicial) +' a '+inttostr(numfinal);

               ColTitulo01.Enabled:=True;
               ColTitulo01.Caption:='C�DIGO';
               Campo01.Enabled:=True;
               Campo01.DataSet:=Self.objdataset;
               Campo01.DataField:='CODIGO';

               ColTitulo0103.Enabled:=True;
               ColTitulo0103.caption:='HIST�RICO';
               Campo0103.Enabled:=True;
               Campo0103.DataSet:=Self.objdataset;
               Campo0103.DataField:='HISTORICO';
               Campo0103.AutoSize:=false;
               Campo0103.Width:=Campo07.left-1-campo0103.left;

               ColTitulo07.Enabled:=True;
               ColTitulo07.Caption:='EMISS�O';
               Campo07.Enabled:=True;
               Campo07.DataSet:=Self.objdataset;
               Campo07.DataField:='EMISSAO';

               ColTitulo08.Enabled:=True;
               ColTitulo08.Caption:='VALOR';
               Campo08.Enabled:=True;
               Campo08.DataSet:=Self.objdataset;
               Campo08.DataField:='VALOR';
               Campo08.Mask:='0.00';
               Campo08.Alignment:=taRightJustify;
               campo08.left:=campo08.left+15;
               qr.preview;
               //******
               Campo08.Alignment:=taLeftJustify;
               campo08.left:=campo08.left-15;
          End;
     END;
end;


procedure TObjTitulo.ImprimeRelacaoDebitoCreditoFornecedor(PGeradoPeloSistema:string);
var
  linha,CredorDevedorTemp,CodigoCredorDevedorTemp:Integer;
  TCredorDevedor,TCodigoCredorDevedor:Integer;
  QLocal:tibquery;
  Ttipo:string;
  SomaCRedito,SomaDebito:Currency;
  Pdata1,Pdata2:string;
  PnomeCredorDevedor:string;
  pOpcao:string;
begin
     Try
        Qlocal:=Tibquery.create(nil);
        Qlocal.database:=Self.ObjDataset.Database;
     Except
           Messagedlg('Erro na Cria��o da Query para relat�rio!',mterror,[mbok],0);
           exit;
     End;

Try
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo01.Caption:='Data Inicial';
          LbGrupo02.Caption:='Data Final';
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          Grupo07.Enabled:=True;
          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Grupo15.Enabled:=True;
          lbgrupo15.Caption:='Op��es';
          ComboGrupo15.Items.Clear;

          ComboGrupo15.Font.Style:=[fsbold];

          ComboGrupo15.Items.Add('1 - Com saldo');
          ComboGrupo15.Items.Add('2 - Sem saldo');
          ComboGrupo15.Items.Add('');
          ComboGrupo15.ItemIndex:=2;

          
          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;
          edtgrupo07.Color:=$005CADFE;

          showmodal;

          If tag=0
          Then exit;
          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                MessageDlg('Escolha Um Credor/Devedor',mtinformation,[mbok],0);
                exit;
          End;


          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              MessageDlg('Escolha Um Credor/Devedor',mtinformation,[mbok],0);
              exit;
          End;

          PnomeCredorDevedor:=ComboGrupo07.items[ComboGrupo07.itemindex];

          if (Length(pOpcao) > 0) then
            pOpcao:=ComboGrupo15.Text[1];
          
          Try
             StrToDate(edtgrupo01.Text);
             Pdata1:=edtgrupo01.Text;
          Except
             Pdata1:='';
          End;

          Try
             StrToDate(edtgrupo02.Text);
             Pdata2:=edtgrupo02.Text;
          Except
             Pdata2:='';
          End;

     End;


     With Qlocal do
     Begin
          close;
          Sql.clear;
          Sql.add('select tabtitulo.codigo as CODTITULO,tabtitulo.Historico,tabtitulo.contagerencial,tabtitulo.emissao,');
          Sql.add('tabpendencia.codigo CODPENDENCIA,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo,');
          Sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
          Sql.add('tabcontager.tipo');
          Sql.add('from tabpendencia join tabtitulo');
          Sql.add('on tabpendencia.titulo=tabtitulo.codigo');
          sql.add('join tabcontager on');
          sql.add('tabtitulo.contagerencial=tabcontager.codigo');

          if (pOpcao = '1') then
            Sql.add('where Saldo > 0')
          else if (pOpcao = '2') then
            sql.Add('where saldo <= 0');


          If (CredorDevedorTemp<>-1)
          Then SQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));
          If(CodigoCredorDevedorTemp<>-1)
          Then SQL.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));

          if (PGeradoPeloSistema<>'')
          Then sql.add('and TabTitulo.GeradoPeloSistema='+#39+PGeradoPeloSistema+#39);

          if (Pdata1<>'')
          Then Sql.add('and TabPendencia.Vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata1))+#39);

          if (Pdata2<>'')
          Then Sql.add('and TabPendencia.Vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata2))+#39);

          SQL.add(' order by tabtitulo.credordevedor,tabtitulo.codigocredordevedor,tabcontager.Tipo');

          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.rdprint.Abrir;
          if (FreltxtRDPRINT.rdprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.rdprint.Fechar;
                    exit;
          End;
          linha:=3;

          Self.LocalizaCodigo(Fieldbyname('codtitulo').asstring);
          Self.TabelaparaObjeto;

          TCredorDevedor:=Fieldbyname('credordevedor').asinteger;
          TCodigoCredorDevedor:=Fieldbyname('codigocredordevedor').asinteger;
          Ttipo:=Fieldbyname('tipo').asstring;

          SomaCRedito:=0;
          SomaDebito:=0;

          FreltxtRDPRINT.rdprint.Impc(linha,45,Self.NumeroRelatorio+' RELA��O D�BITO/CR�DITO POR '+UpperCase(PnomeCredorDevedor),[negrito]);
          INC(LINHA,2);


          if (PGeradoPeloSistema='N')
          Then Begin
                  FreltxtRDPRINT.rdprint.impf(linha,1,'OBS: OS T�TULOS GERADOS PELO SALDO CR�DITO-D�BITO PELO M�DULO DO SISTEMA N�O CONSTAM NESTA RELA��O',[negrito]);
                  inc(linha,1);
                  FreltxtRDPRINT.rdprint.impf(linha,1,completapalavra('-',90,'-'),[negrito]);
                  inc(linha,1);
          End;
          //titulo das colunas
          FreltxtRDPRINT.rdprint.impf(linha,1,completapalavra('CODTITULO',9,' ')+' '+CompletaPalavra('HIST�RICO T�TULO',40,' ')+' '+CompletaPalavra('EMISS�O',10,' ')+' '+CompletaPalavra('PENDENCIA',9,' ')+' '+CompletaPalavra('VENCIMENTO',10,' ')+' '+CompletaPalavra('SALDO',10,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.rdprint.impf(linha,1,completapalavra('-',90,'-'),[negrito]);
          inc(linha,1);
          //imprimindo o primeiro fornecedor e se � d�bito ou cr�dito
          FreltxtRDPRINT.rdprint.impf(linha,1,Self.CREDORDEVEDOR.Get_Nome+':'+Self.CREDORDEVEDOR.Get_RazaoSocialCredorDevedor(Self.credordevedor.get_codigo,Self.codigocredordevedor),[negrito]);
          inc(linha,2);

          IF fieldbyname('tipo').asstring='C'
          Then FreltxtRDPRINT.rdprint.impC(linha,45,'*************CONTAS A RECEBER*************',[negrito])
          Else FreltxtRDPRINT.rdprint.impC(linha,45,'**************CONTAS A PAGAR**************',[negrito]);
          inc(linha,2);

          While Not(eof) do
          Begin
               Self.LocalizaCodigo(Fieldbyname('codtitulo').asstring);
               Self.TabelaparaObjeto;

               If (TTipo<>fieldbyname('Tipo').asstring)//mudou o tipo
               Then Begin
                        inc(linha,1);
                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                        Ttipo:=fieldbyname('Tipo').asstring;
                        If (Ttipo='C')
                        Then FreltxtRDPRINT.rdprint.impc(linha,45,'*************CONTAS A RECEBER*************',[negrito])
                        Else FreltxtRDPRINT.rdprint.impc(linha,45,'**************CONTAS A PAGAR**************',[negrito]);
                        inc(linha,2);
               End;

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.rdprint.imp(linha,1,completapalavra(fieldbyname('CODTITULO').ASSTRING,9,' ')+' '+CompletaPalavra(fieldbyname('historico').asstring,40,' ')+' '+CompletaPalavra(fieldbyname('emissao').asstring,10,' ')+' '+CompletaPalavra(fieldbyname('CODPENDENCIA').asstring,9,' ')+' '+CompletaPalavra(fieldbyname('VENCIMENTO').asstring,10,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('sALDO').asstring),10,' '));
               inc(linha,1);

               If (fieldbyname('tipo').asstring='C')
               Then Somacredito:=SomaCredito+fieldbyname('Saldo').asfloat
               Else SomaDebito:=SomaDebito+fieldbyname('Saldo').asfloat;
               next;
          End;
          //totalizo os debitos e creditos
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.rdprint.impf(linha,1,completapalavra('-',90,'-'),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.rdprint.impf(linha,1,'Total a Receber: R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somacredito)),12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.rdprint.impf(linha,1,'Total a Pagar  : R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somadebito)),12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.rdprint.impf(linha,1,'Saldo          : R$'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(somacredito-somadebito)),12,' '),[negrito]);
     End;
     FreltxtRDPRINT.rdprint.fechar;
     Qlocal.close;



Finally
       freeandnil(qlocal);
End;
end;


//Este M�dulo Da desconto em todos os t�tulos (a Pagar e a Receber)
//de um determinado portador e gera um t�tulo com o Saldo Receber-Pagar
//A pedido de Lopes&Morgado Loanda(PR)
Procedure TobjTitulo.GeratituloSaldoPagar_receber(Parametro:string);
var
CredorDevedorTemp,CodigoCredorDevedorTemp:Integer;
Contg,TCredorDevedor,TCodigoCredorDevedor:Integer;
QLocal:tibquery;
SomaCRedito,SomaDebito:Currency;
ObjGeraLancamento:TobjGeraLancamento;
GeradorTmp,CodigoTituloSaldo:string;
Objpendencia:TObjPendencia;
begin
     Try
        Qlocal:=Tibquery.create(nil);
        Qlocal.database:=Self.ObjDataset.Database;
        objgeralancamento:=TObjGeraLancamento.create;
        Objpendencia:=TObjPendencia.create;
     Except
           Messagedlg('Erro na Cria��o da Query para relat�rio!',mterror,[mbok],0);
           exit;
     End;

     Try
             //Procurando um Fornecedor
             Limpaedit(Ffiltroimp);
             With FfiltroImp do
             Begin
                  DesativaGrupos;

                  Grupo07.Enabled:=True;
                  LbGrupo07.caption:='Credor/Devedor';
                  ComboGrupo07.Items.clear;
                  Combo2Grupo07.Items.clear;
                  Self.Get_ListacredorDevedor(ComboGrupo07.items);
                  Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
                  edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;

                  showmodal;

                  If tag=0
                  Then exit;
                  Try
                        CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
                  Except
                        MessageDlg('Escolha Um Credor/Devedor',mtinformation,[mbok],0);
                        exit;
                  End;

                  Try
                     CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
                  Except
                      Messagedlg('C�digo do Credor/Devedor Inv�lido',mtinformation,[mbok],0);
                      exit;
                  End;
             End;
             //*************************************

             With Qlocal do
             Begin
                  //Pegando o total de T�tulos a Pagar
                  close;
                  Sql.clear;
                  Sql.add('select sum(Tabpendencia.saldo) as SOMA');
                  Sql.add('from TabPendencia join Tabtitulo on TabPendencia.titulo=Tabtitulo.codigo');
                  Sql.add('join TabContaGer on TabTitulo.ContaGerencial=TabContager.codigo');
                  Sql.add('Where Saldo>0');
                  Sql.add('and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));
                  Sql.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));
                  Sql.add('and tabContaGer.Tipo=''D''');
                  open;
                  SomaDebito:=Fieldbyname('SOMA').asfloat;
                  //Pegando o total de T�tulos a Receber
                  close;
                  Sql.clear;
                  Sql.add('select sum(Tabpendencia.saldo) as SOMA');
                  Sql.add('from TabPendencia join Tabtitulo on TabPendencia.titulo=Tabtitulo.codigo');
                  Sql.add('join TabContaGer on TabTitulo.ContaGerencial=TabContager.codigo');
                  Sql.add('Where Saldo>0');
                  Sql.add('and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));
                  Sql.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));
                  Sql.add('and tabContaGer.Tipo=''C''');
                  open;
                  SomaCredito:=Fieldbyname('SOMA').asfloat;

                  If ((SomaCredito-SomaDebito)=0)
                  Then Begin
                            Messagedlg('O Saldo (Cr�dito-D�bito) est� zerado, n�o � poss�vel gerar um t�tulo!',mtinformation,[mbok],0);
                            exit;
                  End;

                  If (Messagedlg('Valor Total de Contas a Receber: R$ '+formata_valor(floattostr(SomaCredito))+#13+
                                 'Valor Total de Contas a Pagar  : R$ '+formata_valor(floattostr(SomaDebito))+#13+
                                 'O Saldo: R$ '+formata_valor(Floattostr(SomaCredito-SomaDebito))+#13'Deseja Continuar?',mtconfirmation,[mbyes,mbno],0)=Mrno)
                  Then exit;
                  //Gerando o T�tulo, preciso pegar alguns dados do titulo
                  //como emissao, conta gerencial, prazo e portador
                  Limpaedit(Ffiltroimp);

                  Repeat
                        With FfiltroImp do
                        Begin
                             DesativaGrupos;

                             Grupo01.enabled:=True;
                             lbgrupo01.caption:='Emiss�o';
                             edtgrupo01.EditMask:='!99/99/9999;1;_';

                             Grupo02.Enabled:=True;
                             lbgrupo02.caption:='Portador';
                             edtgrupo02.EditMask:='';
                             edtgrupo02.OnKeyDown:=Self.edtportadorKeyDown;

                             Grupo03.Enabled:=true;
                             lbgrupo03.caption:='Conta Ger.';
                             edtgrupo03.editmask:='';
                             edtgrupo03.OnKeyDown:=Self.EdtcontagerencialKeyDown_Unico;

                             Grupo06.Enabled:=True;
                             LbGrupo06.caption:='Prazo PG';
                             ComboGrupo06.Items.clear;
                             Combo2Grupo06.Items.clear;
                             Self.Get_listaPrazoPagamentoNome(combogrupo06.items);
                             Self.Get_listaPrazoPagamentoCodigo(combo2grupo06.items);

                             showmodal;

                             If tag=0
                             Then exit;
                             //verificando os dados digitados e escolhidos
                             Try//emissao
                                Strtodate(edtgrupo01.text);
                             Except
                                   Messagedlg('Data de Emiss�o Inv�lida!',mtinformation,[mbok],0);
                                   tag:=0;
                             End;

                             Try//portador
                                strtoint(edtgrupo02.text);
                                If (Self.portador.localizacodigo(edtgrupo02.text)=False)
                                Then strtoint('a');
                             Except
                                   Messagedlg('Portador Inv�lido!',mtinformation,[mbok],0);
                                   tag:=0;
                             End;

                             Try//conta gerencial
                                strtoint(edtgrupo03.text);
                                
                                If (Self.contagerencial.localizacodigo(edtgrupo03.text)=False)
                                Then strtoint('a');

                                Self.CONTAGERENCIAL.TabelaparaObjeto;
                                //verificando se as Conta gerenciais obedecem ao saldo (a pagar ou a receber)
                                If ((SomaCRedito-SomaDebito)>0)//titulo a receber
                                Then Begin
                                          If (Self.CONTAGERENCIAl.Get_Tipo='D')
                                          Then Begin
                                                    messagedlg('O t�tulo dever� ter uma conta gerencial de cr�dito!',mtinformation,[mbok],0);
                                                    tag:=0;
                                          End;
                                End
                                Else Begin//titulo a pagar
                                          If (Self.CONTAGERENCIAl.Get_Tipo='C')
                                          Then Begin
                                                    messagedlg('O t�tulo dever� ter uma conta gerencial de D�bito!',mtinformation,[mbok],0);
                                                    tag:=0;
                                          End;
                                End;
                             Except
                                   messagedlg('Conta Gerencial Inv�lida',mtinformation,[mbok],0);
                                   Tag:=0;
                             End;

                             //Prazo de Pagamento
                             Try
                                Combo2Grupo06.ItemIndex:=ComboGrupo06.ItemIndex;
                                strtoint(Combo2Grupo06.Text);
                                If(Self.PRAZO.LocalizaCodigo(Combo2Grupo06.Text)=False)
                                Then strtoint('a');
                             Except
                                        messagedlg('Prazo de Pagamento Inv�lido!',mtinformation,[mbok],0);
                                        Tag:=0;
                             End;

                        End;
                  Until(Ffiltroimp.tag=1);

                  If (Self.GERADOR.LocalizaNome('SEM GERADOR')=False)
                  Then Begin
                               messagedlg('O GERADOR "SEM GERADOR" n�o foi encontrado, lan�amentos cancelados',mterror,[mbok],0);
                               exit;
                  End;
                  Self.GERADOR.TabelaparaObjeto;
                  GeradorTmp:=Self.GERADOR.Get_CODIGO;

                  //Agora ja tenho todos os dados necessarios para gerar o titulo

                  Try

                     CodigoTituloSaldo:=Self.Get_NovoCodigo;


                     Self.Status:=Dsinsert;
                     Self.ZerarTabela;

                     Self.Submit_CODIGO(CodigoTituloSaldo);
                     Self.CREDORDEVEDOR.Submit_CODIGO(inttostr(CredorDevedorTemp));
                     Self.CREDORDEVEDOR.LocalizaCodigo(Self.CREDORDEVEDOR.Get_codigo);
                     Self.CREDORDEVEDOR.TabelaparaObjeto;
                     Self.Submit_CODIGOCREDORDEVEDOR(inttostr(CodigoCredorDevedorTemp));
                     Self.Submit_HISTORICO('T�TULO SALDO DOS D�BITOS E CR�DITOS DE '+Self.CREDORDEVEDOR.Get_RazaoSocialCredorDevedor(inttostr(CredorDevedorTemp),inttostr(Codigocredordevedortemp)));
                     Self.Submit_GERADOR(GeradorTmp);
                     Self.Submit_CODIGOGERADOR('0');
                     Self.Submit_EMISSAO(Ffiltroimp.edtgrupo01.text);
                     Self.Submit_PRAZO(Ffiltroimp.Combo2Grupo06.text);
                     Self.Submit_PORTADOR(Ffiltroimp.edtgrupo02.text);

                     if ((SomaCredito-SomaDebito)>0)
                     Then Self.Submit_VALOR(Floattostr(SomaCredito-SomaDebito))
                     Else Self.Submit_VALOR(Floattostr(-1*(SomaCredito-SomaDebito)));//para naum dar negativo

                     Self.Submit_CONTAGERENCIAL(Ffiltroimp.edtgrupo03.text);
                     Self.Submit_NUMDCTO('0');
                     Self.GeradoPeloSistema:='S';

                     If (Self.Salvar(False)=False)
                     Then Begin
                               messagedlg('Erro no lan�amento do T�tulo do Saldo! Lan�amentos Cancelados',mterror,[mbok],0);
                               FDataModulo.IBTransaction.RollbackRetaining;
                               exit;
                     End;
                     //lancando desconto em todos titulos fora o ultimo (que � o saldo)
                     With QLocal do
                     Begin
                          close;
                          SQL.clear;
                          sql.add('Select distinct(tabtitulo.codigo) from tabtitulo ');
                          sql.add('join tabpendencia on tabtitulo.codigo=tabpendencia.titulo');
                          sql.add('where TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));
                          Sql.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));
                          Sql.add('and not (Tabtitulo.codigo='+CodigoTituloSaldo+') and Tabpendencia.saldo>0');
                          open;
                          first;
                          while not(eof) do
                          Begin
                               If (Objpendencia.LancaDescontoTodos(Qlocal.fieldbyname('codigo').asstring,
                                   '',Ffiltroimp.edtgrupo01.text,'DESCONTO DE ACERTO DE SALDO(CR-CP).T�TULO RESULT. N� '+CodigoTituloSaldo)=False)
                               Then Begin
                                         messagedlg('N�o foi poss�vel lan�ar o desconto nos t�tulos!'+#13+'Todo processo ser� cancelado!',mterror,[mbok],0);
                                         FDataModulo.IBTransaction.RollbackRetaining;
                                         exit;
                               End;
                               next;
                          End;
                          Messagedlg('Processo Conclu�do!',mtinformation,[mbok],0);
                          FDataModulo.IBTransaction.CommitRetaining;
                     End;
                  Except
                        messagedlg('Erro no Lan�amento do T�tulo! Lan�amentos Cancelados!',mterror,[mbok],0);
                        FDataModulo.IBTransaction.RollbackRetaining;
                        exit;
                  End;
             End;

     Finally
                Freeandnil(Qlocal);
                Objgeralancamento.free;
                Objpendencia.free;
     End;
End;


procedure TObjTitulo.ImprimeTitulosEmAtraso_NomeCidade(TiPO_DC: string;
  Intervalo: Boolean);
var
saida:boolean;
DataLimite:Tdate;
Linha,Temp,ContaGerencialTemp,GeradorTemp,CredorDevedorTemp:Integer;
TempStr,GeradorTExto:String;
CodigoCredorDevedorTemp:TStringList;
tmpSomaSaldo:Currency;
tmpcliente,tmpcidade,tmptitulo:String;
begin
     
     
     Try
        CodigoCredorDevedorTemp:=TStringList.create;
        CodigoCredorDevedorTemp.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "CodigoCredorDevedorTemp"',mterror,[mbok],0);
           exit;
     End;

Try
     
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=Intervalo;

          If (Intervalo=True)
          Then Begin
                LbGrupo02.caption:='Data 02';
                LbGrupo01.caption:='Data 01';
           End
           Else LbGrupo01.caption:='Limite';

          Grupo05.Enabled:=True;
          Grupo06.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown;
          LbGrupo05.caption:='Conta Gerencial';
          edtgrupo05.Color:=$005CADFE;

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;
          
          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
          edtgrupo07.Color:=$005CADFE;


          lbgrupo06.caption:='Gerador';
          ComboGrupo06.items.clear;
          Combo2Grupo06.items.clear;
          Self.Get_ListaGerador(ComboGrupo06.items);
          Self.Get_ListaGeradorCodigo(Combo2Grupo06.items);

          showmodal;
          If tag=0
          Then exit;


          If (intervalo=True)
          Then Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                       DataLimite:=Strtodate(edtgrupo02.text);
                    Except
                       Messagedlg('Datas Inv�lidas para o Intervalo!',mterror,[mbok],0);
                       exit;
                    End;

          End
          Else Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                    Except
                       Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                       exit;
                    End;
          End;

          Try
                GeradorTemp:=strtoint(Combo2Grupo06.Items[ComboGrupo06.itemindex]);
                GEradorTExto:='';
                If geradortemp=-1
                Then GeradorTemp:=strtoint('  ')
                Else GeradorTExto:=ComboGrupo06.items[ComboGrupo06.itemindex];

          Except
                GeradorTemp:=-1;
                Messagedlg('N�o foi escolhido nenhum Gerador ser� pesquisado por Todos os Geradores',mtinformation,[mbok],0);
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          //pegando os c�digos dos credores/devedores
          TempStr:='';
          CodigoCredorDevedorTemp.clear;
          for Temp:=1 to length(EdtGrupo07.text) do
          Begin
               If ((edtgrupo07.text[temp]=';')
               or (temp=length(EdtGrupo07.text)))
               Then Begin
                         if ((temp=length(EdtGrupo07.text)) and (edtgrupo07.text[temp]<>';'))
                         Then TempStr:=TempStr+edtgrupo07.text[temp];
                         
                         If (Tempstr<>'')
                         Then Begin
                                   Try
                                        Strtoint(tempstr);
                                        CodigoCredorDevedorTemp.add(Tempstr);
                                   Except
                                        CodigoCredorDevedorTemp.clear;
                                        break;
                                   End;
                         End;
                        TempStr:='';
               End
               Else TempStr:=TempStr+edtgrupo07.text[temp];
          End;


          Try
             ContaGerencialTemp:=Strtoint(edtgrupo05.text);
             If (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False)
             Then ContaGerencialTemp:=-1
             Else Begin
                       Self.CONTAGERENCIAL.TabelaparaObjeto;
                       If (Self.CONTAGERENCIAL.Get_Tipo<>TIPO_DC)
                       Then Begin
                                 Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                                 exit;
                            End;
                  End;
          Except
             ContaGerencialTemp:=-1;
          End;
     End;


     With Self.ObjDataset do
     Begin
          Frelatorio.DesativaCampos;
          Frelatorio.QR.DataSet:=Self.ObjDataset;

          close;
          SelectSQL.clear;
          SelectSql.add('select tabtitulo.codigo as CODTITULO,tabtitulo.credordevedor,');
          SelectSql.add('tabtitulo.codigocredordevedor,');
          SelectSql.add('tabpendencia.codigo CODPENDENCIA,');
          SelectSql.add('tabpendencia.vencimento,');
          SelectSql.add('tabpendencia.saldo');
          SelectSql.add('from tabpendencia join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add('where tabpendencia.Saldo>0');

          If (intervalo=false)
          Then SelectSql.add(' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39)
          Else SelectSql.add(' and (Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39+' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39+')');
          SelectSql.add(' and tabcontager.tipo='+#39+TIPO_DC+#39);
          
          
          If (GeradorTemp<>-1)
          Then SelectSql.add(' and Tabtitulo.Gerador='+inttostr(geradortemp));

          If (ContagerencialTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.ContaGerencial='+INttostr(COntaGerencialTemp));

          If (CredorDevedorTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp.count<>0)
          Then Begin
                    SelectSQL.add(' and (Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[0]);
                    for temp:=1 to CodigoCredorDevedorTemp.count-1 do
                    Begin
                         SelectSQL.add(' or Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[temp]);
                    End;
                    SelectSQL.add(' )');
          End;
          open;
          
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.RDprint.Abrir;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S20cpp;

          Linha:=3;
          tmptitulo:='';
          If (TIPO_DC='D')
          Then tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A PAGAR DATA '+DATETOSTR(DataLimite)
          Else tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A RECEBER DATA '+DATETOSTR(DataLimite);

          If GeradorTemp<>-1
          Then tmptitulo:=tmptitulo+' - '+geradortexto;

          FreltxtRDPRINT.RDprint.ImpC(linha,45,tmptitulo,[negrito]);
          inc(linha,2);
                    
          If (ContaGerencialTemp<>-1)
          Then Begin
                    FreltxtRDPRINT.RDprint.Imp(linha,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)));
                    inc(linha,1);
          End;

          If (CredorDevedorTemp<>-1)
          Then Begin
                    If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                    Then Begin
                           Self.CREDORDEVEDOR.TabelaparaObjeto;
                           tmptitulo:=Self.CredorDevedor.Get_nome;
                           inc(linha,1);
                           IF (CodigoCredorDevedorTemp.count=1)
                           Then tmptitulo:=tmptitulo+'='+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),CodigoCredorDevedorTemp[0])
                           Else Begin
                                   If (CodigoCredorDevedorTemp.count>1)
                                   Then tmptitulo:=tmptitulo+'=V�RIOS';
                           End;
                           FreltxtRDPRINT.RDprint.Imp(linha,1,tmptitulo);
                           inc(linha,1);
                    End;
          End;

          inc(linha,1);
          //MARILIA FAZER
          //Titulo     Cliente  Cidade  Pendencia   vencimento Saldo
          
          FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('T�TULO',6,' ')+' '+
                                CompletaPalavra('CLIENTE',29,' ')+' '+
                                CompletaPalavra('CIDADE',19,' ')+' '+
                                CompletaPalavra('PEND.',6,' ')+' '+
                                CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                CompletaPalavra_a_Esquerda('SALDO',12,' '),[negrito]);
          inc(linha,2);
          tmpSomaSaldo:=0;

          While not eof do
          Begin
               TmpCliente:=Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring);
               tmpcidade:=Self.CREDORDEVEDOR.Get_CidadeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring);

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(FIELDBYNAME('CODTITULO').ASSTRING,6,' ')+' '+
                                CompletaPalavra(TMPcliente,29,' ')+' '+
                                CompletaPalavra(TMpCidade,19,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CODpendencia').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('VENCIMENTO').ASSTRING,10,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDO').ASSTRING),12,' '));
               inc(linha,1);
               tmpSomaSaldo:=tmpSomaSaldo+FIELDBYNAME('SALDO').asfloat;
               NEXT;
          end;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL R$ '+formata_valor(FloatToStr(tmpSomaSaldo)),[negrito]);
          FreltxtRDPRINT.rdprint.fechar;
     End;

Finally
       Freeandnil(CodigoCredorDevedorTemp);
End;

end;

procedure TObjTitulo.ImprimeTitulosEmAtraso(TiPO_DC:string;Intervalo:Boolean);
var
saida:boolean;
DataLimite:Tdate;
Linha,Temp,ContaGerencialTemp,GeradorTemp,CredorDevedorTemp:Integer;
TempStr,GeradorTExto:String;
CodigoCredorDevedorTemp:TStringList;
tmpSomaSaldo:Currency;
tmptitulo:String;
begin
     Try
        CodigoCredorDevedorTemp:=TStringList.create;
        CodigoCredorDevedorTemp.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "CodigoCredorDevedorTemp"',mterror,[mbok],0);
           exit;
     End;

Try

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=Intervalo;

          If (Intervalo=True)
          Then Begin
                LbGrupo02.caption:='Data 02';
                LbGrupo01.caption:='Data 01';
           End
           Else LbGrupo01.caption:='Limite';

          Grupo05.Enabled:=True;
          Grupo06.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown;
          edtgrupo05.Color:=$005CADFE;
          LbGrupo05.caption:='Conta Gerencial';

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
          edtgrupo07.Color:=$005CADFE;


          lbgrupo06.caption:='Gerador';
          ComboGrupo06.items.clear;
          Combo2Grupo06.items.clear;
          Self.Get_ListaGerador(ComboGrupo06.items);
          Self.Get_ListaGeradorCodigo(Combo2Grupo06.items);

          showmodal;
          If tag=0
          Then exit;


          If (intervalo=True)
          Then Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                       DataLimite:=Strtodate(edtgrupo02.text);
                    Except
                       Messagedlg('Datas Inv�lidas para o Intervalo!',mterror,[mbok],0);
                       exit;
                    End;

          End
          Else Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                    Except
                       Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                       exit;
                    End;
          End;

          Try
                GeradorTemp:=strtoint(Combo2Grupo06.Items[ComboGrupo06.itemindex]);
                GEradorTExto:='';
                If geradortemp=-1
                Then GeradorTemp:=strtoint('  ')
                Else GeradorTExto:=ComboGrupo06.items[ComboGrupo06.itemindex];

          Except
                GeradorTemp:=-1;
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          //pegando os c�digos dos credores/devedores
          TempStr:='';
          CodigoCredorDevedorTemp.clear;
          for Temp:=1 to length(EdtGrupo07.text) do
          Begin
               If ((edtgrupo07.text[temp]=';')
               or (temp=length(EdtGrupo07.text)))
               Then Begin
                         if ((temp=length(EdtGrupo07.text)) and (edtgrupo07.text[temp]<>';'))
                         Then TempStr:=TempStr+edtgrupo07.text[temp];

                         If (Tempstr<>'')
                         Then Begin
                                   Try
                                        Strtoint(tempstr);
                                        CodigoCredorDevedorTemp.add(Tempstr);
                                   Except
                                        CodigoCredorDevedorTemp.clear;
                                        break;
                                   End;
                         End;
                        TempStr:='';
               End
               Else TempStr:=TempStr+edtgrupo07.text[temp];
          End;


          Try
             ContaGerencialTemp:=Strtoint(edtgrupo05.text);
             If (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False)
             Then ContaGerencialTemp:=-1
             Else Begin
                       Self.CONTAGERENCIAL.TabelaparaObjeto;
                       If (Self.CONTAGERENCIAL.Get_Tipo<>TIPO_DC)
                       Then Begin
                                 Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                                 exit;
                            End;
                  End;
          Except
             ContaGerencialTemp:=-1;
          End;
     End;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('select tabtitulo.codigo as CODTITULO,tabtitulo.NumDcto,tabtitulo.Historico,contagerencial,');
          SelectSql.add('tabpendencia.codigo CODPENDENCIA,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo');
          SelectSql.add('from tabpendencia left join tabtitulo');
          SelectSql.add('on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add(' where Tabpendencia.Saldo>0 ');

          If (intervalo=false)
          Then SelectSql.add(' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39)
          Else SelectSql.add(' and (Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39+' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39+')');

          SelectSql.add(' and tabcontager.tipo='+#39+TIPO_DC+#39);

          If (GeradorTemp<>-1)
          Then SelectSql.add(' and Tabtitulo.Gerador='+inttostr(geradortemp));

          If (ContagerencialTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.ContaGerencial='+INttostr(COntaGerencialTemp));

          If (CredorDevedorTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp.count<>0)
          Then Begin
                    SelectSQL.add(' and (Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[0]);
                    for temp:=1 to CodigoCredorDevedorTemp.count-1 do
                    Begin
                         SelectSQL.add(' or Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[temp]);
                    End;
                    SelectSQL.add(' )');
          End;
          SelectSQL.add('Order by tabpendencia.vencimento');
          //inputbox('','',SelectSQL.Text);
          open;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.RDprint.Abrir;
          FreltxtRDPRINT.ConfiguraImpressao;
          //FreltxtRDPRINT.RDprint.Orientacao:=poLandscape;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S20cpp;

          Linha:=3;
          tmptitulo:='';
          If (TIPO_DC='D')
          Then tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A PAGAR DATA '+DATETOSTR(DataLimite)
          Else tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A RECEBER DATA '+DATETOSTR(DataLimite);

          If GeradorTemp<>-1
          Then tmptitulo:=tmptitulo+' - '+geradortexto;

          FreltxtRDPRINT.RDprint.ImpC(linha,45,tmptitulo,[negrito]);
          inc(linha,2);
                    
          If (ContaGerencialTemp<>-1)
          Then Begin
                    FreltxtRDPRINT.RDprint.Imp(linha,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)));
                    inc(linha,1);
          End;

          If (CredorDevedorTemp<>-1)
          Then Begin
                    If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                    Then Begin
                           Self.CREDORDEVEDOR.TabelaparaObjeto;
                           tmptitulo:=Self.CredorDevedor.Get_nome;
                           inc(linha,1);
                           IF (CodigoCredorDevedorTemp.count=1)
                           Then tmptitulo:=tmptitulo+'='+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),CodigoCredorDevedorTemp[0])
                           Else Begin
                                   If (CodigoCredorDevedorTemp.count>1)
                                   Then tmptitulo:=tmptitulo+'=V�RIOS';
                           End;
                           FreltxtRDPRINT.RDprint.Imp(linha,1,tmptitulo);
                           inc(linha,1);
                    End;
          End;

          inc(linha,1);
          FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('T�TULO',6,' ')+' '+
                                CompletaPalavra('HIST�RICO',35,' ')+' '+
                                CompletaPalavra('N�DCTO',6,' ')+' '+
                                CompletaPalavra('CNTGER',6,' ')+' '+
                                CompletaPalavra('PEND.',6,' ')+' '+
                                CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                CompletaPalavra('SALDO',15,' '),
                                [negrito]);
          inc(linha,2);
          tmpSomaSaldo:=0;

          While not eof do
          Begin
          
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(FIELDBYNAME('CODTITULO').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('HISTORICO').ASSTRING,35,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('NUMDCTO').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CONTAGERENCIAL').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CODPENDENCIA').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('VENCIMENTO').ASSTRING,10,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDO').ASSTRING),15,' '));
               inc(linha,1);
               If (linha>=(FreltxtRDPRINT.RDprint.TamanhoQteLinhas-3))
               Then Begin
                        FreltxtRDPRINT.RDprint.Novapagina;
                        Linha:=3;
               End;
               tmpSomaSaldo:=tmpSomaSaldo+FIELDBYNAME('SALDO').asfloat;
               NEXT;
          end;
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',92,'_'));
          inc(linha,1);
          If (linha>=(FreltxtRDPRINT.RDprint.TamanhoQteLinhas-3))
          Then Begin
                        FreltxtRDPRINT.RDprint.Novapagina;
                        Linha:=3;
          End;
          FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL R$ '+formata_valor(FloatToStr(tmpSomaSaldo)),[negrito]);
          FreltxtRDPRINT.rdprint.fechar;
     End;

Finally
       Freeandnil(CodigoCredorDevedorTemp);
End;

end;


procedure TObjTitulo.ImprimeTitulosEmAtraso_COMFORNECEDOR;
var
saida:boolean;
DataLimite:Tdate;
Linha,Temp,ContaGerencialTemp,GeradorTemp,CredorDevedorTemp:Integer;
TempStr,GeradorTExto:String;
CodigoCredorDevedorTemp:TStringList;
tmpSomaSaldo:Currency;
nomefornecedor,tmptitulo:String;
begin
     Try
        CodigoCredorDevedorTemp:=TStringList.create;
        CodigoCredorDevedorTemp.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "CodigoCredorDevedorTemp"',mterror,[mbok],0);
           exit;
     End;

Try

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo02.caption:='Data 02';
          LbGrupo01.caption:='Data 01';

          Grupo05.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown;
          LbGrupo05.caption:='Conta Gerencial';
          edtgrupo05.color:=$005CADFE;

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;
          
          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
          edtgrupo07.Color:=$005CADFE;


          showmodal;

          If tag=0
          Then exit;


          Try
                 DataLimite:=Strtodate(edtgrupo01.text);
                 DataLimite:=Strtodate(edtgrupo02.text);
          Except
                 Messagedlg('Datas Inv�lidas para o Intervalo!',mterror,[mbok],0);
                 exit;
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          //pegando os c�digos dos credores/devedores
          TempStr:='';
          CodigoCredorDevedorTemp.clear;
          for Temp:=1 to length(EdtGrupo07.text) do
          Begin
               If ((edtgrupo07.text[temp]=';')
               or (temp=length(EdtGrupo07.text)))
               Then Begin
                         if ((temp=length(EdtGrupo07.text)) and (edtgrupo07.text[temp]<>';'))
                         Then TempStr:=TempStr+edtgrupo07.text[temp];
                         
                         If (Tempstr<>'')
                         Then Begin
                                   Try
                                        Strtoint(tempstr);
                                        CodigoCredorDevedorTemp.add(Tempstr);
                                   Except
                                        CodigoCredorDevedorTemp.clear;
                                        break;
                                   End;
                         End;
                        TempStr:='';
               End
               Else TempStr:=TempStr+edtgrupo07.text[temp];
          End;


          Try

             ContaGerencialTemp:=Strtoint(edtgrupo05.text);
             If (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False)
             Then ContaGerencialTemp:=-1
             Else Begin
                       Self.CONTAGERENCIAL.TabelaparaObjeto;
                       If (Self.CONTAGERENCIAL.Get_Tipo<>'D')
                       Then Begin
                                 Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                                 exit;
                            End;
                  End;
          Except
             ContaGerencialTemp:=-1;
          End;
     End;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('select tabtitulo.codigo as CODTITULO,tabtitulo.NumDcto,tabtitulo.Historico,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
          SelectSql.add('tabpendencia.codigo CODPENDENCIA,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo');
          SelectSql.add('from tabpendencia join tabtitulo');
          SelectSql.add('on tabpendencia.titulo=tabtitulo.codigo');
          selectsql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add(' where tabpendencia.Saldo>0 ');

          SelectSql.add(' and (Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39+' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39+')');

          SelectSql.add(' and Tabcontager.TIPO='+#39+'D'+#39);

          If (ContagerencialTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.ContaGerencial='+INttostr(COntaGerencialTemp));

          If (CredorDevedorTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp.count<>0)
          Then Begin
                    SelectSQL.add(' and (Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[0]);
                    for temp:=1 to CodigoCredorDevedorTemp.count-1 do
                    Begin
                         SelectSQL.add(' or Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[temp]);
                    End;
                    SelectSQL.add(' )');
          End;
          SelectSQL.add(' order by tabpendencia.vencimento');

          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;

          FrelTXT.LimpaCampos;
          FrelTXT.SB.Items.clear;
          FrelTXT.LBTITULO.caption:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A PAGAR DATA '+DATETOSTR(DataLimite);


          If (ContaGerencialTemp<>-1)
          Then Begin
                    FrelTXT.LBSUBTITULO1.enabled:=True;
                    FrelTXT.LBSUBTITULO1.Caption:='?Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp));
          End;

          inc(linha,1);
          FrelTXT.SB.Items.clear;
          FrelTXT.SB.Items.add('?'+CompletaPalavra('T�TULO',6,' ')+' '+
                                CompletaPalavra('HIST�RICO',40,' ')+'  '+
                                CompletaPalavra('FORNECEDOR/CLIENTE',40,' ')+' '+
                                CompletaPalavra('N�DCTO',16,' ')+' '+
                                CompletaPalavra('CNTGER',6,' ')+' '+
                                CompletaPalavra('PEND.',6,' ')+' '+
                                CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                CompletaPalavra_a_Esquerda('SALDO',14,' '));
          tmpSomaSaldo:=0;

          While not eof do
          Begin
               nomefornecedor:='';
               nomefornecedor:=Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring);
               FrelTXT.SB.Items.add(CompletaPalavra(FIELDBYNAME('CODTITULO').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('HISTORICO').ASSTRING,40,' ')+'  '+
                                CompletaPalavra(NOMEFORNECEDOR,40,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('NUMDCTO').ASSTRING,16,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CONTAGERENCIAL').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CODPENDENCIA').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('VENCIMENTO').ASSTRING,10,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDO').ASSTRING),14,' '));
               tmpSomaSaldo:=tmpSomaSaldo+FIELDBYNAME('SALDO').asfloat;
               NEXT;
          end;
          FrelTXT.SB.Items.add(CompletaPalavra('_',182,'_'));
          FrelTXT.SB.Items.add('?TOTAL R$ '+formata_valor(FloatToStr(tmpSomaSaldo)));
          FrelTXT.QR.Page.Orientation:=poLandscape;
          FrelTXT.QR.Preview;
          FrelTXT.QR.Page.Orientation:=poPortrait;
     End;

Finally
       Freeandnil(CodigoCredorDevedorTemp);
End;

end;





//Imprime todos os lancamentos efetuados no titulo em parametro
//ou em todos os t�tulos de um determinado portador
procedure TObjTitulo.ImprimeLancamentosTitulo(parametro: string);
var
QLocal,Qlocal2:Tibquery;
Emissao1,Emissao2:string;
ExcetoCredorDevedorTemp,CredorDevedorTemp:integer;
TipoTemp:string;
ObjLancamento:TObjLancamento;
OBJvalorestransferenciaportador:TObjValoresTransferenciaPortador;
ObjTalaodeCheques:tobjtalaodecheques;
Ptipo:string;
ListaChequePortador:TStringList;
tempstr,SomaDinheiroTransf:string;
temp,Cont:Integer;
SomaLancCreditoTotal,SomaLancCredito,SomaLancDebito,SomaLancDebitoTotal:Currency;
ContaGerencialTemp,ExcetoCodigoCredorDevedorTemp,CodigoCredorDevedorTemp:TStringList;

begin

     If (parametro='')
     Then Begin
             With FopcaoRel do
             Begin

                  With RgOpcoes do
                  Begin
                        items.clear;
                        items.add('T�tulos a Pagar');//0
                        items.add('T�tulos a Receber');//1
                        items.add('T�tulos a Pagar e a Receber');//2
                  End;

                  showmodal;
                  If (Tag=0)//indica botao cancel ou fechar
                  Then exit;

                  Case RgOpcoes.ItemIndex of
                  0:PTipo:='D';
                  1:PTipo:='C';
                  2:PTipo:='';
                  End;
             End;
     End;

     Try
        Qlocal:=Tibquery.create(nil);
        Qlocal.database:=Self.Objdataset.Database;
        Qlocal2:=Tibquery.create(nil);
        Qlocal2.database:=Self.Objdataset.Database;
        Objlancamento:=tobjlancamento.create;
        ObjValoresTransferenciaPortador:=TObjValoresTransferenciaPortador.create;
        ListaChequePortador:=TStringList.create;
        ObjTalaodeCheques:=tobjtalaodecheques.create;
        CodigoCredorDevedorTemp:=TStringList.create;
        CodigoCredorDevedorTemp.clear;
        ExcetoCodigoCredorDevedorTemp:=TStringList.create;
        ExcetoCodigoCredorDevedorTemp.clear;
        ContaGerencialTemp:=TStringList.Create;
        ContaGerencialTemp.clear;
     Except
           Messagedlg('Erro na cria��o da Query!',mterror,[mbok],0);
           exit;
     End;

Try
     If (Parametro<>'')//s� um t�tulo
     Then Begin
               If (Self.localizacodigo(parametro)=False)
               Then Begin
                         Messagedlg('O t�tulo n�o foi encontrado!',mtinformation,[mbok],0);
                         exit;
               End;
               
               Qlocal.close;
               Qlocal.sql.clear;
               Qlocal.SQL.add('select PROC.codigo as titulo,PROC.emissao,PROC.historico,PROC.credordevedor,PROC.codigocredordevedor,');
               Qlocal.SQL.add('proc.tipo as tipocg, proc.valor');

               Qlocal.SQL.add('from PROCTITULOS PROC where proc.codigo='+Parametro);
               Qlocal.open;
     End
     Else Begin

               Limpaedit(Ffiltroimp);
               With FfiltroImp do
               Begin
                    DesativaGrupos;

                    Grupo01.enabled:=True;
                    lbgrupo01.caption:='Emiss�o(inicial)';
                    edtgrupo01.EditMask:='!99/99/9999;1;_';

                    Grupo02.enabled:=True;
                    lbgrupo02.caption:='Emiss�o(final)';
                    edtgrupo02.EditMask:='!99/99/9999;1;_';

                    Grupo03.enabled:=True;
                    lbgrupo03.caption:='Conta Ger.';
                    edtgrupo03.EditMask:='';
                    edtgrupo03.OnKeyDown:=Self.EdtcontagerencialKeyDown;
                    edtgrupo03.Color:=$005CADFE;

                    //Fornecedor
                    Grupo07.Enabled:=True;
                    LbGrupo07.caption:='Credor/Devedor';
                    ComboGrupo07.Items.clear;
                    Combo2Grupo07.Items.clear;
                    Self.Get_ListacredorDevedor(ComboGrupo07.items);
                    Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
                    edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
                    edtgrupo07.Color:=$005CADFE;

                    //Fornecedor
                    Grupo13.Enabled:=True;
                    LbGrupo13.caption:='Exceto CR/DV';
                    ComboGrupo13.Items.clear;
                    Combo2Grupo13.Items.clear;
                    Self.Get_ListacredorDevedor(ComboGrupo13.items);
                    Self.Get_ListacredorDevedorcodigo(Combo2Grupo13.items);
                    edtgrupo13.onkeydown:=Self.edtexcetocodigocredordevedorKeyDown;
                    edtgrupo13.Color:=$005CADFE;

                    showmodal;

                    If tag=0
                    Then exit;

                    Try
                       strtodate(edtgrupo01.text);
                       emissao1:=edtgrupo01.text;
                    Except
                          emissao1:='';
                    End;

                    Try
                       strtodate(edtgrupo02.text);
                       emissao2:=edtgrupo02.text;
                    Except
                          emissao2:='';
                    End;


                    Try
                          CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
                          //pegando os c�digos dos credores/devedores
                          TempStr:='';
                          CodigoCredorDevedorTemp.clear;
                          for Temp:=1 to length(EdtGrupo07.text) do
                          Begin
                                If ((edtgrupo07.text[temp]=';')
                                or (temp=length(EdtGrupo07.text)))
                                Then Begin
                                        if ((temp=length(EdtGrupo07.text)) and (edtgrupo07.text[temp]<>';'))
                                        Then TempStr:=TempStr+edtgrupo07.text[temp];

                                        If (Tempstr<>'')
                                        Then Begin
                                                Try
                                                        Strtoint(tempstr);
                                                        CodigoCredorDevedorTemp.add(Tempstr);
                                                Except
                                                        CodigoCredorDevedorTemp.clear;
                                                       break;
                                                End;
                                        End;
                                        TempStr:='';
                                End
                                Else TempStr:=TempStr+edtgrupo07.text[temp];
                          End;
                    Except
                          CredorDevedorTemp:=-1;
                          CodigoCredorDevedorTemp.clear;
                    End;

                    Try
                          TempStr:='';
                          ContaGerencialTemp.clear;
                          for Temp:=1 to length(EdtGrupo03.text) do
                          Begin
                                If ((edtgrupo03.text[temp]=';')
                                or (temp=length(EdtGrupo03.text)))
                                Then Begin
                                        if ((temp=length(EdtGrupo03.text)) and (edtgrupo03.text[temp]<>';'))
                                        Then TempStr:=TempStr+edtgrupo03.text[temp];

                                        If (Tempstr<>'')
                                        Then Begin
                                                Try
                                                        Strtoint(tempstr);
                                                        ContaGerencialTemp.add(Tempstr);
                                                Except
                                                        ContaGerencialTemp.clear;
                                                        Break;
                                                End;
                                        End;
                                        TempStr:='';
                                End
                                Else TempStr:=TempStr+edtgrupo03.text[temp];
                          End;
                    Except
                          ContaGerencialTemp.clear;
                    End;


                    Try
                          ExcetoCredorDevedorTemp:=strtoint(Combo2Grupo13.Items[ComboGrupo13.itemindex]);
                          //pegando os c�digos dos credores/devedores
                          TempStr:='';
                          ExcetoCodigoCredorDevedorTemp.clear;
                          for Temp:=1 to length(EdtGrupo13.text) do
                          Begin
                                If ((edtgrupo13.text[temp]=';')
                                or (temp=length(EdtGrupo13.text)))
                                Then Begin
                                        if ((temp=length(EdtGrupo13.text)) and (edtgrupo13.text[temp]<>';'))
                                        Then TempStr:=TempStr+edtgrupo13.text[temp];

                                        If (Tempstr<>'')
                                        Then Begin
                                                Try
                                                        Strtoint(tempstr);
                                                        ExcetoCodigoCredorDevedorTemp.add(Tempstr);
                                                Except
                                                        ExcetoCodigoCredorDevedorTemp.clear;
                                                       break;
                                                End;
                                        End;
                                        TempStr:='';
                                End
                                Else TempStr:=TempStr+edtgrupo13.text[temp];
                          End;
                    Except
                          ExcetoCredorDevedorTemp:=-1;
                          ExcetoCodigoCredorDevedorTemp.clear;
                    End;

               End;//with
               With Qlocal do
               Begin

                      close;
                      sql.clear;
                      SQL.add('select PROC.codigo as titulo,PROC.emissao,PROC.historico,PROC.credordevedor,PROC.codigocredordevedor,');
                      SQL.add('proc.tipo as tipocg, proc.valor');


                      SQL.add('from PROCTITULOS PROC where 1=1');
                      sql.add('and proc.saldopendencia<>proc.valor');//assim eu garanto que teve algum lancamento

                      if (emissao1<>'')
                      Then sql.add('and PROC.emissao>'+#39+formatdatetime('mm/dd/yyyy',strtodate(emissao1))+#39);

                      if (emissao2<>'')
                      then SQL.add('and Proc.emissao<'+#39+formatdatetime('mm/dd/yyyy',strtodate(emissao2))+#39);

                      if ptipo<>''
                      then sql.add('and proc.tipo='+#39+Ptipo+#39);


                      //**********************************************************************
                        If(ContaGerencialTemp.count<>0)
                        Then Begin
                                    SQL.add(' and (PROC.ContaGerencial='+ContaGerencialTemp[0]);
                                    for temp:=1 to ContaGerencialTemp.count-1 do
                                    Begin
                                            SQL.add(' or PROC.ContaGerencial='+ContaGerencialTemp[temp]);
                                    End;
                                    SQL.add(' )');
                        End;
                        //**********************************************************************

                      If (CREDORDEVEDORtemp<>-1)
                      Then SQL.add(' and PROC.CredorDevedor='+INttostr(CredorDevedorTemp));

                      If(CodigoCredorDevedorTemp.count<>0)
                      Then Begin
                                  SQL.add(' and (PROC.CodigoCredorDevedor='+CodigoCredorDevedorTemp[0]);
                                  for temp:=1 to CodigoCredorDevedorTemp.count-1 do
                                  Begin
                                          SQL.add(' or PROC.CodigoCredorDevedor='+CodigoCredorDevedorTemp[temp]);
                                  End;
                                  SQL.add(' )');
                      End;

                      
                    if (excetoCREDORDEVEDORtemp<>-1)
                    Then Begin
                                sql.add('and not ( proc.CredorDevedor='+Inttostr(excetocredordevedortemp));
                                If(ExcetoCodigoCredorDevedorTemp.count<>0)
                                Then Begin
                                        SQL.add(' and (proc.CodigoCredorDevedor='+ExcetoCodigoCredorDevedorTemp[0]);
                                        for temp:=1 to excetoCodigoCredorDevedorTemp.count-1 do
                                        Begin
                                                SQL.add(' or proc.CodigoCredorDevedor='+ExcetoCodigoCredorDevedorTemp[temp]);
                                        End;
                                        SQL.add(' )');
                                End;

                                SQL.add(' )');
                    End;


                      sql.add('order by PROC.CredorDevedor,PROC.CodigoCredorDevedor, proc.tipo');
                      
                    open;
                    //showmessage(sql.text);
               End;//with qlocal
     End;//else

     With Qlocal do
     Begin
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum dado foi selecionado na pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          Last;
          InicializaBarradeProgressoRelatorio(Recordcount,'Gerando Relat�rio, Aguarde...');
          first;
          FrelTXT.sb.Items.clear;

          CredorDevedorTemp       :=fieldbyname('credordevedor').asinteger;
          Temp :=fieldbyname('codigocredordevedor').asinteger;
          TipoTemp                :=Fieldbyname('TIPOCG').asstring;

          //Imprimindo o primeiro fornecedor e se � CG ou CP
          FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+UpperCase(Self.credordevedor.Get_RazaoSocialCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring)));
          IF(fieldbyname('tipocg').asstring='C')
          Then FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'CONTAS A RECEBER')
          Else FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'CONTAS A PAGAR');

          SomaLancCredito:=0;//sao usados para totalizar na troca de fornecedor,o total de lanc. de credito
          SomaLancDebito:=0;//sao usados para totalizar na troca de fornecedor,o total de lanc. de debito
          SomaLancCreditoTotal:=0;
          SomaLancDebitoTotal:=0;

          While not(eof) do//esse while � dos t�tulos
          Begin
               //Verif. se mudou o cadastro do credordevedor
//               preciso totalizar os lancamentos

               If (CredorDevedorTemp<>fieldbyname('credordevedor').asinteger)
               or (Temp<>fieldbyname('codigocredordevedor').asinteger)
               Then Begin
                         //Totalizando os Lancamentos de Credito e Debito
                         FrelTXT.sb.Items.add(CompletaPalavra('',5,' ')+'TOTAL DE LAN�AMENTOS DE CR�DITO R$ '+formata_valor(floattostr(SomaLancCredito)));
                         FrelTXT.sb.Items.add(CompletaPalavra('',5,' ')+'TOTAL DE LAN�AMENTOS DE D�BITO R$ '+formata_valor(floattostr(SomaLancDebito)));
                         SomaLancCredito:=0;
                         SomaLancDebito:=0;
                         //************************************************
                         CredorDevedorTemp       :=fieldbyname('credordevedor').asinteger;
                         Temp :=fieldbyname('codigocredordevedor').asinteger;
                         FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+completapalavra('',83,'*'));
                         FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+UpperCase(Self.credordevedor.Get_RazaoSocialCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring)));
                         TipoTemp                :=Fieldbyname('TIPOCG').asstring;
                         IF(fieldbyname('tipocg').asstring='C')
                         Then FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'CONTAS A RECEBER')
                         Else FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'CONTAS A PAGAR');
               End
               Else Begin//VERF. mudou o tipo de receber para pagar
                         If (TipoTemp<>Fieldbyname('TIPOCG').asstring)
                         Then Begin
                                   //Totalizando os Lancamentos de Credito e Debito
                                   FrelTXT.sb.Items.add(CompletaPalavra('',5,' ')+'TOTAL DE LAN�AMENTOS DE CR�DITO R$ '+formata_valor(floattostr(SomaLancCredito)));
                                   FrelTXT.sb.Items.add(CompletaPalavra('',5,' ')+'TOTAL DE LAN�AMENTOS DE D�BITO R$ '+formata_valor(floattostr(SomaLancDebito)));
                                   SomaLancCredito:=0;
                                   SomaLancDebito:=0;
                                   //************************************************
                                   TipoTemp                :=Fieldbyname('TIPOCG').asstring;
                                   IF(fieldbyname('tipocg').asstring='C')
                                   Then FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'CONTAS A RECEBER')
                                   Else FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'CONTAS A PAGAR');
                         End;
               End;
               //a cada la�o dos t�tulos eu pesquiso os la�amentos que foram dados

               Qlocal2.close;
               Qlocal2.sql.clear;
               Qlocal2.sql.add('Select Tablancamento.*,tabtipolancto.Nome from Tablancamento join tabpendencia');
               Qlocal2.sql.add('on tablancamento.pendencia=tabpendencia.codigo');
               Qlocal2.sql.add('join TabTipoLancto on Tablancamento.Tipolancto=Tabtipolancto.codigo');
               Qlocal2.sql.add('where tabpendencia.titulo='+fieldbyname('titulo').asstring);
               Qlocal2.open;

               If (Qlocal2.recordcount>0)
               Then Begin//s� imprimo se tiver lancamentos
                         //imprimindo os dados dos t�tulos
                        FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+CompletaPalavra(Fieldbyname('titulo').asstring,9,' ')+' '+CompletaPalavra(Fieldbyname('historico').asstring,50,' ')+' '+completapalavra(Fieldbyname('emissao').asstring,10,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring),12,' '));
                        //imprimindo o cabecalho dos lanctos
                        FrelTXT.sb.Items.add('?'+completapalavra('',15,' ')+completapalavra('CODLANC',9,' ')+' '+completapalavra('CODPEND',9,' ')+' '+completapalavra('DATA LANCT',10,' ')+' '+completapalavra('TIPO LANCTO',20,' ')+' '+completapalavra('HIST�RICO',50,' ')+' '+CompletaPalavra_a_Esquerda('VALOR',12,' ')+'     '+completapalavra('USUARIO',10,' '));
                        //percorro todos os lancamentos
                        Qlocal2.first;
                        While not(qlocal2.eof) do
                        Begin

                             FrelTXT.sb.Items.add(completapalavra('',15,' ')+completapalavra(qlocal2.fieldbyname('codigo').asstring,9,' ')+' '+completapalavra(qlocal2.fieldbyname('pendencia').asstring,9,' ')+' '+completapalavra(qlocal2.fieldbyname('data').asstring,10,' ')+' '+completapalavra(qlocal2.fieldbyname('TipoLancto').asstring+'-'+qlocal2.fieldbyname('Nome').asstring,20,' ')+' '+completapalavra(qlocal2.fieldbyname('historico').asstring,50,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(Qlocal2.fieldbyname('Valor').asstring),12,' ')+'     '+CompletaPalavra(Qlocal2.Fieldbyname('userc').asstring,15,' '));
                             //SE FOR CP, e O LANCTO GERA VALOR
                             //PEGAR COMO FOI FEITO O PAGAMENTO
                             ObjLancamento.LocalizaCodigo(Qlocal2.fieldbyname('codigo').asstring);
                             ObjLancamento.TabelaparaObjeto;

                             If (Objlancamento.TipoLancto.Get_Tipo='D')
                             Then Begin
                                        SomaLancDebito:=SomaLancDebito+strtofloat(Objlancamento.Get_Valor);
                                        SomaLancDebitoTotal:=SomaLancDebitoTotal+strtofloat(Objlancamento.Get_Valor);
                             End
                             Else Begin
                                        SomaLancCredito:=SomaLancCredito+strtofloat(Objlancamento.Get_Valor);
                                        SomaLancCreditoTotal:=SomaLancCreditoTotal+strtofloat(Objlancamento.Get_Valor);
                             End;


                             If (Qlocal.fieldbyname('TIPOCG').asstring='D')//contas a pagar)
                             Then Begin
                                     If (ObjLancamento.TipoLancto.Get_GeraValor='S')
                                     Then Begin//pegar como foi pago
                                               SomaDinheiroTransf:='';
                                               ListaChequePortador.clear;
                                               //pegando quanto foi com dinheiro
                                               ObjValoresTransferenciaPortador.TransferenciaPortador.PegaSomaValorDinheiro_lancamento(Qlocal2.fieldbyname('codigo').asstring,SomaDinheiroTransf);
                                               //pegando os codigos dos cheques usados
                                               ObjValoresTransferenciaPortador.Retorna_Cheques(Qlocal2.fieldbyname('codigo').asstring,ListaChequePortador);

                                               If (SomaDinheiroTransf<>'')//Teve pagamento em dinheiro
                                               Then FrelTXT.sb.Items.add('?'+completapalavra('',30,' ')+'Pagamento em Dinheiro: '+formata_valor(SomaDinheiroTransf));

                                               If (ListaChequePortador.Count>0)
                                               Then Begin
                                                         FrelTXT.sb.Items.add('?'+completapalavra('',30,' ')+'Pagamento em Cheques ');
                                                         for cont:=0 to ListaChequePortador.Count-1 do
                                                         Begin
                                                                 OBJvalorestransferenciaportador.Valores.LocalizaCodigo(ListaChequePortador[cont]);
                                                                 OBJvalorestransferenciaportador.Valores.TabelaparaObjeto;
                                                                 If (OBJvalorestransferenciaportador.Valores.Get_Chequedoportador='S')//cheque do portador
                                                                 Then Begin//localizo pelo talao de cheques para pegar  o nome do portador e por na linha impressa
                                                                          If (ObjTalaodeCheques.LocalizaChequePortador(OBJvalorestransferenciaportador.Valores.Get_CODIGO)=False)
                                                                          Then Begin
                                                                                    Messagedlg('O Objeto de Tal�o de Cheques n�o localizou o registro na ChequePortador!',mterror,[mbok],0);
                                                                                    ObjTalaodeCheques.ZerarTabela;
                                                                          End
                                                                          Else ObjTalaodeCheques.TabelaparaObjeto;

                                                                          FrelTXT.sb.Items.add(completapalavra('',50,' ')+'N�CH '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_NumCheque,9,' ')+' N�CC: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Conta,9,' ')+' Banco: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Banco,9,' ')+' Valor R$'+CompletaPalavra(FORMATA_VALOR(OBJvalorestransferenciaportador.valores.Get_Valor),9,' ')+' - CHEQUE DO PORTADOR '+ObjTalaodeCheques.Portador.Get_Nome);
                                                                 End
                                                                 Else FrelTXT.sb.Items.add(completapalavra('',50,' ')+'N�CH '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_NumCheque,9,' ')+' N�CC: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Conta,9,' ')+' Banco: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Banco,9,' ')+' Valor R$'+CompletaPalavra(FORMATA_VALOR(OBJvalorestransferenciaportador.valores.Get_Valor),9,' '));
                                                         End;
                                               End;
                                     End//then
                                     Else Begin//lancamento que naum geram valor
                                               If (ObjLancamento.Get_lancamentoPai<>'')//pagamento em lote
                                               Then Begin
                                                         SomaDinheiroTransf:='';
                                                         ListaChequePortador.clear;
                                                         //pegando quanto foi com dinheiro
                                                         ObjValoresTransferenciaPortador.TransferenciaPortador.PegaSomaValorDinheiro_lancamento(ObjLancamento.Get_lancamentoPai,SomaDinheiroTransf);
                                                        //pegando os codigos dos cheques usados
                                                        ObjValoresTransferenciaPortador.Retorna_Cheques(ObjLancamento.Get_lancamentoPai,ListaChequePortador);

                                                        If (SomaDinheiroTransf<>'')//Teve pagamento em dinheiro
                                                        Then FrelTXT.sb.Items.add('?'+completapalavra('',30,' ')+'Pagamento em Lote Dinheiro: '+formata_valor(SomaDinheiroTransf));

                                                        If (ListaChequePortador.Count>0)
                                                        Then Begin
                                                                FrelTXT.sb.Items.add('?'+completapalavra('',30,' ')+'Pagamento em Lotes Cheques ');
                                                                for cont:=0 to ListaChequePortador.Count-1 do
                                                                Begin
                                                                        OBJvalorestransferenciaportador.Valores.LocalizaCodigo(ListaChequePortador[cont]);
                                                                        OBJvalorestransferenciaportador.Valores.TabelaparaObjeto;
                                                                        If (OBJvalorestransferenciaportador.Valores.Get_Chequedoportador='S')//cheque do portador
                                                                        Then Begin//localizo pelo talao de cheques para pegar  o nome do portador e por na linha impressa
                                                                                  If (ObjTalaodeCheques.LocalizaChequePortador(OBJvalorestransferenciaportador.Valores.Get_CODIGO)=False)
                                                                                  Then Begin
                                                                                          Messagedlg('O Objeto de Tal�o de Cheques n�o localizou o registro na ChequePortador!',mterror,[mbok],0);
                                                                                        ObjTalaodeCheques.ZerarTabela;
                                                                                  End
                                                                                  Else ObjTalaodeCheques.TabelaparaObjeto;

                                                                                  FrelTXT.sb.Items.add(completapalavra('',50,' ')+'N�CH '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_NumCheque,9,' ')+' N�CC: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Conta,9,' ')+' Banco: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Banco,9,' ')+' Valor R$'+CompletaPalavra(FORMATA_VALOR(OBJvalorestransferenciaportador.valores.Get_Valor),9,' ')+' - CHEQUE DO PORTADOR '+ObjTalaodeCheques.Portador.Get_Nome);
                                                                        End
                                                                        Else FrelTXT.sb.Items.add(completapalavra('',50,' ')+'N�CH '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_NumCheque,9,' ')+' N�CC: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Conta,9,' ')+' Banco: '+CompletaPalavra(OBJvalorestransferenciaportador.valores.Get_Banco,9,' ')+' Valor R$'+CompletaPalavra(FORMATA_VALOR(OBJvalorestransferenciaportador.valores.Get_Valor),9,' '));
                                                                End;
                                                        End;
                                               End;//then
                                     End;
                             End;
                             //******************************************
                             Qlocal2.next;
                        End;
               End;
               IncrementaBarraProgressoRelatorio;
               next;
          End;//while
          //Totalizando os Lancamentos de Credito e Debito
          FrelTXT.sb.Items.add(CompletaPalavra('',5,' ')+'TOTAL DE LAN�AMENTOS DE CR�DITO R$ '+formata_valor(floattostr(SomaLancCredito)));
          FrelTXT.sb.Items.add(CompletaPalavra('',5,' ')+'TOTAL DE LAN�AMENTOS DE D�BITO R$ '+formata_valor(floattostr(SomaLancDebito)));
          SomaLancCredito:=0;
          SomaLancDebito:=0;
          //************************************************
          FrelTXT.sb.Items.add('?------------------------------------------------');
          FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'TOTAL GERAL DE LAN�AMENTOS DE CR�DITO R$ '+formata_valor(floattostr(SomaLancCreditoTotal)));
          FrelTXT.sb.Items.add('?'+CompletaPalavra('',5,' ')+'TOTAL GERAL DE LAN�AMENTOS DE D�BITO R$ '+formata_valor(floattostr(SomaLancDebitoTotal)));
     End;
     FechaBarraProgressoRelatorio;

     With Freltxt do
     Begin
          LimpaCampos;
          lbtitulo.Enabled:=true;
          lbtitulo.caption:=Self.NumeroRelatorio+'LAN�AMENTOS EFETUADOS EM T�TULOS';
          qr.Page.Orientation:=poLandscape;
          qr.preview;
     End;            

Finally
       Freeandnil(qlocal);
       Freeandnil(Qlocal2);
       Freeandnil(CodigoCredorDevedorTemp);
       Freeandnil(ExcetoCodigoCredorDevedorTemp);
       Objlancamento.free;
       ObjValoresTransferenciaPortador.free;
       Freeandnil(ListaChequePortador);
       ObjTalaodeCheques.free;
       ContaGerencialTemp.free;
End;

end;

procedure TObjTitulo.ImprimeResumoContaGerencialLancamentos;
var
saida:boolean;
DataLimite1,DataLimite2:Tdate;
TipoTemp:string;
SomaCredito,SomaDebito:Currency;
TipoLanctoTemp:TStringList;
temp:integer;
tempstr:string;
begin



     Try
        TipoLanctoTemp:=TStringList.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o da StringList "TipoLanctoTemp"',mterror,[mbok],0);
           exit;
     End;
Try
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo03.Enabled:=True;

          LbGrupo01.caption:='Data 01';
          LbGrupo02.caption:='Data 02';
          LbGrupo03.caption:='Tipo Lancto';

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          edtgrupo03.EditMask:='';
          edtgrupo03.OnKeyDown:=EdtTipoLanctoKeyDown;
          edtgrupo03.Color:=$005CADFE;

          showmodal;

          If tag=0
          Then exit;


          Try
             DataLimite1:=Strtodate(edtgrupo01.text);
             DataLimite2:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Datas Inv�lidas para o Intervalo!',mterror,[mbok],0);
                exit;
          End;

          Try
             TempStr:='';
             TipoLanctoTemp.clear;
             for Temp:=1 to length(EdtGrupo03.text) do
             Begin
                   If ((edtgrupo03.text[temp]=';')
                   or (temp=length(EdtGrupo03.text)))
                   Then Begin
                           if ((temp=length(EdtGrupo03.text)) and (edtgrupo03.text[temp]<>';'))
                           Then TempStr:=TempStr+edtgrupo03.text[temp];

                           If (Tempstr<>'')
                           Then Begin
                                   Try
                                           Strtoint(tempstr);
                                           TipoLanctoTemp.add(Tempstr);
                                   Except
                                           TipoLanctoTemp.clear;
                                           break;
                                   End;
                           End;
                           TempStr:='';
                   End
                   Else TempStr:=TempStr+edtgrupo03.text[temp];
             End;
          Except
                TipoLanctoTemp.clear;
          End;
     End;

     IF (TipoLanctoTemp.count=0)
     Then Begin
               Messagedlg('� necess�rio escolher um Tipo de Lan�amento!',mtinformation,[mbok],0);
               exit;
     End;

     With Self.ObjDataset do
     Begin
          Frelatorio.DesativaCampos;
          Frelatorio.QR.DataSet:=Self.ObjDataset;

          close;
          SelectSQL.clear;
          SelectSql.add('select tabcontager.nome,tabtitulo.contagerencial,sum(tablancamento.valor) as SOMA,');
          SelectSql.add('tabcontager.tipo');
          SelectSql.add('from tablancamento');
          SelectSql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
          SelectSql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add(' where (Tablancamento.data>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39+' and Tablancamento.data<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39+')');
          //**********************************************************************
          If(TipoLanctoTemp.count<>0)
          Then Begin
                      SelectSql.add(' and (Tablancamento.tipolancto='+TipoLanctoTemp[0]);
                      for temp:=1 to TipoLanctoTemp.count-1 do
                      Begin
                              SelectSql.add(' or Tablancamento.tipolancto='+TipoLanctoTemp[temp]);
                      End;
                      SelectSql.add(' )');
          End;
          //**********************************************************************

          SelectSql.add('group by tabtitulo.contagerencial,tabcontager.nome,tabcontager.tipo');
          SelectSql.add('order by tabcontager.tipo,tabcontager.nome');
          open;
          //showmessage(selectsql.text);
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
               End;
           first;

           FrelTXT.sb.Items.clear;

           //Imprimindo o primeiro fornecedor e se � CG ou CP
           FrelTXT.sb.Items.add('?'+CompletaPalavra('CONTA GERENCIAL',50,' ')+CompletaPalavra_a_Esquerda('VALOR',15,' '));
           If (FieldByName('Tipo').asstring='C')
           Then FrelTXT.sb.Items.add('?*******************CR�DITOS*****************')
           Else FrelTXT.sb.Items.add('?********************D�BITOS*****************');

           tipoTemp:=FieldByName('Tipo').asstring;
           SomaCredito:=0;
           SomaDebito:=0;
           While not(eof) do
           Begin
                if (tipotemp<>FieldByName('Tipo').asstring)
                Then Begin
                          If (tipotemp='C')
                          Then FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaCredito)))
                          Else FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaDebito)));

                          If (FieldByName('Tipo').asstring='C')
                          Then FrelTXT.sb.Items.add('?*******************CR�DITOS*****************')
                          Else FrelTXT.sb.Items.add('?********************D�BITOS*****************');
                          tipoTemp:=FieldByName('Tipo').asstring;
                End;
                If (FieldByName('Tipo').asstring='C')
                Then SomaCredito:=SomaCredito+FieldByName('SOMA').ASFLOAT
                Else SomaDebito:=SomaDebito+FieldByName('SOMA').ASFLOAT;

                FrelTXT.sb.Items.add(CompletaPalavra(FieldByName('CONTAGERENCIAL').asstring+'-'+FieldByName('NOME').asstring,50,' ')+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('soma').asstring),15,' '));

                next;
           End;

           If (tipotemp='C')
           Then FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaCredito)))
           Else FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaDebito)));

           FrelTXT.sb.Items.add('?'+CompletaPalavra('',65,'-'));
           FrelTXT.sb.Items.add('?RESULTADO');
           FrelTXT.sb.Items.add('?CR�DITOS  R$ '+formata_valor(floattostr(SomaCredito)));
           FrelTXT.sb.Items.add('?D�BITOS   R$ '+formata_valor(floattostr(SomaDebito)));
           FrelTXT.sb.Items.add('?RESULTADO R$ '+formata_valor(floattostr(SomaCredito-SomaDebito)));
     End;

     With Freltxt do
     Begin
          LimpaCampos;
          lbtitulo.Enabled:=true;
          lbtitulo.caption:='RESUMO POR CONTA GERENCIAL - PER�ODO DE '+datetostr(DataLimite1)+' A '+datetostr(DataLimite2);

          LBSUBTITULO1.caption:='Tipos de Lan�amentos: ';
                 
          for temp:=0 to TipoLanctoTemp.Count-1 do
                LBSUBTITULO1.caption:=LBSUBTITULO1.caption+'/'+TipoLanctoTemp[temp];

          qr.Page.Orientation:=poportrait;
          qr.preview;        

     End;
Finally
       freeandnil(TipoLanctoTemp);
End;

end;

procedure TObjTitulo.EdtTipoLanctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   ObjLancamento:TobjLancamento;
begin

     ObjLancamento:=TobjLancamento.create;
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjLancamento.TipoLancto.Get_Pesquisa,ObjLancamento.TipoLancto.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(Sender).text:=Tedit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           ObjLancamento.free;
     End;


end;

Procedure TObjTitulo.ImprimeResumoContaGerencialTitulos;
var
saida:boolean;
DataLimite1,DataLimite2:Tdate;
TipoTemp:string;
SomaCredito,SomaDebito:Currency;
temp:integer;
tempstr:string;
begin

Try
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo01.caption:='Data 01';
          LbGrupo02.caption:='Data 02';
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          edtgrupo03.EditMask:='';
          edtgrupo03.OnKeyDown:=EdtTipoLanctoKeyDown;
          edtgrupo03.Color:=$005CADFE;

          showmodal;

          If tag=0
          Then exit;


          Try
             DataLimite1:=Strtodate(edtgrupo01.text);
             DataLimite2:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Datas Inv�lidas para o Intervalo!',mterror,[mbok],0);
                exit;
          End;

     End;


     With Self.ObjDataset do
     Begin
          Frelatorio.DesativaCampos;
          Frelatorio.QR.DataSet:=Self.ObjDataset;

          close;
          SelectSQL.clear;
          SelectSql.add('select tabcontager.nome,tabtitulo.contagerencial,sum(Tabtitulo.valor) as SOMA,');
          SelectSql.add('tabcontager.tipo');
          SelectSql.add('from TabTitulo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add('where (TabTitulo.emissao>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39+' and TabTitulo.emissao<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39+')');
          SelectSQL.add('and Tabtitulo.GeradopeloSistema=''N'' ');
          SelectSql.add('group by tabtitulo.contagerencial,tabcontager.nome,tabcontager.tipo');
          SelectSql.add('order by tabcontager.tipo,tabcontager.nome');
          open;
          
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
               End;
           first;

           FrelTXT.sb.Items.clear;

           //Imprimindo o primeiro fornecedor e se � CG ou CP
           FrelTXT.sb.Items.add('?'+CompletaPalavra('CONTA GERENCIAL',50,' ')+CompletaPalavra_a_Esquerda('VALOR',15,' '));
           If (FieldByName('Tipo').asstring='C')
           Then FrelTXT.sb.Items.add('?*******************CR�DITOS*****************')
           Else FrelTXT.sb.Items.add('?********************D�BITOS*****************');

           tipoTemp:=FieldByName('Tipo').asstring;
           SomaCredito:=0;
           SomaDebito:=0;
           While not(eof) do
           Begin
                if (tipotemp<>FieldByName('Tipo').asstring)
                Then Begin
                          If (tipotemp='C')
                          Then FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaCredito)))
                          Else FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaDebito)));

                          If (FieldByName('Tipo').asstring='C')
                          Then FrelTXT.sb.Items.add('?*******************CR�DITOS*****************')
                          Else FrelTXT.sb.Items.add('?********************D�BITOS*****************');
                          tipoTemp:=FieldByName('Tipo').asstring;
                End;
                If (FieldByName('Tipo').asstring='C')
                Then SomaCredito:=SomaCredito+FieldByName('SOMA').ASFLOAT
                Else SomaDebito:=SomaDebito+FieldByName('SOMA').ASFLOAT;

                FrelTXT.sb.Items.add(CompletaPalavra(FieldByName('CONTAGERENCIAL').asstring+'-'+FieldByName('NOME').asstring,50,' ')+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('soma').asstring),15,' '));

                next;
           End;

           If (tipotemp='C')
           Then FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaCredito)))
           Else FrelTXT.sb.Items.add('?SUBTOTAL R$'+formata_valor(floattostr(SomaDebito)));

           FrelTXT.sb.Items.add('?'+CompletaPalavra('',65,'-'));
           FrelTXT.sb.Items.add('?RESULTADO');
           FrelTXT.sb.Items.add('?CR�DITOS  R$ '+formata_valor(floattostr(SomaCredito)));
           FrelTXT.sb.Items.add('?D�BITOS   R$ '+formata_valor(floattostr(SomaDebito)));
           FrelTXT.sb.Items.add('?RESULTADO R$ '+formata_valor(floattostr(SomaCredito-SomaDebito)));
     End;

     With Freltxt do
     Begin
          LimpaCampos;
          lbtitulo.Enabled:=true;
          lbtitulo.caption:=Self.NumeroRelatorio+'RESUMO DE T�TULOS LAN�ADOS POR CONTA GERENCIAL';
          LBSUBTITULO1.enabled:=True;
          LBSUBTITULO1.caption:='PER�ODO DE '+datetostr(DataLimite1)+' A '+datetostr(DataLimite2);
          qr.Page.Orientation:=poportrait;
          qr.preview;
     End;
Finally

End;

end;




function TObjTitulo.Get_GeradoPeloSistema: string;
begin
     Result:=Self.GeradoPeloSistema;
end;

procedure TObjTitulo.Submit_GeradoPeloSistema(parametro: string);
begin
     //Self.GeradoPeloSistema:=parametro;
     //S� � USADO O GERADO PELO SISTEMA ='S' EM CASOS
     //DE CRIACAO DE TITULO DE DEBITO-CREDITO PEDIDO POR LOANDA
     //QUE INFLUENCIA O REL N� 8
     Self.GeradoPeloSistema:='N';
     
end;

procedure TObjTitulo.EdtcontagerencialKeyDown_Unico(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
Fpesquisalocal:Tfpesquisa;
Begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaContaGerencial,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjTitulo.ImprimeTitulosGeradosPeloSaldo;
begin

end;


procedure TObjTitulo.EdtcontagerencialPAGARKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CONTAGERENCIAL.get_pesquisadebito,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=Tedit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;

procedure TObjTitulo.EdtcontagerencialRECEBERKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CONTAGERENCIAL.Get_PesquisaCredito,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=Tedit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;

procedure TObjTitulo.EdtcontagerencialPAGARKeyDown_spv(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CONTAGERENCIAL.get_pesquisadebito,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;


procedure TObjTitulo.EdtcontagerencialRECEBERKeyDown_spv(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CONTAGERENCIAL.Get_PesquisaCredito,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjTitulo.EdtcontagerencialRECEBERKeyDown_spv(Sender: TObject;
  var Key: Word; Shift: TShiftState;LabelNome:TLabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CONTAGERENCIAL.Get_PesquisaCredito,Self.Get_TituloPesquisaContaGerencial,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                if (labelnome<>nil)
                                Then Begin
                                          LabelNome.caption:='';
                                          LabelNome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                                End;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjTitulo.RetornaCampoCodigo: string;
begin
     result:='codigo';
end;

function TObjTitulo.RetornaCampoNome: string;
begin
     result:='historico';
end;

function TObjtitulo.VerificaPermissao: Boolean;
begin
     result:=False;
     iF (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE TITULOS')=fALSE)
     Then exit;

     result:=True;
end;



Function TObjTitulo.AcertaNotaFiscal(PNumTitulo:string;PNUmDcto: string;PnotaFiscal:string;ComCommit:Boolean):boolean;
begin
     With self.ObjDataset do
     Begin
          Try
                  close;
                  SelectSQL.clear;
                  SelectSQL.add('Update Tabtitulo set NotaFiscal=''NF '''+#39+Pnotafiscal+#39+',NumDcto='+#39+PNumDcto+#39+' where codigo='+PNumtitulo);
                  ExecSQL;
                  If (ComCommit=True)
                  Then Commit;
                  result:=True;
          Except
                result:=False;
          End;
     End;
end;



procedure TObjTitulo.rollback;
begin
     FdataModulo.IBTransaction.RollbackRetaining;
end;



function TObjTitulo.Get_NotaFiscal: string;
begin
     Result:=Self.NotaFiscal;
end;

procedure TObjTitulo.Submit_NotaFiscal(parametro: string);
begin
     Self.NotaFiscal:=Parametro;
end;

procedure TObjTitulo.AcertaHistoricoPendencias;
var
   ObjPendencia:tobjPendencia;
   StrTemp:TIBStoredProc;
begin
     If (Messagedlg('Este procedimento ir� acerta todos os hist�ricos das pend�ncias do sistema. Deseja Continuar?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROCATUALIZAHISTORICOPENDENCIA';
           StrTemp.ExecProc;
           FDataModulo.IBTransaction.CommitRetaining;
           Showmessage('Conclu�do');
        Except
           FDataModulo.IBTransaction.RollbackRetaining;
           Messagedlg('ERRO DURANTE A EXECU��O DA PROCEDURE DE ATUALIZA��O DO HIST�RICO DA PEND�NCIA',mterror,[mbok],0);
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;

end;

procedure TObjTitulo.ImprimeFichaFinanceiraCRCP(CreditoDebito:string);
var
CodigoCredorDevedorTemp,CredorDevedorTemp:integer;
data1,data2:Tdate;
Querytemp,Querypendencias:Tibquery;
NomeClienteTemp:string;
SomaJurostemp,SomaDescontoTemp,SomaValorInicialTemp,SomaQuitacaotemp,SomaSaldoTemp:Currency;
SomaDiasAtraso,NumerodePendencias:Currency;
DataAtual:TDate;
MediaAtraso:Real;
begin
     //primeiros escolho
     //aberto,pagas ou todas
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.items.add('Com Saldo');
          RgOpcoes.items.add('Sem Saldo');
          RgOpcoes.items.add('Todas');
          Showmodal;
          if (tag=0)
          Then exit;
     End;

     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          limpaedit(FfiltroImp);
          DesativaGrupos;
          Grupo01.Enabled:=True;
          LbGrupo01.caption:='Data Inicial';
          Grupo02.Enabled:=True;
          LbGrupo02.caption:='Data Final';
          edtGrupo01.EditMask:='!99/99/9999;1;_';
          edtGrupo02.EditMask:='!99/99/9999;1;_';
          Grupo07.Enabled:=True;
          LbGrupo07.caption:='Cadastro';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;
          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedor_SPV_KeyDown;
          edtgrupo07.Color:=$005CADFE;
          Showmodal;

          If (tag=0)
          Then exit;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                Messagedlg('� necess�rio escolher o Cadastro',mterror,[mbok],0);
          End;

          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              Messagedlg('� necess�rio escolher um registro no cadastro!',mterror,[mbok],0);
              exit;
          End;

          NomeClienteTemp:=Self.Get_NomeCredorDevedor(inttostr(credordevedortemp),inttostr(codigocredordevedortemp));

          Try
             data1:=StrToDate(Edtgrupo01.text);
             data2:=StrToDate(Edtgrupo02.text);
          Except
               Messagedlg('Datas Inv�lidas',mterror,[mbok],0);
               exit;
          End;
     End;
     //ja tenho o filtro pronto
     try
        Querytemp:=Tibquery.create(nil);
        Querypendencias:=Tibquery.create(nil);
        Querytemp.database:=FdataModulo.Ibdatabase;
        Querypendencias.database:=FdataModulo.Ibdatabase;
     Except
           Messagedlg('Erro na tentativa de Cria��o da Query!',mterror,[mbok],0);
           exit;
     End;

Try
     With QueryTemp do
     Begin
          close;
          SQL.clear;
          SQL.add('select Tabtitulo.codigo,sum(tabpendencia.Saldo) as SALDO');
          SQL.add('from Tabpendencia join Tabtitulo on Tabpendencia.titulo=tabtitulo.codigo');
          SQL.add('join TabContaGer on Tabtitulo.contagerencial=TabContager.codigo');
          SQL.add('where TabContaGer.Tipo='+#39+CreditoDebito+#39);
          SQL.add('and (TabTitulo.Emissao>='+#39+formatdatetime('mm/dd/yyyy',data1)+#39+' and TabTitulo.Emissao<='+#39+formatdatetime('mm/dd/yyyy',data2)+#39+')');
          Sql.add('and Tabtitulo.CredorDevedor='+inttostr(CREDORDEVEDORtemp)+' and Tabtitulo.codigoCredordevedor='+inttostr(CODIGOCREDORDEVEDORtemp));
          SQL.add('group by tabtitulo.codigo');

          Case  FOpcaorel.RgOpcoes.ItemIndex of
                  0:SQL.add('having sum(tabpendencia.Saldo)>0');//com saldo
                  1:SQL.add('having sum(tabpendencia.Saldo)=0');//sem saldo
          End;
          FrelTXT.sb.Items.clear;
          open;
          If (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma Informa��o foi selecionada na pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;

          FrelTXT.sb.items.add(CompletaPalavra(Self.NumeroRelatorio+'FICHA FINANCEIRA', 30, ' '));
          FrelTXT.sb.items.add(' ');

          first;
          SomaSaldoTemp:=0;
          SomaValorInicialTemp:=0;
          SomaQuitacaotemp:=0;
          SomaJurostemp:=0;
          SomaDescontoTemp:=0;

          NumerodePendencias:=0;
          SomaDiasAtraso:=0;

          //Titulos das Colunas
          FrelTXT.sb.items.add('?'+CompletaPalavra('C�DIGO',9,' ')+' '+
                                   CompletaPalavra('HIST�RICO',30,' ')+' '+
                                   CompletaPalavra('CONTA GERENCIAL',20,' ')+' '+
                                   CompletaPalavra('EMISS�O',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VALOR',9,' ')+' '+
                                   CompletaPalavra_a_Esquerda('SALDO',9,' '));
          FrelTXT.sb.items.add(completapalavra('-',93,'-'));
          FrelTXT.sb.items.add(' ');

          while not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               FrelTXT.sb.items.add(CompletaPalavra(self.codigo,9,' ')+'|'+
                      CompletaPalavra(self.Historico,30,' ')+' '+
                      CompletaPalavra(self.CONTAGERENCIAL.Get_Nome,20,' ')+' '+
                      CompletaPalavra(self.emissao,10,' ')+' '+
                      CompletaPalavra_a_Esquerda(formata_valor(self.Valor),9,' ')+' '+
                      CompletaPalavra_a_esquerda(formata_valor(fieldbyname('Saldo').asstring),9,' '));
              //para cada titulo aberto, devo mostrar quando ja foi pago e quanto esta em aberto
              //em relacao as parcelas (pendencias)

              //Pcodigo,PHistorico,PVencimento,Pvalor,
              //PSaldo,PsomaQuitacao,PsomaJuros,PsomaDesconto

               Querypendencias.close;     
               Querypendencias.SQL.clear;
               Querypendencias.SQL.add('select * from ProcHistoricoPendTit('+Self.codigo+')');
               Querypendencias.Open;
               Querypendencias.first;
               //Titulos Colunas
               FrelTXT.sb.items.add('?   '+
                    CompletaPalavra('Pend.',5,' ')+' '+
                    CompletaPalavra('Hist�rico',09,' ')+' '+
                    CompletaPalavra('Vencim.',08,' ')+' '+
                    CompletaPalavra_a_Esquerda('Valor',10,' ')+' '+
                    CompletaPalavra_a_Esquerda('Juros',10,' ')+' '+
                    CompletaPalavra_a_Esquerda('Desconto',10,' ')+' '+
                    CompletaPalavra_a_Esquerda('Quita��o',10,' ')+' '+
                    CompletaPalavra_a_Esquerda('Saldo',10,' ')+' '+
                    CompletaPalavra_a_Esquerda('Ult.Quit.',09,' '));

               While not(Querypendencias.eof) do
               Begin
                    if (Querypendencias.fieldbyname('pultimaquitacao').asstring<>'')
                    Then Begin
                            FrelTXT.sb.items.add('   '+
                                                CompletaPalavra(Querypendencias.fieldbyname('Pcodigo').asstring,5,' ')+' '+
                                                CompletaPalavra(Querypendencias.fieldbyname('Phistorico').asstring,09,' ')+' '+
                                                CompletaPalavra(formatdatetime('dd/mm/yy',Querypendencias.fieldbyname('Pvencimento').asdatetime),08,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('Pvalor').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSomaJuros').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSomaDesconto').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSomaQuitacao').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSaldo').asstring),10,' ')+' '+
                                                CompletaPalavra(formatdatetime('dd/mm/yy',Querypendencias.fieldbyname('Pultimaquitacao').asdatetime),09,' '));

                    End
                    Else Begin
                            FrelTXT.sb.items.add('   '+
                                                CompletaPalavra(Querypendencias.fieldbyname('Pcodigo').asstring,5,' ')+' '+
                                                CompletaPalavra(Querypendencias.fieldbyname('Phistorico').asstring,09,' ')+' '+
                                                CompletaPalavra(formatdatetime('dd/mm/yy',Querypendencias.fieldbyname('Pvencimento').asdatetime),08,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('Pvalor').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSomaJuros').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSomaDesconto').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSomaQuitacao').asstring),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(Querypendencias.fieldbyname('PSaldo').asstring),10,' ')+' '+
                                                CompletaPalavra('',09,' '));

                    End;
                    SomaJurostemp:=SomaJurostemp+Querypendencias.fieldbyname('PSomaJuros').asfloat;
                    SomaDescontoTemp:=SomaDescontoTemp+Querypendencias.fieldbyname('PSomaDesconto').asfloat;
                    SomaQuitacaotemp:=SomaQuitacaotemp+Querypendencias.fieldbyname('PSomaQuitacao').asfloat;

                    //se houver quita��o blz, se nao pego a data atual para ver o atraso baseado no dia atual
                    //caso ja esteja vencido logicamente
                    DataAtual:=Now;
                    if Querypendencias.fieldbyname('pultimaquitacao').AsString<>''
                    then begin
                            SomaDiasAtraso:=SomaDiasAtraso+Diferencadias(Querypendencias.fieldbyname('pvencimento').AsDateTime,Querypendencias.fieldbyname('pultimaquitacao').AsDateTime);
                            NumerodePendencias:=NumerodePendencias+1;
                    end
                    else if DataAtual>Querypendencias.fieldbyname('pvencimento').AsDateTime
                    then begin
                            SomaDiasAtraso:=SomaDiasAtraso+Diferencadias(Querypendencias.fieldbyname('pvencimento').AsDateTime,DataAtual);
                            NumerodePendencias:=NumerodePendencias+1;
                    end;

                    Querypendencias.next;
               End;


               //divisoria do proximo titulo
               FrelTXT.sb.items.add(completapalavra('-',93,'-'));
               SomaValorInicialTemp:=SomaValorInicialTemp+strtofloat(Self.get_valor);
               SomaSaldoTemp:=SomaSaldoTemp+QueryTemp.fieldbyname('saldo').asfloat;
               next;
          End;
     End;
     //Totalizando o Saldo
     FrelTXT.Sb.items.add('?TOTAL INICIAL     = '+formata_valor(floattostr(SomaValorInicialTemp)));
     FrelTXT.Sb.items.add('?TOTAL PAGO        = '+formata_valor(floattostr(SomaQuitacaotemp)));
     FrelTXT.Sb.items.add('?TOTAL EM JUROS    = '+formata_valor(floattostr(SomaJurostemp)));
     FrelTXT.Sb.items.add('?TOTAL EM DESCONTO = '+formata_valor(floattostr(SomaDescontoTemp)));
     FrelTXT.Sb.items.add('?TOTAL EM ABERTO   = '+formata_valor(floattostr(SomaSaldotemp)));

     if  NumerodePendencias=0
     then NumerodePendencias:=1;
     MediaAtraso:=arredondafloat(SomaDiasAtraso/NumerodePendencias);

     if MediaAtraso>0
     then FrelTXT.SB.Items.Add('?Este Cliente possui M�dia de Atraso de Pagamento em '+FloatToStr(MediaAtraso)+' Dias')
     else FrelTXT.SB.Items.Add('?Este Cliente possui M�dia de Antecipa��o de Pagamento em '+FloatToStr(MediaAtraso*-1)+' Dias');

     FrelTXT.LimpaCampos;
     FrelTXT.LBTITULO.enabled:=True;
     if (CreditoDebito='C')
     Then FrelTXT.LBTITULO.Caption:='FICHA FINANCEIRA (T�TULOS A RECEBER) - '+DateToStr(data1)+' A '+DateToStr(data2)
     else FrelTXT.LBTITULO.Caption:='FICHA FINANCEIRA (T�TULOS A PAGAR) - '+DateToStr(data1)+' A '+DateToStr(data2);

     FrelTXT.LBSUBTITULO1.enabled:=True;
     FrelTXT.LBSUBTITULO1.Caption:=NomeClienteTemp;
     FrelTXT.LBSUBTITULO2.enabled:=True;

     case FOpcaorel.RgOpcoes.ItemIndex of
             0: FrelTXT.LBSUBTITULO2.Caption:='T�TULO COM SALDO';
             1: FrelTXT.LBSUBTITULO2.Caption:='T�TULOS SEM SALDO';
             2: FrelTXT.LBSUBTITULO2.Caption:='TODOS T�TULOS';
     End;
     FreltxtrdPrint.Preview(true);
     
Finally
       FreeAndNil(QueryTemp);
       FreeAndnil(Querypendencias);
End;
     
end;


procedure TObjTitulo.ImprimeFichaFinanceira;
begin
     //escolhendo se a receber ou a pagar
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.items.add('T�tulos a Receber');
          RgOpcoes.items.add('T�tulos a Pagar');
          Showmodal;
          if (tag=0)
          Then exit;
          CAse Rgopcoes.itemindex of
          0:Self.ImprimeFichaFinanceiraCRCP('C');
          1:Self.ImprimeFichaFinanceiraCRCP('D');
          End;
     End;


end;

Function TObjtitulo.Pendencias_em_Atraso(PCredorDevedor,
  PcodigoCredorDevedor: string;RetornaDados:Boolean;PTitulo,PPendencia,Pvencimento,Psaldo:TStringList):Boolean;
begin
     {ele retorna as pendencias que est�o atrasadas em rela��o a data de hoje
     ou seja tudo que vencer de ontem pra tras e que n�o foram pagas}

     result:=False;
     With self.Objdataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select tabpendencia.titulo,tabpendencia.codigo as pendencia,tabpendencia.vencimento,tabpendencia.saldo from tabpendencia');
          SelectSQL.add('join tabtitulo on  tabpendencia.titulo=tabtitulo.codigo');
          SelectSQL.add('where Tabpendencia.saldo<>0 and tabpendencia.vencimento<'+#39+formatdatetime('MM/DD/YYYY',NOW)+#39);
          SelectSQL.add('and Tabtitulo.credordevedor='+Pcredordevedor+' and Tabtitulo.codigocredordevedor='+pcodigocredordevedor);
          OPEN;
          If (recordcount>0)
          Then Result:=True;

          If (RetornaDados=False)
          Then exit;

          If (PTitulo<>nil)
          Then PTitulo.clear;

          If (PPendencia<>nil)
          Then PPendencia.clear;

          If (Pvencimento<>nil)
          Then Pvencimento.clear;

          If (Psaldo<>nil)
          Then Psaldo.clear;

          first;
          While not(EOF) DO
          Begin
               If (PTitulo<>nil)
               Then PTitulo.add(fieldbyname('titulo').asstring);

               If (PPendencia<>nil)
               Then PPendencia.add(fieldbyname('pendencia').asstring);

               If (Pvencimento<>nil)
               Then Pvencimento.add(fieldbyname('vencimento').asstring);

               If (Psaldo<>nil)
               Then Psaldo.add(fieldbyname('saldo').asstring);

               next;
          end;

     End;



end;


function TObjTitulo.SomaQuitacao(Ptitulo: string): Currency;
begin
     result:=0;
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select sum(prochistoricopendtit.PSOMAQUITACAO) as SOMA');
          SelectSQL.add('from prochistoricopendtit('+Ptitulo+')');
          Try
             open;
             result:=fieldbyname('soma').asfloat;
          Except
                result:=0;
          End;
     End;
end;
{
procedure TObjTitulo.ImprimeTotalporContaGerencial(PTipoConta: string);
var
ObjqueryLocal:Tibquery;
PContager,PCredorDevedor,PCodigoCredorDevedor:string;
PSomaTotalgeral,Ptemp,PSomaConta,PsomaFornecedor:Currency;
CodigoCredorDevedorTemp,CredorDevedorTemp,temp,linha:integer;
TempStr,PNomeCredorDevedor:String;
Pdata1,Pdata2:string;
ContaGerencialTemp:TStringList;
begin
     try
        ContaGerencialTemp:= TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Cria��o da StringList',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjqueryLocal:=TIBQuery.create(nil);
        ObjqueryLocal.Database:=FDataModulo.IBDatabase;
     Except
           freeandnil(ContaGerencialTemp);
           Messagedlg('Erro na tentativa de Cria��o da Query',mterror,[mbok],0);
           exit;
     End;
Try


     //filtrando
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          grupo01.enabled:=True;
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.Caption:='EMISS�O INICIAL';

          grupo02.enabled:=True;
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.Caption:='EMISS�O FINAL';

          Grupo03.Enabled:=True;
          edtGrupo03.EditMask:='';
          LbGrupo03.Caption:='Conta Gerencial';
          if (PTipoConta='D')
          Then edtgrupo03.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else edtgrupo03.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;

          Grupo07.enabled:=True;
          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;


          showmodal;

          if(tag=0)
          Then exit;

          try
             StrToDate(edtgrupo01.text);
             StrToDate(edtgrupo02.text);
             Pdata1:=edtgrupo01.text;
             Pdata2:=edtgrupo02.text;
          Except
                Pdata1:='';
                Pdata2:='';
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;




          //verificando contas gerenciais
          Try
             TempStr:='';

             ContaGerencialTemp.clear;
             for Temp:=1 to length(Edtgrupo03.text) do
             Begin
                   If ((edtgrupo03.text[temp]=';')
                   or (temp=length(Edtgrupo03.text)))
                   Then Begin
                           if ((temp=length(Edtgrupo03.text)) and (edtgrupo03.text[temp]<>';'))
                           Then TempStr:=TempStr+edtgrupo03.text[temp];

                           If (Tempstr<>'')
                           Then Begin
                                   Try
                                           Strtoint(tempstr);
                                           ContaGerencialTemp.add(Tempstr);
                                   Except
                                           ContaGerencialTemp.clear;
                                           Break;
                                   End;
                           End;
                           TempStr:='';
                   End
                   Else TempStr:=TempStr+edtgrupo03.text[temp];
             End;
          Except
                ContaGerencialTemp.clear;
          End;
          //***********************
          

     End;


     With objquerylocal do
     BEgin
          Close;
          Sql.clear;
          Sql.add('Select Tabtitulo.Contagerencial,tabcontager.nome as NOMECONTAGERENCIAL,Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor,TabTitulo.Codigo as TITULO');
          Sql.add('from tabtitulo');
          Sql.add('join proctitulosaldo on tabtitulo.codigo=proctitulosaldo.titulo');
          Sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          Sql.add('where proctitulosaldo.saldo=0 and TabContager.Tipo='+#39+PTipoConta+#39);

          if (ContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial='+ContaGerencialTemp[0]);
                  for temp:=1 to ContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.Contagerencial='+ContaGerencialTemp[temp]);

                  Sql.add(')');
          End;

          if (Pdata1<>'')
          Then Begin
                    SQL.add('and (Tabtitulo.Emissao>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                    SQL.add('and Tabtitulo.Emissao<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
          End;

          if (CredorDevedorTemp<>-1)
          Then Begin
                  Sql.add('and Tabtitulo.CredorDevedor='+inttostr(CREDORDEVEDORtemp));
                  if (codigoCredorDevedorTemp<>-1)
                  Then Sql.add('and Tabtitulo.codigoCredorDevedor='+inttostr(codigoCREDORDEVEDORtemp)); 
          End;


          Sql.add('order by tabcontager.nome,tabcontager.codigo,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
          Sql.add('TabTitulo.Emissao');
          open;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum dado foi encontrado!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,'TOTAL POR CONTA GERENCIAL',[negrito]);
          inc(linha,2);

          if (PTipoConta='C')
          Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS A RECEBER',[negrito])
          Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS A PAGAR',[negrito]);
          inc(linha,2);


          //A Id�ia � assim
          //Conta Gerencial XXXXX
          //    Fornecedor  A       Total Pago
          //    Fornecedor  B       Total Pago
          //    Fornecedor  C       Total Pago
          //Total da Conta  R$ XXXXX

          PContager:=Fieldbyname('contagerencial').asstring;
          PCredorDevedor:=Fieldbyname('credordevedor').asstring;
          PCodigoCredorDevedor:=Fieldbyname('codigocredordevedor').asstring;
          PSomaFornecedor:=0;
          PSomaConta:=0;
          //Imprimindo a Primeira Contagerencial
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
          inc(linha,1);
          //primeiro fornecedor
          PNomeCredorDevedor:=completapalavra(fieldbyname('codigocredordevedor').asstring,6,' ')+' - '+
                              completapalavra(Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),50,' ');
          PSomaTotalgeral:=0;
          While not(Eof) do
          Begin
               if (PContager<>Fieldbyname('contagerencial').asstring)
               Then Begin
                         //totalizando o fornecedor
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.imp(linha,10,PNomeCredorDevedor+' '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaFornecedor)),12,' '));
                          inc(linha,1);
                          //totalizando a contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
                          inc(linha,2);

                          //Nova Conta
                          PContager:=Fieldbyname('contagerencial').asstring;
                          PCredorDevedor:=Fieldbyname('credordevedor').asstring;
                          PCodigoCredorDevedor:=Fieldbyname('codigocredordevedor').asstring;
                          PSomaFornecedor:=0;
                          PSomaConta:=0;
                         //Contagerencial
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
                         FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
                         inc(linha,1);
                         //fornecedor
                         PNomeCredorDevedor:=completapalavra(fieldbyname('codigocredordevedor').asstring,6,' ')+' - '+
                                             completapalavra(Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),50,' ');
               End
               Else Begin
                         if ((PCodigoCredorDevedor<>fieldbyname('codigocredordevedor').asstring) or
                            (PCredorDevedor<>fieldbyname('credordevedor').asstring))
                         Then Begin
                                   //totalizando o fornecedor
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                   FreltxtRDPRINT.RDprint.imp(linha,10,PNomeCredorDevedor+' '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaFornecedor)),12,' '));
                                   inc(linha,1);

                                   PCredorDevedor:=Fieldbyname('credordevedor').asstring;
                                   PCodigoCredorDevedor:=Fieldbyname('codigocredordevedor').asstring;
                                   PSomaFornecedor:=0;
                                   //fornecedor
                                   PNomeCredorDevedor:=completapalavra(fieldbyname('codigocredordevedor').asstring,6,' ')+' - '+
                                                      completapalavra(Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),50,' ');
                         End;
               End;

               Ptemp:=Self.SomaQuitacao(fieldbyname('titulo').asstring);
               PsomaFornecedor:=PsomaFornecedor+Ptemp;
               PSomaConta:=PSomaConta+Ptemp;
               PSomaTotalgeral:=PSomaTotalgeral+Ptemp;
               next;
          End;
          

          //totalizando o fornecedor
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,10,PNomeCredorDevedor+' '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaFornecedor)),12,' '));
          inc(linha,1);
          //totalizando a contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SOMA TOTAL '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PSomaTotalgeral)),12,' '),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.Fechar;

     End;
Finally
       freeandnil(ContaGerencialTemp);
       FreeAndNil(ObjqueryLocal);
End;


end;}

procedure TObjTitulo.ImprimeTotalporContaGerencial(PTipoConta: string);
var
ObjqueryLocal:Tibquery;
PContager:string;
PSomaTotalgeral,PSomaConta:Currency;
credordevedortemp,codigocredordevedortemp,temp,linha:integer;
TempStr,PNomeCredorDevedor:String;
Pdata1,Pdata2:string;
ContaGerencialTemp:TStringList;
TmpQuantPEndencias,TmpQuantDevedores:Integer;
begin
     try
        ContaGerencialTemp:= TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Cria��o da StringList',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjqueryLocal:=TIBQuery.create(nil);
        ObjqueryLocal.Database:=FDataModulo.IBDatabase;
     Except
           freeandnil(ContaGerencialTemp);
           Messagedlg('Erro na tentativa de Cria��o da Query',mterror,[mbok],0);
           exit;
     End;
Try


     //filtrando
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          grupo01.enabled:=True;
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.Caption:='LAN�. INICIAL';

          grupo02.enabled:=True;
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.Caption:='LAN�. FINAL';

          Grupo03.Enabled:=True;
          edtGrupo03.EditMask:='';
          LbGrupo03.Caption:='Conta Gerencial';
          if (PTipoConta='D')
          Then edtgrupo03.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else edtgrupo03.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;

          edtgrupo03.Color:=$005CADFE;

          Grupo07.enabled:=True;
          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;
          edtgrupo07.Color:=$005CADFE;


          showmodal;

          if(tag=0)
          Then exit;

          try
             StrToDate(edtgrupo01.text);
             StrToDate(edtgrupo02.text);
             Pdata1:=edtgrupo01.text;
             Pdata2:=edtgrupo02.text;
          Except
                Pdata1:='';
                Pdata2:='';
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;




          //verificando contas gerenciais
          Try
             TempStr:='';

             ContaGerencialTemp.clear;
             for Temp:=1 to length(Edtgrupo03.text) do
             Begin
                   If ((edtgrupo03.text[temp]=';')
                   or (temp=length(Edtgrupo03.text)))
                   Then Begin
                           if ((temp=length(Edtgrupo03.text)) and (edtgrupo03.text[temp]<>';'))
                           Then TempStr:=TempStr+edtgrupo03.text[temp];

                           If (Tempstr<>'')
                           Then Begin
                                   Try
                                           Strtoint(tempstr);
                                           ContaGerencialTemp.add(Tempstr);
                                   Except
                                           ContaGerencialTemp.clear;
                                           Break;
                                   End;
                           End;
                           TempStr:='';
                   End
                   Else TempStr:=TempStr+edtgrupo03.text[temp];
             End;
          Except
                ContaGerencialTemp.clear;
          End;
          //***********************


     End;


     With objquerylocal do
     BEgin
          Close;
          Sql.clear;
          Sql.add('Select tabcontager.nome as nomecontagerencial,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
          Sql.add('sum(tablancamento.valor) as SOMAQUITACAO');
          Sql.add('from tabtitulo');
          Sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          Sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
          Sql.add('join tablancamento on tabpendencia.codigo=tablancamento.pendencia');
          Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
          Sql.add('where TabContager.Tipo='+#39+PTipoConta+#39);
          Sql.add('and tabtipolancto.classificacao=''Q''');

          if (ContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial='+ContaGerencialTemp[0]);
                  for temp:=1 to ContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.Contagerencial='+ContaGerencialTemp[temp]);

                  Sql.add(')');
          End;

          if (Pdata1<>'')
          Then Begin
                    SQL.add('and (tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                    SQL.add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
          End;

          if (CredorDevedorTemp<>-1)
          Then Begin
                  Sql.add('and Tabtitulo.CredorDevedor='+inttostr(CREDORDEVEDORtemp));
                  if (codigoCredorDevedorTemp<>-1)
                  Then Sql.add('and Tabtitulo.codigoCredorDevedor='+inttostr(codigoCREDORDEVEDORtemp));
          End;

          Sql.add('group by tabcontager.nome,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
          Sql.add('order by tabcontager.nome,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');


          open;

          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum dado foi encontrado!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'TOTAL POR CONTA GERENCIAL',[negrito]);
          inc(linha,2);

          if (PTipoConta='C')
          Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS RECEBIDAS',[negrito])
          Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS PAGAS',[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos de '+Pdata1+' a '+Pdata2,[negrito]);
          inc(linha,2);


          //A Id�ia � assim
          //Conta Gerencial XXXXX
          //    Fornecedor  A       Total Pago
          //    Fornecedor  B       Total Pago
          //    Fornecedor  C       Total Pago
          //Total da Conta  R$ XXXXX

          PContager:=Fieldbyname('contagerencial').asstring;
          PSomaConta:=0;
          //Imprimindo a Primeira Contagerencial
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
          inc(linha,1);
          //primeiro fornecedor
          PNomeCredorDevedor:=completapalavra(fieldbyname('codigocredordevedor').asstring,6,' ')+' - '+
                              completapalavra(Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),50,' ');
          PSomaTotalgeral:=0;
          TmpQuantPEndencias:=0;
          TmpQuantDevedores:=0;
          While not(Eof) do
          Begin
               if (PContager<>Fieldbyname('contagerencial').asstring)
               Then Begin
                          //totalizando a contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
                          inc(linha,2);

                          //Nova Conta
                          PContager:=Fieldbyname('contagerencial').asstring;
                          PSomaConta:=0;
                         //Contagerencial
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
                         FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
                         inc(linha,1);
               End;


               PNomeCredorDevedor:=completapalavra(fieldbyname('codigocredordevedor').asstring,6,' ')+' - '+
                                   completapalavra(Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),50,' ');

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,PNomeCredorDevedor+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('somaquitacao').asstring),12,' '));
               inc(linha,1);
               PSomaConta:=PSomaConta+fieldbyname('somaquitacao').asfloat;
               PSomaTotalgeral:=PSomaTotalgeral++fieldbyname('somaquitacao').asfloat;
               inc(TmpQuantPEndencias,1);
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SOMA TOTAL '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PSomaTotalgeral)),12,' '),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.Fechar;

     End;
Finally
       freeandnil(ContaGerencialTemp);
       FreeAndNil(ObjqueryLocal);
End;


end;



procedure TObjTitulo.ImprimeTotalporContaGerencialeSubConta_agrupado_credordevedor(PTipoConta: string);
var
ObjqueryLocal:Tibquery;
PContager,PsubContager:string;
PSomaSUBConta,PSomaTotalgeral,PSomaConta:Currency;
credordevedortemp,codigocredordevedortemp,temp,linha:integer;
TempStr,PNomeCredorDevedor:String;
Pdata1,Pdata2:string;
ContaGerencialTemp,SubContaGerencialTemp:TStringList;
TmpQuantPEndencias,TmpQuantDevedores:Integer;
begin
     try
        ContaGerencialTemp:= TStringList.create;
        SubContaGerencialTemp:= TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Cria��o da StringList',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjqueryLocal:=TIBQuery.create(nil);
        ObjqueryLocal.Database:=FDataModulo.IBDatabase;
     Except
           freeandnil(ContaGerencialTemp);
           Messagedlg('Erro na tentativa de Cria��o da Query',mterror,[mbok],0);
           exit;
     End;
Try


     //filtrando
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          grupo01.enabled:=True;
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.Caption:='LAN�. INICIAL';

          grupo02.enabled:=True;
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.Caption:='LAN�. FINAL';

          Grupo03.Enabled:=True;
          edtGrupo03.EditMask:='';
          LbGrupo03.Caption:='Conta Gerencial';
          if (PTipoConta='D')
          Then edtgrupo03.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else edtgrupo03.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;

          Grupo04.Enabled:=True;
          edtGrupo04.EditMask:='';
          LbGrupo04.Caption:='SubConta Gerencial';
          edtgrupo04.OnKeyDown:=Self.edtsubcontagerencialKeyDown_PV;



          Grupo07.enabled:=True;
          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;


          showmodal;

          if(tag=0)
          Then exit;

          try
             StrToDate(edtgrupo01.text);
             StrToDate(edtgrupo02.text);
             Pdata1:=edtgrupo01.text;
             Pdata2:=edtgrupo02.text;
          Except
                Pdata1:='';
                Pdata2:='';
          End;

          Try
                CredorDevedorTemp:=-1;
                if (ComboGrupo07.itemindex>=0)
                Then CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=-1;
             if (EdtGrupo07.text<>'')
             Then CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;

          //verificando contas gerenciais
          Try
             TempStr:='';

             ContaGerencialTemp.clear;
             for Temp:=1 to length(Edtgrupo03.text) do
             Begin
                   If ((edtgrupo03.text[temp]=';')
                   or (temp=length(Edtgrupo03.text)))
                   Then Begin
                           if ((temp=length(Edtgrupo03.text)) and (edtgrupo03.text[temp]<>';'))
                           Then TempStr:=TempStr+edtgrupo03.text[temp];

                           If (Tempstr<>'')
                           Then Begin
                                   Try
                                           Strtoint(tempstr);
                                           ContaGerencialTemp.add(Tempstr);
                                   Except
                                           ContaGerencialTemp.clear;
                                           Break;
                                   End;
                           End;
                           TempStr:='';
                   End
                   Else TempStr:=TempStr+edtgrupo03.text[temp];
             End;
          Except
                ContaGerencialTemp.clear;
          End;


          //verificando Sub-contas gerenciais
          TempStr:=edtgrupo04.text;
          if (explodestr(tempstr,SubContaGerencialTemp,';','INTEGER')=False)
          Then exit;
          //***********************
     End;


     With objquerylocal do
     BEgin
          Close;
          Sql.clear;
          Sql.add('Select tabcontager.nome as nomecontagerencial,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
          Sql.add('TabSubContagerencial.nome as NOMESUBCONTAGERENCIAL,Tabtitulo.subcontagerencial,');
          Sql.add('sum(tablancamento.valor) as SOMAQUITACAO');
          Sql.add('from tabtitulo');
          Sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          Sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
          Sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
          Sql.add('join tablancamento on tabpendencia.codigo=tablancamento.pendencia');
          Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
          Sql.add('where TabContager.Tipo='+#39+PTipoConta+#39);
          Sql.add('and tabtipolancto.classificacao=''Q''');

          if (ContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial='+ContaGerencialTemp[0]);
                  for temp:=1 to ContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.Contagerencial='+ContaGerencialTemp[temp]);

                  Sql.add(')');
          End;


          if (SubContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.SubContagerencial='+SubContaGerencialTemp[0]);

                  for temp:=1 to SubContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.SubContagerencial='+SubContaGerencialTemp[temp]);

                  Sql.add(')');
          End;



          if (Pdata1<>'')
          Then Begin
                    SQL.add('and (tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                    SQL.add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
          End;

          if (CredorDevedorTemp<>-1)
          Then Begin
                  Sql.add('and Tabtitulo.CredorDevedor='+inttostr(CREDORDEVEDORtemp));
                  if (codigoCredorDevedorTemp<>-1)
                  Then Sql.add('and Tabtitulo.codigoCredorDevedor='+inttostr(codigoCREDORDEVEDORtemp));
          End;


          Sql.add('group by tabcontager.nome,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,TabSubContagerencial.nome,Tabtitulo.subcontagerencial');
          Sql.add('order by tabcontager.nome,tabtitulo.contagerencial,tabsubcontagerencial.nome,tabtitulo.subcontagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
          open;
          
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum dado foi encontrado!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;

          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'TOTAL POR CONTA GERENCIAL/SUB-CONTA',[negrito]);
          inc(linha,2);

          if (PTipoConta='C')
          Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS RECEBIDAS',[negrito])
          Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS PAGAS',[negrito]);
          inc(linha,1);

          
          FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos de '+Pdata1+' a '+Pdata2,[negrito]);
          inc(linha,2);


          //A Id�ia � assim
          //Conta Gerencial XXXXX
          //    Fornecedor  A       Total Pago
          //    Fornecedor  B       Total Pago
          //    Fornecedor  C       Total Pago
          //Total da Conta  R$ XXXXX


          //Imprimindo a Primeira Contagerencial
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
          inc(linha,1);

          PContager:=Fieldbyname('contagerencial').asstring;
          PSubContager:=Fieldbyname('subcontagerencial').asstring;
          PSomaConta:=0;
          PSomaSubConta:=0;
          PSomaTotalgeral:=0;
          TmpQuantPEndencias:=0;
          TmpQuantDevedores:=0;
          While not(Eof) do
          Begin
               if (PContager<>Fieldbyname('contagerencial').asstring)
               Then Begin

                          //totalizando a Sub-contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
                          inc(linha,1);
                          //totalizando a contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
                          inc(linha,2);
                          //Nova Conta
                          PContager:=Fieldbyname('contagerencial').asstring;
                          PSubContager:=Fieldbyname('subcontagerencial').asstring;
                          PSomaConta:=0;
                          PSomaSubConta:=0;
                          //Contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
                          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
                          inc(linha,1);
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
                          FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
                          inc(linha,1);




               End
               Else BEgin
                         if (PSubContager<>Fieldbyname('subcontagerencial').asstring)
                         Then Begin
                                    //totalizando a Sub-contagerencial
                                    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                    FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
                                    inc(linha,2);
                         
                                    //Nova Conta
                                    PSubContager:=Fieldbyname('subcontagerencial').asstring;
                                    PSomaSubconta:=0;
                                    //SubContagerencial
                                    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                    FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
                                    FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
                                    inc(linha,1);
                         End
               End;


               PNomeCredorDevedor:=completapalavra(fieldbyname('codigocredordevedor').asstring,6,' ')+' - '+
                                   completapalavra(Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),50,' ');

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,20,PNomeCredorDevedor+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('somaquitacao').asstring),12,' '));
               inc(linha,1);
               PSomaConta:=PSomaConta+fieldbyname('somaquitacao').asfloat;
               PSomaSubConta:=PSomaSUbConta+fieldbyname('somaquitacao').asfloat;
               PSomaTotalgeral:=PSomaTotalgeral++fieldbyname('somaquitacao').asfloat;
               inc(TmpQuantPEndencias,1);
               next;
          End;
          //totalizando a Sub-contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
          inc(linha,1);
          //totalizando a Contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SOMA TOTAL '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PSomaTotalgeral)),12,' '),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.Fechar;

     End;
Finally
       freeandnil(ContaGerencialTemp);
       FreeAndNil(ObjqueryLocal);
End;
 
end;


procedure TObjTitulo.ImprimeTotalporContaGerencialeSubConta(PTipoConta: string);
var
  ObjQueryLocal : Tibquery;
  PContager,PsubContager:string;
  PSomaSUBConta,PSomaTotalgeral,PSomaConta:Currency;
  credordevedortemp,codigocredordevedortemp,temp,linha:integer;
  TempStr,PNomeCredorDevedor:String;
  Pdata1,Pdata2:string;
  ExcluirContaGerencialTemp, ContaGerencialTemp,SubContaGerencialTemp:TStringList;
  TmpQuantPEndencias,TmpQuantDevedores:Integer;
begin

  try
    ContaGerencialTemp:= TStringList.create;
    ExcluirContaGerencialTemp:=TStringList.Create;
    SubContaGerencialTemp:= TStringList.create;
  except
    Messagedlg('Erro na tentativa de Cria��o da StringList',mterror,[mbok],0);
    exit;
  end;

  try
    ObjqueryLocal:=TIBQuery.create(nil);
    ObjqueryLocal.Database:=FDataModulo.IBDatabase;
  except
    freeandnil(ContaGerencialTemp);
    Messagedlg('Erro na tentativa de Cria��o da Query',mterror,[mbok],0);
    exit;
  end;

  try
    //filtrando
    limpaedit(FfiltroImp);
    with FfiltroImp do
    begin
      DesativaGrupos;
      grupo01.enabled:=True;
      edtgrupo01.EditMask:='!99/99/9999;1;_';
      LbGrupo01.Caption:='LAN�. INICIAL';

      grupo02.enabled:=True;
      edtgrupo02.EditMask:='!99/99/9999;1;_';
      LbGrupo02.Caption:='LAN�. FINAL';

      Grupo03.Enabled:=True;
      edtGrupo03.EditMask:='';
      LbGrupo03.Caption:='Conta Gerencial';

      if (PTipoConta='D')
      then edtgrupo03.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
      else edtgrupo03.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;
      edtgrupo03.Color:=$005CADFE;


      Grupo04.Enabled:=True;
      edtGrupo04.EditMask:='';
      LbGrupo04.Caption:='Excluir Conta Gerencial';

      if (PTipoConta='D')
      then edtgrupo04.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
      else edtgrupo04.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;
      edtgrupo04.Color:=$005CADFE;

      Grupo05.Enabled:=True;
      edtGrupo05.EditMask:='';
      LbGrupo05.Caption:='SubConta Gerencial';
      edtgrupo05.OnKeyDown:=Self.edtsubcontagerencialKeyDown_PV;
      edtgrupo05.color:=$005CADFE;

      Grupo07.enabled:=True;
      LbGrupo07.caption:='Credor/Devedor';
      ComboGrupo07.Items.clear;
      Combo2Grupo07.Items.clear;

      Self.Get_ListacredorDevedor(ComboGrupo07.items);
      Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
      edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;
      edtgrupo07.color:=$005CADFE;

      showmodal;

      if(tag=0)
      then exit;

      try
        StrToDate(edtgrupo01.text);
        StrToDate(edtgrupo02.text);
        Pdata1:=edtgrupo01.text;
        Pdata2:=edtgrupo02.text;
      except
        Pdata1:='';
        Pdata2:='';
      end;

      try
        CredorDevedorTemp:=-1;
        if (ComboGrupo07.itemindex>=0)
        then CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
      except
        CredorDevedorTemp:=-1;
      end;

      try
        CodigoCredorDevedorTemp:=-1;
        if (EdtGrupo07.text<>'')
        then CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
      except
        CodigoCredorDevedorTemp:=-1;
      end;

      //verificando contas gerenciais
      try
        TempStr:='';

        ContaGerencialTemp.clear;
        for Temp:=1 to length(Edtgrupo03.text) do
        begin
          if ((edtgrupo03.text[temp]=';') or (temp=length(Edtgrupo03.text)))then
          begin
            if ((temp=length(Edtgrupo03.text)) and (edtgrupo03.text[temp]<>';'))
            then TempStr:=TempStr+edtgrupo03.text[temp];

            if (Tempstr<>'')then
            begin
              try
                Strtoint(tempstr);
                ContaGerencialTemp.add(Tempstr);
              except
                ContaGerencialTemp.clear;
                Break;
              end;
            end;
            TempStr:='';
          end
          else TempStr:=TempStr+edtgrupo03.text[temp];
        end;
      except
        ContaGerencialTemp.clear;
      end;

      if (ExplodeStr(edtgrupo04.Text,ExcluirContaGerencialTemp,';','INTEGER') = false)
      then exit;

      //verificando Sub-contas gerenciais
      TempStr:=edtgrupo05.text;
      if (explodestr(tempstr,SubContaGerencialTemp,';','INTEGER')=False)
      then exit;
      //***********************
    end;

    with objquerylocal do
    begin
      Close;
      Sql.clear;
      Sql.add('Select tabcontager.nome as nomecontagerencial,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
      Sql.add('TabSubContagerencial.nome as NOMESUBCONTAGERENCIAL,Tabtitulo.subcontagerencial,tabpendencia.titulo,tabtitulo.historico,');
      Sql.add('sum(tablancamento.valor) as SOMAQUITACAO');
      Sql.add('from tabtitulo');
      Sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
      Sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
      Sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
      Sql.add('join tablancamento on tabpendencia.codigo=tablancamento.pendencia');
      Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
      Sql.add('where TabContager.Tipo='+#39+PTipoConta+#39);
      Sql.add('and tabtipolancto.classificacao=''Q''');

      if (ContaGerencialTemp.count>0)then
      begin
        Sql.add('and (TabTitulo.Contagerencial='+ContaGerencialTemp[0]);
        for temp:=1 to ContaGerencialTemp.Count-1 do
          Sql.add('or TabTitulo.Contagerencial='+ContaGerencialTemp[temp]);

        Sql.add(')');
      end;

      if (ExcluirContaGerencialTemp.count>0)then
      begin
        Sql.add('and (TabTitulo.Contagerencial<>'+ExcluirContaGerencialTemp[0]);
        for temp:=1 to ExcluirContaGerencialTemp.Count-1 do
          Sql.add('and TabTitulo.Contagerencial<>'+ExcluirContaGerencialTemp[temp]);

        Sql.add(')');
      end;

      if (SubContaGerencialTemp.count>0)then
      begin
        Sql.add('and (TabTitulo.SubContagerencial='+SubContaGerencialTemp[0]);

        for temp:=1 to SubContaGerencialTemp.Count-1 do
          Sql.add('or TabTitulo.Contagerencial='+SubContaGerencialTemp[temp]);

        Sql.add(')');
      end;

      if (Pdata1<>'')then
      begin
        SQL.add('and (tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
        SQL.add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
      end;

      if (CredorDevedorTemp<>-1)then
      begin
        Sql.add('and Tabtitulo.CredorDevedor='+inttostr(CREDORDEVEDORtemp));
        if (codigoCredorDevedorTemp<>-1)
        then Sql.add('and Tabtitulo.codigoCredorDevedor='+inttostr(codigoCREDORDEVEDORtemp));
      end;

      Sql.add('group by tabcontager.nome,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,TabSubContagerencial.nome,Tabtitulo.subcontagerencial,tabpendencia.titulo,tabtitulo.historico');
      Sql.add('order by tabcontager.nome,tabtitulo.contagerencial,tabsubcontagerencial.nome,tabtitulo.subcontagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');

      open;
          
      if (Recordcount=0)then
      begin
        Messagedlg('Nenhum dado foi encontrado!',mtinformation,[mbok],0);
        exit;
      end;

      first;
      FreltxtRDPRINT.ConfiguraImpressao;
      FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
      FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;
      FreltxtRDPRINT.RDprint.Abrir;

      if (FreltxtRDPRINT.RDprint.setup=False)then
      begin
        FreltxtRDPRINT.RDprint.Fechar;
        exit;
      end;

      linha:=3;
      FreltxtRDPRINT.RDprint.ImpC(linha,65,Self.NumeroRelatorio+'TOTAL POR CONTA GERENCIAL/SUB-CONTA',[negrito]);
      inc(linha,2);

      if (PTipoConta='C')then
        FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS RECEBIDAS',[negrito])
      else FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS PAGAS',[negrito]);
      inc(linha,1);

      FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos de '+Pdata1+' a '+Pdata2,[negrito]);
      inc(linha,2);

      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.Impf(linha,20,CompletaPalavra('CREDOR/DEVEDOR',29,' ')+' '+
                                           CompletaPalavra('HISTORICO',60,' ')+' '+
                                           CompletaPalavra('TITULO',6,' ')+' '+
                                           CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
      inc(linha,1);


      //A Id�ia � assim
      //Conta Gerencial XXXXX
      //    Fornecedor  A       Total Pago
      //    Fornecedor  B       Total Pago
      //    Fornecedor  C       Total Pago
      //Total da Conta  R$ XXXXX


      //Imprimindo a Primeira Contagerencial
      FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
      FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
      inc(linha,1);
      FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
      FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
      inc(linha,1);

      PContager:=Fieldbyname('contagerencial').asstring;
      PSubContager:=Fieldbyname('subcontagerencial').asstring;
      PSomaConta:=0;
      PSomaSubConta:=0;
      PSomaTotalgeral:=0;
      TmpQuantPEndencias:=0;
      TmpQuantDevedores:=0;

      while not(Eof) do
      begin
        if (PContager<>Fieldbyname('contagerencial').asstring)then
        begin
          //totalizando a Sub-contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
          inc(linha,1);
          //totalizando a contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
          inc(linha,2);
          //Nova Conta
          PContager:=Fieldbyname('contagerencial').asstring;
          PSubContager:=Fieldbyname('subcontagerencial').asstring;
          PSomaConta:=0;
          PSomaSubConta:=0;
          //Contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
          inc(linha,1);

        end
        else
        begin
          if (PSubContager<>Fieldbyname('subcontagerencial').asstring)then
          begin
            //totalizando a Sub-contagerencial
            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
            FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
            inc(linha,2);

            //Nova Conta
            PSubContager:=Fieldbyname('subcontagerencial').asstring;
            PSomaSubconta:=0;
            //SubContagerencial
            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
            FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
            FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
            inc(linha,1);
          end
        end;

        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
        FreltxtRDPRINT.RDprint.Imp(linha,20,completapalavra(fieldbyname('codigocredordevedor').asstring+'-'+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),29,' ')+' '+
                                           CompletaPalavra(fieldbyname('historico').asstring,60,' ')+' '+
                                           CompletaPalavra(fieldbyname('titulo').asstring,6,' ')+' '+
                                           CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('somaquitacao').asstring),12,' '));
        inc(linha,1);
        PSomaConta:=PSomaConta+fieldbyname('somaquitacao').asfloat;
        PSomaSubConta:=PSomaSUbConta+fieldbyname('somaquitacao').asfloat;
        PSomaTotalgeral:=PSomaTotalgeral++fieldbyname('somaquitacao').asfloat;
        inc(TmpQuantPEndencias,1);
        next;
      end;

      //totalizando a Sub-contagerencial
      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
      inc(linha,1);
      //totalizando a Contagerencial
      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
      inc(linha,1);

      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
      inc(linha,1);

      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.impf(linha,1,'SOMA TOTAL '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PSomaTotalgeral)),12,' '),[negrito]);
      inc(linha,2);

      FreltxtRDPRINT.RDprint.Fechar;

    end;
  finally
    freeandnil(ContaGerencialTemp);
    FreeAndNil(ObjqueryLocal);
    FreeAndNil(ExcluirContaGerencialTemp);
  end;

end;


procedure TObjTitulo.ImprimeTotalporContaGerencialeSubConta_Sintetico(PTipoConta: string);
var
ObjqueryLocal:Tibquery;
PContager:string;
PSomaTotalgeral,PSomaConta:Currency;
temp,linha:integer;
TempStr,PNomeCredorDevedor:String;
Pdata1,Pdata2:string;
ContaGerencialTemp,SubContaGerencialTemp,ExcluirContaGerencialTemp:TStringList;
TmpQuantPEndencias,TmpQuantDevedores:Integer;
begin
     try
        ContaGerencialTemp:= TStringList.create;
        SubContaGerencialTemp:= TStringList.create;
        ExcluirContaGerencialTemp:= TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Cria��o da StringList',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjqueryLocal:=TIBQuery.create(nil);
        ObjqueryLocal.Database:=FDataModulo.IBDatabase;
     Except
           freeandnil(ContaGerencialTemp);
           Messagedlg('Erro na tentativa de Cria��o da Query',mterror,[mbok],0);
           exit;
     End;
Try


     //filtrando
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          grupo01.enabled:=True;
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.Caption:='LAN�. INICIAL';

          grupo02.enabled:=True;
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.Caption:='LAN�. FINAL';

          Grupo03.Enabled:=True;
          edtGrupo03.EditMask:='';
          LbGrupo03.Caption:='Conta Gerencial';
          if (PTipoConta='D')
          Then edtgrupo03.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else edtgrupo03.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;
          edtgrupo03.color:=$005CADFE;

          Grupo04.Enabled:=True;
          edtGrupo04.EditMask:='';
          LbGrupo04.Caption:='SubConta Gerencial';
          edtgrupo04.OnKeyDown:=Self.edtsubcontagerencialKeyDown_PV;
          edtgrupo04.color:=$005CADFE;

          Grupo05.Enabled:=True;

          edtgrupo05.Text:='';
          lbgrupo05.Caption:='Excluir conta gerencial';
          if (PTipoConta='D')
          Then edtgrupo05.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else edtgrupo05.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;
          edtgrupo05.Color:=$005CADFE;


          showmodal;

          if(tag=0)
          Then exit;

          try
             StrToDate(edtgrupo01.text);
             StrToDate(edtgrupo02.text);
             Pdata1:=edtgrupo01.text;
             Pdata2:=edtgrupo02.text;
          Except
                Pdata1:='';
                Pdata2:='';
          End;

          if (ExplodeStr(edtgrupo05.Text,ExcluirContaGerencialTemp,';','INTEGER')=false)
          then exit;

          //verificando contas gerenciais
          TempStr:=edtgrupo03.text;
          if (explodestr(tempstr,ContaGerencialTemp,';','INTEGER')=False)
          Then exit;

          //verificando Sub-contas gerenciais
          TempStr:=edtgrupo04.text;
          if (explodestr(tempstr,SubContaGerencialTemp,';','INTEGER')=False)
          Then exit;
          //***********************
     End;


     With objquerylocal do
     BEgin
          Close;
          Sql.clear;
          Sql.add('Select tabcontager.nome as nomecontagerencial,tabtitulo.contagerencial,');
          Sql.add('TabSubContagerencial.nome as NOMESUBCONTAGERENCIAL,Tabtitulo.subcontagerencial,');
          Sql.add('sum(tablancamento.valor) as SOMAQUITACAO');
          Sql.add('from tabtitulo');
          Sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          Sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
          Sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
          Sql.add('join tablancamento on tabpendencia.codigo=tablancamento.pendencia');
          Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
          Sql.add('where TabContager.Tipo='+#39+PTipoConta+#39);
          Sql.add('and tabtipolancto.classificacao=''Q''');

          if (ContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial='+ContaGerencialTemp[0]);
                  for temp:=1 to ContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.Contagerencial='+ContaGerencialTemp[temp]);

                  Sql.add(')');
          End;


          if (SubContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.SubContagerencial='+SubContaGerencialTemp[0]);

                  for temp:=1 to SubContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.SubContagerencial='+SubContaGerencialTemp[temp]);

                  Sql.add(')');
          End;

          if (ExcluirContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial<>'+ExcluirContaGerencialTemp[0]);
                  for temp:=1 to ExcluirContaGerencialTemp.Count-1 do
                  Sql.add('and TabTitulo.Contagerencial<>'+ExcluirContaGerencialTemp[temp]);

                  Sql.add(')');
          End;



          if (Pdata1<>'')
          Then Begin
                    SQL.add('and (tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                    SQL.add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
          End;

          Sql.add('group by tabcontager.nome,tabtitulo.contagerencial,tabsubcontagerencial.nome,tabtitulo.subcontagerencial');
          Sql.add('order by tabcontager.nome,tabtitulo.contagerencial,tabsubcontagerencial.nome,tabtitulo.subcontagerencial');

          FmostraStringList.Memo.Text:=sql.text;
          //FmostraStringList.showmodal;


          open;
          
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum dado foi encontrado!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;

          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'TOTAL POR CONTA GERENCIAL/SUB-CONTA',[negrito]);
          inc(linha,2);

          if (PTipoConta='C')
          Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS RECEBIDAS',[negrito])
          Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS PAGAS',[negrito]);
          inc(linha,1);

          
          FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos de '+Pdata1+' a '+Pdata2,[negrito]);
          inc(linha,2);


          //A Id�ia � assim
          //Conta Gerencial XXXXX
          //    Fornecedor  A       Total Pago
          //    Fornecedor  B       Total Pago
          //    Fornecedor  C       Total Pago
          //Total da Conta  R$ XXXXX


          //Imprimindo a Primeira Contagerencial
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
          inc(linha,1);
          PContager:=Fieldbyname('contagerencial').asstring;
          PSomaConta:=0;
          PSomaTotalgeral:=0;
          TmpQuantPEndencias:=0;
          TmpQuantDevedores:=0;
          While not(Eof) do
          Begin
               if (PContager<>Fieldbyname('contagerencial').asstring)
               Then Begin
                          //totalizando a contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
                          inc(linha,2);
                          //Nova Conta
                          PContager:=Fieldbyname('contagerencial').asstring;
                          PSomaConta:=0;
                          //Contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
                          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
                          inc(linha,1);
               End;

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,8,Completapalavra(fieldbyname('subcontagerencial').asstring,6,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nomesubcontagerencial').asstring,50,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('somaquitacao').asstring),12,' '));
               inc(linha,1);
               PSomaConta:=PSomaConta+fieldbyname('somaquitacao').asfloat;
               PSomaTotalgeral:=PSomaTotalgeral++fieldbyname('somaquitacao').asfloat;
               inc(TmpQuantPEndencias,1);
               next;
          End;
          //totalizando a Contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SOMA TOTAL '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PSomaTotalgeral)),12,' '),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.Fechar;

     End;
Finally
       freeandnil(ContaGerencialTemp);
       FreeAndNil(ObjqueryLocal);
End;
 
end;




procedure TObjTitulo.LancapendenciasTitulos;
var
QueryTMp:Tibquery;
begin
     if (ObjUsuarioGlobal.Get_Nome<>'SYSDBA')
     Then Begin
               Messagedlg('Somente o usu�rio SYSDBA tem permiss�o para usar este procedimento!',mtconfirmation,[mbok],0);
               exit;
     End;

     Try
        QueryTMp:=Tibquery.create(nil);
        QueryTMp.Database:=FDataModulo.IBDatabase;
     Except
           Messagedlg('Erro na tentativa de Criar a Query!',mterror,[mbok],0);
           exit;
     End;
Try     

    //este procedimento foi criado para consertar
    //o problema de titulos que naum tem pendencias
    //devido a um bug no sistema
    With QueryTMP do
    Begin
         close;
         SQL.clear;
         SQL.add('select tabtitulo.codigo from tabtitulo');
         SQL.add('left join tabpendencia');
         SQL.add('on tabpendencia.titulo=tabtitulo.codigo');
         SQL.add('where tabpendencia.codigo is null');
         open;
         last;
         if (Recordcount=0)
         Then Begin
                   Messagedlg('N�o h� t�tulos sem pend�ncias',mtConfirmation,[mbok],0);
                   exit;
         End;
         Messagedlg('Quantidade de T�tulos sem pend�ncias '+inttostr(recordcount),mtinformation,[mbok],0);
         first;
         
         While not(eof) do
         Begin
              Self.LocalizaCodigo(fieldbyname('codigo').asstring);
              Self.TabelaparaObjeto;
              Showmessage(Self.CODIGO+'-'+Self.HISTORICO);
              Self.ParcelasIguais:=True;
              if (Self.Grava_Pendencia=False)
              Then Begin
                        FDataModulo.IBTransaction.RollbackRetaining;
                        MEssagedlg('Erro na tentativa de Atualizar o T�tulo '+Self.codigo+' - '+Self.HISTORICO,mterror,[mbok],0);
                        exit;
              End;
              next;
         End;
         FDataModulo.IBTransaction.CommitRetaining;
         Messagedlg('T�tulos Atualizados com Sucesso!',mtconfirmation,[mbok],0);

    End;

Finally
       FreeAndNil(QueryTMp);
End;

end;

procedure TObjTitulo.ImprimeSaldoTotalCredorDevedor;
var
ReceberPagar:string;
linha:integer;
nomeclientefornecedor:string;
Qlocal:Tibquery;
somatotal:currency;
begin


     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.items.add('T�tulos a Receber');
          RgOpcoes.items.add('T�tulos a Pagar');
          showmodal;
          if (tag=0)
          Then exit;
          if (RgOpcoes.ItemIndex=0)
          Then ReceberPagar:='C'
          Else ReceberPagar:='D';
     End;

     Try
        Qlocal:=TIBQuery.create(nil);
        Qlocal.Database:=FDataModulo.IBDatabase;
     Except
           MEssagedlg('Erro na tentativa de Criar a Query!',mterror,[mbok],0);
           exit;
     End;

Try
     With Qlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('select tabtitulo.credordevedor,tabtitulo.codigocredordevedor,sum(tabpendencia.saldo) as SALDO');
          SQL.add('from tabpendencia');
          SQL.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SQL.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SQL.add('where tabpendencia.saldo>0 and Tabcontager.tipo='+#39+ReceberPagar+#39);
          SQL.add('group by tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
          SQL.add('order by credordevedor,codigocredordevedor');
          Open;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if(FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;

          if (ReceberPagar='C')
          Then FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'CONTAS A RECEBER COM SALDO',[negrito])
          Else FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'CONTAS A PAGAR COM SALDO',[negrito]);
          inc(Linha,2);


          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('CLIENTE/FORNECEDOR',75,' ')+' '+
                                             CompletaPalavra_a_Esquerda('SALDO',12,' '),[negrito]);
          inc(linha,2);
          first;
          somatotal:=0;

          While not(eof) do
          Begin
               nomeclientefornecedor:=Self.Get_NomeCredorDevedor(fieldbyname('credordevedor').AsString,fieldbyname('codigocredordevedor').asstring);
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(nomeclientefornecedor,75,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALDO').asstring),12,' '));
               inc(linha,1);
               somatotal:=somatotal+fieldbyname('SALDO').asfloat;
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,completapalavra('_',90,'_'),[negrito]);
          inc(linha,2);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL R$ '+formata_valor(FloatToStr(somatotal)),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.RDprint.Fechar;

     End;
Finally
       freeandnil(qlocal);
End;
end;


procedure TObjTitulo.ImprimeTitulosEmAtraso_somadoporDia(TiPO_DC: string;Intervalo: Boolean;TrocaHistoricoPorCredorDevedor:Boolean);
var
saida:boolean;
DataLimite:Tdate;
Linha,Temp,ContaGerencialTemp,GeradorTemp,CredorDevedorTemp:Integer;
TempStr,GeradorTExto:String;
CodigoCredorDevedorTemp:TStringList;
ExcluirContaGerencial:TStringList;
tmpsomadia,tmpSomaSaldo:Currency;
lancamentotemp,ptemp,tmptitulo:String;
vencimentoagrupado:string;
begin
     Try
        CodigoCredorDevedorTemp:=TStringList.create;
        ExcluirContaGerencial:=TStringList.Create;
        CodigoCredorDevedorTemp.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "CodigoCredorDevedorTemp"',mterror,[mbok],0);
           exit;
     End;

Try
     
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=Intervalo;

          If (Intervalo=True)
          Then Begin
                LbGrupo02.caption:='Data 02';
                LbGrupo01.caption:='Data 01';
           End
           Else LbGrupo01.caption:='Limite';

          Grupo04.Enabled:=True;
          Grupo05.Enabled:=True;
          Grupo06.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          edtgrupo04.EditMask:='';
          edtgrupo04.OnKeyDown:=Self.EdtcontagerencialKeyDown;
          edtgrupo04.Color:=$005CADFE;
          LbGrupo04.caption:='Conta Gerencial';


          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown;
          edtgrupo05.Color:=$005CADFE;
          LbGrupo05.caption:='Excluir Conta Gerencial';

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;
          
          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
          edtgrupo07.color:=$005CADFE;


          lbgrupo06.caption:='Gerador';
          ComboGrupo06.items.clear;
          Combo2Grupo06.items.clear;
          Self.Get_ListaGerador(ComboGrupo06.items);
          Self.Get_ListaGeradorCodigo(Combo2Grupo06.items);

          showmodal;
          If tag=0
          Then exit;


          If (intervalo=True)
          Then Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                       DataLimite:=Strtodate(edtgrupo02.text);
                    Except
                       Messagedlg('Datas Inv�lidas para o Intervalo!',mterror,[mbok],0);
                       exit;
                    End;

          End
          Else Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                    Except
                       Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                       exit;
                    End;
          End;

          Try
                GeradorTemp:=strtoint(Combo2Grupo06.Items[ComboGrupo06.itemindex]);
                GEradorTExto:='';
                If geradortemp=-1
                Then GeradorTemp:=strtoint('  ')
                Else GeradorTExto:=ComboGrupo06.items[ComboGrupo06.itemindex];

          Except
                GeradorTemp:=-1;
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          //pegando os c�digos dos credores/devedores
          TempStr:='';
          CodigoCredorDevedorTemp.clear;
          for Temp:=1 to length(EdtGrupo07.text) do
          Begin
               If ((edtgrupo07.text[temp]=';')
               or (temp=length(EdtGrupo07.text)))
               Then Begin
                         if ((temp=length(EdtGrupo07.text)) and (edtgrupo07.text[temp]<>';'))
                         Then TempStr:=TempStr+edtgrupo07.text[temp];
                         
                         If (Tempstr<>'')
                         Then Begin
                                   Try
                                        Strtoint(tempstr);
                                        CodigoCredorDevedorTemp.add(Tempstr);
                                   Except
                                        CodigoCredorDevedorTemp.clear;
                                        break;
                                   End;
                         End;
                        TempStr:='';
               End
               Else TempStr:=TempStr+edtgrupo07.text[temp];
          End;


          Try
             ContaGerencialTemp:=Strtoint(edtgrupo04.text);
             If (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False)
             Then ContaGerencialTemp:=-1
             Else Begin
                       Self.CONTAGERENCIAL.TabelaparaObjeto;
                       If (Self.CONTAGERENCIAL.Get_Tipo<>TIPO_DC)
                       Then Begin
                                 Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                                 exit;
                            End;
                  End;
          Except
             ContaGerencialTemp:=-1;
          End;


          Try
             ExplodeStr(edtgrupo05.text,ExcluirContaGerencial,';','INTEGER');
          except
             MensagemErro('Erro ao tentar separar as contas gerenciais que ser�o exclu�das');
             exit;
          end;

     End;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('select tabtitulo.credordevedor,tabtitulo.codigocredordevedor,tabtitulo.codigo as CODTITULO,tabtitulo.NumDcto,tabtitulo.Historico,contagerencial,');
          SelectSql.add('tabpendencia.codigo CODPENDENCIA,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo,tabpendencia.observacao');
          SelectSql.add('from tabpendencia join tabtitulo');
          SelectSql.add('on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add(' where Saldo>0 ');

          If (intervalo=false)
          Then SelectSql.add(' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39)
          Else SelectSql.add(' and (Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39+' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39+')');

          SelectSql.add(' and tabcontager.tipo='+#39+TIPO_DC+#39);

          If (GeradorTemp<>-1)
          Then SelectSql.add(' and Tabtitulo.Gerador='+inttostr(geradortemp));

          If (ContagerencialTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.ContaGerencial='+INttostr(COntaGerencialTemp));

          If (CredorDevedorTemp<>-1)
          Then SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp.count<>0)
          Then Begin
                    SelectSQL.add(' and (Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[0]);
                    for temp:=1 to CodigoCredorDevedorTemp.count-1 do
                    Begin
                         SelectSQL.add(' or Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[temp]);
                    End;
                    SelectSQL.add(' )');
          End;

          if (ExcluirContaGerencial.Count>0)
          then Begin
                    SelectSQL.add(' and (Tabtitulo.ContaGerencial<>'+ExcluirContaGerencial[0]);
                    for temp:=1 to ExcluirContaGerencial.count-1 do
                    Begin
                         SelectSQL.add(' and Tabtitulo.ContaGerencial<>'+ExcluirContaGerencial[temp]);
                    End;
                    SelectSQL.add(' )');
          end;

          SelectSQL.add('order by tabpendencia.vencimento');

          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.RDprint.Abrir;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.ImprimeCabecalho:=True;

          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S20cpp;

          Linha:=3;
          tmptitulo:='';
          If (TIPO_DC='D')
          Then tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A PAGAR DATA '+DATETOSTR(DataLimite)
          Else tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A RECEBER DATA '+DATETOSTR(DataLimite);

          If GeradorTemp<>-1
          Then tmptitulo:=tmptitulo+' - '+geradortexto;

          FreltxtRDPRINT.RDprint.ImpC(linha,65,tmptitulo,[negrito]);
          inc(linha,2);
                    
          If (ContaGerencialTemp<>-1)
          Then Begin
                    FreltxtRDPRINT.RDprint.Imp(linha,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)));
                    inc(linha,1);
          End;

          If (CredorDevedorTemp<>-1)
          Then Begin
                    If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                    Then Begin
                           Self.CREDORDEVEDOR.TabelaparaObjeto;
                           tmptitulo:=Self.CredorDevedor.Get_nome;
                           inc(linha,1);
                           IF (CodigoCredorDevedorTemp.count=1)
                           Then tmptitulo:=tmptitulo+'='+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),CodigoCredorDevedorTemp[0])
                           Else Begin
                                   If (CodigoCredorDevedorTemp.count>1)
                                   Then tmptitulo:=tmptitulo+'=V�RIOS';
                           End;
                           FreltxtRDPRINT.RDprint.Imp(linha,1,tmptitulo);
                           inc(linha,1);
                    End;
          End;

          inc(linha,1);
          FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('T�TULO',6,' ')+' '+
                                CompletaPalavra('HIST�RICO',35,' ')+' '+
                                CompletaPalavra('N�DCTO',12,' ')+' '+
                                CompletaPalavra('CNTGER',6,' ')+' '+
                                CompletaPalavra('PEND.',6,' ')+' '+
                                CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                CompletaPalavra_a_Esquerda('SALDO',15,' ')+' '+
                                CompletaPalavra('OBSERVA��O',32,' '),
                                [negrito]);
          inc(linha,2);
          tmpSomaSaldo:=0;
          tmpsomadia:=0;
          vencimentoagrupado:=fieldbyname('vencimento').asstring;

          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'DATA  '+fieldbyname('vencimento').asstring,[negrito]);
          inc(linha,2);

          While not eof do
          Begin
               if (vencimentoagrupado<>fieldbyname('vencimento').asstring)//mudou o dia
               Then Begin
                         inc(linha,1);
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL DO DIA '+vencimentoagrupado+' R$ '+formata_valor(FloatToStr(tmpsomadia)),[negrito]);
                         inc(linha,2);
                         vencimentoagrupado:=fieldbyname('vencimento').asstring;
                         tmpsomadia:=0;

                         inc(linha,1);
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'DATA  '+fieldbyname('vencimento').asstring,[negrito]);
                         inc(linha,2);
               End;

               Ptemp:='';
               if (TrocaHistoricoPorCredorDevedor=True)
               Then Ptemp:=Self.CREDORDEVEDOR.Get_RazaoSocialCredorDevedor(FIELDBYNAME('credordevedor').ASSTRING,FIELDBYNAME('codigocredordevedor').ASSTRING)
               Else PTemp:=FIELDBYNAME('HISTORICO').ASSTRING;

               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(FIELDBYNAME('CODTITULO').ASSTRING,6,' ')+' '+
                                CompletaPalavra(ptemp,35,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('NUMDCTO').ASSTRING,12,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CONTAGERENCIAL').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CODPENDENCIA').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('VENCIMENTO').ASSTRING,10,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDO').ASSTRING),15,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('OBSERVACAO').ASSTRING,32,' '));
               inc(linha,1);
               If (linha>=(FreltxtRDPRINT.RDprint.TamanhoQteLinhas-3))
               Then Begin
                        FreltxtRDPRINT.RDprint.Novapagina;
                        Linha:=3;
               End;
               tmpSomaSaldo:=tmpSomaSaldo+FIELDBYNAME('SALDO').asfloat;
               tmpSomaDia:=tmpSomaDia+FIELDBYNAME('SALDO').asfloat;
               NEXT;
          end;
          //***********totalizando no final do rel ********

          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL DO DIA '+vencimentoagrupado+' R$ '+formata_valor(FloatToStr(tmpsomadia)),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',92,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL DO PERIODO R$ '+formata_valor(FloatToStr(tmpSomaSaldo)),[negrito]);
          FreltxtRDPRINT.rdprint.fechar;
     End;

Finally
       Freeandnil(CodigoCredorDevedorTemp);
       FreeAndNil(ExcluirContaGerencial);
End;

end;

procedure TObjTitulo.Imprime_Pagar_por_Dia_com_Cheque;
var
  saida :Boolean;
  DataLimite1, DataLimite2 :string;
  cont, Linha, Temp, ContaGerencialTemp, CredorDevedorTemp :Integer;
  TempStr :String;
  CodigoCredorDevedorTemp :TStringList;
  TmpSomaTitulo, TmpSomaCheque, TmpSomaGeralTitulo, TmpsomaGeralCheque :Currency;
  tmptitulo :String;
  vencimentoagrupado :string;
  ObjQueryTalaoCheque :Tibquery;
  SqltituloInicial, SqltituloData, SqltituloFinal :String;
  SqlchequeInicial, SqlchequeData, SqlchequeFinal :String;
  strAux : string;
begin

  try
    ObjQueryTalaoCheque:=Tibquery.create(nil);
    ObjQueryTalaoCheque.Database:=FDataModulo.IBDatabase;

    CodigoCredorDevedorTemp:=TStringList.create;
    CodigoCredorDevedorTemp.clear;
  except
    Messagedlg('Erro na tentativa de Criar a StringList "CodigoCredorDevedorTemp"',mterror,[mbok],0);
    exit;
  end;

  try

    Limpaedit(Ffiltroimp);
    with FfiltroImp do
    begin
      DesativaGrupos;

      Grupo01.Enabled:=True;
      Grupo02.Enabled:=True;
      Grupo05.Enabled:=True;
      Grupo07.Enabled:=True;

      LbGrupo01.caption:='Data 01';
      LbGrupo02.caption:='Data 02';
      edtgrupo01.EditMask:='!99/99/9999;1;_';
      edtgrupo02.EditMask:='!99/99/9999;1;_';

      edtgrupo05.EditMask:='';
      edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown_Unico;
      edtgrupo05.color:=$005CADFE;
      LbGrupo05.caption:='Conta Gerencial';

      LbGrupo07.caption:='Credor/Devedor';
      ComboGrupo07.Items.clear;
      Combo2Grupo07.Items.clear;

      Self.Get_ListacredorDevedor(ComboGrupo07.items);
      Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
      edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
      edtgrupo07.Color:=$005CADFE;

      showmodal;

      if tag=0
      then exit;

      try
        DataLimite1:='';
        strtodate(edtgrupo01.text);
        DataLimite1:=edtgrupo01.text;
      except
      end;

      try
        DataLimite2:='';
        strtodate(edtgrupo02.text);
        DataLimite2:=edtgrupo02.text;
      except
      end;

      try
        CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
      except
        CredorDevedorTemp:=-1;
      end;

      //pegando os c�digos dos credores/devedores
      TempStr:='';
      CodigoCredorDevedorTemp.clear;
      for Temp:=1 to length(EdtGrupo07.text) do
      begin

        if ((edtgrupo07.text[temp]=';') or (temp=length(EdtGrupo07.text))) then
        begin
          if ((temp=length(EdtGrupo07.text)) and (edtgrupo07.text[temp]<>';'))
          then TempStr:=TempStr+edtgrupo07.text[temp];

          if (Tempstr<>'') then
          begin
            try
              Strtoint(tempstr);
              CodigoCredorDevedorTemp.add(Tempstr);
            except
              CodigoCredorDevedorTemp.clear;
              break;
            end;
          end;
          TempStr:='';
        end
        else TempStr:=TempStr+edtgrupo07.text[temp];
      end;


      try
        ContaGerencialTemp:=Strtoint(edtgrupo05.text);

        if (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False) then
          ContaGerencialTemp:=-1
        else
        begin
          Self.CONTAGERENCIAL.TabelaparaObjeto;
          
          if (Self.CONTAGERENCIAL.Get_Tipo<>'D') then
          begin
            Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
            exit;
          end;

        end;

      except
        ContaGerencialTemp:=-1;
      end;
    end;


    with Self.ObjDataset do
    begin
      close;
      SelectSQL.clear;
      SelectSql.add('select tabtitulo.codigo as CODTITULO,tabtitulo.NumDcto,tabtitulo.Historico,contagerencial,');
      SelectSql.add('tabpendencia.codigo CODPENDENCIA,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo');
      SelectSql.add('from tabpendencia join tabtitulo');
      SelectSql.add('on tabpendencia.titulo=tabtitulo.codigo');
      SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
      SelectSql.add('where tabpendencia.Saldo>0 ');
      SelectSql.add('and tabcontager.tipo='+#39+'D'+#39);

      if (ContagerencialTemp <> -1)
      then SelectSQL.add(' and TabTitulo.ContaGerencial='+INttostr(COntaGerencialTemp));

      if (CredorDevedorTemp <> -1)
      then SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

      if(CodigoCredorDevedorTemp.count<>0) then
      begin
        SelectSQL.add(' and (Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[0]);
        for temp:=1 to CodigoCredorDevedorTemp.count-1 do
        begin
          SelectSQL.add(' or Tabtitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp[temp]);
        end;
        SelectSQL.add(' )');
      end;

      SqltituloInicial:='';
      SqltituloData:='';
      SqltituloFinal:='';
      SqltituloInicial:=SelectSQL.Text;

      if (DataLimite1<>'')
      then SqltituloData:=SqltituloData+' and Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39;

      if (DataLimite2<>'')
      then SqltituloData:=SqltituloData+' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39;

      SelectSQL.add(SqltituloData);
      SqltituloFinal:='order by tabpendencia.vencimento';
      SelectSQL.add(SqltituloFinal);

      //InputBox('titulos','',SelectSQL.Text);

      open;

      {Selecionando os cheques emitidos que n�o foram descontados
      que vencem no intervalo estipulado}
      ObjQueryTalaoCheque.close;
      ObjQueryTalaoCheque.sql.clear;
      ObjQueryTalaoCheque.sql.add('select TTl.codigo,TTl.portador,tabportador.nome as NOMEPORTADOR,');
      ObjQueryTalaoCheque.sql.add('TTl.numero,TTl.valor,TTl.vencimento,TTL.lancamento,tablancamento.pendencia,tabpendencia.titulo,tabtitulo.historico');
      ObjQueryTalaoCheque.sql.add('from tabtalaodecheques TTl');
      ObjQueryTalaoCheque.sql.add('join tabportador on TTl.portador=tabportador.codigo');
      ObjQueryTalaoCheque.sql.add('join tablancamento on TTL.lancamento=tablancamento.codigo');
      ObjQueryTalaoCheque.sql.add('left join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
      ObjQueryTalaoCheque.sql.add('left join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');

          
      ObjQueryTalaoCheque.sql.add('where descontado=''N'' ');

      SqlchequeInicial:='';
      SqlchequeData:='';
      SqlchequeFinal:='';

      SqlchequeInicial:=ObjQueryTalaoCheque.SQL.Text;

      if (DataLimite1<>'')
      then SqlchequeData:=SqlchequeData+' and TTL.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39;

      if (DataLimite2<>'')
      then SqlchequeData:=SqlchequeData+' and TTL.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39;

      ObjQueryTalaoCheque.sql.add(SqlchequeData);
      SqlchequeFinal:='order by TTL.vencimento';

      ObjQueryTalaoCheque.sql.add(SqlchequeFinal);

      //InputBox('cheques','',ObjQueryTalaoCheque.SQL.Text);

      ObjQueryTalaoCheque.open;

      //Vou resgatar os vencimentos dos dois casos, titulos e cheques
      //e por numa stringlist
      //depois ordenar e deixar somente com datas agrupadas
      //para usar na impressao

      //Este clear estava dentro do if abaixo, por�m se o sql n�o trouxer nenhum registro esse stringlist nao ser� limpo
      //e quando ele for usado novamente nos trechos de c�digo abaixo haver� erro.. Por isso foi retirado de l�..
      CodigoCredorDevedorTemp.Clear;
      
      //preenchimento com o vencto dos titulos
      if (recordcount>0) then
      begin
        first;
        //vencimentoagrupado:=fieldbyname('vencimento').AsString;
        //CodigoCredorDevedorTemp.Add(vencimentoagrupado);
        //next;
        while not(eof) do
        begin
          {if (vencimentoagrupado<>fieldbyname('vencimento').asstring)
          Then Begin
            CodigoCredorDevedorTemp.Add(vencimentoagrupado);
            vencimentoagrupado:=fieldbyname('vencimento').AsString;
          End;}
          
          if (CodigoCredorDevedorTemp.IndexOf(fieldbyname('vencimento').AsString)=-1) then
            CodigoCredorDevedorTemp.Add(fieldbyname('vencimento').AsString);

          next;
        end;
      end;

      //vencimentos dos cheques
      if (ObjQueryTalaoCheque.RecordCount>0) then
      begin
        ObjQueryTalaoCheque.first;
        {vencimentoagrupado:=;
        CodigoCredorDevedorTemp.Add(vencimentoagrupado);
        ObjQueryTalaoCheque.next;}

        while not(ObjQueryTalaoCheque.eof) do
        begin
          if (CodigoCredorDevedorTemp.IndexOf(ObjQueryTalaoCheque.fieldbyname('vencimento').AsString)=-1)
          then CodigoCredorDevedorTemp.Add(ObjQueryTalaoCheque.fieldbyname('vencimento').AsString);

          {if (vencimentoagrupado<>ObjQueryTalaoCheque.fieldbyname('vencimento').asstring)
          Then Begin
                 vencimentoagrupado:=ObjQueryTalaoCheque.fieldbyname('vencimento').AsString;

          End;}
          ObjQueryTalaoCheque.next;
        end;
      end;

      //filtrando as datas
      if(CodigoCredorDevedorTemp.Count=0) then
      begin
        MensagemErro('N�o existem contas com saldo para o per�odo estipulado');
        Exit;
      End;

      Uessencialglobal.Filtradatas(CodigoCredorDevedorTemp);

      first;
      FreltxtRDPRINT.ConfiguraImpressao;
      FreltxtRDPRINT.RDprint.Abrir;
      Linha:=3;

      if (Freltxtrdprint.rdprint.setup=false) then
      begin
        Freltxtrdprint.rdprint.fechar;
        exit;
      end;

      FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A PAGAR E CHEQUES A COMPENSAR',[negrito]);
      inc(linha,2);

      if (ContaGerencialTemp <> -1) then
      begin
        FreltxtRDPRINT.RDprint.Imp(linha,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)));
        inc(linha,1);
      end;

      inc(linha,1);
      FreltxtRDPRINT.RDprint.ImpF(linha,1,
        'T�TULOS '+
        CompletaPalavra('HIST�RICO',48,' ')+' '+
        CompletaPalavra_a_Esquerda('PEND�NCIA',15,' ')+' '+
        CompletaPalavra_a_Esquerda('SALDO',12,' '),
        [negrito]);

      inc(linha,1);

      FreltxtRDPRINT.RDprint.ImpF(linha,1,'CHEQUE  '+
        Completapalavra('PORTADOR',20,' ')+' '+
        CompletaPalavra_a_Esquerda('NUMERO',10,' ')+' '+
        CompletaPalavra_a_Esquerda('LANCTO',06,' ')+' '+
        CompletaPalavra('PENDENCIA/HISTORICO TITULO',27,' ')+' '+
        CompletaPalavra_a_Esquerda('VALOR',12,' '),[NEGRITO]);

      inc(linha,1);
      FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('_',90,'_'));
      inc(linha,2);

      TmpSomaGeraltitulo:=0;
      TmpsomaGeralCheque:=0;
      for cont:=0 to CodigoCredorDevedorTemp.count -1 do
      Begin
        //esse laco percorre as datas
        //para data primeiro localizo os titulos
        //depois os cheques q vaum cair


        inc(linha,1);
        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
        FreltxtRDPRINT.RDprint.Impf(linha,1,'DATA '+CodigoCredorDevedorTemp[cont],[negrito]);
        inc(linha,1);

        //***********localizando os titulos deste vencimento************
        Self.ObjDataset.Close;
        Self.ObjDataset.SelectSQL.clear;
        Self.ObjDataset.SelectSQL.add(SqltituloInicial);
        strAux := CodigoCredorDevedorTemp[cont];
        Self.ObjDataset.SelectSQL.add('and Tabpendencia.Vencimento='+#39+formatdatetime('mm/dd/yyyy',strtodate(CodigoCredorDevedorTemp[cont]))+#39);
        Self.ObjDataset.SelectSQL.add(SqltituloFinal);

        //InputBox('', '' , ObjDataset.SelectSQL.Text);
        Self.ObjDataset.open;

        TmpSomaTitulo:=0;
        if(Self.ObjDataset.recordcount>0) then
        begin
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'T�TULOS',[italico]);
          inc(linha,1);

          Self.ObjDataset.first;
          while not(Self.ObjDataset.eof) do
          begin
            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
            FreltxtRDPRINT.RDprint.Imp(linha,9,CompletaPalavra(FIELDBYNAME('HISTORICO').ASSTRING,48,' ')+' '+
              CompletaPalavra_a_Esquerda(FIELDBYNAME('CODPENDENCIA').ASSTRING,15,' ')+' '+
              CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDO').ASSTRING),12,' '));
            inc(linha,1);
            TmpSomaTitulo:=TmpSomaTitulo+FIELDBYNAME('SALDO').asfloat;
            TmpSomaGeralTitulo:=TmpSomaGeralTitulo+FIELDBYNAME('SALDO').asfloat;
            Self.ObjDataset.NEXT;
          end;
          //imprimindo o total dos titulo
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,74,CompletaPalavra_a_Esquerda(formata_valor(floattostr(TmpSomaTitulo)),12,' '),[negrito]);
          inc(linha,1);
        end;

        //***********localizando os cheques deste vencimento************
        ObjQueryTalaoCheque.Close;
        ObjQueryTalaoCheque.SQL.clear;
        ObjQueryTalaoCheque.SQL.add(SqlchequeInicial);
        ObjQueryTalaoCheque.SQL.add('and TTL.vencimento='+#39+formatdatetime('mm/dd/yyyy',strtodate(CodigoCredorDevedorTemp[cont]))+#39);
        ObjQueryTalaoCheque.SQL.add(SqlchequeFinal);
        ObjQueryTalaoCheque.Open;
        TmpSomaCheque:=0;

        if (ObjQueryTalaoCheque.RecordCount>0) then
        begin
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'CHEQUES',[italico]);
          inc(linha,1);
          ObjQueryTalaoCheque.first;

          while not(ObjQueryTalaoCheque.eof) do
          begin
            //com o lancamento resgat
            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
            FreltxtRDPRINT.RDprint.Imp(linha,9,completapalavra(ObjQueryTalaoCheque.fieldbyname('portador').asstring+'-'+ObjQueryTalaoCheque.fieldbyname('nomeportador').asstring,20,' ')+' '+
                                            CompletaPalavra_a_Esquerda(ObjQueryTalaoCheque.fieldbyname('numero').asstring,10,' ')+' '+
                                            completapalavra(ObjQueryTalaoCheque.fieldbyname('lancamento').asstring,6,' ')+' '+
                                            completapalavra(ObjQueryTalaoCheque.fieldbyname('pendencia').asstring+'-'+ObjQueryTalaoCheque.fieldbyname('historico').asstring,27,' ')+' '+
                                            CompletaPalavra_a_Esquerda(formata_valor(ObjQueryTalaoCheque.fieldbyname('valor').asstring),12,' '));

            inc(linha,1);
            TmpSomaCheque:=TmpSomaCheque+ObjQueryTalaoCheque.FIELDBYNAME('valor').asfloat;
            TmpSomaGeralCheque:=TmpSomaGeralCheque+ObjQueryTalaoCheque.FIELDBYNAME('valor').asfloat;
            ObjQueryTalaoCheque.NEXT;
          end;

          //imprimindo o total dos cheques
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,74,CompletaPalavra_a_Esquerda(formata_valor(floattostr(TmpSomaCheque)),12,' '),[negrito]);
          inc(linha,1);

        end;

        //totalizando o dia
        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
        FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL DO DIA '+CodigoCredorDevedorTemp[cont]+' R$ '+formata_valor(FloatToStr(TmpSomaTitulo+TmpSomaCheque)),[negrito]);
        inc(linha,2);
      end;//cont

      //***********totalizando no final do rel ********
      FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',92,'_'));
      inc(linha,1);
      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL EM T�TULOS  R$ '+formata_valor(FloatToStr(TmpSomaGeralTitulo)),[negrito]);
      inc(linha,1);
      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL EM CHEQUES  R$ '+formata_valor(FloatToStr(TmpsomaGeralCheque)),[negrito]);
      inc(linha,1);
      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
      FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL FINAL       R$ '+formata_valor(FloatToStr(TmpSomaGeralTitulo+TmpsomaGeralCheque)),[negrito]);
      inc(linha,1);

      FreltxtRDPRINT.rdprint.fechar;
    end;

  finally
    Freeandnil(CodigoCredorDevedorTemp);
    Freeandnil(ObjQueryTalaoCheque);
  end;
end;


procedure TObjTitulo.Imprime_Titulos_Em_Atraso_somado_por_Cidade_e_Dia(TiPO_DC: string;
  Intervalo: Boolean);
var
saida:boolean;
DataLimite:Tdate;
Linha,Temp,ContaGerencialTemp,CredorDevedorTemp:Integer;
TempStr:String;
CodigoCredorDevedorTemp:TStringList;
tmpsomadia,tmpSomaSaldo:Currency;
tmpcidade,tmptitulo:String;
vencimentoagrupado:string;
begin
     Try
        CodigoCredorDevedorTemp:=TStringList.create;
        CodigoCredorDevedorTemp.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "CodigoCredorDevedorTemp"',mterror,[mbok],0);
           exit;
     End;

Try
     
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=Intervalo;

          If (Intervalo=True)
          Then Begin
                LbGrupo02.caption:='Data 02';
                LbGrupo01.caption:='Data 01';
           End
           Else LbGrupo01.caption:='Limite';




          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          Grupo05.Enabled:=True;
          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.EdtcontagerencialKeyDown;
          LbGrupo05.caption:='Conta Gerencial';
          edtgrupo05.color:=$005CADFE;

          Grupo07.Enabled:=True;
          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown;
          edtgrupo07.Color:=$005CADFE;

          showmodal;
          If tag=0
          Then exit;


          If (intervalo=True)
          Then Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                       DataLimite:=Strtodate(edtgrupo02.text);
                    Except
                       Messagedlg('Datas Inv�lidas para o Intervalo!',mterror,[mbok],0);
                       exit;
                    End;

          End
          Else Begin
                    Try
                       DataLimite:=Strtodate(edtgrupo01.text);
                    Except
                       Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                       exit;
                    End;
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          //pegando os c�digos dos credores/devedores
          TempStr:='';
          CodigoCredorDevedorTemp.clear;
          for Temp:=1 to length(EdtGrupo07.text) do
          Begin
               If ((edtgrupo07.text[temp]=';')
               or (temp=length(EdtGrupo07.text)))
               Then Begin
                         if ((temp=length(EdtGrupo07.text)) and (edtgrupo07.text[temp]<>';'))
                         Then TempStr:=TempStr+edtgrupo07.text[temp];
                         
                         If (Tempstr<>'')
                         Then Begin
                                   Try
                                        Strtoint(tempstr);
                                        CodigoCredorDevedorTemp.add(Tempstr);
                                   Except
                                        CodigoCredorDevedorTemp.clear;
                                        break;
                                   End;
                         End;
                        TempStr:='';
               End
               Else TempStr:=TempStr+edtgrupo07.text[temp];
          End;


          Try
             ContaGerencialTemp:=Strtoint(edtgrupo05.text);
             If (Self.CONTAGERENCIAL.LocalizaCodigo(inttostr(ContaGerencialTemp))=False)
             Then ContaGerencialTemp:=-1
             Else Begin
                       Self.CONTAGERENCIAL.TabelaparaObjeto;
                       If (Self.CONTAGERENCIAL.Get_Tipo<>TIPO_DC)
                       Then Begin
                                 Messagedlg(' A Conta Gerencial escolhida difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                                 exit;
                            End;
                  End;
          Except
             ContaGerencialTemp:=-1;
          End;
     End;


     With Self.ObjDataset do
     Begin

          close;
          SelectSQL.clear;
          SelectSql.add('select * from proctitulopendencias ');
          SelectSql.add('where cODIGO<>-100 and SaldoPendencia>0');

          If (intervalo=false)
          Then SelectSql.add(' and vencimentoPENDENCIA<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39)
          Else SelectSql.add(' and (vencimentopendencia>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo01.text))+#39+' and vencimentopendencia<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Ffiltroimp.edtgrupo02.text))+#39+')');

          SelectSql.add(' and tipo='+#39+TIPO_DC+#39);

          If (ContagerencialTemp<>-1)
          Then SelectSQL.add(' and ContaGerencial='+INttostr(COntaGerencialTemp));

          If (CredorDevedorTemp<>-1)
          Then SelectSQL.add(' and CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp.count<>0)
          Then Begin
                    SelectSQL.add(' and (CodigoCredorDevedor='+CodigoCredorDevedorTemp[0]);
                    for temp:=1 to CodigoCredorDevedorTemp.count-1 do
                    Begin
                         SelectSQL.add(' or CodigoCredorDevedor='+CodigoCredorDevedorTemp[temp]);
                    End;
                    SelectSQL.add(' )');
          End;
          SelectSQL.add('order by Cd_cidade,vencimentopendencia');


          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.RDprint.Abrir;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S20cpp;

          Linha:=3;
          tmptitulo:='';
          If (TIPO_DC='D')
          Then tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A PAGAR DATA '+DATETOSTR(DataLimite)
          Else tmptitulo:=Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A RECEBER DATA '+DATETOSTR(DataLimite);

          
          FreltxtRDPRINT.RDprint.ImpC(linha,45,tmptitulo,[negrito]);
          inc(linha,2);

          If (ContaGerencialTemp<>-1)
          Then Begin
                    FreltxtRDPRINT.RDprint.Imp(linha,1,'Conta Gerencial->'+Self.Get_NomeContaGerencial(InttoStr(ContaGerencialTemp)));
                    inc(linha,1);
          End;

          If (CredorDevedorTemp<>-1)
          Then Begin
                    If (Self.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                    Then Begin
                           Self.CREDORDEVEDOR.TabelaparaObjeto;
                           tmptitulo:=Self.CredorDevedor.Get_nome;
                           inc(linha,1);
                           IF (CodigoCredorDevedorTemp.count=1)
                           Then tmptitulo:=tmptitulo+'='+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),CodigoCredorDevedorTemp[0])
                           Else Begin
                                   If (CodigoCredorDevedorTemp.count>1)
                                   Then tmptitulo:=tmptitulo+'=V�RIOS';
                           End;
                           FreltxtRDPRINT.RDprint.Imp(linha,1,tmptitulo);
                           inc(linha,1);
                    End;
          End;

          inc(linha,1);
          FreltxtRDPRINT.RDprint.ImpF(linha,5,CompletaPalavra('T�TULO',6,' ')+' '+
                                CompletaPalavra('HIST�RICO',35,' ')+' '+
                                CompletaPalavra('N�DCTO',6,' ')+' '+
                                CompletaPalavra('CNTGER',6,' ')+' '+
                                CompletaPalavra('PEND.',6,' ')+' '+
                                CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                CompletaPalavra('SALDO',9,' '),
                                [negrito]);
          inc(linha,2);
          tmpSomaSaldo:=0;
          tmpsomadia:=0;
          vencimentoagrupado:=fieldbyname('vencimentopendencia').asstring;
          tmpcidade:=trim(uppercase(fieldbyname('cd_cidade').asstring));
          //Nome da Primeira Cidade
          FreltxtRDPRINT.RDprint.ImpF(linha,1,'CIDADE: '+tmpcidade,[negrito]);
          inc(linha,2);

          While not eof do
          Begin

               if (tmpcidade<>trim(uppercase(fieldbyname('cd_cidade').asstring)))
               Then Begin
                         inc(linha,1);
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,5,'TOTAL DO DIA '+vencimentoagrupado+' R$ '+formata_valor(FloatToStr(tmpsomadia)),[negrito]);
                         inc(linha,2);
                         vencimentoagrupado:=fieldbyname('vencimentopendencia').asstring;
                         tmpsomadia:=0;
                         //****************************************************************
                         inc(linha,1);
                         tmpcidade:=trim(uppercase(fieldbyname('cd_cidade').asstring));
                         //Nome da Primeira Cidade
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.ImpF(linha,1,'CIDADE: '+tmpcidade,[negrito]);
                         inc(linha,2);
               End
               Else Begin

                         if (vencimentoagrupado<>fieldbyname('vencimentopendencia').asstring)//mudou o dia
                         Then Begin
                                   inc(linha,1);
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,5,'TOTAL DO DIA '+vencimentoagrupado+' R$ '+formata_valor(FloatToStr(tmpsomadia)),[negrito]);
                                   inc(linha,2);
                                   vencimentoagrupado:=fieldbyname('vencimentopendencia').asstring;
                                   tmpsomadia:=0;
                         End;
               End;

               FreltxtRDPRINT.RDprint.Imp(linha,5,CompletaPalavra(FIELDBYNAME('codigo').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('HISTORICO').ASSTRING,35,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('NUMDCTO').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('CONTAGERENCIAL').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('PENDENCIA').ASSTRING,6,' ')+' '+
                                CompletaPalavra(FIELDBYNAME('VENCIMENTOPENDENCIA').ASSTRING,10,' ')+' '+
                                CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDOPENDENCIA').ASSTRING),9,' '));
               inc(linha,1);
               If (linha>=(FreltxtRDPRINT.RDprint.TamanhoQteLinhas-3))
               Then Begin
                        FreltxtRDPRINT.RDprint.Novapagina;
                        Linha:=3;
               End;
               tmpSomaSaldo:=tmpSomaSaldo+FIELDBYNAME('SALDOPENDENCIA').asfloat;
               tmpSomaDia:=tmpSomaDia+FIELDBYNAME('SALDOPENDENCIA').asfloat;
               NEXT;
          end;
          //***********totalizando no final do rel ********

          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,5,'TOTAL DO DIA '+vencimentoagrupado+' R$ '+formata_valor(FloatToStr(tmpsomadia)),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',92,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'TOTAL R$ '+formata_valor(FloatToStr(tmpSomaSaldo)),[negrito]);
          FreltxtRDPRINT.rdprint.fechar;
     End;

Finally
       Freeandnil(CodigoCredorDevedorTemp);
End;

end;





Function TObjTitulo.VerificaseTemLote(Ptitulo:string;Pquery:tibquery):boolean;
Begin
     result:=False;
     With Pquery do
     begin
          close;
          sql.clear;
          sql.add('Select count(codigo) as codigo from tablancamento');
          sql.add('join tabpendencia on tabpendencia.codigo=tablancamento.pendencia');
          sql.add('where tabpendencia.titulo='+ptitulo);
          sql.add('and not (TabLancamento.lancamentopai is null)');
          open;
          if (fieldbyname('codigo').asinteger>0)
          Then result:=true;

          close;
     End;
End;

Function TObjTitulo.Apaga_sem_Saldo_Periodo(etapa:integer):Boolean;
var
  Pdata:Tdate;
  Pquery,PQuerylote:TibQuery;
  ptitulos:TStringLIst;
  ObjLancamento:tobjLancamento;
  cont:integer;
  Parquivo:TextFile;
begin
     Result:=False;
     
     //este procedimento foi encomendado pelo Lopes&Mporgado/PR
     //para excluir titulos antigos


     limpaedit(FfiltroImp);

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.editmask:='!99/99/9999;1;_';
          LbGrupo01.Caption:='T�tulos e Lan�amentos Inferiores a ';
          showmodal;
          if (tag=0)
          Then exit;

          Try
             Pdata:=strtodate(edtgrupo01.text);
          Except
                Messagedlg('Data Inv�lida!',mterror,[mbok],0);
                exit;
          End;
     End;

     Try
        Pquery:= TIBQuery.Create(nil);
        Pquery.database:=FDataModulo.IBDatabase;
        PQuerylote:= TIBQuery.Create(nil);
        PqueryLote.database:=FDataModulo.IBDatabase;
        ptitulos:=TStringLIst.create;
        objlancamento:=TObjLancamento.Create;
        AssignFile(Parquivo,'C:\TITULOSEXCLUIDOS.TXT');
        Rewrite(parquivo);
     Except
           Messagedlg('Erro na Tentativa de Criar a Query!',mterror,[mbok],0);
           exit;
     End;

     Try
        With Pquery do
        Begin

             if (etapa=1)
             or (etapa=0)
             Then Begin
                   //Esses titulos sao titulos sem saldo
                   //que nao foram pagos em lotes
                   close;
                   sql.clear;
                   sql.add('Select distinct(Tabtitulo.codigo) as titulo');
                   sql.add('from tablancamento');
                   sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                   sql.add('join Tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                   sql.add('join proctitulosaldo on tabtitulo.codigo=proctitulosaldo.titulo');
                   sql.add('where tablancamento.lancamentopai is null');
                   sql.add('and proctitulosaldo.saldo=0');
                   sql.add('and tabtitulo.emissao<'+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                   
                   open;
                   last;
                   
                   
                   FMostraBarraProgresso.Lbmensagem.caption:='Excluindo T�tulos';
                   FMostraBarraProgresso.BarradeProgresso.MinValue:=0;
                   FMostraBarraProgresso.BarradeProgresso.MaxValue:=RecordCount;
                   FMostraBarraProgresso.BarradeProgresso.Progress:=0;
                   FMostraBarraProgresso.show;
                   first;
                   
                   While not(eof) do
                   Begin
                        FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
                        FMostraBarraProgresso.Lbmensagem.Repaint;
                        FMostraBarraProgresso.show;
                        FMostraBarraProgresso.repaint;
                   
                        if (Self.Verificasetemlote(fieldbyname('titulo').asstring,PQuerylote)=False)
                        Then Begin
                                Writeln(parquivo,'T�TULO='+fieldbyname('titulo').asstring);
                                //exclui apenas os que nao sao em lote
                                if (ObjLancamento.ExcluiTitulo(fieldbyname('titulo').asstring,false)=False)
                                Then Begin
                                        Messagedlg('Erro na tentativa de Exclus�o do T�tulo N� '+fieldbyname('titulo').asstring+#13+'Os T�tulos exclu�dos ser�o retornados ao sistema',mterror,[mbok],0);
                                        exit;
                                End;
                        End;
                        next;
                   End;
                   FMostraBarraProgresso.hide;
             End;

             if (etapa=2)
             or (etapa=0)
             Then Begin
                      //Anotando os titulos que serao excluidos depois
                      close;
                      sql.clear;
                      sql.add('Select');
                      sql.add('distinct(tabtitulo.codigo) as titulo');
                      sql.add('from tablancamento');
                      sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                      sql.add('join Tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                      sql.add('join proctitulosaldo on tabtitulo.codigo=proctitulosaldo.titulo');
                      sql.add('where not (tablancamento.lancamentopai is null)');
                      sql.add('and proctitulosaldo.saldo=0');
                      sql.add('and tabtitulo.emissao<'+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                      open;
                      last;
                      FMostraBarraProgresso.Lbmensagem.caption:='Copiando T�tulos de lotes';
                      FMostraBarraProgresso.BarradeProgresso.MinValue:=0;
                      FMostraBarraProgresso.BarradeProgresso.MaxValue:=RecordCount;
                      FMostraBarraProgresso.BarradeProgresso.Progress:=0;
                      FMostraBarraProgresso.show;
                      first;
                      ptitulos.clear;
                      While not(eof) do
                      Begin
                           FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
                           FMostraBarraProgresso.Lbmensagem.Repaint;
                           FMostraBarraProgresso.show;
                           ptitulos.add(fieldbyname('titulo').asstring);
                           next;
                      End;
                      FMostraBarraProgresso.hide;
             End;

             if (etapa=3)
             or (etapa=0)
             Then Begin
                      //Excluindo os Lancamentos em lote
                      close;
                      sql.clear;
                      sql.add('Select');
                      sql.add('distinct(tablancamento.lancamentopai)');
                      sql.add('from tablancamento');
                      sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                      sql.add('join Tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                      sql.add('join proctitulosaldo on tabtitulo.codigo=proctitulosaldo.titulo');
                      sql.add('where not (tablancamento.lancamentopai is null)');
                      sql.add('and proctitulosaldo.saldo=0');
                      sql.add('and tabtitulo.emissao<'+#39+formatdatetime('mm/dd/yyyy',pdata)+#39);
                      open;
                      last;
                      FMostraBarraProgresso.Lbmensagem.caption:='Excluindo os Lan�amentos em Lotes';
                      FMostraBarraProgresso.BarradeProgresso.MinValue:=0;
                      FMostraBarraProgresso.BarradeProgresso.MaxValue:=RecordCount;
                      FMostraBarraProgresso.BarradeProgresso.Progress:=0;
                      FMostraBarraProgresso.show;
                      first;
                      While not(eof) do
                      Begin
                           FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
                           FMostraBarraProgresso.Lbmensagem.Repaint;
                           FMostraBarraProgresso.show;
                           Writeln(parquivo,'LAN�AMENTO LOTE='+fieldbyname('LANCAMENTOPAI').asstring);
                           flush(parquivo);

                           if (ObjLancamento.ApagaLancamento(fieldbyname('lancamentopai').asstring,false)=False)
                           Then Begin
                                     Messagedlg('Erro na tentativa de Excluir o lan�amento Pai n� '+fieldbyname('lancamentopai').asstring,mterror,[mbok],0);
                                     exit;
                           End;
                           
                           next;
                      End;
                      FMostraBarraProgresso.hide;
             End;

             if (Etapa=4)
             or (etapa=0)
             Then Begin
                     ptitulos.clear;
                     ptitulos.LoadFromFile('c:\titulosexcluir.txt');
                     //excluindo os titulos anotados
                     FMostraBarraProgresso.Lbmensagem.caption:='Excluindo os T�tulos Anotados';
                     FMostraBarraProgresso.BarradeProgresso.MinValue:=0;
                     FMostraBarraProgresso.BarradeProgresso.MaxValue:=ptitulos.count;
                     FMostraBarraProgresso.BarradeProgresso.Progress:=0;
                     FMostraBarraProgresso.show;
                     
                     for cont:=0  to ptitulos.count-1 do
                     Begin
                          FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
                          FMostraBarraProgresso.Lbmensagem.Repaint;
                          FMostraBarraProgresso.show;
                     
                          if (Self.Verificasetemlote(ptitulos[cont],PQuerylote)=False)
                          Then Begin
                                  Writeln(parquivo,'LAN�AMENTO T�TULO='+PTITULOS[CONT]);
                                  //exclui apenas os que nao sao em lote
                                  if (ObjLancamento.ExcluiTitulo(ptitulos[cont],false)=False)
                                  Then Begin
                                          Messagedlg('Erro na tentativa de Exclus�o do T�tulo N� '+ptitulos[cont]+#13+'Os T�tulos exclu�dos ser�o retornados ao sistema',mterror,[mbok],0);
                                          exit;
                                  End;
                          End;
                     End;
             End;

             result:=True;
             Messagedlg('Exclus�o conclu�da com Sucesso!',mtInformation,[mbok],0);
             exit;
        End;
     Finally
            CloseFile(parquivo);
            Freeandnil(pquery);
            Freeandnil(PQuerylote);
            Freeandnil(ptitulos);
            objlancamento.free;
     End;

end;





function TObjTitulo.LancaNumeroduplicataPendencia(ptitulo: string): Boolean;
var
  ObjPendencia            :TobjPendencia;
begin
     //essa duplicata nao � a duplicata de contas a receber, e sim o numero da duplicata
     //ou boletro que o fornecedor manda, ou seja, quando compra-se algo de um fornecedor
     //junto com a NF vem um boleto ou duplicata a ser paga, guarda-se o numero para depois
     //localizar
     result:=False;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACERTAR NUMEROS DE DUPLICATAS A PAGAR')=False)
     then exit;

     Try
        ObjPendencia:=tobjPendencia.create;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto de Pend�ncia',mterror,[mbok],0);
           exit;
     End;

     Try
        Result:=ObjPendencia.Lanca_Numero_BoletoAPagar(Ptitulo);
     Finally
            ObjPendencia.free;
     End;

end;


function TObjTitulo.Get_ExportaLanctoContaGer: string;
begin
     Result:=Self.ExportaLanctoContaGer;
end;

procedure TObjTitulo.Submit_ExportaLanctoContaGer(parametro: string);
begin
     Self.ExportaLanctoContaGer:=uppercase(Parametro);
end;


procedure TObjTitulo.Imprime_Cheques_Clientes_portadores;
var
pportador,PCredorDevedor,PCodigoCredorDevedor:string;
PStrPortador:TStringList;
cont:integer;
ObjChequeportador:TObjChequesportador;
PvalorCheque,PsomaPortador,PSomaATrasado,Psomageral,PsomaAtrasadoGeral:currency;

begin
     try
         PstrPortador:=TStringList.Create;
         ObjChequeportador:=TObjChequesportador.create;
     Except
           Mensagemerro('Erro na tentativa de Criar a String List PStrportador');
           exit;
     End;
try
     With FfiltroImp do
     Begin
         DesativaGrupos;
         Grupo01.Enabled:=True;
         Grupo07.Enabled:=True;

         lbgrupo01.Caption:='Portadores';
         edtgrupo01.OnKeyDown:=Self.edtportadorKeyDown_PV;
         edtgrupo01.Color:=$005CADFE;
         LbGrupo07.caption:='Credor/Devedor';
         ComboGrupo07.Items.clear;
         Combo2Grupo07.Items.clear;
         Self.Get_ListacredorDevedor(ComboGrupo07.items);
         Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
         edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;
         edtgrupo07.color:=$005CADFE;


         showmodal;

         If tag=0
         Then exit;

         

          
          //Conferindo o que foi lancado

          if (ExplodeStr(edtgrupo01.text,PStrportador,';','integer')=False)
          then Begin
                    Mensagemerro('Portador Inv�lido');
                    exit;
          End;

          Try
                strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
                PCredorDevedor:=Combo2Grupo07.Items[ComboGrupo07.itemindex];
          Except
                mensagemerro('� necess�rio escolher um Credor/Devedor');
                exit;
          End;

          Try
             Strtoint(EdtGrupo07.text);
             PCodigoCredorDevedor:=EdtGrupo07.text;
          Except
              mensagemerro('� necess�rio escolher um Codigo Credor/Devedor');
              exit;
          End;

          if (Self.CREDORDEVEDOR.LocalizaCodigo(PcredorDevedor)=False)
          then begin
                    mensagemerro('Credor/Devedor n�o localizado');
                    exit;
          End;
          Self.CREDORDEVEDOR.TabelaparaObjeto;

          If (self.CREDORDEVEDOR.Localizacredordevedor(PcodigoCredorDevedor)=False)
          Then Begin
                    mensagemerro(Self.CREDORDEVEDOR.Get_Nome+' n�o encontrado');
                    exit;
          End;
     End;//with das opcoes


     With Self.ObjDataset do
     Begin
          close;
          selectsql.clear;
          selectsql.add('Select cheque,vencimento,portador');
          selectsql.add('from Proc_Cheques_cliente');
          selectsql.add('where credordevedor='+pcredordevedor+' and codigocredordevedor='+pcodigocredordevedor);

          if (PStrPortador.count>0)
          then begin
                    selectsql.add('and (Portador='+PStrPortador[0]);
                    for cont:=1 to Pstrportador.count-1 do
                    begin
                         selectsql.add('or Portador='+PStrPortador[cont]);
                    end;
                    selectsql.add(')');
          End;
          selectsql.add('group by cheque,vencimento,portador');
          selectsql.add('order by portador,vencimento');
          open;
          if (recordcount=0)
          then begin
                    mensagemaviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;
          //********************************************************************
          With FreltxtrdPrint do
          begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               RDprint.Abrir;
               if (Rdprint.Setup=False)
               then Begin
                         rdprint.Fechar;
                         exit;
               End;

               RDprint.Impc(linhalocal,45,Self.NumeroRelatorio+'CHEQUE NO PORTADOR',[negrito]);
               IncrementaLinha(2);

               RDprint.Impf(linhalocal,1,completapalavra(Self.CREDORDEVEDOR.Get_Nome+':'+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(pcredordevedor,pcodigocredordevedor),90,' '),[negrito]);
               IncrementaLinha(2);
               
               VerificaLinha;
               rdprint.Impf(linhalocal,1,CompletaPalavra('NUMERO CH',10,' ')+' '+
                                         CompletaPalavra('BNC',3,' ')+' '+
                                         CompletaPalavra('AGENCIA',10,' ')+' '+
                                         CompletaPalavra('CONTA',10,' ')+' '+
                                         CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;


               Pportador:=Fieldbyname('portador').asstring;
               Psomaportador:=0;
               PsomaAtrasado:=0;

               Psomageral:=0;
               PsomaAtrasadoGeral:=0;

               While not(Self.ObjDataset.eof) do
               begin
                    ObjChequeportador.LocalizaCodigo(fieldbyname('cheque').asstring);
                    ObjChequeportador.TabelaparaObjeto;
                    PvalorCheque:=strtofloat(ObjChequeportador.Get_Valor);

                    if (fieldbyname('portador').asstring<>Pportador)
                    then begin
                              IncrementaLinha(1);
                              VerificaLinha;
                              rdprint.ImpF(Linhalocal,1,'TOTAL NO PORTADOR    '+formata_valor(psomaportador),[negrito]);
                              IncrementaLinha(1);
                              VerificaLinha;
                              rdprint.ImpF(Linhalocal,1,'ATRASADO NO PORTADOR '+formata_valor(psomaatrasado),[negrito]);
                              IncrementaLinha(1);
                              desenhalinha;


                              Pportador:=Fieldbyname('portador').asstring;
                              Psomaportador:=0;
                              PsomaAtrasado:=0;
                              
                              Self.PORTADOR.LocalizaCodigo(pportador);
                              Self.PORTADOR.TabelaparaObjeto;

                              VerificaLinha;
                              rdprint.ImpF(Linhalocal,1,'PORTADOR '+Self.portador.get_codigo+'-'+self.portador.get_nome,[negrito]);
                              IncrementaLinha(2);

                    End;

                    VerificaLinha;
                    rdprint.Imp(linhalocal,1,CompletaPalavra(ObjChequeportador.Get_NumCheque,10,' ')+' '+
                                             CompletaPalavra(ObjChequeportador.Get_Banco,3,' ')+' '+
                                             CompletaPalavra(ObjChequeportador.Get_agencia,10,' ')+' '+
                                             CompletaPalavra(ObjChequeportador.Get_Conta,10,' ')+' '+
                                             CompletaPalavra(ObjChequeportador.Get_Vencimento,10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(ObjChequeportador.Get_Valor),12,' '));
                    IncrementaLinha(1);



                    PsomaPortador:=PsomaPortador+PvalorCheque;
                    Psomageral:=Psomageral+pvalorcheque;
                    
                    if (strtodate(ObjChequeportador.Get_Vencimento)<strtodate(datetostr(now)))
                    then begin
                              PSomaATrasado:=PSomaATrasado+PvalorCheque;
                              PsomaAtrasadoGeral:=PsomaAtrasadoGeral+PvalorCheque;
                    end;

                    Self.ObjDataset.next;
               End;//while
               IncrementaLinha(1);
               VerificaLinha;
               rdprint.ImpF(Linhalocal,1,'TOTAL NO PORTADOR    '+formata_valor(psomaportador),[negrito]);
               IncrementaLinha(1);
               VerificaLinha;
               rdprint.ImpF(Linhalocal,1,'ATRASADO NO PORTADOR '+formata_valor(psomaatrasado),[negrito]);
               IncrementaLinha(2);
               desenhalinha;

               VerificaLinha;
               rdprint.ImpF(Linhalocal,1,'TOTAL GERAL         '+formata_valor(psomaGERAL),[negrito]);
               IncrementaLinha(1);
               VerificaLinha;
               rdprint.ImpF(Linhalocal,1,'ATRASADO GERAL      '+formata_valor(psomaatrasadogeral),[negrito]);
               IncrementaLinha(2);




               rdprint.Fechar;
          End;
          //********************************************************************
     End;

finally
       freeandnil(PStrportador);
       ObjChequeportador.free;
End;

end;

procedure TObjTitulo.Imprime_Contas_a_Receber_e_Cheques_por_portador;
var
  pportador:string;
  PStrPortador:TStringList;
  cont:integer;
  PdataInicial_Ch,PdataFinal_Ch,PdataInicial,PdataFinal:string;
  PsomaPendencia,PsomaGeral,PsomaPortador:Currency;
begin
     try
         PstrPortador:=TStringList.Create;
     Except
           Mensagemerro('Erro na tentativa de Criar a String List PStrportador');
           exit;
     End;
try
     With FfiltroImp do
     Begin
         DesativaGrupos;
         Grupo01.Enabled:=True;
         Grupo02.Enabled:=True;
         LbGrupo01.caption:='Vencimento Inicial - T�tulo';
         LbGrupo02.caption:='Vencimento Final   - T�tulo';
         edtgrupo01.editmask:=mascaradata;
         edtgrupo02.editmask:=mascaradata;
         edtgrupo03.editmask:=mascaradata;
         edtgrupo04.editmask:=mascaradata;

         Grupo03.Enabled:=True;
         Grupo04.Enabled:=True;
         LbGrupo03.caption:='Vencimento Inicial - Cheques';
         LbGrupo04.caption:='Vencimento Inicial - Cheques';

         Grupo05.Enabled:=True;
         LbGrupo05.caption:='Portador(es)';
         edtgrupo05.OnKeyDown:=Self.edtportadorKeyDown_PV;
         edtgrupo05.color:=$005CADFE;

         showmodal;

         If tag=0
         Then exit;

         //Conferindo o que foi lancado

         PdataInicial:='';
         PdataFinal:='';
         PdataInicial_Ch:='';
         PdataFinal_Ch:='';

         Try
            if (Comebarra(trim(edtgrupo01.text))<>'')
            Then Begin
                    Strtodate(edtgrupo01.text);
                    PdataInicial:=edtgrupo01.text;
            End;
         Except
                MensagemErro('Data Inicial de Vencimento de T�tulo Inv�lida');
                exit;
         End;

         Try
            if (Comebarra(trim(edtgrupo02.text))<>'')
            Then Begin
                    Strtodate(edtgrupo02.text);
                    PdataFinal:=edtgrupo02.text;
            End;
         Except
                MensagemErro('Data Final de Vencimento de T�tulo Inv�lida');
                exit;
         End;

         Try
            if (Comebarra(trim(edtgrupo03.text))<>'')
            Then Begin
                    Strtodate(edtgrupo03.text);
                    PdataInicial_Ch:=edtgrupo03.text;
            End;
         Except
                MensagemErro('Data Inicial de Vencimento de Cheques Inv�lida');
                exit;
         End;

         Try
            if (Comebarra(trim(edtgrupo04.text))<>'')
            Then Begin
                    Strtodate(edtgrupo04.text);
                    PdataFinal_Ch:=edtgrupo04.text;
            End;
         Except
                MensagemErro('Data Final de Vencimento de Cheques Inv�lida');
                exit;
         End;


         if (ExplodeStr(edtgrupo05.text,PStrportador,';','integer')=False)
         then Begin
                   Mensagemerro('Portador(es) Inv�lido(s)');
                   exit;
         End;

         if (PStrPortador.count=0)
         Then Begin
                   MensagemAviso('� necess�rio escolher o portador que deseja selecionar os cheques');
                   exit;
         End;

         for cont:=0 to PStrPortador.Count-1 do
         Begin
              if (self.PORTADOR.localizacodigo(PStrPortador[cont])=False)
              then Begin
                        MensagemAviso('O Portador n� '+PStrPortador[cont]+' n�o foi encontrado');
                        exit;
              End;
         End;
     End;//with das opcoes


     With Self.ObjDataset do
     Begin     
          close;
          selectsql.clear;
          selectsql.add('Select tabpendencia.codigo as pendencia,');
          selectsql.add('tabpendencia.titulo,');
          selectsql.add('tabtitulo.historico,');
          selectsql.add('tabtitulo.emissao,');
          selectsql.add('tabtitulo.credordevedor,');
          selectsql.add('tabtitulo.codigocredordevedor,');
          selectsql.add('tabpendencia.vencimento,');
          selectsql.add('tabpendencia.saldo');
          selectsql.add('from tabpendencia');
          selectsql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          selectsql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');

          selectsql.add('where Tabpendencia.Saldo>0 and TabContager.Tipo=''C'' ');

          if (PdataInicial<>'')
          Then selectsql.add('and TabPendencia.Vencimento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataInicial))+#39);

          if (PdataFinal<>'')
          Then selectsql.add('and TabPendencia.Vencimento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataFinal))+#39);

          selectsql.add('order by tabpendencia.vencimento,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');

          open;
          last;
          if (recordcount=0)
          then begin
                    mensagemaviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Selecionando T�tulos a receber';
          first;
          //********************************************************************
          With FreltxtrdPrint do
          begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.TamanhoQteColunas:=130;
               
               LinhaLocal:=3;
               RDprint.Abrir;
               if (Rdprint.Setup=False)
               then Begin
                         rdprint.Fechar;
                         exit;
               End;

               RDprint.Impc(linhalocal,65,Self.NumeroRelatorio+'CONTAS A RECEBER E CHEQUES POR PORTADOR',[negrito]);
               IncrementaLinha(2);

               rdprint.ImpF(linhalocal,1,'CONTAS A RECEBER',[negrito]);
               IncrementaLinha(2);

               if (PdataInicial<>'') and (PdataFinal<>'')
               Then Begin
                         rdprint.ImpF(linhalocal,1,'Vencimento '+PdataInicial+' a '+PdataFinal,[negrito]);
                         IncrementaLinha(2);
               End
               Else Begin
                         if (PdataInicial<>'') and (PdataFinal='')
                         Then Begin
                                   rdprint.ImpF(linhalocal,1,'Vencimentos superiores a '+PdataInicial,[negrito]);
                                   IncrementaLinha(2);
                         End
                         Else BEgin
                                   if (Pdatainicial='') and (PdataFinal<>'')
                                   Then Begin
                                             rdprint.ImpF(linhalocal,1,'Vencimentos inferiores a '+PdataFinal,[negrito]);
                                             IncrementaLinha(2);
                                   End;
                         End;
               End;
               
               //cabecalho
               VerificaLinha;
               rdprint.Impf(linhalocal,1,CompletaPalavra('PEND',6,' ')+' '+
                                         CompletaPalavra('HISTORICO',49,' ')+' '+
                                         CompletaPalavra('CLIENTE',49,' ')+' '+
                                         CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('SALDO',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;


               PsomaPendencia:=0;
               PsomaGeral:=0;
               PsomaPortador:=0;


               While not(Self.ObjDataset.eof) do
               begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.show;
                    Application.ProcessMessages;

                    verificalinha;
                    rdprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('PENDENCIA').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('HISTORICO').asstring,49,' ')+' '+
                                             CompletaPalavra(fieldbyname('codigocredordevedor').asstring,5,' ')+' - '+Completapalavra(Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),41,' ')+' '+
                                             CompletaPalavra(fieldbyname('VENCIMENTO').asstring,10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALDO').asstring),12,' '));
                    IncrementaLinha(1);
                    

                    PsomaPendencia:=PsomaPendencia+Fieldbyname('saldo').asfloat;
                    PsomaGeral:=PsomaGeral+Fieldbyname('saldo').asfloat;

                    Self.ObjDataset.next;
               End;//while
               desenhalinha;
               verificalinha;
               rdprint.Impf(linhalocal,1,CompletaPalavra('TOTAL EM T�TULOS A RECEBER',118,' ')+
                                        CompletaPalavra_a_Esquerda(formata_valor(PSOMAPENDENCIA),12,' '),[negrito]);
               IncrementaLinha(1);
               
               //*************cheques**************
               close;
               SelectSQL.clear;
               SelectSQL.add('Select tabchequesportador.portador,tabportador.nome as nomeportador,');
               SelectSQL.add('tabchequesportador.vencimento,');
               SelectSQL.add('tabchequesportador.valor,');
               SelectSQL.add('tabchequesportador.banco,');
               SelectSQL.add('tabchequesportador.agencia,');
               SelectSQL.add('tabchequesportador.conta,');
               SelectSQL.add('tabchequesportador.numcheque');
               SelectSQL.add('from tabchequesportador');
               SelectSQL.add('join tabportador on tabchequesportador.portador=tabportador.codigo');
               SelectSQL.add('left join tabvalores on tabvalores.cheque=tabchequesportador.codigo');
               SelectSQL.add('left join tablancamento on Tabvalores.lancamento=tablancamento.codigo');
               SelectSQL.add('left join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
               SelectSQL.add('where not(ChequedoPortador=''S'')');

               if (PdataInicial_Ch<>'')
               Then SelectSQL.add('and (TabChequesPortador.vencimento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial_CH))+#39+')');

               if (PdataFinal_Ch<>'')
               Then SelectSQL.add('and (TabChequesPortador.vencimento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatafinal_CH))+#39+')');

               SelectSQL.add('and (tabchequesportador.portador='+PStrPortador[0]);
               
               for cont:=1 to PStrPortador.Count-1 do
               Begin
                    SelectSQL.add('or tabchequesportador.portador='+PStrPortador[cont]);
               End;

               SelectSQL.add(')');

               SelectSQL.add('order by tabchequesportador.portador,tabchequesportador.vencimento,');
               SelectSQL.add('tabchequesportador.valor');
               //showmessage(SelectSQL.text);
               open;
               last;
               FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
               FMostraBarraProgresso.lbmensagem.caption:='Selecionado Cheques';
               first;
               pportador:='';

               IncrementaLinha(2);
               VerificaLinha;
               RDprint.ImpF(linhalocal,65,'****CHEQUES DE 3�****',[negrito]);
               IncrementaLinha(2);

               Self.PORTADOR.LocalizaCodigo(PStrPortador[0]);
               Self.PORTADOR.TabelaparaObjeto;

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'PORTADOR(ES): '+uppercase(Self.PORTADOR.Get_Nome),[negrito]);
               incrementalinha(1);

               for cont:=1 to PStrPortador.count-1 do
               Begin
                    Self.PORTADOR.LocalizaCodigo(PStrPortador[cont]);
                    Self.PORTADOR.TabelaparaObjeto;
                    VerificaLinha;
                    RDprint.ImpF(linhalocal,15,uppercase(Self.PORTADOR.Get_Nome),[negrito]);
                    incrementalinha(1);
               End;

               incrementalinha(1);
               if (PdataInicial_CH<>'') and (PdataFinal_CH<>'')
               Then Begin
                         VerificaLinha;
                         rdprint.ImpF(linhalocal,1,'Vencimento '+PdataInicial_CH+' a '+PdataFinal_CH,[negrito]);
                         IncrementaLinha(2);
               End
               Else Begin
                         if (PdataInicial_CH<>'') and (PdataFinal_CH='')
                         Then Begin
                                   VerificaLinha;
                                   rdprint.ImpF(linhalocal,1,'Vencimentos superiores a '+PdataInicial_CH,[negrito]);
                                   IncrementaLinha(2);
                         End
                         Else BEgin
                                   if (Pdatainicial_CH='') and (PdataFinal_CH<>'')
                                   Then Begin
                                             VerificaLinha;
                                             rdprint.ImpF(linhalocal,1,'Vencimentos inferiores a '+PdataFinal_CH,[negrito]);
                                             IncrementaLinha(2);
                                   End;
                         End;
               End;

               incrementalinha(1);
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,CompletaPalavra('VENCTO',10,' ')+' '+
                                         CompletaPalavra('BANCO',10,' ')+' '+
                                         CompletaPalavra('AGENCIA',10,' ')+' '+
                                         CompletaPalavra('CONTA',10,' ')+' '+
                                         CompletaPalavra('N�MERO',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;
               
               While not(Self.ObjDataset.eof) do
               Begin
                    FMostraBarraProgresso.IncrementaBarra1(1);
                    FMostraBarraProgresso.show;
                    Application.ProcessMessages;
                    
                    if (Pportador<>fieldbyname('portador').asstring)
                    Then Begin
                              if not(Self.ObjDataset.Bof)
                              Then Begin
                                        //totalizando o portador anterior
                                        IncrementaLinha(1);
                                        VerificaLinha;
                                        rdprint.impf(linhalocal,1,completapalavra('TOTAL DO PORTADOR',55,' ')+
                                                                  CompletaPalavra_a_Esquerda(formata_valor(psomaportador),12,' '),[negrito]);
                                        IncrementaLinha(2);
                              End;
                              IncrementaLinha(2);
                              VerificaLinha;
                              rdprint.impf(linhalocal,1,completapalavra(fieldbyname('portador').asstring,5,' ')+' - '+
                                                        completapalavra(fieldbyname('nomeportador').asstring,122,' '),[negrito]);
                              IncrementaLinha(2);
                              PsomaPortador:=0;
                              pportador:=fieldbyname('portador').asstring;
                    End;
                    
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('BANCO').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('AGENCIA').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('CONTA').asstring,10,' ')+' '+
                                             CompletaPalavra(fieldbyname('numcheque').asstring,10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),12,' '));
                    IncrementaLinha(1);

                    PsomaPortador:=PsomaPortador+fieldbyname('valor').asfloat;
                    PsomaGeral:=PsomaGeral+fieldbyname('valor').asfloat;
                    Self.ObjDataset.Next;
               End;
               IncrementaLinha(1);
               VerificaLinha;
               rdprint.impf(linhalocal,1,completapalavra('TOTAL DO PORTADOR',55,' ')+
                                         CompletaPalavra_a_Esquerda(formata_valor(psomaportador),12,' '),[negrito]);
               IncrementaLinha(2);
               desenhalinha;
               //total geral
               VerificaLinha;
               rdprint.impf(linhalocal,1,'TOTAL FINAL '+formata_valor(PsomaGeral),[negrito]);
               IncrementaLinha(1);
               //*************
               rdprint.Fechar;
          End;

                    {if (PStrPortador.count>0)
          then begin
                    selectsql.add('and (Portador='+PStrPortador[0]);
                    for cont:=1 to Pstrportador.count-1 do
                    begin
                         selectsql.add('or Portador='+PStrPortador[cont]);
                    end;
                    selectsql.add(')');
          End;
          selectsql.add('group by cheque,vencimento,portador');
          selectsql.add('order by portador,vencimento');
          open;}

          //********************************************************************
     End;

finally
       freeandnil(PStrportador);
       FMostraBarraProgresso.close;
End;

end;


procedure TObjTitulo.EdtcontagerencialExit(Sender: TObject;LabelNome:Tlabel);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;


     If (Self.CONTAGERENCIAL.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CONTAGERENCIAL.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CONTAGERENCIAL.get_mascara+' '+Self.CONTAGERENCIAL.GET_NOME;
end;


function TObjTitulo.RetornaSaldoVencido(PQuantDiasAtraso: Integer;PCredorDevedor, Pcliente: string): Currency;
begin
     //Esse procedimento soma o saldo devedor do CredorDevedor em parametro vencido a mais de X dias (em parametro) em atraso 

     Result:=0;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select sum(tabpendencia.saldo) as SOMASALDO');
          SelectSQL.add('from tabpendencia');
          SelectSQL.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSQL.add('where ((cast('+#39+formatdatetime('mm/dd/yyyy',now)+#39' as date)-tabpendencia.vencimento)>'+inttostr(PQuantDiasAtraso)+')');
          SelectSQL.add('and Tabtitulo.credordevedor='+PCredorDevedor);
          SelectSQL.add('and tabtitulo.codigocredordevedor='+Pcliente);
          open;
          Result:=Fieldbyname('somasaldo').asfloat;
          close;
     End;
end;

function TObjTitulo.RetornaNomesCredoresDevedores(PCredorDevedor:string;Pcodigos: String): String;
var
PStrCodigos:TStringList;
cont:integer;
pnome,temp:string;
begin
     //Essa fun��o recebe um credor e v�rios c�digos  e retorna os primeiros nomes

     temp:='';
     Try
        PStrCodigos:=TStringList.create;
     Except
        MensagemErro('Erro na tentativa de Criar a StringList de C�digos');
        exit;
     End;

     Try
        If (ExplodeStr(Pcodigos,PStrCodigos,';','')=False)
        Then Begin
                  Result:='';
                  exit;
        End;

        if (PStrCodigos.Count=0)
        Then Begin
                  Result:='';
                  exit;
        End;

        for cont:=0 to PStrCodigos.Count-1 do
        Begin
              try
                If (PStrCodigos[cont]<>'')
                Then Strtoint(PStrCodigos[cont]);

                Pnome:='';

                Pnome:=Self.CREDORDEVEDOR.Get_NomeCredorDevedor(pcredordevedor,PStrCodigos[cont]);
                if (PStrcodigos.count>1)
                Then Pnome:=PegaPrimeiraPalavra(pnome);

                if (temp<>'')
                then temp:=temp+'/';

                temp:=temp+Pnome;
              Except

              End;
        End;
        result:=temp;
     finally
            Freeandnil(PstrCodigos);
     End;
end;

procedure TObjTitulo.Get_Pendencias_Recibo_cobranca(PvencimentoINicial,PvencimentoFinal: string; PGRID: tstringgrid;PCredorDevedor,PCodigoCredorDevedor:string;ptipocolunas:Tstrings);
VAR
  cont:integer;
  StrCodigos:TstringList;
begin
     Try
          StrCodigos:=TstringList.create;
          StrCodigos.Clear;
     except
           MensagemErro('Erro na tentativa de criar a StringList de C�digos');
           exit;
     End;

Try

     Try
        if (PCredorDevedor<>'')
        Then strtoint(PCredorDevedor);
     Except
           PCredorDevedor:='';
     End;

     Try
        if (PCodigoCredorDevedor<>'')
        Then Begin
                  if (ExplodeStr(PCodigoCredorDevedor,Strcodigos,';','INTEGER')=False)
                  Then exit;
        End;
     Except
           PCodigoCredorDevedor:='';
     End;


     Try
        if (trim(comebarra(Pvencimentoinicial))<>'')
        Then strtodate(PvencimentoInicial)
        Else Pvencimentoinicial:='';
     Except
           Messagedlg('Data de Vencimento Inicial Inv�lida!',mterror,[mbok],0);
           exit;
     End;

     Try
        if (trim(comebarra(PvencimentoFinal))<>'')
        Then strtodate(PvencimentoFinal)
        Else PvencimentoFinal:='';
     Except
           Messagedlg('Data de Vencimento Final Inv�lida!',mterror,[mbok],0);
           exit;
     End;

     PGRID.RowCount:=1;
     PGRID.ColCount:=09;

     ptipocolunas.Clear;
     Pgrid.Cols[0].clear;
     Pgrid.Cols[1].clear;
     Pgrid.Cols[2].clear;
     Pgrid.Cols[3].clear;
     Pgrid.Cols[4].clear;
     Pgrid.Cols[5].clear;
     Pgrid.Cols[6].clear;
     PGRID.Cols[7].clear;
     PGRID.Cols[8].clear;

     ptipocolunas.add('integer');
     PGRID.Cells[00,0]:='>>';

     ptipocolunas.add('integer');
     PGRID.Cells[01,0]:='COD.TIT.';
     
     ptipocolunas.add('integer');
     PGRID.Cells[02,0]:='COD.PEND.';

     ptipocolunas.add('string');
     PGRID.Cells[03,0]:='HIST. PEND�NCIA';

     ptipocolunas.add('decimal');
     PGRID.Cells[04,0]:='VALOR';

     ptipocolunas.add('decimal');
     PGRID.Cells[05,0]:='SALDO';

     ptipocolunas.add('date');
     PGRID.Cells[06,0]:='VENCIMENTO';

     ptipocolunas.add('string');
     PGRID.cells[07,0]:='NUMDCTO';

     ptipocolunas.add('string');
     PGRID.cells[08,0]:='CLIENTE';

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select Tabpendencia.Titulo,TAbtitulo.NUMDCTO,Tabpendencia.Codigo,');
          SelectSQL.add('Tabpendencia.Historico as HistoricoPendencia,');
          SelectSQL.add('Tabpendencia.Vencimento,Tabpendencia.Valor,Tabpendencia.saldo,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
          SelectSQL.add('from Tabtitulo');
          SelectSQL.add('join Tabpendencia on Tabtitulo.codigo=Tabpendencia.titulo');
          SelectSQL.add('join TabContager on TAbtitulo.contagerencial=tabcontager.codigo');
          SelectSQL.add('Where tabpendencia.saldo>0 ');

          if (trim(comebarra(pvencimentoinicial))<>'')
          Then SelectSQL.add('and (Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pvencimentoinicial))+#39+')');

          if (trim(comebarra(pvencimentofinal))<>'')
          Then SelectSQL.add('and (Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pvencimentofinal))+#39+')');


          SelectSQL.add('and(tabcontager.tipo=''C'') ');
          if (PCredorDevedor<>'')
          Then SelectSQL.add('and TabTitulo.CredorDevedor='+PCredorDevedor);

          if (StrCodigos.Count>0)
          Then Begin
                    SelectSQL.add('and (TabTitulo.CodigoCredorDevedor='+Strcodigos[0]);
                    for cont:=1 to StrCodigos.Count-1 do
                    Begin
                         SelectSQL.add('or TabTitulo.CodigoCredorDevedor='+Strcodigos[cont]);
                    End;
                    SelectSQL.add(')');
          End;
          open;

          IF (RecordCount=0)
          Then Begin
                    MensagemAviso('Nenhuma informa��o foi selecionada');
                    Exit;
          End;

          last;
          PGRID.RowCount:=RecordCount+1;

          first;
          cont:=1;

          While not(eof) do
          Begin
               PGRID.Rows[cont].clear;
               PGRID.cells[1,cont]:=FieldByname('TITULO').asstring;
               PGRID.cells[2,cont]:=FieldByname('CODIGO').asstring;
               PGRID.cells[3,cont]:=FieldByname('HISTORICOPENDENCIA').asstring;
               PGRID.cells[4,cont]:=formata_valor(FieldByname('VALOR').asstring);
               PGRID.cells[5,cont]:=formata_valor(FieldByname('SALDO').asstring);
               PGRID.cells[6,cont]:=FieldByname('VENCIMENTO').asstring;
               PGRID.cells[7,cont]:=FieldByname('NUMDCTO').asstring;
               PGRID.cells[8,cont]:=Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring);


               next;
               inc(cont,1);

          End;

     End;

finally
       Freeandnil(StrCodigos);
End;

end;
//*****************************************************************************


procedure TObjTitulo.EdtSubcontagerencialExit(Sender: TObject;
  LabelNome: Tlabel);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SubContaGerencial.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SUBCONTAGERENCIAL.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.SUBCONTAGERENCIAL.get_mascara+' '+Self.SUBCONTAGERENCIAL.GET_NOME;

end;

procedure TObjTitulo.edtsubcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState;lbnome:Tlabel;PContager:string);

var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            if (lbnome<>nil)
            then lbnome.caption:='';

            If (FpesquisaLocal.PreparaPesquisa(Self.SubContaGerencial.Get_Pesquisa(Pcontager),self.SubContaGerencial.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then begin
                                  TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                  if (lbnome<>nil)
                                  then lbnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjTitulo.edtsubcontagerencialKeyDown_PV(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.SubContaGerencial.Get_Pesquisa,self.SubContaGerencial.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


function TObjTitulo.ContaTitulos: integer;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select count(codigo) as conta from tabtitulo');
          open;
          result:=Fieldbyname('conta').asinteger;
          close;
      End;
end;

function TObjTitulo.Get_Observacao: String;
begin
     Result:=Self.Observacao;
end;

procedure TObjTitulo.Submit_observacao(parametro: String);
begin
     Self.Observacao:=parametro;
end;

procedure TObjTitulo.ImprimeTotalporContaGerencialeSubContaComDataDeLancamento(PTipoConta: string);
var
ObjqueryLocal:Tibquery;
PContager,PsubContager:string;
PSomaSUBConta,PSomaTotalgeral,PSomaConta:Currency;
credordevedortemp,codigocredordevedortemp,temp,linha:integer;
TempStr,PNomeCredorDevedor, PExcluirContaGerencial:String;
Pdata1,Pdata2:string;
ContaGerencialTemp,SubContaGerencialTemp, ExcluiContaGerencial:TStringList;
TmpQuantPEndencias,TmpQuantDevedores,POrigemData:integer;
cont :Integer;
ValorRecebidoCheques,ValorChequesCompensados,ValorChequesNaoCompensados, ValorReal:currency;

// F�bio
// Vou colocar mais um filtro nesse relatorio. Excluir Conta Gerencial. Pedido Imob. Alfer

begin
     try
        ContaGerencialTemp:= TStringList.create;
        SubContaGerencialTemp:= TStringList.create;
        ExcluiContaGerencial:=TStringList.Create;
     Except
           Messagedlg('Erro na tentativa de Cria��o da StringList',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjqueryLocal:=TIBQuery.create(nil);
        ObjqueryLocal.Database:=FDataModulo.IBDatabase;
     Except
           freeandnil(ContaGerencialTemp);
           Messagedlg('Erro na tentativa de Cria��o da Query',mterror,[mbok],0);
           exit;
     End;
Try


     //filtrando
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          grupo01.enabled:=True;
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.Caption:='LAN�. INICIAL';

          grupo02.enabled:=True;
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.Caption:='LAN�. FINAL';

          Grupo03.Enabled:=True;
          edtGrupo03.EditMask:='';
          LbGrupo03.Caption:='Conta Gerencial';
          if (PTipoConta='D')
          Then edtgrupo03.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else edtgrupo03.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;
          edtgrupo03.Color:=$005CADFE;

          Grupo04.Enabled:=True;
          edtGrupo04.EditMask:='';
          LbGrupo04.Caption:='SubConta Gerencial';
          edtgrupo04.OnKeyDown:=Self.edtsubcontagerencialKeyDown_PV;
          edtgrupo04.color:=$005CADFE;

          Grupo05.Enabled:=True;
          edtGrupo05.EditMask:='';
          LbGrupo05.Caption:='Excluir Conta Gerencial';
          if (PTipoConta='D')
          Then edtgrupo05.OnKeyDown:=Self.EdtcontagerencialPAGARKeyDown
          Else edtgrupo05.OnKeyDown:=Self.EdtcontagerencialRECEBERKeyDown;
          edtgrupo05.color:=$005CADFE;

          Grupo07.enabled:=True;
          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.edtcodigocredordevedorKeyDown_Unico;
          edtgrupo07.Color:=$005CADFE;

          showmodal;
          edtgrupo13.visible:=true;

          if(tag=0)
          Then exit;

          try
             StrToDate(edtgrupo01.text);
             StrToDate(edtgrupo02.text);
             Pdata1:=edtgrupo01.text;
             Pdata2:=edtgrupo02.text;
          Except
                Pdata1:='';
                Pdata2:='';
          End;

          Try
                CredorDevedorTemp:=-1;
                if (ComboGrupo07.itemindex>=0)
                Then CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=-1;
             if (EdtGrupo07.text<>'')
             Then CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;

          //verificando contas gerenciais
          Try
             TempStr:='';

             ContaGerencialTemp.clear;
             for Temp:=1 to length(Edtgrupo03.text) do
             Begin
                   If ((edtgrupo03.text[temp]=';')
                   or (temp=length(Edtgrupo03.text)))
                   Then Begin
                           if ((temp=length(Edtgrupo03.text)) and (edtgrupo03.text[temp]<>';'))
                           Then TempStr:=TempStr+edtgrupo03.text[temp];

                           If (Tempstr<>'')
                           Then Begin
                                   Try
                                           Strtoint(tempstr);
                                           ContaGerencialTemp.add(Tempstr);
                                   Except
                                           ContaGerencialTemp.clear;
                                           Break;
                                   End;
                           End;
                           TempStr:='';
                   End
                   Else TempStr:=TempStr+edtgrupo03.text[temp];
             End;
          Except
                ContaGerencialTemp.clear;
          End;

          PExcluirContaGerencial:=edtgrupo05.Text;
          if (PExcluirContaGerencial <> '')
          then Begin
                 if (ExplodeStr(PExcluirContaGerencial,ExcluiContaGerencial,';','INTEGER')=false)
                 then exit;
          end;

          //verificando Sub-contas gerenciais
          TempStr:=edtgrupo04.text;
          If (explodestr(tempstr,SubContaGerencialTemp,';','INTEGER')=False)
          Then exit;

         (* If(ComboGrupo13.ItemIndex=0)
          Then POrigemData:=1
          Else If(ComboGrupo13.ItemIndex=1)
               Then POrigemData:=2
               Else POrigemData:=0;

          {POrigemData
             0 = vazio
             1 = pendencia
             2 = boleto
          }*)

          //***********************
     End;


     With objquerylocal do
     BEgin
          Close;
          Sql.clear;
          Sql.add('Select tabcontager.nome as nomecontagerencial,tabtitulo.contagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
          Sql.add('TabSubContagerencial.nome as NOMESUBCONTAGERENCIAL,Tabtitulo.subcontagerencial,tabpendencia.titulo,tabtitulo.historico,');
          Sql.add('tabpendencia.codigo as pendencia,sum(tablancamento.valor) as SOMAQUITACAO,tabpendencia.vencimento');
          Sql.add('from tabtitulo');
          Sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          Sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
          Sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
          Sql.add('join tablancamento on tabpendencia.codigo=tablancamento.pendencia');
          Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
          //Sql.add('left join tabregistroretornocobranca trrc on trrc.boleto=tabpendencia.boletobancario');
          Sql.add('where TabContager.Tipo='+#39+PTipoConta+#39);
          Sql.add('and tabtipolancto.classificacao=''Q''');


          if (ContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial='+ContaGerencialTemp[0]);
                  for temp:=1 to ContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.Contagerencial='+ContaGerencialTemp[temp]);

                  Sql.add(')');
          End;


          if (ExcluiContaGerencial.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial <> '+ExcluiContaGerencial[0]);
                  for temp:=1 to ExcluiContaGerencial.Count-1 do
                  Sql.add('and TabTitulo.Contagerencial <> '+ExcluiContaGerencial[temp]);

                  Sql.add(')');
          End;


          if (SubContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.SubContagerencial='+SubContaGerencialTemp[0]);

                  for temp:=1 to SubContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.SubContagerencial='+SubContaGerencialTemp[temp]);

                  Sql.add(')');
          End;

          if (Pdata1<>'')
          Then Begin

                    If(POrigemData=2)
                    Then Begin
                          SQL.add('and (trrc.dataocorrencia>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                          SQL.add('and trrc.dataocorrencia<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
                    End
                    Else Begin
                          SQL.add('and (tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                          SQL.add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
                    End;
          End;

          if (CredorDevedorTemp<>-1)
          Then Begin
                  Sql.add('and Tabtitulo.CredorDevedor='+inttostr(CREDORDEVEDORtemp));
                  if (codigoCredorDevedorTemp<>-1)
                  Then Sql.add('and Tabtitulo.codigoCredorDevedor='+inttostr(codigoCREDORDEVEDORtemp));
          End;


          Sql.add('group by tabcontager.nome,Tablancamento.Data, tabtitulo.contagerencial,tabtitulo.credordevedor,');
          Sql.add('tabtitulo.codigocredordevedor,TabSubContagerencial.nome,Tabtitulo.subcontagerencial,tabpendencia.titulo,');
          Sql.add('tabtitulo.historico,tabpendencia.codigo,tabpendencia.vencimento');//,trrc.dataocorrencia
          Sql.add('order by tabcontager.nome,Tablancamento.Data, tabtitulo.contagerencial,tabsubcontagerencial.nome,tabtitulo.subcontagerencial,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
          FmostraStringList.Memo.Text:=Sql.text;
//          FmostraStringList.showmodal;
          
          open;
          last;

          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Gerando relat�rio';

          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum dado foi encontrado!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
                                                                 
          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,65,Self.NumeroRelatorio+'TOTAL POR CONTA GERENCIAL/SUB-CONTA',[negrito]);
          inc(linha,2);
          FreltxtRDPRINT.RDprint.ImpC(linha,1,'ATEN��O: A coluna "DATA" refere-se � data da ultima quita��o.',[negrito]);
          inc(linha,2);


          if (PTipoConta='C')
          Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS RECEBIDAS',[negrito])
          Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'CONTAS PAGAS',[negrito]);
          inc(linha,1);


          FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos de '+Pdata1+' a '+Pdata2,[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,09,CompletaPalavra('CREDOR/DEVEDOR',29,' ')+' '+
                                               CompletaPalavra('HISTORICO',50,' ')+' '+
                                               CompletaPalavra('TITULO',6,' ')+' '+
                                               CompletaPalavra('DATA',10,' ')+' '+
                                               CompletaPalavra('VENCTO',10,' ')+' '+
                                               CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
          inc(linha,1);


          //A Id�ia � assim
          //Conta Gerencial XXXXX
          //    Fornecedor  A       Total Pago
          //    Fornecedor  B       Total Pago
          //    Fornecedor  C       Total Pago
          //Total da Conta  R$ XXXXX


          //Imprimindo a Primeira Contagerencial
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
          FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
          inc(linha,1);

          PContager:=Fieldbyname('contagerencial').asstring;
          PSubContager:=Fieldbyname('subcontagerencial').asstring;
          PSomaConta:=0;
          PSomaSubConta:=0;
          PSomaTotalgeral:=0;
          TmpQuantPEndencias:=0;
          TmpQuantDevedores:=0;
          While not(Eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.Show;
               Application.ProcessMessages;

               If (PContager<>Fieldbyname('contagerencial').asstring)
               Then Begin

                          //totalizando a Sub-contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
                          inc(linha,1);
                          //totalizando a contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
                          inc(linha,2);
                          //Nova Conta
                          PContager:=Fieldbyname('contagerencial').asstring;
                          PSubContager:=Fieldbyname('subcontagerencial').asstring;
                          PSomaConta:=0;
                          PSomaSubConta:=0;
                          //Contagerencial
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta: ',[negrito]);
                          FreltxtRDPRINT.RDprint.Imp(linha,8,completapalavra(fieldbyname('contagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomecontagerencial').asstring,40,' '));
                          inc(linha,1);
                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                          FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
                          FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
                          inc(linha,1);
                          //inc(cont,1);


               End
               Else BEgin
                         if (PSubContager<>Fieldbyname('subcontagerencial').asstring)
                         Then Begin
                                    //totalizando a Sub-contagerencial
                                    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                    FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
                                    inc(linha,2);

                                    //Nova Conta
                                    PSubContager:=Fieldbyname('subcontagerencial').asstring;
                                    PSomaSubconta:=0;
                                    //SubContagerencial
                                    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                    FreltxtRDPRINT.RDprint.Impf(linha,3,'Sub-Conta: ',[negrito]);
                                    FreltxtRDPRINT.RDprint.Imp(linha,14,completapalavra(fieldbyname('subcontagerencial').asstring,5,' ')+' - '+completapalavra(fieldbyname('nomesubcontagerencial').asstring,40,' '));
                                    inc(linha,1);
                                    //inc(cont,1);
                         End
               End;

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);

               If(POrigemData=2)
               Then FreltxtRDPRINT.RDprint.Imp(linha,09,completapalavra(fieldbyname('codigocredordevedor').asstring+'-'+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),29,' ')+' '+
                                                   CompletaPalavra(fieldbyname('historico').asstring,50,' ')+' '+
                                                   CompletaPalavra(fieldbyname('titulo').asstring,6,' ')+' '+
                                                   CompletaPalavra(Self.RetornaDataUltimaQuitacaodaPendenciaBoleto(fieldbyname('pendencia').asstring,Pdata1, PData2) ,10,' ')+' '+
                                                   CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('somaquitacao').asstring),12,' '))
               Else FreltxtRDPRINT.RDprint.Imp(linha,09,completapalavra(fieldbyname('codigocredordevedor').asstring+'-'+Self.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),29,' ')+' '+
                                                   CompletaPalavra(fieldbyname('historico').asstring,50,' ')+' '+
                                                   CompletaPalavra(fieldbyname('titulo').asstring,6,' ')+' '+
                                                   CompletaPalavra(Self.RetornaDataUltimaQuitacaodaPendencia(fieldbyname('pendencia').asstring,Pdata1,Pdata2) ,10,' ')+' '+
                                                   CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('somaquitacao').asstring),12,' '));

               inc(linha,1);
               PSomaConta:=PSomaConta+fieldbyname('somaquitacao').asfloat;
               PSomaSubConta:=PSomaSUbConta+fieldbyname('somaquitacao').asfloat;
               PSomaTotalgeral:=PSomaTotalgeral++fieldbyname('somaquitacao').asfloat;
               inc(TmpQuantPEndencias,1);
               inc(cont,1);
               next;
          End;
          //totalizando a Sub-contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,3,'Total da Sub-Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaSubConta)),12,' '),[negrito]);
          inc(linha,1);
          //totalizando a Contagerencial
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'Total da Conta '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PsomaConta)),12,' '),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SOMA TOTAL '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PSomaTotalgeral)),12,' '),[negrito]);
          inc(linha,1);
          
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SOMA TOTAL DE PENDENCIAS '+CompletaPalavra_a_Esquerda((floattostr(cont)),12,' '),[negrito]);
          //PEGA VALOR RECEBIDO EM CHEQUES
          Close();
          SQL.Clear();
          SQL.Add('select Sum(tabvalores.valor) as SOMA');
          SQL.Add('from tabvalores');
          SQL.Add('left join tabchequesportador on tabchequesportador.codigo = tabvalores.cheque');
          SQL.Add('left join tablancamento on tablancamento.codigo = tabvalores.lancamento');
          SQL.Add('left join tabpendencia on tabpendencia.codigo = tablancamento.pendencia');
          SQL.Add('left join tabtitulo on tabtitulo.codigo = tabpendencia.titulo');
          SQL.Add('left join tabcontager on tabcontager.codigo = tabtitulo.contagerencial');
          SQL.Add('left join tabsubcontagerencial on tabsubcontagerencial.codigo = tabtitulo.subcontagerencial');
          SQL.Add('where tabvalores.cheque > 0');
          SQL.Add('and tabvalores.tipo ='+#39+PTipoConta+#39);

          if (ContaGerencialTemp.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial='+ContaGerencialTemp[0]);
                  for temp:=1 to ContaGerencialTemp.Count-1 do
                  Sql.add('or TabTitulo.Contagerencial='+ContaGerencialTemp[temp]);

                  Sql.add(')');
          End;


          if (ExcluiContaGerencial.count>0)
          Then begin
                  Sql.add('and (TabTitulo.Contagerencial <> '+ExcluiContaGerencial[0]);
                  for temp:=1 to ExcluiContaGerencial.Count-1 do
                  Sql.add('and TabTitulo.Contagerencial <> '+ExcluiContaGerencial[temp]);

                  Sql.add(')');
          End;



          if (Pdata1<>'')
          Then Begin

                    SQL.Add('and tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                    SQL.Add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39);


          end;

          if (CredorDevedorTemp<>-1)
          Then Begin
                  Sql.add('and Tabtitulo.CredorDevedor='+inttostr(CREDORDEVEDORtemp));
                  if (codigoCredorDevedorTemp<>-1)
                  Then Sql.add('and Tabtitulo.codigoCredorDevedor='+inttostr(codigoCREDORDEVEDORtemp));
          End;
          open;
          ValorRecebidoCheques:= fieldbyname('SOMA').asfloat;
          ValorReal:=PSomaTotalgeral- ValorRecebidoCheques;
          inc(linha,2);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'RESUMO DE RECEBIMENTO',[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'VALOR EM CHEQUES '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(ValorRecebidoCheques)),12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'VALOR EM DINHEIRO '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(ValorReal)),12,' '),[negrito]);
          inc(linha,1);
          Close();
          SQL.Clear();
          SQL.Add('select SUM(valor) as SOMA');                                //SOMA O VALOR
          SQL.Add('from tabchequesportador');
          SQL.Add('where tabchequesportador.baixado = ''S'' ');   //DOS CHEQUES BAIXADOS
          if(PTipoConta = 'C')
          then begin
             SQL.Add('and tabchequesportador.chequedoportador = ''N'' ');                  //DOS CHEQUES RECEBIDOS

          end
          else
          begin
              SQL.Add('and tabchequesportador.chequedoportador = ''S'' ');                  //DOS CHEQUES RECEBIDOS
          end;
          SQL.Add('and tabchequesportador.vencimento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
          SQL.Add('and tabchequesportador.vencimento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39);
          open;

          if(ContaGerencialTemp.count=0) and (ExcluiContaGerencial.count=0) and  (CredorDevedorTemp=-1)
          then
          begin
              ValorChequesCompensados :=fieldbyname('SOMA').asfloat;
              if(ValorRecebidoCheques < ValorChequesCompensados)
              then begin
                ValorChequesCompensados:=ValorRecebidoCheques;
              end;
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.impf(linha,1,'RESUMO DE MOVIMENTO DE CHEQUES NO PER�ODO',[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.impf(linha,1,'VALOR EM CHEQUES J� COMPENSADO '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(ValorChequesCompensados)),12,' '),[negrito]);
              inc(linha,1);
              ValorReal:=ValorReal+ValorChequesCompensados;
              ValorChequesNaoCompensados := ValorRecebidoCheques -  fieldbyname('SOMA').asfloat;
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.impf(linha,1,'VALOR COMPUTADODO '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(ValorReal)),12,' '),[negrito]);
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',95,'_'));
              inc(linha,1);
              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
              FreltxtRDPRINT.RDprint.impf(linha,1,'VALOR REAL'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(ValorReal)),12,' '),[negrito]);
              inc(linha,1);
          end;


          FMostraBarraProgresso.close;
          FreltxtRDPRINT.RDprint.Fechar;



     End;
Finally
       freeandnil(ContaGerencialTemp);
       FreeAndNil(ObjqueryLocal);
       FreeAndNil(ExcluiContaGerencial);
End;

end;


function TObjTitulo.RetornaDataUltimaQuitacaodaPendencia(Ppendencia, Pdata1, PData2:string):string;
begin
     Result:='';
     With Self.ObjDataset do
     Begin
         Close;
         SelectSQL.Clear;
         SelectSQL.Add('Select MAX(TabLancamento.Data) as Data from TabLancamento');
         SelectSQL.Add('join TabTipoLancto on TabTipoLancto.Codigo = TabLancamento.TipoLancto');
         SelectSQL.Add('join TabPendencia on TabPendencia.Codigo = TabLancamento.Pendencia');
         SelectSQL.Add('Where tabPendencia.codigo = '+Ppendencia);
         SelectSQL.Add('and   TabTipoLancto.Classificacao = ''Q''  ');
         if (Pdata1 <> '  /  /    ')
         then SelectSQL.Add('and   TabLancamento.data >= '+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PData1))+#39);
         if (Pdata2 <> '  /  /    ')
         then SelectSQL.Add('and   TabLancamento.data <= '+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PData2))+#39);

         try
             Open;
         except
             MensagemErro('Erro ao tentar resgatar a data do lancamento');

             exit;
         end;

         Result:=fieldbyname('Data').AsString;
     end;

end;

function TObjTitulo.LocalizaDuplicata(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add(' Select tabtitulo.CODIGO,tabtitulo.HISTORICO,tabtitulo.GERADOR,tabtitulo.CODIGOGERADOR,tabtitulo.CREDORDEVEDOR,tabtitulo.CODIGOCREDORDEVEDOR,');
           SelectSql.add(' tabtitulo.EMISSAO,tabtitulo.PRAZO,tabtitulo.PORTADOR,tabtitulo.VALOR,tabtitulo.CONTAGERENCIAL,tabtitulo.subcontagerencial,tabtitulo.NUMDCTO,');
           SelectSql.add(' Tabtitulo.Notafiscal,tabtitulo.geradopelosistema,tabtitulo.ExportaLanctoContaGer,tabtitulo.observacao');
           SelectSql.add(' from tabtitulo join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
           SelectSql.add(' where tabpendencia.duplicata='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;


end;



procedure TObjTitulo.edttituloKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa(true),Self.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjTitulo.edttituloKeyDown_PV(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa(true),Self.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=Tedit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjTitulo.VerificaNumeroDocumento: boolean;
begin
     result:=False;


     if (VerificaNumeroDocumentoTituloGlobal=False)
     or (Self.NumDcto='')
     Then Begin
               result:=True;
               exit;
     End;

     With Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo from tabtitulo where credordevedor='+Self.CREDORDEVEDOR.get_codigo);
          sql.add('and codigocredordevedor='+Self.CODIGOCREDORDEVEDOR);
          sql.add('and numdcto='+#39+Self.NumDcto+#39);
          open;
          if (recordcount>0)
          Then Begin
                    if (Self.codigo<>fieldbyname('codigo').asstring)
                    Then Begin
                              MensagemErro('Esse n�mero de documento est� cadastrado no t�tulo '+fieldbyname('codigo').asstring);
                              exit;

                    End;
          End;
          result:=True;
     End;
end;

function TObjTitulo.RetornaDataUltimaQuitacaodaPendenciaBoleto(Ppendencia,Pdata1, PData2: string): string;
begin
     Result:='';
     With Self.ObjDataset do
     Begin
         Close;
         SelectSQL.Clear;     //alterei com max
         SelectSQL.Add('Select MAX(tabregistroretornocobranca.dataocorrencia) as dataocorrencia from TabLancamento');
         SelectSQL.Add('join TabTipoLancto on TabTipoLancto.Codigo = TabLancamento.TipoLancto');
         SelectSQL.Add('join TabPendencia on TabPendencia.Codigo = TabLancamento.Pendencia');
         SelectSQL.Add('join tabregistroretornocobranca on tabregistroretornocobranca.boleto=tabpendencia.boletobancario');
         SelectSQL.Add('Where tabPendencia.codigo = '+Ppendencia);
         SelectSQL.Add('and   TabTipoLancto.Classificacao = ''Q''  ');
         if (Pdata1 <> '  /  /    ')
         then SelectSQL.Add('and   TabLancamento.data >= '+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PData1))+#39);
         if (Pdata2 <> '  /  /    ')
         then SelectSQL.Add('and   TabLancamento.data <= '+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PData2))+#39);

         //SelectSQL.SaveToFile('c:\celio2.sql');
         try
             Self.ObjDataset.Open;
         except
             MensagemErro('Erro ao tentar resgatar a data do lancamento');
             exit;
         end;
         If(fieldbyname('dataocorrencia').AsString<>'')//(Self.ObjDataset.RecordCount>0) tava retornando 1 mesmo sem anda
         Then Begin
              Result:=fieldbyname('dataocorrencia').AsString;
         End
         Else Begin
              Close;
              SelectSQL.Clear;
              SelectSQL.Add('Select MAX(TabLancamento.Data) as Data from TabLancamento');
              SelectSQL.Add('join TabTipoLancto on TabTipoLancto.Codigo = TabLancamento.TipoLancto');
              SelectSQL.Add('join TabPendencia on TabPendencia.Codigo = TabLancamento.Pendencia');
              SelectSQL.Add('Where tabPendencia.codigo = '+Ppendencia);
              SelectSQL.Add('and   TabTipoLancto.Classificacao = ''Q''  ');
              if (Pdata1 <> '  /  /    ')
              then SelectSQL.Add('and   TabLancamento.data >= '+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PData1))+#39);
              if (Pdata2 <> '  /  /    ')
              then SelectSQL.Add('and   TabLancamento.data <= '+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PData2))+#39);

              try
                  Self.ObjDataset.Open;
              except
                  MensagemErro('Erro ao tentar resgatar a data do lancamento');

                  exit;
              end;
              Result:=fieldbyname('Data').AsString;
         End;
     End;
end;


function TObjTitulo.RetornaSaldoPendencias(PCredordevedor,
  PCodigoCredorDevedor: string): Currency;
var
      Qtemp : Tibquery;
begin
      Try
            Qtemp := TIBQuery.Create(nil);
            Qtemp.Database := FDataModulo.IBDatabase;
      Except
            MensagemErro('Erro ao criar Query Saldo de Pendencias');
      End;

      With Qtemp do
      Begin
            Close;
            SQL.Clear;
            SQL.Add('select sum(tp.saldo) as saldo from tabtitulo tt');
            SQL.Add('join tabpendencia tp on tp.titulo=tt.codigo');
            SQL.add('JOIN tabCONTAGER tc on tt.contagerencial=tc.codigo');
            SQL.Add('where tt.credordevedor=' + PCredordevedor + 'and tt.codigocredordevedor=' + PCodigoCredorDevedor);
            SQL.Add('and tc.tipo=''C''');
            SQL.Add('and tp.saldo<>0');
            Open;
            If(FieldByName('Saldo').AsCurrency>0)
            Then Result := FieldByName('Saldo').AsCurrency
            Else Result := 0;
      End;

      FreeAndNil(Qtemp);
end;

procedure TObjTitulo.ImprimeClientesAdimplentes();
var // Feito por F�bio. Ordem do Sadol 27/10/2009. Esse relatorio soomente mostror� os clientes em DIA separados por contagerencial
ObjqueryLocal:Tibquery;
PContager,PsubContager:String;
Pdata1,Pdata2:string;
Linha, Cont:Integer;
StrListNomes, StrListSubConta:TStringList;
Nome:String;
begin

try
     //filtrando
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          grupo01.enabled:=True;
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.Caption:='LAN�. INICIAL';

          grupo02.enabled:=True;
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.Caption:='LAN�. FINAL';

          Grupo03.Enabled:=True;
          edtGrupo03.EditMask:='';
          LbGrupo03.Caption:='Conta Gerencial';
          edtgrupo03.OnKeyDown:=Self.EdtcontagerencialKeyDown;

          Grupo04.Enabled:=True;
          edtGrupo04.EditMask:='';
          LbGrupo04.Caption:='SubConta Gerencial';
          edtgrupo04.OnKeyDown:=Self.edtsubcontagerencialKeyDown_PV;

          showmodal;
          if(tag=0)
          Then exit;

          try
             StrToDate(edtgrupo01.text);
             StrToDate(edtgrupo02.text);
             Pdata1:=edtgrupo01.text;
             Pdata2:=edtgrupo02.text;
          Except
                Pdata1:='';
                Pdata2:='';
          End;

          PContager:=edtgrupo03.Text;
          PsubContager:=edtgrupo04.Text;

          PContager:=StrReplace(PContager,';','');
          PSubContager:=StrReplace(PSubContager,';','');
     end;

     With Self.ObjDataset do
     BEgin
          Close;
          SelectSql.clear;
          SelectSql.add('Select ContaGerencial,NomeContaGerencial,SubContaGerencial,');
          SelectSql.add('NomeSUBContaGerencial, CredorDevedor, CodigoCredorDevedor');
          SelectSql.add('from  Proc_Clientes_Adimplentes('+#39+FormatDateTime('mm/dd/yyyy',StrToDate(Pdata1))+#39+', '+#39+FormatDateTime('mm/dd/yyyy',StrToDate(Pdata2))+#39+')');
          SelectSql.add('Where  ContaGerencial > -100');

          if (PContager<>'')
          Then SelectSql.Add('and Contagerencial='+PContager);

          if (pSubContaGer <> '')
          Then SelectSql.add('and SubContagerencial = '+PsubContager);

          SelectSql.add('Order By ContaGerencial, SubContaGerencial');

          PContager:='';
          PsubContager:='';

          open;
          if (RecordCount = 0)
          then Begin
                   MensagemErro('Nenhum dado encontrado na pesquisa');
                   exit;
          end;

          try
              StrListNomes:=TStringList.Create;
              StrListNomes.Clear;
              StrListSubConta:=TStringList.Create;
              StrListSubConta.Clear;
          except
              MensagemErro('Erro ao tentar criar a StringList');
              exit;
          end;

          last;
          AbrirImpressao;
          InicializaBarradeProgressoRelatorio(RecordCount,'Gerando Relat�rio... Aguarde...');
          First;

          Linha:=3;
          ImprimirNegrito(Linha,20,'RELA��O DE CLIENTES ADIMPLENTES PER�ODO '+Pdata1+' a '+Pdata2);
          inc(linha,2);
          ImprimirNegrito(Linha,1, 'CONTA'); inc(linha,1);
          ImprimirNegrito(Linha,1, '    SUBCONTA');inc(linha,1);
          ImprimirNegrito(Linha,1, '        CLIENTE'); inc(linha,1);

          PContaGer:=fieldbyname('ContaGerencial').asString;
          inc(linha,1);
          ImprimirNegrito(Linha,1,fieldbyname('NomeContaGerencial').AsString);
          inc(linha,1);

          While not (eof) do
          Begin
               if (PContager <> fieldbyname('ContaGerencial').asString)
               then Begin
                       // Quando muda de Conta Gerenial eu Imprimo seus clientes e
                       // preparo para outra contagerencial
                       inc(linha,1);
                       ImprimirNegrito(Linha,1,'    '+StrListSubConta[0]);
                       PsubContager:=StrListSubConta[0];
                       inc(linha,1);
                       for Cont:=1 to StrListNomes.Count-1 do
                       Begin
                            if (PsubContager <> StrListSubConta[Cont])
                            then Begin
                                    inc(linha,1);
                                    ImprimirNegrito(Linha,1,'    '+StrListSubConta[Cont]);
                                    PsubContager:=StrListSubConta[Cont];
                                    inc(linha,1);
                            end;
                            ImprimirSimples(Linha,1,'       '+StrListNomes[Cont]);
                            inc(linha,1);
                       end;
                       StrListNomes.Clear;
                       StrListSubConta.Clear;

                       inc(linha,1);
                       ImprimirNegrito(Linha,1,fieldbyname('NomeContaGerencial').AsString);
                       inc(linha,1);
                       PContaGer:=fieldbyname('ContaGerencial').asString;
               end;

               Nome:= Self.RetornaNomesCredoresDevedores(fieldbyname('CredorDevedor').AsString, fieldbyname('CodigoCredorDevedor').AsString);
               Self.InsereSeNaoExisteNaStringList(StrListNomes,StrListSubConta, Nome, fieldbyname('NomeSUBContaGerencial').AsString);
               Next;
          end;

          ImprimirNegrito(Linha,1,'    '+StrListSubConta[0]);
          PsubContager:=StrListSubConta[0];
          for Cont:=1 to StrListNomes.Count-1 do
          Begin
               if (PsubContager <> StrListSubConta[Cont])
               then Begin
                       inc(linha,1);
                       ImprimirNegrito(Linha,1,'    '+StrListSubConta[Cont]);
                       PsubContager:=StrListSubConta[Cont];
                       inc(linha,1);
               end;
               ImprimirSimples(Linha,1,'       '+StrListNomes[Cont]);
               inc(linha,1);
          end;
          StrListNomes.Clear;
          StrListSubConta.Clear;

          FecharImpressao;
     end;
finally
     FreeAndNil(StrListNomes);
end;

end;

Procedure TObjTitulo.InsereSeNaoExisteNaStringList(Var PStrListNomes, PStrListNomeSubConta:TStringList; PNome, PNomeSubConta:String);
Var Cont:Integer;
Begin
     Cont:=0;
     if (PStrListNomes.Count = 0)
     then Begin
             PStrListNomeSubConta.Add(PNomeSubConta);
             PStrListNomes.Add(Pnome);                    
             exit;
     end;

     for Cont:=0 to PStrListNomes.Count-1 do
     Begin
           if (PStrListNomes[Cont] = PNome)
           then exit;
     end;

     PStrListNomeSubConta.Add(PNomeSubConta);
     PStrListNomes.add(PNome);

end;

procedure TobjTitulo.MostraLancamentos_mes_subconta(psubconta,pdata:string);
var
psoma:currency;
plinha:integer;
Begin
      With Self.Objquery do
      Begin
           close;
           sql.clear;
           sql.add('Select tabpendencia.titulo,tabpendencia.codigo as pendencia,tabtitulo.historico as historicotitulo,tablancamento.data,tablancamento.historico as historicolancamento,tablancamento.valor');
           sql.add('from tabtitulo');
           sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
           sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
           sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
           sql.add('join tablancamento on tabpendencia.codigo=tablancamento.pendencia');
           Sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
           Sql.add('where tabtipolancto.classificacao=''Q'' and');
           sql.add('tabtitulo.subcontagerencial='+psubconta);
           sql.add('and extract(month from tablancamento.data)='+FormatDateTime('mm',strtodate(pdata)));
           sql.add('and extract(year from tablancamento.data)='+FormatDateTime('yyyy',strtodate(pdata)));
           sql.add('order by tablancamento.data');
           open;
           
           FmostraStringGrid.StringGrid.ColCount:=6;
           FmostraStringGrid.StringGrid.RowCount:=1;

           FmostraStringGrid.StringGrid.Cells[0,0]:='T�tulo';
           FmostraStringGrid.StringGrid.Cells[1,0]:='Pend';
           FmostraStringGrid.StringGrid.Cells[2,0]:='Data';
           FmostraStringGrid.StringGrid.Cells[3,0]:='Titulo';
           FmostraStringGrid.StringGrid.Cells[4,0]:='Lancamento';
           FmostraStringGrid.StringGrid.Cells[5,0]:='Valor';
           FmostraStringGrid.panelrodape.visible:=True;
           psoma:=0;
           While not(eof) do
           Begin
                FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                Plinha:=FmostraStringGrid.StringGrid.RowCount-1;
                
                FmostraStringGrid.StringGrid.Cells[0,plinha]:=fieldbyname('titulo').asstring;
                FmostraStringGrid.StringGrid.Cells[1,plinha]:=fieldbyname('pendencia').asstring;
                FmostraStringGrid.StringGrid.Cells[2,plinha]:=fieldbyname('data').asstring;
                FmostraStringGrid.StringGrid.Cells[3,plinha]:=fieldbyname('historicotitulo').asstring;
                FmostraStringGrid.StringGrid.Cells[4,plinha]:=fieldbyname('historicolancamento').asstring;
                FmostraStringGrid.StringGrid.Cells[5,plinha]:=formata_valor(fieldbyname('valor').asstring);
                
                psoma:=psoma+fieldbyname('valor').ascurrency;

                next;
           End;

           if (FmostraStringGrid.StringGrid.rowcount>1)
           Then FmostraStringGrid.StringGrid.FixedRows:=1;

           FmostraStringGrid.lbrodape.caption:='Total de Lan�amentos: '+formata_valor(psoma);


           AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);
           FmostraStringGrid.showmodal;
      End;
End;



procedure TobjTitulo.MostraPendencias_mes_subconta(psubconta,pdata:string);
var
psoma:currency;
plinha:integer;
Begin
      With Self.Objquery do
      Begin
           close;
           sql.clear;
           sql.add('Select tabpendencia.titulo,tabpendencia.codigo as pendencia,tabtitulo.historico as historicotitulo,tabpendencia.vencimento,tabpendencia.saldo');
           sql.add('from tabtitulo');
           sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
           sql.add('left join tabsubcontagerencial on tabtitulo.Subcontagerencial=tabSubcontagerencial.codigo');
           sql.add('join tabpendencia on tabpendencia.titulo=tabtitulo.codigo');
           Sql.add('where tabpendencia.saldo>0 and tabtitulo.subcontagerencial='+psubconta);
           sql.add('and extract(month from tabpendencia.vencimento)='+FormatDateTime('mm',strtodate(pdata)));
           sql.add('and extract(year  from tabpendencia.vencimento)='+FormatDateTime('yyyy',strtodate(pdata)));
           sql.add('order by tabpendencia.vencimento');
           open;
           
           FmostraStringGrid.StringGrid.ColCount:=6;
           FmostraStringGrid.StringGrid.RowCount:=1;

           FmostraStringGrid.StringGrid.Cells[0,0]:='T�tulo';
           FmostraStringGrid.StringGrid.Cells[1,0]:='Pend';
           FmostraStringGrid.StringGrid.Cells[2,0]:='Data';
           FmostraStringGrid.StringGrid.Cells[3,0]:='Titulo';
           FmostraStringGrid.StringGrid.Cells[4,0]:='Saldo';
           FmostraStringGrid.panelrodape.visible:=True;
           psoma:=0;
           While not(eof) do
           Begin
                FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                Plinha:=FmostraStringGrid.StringGrid.RowCount-1;
                
                FmostraStringGrid.StringGrid.Cells[0,plinha]:=fieldbyname('titulo').asstring;
                FmostraStringGrid.StringGrid.Cells[1,plinha]:=fieldbyname('pendencia').asstring;
                FmostraStringGrid.StringGrid.Cells[2,plinha]:=fieldbyname('vencimento').asstring;
                FmostraStringGrid.StringGrid.Cells[3,plinha]:=fieldbyname('historicotitulo').asstring;
                FmostraStringGrid.StringGrid.Cells[4,plinha]:=formata_valor(fieldbyname('saldo').asstring);
                
                psoma:=psoma+fieldbyname('saldo').ascurrency;

                next;
           End;

           if (FmostraStringGrid.StringGrid.rowcount>1)
           Then FmostraStringGrid.StringGrid.FixedRows:=1;

           FmostraStringGrid.lbrodape.caption:='Total Aberto: '+formata_valor(psoma);


           AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);
           FmostraStringGrid.showmodal;
      End;
End;

function TObjTitulo.ListaTitulos(Ptipo,PComSemSaldo:string):Boolean;//Ptipo: C-Credito, D-Debito, PComSemSaldo, C ou S
begin
      result := self.ListaTitulos(Ptipo,PComSemSaldo,'','');
end;

function TObjTitulo.ListaTitulos(Ptipo, PComSemSaldo, PCampo, Pvalor : string): Boolean;
begin

      with Self.ObjQueryMostraTitulo do
      begin

           try

            Screen.Cursor:=crHourGlass;

            Close;
            SQL.Clear;

            SQL.add('select');

            sql.add('PROC.codigo ,PROC.NUMDCTO, PROC.codigocredordevedor ,');

            SQL.Add('PROC.cd_razaosocial ,PROC.historico,PROC.emissao,');
            SQL.add('PROC.valor,PROC.saldopendencia ,PROC.Tipo');
            SQL.add('from PROCTITULOS PROC');

            SQL.Add('where 1=1');   //where 1=1 ???
            SQL.Add('and PROC.tipo='+#39+Ptipo+#39);

            if PComSemSaldo='C'
            then SQL.Add('and PROC.saldopendencia>0')
            else SQL.Add('and PROC.saldopendencia=0');

            if PCampo<>''
            then sql.Add('AND '+Pvalor);

            SQL.add('group by proc.codigo,PROC.NUMDCTO,PROC.codigocredordevedor,');
            SQL.add('PROC.cd_razaosocial,PROC.historico,PROC.emissao,PROC.valor,PROC.saldopendencia,PROC.Tipo');

            SQL.add('order by proc.codigo');

            //InputBox('listatitulos','a',sql.Text);
            Open;

           finally
            Screen.Cursor:=crDefault;
           end
      end;

end;

function TObjTitulo.RetornaValoresFinanceiros(Ptipo:string;PdataLimite:string):string;
var
 ObjqueryLocal:Tibquery;
begin
      result:='';
      Try
              ObjqueryLocal:=TIBQuery.create(nil);
              ObjqueryLocal.Database:=FDataModulo.IBDatabase;
      Except
              Messagedlg('Erro na tentativa de Cria��o da Query',mterror,[mbok],0);
              exit;
      End;
      try
           with objquerylocal do
           begin
                close;
                sql.add('Select sum(saldo) as SOMA from tabpendencia');
                sql.add('join tabtitulo   on tabpendencia.titulo=tabtitulo.codigo');
                sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
                sql.add('Where TabContaGer.Tipo='+#39+Ptipo+#39);
                if PdataLimite <> ''
                then sql.add('and tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PdataLimite))+#39);
                
                open;
                result:=fieldbyname('soma').asstring;
           end;
      finally
           FreeAndNil(ObjqueryLocal);
      end;
End;

{Rodolfo}
function TObjTitulo.ListaTitulos2(Ptipo,PComSemSaldo, PQuant:string):Boolean; //pQuant eh quantidade de registros que serao trazidos - Rodolfo
var
  data : string;
begin
    //result := self.ListaTitulos(Ptipo,PComSemSaldo,Pcampo,Pvalor, '');
    //26/04/2013 celio
    data := '';
    if(Pos('/',PQuant) > 0) then //veio data
    begin
      data := PQuant;
      PQuant := '10000';
    end;

    with Self.ObjQueryMostraTitulo do
    begin
        try
            Screen.Cursor:=crHourGlass;

            Close;
            SQL.Clear;

            SQL.add('select');

            sql.add('PROC.codigo, PROC.NUMDCTO, PROC.codigocredordevedor,');
            SQL.Add('PROC.cd_razaosocial, PROC.historico, PROC.emissao,');
            SQL.add('PROC.valor, PROC.saldopendencia, PROC.Tipo');

            {Rodolfo == }
            //verifico se o sql vai pesquisar os 100 ou os 1000 registros
            SQL.add('from PROCTITULOS2(' + pQuant );

            SQL.Add(','+#39+Ptipo+#39);

            if PComSemSaldo='C'
            then SQL.Add(', 1, 999999999')       //Saldo > 0
            else SQL.Add(', 0, 0');           //Saldo = 0

            if(data = '') then
              SQL.Add(','+'null')
            else
              SQL.Add(','+InverteDataSQL(data));  


            SQL.Add(') PROC');
            SQL.add('order by proc.codigo');

            //InputBox('uobjtitulo2','a',sql.Text);

            Open;

        finally
            Screen.Cursor:=crDefault;
        end
    end;
end;

end.
