object FlancaTroco: TFlancaTroco
  Left = 432
  Top = 229
  Width = 626
  Height = 423
  Caption = 'LAN'#199'AMENTO DE TROCO '
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 270
    Height = 32
    Caption = 'VALOR DO TROCO: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbvalortroco: TLabel
    Left = 280
    Top = 8
    Width = 337
    Height = 32
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'VALOR DO TROCO'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 0
    Top = 371
    Width = 617
    Height = 19
    Align = alBottom
    Alignment = taCenter
    Caption = 'Pressione <ESC> para Sair'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  inline FrRecebimento1: TFrRecimento
    Left = 8
    Top = 55
    Width = 606
    Height = 316
    Color = clBtnFace
    Ctl3D = False
    ParentColor = False
    ParentCtl3D = False
    TabOrder = 1
    inherited Bevel1: TBevel
      Height = 316
    end
  end
  inline FrPagamento1: TFrPagamento
    Left = 8
    Top = 55
    Width = 606
    Height = 316
    Color = clBtnFace
    Ctl3D = False
    ParentColor = False
    ParentCtl3D = False
    TabOrder = 0
    inherited Bevel1: TBevel
      Height = 316
    end
    inherited btinserir: TBitBtn
      OnClick = FrPagamento1btinserirClick
    end
  end
end
