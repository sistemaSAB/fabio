unit UCorrecaodeTitulos;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, Buttons,uobjpendencia,uobjlancamento;

type
  TFcorrecaodeTitulos = class(TForm)
    edtdata: TMaskEdit;
    edttaxa: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Btgravar: TBitBtn;
    edttipolancto: TEdit;
    Label3: TLabel;
    procedure edttaxaKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtgravarClick(Sender: TObject);
    procedure edttipolanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FcorrecaodeTitulos: TFcorrecaodeTitulos;
  ObjPendencia:Tobjpendencia;
  ObjLancamento:TobjLancamento;
  
implementation

uses Upesquisa, UTipoLancto, UessencialGlobal, UescolheImagemBotao;

{$R *.DFM}

procedure TFcorrecaodeTitulos.edttaxaKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in ['0'..'9',#8,','])
     Then Begin
              if key='.'
              Then key:=','
              Else key:=#0;
          End;
end;

procedure TFcorrecaodeTitulos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If ObjPendencia<>nil
     Then objpendencia.free;
     
     If ObjLancamento<>nil
     Then objlancamento.free;
end;

procedure TFcorrecaodeTitulos.BtgravarClick(Sender: TObject);
var
Data:Tdate;
Taxa:Extended;
teste:byte;
begin
     MessageDlg('M�dulo em Implementa�ao!',mterror,[mbok],0);
     exit;
     try
        teste:=0;
        Data:=strtodate(edtdata.text);
        teste:=1;
        taxa:=StrToFloat(edttaxa.text);
     Except
           If teste=0
           Then Messagedlg('Erro na Data!',mterror,[mbok],0)
           Else Messagedlg('Erro na Taxa de Juros!',mterror,[mbok],0)
     End;


end;

procedure TFcorrecaodeTitulos.edttipolanctoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FTipoLancto:TFTipoLancto;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FTipoLancto:=TFTipoLancto.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Get_PesquisaTipoLanctoNaoGeravalor,ObjLancamento.Get_TituloPesquisatipoLancto,FTipoLancto)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTipoLancto);
     End;


end;

procedure TFcorrecaodeTitulos.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     edtdata.text:='';
     edtdata.text:=DateToStr(now);
     edttaxa.text:='';
     edttipolancto.text:='';
     try
        ObjPendencia:=TObjPendencia.create;
        ObjLancamento:=TObjLancamento.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Pend�ncia!',mterror,[mbok],0);
           Btgravar.enabled:=False;
           exit;
     End;
     Btgravar.Enabled:=True;
     FescolheImagemBotao.PegaFiguraBotoes(nil,nil,nil,btgravar,nil,nil,nil,nil);
end;

end.
