unit UAlteraPendencias_Titulo;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TFAlteraPendencias_Titulo = class(TForm)
    LBVencimentos: TListBox;
    LBValores: TListBox;
    EdtValor: TEdit;
    GroupBox2: TGroupBox;
    labTotalparcelas: TLabel;
    btajustavalores: TButton;
    btsair: TBitBtn;
    btajustadata: TBitBtn;
    Label2: TLabel;
    Label1: TLabel;
    edtqtdeparcelas: TEdit;
    btcancelar: TBitBtn;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lbvalortitulo: TLabel;
    lbquantparcelas: TLabel;
    lbparcelasquitadas: TLabel;
    lbvalorparcelasquitadas: TLabel;
    lbparcelasquitadasparcialmente: TLabel;
    lbvalorparcelasquitadasparcialmente: TLabel;
    lbparcelasabertas: TLabel;
    lbvalorparcelasabertas: TLabel;
    Shape2: TShape;
    Shape1: TShape;
    Label19: TLabel;
    Label20: TLabel;
    LbCodigos: TListBox;
    BtAlteradaQuantidades: TBitBtn;
    BtAlteraValor: TBitBtn;
    procedure EdtValorKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure LBValoresClick(Sender: TObject);
    procedure LBVencimentosClick(Sender: TObject);
    procedure LBVencimentosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LBValoresDblClick(Sender: TObject);
    procedure btajustavaloresClick(Sender: TObject);
    procedure LBVencimentosDblClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure LBVencimentosKeyPress(Sender: TObject; var Key: Char);
    procedure btajustadataClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtqtdeparcelasKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
         Procedure RecalculaParcelas;
         Procedure  SomaParcelas;
    { Private declarations }
  public
        Function PegaValor(NumParcela:Integer):String;
        Function PegaVencimento(NumParcela:Integer):String;
    { Public declarations }
  end;

var
  FAlteraPendencias_Titulo: TFAlteraPendencias_Titulo;
implementation

uses UessencialGlobal, UData;

{$R *.DFM}

procedure TFAlteraPendencias_Titulo.EdtValorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If key=#13
     Then btajustavaloresClick(Sender);


     If not (key in['0'..'9',#8,',','.'])
     Then key:=#0
     Else Begin
               If (Key='.')
               Then Key:=',';
          End;
end;

procedure TFAlteraPendencias_Titulo.FormActivate(Sender: TObject);
begin
     Tag:=0;
     EdtValor.text:='';
     edtqtdeparcelas.text:=inttostr(LBValores.items.count);
     Self.SomaParcelas;
     PegaFiguraBotao(btsair,'BOTAOSAIR.BMP');
     PegaFiguraBotao(btcancelar,'BOTAOCANCELAR.BMP');
end;

function TFAlteraPendencias_Titulo.PegaValor(NumParcela: Integer): String;
begin

     Result:=LBValores.items[NumParcela-1];

end;

procedure TFAlteraPendencias_Titulo.LBValoresClick(Sender: TObject);
begin
     LBVencimentos.ItemIndex:=LBValores.itemindex;
end;

procedure TFAlteraPendencias_Titulo.LBVencimentosClick(Sender: TObject);
begin
     LBValores.itemindex:=LBVencimentos.ItemIndex;
end;

procedure TFAlteraPendencias_Titulo.LBVencimentosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     If Key=vk_return
     Then Begin
             EdtValor.text:=Lbvalores.Items[Lbvalores.itemindex];
             edtvalor.setfocus;
          End;

end;

procedure TFAlteraPendencias_Titulo.LBValoresDblClick(Sender: TObject);
begin
     EdtValor.text:=Lbvalores.Items[Lbvalores.itemindex];
     edtvalor.setfocus;
end;

procedure TFAlteraPendencias_Titulo.RecalculaParcelas;
var
cont:Integer;
ValorParcelaStr:String;
begin
     if LBValores.ItemIndex=-1
     Then LBValores.itemindex:=0;

     ValorParcelaStr:=LBValores.Items[LBValores.itemindex];

     for cont:=LBValores.itemindex to LBValores.items.count-1 do
     Begin
           LBValores.Items[cont]:=ValorParcelaStr;
     End;

end;

Procedure TFAlteraPendencias_Titulo.SomaParcelas;
var
Cont:Integer;
Soma:Currency;
begin
     soma:=0;
     for cont:=0 to LbValores.items.count-1 do
     Begin
       soma:=soma+StrToFloat(LbValores.items[cont]);
     End;
     labTotalparcelas.caption:=formata_valor(FloatToStr(Soma));
end;

procedure TFAlteraPendencias_Titulo.btajustavaloresClick(Sender: TObject);
begin
     RecalculaParcelas;
     Self.SomaParcelas;
end;

procedure TFAlteraPendencias_Titulo.LBVencimentosDblClick(Sender: TObject);
begin
        if (Fdata=nil) //*** ALEX - CRIANDO FORM
        then Application.CreateForm(TFdata,Fdata);
        Fdata.edtdata.text:=LBVencimentos.items[lbvencimentos.itemindex];
        Fdata.ShowModal;

        if (Fdata.tag=1)
        Then Begin
                  Try
                     strtodate(Fdata.edtdata.text);
                     LBVencimentos.Items[lbvencimentos.itemindex]:=Fdata.edtdata.text;
                  Except
                        mensagemerro('Data inv�lida');
                        exit;
                  End;
        End;
        FreeAndNil(Fdata);  //***ALEX - DESTRUINDO FORM
end;

procedure TFAlteraPendencias_Titulo.btsairClick(Sender: TObject);
begin
     Close;
     tag:=0
end;

function TFAlteraPendencias_Titulo.PegaVencimento(NumParcela: Integer): String;
begin
     Result:=LBVencimentos.items[NumParcela-1];
end;

procedure TFAlteraPendencias_Titulo.LBVencimentosKeyPress(Sender: TObject;
  var Key: Char);
begin
     If Key=#13
     Then Begin
               LBVencimentos.OnDblClick(sender);
     End;
end;

procedure TFAlteraPendencias_Titulo.btajustadataClick(Sender: TObject);
var
Novadata,DataTmp:Tdate;
cont:integer;
dia, mes,ano:word;
begin
     if (LBVencimentos.Items.Count=1)
     Then exit;

     DataTmp:=StrToDate(LBVencimentos.Items[LBVencimentos.itemindex]);

     //a partir do atual
     DecodeDate(datatmp,ano,mes,dia);
     Novadata:=DataTmp;
     for cont:=LBVencimentos.itemindex+1 to LBVencimentos.Items.count-1 do
     Begin
          Novadata:=IncMonth(novadata,1);
          LBVencimentos.Items[cont]:=datetostr(novadata);
          Self.SomaParcelas;
     End;
end;

procedure TFAlteraPendencias_Titulo.btcancelarClick(Sender: TObject);
begin
     tag:=1;
     close;
end;

procedure TFAlteraPendencias_Titulo.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     edtqtdeparcelas.setfocus;
     Self.tag:=1;
end;

procedure TFAlteraPendencias_Titulo.edtqtdeparcelasKeyPress(
  Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.btajustadataClick(Sender);
end;

procedure TFAlteraPendencias_Titulo.BitBtn1Click(Sender: TObject);
var
cont,qt:Integer;
paralela1,paralela2:TStringList;
begin
     Try
        paralela1:=TStringList.create;
        paralela2:=TStringList.create;
     Except
           exit;
     End;

     Try

       Try
          qt:=strtoint(edtqtdeparcelas.text);
       Except
             qt:=0;
             exit;
       End;

       if (qt=LBValores.Items.Count)
       Then exit;

       if (qt<LBValores.Items.Count)
       Then Begin
                 paralela1.clear;
                 paralela2.clear;
                 for cont:=0 to qt-1 do
                 Begin
                      paralela1.add(LBValores.Items[cont]);
                      paralela2.add(LBVencimentos.Items[cont]);
                 End;
                 LBVencimentos.items.clear;
                 LBValores.Items.clear;

                 for cont:=0 to qt-1 do
                 Begin
                      LBValores.items.add(paralela1[cont]);
                      LBVencimentos.Items.add(paralela2[cont]);
                 End;
       End
       Else Begin
                 for cont:=1 to (qt-LBValores.Items.count) do
                 Begin
                      LBValores.Items.add('0');
                      LBVencimentos.items.add('01/01/1500');
                 End;
       End;
     Finally
            Self.SomaParcelas;
            FreeAndNil(paralela1);
            FreeAndNil(paralela2);
     End;
End;
procedure TFAlteraPendencias_Titulo.BitBtn2Click(Sender: TObject);
var
ValorTemp:Currency;
begin
     try
        Valortemp:=strtofloat(edtvalor.text);
        if (LBValores.itemindex=-1)
        or (LBVencimentos.itemindex=-1)
        Then Begin
                  LBValores.itemindex:=0;
                  LBVencimentos.itemindex:=0;
        End;

        LBValores.items[LBValores.itemindex]:=edtvalor.text;
        edtvalor.text:='';
        Self.SomaParcelas;

     Except
     End;

end;

end.
