unit UCOTACAOMOEDA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, Grids, UObjCOTACAOMOEDA,
  jpeg;

type
  TFCOTACAOMOEDA = class(TForm)
    ImagemFundo: TImage;
    lbNomeMoeda1: TLabel;
    lbnomeMoeda2: TLabel;
    panelbotes: TPanel;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btsair: TBitBtn;
    StrGrid: TStringGrid;
    Label2: TLabel;
    lbNomeMoedaPadrao: TLabel;
    lbData: TLabel;
    lbDataHoje: TLabel;
    Label3: TLabel;
    Label1: TLabel;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure StrGridSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
         ObjCOTACAOMOEDA:TObjCOTACAOMOEDA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOTACAOMOEDA: TFCOTACAOMOEDA;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UobjMOEDA,
  UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCOTACAOMOEDA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCOTACAOMOEDA do
    Begin
        Submit_DATA(lbData.Caption);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCOTACAOMOEDA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCOTACAOMOEDA do
     Begin
        lbData.Caption:=Get_DATA;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCOTACAOMOEDA.TabelaParaControles: Boolean;
begin
     If (Self.ObjCOTACAOMOEDA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;

//****************************************
procedure TFCOTACAOMOEDA.FormActivate(Sender: TObject);
begin
     Self.limpaLabels;
     desabilita_campos(Self);
     FescolheImagemBotao.PegaFiguraBotoespequeno(nil,nil,nil,btgravar,btpesquisar,nil,btexcluir,btsair,nil);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     retira_fundo_labels(self);
end;

procedure TFCOTACAOMOEDA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCOTACAOMOEDA=Nil)
     Then exit;

     If (Self.ObjCOTACAOMOEDA.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjCOTACAOMOEDA.free;
end;

procedure TFCOTACAOMOEDA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFCOTACAOMOEDA.btgravarClick(Sender: TObject);
Var DataRetorno:String;
begin
     If (Self.ObjCOTACAOMOEDA.SalvarGrid(StrGrid, lbDataHoje.Caption, true)=False)
     Then exit;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

     Self.ObjCOTACAOMOEDA.MostraCotacaodoDia(StrGrid, DataRetorno, DateToStr(Now));
     lbData.Caption:=DataRetorno;

end;

procedure TFCOTACAOMOEDA.btcancelarClick(Sender: TObject);
begin
     Self.ObjCOTACAOMOEDA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCOTACAOMOEDA.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFCOTACAOMOEDA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCOTACAOMOEDA.Get_pesquisaView,Self.ObjCOTACAOMOEDA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCOTACAOMOEDA.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCOTACAOMOEDA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjCOTACAOMOEDA.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFCOTACAOMOEDA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCOTACAOMOEDA.FormShow(Sender: TObject);
Var  DataRetorno:String;
     pautorizou:String;
begin
     PegaCorForm(Self);

     Try
        Self.ObjCOTACAOMOEDA:=TObjCOTACAOMOEDA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;

     if (Self.ObjCOTACAOMOEDA.Moeda.ResgataMoedaPadrao=false)
     then Begin
             MensagemErro('Nenhuma moeda padr�o cadastrada. Cadastre na tela de Moeda antes de Informar a cota��o');
             Self.Close;
             exit;
     end;
     Self.ObjCOTACAOMOEDA.Moeda.TabelaparaObjeto;
     lbNomeMoedaPadrao.Caption:= Self.ObjCOTACAOMOEDA.MOEDA.Get_SimboloMoedaPadrao+' - '+Self.ObjCOTACAOMOEDA.MOEDA.Get_NOME;


     StrGrid.Cells[0,0]:='COD';
     StrGrid.Cells[1,0]:='MOEDA';
     StrGrid.Cells[2,0]:='VALOR DA MOEDA PARA 1 '+Self.ObjCOTACAOMOEDA.MOEDA.Get_NOME;

     Self.ObjCOTACAOMOEDA.MostraCotacaodoDia(StrGrid, DataRetorno, DateToStr(Now));
     lbData.Caption:=DataRetorno;
     AjustaLArguraColunaGrid(StrGrid);

     lbDataHoje.Caption:=DateToStr(Now);

end;
//CODIFICA ONKEYDOWN E ONEXIT



procedure TFCOTACAOMOEDA.StrGridSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
    if ((ACol = 0) or (ACol = 1))
    then StrGrid.Options:=StrGrid.Options - [goEditing]
    else StrGrid.Options:=StrGrid.Options + [goEditing];   

end;

end.

