unit Ugeralancamento;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjGeraLancamento;

type
  TFGeraLancamento = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    BtCancelar: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label18: TLabel;
    EdtCodigo: TEdit;
    edttipolancto: TEdit;
    edthistorico: TEdit;
    edtobjetogerador: TEdit;
    edthistoricogerador: TEdit;
    edtCodigoPlanodeContas: TMaskEdit;
    ImagemFundo: TImage;
    LbPlanodeContas: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edttipolanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoPlanodeContasKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure edtCodigoPlanodeContasExit(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function atualizaQuantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGeraLancamento: TFGeraLancamento;
  ObjGeraLancamento:TObjGeraLancamento;

implementation

uses UessencialGlobal, Upesquisa, UTipoLancto, UPLanodeContas,
  UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFGeraLancamento.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjGeraLancamento do
    Begin
        Submit_CODIGO           (edtCODIGO.text          );
        Submit_TipoLAncto       (edtTipoLAncto.text      );
        Submit_Historico        (edtHistorico.text       );
        Submit_ObjetoGerador    (edtObjetoGerador.text   );
        Submit_HistoricoGerador (edtHistoricoGerador.text);
        Submit_CodigoPlanodeContas(edtCodigoPlanodeContas.text); 
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFGeraLancamento.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjGeraLancamento do
     Begin
        edtCODIGO.text          :=Get_CODIGO            ;
        edtTipoLAncto.text      :=Get_TipoLAncto        ;
        edtHistorico.text       :=Get_Historico         ;
        edtObjetoGerador.text   :=Get_ObjetoGerador     ;
        edtHistoricoGerador.text:=Get_HistoricoGerador  ;
        edtCodigoPlanodeContas.text:=Get_CodigoPlanodeContas;

        if TipoLAncto.ObjPlanodeContas.LocalizaCodigo(Get_CodigoPlanodeContas)=True
        then TipoLAncto.ObjPlanodeContas.TabelaparaObjeto
        else TipoLAncto.ObjPlanodeContas.ZerarTabela;

        LbPlanodeContas.Caption:=TipoLAncto.ObjPlanodeContas.Get_Nome;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFGeraLancamento.TabelaParaControles: Boolean;
begin
     ObjGeraLancamento.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFGeraLancamento.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjGeraLancamento=Nil)
     Then exit;

    If (ObjGeraLancamento.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjGeraLancamento.free;
end;

procedure TFGeraLancamento.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFGeraLancamento.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFGeraLancamento.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);
     LbPlanodeContas.Caption:='';
     //edtcodigo.text:='0';
     edtcodigo.text:=ObjGeraLancamento.Get_novocodigo;
     edtcodigo.enabled:=False;
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjGeraLancamento.status:=dsInsert;
     edttipolancto.setfocus;
end;

procedure TFGeraLancamento.BtCancelarClick(Sender: TObject);
begin
     ObjGeraLancamento.cancelar;
     
     limpaedit(Self);
     LbPlanodeContas.Caption:='';
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFGeraLancamento.BtgravarClick(Sender: TObject);
begin

     If ObjGeraLancamento.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjGeraLancamento.salvar(true)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     LbPlanodeContas.Caption:='';
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFGeraLancamento.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     ObjGeraLancamento.Status:=dsEdit;
     edttipolancto.setfocus;
end;

procedure TFGeraLancamento.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjGeraLancamento.Get_pesquisa,ObjGeraLancamento.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjGeraLancamento.status<>dsinactive
                                  then exit;

                                  If (ObjGeraLancamento.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGeraLancamento.btalterarClick(Sender: TObject);
begin
    If (ObjGeraLancamento.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFGeraLancamento.btexcluirClick(Sender: TObject);
begin
     If (ObjGeraLancamento.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjGeraLancamento.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjGeraLancamento.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     LbPlanodeContas.Caption:='';
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFGeraLancamento.edttipolanctoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Ftipolancto:TFtipolancto;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Ftipolancto:=TFtipolancto.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjgeraLancamento.Get_PesquisatipoLancto,Objgeralancamento.Get_TituloPesquisaTipolancto,Ftipolancto)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Ftipolancto);
     End;


end;

procedure TFGeraLancamento.edtCodigoPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fplanodecontas:TFplanodecontas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fplanodecontas:=TFplanodecontas.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Objgeralancamento.TipoLAncto.ObjPlanodeContas.Get_Pesquisa,Objgeralancamento.TipoLAncto.ObjPlanodeContas.Get_tituloPesquisa,Fplanodecontas)=True)
            Then Begin
                      Try
                            If (FpesquisaLocal.showmodal=mrok)
                            Then Begin
                                  if ObjGeraLancamento.TipoLAncto.ObjPlanodeContas.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=True
                                  then ObjGeraLancamento.TipoLAncto.ObjPlanodeContas.TabelaparaObjeto;
                                  Self.edtCodigoPlanodeContas.text:=ObjGeraLancamento.TipoLAncto.ObjPlanodeContas.get_codigo;
                                  Self.LbPlanodeContas.Caption:=ObjGeraLancamento.TipoLAncto.ObjPlanodeContas.get_nome;
                            End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fplanodecontas);
     End;

end;
procedure TFGeraLancamento.edtCodigoPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not(key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFGeraLancamento.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

          limpaedit(Self);
     desabilita_campos(Self);
     LbPlanodeContas.Caption:='';

     Try
        ObjGeraLancamento:=TObjGeraLancamento.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

function TFGeraLancamento.atualizaQuantidade: string;
begin
   result:='Existem '+ContaRegistros('TABGERALANCAMENTO','codigo')+' Geradores de Lan�amento Cadastrados';
end;

procedure TFGeraLancamento.edtCodigoPlanodeContasExit(Sender: TObject);
begin
     LbPlanodeContas.Caption:='';
     if edtCodigoPlanodeContas.Text<>''
     then begin
            if ObjGeraLancamento.TipoLAncto.ObjPlanodeContas.LocalizaCodigo(edtCodigoPlanodeContas.Text)=True
            then begin
                  ObjGeraLancamento.TipoLAncto.ObjPlanodeContas.TabelaparaObjeto;
                  LbPlanodeContas.Caption:=ObjGeraLancamento.TipoLAncto.ObjPlanodeContas.Get_Nome;
            end;
     end;

end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Obj.Get_PesquisaGeraLancamento,Obj.Get_TituloPesquisaGeraLancamento,Self)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If Self.edtTipoLancto.text<>''
                                 Then Self.LbTipoLanctoNome.caption:=OBjLancamento.PegaNometipolancto(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;
}
