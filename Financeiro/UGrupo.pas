unit UGrupo;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjGrupo;

type
  TFgrupo = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    BtNovo: TBitBtn;
    btalterar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    BtCancelar: TBitBtn;
    btsair: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    EdtCodigo: TEdit;
    Label2: TLabel;
    edtdescricao: TEdit;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         Function AtualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fgrupo: TFgrupo;
  ObjGrupo:TObjGrupo;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFgrupo.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjGrupo do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Descricao       ( edtdescricao.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFgrupo.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjGrupo do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtdescricao.text       :=Get_Descricao       ;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFgrupo.TabelaParaControles: Boolean;
begin
     ObjGrupo.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFgrupo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjGrupo=Nil)
     Then exit;

    If (ObjGrupo.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjGrupo.free;
    
end;

procedure TFgrupo.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFgrupo.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFgrupo.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjGrupo.get_novocodigo;
     edtcodigo.Visible:=False;
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;

     ObjGrupo.status:=dsInsert;
     Edtdescricao.setfocus;
end;

procedure TFgrupo.BtCancelarClick(Sender: TObject);
begin
     ObjGrupo.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFgrupo.BtgravarClick(Sender: TObject);
begin

     If ObjGrupo.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjGrupo.salvar=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFgrupo.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     ObjGrupo.Status:=dsEdit;
     edtdescricao.setfocus;
end;

procedure TFgrupo.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjGrupo.Get_pesquisa,ObjGrupo.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjGrupo.status<>dsinactive
                                  then exit;

                                  If (ObjGrupo.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFgrupo.btalterarClick(Sender: TObject);
begin
    If (ObjGrupo.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFgrupo.btexcluirClick(Sender: TObject);
begin
     If (ObjGrupo.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjGrupo.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjGrupo.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFgrupo.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjGrupo:=TObjGrupo.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;


     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;
     lbquantidade.Caption:=AtualizaQuantidade;
     Uessencialglobal.PegaCorForm(Self);
end;

function TFgrupo.AtualizaQuantidade: string;
begin
      result:='Existem '+ ContaRegistros('TABGRUPO','codigo') + ' Grupos Cadastrados';
end;

end.
