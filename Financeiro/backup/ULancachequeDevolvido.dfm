�
 TFLANCACHEQUEDEVOLVIDO 0�  TPF0TFlancaChequeDevolvidoFlancaChequeDevolvidoLeft� Top� WidthbHeight+Caption   Devolução de ChequesColorclMenuCtl3DFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	OldCreateOrderOnClose	FormClose
OnKeyPressFormKeyPressOnShowFormShowPixelsPerInch`
TextHeight TPanelPanel1Left Top/WidthZHeight� AlignalBottomCaptionPanel1TabOrder  TShapeShape3LeftTop	WidthOHeight� Brush.ColorclInfoBk	Pen.ColorclNavy  TLabelLabel4LeftTopWidthHeightCaptionCompTransparent	  TLabelLabel5Left.TopWidthHeightCaptionBancoTransparent	  TLabelLabel6LeftbTopWidth'HeightCaption   AgênciaTransparent	  TLabelLabel7Left� TopWidthHeightCaptionDVTransparent	  TLabelLabel8Left� TopWidthHeightCaptionC1Transparent	  TLabelLabel9Left� TopWidthHeightCaptionContaTransparent	  TLabelLabel10Left@TopWidthHeightCaptionC2Transparent	  TLabelLabel11Left[TopWidthHeightCaption   SérieTransparent	  TLabelLabel12Left�TopWidth7HeightCaption   Cheque N.ºTransparent	  TLabelLabel3Left�TopWidthHeightCaptionC3Transparent	  TLabelLabel17Left�TopWidthHeightCaptionValorTransparent	  TLabelLabel13Left�Top� WidthHeightCaptionParaTransparent	  TLabelLabel16Left� Top� WidthTHeightCaption   Código de Barras Transparent	  TLabelLabel18Left� Top� WidthSHeightCaptionPortador e Cpf 02Transparent	  TLabelLabel14Left� TopoWidthSHeightCaptionPortador e Cpf 01Transparent	  TDBEditDBCompLeftTop(Width!Height
DataSourceDataSourcePesquisaTabOrder   TDBEditDbbancoLeft.Top(Width1Height
DataSourceDataSourcePesquisaTabOrder  TDBEdit	DbAgenciaLeftbTop(Width1Height
DataSourceDataSourcePesquisaTabOrder  TDBEditDbDVLeft� Top(WidthHeight
DataSourceDataSourcePesquisaTabOrder  TDBEditDbC1Left� Top(Width!Height
DataSourceDataSourcePesquisaTabOrder  TDBEditDbContaLeft� Top(WidthiHeight
DataSourceDataSourcePesquisaTabOrder  TDBEditDbC2Left@Top(WidthHeight
DataSourceDataSourcePesquisaTabOrder  TDBEditDbSerieLeft[Top(Width1Height
DataSourceDataSourcePesquisaTabOrder  TDBEditDbNumChequeLeft�Top(WidthPHeight
DataSourceDataSourcePesquisaTabOrder  TDBEditDbC3Left�Top(WidthHeight
DataSourceDataSourcePesquisaTabOrder	  TDBEditDbValorLeft�Top(WidthVHeight
DataSourceDataSourcePesquisaTabOrder
  TDBEditDbCpfCliente1Left�TopnWidthHeight
DataSourceDataSourcePesquisaTabOrder  TDBEditDbCpfCliente2Left�Top� WidthHeight
DataSourceDataSourcePesquisaTabOrder  TDBEditDbVencimentoLeft�Top� WidthUHeight
DataSourceDataSourcePesquisaTabOrder  TDBEditdbcodigobarrasLeft� Top� Width�Height
DataSourceDataSourcePesquisaTabOrder  TDBEdit
DbCliente1Left/TopnWidth� Height
DataSourceDataSourcePesquisaTabOrder  TDBEdit
DbCliente2Left/Top� Width� Height
DataSourceDataSourcePesquisaTabOrder   TDBGridDbGridPesquisaLeft Top_WidthZHeight� AlignalBottom
DataSourceDataSourcePesquisaTabOrderTitleFont.CharsetDEFAULT_CHARSETTitleFont.ColorclWindowTextTitleFont.Height�TitleFont.NameMS Sans SerifTitleFont.Style 
OnKeyPressDbGridPesquisaKeyPress  TTabbedNotebookGuiaLeft Top WidthZHeight_AlignalClientTabFont.CharsetDEFAULT_CHARSETTabFont.Color	clBtnTextTabFont.Height�TabFont.NameMS Sans SerifTabFont.Style TabOrder TTabPage LeftTopCaption   &1 - Cheque de 3º TBevelBevel2LeftTopWidth�Height9ShapebsFrameStylebsRaised  TLabelLabel1LeftTopWidthJHeightCaptionFiltros de buscaTransparent  TLabelLabel22LeftTopWidth(HeightCaptionPortadorTransparent	  TLabellbnomeportadorLeftTop0Width(HeightCaptionPortadorTransparent	  TLabelLabel2Left=TopWidth3HeightCaption
   Nº chequeTransparent	  TLabelLabel24Left� TopWidthHeightCaptionValorTransparent	  TLabelLabel23Left	TopWidthHeightCaptionCpf 01Transparent	  TEditedtportador_filtroLeftTopWidth3Height	MaxLengthTabOrder OnExitedtportador_filtroExit	OnKeyDownedtportador_filtroKeyDown
OnKeyPressedtportador_filtroKeyPress  TEditedtnumcheque_filtroLeft>TopWidth;Height	MaxLengthTabOrder  TEditedtvalor_filtroLeft� TopWidthXHeight	MaxLengthTabOrder
OnKeyPressedtvalor_filtroKeyPress  TEditedtcpf_filtroLeft
TopWidth� Height	MaxLengthTabOrder  TButtonBtConsultarLeft�Top
WidthRHeight5Hint3   Será gerada uma conta para  o cliente deste chequeCaption
&ConsultarTabOrderOnClickBtConsultarClick  TButtonBtGeraContasLeft�TopWidthZHeight9HintS   O cheque será enviado par ao portador cheques devolvidos e contas serão lançadasCaption&Gera ContasParentShowHintShowHint	TabOrderOnClickBtGeraContasClick   TTabPage LeftTopCaption&2 - Cheque do Portador TBevelBevel1LeftTopWidth�Height9ShapebsFrameStylebsRaised  TLabelLabel21Left� TopWidthHeightCaptionValorTransparent	  TLabelLabel20Left=TopWidth3HeightCaption
   Nº chequeTransparent	  TLabellbnomeportador_CPLeftTop0Width(HeightCaptionPortadorTransparent	  TLabelLabel15LeftTopWidth(HeightCaptionPortadorTransparent	  TButtonBtGeraConta_CPLeft�TopWidthZHeight9HintS   O cheque será enviado par ao portador cheques devolvidos e contas serão lançadasCaption&Gera ContasParentShowHintShowHint	TabOrder  TButtonbtConsulta_CPLeft�Top
WidthRHeight5Hint3   Será gerada uma conta para  o cliente deste chequeCaption
&ConsultarTabOrderOnClickbtConsulta_CPClick  TEditedtvalor_CPLeft� TopWidthXHeight	MaxLengthTabOrder
OnKeyPressedtvalor_filtroKeyPress  TEditEdtnumCheque_CPLeft>TopWidth;Height	MaxLengthTabOrder  TEditedtportador_CPLeftTopWidth3Height	MaxLengthTabOrder OnExitedtportador_CPExit	OnKeyDownedtportador_CPKeyDown
OnKeyPressedtportador_filtroKeyPress    TIBQueryQueryPesquisaLeft6Top\  TDataSourceDataSourcePesquisaDataSetQueryPesquisaLeftTop^   