unit ULancachequeDevolvido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, ComCtrls, Grids, DBGrids, ExtCtrls,
  DB, IBCustomDataSet, IBQuery, DBCtrls,UobjCHEQUEDEVOLVIDO, TabNotBk;

type
  TFlancaChequeDevolvido = class(TForm)
    QueryPesquisa: TIBQuery;
    DataSourcePesquisa: TDataSource;
    Panel1: TPanel;
    Shape3: TShape;
    Label4: TLabel;
    DBComp: TDBEdit;
    Dbbanco: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DbAgencia: TDBEdit;
    DbDV: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    DbC1: TDBEdit;
    DbConta: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    DbC2: TDBEdit;
    DbSerie: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DbNumCheque: TDBEdit;
    DbC3: TDBEdit;
    Label3: TLabel;
    Label17: TLabel;
    DbValor: TDBEdit;
    DbCpfCliente1: TDBEdit;
    DbCpfCliente2: TDBEdit;
    DbVencimento: TDBEdit;
    Label13: TLabel;
    dbcodigobarras: TDBEdit;
    Label16: TLabel;
    Label18: TLabel;
    Label14: TLabel;
    DbCliente1: TDBEdit;
    DbCliente2: TDBEdit;
    DbGridPesquisa: TDBGrid;
    Guia: TTabbedNotebook;
    Bevel2: TBevel;
    Label1: TLabel;
    Label22: TLabel;
    edtportador_filtro: TEdit;
    lbnomeportador: TLabel;
    Label2: TLabel;
    edtnumcheque_filtro: TEdit;
    edtvalor_filtro: TEdit;
    Label24: TLabel;
    Label23: TLabel;
    edtcpf_filtro: TEdit;
    BtConsultar: TButton;
    BtGeraContas: TButton;
    BtGeraConta_CP: TButton;
    btConsulta_CP: TButton;
    Bevel1: TBevel;
    edtvalor_CP: TEdit;
    Label21: TLabel;
    Label20: TLabel;
    EdtnumCheque_CP: TEdit;
    lbnomeportador_CP: TLabel;
    edtportador_CP: TEdit;
    Label15: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtportador_filtroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportador_filtroExit(Sender: TObject);
    procedure edtportador_filtroKeyPress(Sender: TObject; var Key: Char);
    procedure BtConsultarClick(Sender: TObject);
    procedure edtvalor_filtroKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtGeraContasClick(Sender: TObject);
    procedure DbGridPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure edtportador_CPExit(Sender: TObject);
    procedure edtportador_CPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btConsulta_CPClick(Sender: TObject);
  private
    { Private declarations }
    objChequeDevolvido:TobjChequeDevolvido;
    Procedure LimpaLabels;
  public
    { Public declarations }
  end;

var
  FlancaChequeDevolvido: TFlancaChequeDevolvido;


implementation

uses UessencialGlobal,UobjPortador, UDataModulo, UObjTitulo,
  UObjGeraTitulo, UObjGeraTransferencia, UFiltraImp, UmostraStringList,
  UmostraStringGrid, UObjCredorDevedor, UObjLancamento;

{$R *.dfm}

procedure TFlancaChequeDevolvido.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     Try
        //Self.ObjChequeDevolvido.Cheque=TobjChequesPortador.create;
        QueryPesquisa.Database:=FdataModulo.Ibdatabase;
        //ObjValoresTransferenciaPortador:=TObjValoresTransferenciaPortador.Create;
        ObjChequeDevolvido:=TobjChequeDevolvido.CReate;
     Except
           desabilita_campos(Self);
           Messagedlg('N�o foi poss�vel criar o Objeto de Cheques Devolvidos!',mterror,[mbok],0);
           exit;
     End;
     guia.pageindex:=0;
     edtportador_filtro.SetFocus;

     DBComp.DataField:='Comp';
     DBComp.Enabled:=False;
     Dbbanco.DataField:='Banco';
     Dbbanco.Enabled:=False;
     DbAgencia.DataField:='Agencia';
     DbAgencia.Enabled:=False;
     DbC1.DataField:='C1';
     DbC1.Enabled:=False;
     DbConta.DataField:='Conta';
     DbConta.Enabled:=False;
     DbC2.DataField:='C2';
     DbC2.Enabled:=False;
     DbSerie.DataField:='Serie';
     DbSerie.Enabled:=False;
     DbNumCheque.DataField:='NumCheque';
     DbNumCheque.Enabled:=False;
     DbC3.DataField:='C3';
     DbC3.Enabled:=False;
     DbCliente1.DataField:='Cliente1';
     DbCliente1.Enabled:=False;
     DbCpfCliente1.DataField:='CPFCliente1';
     DbCpfCliente1.Enabled:=False;
     DbCliente2.DataField:='Cliente2';
     DbCliente2.Enabled:=False;
     DbCpfCliente2.DataField:='CPFCliente2';
     DbCpfCliente2.Enabled:=False;
     dbcodigobarras.datafield:='CodigodeBarras';
     dbcodigobarras.Enabled:=False;
     DbValor.DataField:='Valor';
     DbValor.Enabled:=False;
     DbVencimento.DataField:='Vencimento';
     DbVencimento.Enabled:=False;

end;

procedure TFlancaChequeDevolvido.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     {if (Self.ObjChequeDevolvido.Cheque>nil)
     Then Self.ObjChequeDevolvido.ChequeFree;

     if (ObjValoresTransferenciaPortador<>nil)
     Then ObjValoresTransferenciaPortador.free;}

     if (ObjChequeDevolvido<>nil)
     Then ObjChequeDevolvido.Free;
end;

procedure TFlancaChequeDevolvido.edtportador_filtroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     objChequeDevolvido.Cheque.Portador.edtportadorKeyDown(sender,key,shift);
end;

procedure TFlancaChequeDevolvido.edtportador_filtroExit(Sender: TObject);
begin
     lbnomeportador.caption:='';

     if Tedit(Sender).Text=''
     Then exit;

     if (Self.ObjChequeDevolvido.Cheque.Portador.LocalizaCodigo(Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.ObjChequeDevolvido.Cheque.Portador.TabelaparaObjeto;
     lbnomeportador.Caption:=Self.ObjChequeDevolvido.Cheque.Portador.Get_Nome;
end;

procedure TFlancaChequeDevolvido.LimpaLabels;
begin
     lbnomeportador.caption:='';
end;

procedure TFlancaChequeDevolvido.edtportador_filtroKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFlancaChequeDevolvido.BtConsultarClick(Sender: TObject);
begin
     With QueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.add('Select * from TabChequesPortador where ChequedoPortador=''N'' ');

          if (edtportador_filtro.Text<>'')
          Then Sql.add('and TabChequesPortador.Portador='+edtportador_filtro.Text);

          if (edtnumcheque_FILTRO.Text<>'')
          Then Sql.add('and TabChequesPortador.NumCheque like '+#39+'%'+edtnumcheque_filtro.Text+'%'+#39);

          if (edtvalor_filtro.Text<>'')
          Then Sql.add('and TabchequesPortador.valor='+virgulaparaponto(edtvalor_filtro.text));

          if (edtcpf_filtro.Text<>'')
          Then Sql.add('and TabChequesPortador.CpfCliente1 like ''%'+edtcpf_filtro.text+'%'' ');
          
          open;
          formatadbgrid(DbGridPesquisa);
          dbgridpesquisa.setfocus;

                   
     End;

end;

procedure TFlancaChequeDevolvido.edtvalor_filtroKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then if key='.'
          Then key:=','
          Else key:=#0; 
end;

procedure TFlancaChequeDevolvido.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFlancaChequeDevolvido.BtGeraContasClick(Sender: TObject);
var
ObjQueryTemp:tibquery;
plancamento:Str09;
ptitulo:str09;
PcredorDevedor,PCodigoCredorDevedor:Str09;
Ptituloapagar,Ptituloareceber,Ptransferencia:Str09;
objgeratitulo:TObjGeraTitulo;
ObjGeraTransferencia:TObjGeraTransferencia;
pdatadevolucao:Tdate;
PcodigochequeDevolvido,PmotivoDevolucao:Str09;
cont:integer;
PlistaCheque:TStringList;
begin
     if (Messagedlg('Tem certeza que deseja lan�ar esse cheque como devolvido?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     //Primeiro descobrir qual a conta  a receber que o cadastrou
     //assim tenho Quem me Deu o Cheque
     //De posse dessa informacao gero uma Conta a receber da mesma pessoa
     //*************************************************************************

     //Verificar se o cheque foi utilizado por ultimo numa conta a pagar
     //se sim, gerar uma conta a pagar para o fornecedor
     //caso ele tenha sido usado em uma transferencia para algum banco
     //nao precisa gerar essa conta a pagar
     //*************************************************************************

     //Transferir ele do banco Atual para um Portador Cheques Devolvidos
     //*************************************************************************
     Ptituloapagar:='';
     Ptituloareceber:='';
     Ptransferencia:='';

     if (Self.objChequeDevolvido.localizaCheque(QueryPesquisa.FieldByName('codigo').asstring)=True)
     Then Begin
               MensagemErro('J� existe uma ocorr�ncia de devolu��o para este cheque');
               exit;
     End;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.EditMask:=MascaraData;
          LbGrupo01.Caption:='Data de Devolu��o';

          Grupo02.Enabled:=True;
          LbGrupo02.Caption:='Al�nea';
          edtgrupo02.OnKeyDown:=Self.ObjChequeDevolvido.Cheque.EdtMotivoDevolucaoKeyDown;

          Showmodal;

          if (tag=0)
          then exit;

          if (Validadata(1,pdatadevolucao,false)=False)
          Then exit;

          PmotivoDevolucao:='';

          if (edtgrupo02.Text<>'')
          then Begin
                    Try
                       strtoint(edtgrupo02.text);
                       PmotivoDevolucao:=edtgrupo02.text;

                       if (Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.LocalizaCodigo(pmotivodevolucao)=False)
                       Then begin
                                 MensagemErro('Al�nea n�o encontrada');
                                 exit;
                       End;
                       Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.TabelaparaObjeto;


                    except
                          MensagemErro('Al�nea inv�lida');
                          exit;
                    End;
          end;

     end;

     Try
        ObjQueryTemp:=tibquery.create(nil);
        ObjQueryTemp.Database:=FdataModulo.ibdatabase;

        objgeratitulo:=TObjGeraTitulo.create;
        ObjGeraTransferencia:=TObjGeraTransferencia.create;

        PlistaCheque:=TStringList.create;
     Except
        MensagemErro('Erro na tentativa de Criar a Query ou os Objetos Geradores');
        exit;
     End;

     Try

        With ObjQueryTemp do
        begin
             close;
             sql.clear;
             sql.add('Select TabTitulo.codigo as TITULO,TabTitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor,tablancamento.codigo as LANCAMENTO');
             sql.add('from tabValores');
             sql.add('join tablancamento on tabValores.Lancamento=Tablancamento.codigo');
             sql.add('left join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
             sql.add('left join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
             sql.add('where tabvalores.cheque='+QueryPesquisa.fieldbyname('codigo').asstring);
             open;
             ptitulo:=Fieldbyname('titulo').asstring;
             Plancamento:=Fieldbyname('lancamento').asstring;
             PcredorDevedor:=Fieldbyname('CredorDevedor').asstring;
             PCodigoCredorDevedor:=Fieldbyname('CodigoCredorDevedor').asstring;


             if (plancamento='')
             Then Begin
                       MensagemErro('N�o foi localizado o Lan�amento que cadastrou esse cheque no sistema');
                       exit;
             End;

             if (ptitulo='')//foi em lote, por isso nao tem titulo
             Then Begin
                       close;
                       sql.clear;
                       sql.add('Select Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                       sql.add('from tabLancamento');
                       sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                       sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                       sql.add('where LancamentoPai='+plancamento);
                       sql.add('group by Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                       open;
                       last;
                       if (recordcount=1)
                       then Begin
                                 //todos os titulos do mesmo credor
                                 PcredorDevedor:=Fieldbyname('CredorDevedor').asstring;
                                 PCodigoCredorDevedor:=Fieldbyname('CodigoCredorDevedor').asstring;
                       End
                       Else Begin
                                 //escolhendo qual credor devedor
                                 FmostraStringGrid.Configuracoesiniciais;
                                 FmostraStringGrid.StringGrid.colcount:=4;
                                 FmostraStringGrid.StringGrid.RowCount:=recordcount+1;;
                                 FmostraStringGrid.StringGrid.cols[0].clear;
                                 FmostraStringGrid.StringGrid.cols[1].clear;
                                 FmostraStringGrid.StringGrid.cols[2].clear;
                                 FmostraStringGrid.StringGrid.cols[3].clear;
                                 FmostraStringGrid.StringGrid.Cells[0,0]:='XX';
                                 FmostraStringGrid.StringGrid.Cells[1,0]:='CADASTRO';
                                 FmostraStringGrid.StringGrid.Cells[2,0]:='CODIGO ';
                                 FmostraStringGrid.StringGrid.Cells[3,0]:='NOME';
                                 first;
                                 cont:=1;
                                 While not(eof) do
                                 Begin
                                      PcredorDevedor:=fieldbyname('credordevedor').asstring;
                                      PCodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;
                                      
                                      Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.LocalizaCodigo(fieldbyname('credordevedor').asstring);
                                      Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.TabelaparaObjeto;

                                      FmostraStringGrid.StringGrid.Cells[0,cont]:=fieldbyname('credordevedor').asstring;
                                      FmostraStringGrid.StringGrid.Cells[1,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Nome;
                                      FmostraStringGrid.StringGrid.Cells[2,cont]:=fieldbyname('codigocredordevedor').asstring;
                                      FmostraStringGrid.StringGrid.Cells[3,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PCodigoCredorDevedor);
                                      inc(cont,1);
                                      next;
                                 End;
                                 AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);
                                 FmostraStringGrid.caption:='Escolha uma op��o para Conta a Receber';
                                 FmostraStringGrid.ShowModal;

                                 if (FmostraStringGrid.tag=0)
                                 Then exit;

                                 PcredorDevedor:=FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.row];
                                 PCodigocredorDevedor:=FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.row];
                       End;
             End;


             if (objgeratitulo.LocalizaHistorico('T�TULO A RECEBER DE CHEQUES DEVOLVIDOS')=False)
             then Begin
                       mensagemerro('O gerador de T�tulos com Hist�rico: "T�TULO A RECEBER DE CHEQUES DEVOLVIDOS" n�o foi encontrado');
                       exit;
             End;
             objgeratitulo.TabelaparaObjeto;

             With Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo do
             Begin
                  ZerarTabela;
                  Ptituloareceber:=Get_NovoCodigo;
                  Status:=dsinsert;
                  Submit_CODIGO(Ptituloareceber);
                  Submit_HISTORICO('REF. CHEQUE DEVOLVIDO NUM '+QueryPesquisa.fieldbyname('numcheque').asstring);
                  Submit_GERADOR(Objgeratitulo.Get_Gerador);
                  Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
                  Submit_CREDORDEVEDOR(PcredorDevedor);
                  Submit_CODIGOCREDORDEVEDOR(PCodigoCredorDevedor);
                  Submit_EMISSAO(datetostr(pdatadevolucao));
                  Submit_PRAZO(Objgeratitulo.Get_Prazo);
                  Submit_PORTADOR(Objgeratitulo.get_portador);
                  Submit_VALOR(querypesquisa.fieldbyname('valor').asstring);
                  Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
                  Submit_NUMDCTO(querypesquisa.fieldbyname('numcheque').asstring);//vai o numero do cheque
                  Submit_ISSQN('0');
                  Submit_INSS('0');
                  Submit_GeradoPeloSistema('N');
                  Submit_ParcelasIguais(True);
                  Submit_ExportaLanctoContaGer('S');
                  //salva sem commitar e sem exportar contabilidade
                  if (salvar(False,False)=False)
                  Then Begin
                            MensagemErro('N�o foi poss�vel salvar o T�tulo a receber do cheque devolvido');
                            exit;
                  End;
                  
             End;

             //localizando as transferencia ou pagamento
             PcredorDevedor:='';
             PCodigoCredorDevedor:='';

             close;
             SQL.clear;
             sql.add('Select TTP.* from TabValoresTransferenciaPortador TVTP');
             sql.add('join TabTransferenciaPortador TTP on TVTP.TransferenciaPortador=TabTransferenciaPortador.codigo');
             sql.add('where TVTP.valores='+QueryPesquisa.fieldbyname('codigo').asstring);
             sql.add('order by TTP.data,TTP.codigo');
             open;
             last;//pegando a ultima transferencia feita com esse cheque

             if (Fieldbyname('codigolancamento').asstring<>'')//foi um pagamento
             Then Begin
                         plancamento:=Fieldbyname('codigolancamento').asstring;
                         //gerar o titulo a pagar
                         Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.ZerarTabela;
                         Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.LocalizaCodigo(fieldbyname('codigolancamento').asstring);
                         Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.TabelaparaObjeto;
                         if (Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Get_CODIGO='')//em lote
                         Then Begin
                                   close;
                                   sql.clear;
                                   sql.add('Select Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                                   sql.add('from tabLancamento');
                                   sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                                   sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                                   sql.add('where LancamentoPai='+plancamento);
                                   sql.add('group by Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                                   open;
                                   last;
                                   if (recordcount=1)
                                   then Begin
                                             //todos os titulos do mesmo credor
                                             PcredorDevedor:=Fieldbyname('CredorDevedor').asstring;
                                             PCodigoCredorDevedor:=Fieldbyname('CodigoCredorDevedor').asstring;
                                   End
                                   Else Begin
                                             //escolhendo qual credor devedor
                                             FmostraStringGrid.Configuracoesiniciais;
                                             FmostraStringGrid.StringGrid.colcount:=4;
                                             FmostraStringGrid.StringGrid.RowCount:=recordcount+1;;
                                             FmostraStringGrid.StringGrid.cols[0].clear;
                                             FmostraStringGrid.StringGrid.cols[1].clear;
                                             FmostraStringGrid.StringGrid.cols[2].clear;
                                             FmostraStringGrid.StringGrid.cols[3].clear;
                                             FmostraStringGrid.StringGrid.Cells[0,0]:='XX';
                                             FmostraStringGrid.StringGrid.Cells[1,0]:='CADASTRO';
                                             FmostraStringGrid.StringGrid.Cells[2,0]:='CODIGO ';
                                             FmostraStringGrid.StringGrid.Cells[3,0]:='NOME';
                                             first;
                                             cont:=1;
                                             While not(eof) do
                                             Begin
                                                  PcredorDevedor:=fieldbyname('credordevedor').asstring;
                                                  PCodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;
                                                  
                                                  Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.LocalizaCodigo(fieldbyname('credordevedor').asstring);
                                                  Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.TabelaparaObjeto;
                                   
                                                  FmostraStringGrid.StringGrid.Cells[0,cont]:=fieldbyname('credordevedor').asstring;
                                                  FmostraStringGrid.StringGrid.Cells[1,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Nome;
                                                  FmostraStringGrid.StringGrid.Cells[2,cont]:=fieldbyname('codigocredordevedor').asstring;
                                                  FmostraStringGrid.StringGrid.Cells[3,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PCodigoCredorDevedor);
                                                  inc(cont,1);
                                                  next;
                                             End;
                                             AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);
                                             FmostraStringGrid.caption:='Escolha uma op��o para Conta a Pagar';
                                             FmostraStringGrid.ShowModal;
                                   
                                             if (FmostraStringGrid.tag=0)
                                             Then exit;
                                   
                                             PcredorDevedor:=FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.row];
                                             PCodigocredorDevedor:=FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.row];
                                   End;
                         End//lote
                         Else Begin
                                   PcredorDevedor:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo;
                                   PcodigocredorDevedor:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
                         End;
                         //titulo

                         if (objgeratitulo.LocalizaHistorico('T�TULO A PAGAR DE CHEQUES DEVOLVIDOS')=False)
                         then Begin
                                   mensagemerro('O gerador de T�tulos com Hist�rico: "T�TULO A PAGAR DE CHEQUES DEVOLVIDOS" n�o foi encontrado');
                                   exit;
                         End;
                         objgeratitulo.TabelaparaObjeto;
                         
                         With Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo do
                         Begin
                              ZerarTabela;
                              Ptituloapagar:=Get_NovoCodigo;
                              Status:=dsinsert;
                              Submit_CODIGO(Ptituloapagar);
                              Submit_HISTORICO('REF. CHEQUE DEVOLVIDO NUM '+QueryPesquisa.fieldbyname('numcheque').asstring);
                              Submit_GERADOR(Objgeratitulo.Get_Gerador);
                              Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
                              Submit_CREDORDEVEDOR(PcredorDevedor);
                              Submit_CODIGOCREDORDEVEDOR(PCodigoCredorDevedor);
                              Submit_EMISSAO(datetostr(pdatadevolucao));
                              Submit_PRAZO(Objgeratitulo.Get_Prazo);
                              Submit_PORTADOR(Objgeratitulo.get_portador);
                              Submit_VALOR(querypesquisa.fieldbyname('valor').asstring);
                              Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
                              Submit_NUMDCTO(querypesquisa.fieldbyname('numcheque').asstring);//vai o numero do cheque
                              Submit_ISSQN('0');
                              Submit_INSS('0');
                              Submit_GeradoPeloSistema('N');
                              Submit_ParcelasIguais(True);
                              Submit_ExportaLanctoContaGer('S');

                              //salva sem commitar e sem exportar contabilidade
                              if (salvar(False,False)=False)
                              Then Begin
                                        MensagemErro('N�o foi poss�vel salvar o T�tulo a pagar do cheque devolvido');
                                        exit;
                              End;
                         End;
             End;//foi um pagamento

             //e por fim transferindo o cheque para um portador de cheques devolvidos

             if (ObjGeraTransferencia.LocalizaHistorico('TRANSFER�NCIA DE CHEQUE DEVOLVIDO')=False)
             then Begin
                        MensagemErro('Gerador de Transfer�ncia: "TRANSFER�NCIA DE CHEQUE DEVOLVIDO" n�o encontrado');
                        exit;
             End;
             ObjGeraTransferencia.TabelaparaObjeto;

             With Self.objChequeDevolvido do
             Begin
                  Transferencia.ZerarTabela;
                  Ptransferencia:=Transferencia.Get_NovoCodigo;
                  Transferencia.Submit_codigo(PTransferencia);
                  Transferencia.PortadorOrigem.Submit_codigo(QueryPesquisa.fieldbyname('portador').asstring);
                  Transferencia.PortadorDestino.submit_codigo(ObjGeraTransferencia.PortadorDestino.get_codigo);
                  Transferencia.Grupo.Submit_CODIGO(ObjGeraTransferencia.Grupo.get_codigo);
                  Transferencia.Submit_Data(datetostr(pdatadevolucao));
                  Transferencia.Submit_Valor('0');//em dinheiro
                  Transferencia.Submit_Documento(Querypesquisa.fieldbyname('numcheque').asstring);
                  Transferencia.Submit_Complemento('CHEQ.DEVOLVIDO');

                  PlistaCheque.Clear;
                  PlistaCheque.add(QueryPesquisa.Fieldbyname('codigo').asstring);
                  Transferencia.Submit_ListaChequesTransferencia(PlistaCheque);
                  if (Ptituloapagar<>'')//se foi usado para pagar fornecedor nao exporta essa transferencia
                  Then Begin
                          if (Transferencia.Salvar(False,True,True,False,False,False,'')=False)
                          Then Begin
                                    MensagemErro('N�o foi poss�vel salvar a transfer�ncia do cheque devolvido');
                                    exit;
                          End;
                  End
                  Else Begin
                          if (Transferencia.Salvar(False,True,True,False,False,True,'')=False)
                          Then Begin
                                    MensagemErro('N�o foi poss�vel salvar a transfer�ncia do cheque devolvido');
                                    exit;
                          End;
                  End;

             End;

             //salvando a ocorrencia de cheque devolvido

             With Self.objChequeDevolvido do
             Begin
                  ZerarTabela;
                  Submit_CODIGO('0');
                  Cheque.Submit_CODIGO(QueryPesquisa.fieldbyname('codigo').asstring);
                  TituloaReceber.Submit_CODIGO(Ptituloareceber);
                  TituloaPagar.Submit_CODIGO(Ptituloapagar);
                  Transferencia.Submit_CODIGO(Ptransferencia);
                  Status:=dsInsert;
                  if (Salvar(False)=False)
                  Then Begin
                            MensagemErro('Erro na tentativa de Criar uma Ocorr�ncia de Cheque Devolvido');
                            exit;
                  End;
                  PcodigochequeDevolvido:=Get_CODIGO;
                  FDataModulo.IBTransaction.CommitRetaining;
                  ImprimeOcorrenciaChequeDevolvido(PcodigochequeDevolvido);
             End;
             BtConsultarClick(sender);
        End;
     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
            freeandnil(ObjQueryTemp);
            freeandnil(objgeratitulo);
            freeandnil(ObjGeraTransferencia);
            Freeandnil(PlistaCheque);
     End;
end;

procedure TFlancaChequeDevolvido.DbGridPesquisaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then BtGeraContasClick(sender);
end;

procedure TFlancaChequeDevolvido.edtportador_CPExit(Sender: TObject);
begin
     lbnomeportador_Cp.caption:='';

     if Tedit(Sender).Text=''
     Then exit;

     if (Self.ObjChequeDevolvido.Cheque.Portador.LocalizaCodigo(Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.ObjChequeDevolvido.Cheque.Portador.TabelaparaObjeto;
     lbnomeportador_Cp.Caption:=Self.ObjChequeDevolvido.Cheque.Portador.Get_Nome;

end;

procedure TFlancaChequeDevolvido.edtportador_CPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    objChequeDevolvido.Cheque.Portador.edtportadorKeyDown(sender,key,shift);
end;

procedure TFlancaChequeDevolvido.btConsulta_CPClick(Sender: TObject);
begin
     With QueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.add('Select * from TabChequesPortador ');
          sql.add('join tabtalaodecheques on tabchequesportador.codigo=tabtalaodecheques.codigochequeportador');
          sql.add('where TabChequesPortador.ChequedoPortador=''S'' and TabTalaodeCheques.Usado=''S'' ');


          if (edtportador_CP.Text<>'')
          Then Sql.add('and TabTalaodeCheques.Portador='+edtportador_CP.Text);

          if (EdtnumCheque_CP.Text<>'')
          Then Sql.add('and TabTalaodeCheques.numero='+EdtnumCheque_CP.Text);

          if (edtvalor_CP.Text<>'')
          Then Sql.add('and TabTalaodeCheques.valor='+virgulaparaponto(edtvalor_CP.text));

          open;
          formatadbgrid(DbGridPesquisa);
          dbgridpesquisa.setfocus;

     End;

end;

end.
