object FrlancaChequeDevolvido: TFrlancaChequeDevolvido
  Left = 0
  Top = 0
  Width = 622
  Height = 467
  Color = clMenu
  Ctl3D = False
  ParentColor = False
  ParentCtl3D = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 298
    Width = 622
    Height = 169
    Align = alBottom
    Caption = 'Panel1'
    TabOrder = 2
    object Shape3: TShape
      Left = 1
      Top = 1
      Width = 620
      Height = 167
      Align = alClient
      Brush.Color = clInfoBk
      Pen.Color = clNavy
    end
    object Label4: TLabel
      Left = 11
      Top = 6
      Width = 27
      Height = 13
      Caption = 'Comp'
      Transparent = True
    end
    object Label5: TLabel
      Left = 46
      Top = 6
      Width = 31
      Height = 13
      Caption = 'Banco'
      Transparent = True
    end
    object Label6: TLabel
      Left = 98
      Top = 6
      Width = 39
      Height = 13
      Caption = 'Ag'#234'ncia'
      Transparent = True
    end
    object Label7: TLabel
      Left = 149
      Top = 6
      Width = 15
      Height = 13
      Caption = 'DV'
      Transparent = True
    end
    object Label8: TLabel
      Left = 177
      Top = 6
      Width = 13
      Height = 13
      Caption = 'C1'
      Transparent = True
    end
    object Label9: TLabel
      Left = 212
      Top = 6
      Width = 28
      Height = 13
      Caption = 'Conta'
      Transparent = True
    end
    object Label10: TLabel
      Left = 320
      Top = 6
      Width = 13
      Height = 13
      Caption = 'C2'
      Transparent = True
    end
    object Label11: TLabel
      Left = 347
      Top = 6
      Width = 24
      Height = 13
      Caption = 'S'#233'rie'
      Transparent = True
    end
    object Label12: TLabel
      Left = 399
      Top = 6
      Width = 55
      Height = 13
      Caption = 'Cheque N.'#186
      Transparent = True
    end
    object Label3: TLabel
      Left = 482
      Top = 6
      Width = 13
      Height = 13
      Caption = 'C3'
      Transparent = True
    end
    object Label17: TLabel
      Left = 507
      Top = 6
      Width = 24
      Height = 13
      Caption = 'Valor'
      Transparent = True
    end
    object Label13: TLabel
      Left = 473
      Top = 117
      Width = 22
      Height = 13
      Caption = 'Para'
      Transparent = True
    end
    object Label16: TLabel
      Left = 192
      Top = 123
      Width = 84
      Height = 13
      Caption = 'C'#243'digo de Barras '
      Transparent = True
    end
    object Label18: TLabel
      Left = 192
      Top = 83
      Width = 83
      Height = 13
      Caption = 'Portador e Cpf 02'
      Transparent = True
    end
    object Label14: TLabel
      Left = 192
      Top = 59
      Width = 83
      Height = 13
      Caption = 'Portador e Cpf 01'
      Transparent = True
    end
    object DBComp: TDBEdit
      Left = 11
      Top = 22
      Width = 33
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 0
    end
    object Dbbanco: TDBEdit
      Left = 46
      Top = 22
      Width = 49
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 1
    end
    object DbAgencia: TDBEdit
      Left = 98
      Top = 22
      Width = 49
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 2
    end
    object DbDV: TDBEdit
      Left = 149
      Top = 22
      Width = 25
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 3
    end
    object DbC1: TDBEdit
      Left = 177
      Top = 22
      Width = 33
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 4
    end
    object DbConta: TDBEdit
      Left = 212
      Top = 22
      Width = 105
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 5
    end
    object DbC2: TDBEdit
      Left = 320
      Top = 22
      Width = 25
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 6
    end
    object DbSerie: TDBEdit
      Left = 347
      Top = 22
      Width = 49
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 7
    end
    object DbNumCheque: TDBEdit
      Left = 399
      Top = 22
      Width = 80
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 8
    end
    object DbC3: TDBEdit
      Left = 482
      Top = 22
      Width = 21
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 9
    end
    object DbValor: TDBEdit
      Left = 507
      Top = 22
      Width = 86
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 10
    end
    object DbCpfCliente1: TDBEdit
      Left = 466
      Top = 56
      Width = 127
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 11
    end
    object DbCpfCliente2: TDBEdit
      Left = 466
      Top = 80
      Width = 127
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 12
    end
    object DbVencimento: TDBEdit
      Left = 508
      Top = 113
      Width = 85
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 13
    end
    object dbcodigobarras: TDBEdit
      Left = 192
      Top = 137
      Width = 401
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 14
    end
    object DbCliente1: TDBEdit
      Left = 283
      Top = 56
      Width = 154
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 15
    end
    object DbCliente2: TDBEdit
      Left = 283
      Top = 80
      Width = 154
      Height = 19
      DataSource = DataSourcePesquisa
      TabOrder = 16
    end
  end
  object DbGridPesquisa: TDBGrid
    Left = 0
    Top = 135
    Width = 622
    Height = 163
    Align = alBottom
    DataSource = DataSourcePesquisa
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyPress = DbGridPesquisaKeyPress
  end
  object Guia: TPageControl
    Left = 0
    Top = 0
    Width = 622
    Height = 97
    ActivePage = TabSheet2
    Align = alTop
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '&1 - Cheque de 3'#186
      object Label19: TLabel
        Left = 4
        Top = 1
        Width = 74
        Height = 13
        Caption = 'Filtros de busca'
        Transparent = False
      end
      object Bevel2: TBevel
        Left = 1
        Top = 13
        Width = 504
        Height = 57
        Shape = bsFrame
        Style = bsRaised
      end
      object Label22: TLabel
        Left = 5
        Top = 23
        Width = 40
        Height = 13
        Caption = 'Portador'
        Transparent = True
      end
      object lbnomeportador: TLabel
        Left = 6
        Top = 56
        Width = 40
        Height = 13
        Caption = 'Portador'
        Transparent = True
      end
      object Label23: TLabel
        Left = 61
        Top = 23
        Width = 51
        Height = 13
        Caption = 'N'#186' cheque'
        Transparent = True
      end
      object Label24: TLabel
        Left = 151
        Top = 23
        Width = 24
        Height = 13
        Caption = 'Valor'
        Transparent = True
      end
      object Label25: TLabel
        Left = 265
        Top = 23
        Width = 31
        Height = 13
        Caption = 'Cpf 01'
        Transparent = True
      end
      object edtportador_filtro: TEdit
        Left = 6
        Top = 36
        Width = 51
        Height = 19
        MaxLength = 5
        TabOrder = 0
        OnExit = edtportador_filtroExit
        OnKeyDown = edtportador_filtroKeyDown
        OnKeyPress = edtportador_filtroKeyPress
      end
      object edtnumcheque_filtro: TEdit
        Left = 62
        Top = 36
        Width = 59
        Height = 19
        MaxLength = 20
        TabOrder = 1
      end
      object edtvalor_filtro: TEdit
        Left = 155
        Top = 36
        Width = 88
        Height = 19
        MaxLength = 12
        TabOrder = 2
        OnKeyPress = edtvalor_filtroKeyPress
      end
      object edtcpf_filtro: TEdit
        Left = 266
        Top = 36
        Width = 140
        Height = 19
        MaxLength = 20
        TabOrder = 3
      end
      object BtConsultar: TButton
        Left = 421
        Top = 17
        Width = 82
        Height = 53
        Hint = 'Ser'#225' gerada uma conta para  o cliente deste cheque'
        Caption = '&Consultar'
        TabOrder = 4
        OnClick = BtConsultarClick
      end
      object BtGeraContas: TButton
        Left = 505
        Top = 13
        Width = 90
        Height = 57
        Hint = 
          'O cheque ser'#225' enviado par ao portador cheques devolvidos e conta' +
          's ser'#227'o lan'#231'adas'
        Caption = '&Gera Contas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtGeraContasClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = '&2 - Cheque do Tal'#227'o (Portador)'
      ImageIndex = 1
      object Bevel1: TBevel
        Left = 1
        Top = 8
        Width = 504
        Height = 57
        Shape = bsFrame
        Style = bsRaised
      end
      object Label15: TLabel
        Left = 5
        Top = 15
        Width = 40
        Height = 13
        Caption = 'Portador'
        Transparent = True
      end
      object lbnomeportador_CP: TLabel
        Left = 6
        Top = 48
        Width = 40
        Height = 13
        Caption = 'Portador'
        Transparent = True
      end
      object Label20: TLabel
        Left = 61
        Top = 15
        Width = 51
        Height = 13
        Caption = 'N'#186' cheque'
        Transparent = True
      end
      object Label21: TLabel
        Left = 151
        Top = 15
        Width = 24
        Height = 13
        Caption = 'Valor'
        Transparent = True
      end
      object edtportador_CP: TEdit
        Left = 6
        Top = 28
        Width = 51
        Height = 19
        MaxLength = 5
        TabOrder = 0
        OnExit = edtportador_CPExit
        OnKeyDown = edtportador_CPKeyDown
        OnKeyPress = edtportador_filtroKeyPress
      end
      object EdtnumCheque_CP: TEdit
        Left = 62
        Top = 28
        Width = 59
        Height = 19
        MaxLength = 20
        TabOrder = 1
      end
      object edtvalor_CP: TEdit
        Left = 155
        Top = 28
        Width = 88
        Height = 19
        MaxLength = 12
        TabOrder = 2
        OnKeyPress = edtvalor_filtroKeyPress
      end
      object btConsulta_CP: TButton
        Left = 421
        Top = 10
        Width = 82
        Height = 53
        Hint = 'Ser'#225' gerada uma conta para  o cliente deste cheque'
        Caption = '&Consultar'
        TabOrder = 3
        OnClick = btConsulta_CPClick
      end
      object BtGeraConta_CP: TButton
        Left = 506
        Top = 8
        Width = 90
        Height = 57
        Hint = 
          'O cheque ser'#225' enviado par ao portador cheques devolvidos e conta' +
          's ser'#227'o lan'#231'adas'
        Caption = '&Gera Contas'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtGeraConta_CPClick
      end
    end
  end
  object QueryPesquisa: TIBQuery
    Left = 54
    Top = 92
  end
  object DataSourcePesquisa: TDataSource
    DataSet = QueryPesquisa
    Left = 22
    Top = 94
  end
end
