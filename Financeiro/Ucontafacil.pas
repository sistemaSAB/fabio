unit Ucontafacil;

interface

uses
  db,UessencialGlobal, UDataModulo,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,uobjgeratitulo,uobjlancamento, ExtCtrls, StdCtrls, Grids, DBGrids;

type
  TFcontaFacil = class(TForm)
    PanelSuperior: TPanel;
    edtpesquisa: TEdit;
    Label1: TLabel;
    btok: TButton;
    PanelINferior: TPanel;
    btnovomodelo: TButton;
    DBGrid: TDBGrid;
    panelbotoes: TPanel;
    btsalvarquitar: TButton;
    btsalvar: TButton;
    btcancelar: TButton;
    PanelValor: TPanel;
    lbvalor: TLabel;
    edtvalor: TEdit;
    PanelCredorDevedor: TPanel;
    lbcredordevedor: TLabel;
    edtcredordevedor: TEdit;
    panelcodigocredordevedor: TPanel;
    lbcodigocredordevedor: TLabel;
    edtcodigocredordevedor: TEdit;
    panelportador: TPanel;
    lbportador: TLabel;
    edtportador: TEdit;
    panelcontagerencial: TPanel;
    panelsubcontagerencial: TPanel;
    panelprazo: TPanel;
    edtprazo: TEdit;
    Lbprazo: TLabel;
    lbcontagerencial: TLabel;
    edtcontagerencial: TEdit;
    lbsubcontagerencial: TLabel;
    edtsubcontagerencial: TEdit;
    panelhistoricocontafacil: TPanel;
    Label2: TLabel;
    edthistoricocontafacil: TEdit;
    procedure FormShow(Sender: TObject);
    procedure edtpesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure btokClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnovomodeloClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btcancelarClick(Sender: TObject);
    procedure edtcredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtprazoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure btsalvarClick(Sender: TObject);
    procedure btsalvarquitarClick(Sender: TObject);
  private
    { Private declarations }
    PGeraTitulo:String;
    Function Salvar(ComCommit:boolean):boolean;
  public
    { Public declarations }
    ObjgeraTitulo:TObjGeraTitulo;
    objlancamento:TObjLancamento;

  end;

var
  FcontaFacil: TFcontaFacil;

implementation

uses UGeraTitulo, UObjTitulo;



{$R *.dfm}

procedure TFcontaFacil.FormShow(Sender: TObject);
var
ID:integer;
begin
    desab_botoes(self);
    desabilita_campos(self);
    Try
          id:=1;
          ObjgeraTitulo:=TObjGeraTitulo.create;
          id:=2;
          objlancamento:=TObjLancamento.create;

          Self.DBGrid.DataSource:=Self.ObjgeraTitulo.ObjDatasource;

          habilita_campos(self);
          habilita_botoes(Self);

          Self.ObjgeraTitulo.Pesquisa('HISTORICO','','');

          btcancelarclick(sender);

     Except
           on E:exception do
           Begin
                case id of
                  1:MensagemErro(E.message+#13+'Erro na cria��o do Objeto de Gera��o de T�tulo');
                  2:MensagemErro(E.message+#13+'Erro na cria��o do Objeto de Lan�amento');
                End;
                exit;
           End;
     End;
end;

procedure TFcontaFacil.edtpesquisaKeyPress(Sender: TObject;
  var Key: Char);
var
valor:string;
begin

     if (key=#27)
     Then exit;

     if (Key=#13)
     Then Begin
               btokclick(sender);
               exit;
     End;

     valor:=edtpesquisa.Text;

     if (key<>#8)
     Then valor:=valor+key
     Else valor:=copy(valor,1,length(valor)-1);

     //pesquisa incremental
     Self.ObjgeraTitulo.Pesquisa('HISTORICO',valor,'');
     formatadbgrid(Self.DBGrid);
end;




procedure TFcontaFacil.btokClick(Sender: TObject);
begin
     if (Self.ObjgeraTitulo.LocalizaCodigo(Self.DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     then exit;

     Self.ObjgeraTitulo.TabelaparaObjeto;

     if (MensagemPergunta(Self.ObjgeraTitulo.Get_Historico+' ? ')=mrno)
     Then exit;

     PGeraTitulo:=Self.objgeratitulo.get_codigo;


     With Self.ObjGeraTitulo do
     Begin

        limpaedit(PanelINferior);

        PanelINferior.Visible:=true;

        panelsubcontagerencial.Visible:=False;
        panelcontagerencial.Visible:=False;
        panelportador.Visible:=False;
        panelprazo.Visible:=False;
        panelcodigocredordevedor.Visible:=False;
        PanelCredorDevedor.Visible:=False;
        panelhistoricocontafacil.Visible:=False;

        edtCredorDevedor      .text:=Get_CredorDevedor               ;
        edtCodigoCredorDevedor.text:=Get_CodigoCredorDevedor         ;
        edtPrazo              .text:=Get_Prazo                       ;
        edtPortador           .text:=Get_Portador                    ;
        edtContaGerencial     .text:=Get_ContaGerencial              ;
        edtsubcontagerencial  .Text:=SubContaGerencial.Get_CODIGO    ;
        
        if (Self.ObjgeraTitulo.Get_HistoricoContaFacil='')
        Then begin
                panelhistoricocontafacil.Visible:=True;

        End;

        if (Self.ObjgeraTitulo.Get_CredorDevedor='')
        Then begin
                panelcredordevedor.Visible:=True;
        End;

        if (Self.ObjgeraTitulo.Get_codigoCredorDevedor='')
        Then begin
                panelcodigocredordevedor.Visible:=True; 
        End;

        if (Self.ObjgeraTitulo.Get_Prazo='')
        Then begin
                panelprazo.Visible:=True;
        End;

        if (Self.ObjgeraTitulo.Get_portador='')
        Then begin
                panelportador.Visible:=True;
        End;

        if (Self.ObjgeraTitulo.Get_contagerencial='')
        Then begin
                panelcontagerencial.Visible:=True;
        End;

        if (Self.ObjgeraTitulo.SubContaGerencial.Get_codigo='')
        and (Self.ObjgeraTitulo.Get_MostraSubConta_ContaFacil='S')
        Then begin
                panelsubcontagerencial.Visible:=True;
        End;

        PanelSuperior.Visible:=False;

        if (panelhistoricocontafacil.Visible=False)
        Then edtvalor.SetFocus
        Else edthistoricocontafacil.setfocus;
        
     End;
End;

procedure TFcontaFacil.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Try
          if (Self.ObjgeraTitulo<>nil)
          Then Self.ObjgeraTitulo.free;
     Except
     End;

     Try
        if (self.objlancamento<>nil)
        Then self.objlancamento.free;
     except
     End;
end;

procedure TFcontaFacil.btnovomodeloClick(Sender: TObject);
var
FGeraTituloX:TFGeraTitulo;
begin
     Try
        FGeraTituloX:=TFGeraTitulo.create(nil);
        FGeraTituloX.Showmodal;
        Self.ObjgeraTitulo.Pesquisa('HISTORICO','','');
     Finally
            Freeandnil(FGeraTituloX);
     End;
end;

procedure TFcontaFacil.FormKeyPress(Sender: TObject; var Key: Char);
begin
if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFcontaFacil.btcancelarClick(Sender: TObject);
begin
     PanelSuperior.Visible:=True;
     PanelINferior.Visible:=False;

     edtpesquisa.SetFocus;

end;

procedure TFcontaFacil.edtcredordevedorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
     Self.ObjgeraTitulo.edtcredordevedorKeyDown(sender,key,shift);
end;

procedure TFcontaFacil.edtcodigocredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjgeraTitulo.edtcodigocredordevedorKeyDown(edtcredordevedor.Text,sender,key,shift);
end;

procedure TFcontaFacil.edtprazoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Self.ObjgeraTitulo.edtprazoKeyDown(sender,key,shift);
end;

procedure TFcontaFacil.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Self.ObjgeraTitulo.edtportadorKeyDown(sender,key,shift);
end;

procedure TFcontaFacil.edtcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjgeraTitulo.edtcontagerencialKeyDown(sender,key,shift);
end;

procedure TFcontaFacil.edtsubcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjgeraTitulo.edtsubcontagerencialKeyDown(sender,key,shift,nil,edtcontagerencial.text);
end;

procedure TFcontaFacil.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
     validanumeros(tedit(sender),key,'float');
end;

procedure TFcontaFacil.btsalvarClick(Sender: TObject);
Begin
     if (Self.Salvar(true)=True)
     Then Begin
              mensagemAviso('T�tulo lan�ado com Sucesso. T�tulo n� '+Self.objlancamento.Pendencia.Titulo.Get_CODIGO);
              Self.btcancelarClick(sender);
     End;
End;

function TFcontaFacil.Salvar(ComCommit:boolean): boolean;
begin
     result:=False;
Try
    With Self.objlancamento.Pendencia.Titulo do
    Begin

         Submit_CODIGO         (Get_NovoCodigo);
         Submit_HISTORICO      (Self.ObjgeraTitulo.Get_Historico);

         Submit_Gerador            (Self.ObjgeraTitulo.GET_Gerador);
         Submit_CodigoGerador      (Self.ObjgeraTitulo.GET_CodigoGerador);
         Submit_CredorDevedor      (edtCredorDevedor      .text);
         Submit_CodigoCredorDevedor(edtCodigoCredorDevedor.text);
         Submit_Prazo              (edtPrazo              .text);
         Submit_Portador           (edtPortador           .text);
         Submit_ContaGerencial     (edtContaGerencial     .text);
         SubContaGerencial.Submit_CODIGO(edtsubcontagerencial.Text);


         Submit_VALOR          (tira_ponto(edtVALOR.text));
         Submit_EMISSAO        (datetostr(RetornaDataServidor));
         Submit_ParcelasIguais (true);
         Submit_NumDcto('');
         Submit_GeradoPeloSistema('N');
         Submit_NotaFiscal('');
         Submit_ExportaLanctoContaGer('N');
         Submit_Observacao('');

         status:=dsinsert;

         Try

            If (salvar(ComCommit)=False)
            Then Begin
                     FDataModulo.IBTransaction.RollbackRetaining;
                     MensagemErro('Erro na tentativa de salvar');
            End;

            result:=true;
         Finally

         End;
    End;
  Except
        on e:exception do
        Begin
             mensagemerro(e.message);
             exit;
        End;
  End;

end;

procedure TFcontaFacil.btsalvarquitarClick(Sender: TObject);
var
phistorico,ptitulo:String;
pcodigo:string;
begin
     Try
        if (Self.Salvar(False)=False)
        Then Begin
                  MensagemErro('Erro na tentativa de salvar o t�tulo');
                  exit;
        End;

        Ptitulo:=Self.objlancamento.Pendencia.Titulo.Get_CODIGO;

        if (Self.ObjLancamento.Pendencia.Get_QuantRegs(Ptitulo)>1)
        Then Begin
                  //em lote
        End
        Else Begin
                  if (Self.objlancamento.Pendencia.LocalizaporTitulo(ptitulo)=False)
                  Then Begin
                            MensagemErro('T�tulo n�o localizado');
                            exit;
                  End;
                  Self.objlancamento.Pendencia.TabelaparaObjeto;

                  if (Self.objlancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C')
                  Then Begin
                          Phistorico:='Recebimento '+Self.objlancamento.Pendencia.Titulo.get_historico;
                          pcodigo:='';

                          if (Self.ObjLancamento.NovoLancamento(Self.objlancamento.Pendencia.get_codigo,'R',Self.objlancamento.Pendencia.get_saldo,pcodigo,phistorico,False,true,true,'','',datetostr(now))=False)
                          Then Begin
                                    Messagedlg('Erro na tentativa de Salvar a Quita��o',mterror,[mbok],0);
                                    exit;
                          End;
                  End;

                  if (Self.objlancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
                  Then Begin
                          Phistorico:='Pagamento '+Self.objlancamento.Pendencia.Titulo.get_historico;
                          pcodigo:='';
                          if (Self.ObjLancamento.NovoLancamento(Self.objlancamento.Pendencia.get_codigo,'P',Self.objlancamento.Pendencia.get_saldo,pcodigo,phistorico,False,true,true,'','',datetostr(now))=False)
                          Then Begin
                                    Messagedlg('Erro na tentativa de Salvar a Quita��o',mterror,[mbok],0);
                                    exit;
                          End;
                  End;

                  FdataModulo.IBTransaction.CommitRetaining;
                  MensagemAviso('Quita��o conclu�da com sucesso. T�tulo N�'+ptitulo);
                  btcancelarClick(sender);

        End;



        

        
     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
     End;
end;

end.
