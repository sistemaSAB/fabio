unit UHistorico;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjhistorico;

type
  TFhistorico = class(TForm)
    Guia: TTabbedNotebook;
    EdtCodigo: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtdescricao: TEdit;
    combodebitocredito: TComboBox;
    Label3: TLabel;
    BtCancelar: TBitBtn;
    Btgravar: TBitBtn;
    btalterar: TBitBtn;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fhistorico: TFhistorico;
  Objhistorico:TObjhistorico;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFhistorico.ControlesParaObjeto: Boolean;
Begin
  Try
    With Objhistorico do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_descricao       ( edtdescricao.text);
         Submit_debitocredito   ( combodebitocredito.text[1]);

         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFhistorico.ObjetoParaControles: Boolean;
Begin
  Try
     With Objhistorico do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtdescricao.text       :=Get_descricao       ;

        If (get_debitocredito='D')
        Then combodebitocredito.itemindex:=0
        Else combodebitocredito.itemindex:=1;
        


        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFhistorico.TabelaParaControles: Boolean;
begin
     Objhistorico.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFhistorico.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Objhistorico=Nil)
     Then exit;

    If (Objhistorico.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Objhistorico.free;
end;

procedure TFhistorico.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFhistorico.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFhistorico.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;

     Objhistorico.status:=dsInsert;
     Guia.pageindex:=0;
     edtdescricao.setfocus;
end;

procedure TFhistorico.BtCancelarClick(Sender: TObject);
begin
     Objhistorico.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);


end;

procedure TFhistorico.BtgravarClick(Sender: TObject);
begin

     If Objhistorico.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Objhistorico.salvar=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFhistorico.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     Objhistorico.Status:=dsEdit;
     guia.pageindex:=0;
     edtdescricao.setfocus;
end;

procedure TFhistorico.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Objhistorico.Get_pesquisa,Objhistorico.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Objhistorico.status<>dsinactive
                                  then exit;

                                  If (Objhistorico.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFhistorico.btalterarClick(Sender: TObject);
begin
    If (Objhistorico.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFhistorico.btexcluirClick(Sender: TObject);
begin
     If (Objhistorico.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Objhistorico.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (Objhistorico.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFhistorico.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Objhistorico:=TObjhistorico.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

end.
