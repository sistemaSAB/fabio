object FAlteraPendencias_Titulo: TFAlteraPendencias_Titulo
  Left = 1193
  Top = 140
  Width = 501
  Height = 537
  Caption = 'Altera'#231#227'o de Pend'#234'ncias - EXCLAIM TECNOLOGIA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 3
    Top = 1
    Width = 479
    Height = 71
    Brush.Style = bsClear
  end
  object Shape2: TShape
    Left = 3
    Top = 357
    Width = 480
    Height = 141
    Brush.Style = bsClear
  end
  object Label2: TLabel
    Left = 4
    Top = 34
    Width = 57
    Height = 14
    Caption = 'Valor R$'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 5
    Top = 2
    Width = 78
    Height = 14
    Caption = 'N'#186' Parcelas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 4
    Top = 358
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Valor do T'#237'tulo'
    Color = 14803425
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label4: TLabel
    Left = 4
    Top = 375
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Quantidade de Pend'#234'ncias'
    Color = 13816530
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label5: TLabel
    Left = 4
    Top = 392
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Pend'#234'ncias quitadas'
    Color = 14803425
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label6: TLabel
    Left = 4
    Top = 409
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Valor Pend'#234'ncias Qiutadas'
    Color = 13816530
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label7: TLabel
    Left = 4
    Top = 426
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Pend'#234'ncias Quitadas parcialmente'
    Color = 14803425
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label8: TLabel
    Left = 4
    Top = 443
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Valor das Pend'#234'ncias Quitadas Parcialmente  '
    Color = 13816530
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label9: TLabel
    Left = 4
    Top = 461
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Pend'#234'ncias Abertas'
    Color = 14803425
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label10: TLabel
    Left = 4
    Top = 478
    Width = 476
    Height = 17
    AutoSize = False
    Caption = 'Valor Pend'#234'ncias Abertas '
    Color = 13816530
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object lbvalortitulo: TLabel
    Left = 289
    Top = 358
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbquantparcelas: TLabel
    Left = 289
    Top = 375
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbparcelasquitadas: TLabel
    Left = 289
    Top = 392
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbvalorparcelasquitadas: TLabel
    Left = 289
    Top = 409
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbparcelasquitadasparcialmente: TLabel
    Left = 289
    Top = 426
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbvalorparcelasquitadasparcialmente: TLabel
    Left = 289
    Top = 443
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbparcelasabertas: TLabel
    Left = 289
    Top = 461
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbvalorparcelasabertas: TLabel
    Left = 289
    Top = 478
    Width = 190
    Height = 17
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'R$ 2.000,00'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label19: TLabel
    Left = 230
    Top = 16
    Width = 246
    Height = 18
    Caption = 'Altera'#231#245'es de Valores e Datas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label20: TLabel
    Left = 227
    Top = 40
    Width = 253
    Height = 18
    Caption = 'de Parcelas ainda n'#227'o quitadas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LBVencimentos: TListBox
    Left = 2
    Top = 96
    Width = 121
    Height = 255
    ItemHeight = 13
    TabOrder = 6
    OnClick = LBVencimentosClick
    OnDblClick = LBVencimentosDblClick
    OnKeyPress = LBVencimentosKeyPress
  end
  object LBValores: TListBox
    Left = 127
    Top = 96
    Width = 130
    Height = 255
    ItemHeight = 13
    TabOrder = 7
    OnClick = LBValoresClick
    OnDblClick = LBValoresDblClick
    OnKeyDown = LBVencimentosKeyDown
  end
  object EdtValor: TEdit
    Left = 6
    Top = 49
    Width = 117
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    Text = 'EdtValor'
    OnKeyPress = EdtValorKeyPress
  end
  object GroupBox2: TGroupBox
    Left = 261
    Top = 77
    Width = 221
    Height = 72
    Caption = 'SOMA DAS PARCELAS'
    Color = clBtnText
    Font.Charset = ANSI_CHARSET
    Font.Color = 16744448
    Font.Height = -16
    Font.Name = 'BankGothic Lt BT'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    object labTotalparcelas: TLabel
      Left = 54
      Top = 27
      Width = 160
      Height = 31
      Alignment = taRightJustify
      AutoSize = False
      Caption = '99000,99'
      Color = clBlack
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'Arial'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
  end
  object btajustavalores: TButton
    Left = 127
    Top = 75
    Width = 130
    Height = 20
    Caption = 'Ajusta &Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = btajustavaloresClick
  end
  object btsair: TBitBtn
    Left = 261
    Top = 246
    Width = 221
    Height = 50
    Caption = '&Concluir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 8
    OnClick = btsairClick
  end
  object btajustadata: TBitBtn
    Left = 2
    Top = 75
    Width = 120
    Height = 20
    Caption = 'Ajusta &Data'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = btajustadataClick
  end
  object edtqtdeparcelas: TEdit
    Left = 6
    Top = 15
    Width = 117
    Height = 19
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    Text = 'EdtValor'
    OnKeyPress = edtqtdeparcelasKeyPress
  end
  object btcancelar: TBitBtn
    Left = 261
    Top = 298
    Width = 221
    Height = 50
    Caption = 'Canc&elar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
    OnClick = btcancelarClick
  end
  object LbCodigos: TListBox
    Left = 440
    Top = 152
    Width = 41
    Height = 89
    ItemHeight = 13
    TabOrder = 11
    Visible = False
    OnClick = LBValoresClick
    OnDblClick = LBValoresDblClick
    OnKeyDown = LBVencimentosKeyDown
  end
  object BtAlteradaQuantidades: TBitBtn
    Left = 124
    Top = 15
    Width = 65
    Height = 19
    Caption = 'Ajustar'
    TabOrder = 1
    OnClick = BitBtn1Click
  end
  object BtAlteraValor: TBitBtn
    Left = 123
    Top = 49
    Width = 65
    Height = 19
    Caption = 'Ajustar'
    TabOrder = 3
    OnClick = BitBtn2Click
  end
end
