unit UobjCHEQUEDEVOLVIDO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJCHEQUESPORTADOR
,UOBJTITULO
,UOBJTRANSFERENCIAPORTADOR;
//USES_INTERFACE





Type
   TObjCHEQUEDEVOLVIDO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               Cheque:TOBJCHEQUESPORTADOR;
               TituloaReceber:TOBJTITULO;
               TituloaPagar:TOBJTITULO;
               Transferencia:TOBJTRANSFERENCIAPORTADOR;
//CODIFICA VARIAVEIS PUBLICAS





                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaCheque(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                procedure EdtChequeExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtChequeKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure EdtTituloaReceberExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTituloaReceberKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtTituloaPagarExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTituloaPagarKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtTransferenciaExit(Sender: TObject);
                procedure EdtTransferenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                //CODIFICA DECLARA GETSESUBMITS
                Procedure ImprimeOcorrenciaChequeDevolvido(Pcodigo:string);

                Function  ExcluiTituloseTransferencia:Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UReltxtRDPRINT,rdprint, UObjValoresTransferenciaPortador,
  UobjExportaContabilidade;





Function  TObjCHEQUEDEVOLVIDO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('Cheque').asstring<>'')
        Then Begin
                 If (Self.Cheque.LocalizaCodigo(FieldByName('Cheque').asstring)=False)
                 Then Begin
                          Messagedlg('Cheque N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cheque.TabelaparaObjeto;
        End;
        If(FieldByName('TituloaReceber').asstring<>'')
        Then Begin
                 If (Self.TituloaReceber.LocalizaCodigo(FieldByName('TituloaReceber').asstring)=False)
                 Then Begin
                          Messagedlg('T�tulo a Receber N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TituloaReceber.TabelaparaObjeto;
        End;
        If(FieldByName('TituloaPagar').asstring<>'')
        Then Begin
                 If (Self.TituloaPagar.LocalizaCodigo(FieldByName('TituloaPagar').asstring)=False)
                 Then Begin
                          Messagedlg('T�tulo a Pagar N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TituloaPagar.TabelaparaObjeto;
        End;
        If(FieldByName('Transferencia').asstring<>'')
        Then Begin
                 If (Self.Transferencia.LocalizaCodigo(FieldByName('Transferencia').asstring)=False)
                 Then Begin
                          Messagedlg('Transfer�ncia N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Transferencia.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjCHEQUEDEVOLVIDO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Cheque').asstring:=Self.Cheque.GET_CODIGO;
        ParamByName('TituloaReceber').asstring:=Self.TituloaReceber.GET_CODIGO;
        ParamByName('TituloaPagar').asstring:=Self.TituloaPagar.GET_CODIGO;
        ParamByName('Transferencia').asstring:=Self.Transferencia.GET_CODIGO;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjCHEQUEDEVOLVIDO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 if (Self.Status=dsInsert)
 Then Begin
           if (ObjIntegracaoglobal<>nil)
           Then Begin
                   if (ObjIntegracaoglobal.LancaComissaoNegativa(Self.Codigo)=False)
                   Then Begin
                              if (ComCommit=True)
                              then FDataModulo.IBTransaction.RollbackRetaining;

                              exit;
                   End;
           End;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;


 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCHEQUEDEVOLVIDO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Cheque.ZerarTabela;
        TituloaReceber.ZerarTabela;
        TituloaPagar.ZerarTabela;
        Transferencia.ZerarTabela;
//CODIFICA ZERARTABELA





     End;
end;

Function TObjCHEQUEDEVOLVIDO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (Cheque.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Cheque';

      {If (TituloaReceber.Get_CODIGO='') and (Status=dsinsert)
      Then Begin
      Mensagem:=mensagem+'/T�tulo a Receber';}

      If (Transferencia.Get_CODIGO='')  and (Status=dsinsert)
      Then Mensagem:=mensagem+'/Transfer�ncia';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCHEQUEDEVOLVIDO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.Cheque.LocalizaCodigo(Self.Cheque.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Cheque n�o Encontrado!';

     if (Self.TituloaReceber.Get_CODIGO<>'')
     then Begin
               If (Self.TituloaReceber.LocalizaCodigo(Self.TituloaReceber.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ T�tulo a Receber n�o Encontrado!';
     End;

      if (Self.TituloaPagar.Get_CODIGO<>'')
      Then Begin
                If (Self.TituloaPagar.LocalizaCodigo(Self.TituloaPagar.Get_CODIGO)=False)
                Then Mensagem:=mensagem+'/ T�tulo a Pagar n�o Encontrado!';
      End;

      if (Self.Transferencia.Get_CODIGO<>'')
      Then Begin
              If (Self.Transferencia.LocalizaCodigo(Self.Transferencia.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'/ Transfer�ncia n�o Encontrado!';
      End;
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;
End;

function TObjCHEQUEDEVOLVIDO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Cheque.Get_Codigo<>'')
        Then Strtoint(Self.Cheque.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cheque';
     End;
     try
        If (Self.TituloaReceber.Get_Codigo<>'')
        Then Strtoint(Self.TituloaReceber.Get_Codigo);
     Except
           Mensagem:=mensagem+'/T�tulo a Receber';
     End;
     try
        If (Self.TituloaPagar.Get_Codigo<>'')
        Then Strtoint(Self.TituloaPagar.Get_Codigo);
     Except
           Mensagem:=mensagem+'/T�tulo a Pagar';
     End;
     try
        If (Self.Transferencia.Get_Codigo<>'')
        Then Strtoint(Self.Transferencia.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Transfer�ncia';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCHEQUEDEVOLVIDO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCHEQUEDEVOLVIDO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCHEQUEDEVOLVIDO.LocalizaCheque(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CHEQUEDEVOLVIDO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Cheque,TituloaReceber,TituloaPagar,Transferencia');
           SQL.ADD(' ');
           SQL.ADD(' from  TABCHEQUEDEVOLVIDO');
           SQL.ADD(' WHERE Cheque='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;
function TObjCHEQUEDEVOLVIDO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CHEQUEDEVOLVIDO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Cheque,TituloaReceber,TituloaPagar,Transferencia');
           SQL.ADD(' ');
           SQL.ADD(' from  TABCHEQUEDEVOLVIDO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCHEQUEDEVOLVIDO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCHEQUEDEVOLVIDO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        Self.ZerarTabela;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.TabelaparaObjeto;


                 if (ObjIntegracaoglobal<>nil)
                 Then Begin
                          if (ObjIntegracaoglobal.ExcluiporChequeDevolvido(Pcodigo)=False)
                          Then exit;
                 End;

                 
                 if (self.ExcluiTituloseTransferencia=False)
                 Then exit;

                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
        End
        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCHEQUEDEVOLVIDO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Cheque:=TOBJCHEQUESPORTADOR.create;
        Self.TituloaReceber:=TOBJTITULO.create;
        Self.TituloaPagar:=TOBJTITULO.create;
        Self.Transferencia:=TOBJTRANSFERENCIAPORTADOR.create;
//CODIFICA CRIACAO DE OBJETOS





        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCHEQUEDEVOLVIDO(CODIGO,Cheque,TituloaReceber');
                InsertSQL.add(' ,TituloaPagar,Transferencia)');
                InsertSQL.add('values (:CODIGO,:Cheque,:TituloaReceber,:TituloaPagar');
                InsertSQL.add(' ,:Transferencia)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCHEQUEDEVOLVIDO set CODIGO=:CODIGO,Cheque=:Cheque');
                ModifySQL.add(',TituloaReceber=:TituloaReceber,TituloaPagar=:TituloaPagar');
                ModifySQL.add(',Transferencia=:Transferencia');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCHEQUEDEVOLVIDO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCHEQUEDEVOLVIDO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCHEQUEDEVOLVIDO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewCHEQUEDEVOLVIDO');
     Result:=Self.ParametroPesquisa;
end;

function TObjCHEQUEDEVOLVIDO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CHEQUEDEVOLVIDO ';
end;


function TObjCHEQUEDEVOLVIDO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCHEQUEDEVOLVIDO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCHEQUEDEVOLVIDO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCHEQUEDEVOLVIDO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Cheque.FREE;
Self.TituloaReceber.FREE;
Self.TituloaPagar.FREE;
Self.Transferencia.FREE;
//CODIFICA DESTRUICAO DE OBJETOS





end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCHEQUEDEVOLVIDO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCHEQUEDEVOLVIDO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjChequeDevolvido.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjChequeDevolvido.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
//CODIFICA GETSESUBMITS


procedure TObjCHEQUEDEVOLVIDO.EdtChequeExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cheque.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cheque.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.Cheque.GET_NOME;
End;
procedure TObjCHEQUEDEVOLVIDO.EdtChequeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Cheque.Get_Pesquisa,Self.Cheque.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjCHEQUEDEVOLVIDO.EdtTituloaReceberExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TituloaReceber.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TituloaReceber.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.TituloaReceber.Get_HISTORICO;
End;
procedure TObjCHEQUEDEVOLVIDO.EdtTituloaReceberKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TituloaReceber.Get_Pesquisa,Self.TituloaReceber.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TituloaReceber.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.TituloaReceber.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TituloaReceber.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjCHEQUEDEVOLVIDO.EdtTituloaPagarExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TituloaPagar.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TituloaPagar.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.TituloaPagar.Get_HISTORICO;
End;
procedure TObjCHEQUEDEVOLVIDO.EdtTituloaPagarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TituloaPagar.Get_Pesquisa,Self.TituloaPagar.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TituloaPagar.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.TituloaPagar.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TituloaPagar.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjCHEQUEDEVOLVIDO.EdtTransferenciaExit(Sender: TObject);
Begin
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Transferencia.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     //LABELNOME.CAPTION:=Self.Transferencia.Get_Complemento;
End;
procedure TObjCHEQUEDEVOLVIDO.EdtTransferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Transferencia.Get_Pesquisa,Self.Transferencia.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN




procedure TObjCHEQUEDEVOLVIDO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCHEQUEDEVOLVIDO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TObjCHEQUEDEVOLVIDO.ImprimeOcorrenciaChequeDevolvido(
  Pcodigo: string);
begin
     if (pcodigo='')
     Then Begin
               MensagemAviso('Escolha uma ocorr�ncia');
               exit;
     End;

     if (Self.LocalizaCodigo(Pcodigo)=false)
     Then Begin
               MensagemAviso('Ocorr�ncia n�o encontrada');
               exit;
     End;
     Self.TabelaparaObjeto;

     With FreltxtRDPRINT do
     Begin
          ConfiguraImpressao;
          RDprint.Abrir;
          if (RDprint.Setup=False)
          Then Begin
                    RDprint.Fechar;
                    exit;
          End;

          LinhaLocal:=3;
          RDprint.ImpC(linhalocal,45,'DEVOLU��O DE CHEQUE - '+Self.CODIGO,[negrito]);
          IncrementaLinha(3);

          RDprint.Imp(linhalocal,1,'C�digo do Sistema: '+Self.Cheque.get_codigo     );
          IncrementaLinha(1);
          RDprint.Imp(linhalocal,1,'Banco            : '+Self.Cheque.get_numcheque  );
          IncrementaLinha(1);
          RDprint.Imp(linhalocal,1,'Ag�ncia          : '+Self.Cheque.get_valor      );
          IncrementaLinha(1);
          RDprint.Imp(linhalocal,1,'N�mero do Cheque : '+Self.Cheque.get_numcheque  );
          IncrementaLinha(1);
          RDprint.Imp(linhalocal,1,'Vencimento       : '+Self.Cheque.get_vencimento );
          IncrementaLinha(1);
          RDprint.Imp(linhalocal,1,'Valor do Cheque  : '+formata_valor(Self.Cheque.get_valor));
          IncrementaLinha(2);

          RDprint.Imp(linhalocal,1,'T�tulo a receber : '+Self.TituloaReceber.Get_CODIGO);
          IncrementaLinha(1);
          RDprint.Imp(linhalocal,1,'T�tulo a pagar   : '+Self.TituloaPagar.Get_CODIGO);
          IncrementaLinha(1);
          RDprint.Imp(linhalocal,1,'Transfer�ncia    : '+Self.Transferencia.Get_CODIGO);
          IncrementaLinha(1);
          
          DesenhaLinha;
          RDprint.Fechar;
     End;
end;

function TObjCHEQUEDEVOLVIDO.ExcluiTituloseTransferencia: Boolean;
var
ObjValoresTransferenciaPortador:TObjValoresTransferenciaPortador;
temp,ptituloareceber,ptituloapagar,ptransferencia,pcheque:string;

begin
    result:=False;

    Try
        ObjValoresTransferenciaPortador:=TObjValoresTransferenciaPortador.Create;
    Except
        MensagemErro('Erro na tentativa de Criar o Objeto de Valores Transfer�ncia de Portador');
        exit;
    End;

    Try
        Ptituloareceber:=Self.TituloaReceber.Get_CODIGO;
        ptituloapagar:=Self.TituloaPagar.Get_CODIGO;
        ptransferencia:=Self.Transferencia.Get_CODIGO;
        pcheque := Self.Cheque.Get_CODIGO;

        Self.Status:=dsedit;
        Self.TituloaReceber .ZerarTabela;
        Self.TituloaPagar   .ZerarTabela;
        Self.Transferencia  .ZerarTabela;
        
        if (Self.Salvar(false)=False)
        then Begin
                  MensagemErro('Erro na tentativa de gravar a ocorr�ncia sem as chaves estrangeiras');
                  exit;
        end;


        If(pcheque <> '')
        Then Begin
              self.Cheque.LocalizaCodigo(pcheque);
              self.Cheque.Status := dsedit;
              Self.Cheque.MotivoDevolucao.ZerarTabela;
              If(self.Cheque.Salvar(false,false,false,'')=false)
              Then Begin
                    MensagemErro('Erro na tentativa de estornar a devolu��o do cheque: '+pcheque );
                    Exit;
              End;

        End;

        if (ptituloareceber<>'')
        then begin
                  If (Self.Transferencia.CodigoLancamento.ExcluiTitulo(ptituloareceber,False)=False)
                  Then Begin
                          MensagemErro('Erro na tentativa de Excluir o T�tulo a Receber');
                          exit;
                  End;
        End;

        if (Ptituloapagar<>'')
        Then begin
                  If (Self.Transferencia.CodigoLancamento.ExcluiTitulo(pTituloaPagar,False)=False)
                  Then Begin
                          MensagemErro('Erro na tentativa de Excluir o T�tulo a Pagar');
                          exit;
                  End;
        End;

        If (ObjValoresTransferenciaPortador.ExcluiTransferencia(ptransferencia,False)=False)
        Then Begin
                  MensagemErro('Erro na tentativa de Excluir a Transfer�ncia');
                  exit;
        End;

        //Se tiver um registro na contabilidade excluir

        if (Self.TituloaReceber.ObjExportaContabilidade.LocalizaGerador('OBJCHEQUEDEVOLVIDO',sELF.CODIGO)=TRUE)
        Then begin
                  Self.TituloaReceber.ObjExportaContabilidade.TabelaparaObjeto;
                  if (Self.TituloaReceber.ObjExportaContabilidade.Get_Exportado='S')//ja foi exportado, apenas inverto os lancamentos
                  Then begin
                            temp:=Self.TituloaReceber.ObjExportaContabilidade.Get_ContaDebite;
                            Self.TituloaReceber.ObjExportaContabilidade.Status:=dsedit;
                            Self.TituloaReceber.ObjExportaContabilidade.Submit_ContaDebite(Self.TituloaReceber.ObjExportaContabilidade.Get_ContaCredite);
                            Self.TituloaReceber.ObjExportaContabilidade.Submit_ContaCredite(temp);
                            temp:=Self.TituloaReceber.ObjExportaContabilidade.Get_Historico;
                            Self.TituloaReceber.ObjExportaContabilidade.Submit_Historico('EXCLUS�O '+temp);

                            if (Self.TituloaReceber.ObjExportaContabilidade.salvar(False)=False)
                            then  Begin
                                       MensagemErro('Erro na tentativa de Lan�ar o Inverso na Contabilidade');
                                       exit;
                            End;
                  End
                  Else Begin
                            if (Self.TituloaReceber.ObjExportaContabilidade.exclui(Self.TituloaReceber.ObjExportaContabilidade.get_codigo,False)=False)
                            then begin
                                      MensagemErro('Erro na tentativa de Excluir a Contabilidade da Ocorr�ncia de  Cheque Devolvido de n�mero '+Self.codigo);
                                      exit;
                            End;
                  End;

        End;



        result:=true;
        
    Finally
           if (result=False)
           Then FDataModulo.IBTransaction.RollbackRetaining;

           ObjValoresTransferenciaPortador.Free;
    End;
end;

end.



