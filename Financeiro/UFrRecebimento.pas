unit UFrRecebimento;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db,UObjValores,Uobjchequesportador,
  UessencialGlobal, Grids, DBGrids;

type
  TFrRecimento = class(TFrame)
    Label19: TLabel;
    edtvalor: TEdit;
    btinserir: TBitBtn;
    btexcluir: TBitBtn;
    combotipo: TComboBox;
    Label23: TLabel;
    Label21: TLabel;
    edtportador: TEdit;
    lbportador: TLabel;
    PanelCheque: TPanel;
    Label3: TLabel;
    edtnumcheque: TEdit;
    edtc3: TEdit;
    Label12: TLabel;
    Label11: TLabel;
    edtserie: TEdit;
    Label10: TLabel;
    edtc2: TEdit;
    edtconta: TEdit;
    Label9: TLabel;
    edtc1: TEdit;
    Label8: TLabel;
    edtdv: TEdit;
    Label7: TLabel;
    edtagencia: TEdit;
    Label6: TLabel;
    Label5: TLabel;
    edtbanco: TEdit;
    edtcomp: TEdit;
    Label4: TLabel;
    edtCliente1: TEdit;
    Label14: TLabel;
    Label15: TLabel;
    edtcliente2: TEdit;
    edtcpfcliente2: TEdit;
    edtcpfcliente1: TEdit;
    Label16: TLabel;
    edtcodigodebarras: TEdit;
    edtvencimento: TMaskEdit;
    Label13: TLabel;
    edtcodigo: TEdit;
    BtAlternaCheque_Grid: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    LbTotalCheque: TLabel;
    LbtotalDinheiro: TLabel;
    DBGrid: TDBGrid;
    LbSaldo: TLabel;
    Label18: TLabel;
    Bevel8: TBevel;
    Bevel1: TBevel;
    lbcredito: TLabel;
    lbtotalcredito: TLabel;
    PanelValorCredito: TPanel;
    procedure btinserirClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure combotipoChange(Sender: TObject);
    procedure edtportadorExit(Sender: TObject);
    procedure BtAlternaCheque_GridClick(Sender: TObject);
    procedure edtcodigodebarrasExit(Sender: TObject);
    procedure combotipoExit(Sender: TObject);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure edtc3Exit(Sender: TObject);
    procedure edtportadorDblClick(Sender: TObject);

  private
         PLancaCodigoBarras:boolean;
         CredorDevedorAtual,CodigoCredorDevedorAtual:string;
         CodigoPortador:string;
         CodigoValores:string;
         Function  SalvaDadosCheque:boolean;
         Procedure ExcluiCheque(Cod:string);
         Procedure AtualizaGrid;
         Function AtualizaCredito(Pvalorusado:string):Currency;


    { Private declarations }
    Procedure limpacheques;
  public
    { Public declarations }
    //**Variaveis que serao preenchidas pelo FlancaNovoLancamento***
    PValorLancamento:Currency;
    PcodigoLancamento:string;
    PhistoricoLancamento:String;
    Function LancaTrocoComoCredito(var CodigoCredorDevedor: string; Var CredorDevedor: string;var portador:string;var CodValores:string):Boolean;
    //**************************************************************

    Procedure PassaObjeto(objeto:TobjValores;Pvalor_Lancamento:Currency;PcredorDevedorLote,PcodigoCredorDevedorLote:string);

  end;

var
  ObjValores:TObjValores;

  codigocheque:string[09];



implementation

uses Upesquisa, UObjLancamento, UDataModulo, UobjCmC7, UObjCredorDevedor,
  UObjTitulo, UobjCREDITOVALORES;



{$R *.DFM}



procedure TFrRecimento.btinserirClick(Sender: TObject);
var
TmpCodigoCredorDevedor,TmpCredorDevedor:string;
ValorSaldoCredito:Currency;
begin
     Try
        strtocurr(edtvalor.Text);
     Except
           mensagemerro('Valor Inv�lido');
           exit;
     End;

     if (combotipo.itemindex=-1)
     Then Begin
               Mensagemerro('Esp�cie Inv�lida');
               exit;
     End;


     With ObjValores do
     Begin
          if (Self.CodigoCredorDevedorAtual<>'')
          then ValorSaldoCredito:=Self.AtualizaCredito('')
          Else ValorSaldoCredito:=0;

          if (combotipo.ItemIndex=1)
          Then Begin
                    if (Self.SalvaDadosCheque=False)
                    Then exit;
          End
          Else Begin
                    codigocheque:='';

                    if (combotipo.itemindex=2)//credito
                    Then Begin
                            {  if (ValorSaldoCredito<strtocurr(edtvalor.Text))
                              Then Begin
                                        mensagemerro('O Cliente n�o possui saldo de Cr�dito suficiente');
                                        exit;
                              End;
                              }

                              // Novo Modulo // Fabio
                              if (ObjValores.RetornaSaldoCliente(CodigoCredorDevedorAtual)<strtocurr(edtvalor.Text))
                              Then Begin
                                        mensagemerro('O Cliente n�o possui saldo de Cr�dito suficiente');
                                        exit;
                              End;

                    End;
          End;

          ZerarTabela;
          Submit_CODIGO(Get_NovoCodigo);
          Submit_Historico(PhistoricoLancamento);
          Submit_Tipo(combotipo.Text[1]);
          Submit_Valor(edtvalor.Text);
          Submit_Lancamento(pcodigolancamento);
          Submit_Portador(edtportador.Text);
          CodigoPortador:=edtportador.Text;
          CodigoValores:=Get_CODIGO;
          Submit_Cheque(codigocheque);
          status:=dsinsert;
          If (salvar(True,true,PhistoricoLancamento)=False)
          Then Begin
               //se deu erro na hora de gravar na tabvalores
               //exclui o cheque lan�ado
               If (uppercase(combotipo.text[1])='C')//cheque
               Then ExcluiCheque(codigocheque);
               exit;
          End
          Else Begin
                    if (combotipo.itemindex=2)//credito
                    then Begin
                               //lan�ando o cr�dito na ObjCreditoValores
                               ObjCreditoValoresGlobal.ZerarTabela;
                               ObjCreditovaloresGlobal.Status:=dsinsert;
                               ObjCreditovaloresGlobal.Submit_CODIGO('0');
                               ObjCreditovaloresGlobal.credordevedor.Submit_CODIGO(CredorDevedorAtual);
                               ObjCreditovaloresGlobal.Submit_codigocredordevedor(CodigoCredorDevedorAtual);
                               ObjCreditovaloresGlobal.Submit_valorusado(ObjValores.Get_Valor);
                               ObjCreditovaloresGlobal.valores.Submit_CODIGO(ObjValores.Get_CODIGO);
                               if (ObjCreditovaloresGlobal.Salvar(True)=False)
                               then Begin
                                         MensagemErro('N�o foi poss�vel criar um registro de relacionamento entre o Valor Recebido e o Cr�dito do Credor/Devedor');

                                         If (ObjValores.exclui(ObjValores.Get_CODIGO,true)=False)
                                         Then Begin
                                                   Messagedlg('Erro Durante a Exclus�o do Valor Lan�ado!!',mterror,[mbok],0);
                                                   exit;
                                         End;
                               End;

                               // Feito por F�bio // Novo modulo de Credito
                               if (ObjValores.DiminuiCreditoComValores(ObjValores.Get_CODIGO,
                                                                                      CodigoCredorDevedorAtual,
                                                                                      ObjValores.Get_Valor,DateToStr(Now))=false)
                               then Begin
                                     FDataModulo.IBTransaction.RollbackRetaining;
                                     exit;
                               end
                               else
                                //FDataModulo.IBTransaction.CommitRetaining;
                                FDataModulo.IBTransaction.Commit;

                    End;

          End;
     End;
     Self.atualizaGrid;
     edtvalor.Text:='';
     edtportador.Text:='';
     lbportador.Caption:='';
     combotipo.ItemIndex:=0;
     edtvalor.SetFocus;
     limpaedit(PanelCheque);
     limpacheques;
     panelcheque.visible:=False;
     DBGrid.visible:=True;

end;


procedure TFrRecimento.btexcluirClick(Sender: TObject);
begin
     if (DBGrid.DataSource.DataSet.RecordCount=0)
     then exit;

     edtcodigo.Text:=DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring;


     If (ObjValores.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjValores.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     if (ObjValores.ExcluiCredito(edtcodigo.Text)=false) // Essa funcao excliu o regsitro
     then exit;

     If (ObjValores.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
     End;

     //limpaedit(Self);
     lbportador.caption:='';
     Self.AtualizaGrid;
end;




procedure TFrRecimento.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjValores.Get_PesquisaPortador,ObjValores.get_titulopesquisaPortador,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        edtportador.OnExit(sender);

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFrRecimento.combotipoChange(Sender: TObject);
var
pcliente,pcpf:string;
begin
     If Combotipo.itemindex=1
     Then Begin
                Try
                   strtoint(PcodigoLancamento);
                   edtcliente1.text:=ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.CredorDevedorAtual,Self.CodigoCredorDevedorAtual);
                   edtcpfcliente1.text:=ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CPFCNPJCredorDevedor(Self.CredorDevedorAtual,Self.CodigoCredorDevedorAtual);
                   edtvencimento.Text:=ObjValores.Lancamento.Get_Data;
                Except

                End;
                PanelCheque.Visible:=True;
                DbGrid.Visible:=False;
     End
     Else Begin
               PanelCheque.Visible:=False;
               DBGrid.visible:=True;
     End;


end;

function TFrRecimento.SalvaDadosCheque: boolean;
var
  ObjChequesPortador:tobjchequesportador;
begin
     Try
        ObjChequesPortador:=Tobjchequesportador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Controle de Cheques por Portadores',mterror,[mbok],0);
           result:=False;
           exit;
     End;

     Try
        ObjchequesPortador.status:=dsinsert;
        codigocheque:=ObjchequesPortador.Get_NovoCodigo;
        ObjchequesPortador.Submit_CODIGO(codigocheque);
        ObjchequesPortador.Submit_Portador(edtportador.text);
        ObjchequesPortador.Submit_Valor           ( edtvalor.text);
        ObjchequesPortador.Submit_Vencimento      (edtVencimento.text);
        ObjchequesPortador.Submit_Comp            (edtComp.text);
        ObjchequesPortador.Submit_Banco           (edtBanco.text);
        ObjchequesPortador.Submit_Agencia         (edtAgencia.text);
        ObjchequesPortador.Submit_C1              (edtC1.text);
        ObjchequesPortador.Submit_Conta           (edtConta.text);
        ObjchequesPortador.Submit_C2              (edtC2.text);
        ObjchequesPortador.Submit_Serie           (edtSerie.text);
        ObjchequesPortador.Submit_NumCheque       (edtNumCheque.text);
        ObjchequesPortador.Submit_C3              (edtC3.text);
        ObjchequesPortador.Submit_Cliente1        (edtCliente1.text);
        ObjchequesPortador.Submit_CPFCliente1     (edtCPFCliente1.text);
        ObjchequesPortador.Submit_Cliente2        (edtCliente2.text);
        ObjchequesPortador.Submit_CPFCliente2     (edtCPFCliente2.text);
        ObjchequesPortador.Submit_CodigodeBarras  (edtCodigodeBarras.text);

        If (ObjChequesPortador.SalvarChequeRecebido(False)=False)
        Then Begin
                  result:=False;
                  CodigoCheque:='';
                  exit;
             End
        Else result:=True;
     Finally
            ObjChequesPortador.free;
     End;


end;

procedure TFrRecimento.ExcluiCheque(Cod: string);
var
  ObjChequesPortador:tobjchequesportador;
begin
     Try
        ObjChequesPortador:=Tobjchequesportador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Controle de Cheques por Portadores',mterror,[mbok],0);
           exit;
     End;

     Try
     If (ObjChequesPortador.status<>dsinactive) or (cod='')
     Then exit;

     If (ObjChequesPortador.LocalizaCodigo(cod)=False)
     Then Begin
               Messagedlg('Cheque n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (ObjChequesPortador.Exclui(cod,false)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o do Cheque!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;

        
     Finally
            ObjChequesPortador.free;
     End;

end;


procedure TFrRecimento.edtportadorExit(Sender: TObject);
begin
     lbportador.caption:='';
     if (edtportador.text='')
     Then exit;

     If (ObjValores.Portador.LocalizaCodigo(edtportador.text)=False)
     Then Begin
               edtportador.text:='';
               exit;
     End;
     ObjValores.Portador.TabelaparaObjeto;
     lbportador.caption:=ObjValores.Portador.Get_Nome;


end;

procedure TFrRecimento.AtualizaGrid;
var
P1,P2,P3:string;
begin
    ObjValores.Pesquisa_Recebimentos_por_Lancamento(PcodigoLancamento,p1,p2,p3);
    LbTotalCheque.caption:=formata_valor(p2);
    lbtotaldinheiro.caption:=formata_valor(p1);
    Try
        lbtotalcredito.caption:=formata_valor(p3);
        if (P3='')
        Then p3:='0';
    Except
          lbtotalcredito.caption:='0,00';
          p3:='0';
    End;

    Lbsaldo.caption:=formata_valor(floattostr(PValorLancamento-(strtofloat(p1)+strtofloat(p2)+strtofloat(p3))));
    formatadbgrid(DBGrid);

    Self.AtualizaCredito(p3);

end;

procedure TFrRecimento.PassaObjeto(objeto:TobjValores;Pvalor_Lancamento:Currency;PcredorDevedorLote,PcodigoCredorDevedorLote:string);

begin
     ObjValores:=objeto;
     DBGrid.DataSource:=ObjValores.ObjDatasource;
     PValorLancamento:=Pvalor_Lancamento;
     Self.AtualizaGrid;
     Self.limpacheques;
     PLancaCodigoBarras:=False;
     if (ObjParametroGlobal.ValidaParametro('VALIDA CMC7 NO CHEQUE DE TERCEIRO?')=true)
     Then begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then PLancaCodigoBarras:=true;
     End;

     Self.CredorDevedorAtual:='';
     Self.codigoCredorDevedorAtual:='';

     combotipo.Items.clear;
     combotipo.Items.add('Dinheiro');
     combotipo.Items.add('Cheque');
     PanelValorCredito.Visible:=False;

     ObjValores.Lancamento.ZerarTabela;
     If (ObjValores.Lancamento.LocalizaCodigo(PcodigoLancamento)=True)
     Then Begin
               ObjValores.Lancamento.TabelaparaObjeto;
               if (ObjValores.Lancamento.Pendencia.Get_CODIGO<>'')
               Then begin//nao foi em lote
                         Self.CredorDevedorAtual:=ObjValores.Lancamento.Pendencia.titulo.credordevedor.get_codigo;
                         Self.CodigoCredorDevedorAtual:=ObjValores.Lancamento.Pendencia.titulo.get_codigocredordevedor;
                         
               End
               Else Begin//em lote
                         //Nao � possivel localizar via sql, porque primeiro � gravado o lancamento pai
                         //e s� posteriormente os lancamentos filhos, assim como esta sendo gravado o
                         //lancamento pai os filhos ainda nao existem, para isso
                         //o codigo do cliente ou fornecedor, deve vir parametro para
                         //a tela de lancanovolancamento atraves da tela de LANCAMENTOFR
                         Self.CredorDevedorAtuaL:=PcredorDevedorLote;
                         Self.CodigoCredorDevedorAtuaL:=PcodigoCredorDevedorLote;
               End;

               if (self.CodigoCredorDevedorAtual<>'')
               then Begin
                        combotipo.Items.add('R - Cr�dito');
                        PanelValorCredito.Visible:=true;
               End;
     End;
     //selecionando dinheiro
     combotipo.itemindex:=0;
     Self.AtualizaCredito('');

end;

procedure TFrRecimento.BtAlternaCheque_GridClick(Sender: TObject);
begin
     DBGrid.Visible:=not(DBGrid.Visible);
     PanelCheque.Visible:=not(PanelCheque.Visible);
     if (PanelCheque.Visible=true)
     Then edtcomp.Setfocus;
end;

procedure TFrRecimento.edtcodigodebarrasExit(Sender: TObject);
begin

     if (PLancaCodigoBarras=true)
     then Begin
               if (ObjValores.ObjCMC7.ValidaCmc7(edtcodigodebarras.Text)=False)
               Then Begin
                          edtcodigodebarras.text:='';
                          edtcomp.SetFocus;
               end
               Else Begin
                         edtagencia.text:=ObjValores.ObjCMC7.Get_Agencia;
                         edtnumcheque.text:=ObjValores.ObjCMC7.Get_numerocheque;
                         edtconta.text:=ObjValores.ObjCMC7.Get_ContaCorrente;
                         edtcomp.text:=ObjValores.ObjCMC7.Get_CamaraCompensacao;
                         edtbanco.Text:=ObjValores.ObjCMC7.Get_Banco;
                         btinserir.SetFocus;
               end;
     End
     Else btinserir.SetFocus;
end;


procedure TFrRecimento.combotipoExit(Sender: TObject);
begin
     if (PanelCheque.Visible=True)
     Then Begin
                if (PLancaCodigoBarras=False)
                Then edtcomp.SetFocus
                Else edtCliente1.SetFocus;
     end;
end;

procedure TFrRecimento.limpacheques;
begin
     edtcomp.text:='';
     edtbanco.text:='';
     edtagencia.text:='';
     edtdv.text:='';
     edtc1.text:='';
     edtconta.text:='';
     edtc2.text:='';
     edtserie.text:='';
     edtnumcheque.text:='';
     edtc3.text:='';
     edtCliente1.text:='';
     edtcpfcliente1.text:='';
     edtcliente2.text:='';
     edtcpfcliente2.text:='';
     edtvencimento.text:='';
     edtcodigodebarras.text:='';
end;

procedure TFrRecimento.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
     if not (key in ['0'..'9',',',#8])
     Then if key='.'
          Then key:=','
          Else key:=#0;
end;

procedure TFrRecimento.edtc3Exit(Sender: TObject);
begin
     if PLancaCodigoBarras=true
     then  btinserir.setfocus;
end;

Function TFrRecimento.AtualizaCredito(Pvalorusado:string):Currency;
var
TMPvalorcredito:string;
PvalorCredito:currency;
begin

     result:=0;
     
     if (Self.CodigoCredorDevedorAtual='')
     then exit;


     TMPvalorcredito:='';
     TMPvalorCredito:=ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CreditoCredorDevedor(Self.CredorDevedorAtual,Self.CodigoCredorDevedorAtual);

     Try
        if (TMPvalorcredito='')
        then PvalorCredito:=0
        Else PvalorCredito:=strtocurr(TMPvalorcredito);
     Except
           PvalorCredito:=0;
     End;

{     if (Pvalorusado<>'')
     Then Begin
               Try
                  strtofloat(Pvalorusado);
                  PvalorCredito:=PvalorCredito-strtofloat(Pvalorusado);
               Except

               End;
     End;
}     

     PanelValorCredito.caption:='Cr�dito dispon�vel do cliente '+formata_valor(pvalorcredito);
     result:=PvalorCredito;
end;

function TFrRecimento.LancaTrocoComoCredito(var CodigoCredorDevedor:string; Var CredorDevedor:string;var portador:string;var CodValores:string):Boolean;
begin
     CodigoCredorDevedor:=CodigoCredorDevedorAtual;
     CredorDevedor:= CredorDevedoratual;
     portador:=CodigoPortador;
     CodValores:=CodigoValores;
end;


procedure TFrRecimento.edtportadorDblClick(Sender: TObject);
var
  Shift : TShiftState;
  key : Word;
begin
  key := VK_F9;
  edtportadorKeyDown(Sender, Key, Shift);
end;

end.
