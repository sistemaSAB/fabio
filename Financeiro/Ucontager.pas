unit UCONTAGER;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjSubCONTAGERENCIAL,
  Grids, DBGrids;

type
  TFCONTAGER = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    PanelConta: TPanel;
    LbCODIGO: TLabel;
    LbNOME: TLabel;
    LbTIPO: TLabel;
    LbCODIGOPLANODECONTAS: TLabel;
    LbNomePlanodeContas: TLabel;
    Label2: TLabel;
    EdtCODIGO: TEdit;
    EdtNOME: TEdit;
    EdtCODIGOPLANODECONTAS: TEdit;
    ComboTipo: TComboBox;
    edtmascara: TMaskEdit;
    PanelSubConta: TPanel;
    Label5: TLabel;
    lbnomeplanodecontasSUBCONTA: TLabel;
    LbMascara: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtnomesubconta: TEdit;
    EdtCODIGOPLANODECONTASsubconta: TEdit;
    edtmascarasubconta: TMaskEdit;
    edtcodigosubconta: TEdit;
    DbGridSubConta: TDBGrid;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
    Shape1: TShape;
    BtGravarSubConta: TBitBtn;
    Btexcluirsubconta: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure EdtCODIGOPLANODECONTASKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtCODIGOPLANODECONTASExit(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure BtGravarSubContaClick(Sender: TObject);
    procedure DbGridSubContaKeyPress(Sender: TObject; var Key: Char);
    procedure EdtCODIGOPLANODECONTASsubcontaExit(Sender: TObject);
    procedure EdtCODIGOPLANODECONTASsubcontaKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure DbGridSubContaDblClick(Sender: TObject);
    procedure BtexcluirsubcontaClick(Sender: TObject);
  private
         ObjSubContagerencial:TObjSUBCONTAGERENCIAL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         Procedure ResgataSubContas;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCONTAGER: TFCONTAGER;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCONTAGER.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjSubContagerencial.ContaGerencial do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_NOME(edtNOME.text);
        Submit_TIPO(Submit_ComboBox(ComboTipo));
        Submit_CODIGOPLANODECONTAS(edtCODIGOPLANODECONTAS.text);
        Submit_Mascara(edtmascara.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCONTAGER.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjSubContagerencial.ContaGerencial do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;
        if (Get_TIPO='D')
        then ComboTipo.itemindex:=0
        else ComboTipo.itemindex:=1;
        EdtCODIGOPLANODECONTAS.text:=Get_CODIGOPLANODECONTAS;
        Self.EdtCODIGOPLANODECONTASExit(Self.EdtCODIGOPLANODECONTAS);
        edtmascara.text:=get_mascara;

        //Mauricio 11/07/2009
        Self.ResgataSubContas;
        habilita_campos(PanelSubConta);
        edtcodigosubconta.Enabled:=False;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCONTAGER.TabelaParaControles: Boolean;
begin
     If (Self.ObjSubContagerencial.ContaGerencial.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCONTAGER.FormClose(Sender: TObject; var Action: TCloseAction);
begin

     If (Self.ObjSubContagerencial=Nil)
     Then exit;

    If (Self.ObjSubContagerencial.ContaGerencial.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    Self.ObjSubContagerencial.free;
end;

procedure TFCONTAGER.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
           Perform(Wm_NextDlgCtl,0,0);
      end;
end;



procedure TFCONTAGER.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(PanelConta);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjSubContagerencial.ContaGerencial.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     Self.ObjSubContagerencial.ContaGerencial.status:=dsInsert;
     Edtmascara.setfocus;

end;


procedure TFCONTAGER.btalterarClick(Sender: TObject);
begin
    If (Self.ObjSubContagerencial.ContaGerencial.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                desabilita_campos(PanelSubConta);
                habilita_campos(PanelConta);
                EdtCodigo.enabled:=False;
                //ComboTipo.enabled:=False;
                Self.ObjSubContagerencial.ContaGerencial.Status:=dsEdit;
                edtmascara.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
          End;


end;

procedure TFCONTAGER.btgravarClick(Sender: TObject);
begin

     If Self.ObjSubContagerencial.ContaGerencial.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjSubContagerencial.ContaGerencial.Salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjSubContagerencial.ContaGerencial.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     ObjetoParaControles;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
     edtnomesubconta.SetFocus;

end;

procedure TFCONTAGER.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjSubContagerencial.ContaGerencial.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjSubContagerencial.ContaGerencial.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjSubContagerencial.ContaGerencial.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCONTAGER.btcancelarClick(Sender: TObject);
begin
     Self.ObjSubContagerencial.ContaGerencial.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCONTAGER.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCONTAGER.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjSubContagerencial.ContaGerencial.Get_pesquisa,Self.ObjSubContagerencial.ContaGerencial.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjSubContagerencial.ContaGerencial.status<>dsinactive
                                  then exit;

                                  If (Self.ObjSubContagerencial.ContaGerencial.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjSubContagerencial.ContaGerencial.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;
                                  edtnomesubconta.SetFocus;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCONTAGER.LimpaLabels;
begin
//LIMPA LABELS
     LbNomePlanodeContas.caption:='';
     lbnomeplanodecontasSUBCONTA.caption:='';

end;

procedure TFCONTAGER.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjSubContagerencial:=TObjSubContagerencial.create;
        Self.DbGridSubConta.DataSource:=Self.ObjSubContagerencial.ObjDatasourcepesquisa;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     FescolheImagemBotao.PegaFiguraBotaopequeno(BtGravarSubConta,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(Btexcluirsubconta,'BOTAORETIRAR.BMP');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;

     if (ObjParametroGlobal.ValidaParametro('MASCARA DA CLASSIFICACAO NO CADASTRO DE PLANO DE CONTAS')=False)
     Then edtmascara.EditMask:='999.999.999.999.999.999.999;1;_'
     Else edtmascara.EditMask:=ObjParametroGlobal.Get_Valor;

     edtmascarasubconta.editmask:=edtmascara.EditMask;

end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCONTAGER.btrelatoriosClick(Sender: TObject);
begin
     Self.ObjSubContagerencial.Imprime(edtcodigo.text);
end;

procedure TFCONTAGER.EdtCODIGOPLANODECONTASKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjSubContagerencial.ContaGerencial.EdtCODIGOPLANODECONTASKeyDown(sender,key,shift,LbNomePlanodeContas);
end;

procedure TFCONTAGER.EdtCODIGOPLANODECONTASExit(Sender: TObject);
begin
     Self.ObjSubContagerencial.ContaGerencial.EdtCODIGOPLANODECONTASExit(Sender,LbNomePlanodeContas);
end;

procedure TFCONTAGER.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (newtab=1)
     Then Begin
               if (EdtCODIGO.Text='')
               or (Self.ObjSubContagerencial.ContaGerencial.Status=dsinsert)
               Then begin
                        AllowChange:=False;
                        exit;
               End;
               lbnomeplanodecontasSUBCONTA.caption:='';
               limpaedit(PanelSubConta);
               habilita_campos(PanelSubConta);
               edtcodigosubconta.Enabled:=False;
               Self.ResgataSubContas;
     End;
end;

procedure TFCONTAGER.ResgataSubContas;
begin
    Self.ObjSubContagerencial.RetornaSubContas(edtcodigo.text);
    formatadbgrid(DbGridSubConta);
end;

procedure TFCONTAGER.BtGravarSubContaClick(Sender: TObject);
begin
     if (EdtCODIGO.Text='')
     then begin
               mensagemerro('Escolha uma contagerencial');
               exit;
     End
     else if edtnomesubconta.Text=''
     then begin
               mensagemerro('Digite o Nome da SubConta');
               exit;
     End;
     
     With Self.ObjSubContagerencial do
     begin
          ZerarTabela;
          if (edtcodigosubconta.Text='')
          then begin
                    Status:=dsInsert;
                    Submit_CODIGO('0');
          End
          Else Begin
                    Submit_CODIGO(edtcodigosubconta.text);
                    Status:=dsEdit;
          End;
          Submit_Nome(edtnomesubconta.text);
          submit_mascara(edtmascarasubconta.text);
          Submit_CODIGOPLANODECONTAS(EdtCODIGOPLANODECONTASsubconta.Text);
          ContaGerencial.Submit_CODIGO(EdtCODIGO.Text);//codigo da aba 1
          if (salvar(true)=True)
          Then Begin
                    lbnomeplanodecontasSUBCONTA.caption:='';
                    limpaedit(PanelSubConta);
                    edtnomesubconta.SetFocus;
                    Self.ResgataSubContas;
          End;
     End;
end;

procedure TFCONTAGER.DbGridSubContaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Begin
               Self.DbGridSubContaDblClick(sender);
     End;
end;

procedure TFCONTAGER.EdtCODIGOPLANODECONTASsubcontaExit(Sender: TObject);
begin
     Self.ObjSubContagerencial.EdtCODIGOPLANODECONTASExit(sender,lbnomeplanodecontasSUBCONTA);
end;

procedure TFCONTAGER.EdtCODIGOPLANODECONTASsubcontaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjSubContagerencial.EdtCODIGOPLANODECONTASKeyDown(sender,key,shift,lbnomeplanodecontasSUBCONTA);
end;

procedure TFCONTAGER.DbGridSubContaDblClick(Sender: TObject);
begin
     Self.ObjSubContagerencial.ZerarTabela;
     if (Self.ObjSubContagerencial.LocalizaCodigo(DbGridSubConta.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     then exit;
     Self.ObjSubContagerencial.TabelaparaObjeto;

     With Self.ObjSUBCONTAGERENCIAL do
     Begin
        edtcodigosubconta.text:=Get_CODIGO;
        edtnomesubconta.text:=Get_Nome;
        edtmascarasubconta.text:=Get_Mascara;
        EdtCODIGOPLANODECONTASsubconta.text:=Get_CODIGOPLANODECONTAS;
        Self.EdtCODIGOPLANODECONTASsubcontaExit(Self.EdtCODIGOPLANODECONTAS);
        edtmascarasubconta.SetFocus;
     End;

end;

procedure TFCONTAGER.BtexcluirsubcontaClick(Sender: TObject);
begin

    Self.ObjSubContagerencial.ZerarTabela;

    if (Self.ObjSubContagerencial.LocalizaCodigo(DbGridSubConta.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
    then exit;

    Self.ObjSubContagerencial.TabelaparaObjeto;

    if (MensagemPergunta('Certeza que deseja excluir a SubConta '+Self.ObjSubContagerencial.Get_Nome+' ?')=mrno)
    then exit;

    if (Self.ObjSubContagerencial.Exclui(Self.ObjSubContagerencial.Get_CODIGO,true)=False)
    then begin
              mensagemerro('N�o foi poss�vel excluir a Sub-Conta');
              exit;
    End;

    Self.ResgataSubContas;
end;

function TFCONTAGER.atualizaQuantidade: string;
begin
     Result:='Existem '+ContaRegistros('tabcontager','codigo')+' Contas Gerenciais Cadastradas';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjSubContagerencial.ContaGerencial.OBJETO.Get_Pesquisa,Self.ObjSubContagerencial.ContaGerencial.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjSubContagerencial.ContaGerencial.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjSubContagerencial.ContaGerencial.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjSubContagerencial.ContaGerencial.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
