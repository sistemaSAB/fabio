object FvalorLanctoLote_REC: TFvalorLanctoLote_REC
  Left = 275
  Top = 291
  Width = 491
  Height = 211
  Caption = 'VALOR A SER RECEBIDO'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 6
    Width = 70
    Height = 24
    Caption = 'VALOR'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BTOK: TBitBtn
    Left = 320
    Top = 8
    Width = 153
    Height = 38
    Caption = '&OK'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = BTOKClick
  end
  object BitBtn2: TBitBtn
    Left = 320
    Top = 48
    Width = 153
    Height = 38
    Caption = '&CANCELAR'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BitBtn2Click
  end
  inline FrValorLanctoLote: TFrValorLanctoLote
    Left = 0
    Top = 87
    Width = 476
    Height = 83
    TabOrder = 2
    inherited paneljuros: TPanel
      Left = 1
      Top = 4
      inherited UpDown: TUpDown
        OnChanging = FrValorLanctoLoteUpDownChanging
        OnChangingEx = FrValorLanctoLoteUpDownChangingEx
      end
      inherited edtcarencia: TMaskEdit
        OnExit = FrValorLanctoLoteedtcarenciaExit
        OnKeyPress = FrValorLanctoLoteedtcarenciaKeyPress
      end
      inherited edtdatalancamento: TMaskEdit
        OnExit = FrValorLanctoLoteedtdatalancamentoExit
      end
    end
  end
  object edtvalor: TMaskEdit
    Left = 8
    Top = 32
    Width = 294
    Height = 35
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Text = 'edtvalor'
    OnKeyPress = edtvalorKeyPress
  end
end
