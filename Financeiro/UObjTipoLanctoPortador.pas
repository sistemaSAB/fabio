unit UObjTipoLanctoPortador;
Interface
Uses Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,uobjplanodecontas;

Type
   TObjTipoLanctoPortador=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PlanodeContas                               :Tobjplanodecontas;


                Constructor Create;
                Destructor Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaHISTORICO(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Historico        :string;
                Function Get_Debito_Credito   :string;
                Function Get_LancaTitulo      :string;
                Function Get_ImprimeRecibo    :string;
                Procedure Submit_ImprimeRecibo    (parametro:string);

                Function Get_CodigoPlanodecontas:string;

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Historico        (parametro:string);
                Procedure Submit_Debito_Credito   (parametro:string);
                Procedure Submit_LancaTitulo      (parametro:string);
                Procedure Submit_CodigoPlanodeContas(parametro:string);
                Function  Get_NovoCodigo:string;



         Private
               ObjDataset:Tibdataset;

               CODIGO           :String[09];
               Historico        :String[60];
               Debito_Credito   :String[01];
               LancaTitulo      :String[01];
               ImprimeRecibo    :string;

               CodigoPLanodeContas:string;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
   End;


implementation
uses Classes,SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Procedure  TObjTipoLanctoPortador.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring          ;
        Self.Historico        :=FieldByName('Historico').asstring       ;
        Self.Debito_Credito   :=FieldByName('Debito_Credito').asstring  ;
        Self.CodigoPLanodeContas:=FieldByName('CodigoPLanodeContas').asstring  ;
        Self.LancaTitulo      :=FieldByName('LancaTitulo').asstring  ;
        Self.ImprimeRecibo    :=FieldByName('ImprimeRecibo').asstring  ;
     End;
end;


Procedure TObjTipoLanctoPortador.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring          :=Self.CODIGO           ;
      FieldByName('Historico').asstring       :=Self.Historico        ;
      FieldByName('Debito_Credito').asstring  :=Self.Debito_Credito   ;
      FieldByName('LancaTitulo').asstring     :=Self.LancaTitulo      ;
      FieldByName('CodigoPLanodeContas').asstring:=Self.CodigoPLanodeContas;
      FieldByName('ImprimeRecibo').asstring  :=Self.ImprimeRecibo;
  End;
End;

//***********************************************************************

function TObjTipoLanctoPortador.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




    if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;



 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;


 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjTipoLanctoPortador.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Historico        :='';
        Self.Debito_Credito   :='';
        Self.CodigoPLanodeContas:='';
        Self.LancaTitulo        :='';
        Self.ImprimeRecibo      :='';

     End;
end;

Function TObjTipoLanctoPortador.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Historico='')
       Then Mensagem:=mensagem+'/Hist�rico';

       If (Debito_Credito='')
       Then Mensagem:=Mensagem+'/D�bito ou Cr�dito';

       If (LancaTitulo='')
       Then Mensagem:=Mensagem+'/Lan�a T�tulo?';

       if (ImprimeRecibo='')
       Then Mensagem:=Mensagem+'/Imprime Recibo?';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjTipoLanctoPortador.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Curso n�o Encontrado!';}

     If (Self.CodigoPLanodeContas<>'')
     Then Begin
               If (Self.PlanodeContas.LocalizaCodigo(Self.codigoPlanodeContas)=False)
               Then Mensagem:=mensagem+'/ C�digo do Plano de Contas n�o Encontrado!';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjTipoLanctoPortador.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If (self.CodigoPLanodeContas<>'')
     Then Begin
               Try
                Inteiros:=strtoint(Self.codigoPlanodeContas);
               Except
                     Mensagem:=Mensagem+'/C�digo do Plano de Contas!';
               End;
     End;

     
     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjTipoLanctoPortador.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjTipoLanctoPortador.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try


     Mensagem:='';

     IF (Self.Debito_Credito<>'C') and (Self.Debito_Credito<>'D')
     Then Mensagem:=Mensagem+'Valor Inv�lido no Campo de Classifica��o de D�bito ou Cr�dito';

     IF (Self.LancaTitulo<>'S') and (Self.LancaTitulo<>'N')
     Then Mensagem:=Mensagem+'Valor Inv�lido no Campo de Lan�a T�tulo?';

     IF (Self.ImprimeRecibo<>'S') and (Self.ImprimeRecibo<>'N')
     Then Mensagem:=Mensagem+'Valor Inv�lido no Campo de Imprime Recibo?';


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;
function TObjTipoLanctoPortador.LocalizaHISTORICO(
  Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,debito_credito,codigoplanodecontas,lancatitulo,imprimerecibo from Tabtipolanctoportador where historico='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjTipoLanctoPortador.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,debito_credito,codigoplanodecontas,lancatitulo,imprimerecibo from Tabtipolanctoportador where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjTipoLanctoPortador.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjTipoLanctoPortador.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjTipoLanctoPortador.create;
begin   

        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.PlanodeContas:=Tobjplanodecontas.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,historico,debito_credito,codigoplanodecontas,lancatitulo,imprimerecibo from tabtipolanctoportador where codigo=-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' insert into tabtipolanctoportador (codigo,historico,debito_credito,codigoplanodecontas,lancatitulo,imprimerecibo) values (:codigo,:historico,:debito_credito,:codigoplanodecontas,:lancatitulo,:imprimerecibo)');

                ModifySQL.clear;
                ModifySQL.add(' UPdate Tabtipolanctoportador set codigo=:codigo,historico=:historico,debito_credito=:debito_credito,codigoplanodecontas=:codigoplanodecontas,lancatitulo=:lancatitulo,imprimerecibo=:imprimerecibo where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add('Delete from Tabtipolanctoportador where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,historico,debito_credito,codigoplanodecontas,lancatitulo,imprimerecibo from tabtipolanctoportador where codigo=-1 ');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjTipoLanctoPortador.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjTipoLanctoPortador.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjTipoLanctoPortador.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjTipoLanctoPortador.Get_Pesquisa: string;
begin
     Result:=' Select * from TabTipoLanctoPortador ';
end;

function TObjTipoLanctoPortador.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Tipo de Lan�amento de Portadores';
end;



function TObjTipoLanctoPortador.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_TIPOLANCTOPORTADOR';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o TabTipoLanctoPortador',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


function TObjTipoLanctoPortador.Get_Debito_Credito: string;
begin
     Result:=Self.Debito_Credito;
end;

function TObjTipoLanctoPortador.Get_Historico: string;
begin
     Result:=Self.Historico;
end;

procedure TObjTipoLanctoPortador.Submit_Debito_Credito(parametro: string);
begin
     Self.Debito_Credito:=parametro;
end;

procedure TObjTipoLanctoPortador.Submit_Historico(parametro: string);
begin
     Self.Historico:=parametro;
end;


destructor TObjTipoLanctoPortador.Free;
begin
    Freeandnil(Self.ObjDataset);
    Self.PLanodeContas.free;
end;

function TObjTipoLanctoPortador.Get_CodigoPlanodecontas: string;
begin
     Result:=Self.codigoplanodecontas;
end;

procedure TObjTipoLanctoPortador.Submit_CodigoPlanodeContas(
  parametro: string);
begin
     Self.codigoplanodecontas:=parametro;
end;

function TObjTipoLanctoPortador.Get_LancaTitulo: string;
begin
     Result:=Self.LancaTitulo;
end;

procedure TObjTipoLanctoPortador.Submit_LancaTitulo(parametro: string);
begin
     Self.LancaTitulo:=parametro;
end;

function TObjTipoLanctoPortador.Get_ImprimeRecibo: string;
begin
     Result:=Self.ImprimeRecibo;
end;

procedure TObjTipoLanctoPortador.Submit_ImprimeRecibo(parametro: string);
begin
     Self.ImprimeRecibo:=parametro;
end;

end.



