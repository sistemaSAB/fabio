unit UObjTalaodeCheques;
Interface
Uses windows,controls,classes,rdprint,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjPortador,UobjChequesPortador,Uobjlancamento;

Type
   TObjTalaodeCheques=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Portador                                    :TobjPortador;
                CodigoChequePortador                        :TobjChequesPortador;
                Lancamento                                  :TobjLancamento;


                Constructor Create;
                Destructor Free;

                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaChequePortador(Parametro:string) :boolean;
                Function    LocalizaChequeporPortador(ParNumcheque,ParPortador:string):boolean;
                Function    exclui(Pcodigo:string;ComCommit:Boolean):Boolean;
                Function    Get_Pesquisa                    :string;

                Function    Get_PesquisaAbertos             :string;overload;
                Function    Get_PesquisaAbertos(Pportador:string):string;overload;

                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaUsadosNoPortadorOriginal:string;
                function    Get_pesquisadescontados         :string;
                Function    Get_TituloPesquisaUsados        :string;


                Function    Get_PesquisaPortador          :string;
                Function    Get_TituloPesquisaPortador    :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO          :string;
                Function Get_Portador        :string;
                Function Get_AgenciaPortador :string;
                Function Get_ContaPortador   :string;
                Function Get_Numero          :string;
                Function Get_Valor           :string;
                Function Get_Vencimento      :string;
                Function Get_USado           :string;
                Function Get_CodigoChequePortador:string;
                Function Get_HistoricoPagamento:string;
                Function Get_NumTalaoCheque   :string;
                Function Get_Descontado          :string;
                Function Get_Transferencia:string;
                Procedure Submit_CODIGO          (parametro:string);
                Procedure Submit_Portador        (parametro:string);
                Procedure Submit_Numero          (parametro:string);
                Procedure Submit_NumeroFinal     (parametro:string);//no caso de lancar talao
                Procedure Submit_Valor           (parametro:string);
                Procedure Submit_Vencimento      (parametro:string);
                Procedure Submit_USado           (parametro:string);
                Procedure Submit_CodigoChequePortador(parametro:string);
                Procedure Submit_HistoricoPagamento(parametro:string);
                Procedure Submit_NumTalaoCheque   (parametro:string);
                procedure Submit_Descontado(parametro:string);
                procedure Submit_Transferencia(Parametro:string);

                Function Get_NovoCodigo:string;
                Function BaixaCheque(Parametro:string;ParametroData:string;ParametroPortador:string):Boolean;
                Procedure Imprime(PParametro: string);

                Function Get_NovoCodigoTalao: string;
                procedure ImprimeChequeRDPRINT(Pcodigo: string);
                Procedure Opcoes(Pcheque:string);
                Function ExtornaBaixa:boolean;
         Private
               ObjDataset:Tibdataset;

               CODIGO              :string;
               Numero              :string;
               Valor               :string;
               Vencimento          :string;
               Usado               :string;
               HistoricoPagamento  :string;
               Descontado          :string;
               Transferencia       :string;

               //**************************
               Numerofinal      :string;//no caso de lancar talao
               NumTalaoCheque   :string;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Function  ExisteCheque(ParametroPortador,ParametroCheque:string):boolean;
               Procedure VerificaLancaTalao;
               Procedure InutilizaCheque(Pcheque:string);
               procedure edtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
               procedure edtNumeroChequeDescontadoKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);


               {por o codigo do lancamento no talaodechques
               na hora de baixar cheques colocar esse codigo
               na tablanctoportador}
   End;


implementation
uses stdctrls,SysUtils,Dialogs,UDatamodulo,
UObjComprovanteCheque,UopcaoRel,UconfiguraChequeRDPRINT,
  UMenuRelatorios, UReltxtRDPRINT, UFiltraImp, UObjTransferenciaPortador,
  Upesquisa, UObjValoresTransferenciaPortador;


{ TTabTitulo }


Procedure  TObjTalaodeCheques.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO           :=FieldByName('CODIGO').asstring          ;
        Self.Numero           :=FieldByName('Numero').asstring          ;
        Self.Valor            :=FieldByName('Valor').asstring           ;
        Self.Vencimento       :=FieldByName('Vencimento').asstring      ;
        Self.USado            :=FieldByName('usado').asstring           ;
        Self.Descontado       :=FieldByName('Descontado').asstring      ;
        Self.HistoricoPagamento:=FieldByName('HistoricoPagamento').asstring           ;
        Self.NumTalaoCheque:=Fieldbyname('NumTalaoCheque').asstring;
        Self.Transferencia:=FieldByName('Transferencia').asstring;

        If (Self.Portador.LocalizaCodigo(FieldByName('Portador').asstring)=False)
        Then Begin
                  Messagedlg('Portador N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.Portador.TabelaparaObjeto;

        if (FieldByName('lancamento').asstring<>'')
        Then Begin
                  If (Self.Lancamento.LocalizaCodigo(FieldByName('lancamento').asstring)=False)
                  Then Begin
                            Messagedlg('Lan�amento N�o encontrado!',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                  End
                  Else Self.Lancamento.TabelaparaObjeto;
        End;

        If (FieldByName('CodigoChequePortador').asstring<>'')
        Then Begin
                  If (Self.CodigoChequePortador.LocalizaCodigo(FieldByName('CodigoChequePortador').asstring)=False)
                  Then Begin
                                Messagedlg('Codigo na Cheque Portador n�o encontrado!',mterror,[mbok],0);
                                Self.ZerarTabela;
                                exit;
                        End
                  Else Self.CodigoChequePortador.TabelaparaObjeto;
             End;



     End;
     
end;


Procedure TObjTalaodeCheques.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring          :=Self.CODIGO             ;
      FieldByName('Numero').asstring          :=Self.Numero             ;
      FieldByName('Valor').asstring           :=Self.Valor              ;
      FieldByName('Vencimento').asstring      :=Self.Vencimento         ;
      FieldByName('Portador').asstring        :=Self.Portador.Get_CODIGO;
      FieldByName('CodigoChequePortador').asstring:=Self.CodigoChequePortador.Get_CODIGO;
      FieldByName('USado').asstring           :=Self.usado;
      FieldByName('descontado').asstring           :=Self.descontado;
      FieldByName('Transferencia').asstring:=Self.Transferencia;
      FieldByName('historicopagamento').asstring           :=Self.HistoricoPagamento;
      Fieldbyname('NumTalaoCheque').asstring:=Self.NumTalaoCheque;
      Fieldbyname('lancamento').asstring:=Self.Lancamento.Get_CODIGO;
  End;
End;

//***********************************************************************

Function TObjTalaodeCheques.Salvar(ComCommit:Boolean): Boolean;//Ok
var
cont:Integer;
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;

   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;


 Self.VerificaLancaTalao;//ajeita o inicio e o fim caso tenha talao ou nao


 For cont:=Strtoint(Self.NUmero) to Strtoint(self.NUmerofinal) do
 Begin
     Self.numero:=Inttostr(cont);

     If Self.Status=dsinsert
     Then Begin
                If Self.ExisteCheque(Self.Portador.Get_codigo,Self.Numero)=True
                Then Begin
                         Messagedlg('O Cheque '+Self.Numero+' J� est� cadastrado neste Portador',mterror,[mbok],0);

                         If (ComCommit=True)
                         Then FDataModulo.IBTransaction.RollbackRetaining;

                         result:=False;
                         exit;
                     End;
          End;

if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

     Self.ObjetoParaTabela;
     Self.ObjDataset.Post;
     If (cont<Strtoint(Self.numerofinal))
     Then Self.CODIGO:=Self.Get_NovoCodigo;
 End;

 If (ComCommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjTalaodeCheques.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Numero           :='';
        Self.Numerofinal      :='';
        Self.Valor            :='';
        Self.Vencimento       :='';
        Self.USado             :='';
        Self.descontado:='';
        Self.HistoricoPagamento:='';
        Self.NumTalaoCheque:='';
        Self.Transferencia:='';
        Self.Portador.ZerarTabela;
        Self.CodigoChequePortador.ZerarTabela;
        Self.Lancamento.ZerarTabela;
     End;
end;

Function TObjTalaodeCheques.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Numero='')
       Then Mensagem:=mensagem+'/Numero';

       If (Valor='')
       Then Mensagem:=mensagem+'/Valor';

       If (Vencimento='')
       Then Mensagem:=mensagem+'/Vencimento';

       If (Portador.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Portador';

       If (USado='')
       Then Mensagem:=mensagem+'/Portador';

       if (Descontado='')
       Then Mensagem:=mensagem+'/Descontado?';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjTalaodeCheques.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador n�o Encontrado!'
     Else Begin
               Self.Portador.TabelaparaObjeto;
               IF (Self.Portador.Get_PermiteEmissaoCheque='N')
               Then Mensagem:=mensagem+'O Portador escolhido n�o permite cadastramento de cheque!';
     End;

     If Self.CodigoChequePortador.Get_CODIGO<>''
     Then Begin
               If (Self.CodigoChequePortador.LocalizaCodigo(Self.CodigoChequePortador.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Codigo de Cheque Portador n�o Encontrado!';
     End;

     If (Self.Lancamento.Get_CODIGO<>'')
     Then Begin
               If (Self.Lancamento.LocalizaCodigo(Self.Lancamento.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Lan�amento n�o Encontrado!';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjTalaodeCheques.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.numero);
     Except
           Mensagem:=mensagem+'/Numero do Cheque';
     End;

     try
        Reais:=Strtofloat(Self.valor);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;


     try
        Inteiros:=Strtoint(Self.portador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Portador';
     End;

     try
        if (self.Lancamento.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.Lancamento.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Lan�amento';
     End;

     try
        If (Self.CodigoChequePortador.get_codigo<>'')
        Then Inteiros:=Strtoint(Self.CodigoChequePortador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo de Cheque Portador';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjTalaodeCheques.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     Try
        Datas:=strtodate(Self.vencimento);
     Except
           Mensagem:=mensagem+'/Vencimento do Cheque';
     End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjTalaodeCheques.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try
     If (Self.usado<>'S') and (Self.usado<>'N')
     Then mensagem:=mensagem+'O Campo usado deve ser preenchido com S/N';

     If (Self.descontado<>'S') and (Self.descontado<>'N')
     Then mensagem:=mensagem+'O Campo Descontado deve ser preenchido com S/N';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjTalaodeCheques.LocalizaChequeporPortador(ParNumcheque, ParPortador: string): boolean;
begin
  with Self.ObjDataset do
  begin
    close;
    SelectSql.Clear;
    SelectSql.add('Select CODIGO, Numero, Valor, Vencimento, Portador, ');
    SelectSql.add('usado, CodigoChequePortador, HistoricoPagamento, NumTalaoCheque, ');
    SelectSql.add('descontado, transferencia, lancamento ');
    SelectSql.add('from TabtalaodeCheques ');
    SelectSql.add('where numero = ' + ParNumcheque +' and portador = ' + ParPortador);
    Open;

    if (recordcount > 0) then
      Result := True
    else Result := False;
  end;
end;

function TObjTalaodeCheques.LocalizaCodigo(parametro: string): boolean;//ok
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSql.Clear;
    SelectSql.add('Select CODIGO,Numero,Valor,Vencimento,Portador, ');
    SelectSql.add('usado,CodigoChequePortador,HistoricoPagamento, ');
    SelectSql.add('NumTalaoCheque,descontado,transferencia,lancamento ');
    SelectSql.add('from TabtalaodeCheques ');
    SelectSql.add('where codigo=' + parametro);
    Open;

    if (recordcount > 0) then
      Result:=True
    else Result:=False;
  End;
end;

function TObjTalaodeCheques.LocalizaChequePortador(Parametro: string): boolean;
begin
  with Self.ObjDataset do
  begin
    close;
    SelectSql.Clear;
    SelectSql.add('Select CODIGO, Numero, Valor, Vencimento, Portador, usado, ');
    SelectSql.add('CodigoChequePortador, HistoricoPagamento, NumTalaoCheque, descontado, ');
    SelectSql.add('transferencia, transferencia, lancamento ');
    SelectSql.add('from TabtalaodeCheques ');
    SelectSql.add('where CodigoChequePortador=' + parametro);
    Open;

    if (recordcount>0) then
      Result:=True
    else Result:=False;
  end;
end;


procedure TObjTalaodeCheques.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjTalaodeCheques.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.TabelaparaObjeto;

                 if ((Self.Usado='S') or (Self.CodigoChequePortador.Get_CODIGO<>''))
                 Then BEgin
                           MensagemErro ('N�o � poss�vel excluir um cheque que j� foi utilizado');
                           result:=False;
                           exit;
                 End;

                 Self.ObjDataset.delete;
                 If ComCommit=true
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjTalaodeCheques.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.Portador:=TObjPortador.Create;
        Self.CodigoChequePortador:=TObjChequesPortador.create;
        Self.Lancamento:=TobjLancamento.create;

        ZerarTabela;
        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Numero,Valor,Vencimento,Portador,usado,CodigoChequePortador,HistoricoPagamento,NumTalaoCheque,descontado,transferencia,lancamento from TabtalaodeCheques where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabtalaodeCheques (CODIGO,Numero,Valor,Vencimento,Portador,usado,CodigoChequePortador,HistoricoPagamento,NumTalaoCheque,descontado,transferencia,lancamento) values ');
                InsertSQL.add('(:CODIGO,:Numero,:Valor,:Vencimento,:Portador,:usado,:CodigoChequePortador,:HistoricoPagamento,:NumTalaoCheque,:descontado,:transferencia,:lancamento) ');

                ModifySQL.clear;
                ModifySQL.add(' Update TabTalaodeCheques set CODIGO=:CODIGO,Numero=:Numero,Valor=:Valor,Vencimento=:Vencimento,');
                ModifySQL.add(' Portador=:Portador,usado=:usado,CodigoChequePortador=:CodigoChequePortador,HistoricoPagamento=:HistoricoPagamento,NumTalaoCheque=:NumTalaoCheque,descontado=:descontado,transferencia=:transferencia,lancamento=:lancamento where codigo=:codigo ');


                DeleteSQL.clear;
                DeleteSQL.add(' Delete from TabTalaodeCheques where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Numero,Valor,Vencimento,Portador,usado,CodigoChequePortador,HistoricoPagamento,NumTalaoCheque,descontado,transferencia,lancamento from TabtalaodeCheques where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;
end;

//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjTalaodeCheques.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjTalaodeCheques.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjTalaodeCheques.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjTalaodeCheques.Get_Pesquisa: string;
begin
     Result:=' Select * from TabTalaodeCheques ';
end;

function TObjTalaodeCheques.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Cheques no Tal�o de Cheques ';
end;

function TObjTalaodeCheques.Get_Numero: string;
begin
     Result:=Self.Numero;
end;

function TObjTalaodeCheques.Get_PesquisaPortador: string;
begin
     Result:=Self.portador.Get_Pesquisa;
end;

function TObjTalaodeCheques.Get_Portador: string;
begin
     Result:=Self.portador.get_codigo;
end;

function TObjTalaodeCheques.Get_TituloPesquisaPortador: string;
begin
     Result:=Self.portador.Get_TituloPesquisa;
end;

function TObjTalaodeCheques.Get_Valor: string;
begin
     Result:=Self.valor;
end;

function TObjTalaodeCheques.Get_Vencimento: string;
begin
     Result:=Self.Vencimento;
end;

procedure TObjTalaodeCheques.Submit_Numero(parametro: string);
begin
     Self.numero:=parametro;
end;

procedure TObjTalaodeCheques.Submit_Portador(parametro: string);
begin
     Self.Portador.Submit_CODIGO(parametro);
end;

procedure TObjTalaodeCheques.Submit_Valor(parametro: string);
begin
     Self.valor:=parametro;
end;

procedure TObjTalaodeCheques.Submit_Vencimento(parametro: string);
begin
     Self.vencimento:=parametro;
end;

function TObjTalaodeCheques.ExisteCheque(ParametroPortador,
  ParametroCheque: string): boolean;
begin
     With ObjDataset do
     Begin
          close;
          selectsql.clear;
          selectsql.add(' Select count(codigo) as CNT from TabtalaodeCheques where Portador='+ParametroPortador+' and Numero='+ParametroCheque);
          open;
          If (Fieldbyname('CNT').asinteger>0)
          Then result:=True
          else result:=False;

          close;
          SelectSql.clear;
          SelectSql.add('Select CODIGO,Numero,Valor,Vencimento,Portador,usado,codigochequeportador,historicopagamento,numtalaocheque,descontado,lancamento,transferencia from TabtalaodeCheques where codigo=0');
          open;
     End;
End;

function TObjTalaodeCheques.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_CODIGOTALAODECHEQUES' ;

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Cheque do Tal�o de Cheques',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;

end;

function TObjTalaodeCheques.Get_NovoCodigoTalao: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_NUMTALAOCHEQUE' ;

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Tal�o de Cheques',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;
end;


procedure TObjTalaodeCheques.Submit_NumeroFinal(parametro: string);
begin
     Self.Numerofinal:=parametro;
end;

Procedure TObjTalaodeCheques.VerificaLancaTalao;
var
Inteiro:Integer;
begin

     try
        if (Self.NumeroFinal='')
        Then Self.NumeroFinal:=Self.numero;
         
        Inteiro:=Strtoint(Self.Numerofinal);
     except
           Self.NumeroFinal:=Self.numero;
     End;

     If (Inteiro<=Strtoint(Self.numero)) or (Self.Status=dsedit)
     Then Self.NumeroFinal:=Self.Numero;
end;

function TObjTalaodeCheques.Get_AgenciaPortador: string;
begin
     Result:=Self.Portador.Get_Agencia;
end;

function TObjTalaodeCheques.Get_ContaPortador: string;
begin
     Result:=Self.Portador.Get_NumeroConta;
end;

function TObjTalaodeCheques.Get_USado: string;
begin
     Result:=Self.usado;
end;

procedure TObjTalaodeCheques.Submit_USado(parametro: string);
begin
     Self.usado:=parametro;
end;

function TObjTalaodeCheques.Get_CodigoChequePortador: string;
begin
     Result:=Self.CodigoChequePortador.get_codigo;
end;

procedure TObjTalaodeCheques.Submit_CodigoChequePortador(parametro: string);
begin
     Self.CodigoChequePortador.Submit_CODIGO(parametro);
end;

function TObjTalaodeCheques.Get_PesquisaUsadosNoPortadorOriginal: string;
var
apoio:String[254];
begin
     apoio:='';
     apoio:='select tabtalaodecheques.* from tabtalaodecheques left';
     apoio:=apoio+' join tabchequesportador on tabtalaodecheques.codigochequeportador=tabchequesportador.codigo';
     apoio:=apoio+' where tabchequesportador.portador=tabtalaodecheques.portador';
     apoio:=apoio+' and usado=''S''';
     result:=apoio;

end;


function TObjTalaodeCheques.Get_pesquisadescontados: string;
var
apoio:String[254];
begin
     apoio:='';
     apoio:='select tabtalaodecheques.* from tabtalaodecheques';
     apoio:=apoio+' where usado=''S'' and descontado=''S'' and transferencia is not null';
     result:=apoio;

end;


function TObjTalaodeCheques.Get_TituloPesquisaUsados: string;
begin
     Result:='PESQUISA DE CHEQUES USADOS';
end;

function TObjTalaodeCheques.BaixaCheque(Parametro: string;ParametroData:string;ParametroPortador:string): Boolean;
var
Pcodigo:string;
plancamento,PContaTransicaochequeportador,PcodigoTransferencia:string;
begin
     if parametro=''
     Then Begin
                result:=false;
                exit;
     End;

     try

         If (Self.LocalizaChequeporPortador(parametro,ParametroPortador)=False)
         Then Begin
                   Messagedlg('Cheque N�o Localizado nos Tal�es de Cheques!',mterror,[mbok],0);
                   result:=False;
                   exit;
         End;
    
         Self.TabelaparaObjeto;
         Pcodigo:=Self.CODIGO;
    
         If (Messagedlg('Valor do Cheque '+formata_valor(Self.Valor)+' tem certeza?',mtconfirmation,[mbyes,mbno],0)=Mrno)
         Then exit;
    

         PcodigoTransferencia:='';
         If (Self.CodigoChequePortador.baixacheque(Self.CodigoChequePortador.Get_CODIGO,Self.portador.get_codigo,Parametrodata,Self.Numero,Self.historicopagamento,Self.Lancamento.Get_codigo,PcodigoTransferencia)=False)
         Then Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Opera��o N�o Conclu�da !',mterror,[mbok],0) ;
                    exit;
         End;
    
         //passando o cheque para descontado='S'
         if (Self.LocalizaCodigo(Pcodigo)=False)
         Then begin
                  FdataMOdulo.IBTransaction.RollbackRetaining;
                  Messagedlg('Cheque n�o localizado na tabela tal�o de cheques!',mtinformation,[mbok],0);
                  exit;
         End;
    
         Self.TabelaparaObjeto;
         Self.Submit_Descontado('S');

         Self.status:=dsedit;
         //guardar o numero da transferencia para poder extornar
         Self.Submit_Transferencia(PcodigoTransferencia);         
         if (Self.Salvar(False)=False)
         Then Begin
                   FdataMOdulo.IBTransaction.RollbackRetaining;
                   Messagedlg('Erro na tentativa de passar o cheque para descontado="SIM"',mterror,[mbok],0);
                   exit;
         End;

         //*********************************************************************
         //esse � o lan�amento que deu origem ao cheque do tal�o no pagamento da conta
         Plancamento:=Self.Lancamento.Get_CODIGO;
         //*********************************************************************

         

         //lancando a contabilidade em caso de uso de conta de transicao
         PContaTransicaochequeportador:='';
    
         if (ObjParametroGlobal.ValidaParametro('CONTA DE TRANSICAO NO CHEQUE DO PORTADOR')=true)
         Then Begin
                   //A conta de Transicao � usada seguinte maneira
                   //No momento que emito o cheque � feito um lan�amento
                   //Debitando o Fornecedor e Creditando "Cheques a Compensar" que � a conta de transicao
                   //No momento que o cheque � compensado preciso Debitar "cheques a compensar" e creditar o caixa

                   PContaTransicaochequeportador:=ObjParametroGlobal.get_valor;
                   if (ObjLanctoPortadorGlobal.Portador.PlanodeContas.LocalizaCodigo(PContaTransicaochequeportador)=true)
                   Then Begin
                             //tenho que debitar conta transicao e creditar o banco
                             Self.Lancamento.ObjExportaContabilidade.ZerarTabela;
                             Self.Lancamento.ObjExportaContabilidade.Status:=dsinsert;
                             Self.Lancamento.ObjExportaContabilidade.Submit_CODIGO('0');
                             Self.Lancamento.ObjExportaContabilidade.Submit_Data(parametrodata);
                             Self.Lancamento.ObjExportaContabilidade.Submit_ContaDebite(PContaTransicaochequeportador);
                             Self.Lancamento.ObjExportaContabilidade.Submit_ContaCredite(self.Portador.Get_CodigoPlanodeContas);
                             Self.Lancamento.ObjExportaContabilidade.Submit_Valor(Self.Get_Valor);
                             Self.Lancamento.ObjExportaContabilidade.Submit_Historico(Self.HistoricoPagamento);
                             Self.Lancamento.ObjExportaContabilidade.Submit_ObjGerador('OBJTALAODECHEQUES');
                             Self.Lancamento.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                             Self.Lancamento.ObjExportaContabilidade.Submit_Exportado('N');
                             Self.Lancamento.ObjExportaContabilidade.Submit_Exporta('S');
                             Self.Lancamento.ObjExportaContabilidade.Submit_NomedoArquivo('');
                             Self.Lancamento.ObjExportaContabilidade.Submit_NumDocto(Self.Numero);
                             if (Self.Lancamento.ObjExportaContabilidade.Salvar(True)=False)
                             Then begin
                                       MensagemErro('Erro na tentativa de lan�ar a contabilidade com conta de transi��o da baixa do cheque');
                                       exit; 
                             End;
                   End;
         End
         Else Begin
                  //como nao foi usado conta de transicao (cheques a compensar)
                  //tenho a opcao de no momento da quitacao nao ter exportado nada
                  //e agora quando o cheque for compensado
                  //Debitar os Fornecedores que receberam esse cheque
                  //e creditar o banco
                  //nesse caso vai ter  q ser partida simples
                  
                  if (ObjParametroGlobal.ValidaParametro('LAN�A CONTABILIDADE COM FORNECEDOR APENAS NA COMPENSA��O DE CHEQUES?')=true)
                  Then Begin
                            if (ObjParametroGlobal.get_valor='SIM')
                            then Begin
                                      {PContaTransicaochequeportador:=ObjParametroGlobal.get_valor;
                                      if (ObjLanctoPortadorGlobal.Portador.PlanodeContas.LocalizaCodigo(PContaTransicaochequeportador)=true)
                                      Then Begin
                                                //tenho que debitar conta transicao e creditar o banco
                                                Self.Lancamento.ObjExportaContabilidade.ZerarTabela;
                                                Self.Lancamento.ObjExportaContabilidade.Status:=dsinsert;
                                                Self.Lancamento.ObjExportaContabilidade.Submit_CODIGO('0');
                                                Self.Lancamento.ObjExportaContabilidade.Submit_Data(parametrodata);
                                                Self.Lancamento.ObjExportaContabilidade.Submit_ContaDebite(PContaTransicaochequeportador);
                                                Self.Lancamento.ObjExportaContabilidade.Submit_ContaCredite(self.Portador.Get_CodigoPlanodeContas);
                                                Self.Lancamento.ObjExportaContabilidade.Submit_Valor(Self.Get_Valor);
                                                Self.Lancamento.ObjExportaContabilidade.Submit_Historico(Self.HistoricoPagamento);
                                                Self.Lancamento.ObjExportaContabilidade.Submit_ObjGerador('OBJTALAODECHEQUES');
                                                Self.Lancamento.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                                                Self.Lancamento.ObjExportaContabilidade.Submit_Exportado('N');
                                                Self.Lancamento.ObjExportaContabilidade.Submit_Exporta('S');
                                                Self.Lancamento.ObjExportaContabilidade.Submit_NomedoArquivo('');
                                                Self.Lancamento.ObjExportaContabilidade.Submit_NumDocto(Self.Numero);
                                                if (Self.Lancamento.ObjExportaContabilidade.Salvar(True)=False)
                                                Then begin
                                                          MensagemErro('Erro na tentativa de lan�ar a contabilidade com conta de transi��o da baixa do cheque');
                                                          exit;
                                                End;
                                      End;}
                            End;
                  End;
         End;

         FdataMOdulo.IBTransaction.CommitRetaining;
         Messagedlg('Opera��o Conclu�da!',mtInformation,[mbOK],0);
     Finally
            FdataMOdulo.IBTransaction.RollbackRetaining;
     End;
End;

function TObjTalaodeCheques.Get_PesquisaAbertos(Pportador: string): string;
var
temp:String;
begin
     Temp:=' Select * from ViewTalaodeCheques where usado=''N''';      //Rodolfo

     if (Pportador<>'')
     Then temp:=temp+' and portador='+Pportador;

     Result:=temp;
end;

function TObjTalaodeCheques.Get_PesquisaAbertos: string;
begin
    Result:=' Select * from ViewTalaodeCheques where usado = ''N'' ';  {Rodolfo}
end;

destructor TObjTalaodeCheques.Free;
begin
     Freeandnil(Self.ObjDataset);
     Self.Portador.free;
     Self.CodigoChequePortador.free;
     Self.Lancamento.Free;
end;

function TObjTalaodeCheques.Get_HistoricoPagamento: string;
begin
     Result:=Self.HistoricoPagamento;
end;

procedure TObjTalaodeCheques.Submit_HistoricoPagamento(parametro: string);
begin
     Self.HistoricoPagamento:=Parametro;
end;

procedure TObjTalaodeCheques.Imprime(PParametro: string);
begin
     With FMenuRelatorios do
     Begin
          NomeObjeto:='UOBJTALAODECHEQUES';
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Impress�o de Cheques');//0
          End;
          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0:Self.ImprimeChequeRDPRINT(PParametro);
          End;

     end;
End;
procedure TObjTalaodeCheques.ImprimeChequeRDPRINT(Pcodigo: string);
var
valorcheque,ValorExtensoCheque,extenso1,extenso2:String;
Tmpano,TmpMes,TmpDia:word;
ObjComprovanteCheque:tobjComprovantecheque;

Begin
     If (Self.localizacodigo(Pcodigo)=False)
     Then Begin
               Messagedlg('Cheque n�o localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     Try
        ObjComprovanteCheque:=tobjComprovantecheque.create;
     Except
           Messagedlg('Erro na cria��o do Objeto de Comprovante de Cheque!',mterror,[mbok],0);
           exit;
     End;

     Try
       if (Self.CodigoChequePortador.get_codigo='')
       then Begin
                 MensagemErro('Cheque n�o encontrado no portador. Possivelmente esse cheque ainda n�o foi emitido');
                 exit;
       end;


       If (ObjComprovanteCheque.LocalizaCheque(Self.CodigoChequePortador.get_codigo)=False)
       Then Begin
                 Messagedlg('O Comprovante n�o foi localizado!',mterror,[mbok],0);
                 exit;
       End;
       ObjComprovanteCheque.TabelaparaObjeto;

       With FConfiguraChequeRDPRINT do
       Begin
            //informando qual o portador
            PortadorTemp:=strtoint(Self.Portador.get_codigo);

            FreltxtRDPRINT.ConfiguraImpressao;
            If (FConfiguraChequeRDPRINT.CarregaConfiguracoes(FreltxtRDPRINT.rdprint)=False)
            Then Exit;
            FreltxtRDPRINT.ImprimeCabecalho:=False;

            FreltxtRDPRINT.rdprint.CaptionSetup:='IMPRESS�O DE CHEQUE';
            FreltxtRDPRINT.rdprint.abrir;
            if (FreltxtRDPRINT.rdprint.Setup=False)
            then Begin
                      FreltxtRDPRINT.rdprint.fechar;
                      exit;
            End;

            if (USaFonte=false)
            Then Begin

                      FreltxtRDPRINT.rdprint.Imp(strtoint(floattostr(int(LBpagoA.top/5))),strtoint(floattostr(int(LBpagoA.left/5))),completapalavra(ObjComprovanteCheque.Get_PagoA,LBpagoA.tag,' '));
                      FreltxtRDPRINT.rdprint.Imp(strtoint(floattostr(int(LBDATAATUAL.top/5))),strtoint(floattostr(int(LBDATAATUAL.left/5))),completapalavra(ObjComprovanteCheque.Get_Data,LBdataatual.tag,' '));
                      //************************************************************************************
                      valorcheque:='';
                      valorcheque:='R$ '+formatfloat('###,##0.00',strtofloat(Self.Valor))+' ';
                      
                      ValorExtensoCheque:='';
                      extenso1:='';
                      extenso2:='';
                      ValorExtensoCheque:=valorextenso(strtofloat(Self.Valor));
                      DividirValor(ValorExtensoCheque,LBextenso1.tag,200,extenso1,extenso2);
                      extenso1:=extenso1+' ';
                      extenso2:=extenso2+' ';


                      // Alterado por F�bio
                      // Quando o valor precisa escrever as duas linha o EXTENSO1 ficava com XXX na frente
                      if (extenso2 <> ' ')
                      then FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBextenso1.top/5))),strtoint(floattostr(int(LBextenso1.left/5))),completapalavra(uppercase(extenso1),LBextenso1.tag,' '))
                      else FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBextenso1.top/5))),strtoint(floattostr(int(LBextenso1.left/5))),completapalavra(uppercase(extenso1),LBextenso1.tag,'X'));

                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBextenso2.top/5))),strtoint(floattostr(int(LBextenso2.left/5))),completapalavra(uppercase(extenso2),LBextenso2.tag,'X.'));
                      //************************************************************************************
                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBvalorcanhoto.top/5))),strtoint(floattostr(int(LBvalorcanhoto.left/5))),completapalavra(valorcheque,LBvalorcanhoto.tag,'X.'));
                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBnominal.top/5))),strtoint(floattostr(int(LBnominal.left/5))),completapalavra(ObjComprovanteCheque.Get_Nominador,LBnominal.tag,' '));
                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBvalor.top/5))),strtoint(floattostr(int(LBvalor.left/5))),completapalavra('XXX '+valorcheque,LBvalor.tag,'X'));
                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBcidadecheque.top/5))),strtoint(floattostr(int(LBcidadecheque.left/5))),completapalavra(ObjComprovanteCheque.Get_Cidade,LBcidadecheque.tag,' '));
                      DecodeDate(strtodate(ObjComprovanteCheque.Get_data),Tmpano,TmpMes,TmpDia);
                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBdiacheque.top/5))),strtoint(floattostr(int(LBdiacheque.left/5))),completapalavra(Inttostr(TmpDia),LBdiacheque.tag,' '));
                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBmescheque.top/5))),strtoint(floattostr(int(LBmescheque.left/5))),completapalavra(MesExtenso(TmpMes),LBmescheque.tag,' '));
                      FreltxtRDPRINT.rdprint.imp(strtoint(floattostr(int(LBanocheque.top/5))),strtoint(floattostr(int(LBanocheque.left/5))),completapalavra(FormatDateTime('yyyy',strtodate(ObjComprovanteCheque.Get_data)),LBanocheque.tag,' '));
            End
            Else Begin

                      FreltxtRDPRINT.rdprint.Impf(strtoint(floattostr(int(LBpagoA.top/5))),strtoint(floattostr(int(LBpagoA.left/5))),completapalavra(ObjComprovanteCheque.Get_PagoA,LBpagoA.tag,' '),[TipoFonte]);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBDATAATUAL.top/5))),strtoint(floattostr(int(LBDATAATUAL.left/5))),completapalavra(ObjComprovanteCheque.Get_Data,LBdataatual.tag,' '),[TipoFonte]);
                      //************************************************************************************
                      valorcheque:='';
                      valorcheque:='R$ '+formatfloat('###,##0.00',strtofloat(Self.Valor))+' ';

                      ValorExtensoCheque:='';
                      extenso1:='';
                      extenso2:='';
                      ValorExtensoCheque:=valorextenso(strtofloat(Self.Valor));
                      DividirValor(ValorExtensoCheque,LBextenso1.tag,200,extenso1,extenso2);
                      extenso1:=extenso1+' ';
                      extenso2:=extenso2+' ';
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBextenso1.top/5))),strtoint(floattostr(int(LBextenso1.left/5))),completapalavra(uppercase(extenso1),LBextenso1.tag,'X.'),[TipoFonte]);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBextenso2.top/5))),strtoint(floattostr(int(LBextenso2.left/5))),completapalavra(uppercase(extenso2),LBextenso2.tag,'X.'),[TipoFonte]);
                      //************************************************************************************
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBvalorcanhoto.top/5))),strtoint(floattostr(int(LBvalorcanhoto.left/5))),completapalavra(valorcheque,LBvalorcanhoto.tag,'X.'),[TipoFonte]);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBnominal.top/5))),strtoint(floattostr(int(LBnominal.left/5))),completapalavra(ObjComprovanteCheque.Get_Nominador,LBnominal.tag,' '),[TipoFonte]);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBvalor.top/5))),strtoint(floattostr(int(LBvalor.left/5))),completapalavra('XXX '+valorcheque,LBvalor.tag,'X'),[TipoFonte]);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBcidadecheque.top/5))),strtoint(floattostr(int(LBcidadecheque.left/5))),completapalavra(ObjComprovanteCheque.Get_Cidade,LBcidadecheque.tag,' '),[TipoFonte]);
                      DecodeDate(strtodate(ObjComprovanteCheque.Get_data),Tmpano,TmpMes,TmpDia);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBdiacheque.top/5))),strtoint(floattostr(int(LBdiacheque.left/5))),completapalavra(Inttostr(TmpDia),LBdiacheque.tag,' '),[TipoFonte]);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBmescheque.top/5))),strtoint(floattostr(int(LBmescheque.left/5))),completapalavra(MesExtenso(TmpMes),LBmescheque.tag,' '),[TipoFonte]);
                      FreltxtRDPRINT.rdprint.impf(strtoint(floattostr(int(LBanocheque.top/5))),strtoint(floattostr(int(LBanocheque.left/5))),completapalavra(FormatDateTime('yy',strtodate(ObjComprovanteCheque.Get_data)),LBanocheque.tag,' '),[TipoFonte]);

            End;

            FreltxtRDPRINT.rdprint.fechar;
       End;
     Finally
            ObjComprovanteCheque.free;
     End;

end;




function TObjTalaodeCheques.Get_NumTalaoCheque: string;
begin
     Result:=Self.NumTalaoCheque;
end;

procedure TObjTalaodeCheques.Submit_NumTalaoCheque(parametro: string);
begin
     Self.NumTalaoCheque:=Parametro;
end;

procedure TObjTalaodeCheques.Opcoes(Pcheque:string);
begin
     With FOpcaorel do
     Begin
        RgOpcoes.Items.clear;
        RgOpcoes.Items.add('Inutilizar Cheque');
        ShowModal;
        if (tag=0)
        Then exit;

        case RgOpcoes.ItemIndex of
           0:Self.InutilizaCheque(Pcheque);
        End;
     End;
end;

procedure TObjTalaodeCheques.InutilizaCheque(Pcheque: string);
begin
     if (ObjPermissoesUsoGlobal.ValidaPermissao('INUTILIZAR CHEQUE')=False)
     Then exit;


     if (Pcheque='')
     Then Begin
               Messagedlg('Escolha um Cheque',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.LocalizaCodigo(pcheque)=False)
     Then Begin
               Messagedlg('Cheque n�o encontrado!',mtinformation,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;
     Self.status:=dsedit;

     if (Self.Usado='S')
     Then Begin
               Messagedlg('Este Cheque j� foi utilizado!',mtinformation,[mbok],0);
               exit;
     End;
     Self.Usado:='S';
     Self.HistoricoPagamento:='CHEQUE INUTILIZADO';

     If (Self.Salvar(true)=False)
     Then Begin
               Messagedlg('N�o foi poss�vel inutilizar o Cheque',mterror,[mbok],0);
               exit;
     End;
     Messagedlg('Cheque Inutilizado!',mtinformation,[mbok],0);
     exit;
end;

function TObjTalaodeCheques.Get_Descontado: string;
begin
     Result:=Self.descontado;
end;

procedure TObjTalaodeCheques.Submit_Descontado(parametro: string);
begin
     Self.descontado:=parametro;
end;


function TObjTalaodeCheques.Get_Transferencia: string;
begin
     Result:=Self.Transferencia;
end;

procedure TObjTalaodeCheques.Submit_Transferencia(Parametro: string);
begin
     Self.Transferencia:=Parametro;
end;

Function TObjTalaodeCheques.ExtornaBaixa:boolean;
var
ObjValorestransferencia:TObjValoresTransferenciaPortador;
ptransferencia:string;
begin
     result:=true;
     Self.ZerarTabela;

     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Portador do Tal�o';
          edtgrupo01.OnKeyDown:=Self.edtportadorkeydown;

          Grupo02.Enabled:=true;
          LbGrupo02.caption:='N� do Cheque';
          edtgrupo02.OnKeyDown:=Self.edtNumeroChequeDescontadoKeyDown;

          showmodal;

          if (tag=0)
          Then begin
                    result:=False;
                    exit;
          End;

          if ((edtgrupo01.text='') or (edtgrupo02.Text=''))
          then begin
                    mensagemerro('� necess�rio escolher o portador e digitar o n�mero do cheque que deseja estornar');
                    exit;
          End;


          if (self.LocalizaChequeporPortador(edtgrupo02.text,edtgrupo01.text)=False)
          then Begin
                    mensagemerro('Cheques n�o encontrado');
                    exit;
          End;

          self.TabelaparaObjeto;

          if (MensagemPergunta('Valor do Cheque '+formata_valor(self.Valor)+' confirma?')=mrno)
          then exit;
     End;

     if (Self.Descontado<>'S')
     then Begin
               mensagemErro('O Cheque n�o foi descontado, por isso n�o � poss�vel baix�-lo!');
               exit;
     End;

     //verificando se tenho o campo transferencia preenchido
     if (Self.Transferencia='')
     then Begin
               MensagemErro('O cheque foi baixado anteriormente a atualiza��o que permite o estorno de baixa de cheque. Solicite ao suporte do sistema a baixa do mesmo');
               exit;

     End;

     //o extorno consiste em retorna-lo para o portador origem da transferencia de baixa
     //passar o descontado=N, apagar o campo transferencia

    Try
       ObjValorestransferencia:=TObjValoresTransferenciaPortador.create;
    Except
          mensagemerro('Erro na tentativa de Criar o Objeto de Transfer�ncia');
          exit;
    End;

    Try
       if (ObjValorestransferencia.TransferenciaPortador.LocalizaCodigo(Self.Transferencia)=False)
       Then Begin
                 mensagemerro('Transfer�ncia n�o localizada');
                 exit;
       End;
       ObjValorestransferencia.TransferenciaPortador.TabelaparaObjeto;

       if (self.CodigoChequePortador.Portador.get_codigo<>ObjValorestransferencia.TransferenciaPortador.PortadorDestino.get_codigo)
       Then Begin
                 mensagemerro('O cheque n�o se encontra no portador da baixa, pode ter sido devolvido ou alterado manualmente');
                 exit; 
       End;

       Ptransferencia:=Self.Transferencia;
       Self.Transferencia:='';
       Self.Descontado:='N';
       Self.Status:=dsedit;

       if (self.salvar(false)=False)
       then Begin
                 mensagemerro('N�o foi poss�vel para passar o cheque para descontao=N');
                 exit;
       End;

       if (ObjValorestransferencia.ExcluiTransferencia(ptransferencia,false,true)=False)
       Then Begin
                 mensagemerro('Erro na tentativa de excluir a transfer�ncia de baixa');
                 exit;
       End;

       FDataModulo.IBTransaction.CommitRetaining;
       mensagemaviso('Conclu�do');

    Finally
           FDataModulo.IBTransaction.RollbackRetaining;
           ObjValorestransferencia.free;
    End;
     

end;

procedure TobjTalaodeCheques.edtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaPortador,Self.get_titulopesquisaportador,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//************
procedure TobjTalaodeCheques.edtNumeroChequeDescontadoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_pesquisadescontados,'Cheques Descontados',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('numero').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




end.
