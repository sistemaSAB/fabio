unit UPendencia;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPendencia,
  Grids, UopcaoRel;

type
  TFpendencia = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    edttitulo: TEdit;
    edtportador: TEdit;
    edtvalor: TEdit;
    edtsaldo: TEdit;
    edtvencimento: TMaskEdit;
    edtboletobancario: TEdit;
    edtobservacao: TEdit;
    SGLancto: TStringGrid;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    BtCancelar: TBitBtn;
    btopcoes: TBitBtn;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edttituloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edttituloKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure MostraLancamentos;
    procedure btopcoesClick(Sender: TObject);
    procedure edtboletobancarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function  atualizaQuantidade:string;
         procedure LimpaStrGrid;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fpendencia: TFpendencia;
  ObjPendencia:TObjPendencia;

implementation

uses UessencialGlobal, Upesquisa, UTitulo, Uportador, UBoletoBancario,
  UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFpendencia.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjPendencia do
    Begin
         Submit_CODIGO           (edtCODIGO.text);
         Submit_Historico        (edtHistorico.text);
         Submit_Titulo           (edtTitulo.text);
         Submit_VENCIMENTO       (edtVENCIMENTO.text);
         Submit_PORTADOR         (edtPORTADOR.text);
         Submit_VALOR            (edtVALOR.text);
         Submit_Saldo            (edtSaldo.text);
         Submit_Observacao(edtobservacao.Text);
         BoletoBancario.submit_codigo(edtboletobancario.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFpendencia.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjPendencia do
     Begin
        edtCODIGO.text        :=Get_CODIGO            ;
        edtHistorico.text     :=Get_Historico         ;
        edtTitulo.text        :=Get_Titulo            ;
        edtVENCIMENTO.text    :=Get_VENCIMENTO        ;
        edtPORTADOR.text      :=Get_PORTADOR          ;
        edtVALOR.text         :=Get_VALOR             ;
        edtSaldo.text         :=Get_Saldo             ;
        edtobservacao.Text    :=Get_Observacao        ;  
        edtboletobancario.text:=BoletoBancario.Get_codigo;
        Self.MostraLancamentos;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFpendencia.TabelaParaControles: Boolean;
begin
     ObjPendencia.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFpendencia.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjPendencia=Nil)
     Then exit;

    If (ObjPendencia.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjPendencia.free;
end;

procedure TFpendencia.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFpendencia.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFpendencia.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);
     Self.LimpaStrGrid;
     edtcodigo.text:='0';
     edtcodigo.enabled:=False;
     edtboletobancario.enabled:=False;
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjPendencia.status:=dsInsert;
     Edthistorico.setfocus;
end;

procedure TFpendencia.BtCancelarClick(Sender: TObject);
begin
     ObjPendencia.cancelar;
     self.LimpaStrGrid;
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFpendencia.BtgravarClick(Sender: TObject);
begin

     If ObjPendencia.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPendencia.salvar(true)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     Self.TabelaParaControles;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFpendencia.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     Edttitulo.enabled:=False;
     Edtvalor.enabled:=False;
     Edtsaldo.enabled:=False;
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjPendencia.Status:=dsEdit;
     edthistorico.setfocus;
end;

procedure TFpendencia.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPendencia.Get_pesquisa,ObjPendencia.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPendencia.status<>dsinactive
                                  then exit;

                                  If (ObjPendencia.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFpendencia.btalterarClick(Sender: TObject);
begin
    If (ObjPendencia.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFpendencia.btexcluirClick(Sender: TObject);
begin
     If (ObjPendencia.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjPendencia.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     

     If (ObjPendencia.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.LimpaStrGrid;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFpendencia.edttituloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FPesquisaLocal:Tfpesquisa;
   Ftitulo:TFtitulo;
Begin

    If key <> vk_f9
    then exit;

    Try
        FPesquisaLocal:=Tfpesquisa.create(Self);
        Ftitulo:=TFtitulo.create(nil);
        If (FpesquisaLocal.PreparaPesquisa(ObjPendencia.get_PesquisaTitulo,ObjPendencia.Get_TituloPesquisaTitulo,Ftitulo)=True)
        Then Begin
                If (FpesquisaLocal.showmodal=mrok)
                Then edttitulo.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
             End;
    Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Ftitulo);
    End;

end;

procedure TFpendencia.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FPesquisaLocal:Tfpesquisa;
Begin

    If key <> vk_f9
    then exit;

    Try
        FPesquisaLocal:=Tfpesquisa.create(Self);
        If (FpesquisaLocal.PreparaPesquisa(ObjPendencia.get_PesquisaPortador,ObjPendencia.Get_TituloPesquisaportador,Fportador)=True)
        Then Begin
                If (FpesquisaLocal.showmodal=mrok)
                Then edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
             End;
    Finally
           FreeandNil(FPesquisaLocal);
    End;

end;

procedure TFpendencia.edttituloKeyPress(Sender: TObject; var Key: Char);
begin
     If not (Key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFpendencia.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
     If not (Key in ['0'..'9',#8,',','.'])
     Then key:=#0
     Else Begin
               If key='.'
               Then key:=',';
          End;

end;

procedure TFpendencia.MostraLancamentos;
begin
     SGLancto.ColCount:=5;
     SGLancto.RowCount:=ObjPendencia.Get_QuantRegsLancamento(edtcodigo.text)+1;
     If (SGLancto.RowCount>1)
     Then Begin
                SGLancto.FixedRows:=1;
                ObjPendencia.Get_ListaLancamentos(SGLancto.Cols[1],SGLancto.Cols[2],SGLancto.Cols[3],SGLancto.Cols[4],SGLancto.Cols[0],EdtCodigo.text);
          End
     Else SGLancto.RowCount:=0;
     AjustaLArguraColunaGrid(SGLancto);

end;

procedure TFpendencia.btopcoesClick(Sender: TObject);
begin
     with FOpcaorel do
     begin
           With RgOpcoes do
           Begin
                   items.clear;
                   items.add('Excluir Lan�amentos');   //0
           end;
           If (Tag=0)//indica botao cancel ou fechar
           Then exit;

           Case RgOpcoes.ItemIndex of
               0:  If (ObjPendencia.ExcluiLancamentos(edtcodigo.text,true)=True)
                   Then Messagedlg('Lan�amentos Exclu�dos!',mtinformation,[mbok],0)
                   Else Messagedlg('Erro na Exclus�o dos Lan�amentos!',mterror,[mbok],0);
           end;

     end;
end;

procedure TFpendencia.edtboletobancarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FPesquisaLocal:Tfpesquisa;
Begin

    If key <> vk_f9
    then exit;

    Try
        FPesquisaLocal:=Tfpesquisa.create(Self);
        If (FpesquisaLocal.PreparaPesquisa(ObjPendencia.BoletoBancario.get_pesquisa,ObjPendencia.BoletoBancario.get_titulopesquisa,Fboletobancario)=True)
        Then Begin
                If (FpesquisaLocal.showmodal=mrok)
                Then edtboletobancario.text:=FpesquisaLocal.QueryPesq.fieldbyname(objpendencia.boletobancario.RetornaCampoCodigo).asstring;
             End;
    Finally
           FreeandNil(FPesquisaLocal);
    End;

end;

procedure TFpendencia.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjPendencia:=TObjPendencia.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
     Uessencialglobal.PegaCorForm(Self);
     
end;

function TFpendencia.atualizaQuantidade: string;
begin
     result:='Existem '+Contaregistros('tabpendencia','codigo')+' Pend�ncias Cadastradas';
end;

procedure TFpendencia.LimpaStrGrid;
begin
     SGLancto.RowCount:=1;
     SGLancto.ColCount:=1;
     SGLancto.Cols[0].clear;
     SGLancto.Cols[1].clear;
     SGLancto.Cols[2].clear;
     SGLancto.Cols[3].clear;
end;

end.
