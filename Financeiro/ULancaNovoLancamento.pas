
unit ULancaNovoLancamento;

interface

uses
  db,UessencialGlobal, UobjLancamento,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  UFrRecebimento, StdCtrls, ExtCtrls, Buttons, Mask,
  UObjValores,Dialogs, UObjValoresTransferenciaPortador,UFrPagamento,UobjgeraTransferencia,IBQuery,
  ComCtrls, UfrValorLanctoLote;

type
  TFLancaNovoLancamento = class(TForm)
    PanelLancamento: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    edtvalor: TMaskEdit;
    edtcodigohistoricosimples: TEdit;
    edthistorico: TEdit;
    edtdata: TMaskEdit;
    btok: TBitBtn;
    btcancelar: TBitBtn;
    Rgopcoes: TRadioGroup;
    edtpendencia: TEdit;
    LbCPCR: TLabel;
    FrRecimento1: TFrRecimento;
    FrPagamento1: TFrPagamento;
    Shape1: TShape;
    FrValorLancto: TFrValorLanctoLote;
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure edtcodigohistoricosimplesKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtcodigohistoricosimplesExit(Sender: TObject);
    procedure edtcodigohistoricosimplesKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btokClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FrRecimento1btinserirClick(Sender: TObject);
    procedure FrRecimento1btexcluirClick(Sender: TObject);
    procedure FrPagamento1BtGridChequeClick(Sender: TObject);
    procedure FrPagamento1edtportadorEnter(Sender: TObject);
    procedure FrPagamento1BtLancaChequeClick(Sender: TObject);
    procedure RgopcoesClick(Sender: TObject);
    procedure FrPagamento1btinserirClick(Sender: TObject);
    procedure FrValorLanctoedtdatalancamentoExit(Sender: TObject);
    procedure FrValorLanctoedtcarenciaExit(Sender: TObject);
    procedure FrValorLanctoUpDownChangingEx(Sender: TObject;
      var AllowChange: Boolean; NewValue: Smallint;
      Direction: TUpDownDirection);
    procedure FrValorLanctoUpDownChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure FrRecimento1edtcodigodebarrasExit(Sender: TObject);
    procedure FrRecimento1combotipoExit(Sender: TObject);
    procedure FrPagamento1edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure FrPagamento1btexcluirClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FrValorLanctobtLancaJurosClick(Sender: TObject);
    procedure FrValorLanctobtHistoricoClick(Sender: TObject);
  private
    { Private declarations }

      //*****Objetos que serao usados**********
      ObjLancamento:TobjLancamento;
      ObjValores:Tobjvalores;
      ObjValoresTransferenciaPortador:TobjvaloresTransferenciaPortador;
      ObjGeraTransferencia:TObjGeraTransferencia;
      //***************************************
      CredorDevedorLote,CodigoCredorDevedorLote:string;
      PtituloPagarReceberLocal:string;
      PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;
      PcodigoLancamentoJuros:string;
      PsaldoPendencia:String;
      //***************************************
    Function LancaDadosPagamento:boolean;
    Function LancaDadosRecebimento:boolean;
    function ConcluiRecebimento(Planca_Diferenca_como_Troco:boolean;Planca_Diferenca_Credito:Boolean): Boolean;
    function ConcluiPagamento(Planca_Diferenca_como_Troco:boolean): Boolean;
  public
    { Public declarations }
      PcodigoLancamentoLocal,ValorDinheiro:string;
      Procedure PassaObjeto(PObjeto:TobjLancamento;PDebitoCredito:string;P_ExportaContabilidade,P_ComCommit,P_LancaAutomatico:Boolean;PcredorDevedorLote,PcodigoCredorDevedorLote:string);
  end;

var

  FLancaNovoLancamento: TFLancaNovoLancamento;

implementation

uses Upesquisa, UHistoricoSimples, UDataModulo, UlancaTroco,
  UMostraValorMoedas, UobjLANCAMENTOCREDITO, UObjLanctoPortador,
  UObjGeraTitulo, UObjPendencia, UobjCREDITO, UHistoricoLancamentos;

{$R *.dfm}

procedure TFLancaNovoLancamento.edtvalorKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then if key='.'
          Then key:=','
          Else Key:=#0;
end;

procedure TFLancaNovoLancamento.FormShow(Sender: TObject);
begin
     habilita_campos(Self);
     habilita_botoes(Self);
     PegaCorForm(Self);
     PegaFiguraBotao(btok,'BOTAOGRAVAR.BMP');
     PegaFiguraBotao(btcancelar,'BOTAOCANCELAR.BMP');
     Self.Tag:=0;
     Rgopcoes.ItemIndex:=0;
     Rgopcoes.Enabled:=True;
     edtvalor.SetFocus;
     FrRecimento1.Visible:=False;
     FrPagamento1.Visible:=False;
     Self.AutoSize:=False;
     LbCPCR.Caption:='';
     Self.Height:=251;
     FrValorLancto.edtcarencia.Text:='0';

     ObjValores:=nil;
     ObjValoresTransferenciaPortador:=nil;
     ObjGeraTransferencia:=nil;

     Try
        ObjValores:=TObjValores.create;
     Except
           PanelLancamento.Enabled:=False;
           Messagedlg('Erro na tentativa de Criar o Objeto de Valores!',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjValoresTransferenciaPortador:=TObjValoresTransferenciaPortador.create;
        ObjGeraTransferencia:=TObjGeraTransferencia.create;
     Except
           PanelLancamento.Enabled:=False;
           Messagedlg('Erro na tentativa de Criar os Objetos de Transfer�ncia e Gerador de Transfer�ncia!',mterror,[mbok],0);
           exit;
     End;

     PanelLancamento.Enabled:=True;

     //Se veio pendencia preenchida � um lancamento normal
     //senaum � porque � lote, e sendo lote
     //nao posso deixar o usuario lancar o valor que quiser
     //nem hsitorico nem nada, estas informacoes
     //ja foram preenchidas, assim eu ja chamo o botao ok
     if (edtpendencia.text='')
     or (PLancaAutomatico=True)
     Then Begin
               Self.btokClick(sender);
     End
     Else Begin
               //lan�amento normal
               //por uma permiss�o que indique se o usu�rio pode ou n�o alterar
               //a data de lan�amento
               if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('ALTERAR A DATA DO LAN�AMENTO NA PEND�NCIA FINANCEIRA')=False)
               Then Self.edtdata.enabled:=False
               Else Self.edtdata.enabled:=true;
     End;
     
end;

procedure TFLancaNovoLancamento.btcancelarClick(Sender: TObject);
begin
     Self.Tag:=0;
     Self.Close;
end;

procedure TFLancaNovoLancamento.edtcodigohistoricosimplesKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   FHistoricoSimples:TFHistoricoSimples;
begin
        If (key<>vk_f9)
        Then exit;

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           FHistoricoSimples:=TFHistoricoSimples.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                                  If (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
                                  Then Begin
                                            TEdit(Sender).text:='';
                                            exit;
                                  End;
                                  ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
                                  edthistorico.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FHistoricoSimples);
        End;
end;

procedure TFLancaNovoLancamento.edtcodigohistoricosimplesExit(
  Sender: TObject);
begin
     IF (TEdit(Sender).text='')
     Then exit;

     If (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
     edthistorico.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;

end;

procedure TFLancaNovoLancamento.edtcodigohistoricosimplesKeyPress(
  Sender: TObject; var Key: Char);
begin
     If (not (key in ['0'..'9',#8]))
     Then key:=#0;

end;

procedure TFLancaNovoLancamento.FormKeyPress(Sender: TObject; var Key: Char);
Begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);

end;


procedure TFLancaNovoLancamento.PassaObjeto(PObjeto:TobjLancamento;PDebitoCredito:string;P_ExportaContabilidade,P_ComCommit,P_LancaAutomatico:Boolean;PcredorDevedorLote,PcodigoCredorDevedorLote:string);
begin
     Self.ObjLancamento:=Pobjeto;
     self.PtituloPagarReceberLocal:=PDebitoCredito;
     Self.PExportaContabilidade:=P_exportacontabilidade;
     Self.PComCommit:=P_ComCommit;
     PlancaAutomatico:=P_lancaAutomatico;
     Self.CredorDevedorLote      :=PcredorDevedorLote;
     Self.CodigoCredorDevedorLote:=PCodigoCredorDevedorLote;
end;
                                                                                           

procedure TFLancaNovoLancamento.btokClick(Sender: TObject);
begin

     ObjLancamento.ZerarTabela;
     if (PcodigoLancamentoLocal='')
     Then PcodigoLancamentoLocal:=ObjLancamento.Get_NovoCodigo;
     ObjLancamento.submit_CODIGO(PcodigoLancamentoLocal);
     ObjLancamento.Pendencia.submit_codigo(edtpendencia.Text);

     if (edtpendencia.text<>'')
     Then Begin//em lote nao tem pendencia
               ObjLancamento.Pendencia.LocalizaCodigo(edtpendencia.Text);
               ObjLancamento.Pendencia.TabelaparaObjeto;
     End;

     ObjLancamento.submit_Data(edtdata.text);
     ObjLancamento.submit_Historico(edthistorico.Text);
     ObjLancamento.submit_Valor(Self.edtvalor.Text);
     ObjLancamento.submit_LancamentoPai('');

     Case Rgopcoes.ItemIndex of
     0:Begin//quitacao
           if (edtpendencia.text='')//lan�amento em lote
           Then Begin
                    if (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO SEM PEND�NCIA')=fALSE)
                    Then Begin
                           Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO SEM PEND�NCIA", n�o foi encontrado!',mterror,[mbok],0);
                           Self.Close;
                           exit;
                     End
           End
           Else Begin
                     If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','QUITA��O')=False)
                     Then Begin
                               Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="QUITA��O", n�o foi encontrado!',mterror,[mbok],0);
                               Self.Close;
                               exit;
                     End;

           End;
     End;

     1:Begin//juros

           if (ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
           Then Begin
                     If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','JUROS A PAGAR')=False)
                     Then Begin
                              Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="JUROS A PAGAR", n�o foi encontrado!',mterror,[mbok],0);
                              Self.Close;
                              exit;
                      End;
           End
           else begin
                     If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','JUROS A RECEBER')=False)
                     Then Begin
                              Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="JUROS A RECEBER", n�o foi encontrado!',mterror,[mbok],0);
                              Self.Close;
                              exit;
                     End;
                     if not(ObjLancamento.VerificaPermissaoDescontoAcrescimo(ObjLancamento,'A'))
                     then exit;
           end;
     End;
     2:Begin//desconto
           if (ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
           Then Begin
                     If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO A PAGAR')=False)
                     Then Begin
                              Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="DESCONTO A PAGAR", n�o foi encontrado!',mterror,[mbok],0);
                              Self.Close;
                              exit;
                     End;
           End
           Else begin
                     If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO A RECEBER')=False)
                     Then Begin
                              Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="DESCONTO A RECEBER", n�o foi encontrado!',mterror,[mbok],0);
                              Self.Close;
                              exit;
                     End;
                     if not(ObjLancamento.VerificaPermissaoDescontoAcrescimo(ObjLancamento,'D'))
                     then exit;
           End;
     End;
     End;//case

     ObjLancamento.ObjGeraLancamento.TabelaparaObjeto;

     ObjLancamento.TipoLancto.Submit_CODIGO(ObjLancamento.ObjGeraLancamento.TipoLAncto.Get_CODIGO);
     ObjLancamento.status:=dsinsert;

     if (Rgopcoes.ItemIndex<>0)
     Then Begin//se for desconto ou juro ja salvo ComCommit=True
             If (ObjLancamento.salvar(True,True,ObjLancamento.ObjGeraLancamento.Get_CodigoPlanodeContas,true,False)=False)
             Then ObjLancamento.Rolback;

             Self.Close;
             exit;
     End
     Else Begin//quitacao
               if (edtpendencia.Text='')
               Then Begin//Pagamento ou Recebimento em Lote
                         if (ObjLancamento.SalvarSemPendencia(False)=False)
                         Then Begin
                                   Messagedlg('Erro na Tentativa de Salvar!',mterror,[mbok],0);
                                   Self.Close;
                         End;
               End
               Else Begin
                         ObjLancamento.Pendencia.LocalizaCodigo(edtpendencia.Text);
                         ObjLancamento.Pendencia.TabelaparaObjeto;
                         PsaldoPendencia:=ObjLancamento.Pendencia.Get_Saldo;
                         If (ObjLancamento.salvar(False,True,'',true,False,PcodigoLancamentoJuros)=False)
                         Then Begin
                                   Messagedlg('Erro na Tentativa de Salvar!',mterror,[mbok],0);
                                   Self.Close;
                         End;
                         PcodigoLancamentoLocal:=ObjLancamento.Get_codigo;

               End;

     End;

     //deu certo desativo a parte de cima
     PanelLancamento.Enabled:=False;
     desabilita_campos(PanelLancamento);
     Rgopcoes.Enabled:=False;
     desab_botoes(PanelLancamento);

     //ativo a parte referente ao recebimento ou pagamento

     Self.Height:=558;
     Self.Top:=0;

     if (PtituloPagarReceberLocal='R')
     Then Begin//recebimento

              if (Self.LancaDadosRecebimento=False)
              Then exit;

              FrRecimento1.passaobjeto(objvalores,strtofloat(ObjLancamento.Get_Valor),Self.CredorDevedorLote,Self.CodigoCredorDevedorLote);
              FrRecimento1.Visible:=True;
              //FrRecimento1.edtvalor.text:=Self.valordinheiro;   anterior
              FrRecimento1.edtvalor.Text := edtvalor.Text; //Pega o valor que foi digitado em cima  -   Rodolfo
              FrRecimento1.edtvalor.SetFocus;
              FrRecimento1.lbportador.caption:='';
              LbCPCR.Caption:='VALORES RECEBIDOS';
     End
     Else Begin

              if (Self.LancaDadosPagamento=False)
              Then exit;

              FrPagamento1.LbtotalDinheiro.caption:='0';
              FrPagamento1.LbTotalChequeProprio.caption:='0';
              FrPagamento1.LbTotalCheque.caption:='0';
              FrPagamento1.PassaObjeto(ObjValoresTransferenciaPortador,ObjLancamento,Self.CredorDevedorLote,Self.CodigoCredorDevedorLote);
              FrPagamento1.Visible:=True;
              FrPagamento1.edtvalor.text:=edtvalor.Text;
              FrPagamento1.edtvalor.SetFocus;
              FrPagamento1.lbportador.caption:='';
              FrPagamento1.BtGridCheque.caption:='&Cheques de terceiros';
              FrPagamento1.DBGrid1.Visible:=True;
              FrPagamento1.STRGChequePortador.Visible:=False;
              LbCPCR.Caption:='VALORES PAGOS';
     End;

end;

procedure TFLancaNovoLancamento.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
Psaldo:currency;
Pvalorsaldo,ValorRecebido,ValorLancamento:Currency;
LancaDiferencaTroco:Boolean;
LancaDiferencaCredito:Boolean;
begin
      LancaDiferencaTroco:=False;
      LancaDiferencaCredito:=False;
 Try
      
      if (Rgopcoes.itemindex=0)//E quitacao
      Then Begin
                if (PanelLancamento.Enabled=True)
                Then exit;//nem chegou lancar nada

                if (PtituloPagarReceberLocal='R')//a receber
                Then Begin 
                        ValorRecebido:=0;
                        ValorRecebido:=ObjValores.Get_SomaValores(ObjLancamento.Get_CODIGO);
                        ValorLancamento:=StrTofloat(ObjLancamento.get_valor);
                        PValorSaldo:=strtofloat(tira_ponto(formata_valor(ValorRecebido-ValorLancamento)));

                        
                End
                Else BEgin
                          //Pego o Valor do Lan�amento
                          ValorLancamento:=0;
                          ValorLancamento:=Strtofloat(ObjLancamento.Get_Valor);
                          //Pego o Valor das Transferencias onde o Lan�amento Tenha Sido este e verifico com o valor do lan�amento
                          PValorsaldo:=strtofloat(tira_ponto(formata_valor(ObjValoresTransferenciaPortador.TransferenciaPortador.get_somaporlancamento(objlancamento.get_codigo)-ValorLancamento)));
                End;

                if (PvalorSaldo<>0)
                Then Begin
                        If (PvalorSaldo>0)//troco
                        Then Begin
                                  if (Messagedlg('Deseja lan�ar a Diferen�a de R$ '+formata_valor(PvalorSaldo)+' como Troco?',mtconfirmation,[mbyes,mbno],0)=mrno)
                                  Then Begin
                                            if (Messagedlg('Deseja lan�ar a Diferen�a de R$ '+formata_valor(PvalorSaldo)+' como Cr�dito ao cliente?',mtconfirmation,[mbyes,mbno],0)=mrno)
                                            Then Begin
                                                      if (Messagedlg('N�o � poss�vel concluir o recebimento com essa diferen�a. Deseja continuar os lan�amentos? '+#13+
                                                                     ' Pressione SIM para retornar aos lan�amentos ou n�o para cancel�-los',mtconfirmation,[mbyes,mbno],0)=mryes)
                                                      Then abort;
                                            End
                                            else LancaDiferencaCredito:=True;

                                  End
                                  Else LancaDiferencaTroco:=True;

                        end
                        Else Begin//falta lancar alguma coisa
                                  if (Messagedlg('N�o � poss�vel concluir o recebimento com um saldo positivo. Deseja continuar os lan�amentos? '+#13+
                                                'Pressione SIM para retornar aos lan�amentos ou n�o para cancel�-los',mtconfirmation,[mbyes,mbno],0)=mryes)
                                  Then Begin
                                        FDataModulo.IBTransaction.RollbackRetaining;
                                        //abort;
                                  End;
                        End;
                End;
      End;



    if (PcodigoLancamentoLocal<>'')
    Then Begin//houve um lancamento, devo procurar pelos recebimentos ou pagamentos

              if (Rgopcoes.ItemIndex<>0)
              Then exit;

              ObjLancamento.ZerarTabela;

              if (ObjLancamento.LocalizaCodigo(PcodigoLancamentoLocal)=False)
              Then Begin
                        if (PComCommit=True)
                        Then  ObjLancamento.Rolback;
                        Messagedlg('Lan�amento n�o Encontrado!',mtInformation,[mbok],0);
                        exit;
              End;

              ObjLancamento.TabelaparaObjeto;
              if (PtituloPagarReceberLocal='R')  //Titulos a Receber
              Then Begin
                        if (Self.ConcluiRecebimento(LancaDiferencaTroco,LancaDiferencaCredito)=False)
                        Then Begin
                                  //Antes era s� dar um rollback, mas tive problemas de lock conflict na glassbox
                                  //porque duas pessoas iriram fazer quitacao, ai travava a tabela de portador
                                  //que estava aumentando ou diminuindo o saldo
                                  //Por isso Devo Excluir Manualmente os Lancamentos Feitos
                                  
                                  Self.ObjValores.ExcluiporLancamento(PcodigoLancamentoLocal,true);
                                  Self.ObjValoresTransferenciaPortador.RetornaPagamentoLancamento(PcodigoLancamentoLocal);
                                  self.ObjLancamento.Exclui(PcodigoLancamentoLocal,true);
                                  Messagedlg('N�o foi poss�vel Concluir o Recebimento',mterror,[mbok],0);
                                  exit;
                        End
                        Else Begin
                                Self.Tag:=1;//sinal que deu tudo certo
                                if (PComCommit=True)
                                Then  begin
                                    ObjLancamento.Commit;
                                end;
                        End;
              End
              Else Begin//Titulos a Pagar
                       if (Self.ConcluiPagamento(LancaDiferencaTroco)=False)
                       Then Begin
                                  //Antes era s� dar um rollback, mas tive problemas de lock conflict na glassbox
                                  //porque duas pessoas iriram fazer quitacao, ai travava a tabela de portador
                                  //que estava aumentando ou diminuindo o saldo
                                  //Por isso Devo Excluir Manualmente os Lancamentos Feitos

                                  Self.ObjValoresTransferenciaPortador.RetornaPagamentoLancamento(PcodigoLancamentoLocal);
                                  Self.ObjValores.ExcluiporLancamento(PcodigoLancamentoLocal,true);//troco
                                  Self.ObjLancamento.Exclui(PcodigoLancamentoLocal,true);
                                  Messagedlg('N�o foi poss�vel Concluir o Pagamento',mterror,[mbok],0);
                                  exit;
                       End
                       Else Begin
                                Self.Tag:=1;//sinal que deu tudo certo
                                if (PComCommit=True)
                                Then ObjLancamento.Commit;
                       End;
              End;

              //imprimindo o lancamento
              //baixAR O CHEQUE DO PORTADOR ASUTOMATICO ATRAVES DE UM PARAMETROF

              Try
                    if (PtituloPagarReceberLocal='R') and (Self.edtpendencia.Text<>'')//so entra se nao for em lote 
                    Then Begin
                              if (ObjParametroGlobal.ValidaParametro('IMPRIME RECIBO DE RECEBIMENTO AP�S LAN�AMENTO')=False)
                              then exit;
        
                              if (ObjParametroGlobal.Get_Valor='SIM')
                              Then ObjLancamento.ImprimeRecibo(PcodigoLancamentoLocal,PcodigoLancamentoJuros,PsaldoPendencia);
                      End;
        
                      if (PtituloPagarReceberLocal='P') and (self.edtpendencia.Text<>'')
                      Then Begin
                                if (ObjParametroGlobal.ValidaParametro('IMPRIME RECIBO DE PAGAMENTO AP�S LAN�AMENTO')=False)
                                then exit;
                                
                                if (ObjParametroGlobal.Get_Valor='SIM')
                                Then Begin
                                          if (Self.ObjLancamento.LocalizaCodigo(PcodigoLancamentoLocal)=True)
                                          then Begin
                                                    Self.ObjLancamento.TabelaparaObjeto;
                                                    if (Self.ObjLancamento.Pendencia.Get_CODIGO<>'')
                                                    Then Self.ObjLancamento.ImprimeReciboPagamento(PCodigoLancamentoLocal);
                                                    //se for em lote soh pode imprimir no frame de lancamento
                                                    //porque as pendencias filhas ainda nao foram lancadas
                                          End;
                                End;
                      End;
              Except
                    on e:exception do
                    Begin
                        Mensagemerro('Erro na tentativa de emitir o recibo'+#13+e.message);
                    End;

              End;

    End;

  Finally

        if (ObjValores<>nil)
        Then ObjValores.Free;

        if (ObjValoresTransferenciaPortador<>nil)
        Then ObjValoresTransferenciaPortador.free;

        if (ObjGeraTransferencia<>nil)
        Then ObjGeraTransferencia.free;

        Self.edtvalor.text:='';
  End;

end;

procedure TFLancaNovoLancamento.FrRecimento1btinserirClick(Sender: TObject);
begin
  FrRecimento1.btinserirClick(Sender);
end;

procedure TFLancaNovoLancamento.FrRecimento1btexcluirClick(Sender: TObject);
begin
  FrRecimento1.btexcluirClick(Sender);
end;


function TFLancaNovoLancamento.LancaDadosPagamento: boolean;
begin
     result:=False;

     If (ObjGeraTransferencia.LocalizaHistorico('PAGAMENTO DE CONTAS COM DINHEIRO')=False)
     Then Begin
                  Messagedlg('O gerador de Transfer�ncia "PAGAMENTO DE CONTAS COM DINHEIRO" n�o foi encontrado',mterror,[mbok],0);
                  exit;
     End;

     ObjGeraTransferencia.TabelaparaObjeto;

     FrPagamento1.PValorLancamento:=strtofloat(edtvalor.Text);
     FrPagamento1.PHistoricoLancamento:=edthistorico.Text;
     FrPagamento1.PCodigoLancamento:=PcodigoLancamentoLocal;
     FrPagamento1.PportadorDestino:=ObjGeraTransferencia.PortadorDestino.Get_CODIGO;
     FrPagamento1.PdataLancamento:=edtdata.text;

     result:=true;
end;

{function TFLancaNovoLancamento.LancaDadosPagamento_TROCO(PvalorTroco:Currency): boolean;
begin
end;}


function TFLancaNovoLancamento.LancaDadosRecebimento: boolean;
begin
     result:=False;
     FrRecimento1.PcodigoLancamento:=PcodigoLancamentoLocal;
     FrRecimento1.PhistoricoLancamento:=edthistorico.Text;
     result:=True;
end;

procedure TFLancaNovoLancamento.FrPagamento1BtGridChequeClick(
  Sender: TObject);
begin
  FrPagamento1.BtGridChequeClick(Sender);

end;

procedure TFLancaNovoLancamento.FrPagamento1edtportadorEnter(
  Sender: TObject);
begin
  FrPagamento1.edtportadorEnter(Sender);
  

end;

function TFLancaNovoLancamento.ConcluiRecebimento(Planca_Diferenca_como_Troco:boolean;Planca_Diferenca_Credito:Boolean): Boolean;
var
PvalorLancadoTroco,ValorRecebido,Valorlancamento,PSaldo:Currency;
ObjlancamentoCredito:TObjLANCAMENTOCREDITO;
CredorDevedorTemp, CodigoCredorDevedorTemp,CodigoPortadorTemp,codigovalores:string;
NovoValorLancamentoPortador:Currency;
ObjlancamentoPortador:Tobjlanctoportador;
Query:TIbquery;
Objgeratitulo:TObjGeraTitulo;
ObjPendencia:TobjPendencia;
Codigotitulo:string;
CodigoPendencia:string;
ObjCredito:TObjCREDITO;
begin
     Result:=False;
     ObjlancamentoCredito:=TObjLANCAMENTOCREDITO.Create;
     ObjlancamentoPortador:=TObjLanctoPortador.Create;
     Objgeratitulo:=TObjGeraTitulo.create;
     ObjPendencia:=TobjPendencia.Create;
     ObjCredito:=TObjCREDITO.Create;

     Query:=TIBQuery.Create(nil);
     Query.Database:=FDataModulo.IBDatabase;

     Try
        Try
           ValorRecebido:=0;
           ValorRecebido:=ObjValores.Get_SomaValores(ObjLancamento.Get_CODIGO);
           ValorLancamento:=StrTofloat(ObjLancamento.get_valor);
           Psaldo:=ValorRecebido-Valorlancamento;

           If (PSaldo<>0)//Troco ou erro
           Then Begin
                     if (PSaldo>0)
                     Then BEgin
                               if (Planca_Diferenca_como_Troco=False) then
                               begin
                                    if(Planca_Diferenca_Credito=false)
                                    then Exit;
                               end;

                               if(Planca_Diferenca_como_Troco=True) then
                               begin
                                       If (ObjGeraTransferencia.LocalizaHistorico('TRANSFER�NCIA PARA TROCO')=False)
                                       Then Begin
                                                    Messagedlg('O gerador de Transfer�ncia "TRANSFER�NCIA PARA TROCO" n�o foi encontrado',mterror,[mbok],0);
                                                    exit;
                                       End;

                                       ObjGeraTransferencia.TabelaparaObjeto;
                                       FlancaTroco.FrPagamento1.PValorLancamento:=PSaldo;
                                       FlancaTroco.FrPagamento1.PHistoricoLancamento:='TROCO-'+edthistorico.Text;
                                       FlancaTroco.FrPagamento1.PCodigoLancamento:=PcodigoLancamentoLocal;
                                       FlancaTroco.FrPagamento1.PportadorDestino:=ObjGeraTransferencia.PortadorDestino.Get_CODIGO;
                                       FlancaTroco.FrPagamento1.PdataLancamento:=edtdata.text;
                                       FlancaTroco.FrPagamento1.VisiBle:=true;
                                       FlancaTroco.FrRecebimento1.VisiBle:=False;
                                       FlancaTroco.FrPagamento1.PassaObjeto(ObjValoresTransferenciaPortador,ObjLancamento,Self.CredorDevedorLote,Self.CodigoCredorDevedorLote);
                                       FlancaTroco.lbvalortroco.caption:=formata_valor(psaldo);

                                       FlancaTroco.showmodal;

                                       PvalorLancadoTroco:=ObjValoresTransferenciaPortador.TransferenciaPortador.get_somaporlancamento(PcodigoLancamentoLocal);

                                       if (Pvalorlancadotroco<>PSaldo)
                                       Then Begin
                                                  Messagedlg('A soma dos valores lan�ados para troco n�o � igual ao valor total do troco',mterror,[mbok],0);
                                                  exit;
                                       End;
                               end;
                               if(Planca_Diferenca_Credito=true) then
                               begin
                                        {
                                              Jonatan Medina 18/07/2011

                                              A id�ia pricipal � poder lan�ar o valor do troco como cr�dito ao cliente
                                              ou seja ao inv�s de dar o troco pro cliente, lan�ar o valor como um cr�dito para o cliente
                                              usar numa outra venda...

                                        }
                                        //Busca o credordevedor (Cliente), Portador, o codigo desse recebimento na tabvalores
                                        CredorDevedorTemp:='';
                                        CodigoCredorDevedorTemp:='';
                                        CodigoPortadorTemp:='';
                                        codigovalores:='';
                                        FrRecimento1.LancaTrocoComoCredito(CredorDevedorTemp, CodigoCredorDevedorTemp,CodigoPortadorTemp,Codigovalores);
                                        {
                                              Preciso lan�ar um t�tulo a receber deste cliente, lan�ar uma pendencia a vista pra
                                              esse t�tulo e fazer um lan�amento de quita��o
                                              Al�m disso preciso gravar em uma tabela esse titulo relacionado com o lan�amento
                                              que gerou o mesmo, pra na hora que for estornar o lan�amento estornar esse titulo,
                                              essa pendencia e esse lan�amento...
                                        }

                                        If (Objgeratitulo.LocalizaHistorico('CR�DITO AO CLIENTE GERADO POR TROCO')=False)
                                        Then Begin
                                                    Messagedlg('O GERATITULO de hist�rico="CR�DITO AO CLIENTE GERADO POR TROCO" n�o foi encontrado!',mterror,[mbok],0);
                                                    exit;
                                        End;
                                        Objgeratitulo.TabelaparaObjeto;
                                        //************************************************/
                                        //Gerando a pend�ncia
                                        //************************************************/
                                        ObjPendencia.Titulo.ZerarTabela;
                                        ObjPendencia.Titulo.Status:=dsinsert;
                                        codigotitulo:=ObjPendencia.Titulo.get_novocodigo;
                                        ObjPendencia.Titulo.Submit_CODIGO(codigotitulo);
                                        ObjPendencia.Titulo.Submit_HISTORICO('CR�DITO AO CLIENTE GERADO POR TROCO');
                                        ObjPendencia.Titulo.Submit_GERADOR(Objgeratitulo.Get_Gerador);
                                        ObjPendencia.Titulo.Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
                                        ObjPendencia.Titulo.Submit_CREDORDEVEDOR(Objgeratitulo.Get_CredorDevedor);
                                        ObjPendencia.Titulo.Submit_CODIGOCREDORDEVEDOR(CredorDevedorTemp);
                                        ObjPendencia.Titulo.Submit_EMISSAO(Datetostr(Now));
                                        ObjPendencia.Titulo.Submit_PRAZO(Objgeratitulo.Get_Prazo);
                                        ObjPendencia.Titulo.Submit_PORTADOR(Objgeratitulo.get_portador);
                                        ObjPendencia.Titulo.Submit_VALOR(CurrToStr(PSaldo));
                                        ObjPendencia.Titulo.Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
                                        ObjPendencia.Titulo.Submit_NUMDCTO('CR�DITO GERADO POR TROCO NO LANC N�'+PcodigoLancamentoLocal);
                                        ObjPendencia.Titulo.Submit_GeradoPeloSistema('N');
                                        ObjPendencia.Titulo.Submit_ParcelasIguais(True);
                                        if (ObjPendencia.Titulo.salvar(false)=False)//nao exporta a contabilidade pois vamos exportar com estoque por esse modulo
                                        Then Begin
                                                    Messagedlg('Erro na tentativa de Gerar o T�tulo',mterror,[mbok],0);
                                                    exit;
                                        End;
                                        //************************************************/
                                        //Lan�ando a quita��o pra essa pendencia
                                        //************************************************/
                                        if(ObjPendencia.LocalizaporTitulo(Codigotitulo)=False) then
                                        begin
                                             MensagemErro('Erro na tentativa de buscar a pendencia gerada para o lan�amento de cr�dito');
                                             Exit;
                                        end;
                                        ObjPendencia.TabelaparaObjeto;
                                        CodigoPendencia:=ObjPendencia.Get_CODIGO;
                                        ObjLancamento.ZerarTabela;
                                        ObjLancamento.Status:=dsInsert;
                                        ObjLancamento.Submit_CODIGO(ObjLancamento.Get_NovoCodigo);
                                        ObjLancamento.Pendencia.Submit_CODIGO(CodigoPendencia);
                                        ObjLancamento.Submit_Valor(CurrToStr(PSaldo));
                                        ObjLancamento.Submit_Historico('Cr�dito gerado pelo troco para'+edthistorico.Text);
                                        ObjLancamento.Submit_Data(edtdata.Text);
                                        ObjLancamento.Submit_LancamentoPai('');
                                        If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','CR�DITO GERADO PELO TROCO')=False)
                                        Then Begin
                                                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="CR�DITO GERADO PELO TROCO", n�o foi encontrado!',mterror,[mbok],0);
                                                  Self.Close;
                                                  exit;
                                        End;
                                        ObjLancamento.ObjGeraLancamento.TabelaparaObjeto;
                                        ObjLancamento.TipoLancto.Submit_CODIGO(ObjLancamento.ObjGeraLancamento.TipoLAncto.Get_CODIGO);
                                        If (ObjLancamento.salvar(False,True,'',true,False,PcodigoLancamentoJuros)=False)
                                        Then Begin
                                                  Messagedlg('Erro na Tentativa de Salvar!',mterror,[mbok],0);
                                                  Self.Close;
                                        End;


                                        //**************************************************************************
                                        {
                                                Lan�o na tabcredito os dados desse lan�amento
                                                O campo Lan�amento vai receber o lan�amento que gerou o codigo
                                                O campo titulo vai receber o titulo gerado por esse lanc de credito

                                        }
                                        ObjCredito.ZerarTabela;
                                        ObjCredito.Status:=dsInsert;
                                        ObjCredito.Submit_CODIGO(ObjCredito.Get_NovoCodigo);
                                        ObjCredito.Submit_Valor(CurrToStr(PSaldo));
                                        ObjCredito.Submit_Historico('CR�DITO GERADO POR UM TROCO');
                                        ObjCredito.TITULO.Submit_CODIGO(Codigotitulo);
                                        ObjCredito.CLIENTE.Submit_Codigo(CredorDevedorTemp);
                                        ObjCredito.LANCAMENTO.Submit_CODIGO(PcodigoLancamentoLocal);
                                        if(ObjCredito.SalvarSemGerarTitulo(False)=False) then
                                        begin
                                            MensagemErro('Erro ao relacionar o cr�dito na tabcr�dito');
                                            Exit;
                                        end;

                                        //************************************************/
                                        //Localizo na tablanctoportador o lan�amento ao portador que esse recebimento gerou
                                        //************************************************/
                                        with query do
                                        begin
                                            Close;
                                            sql.Clear;
                                            sql.Add('select codigo from tablanctoportador where valores='+codigovalores);
                                            open;
                                            ObjlancamentoPortador.ZerarTabela;
                                            //Localizo esse lan�amento, pra alterar
                                            if(ObjlancamentoPortador.LocalizaCodigo(fieldbyname('codigo').AsString)=false)
                                            then Exit;

                                            ObjlancamentoPortador.TabelaparaObjeto;
                                            NovoValorLancamentoPortador:= StrToCurr(ObjlancamentoPortador.Get_Valor);
                                            {
                                                Preciso pegar o valor inteiro lan�ado e diminuir da quantidade que passou
                                                Exemplo
                                                Valor Pendencia = 150,00
                                                Valor pago = 200,00
                                                Sobrou como troco = 50,00
                                                mas esse 50,00 ser� lan�ado como cr�dito ao cliente, ent�o

                                                Foi lan�ado na Tabvalores o valor recebido = 200,00
                                                Foi lan�ado na tablan�ameto o valor dessa pendencia = 150,00
                                                Foi lan�ado na Tablanctoportador o valor recebido = 200,00

                                                Preciso lan�ar o valor do credito, de  50,00 ... mas se eu fizer isso
                                                vo ter 50,00 reais a mais em caixa, portanto preciso pegar o primeiro
                                                lan�amento na tablanctoportodar, no valor de 200,00 e tirar o valor que
                                                era pra ser troco e foi usado como credito, 50,00...

                                                Portanto, atualizo na tablanctoportador
                                                onde era 200,00 agora eu altero pra 150,00 (valor real da pendencia)
                                                e fa�o lan�amento de  50,00 na tablanctoportador como cr�dito gerado
                                                ao cliente


                                            }
                                            NovoValorLancamentoPortador:=NovoValorLancamentoPortador-PSaldo;
                                            ObjlancamentoPortador.Submit_Valor(CurrToStr(NovoValorLancamentoPortador));
                                            ObjlancamentoPortador.Status:=dsEdit;
                                            if(ObjlancamentoPortador.Salvar(false,false)=False) then
                                            begin
                                                 MensagemErro('Erro ao atualizar valores no lan�amento');
                                                 Exit;
                                            end;
                                        end;

                                        //************************************************/
                                        //Lan�ando na tablanctoportador o valor do credito
                                        //************************************************/

                                        ObjlancamentoPortador.ZerarTabela;
                                        ObjlancamentoPortador.Status:=dsInsert;
                                        ObjlancamentoPortador.Submit_CODIGO(ObjlancamentoPortador.Get_NovoCodigo);
                                        ObjlancamentoPortador.Submit_Portador(CodigoPortadorTemp);

                                        //***************************************************************************/
                                        //tipo de lan�amento "credito gerado pelo troco"   = 12
                                        //colocar um sql pra achar esse tipo de lan�amento ao portador...
                                        //num da colocar doze direto, pq em algumas bases ja existe e tipo de lan�amento ao portador
                                        //com esse codigo... foda =\
                                        //**************************************************************************/

                                       Query.Close;
                                       Query.SQL.Clear;
                                       Query.sql.Add('select codigo from tabtipolanctoportador where historico=''CR�DITO GERADO PELO TROCO'' ');
                                       Query.Open;
                                       //se n�o achar � porque o nome esta cadastrado errado ou porque n�o foi cadastrado
                                       if(query.fieldbyname('codigo').asstring='')then
                                       begin
                                            MensagemErro('o Tipo de Lan�amento ao portador "CR�DITO GERADO PELO TROCO" n�o foi encontrado');
                                            exit;
                                       end;

                                        ObjlancamentoPortador.Submit_TipoLancto(Query.fieldbyname('codigo').AsString);
                                        ObjlancamentoPortador.Submit_Historico('Cr�dito gerado pelo troco para '+edthistorico.text);
                                        ObjlancamentoPortador.Submit_Data(edtdata.Text);
                                        ObjlancamentoPortador.Submit_Valor(CurrToStr(PSaldo));
                                        ObjlancamentoPortador.Submit_TabelaGeradora('TABLANCAMENTO');
                                        ObjlancamentoPortador.Submit_ObjetoGerador('OBJLANCAMENTO');
                                        ObjlancamentoPortador.Submit_CampoPrimario('CODIGO');
                                        ObjlancamentoPortador.Submit_ValordoCampo(PcodigoLancamentoLocal);
                                        ObjlancamentoPortador.Submit_ImprimeRel(True);
                                        ObjlancamentoPortador.Lancamento.Submit_CODIGO(PcodigoLancamentoLocal);
                                        ObjlancamentoPortador.Salvar(false,false);

                                        //************************************************/
                                        //lan�ando na tablancamento credito o valor do cr�dito pro cliente
                                        //************************************************/

                                        ObjlancamentoCredito.ZerarTabela;
                                        ObjlancamentoCredito.Status:=dsInsert;
                                        ObjlancamentoCredito.Submit_Codigo('0');
                                        ObjlancamentoCredito.Submit_Data(DateToStr(Now));
                                        ObjlancamentoCredito.Submit_Cliente(CredorDevedorTemp);
                                        ObjlancamentoCredito.Submit_Valor(CurrToStr(PSaldo));
                                        ObjlancamentoCredito.Submit_Pedido('');
                                        ObjlancamentoCredito.Submit_Valores(codigovalores);
                                        ObjlancamentoCredito.Submit_Historico('Cr�dito gerado pelo troco');
                                        if(ObjlancamentoCredito.Salvar(false)=False)
                                        then Exit;

                               end;


                     End
                     Else exit;//vai voltar com False
           End;

           result:=True;
        Except
              exit;
        End;
     Finally
            If (result=True) and (PExportaContabilidade=True)//Tenho que exportar para a contabilidade o que aconteceu
            Then Result:=Objlancamento.ExportaContabilidade_Recebimento;
            ObjlancamentoCredito.free;
            ObjlancamentoPortador.Free;
            Objgeratitulo.free;
            ObjPendencia.Free;
            ObjCredito.Free;
            FreeAndNil(Query);
     End;
End;

function TFLancaNovoLancamento.ConcluiPagamento(Planca_Diferenca_como_Troco:boolean): Boolean;
var
PvalorLancadoTroco,Psaldo,ValorLancamento:Currency;
begin
     result:=False;

     Try
              //Pego o Valor do Lan�amento
              ValorLancamento:=0;
              ValorLancamento:=Strtofloat(ObjLancamento.Get_Valor);
              //Pego o Valor das Transferencias onde o Lan�amento Tenha Sido este e verifico com o valor do lan�amento
              Psaldo:=ObjValoresTransferenciaPortador.TransferenciaPortador.get_somaporlancamento(objlancamento.get_codigo)-ValorLancamento;
              
              If (Psaldo>0)//lancou a mais, pode ser troco
              Then Begin
                        if (Planca_Diferenca_como_Troco=False)
                        Then exit;//retorna como false

                        FlancaTroco.FrRecebimento1.VisiBle:=true;
                        FlancaTroco.FrRecebimento1.edtvalor.text:=edtvalor.Text;
                        FlancaTroco.FrRecebimento1.lbportador.caption:='';
                        FlancaTroco.FrRecebimento1.PcodigoLancamento:=PcodigoLancamentoLocal;
                        FlancaTroco.FrRecebimento1.PHistoricoLancamento:='TROCO-'+edthistorico.Text;
                        FlancaTroco.FrPagamento1.VisiBle:=False;
                        FlancaTroco.lbvalortroco.caption:=formata_valor(psaldo);
                        FlancaTroco.FrRecebimento1.PassaObjeto(ObjValores,psaldo,Self.CredorDevedorLote,Self.CodigoCredorDevedorLote);
                        
                        FlancaTroco.showmodal;

                        PvalorLancadoTroco:=ObjValores.Get_SomaValores(PcodigoLancamentoLocal);

                        if (Pvalorlancadotroco<>PSaldo)
                        Then Begin
                                   Messagedlg('A soma dos valores lan�ados para troco n�o � igual ao valor total do troco',mterror,[mbok],0);
                                   exit;
                        End;
                        result:=true;

              End
              Else Begin
                        if (Psaldo<0)//faltou lancar alguma coisa, precisa ser menor ou igual
                        Then begin
                                  Messagedlg('A soma dos valores lan�ados n�o � igual ao valor do lan�amento',mterror,[mbok],0);
                                  exit;
                        end
                        else result:=True;
              End;

    Finally
            If (Result=True) and (PExportaContabilidade=True)
            Then Result:=Objlancamento.ExportaContabilidade_Pagamento;
    End;
    


end;


procedure TFLancaNovoLancamento.FrPagamento1BtLancaChequeClick(
  Sender: TObject);
begin
    FrPagamento1.BtLancaChequeClick(Sender);
end;


procedure TFLancaNovoLancamento.RgopcoesClick(Sender: TObject);
begin
     case Rgopcoes.ItemIndex of
     0:FrValorLancto.AcertapelaCarencia(edtvalor);
     else edtvalor.text:='0';
     End;
end;

procedure TFLancaNovoLancamento.FrPagamento1btinserirClick(
  Sender: TObject);
begin
  FrPagamento1.btinserirClick(Sender);

end;

procedure TFLancaNovoLancamento.FrValorLanctoedtdatalancamentoExit(
  Sender: TObject);
begin
     FrValorLancto.AcertapelaCarencia(edtvalor);
end;

procedure TFLancaNovoLancamento.FrValorLanctoedtcarenciaExit(
  Sender: TObject);
begin
  FrValorLancto.edtcarenciaExit(Sender);
  FrValorLancto.AcertapelaCarencia(edtvalor);


end;

procedure TFLancaNovoLancamento.FrValorLanctoUpDownChangingEx(
  Sender: TObject; var AllowChange: Boolean; NewValue: Smallint;
  Direction: TUpDownDirection);
begin
  FrValorLancto.UpDownChangingEx(Sender, AllowChange, NewValue,Direction);
  FrValorLancto.AcertapelaCarencia(edtvalor);


end;

procedure TFLancaNovoLancamento.FrValorLanctoUpDownChanging(
  Sender: TObject; var AllowChange: Boolean);
begin
   FrValorLancto.AcertapelaCarencia(edtvalor);
end;




procedure TFLancaNovoLancamento.FrRecimento1edtcodigodebarrasExit(
  Sender: TObject);
begin
  FrRecimento1.edtcodigodebarrasExit(Sender);
end;

procedure TFLancaNovoLancamento.FrRecimento1combotipoExit(Sender: TObject);
begin
  FrRecimento1.combotipoExit(Sender);

end;

procedure TFLancaNovoLancamento.FrPagamento1edtvalorKeyPress(
  Sender: TObject; var Key: Char);
begin
  FrPagamento1.edtvalorKeyPress(Sender, Key);

end;

procedure TFLancaNovoLancamento.FrPagamento1btexcluirClick(
  Sender: TObject);
begin
  FrPagamento1.btexcluirClick(Sender);

end;

procedure TFLancaNovoLancamento.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
Var FMostraValorMoeda:TFMostraValorMoedas;
begin
     if (key=vk_F2)
     then Begin
           try
              FMostraValorMoeda:=TFMostraValorMoedas.create(nil);

              if (FrRecimento1.Visible = true)
              then FMostraValorMoeda.EdtValor.Text:=FrRecimento1.LbSaldo.Caption
              else FMostraValorMoeda.EdtValor.Text:=FrPagamento1.LbSaldo.Caption;

              FMostraValorMoeda.ShowModal;
          finally
              FreeAndnil(FMostraValorMoeda);
          end;
     end;

end;

procedure TFLancaNovoLancamento.FrValorLanctobtLancaJurosClick(
  Sender: TObject);
var
  CodigoLanctoJuros, temp : string;
begin
  temp := '';
  if(StrToCurr(FrValorLancto.PJ_lbValorJuros.Caption) = 0) then
    Exit;

  if self.ObjLancamento.LancaJuros_NovoObjeto(Self.ObjLancamento.Pendencia.Get_CODIGO,StrToCurr(FrValorLancto.PJ_lbValorJuros.Caption)
    ,FrValorLancto.edtdatalancamento.Text,'',CodigoLanctoJuros,False,'Lan�amento de juros na quita��o') then
  begin
    FDataModulo.IBTransaction.CommitRetaining;
    Self.ObjLancamento.AtualizaTelaNovoLancamento(edtpendencia.Text,'','',temp,edthistorico.text,true,true,False,'','',FrValorLancto.edtdatalancamento.Text,0);
  end
  else
    MensagemErro('N�o gravou os juros');
end;

procedure TFLancaNovoLancamento.FrValorLanctobtHistoricoClick(
  Sender: TObject);
var
  FHistorico : TFhistoricoLancamento;
  temp : string;
begin
  temp := '';
  FHistorico := TFhistoricoLancamento.Create(self);
  try
    FHistorico.LbPendencia.Caption := edtpendencia.Text;
    FHistorico.Objlancamento := ObjLancamento;
    FHistorico.btRecibo.Visible:=FHistorico.ObjLancamento.LancamentoPagamento(edtpendencia.Text);
    FHistorico.ShowModal;
    Self.ObjLancamento.AtualizaTelaNovoLancamento(edtpendencia.Text,'','',temp,edthistorico.text,true,true,False,'','',FrValorLancto.edtdatalancamento.Text,0)
  finally
    FreeAndNil(FHistorico);
  end;
end;

end.


