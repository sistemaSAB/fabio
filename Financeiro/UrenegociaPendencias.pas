unit UrenegociaPendencias;

interface

uses
  db,UessencialGlobal, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, Buttons, Mask, ExtCtrls,UobjLancamento,ComObj;

type
  TFrenegociaPendencias = class(TForm)
    StrgPendencias: TStringGrid;
    Panel1: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LbNomeCredorDevedor: TLabel;
    edtvencimento1: TMaskEdit;
    edtvencimento2: TMaskEdit;
    combocredordevedor: TComboBox;
    combocredordevedorcodigo: TComboBox;
    edtcodigocredordevedor: TEdit;
    Btpesquisar: TBitBtn;
    BTRenegocia: TBitBtn;
    LbtotalSelecionado: TLabel;
    Label5: TLabel;
    procedure BtpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorExit(Sender: TObject);
    procedure StrgPendenciasDblClick(Sender: TObject);
    procedure StrgPendenciasKeyPress(Sender: TObject; var Key: Char);
    procedure BTRenegociaClick(Sender: TObject);
    procedure edtSomenteNumerosKeyPress(Sender: TObject;var Key: Char);
  private
    { Private declarations }
    PcredorDevedor,PcodigoCredorDevedor:string;
    procedure SomaSelecionadas;
    function AdiquiraOuCrieObjeto(const ClasseNome: String): IDispatch;



  public
    { Public declarations }
    ObjLancamento:TobjLancamento;
  end;

var
  FrenegociaPendencias: TFrenegociaPendencias;

implementation

uses Upesquisa, UDataModulo, UObjGeraTitulo, UObjTitulo, IBQuery,
  UFiltraImp;

{$R *.dfm}

procedure TFrenegociaPendencias.BtpesquisarClick(Sender: TObject);
var
cont:integer;
begin

     if (edtcodigocredordevedor.Text='')
     Then Begin
               Messagedlg('Escolha um Credor/Devedor!',mtInformation,[mbok],0);
               exit;
     End;



     StrgPendencias.ColCount:=7;
     StrgPendencias.Cols[0].clear;
     StrgPendencias.Cols[1].clear;
     StrgPendencias.Cols[2].clear;
     StrgPendencias.Cols[3].clear;
     StrgPendencias.Cols[4].clear;
     StrgPendencias.Cols[5].clear;


     StrgPendencias.Cells[0,0]:='..';
     StrgPendencias.Cells[1,0]:='T�TULO';
     StrgPendencias.Cells[2,0]:='PEND.';
     StrgPendencias.Cells[3,0]:='VENCIMENTO';
     StrgPendencias.Cells[4,0]:='VALOR';
     StrgPendencias.Cells[5,0]:='SALDO';
     StrgPendencias.Cells[6,0]:='HIST�RICO';

     With Self.Objlancamento.Pendencia.ObjQlocal do
     Begin
          close;
          SQL.clear;
          sql.add('Select tabpendencia.titulo,tabpendencia.codigo as pendencia,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo,tabtitulo.historico from tabpendencia');
          SQL.add('join TabTitulo on TabPendencia.Titulo=TabTitulo.codigo');
          sql.add('join tabcontager on TabTitulo.ContaGerencial=TabContaGer.codigo');
          sql.add('where tabcontager.tipo=''D'' and tabpendencia.saldo>0');
          sql.add('and TabTitulo.credordevedor='+combocredordevedorcodigo.Items[combocredordevedor.itemindex]);
          sql.add('and TabTitulo.CodigocredorDevedor='+edtcodigocredordevedor.Text);
          Try
             StrToDate(edtvencimento1.text);
             StrToDate(edtvencimento2.text);
             sql.add('and TabPendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento1.text))+#39);
             sql.add('and TabPendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento2.text))+#39);
          Except

          End;
          open;
          PcredorDevedor:=combocredordevedorcodigo.Items[combocredordevedor.itemindex];
          PcodigoCredorDevedor:=edtcodigocredordevedor.Text;
          
          last;
          
          if (RecordCount>0)
          Then StrgPendencias.RowCount:=Recordcount+1
          Else StrgPendencias.RowCount:=2;
          
          first;

          cont:=1;
          While not(eof) do
          Begin
               StrgPendencias.Cells[0,cont]:='';
               StrgPendencias.Cells[1,cont]:=fieldbyname('titulo').asstring;
               StrgPendencias.Cells[2,cont]:=fieldbyname('pendencia').asstring;
               StrgPendencias.Cells[3,cont]:=fieldbyname('vencimento').asstring;
               StrgPendencias.Cells[4,cont]:=formata_valor(fieldbyname('valor').asstring);
               StrgPendencias.Cells[5,cont]:=formata_valor(fieldbyname('saldo').asstring);
               StrgPendencias.Cells[6,cont]:=fieldbyname('historico').asstring;
               next;
               inc(cont,1);
          End;

          LbtotalSelecionado.caption:='0';
          AjustaLArguraColunaGrid(StrgPendencias);
          StrgPendencias.SetFocus;
     End;
End;

procedure TFrenegociaPendencias.FormShow(Sender: TObject);
begin
     Btpesquisar.Enabled:=False;
     PcredorDevedor:='';
     PcodigoCredorDevedor:='';
     LbtotalSelecionado.caption:='0';
     LbNomeCredorDevedor.caption:='';
     
     Try
        Self.Objlancamento:=TobjLancamento.create;
        Btpesquisar.Enabled:=True;
        edtvencimento1.setfocus;
        Self.Objlancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedor.items);
        Self.Objlancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigo.items);
     Except
           mensagemerro('Erro na tentativa de Criar o Objeto de Pend�ncia');
           exit;
     End;
     
end;

procedure TFrenegociaPendencias.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then begin
           Perform(Wm_NextDlgCtl,0,0);
           //key:=#0;
      end;
end;

procedure TFrenegociaPendencias.edtcodigocredordevedorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combocredordevedor.itemindex=-1)
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(Self.Objlancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigo.items[combocredordevedor.itemindex]),'Pesquisa de '+Combocredordevedor.text,Self.Objlancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex]))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedor.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Objlancamento.Pendencia.Titulo.Get_CampoNomeCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex])).asstring;
                             End;

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFrenegociaPendencias.edtcodigocredordevedorExit(
  Sender: TObject);
begin
      //preciso sair com o nome do credordevedor escolhido
     //atraves do FrLancamento1.ObjLancamento.Pendencia.Titulo
     LbNomeCredorDevedor.caption:='';

     If (edtcodigocredordevedor.text='') or (combocredordevedor.ItemIndex=-1)
     Then exit;

     LbNomeCredorDevedor.caption:=Self.Objlancamento.Pendencia.Titulo.Get_NomeCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex],edtcodigocredordevedor.text);
end;

procedure TFrenegociaPendencias.StrgPendenciasDblClick(Sender: TObject);
begin
     if (StrgPendencias.Cells[0,StrgPendencias.row]='X')
     then StrgPendencias.Cells[0,StrgPendencias.row]:=''
     Else StrgPendencias.Cells[0,StrgPendencias.row]:='X';
     Self.SomaSelecionadas;
end;

procedure TFrenegociaPendencias.StrgPendenciasKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     then Begin
                StrgPendencias.setfocus;
                StrgPendenciasDblClick(sender);
     End;
end;

procedure TFrenegociaPendencias.BTRenegociaClick(Sender: TObject);
var
cont:integer;
PcodigoLancamento,PtituloResultante:string;
ObjgeraTitulo:TobjgeraTitulo;
PathModeloStr,Pnome:String;
VelhoWord, NovoWord: variant;
WinWord, Doc: Variant;
Pnovovalor,Psoma:Currency;
begin
     Pnome:=ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PcodigoCredorDevedor);

     Try
        ObjgeraTitulo:=TobjgeraTitulo.create;
     Except
          MensagemErro('Erro na tentativa de Criar o Objeto gerador de t�tulo');
          exit;
     End;


     try
         if (StrgPendencias.Cols[0].IndexOf('X')=-1)
         then Begin
                   MensagemAviso('Selecione uma Pend�ncia');
                   exit;
         End;

         Ptituloresultante:='';
         PcodigoLancamento:='';
         Ptituloresultante:=ObjLancamento.Pendencia.Titulo.Get_NovoCodigo;
         PnovoValor:=0;

         FfiltroImp.DesativaGrupos;
         FfiltroImp.Grupo01.Enabled:=true;
         FfiltroImp.edtgrupo01.EditMask:='';
         FfiltroImp.LbGrupo01.Caption:='Novo Valor ';
         FfiltroImp.edtgrupo01.text:=tira_ponto(LbtotalSelecionado.caption);
         FfiltroImp.edtgrupo01.OnKeyPress:=self.edtSomenteNumerosKeyPress;
         FfiltroImp.showmodal;

         if (FfiltroImp.tag=0)
         then exit;

         try
            Pnovovalor:=strtofloat(FfiltroImp.edtgrupo01.text);
         Except
                mensagemerro('Valor Inv�lido');
                exit;
         End;



         
         For cont:=1 to StrgPendencias.RowCount-1 do
         Begin
              //********************************************************************
              if (StrgPendencias.Cells[0,cont]='X')
              Then begin
                        if (ObjLancamento.LancamentoAutomatico('D',StrgPendencias.Cells[2,cont],TIRA_PONTO(StrgPendencias.Cells[5,cont]),Datetostr(now),'PARCELA RENEGOCIADA - TITULO RESULTANTE '+ptituloresultante,pcodigolancamento,False)=False)
                        then Begin
                               MensagemErro('Erro na tentativa de Gravar Desconto na Pend�ncia '+StrgPendencias.Cells[2,cont]);
                               exit;
                        End; 
              End;
         End;

         //Criando o titulo resultante
         if (Objgeratitulo.LocalizaHistorico('T�TULO RESULTANTE DE RENEGOCIA��O DE PEND�NCIAS')=False)
         then Begin
                  MensagemErro('O gerador do T�tulo "T�TULO RESULTANTE DE RENEGOCIA��O DE PEND�NCIAS" n�o foi encontrado');
                  exit;
         end;

         Objgeratitulo.TabelaparaObjeto;

         With Self.ObjLancamento.Pendencia.Titulo do
         begin
              ZerarTabela;
              Status:=dsinsert;
              Submit_CODIGO(PtituloResultante);
              Submit_CREDORDEVEDOR(PcredorDevedor);
              Submit_CODIGOCREDORDEVEDOR(PcodigoCredorDevedor);
              Submit_HISTORICO('T�TULO RESULTANTE DE RENEGOCIA��O DE PEND�NCIAS - '+Pnome);
              Submit_GERADOR(Objgeratitulo.Get_Gerador);
              Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
              Submit_EMISSAO(datetostr(now));
              Submit_PRAZO(Objgeratitulo.Get_Prazo);
              Submit_PORTADOR(Objgeratitulo.Get_Portador);
              Submit_VALOR(floattostr(pnovovalor));
              Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
              Submit_NUMDCTO('0');
              Submit_GeradoPeloSistema('N');
              
              If (Salvar(False)=False)
              Then Begin
                        MensagemErro('Erro no lan�amento do T�tulo do Saldo! Lan�amentos Cancelados');
                        exit;
              End;
         End;

         //Pegando o Local onde estara guardado o modelo
         PathModeloStr:='';

          if ObjParametroGlobal.ValidaParametro('CAMINHO DOS MODELOS')=false
          then exit;
          PathModeloStr:=ObjParametroGlobal.Get_Valor;

          If (PathModeloStr[length(pathmodelostr)]<>'\')
          Then PathModeloStr:=PathModeloStr+'\';

         PathModeloStr:=pathmodelostr+'CartaRenegociacaoPendencia.doc';
         
         if (FileExists(PathModeloStr)=False)
         Then Begin
                   Messagedlg('O Arquivo '+pathmodelostr+' n�o foi encontrado!',mterror,[mbok],0);
                   exit;
         End;


         VelhoWord:=AdiquiraOuCrieObjeto('Word.Basic');
         NovoWord :=AdiquiraOuCrieObjeto('Word.Application');
         //Tornar o word visivel
         NovoWord.Visible := True;
         //Abrir arquivo para edi��o
         Doc:= NovoWord.Documents.Open(PathModeloStr);


         Doc.Content.Find.Execute(FindText := '<<CREDORDEVEDOR>>', ReplaceWith :=uppercase(pnome));
         For cont:=1 to StrgPendencias.RowCount-1 do
         Begin
              //********************************************************************
              if (StrgPendencias.Cells[0,cont]='X')
              Then Doc.Content.Find.Execute(FindText := '<<VALOR_ATUAL>>', ReplaceWith :='VENCIMENTO: '+StrgPendencias.cells[3,cont]+' SALDO: R$ '+CompletaPalavra_a_Esquerda(StrgPendencias.cells[5,cont],12,' ')+#13+'<<VALOR_ATUAL>>');
         End;
         Doc.Content.Find.Execute(FindText := '<<VALOR_ATUAL>>', ReplaceWith :='TOTAL '+CompletaPalavra_a_Esquerda(LbtotalSelecionado.caption,39,' '));

         With Self.ObjLancamento.Pendencia.ObjQlocal do
         Begin
              close;
              SQL.clear;
              sql.add('Select * from Tabpendencia where titulo='+PtituloResultante);
              open;
              first;
              Psoma:=0;
              Doc.Content.Find.Execute(FindText := '<<NOVO_VALOR>>', ReplaceWith :='T�TULO    : '+PtituloResultante+#13+'<<NOVO_VALOR>>');
              While not(eof) do
              begin
                   Doc.Content.Find.Execute(FindText := '<<NOVO_VALOR>>', ReplaceWith :='VENCIMENTO: '+fieldbyname('vencimento').asstring+' VALOR: R$ '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('saldo').asstring),12,' ')+#13+'<<NOVO_VALOR>>');
                   Psoma:=PSoma+Fieldbyname('saldo').asfloat;
                   next;
              end;
         End;
         Doc.Content.Find.Execute(FindText := '<<NOVO_VALOR>>', ReplaceWith :='TOTAL '+CompletaPalavra_a_Esquerda(formata_valor(Psoma),39,' '));
         FDataModulo.IBTransaction.CommitRetaining;
         BtpesquisarClick(sender);


     Finally
         FDataModulo.IBTransaction.RollbackRetaining;
         ObjgeraTitulo.free;
     End;
end;

procedure TFrenegociaPendencias.SomaSelecionadas;
var
cont:integer;
Psoma:Currency;
begin
     Psoma:=0;
     for cont:=1 to StrgPendencias.RowCount-1 do
     begin
          if (StrgPendencias.Cells[0,cont]='X')
          then Psoma:=Psoma+strtofloat(StrgPendencias.Cells[5,cont]);
     End;
     LbtotalSelecionado.caption:=formata_valor(psoma);
end;

function TFrenegociaPendencias.AdiquiraOuCrieObjeto(const ClasseNome: String): IDispatch;
var Classifique_Id : TGUID;
begin
  Classifique_ID:=ProgIdToClassId(ClasseNome);
  Result:=CreateOleObject(ClasseNome);
end;


procedure TFrenegociaPendencias.edtSomenteNumerosKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     then Begin
               if Key='.'
               Then Key:=','
               Else key:=#0;
     End;
end;

end.
