object Fgerador: TFgerador
  Left = 157
  Top = 245
  Width = 671
  Height = 310
  Caption = 'Cadastro de Geradores de T'#237'tulos - EXCLAIM TECNOLOGIA'
  Color = clSilver
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelrodape: TPanel
    Left = 0
    Top = 222
    Width = 655
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    DesignSize = (
      655
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 655
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 357
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 826
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 826
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 655
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 535
      Top = 0
      Width = 120
      Height = 51
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Geradores'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btOpcoes: TBitBtn
      Left = 350
      Top = -1
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
    end
    object btsair: TBitBtn
      Left = 400
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object BtCancelar: TBitBtn
      Left = 150
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object Btgravar: TBitBtn
      Left = 100
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object btexcluir: TBitBtn
      Left = 200
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btrelatorios: TBitBtn
      Left = 300
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object btalterar: TBitBtn
      Left = 50
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object BtNovo: TBitBtn
      Left = 0
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btpesquisar: TBitBtn
      Left = 250
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 51
    Width = 655
    Height = 171
    Align = alClient
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 653
      Height = 169
      Align = alClient
      Stretch = True
    end
    object Label1: TLabel
      Left = 27
      Top = 22
      Width = 39
      Height = 14
      Caption = 'Codigo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 27
      Top = 45
      Width = 32
      Height = 14
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 27
      Top = 68
      Width = 35
      Height = 14
      Caption = 'Tabela'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 27
      Top = 91
      Width = 36
      Height = 14
      Caption = 'Objeto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 27
      Top = 115
      Width = 77
      Height = 14
      Caption = 'Instru'#231#227'o SQL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 27
      Top = 138
      Width = 87
      Height = 14
      Caption = 'Conta Gerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCodigo: TEdit
      Left = 121
      Top = 20
      Width = 72
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 0
    end
    object edtnome: TEdit
      Left = 121
      Top = 43
      Width = 199
      Height = 19
      Ctl3D = False
      MaxLength = 50
      ParentCtl3D = False
      TabOrder = 1
    end
    object edttabela: TEdit
      Left = 121
      Top = 66
      Width = 199
      Height = 19
      Ctl3D = False
      MaxLength = 50
      ParentCtl3D = False
      TabOrder = 2
    end
    object edtobjeto: TEdit
      Left = 121
      Top = 89
      Width = 199
      Height = 19
      Ctl3D = False
      MaxLength = 50
      ParentCtl3D = False
      TabOrder = 3
    end
    object edtinstrucaosql: TEdit
      Left = 121
      Top = 112
      Width = 199
      Height = 19
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 4
    end
    object EdtContagerencial: TEdit
      Left = 121
      Top = 135
      Width = 72
      Height = 19
      Color = 6073854
      Ctl3D = False
      MaxLength = 100
      ParentCtl3D = False
      TabOrder = 5
      OnKeyDown = EdtContagerencialKeyDown
    end
  end
end
