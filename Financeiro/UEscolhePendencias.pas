unit UEscolhePendencias;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons, ExtCtrls;

type
  TFescolhePendencias = class(TForm)
    Grid: TStringGrid;
    Cliente: TLabel;
    lbCliente: TLabel;
    btSair: TSpeedButton;
    pnlRodape: TPanel;
    Img1: TImage;
    lb1: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    procedure GridDblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtsairClick(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure lb1MouseLeave(Sender: TObject);
    procedure lb1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FescolhePendencias: TFescolhePendencias;

implementation

uses UessencialGlobal, UescolheImagemBotao;

{$R *.DFM}

procedure TFescolhePendencias.GridDblClick(Sender: TObject);
begin
     if (grid.cells[0,grid.row]='X')
     Then grid.cells[0,grid.row]:=''
     Else grid.cells[0,grid.row]:='X'
end;

procedure TFescolhePendencias.FormShow(Sender: TObject);
var
cont:integer;
begin
    // Uessencialglobal.PegaCorForm(Self);
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
          //marcando todas as pendencias
     for cont:=1 to Grid.rowcount do
     Begin
          Grid.cells[0,cont]:='X';
     End;
     PegaFiguraBotao(Btsair,'BOTAOSAIR.BMP');

end;

procedure TFescolhePendencias.BtsairClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TFescolhePendencias.GridKeyPress(Sender: TObject; var Key: Char);
begin
     If key=#13
     Then Self.GridDblClick(sender);
end;

procedure TFescolhePendencias.BitBtn1Click(Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to grid.RowCount-1 do
     grid.Cells[0,cont]:='X';
end;

procedure TFescolhePendencias.BitBtn2Click(Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to grid.RowCount-1 do
     grid.Cells[0,cont]:='';
end;

procedure TFescolhePendencias.BitBtn3Click(Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to grid.RowCount-1 do
     Begin
        If (grid.Cells[0,cont]='X')
        Then grid.Cells[0,cont]:=''
        Else grid.Cells[0,cont]:='X';  
     End
end;

procedure TFescolhePendencias.lb1MouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFescolhePendencias.lb1MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

end.
