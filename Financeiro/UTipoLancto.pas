unit UTipoLancto;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjTipoLancto;

type
  TFtipoLancto = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    Combotipo: TComboBox;
    Combogeravalor: TComboBox;
    comboclassificacao: TComboBox;
    BtNovo: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function atualizaQuantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FtipoLancto: TFtipoLancto;
  ObjTipoLancto:TObjTipoLancto;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFtipoLancto.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjTipoLancto do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_nome            ( edtnome.text);
         Submit_Tipo            ( combotipo.text[1]);
         Submit_GeraValor       ( combogeravalor.text[1]);
         Submit_Classificacao(comboclassificacao.text[1]);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFtipoLancto.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjTipoLancto do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtnome.text            :=Get_nome            ;


        If (Get_tipo='D')
        Then Combotipo.itemindex:=0
        Else If (Get_tipo='C')
             then combotipo.itemindex:=1;

        If (Get_geraValor='S')
        Then Combogeravalor.itemindex:=0
        Else If (Get_geravalor='N')
             then combogeravalor.itemindex:=1;

        If (Get_Classificacao='Q')
        Then ComboClassificacao.itemindex:=0
        Else If (Get_Classificacao='J')
             then comboClassificacao.itemindex:=1
             Else
                if (Get_Classificacao='D')
                Then comboClassificacao.itemindex:=2
                else
                    if (Get_Classificacao='T')
                    Then comboClassificacao.itemindex:=3
                    Else comboClassificacao.itemindex:=-1;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFtipoLancto.TabelaParaControles: Boolean;
begin
     ObjTipoLancto.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFtipoLancto.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjTipoLancto=Nil)
     Then exit;

    If (ObjTipoLancto.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjTipoLancto.free;
end;

procedure TFtipoLancto.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFtipoLancto.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFtipoLancto.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     ObjTipoLancto.status:=dsInsert;
     //edtcodigo.text:='0';
     edtcodigo.text:=ObjTipoLancto.get_novocodigo;

     Edtcodigo.Enabled:=False;
     edtnome.SetFocus;

end;

procedure TFtipoLancto.BtCancelarClick(Sender: TObject);
begin
     ObjTipoLancto.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFtipoLancto.BtgravarClick(Sender: TObject);
begin

     If ObjTipoLancto.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjTipoLancto.salvar=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFtipoLancto.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     EdtCodigo.enabled:=False;
     ObjTipoLancto.Status:=dsEdit;
     edtNome.setfocus;
end;

procedure TFtipoLancto.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjTipoLancto.Get_pesquisa,ObjTipoLancto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjTipoLancto.status<>dsinactive
                                  then exit;

                                  If (ObjTipoLancto.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFtipoLancto.btalterarClick(Sender: TObject);
begin
    If (ObjTipoLancto.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFtipoLancto.btexcluirClick(Sender: TObject);
begin
     If (ObjTipoLancto.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjTipoLancto.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;
     
     If (ObjTipoLancto.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFtipoLancto.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjTipoLancto:=TObjTipoLancto.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto Tipo de Professor!',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

function TFtipoLancto.atualizaQuantidade: string;
begin
      result:='Existem '+ContaRegistros('tabtipolancto','codigo')+' Tipos de lan�amento Cadastrados';
end;

end.
