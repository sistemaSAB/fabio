unit UComprovanteCheque;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db,UObjComprovanteCheque;

type
  TFcomprovanteCheque = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdtCodigo: TEdit;
    edtcheque: TEdit;
    edtpagoa: TEdit;
    edtnominador: TEdit;
    edtdescricaorecibo: TEdit;
    edtcomprovante: TEdit;
    edtprovidencias: TEdit;
    edtdata: TMaskEdit;
    edtnomerecebedor: TEdit;
    edtcpfcnpjrecebedor: TEdit;
    edtcidade: TEdit;
    memodescricaocomprovante: TMemo;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    btcancelar: TBitBtn;
    Label12: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);
    procedure memodescricaocomprovanteKeyPress(Sender: TObject;
          var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         function  atualizaQuantidade:string;
         procedure controlChange(sender: TObject);

    { Private declarations }
  public
    { Public declarations }
        { Public declarations }
        Function Criar:boolean;
        Procedure Destruir;

  end;

var
  FcomprovanteCheque: TFcomprovanteCheque;
  ObjComprovanteCheque:TObjComprovanteCheque;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UopcaoRel;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFcomprovanteCheque.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjComprovanteCheque do
    Begin

         Submit_CODIGO           (edtCODIGO.text        );
         Submit_Cheque           (edtCheque.text        );
         Submit_PagoA            (edtPagoA.text         );
         Submit_Nominador        (edtNominador.text     );
         Submit_Descricaorecibo        (edtdescricaorecibo.text     );
         Submit_Comprovante      (edtComprovante.text   );
         Submit_Providencias     (edtProvidencias.text  );
         Submit_Cidade           (edtcidade.text        );
         Submit_Data             (edtData.text          );
         Submit_NomeRecebedor    (edtNomeRecebedor.text );
         Submit_CPFCNPJRecebedor (edtCPFCNPJRecebedor.text);
         Submit_descricaocomprovante(memodescricaocomprovante.lines.text);

         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFcomprovanteCheque.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjComprovanteCheque do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtCheque.text          :=Get_Cheque          ;
        edtPagoA.text           :=Get_PagoA           ;
        edtNominador.text       :=Get_Nominador       ;
        edtDescricaorecibo.text :=Get_descricaorecibo       ;
        edtComprovante.text     :=Get_Comprovante     ;
        edtProvidencias.text    :=Get_Providencias    ;
        edtcidade.text          :=Get_Cidade          ;
        edtData.text            :=Get_Data            ;
        edtNomeRecebedor.text   :=Get_NomeRecebedor   ;
        edtCPFCNPJRecebedor.text:=Get_CPFCNPJRecebedor;
        memodescricaocomprovante.lines.text:='';
        memodescricaocomprovante.lines.text:=Get_descricaocomprovante;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFcomprovanteCheque.TabelaParaControles: Boolean;
begin
     If (ObjComprovanteCheque.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFcomprovanteCheque.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjComprovanteCheque=Nil)
     Then exit;

    If (ObjComprovanteCheque.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjComprovanteCheque.free;
end;

procedure TFcomprovanteCheque.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFcomprovanteCheque.BtnovoClick(Sender: TObject);
begin
     Messagedlg('Os Comprovantes s�o gerados no momento do pagamento com Cheque, n�o pode ser inclu�do via cadastro!',mtinformation,[mbok],0);
     exit;

     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjComprovanteCheque.Get_novocodigo;
     edtcodigo.enabled:=False;


     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjComprovanteCheque.status:=dsInsert;
     edtprovidencias.text:='Contabilizar e Arquivar';
     edtcidade.text:='Dourados';
     edtpagoa.setfocus;

end;


procedure TFcomprovanteCheque.btalterarClick(Sender: TObject);
begin
    If (ObjComprovanteCheque.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                edtcheque.enabled:=False;
                ObjComprovanteCheque.Status:=dsEdit;
                edtnominador.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
    End;


end;

procedure TFcomprovanteCheque.btgravarClick(Sender: TObject);
begin

     If ObjComprovanteCheque.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjComprovanteCheque.salvar(True)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFcomprovanteCheque.btexcluirClick(Sender: TObject);
begin
     If (ObjComprovanteCheque.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjComprovanteCheque.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjComprovanteCheque.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFcomprovanteCheque.btcancelarClick(Sender: TObject);
begin
     ObjComprovanteCheque.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFcomprovanteCheque.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFcomprovanteCheque.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjComprovanteCheque.Get_pesquisa,ObjComprovanteCheque.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjComprovanteCheque.status<>dsinactive
                                  then exit;

                                  If (ObjComprovanteCheque.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

function TFcomprovanteCheque.Criar: boolean;
begin


end;

procedure TFcomprovanteCheque.Destruir;
begin

end;

procedure TFcomprovanteCheque.btrelatoriosClick(Sender: TObject);
begin
     ObjComprovanteCheque.Imprime(edtcodigo.text);
end;

procedure TFcomprovanteCheque.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

      limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjComprovanteCheque:=TObjComprovanteCheque.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;



procedure TFcomprovanteCheque.btOpcoesClick(Sender: TObject);
begin
     With FOpcaorel do
     Begin
        RgOpcoes.Items.clear;
        RgOpcoes.Items.add('Alterar Sequ�ncia');
        ShowModal;
        if (tag=0)
        Then exit;

        case RgOpcoes.ItemIndex of
           0:     ObjComprovanteCheque.alterasequencia;
        End;
     End;
end;

function TFcomprovanteCheque.atualizaQuantidade: string;
begin
    result:='Existem '+ContaRegistros('TABCOMPROVANTECHEQUE','CODIGO')+' Comprovantes de Cheque Cadasdtrados';
end;

procedure TFcomprovanteCheque.controlChange(sender: TObject);
begin
     controlChange_focus(sender,Self);
end;

procedure TFcomprovanteCheque.FormCreate(Sender: TObject);
begin
     Screen.OnActiveControlChange:=Self.controlChange;
end;

procedure TFcomprovanteCheque.FormDestroy(Sender: TObject);
begin
     Screen.OnActiveControlChange:=nil;
end;

procedure TFcomprovanteCheque.memodescricaocomprovanteKeyPress(
  Sender: TObject; var Key: Char);
begin
     if ( (key=#13) and (Key=#11))
     Then memodescricaocomprovante.setfocus
     else if (key=#13)
     then begin
          btgravar.SetFocus;
          Key:=#0;
     end;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjComprovanteCheque.Get_PesquisaXXX,ObjComprovanteCheque.Get_TituloPesquisaXXX,XXX)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                 //Pegando o nome, no caso de ter uma label para isso
                                 
                                 If TEdit(Sender).text<>''
                                 Then Self.Lb.caption:=ObjComprovanteCheque.Get_nomeXXX(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;
}
