unit UacertaObservacaoPendencia;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons, ExtCtrls;

type
  TFAcertaObservacaoPendencia = class(TForm)
    Grid: TStringGrid;
    pnl3: TPanel;
    btCancelar: TSpeedButton;
    lb1: TLabel;
    pnlRodape: TPanel;
    Img1: TImage;
    lbdata: TLabel;
    pnl1: TPanel;
    lb2: TLabel;
    lb3: TLabel;
    lbcredordevedor: TLabel;
    lbnumtitulo: TLabel;
    lbvalortitulo: TLabel;
    btSair: TSpeedButton;
    lb4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btBtsairClick(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure btBtcancelarClick(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure lb4MouseLeave(Sender: TObject);
    procedure lb4MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAcertaObservacaoPendencia: TFAcertaObservacaoPendencia;

implementation

uses UessencialGlobal;

{$R *.DFM}

procedure TFAcertaObservacaoPendencia.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     Self.Tag:=0;
     Self.Grid.SetFocus;
     Self.Grid.Col:=5;
     Self.Grid.row:=1;

     lbdata.Caption:= DiaSemana(Now);
     lbdata.Caption:=lbdata.Caption+', '+DateToStr(now);

end;

procedure TFAcertaObservacaoPendencia.btBtsairClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;

end;

procedure TFAcertaObservacaoPendencia.GridKeyPress(Sender: TObject;
  var Key: Char);
begin
     if self.grid.Col<>5
     Then key:=#0;
end;

procedure TFAcertaObservacaoPendencia.btBtcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.Close;
end;

procedure TFAcertaObservacaoPendencia.bt1Click(Sender: TObject);
var
Cont:Integer;
temp:string;
begin
     //este procedimento copia da linha atual para as demais a observação digitada

     if (Grid.Row<=1)
     Then Grid.Row:=1;

     temp:=Grid.Cells[5,Grid.Row];

     for cont:=Grid.Row+1 to Grid.RowCount-1 do
     Begin
          Grid.Cells[5,cont]:=temp;
     End;
end;

procedure TFAcertaObservacaoPendencia.lb4MouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFAcertaObservacaoPendencia.lb4MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

end.
