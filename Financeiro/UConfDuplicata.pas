unit UConfDuplicata;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UobjConfRelatorio,StdCtrls, Qrctrls, QuickRpt, Buttons, Mask, ExtCtrls,rdprint;

type
  TFConfDuplicata = class(TForm)
    ValorFatura: TLabel;
    NumeroFatura: TLabel;
    ValorDuplicata: TLabel;
    numeroduplicata: TLabel;
    vencimento: TLabel;
    nome: TLabel;
    endereco: TLabel;
    estado: TLabel;
    cidade: TLabel;
    cnpj: TLabel;
    ie: TLabel;
    valorextenso1: TLabel;
    valorextenso2: TLabel;
    dataemissao: TLabel;
    dataaceite: TLabel;
    Panel1: TPanel;
    LBTOP: TLabel;
    LBLEFT: TLabel;
    LBNOMECOMPONENTE: TLabel;
    edttop: TMaskEdit;
    edtleft: TMaskEdit;
    botaogravar: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    pracapagamento: TLabel;
    Descontode: TLabel;
    CondicoesEspeciais: TLabel;
    ate: TLabel;
    cep: TLabel;
    notafiscal: TLabel;
    procedure ValorFaturaClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtleftExit(Sender: TObject);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botaogravarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConfDuplicata: TFConfDuplicata;
  ObjConfiguraRel:TObjConfRelatorio;
const
FATORLINHA=15;
FATORCOLUNA=10;
MAXIMOCOLUNA=80;
MAXIMOLINHA=36;

implementation

uses  UimpDuplicata, UrelBoleto_PI, URDPRINT, UessencialGlobal, UDataModulo;

{$R *.DFM}

procedure TFConfDuplicata.ValorFaturaClick(Sender: TObject);
begin

     edttop.text:=floattostr(int(Tlabel(sender).top/FATORLINHA));
     edtleft.text:=floattostr(int(Tlabel(sender).left/FATORCOLUNA));


     LbNomeComponente.caption:=Tlabel(sender).name;
     edttop.SetFocus;
end;

procedure TFConfDuplicata.BitBtn3Click(Sender: TObject);
begin
     sELF.CLOSE;
end;

procedure TFConfDuplicata.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
keyc:char;
begin
     keyc:=#13;
     if key=vk_left
     Then begin
                edtleft.text:=floattostr(strtoint(edtleft.text)-1);
                self.edtleftKeyPress(Sender,keyc);
     End
     Else
        if key=vk_right
        Then Begin
                edtleft.text:=floattostr(strtoint(edtleft.text)+1);
                self.edtleftKeyPress(Sender,keyc);
        End
        Else
            if key=vk_up
            Then Begin
                edttop.text:=inttostr(strtoint(edttop.text)-1);
                self.edtleftKeyPress(Sender,keyc);
            End
            Else
                if key=vk_down
                Then Begin
                        edttop.text:=inttostr(strtoint(edttop.text)+1);
                        self.edtleftKeyPress(Sender,keyc);
                End;
End;

procedure TFConfDuplicata.edtleftExit(Sender: TObject);
var
numerico:integer;
begin
     Try
        numerico:=Strtoint(edtleft.text);
        if (numerico<0)
        Then edtleft.text:='0'
        Else Begin
                If (Numerico>MAXIMOCOLUNA)
                Then edtleft.text:=inttostr(MAXIMOCOLUNA);
        End;
        
        numerico:=Strtoint(edttop.text);
        if (numerico<0)
        Then edttop.text:='0'
        Else Begin
                  If (Numerico>MAXIMOLINHA)
                  Then edttop.text:=inttostr(MAXIMOLINHA);
        End;
     Except
           exit;
     End;


     TLabel(Self.FindComponent(LBNOMECOMPONENTE.caption)).left:=Strtoint(Edtleft.text)*FATORCOLUNA;
     TLabel(Self.FindComponent(LBNOMECOMPONENTE.caption)).Top:=Strtoint(EdtTop.text)*FATORLINHA;

end;

procedure TFConfDuplicata.edtleftKeyPress(Sender: TObject; var Key: Char);
var
numerico:integer;
begin
     if (not (key in ['0'..'9',#8,#13]))
     Then key:=#0;

     if key=#13
     Then edtleftExit(sender);
end;

procedure TFConfDuplicata.FormShow(Sender: TObject);
VAR
INT_HABILITA:INTEGER;
saida:boolean;
begin
     Uessencialglobal.PegaCorForm(Self);     If (Tag=1)
     Then exit;
     edtleft.text:='0';
     edttop.text:='0';

     Try
        ObjConfiguraRel:=TObjConfRelatorio.create;
     Except
           Messagedlg('N�o � poss�vel Configurar a Impress�o Devido a um Problema na Cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0);
           exit;
     End;

     //resgatando as configura��es atuais do quickreport
    ObjconfiguraRel.RecuperaArquivoRelatorio(FImpDuplicata,'IMPRIMEDUPLICATA01');

    for int_habilita:=0 to FImpDuplicata.ComponentCount -1 do
    Begin
        if FImpDuplicata.Components [int_habilita].ClassName = 'TQRLabel'
        then Begin
                Tlabel(Self.FindComponent(FImpDuplicata.Components [int_habilita].Name)).Left:=TQrlabel(FImpDuplicata.Components [int_habilita]).left;
                Tlabel(Self.FindComponent(FImpDuplicata.Components [int_habilita].Name)).Top:=TQrlabel(FImpDuplicata.Components [int_habilita]).Top;
        End
        Else Begin
                  if FImpDuplicata.Components [int_habilita].ClassName = 'TQRMemo'
                  then Begin
                        Tmemo(Self.FindComponent(FImpDuplicata.Components [int_habilita].Name)).Left:=TQrmemo(FImpDuplicata.Components [int_habilita]).left;
                        Tmemo(Self.FindComponent(FImpDuplicata.Components [int_habilita].Name)).Top:=TQrmemo(FImpDuplicata.Components [int_habilita]).Top;
                  End
        End;
   End;
   tag:=1;
   ValorFaturaClick(ValorFatura);
end;

procedure TFConfDuplicata.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (ObjConfiguraRel<>nil)
     Then ObjConfiguraRel.free;
     tag:=0;
end;

procedure TFConfDuplicata.botaogravarClick(Sender: TObject);
var
int_habilita:Integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1 do
   Begin
        if ((Self.Components [int_habilita].ClassName = 'TLabel')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBLEFT')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBTOP')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBNOMECOMPONENTE'))
        Then Begin
                TQRlabel(FImpDuplicata.FindComponent(Self.Components [int_habilita].Name)).Left:=Tlabel(Self.Components [int_habilita]).left;
                TQRlabel(FImpDuplicata.FindComponent(Self.Components [int_habilita].Name)).Top:=TLabel(Self.Components [int_habilita]).Top;
             End;
        If (Self.Components[int_habilita].ClassName='TMemo')
        Then Begin
                TQRmemo(FImpDuplicata.FindComponent(Self.Components [int_habilita].Name)).Left:=Tlabel(Self.Components [int_habilita]).left;
                TQRmemo(FImpDuplicata.FindComponent(Self.Components [int_habilita].Name)).Top:=TLabel(Self.Components [int_habilita]).Top;
        End;
   End;

     //GRAVANDO OS DADOS ATUAIS NO QUICKREPORT
     IF (ObjconfiguraRel.NovoArquivoRelatorio(FImpDuplicata,'IMPRIMEDUPLICATA01')=False)
     Then Begin
               Messagedlg('N�o foi poss�vel gravar o arquivo de configura��o!',mterror,[mbok],0);
               exit;
          End;
     Messagedlg('Arquivo Gravado com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFConfDuplicata.BitBtn2Click(Sender: TObject);
var
extenso1,extenso2:string;
cont:integer;
begin


     //resgatando as configuracoes
     ObjConfiguraRelGlobal.RecuperaArquivoRelatorio(FImpDuplicata,'IMPRIMEDUPLICATA01');

     With FRDPRINT.RDprint do
     Begin
          FRDPRINT.RDprint.TamanhoQteLinhas:=36;
          FRDPRINT.RDprint.TamanhoQteColunas:=80;
          FRDPRINT.RDprint.UsaGerenciadorImpr:=True;
          FRDPRINT.RDprint.TamanhoQteLPP:=Seis;
          FRDPRINT.RDprint.OpcoesPreview.Preview:=true;
          FRDPRINT.RDprint.abrir;           //dataemissao
          

           imp(strtoint(floattostr(int(FImpDuplicata.dataemissao.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataemissao.left/10))),datetostr(now));
           //dataaceite
           imp(strtoint(floattostr(int(FImpDuplicata.dataaceite.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataaceite.left/10))),datetostr(now));
           //ValorFatura
           impf(strtoint(floattostr(int(FImpDuplicata.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorFatura.left/10))),'1.000,00',[comp12]);
           //NumeroFatura,  uso o NUmerodaPendencia|NUmDcto do Titulo, que geralmente sera na venda Pedido/NF, e em outros casos depende do usuario que criou
           imp(strtoint(floattostr(int(FImpDuplicata.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.NumeroFatura.left/10))),'NUMERO');
           //ValorDuplicata=ValorFatura, usado o Saldo
           impf(strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.left/10))),'1.000,00',[comp12]);
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.numeroduplicata.left/10))),'N� DUP');
           //vencimento
           imp(strtoint(floattostr(int(FImpDuplicata.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata.vencimento.left/10))),'01/01/1500');
           //DESCONTEDE
           imp(strtoint(floattostr(int(FImpDuplicata.descontode.Top/15))),strtoint(floattostr(int(FImpDuplicata.descontode.left/10))),'desconto de');
           //ate
           imp(strtoint(floattostr(int(FImpDuplicata.ate.Top/15))),strtoint(floattostr(int(FImpDuplicata.ate.left/10))),'ate');
           //ate
           imp(strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.Top/15))),strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.left/10))),'condicoes ');
           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata.nome.left/10))),'nome');
           //Endereco do Sacado
           imp(strtoint(floattostr(int(FImpDuplicata.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata.endereco.left/10))),'endereco');
           //Cidade
           imp(strtoint(floattostr(int(FImpDuplicata.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata.cidade.left/10))),'cidade');
           //Cep
           imp(strtoint(floattostr(int(FImpDuplicata.cep.Top/15))),strtoint(floattostr(int(FImpDuplicata.cep.left/10))),'cep');
           //estado
           imp(strtoint(floattostr(int(FImpDuplicata.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata.estado.left/10))),'estado');
           //pracapagaento
           imp(strtoint(floattostr(int(FImpDuplicata.pracapagamento.Top/15))),strtoint(floattostr(int(FImpDuplicata.pracapagamento.left/10))),'pra�a');
           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata.cnpj.left/10))),'cnpj');
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata.ie.left/10))),'ie');
           //valorextenso
           ValorExtenso_DivididoDois(998999.99,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso2.left/10))),extenso2);
           NOVAPAGINA;
           //dataemissao
           imp(strtoint(floattostr(int(FImpDuplicata.dataemissao.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataemissao.left/10))),datetostr(now));
           //dataaceite
           imp(strtoint(floattostr(int(FImpDuplicata.dataaceite.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataaceite.left/10))),datetostr(now));
           //ValorFatura
           imp(strtoint(floattostr(int(FImpDuplicata.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorFatura.left/10))),'1.000,00');
           //NumeroFatura,  uso o NUmerodaPendencia|NUmDcto do Titulo, que geralmente sera na venda Pedido/NF, e em outros casos depende do usuario que criou
           imp(strtoint(floattostr(int(FImpDuplicata.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.NumeroFatura.left/10))),'NUMERO FATURA');
           //ValorDuplicata=ValorFatura, usado o Saldo
           imp(strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.left/10))),'1.000,00');
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.numeroduplicata.left/10))),'N� DUP');
           //vencimento
           imp(strtoint(floattostr(int(FImpDuplicata.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata.vencimento.left/10))),'01/01/1500');
           //DESCONTEDE
           imp(strtoint(floattostr(int(FImpDuplicata.descontode.Top/15))),strtoint(floattostr(int(FImpDuplicata.descontode.left/10))),'desconto de');
           //ate
           imp(strtoint(floattostr(int(FImpDuplicata.ate.Top/15))),strtoint(floattostr(int(FImpDuplicata.ate.left/10))),'ate');
           //ate
           imp(strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.Top/15))),strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.left/10))),'condicoes ');
           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata.nome.left/10))),'nome');
           //Endereco do Sacado
           imp(strtoint(floattostr(int(FImpDuplicata.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata.endereco.left/10))),'endereco');
           //Cidade
           imp(strtoint(floattostr(int(FImpDuplicata.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata.cidade.left/10))),'cidade');
           //Cep
           imp(strtoint(floattostr(int(FImpDuplicata.cep.Top/15))),strtoint(floattostr(int(FImpDuplicata.cep.left/10))),'cep');
           //estado
           imp(strtoint(floattostr(int(FImpDuplicata.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata.estado.left/10))),'estado');
           //pracapagaento
           imp(strtoint(floattostr(int(FImpDuplicata.pracapagamento.Top/15))),strtoint(floattostr(int(FImpDuplicata.pracapagamento.left/10))),'pra�a');
           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata.cnpj.left/10))),'cnpj');
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata.ie.left/10))),'ie');
           //valorextenso
           ValorExtenso_DivididoDois(99999.99,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso2.left/10))),extenso2);

           NOVAPAGINA;
           //dataemissao
           imp(strtoint(floattostr(int(FImpDuplicata.dataemissao.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataemissao.left/10))),datetostr(now));
           //dataaceite
           imp(strtoint(floattostr(int(FImpDuplicata.dataaceite.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataaceite.left/10))),datetostr(now));
           //ValorFatura
           imp(strtoint(floattostr(int(FImpDuplicata.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorFatura.left/10))),'1.000,00');
           //NumeroFatura,  uso o NUmerodaPendencia|NUmDcto do Titulo, que geralmente sera na venda Pedido/NF, e em outros casos depende do usuario que criou
           imp(strtoint(floattostr(int(FImpDuplicata.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.NumeroFatura.left/10))),'NUMERO FATURA');
           //ValorDuplicata=ValorFatura, usado o Saldo
           imp(strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.left/10))),'1.000,00');
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.numeroduplicata.left/10))),'N� DUP');
           //vencimento
           imp(strtoint(floattostr(int(FImpDuplicata.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata.vencimento.left/10))),'01/01/1500');
           //DESCONTEDE
           imp(strtoint(floattostr(int(FImpDuplicata.descontode.Top/15))),strtoint(floattostr(int(FImpDuplicata.descontode.left/10))),'desconto de');
           //ate
           imp(strtoint(floattostr(int(FImpDuplicata.ate.Top/15))),strtoint(floattostr(int(FImpDuplicata.ate.left/10))),'ate');
           //ate
           imp(strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.Top/15))),strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.left/10))),'condicoes ');
           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata.nome.left/10))),'nome');
           //Endereco do Sacado
           imp(strtoint(floattostr(int(FImpDuplicata.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata.endereco.left/10))),'endereco');
           //Cidade
           imp(strtoint(floattostr(int(FImpDuplicata.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata.cidade.left/10))),'cidade');
           //Cep
           imp(strtoint(floattostr(int(FImpDuplicata.cep.Top/15))),strtoint(floattostr(int(FImpDuplicata.cep.left/10))),'cep');
           //estado
           imp(strtoint(floattostr(int(FImpDuplicata.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata.estado.left/10))),'estado');
           //pracapagaento
           imp(strtoint(floattostr(int(FImpDuplicata.pracapagamento.Top/15))),strtoint(floattostr(int(FImpDuplicata.pracapagamento.left/10))),'pra�a');
           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata.cnpj.left/10))),'cnpj');
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata.ie.left/10))),'ie');
           //valorextenso
           ValorExtenso_DivididoDois(99999.99,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso2.left/10))),extenso2);

           fechar;
     End;

end;

procedure TFConfDuplicata.FormDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
     Accept:=true;
end;

procedure TFConfDuplicata.FormDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     Panel1.left:=X;
     Panel1.top:=y;

end;

end.
