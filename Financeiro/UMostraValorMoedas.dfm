object FMostraValorMoedas: TFMostraValorMoedas
  Left = 342
  Top = 174
  Width = 463
  Height = 328
  BorderIcons = [biSystemMenu]
  Caption = 'Valor em outras Moedas'
  Color = clWhite
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StrGrid: TStringGrid
    Left = 0
    Top = 137
    Width = 447
    Height = 153
    Align = alClient
    Color = clInfoBk
    ColCount = 3
    FixedCols = 0
    RowCount = 2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ColWidths = (
      2
      206
      206)
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 447
    Height = 137
    Align = alTop
    Caption = 'Convers'#227'o de valores'
    TabOrder = 1
    object Label2: TLabel
      Left = 167
      Top = 17
      Width = 111
      Height = 16
      Caption = 'Escolha a moeda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 5
      Top = 17
      Width = 100
      Height = 16
      Caption = 'Informe o valor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Bevel1: TBevel
      Left = 166
      Top = 34
      Width = 263
      Height = 63
      Shape = bsFrame
      Style = bsRaised
    end
    object Bevel2: TBevel
      Left = 3
      Top = 34
      Width = 155
      Height = 63
      Shape = bsFrame
      Style = bsRaised
    end
    object lbFrase1: TLabel
      Left = 167
      Top = 99
      Width = 199
      Height = 13
      Caption = 'Moedas que n'#227'o permitem sele'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object lbFrase2: TLabel
      Left = 167
      Top = 111
      Width = 261
      Height = 13
      Caption = 'necessita-se informar ao menos uma cota'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object EdtValor: TEdit
      Left = 7
      Top = 50
      Width = 148
      Height = 29
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnExit = EdtValorExit
      OnKeyPress = EdtValorKeyPress
    end
  end
end
