unit UObjHistoricoSimples;
Interface
Uses Db,UessencialGlobal,Ibcustomdataset,IBStoredProc;

Type
   TObjHistoricoSimples=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar                          :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string)            :Boolean;
                Function    Get_Pesquisa                         :string;
                Function    Get_TituloPesquisa                         :string;
                Procedure   TabelaparaObjeto                        ;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Nome        :string;
                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Nome        (parametro:string);
                Procedure Imprime;
                Function Get_NOVOCODIGO           :string;

         Private
               ObjDataset:Tibdataset;

               CODIGO           :String[09];
               Nome        :String[250];


                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ImprimeHistorico;
   End;


implementation
uses Classes,SysUtils,Dialogs,UDatamodulo,Controls,
urelatorio, UMenuRelatorios;


{ TTabTitulo }


Procedure  TObjHistoricoSimples.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.Nome        :=FieldByName('Nome').asstring;
     End;
end;


Procedure TObjHistoricoSimples.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring           :=        Self.CODIGO                    ;
      FieldByName('Nome').asstring        :=        Self.Nome                 ;
  End;
End;

//***********************************************************************

function TObjHistoricoSimples.Salvar: Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;
 FDataModulo.IBTransaction.CommitRetaining;
 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjHistoricoSimples.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Nome        :='';
     End;
end;

Function TObjHistoricoSimples.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (Nome='')
       Then Mensagem:=mensagem+'/Descri��o';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjHistoricoSimples.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Curso n�o Encontrado!';}

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjHistoricoSimples.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjHistoricoSimples.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjHistoricoSimples.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjHistoricoSimples.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,Nome from tabHistoricoSimples where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjHistoricoSimples.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjHistoricoSimples.Exclui(Pcodigo: string): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 FDataModulo.IBTransaction.CommitRetaining;
             End
                 
        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjHistoricoSimples.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,Nome from tabHistoricoSimples where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert into TabHistoricoSimples (codigo,Nome) values (:codigo,:Nome) ');

                ModifySQL.clear;
                ModifySQL.add(' Update TabHistoricoSimples set codigo=:codigo,Nome=:Nome where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add(' Delete from TabHistoricoSimples where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,Nome from tabHistoricoSimples where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjHistoricoSimples.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjHistoricoSimples.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjHistoricoSimples.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjHistoricoSimples.Get_Pesquisa: string;
begin
     Result:=' Select * from TabHistoricoSimples ';
end;

function TObjHistoricoSimples.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Hist�rico Simples ';
end;

function TObjHistoricoSimples.Get_Nome: string;
begin
     Result:=Self.Nome;
end;

procedure TObjHistoricoSimples.Submit_Nome(parametro: string);
begin
     Self.Nome:=Parametro;
end;

destructor TObjHistoricoSimples.Free;
begin
    Freeandnil(Self.ObjDataset);
end;

procedure TObjHistoricoSimples.Imprime;
begin
     With FMenuRelatorios  do
     Begin
          NomeObjeto:='UOBJHISTORICOSIMPLES';
          With RgOpcoes do
          Begin
               Items.clear;
               Items.add('Relat�rio de Hist�ricos');
          End;
          ShowModal;
          If (tag=0)
          Then exit;
          Case RgOpcoes.ItemIndex of
          0:Self.ImprimeHistorico;
          End;
     End;



end;

procedure TObjHistoricoSimples.ImprimeHistorico;
begin
     FRelatorio.QR.DataSet:=Self.ObjDataset;
     Self.ObjDataset.Close;
     Self.ObjDataset.SelectSQL.Clear;
     Self.ObjDataset.SelectSQL.add('select * from Tabhistoricosimples');
     Self.ObjDataset.open;
     If (Self.ObjDataset.RecordCount=0)
     Then Begin
               MessageDlg('N�o h� hist�ricos cadastrados!',mterror,[mbok],0);
               exit;

     End;

     With Frelatorio do
     Begin
          Frelatorio.desativacampos;
          Frelatorio.titulo.caption:='RELAT�RIO DE HIST�RICOS CONT�BEIS';

          Campo01.Enabled:=True;
          ColTitulo01.Enabled:=True;
          ColTitulo01.Caption:='C�DIGO';
          Campo01.DataSet:=Self.ObjDataset;
          Campo01.DataField:='CODIGO';

          ColTitulo02.Enabled:=True;
          ColTitulo02.Caption:='NOME';
          Campo02.Enabled:=True;
          Campo02.DataSet:=Self.ObjDataset;
          Campo02.DataField:='NOME';

          LbCampoExpr01.enabled:=True;
          LbCampoExpr01.Left:=01;
          LbCampoExpr01.Top:=08;
          LbCampoExpr01.Caption:='TOTAL DE REGISTROS-> ';
          CampoExpr01.Expression:='count';
          CampoExpr01.Enabled:=True;
          CampoExpr01.Left:=200;
          CampoExpr01.Top:=08;
          qr.preview;
    End;




end;

function TObjHistoricoSimples.Get_NOVOCODIGO: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_HISTORICOSIMPLES' ;
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Filiados',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;

end.
