object FCOTACAOMOEDA: TFCOTACAOMOEDA
  Left = 360
  Top = 219
  Width = 533
  Height = 323
  Caption = 'Cadastro de COTACAOMOEDA - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    517
    285)
  PixelsPerInch = 96
  TextHeight = 13
  object ImagemFundo: TImage
    Left = 0
    Top = 50
    Width = 517
    Height = 235
    Align = alClient
    Stretch = True
  end
  object lbNomeMoeda1: TLabel
    Left = 236
    Top = 168
    Width = 4
    Height = 13
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbnomeMoeda2: TLabel
    Left = 236
    Top = 190
    Width = 4
    Height = 13
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 15
    Top = 72
    Width = 96
    Height = 13
    Caption = 'Modea padr'#227'o:'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbNomeMoedaPadrao: TLabel
    Left = 117
    Top = 68
    Width = 49
    Height = 18
    Caption = 'DATA'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbData: TLabel
    Left = 397
    Top = 66
    Width = 110
    Height = 18
    Alignment = taRightJustify
    Anchors = [akLeft, akTop, akRight]
    Caption = '01/01/2009'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbDataHoje: TLabel
    Left = 397
    Top = 265
    Width = 110
    Height = 18
    Alignment = taRightJustify
    Anchors = [akLeft, akTop, akRight]
    Caption = '01/01/2009'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 350
    Top = 267
    Width = 30
    Height = 13
    Caption = 'Hoje'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 282
    Top = 69
    Width = 98
    Height = 13
    Caption = 'Ultima Cota'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 517
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object btpesquisar: TBitBtn
      Left = 101
      Top = -1
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btpesquisarClick
    end
    object btexcluir: TBitBtn
      Left = 51
      Top = -1
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
    object btgravar: TBitBtn
      Left = 2
      Top = -1
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btgravarClick
    end
    object btsair: TBitBtn
      Left = 151
      Top = -1
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btsairClick
    end
  end
  object StrGrid: TStringGrid
    Left = 10
    Top = 87
    Width = 497
    Height = 176
    ColCount = 3
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goColMoving]
    TabOrder = 1
    OnSelectCell = StrGridSelectCell
  end
end
