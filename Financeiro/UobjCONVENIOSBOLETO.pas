unit UobjCONVENIOSBOLETO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,Uobjportador;

Type
   TObjCONVENIOSBOLETO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Portador:TobjPortador;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaCodigoBanco(Parametro:string) :boolean;
                Function    LocalizaConvenio(PNumeroConvenio:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function    TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                Procedure Submit_NomeBanco(parametro: string);
                Function Get_NomeBanco: string;
                Procedure Submit_CodigoBAnco(parametro: string);
                Function Get_CodigoBAnco: string;
                Procedure Submit_DgBanco(parametro: string);
                Function Get_DgBanco: string;
                Procedure Submit_LocalPagamento(parametro: string);
                Function Get_LocalPagamento: string;

                Procedure Submit_Nomecedente(parametro: string);
                Function Get_Nomecedente: string;
                Procedure Submit_Carteira(parametro: string);
                Function Get_Carteira: string;
                Procedure Submit_CompCarteira(parametro: string);
                Function Get_CompCarteira: string;
                Procedure Submit_Agencia(parametro: string);
                Function Get_Agencia: string;
                Procedure Submit_DGAgencia(parametro: string);
                Function Get_DGAgencia: string;
                Procedure Submit_ContaCorrente(parametro: string);
                Function Get_ContaCorrente: string;
                Procedure Submit_DgContaCorrente(parametro: string);
                Function Get_DgContaCorrente: string;
                Procedure Submit_numeroconvenio(parametro: string);
                Function Get_numeroconvenio: string;
                Procedure Submit_EspecieDoc(parametro: string);
                Function Get_EspecieDoc: string;
                Procedure Submit_Aceite(parametro: string);
                Function Get_Aceite: string;
                Procedure Submit_NumContaResponsavel(parametro: string);
                Function Get_NumContaResponsavel: string;
                Procedure Submit_EspecieMoeda(parametro: string);
                Function Get_EspecieMoeda: string;
                Procedure Submit_QuantidadeMoeda(parametro: string);
                Function Get_QuantidadeMoeda: string;
                Procedure Submit_ValorMoeda(parametro: string);
                Function Get_ValorMoeda: string;
                Procedure Submit_Instrucoes(parametro: String);
                Function Get_Instrucoes: String;
                Procedure Submit_sacado(parametro: String);
                Function Get_sacado: String;
                Function  Get_PreImpresso:string;
                Procedure Submit_PreImpresso(parametro:string);
                Function get_codigocedente:String;
                Function get_QtdeDigitosNossoNumero:string;
                Procedure Submit_codigocedente(parametro:String);
                Procedure Submit_QtdeDigitosNossoNumero(parametro:string);
                Function Get_taxabanco:string;
                Procedure Submit_taxabanco(parametro:string);
                Function Get_taxamensal:string;
                Procedure Submit_taxamensal(parametro:string);
                Procedure Submit_Quantdiasprotesto(parametro:string);
                function Get_Quantdiasprotesto: string;

                Function Get_CodigoSICOB_caixa:string;
                Procedure Submit_CodigoSICOB_caixa(parametro:string);

                Function Get_codigotransacao_unibanco:string;
                Function Get_posicoes_28_a_29_UNIBANCO:string;
                Function Get_numerocliente_UNIBANCO:string;

                Procedure Submit_codigotransacao_unibanco(parametro:string);
                Procedure Submit_posicoes_28_a_29_UNIBANCO(parametro:string);
                Procedure Submit_numerocliente_UNIBANCO(parametro:string);


                Function Get_CodigoCarteira_Caixa:string;
                Procedure Submit_CodigoCarteira_Caixa(parametro:string);

                Function get_posicao20_sicred:string;
                Function get_posicao21_sicred:string;
                Function get_posicao31_34_sicred:string;
                Function get_posicao35_36_sicred:string;
                Function get_posicao37_41_sicred:string;
                Function get_posicao42_sicred:string;
                Function get_posicao43_sicred:string;

                Procedure Submit_posicao20_sicred(parametro:string);
                Procedure Submit_posicao21_sicred(parametro:string);
                Procedure Submit_posicao31_34_sicred(parametro:string);
                Procedure Submit_posicao35_36_sicred(parametro:string);
                Procedure Submit_posicao37_41_sicred(parametro:string);
                Procedure Submit_posicao42_sicred(parametro:string);
                Procedure Submit_posicao43_sicred(parametro:string);

                Function get_tipoidentificador_HSBC : string;
                Function get_codigocedente_HSBC : string;
                Function get_codigoCNR_HSBC : string;

                procedure Submit_tipoidentificador_HSBC(parametro : string);
                procedure Submit_codigocedente_HSBC(parametro : string);
                procedure Submit_codigoCNR_HSBC(parametro : string);

                Function  Pegaconvenio:boolean;
                procedure edtPortadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LbNome: TLabel);
                procedure edtPortadorExit(Sender: TObject; LbNome: TLabel);

                Function Get_BYTE_NOSSONUMERO_SICRED:string;
                Procedure  Submit_BYTE_NOSSONUMERO_SICRED(parametro:string);

                Function Get_CobreBemLayoutBoleto:String;
                Procedure Submit_CobreBemLayoutBoleto(Parametro:String);

                Function Get_CobreBem                       :String;
                Function Get_CobreBemArquivoLicenca         :String;
                Function Get_CobreBemCodigoAgencia          :String;
                Function Get_CobreBemNumeroContaCorrente    :String;
                Function Get_CobreBemCodigoCedente          :String;
                Function Get_CobreBemInicioNossoNumero      :String;
                Function Get_CobreBemFimNossoNumero         :String;
                Function Get_CobreBemProximoNossoNumero     :String;
                Function Get_CobreBemOutroDadoConfiguracao1 :String;
                Function Get_CobreBemOutroDadoConfiguracao2 :String;
                Function Get_CobreBemArquivoLogotipo        :String;
                Function Get_CobreBemCaminhoImagensCodBarras:String;
                Function Get_CobreBemDiretorioRemessa       :String;
                Function Get_CobreBemArquivoRemessa         :String;
                Function Get_CobreBemLayoutRemessa          :String;
                Function Get_CobreBemDiretorioRetorno       :String;
                Function Get_CobreBemArquivoRetorno         :String;
                Function Get_CobreBemLayoutRetorno          :String;

                Procedure Submit_CobreBem(Parametro:String);
                Procedure Submit_CobreBemArquivoLicenca(Parametro:String);
                Procedure Submit_CobreBemCodigoAgencia(Parametro:String);
                Procedure Submit_CobreBemNumeroContaCorrente(Parametro:String);
                Procedure Submit_CobreBemCodigoCedente(Parametro:String);
                Procedure Submit_CobreBemInicioNossoNumero(Parametro:String);
                Procedure Submit_CobreBemFimNossoNumero(Parametro:String);
                Procedure Submit_CobreBemProximoNossoNumero(Parametro:String);
                Procedure Submit_CobreBemOutroDadoConfiguracao1(Parametro:String);
                Procedure Submit_CobreBemOutroDadoConfiguracao2(Parametro:String);
                Procedure Submit_CobreBemArquivoLogotipo(Parametro:String);
                Procedure Submit_CobreBemCaminhoImagensCodBarras(Parametro:String);
                Procedure Submit_CobreBemDiretorioRemessa(Parametro:String);
                Procedure Submit_CobreBemArquivoRemessa(Parametro:String);
                Procedure Submit_CobreBemLayoutRemessa(Parametro:String);
                Procedure Submit_CobreBemDiretorioRetorno(Parametro:String);
                Procedure Submit_CobreBemArquivoRetorno(Parametro:String);
                Procedure Submit_CobreBemLayoutRetorno(Parametro:String);

                Function Get_CobreBemPercJurosDiaAtraso:String;
                Procedure Submit_CobreBemPercJurosDiaAtraso(Parametro:String);
                Function Get_CobreBemPercMultaAtraso:String;
                Procedure Submit_CobreBemPercMultaAtraso(Parametro:String);
                Function Get_CobreBemPercentualDesconto:String;
                Procedure Submit_CobreBemPercentualDesconto(parametro:String);
                Function Get_CobreBemBancoImprimeBoleto:String;
                Procedure Submit_CobreBemBancoImprimeBoleto(Parametro:String);
                Function Get_CobreBemDiasProtesto:String;
                Procedure Submit_CobreBemDiasProtesto(Parametro:String);
                Function  Get_CobreBemLimiteVencimento:String;
                Procedure Submit_CobreBemLimiteVencimento(Parametro:String);
                Function Get_CobreBemNumeroRemessa:String;
                Procedure Submit_CobreBemNumeroRemessa(Parametro:String);

                Procedure Opcoes(Pcodigo:string);

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Nome:string;
               NomeBanco:string;
               CodigoBAnco:string;
               DgBanco:string;
               LocalPagamento:string;
               Nomecedente:string;
               Carteira:string;
               CompCarteira:string;
               Agencia:string;
               DGAgencia:string;
               ContaCorrente:string;
               DgContaCorrente:string;
               numeroconvenio:string;
               EspecieDoc:string;
               Aceite:string;
               NumContaResponsavel:string;
               EspecieMoeda:string;
               QuantidadeMoeda:string;
               ValorMoeda:string;
               taxamensal:string;
               Quantdiasprotesto:string;
               taxabanco:string;
               codigocedente:String;
               QtdeDigitosNossoNumero:string;
               PreImpresso:string;

               Instrucoes:String;
               Sacado    :String;

               //CAIXA ECONOMICA
               CodigoSICOB_caixa:string;
               CodigoCarteira_Caixa:string;
               //****************

               //UNIBANCO*********************
               codigotransacao_unibanco:string;
               posicoes_28_a_29_UNIBANCO:string;
               numerocliente_UNIBANCO:string;
               //*****************************

               //****sicred****
               posicao20_sicred:string;
               posicao21_sicred:string;
               posicao31_34_sicred:string;
               posicao35_36_sicred:string;
               posicao37_41_sicred:string;
               posicao42_sicred:string;
               posicao43_sicred:string;
               BYTE_NOSSONUMERO_SICRED:string;
               //*****************

               //*****HSBC**************
               tipoidentificador_HSBC : string;
               codigocedente_HSBC : string;
               codigoCNR_HSBC : string;

               //******CobreBem*********
               CobreBem                       :String;
               CobreBemArquivoLicenca         :String;
               CobreBemCodigoAgencia          :String;
               CobreBemNumeroContaCorrente    :String;
               CobreBemCodigoCedente          :String;
               CobreBemInicioNossoNumero      :String;
               CobreBemFimNossoNumero         :String;
               CobreBemProximoNossoNumero     :String;
               CobreBemOutroDadoConfiguracao1 :String;
               CobreBemOutroDadoConfiguracao2 :String;
               CobreBemArquivoLogotipo        :String;
               CobreBemCaminhoImagensCodBarras:String;
               CobreBemDiretorioRemessa       :String;
               CobreBemArquivoRemessa         :String;
               CobreBemLayoutRemessa          :String;
               CobreBemDiretorioRetorno       :String;
               CobreBemArquivoRetorno         :String;
               CobreBemLayoutRetorno          :String;
               CobreBemLayoutBoleto           :String;
               CobreBemPercJurosDiaAtraso     :String;
               CobreBemPercMultaAtraso        :String;
               CobreBemPercentualDesconto     :String;
               CobreBemBancoImprimeBoleto     :String;
               CobreBemDiasProtesto           :String;
               CobreBemLimiteVencimento       :String;
               CobreBemNumeroRemessa          :String;

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure replicarconvenio(pcodigo:string);

   End;


implementation
uses UopcaoRel,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
, UescolheConvenio, Uportador;


{ TTabTitulo }


Function  TObjCONVENIOSBOLETO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.NomeBanco:=fieldbyname('NomeBanco').asstring;
        Self.CodigoBAnco:=fieldbyname('CodigoBAnco').asstring;
        Self.DgBanco:=fieldbyname('DgBanco').asstring;
        Self.LocalPagamento:=fieldbyname('LocalPagamento').asstring;
        Self.Nomecedente:=fieldbyname('Nomecedente').asstring;
        Self.Carteira:=fieldbyname('Carteira').asstring;
        Self.CompCarteira:=fieldbyname('CompCarteira').asstring;
        Self.Agencia:=fieldbyname('Agencia').asstring;
        Self.DGAgencia:=fieldbyname('DGAgencia').asstring;
        Self.ContaCorrente:=fieldbyname('ContaCorrente').asstring;
        Self.DgContaCorrente:=fieldbyname('DgContaCorrente').asstring;
        Self.numeroconvenio:=fieldbyname('numeroconvenio').asstring;
        Self.EspecieDoc:=fieldbyname('EspecieDoc').asstring;
        Self.Aceite:=fieldbyname('Aceite').asstring;
        Self.NumContaResponsavel:=fieldbyname('NumContaResponsavel').asstring;
        Self.EspecieMoeda:=fieldbyname('EspecieMoeda').asstring;
        Self.QuantidadeMoeda:=fieldbyname('QuantidadeMoeda').asstring;
        Self.ValorMoeda:=fieldbyname('ValorMoeda').asstring;
        Self.Instrucoes:=fieldbyname('instrucoes').asstring;
        Self.sacado:=fieldbyname('sacado').asstring;
        Self.taxamensal:=fieldbyname('taxamensal').asstring;
        Self.Quantdiasprotesto:=fieldbyname('quantdiasprotesto').asstring;
        Self.taxabanco:=Fieldbyname('taxabanco').asstring;
        Self.codigocedente:=Fieldbyname('codigocedente').asstring;
        Self.QtdeDigitosNossoNumero:=Fieldbyname('QtdeDigitosNossoNumero').asstring;
        Self.PreImpresso:=Fieldbyname('PreImpresso').asstring;
        Self.CodigoSICOB_caixa:=Fieldbyname('CodigoSICOB_caixa').asstring;
        Self.CodigoCarteira_Caixa:=Fieldbyname('CodigoCarteira_Caixa').asstring;

        Self.codigotransacao_unibanco:=Fieldbyname('codigotransacao_unibanco').asstring;
        Self.posicoes_28_a_29_UNIBANCO:=FieldByname('posicoes_28_a_29_UNIBANCO').asstring;
        Self.numerocliente_UNIBANCO:=Fieldbyname('numerocliente_UNIBANCO').asstring;

        if (FieldByName('Portador').asstring<>'')
        Then Begin
                  If (Self.Portador.LocalizaCodigo(FieldByName('Portador').asstring)=False)
                  Then Begin
                            Messagedlg('Portador N�o encontrado!',mterror,[mbok],0);
                            Self.ZerarTabela;
                            exit;
                       End
                  Else Self.Portador.TabelaparaObjeto;
        End;

        Self.posicao20_sicred:=Fieldbyname('posicao20_sicred').asstring;
        Self.posicao21_sicred:=Fieldbyname('posicao21_sicred').asstring;
        Self.posicao31_34_sicred:=Fieldbyname('posicao31_34_sicred').asstring;
        Self.posicao35_36_sicred:=Fieldbyname('posicao35_36_sicred').asstring;
        Self.posicao37_41_sicred:=Fieldbyname('posicao37_41_sicred').asstring;
        Self.posicao42_sicred:=Fieldbyname('posicao42_sicred').asstring;
        Self.posicao43_sicred:=Fieldbyname('posicao43_sicred').asstring;
        Self.BYTE_NOSSONUMERO_SICRED:=Fieldbyname('BYTE_NOSSONUMERO_SICRED').asstring;

        Self.tipoidentificador_HSBC := Fieldbyname('tipoidentificador_HSBC').asstring;
        Self.codigocedente_HSBC := Fieldbyname('codigocedente_HSBC').asstring;
        Self.codigoCNR_HSBC := Fieldbyname('codigoCNR_HSBC').asstring;

        Self.CobreBem                       :=fieldbyname('CobreBem').AsString;
        Self.CobreBemArquivoLicenca         :=fieldbyname('CobreBemArquivoLicenca').AsString;
        Self.CobreBemCodigoAgencia          :=fieldbyname('CobreBemCodigoAgencia').AsString;
        Self.CobreBemNumeroContaCorrente    :=fieldbyname('CobreBemNumeroContaCorrente').AsString;
        Self.CobreBemCodigoCedente          :=fieldbyname('CobreBemCodigoCedente').AsString;
        Self.CobreBemInicioNossoNumero      :=fieldbyname('CobreBemInicioNossoNumero').AsString;
        Self.CobreBemFimNossoNumero         :=fieldbyname('CobreBemFimNossoNumero').AsString;
        Self.CobreBemProximoNossoNumero     :=fieldbyname('CobreBemProximoNossoNumero').AsString;
        Self.CobreBemOutroDadoConfiguracao1 :=fieldbyname('CobreBemOutroDadoConfiguracao1').AsString;
        Self.CobreBemOutroDadoConfiguracao2 :=fieldbyname('CobreBemOutroDadoConfiguracao2').AsString;
        Self.CobreBemArquivoLogotipo        :=fieldbyname('CobreBemArquivoLogotipo').AsString;
        Self.CobreBemCaminhoImagensCodBarras:=fieldbyname('CobreBemCaminhoImagensCodBarras').AsString;
        Self.CobreBemDiretorioRemessa       :=fieldbyname('CobreBemDiretorioRemessa').AsString;
        Self.CobreBemArquivoRemessa         :=fieldbyname('CobreBemArquivoRemessa').AsString;
        Self.CobreBemLayoutRemessa          :=fieldbyname('CobreBemLayoutRemessa').AsString;
        Self.CobreBemDiretorioRetorno       :=fieldbyname('CobreBemDiretorioRetorno').AsString;
        Self.CobreBemArquivoRetorno         :=fieldbyname('CobreBemArquivoRetorno').AsString;
        Self.CobreBemLayoutRetorno          :=fieldbyname('CobreBemLayoutRetorno').AsString;
        Self.CobreBemLayoutBoleto           :=fieldbyname('CobreBemLayoutBoleto').AsString;
        Self.CobreBemPercJurosDiaAtraso     :=fieldbyname('CobreBemPercJurosDiaAtraso').AsString;
        Self.CobreBemPercMultaAtraso        :=fieldbyname('CobreBemPercMultaAtraso').AsString;
        Self.CobreBemPercentualDesconto     :=fieldbyname('CobreBemPercentualDesconto').AsString;
        Self.CobreBemBancoImprimeBoleto     :=fieldbyname('CobreBemBancoImprimeBoleto').AsString;
        Self.CobreBemDiasProtesto           :=fieldbyname('CobreBemDiasProtesto').AsString;
        Self.CobreBemLimiteVencimento       :=fieldbyname('CobreBemLimiteVencimento').AsString;
        Self.CobreBemNumeroRemessa          :=fieldbyname('CobreBemNumeroRemessa').AsString;

        result:=True;
     End;
end;


Procedure TObjCONVENIOSBOLETO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('NomeBanco').asstring:=Self.NomeBanco;
        ParamByName('CodigoBAnco').asstring:=Self.CodigoBAnco;
        ParamByName('DgBanco').asstring:=Self.DgBanco;
        ParamByName('LocalPagamento').asstring:=Self.LocalPagamento;
        ParamByName('Nomecedente').asstring:=Self.Nomecedente;
        ParamByName('Carteira').asstring:=Self.Carteira;
        ParamByName('CompCarteira').asstring:=Self.CompCarteira;
        ParamByName('Agencia').asstring:=Self.Agencia;
        ParamByName('DGAgencia').asstring:=Self.DGAgencia;
        ParamByName('ContaCorrente').asstring:=Self.ContaCorrente;
        ParamByName('DgContaCorrente').asstring:=Self.DgContaCorrente;
        ParamByName('numeroconvenio').asstring:=Self.numeroconvenio;
        ParamByName('EspecieDoc').asstring:=Self.EspecieDoc;
        ParamByName('Aceite').asstring:=Self.Aceite;
        ParamByName('NumContaResponsavel').asstring:=Self.NumContaResponsavel;
        ParamByName('EspecieMoeda').asstring:=Self.EspecieMoeda;
        ParamByName('QuantidadeMoeda').asstring:=Self.QuantidadeMoeda;
        ParamByName('ValorMoeda').asstring:=Self.ValorMoeda;
        ParamByName('Instrucoes').asstring:=Self.Instrucoes;
        ParamByName('sacado').asstring:=Self.sacado;
        ParamByName('Portador').asstring:=Self.Portador.Get_codigo;
        ParamByName('taxamensal').asstring       :=virgulaparaponto(Self.taxamensal);
        ParamByName('quantdiasprotesto').asstring:=virgulaparaponto(Self.Quantdiasprotesto);
        ParamByName('taxabanco').asstring:=virgulaparaponto(Self.taxabanco);
        ParamByName('codigocedente').asstring:=Self.codigocedente;
        ParamByName('QtdeDigitosNossoNumero').asstring:=Self.QtdeDigitosNossoNumero;
        ParamByName('PreImpresso').asstring:=Self.PreImpresso;
        ParamByName('CodigoSICOB_caixa').asstring:=Self.CodigoSICOB_caixa;
        ParamByName('CodigoCarteira_Caixa').asstring:=Self.CodigoCarteira_Caixa;
        Parambyname('codigotransacao_unibanco').asstring:=Self.codigotransacao_unibanco;
        Parambyname('posicoes_28_a_29_UNIBANCO').asstring:=Self.posicoes_28_a_29_UNIBANCO;
        Parambyname('numerocliente_UNIBANCO').asstring:=Self.numerocliente_UNIBANCO;
        Parambyname('posicao20_sicred').asstring:=Self.posicao20_sicred;
        Parambyname('posicao21_sicred').asstring:=Self.posicao21_sicred;
        Parambyname('posicao31_34_sicred').asstring:=Self.posicao31_34_sicred;
        Parambyname('posicao35_36_sicred').asstring:=Self.posicao35_36_sicred;
        Parambyname('posicao37_41_sicred').asstring:=Self.posicao37_41_sicred;
        Parambyname('posicao42_sicred').asstring:=Self.posicao42_sicred;
        Parambyname('posicao43_sicred').asstring:=Self.posicao43_sicred;
        Parambyname('BYTE_NOSSONUMERO_SICRED').asstring:=Self.BYTE_NOSSONUMERO_SICRED;
        Parambyname('tipoidentificador_HSBC').asstring:=Self.tipoidentificador_HSBC;
        Parambyname('codigocedente_HSBC').asstring:=Self.codigocedente_HSBC;
        Parambyname('codigoCNR_HSBC').asstring:=Self.codigoCNR_HSBC;

        Parambyname('CobreBem').AsString:=Self.CobreBem;
        Parambyname('CobreBemArquivoLicenca').AsString:=Self.CobreBemArquivoLicenca;
        Parambyname('CobreBemCodigoAgencia').AsString:=Self.CobreBemCodigoAgencia;
        Parambyname('CobreBemNumeroContaCorrente').AsString:=Self.CobreBemNumeroContaCorrente;
        Parambyname('CobreBemCodigoCedente').AsString:=Self.CobreBemCodigoCedente;
        Parambyname('CobreBemInicioNossoNumero').AsString:=Self.CobreBemInicioNossoNumero;
        Parambyname('CobreBemFimNossoNumero').AsString:=Self.CobreBemFimNossoNumero;
        Parambyname('CobreBemProximoNossoNumero').AsString:=Self.CobreBemProximoNossoNumero;
        Parambyname('CobreBemOutroDadoConfiguracao1').AsString:=Self.CobreBemOutroDadoConfiguracao1;
        Parambyname('CobreBemOutroDadoConfiguracao2').AsString:=Self.CobreBemOutroDadoConfiguracao2;
        Parambyname('CobreBemArquivoLogotipo').AsString:=Self.CobreBemArquivoLogotipo;
        Parambyname('CobreBemCaminhoImagensCodBarras').AsString:=Self.CobreBemCaminhoImagensCodBarras;
        Parambyname('CobreBemDiretorioRemessa').AsString:=Self.CobreBemDiretorioRemessa;
        Parambyname('CobreBemArquivoRemessa').AsString:=Self.CobreBemArquivoRemessa;
        Parambyname('CobreBemLayoutRemessa').AsString:=Self.CobreBemLayoutRemessa;
        Parambyname('CobreBemDiretorioRetorno').AsString:=Self.CobreBemDiretorioRetorno;
        Parambyname('CobreBemArquivoRetorno').AsString:=Self.CobreBemArquivoRetorno;
        Parambyname('CobreBemLayoutRetorno').AsString:=Self.CobreBemLayoutRetorno;
        ParamByName('CobreBemLayoutBoleto').AsString:=Self.CobreBemLayoutBoleto;
        ParamByName('CobreBemPercJurosDiaAtraso').AsString:=virgulaparaponto(tira_ponto(Self.CobreBemPercJurosDiaAtraso));
        ParamByName('CobreBemPercMultaAtraso').AsString:=virgulaparaponto(tira_ponto(Self.CobreBemPercMultaAtraso));
        ParamByName('CobreBemPercentualDesconto').AsString:=virgulaparaponto(tira_ponto(Self.CobreBemPercentualDesconto));
        ParamByName('CobreBemBancoImprimeBoleto').AsString:=Self.CobreBemBancoImprimeBoleto;
        ParamByName('CobreBemDiasProtesto').AsString:=Self.CobreBemDiasProtesto;
        ParamByName('CobreBemLimiteVencimento').AsString:=Self.CobreBemLimiteVencimento;
        ParamByName('CobreBemNumeroRemessa').AsString:=Self.CobreBemNumeroRemessa;
  End;
End;

//***********************************************************************

function TObjCONVENIOSBOLETO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    //Self.Objquery.SQL.SaveToFile('c:\celio.sql');
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCONVENIOSBOLETO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Nome:='';
        NomeBanco:='';
        CodigoBAnco:='';
        DgBanco:='';
        LocalPagamento:='';
        Nomecedente:='';
        Carteira:='';
        CompCarteira:='';
        Agencia:='';
        DGAgencia:='';
        ContaCorrente:='';
        DgContaCorrente:='';
        numeroconvenio:='';
        EspecieDoc:='';
        Aceite:='';
        NumContaResponsavel:='';
        EspecieMoeda:='';
        QuantidadeMoeda:='';
        ValorMoeda:='';
        Instrucoes:='';
        sacado:='';
        taxamensal:='';
        Quantdiasprotesto:='';
        TaxaBanco:='';
        Portador.ZerarTabela;
        codigocedente:='';
        QtdeDigitosNossoNumero:='';
        PreImpresso:='';
        CodigoSICOB_caixa:='';
        CodigoCarteira_Caixa:='';
        codigotransacao_unibanco:='';
        posicoes_28_a_29_UNIBANCO:='';
        numerocliente_UNIBANCO:='';

        posicao20_sicred:='';
        posicao21_sicred:='';
        posicao31_34_sicred:='';
        posicao35_36_sicred:='';
        posicao37_41_sicred:='';
        posicao42_sicred:='';
        posicao43_sicred:='';
        BYTE_NOSSONUMERO_SICRED:='';

        Self.tipoidentificador_HSBC := '';
        Self.codigocedente_HSBC := '';
        Self.codigoCNR_HSBC := '';

        Self.CobreBem                       :='';
        Self.CobreBemArquivoLicenca         :='';
        Self.CobreBemCodigoAgencia          :='';
        Self.CobreBemNumeroContaCorrente    :='';
        Self.CobreBemCodigoCedente          :='';
        Self.CobreBemInicioNossoNumero      :='';
        Self.CobreBemFimNossoNumero         :='';
        Self.CobreBemProximoNossoNumero     :='';
        Self.CobreBemOutroDadoConfiguracao1 :='';
        Self.CobreBemOutroDadoConfiguracao2 :='';
        Self.CobreBemArquivoLogotipo        :='';
        Self.CobreBemCaminhoImagensCodBarras:='';
        Self.CobreBemDiretorioRemessa       :='';
        Self.CobreBemArquivoRemessa         :='';
        Self.CobreBemLayoutRemessa          :='';
        Self.CobreBemDiretorioRetorno       :='';
        Self.CobreBemArquivoRetorno         :='';
        Self.CobreBemLayoutRetorno          :='';
        Self.CobreBemLayoutBoleto           :='';
        Self.CobreBemPercJurosDiaAtraso     :='';
        Self.CobreBemPercMultaAtraso        :='';
        Self.CobreBemPercentualDesconto     :='';
        Self.CobreBemBancoImprimeBoleto     :='';
        Self.CobreBemDiasProtesto      :='';
        Self.CobreBemLimiteVencimento:='';
        Self.CobreBemNumeroRemessa:='';
     End;
end;

Function TObjCONVENIOSBOLETO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      if (taxamensal='')
      Then taxamensal:='0';

      if (taxabanco='')
      Then taxabanco:='0';

      if (Portador.Get_codigo='')
      Then Mensagem:=Mensagem+'/Portador';


      if (Quantdiasprotesto='')
      then Quantdiasprotesto:='0';

      if (QtdeDigitosNossoNumero='')
      then Mensagem:=Mensagem+'/Qtde de D�gitos do nosso n�mero';

      if (PreImpresso='')
      then Mensagem:=Mensagem+'/Pr�-Impresso';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCONVENIOSBOLETO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     if (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Mensagem:=Mensagem+'\Portador n�o encontrado';


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCONVENIOSBOLETO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';

     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Strtoint(Self.QtdeDigitosNossoNumero);
     Except
           Mensagem:=mensagem+'/Qtde de D�gitos do Nosso N�mero';
     End;

     try
        Strtoint(Self.Portador.get_codigo);
     Except
           Mensagem:=mensagem+'/Portador';
     End;

     try
        if (taxamensal<>'')
        Then Strtofloat(Self.taxamensal);
     Except
           Mensagem:=mensagem+'/Taxa Mensal';
     End;

     try
        if (taxabanco<>'')
        Then Strtofloat(Self.taxabanco);
     Except
           Mensagem:=mensagem+'/Taxa Banco';
     End;

     try
        if (Quantdiasprotesto<>'')
        Then Strtoint(Self.Quantdiasprotesto);
     Except
           Mensagem:=mensagem+'/Qtde de Dias para Protesto';
     End;


//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCONVENIOSBOLETO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCONVENIOSBOLETO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA
        if (PreImpresso<>'S') and (PreImpresso<>'N')
        Then mensagem:=Mensagem+'/O Campo Pr�-Impresso cont�m um valor inv�lido';

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;
function TObjCONVENIOSBOLETO.LocalizaCodigoBanco(Parametro: string): boolean;
Begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Nome,NomeBanco,CodigoBAnco,DgBanco,LocalPagamento');
           SQL.ADD(',Nomecedente,Carteira,CompCarteira,Agencia,DGAgencia');
           SQL.ADD(',ContaCorrente,DgContaCorrente,numeroconvenio,EspecieDoc,Aceite');
           SQL.ADD(',NumContaResponsavel,EspecieMoeda,QuantidadeMoeda,ValorMoeda,instrucoes,taxamensal,Quantdiasprotesto,taxabanco,portador');
           SQL.ADD(',codigocedente,QtdeDigitosNossoNumero,preimpresso,sacado,CodigoSICOB_caixa,CodigoCarteira_Caixa,codigotransacao_unibanco,posicoes_28_a_29_UNIBANCO,numerocliente_UNIBANCO');
           sql.add(',posicao20_sicred,posicao21_sicred,posicao31_34_sicred,posicao35_36_sicred,posicao37_41_sicred,posicao42_sicred,posicao43_sicred,BYTE_NOSSONUMERO_SICRED');
           sql.add(',tipoidentificador_HSBC,codigocedente_HSBC,codigoCNR_HSBC');
           sql.add(',CobreBem,CobreBemArquivoLicenca,CobreBemCodigoAgencia');
           sql.add(',CobreBemNumeroContaCorrente,CobreBemCodigoCedente,CobreBemInicioNossoNumero');
           sql.add(',CobreBemFimNossoNumero,CobreBemProximoNossoNumero,CobreBemOutroDadoConfiguracao1, CobreBemOutroDadoConfiguracao2, ');
           sql.add(',CobreBemArquivoLogotipo,CobreBemCaminhoImagensCodBarras,CobreBemDiretorioRemessa');
           sql.add(',CobreBemArquivoRemessa,CobreBemLayoutRemessa,CobreBemDiretorioRetorno');
           sql.add(',CobreBemArquivoRetorno,CobreBemLayoutRetorno,CobreBemLayoutBoleto,CobreBemPercJurosDiaAtraso,CobreBemPercMultaAtraso');
           sql.Add(',CobreBemPercentualDesconto,CobreBemBancoImprimeBoleto,CobreBemDiasProtesto,CobreBemLimiteVencimento,CobreBemNumeroRemessa');
           SQL.ADD('from  TabConveniosBoleto WHERE Codigobanco='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjCONVENIOSBOLETO.LocalizaConvenio(PNumeroConvenio: string): boolean;
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Nome,NomeBanco,CodigoBAnco,DgBanco,LocalPagamento');
           SQL.ADD(',Nomecedente,Carteira,CompCarteira,Agencia,DGAgencia');
           SQL.ADD(',ContaCorrente,DgContaCorrente,numeroconvenio,EspecieDoc,Aceite');
           SQL.ADD(',NumContaResponsavel,EspecieMoeda,QuantidadeMoeda,ValorMoeda,instrucoes,taxamensal,Quantdiasprotesto,taxabanco,portador');
           SQL.ADD(',codigocedente,QtdeDigitosNossoNumero,preimpresso,sacado,CodigoSICOB_caixa,CodigoCarteira_Caixa,codigotransacao_unibanco,posicoes_28_a_29_UNIBANCO,numerocliente_UNIBANCO');
           sql.add(',posicao20_sicred,posicao21_sicred,posicao31_34_sicred,posicao35_36_sicred,posicao37_41_sicred,posicao42_sicred,posicao43_sicred,BYTE_NOSSONUMERO_SICRED');
           sql.add(',tipoidentificador_HSBC,codigocedente_HSBC,codigoCNR_HSBC');
           sql.add(',CobreBem,CobreBemArquivoLicenca,CobreBemCodigoAgencia');
           sql.add(',CobreBemNumeroContaCorrente,CobreBemCodigoCedente,CobreBemInicioNossoNumero');
           sql.add(',CobreBemFimNossoNumero,CobreBemProximoNossoNumero,CobreBemOutroDadoConfiguracao1, CobreBemOutroDadoConfiguracao2');
           sql.add(',CobreBemArquivoLogotipo,CobreBemCaminhoImagensCodBarras,CobreBemDiretorioRemessa');
           sql.add(',CobreBemArquivoRemessa,CobreBemLayoutRemessa,CobreBemDiretorioRetorno');
           sql.add(',CobreBemArquivoRetorno,CobreBemLayoutRetorno,CobreBemLayoutBoleto,CobreBemPercJurosDiaAtraso');
           sql.Add(',CobreBemPercMultaAtraso,CobreBemPercentualDesconto,CobreBemBancoImprimeBoleto,CobreBemDiasProtesto,CobreBemLimiteVencimento,CobreBemNumeroRemessa');
           SQL.ADD('from  TabConveniosBoleto WHERE NumeroConvenio='+#39+PNumeroConvenio+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


function TObjCONVENIOSBOLETO.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Nome,NomeBanco,CodigoBAnco,DgBanco,LocalPagamento');
           SQL.ADD(',Nomecedente,Carteira,CompCarteira,Agencia,DGAgencia');
           SQL.ADD(',ContaCorrente,DgContaCorrente,numeroconvenio,EspecieDoc,Aceite');
           SQL.ADD(',NumContaResponsavel,EspecieMoeda,QuantidadeMoeda,ValorMoeda,instrucoes,taxamensal,Quantdiasprotesto,taxabanco,portador');
           SQL.ADD(',codigocedente,QtdeDigitosNossoNumero,preimpresso,sacado,CodigoSICOB_caixa,CodigoCarteira_Caixa,codigotransacao_unibanco,posicoes_28_a_29_UNIBANCO,numerocliente_UNIBANCO');
           sql.add(',posicao20_sicred,posicao21_sicred,posicao31_34_sicred,posicao35_36_sicred,posicao37_41_sicred,posicao42_sicred,posicao43_sicred,BYTE_NOSSONUMERO_SICRED');
           sql.add(',tipoidentificador_HSBC,codigocedente_HSBC,codigoCNR_HSBC');
           sql.add(',CobreBem,CobreBemArquivoLicenca,CobreBemCodigoAgencia');
           sql.add(',CobreBemNumeroContaCorrente,CobreBemCodigoCedente,CobreBemInicioNossoNumero');
           sql.add(',CobreBemFimNossoNumero,CobreBemProximoNossoNumero,CobreBemOutroDadoConfiguracao1, CobreBemOutroDadoConfiguracao2');
           sql.add(',CobreBemArquivoLogotipo,CobreBemCaminhoImagensCodBarras,CobreBemDiretorioRemessa');
           sql.add(',CobreBemArquivoRemessa,CobreBemLayoutRemessa,CobreBemDiretorioRetorno');
           sql.add(',CobreBemArquivoRetorno,CobreBemLayoutRetorno,CobreBemLayoutBoleto,CobreBemPercJurosDiaAtraso,  CobreBemPercMultaAtraso');
           sql.Add(',CobreBemPercentualDesconto,CobreBemBancoImprimeBoleto,CobreBemDiasProtesto,CobreBemLimiteVencimento,CobreBemNumeroRemessa');
           SQL.ADD('  from  TabConveniosBoleto WHERE Codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCONVENIOSBOLETO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCONVENIOSBOLETO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCONVENIOSBOLETO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Portador:=TobjPortador.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('insert into TABCONVENIOSBOLETO');
                InsertSQL.add('(');
                InsertSQL.add('  CODIGO');
                InsertSQL.add(', NOME');
                InsertSQL.add(', NOMEBANCO');
                InsertSQL.add(', CODIGOBANCO');
                InsertSQL.add(', DGBANCO');
                InsertSQL.add(', LOCALPAGAMENTO');
                InsertSQL.add(', NOMECEDENTE');
                InsertSQL.add(', CARTEIRA');
                InsertSQL.add(', COMPCARTEIRA');
                InsertSQL.add(', AGENCIA');
                InsertSQL.add(', DGAGENCIA');
                InsertSQL.add(', CONTACORRENTE');
                InsertSQL.add(', DGCONTACORRENTE');
                InsertSQL.add(', NUMEROCONVENIO');
                InsertSQL.add(', ESPECIEDOC');
                InsertSQL.add(', ACEITE');
                InsertSQL.add(', NUMCONTARESPONSAVEL');
                InsertSQL.add(', ESPECIEMOEDA');
                InsertSQL.add(', QUANTIDADEMOEDA');
                InsertSQL.add(', VALORMOEDA');
                InsertSQL.add(', INSTRUCOES');
                InsertSQL.add(', TAXAMENSAL');
                InsertSQL.add(', QUANTDIASPROTESTO');
                InsertSQL.add(', TAXABANCO');
                InsertSQL.add(', CODIGOCEDENTE');
                InsertSQL.add(', QTDEDIGITOSNOSSONUMERO');
                InsertSQL.add(', PORTADOR');
                InsertSQL.add(', PREIMPRESSO');
                InsertSQL.add(', SACADO');
                InsertSQL.add(', CODIGOSICOB_CAIXA');
                InsertSQL.add(', CODIGOCARTEIRA_CAIXA');
                InsertSQL.add(', CODIGOTRANSACAO_UNIBANCO');
                InsertSQL.add(', POSICOES_28_A_29_UNIBANCO');
                InsertSQL.add(', NUMEROCLIENTE_UNIBANCO');
                InsertSQL.add(', POSICAO20_SICRED');
                InsertSQL.add(', POSICAO21_SICRED');
                InsertSQL.add(', POSICAO31_34_SICRED');
                InsertSQL.add(', POSICAO35_36_SICRED');
                InsertSQL.add(', POSICAO37_41_SICRED');
                InsertSQL.add(', POSICAO42_SICRED');
                InsertSQL.add(', POSICAO43_SICRED');
                InsertSQL.add(', TIPOIDENTIFICADOR_HSBC');
                InsertSQL.add(', CODIGOCEDENTE_HSBC');
                InsertSQL.add(', CODIGOCNR_HSBC');
                InsertSQL.add(', BYTE_NOSSONUMERO_SICRED');
                InsertSQL.add(', COBREBEM');
                InsertSQL.add(', COBREBEMARQUIVOLICENCA');
                InsertSQL.add(', COBREBEMCODIGOAGENCIA');
                InsertSQL.add(', COBREBEMNUMEROCONTACORRENTE');
                InsertSQL.add(', COBREBEMCODIGOCEDENTE');
                InsertSQL.add(', COBREBEMINICIONOSSONUMERO');
                InsertSQL.add(', COBREBEMFIMNOSSONUMERO');
                InsertSQL.add(', COBREBEMPROXIMONOSSONUMERO');
                InsertSQL.add(', COBREBEMOUTRODADOCONFIGURACAO1');
                InsertSQL.add(', COBREBEMOUTRODADOCONFIGURACAO2');
                InsertSQL.add(', COBREBEMARQUIVOLOGOTIPO');
                InsertSQL.add(', COBREBEMCAMINHOIMAGENSCODBARRAS');
                InsertSQL.add(', COBREBEMDIRETORIOREMESSA');
                InsertSQL.add(', COBREBEMARQUIVOREMESSA');
                InsertSQL.add(', COBREBEMLAYOUTREMESSA');
                InsertSQL.add(', COBREBEMDIRETORIORETORNO');
                InsertSQL.add(', COBREBEMARQUIVORETORNO');
                InsertSQL.add(', COBREBEMLAYOUTRETORNO,CobreBemLayoutBoleto,CobreBemPercJurosDiaAtraso');
                InsertSQL.Add(', CobreBemPercMultaAtraso,CobreBemPercentualDesconto,CobreBemBancoImprimeBoleto,CobreBemDiasProtesto,CobreBemLimiteVencimento');
                InsertSql.Add(',CobreBemNumeroRemessa');
                InsertSQL.add(')');
                InsertSQL.add('values');
                InsertSQL.add('(');
                InsertSQL.add('  :CODIGO');
                InsertSQL.add(', :NOME');
                InsertSQL.add(', :NOMEBANCO');
                InsertSQL.add(', :CODIGOBANCO');
                InsertSQL.add(', :DGBANCO');
                InsertSQL.add(', :LOCALPAGAMENTO');
                InsertSQL.add(', :NOMECEDENTE');
                InsertSQL.add(', :CARTEIRA');
                InsertSQL.add(', :COMPCARTEIRA');
                InsertSQL.add(', :AGENCIA');
                InsertSQL.add(', :DGAGENCIA');
                InsertSQL.add(', :CONTACORRENTE');
                InsertSQL.add(', :DGCONTACORRENTE');
                InsertSQL.add(', :NUMEROCONVENIO');
                InsertSQL.add(', :ESPECIEDOC');
                InsertSQL.add(', :ACEITE');
                InsertSQL.add(', :NUMCONTARESPONSAVEL');
                InsertSQL.add(', :ESPECIEMOEDA');
                InsertSQL.add(', :QUANTIDADEMOEDA');
                InsertSQL.add(', :VALORMOEDA');
                InsertSQL.add(', :INSTRUCOES');
                InsertSQL.add(', :TAXAMENSAL');
                InsertSQL.add(', :QUANTDIASPROTESTO');
                InsertSQL.add(', :TAXABANCO');
                InsertSQL.add(', :CODIGOCEDENTE');
                InsertSQL.add(', :QTDEDIGITOSNOSSONUMERO');
                InsertSQL.add(', :PORTADOR');
                InsertSQL.add(', :PREIMPRESSO');
                InsertSQL.add(', :SACADO');
                InsertSQL.add(', :CODIGOSICOB_CAIXA');
                InsertSQL.add(', :CODIGOCARTEIRA_CAIXA');
                InsertSQL.add(', :CODIGOTRANSACAO_UNIBANCO');
                InsertSQL.add(', :POSICOES_28_A_29_UNIBANCO');
                InsertSQL.add(', :NUMEROCLIENTE_UNIBANCO');
                InsertSQL.add(', :POSICAO20_SICRED');
                InsertSQL.add(', :POSICAO21_SICRED');
                InsertSQL.add(', :POSICAO31_34_SICRED');
                InsertSQL.add(', :POSICAO35_36_SICRED');
                InsertSQL.add(', :POSICAO37_41_SICRED');
                InsertSQL.add(', :POSICAO42_SICRED');
                InsertSQL.add(', :POSICAO43_SICRED');
                InsertSQL.add(', :TIPOIDENTIFICADOR_HSBC');
                InsertSQL.add(', :CODIGOCEDENTE_HSBC');
                InsertSQL.add(', :CODIGOCNR_HSBC');
                InsertSQL.add(', :BYTE_NOSSONUMERO_SICRED');
                InsertSQL.add(', :COBREBEM');
                InsertSQL.add(', :COBREBEMARQUIVOLICENCA');
                InsertSQL.add(', :COBREBEMCODIGOAGENCIA');
                InsertSQL.add(', :COBREBEMNUMEROCONTACORRENTE');
                InsertSQL.add(', :COBREBEMCODIGOCEDENTE');
                InsertSQL.add(', :COBREBEMINICIONOSSONUMERO');
                InsertSQL.add(', :COBREBEMFIMNOSSONUMERO');
                InsertSQL.add(', :COBREBEMPROXIMONOSSONUMERO');
                InsertSQL.add(', :COBREBEMOUTRODADOCONFIGURACAO1');
                InsertSQL.add(', :COBREBEMOUTRODADOCONFIGURACAO2');
                InsertSQL.add(', :COBREBEMARQUIVOLOGOTIPO');
                InsertSQL.add(', :COBREBEMCAMINHOIMAGENSCODBARRAS');
                InsertSQL.add(', :COBREBEMDIRETORIOREMESSA');
                InsertSQL.add(', :COBREBEMARQUIVOREMESSA');
                InsertSQL.add(', :COBREBEMLAYOUTREMESSA');
                InsertSQL.add(', :COBREBEMDIRETORIORETORNO');
                InsertSQL.add(', :COBREBEMARQUIVORETORNO');
                InsertSQL.add(', :COBREBEMLAYOUTRETORNO, :CobreBemLayoutBoleto, :CobreBemPercJurosDiaAtraso');
                InsertSQL.Add(', :CobreBemPercMultaAtraso,:CobreBemPercentualDesconto, :CobreBemBancoImprimeBoleto, :CobreBemDiasProtesto,:CobreBemLimiteVencimento');
                InsertSql.Add(', :CobreBemNumeroRemessa)');

                ModifySQL.clear;
                ModifySQL.add('update TABCONVENIOSBOLETO');
                ModifySQL.add('set');
                ModifySQL.add(' CODIGO = :CODIGO');
                ModifySQL.add(', NOME = :NOME');
                ModifySQL.add(', NOMEBANCO = :NOMEBANCO');
                ModifySQL.add(', CODIGOBANCO = :CODIGOBANCO');
                ModifySQL.add(', DGBANCO = :DGBANCO');
                ModifySQL.add(', LOCALPAGAMENTO = :LOCALPAGAMENTO');
                ModifySQL.add(', NOMECEDENTE = :NOMECEDENTE');
                ModifySQL.add(', CARTEIRA = :CARTEIRA');
                ModifySQL.add(', COMPCARTEIRA = :COMPCARTEIRA');
                ModifySQL.add(', AGENCIA = :AGENCIA');
                ModifySQL.add(', DGAGENCIA = :DGAGENCIA');
                ModifySQL.add(', CONTACORRENTE = :CONTACORRENTE');
                ModifySQL.add(', DGCONTACORRENTE = :DGCONTACORRENTE');
                ModifySQL.add(', NUMEROCONVENIO = :NUMEROCONVENIO');
                ModifySQL.add(', ESPECIEDOC = :ESPECIEDOC');
                ModifySQL.add(', ACEITE = :ACEITE');
                ModifySQL.add(', NUMCONTARESPONSAVEL = :NUMCONTARESPONSAVEL');
                ModifySQL.add(', ESPECIEMOEDA = :ESPECIEMOEDA');
                ModifySQL.add(', QUANTIDADEMOEDA = :QUANTIDADEMOEDA');
                ModifySQL.add(', VALORMOEDA = :VALORMOEDA');
                ModifySQL.add(', INSTRUCOES = :INSTRUCOES');
                ModifySQL.add(', TAXAMENSAL = :TAXAMENSAL');
                ModifySQL.add(', QUANTDIASPROTESTO = :QUANTDIASPROTESTO');
                ModifySQL.add(', TAXABANCO = :TAXABANCO');
                ModifySQL.add(', CODIGOCEDENTE = :CODIGOCEDENTE');
                ModifySQL.add(', QTDEDIGITOSNOSSONUMERO = :QTDEDIGITOSNOSSONUMERO');
                ModifySQL.add(', PORTADOR = :PORTADOR');
                ModifySQL.add(', PREIMPRESSO = :PREIMPRESSO');
                ModifySQL.add(', SACADO = :SACADO');
                ModifySQL.add(', CODIGOSICOB_CAIXA = :CODIGOSICOB_CAIXA');
                ModifySQL.add(', CODIGOCARTEIRA_CAIXA = :CODIGOCARTEIRA_CAIXA');
                ModifySQL.add(', CODIGOTRANSACAO_UNIBANCO = :CODIGOTRANSACAO_UNIBANCO');
                ModifySQL.add(', POSICOES_28_A_29_UNIBANCO = :POSICOES_28_A_29_UNIBANCO');
                ModifySQL.add(', NUMEROCLIENTE_UNIBANCO = :NUMEROCLIENTE_UNIBANCO');
                ModifySQL.add(', POSICAO20_SICRED = :POSICAO20_SICRED');
                ModifySQL.add(', POSICAO21_SICRED = :POSICAO21_SICRED');
                ModifySQL.add(', POSICAO31_34_SICRED = :POSICAO31_34_SICRED');
                ModifySQL.add(', POSICAO35_36_SICRED = :POSICAO35_36_SICRED');
                ModifySQL.add(', POSICAO37_41_SICRED = :POSICAO37_41_SICRED');
                ModifySQL.add(', POSICAO42_SICRED = :POSICAO42_SICRED');
                ModifySQL.add(', POSICAO43_SICRED = :POSICAO43_SICRED');
                ModifySQL.add(', TIPOIDENTIFICADOR_HSBC = :TIPOIDENTIFICADOR_HSBC');
                ModifySQL.add(', CODIGOCEDENTE_HSBC = :CODIGOCEDENTE_HSBC');
                ModifySQL.add(', CODIGOCNR_HSBC = :CODIGOCNR_HSBC');
                ModifySQL.add(', BYTE_NOSSONUMERO_SICRED = :BYTE_NOSSONUMERO_SICRED');
                ModifySQL.add(', COBREBEM = :COBREBEM');
                ModifySQL.add(', COBREBEMARQUIVOLICENCA = :COBREBEMARQUIVOLICENCA');
                ModifySQL.add(', COBREBEMCODIGOAGENCIA = :COBREBEMCODIGOAGENCIA');
                ModifySQL.add(', COBREBEMNUMEROCONTACORRENTE = :COBREBEMNUMEROCONTACORRENTE');
                ModifySQL.add(', COBREBEMCODIGOCEDENTE = :COBREBEMCODIGOCEDENTE');
                ModifySQL.add(', COBREBEMINICIONOSSONUMERO = :COBREBEMINICIONOSSONUMERO');
                ModifySQL.add(', COBREBEMFIMNOSSONUMERO = :COBREBEMFIMNOSSONUMERO');
                ModifySQL.add(', COBREBEMPROXIMONOSSONUMERO = :COBREBEMPROXIMONOSSONUMERO');
                ModifySQL.add(', COBREBEMOUTRODADOCONFIGURACAO1 = :COBREBEMOUTRODADOCONFIGURACAO1');
                ModifySQL.add(', COBREBEMOUTRODADOCONFIGURACAO2 = :COBREBEMOUTRODADOCONFIGURACAO2');
                ModifySQL.add(', COBREBEMARQUIVOLOGOTIPO = :COBREBEMARQUIVOLOGOTIPO');
                ModifySQL.add(', COBREBEMCAMINHOIMAGENSCODBARRAS = :COBREBEMCAMINHOIMAGENSCODBARRAS');
                ModifySQL.add(', COBREBEMDIRETORIOREMESSA = :COBREBEMDIRETORIOREMESSA');
                ModifySQL.add(', COBREBEMARQUIVOREMESSA = :COBREBEMARQUIVOREMESSA');
                ModifySQL.add(', COBREBEMLAYOUTREMESSA = :COBREBEMLAYOUTREMESSA');
                ModifySQL.add(', COBREBEMDIRETORIORETORNO = :COBREBEMDIRETORIORETORNO');
                ModifySQL.add(', COBREBEMARQUIVORETORNO = :COBREBEMARQUIVORETORNO');
                ModifySQL.add(', COBREBEMLAYOUTRETORNO = :COBREBEMLAYOUTRETORNO');
                ModifySQl.Add(', CobreBemLayoutBoleto = :CobreBemLayoutBoleto');
                ModifySQl.Add(', CobreBemPercJurosDiaAtraso = :CobreBemPercJurosDiaAtraso');
                ModifySQl.Add(', CobreBemPercMultaAtraso = :CobreBemPercMultaAtraso');
                ModifySQL.Add(' ,CobreBemPercentualDesconto = :CobreBemPercentualDesconto');
                ModifySQl.Add(' ,CobreBemBancoImprimeBoleto = :CobreBemBancoImprimeBoleto');
                ModifySQl.Add(' ,CobreBemDiasProtesto = :CobreBemDiasProtesto');
                ModifySQl.Add(' ,CobreBemLimiteVencimento=:CobreBemLimiteVencimento');
                ModifySQl.Add(' ,CobreBemNumeroRemessa=:CobreBemNumeroRemessa');
                ModifySQL.add('where');
                ModifySQL.add('  CODIGO = :CODIGO');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabConveniosBoleto where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCONVENIOSBOLETO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCONVENIOSBOLETO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCONVENIOSBOLETO');
     Result:=Self.ParametroPesquisa;
end;

function TObjCONVENIOSBOLETO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Conv�nios de Boleto ';
end;


function TObjCONVENIOSBOLETO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCONVENIOSBOLETO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCONVENIOSBOLETO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCONVENIOSBOLETO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Portador.free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCONVENIOSBOLETO.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCONVENIOSBOLETO.RetornaCampoNome: string;
begin
      result:='NomeBanco';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjConveniosBoleto.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjConveniosBoleto.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjConveniosBoleto.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjConveniosBoleto.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
procedure TObjConveniosBoleto.Submit_NomeBanco(parametro: string);
begin
        Self.NomeBanco:=Parametro;
end;
function TObjConveniosBoleto.Get_NomeBanco: string;
begin
        Result:=Self.NomeBanco;
end;
procedure TObjConveniosBoleto.Submit_CodigoBAnco(parametro: string);
begin
        Self.CodigoBAnco:=Parametro;
end;
function TObjConveniosBoleto.Get_CodigoBAnco: string;
begin
        Result:=Self.CodigoBAnco;
end;
procedure TObjConveniosBoleto.Submit_DgBanco(parametro: string);
begin
        Self.DgBanco:=Parametro;
end;
function TObjConveniosBoleto.Get_DgBanco: string;
begin
        Result:=Self.DgBanco;
end;
procedure TObjConveniosBoleto.Submit_LocalPagamento(parametro: string);
begin
        Self.LocalPagamento:=Parametro;
end;
function TObjConveniosBoleto.Get_LocalPagamento: string;
begin
        Result:=Self.LocalPagamento;
end;
procedure TObjConveniosBoleto.Submit_Nomecedente(parametro: string);
begin
        Self.Nomecedente:=Parametro;
end;
function TObjConveniosBoleto.Get_Nomecedente: string;
begin
        Result:=Self.Nomecedente;
end;
procedure TObjConveniosBoleto.Submit_Carteira(parametro: string);
begin
        Self.Carteira:=Parametro;
end;
function TObjConveniosBoleto.Get_Carteira: string;
begin
        Result:=Self.Carteira;
end;
procedure TObjConveniosBoleto.Submit_CompCarteira(parametro: string);
begin
        Self.CompCarteira:=Parametro;
end;
function TObjConveniosBoleto.Get_CompCarteira: string;
begin
        Result:=Self.CompCarteira;
end;
procedure TObjConveniosBoleto.Submit_Agencia(parametro: string);
begin
        Self.Agencia:=Parametro;
end;
function TObjConveniosBoleto.Get_Agencia: string;
begin
        Result:=Self.Agencia;
end;
procedure TObjConveniosBoleto.Submit_DGAgencia(parametro: string);
begin
        Self.DGAgencia:=Parametro;
end;
function TObjConveniosBoleto.Get_DGAgencia: string;
begin
        Result:=Self.DGAgencia;
end;
procedure TObjConveniosBoleto.Submit_ContaCorrente(parametro: string);
begin
        Self.ContaCorrente:=Parametro;
end;
function TObjConveniosBoleto.Get_ContaCorrente: string;
begin
        Result:=Self.ContaCorrente;
end;
procedure TObjConveniosBoleto.Submit_DgContaCorrente(parametro: string);
begin
        Self.DgContaCorrente:=Parametro;
end;
function TObjConveniosBoleto.Get_DgContaCorrente: string;
begin
        Result:=Self.DgContaCorrente;
end;
procedure TObjConveniosBoleto.Submit_numeroconvenio(parametro: string);
begin
        Self.numeroconvenio:=Parametro;
end;
function TObjConveniosBoleto.Get_numeroconvenio: string;
begin
        Result:=Self.numeroconvenio;
end;
procedure TObjConveniosBoleto.Submit_EspecieDoc(parametro: string);
begin
        Self.EspecieDoc:=Parametro;
end;
function TObjConveniosBoleto.Get_EspecieDoc: string;
begin
        Result:=Self.EspecieDoc;
end;
procedure TObjConveniosBoleto.Submit_Aceite(parametro: string);
begin
        Self.Aceite:=Parametro;
end;
function TObjConveniosBoleto.Get_Aceite: string;
begin
        Result:=Self.Aceite;
end;
procedure TObjConveniosBoleto.Submit_NumContaResponsavel(parametro: string);
begin
        Self.NumContaResponsavel:=Parametro;
end;
function TObjConveniosBoleto.Get_NumContaResponsavel: string;
begin
        Result:=Self.NumContaResponsavel;
end;
procedure TObjConveniosBoleto.Submit_EspecieMoeda(parametro: string);
begin
        Self.EspecieMoeda:=Parametro;
end;
function TObjConveniosBoleto.Get_EspecieMoeda: string;
begin
        Result:=Self.EspecieMoeda;
end;
procedure TObjConveniosBoleto.Submit_QuantidadeMoeda(parametro: string);
begin
        Self.QuantidadeMoeda:=Parametro;
end;
function TObjConveniosBoleto.Get_QuantidadeMoeda: string;
begin
        Result:=Self.QuantidadeMoeda;
end;
procedure TObjConveniosBoleto.Submit_ValorMoeda(parametro: string);
begin
        Self.ValorMoeda:=Parametro;
end;
function TObjConveniosBoleto.Get_ValorMoeda: string;
begin
        Result:=Self.ValorMoeda;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
function TObjCONVENIOSBOLETO.Get_Instrucoes: String;
begin
     Result:=Self.Instrucoes;
end;

procedure TObjCONVENIOSBOLETO.Submit_Instrucoes(parametro: String);
begin
     Self.Instrucoes:=parametro;
end;


function TObjCONVENIOSBOLETO.Pegaconvenio: boolean;
var
cont:integer;
begin
     result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from TabConveniosBoleto');
          open;
          Self.Objquery.last;
          if (Self.Objquery.RecordCount>0)
          Then Begin
                    //em casos de mais de um convenio tenho que escolher qual usar
                    FescolheConvenio.STRGConvenio.ColCount:=2;
                    FescolheConvenio.STRGConvenio.FixedCols:=1;
                    FescolheConvenio.STRGConvenio.FixedRows:=1;
                    FescolheConvenio.STRGConvenio.Cols[0].clear;
                    FescolheConvenio.STRGConvenio.Cols[1].clear;
                    FescolheConvenio.STRGConvenio.RowCount:=Self.Objquery.RecordCount+1;
                    FescolheConvenio.STRGConvenio.Cells[0,0]:='C�DIGO';
                    FescolheConvenio.STRGConvenio.Cells[1,0]:='CONV�NIO';
                    Self.Objquery.First;
                    cont:=1;
                    while not(Self.Objquery.Eof) do
                    begin
                         FescolheConvenio.STRGConvenio.Cells[0,cont]:=Self.Objquery.fieldbyname('codigo').asstring;
                         FescolheConvenio.STRGConvenio.Cells[1,cont]:=Self.Objquery.fieldbyname('NOME').asstring+'-'+Self.Objquery.fieldbyname('NUMEROCONVENIO').asstring;
                         inc(cont,1);
                         Self.Objquery.next;
                    end;
                    AjustaLArguraColunaGrid(FescolheConvenio.STRGConvenio);

                    FescolheConvenio.showmodal;
          
                    if (FescolheConvenio.tag=-1)
                    Then exit;
          
                    Self.LocalizaCodigo(FescolheConvenio.STRGConvenio.Cells[0,Fescolheconvenio.tag]);
                    //alterado por celio - essas duas linhas abaixo estavam fora do bloco, se nao houvesse
                    //nenhum convenio, ele retornava true.
                    Self.TabelaparaObjeto;
                    result:=true;
          End
          Else Begin
                MensagemErro('N�o foram encontrados Conv�nios Cadastrados.');
                result := false;
          End;
     End;
End;

function TObjCONVENIOSBOLETO.Get_taxamensal: string;
begin
     Result:=Self.taxamensal;
end;

procedure TObjCONVENIOSBOLETO.Submit_Quantdiasprotesto(parametro: string);
begin
     Self.quantdiasprotesto:=parametro;
end;

function TObjCONVENIOSBOLETO.Get_Quantdiasprotesto: string;
begin
     Result:=Self.quantdiasprotesto;
end;

procedure TObjCONVENIOSBOLETO.Submit_taxamensal(parametro: string);
begin
     Self.taxamensal:=parametro;
end;

function TObjCONVENIOSBOLETO.Get_taxabanco: string;
begin
     Result:=Self.taxabanco;
end;

procedure TObjCONVENIOSBOLETO.Submit_taxabanco(parametro: string);
begin
     Self.taxabanco:=parametro;
end;

procedure TObjCONVENIOSBOLETO.edtPortadorExit(Sender: TObject;
  LbNome: TLabel);
begin
     LbNome.caption:='';
     if (TEdit(Sender).text='')
     Then exit;

     Try
        strtoint(TEdit(Sender).text);
     Except
           TEdit(Sender).text:='';
     End;

     if (Self.Portador.LocalizaCodigo(TEdit(Sender).text)=False)
     Then begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.portador.TabelaparaObjeto;
     LbNome.caption:=Self.portador.Get_Nome;

end;

procedure TObjCONVENIOSBOLETO.edtPortadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState; LbNome: TLabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPortador1:TFPortador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FPortador1:=TFPortador.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Portador.Get_Pesquisa,Self.Portador.Get_TituloPesquisa,FPortador1)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPortador1);
     End;
end;

function TObjCONVENIOSBOLETO.get_codigocedente: String;
begin
     Result:=Self.codigocedente;
end;

function TObjCONVENIOSBOLETO.get_QtdeDigitosNossoNumero: string;
begin
     Result:=Self.QtdeDigitosNossoNumero;
end;

procedure TObjCONVENIOSBOLETO.Submit_codigocedente(parametro: String);
begin
     Self.codigocedente:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_QtdeDigitosNossoNumero(
  parametro: string);
begin
     Self.QtdeDigitosNossoNumero:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_PreImpresso: string;
begin
     Result:=Self.PreImpresso;
end;

procedure TObjCONVENIOSBOLETO.Submit_PreImpresso(parametro: string);
begin
     Self.PreImpresso:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_sacado: String;
begin
     Result:=Self.Sacado;
end;

procedure TObjCONVENIOSBOLETO.Submit_sacado(parametro: String);
begin
     Self.Sacado:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CodigoCarteira_Caixa: string;
begin
     Result:=Self.CodigoCarteira_Caixa;
end;

function TObjCONVENIOSBOLETO.Get_CodigoSICOB_caixa: string;
begin
     Result:=Self.CodigoSICOB_caixa;
end;


procedure TObjCONVENIOSBOLETO.Submit_CodigoCarteira_Caixa(
  parametro: string);
begin
     Self.CodigoCarteira_Caixa:=parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CodigoSICOB_caixa(parametro: string);
begin
     Self.CodigoSICOB_caixa:=parametro;
end;


procedure TObjCONVENIOSBOLETO.Opcoes(Pcodigo: string);
begin
     with FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Replicar Conv�nio');
          Showmodal;
          
          if (Tag=0)
          Then exit;

          case RgOpcoes.ItemIndex of

            0:Self.replicarconvenio(pcodigo);

          End;
     End;
end;

procedure TObjCONVENIOSBOLETO.replicarconvenio(pcodigo: string);
begin
     if (Pcodigo='')
     Then Begin
               MensagemAviso('Escolha o conv�nio');
               exit;
     End;

     if (Self.LocalizaCodigo(pcodigo)=False)
     then Begin
               MensagemAviso('Conv�nio n�o encontrado');
               exit;
     End;

     Self.TabelaparaObjeto;

     Self.CODIGO:='0';
     Self.Status:=dsinsert;

     if (Self.Salvar(True)=False)
     Then Begin
               MensagemErro('Erro na tentativa de Salvar o Novo conv�nio');
               exit;
     End;
     mensagemaviso('C�digo do Novo Conv�nio '+Self.CODIGO);
     exit;
end;

function TObjCONVENIOSBOLETO.Get_codigotransacao_unibanco: string;
begin
     Result:=Self.codigotransacao_unibanco;
end;

function TObjCONVENIOSBOLETO.Get_numerocliente_UNIBANCO: string;
begin
     Result:=Self.numerocliente_UNIBANCO;
end;

function TObjCONVENIOSBOLETO.Get_posicoes_28_a_29_UNIBANCO: string;
begin
     Result:=Self.posicoes_28_a_29_UNIBANCO;
end;

procedure TObjCONVENIOSBOLETO.Submit_codigotransacao_unibanco(
  parametro: string);
begin
     Self.codigotransacao_unibanco:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_numerocliente_UNIBANCO(
  parametro: string);
begin
     Self.numerocliente_UNIBANCO:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_posicoes_28_a_29_UNIBANCO(
  parametro: string);
begin
     Self.posicoes_28_a_29_UNIBANCO:=Parametro;
end;

function TObjCONVENIOSBOLETO.get_posicao20_sicred: string;
begin
     Result:=Self.posicao20_sicred;
end;

function TObjCONVENIOSBOLETO.get_posicao21_sicred: string;
begin
     Result:=Self.posicao21_sicred;
end;

function TObjCONVENIOSBOLETO.get_posicao31_34_sicred: string;
begin
     Result:=Self.posicao31_34_sicred;
end;

function TObjCONVENIOSBOLETO.get_posicao35_36_sicred: string;
begin
     Result:=Self.posicao35_36_sicred;
end;

function TObjCONVENIOSBOLETO.get_posicao37_41_sicred: string;
begin
     Result:=Self.posicao37_41_sicred;
end;

function TObjCONVENIOSBOLETO.get_posicao42_sicred: string;
begin
     Result:=Self.posicao42_sicred;
end;

function TObjCONVENIOSBOLETO.get_posicao43_sicred: string;
begin
     Result:=Self.posicao43_sicred;
end;

function TObjCONVENIOSBOLETO.get_BYTE_NOSSONUMERO_SICRED: string;
begin
     Result:=Self.BYTE_NOSSONUMERO_SICRED;
end;


procedure TObjCONVENIOSBOLETO.Submit_posicao20_sicred(parametro: string);
begin
     Self.posicao20_sicred:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_posicao21_sicred(parametro: string);
begin
     Self.posicao21_sicred:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_posicao31_34_sicred(parametro: string);
begin
     Self.posicao31_34_sicred:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_posicao35_36_sicred(parametro: string);
begin
     Self.posicao35_36_sicred:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_posicao37_41_sicred(parametro: string);
begin
     Self.posicao37_41_sicred:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_posicao42_sicred(parametro: string);
begin
     Self.posicao42_sicred:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_posicao43_sicred(parametro: string);
begin
     Self.posicao43_sicred:=Parametro;
end;


procedure TObjCONVENIOSBOLETO.Submit_BYTE_NOSSONUMERO_SICRED(parametro: string);
begin
     Self.BYTE_NOSSONUMERO_SICRED:=Parametro;
end;


function TObjCONVENIOSBOLETO.get_codigocedente_HSBC: string;
begin
      result := self.codigocedente_HSBC;
end;

function TObjCONVENIOSBOLETO.get_codigoCNR_HSBC: string;
begin
      result := self.codigoCNR_HSBC;
end;

function TObjCONVENIOSBOLETO.get_tipoidentificador_HSBC: string;
begin
      result := self.tipoidentificador_HSBC;
end;

procedure TObjCONVENIOSBOLETO.Submit_codigocedente_HSBC(parametro: string);
begin
      self.codigocedente_HSBC := parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_codigoCNR_HSBC(parametro: string);
begin
      self.codigoCNR_HSBC := parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_tipoidentificador_HSBC(
  parametro: string);
begin
      Self.tipoidentificador_HSBC := parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBem: String;
begin
    Result:=Self.CobreBem;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemArquivoLicenca: String;
begin
    Result:=Self.CobreBemArquivoLicenca;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemArquivoLogotipo: String;
begin
    Result:=Self.CobreBemArquivoLogotipo;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemArquivoRemessa: String;
begin
    Result:=Self.CobreBemArquivoRemessa;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemArquivoRetorno: String;
begin
    Result:=Self.CobreBemArquivoRetorno;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemCaminhoImagensCodBarras: String;
begin
    Result:=Self.CobreBemCaminhoImagensCodBarras;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemCodigoAgencia: String;
begin
    Result:=Self.CobreBemCodigoAgencia;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemCodigoCedente: String;
begin
    Result:=Self.CobreBemCodigoCedente;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemDiretorioRemessa: String;
begin
    Result:=Self.CobreBemDiretorioRemessa;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemDiretorioRetorno: String;
begin
    Result:=Self.CobreBemDiretorioRetorno;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemFimNossoNumero: String;
begin
    Result:=Self.CobreBemFimNossoNumero;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemInicioNossoNumero: String;
begin
    Result:=Self.CobreBemInicioNossoNumero;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemLayoutRemessa: String;
begin
    Result:=Self.CobreBemLayoutRemessa;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemLayoutRetorno: String;
begin
    Result:=Self.CobreBemLayoutRetorno;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemNumeroContaCorrente: String;
begin
    Result:=Self.CobreBemNumeroContaCorrente;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemOutroDadoConfiguracao1: String;
begin
    Result:=Self.CobreBemOutroDadoConfiguracao1;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemProximoNossoNumero: String;
begin
    Result:=Self.CobreBemProximoNossoNumero;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBem(Parametro: String);
begin
    Self.CobreBem := Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemArquivoLicenca(
  Parametro: String);
begin
    Self.CobreBemArquivoLicenca:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemArquivoLogotipo(
  Parametro: String);
begin
    Self.CobreBemArquivoLogotipo:=parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemArquivoRemessa(
  Parametro: String);
begin
    Self.CobreBemArquivoRemessa:=parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemArquivoRetorno(
  Parametro: String);
begin
    Self.CobreBemArquivoRetorno:=parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemCaminhoImagensCodBarras(
  Parametro: String);
begin
    Self.CobreBemCaminhoImagensCodBarras:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemCodigoAgencia(
  Parametro: String);
begin
    Self.CobreBemCodigoAgencia:=Parametro
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemCodigoCedente(
  Parametro: String);
begin
    Self.CobreBemCodigoCedente:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemDiretorioRemessa(
  Parametro: String);
begin
    Self.CobreBemDiretorioRemessa:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemDiretorioRetorno(
  Parametro: String);
begin
    Self.CobreBemDiretorioRetorno:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemFimNossoNumero(
  Parametro: String);
begin
    Self.CobreBemFimNossoNumero:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemInicioNossoNumero(
  Parametro: String);
begin
    Self.CobreBemInicioNossoNumero:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemLayoutRemessa(
  Parametro: String);
begin
    Self.CobreBemLayoutRemessa:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemLayoutRetorno(
  Parametro: String);
begin
    Self.CobreBemLayoutRetorno:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemNumeroContaCorrente(
  Parametro: String);
begin
    Self.CobreBemNumeroContaCorrente:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemOutroDadoConfiguracao1(
  Parametro: String);
begin
    Self.CobreBemOutroDadoConfiguracao1:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemProximoNossoNumero(
  Parametro: String);
begin
    Self.CobreBemProximoNossoNumero:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemLayoutBoleto: String;
begin
    Result:= Self.CobreBemLayoutBoleto;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemLayoutBoleto(Parametro: String);
begin
    Self.CobreBemLayoutBoleto:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemPercJurosDiaAtraso: String;
begin
    Result:=Self.CobreBemPercJurosDiaAtraso;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemPercMultaAtraso: String;
begin
    Result:=Self.CobreBemPercMultaAtraso;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemPercJurosDiaAtraso(
  Parametro: String);
begin
    Self.CobreBemPercJurosDiaAtraso:=Parametro;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemPercMultaAtraso(
  Parametro: String);
begin
    Self.CobreBemPercMultaAtraso:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemPercentualDesconto: String;
begin
    Result:=Self.CobreBemPercentualDesconto;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemPercentualDesconto(
  parametro: String);
begin
    Self.CobreBemPercentualDesconto:=parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemBancoImprimeBoleto: String;
begin
    Result:=Self.CobreBemBancoImprimeBoleto;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemBancoImprimeBoleto(
  Parametro: String);
begin
    Self.CobreBemBancoImprimeBoleto:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemDiasProtesto: String;
begin
    Result:=Self.CobreBemDiasProtesto;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemDiasProtesto(
  Parametro: String);
begin
    Self.CobreBemDiasProtesto:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemLimiteVencimento: String;
begin
    Result:=Self.CobreBemLimiteVencimento;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemLimiteVencimento(
  Parametro: String);
begin
    Self.CobreBemLimiteVencimento:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemNumeroRemessa: String;
begin
    Result:=Self.CobreBemNumeroRemessa;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemNumeroRemessa(Parametro: String);
begin
    Self.CobreBemNumeroRemessa:=Parametro;
end;

function TObjCONVENIOSBOLETO.Get_CobreBemOutroDadoConfiguracao2: String;
begin
    Result:=Self.CobreBemOutroDadoConfiguracao2;
end;

procedure TObjCONVENIOSBOLETO.Submit_CobreBemOutroDadoConfiguracao2(
  Parametro: String);
begin
    Self.CobreBemOutroDadoConfiguracao2:=Parametro;
end;

end.



