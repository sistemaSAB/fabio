unit UEscolheBoletos;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids,UobjBoletoBancario, StdCtrls, Buttons, ExtCtrls;

type
  TFEscolheBoletos = class(TForm)
    Grid: TStringGrid;
    Btsair: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    edtboletoatual: TEdit;
    btok: TBitBtn;
    btajusta: TBitBtn;
    edtcodigoconvenio: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtboletoatualKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btokClick(Sender: TObject);
    procedure btajustaClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtsairClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEscolheBoletos: TFEscolheBoletos;
  ObjBoletos:TObjBoletoBancario;

implementation

uses Upesquisa, UBoletoBancario, UessencialGlobal;



{$R *.DFM}

procedure TFEscolheBoletos.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     
     Try
           ObjBoletos:=TObjBoletoBancario.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o dos Boletos Banc�rios!',mterror,[mbok],0);
           Self.close;
           exit;
     End;
     edtboletoatual.setfocus;
     PegaFiguraBotao(Btsair,'BOTAOSAIR.BMP');
     
end;

procedure TFEscolheBoletos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (ObjBoletos<>nil)
     Then ObjBoletos.Free;
end;

procedure TFEscolheBoletos.edtboletoatualKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

var
   FPesquisaLocal:Tfpesquisa;
Begin

    If (key <> vk_f9)
    then exit;

    Try
        FPesquisaLocal:=Tfpesquisa.create(Self);
        If (FpesquisaLocal.PreparaPesquisa(ObjBoletos.get_pesquisanaousados(edtcodigoconvenio.text),ObjBoletos.get_titulopesquisa,Fboletobancario)=True)
        Then Begin
                If (FpesquisaLocal.showmodal=mrok)
                Then edtboletoatual.text:=FpesquisaLocal.QueryPesq.fieldbyname(Objboletos.RetornaCampoCodigo).asstring;
             End;
    Finally
           FreeandNil(FPesquisaLocal);
    End;

end;

procedure TFEscolheBoletos.btokClick(Sender: TObject);
begin
     grid.Cells[2,grid.row]:=edtboletoatual.text;
     If (Grid.row<(grid.rowcount-1))//se naum for o ultimo
     Then grid.row:=grid.row+1;//vai para o proximo
     edtboletoatual.setfocus;
end;

procedure TFEscolheBoletos.btajustaClick(Sender: TObject);
var
strlist:TStringList;
cont:integer;
begin
     If (edtboletoatual.text='')
     Then exit;

     Try
        strlist:=TStringLIst.create;
     Except
           Messagedlg('Erro na tentativa de  cria��o da StringList que ir� conter os pr�ximos Boletos!',mterror,[mbok],0);
           exit;
     End;
     Try
             grid.Cells[2,grid.row]:=edtboletoatual.text;
             strlist.clear;
             //pega os proximosa boletos a partir do atual para preencher o resto do grid
             ObjBoletos.pegaproximosnaousados(edtboletoatual.text,StrList,grid.rowcount-grid.row-1);
             for cont:=0 to strlist.count-1 do
             Begin
                  Grid.cells[2,Grid.Row+1]:=strlist[cont];
                  Grid.Row:=Grid.Row+1;
             End;


     Finally
            edtboletoatual.setfocus;
            freeandnil(strlist);
     End;
end;

procedure TFEscolheBoletos.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFEscolheBoletos.BtsairClick(Sender: TObject);
begin
     Self.Close;
end;

end.
