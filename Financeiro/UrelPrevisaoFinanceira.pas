unit UrelPrevisaoFinanceira;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, UObjTitulo, Mask, ComCtrls, Buttons,
  DB, IBCustomDataSet, IBQuery;

type
  TFrelPrevisaoFinanceira = class(TForm)
    Guia: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    StrgPrevisaoMensal: TStringGrid;
    StrgPrevisao: TStringGrid;
    Panel2: TPanel;
    BtGerarmensal: TButton;
    PaneIRodape: TPanel;
    memorodape: TMemo;
    btsalvardados: TButton;
    memosalvadados: TMemo;
    SaveDialog: TSaveDialog;
    PanelSuperior: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdtPortador: TEdit;
    EdtExcluirContasGerenciais: TEdit;
    edtdatalimite: TMaskEdit;
    edtdatainicial: TMaskEdit;
    edtexcluirsubconta: TEdit;
    MemoCabecalho: TMemo;
    btGerarDiario: TButton;
    CheckPrevisao: TCheckBox;
    barrastatus: TPanel;
    Label6: TLabel;
    edtexcluititulo: TEdit;
    btsalvarcsv: TButton;
    edtpesquisa: TMaskEdit;
    lbtipocoluna: TListBox;
    btatualizasaldo: TButton;
    btadicionalinha: TBitBtn;
    lbdatas: TLabel;
    TabSheet3: TTabSheet;
    Panel1: TPanel;
    lbdatasrealizado: TLabel;
    BTGerar_realizado: TButton;
    Btsalvadados_realizado: TButton;
    Memo1: TMemo;
    BTAtualizaSaldo_Realizado: TButton;
    btadicionalinha_realizado: TBitBtn;
    STRG_Realizado: TStringGrid;
    QueryRealizado: TIBQuery;
    procedure FormShow(Sender: TObject);
    procedure btGerarDiarioClick(Sender: TObject);
    procedure EdtPortadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtExcluirContasGerenciaisKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure StrgPrevisaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StrgPrevisaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtexcluirsubcontaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtGerarmensalClick(Sender: TObject);
    procedure btsalvardadosClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StrgPrevisaoMensalDrawCell(Sender: TObject; ACol,
      ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure edtexcluitituloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btsalvarcsvClick(Sender: TObject);
    procedure edtpesquisaKeyPress(Sender: TObject;
      var Key: Char);
    procedure StrgPrevisaoEnter(Sender: TObject);
    procedure btatualizasaldoClick(Sender: TObject);
    procedure btadicionalinhaClick(Sender: TObject);
    procedure StrgPrevisaoMensalKeyPress(Sender: TObject; var Key: Char);
    procedure BTGerar_realizadoClick(Sender: TObject);
    procedure BTAtualizaSaldo_RealizadoClick(Sender: TObject);
    procedure btadicionalinha_realizadoClick(Sender: TObject);
    procedure STRG_RealizadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Btsalvadados_realizadoClick(Sender: TObject);
    procedure StrgPrevisaoMensalKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    PsaldoInicial:Currency;
    PdataInicial,PdataFinal,Pportadores,PExcluirContas,PExcluirSubContas:String;

    procedure AtualizaSaldo;
    Procedure CalculaSaldoMensal(PGrid:TStringGrid);
    procedure AdicionaLInha(Pgrid: TStringGrid);
    procedure CalculaSaldoMensal_Realizado(PGrid: TStringGrid);
    procedure SalvaDadosCSV(Pgrid: TStringGrid);
  public
    { Public declarations }
    ObjTitulo:tobjTitulo;
  end;

var
  FrelPrevisaoFinanceira: TFrelPrevisaoFinanceira;

implementation

uses UessencialGlobal, UTitulo, UprevisaoFinanceira, 
  Uformata_String_Grid, UDataModulo, UmostraStringGrid;

{$R *.dfm}

procedure TFrelPrevisaoFinanceira.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     guia.ActivePageIndex:=0;
     Edtdatainicial.SetFocus;
     barrastatus.caption:='F5 - Cadastro de T�tulos | F6 - Previs�o Financeira | F3 - Mostra lan�amentos do realizado';

     edtpesquisa.enabled:=True;
     edtpesquisa.Visible:=False;
end;

procedure TFrelPrevisaoFinanceira.btGerarDiarioClick(Sender: TObject);
begin
    Self.Objtitulo.GeraFormularioPrevisaoFinanceira(edtdatainicial.text,edtdatalimite.Text,EdtPortador.text,EdtExcluirContasGerenciais.Text,edtexcluirsubconta.Text,MemoCabecalho.Lines,memorodape.Lines,StrgPrevisao,PsaldoInicial,edtexcluititulo.text,lbtipocoluna.items);
    StrgPrevisao.col:=2;
    Ordena_StringGrid(StrgPrevisao,lbtipocoluna.Items);
    AjustaLArguraColunaGrid(StrgPrevisao);
    Self.AtualizaSaldo;
end;

procedure TFrelPrevisaoFinanceira.EdtPortadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjTitulo.edtportadorKeyDown_PV(sender,key,shift);
end;

procedure TFrelPrevisaoFinanceira.EdtExcluirContasGerenciaisKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    Self.objtitulo.EdtcontagerencialKeyDown(sender,key,shift);
end;

procedure TFrelPrevisaoFinanceira.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFrelPrevisaoFinanceira.AtualizaSaldo;
var
SaldoAtual:Currency;
Pvalor,pdata,pdata2:String;
ano,mes,dia,mesanterior:word;
cont:integer;
begin

     memorodape.Lines.clear;
     SaldoAtual:=Self.Psaldoinicial;



     for cont:=1 to StrgPrevisao.rowcount-1 do
     Begin

          Try
              pdata:=trim(StrgPrevisao.Cells[2,cont]);//coluna 2 � a data

              DecodeDate(strtodate(pdata),ano,mes,dia);//extraio dia, mes ano

              if (cont=1)
              Then mesanterior:=mes;//primeira linha

              if (mesanterior<>mes)
              Then Begin
                      //totalizo a data anterior
                      memorodape.Lines.add(StrgPrevisao.Cells[2,cont-1]+' '+StrgPrevisao.Cells[4,cont-1]);
                      mesanterior:=mes;
              End;
          Except

          End;

          Pvalor:=trim(StrgPrevisao.Cells[3,cont]);//coluna 3 � o valor
          Pvalor:=tira_ponto(pvalor);

          Try
              Strtofloat(Pvalor);
              SaldoAtual:=SaldoAtual+Strtofloat(Pvalor);
          Except

          End;

          StrgPrevisao.Cells[4,cont]:=completapalavra_a_esquerda(formata_valor(SaldoAtual),12,' ');
     End;
     memorodape.Lines.add('SALDO PREVISTO FINAL R$ '+Formata_valor(Saldoatual));
     
end;

procedure TFrelPrevisaoFinanceira.StrgPrevisaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
Cont:Integer;
begin
     if key=vk_delete
     Then Begin
               if (Messagedlg('Certeza que deseja excluir a linha atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
               Then exit;

               for cont:=StrgPrevisao.row+1 to StrgPrevisao.rowcount-1 do
               begin
                    StrgPrevisao.Cells[0,cont-1]:=StrgPrevisao.Cells[0,cont];
                    StrgPrevisao.Cells[1,cont-1]:=StrgPrevisao.Cells[1,cont];
                    StrgPrevisao.Cells[2,cont-1]:=StrgPrevisao.Cells[2,cont];
                    StrgPrevisao.Cells[3,cont-1]:=StrgPrevisao.Cells[3,cont];
                    StrgPrevisao.Cells[4,cont-1]:=StrgPrevisao.Cells[4,cont];
               End;
               StrgPrevisao.rowcount:=StrgPrevisao.rowcount-1;
               Self.AtualizaSaldo;
     End;

     if (key=vk_f12) and (StrgPrevisao.col<>4)//nao pode ser a coluna de saldo 
     Then Begin
              Ordena_StringGrid(StrgPrevisao,lbtipocoluna.Items);
              Self.AtualizaSaldo
     End;


end;

procedure TFrelPrevisaoFinanceira.StrgPrevisaoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if (key=#32)
     Then Begin
               PreparaPesquisa_StringGrid(StrgPrevisao,edtpesquisa,lbtipocoluna.Items);
     End;

end;

procedure TFrelPrevisaoFinanceira.edtexcluirsubcontaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    Self.objtitulo.edtsubcontagerencialKeyDown_PV(sender,key,shift);
end;

procedure TFrelPrevisaoFinanceira.BtGerarmensalClick(Sender: TObject);
begin

    //Backup dos valores para serem usados depois para
    //detalhas as contas e sub-contas nessas datas
    // para evitar de mexer nos edits da Aba 1 e
    //trazer valores errados na hora de detalhar durante a an�lise
    PdataInicial:=edtdatainicial.text;
    PdataFinal:=edtdatalimite.Text;
    lbdatas.caption:=PdataInicial+' a '+Pdatafinal;
    Pportadores:=EdtPortador.text;
    PExcluirContas:=EdtExcluirContasGerenciais.Text;
    PExcluirSubContas:=edtexcluirsubconta.Text;
    //********************************************
    Self.Objtitulo.GeraFormularioPrevisaoFinanceira_mensal(edtdatainicial.text,edtdatalimite.Text,EdtPortador.text,EdtExcluirContasGerenciais.Text,edtexcluirsubconta.Text,MemoCabecalho.Lines,memorodape.Lines,StrgPrevisaoMensal,PsaldoInicial,checkprevisao.checked);
    Self.CalculaSaldoMensal(StrgPrevisaoMensal);
    AjustaLArguraColunaGrid(StrgPrevisaoMensal);
end;

procedure TFrelPrevisaoFinanceira.btsalvardadosClick(Sender: TObject);
Begin
     Self.SalvaDadosCSV(Self.StrgPrevisaoMensal);
end;

procedure TFrelPrevisaoFinanceira.SalvaDadosCSV(Pgrid:TStringGrid);
var
linha,coluna,cont,cont2:integer;
strlinha:string;
begin
     memosalvadados.Lines.clear;


     for cont:=0 to PgRID.RowCount-1 do
     Begin
          strlinha:='';

          for cont2:=0 to PgRID.ColCount-1 do
          Begin
               strlinha:=strlinha+';'+PgRID.Cells[cont2,cont];
          End;
          memosalvadados.lines.Add(strlinha);
     End;

     if (savedialog.execute)
     then Begin
                memosalvadados.lines.SaveToFile(SaveDialog.FileName);
                MensagemAviso('Conclu�do');
     End;

End;




procedure TFrelPrevisaoFinanceira.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
Ftituloxx:Tftitulo;
FprevisaoFinanceiraxx:TfPrevisaoFinanceira;
begin
     case key of

      VK_F5:Begin
                 Try
                    Ftituloxx:=Tftitulo.create(nil);
                 Except
                       Mensagemerro('Erro na tentativa de criar o Formul�rio de T�tulo');
                       exit;
                 End;

                 Try
                    Ftituloxx.showmodal;
                 Finally
                        freeandnil(Ftituloxx);
                 End;
       End;

       VK_F6:Begin
                 Try
                    Fprevisaofinanceiraxx:=Tfprevisaofinanceira.create(nil);
                 Except
                       Mensagemerro('Erro na tentativa de criar o Formul�rio de Previs�o Financeira');
                       exit;
                 End;

                 Try
                    Fprevisaofinanceiraxx.showmodal;
                 Finally
                        freeandnil(Fprevisaofinanceiraxx);
                 End;
       End;

     End;
end;

procedure TFrelPrevisaoFinanceira.StrgPrevisaoMensalDrawCell(
  Sender: TObject; ACol, ARow: Integer; Rect: TRect;
  State: TGridDrawState);
begin
    If (Sender as TStringGrid).Cells[(Sender as TStringGrid).colcount-1,Arow]='T'//conta titulo
    then begin
          (Sender as TStringGrid).canvas.brush.color :=RGB(255,255,153);
          (Sender as TStringGrid).canvas.FillRect(rect);
          (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
    end
    else Begin
              If (Sender as TStringGrid).Cells[(Sender as TStringGrid).colcount-1,Arow]='P'//previsao
              Then begin
                      (Sender as TStringGrid).canvas.brush.color :=RGB(204,255,255);
                      (Sender as TStringGrid).canvas.FillRect(rect);
                      (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
              End
              Else Begin
                        If (Sender as TStringGrid).Cells[(Sender as TStringGrid).colcount-1,Arow]='M'//manual
                        Then begin
                                (Sender as TStringGrid).canvas.brush.color :=RGB(178,162,199);
                                (Sender as TStringGrid).canvas.FillRect(rect);
                                (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
                        End
                        else Begin
                                  If (Sender as TStringGrid).Cells[(Sender as TStringGrid).colcount-1,Arow]='S'//manual
                                  Then begin
                                          (Sender as TStringGrid).canvas.brush.color :=clwhite;
                                          (Sender as TStringGrid).canvas.FillRect(rect);
                                          (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
                                  End
                                  Else BEgin
                                            If (Arow=Self.StrgPrevisaoMensal.RowCount-1)//ultima linha
                                            Then begin
                                                    (Sender as TStringGrid).canvas.brush.color :=RGB(255,192,0);
                                                    (Sender as TStringGrid).canvas.FillRect(rect);
                                                    (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
                                            End
                                            Else Begin
                                                      If (Arow=0)//primeira Linha
                                                      Then begin
                                                              (Sender as TStringGrid).canvas.brush.color :=clSilver;
                                                              (Sender as TStringGrid).canvas.FillRect(rect);
                                                              (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
                                                      End
                                            End;
                                  End;
                        End;
              End;
    End;

end;

procedure TFrelPrevisaoFinanceira.edtexcluitituloKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjTitulo.edttituloKeyDown_PV(sender,key,shift);
end;

procedure TFrelPrevisaoFinanceira.btsalvarcsvClick(Sender: TObject);
var
linha,coluna,cont,cont2:integer;
strlinha:string;
begin
     memosalvadados.Lines.clear;

     for cont:=0 to StrgPrevisao.RowCount-1 do
     Begin
          strlinha:='';

          for cont2:=0 to StrgPrevisao.ColCount-1 do
          Begin
               strlinha:=strlinha+';'+StrgPrevisao.Cells[cont2,cont];
          End;
          memosalvadados.lines.Add(strlinha);
     End;

     if (savedialog.execute)
     then Begin
                memosalvadados.lines.SaveToFile(SaveDialog.FileName);
                MensagemAviso('Conclu�do');
     End;

end;

procedure TFrelPrevisaoFinanceira.edtpesquisaKeyPress(
  Sender: TObject; var Key: Char);
begin
   if key =#27//ESC
   then Begin
           edtpesquisa.Visible := false;
           StrgPrevisao.SetFocus;
           exit;
   End;

   if key=#13
   Then Begin
            Pesquisa_StringGrid(StrgPrevisao,edtpesquisa,lbtipocoluna.Items);
            if (edtpesquisa.Visible=False)
            Then StrgPrevisao.SetFocus;
   End;

end;

procedure TFrelPrevisaoFinanceira.StrgPrevisaoEnter(Sender: TObject);
begin
    edtpesquisa.Visible:=false;
end;

procedure TFrelPrevisaoFinanceira.CalculaSaldoMensal(PGrid:TStringGrid);
var
coluna,linha,plinhatotal:integer;
psaldoacumulado:currency;
begin
    //primeiramente atualizo o saldo das Contas e SubContas
    plinhatotal:=0;
    psaldoacumulado:=0;

    //  0   1  2  3  4   5
    //nome 11 12 CG SUB TIT = 6 colunas
    //percorre da 1 at� 2


    for coluna:=1 to PGRID.ColCount-4 do
    Begin
         for linha:=1 to PGRID.RowCount-2 do//para na penultima
         Begin
              if (PGRID.Cells[PGRID.colcount-1,linha]='T')//as abaixo serao
              Then Begin
                        plinhatotal:=linha;
                        psaldoacumulado:=0;
              End
              Else Begin
                        if (PGRID.Cells[coluna,linha]<>'')
                        Then Begin
                                  Try
                                     if (plinhatotal<>0)
                                     Then Begin
                                               //soma a c�lula atual
                                               psaldoacumulado:=psaldoacumulado+strtocurr(tira_ponto(PGRID.Cells[coluna,linha]));
                                               //grava na c�lula titulo
                                               PGRID.Cells[coluna,plinhatotal]:=formata_valor(psaldoacumulado);
                                     End;
                                  Except

                                  End;
                        End;
              End;
         End;
    End;



     //o saldo dos bancos envolvidos na previs�o est� na primeira coluna �ltima linha
    psaldoacumulado:=strtocurr(tira_ponto(PGRID.Cells[0,PGRID.rowcount-1]));

    for coluna:=1 to PGRID.ColCount-4 do
    Begin
         for linha:=1 to PGRID.RowCount-2 do//para na penultima
         Begin
              if (PGRID.Cells[PGRID.ColCount-1,linha]='T')//� a soma das linhas
              Then Begin
                        Try
                            if (PGRID.Cells[coluna,linha]<>'')
                            then Begin
                                    psaldoacumulado:=psaldoacumulado+strtocurr(tira_ponto(PGRID.Cells[coluna,linha]));
                                    PGRID.Cells[coluna,PGRID.rowcount-1]:=formata_valor(psaldoacumulado);
                            End;
                        Except
                              on e:exception do
                              Begin
                                   Mensagemerro(e.message+#13+'Valor: '+PGRID.Cells[coluna,linha]);

                              End;
                        End;
              End;
         End;
    End;
end;


procedure TFrelPrevisaoFinanceira.CalculaSaldoMensal_Realizado(PGrid:TStringGrid);
var
coluna,linha,plinhatotal:integer;
psaldoacumulado:currency;
begin
    //primeiramente atualizo o saldo das Contas e SubContas
    plinhatotal:=0;
    psaldoacumulado:=0;

    //  0   1  2  3  4   5
    //nome 11 12 CG SUB TIT = 6 colunas
    //percorre da 1 at� 2


    for coluna:=1 to PGRID.ColCount-4 do
    Begin
         //percorre linha a linha da coluna
         
         for linha:=1 to PGRID.RowCount-2 do//para na penultima
         Begin
              if (PGRID.Cells[PGRID.colcount-1,linha]='T')//as abaixo serao
              Then Begin
                        plinhatotal:=linha;
                        psaldoacumulado:=0;
              End
              Else Begin
                        if (PGRID.Cells[coluna,linha]<>'')
                        Then Begin
                                  Try
                                     if (plinhatotal<>0)
                                     Then Begin
                                               //soma a c�lula atual
                                               psaldoacumulado:=psaldoacumulado+strtocurr(tira_ponto(PGRID.Cells[coluna,linha]));
                                               //grava na c�lula titulo
                                               PGRID.Cells[coluna,plinhatotal]:=formata_valor(psaldoacumulado);
                                     End;
                                  Except

                                  End;
                        End;
              End;
         End;
    End;


    //Neste Caso o saldo da ultima linha � apenas o saldo
    //que envolvem receitas-despesas do m�s
    //n�o tem liga��o com o Saldo dos Bancos Atuais
    //nem acumula valor m�s a m�s
    
    for coluna:=1 to PGRID.ColCount-4 do
    Begin
         //a cada coluna(mes) zero o saldo
         psaldoacumulado:=0;

         for linha:=1 to PGRID.RowCount-2 do//para na penultima
         Begin
              if (PGRID.Cells[PGRID.ColCount-1,linha]='T')//� a soma das linhas
              Then Begin
                        Try
                            if (PGRID.Cells[coluna,linha]<>'')
                            then Begin
                                    psaldoacumulado:=psaldoacumulado+strtocurr(tira_ponto(PGRID.Cells[coluna,linha]));
                                    PGRID.Cells[coluna,PGRID.rowcount-1]:=formata_valor(psaldoacumulado);
                            End;
                        Except
                              on e:exception do
                              Begin
                                   Mensagemerro(e.message+#13+'Valor: '+PGRID.Cells[coluna,linha]);

                              End;
                        End;
              End;
         End;
    End;
end;

procedure TFrelPrevisaoFinanceira.btatualizasaldoClick(Sender: TObject);
begin
    Self.CalculaSaldoMensal(StrgPrevisaoMensal);
    AjustaLArguraColunaGrid(StrgPrevisaoMensal);
end;

procedure TFrelPrevisaoFinanceira.btadicionalinhaClick(Sender: TObject);
Begin
     Self.AdicionaLInha(Self.StrgPrevisaoMensal);
End;

procedure TFrelPrevisaoFinanceira.AdicionaLInha(Pgrid:TStringGrid);
var
prow,cont:integer;
begin
     if (PGRID.Row>1)//nao pode ser a primeira
     Then Begin
               prow:=PGRID.Row;//pego a linha atual
               //aumento uma linha
               PGRID.RowCount:=PGRID.RowCount+1;

               for cont:=PGRID.RowCount-1 downto prow do
               Begin
                    //a partir da ultima copio a anterior
                    PGRID.rows[cont]:=PGRID.rows[cont-1];
               End;

               for cont:=0 to PGRID.ColCount-1 do
               Begin
                    //zero a linha que eu estava antes que sera a linha nova
                    PGRID.cells[cont,prow]:='';
               End;
               //indico na ultima linha que � uma linha Manual
               PGRID.cells[PGRID.ColCount-1,prow]:='M';
     End;
     AjustaLArguraColunaGrid(PGRID);
end;

procedure TFrelPrevisaoFinanceira.StrgPrevisaoMensalKeyPress(
  Sender: TObject; var Key: Char);
begin
     if key=#13
     Then TStringGrid(Sender).SetFocus;

     if ((key=char(VK_LEFT)) or (key=char(VK_Right))
     or (key=char(VK_DOWN)) or (key=char(VK_UP)) or (key=char(VK_TAB)))
     Then exit;

     if (
        (TStringGrid(Sender).Col=(TStringGrid(Sender).ColCount-1))
        )
     Then Key:=#0; //se for a ultima coluna ou nao for uma linha manual nao pode alterar
end;

procedure TFrelPrevisaoFinanceira.BTGerar_realizadoClick(Sender: TObject);
begin
   //***************************************************************************
    PdataInicial:=edtdatainicial.text;
    PdataFinal:=edtdatalimite.Text;
    lbdatasrealizado.caption:=PdataInicial+' a '+Pdatafinal;
   //***************************************************************************
    Pportadores:=EdtPortador.text;
   //**************************************************************************
    PExcluirContas:=EdtExcluirContasGerenciais.Text;
    PExcluirSubContas:=edtexcluirsubconta.Text;
   //**************************************************************************
    Self.Objtitulo.GeraFormularioPrevisaoFinanceira_Realizado(edtdatainicial.text,edtdatalimite.Text,EdtPortador.text,EdtExcluirContasGerenciais.Text,edtexcluirsubconta.Text,MemoCabecalho.Lines,memorodape.Lines,STRG_Realizado,PsaldoInicial);
   //**************************************************************************
    Self.CalculaSaldoMensal_Realizado(Self.STRG_Realizado);
    AjustaLArguraColunaGrid(STRG_Realizado);
    //**************************************************************************
End;

procedure TFrelPrevisaoFinanceira.BTAtualizaSaldo_RealizadoClick(Sender: TObject);
begin
    Self.CalculaSaldoMensal_Realizado(STRG_Realizado);
    AjustaLArguraColunaGrid(STRG_Realizado);
end;

procedure TFrelPrevisaoFinanceira.btadicionalinha_realizadoClick(Sender: TObject);
begin
     Self.AdicionaLInha(Self.STRG_Realizado);
end;

procedure TFrelPrevisaoFinanceira.STRG_RealizadoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
Plinha,pcoluna:integer;
psubconta,pdata,temp:string;
begin
     if (key=vk_f3)//Listar o que ocorreu no lan�amento
     Then Begin
               Plinha:=Self.STRG_Realizado.Row;
               Pcoluna:=Self.STRG_Realizado.Col;

               if (Self.STRG_Realizado.Cells[Self.STRG_Realizado.ColCount-1,plinha]<>'S')
               then exit;

               psubconta:=Self.STRG_Realizado.Cells[Self.STRG_Realizado.ColCount-2,plinha];

               temp:=Self.STRG_Realizado.Cells[pcoluna,0];
               pdata:='01/'+copy(temp,3,length(temp)-2);


               if (temp[1]='A')
               Then Self.ObjTitulo.MostraPendencias_mes_subconta(psubconta,pdata)//a realizar;
               Else Self.ObjTitulo.MostraLancamentos_mes_subconta(psubconta,pdata);//realizado

     End;
end;

procedure TFrelPrevisaoFinanceira.Btsalvadados_realizadoClick(Sender: TObject);
begin
   Self.SalvaDadosCSV(Self.STRG_Realizado);
end;

procedure TFrelPrevisaoFinanceira.StrgPrevisaoMensalKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
plinha,pcoluna:integer;
psubconta,temp,pdata:string;  
begin
     if (key=vk_f3)//Listar o que tem lan�ado para esta data
     Then Begin
               Plinha:=Self.STRGPrevisaoMensal.Row;
               Pcoluna:=Self.STRGPrevisaoMensal.Col;

               //verificando na ultima coluna se � uma linha de Titulo
               if (Self.STRGPrevisaoMensal.Cells[Self.STRGPrevisaoMensal.ColCount-1,plinha]<>'S')
               then exit;

               psubconta:=Self.STRGPrevisaoMensal.Cells[Self.STRGPrevisaoMensal.ColCount-2,plinha];

               temp:=Self.STRGPrevisaoMensal.Cells[pcoluna,0];
               pdata:='01/'+temp;//montando a data pelo titulo da coluna

               Self.ObjTitulo.MostraPendencias_mes_subconta(psubconta,pdata)//a realizar;
     End;

end;

end.
