unit UTitulo;

interface

uses
  UessencialGlobal, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPendencia,
  Grids, ULancamentoFR;

type
  TFtitulo = class(TForm)
    Guia: TTabbedNotebook;
    Label1: TLabel;                            
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    LbPortador: TLabel;
    LbContaGerencial: TLabel;
    Label7: TLabel;
    LbNomeCredorDevedor: TLabel;
    LbCRCP: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    combocredordevedor: TComboBox;
    edtcodigocredordevedor: TEdit;
    Panel1: TPanel;
    btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    btpesquisasemsaldo: TBitBtn;
    edtemissao: TMaskEdit;
    edtportador: TEdit;
    edtvalor: TEdit;
    edtcontagerencial: TEdit;
    edtcodigogerador: TEdit;
    combogerador: TComboBox;
    combogeradorcodigo: TComboBox;
    combocredordevedorcodigo: TComboBox;
    comboprazonome: TComboBox;
    comboprazocodigo: TComboBox;
    CheckParcelasIguais: TCheckBox;
    edtnumdcto: TEdit;
    StrGridPendencias: TStringGrid;
    BtPesqMensa: TBitBtn;
    FrLancamento1: TFrLancamento;
    edtcodigohistoricosimples: TEdit;
    btopcoes: TBitBtn;
    btpesquisapendencia: TBitBtn;
    btpesquisaboleto: TBitBtn;
    Label8: TLabel;
    edtnotafiscal: TEdit;
    btpesquisatitulo: TBitBtn;
    Label12: TLabel;
    Label14: TLabel;
    edtsubcontagerencial: TEdit;
    lbnomesubcontagerencial: TLabel;
    btopcoesboleto: TBitBtn;
    BthistoricoPendencias: TBitBtn;
    memoobservacao: TMemo;
    Label15: TLabel;
    btPesquisaDuplicata: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure combogeradorChange(Sender: TObject);
    procedure combocredordevedorChange(Sender: TObject);
    procedure combocredordevedorcodigoChange(Sender: TObject);
    procedure combogeradorcodigoChange(Sender: TObject);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigogeradorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure comboprazonomeChange(Sender: TObject);
    procedure comboprazocodigoChange(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure StrGridPendenciasDblClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure StrGridPendenciasKeyPress(Sender: TObject; var Key: Char);
    procedure comboprazonomeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtPesqMensaClick(Sender: TObject);
    procedure edtportadorExit(Sender: TObject);
    procedure edtcontagerencialExit(Sender: TObject);
    procedure edtvalorExit(Sender: TObject);
    procedure btpesquisasemsaldoClick(Sender: TObject);
    procedure edtcodigocredordevedorExit(Sender: TObject);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure FrLancamento1BtpesquisaLoteClick(Sender: TObject);
    procedure FrLancamento1BtexecutaLoteClick(Sender: TObject);
    procedure edtcodigohistoricosimplesKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtcodigohistoricosimplesExit(Sender: TObject);
    procedure edtcodigohistoricosimplesKeyPress(Sender: TObject;
      var Key: Char);
    procedure btopcoesClick(Sender: TObject);
    procedure FrLancamento1btexecutacrClick(Sender: TObject);
    procedure btopcoesboletoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btpesquisapendenciaClick(Sender: TObject);
    procedure BthistoricoPendenciasClick(Sender: TObject);
    procedure btpesquisaboletoClick(Sender: TObject);
    procedure btpesquisatituloClick(Sender: TObject);
    procedure FrLancamento1btpesquisaClick(Sender: TObject);
    procedure FrLancamento1BtApagaLoteClick(Sender: TObject);
    procedure FrLancamento1btchequecaixaClick(Sender: TObject);
    procedure FrLancamento1btlancaboletoClick(Sender: TObject);
    procedure FrLancamento1btimprimecomprovantepagamentoClick(
      Sender: TObject);
    procedure FrLancamento1btpesquisacrClick(Sender: TObject);
    procedure edtcodigocredordevedorKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtsubcontagerencialExit(Sender: TObject);
    procedure edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btPesquisaDuplicataClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FrLancamento1btAlterarHistoricoLancamentoClick(
      Sender: TObject);
    procedure FrLancamento1strgcrKeyPress(Sender: TObject; var Key: Char);
    procedure FrLancamento1strgcrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FrLancamento1STRGCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FrLancamento1STRGCPKeyPress(Sender: TObject; var Key: Char);
    procedure FrLancamento1BtSelecionaTodas_PGClick(Sender: TObject);
    procedure FrLancamento1BtSelecionaTodas_CRClick(Sender: TObject);
    procedure FrLancamento1strgcrDblClick(Sender: TObject);
    procedure FrLancamento1Button2Click(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         Procedure Preenche_Dados_Externo;
         procedure limpalabels;
    { Private declarations }
  public
    { Public declarations }
    QuitaPrimeiraParcela:Boolean;
    Procedure LocalizaCodigoInicial(Pcodigo:string);

  end;

var
  Ftitulo: TFtitulo;
  CodigoaserLocalizado:string='';

implementation

uses Upesquisa, Uportador, UContaGer, UHistoricoLancamentos,
  UPrazoPagamento, UopcaoRel, UHistoricoSimples,
  UObjTitulo, UDataModulo, UescolheImagemBotao;




{$R *.DFM}

//***********************************************
//********LANCAMENTO EXTERNO*********************
//***********************************************

procedure TFtitulo.Preenche_Dados_Externo;
var
   auxilio:string;
   cont,contg:integer;
begin
     If RegLancTituloExterno.LancamentoExterno=True
     Then Begin
               With RegLancTituloExterno do
               Begin
                       edtcodigo.text           :=LE_CODIGO             ;
                       edthistorico.text        :=LE_HISTORICO          ;
                       auxilio                  :=LE_CREDORDEVEDOR      ;

                       if (auxilio<>'')
                       Then Begin
                                for cont:=0 to combocredordevedorcodigo.items.Count-1 do
                                Begin
                                        if combocredordevedorcodigo.Items[cont]=auxilio
                                        Then Begin
                                                combocredordevedor.itemindex:=cont;
                                                combocredordevedorcodigo.itemindex:=cont;
                                                combocredordevedor.enabled:=False;
                                                break;
                                        End;
                                End;
                       End;

                       If (LE_CODIGOCREDORDEVEDOR<>'')
                       Then Begin
                               edtcodigocredordevedor.text      :=LE_CODIGOCREDORDEVEDOR;
                               edtcodigocredordevedor.enabled:=False;
                       End;

                       auxilio                          := LE_gerador;

                       If auxilio<>''
                       Then Begin
                               for cont:=0 to combogeradorcodigo.items.Count-1 do
                               Begin
                                        if combogeradorcodigo.Items[cont]=auxilio
                                        Then Begin
                                                combogerador.itemindex:=cont;
                                                combogeradorcodigo.itemindex:=cont;
                                                combogeradorcodigo.enabled:=False;
                                                break;
                                             End;
                               End;
                               combogerador.enabled:=False;
                               edtcodigogerador.text            :=LE_codigogerador;
                               edtcodigogerador.enabled:=False;
                       End
                       Else Begin//sem gerador uso o 'SEM GERADOR'
                                 for contg:=0 to combogerador.Items.Count-1 do
                                 Begin
                                        If (combogerador.Items[contg]='SEM GERADOR')
                                        Then Begin
                                                combogerador.ItemIndex:=contg;
                                                combogeradorcodigo.ItemIndex:=contg;
                                                break;
                                        End;
                                 End;
                                edtcodigogerador.text:='0';
                       End;

                        edtEMISSAO.text                 :=LE_EMISSAO           ;

                        auxilio                         :=LE_prazo             ;
                        for cont:=0 to comboprazocodigo.items.Count-1 do
                        Begin
                                if (comboprazocodigo.Items[cont]=auxilio)
                                Then Begin
                                        comboprazocodigo.itemindex:=cont;
                                        comboprazonome.itemindex:=cont;
                                        break;
                                      End;
                        End;

                        edtPORTADOR.text                :=LE_PORTADOR          ;
                        edtVALOR.text                   :=LE_VALOR             ;
                        edtCONTAGERENCIAL.text          :=LE_CONTAGERENCIAL    ;
                        edtSUBCONTAGERENCIAL.text          :=LE_SUBCONTAGERENCIAL    ;

                        LbContaGerencial.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomeContaGerencial(edtcontagerencial.TEXT);
                        LbPortador.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomePortador(EDTPORTADOR.TEXT);
               End;
          End
     Else Begin
               combogerador.text:='';
               combogerador.ItemIndex:=-1;
               for contg:=0 to combogerador.Items.Count-1 do
               Begin
                        If (combogerador.Items[contg]='SEM GERADOR')
                        Then Begin
                                combogerador.ItemIndex:=contg;
                                //combogeradorcodigo.ItemIndex:=contg;
                                combogeradorcodigo.text:=combogeradorcodigo.items[contg];
                                break;
                              End;
                End;
                edtcodigogerador.text:='0';
                edtemissao.text:=datetostr(now);
          End;
end;



//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************

function TFtitulo.ControlesParaObjeto: Boolean;
Begin
  Try
    With FrLancamento1.ObjLancamento.Pendencia.Titulo do
    Begin

         Submit_CODIGO         (edtCODIGO.text);
         Submit_HISTORICO      (edtHISTORICO.text);

         Submit_GERADOR        (combogeradorcodigo.text);
         Submit_codigoGERADOR  (edtcodigogerador.text);
         Submit_CREDORDEVEDOR  (comboCREDORDEVEDORcodigo.text);
         Submit_codigocredordevedor(edtcodigocredordevedor.text);


         Submit_EMISSAO        (edtEMISSAO.text);
         Submit_PRAZO          (comboprazocodigo.text);

         Submit_PORTADOR       (edtPORTADOR.text);
         Submit_VALOR          (tira_ponto(edtVALOR.text));
         Submit_CONTAGERENCIAL (edtCONTAGERENCIAL.text);
         SubContaGerencial.Submit_CODIGO(edtsubcontagerencial.text);


         Submit_ParcelasIguais (CheckParcelasIguais.Checked);
         Submit_NumDcto(edtnumdcto.text);
         Submit_GeradoPeloSistema('N');
         Submit_NotaFiscal(edtnotafiscal.text);
         Submit_ExportaLanctoContaGer('N');//este campo indica se no momento do lancamento de quitacao em vez e usar
         Submit_Observacao(memoobservacao.text);
         result:=true;//o credor devedor use a conta gerencial          
    End;
  Except
        result:=False;
  End;
End;

function TFtitulo.ObjetoParaControles: Boolean;
var
auxilio:string;
cont:integer;
Begin
  Try
     With FrLancamento1.ObjLancamento.Pendencia.Titulo do
     Begin
        edtCODIGO.text          :=Get_CODIGO            ;
        edtHISTORICO.text       :=Get_HISTORICO         ;

        auxilio:= Get_CREDORDEVEDOR;

        for cont:=0 to combocredordevedor.items.Count-1 do
        Begin
             if combocredordevedor.Items[cont]=auxilio
             Then Begin
                       combocredordevedor.itemindex:=cont;
                       combocredordevedorcodigo.itemindex:=cont;
                       break;
                  End;
        End;
        edtcodigocredordevedor.text:=Get_CODIGOCREDORDEVEDOR;


        auxilio:= Get_gerador;

        for cont:=0 to combogerador.items.Count-1 do
        Begin
             if combogerador.Items[cont]=auxilio
             Then Begin
                       combogerador.itemindex:=cont;
                       combogeradorcodigo.itemindex:=cont;
                       break;
                  End;
        End;
        edtcodigogerador.text:=Get_codigogerador;



        edtEMISSAO.text         :=Get_EMISSAO           ;
        auxilio:= Get_prazo;
        for cont:=0 to comboprazocodigo.items.Count-1 do
        Begin
             if comboprazocodigo.Items[cont]=auxilio
             Then Begin
                       comboprazocodigo.itemindex:=cont;
                       comboprazonome.itemindex:=cont;
                       break;
                  End;
        End;


        edtPORTADOR.text        :=Get_PORTADOR          ;
        edtVALOR.text           :=formata_valor(Get_VALOR)             ;
        edtCONTAGERENCIAL.text  :=Get_CONTAGERENCIAL    ;
        LbContaGerencial.caption:=CONTAGERENCIAL.get_mascara+' '+CONTAGERENCIAL.Get_Nome;
        
        edtsubcontagerencial.text:=SubContaGerencial.Get_CODIGO;
        lbnomesubcontagerencial.caption:=SubContaGerencial.get_mascara+' '+SubContaGerencial.get_nome;



        If FrLancamento1.ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D'
        Then LbCRCP.caption:='CONTA A PAGAR'
        Else LbCRCP.caption:='CONTA A RECEBER';

        LbPortador.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomePortador(EDTPORTADOR.TEXT);
        edtnumdcto.text:=Get_NumDcto;
        LbNomeCredorDevedor.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomeCredorDevedor(combocredordevedorcodigo.text,edtcodigocredordevedor.text);
        edtnotafiscal.text:=Get_NotaFiscal;
        memoobservacao.text:=Get_Observacao;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFtitulo.TabelaParaControles: Boolean;
begin
     FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFtitulo.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  try

      CodigoaserLocalizado:='';

      If (FrLancamento1.ObjLancamento.Pendencia=Nil)
      Then exit;

      Exit;

      If (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
      or (FrLancamento1.ObjLancamento.Status<>dsinactive)
      Then Begin
                Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
                abort;
                exit;
      End;

      //If (UObjTitulo.RegLancTituloExterno.LancamentoExterno=false)
      //then

      //FrLancamento1.ObjLancamento.Pendencia.free;
      //NO caso de lancamento externo provavelmente o form que o chamou
      //possui um Frame Lancamento, se eu destrui-lo aqui, dara um
      //erro de excecao se eu precisar utiliza-lo
      //por isso verifico se vem de lancamento externo ou nao
      sELF.TAG:=0;
      Self.QuitaPrimeiraParcela:=False;

  finally

      FrLancamento1.Destruir;


  end

end;

procedure TFtitulo.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFtitulo.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFtitulo.BtNovoClick(Sender: TObject);
begin

     if (SistemaemModoDemoGlobal=True)
     then Begin
               if (FrLancamento1.ObjLancamento.Pendencia.Titulo.ContaTitulos>250)
               then Begin
                         MostraMensagemSistemaDemo('S� � permitido 250 T�tulos');
                         exit;
               End;
     End;



     limpaedit(Self);
     habilita_campos(Self);
     desab_botoes(Self);

     Self.limpalabels;
     Preenche_Dados_Externo;



     edtcodigo.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NovoCodigo;
     edtcodigo.enabled:=False;

     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;

     FrLancamento1.ObjLancamento.Pendencia.Titulo.status:=dsInsert;
     edtcodigohistoricosimples.setfocus;
     CheckParcelasIguais.Checked:=True;


end;

procedure TFtitulo.BtCancelarClick(Sender: TObject);
begin
     FrLancamento1.ObjLancamento.Pendencia.Titulo.cancelar;
     
     limpaedit(Self);
     Self.limpalabels;
     desabilita_campos(Self);
     habilita_botoes(Self);


end;

procedure TFtitulo.BtgravarClick(Sender: TObject);
var
codigotitulo:string;
begin

     If FrLancamento1.ObjLancamento.Pendencia.Titulo.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (FrLancamento1.ObjLancamento.Pendencia.Titulo.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(EdtCodigo.TEXT)=False)
     Then Begin
               Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpalabels;

     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

     TabelaParaControles;

end;

procedure TFtitulo.PreparaAlteracao;
begin
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ALTERAR T�TULO')=False)
     then exit;

     //habilita_campos(Self);
     edthistorico.enabled:=true;
     memoobservacao.enabled:=true;
     edtportador.enabled:=true;
     edtcontagerencial.Enabled:=true;
     edtsubcontagerencial.Enabled:=true;
     edtnumdcto.Enabled:=true;
     edtnotafiscal.Enabled:=true;
     
     FrLancamento1.ObjLancamento.Pendencia.Titulo.Status:=dsEdit;
     edtportador.setfocus;
end;

procedure TFtitulo.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
 Pparametro:TStringList;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
            Pparametro:=TStringList.create;

            pparametro.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_pesquisa(true).text;

            if (Pparametro.Text='')
            Then exit;

            FpesquisaLocal.NomeCadastroPersonalizacao:='TFtitulo.btpesquisarClick';
            If (FpesquisaLocal.PreparaPesquisa(pparametro,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive
                                  then exit;

                                  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(Pparametro);
        End;
end;

procedure TFtitulo.btalterarClick(Sender: TObject);
begin
    If (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Self.PreparaAlteracao;
end;

procedure TFtitulo.btexcluirClick(Sender: TObject);
begin
     If (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR TITULO')=False)
     then Exit;

     If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (FrLancamento1.ObjLancamento.ExcluiTitulo(edtcodigo.text,False)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               FrLancamento1.ObjLancamento.Pendencia.Titulo.rollback;
               exit;
     End
     Else FrLancamento1.ObjLancamento.Pendencia.Titulo.Commit;

     limpaedit(Self);
     Self.limpalabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFtitulo.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fportador:=TFportador.create(nil);
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaPortador,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisaPortador,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then self.edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;


end;

procedure TFtitulo.edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fcontager:TFcontager;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);



            Fcontager:=TFcontager.create(nil);
            if (SistemaMdiGlobal=False)
            Then Begin
                      Fcontager.FormStyle:=fsNormal;
                      Fcontager.Visible:=False;
            End;
              
            If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaContaGerencial,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisaContaGerencial,Fcontager)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtcontagerencial.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fcontager);
     End;


end;

procedure TFtitulo.combogeradorChange(Sender: TObject);
begin
     combogeradorcodigo.itemindex:=combogerador.itemindex;
end;

procedure TFtitulo.combocredordevedorChange(Sender: TObject);
begin
     combocredordevedorcodigo.itemindex:=combocredordevedor.itemindex;
end;

procedure TFtitulo.combocredordevedorcodigoChange(Sender: TObject);
begin
     combocredordevedor.itemindex:=Combocredordevedorcodigo.itemindex;
end;

procedure TFtitulo.combogeradorcodigoChange(Sender: TObject);
begin
     ComboGerador.itemindex:=ComboGeradorCodigo.itemindex;
end;

procedure TFtitulo.edtcodigocredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combocredordevedor.itemindex=-1)
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigo.text),'Pesquisa de '+Combocredordevedor.text,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigo.text))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedor.caption:=FpesquisaLocal.QueryPesq.fieldbyname(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_CampoNomeCredorDevedor(combocredordevedorcodigo.text)).asstring;
                             End;

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TFtitulo.edtcodigogeradorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combogerador.itemindex=-1)
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaGeradorinstrucaoSql(combogeradorcodigo.text),'Pesquisa de '+Combogerador.text,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtcodigogerador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFtitulo.comboprazonomeChange(Sender: TObject);
begin
     Comboprazocodigo.itemindex:=ComboPrazonome.itemindex;
end;

procedure TFtitulo.comboprazocodigoChange(Sender: TObject);
begin
     ComboPrazonome.itemindex:=Comboprazocodigo.itemindex;
end;

procedure TFtitulo.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     If NewTab=1
     Then Begin
             If (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status=dsinactive) and (EdtCodigo.text<>'')
             Then Begin
                       StrGridPendencias.Cols[0].clear;
                       StrGridPendencias.Cols[1].clear;
                       StrGridPendencias.Cols[2].clear;
                       StrGridPendencias.Cols[3].clear;
                       StrGridPendencias.Cols[4].clear;
                       StrGridPendencias.Cols[5].clear;
                       btpesqmensa.onclick(sender);

                  End
             Else AllowChange:=False;
          End
     Else Begin
               If NewTab=2//lancamento de pequenos debitos e outros pre definidos
               Then Begin
                        If (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status<>dsinactive)
                        Then AllowChange:=False;

                        if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO LAN�AMENTO EM LOTE')=False)
                        then AllowChange:=False;

                        FrLancamento1.Guia.ActivePageIndex:=0;
                    End;
     End;


end;


procedure TFtitulo.StrGridPendenciasDblClick(Sender: TObject);
var
Ptemp:string;
begin
     Ptemp:='';

     if (EdtCodigo.Text='') or
        (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status<>dsInactive)
     Then exit;

     If (StrGridPendencias.Cells[0,StrGridPendencias.row]<>'')
     Then Begin

               If StrGridPendencias.cells[0,0]<>'C�DIGO'
               Then Messagedlg('A Configura��o do Grid com C�digo na Coluna 0 Foi Alterada, verifique o C�digo!',mterror,[mbok],0)
               Else FrLancamento1.ObjLancamento.NovoLancamento(StrGridPendencias.cells[0,StrGridPendencias.row],'','','',edthistorico.text,True,True,False);
               BtPesqMensaClick(sender);
     End;
end;

procedure TFtitulo.btrelatoriosClick(Sender: TObject);
begin
     FrLancamento1.ObjLancamento.Imprime(edtcodigo.text);
end;

procedure TFtitulo.StrGridPendenciasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If key=#13
     Then StrGridPendencias.OnDblClick(sender);
end;

procedure TFtitulo.comboprazonomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FprazoPagamento:TFprazoPagamento;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FprazoPagamento:=TFprazoPagamento.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaPrazo,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisaPrazo,FprazoPagamento)=True)
            Then Begin
                      Try
                         FpesquisaLocal.showmodal;
                         FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoCodigo(comboprazocodigo.items);
                         FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoNome(comboprazonome.items);
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FprazoPagamento);
     End;


end;
               

procedure TFtitulo.BtPesqMensaClick(Sender: TObject);
begin
        btopcoesboleto.Enabled:=False;
        StrGridPendencias.Colcount:=2;
        StrGridPendencias.cols[0].clear;
        StrGridPendencias.cols[1].clear;

        StrGridPendencias.RowCount:=FrLancamento1.ObjLancamento.Pendencia.Get_QuantRegs(edtcodigo.text)+1;
        If (StrGridPendencias.RowCount>1)
        Then Begin
                StrGridPendencias.FixedRows:=1;
                //*****Localizando o Titulo***************
                if (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCODIGO(EdtCodigo.text)=False)
                Then Begin
                          Messagedlg('T�tulo n�o localizado',mterror,[mbok],0);
                          exit;
                End;
                FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;

                if (FrLancamento1.ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C')
                Then Begin
                          //titulo a receber
                          //StrGridPendencias.ColCount:=9;
                          btopcoesboleto.Enabled:=True;
                          //FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListaPendenciaCampos(StrGridPendencias.Cols[0],StrGridPendencias.Cols[1],StrGridPendencias.Cols[2],StrGridPendencias.Cols[3],StrGridPendencias.Cols[4],StrGridPendencias.Cols[5],StrGridPendencias.Cols[6],StrGridPendencias.Cols[7],nil,strgridpendencias.cols[8],Edtcodigo.text)
                          Frlancamento1.Objlancamento.Pendencia.RetornaGrid_Receber(StrGridPendencias,Edtcodigo.text);
                End
                Else Begin
                         StrGridPendencias.ColCount:=7;
                         FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListaPendenciaCampos(StrGridPendencias.Cols[0],StrGridPendencias.Cols[1],StrGridPendencias.Cols[2],StrGridPendencias.Cols[3],StrGridPendencias.Cols[4],nil,nil,nil,StrGridPendencias.Cols[5],strgridpendencias.cols[6],Edtcodigo.text);
                End;

                BthistoricoPendencias.Enabled:=true;
             End
        Else StrGridPendencias.RowCount:=0;

        AjustaLArguraColunaGrid(StrGridPendencias);
end;

procedure TFtitulo.edtportadorExit(Sender: TObject);
begin
     LbPortador.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomePortador(edtportador.text);
end;

procedure TFtitulo.edtcontagerencialExit(Sender: TObject);
begin
      FrLancamento1.ObjLancamento.Pendencia.Titulo.EdtcontagerencialExit(sender,LbContaGerencial);
End;

procedure TFtitulo.edtvalorExit(Sender: TObject);
begin
     edtvalor.text:=formata_valor(edtvalor.text);
end;

procedure TFtitulo.btpesquisasemsaldoClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
   Pparametro:TStringList;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           Pparametro:=TStringList.create;

            Pparametro.Text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_pesquisa(false).Text;

            if (Pparametro.text='')
            Then exit;
            
            FpesquisaLocal.NomeCadastroPersonalizacao:='TFtitulo.btpesquisasemsaldoClick';
            If (FpesquisaLocal.PreparaPesquisa(pparametro,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive
                                  then exit;

                                  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Pparametro);
        End;
end;

procedure TFtitulo.edtcodigocredordevedorExit(Sender: TObject);
begin
     //preciso sair com o nome do credordevedor escolhido
     //atraves do FrLancamento1.ObjLancamento.Pendencia.Titulo
     LbNomeCredorDevedor.caption:='';

     If (edtcodigocredordevedor.text='') or (combocredordevedor.ItemIndex=-1)
     Then exit;

     LbNomeCredorDevedor.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomeCredorDevedor(combocredordevedorcodigo.text,edtcodigocredordevedor.text);
     //pegando a contagerencial deste cadastro escolhido
     edtcontagerencial.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_ContaGerencial(combocredordevedorcodigo.text,edtcodigocredordevedor.text);
     edtcontagerencialExit(edtcontagerencial);//colocando o nome na label
end;

procedure TFtitulo.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in ['0'..'9',#8,','])
     Then Begin
        If (key='.')
        Then Key:=','
        Else Key:=#0;
     End;
end;

procedure TFtitulo.FrLancamento1BtpesquisaLoteClick(Sender: TObject);
begin
  FrLancamento1.BtpesquisacpClick(Sender);

end;

procedure TFtitulo.FrLancamento1BtexecutaLoteClick(Sender: TObject);
begin
  FrLancamento1.BtexecutacpClick(Sender);

end;

procedure TFtitulo.edtcodigohistoricosimplesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   FHistoricoSimples:TFHistoricoSimples;
begin
        If (key<>vk_f9)
        Then exit;

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           FHistoricoSimples:=TFHistoricoSimples.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,FrLancamento1.ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                                  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
                                  Then Begin
                                            TEdit(Sender).text:='';
                                            exit;
                                  End;
                                  FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
                                  edthistorico.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FHistoricoSimples);
        End;
end;

procedure TFtitulo.edtcodigohistoricosimplesExit(Sender: TObject);
begin
     IF (TEdit(Sender).text='')
     Then exit;
     
     If (FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
     edthistorico.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
end;

procedure TFtitulo.edtcodigohistoricosimplesKeyPress(Sender: TObject;
  var Key: Char);
begin
     If (not (key in ['0'..'9',#8]))
     Then key:=#0;
end;


procedure TFtitulo.btopcoesClick(Sender: TObject);
begin
     FrLancamento1.ObjLancamento.Pendencia.Opcoes(edtcodigo.text);

     if (edtcodigo.text<>'')
     Then Begin
              if (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(EdtCodigo.Text)=False)
              Then Begin
                        limpaedit(Self);
                        exit;
              End;
              FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
              Self.TabelaParaControles;
     End;
end;

procedure TFtitulo.FrLancamento1btexecutacrClick(Sender: TObject);
begin
  FrLancamento1.btexecutacrClick(Sender);

end;

procedure TFtitulo.btopcoesboletoClick(Sender: TObject);
begin
     //Numero da pendencia
     If StrGridPendencias.cells[0,0]<>'C�DIGO'
     Then Messagedlg('A Configura��o do Grid com C�digo na Coluna 0 Foi Alterada, verifique o C�digo!',mterror,[mbok],0);

     With FOpcaorel do
     begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Cancelar Boleto');
          RgOpcoes.Items.add('Reimpress�o de Boleto');
          RgOpcoes.Items.add('Excluir Duplicata');
          RgOpcoes.Items.Add('Reimpress�o de Duplicata');
          showmodal;
          if (tag=0)
          then exit;

          case RgOpcoes.ItemIndex of

          0: Begin
                  FrLancamento1.ObjLancamento.Pendencia.CancelaBoleto(StrGridPendencias.cells[0,StrGridPendencias.row]);
                  BtPesqMensaClick(sender);
          End;
          1: Begin
                  FrLancamento1.ObjLancamento.Pendencia.Re_imprimeboleto(StrGridPendencias.cells[0,StrGridPendencias.row],true);
                  BtPesqMensaClick(sender);
          End;
          2: Begin
                  FrLancamento1.ObjLancamento.Pendencia.ExcluiDuplicata(StrGridPendencias.cells[0,StrGridPendencias.row]);
                  BtPesqMensaClick(sender);
          End;
          3: Begin
                  if (StrGridPendencias.cells[7,StrGridPendencias.row]='')
                  then exit;
                  FrLancamento1.ObjLancamento.Pendencia.Re_imprimeduplicata(StrGridPendencias.cells[7,StrGridPendencias.row]);
                  BtPesqMensaClick(sender);
          End;
          End;
     End;
end;

procedure TFtitulo.FormShow(Sender: TObject);
var
Pallow:boolean;
begin
     Uessencialglobal.PegaCorForm(Self);

     IF (TAG=1)
     tHEN EXIT;

     limpaedit(Self);
     desabilita_campos(Self);
     Self.limpalabels;
     Guia.PageIndex:=0;

     Try
        //FrLancamento1.ObjLancamento.Pendencia:=TFrLancamento1.ObjLancamento.Pendencia.create;

        If FrLancamento1.Criar=False
        Then Begin
                  desabilita_campos(self);  
        End;

        //FrLancamento1.ObjLancamento.Pendencia.
        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedor.items);
        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigo.items);

        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListaGerador(combogerador.items);
        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListaGeradorCodigo(combogeradorcodigo.items);

        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoCodigo(comboprazocodigo.items);
        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoNome(comboprazonome.items);

     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
     PegaFiguraBotao(BthistoricoPendencias,'BotaoPesquisar.Bmp');
     PegaFiguraBotao(btpesquisasemsaldo,'BotaoPesquisar.Bmp');
     PegaFiguraBotao(btopcoes,'BotaoPesquisar.Bmp');
     PegaFiguraBotao(btpesquisapendencia,'BotaoPesquisar.Bmp');
     PegaFiguraBotao(btpesquisaboleto,'botaoPesquisar.Bmp');
     PegaFiguraBotao(btpesquisatitulo,'botaoPesquisar.Bmp');

     tag:=1;
     If (FrLancamento1.ObjLancamento.Pendencia.Titulo.VerificaPermissao=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);

     //esta variavel � usada em casos que eu quero chamr o form e ele ja
     //procurar um codigo e trazer os dados pra tela
     if(CodigoaserLocalizado<>'')
     Then Begin
               if (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(CodigoaserLocalizado)=true)
               Then Begin
                         FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
                         Self.ObjetoParaControles;

                         if (self.QuitaPrimeiraParcela)
                         Then Begin
                                   Self.GuiaChange(sender,1,pallow);
                                   StrGridPendencias.Row:=1;
                                   StrGridPendenciasDblClick(sender);

                         End;
               End;
    end;
end;

procedure TFtitulo.btpesquisapendenciaClick(Sender: TObject);
VAR
NumPend:string;
begin
     if (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
     Then exit;
     numpend:='';
     Numpend:=FrLancamento1.ObjLancamento.Pendencia.PegaPendencia;
     try
        strtoint(numpend);
        If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaPendencia(NUmpend)=False)
        Then exit;
        FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
        self.ObjetoParaControles;
     except
           exit;
     end;
end;

procedure TFtitulo.BthistoricoPendenciasClick(Sender: TObject);
var
FhistoricoLancamento:TFhistoricoLancamento;
begin
        Try
             FhistoricoLancamento:=TFhistoricoLancamento.create(nil);

             If StrGridPendencias.cells[0,StrGridPendencias.row]=''
             Then exit;

             If StrGridPendencias.cells[0,0]<>'C�DIGO'
             Then Messagedlg('A Configura��o do Grid com C�digo na Coluna 0 Foi Alterada, verifique o C�digo!',mterror,[mbok],0)
             Else Begin
                        FhistoricoLancamento.LbPendencia.caption:=StrGridPendencias.cells[0,StrGridPendencias.row];
                        FhistoricoLancamento.Objlancamento:=FrLancamento1.ObjLancamento;
                        FhistoricoLancamento.btRecibo.Visible:=FhistoricoLancamento.ObjLancamento.LancamentoPagamento(StrGridPendencias.cells[0,StrGridPendencias.row]);

                        //CHAMA FORMUL�RIO - ALEX
                        if   (FhistoricoLancamento=nil)
                        then Application.CreateForm(TFhistoricoLancamento, FhistoricoLancamento);
                             FhistoricoLancamento.ShowModal;
                             FreeAndNil(FhistoricoLancamento);

                        BtPesqMensaClick(sender);
                  End;
        Finally
               Freeandnil(FhistoricoLancamento);
        End;


end;

procedure TFtitulo.btpesquisaboletoClick(Sender: TObject);
VAR
Numboleto:string;
begin
     if (FrLancamento1.ObjLancamento.Pendencia.LocalizaBoleto_Convenio=true)
     Then self.ObjetoParaControles
     Else limpaedit(self); 
end;

procedure TFtitulo.btpesquisatituloClick(Sender: TObject);
VAR
NumPend:string;
begin
     if (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
     Then exit;

     numpend:='';
     NUmPend:=FrLancamento1.ObjLancamento.Pendencia.pegatitulo;
     if (numpend = '')
     then exit;
     try
        strtoint(numpend);
        If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(NUmpend)=False)
        Then Begin
               Messagedlg('T�tulo n�o Localizado!',mtinformation,[mbok],0);
               exit;
        End;
        FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
        self.ObjetoParaControles;
     except
           exit;
     end;
end;

procedure TFtitulo.LocalizaCodigoInicial(Pcodigo: string);
begin
     CodigoaserLocalizado:=pcodigo;
end;

procedure TFtitulo.FrLancamento1btpesquisaClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
   numpend:String;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Get_Pesquisa,FrLancamento1.ObjLancamento.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        if (FpesquisaLocal.showmodal=mrOk)
                        Then Begin
                                  //escolheu um Lancamento
                                  if (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status<>dsinactive)
                                  or (FpesquisaLocal.querypesq.fieldbyname('pendencia').asstring='')
                                  then exit;

                                  try
                                      numpend:=FpesquisaLocal.querypesq.fieldbyname('pendencia').asstring;
                                      If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaPendencia(NUmpend)=False)
                                      Then exit;
                                      FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
                                      self.ObjetoParaControles;
                                      Guia.PageIndex:=0;
                                  except
                                        exit;
                                  end;
                        End;

                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFtitulo.FrLancamento1BtApagaLoteClick(Sender: TObject);
begin
  FrLancamento1.BtApagaLoteClick(Sender);

end;

procedure TFtitulo.FrLancamento1btchequecaixaClick(Sender: TObject);
begin
  FrLancamento1.btchequecaixaClick(Sender);
end;

procedure TFtitulo.FrLancamento1btlancaboletoClick(Sender: TObject);
begin
  FrLancamento1.btlancaboletoClick(Sender);

end;

procedure TFtitulo.FrLancamento1btimprimecomprovantepagamentoClick(
  Sender: TObject);
begin
  FrLancamento1.btimprimecomprovantepagamentoClick(Sender);

end;

procedure TFtitulo.FrLancamento1btpesquisacrClick(Sender: TObject);
begin
  FrLancamento1.btpesquisacrClick(Sender);

end;

procedure TFtitulo.edtcodigocredordevedorKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (key in['0'..'9',#8])
    Then Key:=#0;
end;

procedure TFtitulo.edtsubcontagerencialExit(Sender: TObject);
begin
     FrLancamento1.ObjLancamento.Pendencia.Titulo.EdtSubcontagerencialExit(sender,lbnomesubcontagerencial);
end;

procedure TFtitulo.edtsubcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     FrLancamento1.ObjLancamento.Pendencia.Titulo.edtsubcontagerencialKeyDown(sender,key,shift,lbnomesubcontagerencial,edtcontagerencial.Text);
end;

procedure TFtitulo.limpalabels;
begin
     LbNomeCredorDevedor.caption:='';
     lbnomesubcontagerencial.caption:='';
     LbContaGerencial.caption:='';
     LbPortador.caption:='';
     LbCRCP.caption:='';
end;


procedure TFtitulo.btPesquisaDuplicataClick(Sender: TObject);
VAR
NumDupl:string;
begin
     if (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
     Then exit;
     NumDupl:='';
     NumDupl:=FrLancamento1.ObjLancamento.Pendencia.PegaDuplicata;
     try
        strtoint(NumDupl);
        If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaDuplicata(NumDupl)=False)
        Then exit;
        FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
        self.ObjetoParaControles;
     except
           exit;
     end;
end;

procedure TFtitulo.FormCreate(Sender: TObject);
begin
     Self.QuitaPrimeiraParcela:=false;
end;

procedure TFtitulo.FrLancamento1btAlterarHistoricoLancamentoClick(
  Sender: TObject);
begin
  FrLancamento1.btAlterarHistoricoLancamentoClick(Sender);

end;

procedure TFtitulo.FrLancamento1strgcrKeyPress(Sender: TObject;
  var Key: Char);
begin
  FrLancamento1.strgcrKeyPress(Sender, Key);

end;

procedure TFtitulo.FrLancamento1strgcrKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FrLancamento1.strgcrKeyDown(Sender, Key, Shift);

end;


procedure TFtitulo.FrLancamento1STRGCPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FrLancamento1.STRGCPKeyDown(Sender, Key, Shift);

end;

procedure TFtitulo.FrLancamento1STRGCPKeyPress(Sender: TObject;
  var Key: Char);
begin
  FrLancamento1.STRGCPKeyPress(Sender, Key);

end;

procedure TFtitulo.FrLancamento1BtSelecionaTodas_PGClick(Sender: TObject);
begin
  FrLancamento1.BtSelecionaTodas_PGClick(Sender);

end;

procedure TFtitulo.FrLancamento1BtSelecionaTodas_CRClick(Sender: TObject);
begin
  FrLancamento1.BtSelecionaTodas_CRClick(Sender);

end;

procedure TFtitulo.FrLancamento1strgcrDblClick(Sender: TObject);
begin
  FrLancamento1.strgcrDblClick(Sender);

end;

procedure TFtitulo.FrLancamento1Button2Click(Sender: TObject);
begin
  FrLancamento1.Button2Click(Sender);

end;

end.


