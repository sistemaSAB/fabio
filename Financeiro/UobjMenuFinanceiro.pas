unit UobjMenuFinanceiro;
Interface
Uses forms,menus,classes,Db,UessencialGlobal;

Type
   TObjMenuFinanceiro=class(Tform)//para poder chamar o executa metodo

                //procedimentos que podem ser acessado
                procedure n2_prazopagamentoClick(Sender: TObject);
                procedure N2_portadorClick(Sender: TObject);
                procedure N2_TitulosaPagareReceberClick(Sender: TObject);
                procedure N2_TransferenciaEntreContasClick(Sender: TObject);
                procedure N2_DepositoClick(Sender: TObject);
                procedure N2_PrevisaoFinanceiraClick(Sender: TObject);
                procedure N2_InformacaoChequesRecebidosClick(Sender: TObject);
                procedure N2_PlanoDeContasClick(Sender: TObject);
                procedure n2_ExportaContabilidadeClick(Sender: TObject);
                procedure n2_grupoClick(Sender: TObject);
                procedure n2_ContagerClick(Sender: TObject);
                procedure n2_SubContagerClick(Sender: TObject);
                procedure n2_tipolanctoClick(Sender: TObject);

                procedure n2_tipolanctoportadorClick(Sender: TObject);
                procedure n2_talaodechequesClick(Sender: TObject);
                procedure n2_ComprovanteChequeClick(Sender: TObject);
                procedure n2_motivodevolucaoClick(Sender: TObject);
                procedure n2_historicosimplesClick(Sender: TObject);
                procedure n2_duplicataClick(Sender: TObject);
                procedure n2_CRedorDevedorClick(Sender:TObject);
                Procedure n2_GeradorClick(Sender:TObject);
                Procedure n2_geraTransferenciaClick(Sender:TObject);
                Procedure n2_geralancamentoClick(Sender:TObject);
                Procedure n2_geratituloClick(Sender:TObject);
                Procedure n2_pendenciaClick(Sender:TObject);
                Procedure n2_chequesportadorClick(Sender:TObject);
                Procedure n2_ValoresClick(Sender:TObject);
                Procedure n2_creditovaloresClick(Sender:TObject);
                Procedure n2_BaixaChequeClick(Sender:TObject);
                Procedure n2_OcorrenciaChequeDevolvidoClick(sender:Tobject);
                Procedure n2_LancaChequeDevolvidoClick(sender:Tobject);
                Procedure n2_Pagamento_e_geracao_EmprestimoClick(sender:Tobject);
                Procedure n2_ContaFacilClick(sender:Tobject);
                Procedure n2_ContasClick(sender:Tobject);

                procedure n3_boletobancarioClick(Sender: TObject);
                procedure n3_ConvenioBoletosClick(Sender: TObject);
                procedure n3_LoteRetornoCobrancaClick(Sender: TObject);
                procedure n3_RegistroRetornoCobrancaClick(Sender: TObject);
                Procedure n3_OcorrenciaRetornoCobrancaClick(Sender:TObject);
                Procedure n3_GeraArquivoRemessaClick(Sender:TObject);
                Procedure n3_HistoricoBoletoClick(Sender:TObject);
                Procedure n2_Baixa_em_ChequesClick(Sender:TObject);



          Public
                Constructor Create(Sender:Tmenuitem;PForm:Tform;PSistemaMdi:Boolean);
                Destructor  Free;
                procedure   ExecutaMetodo(Pform:Tform;MethodName: string) ;
          Private
                SistemaMdi:Boolean;
                FormPai:Tform;

                N1_Financeiro: TMenuItem;
                        N2_portador: TMenuItem;
                        N2_TitulosaPagareReceber:TMenuItem;
                        N2_TransferenciaEntreContas:TMenuItem;
                        N2_Deposito:TMenuItem;
                        N2_PrevisaoFinanceira:TMenuItem;
                        N2_InformacaoChequesRecebidos:TMenuItem;
                        N2_LancaChequeDevolvido:TMenuItem;
                        N2_ContaFacil:TMenuItem;
                        N2_Contas:TMenuItem;
                        
                N1_Cadastros:TmenuItem;
                        N2_PlanoDeContas        :TMenuITem;
                        n2_ExportaContabilidade :TMenuITem;
                        n2_grupo                :TMenuITem;
                        n2_Contager             :TMenuITem;
                        n2_SubContager          :TMenuITem;
                        n2_tipolancto           :TMenuITem;
                        n2_prazopagamento       :TMenuITem;
                        n2_tipolanctoportador   :TMenuITem;
                        n2_talaodecheques       :TMenuITem;
                        n2_ComprovanteCheque    :TMenuITem;
                        n2_motivodevolucao      :TMenuITem;
                        n2_historicosimples     :TMenuITem;
                        n2_duplicata            :TMenuITem;
                        n2_OcorrenciaChequeDevolvido:TMenuITem;
                        //****Sub menu****
                        n2_Boletos                       :TMenuITem;
                            n3_boletobancario            :TMenuITem;
                            n3_ConvenioBoletos           :TMenuITem;
                            n3_OcorrenciaRetornoCobranca :TMenuITem;
                            n3_LoteRetornoCobranca       :TMenuITem;
                            n3_RegistroRetornoCobranca   :TMenuITem;
                            n3_GeraArquivoRemessa        :TMenuITem;
                            n3_HistoricoBoleto           :TMenuITem;

                N1_Configuracoes:TMenuitem;
                        n2_credordevedor        :TMenuITem;
                        n2_gerador              :TMenuITem;
                        n2_geraTransferencia    :TMenuITem;
                        n2_geralancamento       :TMenuITem;
                        n2_geraTitulo           :TMenuITem;
                        //******SUBMENU*****************
                        n2_apoio                :TMenuitem;
                                n2_pendencia            :TMenuItem;
                                n2_ChequesPortador      :TMenuItem;
                                n2_Valores              :TMenuItem;
                                n2_creditovalores    :TMenuItem;
                N1_Outros:TMenuitem;
                        n2_Pagamento_e_geracao_Emprestimo:TMenuItem;
                        n2_Baixa_em_Cheques: TMenuItem;





   End;


implementation
uses SysUtils,Dialogs,uportador,utitulo,uobjtitulo,utransferenciaportador,
udeposito,uprevisaofinanceira,UplanodeContas,uexportacontabilidade,Uboletobancario,
Ucomprovantecheque,Ucontager,Ugrupo,Uhistoricosimples,Umotivodevolucao,
uprazopagamento,Utalaodecheques,Utipolancto,Utipolanctoportador,Ucredordevedor,UGerador,
Ugeratransferencia,UgeraLancamento,UGeraTitulo,UPendencia,UchequesPortador,Uvalores,Uobjvalores,
Uinfocheque, UchequeDevolvido, UBaixaCheque, UCONVENIOSBOLETO, 
  ULOTERETORNOCOBRANCA, UREGISTRORETORNOCOBRANCA,
  UOCORRENCIARETORNOCOBRANCA, UchequesDevolvidos,
  UPagamento_e_geracao_emprestimo, USUBCONTAGERENCIAL, Ucreditovalores,
  Ucontafacil, UDataModulo, UobjICONES, UGeraArquivoRemessa, UHistorico,
  UHistoricoBoleto,Utitulo_novo,UobjBaixaemcheques;




{ TObjMenuFinanceiro }

constructor TObjMenuFinanceiro.Create(Sender:Tmenuitem;PForm:Tform;PSistemaMdi:Boolean);
var
ContNIvel0:INteger;
begin
     Self.SistemaMdi:=PSistemaMdi;
     Self.FormPai:=Pform;

     //Financeiro (NIVEL 0)
     N1_Financeiro:=TMenuItem.Create(Tmainmenu(sender));
     sender.Insert(0,N1_Financeiro);
     N1_Financeiro.Caption:='Financeiro';
                ContNIvel0:=0;
                //portador 0
                N2_portador:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_portador);
                N2_portador.caption:='Portador';
                N2_portador.OnClick:=Self.N2_portadorClick;
                Inc(ContNIvel0,1);
                //titulos a pagar e receber 1
                N2_TitulosaPagareReceber:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_TitulosaPagareReceber);
                N2_TitulosaPagareReceber.caption:='T�tulos a Pagar e Receber';
                N2_TitulosaPagareReceber.OnClick:=Self.N2_TitulosaPagareReceberclick;
                Inc(ContNIvel0,1);
                //TransferenciaEntreContas 2
                N2_TransferenciaEntreContas:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_TransferenciaEntreContas);
                N2_TransferenciaEntreContas.caption:='Transfer�ncia Entre Contas';


                N2_TransferenciaEntreContas.OnClick:=Self.N2_TransferenciaEntreContasclick;
                Inc(ContNIvel0,1);
                //Deposito 3
                N2_Deposito:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_Deposito);
                N2_Deposito.caption:='Dep�sito';
                N2_Deposito.OnClick:=Self.N2_Depositoclick;
                Inc(ContNIvel0,1);
                //Previsao Financeira
                N2_previsaofinanceira:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_previsaofinanceira);
                N2_previsaofinanceira.caption:='Previs�o Financeira';
                N2_previsaofinanceira.OnClick:=Self.N2_previsaofinanceiraclick;
                Inc(ContNIvel0,1);
                //Cheques Recebidos
                N2_informacaochequesrecebidos:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_informacaochequesrecebidos);
                N2_informacaochequesrecebidos.caption:='Informa��o de Cheques Recebidos';
                N2_informacaochequesrecebidos.OnClick:=Self.N2_informacaochequesrecebidosclick;
                Inc(ContNIvel0,1);

                //Lan�a Devolu��o de Cheques
                N2_LancaChequeDevolvido:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_LancaChequeDevolvido);
                N2_LancaChequeDevolvido.caption:='Lan�a Devolu��o de Cheques';
                N2_LancaChequeDevolvido.OnClick:=Self.n2_LancaChequeDevolvidoClick;
                Inc(ContNIvel0,1);

                //Conta F�cil
                N2_ContaFacil:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_ContaFacil);
                N2_ContaFacil.caption:='Conta F�cil';
                N2_ContaFacil.OnClick:=Self.n2_ContaFacilClick;
                Inc(ContNIvel0,1);

                //Contas
                N2_Contas:=TMenuItem.Create(N1_Financeiro);
                N1_Financeiro.insert(ContNIvel0,N2_Contas);
                N2_Contas.caption:='Contas';
                N2_Contas.OnClick:=Self.n2_ContasClick;
                Inc(ContNIvel0,1);

     //Cadastros(0,1)
     N1_Cadastros:=TMenuItem.Create(Tmainmenu(sender));
     sender.Insert(1,N1_Cadastros);
     N1_Cadastros.Caption:='Cadastros';
                ContNIvel0:=0;
                //PLANO DE CONTAS
                N2_planodecontas:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,N2_planodecontas);
                N2_planodecontas.caption:='Plano de Contas';
                N2_planodecontas.OnClick:=Self.N2_planodecontasClick;
                Inc(ContNIvel0,1);
                //Exportacao de ContaBilidade
                n2_ExportaContabilidade:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_ExportaContabilidade);
                n2_ExportaContabilidade.caption:='Lan�amentos Cont�beis (EXPORTAR PARA SISTEMA CONT�BIL)';
                n2_ExportaContabilidade.OnClick:=Self.n2_ExportaContabilidadeClick;
                Inc(ContNIvel0,1);
                //Grupo
                n2_grupo:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_grupo);
                n2_grupo.caption:='Grupo';
                n2_grupo.OnClick:=Self.n2_grupoClick;
                Inc(ContNIvel0,1);
                //Contas Gerenciais
                n2_Contager:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_Contager);
                n2_Contager.caption:='Contas Gerenciais';
                n2_Contager.OnClick:=Self.n2_ContagerClick;
                Inc(ContNIvel0,1);
                //SubContas Gerenciais
                n2_SubContager:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_SubContager);
                n2_SubContager.caption:='Sub-Contas Gerenciais';
                n2_SubContager.OnClick:=Self.n2_SubContagerClick;
                Inc(ContNIvel0,1);
                //tipos de lancto
                n2_tipolancto:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_tipolancto);
                n2_tipolancto.caption:='Tipos de Lan�amentos';
                n2_tipolancto.OnClick:=Self.n2_tipolanctoClick;
                Inc(ContNIvel0,1);
                //Prazo de Pagamento
                n2_prazopagamento:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_prazopagamento);
                n2_prazopagamento.caption:='Prazos de Pagamentos';
                n2_prazopagamento.OnClick:=Self.n2_prazopagamentoClick;
                Inc(ContNIvel0,1);
                //tipo de lancto em portadores
                n2_tipolanctoportador:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_tipolanctoportador);
                n2_tipolanctoportador.caption:='Tipo de Lan�amento em Portador';
                n2_tipolanctoportador.OnClick:=Self.n2_tipolanctoportadorClick;
                Inc(ContNIvel0,1);
                //Talao de Cheques
                n2_talaodecheques:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_talaodecheques);
                n2_talaodecheques.caption:='Tal�o de Cheques';
                n2_talaodecheques.OnClick:=Self.n2_talaodechequesClick;
                Inc(ContNIvel0,1);
                //Comprovante de Cheques
                n2_ComprovanteCheque:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_ComprovanteCheque);
                n2_ComprovanteCheque.caption:='Comprovante de Cheques';
                n2_ComprovanteCheque.OnClick:=Self.n2_ComprovanteChequeClick;
                Inc(ContNIvel0,1);
                //Motivo de Devolu��o
                n2_motivodevolucao:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_motivodevolucao);
                n2_motivodevolucao.caption:='Motivo de Devolu��o de Cheques';
                n2_motivodevolucao.OnClick:=Self.n2_motivodevolucaoClick;
                Inc(ContNIvel0,1);
                //HIstorico Simples (CONTABIL)
                n2_historicosimples:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_historicosimples);
                n2_historicosimples.caption:='Hist�rico Simples (Cont�bil)';
                n2_historicosimples.OnClick:=Self.n2_historicosimplesClick;
                Inc(ContNIvel0,1);
                //Duplicatas
                n2_duplicata:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_duplicata);
                n2_duplicata.caption:='Duplicatas';
                n2_duplicata.OnClick:=Self.n2_duplicataClick;
                Inc(ContNIvel0,1);

                //Ocorrencia de Cheque Devolvido
                n2_OcorrenciaChequeDevolvido:=TMenuItem.Create(n1_cadastros);
                n1_cadastros.insert(ContNIvel0,n2_OcorrenciaChequeDevolvido);
                n2_OcorrenciaChequeDevolvido.caption:='Ocorr�ncia de Cheque Devolvido';
                n2_OcorrenciaChequeDevolvido.OnClick:=Self.n2_OcorrenciaChequeDevolvidoClick;
                Inc(ContNIvel0,1);



                //***************************
                //SUBMENU BOLETOS
                n2_boletos:=TMenuItem.Create(N1_Cadastros);
                N1_Cadastros.insert(ContNIvel0,n2_boletos);
                n2_boletos.caption:='Boletos';
                ContNIvel0:=0;
                        //Boleto Banc�rio
                        n3_boletobancario:=TMenuItem.Create(n2_Boletos);
                        n2_Boletos.insert(ContNIvel0,n3_boletobancario);
                        n3_boletobancario.caption:='Boleto Banc�rio';
                        n3_boletobancario.OnClick:=Self.n3_boletobancarioClick;
                        Inc(ContNIvel0,1);
        
                        //ConvenioBoletos
                        n3_ConvenioBoletos:=TMenuItem.Create(n2_Boletos);
                        n2_Boletos.insert(ContNIvel0,n3_ConvenioBoletos);
                        n3_ConvenioBoletos.caption:='Conv�nios Banc�rio para Boletos';
                        n3_ConvenioBoletos.OnClick:=Self.n3_ConvenioBoletosclick;
                        Inc(ContNIvel0,1);
                        //Lote de Retorno de Cobranca
                        n3_LoteRetornoCobranca:=TMenuItem.Create(n2_Boletos);
                        n2_Boletos.insert(ContNIvel0,n3_LoteRetornoCobranca);
                        n3_LoteRetornoCobranca.caption:='Lote de Retorno de Cobran�a(boletos)';
                        n3_LoteRetornoCobranca.OnClick:=Self.n3_LoteRetornoCobrancaClick;
                        Inc(ContNIvel0,1);

                        //Ocorrencia de Retorno de Cobranca
                        n3_OcorrenciaRetornoCobranca:=TMenuItem.Create(n2_Boletos);
                        n2_Boletos.insert(ContNIvel0,n3_OcorrenciaRetornoCobranca);
                        n3_OcorrenciaRetornoCobranca.caption:='Ocorr�ncia de Retorno de Cobran�a(boletos)';
                        n3_OcorrenciaRetornoCobranca.OnClick:=Self.n3_OcorrenciaRetornoCobrancaClick;
                        Inc(ContNIvel0,1);


                        //Gera Arquivo de Remessa
                        n3_GeraArquivoRemessa:=TMenuItem.Create(n2_Boletos);
                        n2_Boletos.insert(ContNIvel0,n3_GeraArquivoRemessa);
                        n3_GeraArquivoRemessa.caption:='Gera Arquivo de Remessa (Boletos)';
                        n3_GeraArquivoRemessa.OnClick:=Self.n3_GeraArquivoRemessaClick;
                        Inc(ContNIvel0,1);

                        //Historico de Boletos
                        n3_HistoricoBoleto:=TMenuItem.Create(n2_Boletos);
                        n2_Boletos.insert(ContNIvel0,n3_HistoricoBoleto);
                        n3_HistoricoBoleto.caption:='Historico de Boletos';
                        n3_HistoricoBoleto.OnClick:=Self.n3_HistoricoBoletoClick;
                        Inc(ContNIvel0,1);


                        {//Registro de Retorno de Cobranca
                        n3_RegistroRetornoCobranca:=TMenuItem.Create(n2_Boletos);
                        n2_Boletos.insert(ContNIvel0,n3_RegistroRetornoCobranca);
                        n3_RegistroRetornoCobranca.caption:='Registro de Retorno de Cobran�a(boletos)';
                        n3_RegistroRetornoCobranca.OnClick:=Self.n3_RegistroRetornoCobrancaClick;
                        Inc(ContNIvel0,1);}


     //Configuracoes(0,2)
     N1_Configuracoes:=TMenuItem.Create(Tmainmenu(sender));
     sender.Insert(2,N1_Configuracoes);
     N1_Configuracoes.Caption:='Configura��es';
                ContNIvel0:=0;
                //Credor Devedor
                n2_credordevedor:=TMenuItem.Create(n1_configuracoes);
                n1_configuracoes.insert(ContNIvel0,n2_credordevedor);
                n2_credordevedor.caption:='Credor/Devedor';
                n2_credordevedor.OnClick:=Self.n2_credordevedorClick;
                Inc(ContNIvel0,1);
                //Credor Devedor
                n2_gerador:=TMenuItem.Create(n1_configuracoes);
                n1_configuracoes.insert(ContNIvel0,n2_gerador);
                n2_gerador.caption:='Gerador';
                n2_gerador.OnClick:=Self.n2_geradorClick;
                Inc(ContNIvel0,1);
                //Gera Transfer�ncia
                n2_GeraTransferencia:=TMenuItem.Create(n1_configuracoes);
                n1_configuracoes.insert(ContNIvel0,n2_GeraTransferencia);
                n2_GeraTransferencia.caption:='Gerador de Transfer�ncia';
                n2_GeraTransferencia.OnClick:=Self.n2_GeraTransferenciaClick;
                Inc(ContNIvel0,1);
                //Gera Lancto
                n2_geralancamento:=TMenuItem.Create(n1_configuracoes);
                n1_configuracoes.insert(ContNIvel0,n2_geralancamento);
                n2_geralancamento.caption:='Gerador de Lan�amento';
                n2_geralancamento.OnClick:=Self.n2_geralancamentoClick;
                Inc(ContNIvel0,1);
                //Gera T�tulo
                n2_geratitulo:=TMenuItem.Create(n1_configuracoes);
                n1_configuracoes.insert(ContNIvel0,n2_geratitulo);
                n2_geratitulo.caption:='Gerador de T�tulos';
                n2_geratitulo.OnClick:=Self.n2_geratituloClick;
                Inc(ContNIvel0,1);
                //***************************
                //SUBMENU APOIO
                n2_apoio:=TMenuItem.Create(n1_configuracoes);
                n1_configuracoes.insert(ContNIvel0,n2_apoio);
                n2_apoio.caption:='Apoio';
                        ContNIvel0:=0;
                        //PENDENCIA
                        n2_pendencia:=TMenuItem.Create(n2_apoio);
                        n2_apoio.insert(ContNIvel0,n2_pendencia);
                        n2_pendencia.caption:='Pend�ncia dos T�tulos';
                        n2_pendencia.OnClick:=Self.n2_pendenciaClick;
                        Inc(ContNIvel0,1);
                        //ChequesPortador
                        n2_chequesportador:=TMenuItem.Create(n2_apoio);
                        n2_apoio.insert(ContNIvel0,n2_chequesportador);
                        n2_chequesportador.caption:='Cheque do Portador';
                        n2_chequesportador.OnClick:=Self.n2_chequesportadorClick;
                        Inc(ContNIvel0,1);
                        //Valores
                        n2_Valores:=TMenuItem.Create(n2_apoio);
                        n2_apoio.insert(ContNIvel0,n2_Valores);
                        n2_Valores.caption:='Valores Recebidos';
                        n2_Valores.OnClick:=Self.n2_ValoresClick;
                        Inc(ContNIvel0,1);

                        //Credito Lancamento
                        n2_creditovalores:=TMenuItem.Create(n2_apoio);
                        n2_apoio.insert(ContNIvel0,n2_creditovalores);
                        n2_creditovalores.caption:='Cr�dito Usado em Lan�amentos';
                        n2_creditovalores.OnClick:=Self.n2_creditovaloresClick;
                        Inc(ContNIvel0,1);
     //outros(0,3)

     N1_Outros:=TMenuItem.Create(Tmainmenu(sender));
     sender.Insert(3,N1_outros);
     N1_Outros.Caption:='Extras';
                ContNIvel0:=0;
                //Credor Devedor
                n2_Pagamento_e_geracao_Emprestimo:=TMenuItem.Create(n1_Outros);
                n1_outros.insert(ContNIvel0,n2_Pagamento_e_geracao_Emprestimo);
                n2_Pagamento_e_geracao_Emprestimo.caption:='Pagamento e Gera��o de Contas a Receber (Empr�stimo)';
                n2_pagamento_e_geracao_emprestimo.OnClick:=Self.n2_Pagamento_e_geracao_EmprestimoClick;
                Inc(ContNIvel0,1);
                // BAIXAS EM CHEQUES *JONATAN MEDINA
                n2_Baixa_em_Cheques :=TMenuItem.Create(n1_outros);
                n1_outros.insert(ContNIvel0,n2_Baixa_em_Cheques);
                n2_Baixa_em_Cheques.caption:='Dar baixa em cheques(Gravar cheques OK)';
                n2_Baixa_em_Cheques.OnClick:=Self.n2_Baixa_em_ChequesClick;
                Inc(ContNIvel0,1);


end;

destructor TObjMenuFinanceiro.Free;
begin

end;


procedure TObjMenuFinanceiro.n2_BaixaChequeClick(Sender: TObject);
var
fbaixacheque:Tfbaixacheque;
begin
     if (ObjIconesglobal.VerificaClick('n2_BaixaChequeClick__i__objmenufinanceiro'))
     then exit;

     Try
        fbaixacheque:=Tfbaixacheque.create(nil);
        fbaixacheque.Showmodal;
     Finally
            Freeandnil(fbaixacheque);
     End;
end;

procedure TObjMenuFinanceiro.n3_boletobancarioClick(Sender: TObject);
var
    Fboletobancario:TFboletobancario;
begin
     if (ObjIconesglobal.VerificaClick('n3_boletobancarioClick__i__objmenufinanceiro'))
     then exit;
     try
         Fboletobancario:=TFboletobancario.create(nil);
         Fboletobancario.showmodal;
     Finally
            Freeandnil(Fboletobancario);
     End;
end;


procedure TObjMenuFinanceiro.n2_chequesportadorClick(Sender: TObject);
var
FchequesPortador:TFchequesPortador;
begin
     if (ObjIconesglobal.VerificaClick('n2_chequesportadorClick__i__objmenufinanceiro'))
     then exit;
     Try
        FchequesPortador:=TFchequesPortador.create(nil);
        FchequesPortador.Showmodal;
     Finally
            Freeandnil(FchequesPortador);
     End;

end;

procedure TObjMenuFinanceiro.n2_ComprovanteChequeClick(Sender: TObject);
var
fcomprovantecheque:Tfcomprovantecheque;
begin
     if (ObjIconesglobal.VerificaClick('n2_ComprovanteChequeClick__i__objmenufinanceiro'))
     then exit;
     Try
        fcomprovantecheque:=Tfcomprovantecheque.create(nil);
        fcomprovantecheque.Showmodal;
     Finally
            Freeandnil(fcomprovantecheque);
     End;

end;

procedure TObjMenuFinanceiro.n2_ContagerClick(Sender: TObject);
var
Fcontager1:Tfcontager;
begin
     if (ObjIconesglobal.VerificaClick('n2_ContagerClick__i__objmenufinanceiro'))
     then exit;
     Try
        fcontager1:=Tfcontager.create(nil);
        fcontager1.Showmodal;
     Finally
            Freeandnil(fcontager1);
     End;
end;

procedure TObjMenuFinanceiro.n2_SubContagerClick(Sender: TObject);
var
FSubcontager1:TfSubcontagerencial;
begin
     if (ObjIconesglobal.VerificaClick('n2_SubContagerClick__i__objmenufinanceiro'))
     then exit;
     Try
        FSubcontager1:=Tfsubcontagerencial.create(nil);
        FSubcontager1.Showmodal;
     Finally
       Freeandnil(FSubcontager1);
     End;
end;

procedure TObjMenuFinanceiro.n2_CRedorDevedorClick(Sender: TObject);
var
Fcredordevedor:TFcredordevedor;
begin
     if (ObjIconesglobal.VerificaClick('n2_CRedorDevedorClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fcredordevedor:=TFcredordevedor.create(nil);
        Fcredordevedor.Showmodal;
     Finally
            Freeandnil(Fcredordevedor);
     End;

end;

procedure TObjMenuFinanceiro.N2_DepositoClick(Sender: TObject);
var
Fdeposito:TFdeposito;
begin
     if (ObjIconesglobal.VerificaClick('N2_DepositoClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fdeposito:=TFdeposito.create(nil);
        Fdeposito.Showmodal;
     Finally
            Freeandnil(Fdeposito);
     End;
End;
procedure TObjMenuFinanceiro.n2_duplicataClick(Sender: TObject);
begin
end;

procedure TObjMenuFinanceiro.n2_ExportaContabilidadeClick(Sender: TObject);
var
Fexportacontabilidade:TFexportacontabilidade;
begin
     if (ObjIconesglobal.VerificaClick('n2_ExportaContabilidadeClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fexportacontabilidade:=TFexportacontabilidade.create(nil);
        Fexportacontabilidade.Showmodal;
     Finally
            Freeandnil(Fexportacontabilidade);
     End;

end;

procedure TObjMenuFinanceiro.n2_GeradorClick(Sender: TObject);
var
FGerador:TFGerador;
begin
     if (ObjIconesglobal.VerificaClick('n2_GeradorClick__i__objmenufinanceiro'))
     then exit;
     Try
        FGerador:=TFGerador.create(nil);
        FGerador.Showmodal;
     Finally
            Freeandnil(FGerador);
     End;

end;

procedure TObjMenuFinanceiro.n2_geralancamentoClick(Sender: TObject);
var
FgeraLancamento:TFgeraLancamento;
begin
     if (ObjIconesglobal.VerificaClick('n2_geralancamentoClick__i__objmenufinanceiro'))
     then exit;
     Try
        FgeraLancamento:=TFgeraLancamento.create(nil);
        FgeraLancamento.Showmodal;
     Finally
            Freeandnil(FgeraLancamento);
     End;

end;

procedure TObjMenuFinanceiro.n2_geratituloClick(Sender: TObject);
var
FGeraTituloX:TFGeraTitulo;
begin
     if (ObjIconesglobal.VerificaClick('n2_geratituloClick__i__objmenufinanceiro'))
     then exit;
     Try
        FGeraTituloX:=TFGeraTitulo.create(nil);
        FGeraTituloX.Showmodal;
     Finally
            Freeandnil(FGeraTituloX);
     End;

end;

procedure TObjMenuFinanceiro.n2_geraTransferenciaClick(Sender: TObject);
var
Fgeratransferencia:TFgeratransferencia;
begin
     if (ObjIconesglobal.VerificaClick('n2_geraTransferenciaClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fgeratransferencia:=TFgeratransferencia.create(nil);
        Fgeratransferencia.Showmodal;
     Finally
            Freeandnil(Fgeratransferencia);
     End;

end;

procedure TObjMenuFinanceiro.n2_grupoClick(Sender: TObject);
var
Fgrupo:TFgrupo;
begin
     if (ObjIconesglobal.VerificaClick('n2_grupoClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fgrupo:=TFgrupo.create(nil);
        Fgrupo.Showmodal;
     Finally
            Freeandnil(Fgrupo);
     End;

end;

procedure TObjMenuFinanceiro.n2_historicosimplesClick(Sender: TObject);
var
Fhistoricosimples:TFhistoricosimples;
begin
     if (ObjIconesglobal.VerificaClick('n2_historicosimplesClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fhistoricosimples:=TFhistoricosimples.create(nil);
        Fhistoricosimples.Showmodal;
     Finally
            Freeandnil(Fhistoricosimples);
     End;

end;
procedure TObjMenuFinanceiro.N2_InformacaoChequesRecebidosClick(
  Sender: TObject);
var
FinfochequeX:TFinfocheque;
begin
     if (ObjIconesglobal.VerificaClick('N2_InformacaoChequesRecebidosClick__i__objmenufinanceiro'))
     then exit;
     Try
        FinfochequeX:=TFinfocheque.create(nil);
        FinfochequeX.showmodal;
     Finally
            freeandnil(FinfochequeX);
     End;
End;


procedure TObjMenuFinanceiro.n2_motivodevolucaoClick(Sender: TObject);
var
fmotivodevolucao:Tfmotivodevolucao;
begin
     if (ObjIconesglobal.VerificaClick('n2_motivodevolucaoClick__i__objmenufinanceiro'))
     then exit;
     Try
        fmotivodevolucao:=Tfmotivodevolucao.create(nil);
        fmotivodevolucao.Showmodal;
     Finally
            Freeandnil(fmotivodevolucao);
     End;

end;
procedure TObjMenuFinanceiro.n2_pendenciaClick(Sender: TObject);
var
FpendXX:Tfpendencia;
begin
     if (ObjIconesglobal.VerificaClick('n2_pendenciaClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fpendxx:=TFPendencia.create(nil);
        Fpendxx.ShowModal;
     Finally
            Freeandnil(FpendXX);
     End;

end;

procedure TObjMenuFinanceiro.N2_PlanoDeContasClick(Sender: TObject);
var
Fplanodecontas:TFplanodecontas;
begin
     if (ObjIconesglobal.VerificaClick('N2_PlanoDeContasClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fplanodecontas:=TFplanodecontas.create(nil);
        Fplanodecontas.Showmodal;
     Finally
            Freeandnil(Fplanodecontas);
     End;
end;

procedure TObjMenuFinanceiro.N2_portadorClick(Sender: TObject);
var
Fportador:TFportador;
begin

     if (ObjIconesglobal.VerificaClick('N2_portadorClick__i__objmenufinanceiro'))
     then exit;
     


     Try
        Fportador:=TFportador.create(nil);

        Fportador.Showmodal;
     Finally
            Freeandnil(Fportador);
     End;

End;

procedure TObjMenuFinanceiro.n2_prazopagamentoClick(Sender: TObject);
var
fprazopagamento:Tfprazopagamento;
begin
     if (ObjIconesglobal.VerificaClick('n2_prazopagamentoClick__i__objmenufinanceiro'))
     then exit;

     Try
        fprazopagamento:=Tfprazopagamento.create(nil);
        fprazopagamento.Showmodal;
     Finally
            Freeandnil(fprazopagamento);
     End;

end;

procedure TObjMenuFinanceiro.N2_PrevisaoFinanceiraClick(Sender: TObject);
var
fprevisaofinanceira:Tfprevisaofinanceira;
begin
     if (ObjIconesglobal.VerificaClick('N2_PrevisaoFinanceiraClick__i__objmenufinanceiro'))
     then exit;
     Try
        fprevisaofinanceira:=Tfprevisaofinanceira.create(nil);
        fprevisaofinanceira.Showmodal;
     Finally
            Freeandnil(fprevisaofinanceira);
     End;

end;

procedure TObjMenuFinanceiro.n2_talaodechequesClick(Sender: TObject);
var
ftalaodecheques:Tftalaodecheques;
begin

  if (ObjIconesglobal.VerificaClick('n2_talaodechequesClick__i__objmenufinanceiro')) then
  exit;

  Try
  ftalaodecheques:=Tftalaodecheques.create(nil);
  ftalaodecheques.Showmodal;
  Finally
  Freeandnil(ftalaodecheques);
  End;

end;

procedure TObjMenuFinanceiro.n2_tipolanctoClick(Sender: TObject);
var
ftipolancto:Tftipolancto;
begin
     if (ObjIconesglobal.VerificaClick('n2_tipolanctoClick__i__objmenufinanceiro'))
     then exit;
     Try
        ftipolancto:=Tftipolancto.create(nil);
        ftipolancto.Showmodal;
     Finally
            Freeandnil(ftipolancto);
     End;

end;

procedure TObjMenuFinanceiro.n2_tipolanctoportadorClick(Sender: TObject);
var
ftipolanctoportador:Tftipolanctoportador;
begin
     if (ObjIconesglobal.VerificaClick('n2_tipolanctoportadorClick__i__objmenufinanceiro'))
     then exit;
     Try
        ftipolanctoportador:=Tftipolanctoportador.create(nil);
        ftipolanctoportador.Showmodal;
     Finally
            Freeandnil(ftipolanctoportador);
     End;

end;
procedure TObjMenuFinanceiro.N2_TitulosaPagareReceberClick(Sender: TObject);
var
Ftitulo:TFtitulo;
begin
     if (ObjIconesglobal.VerificaClick('N2_TitulosaPagareReceberClick__i__objmenufinanceiro'))
     then exit;
     Try
        Ftitulo:=TFtitulo.create(nil);
        //*Indico que o titulo n�o sera lancamento externo
        RegLancTituloExterno.lancamentoExterno:=False;
        //Limpo os dados de lancamento externo
        LimpaRegLancTituloExterno;
        Ftitulo.showmodal;
     Finally
            Freeandnil(Ftitulo);
     End;
end;

procedure TObjMenuFinanceiro.N2_TransferenciaEntreContasClick(
  Sender: TObject);
var
FtransferenciaPortador:TFtransferenciaPortador;
begin
     if (ObjIconesglobal.VerificaClick('N2_TransferenciaEntreContasClick__i__objmenufinanceiro'))
     then exit;
     Try
        FtransferenciaPortador:=TFtransferenciaPortador.create(nil);
        FtransferenciaPortador.Showmodal;
     Finally
            Freeandnil(FtransferenciaPortador);
    End;

end;

procedure TObjMenuFinanceiro.n2_ValoresClick(Sender: TObject);
var
Fvalores:TFvalores;
begin
     if (ObjIconesglobal.VerificaClick('n2_ValoresClick__i__objmenufinanceiro'))
     then exit;
     Try
        Fvalores:=TFvalores.create(nil);
        LimpaRegistroValores;
        Registrovalores.LancamentoExterno:=False;
        Fvalores.showmodal;
     Finally
            Freeandnil(Fvalores);
     End;

end;


procedure TObjMenuFinanceiro.n2_creditovaloresClick(Sender: TObject);
var
FcreditovaloresX:TFcreditovalores;
begin
     if (ObjIconesglobal.VerificaClick('n2_creditovaloresClick__i__objmenufinanceiro'))
     then exit;
     Try
        FcreditovaloresX:=TFcreditovalores.create(nil);
        FcreditovaloresX.showmodal;
     Finally
            Freeandnil(FcreditovaloresX);
     End;

end;

procedure TObjMenuFinanceiro.n3_ConvenioBoletosClick(Sender: TObject);
var
FConveniosBoleto:TFCONVENIOSBOLETO;
begin
     if (ObjIconesglobal.VerificaClick('n3_ConvenioBoletosClick__i__objmenufinanceiro'))
     then exit;
     Try
        FConveniosBoleto:=TFCONVENIOSBOLETO.Create(nil);
        FConveniosBoleto.showmodal;
     Finally
            Freeandnil(FConveniosBoleto);
     End;

end;

procedure TObjMenuFinanceiro.n3_LoteRetornoCobrancaClick(Sender: TObject);
var
FLoteRetornoCobranca:TFLOTERETORNOCOBRANCA;
begin
     if (ObjIconesglobal.VerificaClick('n3_LoteRetornoCobrancaClick__i__objmenufinanceiro'))
     then exit;
     Try
        FLoteRetornoCobranca:=TFLOTERETORNOCOBRANCA.create(nil);
        FLoteRetornoCobranca.showmodal;
     Finally
            Freeandnil(FLoteRetornoCobranca);
     End;

end;

procedure TObjMenuFinanceiro.n3_RegistroRetornoCobrancaClick(Sender: TObject);
var
FRegistroRetornoCobranca:TFRegistroRETORNOCOBRANCA;
begin
     if (ObjIconesglobal.VerificaClick('n3_RegistroRetornoCobrancaClick__i__objmenufinanceiro'))
     then exit;
     Try
        FRegistroRetornoCobranca:=TFRegistroRETORNOCOBRANCA.create(nil);
        FRegistroRetornoCobranca.showmodal;
     Finally
            Freeandnil(FRegistroRetornoCobranca);
     End;

end;

procedure TObjMenuFinanceiro.n3_OcorrenciaRetornoCobrancaClick(Sender: TObject);
var
FOcorrenciaRetornoCobranca:TFOcorrenciaRETORNOCOBRANCA;
begin
     if (ObjIconesglobal.VerificaClick('n3_OcorrenciaRetornoCobrancaClick__i__objmenufinanceiro'))
     then exit;
     Try
        FOcorrenciaRetornoCobranca:=TFOcorrenciaRETORNOCOBRANCA.create(nil);
        FOcorrenciaRetornoCobranca.showmodal;
     Finally
            Freeandnil(FOcorrenciaRetornoCobranca);
     End;

end;

procedure TObjMenuFinanceiro.n2_OcorrenciaChequeDevolvidoClick(
  sender: Tobject);
var
FOcorrenciaChequeDevolvido:TFCHEQUEDEVOLVIDO;
begin
     if (ObjIconesglobal.VerificaClick('n2_OcorrenciaChequeDevolvidoClick__i__objmenufinanceiro'))
     then exit;
     Try
        FOcorrenciaChequeDevolvido:=TFCHEQUEDEVOLVIDO.create(nil);
        FOcorrenciaChequeDevolvido.showmodal;
     Finally
            Freeandnil(FOcorrenciaChequeDevolvido);
     End;

end;

procedure TObjMenuFinanceiro.n2_LancaChequeDevolvidoClick(sender: Tobject);
Var
FChequeDevolvido:TfChequesDevolvidos;
begin
     if (ObjIconesglobal.VerificaClick('n2_LancaChequeDevolvidoClick__i__objmenufinanceiro'))
     then exit;
     Try
        FChequeDevolvido:=TfChequesDevolvidos.create(nil);
        FChequeDevolvido.showmodal;
     Finally
            Freeandnil(FChequeDevolvido);
     End;
end;

procedure TObjMenuFinanceiro.n2_Pagamento_e_geracao_EmprestimoClick(
  sender: Tobject);
Var
FPagamento_e_Geracao_emprestimoX:TFPagamento_e_geracao_emprestimo;
begin
     if (ObjIconesglobal.VerificaClick('n2_Pagamento_e_geracao_EmprestimoClick__i__objmenufinanceiro'))
     then exit;
     Try
        FPagamento_e_Geracao_emprestimoX:=TFPagamento_e_geracao_emprestimo.create(nil);
        FPagamento_e_Geracao_emprestimoX.showmodal;
     Finally
            Freeandnil(FPagamento_e_Geracao_emprestimoX);
     End;
end;

procedure TObjMenuFinanceiro.n2_ContaFacilClick(sender: Tobject);
var
   FcontaFacilX:tfContaFacil;
begin
     if (ObjIconesglobal.VerificaClick('n2_ContaFacilClick__i__objmenufinanceiro'))
     then exit;

     Try
        FcontaFacilX:=tfContaFacil.Create(nil);
     Except
           on e:exception do
           Begin
                Mensagemerro(e.Message);
                exit;
           End;
     End;

     Try
        FcontaFacilX.showmodal;
     Finally
            freeandnil(FcontaFacilX);
     end;
end;


procedure TObjMenuFinanceiro.ExecutaMetodo(Pform:Tform;MethodName: string) ;
Type
    Texec = procedure of object;
var
   Routine: TMethod;
   Exec: TExec;
begin
   Routine.Data := Pform ;
   Routine.Code := Pform.MethodAddress(MethodName) ;


   If NOT Assigned(Routine.Code)
   Then Begin
              Messagedlg('O Procedimento"'+MethodName+'" N�o encontrado',mterror,[mbok],0);
              Exit;
   End;

   Exec := TExec(Routine) ;

   Exec;

End;


procedure TObjMenuFinanceiro.n3_GeraArquivoRemessaClick(Sender: TObject);
var
FGeraArquivoRemessa :TFGeraArquivoRemessa;
begin

     if (ObjIconesglobal.VerificaClick('n3_GeraArquivoRemessaClick__i__objmenufinanceiro'))
     then exit;

     Try
        FGeraArquivoRemessa:=TFGeraArquivoRemessa.create(nil);
        FGeraArquivoRemessa.showmodal;
     Finally
            Freeandnil(FGeraArquivoRemessa);
     End;

end;

procedure TObjMenuFinanceiro.n3_HistoricoBoletoClick(Sender: TObject);
var
FHistoricoBoleto :TFHistoricoBoleto;
begin
     if (ObjIconesglobal.VerificaClick('n3_HistoricoBoletoClick__i__objmenufinanceiro'))
     then exit;


     Try
        FHistoricoBoleto:=TFHistoricoBoleto.create(nil);
        FHistoricoBoleto.showmodal;
     Finally
            Freeandnil(FHistoricoBoleto);
     End;
end;

procedure TObjMenuFinanceiro.n2_ContasClick(sender: Tobject);
var
   FContasX:TFtitulo_novo;
begin
     if (ObjIconesglobal.VerificaClick('n2_ContasClick__i__objmenufinanceiro'))
     then exit;

     Try
        FcontasX:=tftitulo_novo.Create(nil);
     Except
           on e:exception do
           Begin
                Mensagemerro(e.Message);
                exit;
           End;
     End;

     Try
         if   (Ftitulo_novo=nil)
         then Application.CreateForm(TFtitulo_novo, Ftitulo_novo);
              FContasX.ShowModal;
     Finally
            freeandnil(FcontasX);
     end;

end;

procedure TObjMenuFinanceiro.n2_Baixa_em_ChequesClick(Sender:TObject);
begin
     if (ObjIconesglobal.VerificaClick('n2_Baixa_em_ChequesClick__i__objmenufinanceiro'))
     Then exit;

     FBaixaEmCheques.Create;
     FBaixaEmCheques.ShowModal;

end;

end.
