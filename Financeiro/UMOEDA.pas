unit UMOEDA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjMOEDA,
  jpeg ;

type
  TFMOEDA = class(TForm)
    Guia: TTabbedNotebook;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    ImagemFundo: TImage;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbNOME: TLabel;
    EdtNOME: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ComboMoedaPadrao: TComboBox;
    EdtSimboloMoedaPadrao: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ComboMoedaPadraoKeyPress(Sender: TObject; var Key: Char);
  private
         ObjMOEDA:TObjMOEDA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMOEDA: TFMOEDA;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFMOEDA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjMOEDA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_NOME(edtNOME.text);
        Submit_MoedaPadrao(ComboMoedaPadrao.Text[1]);
        Submit_SimboloMoedaPadrao(EdtSimboloMoedaPadrao.Text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFMOEDA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjMOEDA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;

        if (Get_MoedaPadrao = 'S')
        then ComboMoedaPadrao.ItemIndex:=0
        else ComboMoedaPadrao.ItemIndex:=1;

        EdtSimboloMoedaPadrao.Text:=Get_SimboloMoedaPadrao;
      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFMOEDA.TabelaParaControles: Boolean;
begin
     If (Self.ObjMOEDA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFMOEDA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjMOEDA=Nil)
     Then exit;

     If (Self.ObjMOEDA.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjMOEDA.free;
end;

procedure TFMOEDA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFMOEDA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjMOEDA.status:=dsInsert;
     Guia.pageindex:=0;
     edtNOME.setfocus;

end;


procedure TFMOEDA.btalterarClick(Sender: TObject);
begin
    If (Self.ObjMOEDA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjMOEDA.Status:=dsEdit;
                guia.pageindex:=0;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtNOME.setfocus;
                
    End;


end;

procedure TFMOEDA.btgravarClick(Sender: TObject);
Var Insercao:Boolean;
begin

     If Self.ObjMOEDA.Status=dsInactive
     Then exit;

     if (Self.ObjMOEDA.Status = dsInsert)
     then Insercao:=true
     else Insercao:=false;


     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     if (ComboMoedaPadrao.Text = 'SIM')
     then Begin
             if (Self.ObjMOEDA.AtualizaMoedaPadrao(EdtCODIGO.Text)=false)
             then Begin
                     MensagemErro('Erro ao tentar atualizar a moeda padr�o');
                     exit;
             end;
     end; 

     If (Self.ObjMOEDA.salvar(true)=False)
     Then exit;

     if (Insercao = true) // Assim que eu cadastro uma nova moeda eu j� crio a primeira Cota��o pra ela
     then Begin
             if (Self.ObjMOEDA.CriaPrimeiraCotacao(Self.ObjMOEDA.Get_codigo)=false)
             then Begin
                     MensagemErro('Erro ao tentar criar a Primeira Cota��o');
                     exit;
             end;
     end;


     edtCodigo.text:=Self.ObjMOEDA.Get_codigo;
     Self.ObjMOEDA.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFMOEDA.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjMOEDA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjMOEDA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjMOEDA.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFMOEDA.btcancelarClick(Sender: TObject);
begin
     Self.ObjMOEDA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFMOEDA.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFMOEDA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjMOEDA.Get_pesquisa,Self.ObjMOEDA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjMOEDA.status<>dsinactive
                                  then exit;

                                  If (Self.ObjMOEDA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjMOEDA.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFMOEDA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFMOEDA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjMOEDA:=TObjMOEDA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFMOEDA.ComboMoedaPadraoKeyPress(Sender: TObject; var Key: Char);
begin
    Abort;
end;

end.

