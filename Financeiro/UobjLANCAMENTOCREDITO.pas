unit UobjLANCAMENTOCREDITO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc;

Type
   TObjLANCAMENTOCREDITO=class

          Public
                //ObjDatasource                             :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: String);
                Function Get_Codigo: String;
                Procedure Submit_Data(parametro: String);
                Function Get_Data: String;
                Procedure Submit_Cliente(parametro: String);
                Function Get_Cliente: String;
                Procedure Submit_Valor(parametro: String);
                Function Get_Valor: String;
                Procedure Submit_Historico(parametro: String);
                Function Get_Historico: String;
                Procedure Submit_Devolucao(parametro: String);
                Function Get_Devolucao: String;
                Procedure Submit_Valores(parametro: String);
                Function Get_Valores: String;
                Procedure Submit_Credito(parametro: String);
                Function Get_Credito: String;
                Procedure Submit_LoteCreditoPontos(parametro: String);
                Function Get_LoteCreditoPontos:String;
                Procedure Submit_Pedido(parametro:String);
                Function Get_Pedido:String;

                Function RetornaSaldoCliente(PCliente:String):Currency;
                function RetornaCreditoLancadosCliente(PCliente: string): Currency;
                function RetornaCreditoUtilizadosCliente(PCliente: string): Currency;
                function RetornaCreditoPedido(PPedido: string): Currency;

                Function AumentaCreditoDevolucao(PDevolucao, PCliente, PValor, PData:String):Boolean;
                Function DiminuiCreditoDevolucao(PDevolucao:String):Boolean;

                Function AumentaCreditoComCredito(PCredito, PCliente, PValor,PData: String): Boolean;
                function DiminuiCreditoDeCredito(PCredito: String): Boolean;
                function AlteraCreditoComCredito(PCredito, PCliente, PValor,PData: String): Boolean;

                Function AumentaCreditoComValores(PValores:String):Boolean;
                Function DiminuiCreditoComValores(PValores, PCliente, PValor, PData:String):Boolean;

                Function AumentaCreditoComLoteCreditoPontos(PLoteCreditoPontos, PCliente, PValor,PData: String): Boolean;
                function DiminuiCreditoDeLoteCreditoPontos(PLoteCreditoPontos: String): Boolean;

                Function DiminuiCreditoComPedido(PPedido, PCliente, PValor,PData: String): Boolean;
                function AumentaCreditoComPedido(PPedido: String): Boolean;


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:String;
               Data:String;
               Cliente:String;
               Valor:String;
               Historico:String;
               Devolucao:String;
               Valores:String;
               Credito:String;
               LoteCreditoPontos:String;
               Pedido:String;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjLANCAMENTOCREDITO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.Cliente:=fieldbyname('Cliente').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.Historico:=fieldbyname('Historico').asstring;
        Self.Devolucao:=fieldbyname('Devolucao').asstring;
        Self.Valores:=fieldbyname('Valores').asstring;
        Self.Credito:=fieldbyname('Credito').asstring;
        Self.LoteCreditoPontos:=fieldbyname('LoteCreditoPontos').AsString;
        Self.Pedido:=fieldbyname('Pedido').AsString;
        result:=True;
     End;
end;


Procedure TObjLANCAMENTOCREDITO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('Cliente').asstring:=Self.Cliente;
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
        ParamByName('Historico').asstring:=Self.Historico;
        ParamByName('Devolucao').asstring:=Self.Devolucao;
        ParamByName('Valores').asstring:=Self.Valores;
        ParamByName('Credito').asstring:=Self.Credito;
        ParamByName('LoteCreditoPontos').AsString:=Self.LoteCreditoPontos;
        ParamByName('Pedido').AsString:=Self.Pedido;

  End;
End;

//***********************************************************************

function TObjLANCAMENTOCREDITO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjLANCAMENTOCREDITO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Data:='';
        Cliente:='';
        Valor:='';
        Historico:='';
        Devolucao:='';
        Valores:='';
        Credito:='';
        LoteCreditoPontos:='';
        Pedido:='';
     End;
end;

Function TObjLANCAMENTOCREDITO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjLANCAMENTOCREDITO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjLANCAMENTOCREDITO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        Strtoint(Self.Cliente);
     Except
           Mensagem:=mensagem+'/Cliente';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
     try
        if (Self.Devolucao <> '')
        then Strtoint(Self.Devolucao);
     Except
           Mensagem:=mensagem+'/Devolucao';
     End;
     try
        if (Self.Valores <> '')
        then Strtoint(Self.Valores);
     Except
           Mensagem:=mensagem+'/Valores';
     End;
     try
        if (Self.Credito <> '')
        then Strtoint(Self.Credito);
     Except
           Mensagem:=mensagem+'/Credito';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjLANCAMENTOCREDITO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjLANCAMENTOCREDITO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjLANCAMENTOCREDITO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro LANCAMENTOCREDITO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select * from  TabLancamentoCredito');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjLANCAMENTOCREDITO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjLANCAMENTOCREDITO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjLANCAMENTOCREDITO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjLANCAMENTOCREDITO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabLancamentoCredito(Codigo,Data,Cliente');
                InsertSQL.add(' ,Valor,Historico,Devolucao,Valores,Credito,LoteCreditoPontos, Pedido)');
                InsertSQL.add('values (:Codigo,:Data,:Cliente,:Valor,:Historico');
                InsertSQL.add(' ,:Devolucao,:Valores,:Credito,:LoteCreditoPontos,:Pedido)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabLancamentoCredito set Codigo=:Codigo,Data=:Data');
                ModifySQL.add(',Cliente=:Cliente,Valor=:Valor,Historico=:Historico');
                ModifySQL.add(',Devolucao=:Devolucao,Valores=:Valores');
                ModifySQL.add(',Credito=:Credito, LoteCreditoPontos=:LoteCreditoPontos,Pedido=:Pedido');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabLancamentoCredito where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjLANCAMENTOCREDITO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjLANCAMENTOCREDITO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabLANCAMENTOCREDITO');
     Result:=Self.ParametroPesquisa;
end;

function TObjLANCAMENTOCREDITO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de LANCAMENTOCREDITO ';
end;


function TObjLANCAMENTOCREDITO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENLANCAMENTOCREDITO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENLANCAMENTOCREDITO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjLANCAMENTOCREDITO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjLANCAMENTOCREDITO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjLANCAMENTOCREDITO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjLancamentoCredito.Submit_Codigo(parametro: String);
begin
        Self.Codigo:=Parametro;
end;
function TObjLancamentoCredito.Get_Codigo: String;
begin
        Result:=Self.Codigo;
end;
procedure TObjLancamentoCredito.Submit_Data(parametro: String);
begin
        Self.Data:=Parametro;
end;
function TObjLancamentoCredito.Get_Data: String;
begin
        Result:=Self.Data;
end;
procedure TObjLancamentoCredito.Submit_Cliente(parametro: String);
begin
        Self.Cliente:=Parametro;
end;
function TObjLancamentoCredito.Get_Cliente: String;
begin
        Result:=Self.Cliente;
end;
procedure TObjLancamentoCredito.Submit_Valor(parametro: String);
begin
        Self.Valor:=Parametro;
end;
function TObjLancamentoCredito.Get_Valor: String;
begin
        Result:=Self.Valor;
end;
procedure TObjLancamentoCredito.Submit_Historico(parametro: String);
begin
        Self.Historico:=Parametro;
end;
function TObjLancamentoCredito.Get_Historico: String;
begin
        Result:=Self.Historico;
end;
procedure TObjLancamentoCredito.Submit_Devolucao(parametro: String);
begin
        Self.Devolucao:=Parametro;
end;
function TObjLancamentoCredito.Get_Devolucao: String;
begin
        Result:=Self.Devolucao;
end;
procedure TObjLancamentoCredito.Submit_Valores(parametro: String);
begin
        Self.Valores:=Parametro;
end;
function TObjLancamentoCredito.Get_Valores: String;
begin
        Result:=Self.Valores;
end;
procedure TObjLancamentoCredito.Submit_Credito(parametro: String);
begin
        Self.Credito:=Parametro;
end;
function TObjLancamentoCredito.Get_Credito: String;
begin
        Result:=Self.Credito;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjLANCAMENTOCREDITO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJLANCAMENTOCREDITO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjLANCAMENTOCREDITO.AumentaCreditoDevolucao(PDevolucao, PCliente, PValor, PData: String): Boolean;
begin
     Result:=false;

     // Devolveu um produto aumenta o credito
     Self.ZerarTabela;
     Self.Status:=dsInsert;
     Self.Submit_Codigo(Get_NovoCodigo);
     Self.Submit_Data(PData);
     Self.Submit_Valor(PValor);
     Self.Submit_Devolucao(PDevolucao);
     Self.Submit_Cliente(PCliente);
     if (Self.Salvar(false)=false)
     then Begin
              MensagemErro('Erro ao tentar Gravar na Tabela Lancamento de Credito');
              exit;
     end;

     Result:=true;

end;

function TObjLANCAMENTOCREDITO.DiminuiCreditoDevolucao(PDevolucao:String): Boolean;
begin
    Result:=false;

    // Se Excluiu a devolu��o � s� excluir o registro
    // que o Credito ser� diminuido

    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Delete from TabLancamentoCredito Where Devolucao ='+PDevolucao);
    try
        Self.Objquery.ExecSQL;
    except
        MensagemErro('Erro ao tentar Excluir um Credito da Devolu��o');
        exit;
    end;

    Result:=true;
end;


Function TObjLANCAMENTOCREDITO.AumentaCreditoComCredito(PCredito, PCliente, PValor, PData: String): Boolean;
begin
     Result:=false;

     Self.ZerarTabela;
     Self.Status:=dsInsert;
     Self.Submit_Codigo(Get_NovoCodigo);
     Self.Submit_Data(PData);
     Self.Submit_Valor(PValor);
     Self.Submit_Credito(PCredito);
     Self.Submit_Cliente(PCliente);
     if (Self.Salvar(false)=false)
     then Begin
              MensagemErro('Erro ao tentar Gravar na Tabela Lancamento de Credito');
              exit;
     end;

     Result:=true;
end;

function TObjLANCAMENTOCREDITO.DiminuiCreditoDeCredito(PCredito:String): Boolean;
begin
    Result:=false;

    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Delete from TabLancamentoCredito Where Credito ='+PCredito);
    try
        Self.Objquery.ExecSQL;
    except
        MensagemErro('Erro ao tentar Excluir um Credito da Devolu��o');
        exit;
    end;

    Result:=true;
end;

Function TObjLANCAMENTOCREDITO.AlteraCreditoComCredito(PCredito, PCliente, PValor,PData: String): Boolean;
begin
     Result:=false;

     With Self.Objquery do
     Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select * from  TabLancamentoCredito');
           SQL.ADD(' WHERE credito='+PCredito);
           Open;
           If (recordcount=0)
           Then Begin
                   MensagemErro('Credito n�o encontrado');
                   exit;
           end;
     End;

     Self.TabelaparaObjeto;

     Self.Status:=dsEdit;
     Self.Submit_Data(PData);
     Self.Submit_Valor(PValor);
     Self.Submit_Credito(PCredito);
     Self.Submit_Cliente(PCliente);
     if (Self.Salvar(false)=false)
     then Begin
              MensagemErro('Erro ao tentar Gravar na Tabela Lancamento de Credito');
              exit;
     end;

     Result:=true;
end;




function TObjLANCAMENTOCREDITO.AumentaCreditoComValores(PValores: String): Boolean;
begin
    Result:=false;

    // Se Excluiu os Valores � s� excluir o registro
    // que o Credito ser� Aumentado, porque todos lancamento de credito
    // vindo da tabela de Valores � NEGATIVO

    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Delete from TabLancamentoCredito Where Valores ='+PValores);
    try
        Self.Objquery.ExecSQL;
    except
        MensagemErro('Erro ao tentar Excluir o Credito');
        exit;
    end;

    Result:=true;

end;

function TObjLANCAMENTOCREDITO.DiminuiCreditoComValores(PValores, PCliente,PValor, PData: String): Boolean;
begin
     Result:=false;

     // Quando usa o Credito pela Tabela de Valores
     // eu Crio mais um registro Negativo
     Self.ZerarTabela;
     Self.Status:=dsInsert;
     Self.Submit_Codigo(Get_NovoCodigo);
     Self.Submit_Data(PData);
     Self.Submit_Valor(CurrToStr(StrToCurr(PValor)*-1));
     Self.Submit_Valores(PValores);
     Self.Submit_Cliente(PCliente);
     if (Self.Salvar(false)=false)
     then Begin
              MensagemErro('Erro ao tentar Gravar na Tabela Lancamento de Credito');
              exit;
     end;

     Result:=true;

end;

function TObjLANCAMENTOCREDITO.RetornaSaldoCliente(PCliente:string): Currency;
begin
    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Select SUM(Valor) as Valor from TabLancamentoCredito Where Cliente = '+PCliente);
    Self.Objquery.Open;

    Result:=Self.Objquery.fieldbyname('Valor').AsCurrency;
end;

function TObjLANCAMENTOCREDITO.RetornaCreditoLancadosCliente(PCliente:string): Currency;
begin
    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Select SUM(Valor) as Valor from TabLancamentoCredito Where Cliente = '+PCliente);
    Self.Objquery.SQL.Add('And Valor > 0');

    Self.Objquery.Open;

    Result:=Self.Objquery.fieldbyname('Valor').AsCurrency;
end;

function TObjLANCAMENTOCREDITO.RetornaCreditoUtilizadosCliente(PCliente:string): Currency;
begin
    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Select SUM(Valor) as Valor from TabLancamentoCredito Where Cliente = '+PCliente);
    Self.Objquery.SQL.Add('And Valor < 0');
    Self.Objquery.Open;

    Result:=Self.Objquery.fieldbyname('Valor').AsCurrency;
end;



function TObjLANCAMENTOCREDITO.Get_LoteCreditoPontos: String;
begin
    Result:=Self.LoteCreditoPontos;
end;

procedure TObjLANCAMENTOCREDITO.Submit_LoteCreditoPontos(parametro: String);
begin
    Self.LoteCreditoPontos:=parametro;
end;


function TObjLANCAMENTOCREDITO.AumentaCreditoComLoteCreditoPontos(PLoteCreditoPontos, PCliente, PValor, PData: String): Boolean;
begin
     Result:=false;

     Self.ZerarTabela;
     Self.Status:=dsInsert;
     Self.Submit_Codigo(Get_NovoCodigo);
     Self.Submit_Data(PData);
     Self.Submit_Valor(PValor);
     Self.Submit_LoteCreditoPontos(PLoteCreditoPontos);
     Self.Submit_Cliente(PCliente);
     if (Self.Salvar(false)=false)
     then Begin
              MensagemErro('Erro ao tentar Gravar na Tabela Lancamento de Credito');
              exit;
     end;

     Result:=true;

end;

function TObjLANCAMENTOCREDITO.DiminuiCreditoDeLoteCreditoPontos(PLoteCreditoPontos: String): Boolean;
begin
    Result:=false;

    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Delete from TabLancamentoCredito Where LOTECREDITOPONTOS = '+PLoteCreditoPontos);
    try
        Self.Objquery.ExecSQL;
    except
        MensagemErro('Erro ao tentar Excluir um Credito da Devolu��o');
        exit;
    end;

    Result:=true;
end;

function TObjLANCAMENTOCREDITO.Get_Pedido: String;
begin
    Result:=Self.Pedido;
end;

procedure TObjLANCAMENTOCREDITO.Submit_Pedido(parametro: String);
begin
    Self.Pedido:=parametro;
end;

function TObjLANCAMENTOCREDITO.AumentaCreditoComPedido( PPedido: String): Boolean;
begin
    Result:=false;

    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Delete from TabLancamentoCredito Where Pedido = '+PPedido);
    try
        Self.Objquery.ExecSQL;
    except
        MensagemErro('Erro ao tentar Excluir um Credito da Devolu��o');
        exit;
    end;

    Result:=true;


end;

function TObjLANCAMENTOCREDITO.DiminuiCreditoComPedido(PPedido, PCliente, PValor, PData: String): Boolean;
begin
     Result:=false;

     // Quando usa o Credito pela Tabela de Pedido
     // eu Crio mais um registro Negativo
     Self.ZerarTabela;
     Self.Status:=dsInsert;
     Self.Submit_Codigo(Get_NovoCodigo);
     Self.Submit_Data(PData);
     Self.Submit_Valor(CurrToStr(StrToCurr(PValor)*-1));
     Self.Submit_Pedido(PPedido);
     Self.Submit_Cliente(PCliente);
     if (Self.Salvar(false)=false)
     then Begin
              MensagemErro('Erro ao tentar Gravar na Tabela Lancamento de Credito');
              exit;
     end;

     Result:=true;



end;

function TObjLANCAMENTOCREDITO.RetornaCreditoPedido( PPedido: string): Currency;
begin
    Self.Objquery.Close;
    Self.Objquery.SQL.Clear;
    Self.Objquery.SQL.Add('Select SUM(Valor)*-1 as Valor from TabLancamentoCredito Where Pedido = '+PPedido);
    try
          Self.Objquery.Open;
    except
          on e:Exception do
          begin
                MensagemErro(e.Message);
          end;
    end;

    Result:=Self.Objquery.fieldbyname('Valor').AsCurrency;
end;

end.



