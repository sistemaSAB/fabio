unit UObjHistorico;
Interface
Uses Db,UessencialGlobal,Ibcustomdataset;

Type
   TObjhistorico=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar                          :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaDescricao(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaDebito              :string;
                Function    Get_PesquisaCredito             :string;


                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Descricao        :string;
                Function Get_DebitoCredito    :string;

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Descricao        (parametro:string);
                Procedure Submit_DebitoCredito    (parametro:string);

         Private
               ObjDataset:Tibdataset;

               CODIGO           :String[09];
               Descricao        :String[100];
               DebitoCredito    :String[01];



                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Classes,SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Procedure  TObjhistorico.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring          ;
        Self.Descricao        :=FieldByName('Descricao').asstring       ;
        Self.DebitoCredito    :=FieldByName('DebitoCredito').asstring   ;
     End;
end;


Procedure TObjhistorico.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring          :=Self.CODIGO          ;
      FieldByName('Descricao').asstring       :=Self.Descricao       ;
      FieldByName('DebitoCredito').asstring   :=Self.DebitoCredito   ;
  End;
End;

//***********************************************************************

function TObjhistorico.Salvar: Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;
 FDataModulo.IBTransaction.CommitRetaining;
 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjhistorico.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Descricao        :='';
        Self.DebitoCredito    :='';
     End;
end;

Function TObjhistorico.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Descricao='')
       Then Mensagem:=mensagem+'/Descri��o';

       If (DebitoCredito='')
       Then Mensagem:=mensagem+'/D�bito&Cr�dito';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjhistorico.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Curso n�o Encontrado!';}

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjhistorico.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjhistorico.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjhistorico.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';
     If (DebitoCredito<>'D') and (DebitoCredito<>'C')
     Then mensagem:=mensagem+'/Valor Inv�lido para O campo D�bito&Cr�dito';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjhistorico.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('select codigo,descricao,debitocredito from tabhistorico where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;
function TObjhistorico.LocalizaDescricao(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('select codigo,descricao,debitocredito from tabhistorico where descricao='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


procedure TObjhistorico.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjhistorico.Exclui(Pcodigo: string): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 FDataModulo.IBTransaction.CommitRetaining;
             End
                 
        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjhistorico.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        ZerarTabela;
        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('select codigo,descricao,debitocredito from tabhistorico where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert Into Tabhistorico (codigo,descricao,debitocredito) values (:codigo,:descricao,:debitocredito)');

                ModifySQL.clear;
                ModifySQL.add(' Update TabHistorico set codigo=:codigo,descricao=:descricao,debitocredito=:debitocredito where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add(' Delete from Tabhistorico where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('select codigo,descricao,debitocredito from tabhistorico where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjhistorico.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjhistorico.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjhistorico.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjhistorico.Get_Pesquisa: string;
begin
     Result:=' Select * from Tabhistorico ';
end;

function TObjhistorico.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Hist�rico ';
end;

function TObjhistorico.Get_DebitoCredito: string;
begin
     Result:=Self.DebitoCredito;
end;

function TObjhistorico.Get_Descricao: string;
begin
     Result:=Self.Descricao;
end;

procedure TObjhistorico.Submit_DebitoCredito(parametro: string);
begin
     Self.DebitoCredito:=parametro;
end;

procedure TObjhistorico.Submit_Descricao(parametro: string);
begin
     Self.Descricao:=Parametro;
end;

function TObjhistorico.Get_PesquisaCredito: string;
begin
     Result:=Self.Get_Pesquisa+'  where debitocredito=''C''';
end;

function TObjhistorico.Get_PesquisaDebito: string;
begin
     Result:=Self.Get_Pesquisa+'  where debitocredito=''D''';
end;


destructor TObjhistorico.Free;
begin
    Freeandnil(Self.ObjDataset);
end;


end.
