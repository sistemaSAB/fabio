unit UObjACERTOPENDENCIAS;
Interface
Uses windows,stdctrls,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc
,UOBJPENDENCIA
,UOBJTITULO
//USES


;

Type
   TObjACERTOPENDENCIAS=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               Pendencia:TOBJPENDENCIA;
               TituloResultante:TOBJTITULO;
//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Data(parametro: string);
                Function Get_Data: string;
                procedure EdtPendenciaExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtPendenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtTituloResultanteExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtTituloResultanteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                //CODIFICA DECLARA GETSESUBMITS



                
         Private
               ObjDataset:Tibdataset;
               CODIGO:string;
               Data:string;
//CODIFICA VARIAVEIS PRIVADAS


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
,uPENDENCIA
,uTITULO
//USES IMPLEMENTATION


;


{ TTabTitulo }


Function  TObjACERTOPENDENCIAS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('Pendencia').asstring<>'')
        Then Begin
                 If (Self.Pendencia.LocalizaCodigo(FieldByName('Pendencia').asstring)=False)
                 Then Begin
                          Messagedlg('Pendencia N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Pendencia.TabelaparaObjeto;
        End;
        Self.Data:=fieldbyname('Data').asstring;
        If(FieldByName('TituloResultante').asstring<>'')
        Then Begin
                 If (Self.TituloResultante.LocalizaCodigo(FieldByName('TituloResultante').asstring)=False)
                 Then Begin
                          Messagedlg('TituloResultante N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TituloResultante.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO




        result:=True;
     End;
end;


Procedure TObjACERTOPENDENCIAS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('Pendencia').asstring:=Self.Pendencia.GET_CODIGO;
        fieldbyname('Data').asstring:=Self.Data;
        fieldbyname('TituloResultante').asstring:=Self.TituloResultante.GET_CODIGO;
//CODIFICA OBJETOPARATABELA




  End;
End;

//***********************************************************************

function TObjACERTOPENDENCIAS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;



if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjACERTOPENDENCIAS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Pendencia.ZerarTabela;
        Data:='';
        TituloResultante.ZerarTabela;
//CODIFICA ZERARTABELA




     End;
end;

Function TObjACERTOPENDENCIAS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      If (Pendencia.Get_codigo='')
      Then Mensagem:=mensagem+'/Pendencia';
      If (Data='')
      Then Mensagem:=mensagem+'/Data';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjACERTOPENDENCIAS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
      If (Self.Pendencia.LocalizaCodigo(Self.Pendencia.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Pendencia n�o Encontrado!';

      if (Self.TituloResultante.get_codigo<>'')
      Then Begin
              If (Self.TituloResultante.LocalizaCodigo(Self.TituloResultante.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'/ TituloResultante n�o Encontrado!';
      End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjACERTOPENDENCIAS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        If (Self.Pendencia.Get_Codigo<>'')
        Then Strtoint(Self.Pendencia.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Pendencia';
     End;
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjACERTOPENDENCIAS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
//CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjACERTOPENDENCIAS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjACERTOPENDENCIAS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Pendencia,Data,TituloResultante');
           SelectSQL.ADD(' from  TabAcertoPendencias');
           SelectSQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjACERTOPENDENCIAS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjACERTOPENDENCIAS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjACERTOPENDENCIAS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.Pendencia:=TOBJPENDENCIA.create;
        Self.TituloResultante:=TOBJTITULO.create;
//CODIFICA CRIACAO DE OBJETOS



        ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select CODIGO,Pendencia,Data,TituloResultante');
                SelectSQL.ADD(' from  TabAcertoPendencias');
                SelectSQL.ADD(' WHERE codigo=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TabAcertoPendencias(CODIGO,Pendencia,Data');
                InsertSQL.add(' ,TituloResultante)');
                InsertSQL.add('values (:CODIGO,:Pendencia,:Data,:TituloResultante)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabAcertoPendencias set CODIGO=:CODIGO,Pendencia=:Pendencia');
                ModifySQL.add(',Data=:Data,TituloResultante=:TituloResultante');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabAcertoPendencias where codigo=:codigo ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,Pendencia,Data,TituloResultante');
                RefreshSQL.ADD(' from  TabAcertoPendencias');
                RefreshSQL.ADD(' WHERE codigo=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjACERTOPENDENCIAS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjACERTOPENDENCIAS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabACERTOPENDENCIAS');
     Result:=Self.ParametroPesquisa;
end;

function TObjACERTOPENDENCIAS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ACERTOPENDENCIAS ';
end;


function TObjACERTOPENDENCIAS.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
      StrTemp.StoredProcName:='PROC_GERA_ACERTOPENDENCIAS';
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o ACERTOPENDENCIAS',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjACERTOPENDENCIAS.Free;
begin
Freeandnil(Self.ObjDataset);
Freeandnil(Self.ParametroPesquisa);
Self.Pendencia.FREE;
Self.TituloResultante.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjACERTOPENDENCIAS.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjACERTOPENDENCIAS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjAcertoPendencias.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjAcertoPendencias.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjAcertoPendencias.Submit_Data(parametro: string);
begin
        Self.Data:=Parametro;
end;
function TObjAcertoPendencias.Get_Data: string;
begin
        Result:=Self.Data;
end;
//CODIFICA GETSESUBMITS

procedure TObjACERTOPENDENCIAS.EdtPendenciaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Pendencia.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
End;
procedure TObjACERTOPENDENCIAS.EdtPendenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPENDENCIA:TFPENDENCIA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPENDENCIA:=TFPENDENCIA.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Pendencia.Get_Pesquisa,Self.Pendencia.Get_TituloPesquisa,FPendencia)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Pendencia.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Pendencia.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Pendencia.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPENDENCIA);
     End;
end;
procedure TObjACERTOPENDENCIAS.EdtTituloResultanteExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TituloResultante.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
End;
procedure TObjACERTOPENDENCIAS.EdtTituloResultanteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FTITULO:TFTITULO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FTITULO:=TFTITULO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TituloResultante.Get_Pesquisa,Self.TituloResultante.Get_TituloPesquisa,Ftitulo)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TituloResultante.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.TituloResultante.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TituloResultante.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTITULO);
     End;
end;
//CODIFICA EXITONKEYDOWN





end.


