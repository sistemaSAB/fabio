object FcontaFacil: TFcontaFacil
  Left = 480
  Top = 183
  Width = 800
  Height = 600
  Caption = 'Conta F'#225'cil'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelSuperior: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 56
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 9
      Width = 93
      Height = 14
      Caption = 'Modelo de Conta'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtpesquisa: TEdit
      Left = 5
      Top = 24
      Width = 241
      Height = 21
      TabOrder = 0
      OnKeyPress = edtpesquisaKeyPress
    end
    object btok: TButton
      Left = 253
      Top = 22
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 1
      OnClick = btokClick
    end
    object btnovomodelo: TButton
      Left = 697
      Top = 22
      Width = 88
      Height = 25
      Caption = '&Novo Modelo'
      TabOrder = 2
      OnClick = btnovomodeloClick
    end
  end
  object PanelINferior: TPanel
    Left = 0
    Top = 190
    Width = 784
    Height = 372
    Align = alBottom
    TabOrder = 1
    object panelbotoes: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 31
      Align = alTop
      TabOrder = 8
      object btsalvarquitar: TButton
        Left = 506
        Top = 3
        Width = 90
        Height = 25
        Caption = 'Salvar e &Quitar'
        TabOrder = 0
        OnClick = btsalvarquitarClick
      end
      object btsalvar: TButton
        Left = 602
        Top = 3
        Width = 90
        Height = 25
        Caption = '&Salvar'
        TabOrder = 1
        OnClick = btsalvarClick
      end
      object btcancelar: TButton
        Left = 698
        Top = 3
        Width = 90
        Height = 25
        Caption = '&Cancelar'
        TabOrder = 2
        OnClick = btcancelarClick
      end
    end
    object PanelValor: TPanel
      Left = 1
      Top = 73
      Width = 782
      Height = 42
      Align = alTop
      TabOrder = 1
      object lbvalor: TLabel
        Left = 5
        Top = 1
        Width = 28
        Height = 14
        Caption = 'Valor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtvalor: TEdit
        Left = 5
        Top = 14
        Width = 121
        Height = 21
        TabOrder = 0
        OnKeyPress = edtvalorKeyPress
      end
    end
    object PanelCredorDevedor: TPanel
      Left = 1
      Top = 115
      Width = 782
      Height = 42
      Align = alTop
      TabOrder = 2
      object lbcredordevedor: TLabel
        Left = 5
        Top = 1
        Width = 88
        Height = 14
        Caption = 'Credor/Devedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtcredordevedor: TEdit
        Left = 5
        Top = 14
        Width = 121
        Height = 21
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 0
        OnKeyDown = edtcredordevedorKeyDown
      end
    end
    object panelcodigocredordevedor: TPanel
      Left = 1
      Top = 157
      Width = 782
      Height = 42
      Align = alTop
      TabOrder = 3
      object lbcodigocredordevedor: TLabel
        Left = 5
        Top = 1
        Width = 130
        Height = 14
        Caption = 'C'#243'digo Credor/Devedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtcodigocredordevedor: TEdit
        Left = 5
        Top = 14
        Width = 121
        Height = 21
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 0
        OnKeyDown = edtcodigocredordevedorKeyDown
      end
    end
    object panelportador: TPanel
      Left = 1
      Top = 241
      Width = 782
      Height = 42
      Align = alTop
      TabOrder = 5
      object lbportador: TLabel
        Left = 5
        Top = 1
        Width = 48
        Height = 14
        Caption = 'Portador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtportador: TEdit
        Left = 5
        Top = 14
        Width = 121
        Height = 21
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 0
        OnKeyDown = edtportadorKeyDown
      end
    end
    object panelcontagerencial: TPanel
      Left = 1
      Top = 283
      Width = 782
      Height = 42
      Align = alTop
      TabOrder = 6
      object lbcontagerencial: TLabel
        Left = 5
        Top = 1
        Width = 87
        Height = 14
        Caption = 'Conta Gerencial'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtcontagerencial: TEdit
        Left = 5
        Top = 14
        Width = 121
        Height = 21
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 0
        OnKeyDown = edtcontagerencialKeyDown
      end
    end
    object panelsubcontagerencial: TPanel
      Left = 1
      Top = 325
      Width = 782
      Height = 42
      Align = alTop
      TabOrder = 7
      object lbsubcontagerencial: TLabel
        Left = 5
        Top = 1
        Width = 112
        Height = 14
        Caption = 'Sub-Conta Gerencial'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtsubcontagerencial: TEdit
        Left = 5
        Top = 14
        Width = 121
        Height = 21
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 0
        OnKeyDown = edtsubcontagerencialKeyDown
      end
    end
    object panelprazo: TPanel
      Left = 1
      Top = 199
      Width = 782
      Height = 42
      Align = alTop
      TabOrder = 4
      object Lbprazo: TLabel
        Left = 5
        Top = 1
        Width = 31
        Height = 14
        Caption = 'Prazo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtprazo: TEdit
        Left = 5
        Top = 14
        Width = 121
        Height = 21
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 0
        OnKeyDown = edtprazoKeyDown
      end
    end
    object panelhistoricocontafacil: TPanel
      Left = 1
      Top = 32
      Width = 782
      Height = 41
      Align = alTop
      TabOrder = 0
      object Label2: TLabel
        Left = 5
        Top = 1
        Width = 49
        Height = 14
        Caption = 'Hist'#243'rico'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edthistoricocontafacil: TEdit
        Left = 5
        Top = 14
        Width = 751
        Height = 21
        TabOrder = 0
      end
    end
  end
  object DBGrid: TDBGrid
    Left = 0
    Top = 56
    Width = 784
    Height = 134
    Align = alClient
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
end
