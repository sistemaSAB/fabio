object Fcontager2: TFcontager2
  Left = 158
  Top = 58
  Width = 438
  Height = 293
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'Cadastro de Contas Gerenciais  - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 4
    Top = 4
    Width = 424
    Height = 257
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'MS Sans Serif'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      object Label1: TLabel
        Left = 4
        Top = 14
        Width = 39
        Height = 14
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 40
        Width = 32
        Height = 14
        Caption = 'Nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 4
        Top = 102
        Width = 24
        Height = 14
        Caption = 'Tipo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 189
        Top = 102
        Width = 228
        Height = 14
        Caption = 'C'#243'digo Plano de Contas (apenas cont'#225'bil)'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 45
        Top = 11
        Width = 72
        Height = 19
        CharCase = ecUpperCase
        MaxLength = 9
        TabOrder = 0
      end
      object edtnome: TEdit
        Left = 4
        Top = 56
        Width = 240
        Height = 19
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 1
      end
      object Combotipo: TComboBox
        Left = 4
        Top = 119
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 2
        Text = ' '
        Items.Strings = (
          'D - Contas a Pagar'
          'C - Contas a Receber')
      end
      object edtCodigoPlanodeContas: TMaskEdit
        Left = 190
        Top = 119
        Width = 49
        Height = 19
        Hint = 
          'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
          #225'bil)'
        BiDiMode = bdRightToLeft
        CharCase = ecUpperCase
        Color = clSkyBlue
        MaxLength = 9
        ParentBiDiMode = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnKeyDown = edtCodigoPlanodeContasKeyDown
        OnKeyPress = edtCodigoPlanodeContasKeyPress
      end
      object BtNovo: TBitBtn
        Left = 4
        Top = 150
        Width = 100
        Height = 38
        Caption = '&Novo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnClick = BtNovoClick
      end
      object btpesquisar: TBitBtn
        Left = 4
        Top = 190
        Width = 100
        Height = 38
        Caption = '&Pesquisar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        OnClick = btpesquisarClick
      end
      object btalterar: TBitBtn
        Left = 108
        Top = 150
        Width = 100
        Height = 38
        Caption = '&Alterar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        OnClick = btalterarClick
      end
      object btrelatorios: TBitBtn
        Left = 108
        Top = 190
        Width = 100
        Height = 38
        Caption = '&Relat'#243'rios'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        OnClick = btrelatoriosClick
      end
      object Btgravar: TBitBtn
        Left = 212
        Top = 150
        Width = 100
        Height = 38
        Caption = '&Gravar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        OnClick = BtgravarClick
      end
      object btexcluir: TBitBtn
        Left = 212
        Top = 190
        Width = 100
        Height = 38
        Caption = 'E&xcluir'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        OnClick = btexcluirClick
      end
      object BtCancelar: TBitBtn
        Left = 316
        Top = 150
        Width = 100
        Height = 38
        Caption = '&Cancelar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
        OnClick = BtCancelarClick
      end
      object btsair: TBitBtn
        Left = 316
        Top = 190
        Width = 100
        Height = 38
        Caption = '&Sair'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 11
        OnClick = btsairClick
      end
    end
  end
end
