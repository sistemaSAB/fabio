unit UConfValorParcelas;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons;

type
  TFConfValorParcelas = class(TForm)
    LBVencimentos: TListBox;
    LBValores: TListBox;
    EdtValor: TEdit;
    GroupBox1: TGroupBox;
    LabValorTotal: TLabel;
    GroupBox2: TGroupBox;
    labTotalparcelas: TLabel;
    btajustavalor: TButton;
    btsair: TBitBtn;
    btajustadata: TBitBtn;
    Label2: TLabel;
    Label1: TLabel;
    edtqtdeparcelas: TEdit;
    btajustaqtdeparcelas: TSpeedButton;
    btcancelar: TBitBtn;
    procedure EdtValorKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure LBValoresClick(Sender: TObject);
    procedure LBVencimentosClick(Sender: TObject);
    procedure LBVencimentosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LBValoresDblClick(Sender: TObject);
    procedure btajustavalorClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LBVencimentosDblClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure LBVencimentosKeyPress(Sender: TObject; var Key: Char);
    procedure btajustadataClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btajustaqtdeparcelasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtqtdeparcelasKeyPress(Sender: TObject; var Key: Char);
  private
         Procedure RecalculaParcelas;
         Function  SomaParcelas:Currency;
    { Private declarations }
  public
        ValorTotal:Currency;
        Function PegaValor(NumParcela:Integer):String;
        Function PegaVencimento(NumParcela:Integer):String;

    { Public declarations }
  end;

var
  FConfValorParcelas: TFConfValorParcelas;


implementation

uses UessencialGlobal, UData, DateUtils;

{$R *.DFM}

procedure TFConfValorParcelas.EdtValorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If key=#13
     Then Begin
              try
                 strtofloat(tira_Ponto(edtvalor.text));
                 if (LBValores.itemindex=-1)
                 or (LBVencimentos.itemindex=-1)
                 Then Begin
                           LBValores.itemindex:=0;
                           LBVencimentos.itemindex:=0;
                 End;

                 LBValores.items[LBValores.itemindex]:=Formata_Valor(edtvalor.text);
                 labTotalparcelas.caption:=Formata_valor(Floattostr(SomaParcelas));
                 edtvalor.text:='';

              Except

              End;
     End;


     If not (key in['0'..'9',#8,',','.'])
     Then key:=#0
     Else Begin
               If (Key='.')
               Then Key:=',';
          End;
end;

procedure TFConfValorParcelas.FormActivate(Sender: TObject);
begin
     Tag:=0;
     EdtValor.text:='';
     edtqtdeparcelas.text:=inttostr(LBValores.items.count);
   //  PegaFiguraBotao(btsair,'BOTAOSAIR.BMP');
   //  PegaFiguraBotao(btcancelar,'BOTAOCANCELAR.BMP');
end;

function TFConfValorParcelas.PegaValor(NumParcela: Integer): String;
begin

     Result:=LBValores.items[NumParcela-1];

end;

procedure TFConfValorParcelas.LBValoresClick(Sender: TObject);
begin
     LBVencimentos.ItemIndex:=LBValores.itemindex;
end;

procedure TFConfValorParcelas.LBVencimentosClick(Sender: TObject);
begin
     LBValores.itemindex:=LBVencimentos.ItemIndex;
end;

procedure TFConfValorParcelas.LBVencimentosKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     If Key=vk_return
     Then Begin
             if (Lbvalores.itemindex<0)
             Then Lbvalores.itemindex:=0;
             
             EdtValor.text:=Lbvalores.Items[Lbvalores.itemindex];
             edtvalor.setfocus;
          End;

end;

procedure TFConfValorParcelas.LBValoresDblClick(Sender: TObject);
begin
     EdtValor.text:=Lbvalores.Items[Lbvalores.itemindex];
     edtvalor.setfocus;
end;

procedure TFConfValorParcelas.RecalculaParcelas;
var
QuantFalta,cont:Integer;
ValorParcela,SomaAtual:Currency;
ValorParcelaStr:String;

begin

     //identifica onde esta o itemindex atual
     //soma ele e os anteriores
     somaAtual:=0;
     for cont:=0 to Lbvalores.itemindex do
      somaAtual:=somaatual+StrTofloat(Tira_ponto(lbvalores.items[cont]));

      
      If (SomaAtual<ValorTotal)
      Then Begin
                QuantFalta:=LbValores.items.count-1-LbValores.itemindex;//quantidade que falta

                ValorParcela:=(ValorTotal-somaatual)/QuantFalta;
                ValorParcelaStr:='';
                ValorParcelaStr:=Formata_valor(ArredondaValor(ValorParcela));
                for cont:=Lbvalores.itemindex+1 to LbValores.items.count-1 do
                 LbValores.items[cont]:=ValorParcelaStr;

                somaAtual:=0;
                for cont:=0 to Lbvalores.items.count-1 do
                  somaAtual:=somaatual+StrTofloat(Tira_ponto(lbvalores.items[cont]));

                if ((ValorTotal-SomaAtual)>0)
                Then lbvalores.items[lbvalores.items.count-1]:=formata_valor(floattostr(strtofloat(tira_ponto(lbvalores.items[lbvalores.items.count-1]))+(ValorTotal-SomaAtual)))

      End
      Else Begin
                     for cont:=Lbvalores.itemindex+1 to LbValores.items.count-1 do
                        LbValores.items[cont]:='0,00';
           End;
      labTotalparcelas.caption:=formata_valor(FloatToStr(SomaParcelas));

end;

function TFConfValorParcelas.SomaParcelas: Currency;
var
Cont:Integer;
Soma:Currency;
begin
     soma:=0;
     for cont:=0 to LbValores.items.count-1 do
       soma:=soma+StrToFloat(Tira_ponto(LbValores.items[cont]));

     Result:=Soma;
end;

procedure TFConfValorParcelas.btajustavalorClick(Sender: TObject);
begin
     RecalculaParcelas;
end;

procedure TFConfValorParcelas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     labTotalparcelas.caption:=formata_valor(Floattostr(SomaParcelas));
end;

procedure TFConfValorParcelas.LBVencimentosDblClick(Sender: TObject);
begin
        IF (Fdata=nil)
        then Application.CreateForm(TFdata,Fdata);
        Fdata.edtdata.text:=LBVencimentos.items[lbvencimentos.itemindex];
        Fdata.ShowModal;  //***ALEX - ABRE FORM

        if (Fdata.tag=1)
        Then Begin
                  Try
                     strtodate(Fdata.edtdata.text);
                     LBVencimentos.Items[lbvencimentos.itemindex]:=Fdata.edtdata.text;
                  Except
                        mensagemerro('Data inv�lida');
                        exit;
                  End;
        End;
        FreeAndNil(Fdata);
{
var
TOK:boolean;
begin
     if (lbvencimentos.itemindex<0)
     Then lbvencimentos.itemindex:=0;

     Repeat
           Fdata.edtdata.text:=LBVencimentos.items[lbvencimentos.itemindex];
           If (Fdata.ShowModal=mrcancel)
           Then TOK:=False
           Else Begin
                     LBVencimentos.Items[lbvencimentos.itemindex]:=Fdata.edtdata.text;
                     TOK:=True;
           End;
     Until(TOK=True);

end;
}        
End;

procedure TFConfValorParcelas.btsairClick(Sender: TObject);
begin

     Close;
end;

function TFConfValorParcelas.PegaVencimento(NumParcela: Integer): String;
begin
     Result:=LBVencimentos.items[NumParcela-1];
end;

procedure TFConfValorParcelas.LBVencimentosKeyPress(Sender: TObject;
  var Key: Char);
begin
     If Key=#13
     Then Begin
               LBVencimentosDblClick(sender);
     End;
end;

procedure TFConfValorParcelas.btajustadataClick(Sender: TObject);
var
Novadata,DataTmp:Tdate;
cont:integer;
dia, mes,ano:word;
begin
     if (LBVencimentos.Items.Count=1)
     Then exit;

     if (LBVencimentos.ItemIndex<0)
     Then LBVencimentos.ItemIndex:=0;

     DataTmp:=StrToDate(LBVencimentos.Items[LBVencimentos.itemindex]);

     //a partir do atual
     DecodeDate(datatmp,ano,mes,dia);
     Novadata:=DataTmp;
     for cont:=LBVencimentos.itemindex+1 to LBVencimentos.Items.count-1 do
     Begin
          Novadata:=IncMonth(novadata,1);
          LBVencimentos.Items[cont]:=datetostr(novadata);
     End;
end;

procedure TFConfValorParcelas.btcancelarClick(Sender: TObject);
begin
     tag:=1;
     close;
end;

procedure TFConfValorParcelas.btajustaqtdeparcelasClick(Sender: TObject);
var
cont,qt:Integer;
paralela1,paralela2:TStringList;
dataatual:string;
Novadata,DataTmp:Tdate;
dia, mes,ano:word;
begin
     dataatual:=formatdatetime ('dd/mm/yyyy',now);
     Novadata:=strtodate(dataatual);
     Try
        paralela1:=TStringList.create;
        paralela2:=TStringList.create;
     Except
           exit;
     End;

     Try

       Try
          qt:=strtoint(edtqtdeparcelas.text);
       Except
             qt:=0;
             exit;
       End;

       if (qt=LBValores.Items.Count)
       Then exit;
       
       if (qt<LBValores.Items.Count)
       Then Begin
                 paralela1.clear;
                 paralela2.clear;
                 for cont:=0 to qt-1 do
                 Begin
                      paralela1.add(LBValores.Items[cont]);
                      paralela2.add(LBVencimentos.Items[cont]);
                 End;
                 LBVencimentos.items.clear;
                 LBValores.Items.clear;

                 for cont:=0 to qt-1 do
                 Begin
                      LBValores.items.add(paralela1[cont]);
                      LBVencimentos.Items.add(paralela2[cont]);
                 End;
       End
       Else Begin
                 for cont:=1 to (qt-LBValores.Items.count) do
                 Begin
                      LBValores.Items.add('0');
                      IncMonth(Novadata,1);
                      Novadata:=EndOfTheMonth(Novadata);
                      //showmessage(DateToStr(Novadata));
                      //showmessage(DateToStr(Novadata));
                      //LBVencimentos.items.add('01/01/1500');
                      LBVencimentos.items.add(DateToStr(Novadata));
                 End;
       End;
       labTotalparcelas.caption:=floattostr(SomaParcelas);

     Finally
            FreeAndNil(paralela1);
            FreeAndNil(paralela2);
     End;

end;

procedure TFConfValorParcelas.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
     edtqtdeparcelas.setfocus;
end;

procedure TFConfValorParcelas.edtqtdeparcelasKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then btajustaqtdeparcelasClick(sender);
end;

end.
