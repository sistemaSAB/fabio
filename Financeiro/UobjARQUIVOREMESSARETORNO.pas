unit UobjARQUIVOREMESSARETORNO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjARQUIVOREMESSARETORNO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto(PSalvaAquivoEmDiretorio:Boolean):Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: String);
                Function Get_Codigo: String;
                Procedure Submit_ArquivoRemessa(parametro: String);
                Function Get_ArquivoRemessa: String;
                Procedure Submit_ArquivoRetorno(parametro: String);
                Function Get_ArquivoRetorno: String;
                Procedure Submit_ArquivoRemessaBinario(parametro: String);
                Function Get_ArquivoRemessaBinario: String;
                Procedure Submit_ArquivoRetornoBinario(parametro: String);
                Function Get_ArquivoRetornoBinario: String;
                Procedure Submit_Data(parametro: String);
                Function Get_Data: String;
                Procedure Submit_CodigoBanco(Parametro:String);
                Function Get_CodigoBanco:String;

                Function RetornaNomeArquivoSicredi(PCodigoBanco, PCodigoCedente:String):String;
                Function RetornaNomeArquivoItau(PCodigoBanco, PCodigoCedente:String):String;
                Function RetornaNomeArquivoCEF(PCodigoBanco, PCodigoCedente:String):String;


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:String;
               ArquivoRemessa:String;
               ArquivoRetorno:String;
               ArquivoRemessaBinario:String;
               ArquivoRetornoBinario:String;
               Data:String;
               CodigoBanco:String;


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;

Function  TObjARQUIVOREMESSARETORNO.TabelaparaObjeto(PSalvaAquivoEmDiretorio:Boolean):Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
Var  MemoryStream:TMemoryStream;
     TempPath:String;
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.ArquivoRemessa:=fieldbyname('ArquivoRemessa').asstring;
        Self.ArquivoRetorno:=fieldbyname('ArquivoRetorno').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.CodigoBanco:=fieldbyname('CodigoBanco').AsString;


        if (Self.ArquivoRemessa <> '')
        then Begin
                try
                      try
                            // transferindo o arquivo para o objeto TStream
                            MemoryStream:=TMemoryStream.Create;
                            MemoryStream.LoadFromStream(Objquery.CreateBlobStream(Objquery.FieldByName('ArquivoRemessaBinario'),bmRead));
                            if (PSalvaAquivoEmDiretorio = true)
                            then MemoryStream.SaveToFile(Self.ArquivoRemessa);
                      except
                            MensagemErro('Erro ao tentar recriar o arquivo "'+Self.ArquivoRemessa+'"');
                            Result:=false;
                            exit;
                      end;
                finally
                      // Liberando memoria
                      freeandnil(memorystream);
                end;
        end;

        if (Self.ArquivoRetorno <> '')
        then Begin
                try
                      try
                            // transferindo o arquivo para o objeto TStream
                            MemoryStream:=TMemoryStream.Create;
                            MemoryStream.LoadFromStream(Objquery.CreateBlobStream(Objquery.FieldByName('ArquivoRetornoBinario'),bmRead));
                            if (PSalvaAquivoEmDiretorio=true)
                            then MemoryStream.SaveToFile(Self.ArquivoRetorno);
                      except
                            MensagemErro('Erro ao tentar recriar o arquivo "'+Self.ArquivoRetorno+'"');
                            Result:=false;
                            exit;
                      end;
                finally
                      // Liberando memoria
                      freeandnil(memorystream);
                end;
        end;


        result:=True;
     End;
end;

Procedure TObjARQUIVOREMESSARETORNO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('ArquivoRemessa').asstring:=Self.ArquivoRemessa;
        ParamByName('ArquivoRetorno').asstring:=Self.ArquivoRetorno;
        ParamByName('ArquivoRetornoBinario').asstring:=Self.ArquivoRetornoBinario;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('CodigoBanco').AsString:=Self.CodigoBanco; 

        if (Self.ArquivoRemessa <> '')
        then Begin
                try
                    ParamByName('ArquivoRemessaBinario').LoadFromFile(Self.ArquivoRemessa,ftBlob);
                except
                    MensagemErro('Erro ao tentar Relacionar o arquivo "'+Self.ArquivoRemessa+'" com o Banco de Dados');
                    ParamByName('Codigo').asstring:=''; // S� pra nuam Gravar
                end;
        end;

        if (Self.ArquivoRetorno <> '')
        then Begin
                try
                    ParamByName('ArquivoRetornoBinario').LoadFromFile(Self.ArquivoRetorno,ftBlob);
                except
                    MensagemErro('Erro ao tentar Relacionar o arquivo "'+Self.ArquivoRetorno+'" com o Banco de Dados');
                    ParamByName('Codigo').asstring:=''; // S� pra nuam Gravar
                end;
        end;

  End;
End;

//***********************************************************************

function TObjARQUIVOREMESSARETORNO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjARQUIVOREMESSARETORNO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        ArquivoRemessa:='';
        ArquivoRetorno:='';
        Data:='';
        CodigoBanco:='';

     End;
end;

Function TObjARQUIVOREMESSARETORNO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjARQUIVOREMESSARETORNO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjARQUIVOREMESSARETORNO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjARQUIVOREMESSARETORNO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjARQUIVOREMESSARETORNO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjARQUIVOREMESSARETORNO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ARQUIVOREMESSARETORNO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select * from  TABARQUIVOREMESSARETORNO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjARQUIVOREMESSARETORNO.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjARQUIVOREMESSARETORNO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjARQUIVOREMESSARETORNO.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjARQUIVOREMESSARETORNO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABARQUIVOREMESSARETORNO(Codigo,ArquivoRemessa');
                InsertSQL.add(' ,ArquivoRetorno,ArquivoRemessaBinario,ArquivoRetornoBinario');
                InsertSQL.add(' ,Data,CodigoBanco)');
                InsertSQL.add('values (:Codigo,:ArquivoRemessa,:ArquivoRetorno,:ArquivoRemessaBinario');
                InsertSQL.add(' ,:ArquivoRetornoBinario,:Data,:CodigoBanco)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABARQUIVOREMESSARETORNO set Codigo=:Codigo,ArquivoRemessa=:ArquivoRemessa');
                ModifySQL.add(',ArquivoRetorno=:ArquivoRetorno,ArquivoRemessaBinario=:ArquivoRemessaBinario');
                ModifySQL.add(',ArquivoRetornoBinario=:ArquivoRetornoBinario,Data=:Data,CodigoBanco=:CodigoBanco');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABARQUIVOREMESSARETORNO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjARQUIVOREMESSARETORNO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjARQUIVOREMESSARETORNO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabARQUIVOREMESSARETORNO');
     Result:=Self.ParametroPesquisa;
end;

function TObjARQUIVOREMESSARETORNO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ARQUIVOREMESSARETORNO ';
end;


function TObjARQUIVOREMESSARETORNO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENARQUIVOREMESSARETORNO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENARQUIVOREMESSARETORNO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjARQUIVOREMESSARETORNO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjARQUIVOREMESSARETORNO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjARQUIVOREMESSARETORNO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjArquivoRemessaRetorno.Submit_Codigo(parametro: String);
begin
        Self.Codigo:=Parametro;
end;
function TObjArquivoRemessaRetorno.Get_Codigo: String;
begin
        Result:=Self.Codigo;
end;
procedure TObjArquivoRemessaRetorno.Submit_ArquivoRemessa(parametro: String);
begin
        Self.ArquivoRemessa:=Parametro;
end;
function TObjArquivoRemessaRetorno.Get_ArquivoRemessa: String;
begin
        Result:=Self.ArquivoRemessa;
end;
procedure TObjArquivoRemessaRetorno.Submit_ArquivoRetorno(parametro: String);
begin
        Self.ArquivoRetorno:=Parametro;
end;
function TObjArquivoRemessaRetorno.Get_ArquivoRetorno: String;
begin
        Result:=Self.ArquivoRetorno;
end;
procedure TObjArquivoRemessaRetorno.Submit_ArquivoRemessaBinario(parametro: String);
begin
        Self.ArquivoRemessaBinario:=Parametro;
end;
function TObjArquivoRemessaRetorno.Get_ArquivoRemessaBinario: String;
begin
        Result:=Self.ArquivoRemessaBinario;
end;
procedure TObjArquivoRemessaRetorno.Submit_ArquivoRetornoBinario(parametro: String);
begin
        Self.ArquivoRetornoBinario:=Parametro;
end;
function TObjArquivoRemessaRetorno.Get_ArquivoRetornoBinario: String;
begin
        Result:=Self.ArquivoRetornoBinario;
end;
procedure TObjArquivoRemessaRetorno.Submit_Data(parametro: String);
begin
        Self.Data:=Parametro;
end;
function TObjArquivoRemessaRetorno.Get_Data: String;
begin
        Result:=Self.Data;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjARQUIVOREMESSARETORNO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJARQUIVOREMESSARETORNO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjARQUIVOREMESSARETORNO.Get_CodigoBanco: String;
begin
    Result:=CodigoBanco;
end;

procedure TObjARQUIVOREMESSARETORNO.Submit_CodigoBanco(Parametro: String);
begin
    Self.CodigoBanco:=Parametro;
end;

function TObjARQUIVOREMESSARETORNO.RetornaNomeArquivoSicredi(PCodigoBanco, PCodigoCedente: String): String;
Var Mes:Integer;
    NomeArquivo, MesStr:String;
begin
    Result:='';

    if (PCodigoBAnco = '748')
    then Begin
            //CCCCCmdd.CRM  -  Padr�o Banco Sicredi
            //CCCCC : Codigo do Cedente
            //    m : m�s (1=jan, 2=fev, ...O=Out, N=Nov, D-Dez)
            //   dd : dia
            Mes:=StrToInt(FormatDateTime('mm',Now));
            Case Mes of
                  1 : MesStr:='1';
                  2 : MesStr:='2';
                  3 : MesStr:='3';
                  4 : MesStr:='4';
                  5 : MesStr:='5';
                  6 : MesStr:='6';
                  7 : MesStr:='7';
                  8 : MesStr:='8';
                  9 : MesStr:='9';
                  10 : MesStr:='O';
                  11 : MesStr:='N';
                  12 : MesStr:='D';
            end;

            NomeArquivo:=PCodigoCedente+MesStr+FormatDateTime('dd',now);

            // A extens�ao do arquivo depende da quantidade de vezes que o cliente
            // est� gerando o arquivo.
            //Ex.: Se for o 1� Arq.= *.CRM, 2� Arq.= *.RM2, 3� Arq. = RM2...

            // Verificano a Quantidade e Arquivos enviados no dia
            Self.Objquery.Close;
            Self.Objquery.SQL.Clear;
            Self.Objquery.SQL.Add('Select Count(Codigo) as Qtde from  TabArquivoRemessaRetorno');
            Self.Objquery.SQL.Add('Where CodigoBanco = '+#39+PCodigoBanco+#39);
            Self.Objquery.SQL.Add('and Data = '+#39+FormatDateTime('mm/dd/yyyy',Now)+#39);
            Self.Objquery.Open;
            Self.Objquery.Last;

            if (Self.Objquery.fieldbyname('Qtde').AsInteger = 0)
            then NomeArquivo:=NomeArquivo+'.CRM'
            else NomeArquivo:=NomeArquivo+'.RM'+IntToStr(Self.Objquery.fieldbyname('Qtde').AsInteger+1);

            Result:=NomeArquivo;
            exit;
    end;

end;

function TObjARQUIVOREMESSARETORNO.RetornaNomeArquivoItau(PCodigoBanco,
  PCodigoCedente: String): String;
var
  i :integer;
begin
  Result:='';

  // Verificano a Quantidade e Arquivos enviados no dia
  Self.Objquery.Close;
  Self.Objquery.SQL.Clear;
  Self.Objquery.SQL.Add('Select Count(Codigo) as Qtde from  TabArquivoRemessaRetorno');
  Self.Objquery.SQL.Add('Where CodigoBanco = '+#39+PCodigoBanco+#39);
  Self.Objquery.Open;
  Self.Objquery.Last;
  i := Self.Objquery.fieldbyname('Qtde').AsInteger;

  Inc(i,1);
  Result := IntToStr(i);

  while Length(Result) < 8 do
    Result := '0' + Result;
  Result := Result + '.rem';
end;

function TObjARQUIVOREMESSARETORNO.RetornaNomeArquivoCEF(PCodigoBanco,
  PCodigoCedente: String): String;
var
  i :integer;
begin
  Result:='';

  // Verificano a Quantidade e Arquivos enviados no dia
  Self.Objquery.Close;
  Self.Objquery.SQL.Clear;
  Self.Objquery.SQL.Add('Select Count(Codigo) as Qtde from  TabArquivoRemessaRetorno');
  Self.Objquery.SQL.Add('Where CodigoBanco = '+#39+PCodigoBanco+#39);
  Self.Objquery.Open;
  Self.Objquery.Last;
  i := Self.Objquery.fieldbyname('Qtde').AsInteger;

  Inc(i,1);
  Result := IntToStr(i);

  while Length(Result) < 8 do
    Result := '0' + Result;
  Result := Result + '.rem';
end;

end.



