unit UObjCredorDevedor;
Interface
Uses Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,Classes;

Type
   TObjCredorDevedor=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar                          :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaCredorDevedor(Parametro:string) :boolean;
                Function    LocalizaNome(Parametro:string):Boolean;
                Function    LocalizaTabela(Parametro:String):Integer;

                Function    exclui(Pcodigo:string)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;


                Function Get_CODIGO              :string;
                Function Get_Nome                :string;
                Function Get_Tabela              :string;
                Function Get_Objeto              :string;
                Function Get_InstrucaoSQL        :string;
                Function Get_CampoNome           :string;
                Function Get_CampoRazaoSocial    :string;
                Function Get_NomeFormulario      :string;
                Function Get_CampoContabil       :string;
                Function Get_CAMPOCPFCNPJ        :string;
                Function Get_CAMPORGIE           :string;
                Function Get_CampoContaGerencial :string;

                Function Get_Telefone            :string;
                Function Get_Endereco            :string;
                Function Get_Bairro              :string;
                Function Get_CampoCep            :string;
                Function Get_CampoEstado         :string;
                Function Get_CampoCidade         :string;
                Function Get_Celular             :string;
                Function Get_CampoNumeroCasa     :string;
                Function Get_CampoCredito        :string;

                Function Get_TelefoneCobranca            :string;
                Function Get_EnderecoCobranca            :string;
                Function Get_BairroCobranca              :string;
                Function Get_CampoCepCobranca            :string;
                Function Get_CampoEstadoCobranca         :string;
                Function Get_CampoCidadeCobranca         :string;
                Function Get_CelularCobranca             :string;
                Function Get_CampoNumeroCasaCobranca     :string;

                Procedure Submit_CODIGO              (parametro:string);
                Procedure Submit_Nome                (parametro:string);
                Procedure Submit_Tabela              (parametro:string);
                Procedure Submit_Objeto              (parametro:string);
                Procedure Submit_InstrucaoSQL        (parametro:string);
                Procedure Submit_CampoNome           (parametro:string);
                Procedure Submit_CampoRazaoSocial    (parametro:string);
                Procedure Submit_NomeFormulario      (parametro:string);
                Procedure Submit_CampoContabil       (Parametro:string);
                Procedure Submit_CAMPOCPFCNPJ        (Parametro:string);
                Procedure Submit_CAMPORGIE           (Parametro:string);
                Procedure Submit_CampoContaGerencial (parametro:string);

                Procedure Submit_Telefone            (parametro:string);
                Procedure Submit_Endereco            (parametro:string);
                Procedure Submit_Bairro              (parametro:string);
                Procedure Submit_CampoCep            (parametro:string);
                Procedure Submit_CampoEstado         (parametro:string);
                Procedure Submit_CampoCidade         (parametro:string);
                Procedure Submit_Celular             (parametro:string);
                Procedure Submit_CampoNumeroCasa     (parametro:string);

                Procedure Submit_TelefoneCobranca            (parametro:string);
                Procedure Submit_EnderecoCobranca            (parametro:string);
                Procedure Submit_BairroCobranca              (parametro:string);
                Procedure Submit_CampoCepCobranca            (parametro:string);
                Procedure Submit_CampoEstadoCobranca         (parametro:string);
                Procedure Submit_CampoCidadeCobranca         (parametro:string);
                Procedure Submit_CelularCobranca             (parametro:string);
                Procedure Submit_CampoNumeroCasaCobranca     (parametro:string);

                Procedure Submit_CampoCredito        (parametro:string);

                Procedure Get_listaNomeCredorDevedor(parametro:Tstrings);
                Procedure Get_listaCodigoCredorDevedor(parametro:Tstrings);

                //estes procedimentos buscam a informacao tabela final
                Function  Get_ContaGerencial(PCodigoCredorDevedor,PCampoPrimario: string): string;
                Function  Get_NomeCredorDevedor(PcredorDevedor,PcodigoCredorDevedor:string):string;
                Function  Get_CampoContabilCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                Function  Get_RazaoSocialCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                Function  Get_CPFCNPJCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                Function  Get_RGIECredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;

                function  Get_EnderecoCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_BairroCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_TelefoneCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_CelularCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_CidadeCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                Function  Get_NumeroCasaCredorDevedor(PcredorDevedor,PcodigoCredorDevedor:string):string;
                Function  Get_CreditoCredorDevedor(PcredorDevedor,PcodigoCredorDevedor:string):string;
                function  Get_CepCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_EstadoCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;


                //******celio******270608****

                function  Get_EnderecoCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_BairroCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_TelefoneCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_CelularCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_CidadeCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor: string): string;
                Function  Get_NumeroCasaCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor:string):string;
                function  Get_CepCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor: string): string;
                function  Get_EstadoCredorDevedorCobranca(PcredorDevedor,PcodigoCredorDevedor: string): string;

                //******celio****************

                //*********************************
                function Get_NovoCodigo: string;
         Private
               ObjDataset:Tibdataset;
               ParametroPesquisa:TStringList;

               CODIGO                  :String[09];
               Nome                    :String[50];
               Tabela                  :String[50];
               Objeto                  :String[50];
               InstrucaoSQL            :String[100];
               NomeFormulario          :String[50];

               Telefone                :string;
               Endereco                :string;
               Bairro                  :string;
               CampoCep                :string;
               CampoEstado             :string;
               CampoCidade             :string;
               Celular                 :string;
               CampoNumeroCasa         :string;

               //dados para cobranca
               TelefoneCobranca        :string;
               EnderecoCobranca        :string;
               BairroCobranca          :string;
               CampoCepCobranca        :string;
               CampoEstadoCobranca     :string;
               CampoCidadeCobranca     :string;
               CelularCobranca         :string;
               CampoNumeroCasaCobranca :string;

               //Este campo Indica o nome do campo
               //que define o nome, tipo
               //o de aluno � nome, o de professor � nome,
               //o de fornecedor � fantasia.....
               CampoNome               :string;
               CampoRazaoSocial        :string;
               CAMPOCPFCNPJ            :string;
               CAMPORGIE               :string;
               CampoContaGerencial     :string;
               CampoCredito            :string;

               //*******
               //Este campo define se este credor/devedor
               //possui algum algum campo com codigo
               //do plano de contas contabil
               //para que seja gerado uma exportacao

               CampoContabil           :string;



               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure Get_listaCredorDevedor(parametro: Tstrings);


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Procedure  TObjCredorDevedor.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO        := FieldByName('CODIGO').asstring          ;
        Self.Nome          := FieldByName('Nome').asstring            ;
        Self.Tabela        := FieldByName('Tabela').asstring          ;
        Self.Objeto        := FieldByName('Objeto').asstring          ;
        Self.InstrucaoSQL  := FieldByName('InstrucaoSQL').asstring    ;
        Self.CampoNome     := FieldByName('CampoNome').asstring       ;
        Self.NomeFormulario:= FieldByName('NomeFormulario').asstring  ;
        Self.CampoContabil:= FieldByName('CampoContabil').asstring  ;
        Self.CampoRazaoSocial:= FieldByName('CampoRazaoSocial').asstring  ;
        Self.CAMPOCPFCNPJ       := FieldByName('CAMPOCPFCNPJ').asstring  ;
        Self.CAMPORGIE          := FieldByName('CAMPORGIE').asstring  ;
        Self.campoConTagerencial     :=Fieldbyname('campocontagerencial').asstring;

        Self.Endereco:=fieldbyname('endereco').asstring;
        Self.CampoNumeroCasa:=Fieldbyname('CampoNumeroCasa').asstring;
        Self.Bairro:=fieldbyname('bairro').asstring;
        Self.Telefone:=fieldbyname('telefone').asstring;
        Self.CampoCep:=Fieldbyname('campocep').asstring;
        Self.CampoEstado:=Fieldbyname('campoestado').asstring;
        Self.CampoCidade:=Fieldbyname('CampoCidade').asstring;
        Self.Celular:=Fieldbyname('Celular').asstring;


        Self.CampoCredito:=Fieldbyname('campocredito').asstring;

        Self.EnderecoCobranca:=fieldbyname('enderecocobranca').asstring;
        Self.CampoNumeroCasaCobranca:=Fieldbyname('CampoNumeroCasacobranca').asstring;
        Self.BairroCobranca:=fieldbyname('bairrocobranca').asstring;
        Self.TelefoneCobranca:=fieldbyname('telefonecobranca').asstring;
        Self.CampoCepCobranca:=Fieldbyname('campocepcobranca').asstring;
        Self.CampoEstadoCobranca:=Fieldbyname('campoestadocobranca').asstring;
        Self.CampoCidadeCobranca:=Fieldbyname('CampoCidadecobranca').asstring;
        Self.CelularCobranca:=Fieldbyname('Celularcobranca').asstring;


     End;
end;


Procedure TObjCredorDevedor.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
        FieldByName('CODIGO').asstring        := Self.CODIGO       ;
        FieldByName('Nome').asstring          := Self.Nome         ;
        FieldByName('Tabela').asstring        := Self.Tabela       ;
        FieldByName('Objeto').asstring        := Self.Objeto       ;
        FieldByName('InstrucaoSQL').asstring  := Self.InstrucaoSQL ;
        FieldByName('Camponome').asstring     := Self.CampoNome    ;
        FieldByName('NomeFormulario').asstring:= Self.NomeFormulario;
        FieldByName('CampoContabil').asstring:= Self.CampoContabil;
        FieldByName('CampoRazaoSocial').asstring:=Self.CampoRazaoSocial;
        FieldByName('CAMPOCPFCNPJ').asstring:=Self.CAMPOCPFCNPJ;
        FieldByName('CAMPORGIE').asstring:=Self.CAMPORGIE;
        Fieldbyname('campocontagerencial').asstring:=Self.CampoContaGerencial;

        fieldbyname('endereco').asstring:=Self.Endereco;
        fieldbyname('CampoNumeroCasa').asstring:=Self.CampoNumeroCasa;
        fieldbyname('bairro').asstring:=Self.Bairro;
        fieldbyname('telefone').asstring:=Self.Telefone;
        Fieldbyname('campocep').asstring:=Self.CampoCep;
        Fieldbyname('campoestado').asstring:=Self.CampoEstado;
        Fieldbyname('CampoCidade').asstring:=Self.CampoCidade;
        Fieldbyname('Celular').asstring:=Self.Celular;

        Fieldbyname('campocredito').asstring:=Self.CampoCredito;

        fieldbyname('enderecocobranca').asstring:=Self.EnderecoCobranca;
        fieldbyname('CampoNumeroCasacobranca').asstring:=Self.CampoNumeroCasaCobranca;
        fieldbyname('bairrocobranca').asstring:=Self.BairroCobranca;
        fieldbyname('telefonecobranca').asstring:=Self.TelefoneCobranca;
        Fieldbyname('campocepcobranca').asstring:=Self.CampoCepCobranca;
        Fieldbyname('campoestadocobranca').asstring:=Self.CampoEstadoCobranca;
        Fieldbyname('CampoCidadecobranca').asstring:=Self.CampoCidadeCobranca;
        Fieldbyname('Celularcobranca').asstring:=Self.CelularCobranca;

  End;
End;

//***********************************************************************

function TObjCredorDevedor.Salvar: Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin

           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;
 FDataModulo.IBTransaction.CommitRetaining;
 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjCredorDevedor.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO        :='';
        Self.Nome          :='';
        Self.Tabela        :='';
        Self.Objeto        :='';
        Self.InstrucaoSQL  :='';
        Self.CampoNome     :='';
        Self.NomeFormulario:='';
        Self.CampoContabil :='';
        Self.CampoRazaoSocial:='';
        Self.CAMPOCPFCNPJ  :='';
        Self.CAMPORGIE     :='';
        Self.CampoContaGerencial:='';

        Self.Telefone:='';
        Self.Bairro:='';
        Self.Endereco:='';
        self.CampoNumeroCasa:='';
        Self.CampoCep:='';
        Self.CampoEstado:='';
        Self.CampoCidade:='';
        Self.Celular:='';

        Self.CampoCredito:='';

        Self.TelefoneCobranca:='';
        Self.BairroCobranca:='';
        Self.EnderecoCobranca:='';
        self.CampoNumeroCasaCobranca:='';
        Self.CampoCepCobranca:='';
        Self.CampoEstadoCobranca:='';
        Self.CampoCidadeCobranca:='';
        Self.CelularCobranca:='';
        
     End;
end;

Function TObjCredorDevedor.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Nome='')
       Then Mensagem:=mensagem+'/Nome';

       If (Tabela='')
       Then Mensagem:=mensagem+'/Tabela';

       If (Objeto='')
       Then Mensagem:=mensagem+'/Objeto';

       If (InstrucaoSQL='')
       Then Mensagem:=mensagem+'/Instru��o SQL';

       If (CampoNome='')
       Then Mensagem:=mensagem+'/Campo Nome';

       If (CampoRazaoSocial='')
       Then Mensagem:=mensagem+'/Campo Nome da Raz�o Social';


       If (NomeFormulario='')
       Then Mensagem:=mensagem+'/Nome do Formulario';

       If (CAMPOCPFCNPJ='')
       Then Mensagem:=mensagem+'/Campo CPF/CNPJ';

       If (CAMPORGIE='')
       Then Mensagem:=mensagem+'/Campo RG/IE';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjCredorDevedor.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjCredorDevedor.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjCredorDevedor.VerificaData: Boolean;
var
Datas:Tdate;
Mensagem:string;
begin
     mensagem:='';

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjCredorDevedor.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjCredorDevedor.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,tabela,objeto,instrucaoSQL,camponome,nomeformulario,');
           SelectSql.add('campocontabil,camporazaosocial,CAMPOCPFCNPJ,CAMPORGIE,campocontagerencial,telefone,endereco,');
           SelectSql.add('CampoNumeroCasa,bairro,campocep,campoestado,campocidade,celular,campocredito,telefonecobranca,');
           SelectSql.add('enderecocobranca,CampoNumeroCasacobranca,bairrocobranca,campocepcobranca,campoestadocobranca,');
           SelectSql.add('campocidadecobranca,celularcobranca from Tabcredordevedor where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjCredorDevedor.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjCredorDevedor.Exclui(Pcodigo: string): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCredorDevedor.create;
begin
        ZerarTabela;

        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,nome,tabela,objeto,instrucaoSQL,');
                SelectSql.add('camponome,nomeformulario,campocontabil,camporazaosocial,CAMPOCPFCNPJ,CAMPORGIE,');
                selectsql.add('campocontagerencial,telefone,endereco,CampoNumeroCasa,bairro,campocep,campoestado,');
                SelectSQL.Add('campocidade,celular,campocredito,telefonecobranca,enderecocobranca,CampoNumeroCasacobranca,');
                SelectSQL.Add('bairrocobranca,campocepcobranca,campoestadocobranca,campocidadecobranca,celularcobranca');
                SelectSQL.Add(' from Tabcredordevedor where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert into Tabcredordevedor (codigo,nome,tabela,objeto,instrucaoSQL,camponome,nomeformulario,');
                InsertSQL.add(' campocontabil,camporazaosocial,CAMPOCPFCNPJ,CAMPORGIE,campocontagerencial,telefone,endereco,');
                InsertSQL.add(' CampoNumeroCasa,bairro,campocep,campoestado,campocidade,celular,campocredito,telefonecobranca,');
                InsertSQL.add(' enderecocobranca,CampoNumeroCasacobranca,bairrocobranca,campocepcobranca,campoestadocobranca,');
                InsertSQL.add(' campocidadecobranca,celularcobranca)');
                InsertSQL.add('values (:codigo,:nome,:tabela,:objeto,:instrucaoSQL,:camponome,:nomeformulario,');
                InsertSQL.add(':campocontabil,:camporazaosocial,:CAMPOCPFCNPJ,:CAMPORGIE,:campocontagerencial,');
                InsertSQL.add(':telefone,:endereco,:CampoNumeroCasa,:bairro,:campocep,:campoestado,:campocidade,');
                InsertSQL.add(':celular,:campocredito,:telefonecobranca,:enderecocobranca,:CampoNumeroCasacobranca,:bairrocobranca,');
                InsertSQL.add(' :campocepcobranca,:campoestadocobranca,:campocidadecobranca,:celularcobranca)');

                ModifySQL.clear;
                ModifySQL.add('Update tabcredordevedor set codigo=:codigo,nome=:nome,tabela=:tabela,');
                ModifySQL.add('objeto=:objeto,instrucaoSQL=:instrucaoSQL,camponome=:camponome,');
                ModifySQL.add('nomeformulario=:nomeformulario,campocontabil=:campocontabil,camporazaosocial=:camporazaosocial,');
                ModifySQL.add('CAMPOCPFCNPJ=:CAMPOCPFCNPJ,CAMPORGIE=:CAMPORGIE,campocontagerencial=:campocontagerencial,');
                ModifySql.add('telefone=:telefone,endereco=:endereco,CampoNumeroCasa=:CampoNumeroCasa,bairro=:bairro,');
                ModifySQL.add('campocep=:campocep,campoestado=:campoestado,campocidade=:campocidade,celular=:celular,campocredito=:campocredito,');
                ModifySQL.add('telefonecobranca=:telefonecobranca,enderecocobranca=:enderecocobranca,CampoNumeroCasacobranca=:CampoNumeroCasacobranca,');
                ModifySQL.add('bairrocobranca=:bairrocobranca,campocepcobranca=:campocepcobranca,campoestadocobranca=:campoestadocobranca,campocidadecobranca=:campocidadecobranca,celularcobranca=:celularcobranca');
                ModifySQL.add(' where codigo=:codigo');
                DeleteSQL.clear;
                DeleteSQL.add('Delete  from tabcredordevedor where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,nome,tabela,objeto,instrucaoSQL,');
                RefreshSQL.add('camponome,nomeformulario,campocontabil,camporazaosocial,CAMPOCPFCNPJ,CAMPORGIE,');
                RefreshSQL.add('campocontagerencial,telefone,endereco,CampoNumeroCasa,bairro,campocep,campoestado,');
                RefreshSQL.add('campocidade,celular,campocredito,telefonecobranca,enderecocobranca,CampoNumeroCasacobranca,');
                RefreshSQL.add('bairrocobranca,campocepcobranca,campoestadocobranca,campocidadecobranca,celularcobranca');
                RefreshSQL.add('from Tabcredordevedor where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;

//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjCredorDevedor.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjCredorDevedor.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjCredorDevedor.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCredorDevedor.Get_Pesquisa: string;
begin
     Result:=' Select * from TabCredorDevedor ';
end;

function TObjCredorDevedor.Get_TituloPesquisa: string;
begin
     Result:='Pesquisa de Rela��o de Credores/Devedores';
end;

function TObjCredorDevedor.Get_InstrucaoSQL: string;
begin
     Result:=Self.InstrucaoSQL;
end;

function TObjCredorDevedor.Get_Nome: string;
begin
     Result:=Self.Nome;
end;

function TObjCredorDevedor.Get_Objeto: string;
begin
     Result:=Self.Objeto;
end;

function TObjCredorDevedor.Get_Tabela: string;
begin
     Result:=Self.Tabela;
end;

procedure TObjCredorDevedor.Submit_InstrucaoSQL(parametro: string);
begin
     Self.InstrucaoSql:=parametro;
end;

procedure TObjCredorDevedor.Submit_Nome(parametro: string);
begin
     Self.Nome:=parametro;
end;

procedure TObjCredorDevedor.Submit_Objeto(parametro: string);
begin
     Self.Objeto:=parametro;
end;

procedure TObjCredorDevedor.Submit_Tabela(parametro: string);
begin
     Self.tabela:=parametro;
end;

procedure TObjCredorDevedor.Get_listaCredorDevedor(parametro: Tstrings);
begin

end;

function TObjCredorDevedor.LocalizaNome(Parametro: string): Boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,tabela,objeto,instrucaoSQL,camponome,nomeformulario,campocontabil,');
           SelectSql.add('camporazaosocial,CAMPOCPFCNPJ,CAMPORGIE,campocep,campoestado,campocidade,campocepcobranca,');
           SelectSql.add('campoestadocobranca,campocidadecobranca from TabCredorDevedor where nome='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjCredorDevedor.LocalizaCredorDevedor(Parametro: string): boolean;
begin
     if (parametro='')
     Then Begin
              result:=False;
              exit;
     End;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add(' Select *  from '+Self.tabela+' where codigo='+parametro);
          open;
         // InputBox('','',SelectSQL.Text);
          If (RecordCount>0)
          Then result:=true
          Else result:=false;
     End;
end;

procedure TObjCredorDevedor.Get_listaCodigoCredorDevedor(
  parametro: Tstrings);
begin
     With Self.Objdataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select codigo from Tabcredordevedor');
          open;
          first;
          parametro.clear;
          While Not(eof) do
          Begin
               parametro.add(fieldbyname('codigo').asstring);
               next;
          End;
    End;


end;

procedure TObjCredorDevedor.Get_listaNomeCredorDevedor(
  parametro: Tstrings);
begin
       With Self.Objdataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select nome from Tabcredordevedor');
          open;
          first;
          parametro.clear;
          While Not(eof) do
          Begin
               parametro.add(fieldbyname('Nome').asstring);
               next;
          End;
    End;

end;
//Se Retornar 0 n�o encontrou
//caso contrario retorna o codigo
function TObjCredorDevedor.LocalizaTabela(Parametro: string): Integer;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo from Tabcredordevedor where upper(Tabela)='+#39+uppercase(parametro)+#39);
           Open;
           If (recordcount>0)
           Then Result:=Fieldbyname('codigo').asinteger
           Else Result:=0;
       End;
end;

function TObjCredorDevedor.Get_CampoNome: string;
begin
     Result:=Self.camponome;
end;

procedure TObjCredorDevedor.Submit_CampoNome(parametro: string);
begin
     Self.CampoNome:=parametro;
end;

function TObjCredorDevedor.Get_NomeCredorDevedor(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';

     if (PcredorDevedor='') or (PcodigoCredorDevedor='')
     Then exit;

     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_camponome='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.CampoNome+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;


          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.campoNome).asstring;
     End;
end;

function TObjCredorDevedor.Get_CidadeCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CampoCidade='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.CampoCidade+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.campoCidade).asstring;
     End;
end;


function TObjCredorDevedor.Get_CampoContabilCredorDevedor(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CampoContabil='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_CampoContabil+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_campoContabil).asstring;
     End;
end;
function TObjCredorDevedor.Get_CepCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_campocep='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_CampoCep+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_campoCep).asstring;
     End;
end;

function TObjCredorDevedor.Get_EstadoCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CampoEstado='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_CampoEstado+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';

                  exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_campoEstado).asstring;
     End;
end;


function TObjCredorDevedor.Get_EnderecoCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_Endereco='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_Endereco+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.get_endereco).asstring;
     End;
end;

function TObjCredorDevedor.Get_BairroCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_bairro='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_Bairro+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.get_bairro).asstring;
     End;
end;
function TObjCredorDevedor.Get_CelularCredorDevedor(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_celular='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_Celular+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;
          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_Celular).asstring;
     End;

end;

function TObjCredorDevedor.Get_telefoneCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_telefone='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_Telefone+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;
          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.get_telefone).asstring;
     End;
end;


function TObjCredorDevedor.Get_RGIECredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CAMPORGIE='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.get_CAMPORGIE+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
          open;
          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.get_CAMPORGIE).asstring;
          Except
                result:='';
          End;
     End;
end;

function TObjCredorDevedor.Get_CPFCNPJCredorDevedor(PcredorDevedor,PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CAMPOCPFCNPJ='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.get_CAMPOCPFCNPJ+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
          open;
          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.get_CAMPOCPFCNPJ).asstring;
          Except
                result:='';
          End;
     End;
end;


function TObjCredorDevedor.Get_RazaoSocialCredorDevedor(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CampoRazaoSocial='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.get_camporazaosocial+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
          open;
          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.get_camporazaosocial).asstring;
          Except
                result:='';
          End;
     End;
end;


destructor TObjCredorDevedor.Free;
begin
    Freeandnil(Self.ObjDataset);
    Freeandnil(Self.ParametroPesquisa);
end;

function TObjCredorDevedor.Get_NomeFormulario: string;
begin
     Result:=Self.NomeFormulario;
end;

procedure TObjCredorDevedor.Submit_NomeFormulario(parametro: string);
begin
     Self.NomeFormulario:=Parametro;
end;


function TObjCredorDevedor.Get_CampoContabil: string;
begin
     Result:=self.CampoContabil;
end;

procedure TObjCredorDevedor.Submit_CampoContabil(Parametro: string);
begin
     self.CampoContabil:=Parametro;
end;

function TObjCredorDevedor.Get_CampoRazaoSocial: string;
begin
     Result:=Self.CampoRazaoSocial;
end;

procedure TObjCredorDevedor.Submit_CampoRazaoSocial(parametro: string);
begin
     Self.CampoRazaoSocial:=parametro;
end;



function TObjCredorDevedor.Get_CAMPOCPFCNPJ: string;
begin
     Result:=Self.CAMPOCPFCNPJ;
end;

function TObjCredorDevedor.Get_CAMPORGIE: string;
begin
     Result:=Self.CAMPORGIE;
end;

procedure TObjCredorDevedor.Submit_CAMPOCPFCNPJ(Parametro: string);
begin
     Self.CAMPOCPFCNPJ:=Parametro;
end;

procedure TObjCredorDevedor.Submit_CAMPORGIE(Parametro: string);
begin
     Self.CAMPORGIE:=Parametro;
end;

function TObjCredorDevedor.Get_CampoContaGerencial: string;
begin
     Result:=Self.CampoContaGerencial;
end;

procedure TObjCredorDevedor.Submit_CampoContaGerencial(parametro: string);
begin
     Self.CampoContaGerencial:=Parametro;
end;

Function TObjCredorDevedor.Get_ContaGerencial(PCodigoCredorDevedor,
  PCampoPrimario: string): string;
begin

     Result:='';

     If (PCodigoCredorDevedor='')
     Then exit;

     If (Self.LocalizaCodigo(PCodigoCredorDevedor)=False)
     Then exit;

     Self.TabelaparaObjeto;

     if (Self.get_campocontagerencial='')
     Then exit;

     with Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add(Self.Get_InstrucaoSQL+' where codigo='+PCampoPrimario);
          Try
                open;
                Result:=fieldbyname(Self.get_campocontagerencial).asstring;
          Except
                exit;
          End;
     End;

end;

function TObjCredorDevedor.Get_Bairro: string;
begin
     result:=Self.Bairro;
end;

function TObjCredorDevedor.Get_Endereco: string;
begin
     Result:=Self.Endereco;
end;

function TObjCredorDevedor.Get_Telefone: string;
begin
     Result:=Self.Telefone;
end;

procedure TObjCredorDevedor.Submit_Bairro(parametro: string);
begin
     Self.Bairro:=parametro;
end;

procedure TObjCredorDevedor.Submit_Endereco(parametro: string);
begin
     Self.endereco:=parametro;
end;

procedure TObjCredorDevedor.Submit_Telefone(parametro: string);
begin
     Self.Telefone:=parametro;
end;


function TObjCredorDevedor.Get_CampoCep: string;
begin
     Result:=Self.CampoCep;
end;

function TObjCredorDevedor.Get_CampoEstado: string;
begin
     Result:=Self.CampoEstado;
end;

procedure TObjCredorDevedor.Submit_CampoCep(parametro: string);
begin
     Self.CampoCep:=Parametro;
end;

procedure TObjCredorDevedor.Submit_CampoEstado(parametro: string);
begin
     Self.CampoEstado:=Parametro;
end;

function TObjCredorDevedor.Get_CampoCidade: string;
begin
     Result:=Self.CampoCidade;
end;

procedure TObjCredorDevedor.Submit_CampoCidade(parametro: string);
begin
     Self.CampoCidade:=Parametro;
end;

function TObjCredorDevedor.Get_Celular: string;
begin
     Result:=Self.Celular;
end;

procedure TObjCredorDevedor.Submit_Celular(parametro: string);
begin
     Self.Celular:=parametro;
end;

function TObjCREDORDEVEDOR.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_CREDORDEVEDOR';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
           result:='0';
           exit;

        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


function TObjCredorDevedor.Get_CampoNumeroCasa: string;
begin
     result:=Self.CampoNumeroCasa;
end;


function TObjCredorDevedor.Get_CreditoCredorDevedor(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';

     if ((PCredorDevedor='') or (PcodigoCredorDevedor=''))
     then exit;


     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;


     Self.TabelaparaObjeto;

     if (Self.CampoCredito='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select SUM(valor)  as Valor  From  TabLancamentoCredito  where cliente ='+PcodigoCredorDevedor);
          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname('Valor').asstring;
     End;
end;


function TObjCredorDevedor.Get_NumeroCasaCredorDevedor(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';

     if ((PCredorDevedor='') or (PcodigoCredorDevedor=''))
     then exit;


     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;


     Self.TabelaparaObjeto;

     if (Self.CampoNumeroCasa='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.CampoNumeroCasa+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.CampoNumeroCasa).asstring;
     End;

end;



function TObjCredorDevedor.Get_BairroCredorDevedorCobranca(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_BairroCobranca='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_BairroCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_BairroCobranca).asstring;
     End;

end;

function TObjCredorDevedor.Get_CelularCredorDevedorCobranca(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CelularCobranca='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_CelularCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;
          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_CelularCobranca).asstring;
     End;


end;

function TObjCredorDevedor.Get_CidadeCredorDevedorCobranca(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CampoCidadeCobranca='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.CampoCidadeCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.CampoCidadeCobranca).asstring;
     End;

end;

function TObjCredorDevedor.Get_EnderecoCredorDevedorCobranca(
  PcredorDevedor, PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_EnderecoCobranca='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_EnderecoCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_EnderecoCobranca).asstring;
     End;

end;

function TObjCredorDevedor.Get_NumeroCasaCredorDevedorCobranca(
  PcredorDevedor, PcodigoCredorDevedor: string): string;
begin
     result:='';

     if ((PCredorDevedor='') or (PcodigoCredorDevedor=''))
     then exit;


     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;


     Self.TabelaparaObjeto;

     if (Self.CampoNumeroCasaCobranca='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.CampoNumeroCasaCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.CampoNumeroCasaCobranca).asstring;
     End;

end;

function TObjCredorDevedor.Get_TelefoneCredorDevedorCobranca(
  PcredorDevedor, PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_TelefoneCobranca='') or (Self.get_tabela='')
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_TelefoneCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);
          Try
            open;
          Except
                result:='';
                exit;
          End;
          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_TelefoneCobranca).asstring;
     End;

end;

function TObjCredorDevedor.Get_CepCredorDevedorCobranca(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CampoCepCobranca='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_CampoCepCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';
                exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_CampoCepCobranca).asstring;
     End;

end;

function TObjCredorDevedor.Get_EstadoCredorDevedorCobranca(PcredorDevedor,
  PcodigoCredorDevedor: string): string;
begin
     result:='';
     If (Self.LocalizaCodigo(PcredorDevedor)=False)
     Then exit;
     Self.TabelaparaObjeto;

     if (Self.Get_CampoEstadoCobranca='') or (Self.get_tabela='')
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select '+Self.Get_CampoEstadoCobranca+' From '+Self.Tabela+' where codigo='+PcodigoCredorDevedor);

          Try
                open;
          Except
                result:='';

                  exit;
          End;

          If (Recordcount>0)
          Then Result:=Fieldbyname(Self.Get_CampoEstadoCobranca).asstring;
     End;

end;

procedure TObjCredorDevedor.Submit_CampoNumeroCasa(parametro: string);
begin
     Self.CampoNumeroCasa:=Parametro;
end;

function TObjCredorDevedor.Get_CampoCredito: string;
begin
     Result:=Self.CampoCredito;
end;



procedure TObjCredorDevedor.Submit_CampoCredito(parametro: string);
begin
     Self.CampoCredito:=parametro;
end;

function TObjCredorDevedor.Get_BairroCobranca: string;
begin
      Result:=Self.BairroCobranca;
end;

function TObjCredorDevedor.Get_CampoCepCobranca: string;
begin
      Result:=Self.CampoCepCobranca;
end;

function TObjCredorDevedor.Get_CampoCidadeCobranca: string;
begin
      Result:=Self.CampoCidadeCobranca;
end;


function TObjCredorDevedor.Get_CampoEstadoCobranca: string;
begin
      Result:=Self.CampoEstadoCobranca;
end;

function TObjCredorDevedor.Get_CampoNumeroCasaCobranca: string;
begin
      Result:=Self.CampoNumeroCasaCobranca;
end;

function TObjCredorDevedor.Get_CelularCobranca: string;
begin
      Result:=Self.CelularCobranca;
end;

function TObjCredorDevedor.Get_EnderecoCobranca: string;
begin
      Result:=Self.EnderecoCobranca;
end;

function TObjCredorDevedor.Get_TelefoneCobranca: string;
begin
      Result:=Self.TelefoneCobranca;
end;

procedure TObjCredorDevedor.Submit_BairroCobranca(parametro: string);
begin
      Self.BairroCobranca:=parametro;
end;

procedure TObjCredorDevedor.Submit_CampoCepCobranca(parametro: string);
begin
      Self.CampoCepCobranca:=parametro;
end;

procedure TObjCredorDevedor.Submit_CampoCidadeCobranca(parametro: string);
begin
      Self.CampoCidadeCobranca:=parametro;
end;

procedure TObjCredorDevedor.Submit_CampoEstadoCobranca(parametro: string);
begin
      Self.CampoEstadoCobranca:=parametro;
end;

procedure TObjCredorDevedor.Submit_CampoNumeroCasaCobranca(
  parametro: string);
begin
      Self.CampoNumeroCasaCobranca:=parametro;
end;

procedure TObjCredorDevedor.Submit_CelularCobranca(parametro: string);
begin
      Self.CelularCobranca:=parametro;
end;

procedure TObjCredorDevedor.Submit_EnderecoCobranca(parametro: string);
begin
      Self.EnderecoCobranca:=parametro;
end;

procedure TObjCredorDevedor.Submit_TelefoneCobranca(parametro: string);
begin
      Self.TelefoneCobranca:=parametro;
end;



end.



