object FrPagamento: TFrPagamento
  Left = 0
  Top = 0
  Width = 606
  Height = 309
  Color = clBtnFace
  Ctl3D = False
  ParentColor = False
  ParentCtl3D = False
  TabOrder = 0
  object Bevel2: TBevel
    Left = 349
    Top = 70
    Width = 251
    Height = 49
    Shape = bsFrame
    Style = bsRaised
  end
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 606
    Height = 309
    Align = alClient
    Shape = bsFrame
    Style = bsRaised
  end
  object Label19: TLabel
    Left = 6
    Top = 3
    Width = 98
    Height = 14
    Caption = 'Valor em Dinheiro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 118
    Top = 3
    Width = 48
    Height = 14
    Caption = 'Portador'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbportador: TLabel
    Left = 195
    Top = 20
    Width = 216
    Height = 15
    AutoSize = False
    Caption = 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbTotalCheque: TLabel
    Left = 493
    Top = 89
    Width = 105
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Color = 15596267
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object LbTotalChequeProprio: TLabel
    Left = 493
    Top = 104
    Width = 105
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Color = 15596267
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Bevel8: TBevel
    Left = 416
    Top = 18
    Width = 184
    Height = 52
    Shape = bsFrame
    Style = bsRaised
  end
  object LbSaldo: TLabel
    Left = 419
    Top = 21
    Width = 178
    Height = 46
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label14: TLabel
    Left = 9
    Top = 287
    Width = 590
    Height = 15
    Alignment = taCenter
    AutoSize = False
    Caption = 
      '<ESPACO>  = valor, F5 = data,F6=n'#250'mero | F3 localiza o pr'#243'ximo v' +
      'alor |F7 marca/desmarca tudo | F8 C'#243'd. de Bar.'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbTotalselecionado: TLabel
    Left = 151
    Top = 105
    Width = 164
    Height = 14
    AutoSize = False
    Caption = 'Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbtotalDinheiro: TLabel
    Left = 493
    Top = 73
    Width = 105
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Color = 15596267
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label5: TLabel
    Left = 415
    Top = 4
    Width = 64
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Saldo Atual'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = False
  end
  object Label1: TLabel
    Left = 353
    Top = 73
    Width = 102
    Height = 14
    AutoSize = False
    Caption = 'Total em Dinheiro'
    Color = 15596267
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 353
    Top = 89
    Width = 112
    Height = 13
    AutoSize = False
    Caption = 'Total em Cheque 3'#186
    Color = 15596267
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object Label3: TLabel
    Left = 353
    Top = 104
    Width = 143
    Height = 13
    AutoSize = False
    Caption = 'Total em Cheque Pr'#243'prio'
    Color = 15596267
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    Transparent = True
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 120
    Width = 591
    Height = 164
    Color = clInfoBk
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object STRGChequePortador: TStringGrid
    Left = 8
    Top = 120
    Width = 591
    Height = 163
    TabOrder = 6
    Visible = False
    OnDblClick = STRGChequePortadorDblClick
    OnKeyDown = STRGChequePortadorKeyDown
    OnKeyPress = STRGChequePortadorKeyPress
  end
  object edtvalor: TEdit
    Left = 8
    Top = 17
    Width = 94
    Height = 19
    MaxLength = 9
    TabOrder = 0
    OnKeyPress = edtvalorKeyPress
  end
  object btinserir: TBitBtn
    Left = 7
    Top = 37
    Width = 133
    Height = 20
    Caption = '&Grava'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = btinserirClick
  end
  object edtportador: TEdit
    Left = 119
    Top = 17
    Width = 72
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 1
    OnDblClick = edtportadorDblClick
    OnEnter = edtportadorEnter
    OnExit = edtportadorExit
    OnKeyDown = edtportadorKeyDown
    OnKeyPress = edtportadorKeyPress
  end
  object BtLancaCheque: TBitBtn
    Left = 7
    Top = 58
    Width = 133
    Height = 20
    Caption = 'C&heque do tal'#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BtLancaChequeClick
  end
  object BtGridCheque: TBitBtn
    Left = 7
    Top = 101
    Width = 133
    Height = 18
    Caption = '&Cheques de terceiros'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
    OnClick = BtGridChequeClick
  end
  object btexcluir: TBitBtn
    Left = 7
    Top = 79
    Width = 133
    Height = 20
    Caption = '&Excluir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = btexcluirClick
  end
end
