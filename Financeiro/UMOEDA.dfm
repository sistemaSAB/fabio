object FMOEDA: TFMOEDA
  Left = 271
  Top = 121
  Width = 790
  Height = 377
  Caption = 'Cadastro de MOEDA - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 774
    Height = 339
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object ImagemFundo: TImage
        Left = 0
        Top = 50
        Width = 766
        Height = 211
        Align = alClient
        Stretch = True
      end
      object LbCODIGO: TLabel
        Left = 13
        Top = 100
        Width = 50
        Height = 13
        Caption = 'CODIGO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbNOME: TLabel
        Left = 13
        Top = 126
        Width = 36
        Height = 13
        Caption = 'NOME'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 13
        Top = 152
        Width = 101
        Height = 13
        Caption = 'MOEDA PADR'#195'O'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 13
        Top = 178
        Width = 57
        Height = 13
        Caption = 'SIMBOLO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 119
        Top = 102
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 2
      end
      object EdtNOME: TEdit
        Left = 119
        Top = 125
        Width = 292
        Height = 19
        MaxLength = 100
        TabOrder = 3
      end
      object panelbotes: TPanel
        Left = 0
        Top = 0
        Width = 766
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 0
        DesignSize = (
          766
          50)
        object lbnomeformulario: TLabel
          Left = 543
          Top = 8
          Width = 131
          Height = 38
          Alignment = taRightJustify
          Anchors = []
          Caption = 'MOEDA'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Btnovo: TBitBtn
          Left = 3
          Top = -3
          Width = 50
          Height = 52
          Caption = '&n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BtnovoClick
        end
        object btpesquisar: TBitBtn
          Left = 253
          Top = -3
          Width = 50
          Height = 52
          Caption = '&p'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnClick = btpesquisarClick
        end
        object btrelatorios: TBitBtn
          Left = 303
          Top = -3
          Width = 50
          Height = 52
          Caption = '&r'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object btalterar: TBitBtn
          Left = 53
          Top = -3
          Width = 50
          Height = 52
          Caption = '&a'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btalterarClick
        end
        object btexcluir: TBitBtn
          Left = 203
          Top = -3
          Width = 50
          Height = 52
          Caption = '&e'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = btexcluirClick
        end
        object btgravar: TBitBtn
          Left = 103
          Top = -3
          Width = 50
          Height = 52
          Caption = '&g'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btgravarClick
        end
        object btcancelar: TBitBtn
          Left = 153
          Top = -3
          Width = 50
          Height = 52
          Caption = '&c'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = btcancelarClick
        end
        object btsair: TBitBtn
          Left = 403
          Top = -3
          Width = 50
          Height = 52
          Caption = '&s'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          OnClick = btsairClick
        end
        object btopcoes: TBitBtn
          Left = 353
          Top = -3
          Width = 50
          Height = 52
          Caption = '&o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
        end
      end
      object panelrodape: TPanel
        Left = 0
        Top = 261
        Width = 766
        Height = 50
        Align = alBottom
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 1
        DesignSize = (
          766
          50)
        object ImagemRodape: TImage
          Left = 0
          Top = 0
          Width = 766
          Height = 50
          Align = alClient
          Stretch = True
        end
        object lbquantidadeformulario: TLabel
          Left = 485
          Top = 16
          Width = 269
          Height = 18
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Existem X MOEDA cadastrados'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
      end
      object ComboMoedaPadrao: TComboBox
        Left = 119
        Top = 149
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 4
        OnKeyPress = ComboMoedaPadraoKeyPress
        Items.Strings = (
          'SIM'
          'N'#195'O')
      end
      object EdtSimboloMoedaPadrao: TEdit
        Left = 119
        Top = 172
        Width = 144
        Height = 19
        MaxLength = 100
        TabOrder = 5
      end
    end
  end
end
