unit UObjValoresTransferenciaPortador;
Interface
Uses forms,windows,ibquery,Classes,Grids,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjTransferenciaPortador,Uobjchequesportador,
OleServer;

Type
   TObjValoresTransferenciaPortador=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                TransferenciaPortador   :TObjTransferenciaPortador;
                Valores                 :TObjChequesPortador;

                Constructor Create;
                Destructor Free;
                Function    Salvar(ComCommit:boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean):Boolean;
                Function    ExcluiTransferencia(PTransferencia:string;ComCommit:boolean;PExtornoBaixa:boolean):boolean;overload;
                Function    ExcluiTransferencia(PTransferencia:string;ComCommit:boolean):boolean;overload;

                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;


                Function Get_CODIGO                    :string;
                Function Get_TransferenciaPortador     :string;
                Function Get_Valores                   :string;
                Procedure Submit_CODIGO                 (parametro:string);
                Procedure Submit_TransferenciaPortador  (parametro:string);
                Procedure Submit_Valores                (parametro:string);
                Procedure get_ListaChequesTransferidos(ParametroCodigoTransferencia:string;Listagem:TStringgrid);
                Procedure Retorna_Cheques(Plancamento: string;out ListaCheques: TSTringList);
                function  Get_NovoCodigo: string;
                Function  RetornaPagamentoLancamento(Plancamento:string):boolean;
                procedure Seleciona_Pagamentos_Lancamento( Plancamento:string;out PtotalCheque:string;out PtotalDinheiro:string;out PtotalChequePortador:string);
                procedure FormularioCustodia(Pcodigo: string);
                
         Private
               ObjDataset:Tibdataset;
               ObjQueryLocal:tibquery;
               ObjQueryLocal2:tibquery;

               CODIGO                  :String[09];



                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function  ExcluiValoresTransferencia(Ptransferencia,PPortadorOrigem: string): Boolean;overload;
                function  ExcluiValoresTransferencia(Ptransferencia,PPortadorOrigem: string;PExtornobaixaCheque:boolean): Boolean;overload;



   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls, UObjLanctoPortador,
  UObjTalaodeCheques, UobjComprovanteCheque, UobjExportaContabilidade,
  UObjLancamento, UinterageExcel;


{ TTabTitulo }


Procedure  TObjValoresTransferenciaPortador.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO                     :=FieldByName('CODIGO').asstring                ;

        If (Self.TransferenciaPortador.LocalizaCodigo(FieldByName('TransferenciaPortador').asstring)=False)
        Then Begin
                  Messagedlg('C�digo de Transfer�ncia Entre Contas n�o encontrado',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.TransferenciaPortador.TabelaparaObjeto;

        If (Self.Valores.LocalizaCodigo(FieldByName('valores').asstring)=False)
        Then Begin
                  Messagedlg('C�digo de Cheque n�o encontrado',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.Valores.TabelaparaObjeto;
     End;
end;


Procedure TObjValoresTransferenciaPortador.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring                    :=        Self.CODIGO                           ;
      FieldByName('transferenciaportador').asstring     :=        Self.TransferenciaPortador.get_codigo ;
      FieldByName('valores').asstring                   :=        Self.Valores.get_codigo               ;
  End;
End;

//***********************************************************************

function TObjValoresTransferenciaPortador.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;
 If (ComCommit=true)
 Then FDataModulo.IBTransaction.CommitRetaining;
 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjValoresTransferenciaPortador.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO                     :='';
        Self.TransferenciaPortador.ZerarTabela;
        Self.Valores.ZerarTabela;
     End;
end;

Function TObjValoresTransferenciaPortador.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';


       If (TransferenciaPortador.get_codigo='')
       Then Mensagem:=mensagem+'/C�digo de Transfer�ncia entre Contas';

       If (valores.Get_CODIGO='')
       Then Mensagem:=mensagem+'/C�digo da Tabela de ChequesPortador';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjValoresTransferenciaPortador.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.TransferenciaPortador.LocalizaCodigo(Self.TransferenciaPortador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ C�digo de Transfer�ncia Entre Contas n�o Encontrado!';

     If (Self.Valores.LocalizaCodigo(Self.Valores.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ C�digo de ChequesPortador n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjValoresTransferenciaPortador.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.TransferenciaPortador.get_codigo);
     Except
           Mensagem:=mensagem+'/C�digo de Transfer�ncia ente contas';
     End;

     try
        Inteiros:=Strtoint(Self.Valores.get_codigo);
     Except
           Mensagem:=mensagem+'/C�digo de ChequesPortador';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjValoresTransferenciaPortador.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjValoresTransferenciaPortador.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjValoresTransferenciaPortador.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo, transferenciaportador,valores from TabValoresTransferenciaPortador where codigo='+parametro);
           Open;
           last;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjValoresTransferenciaPortador.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjValoresTransferenciaPortador.Exclui(Pcodigo: string;ComCommit:boolean): Boolean;
begin
     result:=False;
     Try
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                  Self.TabelaparaObjeto;
                  ObjLanctoPortadorGlobal.ZerarTabela;
                  If (ObjLanctoPortadorGlobal.LocalizaValoresTransferenciaPortador(Pcodigo)=True)
                  Then Begin
                            //AQUI NA VERDADE ELE SOFRE DUAS TRANSFERENCIAS PATRA CADA CHEQUE
                            //por isso eu chamo o exclui todos por dataset
                            If (ObjlanctoportadorGlobal.excluiTodosValoresTransferenciaPortadpr(pcodigo,false)=False)
                            Then exit;
                  End
                  Else Begin
                            //este procedimento s� tem sentido porque existem lancamentos antigos que naum obedeciam
                            //aos apdroes atuais como por exemplo o Numero do Lancamento da TabLanctoPortador
                            ObjlanctoportadorGlobal.Submit_Historico('EXCLUS�O.TRANSFER�NCIA.CHEQUE. TRANSF N� '+Self.TransferenciaPortador.Get_CODIGO);
                            ObjlanctoportadorGlobal.Submit_Data(Datetostr(date));
                            ObjlanctoportadorGlobal.Submit_TabelaGeradora('');
                            ObjlanctoportadorGlobal.Submit_ObjetoGerador('');
                            ObjlanctoportadorGlobal.Submit_CampoPrimario('');
                            ObjlanctoportadorGlobal.Submit_ValordoCampo('');
                            ObjlanctoportadorGlobal.Submit_ImprimeRel(True);
                            If (ObjlanctoportadorGlobal.DiminuiSaldo(Self.TransferenciaPortador.PortadorDestino.Get_CODIGO,Self.Valores.Get_Valor,False)=False)
                            Then Begin
                                        Messagedlg('N�o foi poss�vel lan�ar o lancto de exclus�o devido a problemas no acerto de saldo do portador !',mterror,[mbok],0);
                                        exit;
                            End;
                            //este procedimento s� tem sentido porque existem lancamentos antigos que naum obedeciam
                            //aos apdroes atuais como por exemplo o Numero do Lancamento da TabLanctoPortador
                            ObjlanctoportadorGlobal.Submit_Historico('EXCLUS�O.TRANSFER�NCIA.CHEQUE. TRANSF N� '+Self.TransferenciaPortador.Get_CODIGO);
                            ObjlanctoportadorGlobal.Submit_Data(Datetostr(date));
                            ObjlanctoportadorGlobal.Submit_TabelaGeradora('');
                            ObjlanctoportadorGlobal.Submit_ObjetoGerador('');
                            ObjlanctoportadorGlobal.Submit_CampoPrimario('');
                            ObjlanctoportadorGlobal.Submit_ValordoCampo('');
                            ObjlanctoportadorGlobal.Submit_ImprimeRel(True);
                            If (ObjlanctoportadorGlobal.AumentaSaldo(Self.TransferenciaPortador.PortadorOrigem.Get_CODIGO,Self.Valores.Get_Valor,False)=False)
                            Then Begin
                                        Messagedlg('N�o foi poss�vel lan�ar o lancto de exclus�o devido a problemas no acerto de saldo do portador !',mterror,[mbok],0);
                                        exit;
                            End;
                  End;

                  Self.ObjDataset.delete;
                  if ComCommit=true
                  Then FDataModulo.IBTransaction.CommitRetaining;
                  result:=True;
        End;
     Except
           result:=false;
     End;
end;


function TObjValoresTransferenciaPortador.ExcluiValoresTransferencia(Ptransferencia,PPortadorOrigem: string): Boolean;
begin
     //exclui um cheque que nao seja de um extorno de baixa de cheque
     result:=Self.ExcluiValoresTransferencia(Ptransferencia,PPortadorOrigem,true);
End;

function TObjValoresTransferenciaPortador.ExcluiValoresTransferencia(Ptransferencia,PPortadorOrigem: string;PExtornobaixaCheque:boolean): Boolean;
var
ChequeTalao:TObjTalaodeCheques;
objcomprovantecheque:TObjComprovanteCheque;
tmptransf:String;
objquery:TIBQuery;
begin

     //CHEQUE DE 3�

     //Quando Um Cheque de Terceiro � utilizado no pagamento de uma conta:
     //   o Cheque � Transferido para um portador de contas pagas
     //   � lancado um lancto debito portador origem e um credito para o destino

     //se este cheque sofreu uma outra transferencia, ele naum estara mais
     //no portador desconto do pagamento
     //neste caso naum posso mexer no portador dele
     //caso contrario, volto ele para o portador de origem
     //excluo os dois lancamentos no portador
     //excluo o registro na valorestransferenciaportador
     result:=False;

     Try
        ChequeTalao:=TObjTalaodeCheques.Create;
        objcomprovantecheque:=TObjComprovanteCheque.create;
     Except
           Messagedlg('Erro na tentativa de Cria��o do Objeto de Tal�o de Cheques',mterror,[mbok],0);
           exit;
     End;
Try
     With Self.ObjQueryLocal do
     Begin
          close;
          Sql.clear;
          Sql.add('Select codigo,valores from TabValoresTransferenciaPortador');
          sql.add('where TransferenciaPortador='+Ptransferencia);
          sql.add('order by codigo desc');

          open;
          If (recordcount=0)
          Then Begin
                    result:=True;
                    exit;
          end;
          first;

          While not(eof) do
          Begin
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;

               Self.ObjDataset.close;
               Self.ObjDataset.SelectSQL.clear;
               Self.ObjDataset.SelectSQL.add('Select count(codigo) as CONTA from TabValoresTransferenciaPortador');
               Self.ObjDataset.SelectSQL.add('where codigo>'+fieldbyname('codigo').asstring+' and Valores='+fieldbyname('valores').asstring);
               Self.ObjDataset.open;

               //se for uma baixa de cheque, o cheque tem q ser trabsferido para o portador origem
               //proque quando baixo ele estava no protador original depois foi transferido para o
               //contas pagas, sendo assim ele que tem voltar para o portador original
               //Ele tera a transferencia que o gerou sendo assim, verificar se ele ja foi transferido
               //nao adianta

              //AQUI OS CHEQUES RETORNADOS GANHAM O STATUS DE N�O BAIXADOS...
               try
                        Objquery:=TIBQuery.Create(nil);
                        Objquery.Database:=FDataModulo.IBDatabase;
                        ObjQuery.Close;
                        ObjQuery.SQL.Clear;
                        ObjQuery.SQL.Add('update tabchequesportador set baixado = ''N'' ' ) ;
                        ObjQuery.SQL.Add('where CODIGO ='+Get_Valores);
                        ObjQuery.ExecSQL;
                        FDataModulo.IBTransaction.CommitRetaining;

               except

               end;

               if (Self.ObjDataset.fieldbyname('conta').asinteger=0) or (PExtornoBaixaCheque=True)
               Then Begin
                         Self.Valores.Status:=dsedit;
                         Self.Valores.Portador.Submit_CODIGO(PPortadorOrigem);
                         if (Self.Valores.Salvar(False,False,False,'')=False)
                         Then Begin
                                   Messagedlg('N�o foi poss�vel Transferir o Cheque de volta para o Portador',mterror,[mbok],0);
                                   exit;
                         End;
               End
               Else Begin
                         //este cheque foi transferido depois desta transferencia
                         //neste caso naum preciso mexer no portador dele
                         if (Messagedlg('O Cheque utilizado nesta Transfer�ncia, foi transferido para outro portador, deseja mesmo assim excluir esta transfer�ncia e manter este cheque no portador atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
                         Then exit;
               End;

               If (Self.Exclui(self.codigo,False)=False)
               Then exit;

               //caso o cheque utilizado tenha sido um cheque do portador
               //� necess�rio apagar o cheque da TabChequesPortador
               //e deixar ele pronto para ser usado novamente
               //NO Caso deste cheque ter sido transferido por qualquer motivco
               //alem desta transferencia, da uma mensagem de alerta que o cheque
               //naum podera ser reaproveitado pois sofreu transferencia
               //e se possivel dar o numero dessas transferencias

               If (Self.Valores.Get_Chequedoportador='S')//cheque do portador
               Then Begin
                         //primeiro, verificando se este cheque tem mais alguma transferencia
                         Self.ObjDataset.close;
                         Self.ObjDataset.SelectSQL.clear;
                         Self.ObjDataset.SelectSQL.add('Select codigo,transferenciaportador from TabValoresTransferenciaPortador');
                         Self.ObjDataset.SelectSQL.add('where Valores='+fieldbyname('valores').asstring);
                         Self.ObjDataset.open;
                         tmptransf:='';
                         Self.ObjDataset.First;
                         While not(Self.ObjDataset.Eof) do
                         Begin
                              if (tmptransf<>'')
                              Then tmptransf:=tmptransf+'/'+Self.ObjDataset.fieldbyname('transferenciaportador').asstring
                              Else tmptransf:=Self.ObjDataset.fieldbyname('transferenciaportador').asstring;

                              Self.ObjDataset.next;
                         End;

                         if (tmptransf='')
                         Then BEgin
                                   //deixa o cheque para naum usado na TabTalaoDeCheques
                                   //e zera o campo codigochequeportador
                                   if (ChequeTalao.LocalizaChequePortador(Self.valores.get_codigo)=False)
                                   Then Begin
                                             Messagedlg('Cheque n�o localizado no Tal�o!',mterror,[mbok],0);
                                             exit;
                                   End;
                                   ChequeTalao.TabelaparaObjeto;
                                   ChequeTalao.Status:=dsEdit;
                                   ChequeTalao.Submit_Valor('0');
                                   ChequeTalao.Submit_USado('N');
                                   ChequeTalao.CodigoChequePortador.Submit_CODIGO('');
                                   ChequeTalao.Submit_HistoricoPagamento('');
                                   ChequeTalao.Lancamento.ZerarTabela;

                                   if (ChequeTalao.Salvar(False)=False)
                                   Then Begin
                                             Messagedlg('Erro na tentativa de Reaproveitar o Cheque do Portador',mterror,[mbok],0);
                                             exit;
                                   End;
                                   //exclui o comprovante deste cheque
                                   if (objcomprovantecheque.LocalizaCheque(Self.Valores.get_codigo)=True)
                                   Then Begin
                                             objcomprovantecheque.TabelaparaObjeto;
                                             if (objcomprovantecheque.exclui(objcomprovantecheque.get_codigo,False)=False)
                                             Then Begin
                                                       Messagedlg('Erro na Tentativa de Excluir o Comprovante de Cheque N�'+objcomprovantecheque.get_codigo,mterror,[mbok],0);
                                                       exit;
                                             End;

                                   End;

                                   //exclui da TabChequesPortador
                                   If (Self.Valores.Exclui(Self.valores.get_codigo,false)=False)
                                   Then Begin
                                              Messagedlg('Erro na tentativa de excluir o cheque '+Self.Valores.get_codigo+' da TabChequesPortador',mterror,[mbok],0);
                                              exit;
                                   End;
                                   
                         End
                         Else begin
                                   if (PExtornobaixaCheque=False)
                                   then Begin
                                             Messagedlg('O Cheque do portador foi transferido por algum motivo, por isso ele n�o poder� ser reaproveitado'+#13+
                                                        'Transfer�ncia(s): '+tmptransf,mtinformation,[mbok],0);
                                   End;
                         end;
               End;
               next;
          End;
          result:=True
     End;

Finally
       ChequeTalao.Free;
       objcomprovantecheque.free;
End;


end;

constructor TObjValoresTransferenciaPortador.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.TransferenciaPortador:=TObjTransferenciaPortador.Create;
        Self.Valores:=TObjChequesPortador.create;
        Self.ObjQueryLocal:=tibquery.create(nil);
        Self.ObjQueryLocal.database:=FdataModulo.Ibdatabase;
        Self.ObjDatasource:=TDataSource.create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryLocal;
        Self.ObjQueryLocal2:=tibquery.create(nil);
        Self.ObjQueryLocal2.database:=FdataModulo.Ibdatabase;
        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo, transferenciaportador,valores from TabValoresTransferenciaPortador where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert into TabValoresTransferenciaPortador (codigo,transferenciaportador,valores) values (:codigo,:transferenciaportador,:valores)');

                ModifySQL.clear;
                ModifySQL.add('Update TabValoresTransferenciaPortador set codigo=:codigo,transferenciaportador=:transferenciaportador,valores=:valores where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add(' Delete from tabvalorestransferenciaportador where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo, transferenciaportador,valores from TabValoresTransferenciaPortador where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjValoresTransferenciaPortador.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjValoresTransferenciaPortador.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
        if (Self.Codigo='0')
        Then Self.CODIGO:=Self.Get_NovoCodigo;
end;


procedure TObjValoresTransferenciaPortador.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjValoresTransferenciaPortador.Get_Pesquisa: string;
begin
     Result:=' Select * from TabValoresTransferenciaPortador ';
end;

function TObjValoresTransferenciaPortador.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Valores de Transferencia de Portador ';
end;

function TObjValoresTransferenciaPortador.Get_TransferenciaPortador: string;
begin
     Result:=Self.TransferenciaPortador.Get_CODIGO;
end;

function TObjValoresTransferenciaPortador.Get_Valores: string;
begin
     Result:=Self.Valores.Get_CODIGO;
end;

procedure TObjValoresTransferenciaPortador.Submit_TransferenciaPortador(
  parametro: string);
begin
     Self.TransferenciaPortador.Submit_CODIGO(parametro);
end;

procedure TObjValoresTransferenciaPortador.Submit_Valores(
  parametro: string);
begin
     Self.Valores.Submit_CODIGO(parametro);
end;

procedure TObjValoresTransferenciaPortador.get_ListaChequesTransferidos(
  ParametroCodigoTransferencia: string; Listagem: TStringgrid);
var
Cont,linha:Integer;
begin

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select ');
          SelectSQL.add('TabChequesportador.Banco,TabChequesportador.Conta,TabChequesportador.NumCheque as NumerodoCheque,');
          SelectSQL.add('TabChequesportador.Valor,TabChequesportador.Cliente1 as NomeProprietarioCheque,');
          SelectSQL.add('TabValoresTransferenciaPortador.Codigo as CODVALTRANSFCHEQUE,');
          SelectSQL.add('TabValoresTransferenciaPortador.TransferenciaPortador as NUMTRANSF,TabValoresTransferenciaPortador.Valores as CodigoChequeSistema from TabValoresTransferenciaPortador');
          SelectSQL.add('join TabChequesportador on TabValoresTransferenciaPortador.Valores=TabchequesPortador.codigo');
          SelectSQL.add(' where TabValoresTransferenciaPortador.transferenciaportador='+ParametroCodigoTransferencia);
          open;
          last;

          //somo um a mais nas colunas para a coluna 0 onde ficara o check
          //e um a mais nas linhas para linha 0 do T�tulo
          Listagem.ColCount:=Self.ObjDataset.FieldList.Count+1;
          Listagem.RowCount:=RecordCount+1;

          //dando nome as colunas
          for cont:=0 to FieldCount-1 do
          Begin
               Listagem.Cells[cont+1,0]:=Self.ObjDataset.Fields[cont].DisplayName;
          End;

          first;

          linha:=1;
          While not(eof) do
          Begin
               for cont:=0 to FieldCount-1 do
               Begin
                    if (Listagem.Cells[cont+1,0]='VALOR')
                    Then Listagem.Cells[cont+1,linha]:=formata_valor(Fields[cont].AsString)
                    Else Listagem.Cells[cont+1,linha]:=Fields[cont].AsString;
               End;
               inc(linha,1);
               next;
          End;
          
     End;
end;

destructor TObjValoresTransferenciaPortador.Free;
begin
    Freeandnil(Self.ObjDataset);
    Freeandnil(Self.Objdatasource);
    Self.TransferenciaPortador.free;
    Self.Valores.free;
    freeandnil(Self.ObjQueryLocal);
    freeandnil(Self.ObjQueryLocal2);
end;

Procedure TObjValoresTransferenciaPortador.Retorna_Cheques(Plancamento:string;out ListaCheques :TSTringList);
begin
     With Self.ObjDataset do
     Begin
          //Cheques
          ListaCheques.clear;
          close;
          SelectSql.Clear;
          //Retorna o codigo cheque do portador transferidos
          SelectSql.add('select tabvalorestransferenciaportador.valores from tabvalorestransferenciaportador left join tabtransferenciaportador');
          SelectSql.add('on tabvalorestransferenciaportador.transferenciaportador=tabtransferenciaportador.codigo');
          SelectSql.add('where tabtransferenciaportador.codigolancamento='+PLancamento);
          open;
          If (recordcount=0)
          Then exit;
          first;
          While not(eof) do
          Begin
               ListaCheques.add(fieldbyname('valores').asstring);
               next;
          End;
     End;
end;


function TObjValoresTransferenciaPortador.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=Self.ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_VALTRANSFPORTADOR';
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Transfer�ncia de Portadores',mterror,[mbok],0);
           result:='0';
           exit;
           
        End;
     Finally
            FreeandNil(StrTemp);
     End;
end;

function TObjValoresTransferenciaPortador.RetornaPagamentoLancamento(
  Plancamento: string): boolean;
begin
     result:=False;
     //devo encontrar todas as transferencia que ocorreram ligadas a este lan�amento de pagamento
     //em caso de dinheiro basta retornar para os portadores
     With Self.ObjQueryLocal2 do
     Begin
          //selecionando todas as transferencias deste lancamento
          close;
          SQL.clear;
          SQL.add('select codigo,portadororigem from tabtransferenciaportador');
          SQL.add('where codigolancamento='+Plancamento+' order by codigo desc');
          open;
          first;
          While not (eof) do
          Begin
               if (Self.ExcluiTransferencia(fieldbyname('codigo').asstring,False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir a Transfer�ncia N� '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                         exit;
               End;

               next;
          End;
          Result:=true;
     End;
end;

function TObjValoresTransferenciaPortador.ExcluiTransferencia(PTransferencia: string; ComCommit: boolean): boolean;
Begin
     //exclui uma transferencia que nao � um extorno de baixa de cheque
     //porque se for um extorno ele nao emite a mensagem de cheque ja foi transferido, porque ja foi transferido no pagto da conta (cheque do portado)
     result:=Self.ExcluiTransferencia(PTransferencia,ComCommit,False);
End;

function TObjValoresTransferenciaPortador.ExcluiTransferencia(PTransferencia: string; ComCommit: boolean;PExtornoBaixa:boolean): boolean;
var
Temp1,temp2:String;
begin
     Result:=False;

     Self.TransferenciaPortador.LocalizaCodigo(PTransferencia);
     Self.TransferenciaPortador.TabelaparaObjeto;

     if (Self.ExcluiValoresTransferencia(PTransferencia,Self.TransferenciaPortador.PortadorOrigem.get_codigo,PExtornoBaixa)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Excluir a transfer�ncia de N� '+PTransferencia,mterror,[mbok],0);
               if ComCommit=true
               Then FDataModulo.IBTransaction.RollbackRetaining;
               exit;
     End;

     if (Self.TransferenciaPortador.CodigoLancamento.Get_CODIGO='')//nao pode vir de um lancamento
     Then Begin
              With Self.transferenciaportador.codigolancamento do
              Begin
                   if (ObjExportaContabilidade.LocalizaGerador('OBJTRANSFERENCIAPORTADOR',Self.TransferenciaPortador.get_codigo)=True)
                   Then Begin
                             ObjExportaContabilidade.TabelaparaObjeto;
                             if (ObjExportaContabilidade.Get_Exportado='S')
                             Then Begin//ja foi exportado
                                      ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                      ObjExportaContabilidade.Status:=dsinsert;
                                      temp1:=ObjExportaContabilidade.Get_ContaDebite;
                                      temp2:=ObjExportaContabilidade.Get_ContaCredite;
                                      ObjExportaContabilidade.Submit_ContaCredite(temp1);
                                      ObjExportaContabilidade.Submit_ContaDebite(temp2);
                                      temp1:=ObjExportaContabilidade.Get_Historico;
                                      ObjExportaContabilidade.Submit_Historico('EXCLUS�O '+temp1);
                                      if (ObjExportaContabilidade.Salvar(False,False)=False)
                                      Then Begin
                                                FDataModulo.IBTransaction.RollbackRetaining;
                                                Messagedlg('Erro na tentativa de Lan�ar a contabilidade de exclus�o',mterror,[mbok],0);
                                                exit;
                                      End;
                             End
                             Else Begin
                                       If (ObjExportaContabilidade.excluiporgerador('OBJTRANSFERENCIAPORTADOR',Self.TransferenciaPortador.Get_CODIGO)=False)
                                       Then begin
                                                 If (ComCommit=True)
                                                 Then FDataModulo.IBTransaction.RollbackRetaining;
                                                 Result:=False;
                                                 exit;
                                       end;
                             end;
                   End;
              End;
     End;          

     if (Self.TransferenciaPortador.exclui(PTransferencia,False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Excluir a Transfer�ncia N� '+Self.ObjQueryLocal.fieldbyname('codigo').asstring,mterror,[mbok],0);
               if ComCommit=true
               Then FDataModulo.IBTransaction.RollbackRetaining;
               exit;
     End;


     result:=True;

     if ComCommit=true
     Then FDataModulo.IBTransaction.CommitRetaining;
end;




procedure TObjValoresTransferenciaPortador.Seleciona_Pagamentos_Lancamento( Plancamento:string;
  out PtotalCheque:string;out PtotalDinheiro:string;out PtotalChequePortador:string);
begin
     with Self.ObjQueryLocal do
     Begin
          PtotalCheque:='0';
          PtotalDinheiro:='0';
          PtotalChequePortador:='0';

          close;
          SQL.clear;
          sql.add('Select Pespecie as especie,sum(Pvalor) as SOMA');
          sql.add('from RETORNA_PAGAMENTOS_LANCAMENTO('+Plancamento+')');
          sql.add('group by pespecie');
          open;
          first;
          while not(eof) do
          Begin
               if (fieldbyname('especie').asstring='D')
               Then PtotalDinheiro:=fieldbyname('soma').asstring
               Else
                  if (fieldbyname('especie').asstring='C')
                  Then PtotalCheque:=fieldbyname('soma').asstring
                  Else
                     if (fieldbyname('especie').asstring='CP')
                     Then PtotalChequePortador:=fieldbyname('soma').asstring;
               next;
          End;

          close;
          SQL.clear;
          sql.add('Select Pespecie as Especie,Pvalor as Valor,Pportador as Portador,Pcodigo as Codigo');
          sql.add('from RETORNA_PAGAMENTOS_LANCAMENTO('+Plancamento+')');
          open;
     End;
end;

procedure TObjValoresTransferenciaPortador.FormularioCustodia(Pcodigo: string);
var
Pordem:integer;
PquantFolha:Integer;
PlinhasUsadas:integer;
NomeArquivo:String;
Pinicio:Integer;
begin
    //Abrir o Excel e preencher
    //Usado Na Gorethy
    //23 por folha
    //Comeca na 15
    Pinicio:=15;
    PquantFolha:=23;
    NomeArquivo:=ExtractFilePath(application.exename);
    
    if (NomeArquivo[length(NomeArquivo)]<>'\')
    Then nomearquivo:=Nomearquivo+'\';

    NomeArquivo:=NomeArquivo+'\ModeloContrato\FormularioCustodia.xls';


    if (Self.TransferenciaPortador.LocalizaCodigo(pCodigo)=False)
    Then Begin
              MensagemAviso('Transfer�ncia n�o encontrada');
              exit;
    End;
    Self.TransferenciaPortador.TabelaparaObjeto;

    with Self.ObjDataset do
    Begin
          close;
          SelectSQL.Clear;
          SelectSQL.add('Select TabChequesportador.* From TabValoresTransferenciaPortador');
          SelectSQL.add('join TabChequesportador on TabValoresTransferenciaPortador.Valores=TabchequesPortador.codigo');
          SelectSQL.add('where TabValoresTransferenciaPortador.Transferenciaportador='+PCodigo);
          SelectSQL.add('order by tabChequesPortador.vencimento');
          open;
          last;
          if (recordcount=0)
          Then Begin
                    Mensagemaviso('Nenhum Cheque encontrado nessa Transfer�ncia');
                    exit;
          End;
          first;
          Try
              Pordem:=2;
              PlinhasUsadas:=0;
              
              If (FInterageExcel.AbreArquivoExcel(NomeArquivo,'2')=False)
              Then Begin
                        Messagedlg('Erro na tentativa de Abrir o Arquivo '+NomeArquivo,mterror,[mbok],0);
                        exit;
              End;


              While not(eof) do
              begin
                  if (PlinhasUsadas=pquantfolha)
                  Then Begin
                            PlinhasUsadas:=0;
                            FInterageExcel.Salva(Formatdatetime('ddmmyyyy',now)+'_'+Pcodigo+'_'+inttostr(Pordem-1)+'.xls');
                            Pordem:=Pordem+1;
                            If (FInterageExcel.AbreArquivoExcel(NomeArquivo,'2')=False)
                            Then Begin
                                      Messagedlg('Erro na tentativa de Abrir o Arquivo '+NomeArquivo,mterror,[mbok],0);
                                      exit;
                            End;
                  End;

                  FInterageExcel.Escreve('B'+inttostr(plinhasusadas+Pinicio),Fieldbyname('cliente1').asstring);
                  FInterageExcel.Escreve('C'+inttostr(plinhasusadas+Pinicio),''''+Fieldbyname('cpfcliente1').asstring);
                  FInterageExcel.Escreve('D'+inttostr(plinhasusadas+Pinicio),''''+Fieldbyname('vencimento').asstring);
                  FInterageExcel.Escreve('E'+inttostr(plinhasusadas+Pinicio),''''+Fieldbyname('comp').asstring);
                  FInterageExcel.Escreve('F'+inttostr(plinhasusadas+Pinicio),''''+Fieldbyname('banco').asstring);
                  FInterageExcel.Escreve('G'+inttostr(plinhasusadas+Pinicio),''''+Fieldbyname('agencia').asstring);
                  FInterageExcel.Escreve('H'+inttostr(plinhasusadas+Pinicio),''''+Fieldbyname('conta').asstring);
                  FInterageExcel.Escreve('I'+inttostr(plinhasusadas+Pinicio),''''+Fieldbyname('numcheque').asstring);
                  FInterageExcel.Escreve('J'+inttostr(plinhasusadas+Pinicio),Fieldbyname('valor').asstring);

                  inc(plinhasusadas,1);
                  next;
              End;
              FInterageExcel.Salva(Formatdatetime('ddmmyyyy',now)+'_'+Pcodigo+'_'+inttostr(Pordem-1)+'.xls');
          finally
              FInterageExcel.Fecha;
          end;
    End;
end;


end.
