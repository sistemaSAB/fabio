unit UobjCmC7;

Interface

Uses sysutils,Dialogs,Classes;

Type

   TObjCMC7=class

          Public
                Constructor Create;
                Destructor   free;
                //*************************************************************
                Function     ValidaCmc7(PCodigo:String):boolean;
                //*************************************************************
                Function     Get_Banco:string;
                Function     Get_Agencia:string;
                Function     Get_numerocheque:string;
                Function     Get_ContaCorrente:string;
                Function     Get_CamaraCompensacao:string;
                Function     Get_NumeroCodigoBarras:String;

          Private
                Banco:String[03];
                Agencia:String[04];
                NumeroCheque:String[09];
                ContaCorrente:String[10];
                CamaraCompensacao:String[3];
                NumeroCodigoBarras:String;
                Procedure ZerarCampos;
                function Modulo10(PValor: String): Integer;
          End;

implementation

uses UessencialGlobal;


{ TobjCMC7 }

constructor TobjCMC7.Create;
begin
     Self.ZerarCampos;
end;


destructor TobjCMC7.free;
begin

end;

function TobjCMC7.ValidaCmc7(PCodigo: String): boolean;
var
temp:string;
begin
    Result:=False;
{
. Temos 34 posicoes no codigo cmc7
. Da posicao 1 a 1 temos caracter usado pelo cmc7
. Da posicao 2 a 4 temos o codigo do banco
. Da posicao 5 a 8 temos a agencia do cheque

. Da posicao 9 a 9 temos o digito verificador que � calculado pelo modulo 10 da posicao 11 a 20
. Da posicao 10 a 10 temos caracter usado pelo cmc7
. Da posicao 11 a 13 temos a c�mara de compensacao
. Da posicao 14 a 19 temos o numero do cheque
. Da posicao 20 a 20 temos um numero fixo "5"
. Da posicao 21 a 21 temos caracter usado pelo cmc7
. Da posicao 22 a 22 modulo 10 da posicao 2 a 8
. Da posicao 23 a 32 temos o numero da conta corrente
. Da posicao 33 a 33 modulo 10 da posicao 23 a 32
. Da posicao 34 a 34 temos caracter usado pelo cmc7
}
    Self.ZerarCampos;
    Temp:=Pcodigo;

    if (Length(temp)=30)
    Then Begin
              //COLOCAR OS SIMBOLOS
              temp:='<'+copy(pcodigo,1,8)+'<'+copy(pcodigo,9,10)+'>'+copy(pcodigo,19,12)+':';
    End
    else begin
              if (length(temp)<>34)
              Then begin
                        Messagedlg('O c�digo deve ter 30 d�gitos num�ricos ou 34 com os s�mbolos do CMC7',mterror,[mbok],0);
                        exit;
              End;
    End;
    //validando
    //. Da posicao 9 a 9 temos o digito verificador que � calculado pelo modulo 10 da posicao 11 a 20
    if (inttostr(Self.Modulo10(copy(temp,11,10)))<>temp[9])
    Then Begin
              Messagedlg('Erro no d�gito 9',mterror,[mbok],0);
              exit;
    End;
    //. Da posicao 22 a 22 modulo 10 da posicao 2 a 8
    if (inttostr(Self.Modulo10(copy(temp,2,7)))<>temp[22])
    Then Begin
              Messagedlg('Erro no d�gito 22',mterror,[mbok],0);
              exit;
    End;
    //. Da posicao 33 a 33 modulo 10 da posicao 23 a 32
    if (inttostr(Self.Modulo10(copy(temp,23,10)))<>temp[33])
    Then Begin
              Messagedlg('Erro no d�gito 33',mterror,[mbok],0);
              exit;
    End;
    Self.Banco:=Copy(temp,2,4);
    Self.Agencia:=copy(temp,5,4);
    Self.CamaraCompensacao:=copy(temp,11,3);
    Self.NumeroCheque:=copy(temp,14,6);
    Self.ContaCorrente:=copy(temp,23,10);
    Self.NumeroCodigoBarras:=RetornaSoNumeros(temp);
    result:=True;
end;

procedure TobjCMC7.ZerarCampos;
begin
     Self.Banco:='';
     Self.Agencia:='';
     Self.NumeroCheque:='';
end;

Function TobjCMC7.Modulo10(PValor: String) : Integer;
var
   Auxiliar : integer;
   doisdigitos:String[2];
   Contador, Peso : integer;
   Digito : integer;
begin

   //multiplico do fim pro inicio por 2 e 1 respectivamente
   //se a multiplicacao for maior q 9 somo os digitos do resultado
   //exemplo 2*9=18-> 1+ 8 = 9
   //somo todos os resultados

   //deduzo o resultado da diferenca do proximoi valor multiplo de 10
   //exemplo  se deu 25  deduzo de 30
   //30 - 25 = 5(dv)
   //se fosse 30-20=10 entao o digito � 0


   Auxiliar := 0;
   Peso := 2;
   for Contador:=length(pvalor) downto 1 do
   Begin
        if ((StrToInt(PValor[Contador]) * Peso)>9)
        Then Begin
                  //maior q nove somo os dois digitos
                  //o maximo q pode dar � 2*8=18
                  //entao o maior valor vai  ser 1+8 = "9"
                  doisdigitos:=inttostr((StrToInt(PValor[Contador]) * Peso));
                  auxiliar:=auxiliar+strtoint(doisdigitos[1])+strtoint(doisdigitos[2]);
        End
        Else Auxiliar :=Auxiliar+(StrToInt(PValor[Contador])*Peso);

        if Peso = 1
        then Peso := 2
        else Peso := 1;
   end;

   //se deu 10,20,30 .... entao o DV � 0

   if ((Auxiliar mod 10)=0)
   Then Result:=0
   Else Begin
             //procurando o proximo multiplo de 10
             for Contador:=1 to 10 do
             begin
                  if (((auxiliar+contador) mod 10)=0)
                  Then Begin
                            //encontrou entao o dv � ele - o resultado
                            result:=(auxiliar+contador)-auxiliar;
                            break;
                  End;
             End;
   End;

end;


function TObjCMC7.Get_Agencia: string;
begin
     Result:=Self.Agencia;
end;

function TObjCMC7.Get_Banco: string;
begin
     result:=Self.Banco;
end;

function TObjCMC7.Get_ContaCorrente: string;
begin
     Result:=Self.ContaCorrente;
end;

function TObjCMC7.Get_numerocheque: string;
begin
     result:=Self.NumeroCheque;
end;

function TObjCMC7.Get_CamaraCompensacao: string;
begin
     Result:=Self.CamaraCompensacao;
end;

function TObjCMC7.Get_NumeroCodigoBarras: String;
begin
     Result:=Self.NumeroCodigoBarras;
end;

end.
