unit UTalaodeCheques;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjTalaodeCheques;

type
  TFtalaodeCheques = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    BtNovo: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btopcoes: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    lbnomeportador: TLabel;
    Label13: TLabel;
    EdtCodigo: TEdit;
    edtportador: TEdit;
    edtnumero: TEdit;
    edtvalor: TEdit;
    edtvencimento: TMaskEdit;
    edtnumerofinal: TEdit;
    combousado: TComboBox;
    edtCodigoChequePortador: TEdit;
    edthistoricopagamento: TEdit;
    edtnumtalaocheque: TEdit;
    combodescontado: TComboBox;
    edtlancamento: TEdit;
    edttransferencia: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure EdtCodigoKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtnumerofinalEnter(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure edtportadorExit(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         Procedure limpalabels;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
   Ftalaodecheques:TFtalaodecheques;
   Objtalaodecheques:tobjtalaodecheques;

implementation

uses UessencialGlobal, Upesquisa, Uportador, UobjPortador,
  UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFtalaodeCheques.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjTalaodeCheques do
    Begin
         Submit_CODIGO          (edtCODIGO.text);
         Submit_Portador        (edtPortador.text);
         Submit_Numero          (edtNumero.text);
         Submit_NumeroFinal     (edtNumeroFinal.text);
         Submit_Valor           (edtValor.text);
         Submit_Vencimento      (edtVencimento.text);
         Submit_Usado           (combousado.text[1]);
         Submit_CodigoChequePortador(edtCodigoChequePortador.text);
         Submit_HistoricoPagamento(edthistoricopagamento.text);
         Submit_NumTalaoCheque(edtnumtalaocheque.text);
         Submit_Descontado(combodescontado.text[1]);
         Lancamento.Submit_CODIGO(edtlancamento.text);
         Submit_transferencia(edttransferencia.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFtalaodeCheques.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjTalaodeCheques do
     Begin
        edtCODIGO     .text:=Get_CODIGO            ;
        edtPortador   .text:=Get_Portador          ;
        edtNumero     .text:=Get_Numero            ;
        edtValor      .text:=Get_Valor             ;
        edtVencimento .text:=Get_Vencimento        ;
        edtlancamento .Text:=Lancamento.Get_CODIGO ;
        
        If (Get_usado='S')
        Then combousado.itemindex:=0
        Else combousado.itemindex:=1;

        If (Get_Descontado='S')
        Then combodescontado.itemindex:=1
        Else combodescontado.itemindex:=0;

        edtCodigoChequePortador.text:=Get_CodigoChequePortador;
        edthistoricopagamento.text:=Get_HistoricoPagamento;
        edtnumtalaocheque.text:=Get_NumTalaoCheque;
        edttransferencia.text:=Get_transferencia;
        
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFtalaodeCheques.TabelaParaControles: Boolean;
begin
     ObjTalaodeCheques.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFtalaodeCheques.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjTalaodeCheques=Nil)
     Then exit;

    If (ObjTalaodeCheques.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjTalaodeCheques.free;

end;

procedure TFtalaodeCheques.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFtalaodeCheques.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFtalaodeCheques.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     limpalabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjTalaodeCheques.Get_NovoCodigo;
     edtcodigo.enabled:=False;

     edtnumtalaocheque.text:=ObjTalaodeCheques.Get_NovoCodigoTalao;
     edtnumtalaocheque.enabled:=False;


     combousado.enabled:=False;
     combodescontado.enabled:=False;
     edtCodigoChequePortador.enabled:=False;
     edtlancamento.enabled:=False;
     edtvalor.enabled:=False;
     edtvencimento.enabled:=False;
     edttransferencia.enabled:=False;
     
     edtvalor.text:='0';
     edtvencimento.text:=datetostr(now);
     combousado.itemindex:=1;
     combodescontado.itemindex:=0;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;

     ObjTalaodeCheques.status:=dsInsert;
     //Sumindo com os campos que nao sera usados
      edtvalor                  .Visible:=False;
      edtvencimento             .Visible:=False;
      combousado                .Visible:=False;
      edtCodigoChequePortador   .Visible:=False;
      edthistoricopagamento.Visible     :=False;
      //****************************************
     Edtportador.setfocus;
end;

procedure TFtalaodeCheques.BtCancelarClick(Sender: TObject);
begin
     ObjTalaodeCheques.cancelar;

      //Voltando com os campos que nao foram usados
      edtvalor                  .Visible:=True;
      edtvencimento             .Visible:=True;
      combousado                .Visible:=True;
      edtCodigoChequePortador   .Visible:=True;
      edthistoricopagamento.Visible     :=True;
      //****************************************

     limpaedit(Self);
     limpalabels;
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFtalaodeCheques.BtgravarClick(Sender: TObject);
begin

     If ObjTalaodeCheques.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjTalaodeCheques.salvar(True)=False)
     Then exit;

     //Voltando com os campos que nao foram usados
      edtvalor                  .Visible:=True;
      edtvencimento             .Visible:=True;
      combousado                .Visible:=True;
      edthistoricopagamento.Visible     :=True;
      edtCodigoChequePortador   .Visible:=True;
      //****************************************

     mostra_botoes(Self);
     limpaedit(Self);
     limpalabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFtalaodeCheques.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.visible:=True;
     BtCancelar.Visible:=True;
     EdtCodigo.enabled:=False;
     ObjTalaodeCheques.Status:=dsEdit;
     edtportador.setfocus;
     edtCodigoChequePortador.Enabled:=False;
end;

procedure TFtalaodeCheques.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjTalaodeCheques.Get_pesquisa,ObjTalaodeCheques.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjTalaodeCheques.status<>dsinactive
                                  then exit;

                                  If (ObjTalaodeCheques.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFtalaodeCheques.btalterarClick(Sender: TObject);
begin
    Messagedlg('N�o � permitido alterar cheques!',mtinformation,[mbok],0);
    exit;
    {If (ObjTalaodeCheques.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;}
end;

procedure TFtalaodeCheques.btexcluirClick(Sender: TObject);
begin
     If (ObjTalaodeCheques.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjTalaodeCheques.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     

     If (ObjTalaodeCheques.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     limpalabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFtalaodeCheques.EdtCodigoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(Key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFtalaodeCheques.edtvalorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in ['0'..'9',#8,',','.'])
     Then key:=#0
     else If key='.'
          Then key:=',';
         

end;

procedure TFtalaodeCheques.edtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador:=TFportador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjTalaodeCheques.Portador.Get_PesquisaPermiteEmissao,ObjTalaodeCheques.Get_TituloPesquisaPortador,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;



end;

procedure TFtalaodeCheques.edtnumerofinalEnter(Sender: TObject);
begin
     edtnumerofinal.text:=edtnumero.text;
end;

procedure TFtalaodeCheques.btrelatoriosClick(Sender: TObject);
begin
     ObjTalaodeCheques.Imprime(edtcodigo.text);
end;

procedure TFtalaodeCheques.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

          limpaedit(Self);
     limpalabels;
     desabilita_campos(Self);

     Try
        ObjTalaodeCheques:=TObjTalaodeCheques.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

procedure TFtalaodeCheques.btopcoesClick(Sender: TObject);
begin
     Objtalaodecheques.opcoes(edtcodigo.text);
     if (edtcodigo.text='')
     Then exit;

     Objtalaodecheques.LocalizaCodigo(EdtCodigo.text);
     Objtalaodecheques.TabelaparaObjeto;
     Self.ObjetoParaControles;
end;

procedure TFtalaodeCheques.edtportadorExit(Sender: TObject);
var
pportador:string;
begin
     lbnomeportador.caption:='';

     pportador:=edtportador.Text;

     edtportador.Text:='';

     if (pportador='')
     Then exit;

     if (Objtalaodecheques.Portador.LocalizaCodigo(pportador)=False)
     then exit;

     Objtalaodecheques.Portador.TabelaparaObjeto;
     edtportador.Text:=pportador;
     lbnomeportador.caption:=Objtalaodecheques.Portador.Get_Nome;
end;

procedure TFtalaodeCheques.limpalabels;
begin
     lbnomeportador.caption:='';
end;

function TFtalaodeCheques.atualizaQuantidade: string;
begin
     result:='Existem '+ContaRegistros('tabtalaodecheques','codigo')+' Cheques Cadastrados';
end;

end.
