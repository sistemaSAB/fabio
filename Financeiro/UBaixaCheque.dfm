object FbaixaCheque: TFbaixaCheque
  Left = 1071
  Top = 247
  Width = 348
  Height = 141
  Caption = 'CHEQUES DESCONTADOS - EXCLAIM TECNOLOGIA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 77
    Width = 104
    Height = 13
    Caption = 'Cheque a ser Baixado'
  end
  object Label2: TLabel
    Left = 8
    Top = 46
    Width = 74
    Height = 13
    Caption = 'Data do Extrato'
  end
  object Label3: TLabel
    Left = 8
    Top = 11
    Width = 40
    Height = 13
    Caption = 'Portador'
  end
  object Lbportador: TLabel
    Left = 180
    Top = 11
    Width = 60
    Height = 15
    Caption = 'Lbportador'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Btbaixa: TBitBtn
    Left = 200
    Top = 41
    Width = 121
    Height = 61
    Caption = '&Baixar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clActiveCaption
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BtbaixaClick
  end
  object edtcheque: TEdit
    Left = 120
    Top = 73
    Width = 73
    Height = 21
    Color = clSkyBlue
    TabOrder = 2
    OnKeyDown = edtchequeKeyDown
    OnKeyPress = edtchequeKeyPress
  end
  object edtdata: TMaskEdit
    Left = 120
    Top = 43
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object edtportador: TEdit
    Left = 120
    Top = 8
    Width = 57
    Height = 21
    Color = clSkyBlue
    TabOrder = 0
    OnExit = edtportadorExit
    OnKeyDown = edtportadorKeyDown
    OnKeyPress = edtportadorKeyPress
  end
end
