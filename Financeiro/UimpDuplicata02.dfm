object FImpDuplicata02: TFImpDuplicata02
  Left = 99
  Top = 117
  Width = 705
  Height = 534
  VertScrollBar.Position = 27
  Caption = 'FImpDuplicata02'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object QR: TQuickRep
    Left = 8
    Top = -3
    Width = 595
    Height = 842
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 75
    object ValorFatura: TQRLabel
      Left = 20
      Top = 150
      Width = 51
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        70.555555555555560000
        529.166666666666800000
        179.916666666666700000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'ValorFatura'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object NumeroFatura: TQRLabel
      Left = 80
      Top = 150
      Width = 62
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        282.222222222222200000
        529.166666666666800000
        218.722222222222300000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'NumeroFatura'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object ValorDuplicata: TQRLabel
      Left = 150
      Top = 150
      Width = 64
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        529.166666666666800000
        529.166666666666800000
        225.777777777777800000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'ValorDuplicata'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object numeroduplicata: TQRLabel
      Left = 230
      Top = 150
      Width = 72
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        811.388888888889100000
        529.166666666666800000
        254.000000000000000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'numeroduplicata'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object vencimento: TQRLabel
      Left = 310
      Top = 150
      Width = 50
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        1093.611111111111000000
        529.166666666666800000
        176.388888888888900000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'vencimento'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object emissao: TQRLabel
      Left = 370
      Top = 150
      Width = 38
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        1305.277777777778000000
        529.166666666666800000
        134.055555555555600000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'emissao'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object nome: TQRLabel
      Left = 60
      Top = 300
      Width = 25
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        211.666666666666700000
        1058.333333333333000000
        88.194444444444450000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'nome'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object endereco: TQRLabel
      Left = 60
      Top = 315
      Width = 41
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        211.666666666666700000
        1111.250000000000000000
        144.638888888888900000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'endereco'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object estado: TQRLabel
      Left = 250
      Top = 315
      Width = 30
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        881.944444444444400000
        1111.250000000000000000
        105.833333333333300000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'estado'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object cidade: TQRLabel
      Left = 60
      Top = 330
      Width = 29
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        211.666666666666700000
        1164.166666666667000000
        102.305555555555600000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'cidade'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object cnpj: TQRLabel
      Left = 60
      Top = 345
      Width = 19
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        211.666666666666700000
        1217.083333333333000000
        67.027777777777790000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'cnpj'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object ie: TQRLabel
      Left = 280
      Top = 345
      Width = 8
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        987.777777777777800000
        1217.083333333333000000
        28.222222222222220000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'ie'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object valorextenso1: TQRLabel
      Left = 60
      Top = 360
      Width = 60
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        211.666666666666700000
        1270.000000000000000000
        211.666666666666700000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'valorextenso1'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object valorextenso2: TQRLabel
      Left = 60
      Top = 375
      Width = 60
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        211.666666666666700000
        1322.916666666667000000
        211.666666666666700000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'valorextenso2'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object observacoes1: TQRLabel
      Left = 40
      Top = 165
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        141.111111111111000000
        582.083333333333000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'observacoes1'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object observacoes2: TQRLabel
      Left = 40
      Top = 180
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        141.111111111111000000
        635.000000000000000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'observacoes2'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object observacoes3: TQRLabel
      Left = 40
      Top = 195
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        141.111111111111000000
        687.916666666667000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'observacoes3'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object observacoes4: TQRLabel
      Left = 40
      Top = 210
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        141.111111111111000000
        740.833333333333000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'observacoes4'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos1: TQRLabel
      Left = 420
      Top = 30
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        105.833333333333000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos1'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos2: TQRLabel
      Left = 420
      Top = 45
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        158.750000000000000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos2'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos3: TQRLabel
      Left = 420
      Top = 60
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        211.666666666667000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos3'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos4: TQRLabel
      Left = 420
      Top = 75
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        264.583333333333000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos4'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos5: TQRLabel
      Left = 420
      Top = 90
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        317.500000000000000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos5'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos6: TQRLabel
      Left = 420
      Top = 105
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        370.416666666667000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos6'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos7: TQRLabel
      Left = 420
      Top = 120
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        423.333333333333000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos7'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pedidos8: TQRLabel
      Left = 420
      Top = 135
      Width = 93
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        1481.666666666670000000
        476.250000000000000000
        328.083333333333000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = False
      AutoStretch = False
      Caption = 'pedidos8'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object BAIRRO: TQRLabel
      Left = 290
      Top = 300
      Width = 38
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        1023.055555555556000000
        1058.333333333333000000
        134.055555555555600000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'BAIRRO'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object FONE: TQRLabel
      Left = 380
      Top = 330
      Width = 28
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111120000
        1340.555555555556000000
        1164.166666666667000000
        98.777777777777790000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'FONE'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
  end
end
