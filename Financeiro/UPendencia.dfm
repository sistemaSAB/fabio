object Fpendencia: TFpendencia
  Left = 209
  Top = 215
  Width = 667
  Height = 456
  Caption = 'Cadastro de Pend'#234'ncias Financeiras - EXCLAIM TECNOLOGIA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 651
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    object lbnomeformulario: TLabel
      Left = 475
      Top = 0
      Width = 176
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Pend'#234'ncias de T'#237'tulos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object BtNovo: TBitBtn
      Left = 1
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object Btgravar: TBitBtn
      Left = 101
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 401
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object BtCancelar: TBitBtn
      Left = 151
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btopcoes: TBitBtn
      Left = 351
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 368
    Width = 651
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 3
    DesignSize = (
      651
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 651
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 353
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 822
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 822
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 50
    Width = 651
    Height = 143
    Align = alTop
    BevelOuter = bvNone
    Color = clSilver
    TabOrder = 0
    object ImagemFundo: TImage
      Left = 0
      Top = 0
      Width = 651
      Height = 143
      Align = alClient
      Stretch = True
    end
    object Label1: TLabel
      Left = 27
      Top = 15
      Width = 39
      Height = 14
      Caption = 'Codigo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 27
      Top = 38
      Width = 49
      Height = 14
      Caption = 'Hist'#243'rico'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 27
      Top = 60
      Width = 31
      Height = 14
      Caption = 'T'#237'tulo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 203
      Top = 60
      Width = 66
      Height = 14
      Caption = 'Vencimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 355
      Top = 60
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 27
      Top = 82
      Width = 28
      Height = 14
      Caption = 'Valor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label7: TLabel
      Left = 203
      Top = 82
      Width = 30
      Height = 14
      Caption = 'Saldo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 355
      Top = 82
      Width = 85
      Height = 14
      Caption = 'Boleto Banc'#225'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 27
      Top = 104
      Width = 65
      Height = 14
      Caption = 'Observa'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCodigo: TEdit
      Left = 121
      Top = 12
      Width = 71
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 0
    end
    object edthistorico: TEdit
      Left = 121
      Top = 35
      Width = 399
      Height = 19
      Ctl3D = False
      MaxLength = 20
      ParentCtl3D = False
      TabOrder = 1
    end
    object edttitulo: TEdit
      Left = 121
      Top = 57
      Width = 71
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 2
      OnKeyDown = edttituloKeyDown
      OnKeyPress = edttituloKeyPress
    end
    object edtportador: TEdit
      Left = 449
      Top = 57
      Width = 71
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 4
      OnKeyDown = edtportadorKeyDown
    end
    object edtvalor: TEdit
      Left = 121
      Top = 79
      Width = 71
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 5
      OnKeyPress = edtvalorKeyPress
    end
    object edtsaldo: TEdit
      Left = 273
      Top = 79
      Width = 71
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 6
    end
    object edtvencimento: TMaskEdit
      Left = 273
      Top = 57
      Width = 72
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 3
      Text = '  /  /    '
    end
    object edtboletobancario: TEdit
      Left = 449
      Top = 79
      Width = 72
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 7
      OnKeyDown = edtboletobancarioKeyDown
    end
    object edtobservacao: TEdit
      Left = 121
      Top = 101
      Width = 400
      Height = 19
      Ctl3D = False
      MaxLength = 20
      ParentCtl3D = False
      TabOrder = 8
    end
  end
  object SGLancto: TStringGrid
    Left = 0
    Top = 193
    Width = 651
    Height = 175
    Align = alClient
    FixedCols = 0
    RowCount = 2
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing]
    TabOrder = 2
    ColWidths = (
      79
      110
      84
      64
      64)
  end
end
