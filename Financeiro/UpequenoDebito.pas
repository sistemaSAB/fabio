unit UpequenoDebito;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, Mask, StdCtrls,UobjPortador;

type
  TFpequenoDebito = class(TForm)
    edthistorico: TEdit;
    Label3: TLabel;
    Label6: TLabel;
    edtvalor: TEdit;
    edtdata: TMaskEdit;
    Label4: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FpequenoDebito: TFpequenoDebito;
  Objportador:TObjPortador;
implementation

uses UessencialGlobal;



{$R *.DFM}

procedure TFpequenoDebito.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If objportador<>nil
     Then Objportador.free;
end;

procedure TFpequenoDebito.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

       Try
        Objportador:=Tobjportador.create;
        ModalResult:=mrcancel;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Portadores',mterror,[mbok],0);
           close;
     End;
     
end;

end.
