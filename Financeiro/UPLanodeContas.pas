unit UPLanodeContas;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPlanodeContas;

type
  TFplanodeContas = class(TForm)
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    panelrodape: TPanel;
    lbquantidade: TLabel;
    ImagemRodape: TImage;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    Btopcoes: TBitBtn;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    combotipo: TComboBox;
    edtclassificacao: TMaskEdit;
    ComboImprimeBalancoSS: TComboBox;
    edtcodigo2: TEdit;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure BtopcoesClick(Sender: TObject);
    procedure ApagarTudo(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Function AtualizaQuantidade:String;
    { Private declarations }
  public
    { Public declarations }
        { Public declarations }
        Function Criar:boolean;
        Procedure Destruir;

  end;

var
  FplanodeContas: TFplanodeContas;
  ObjPlanodeContas:TObjPlanodeContas;

implementation

uses UessencialGlobal, Upesquisa, UopcaoRel, UDataModulo,
  UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFplanodeContas.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjPlanodeContas do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_CODIGO2          ( edtCODIGO2.text);
         Submit_Tipo            ( combotipo.text[1]);
         Submit_Classificacao   ( edtclassificacao.text);
         Submit_Nome            ( edtnome.text);
         Submit_ImprimeBalancoSS(Submit_ComboBox(ComboImprimeBalancoSS));
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFplanodeContas.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjPlanodeContas do
     Begin
        edtCODIGO.text                  :=Get_CODIGO;
        edtCODIGO2.text                  :=Get_CODIGO2;
        
        If (Get_tipo='T')
        Then Combotipo.itemindex        :=0
        Else Combotipo.itemindex        :=1;
        edtclassificacao.text           :=Get_Classificacao;
        edtnome.text                    :=Get_Nome;



        if (Get_ImprimeBalancoSS='S')
        Then ComboImprimeBalancoSS.itemindex:=1
        Else ComboImprimeBalancoSS.itemindex:=0;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFplanodeContas.TabelaParaControles: Boolean;
begin
     If (ObjPlanodeContas.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
//****************************************




procedure TFplanodeContas.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjPlanodeContas=Nil)
     Then exit;

    If (ObjPlanodeContas.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjPlanodeContas.free;
    self.Tag:=0;
end;

procedure TFplanodeContas.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
          Perform(Wm_NextDlgCtl,0,0);
          //key:=#0;
      end;
end;



procedure TFplanodeContas.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='';
     ComboImprimeBalancoSS.ItemIndex:=0;

     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjPlanodeContas.status:=dsInsert;
     edtcodigo.setfocus;

end;


procedure TFplanodeContas.btalterarClick(Sender: TObject);
begin
    If (ObjPlanodeContas.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjPlanodeContas.Status:=dsEdit;
                edtcodigo2.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
          End;


end;

procedure TFplanodeContas.btgravarClick(Sender: TObject);
begin

     If ObjPlanodeContas.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPlanodeContas.salvar(True)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFplanodeContas.btexcluirClick(Sender: TObject);
begin
     If (ObjPlanodeContas.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjPlanodeContas.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     

     If (ObjPlanodeContas.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFplanodeContas.btcancelarClick(Sender: TObject);
begin
     ObjPlanodeContas.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFplanodeContas.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFplanodeContas.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPlanodeContas.Get_pesquisa,ObjPlanodeContas.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPlanodeContas.status<>dsinactive
                                  then exit;

                                  If (ObjPlanodeContas.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

function TFplanodeContas.Criar: boolean;
begin


end;

procedure TFplanodeContas.Destruir;
begin

end;

procedure TFplanodeContas.BtopcoesClick(Sender: TObject);
begin

    With FOpcaorel do
    Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Importa Plano de Contas');
          RgOpcoes.Items.add('Exporta Plano de Contas');
          RgOpcoes.Items.add('Apagar Todos os Plano de Contas');
          showmodal;
          if(tag=0)
          Then exit;
     End;


     If (ObjPlanodeContas.Status<>dsinactive)
     Then exit;

     case Fopcaorel.RgOpcoes.ItemIndex of

       0:Begin
           If (OpenDialog.Execute)
           Then Begin
                            If (ObjPlanodeContas.ImportaDados(OpenDialog.filename)=True)
                            Then Messagedlg('Importa��o Conclu�da com Sucesso!',mtinformation,[mbok],0)
                            Else Messagedlg('Importa��o Cancelada!',mterror,[mbok],0);
           End;
       End;
       1: Begin
               If (savedialog.Execute)
               Then Begin
                            If (ObjPlanodeContas.ExportaDados(savedialog.filename)=True)
                            Then Messagedlg('Exporta��o Conclu�da com Sucesso!',mtinformation,[mbok],0)
                            Else Messagedlg('Exporta��o Cancelada!',mterror,[mbok],0);
               End;
       End;
       2: begin
               ApagarTudo(nil);
       end;
     End;
end;

procedure TFplanodeContas.ApagarTudo(Sender: TObject);
begin
     If (messagedlg('Certeza que deseja excluir todo o plano de contas?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;
     
     If (ObjPlanodeContas.ExcluiTodos=True)
     Then Begin
                Messagedlg('Plano de Contas Exclu�do com Sucesso!',mtinformation,[mbok],0);
                ObjPlanodeContas.Commit;
                limpaedit(Self);
                lbquantidade.Caption:=AtualizaQuantidade;
          End
     Else Begin
                Messagedlg('O Plano de Contas N�o foi Exclu�do!',mterror,[mbok],0);
                ObjPlanodeContas.Rollback;
          End;
end;

procedure TFplanodeContas.btrelatoriosClick(Sender: TObject);
begin
     ObjPlanodeContas.Imprimir;
end;

procedure TFplanodeContas.FormShow(Sender: TObject);
begin

     PegaCorForm(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);

     Try
        ObjPlanodeContas:=TObjPlanodeContas.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PLANO DE CONTAS')=False)
     Then esconde_botoes(Self);

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;
     lbquantidade.Caption:=AtualizaQuantidade;

     if (ObjParametroGlobal.ValidaParametro('MASCARA DA CLASSIFICACAO NO CADASTRO DE PLANO DE CONTAS')=False)
     Then edtclassificacao.EditMask:='999.999.999.999.999.999.999;1;_'
     Else edtclassificacao.EditMask:=ObjParametroGlobal.Get_Valor;

     if(Tag<>0)
     then
     begin
          if(ObjPlanodeContas.LocalizaCodigo(IntToStr(tag))=true) then
          begin
              ObjPlanodeContas.TabelaparaObjeto;
              self.TabelaParaControles;
          end;
     end;
end;

function TFplanodeContas.AtualizaQuantidade: String;
begin
     result:='Existem '+ContaRegistros('tabplanodecontas','codigo')+' Planos de contas Cadastrados';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjPlanodeContas.Get_PesquisaXXX,ObjPlanodeContas.Get_TituloPesquisaXXX,XXX)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                 //Pegando o nome, no caso de ter uma label para isso
                                 
                                 If TEdit(Sender).text<>''
                                 Then Self.Lb.caption:=ObjPlanodeContas.Get_nomeXXX(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;
}
