unit ULancamentoFR_novo;

interface

uses
  UessencialGlobal, rdprint,Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls, ComCtrls,UObjLancamento,db,Upesquisa,UTitulo,Grids, ULancamentoFR;

type
  TFrLancamento_novo = class(TFrame)
    Guia: TPageControl;
    TabLoteCP: TTabSheet;
    TabLoteCR: TTabSheet;
    TabLoteOpcoes: TTabSheet;
    Panel2: TPanel;
    Label4: TLabel;
    Label3: TLabel;
    LbNomeCredorDevedorCP: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbvalorlanctoCP: TLabel;
    lbvalorpagamento: TLabel;
    lbsaldocp: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Shape2: TShape;
    BtSelecionaTodas_PG: TButton;
    edthistoricoCP: TEdit;
    edtcodigohistoricoCP: TEdit;
    edtvalorlancamentoCP: TMaskEdit;
    combocredordevedorCP: TComboBox;
    edtcodigocredordevedorCP: TEdit;
    edtvencimentocp: TMaskEdit;
    edtdatalancamentoCP: TMaskEdit;
    edtcontagerencial_CP: TEdit;
    edtsubcontagerencial_CP: TEdit;
    STRGCP: TStringGrid;
    Panel1: TPanel;
    Label13: TLabel;
    combocredordevedorcodigoCP: TComboBox;
    edtpesquisa_STRG_GRID_CP: TMaskEdit;
    LbtipoCampoPG: TListBox;
    Panel6: TPanel;
    Label26: TLabel;
    BtexecutaCP: TBitBtn;
    Panel4: TPanel;
    LbValorLancamentocr: TLabel;
    lbrecebe: TLabel;
    LbSaldocr: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    LbNomeCredorDevedorcr: TLabel;
    Label10: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    lbDescontoRS: TLabel;
    lbDescontoPerc: TLabel;
    lbLancarBoleto: TLabel;
    Image3: TImage;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Shape1: TShape;
    Image1: TImage;
    Image2: TImage;
    btlancaboleto: TBitBtn;
    BtSelecionaTodas_CR: TButton;
    edthistoricoCR: TEdit;
    edtcodigohistoricoCR: TEdit;
    edtvalorlancamentoCR: TMaskEdit;
    edtcodigocredordevedorcr: TEdit;
    edtvencimentocr: TMaskEdit;
    edtdatalancamentocr: TMaskEdit;
    edtcodigoboleto: TEdit;
    combocredordevedorcr: TComboBox;
    btdescontoReais: TButton;
    btDescontoPerc: TButton;
    chkSelecionaTodasCR: TCheckBox;
    strgcr: TStringGrid;
    Panel3: TPanel;
    Label6: TLabel;
    edtpesquisa_STRG_GRID_CR: TMaskEdit;
    LbtipoCampoCR: TListBox;
    combocredordevedorcodigocr: TComboBox;
    Panel5: TPanel;
    btAlterarHistoricoLancamento: TBitBtn;
    btchequecaixa: TBitBtn;
    btimprimecomprovantepagamento: TBitBtn;
    BtApagaLote: TBitBtn;
    btpesquisa: TBitBtn;
    btexecutacr: TBitBtn;
    Label29: TLabel;
    CheckSelecionaPagamento: TCheckBox;
    CheckSelecionaRecebimento: TCheckBox;
    chkExatamenteIgual: TCheckBox;
    chkContenha: TCheckBox;
    chkigualaPG: TCheckBox;
    chkContenhaPG: TCheckBox;
    btpesquisacr: TBitBtn;
    BtpesquisaCP: TBitBtn;
    Label24: TLabel;
    Label25: TLabel;
    procedure BtpesquisaCPClick(Sender: TObject);
    procedure BtexecutaCPClick(Sender: TObject);
    procedure STRGCPDblClick(Sender: TObject);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure STRGCPKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaChange(Sender: TObject);
    procedure btpesquisacrClick(Sender: TObject);
    procedure strgcrDblClick(Sender: TObject);
    procedure btexecutacrClick(Sender: TObject);
    procedure strgcrKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigohistoricosimplesKeyPress(Sender: TObject;var Key: Char);
    procedure btchequecaixaClick(Sender: TObject);
    procedure edtcodigocredordevedorCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorCPExit(Sender: TObject);
    procedure combocredordevedorCPChange(Sender: TObject);
    procedure combocredordevedorcrChange(Sender: TObject);
    procedure edtcodigocredordevedorcrExit(Sender: TObject);
    procedure edtcodigocredordevedorcrKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure BtApagaLoteClick(Sender: TObject);
    procedure edtcodigohistoricoCRKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigohistoricoCRKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigohistoricoCRExit(Sender: TObject);
    procedure edtvalorlancamentoCRExit(Sender: TObject);
    procedure edtcodigohistoricoCPExit(Sender: TObject);
    procedure edtcodigohistoricoCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtvalorlancamentoCPExit(Sender: TObject);
    procedure edtvalorlancamentoCPKeyPress(Sender: TObject; var Key: Char);
    procedure STRGCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpesquisa_STRG_GRID_CPKeyPress(Sender: TObject;
      var Key: Char);
    procedure STRGCPEnter(Sender: TObject);
    procedure strgcrEnter(Sender: TObject);
    procedure strgcrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpesquisa_STRG_GRID_CRKeyPress(Sender: TObject;
      var Key: Char);
    procedure btlancaboletoClick(Sender: TObject);
    procedure btimprimecomprovantepagamentoClick(Sender: TObject);
    procedure BtSelecionaTodas_PGClick(Sender: TObject);
    procedure BtSelecionaTodas_CRClick(Sender: TObject);
    procedure edtcodigoboletoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btdescontoReaisClick(Sender: TObject);
    procedure btDescontoPercClick(Sender: TObject);
    procedure strgcrDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btAlterarHistoricoLancamentoClick(Sender: TObject);
    procedure edtcontagerencial_CPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtsubcontagerencial_CPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure chkSelecionaTodasCPClick(Sender: TObject);
    procedure chkSelecionaTodasCRClick(Sender: TObject);
    procedure CheckSelecionaPagamentoClick(Sender: TObject);
    procedure CheckSelecionaRecebimentoClick(Sender: TObject);
    procedure chkContenhaKeyPress(Sender: TObject; var Key: Char);
    procedure chkExatamenteIgualKeyPress(Sender: TObject; var Key: Char);
    procedure chkExatamenteIgualClick(Sender: TObject);
    procedure chkContenhaClick(Sender: TObject);
    procedure chkigualaPGClick(Sender: TObject);
    procedure chkContenhaPGClick(Sender: TObject);
    procedure chkContenhaExit(Sender: TObject);
    procedure chkExatamenteIgualExit(Sender: TObject);
    procedure chkigualaPGExit(Sender: TObject);
    procedure chkContenhaPGExit(Sender: TObject);

    private
      { Private declarations }
      Procedure SalvaLotePagar;
      Procedure SalvaContasReceber;
      Procedure AtualizaLabelsCp;
      procedure AtualizaLabelsCR;
      procedure SalvaPagamento;
      procedure SalvaContasReceber_UmaPendencia(ppendencia, pvalor: string);
      procedure SalvaContasPagar_UmaPendencia(ppendencia, pvalor: string);
      procedure LancaDesconto(PTipo:string);
      Procedure ImprimeGridRecebimento;
      procedure ImprimeGridPagamento;

  public
    { Public declarations }
    ObjLancamento:TobjLancamento;
    Function Criar:boolean;
    Procedure Destruir;
    procedure LimpaLabels;
  end;


var
  Vencimentocp,VencimentocR:STring[10];

implementation

uses UFiltraImp,
  UObjGeraLancamento, 
  UObjTransferenciaPortador, UHistoricoSimples, 
  UDataModulo, UReltxtRDPRINT, UObjPendencia,
  UValorLanctoLote_rec, Uformata_String_Grid,
  UObjTitulo;
{$R *.DFM}


//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************


//****************************************
//****************************************

function TFrLancamento_novo.Criar:boolean;
begin
  limpaedit(Self);
  //desabilita_campos(Self);
  CheckSelecionaPagamento.Enabled:=true;
  CheckSelecionaRecebimento.Enabled:=true;
  //Guia.ActivePageIndex:=0;

  try
    ObjLancamento:=TObjLancamento.create;
  except
    Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
    Result:=False;
    exit;
  end;

  ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedorcp.items);
  ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigocp.items);
  ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedorcr.items);
  ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigocr.items);


  {PegaFiguraBotao(BtpesquisaCP,'BotaoPesquisar.Bmp');
  PegaFiguraBotao(BtexecutaCP,'BotaoGravar.Bmp');
  PegaFiguraBotao(BtpesquisaCR,'BotaoPesquisar.Bmp');
  PegaFiguraBotao(BtexecutaCR,'BotaoGravar.Bmp');
  PegaFiguraBotao(btchequecaixa,'botaogravar.bmp');  }
  Result:=True;
end;

procedure TFrLancamento_novo.Destruir;
begin
  if (Self.objLancamento <> nil) then
    ObjLancamento.free;
end;

procedure TFrLancamento_novo.BtpesquisaCPClick(Sender: TObject);
begin
  edtdatalancamentoCP.text:=datetostr(now);
  VencimentoCP:=EDTVENCIMENTOCP.text;

  objlancamento.Pendencia.Titulo.Get_PendenciaseTitulo('D',EDTVENCIMENTOCP.TEXT,STRGCP,combocredordevedorcodigocp.Items[combocredordevedorcp.itemindex],edtcodigocredordevedorcp.text,'',LbtipoCampoPG.Items,edtcontagerencial_CP.text,edtsubcontagerencial_CP.Text);

  if (STRGCP.ROWCOUNT<=1) then
    STRGCP.FixedRows:=0
  else STRGCP.FixedRows:=1;

  Formata_StringGrid(STRGCP,LbtipoCampoPG.Items);
  AjustaLArguraColunaGrid(STRGCP);
  CheckSelecionaPagamento.Checked:=False;
end;

procedure TFrLancamento_novo.BtexecutaCPClick(Sender: TObject);
begin
  if (STRGCP.RowCount <= 1) then
    exit;

  Self.salvaLotePagar;
  btpesquisacp.OnClick(nil);
  AtualizaLabelsCP;
end;

procedure TFrLancamento_novo.STRGCPDblClick(Sender: TObject);
var
  valor,valordogrid : Currency;
  cont : Integer;
begin
  if STRGCP.RowCount <= 1 then
    exit;

  //pegando o valor a ser pago
  limpaedit(Ffiltroimp);
  With Ffiltroimp do
  Begin
    DesativaGrupos;
    Grupo01.Enabled:=True;
    edtgrupo01.EditMask:='';
    edtgrupo01.text:=STRGCP.cells[7,STRGCP.row];
    edtgrupo01.OnKeyPress:=nil;
    LbGrupo01.caption:='VALOR';
    showmodal;
    edtgrupo01.OnKeyPress:=nil;

    if tag=0 then
      exit;

    try
      valor:=strtofloat(tira_ponto(edtgrupo01.text));
      valordogrid:=strtofloat(tira_ponto(STRGCP.cells[7,STRGCP.row]));
      STRGCP.cells[8,STRGCP.row]:=formata_valor(floattostr(valor));
    except
      STRGCP.cells[8,STRGCP.row]:='';
    end;

    for cont:=0 to STRGCP.ColCount-1 do
    begin
      STRGCP.Cells[cont,STRGCP.row]:=STRGCP.Cells[cont,STRGCP.row];
    end;

  End;
  atualizalabelscp;
  STRGCP.SetFocus;
End;


procedure TFrLancamento_novo.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
  if not(key in ['0'..'9',#8,',']) then
  begin
    if (key='.') then
      Key:=','
    else Key:=#0;
  end;
end;

procedure TFrLancamento_novo.STRGCPKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
    STRGCP.OnDblClick(sender);

  if (key=#32) then
  begin
    if (STRGCP.cells[0,1]='') then
      exit;

    chkigualaPG.Visible:=true;
    chkContenhaPG.visible:=true;
    PreparaPesquisa_StringGrid(STRGCP,edtpesquisa_STRG_GRID_CP,LbtipoCampoPG.Items);
  end;
end;

procedure TFrLancamento_novo.SalvaLotePagar;
var
  Cont : Integer;
  tvalor : Currency;
begin
  tvalor := 0;
  for cont:=1 to STRGCP.RowCount-1 do
  begin
    try
      if (trim(STRGCP.cells[8,cont]) <> '') then
      begin
        strtofloat(tira_ponto(STRGCP.cells[8,cont]));
        tvalor := tvalor+strtofloat(tira_ponto(STRGCP.cells[8,cont]));
      end;
    except
    end;
  end;

  if (Tvalor = 0) then
  begin
    Messagedlg('Nenhuma pend�ncia foi selecionada para pagamento!',mtinformation,[mbok],0);
    exit;
  end;

  try
    strtodate(edtdatalancamentocp.text);
  except
    Messagedlg('Data de Lan�amento Inv�lida!',mterror,[mbok],0);
    exit;
  end;

  Self.SalvaPagamento;
End;

procedure TFrLancamento_novo.SalvaPagamento;
var
  contapag, cont: Integer;
  ValorLanccomPai, ValorSoma: Currency;
  HistoricoTodos: String;
  ObjTransferenciaPortador: TObjTransferenciaPortador;
  PCCredorDevedor, CodigoLAncamentoPai, CodigoLancamentoFilho, TmptipoLancto, tmptipolanctoligado: string;
  NFTEMP, NumeroTransferencia: string;
  ppendencia, pvalor, PCredorDevedorLote, PcodigoCredorDevedorLote: string;
  pTitulo : string;
begin
  //Tenho Que Salvar um lancamento sem estar ligado a pendencia
  //nenhuma, apenas com o valor de todas elas
  //e um hist�rico PAGAMENTO DE PEND. DOS T�TULOS X,Y,Z....

  ValorSoma:=0;
  ContaPag:=0;
  HistoricoTodos:='PAG.LOTE - ';
  PCredorDevedorLote:='';
  PcodigoCredorDevedorLote:='';
  pTitulo := ''; //Rodolfo

  for cont:=1 to StrgCP.RowCount-1 do
  begin
    try
      if (trim(StrgCP.cells[8,cont])<>'') then
      begin
        Inc(ContaPAG,1);
        //guardando pendencia e o valor
        Ppendencia:=StrgCP.cells[3,cont];
        Pvalor:=tira_ponto(trim(StrgCP.Cells[8,cont]));

        pTitulo := StrgCP.cells[0,cont]; //guarda o titulo, para caso seja um lote - Rodolfo

        //******************************************************
        ObjLancamento.Pendencia.Titulo.LocalizaCodigo(StrgCP.cells[0,cont]);
        ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
        //******************************************************
        {Se todas as pend�ncias forem do mesmo credor devedor (cliente, fornecedor...)
        entao as variaveis credordevedorlote e codigocredordevedorlote ficaram
        com o codigo dele, assim quando mandar pra tela de recebimento, os cheques
        de terceiro poderao resgatar automaticamente o nome o cpf do cadastro}
        if (PcredorDevedorLote='') then
        begin
          PcredorDevedorLote:=ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO;
          PCodigoCredorDevedorLote:=ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
        end
        else
        begin

          if (PcredorDevedorLote<>ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO)
            or (PCodigoCredorDevedorLote<>ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR) then
          begin
            //existem pendencias de credordevedores diferentes
            PcredorDevedorLote:='-1';
            PcodigoCredorDevedorLote:='-1';
          end;
        end;
        //******************************************************
        Strtofloat(TIRA_PONTO(StrgCP.cells[8,cont]));
        ValorSoma:=ValorSoma+Strtofloat(Tira_ponto(StrgCP.cells[8,cont]));
        if (edthistoricoCP.text='') then
          HistoricoTodos:=HistoricoTodos + ' / '+StrgCP.cells[0,cont] + ' NF ' + ObjLancamento.Pendencia.Titulo.Get_NUMDCTO;
      end;
    except
    end;
  end;

  if (PCredorDevedorLote='-1') then //tem mais de um credor/devedor
  begin
    PCredorDevedorLote:='';
    PcodigoCredorDevedorLote:='';
    pTitulo := '';  //nao tenho informacoes de apenas um credor/devedor, entao nao preciso do titulo
  end;

  if (edthistoricocp.Text <> '') then
    Historicotodos:=EdthistoricoCp.text;//Historicotodos:=HistoricoTodos+edthistoricoCP.text;

  if (ValorSoma=0) then
    exit;

  if (ContaPag=1) then//somente uma pendencia
  begin
    Self.SalvaContasPagar_UmaPendencia(ppendencia,pvalor);
    exit;
  end;


  //lan�amento das pendencias filhos
  if (Objlancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO')=False) then
  begin
    Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO", n�o foi encontrado!',mterror,[mbok],0);
    exit;
  end;
  Objlancamento.ObjGeraLancamento.TabelaparaObjeto;
  TmptipoLanctoLigado:=Objlancamento.ObjGeraLancamento.Get_TipoLAncto;

  //Salvando o Lan�amento Pai
  CodigoLAncamentoPai:=ObjLancamento.Get_NovoCodigo;
  if (ObjLancamento.NovoLancamento('','P',floattostr(valorsoma),CodigoLAncamentoPai,HistoricoTodos,
    False,False,True,PCredorDevedorLote,PcodigoCredorDevedorLote,edtdatalancamentocp.text)=False) then
  begin
    FDataModulo.IBTransaction.RollbackRetaining;
    Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
    exit;
  end;

  //****************************************************************
  //salvando as pendencias sem gerar valor
  for cont:=1 to StrgCP.RowCount-1 do
  begin
    if (trim(StrgCP.Cells[8,cont])<>'') then
    begin
      try
        ObjLancamento.Pendencia.Titulo.LocalizaCodigo(StrgCP.cells[0,cont]);
        ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
        NFTEMP:=ObjLancamento.Pendencia.Titulo.Get_NUMDCTO;

        ObjLancamento.ZerarTabela;
        CodigoLancamentoFilho:=ObjLancamento.get_novocodigo;
        ObjLancamento.Submit_CODIGO(CodigoLancamentoFilho);
        ObjLancamento.Submit_Pendencia(StrgCP.cells[3,cont]);
        ObjLancamento.Submit_TipoLancto(tmptipolanctoligado);
        ObjLancamento.Submit_Valor(TIRA_PONTO(StrgCP.Cells[8,cont]));
        ObjLancamento.Submit_Historico('PAG.'+StrgCP.Cells[1,cont]+'-NF '+NFTEMP);
        ObjLancamento.Submit_Data(edtdatalancamentocp.text);
        ObjLancamento.Submit_LancamentoPai(CodigoLAncamentoPai);
        ObjLancamento.Status:=dsinsert;

        if (Objlancamento.Salvar(False,False,'',true,true)=False) then
        begin
          Messagedlg('Lan�amentos Cancelados!',mterror,[mbok],0);
          ObjLancamento.Rolback;
          ObjLancamento.Status:=dsInactive;
          exit;
        end;
        
        CodigoLancamentoFilho:=ObjLancamento.get_codigo;
        //lancamento filho
        StrgCP.cells[13,cont]:=Codigolancamentofilho;
      except
      end;
    end;
  end;
  //****************************************************************
  ObjLancamento.Commit;

  if (ObjLancamento.ExportaContabilidade_Pagamento_Lote(CodigoLAncamentoPai)=False) then
  begin
    ObjLancamento.Rolback;
    Messagedlg('Erro na tentativa de Exportar a contabilidade do Pagamento. O lan�amento ser� exclu�do',mterror,[mbok],0);
    if (Objlancamento.LocalizaCodigo(codigolancamentopai)=True) then
      Objlancamento.ExcluiLancamento(CodigoLAncamentoPai,true,false);
    exit;
  end
  else ObjLancamento.Commit;
     
  //******IMPRIMIR RECIBO DE PAGAMENTO**************************************
  if (ObjParametroGlobal.ValidaParametro('IMPRIME RECIBO DE PAGAMENTO AP�S LAN�AMENTO')=False) then
    exit;

  if (ObjParametroGlobal.Get_Valor='SIM') then
    Objlancamento.ImprimeReciboPagamento(CodigoLAncamentoPai, pTitulo);
  //*************************************************************************

  Messagedlg('Pagamento efetuado com Sucesso!',mtinformation,[mbok],0);
  edtvencimentocp.text:=VencimentoCP;
end;

procedure TFrLancamento_novo.GuiaChange(Sender: TObject);
begin
  if (Guia.ActivePage = TabLoteCP) or (Guia.ActivePage = TabLoteCR) then
  begin
    if (objlancamento.status <> dsinactive) then
    begin
      Guia.ActivePageIndex:=1;
      exit;
    end;
  end;

  if Guia.ActivePage = TabLoteCP then
  begin
    EDTVENCIMENTOcp.enabled:=True;
    edtdatalancamentoCP.enabled:=True;
    combocredordevedorcp.enabled:=True;
    edtcodigocredordevedorcp.enabled:=True;
    edtvalorlancamentoCP.enabled:=True;
    edtcodigohistoricoCP.enabled:=True;
    edthistoricoCP.enabled:=True;
    edtpesquisa_STRG_GRID_CP.enabled:=True;
    edtpesquisa_STRG_GRID_CP.Visible:=False;

    edtcontagerencial_CP.enabled:=True;
    edtsubcontagerencial_CP.enabled:=True;

    LbNomeCredorDevedorcp.caption:='';
    combocredordevedorcp.SetFocus;

    STRGCP.ColCount:=1;
    STRGCP.rowcount:=1;
    STRGCP.cols[0].clear;
    EDTVENCIMENTOcp.text:='';
    VencimentoCP:='';
    LbValorPagamento.caption:='';
    combocredordevedorcp.Text:='';
    edtcodigocredordevedorcp.Text:='';
    lbvalorlanctoCP.caption:='';
    lbsaldocp.caption:='';
    AtualizaLabelsCp;
    //combocredordevedorCP.ItemIndex:=1;
    chkigualaPG.visible:=false;
    chkContenhaPG.Visible:=false;
    edtpesquisa_STRG_GRID_CP.Visible:=false;
  End;

  if Guia.ActivePage = TabLoteCR then
  begin
    EDTVENCIMENTOcr.enabled:=True;
    edtdatalancamentoCR.enabled:=True;
    combocredordevedorcr.enabled:=True;
    edtcodigocredordevedorcr.enabled:=True;
    edtvalorlancamentoCR.enabled:=True;
    edtcodigohistoricoCR.enabled:=True;
    edthistoricoCR.enabled:=True;
    edtcodigoboleto.enabled:=True;

    edtpesquisa_STRG_GRID_CR.enabled:=True;
    edtpesquisa_STRG_GRID_CR.Visible:=False;

    LbNomeCredorDevedorcr.caption:='';
    combocredordevedorcr.SetFocus;

    STRGCR.ColCount:=1;
    STRGCR.rowcount:=1;
    STRGCR.cols[0].clear;
    EDTVENCIMENTOcR.text:='';
    VencimentoCR:='';
    lbrecebe.caption:='';
    LbValorLancamentocr.caption:='';
    LbSaldocr.caption:='';

    combocredordevedorcr.Text:='';
    edtcodigocredordevedorcr.Text:='';
    edtcodigoboleto.Text:='';
    AtualizaLabelsCR;
    //combocredordevedorcr.ItemIndex:=0;
    chkContenha.Visible:=false;
    chkExatamenteIgual.visible:=false;
    edtpesquisa_STRG_GRID_CR.Visible:=false;
  end;
end;

procedure TFrLancamento_novo.btpesquisacrClick(Sender: TObject);
begin
  VencimentoCR:=EDTVENCIMENTOCR.text;
  objlancamento.Pendencia.Titulo.Get_PendenciaseTitulo('C',EDTVENCIMENTOCR.TEXT,STRGCR,combocredordevedorcodigocr.Items[combocredordevedorcr.itemindex],edtcodigocredordevedorcr.text,edtcodigoboleto.text,LbtipoCampoCR.Items);
  edtdatalancamentoCR.text:=datetostr(now);
  
  //sdfsdfsd
  if (STRGCR.ROWCOUNT<=1) then
    STRGCR.FixedRows:=0
  else STRGCR.FixedRows:=1;

  Formata_StringGrid(strgcr,LbtipoCampoCR.Items);
  AjustaLArguraColunaGrid(STRGCR);
  Self.AtualizaLabelsCR;
  chkSelecionaTodasCR.Checked:=False;
end;

procedure TFrLancamento_novo.strgcrDblClick(Sender: TObject);
var
  Ptaxa,valor:currency;
  cont:integer;
begin
  if strgcr.RowCount <= 1 then
    exit;

  if (FvalorLanctoLote_REC = nil) then  //CRIANDO FORM *****ALEX****
    Application.CreateForm(TFvalorLanctoLote_REC,FvalorLanctoLote_REC);

  FValorLanctoLote_REC.FrValorLanctoLote.Visible:=False;

  if (ObjParametroGlobal.Validaparametro('CALCULA JUROS NO LAN�AMENTO?')=False) then
    exit;

  if (ObjParametroGlobal.Get_valor = 'SIM') then
  begin
    if (ObjParametroGlobal.Validaparametro('TAXA DE JUROS AO M�S NO LAN�AMENTO (%)')=False)
    then exit;

    try
      Ptaxa:=strtofloat(ObjParametroGlobal.Get_Valor);
      Ptaxa:=Ptaxa/30;//ao dia
      ptaxa:=strtofloat(tira_ponto(formata_valor(ptaxa)));
    except
      Ptaxa:=0;
    end;

    //saldo e vencimento
    FValorLanctoLote_REC.FrValorLanctoLote.visible:=true;
    FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbSaldoPendencia.Caption:=formata_valor(STRGCR.cells[7,STRGCR.row]);
    FValorLanctoLote_REC.FrValorLanctoLote.edtdatalancamento.text:=datetostr(now);
    FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbTaxaDia.caption:=formata_valor(ptaxa);
    FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbdiaatraso.caption:='0';
    FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbSaldoComJuros.caption:=formata_valor(STRGCR.cells[7,STRGCR.row]);
    FValorLanctoLote_REC.FrValorLanctoLote.pj_vencimento.caption:=STRGCR.cells[5,STRGCR.row];
    FValorLanctoLote_REC.FrValorLanctoLote.edtcarencia.Text:='0';
  end;

  FValorLanctoLote_REC.edtvalor.text:=trim(STRGCR.cells[7,STRGCR.row]);
  FValorLanctoLote_REC.ShowModal;

  if FValorLanctoLote_REC.Tag = 0 then
    exit;

  try
    valor:=strtofloat(tira_ponto(FvalorLanctoLote_REC.edtvalor.text));
    STRGCR.cells[8,STRGCR.row]:=formata_valor(floattostr(valor));
  except
    STRGCR.cells[8,STRGCR.row]:='';
  end;

  for cont:=0 to strgcr.ColCount-1 do
  begin
    strgcr.Cells[cont,STRGCR.row]:=strgcr.Cells[cont,STRGCR.row];
  end;

  {//pegando o valor a ser pago
  limpaedit(Ffiltroimp);
  With Ffiltroimp do
  Begin
    DesativaGrupos;
    Grupo01.Enabled:=True;
    edtgrupo01.EditMask:='';
    edtgrupo01.text:=STRGCR.cells[7,STRGCR.row];
    edtgrupo01.OnKeyPress:=nil;
    LbGrupo01.caption:='VALOR';
    showmodal;
    edtgrupo01.OnKeyPress:=nil;
    If Tag=0
    Then exit;

    Try
       valor:=strtofloat(tira_ponto(edtgrupo01.text));
       STRGCR.cells[8,STRGCR.row]:=formata_valor(floattostr(valor));
    Except
          STRGCR.cells[8,STRGCR.row]:='';
    End;
  End;}

  STRGCR.SetFocus;
  AtualizaLabelsCR;
  FreeAndNil(FvalorLanctoLote_REC);
End;

procedure TFrLancamento_novo.btexecutacrClick(Sender: TObject);
begin
  if (STRGCR.RowCount <= 1) then
    exit;

  Self.SalvaContasReceber;
  AtualizaLabelsCR;
end;

procedure TFrLancamento_novo.SalvaContasReceber;
var
  Cont : Integer;
  ContaRec : Integer;
  ValorLanccomPai,ValorSoma : Currency;
  HistoricoTodos : String;
  PortadorUSadoNoRecebimento,NFTEMP,PCCredorDevedor,CodigoLAncamentoPai,CodigoLancamentoFilho,TmptipoLancto,tmptipolanctoligado : string;
  ppendencia,PCredorDevedorLote,PCodigoCredorDevedorLote : string;
  pvalor:string;
  ptitulo: string; //Rodolfo
begin

  ContaRec:=0;

  pTitulo := ''; //Rodolfo

  for cont:=1 to STRGCR.RowCount-1 do
  begin
    if (trim(StrgCR.Cells[8,cont])<>'') then //verificando a coluna de valor a ser recebido
    begin
      Inc(ContaRec,1);
      //guardando pendencia e o valor
      Ppendencia:=StrgCR.cells[3,cont];
      Pvalor:=tira_ponto(trim(StrgCR.Cells[8,cont]));

      //guardando titulo - Rodolfo
      pTitulo:= StrgCR.cells[0,cont];
    end;
  end;

  if (ContaRec = 0) then
  begin
    Messagedlg('N�o foi definido o valor de nenhum T�tulo!',mtinformation,[mbok],0);
    exit;
  end;

  try
    strtodate(edtdatalancamentocr.text);
  except
    Messagedlg('Data Inv�lida para o lan�amento!',mterror,[mbok],0);
    exit;
  end;

  if (ContaRec = 1) then//somente uma pendencia
  begin
    Self.SalvaContasReceber_UmaPendencia(ppendencia,pvalor);
    exit;
  end;

  //Tenho Que Salvar um lancamento sem estar ligado a pendencia
  //nenhuma, apenas com o valor de todas elas
  //e um hist�rico REC. DE PEND. DOS T�TULOS X,Y,Z....
  ValorSoma:=0;

  HistoricoTodos:='LOTE - ';
  PCredorDevedorLote:='';
  PCodigoCredorDevedorLote:='';

  for cont:=1 to StrgCR.RowCount-1 do
  begin
    if (trim(StrgCR.Cells[8,cont]) <> '') then
    begin
      try
        ObjLancamento.Pendencia.Titulo.LocalizaCodigo(trim(StrgCR.cells[0,cont]));
        ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;

        //******************************************************
        {Se todas as pend�ncias forem do mesmo credor devedor (cliente, fornecedor...)
        entao as variaveis credordevedorlote e codigocredordevedorlote ficaram
        com o codigo dele, assim quando mandar pra tela de recebimento, os cheques
        de terceiro poderao resgatar automaticamente o nome o cpf do cadastro}
        if (PcredorDevedorLote='') then
        begin
          PcredorDevedorLote:=ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO;
          PCodigoCredorDevedorLote:=ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
        end
        else
        begin

          if (PcredorDevedorLote <> ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO)
            or (PCodigoCredorDevedorLote <> ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR) then
          begin
            //existem pendencias de credordevedores diferentes
            PcredorDevedorLote:='-1';
            PcodigoCredorDevedorLote:='-1';
          end;
        end;
        //******************************************************
        Strtofloat(TIRA_PONTO(trim(StrgCR.cells[8,cont])));
        ValorSoma:=ValorSoma+Strtofloat(Tira_ponto(trim(StrgCR.cells[8,cont])));

        if (edthistoricoCR.text='') then
          HistoricoTodos:=HistoricoTodos+' / '+StrgCR.cells[0,cont]+' NF '+ObjLancamento.Pendencia.Titulo.Get_NUMDCTO;
      except

      end;
    end;
  end;

  if (PCredorDevedorlote = '-1') then
  begin
    PCredorDevedorLote:= '';
    PCodigoCredorDevedorLote:= '';

    //Rodolfo
    //Se nem todos os credor/devedor sao iguais,
    //entao nao posso ter um titulo base para puxar as informacoes do credor/devedor
    ptitulo := '';
  end;

  if (edthistoricoCR.Text <> '') then
    historicotodos:= historicotodos + edthistoricoCR.Text;

  if (ValorSoma = 0) then
    exit;

  //Usado para os lancamentos filhos
  if (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO')= False) then
  begin
    Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO", n�o foi encontrado!',mterror,[mbok],0);
    exit;
  end;

  ObjLancamento.ObjGeraLancamento.TabelaparaObjeto;
  TmptipoLanctoLigado:= ObjLancamento.ObjGeraLancamento.Get_TipoLAncto;
  //*******************************
  CodigoLAncamentoPai:= ObjLancamento.Get_NovoCodigo;
  ObjLancamento.ZerarTabela;


  //***********************************************************************
  //CHAMANDO A TELA DE QUITACAO
  if (ObjLancamento.NovoLancamento('','R',floattostr(ValorSoma),codigolancamentopai,HistoricoTodos,False,False,True,PcredorDevedorLote,PCodigoCredorDevedorLote,edtdatalancamentocr.text)=False) then
  begin
    FDataModulo.IBTransaction.RollbackRetaining;
    Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
    exit;
  End;

  //****************************************************************
  //salvando as pendencias sem gerar valor
  for cont:=1 to StrgCr.RowCount-1 do
  begin
    if (Trim(StrgCR.Cells[8,cont]) <> '') then
    begin
      try
        ObjLancamento.Pendencia.Titulo.LocalizaCodigo(StrgCR.cells[0,cont]);
        ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
        NFTEMP:=ObjLancamento.Pendencia.Titulo.get_numdcto;

        ObjLancamento.ZerarTabela;
        CodigoLancamentoFilho:=ObjLancamento.get_novocodigo;
        ObjLancamento.Submit_CODIGO(CodigoLancamentoFilho);
        ObjLancamento.Submit_Pendencia(StrgCR.cells[3,cont]);
        ObjLancamento.Submit_TipoLancto(tmptipolanctoligado);
        ObjLancamento.Submit_Valor(TIRA_PONTO(trim(StrgCR.Cells[8,cont])));
        ObjLancamento.Submit_Historico('RC.LOTE-'+StrgCR.Cells[1,cont]+' - '+NFTEMP);
        ObjLancamento.Submit_Data(edtdatalancamentocr.text);
        ObjLancamento.Submit_LancamentoPai(CodigoLAncamentoPai);
        ObjLancamento.Status:=dsinsert;

        if (Objlancamento.Salvar(False,False,'',true,true)=False) then
        begin
          ObjLancamento.Rolback;
          Messagedlg('Lan�amentos Cancelados!',mterror,[mbok],0);
          ObjLancamento.Status:=dsInactive;
          exit;
        end;
        
        Codigolancamentofilho:=Objlancamento.Get_CODIGO;
        //GUARDANDO O CODIGO DO LANCAMENTO FILHO NA LINHA DO STRING GRID
        StrgCR.cells[13,cont]:=Codigolancamentofilho;
      except
      end;
    end;
  end;
  ObjLancamento.Commit;

  if (ObjLancamento.ExportaContabilidade_Recebimento_Lote(CodigoLAncamentoPai)=False) then
  begin
    ObjLancamento.Rolback;
    Messagedlg('Erro na tentativa de exportar a contabilidade do recebimento. O lan�amento ser� exclu�do',mterror,[mbok],0);
    
    if (Objlancamento.LocalizaCodigo(codigolancamentopai)=True) then
      Objlancamento.ExcluiLancamento(CodigoLAncamentoPai,true,false);
    exit;
  end
  else ObjLancamento.Commit;


  // F�bio 30/09/2009 vou colocar o recibo do reportbuilder
  // quando � feito recebimento em lote
  if (ObjParametroGlobal.ValidaParametro('IMPRIME RECIBO DE RECEBIMENTO AP�S LAN�AMENTO')=true) then
  begin
    if (ObjParametroGlobal.Get_Valor = 'SIM') then
      ObjLancamento.ImprimeRecibo(CodigoLAncamentoPai,'','', ptitulo);
  end;

  Messagedlg('Recebimento efetuado com Sucesso!',mtinformation,[mbok],0);
  edtvencimentocR.text:=VencimentoCR;
  btpesquisacR.OnClick(nil);
end;

procedure TFrLancamento_novo.LancaDesconto(PTipo:string);
var
  Cont:Integer;
  ContaRec:Integer;
  ValorLanccomPai,ValorSoma,percentualdesconto:Currency;
  HistoricoTodos:String;
  PortadorUSadoNoRecebimento,NFTEMP,PCCredorDevedor,CodigoLAncamentoPai,CodigoLancamentoFilho,TmptipoLancto,tmptipolanctoligado:string;
  ppendencia,PCredorDevedorLote,PCodigoCredorDevedorLote:string;
  pvalor,pvalordesconto:String;
begin

  ContaRec:=0;
  valorsoma:=0;

  for cont:=1 to STRGCR.RowCount-1 do
  begin
    if (trim(StrgCR.Cells[8,cont]) <> '') then//verificando a coluna de valor a ser recebido
    begin
      Inc(ContaRec,1);
      //somando o valor do saldo das linhas selecionadas
      Ppendencia:=StrgCR.cells[3,cont];
      Pvalor:=tira_ponto(trim(StrgCR.Cells[7,cont]));
      VALORSOMA:=VALORSOMA+strtocurr(PVALOR);
    end;
  end;

  if (ContaRec=0) then
  begin
    Messagedlg('N�o foi definido o valor de nenhum T�tulo!',mtinformation,[mbok],0);
    exit;
  end;

  try
    strtodate(edtdatalancamentocr.text);
  except
    Messagedlg('Data Inv�lida para o lan�amento!',mterror,[mbok],0);
    exit;
  end;

  pvalordesconto:='0';

  if (Ptipo = '%') then
  begin
    if (inputquery('Desconto','Digite o valor do desconto em (%)',pvalordesconto)=False) then
      exit;
  end;

  if (Ptipo = 'R') then
  begin
    if (inputquery('Desconto','Digite o valor do desconto em R$',pvalordesconto)=False) then
      exit;
  end;

  try
    strtocurr(pvalordesconto);

    if (Ptipo='R') then
    begin
      if (strtocurr(Pvalordesconto)>valorsoma) then
      begin
        MensagemErro('O valor do desconto n�o pode ser superior ao saldo das pend�ncias selecionadas');
        exit;
      end;
    end
    else
    begin
      if (strtocurr(Pvalordesconto) > 100) then//100%
      begin
        MensagemErro('O desconto n�o pode ser superior a 100%');
        exit;
      end;
    end;

    if (strtocurr(Pvalordesconto) = 0) then
    begin
      MensagemErro('O desconto n�o pode ser zero');
      exit;
    end;

    //calculando quantos % sera dado de desconto
    //esse calculo � feito sobre  o valor de saldo das linhas selecionadas

    (*ValorSoma    100%
    Pvalordesconto   X*)

    if (ptipo='R') then
      percentualdesconto:=(strtocurr(pvalordesconto)*100)/valorsoma
    else percentualdesconto:=STRTOCURR(pvalordesconto);

    percentualdesconto:=strtocurr(formata_valor(percentualdesconto));

  except
     Mensagemerro('Valor Inv�lido para o Desconto');
     exit;
  end;

  for cont:=1 to StrgCr.RowCount-1 do
  begin
    if (Trim(StrgCR.Cells[8,cont])<>'') then
    begin
      try
         Pvalor:=tira_ponto(trim(StrgCR.Cells[7,cont]));//pego o saldo

         pvalordesconto:=currtostr((percentualdesconto*strtocurr(pvalor))/100);
         pvalordesconto:=tira_ponto(formata_valor(pvalordesconto));

         if (ObjLancamento.LancamentoAutomatico('D',StrgCR.cells[3,cont],pvalordesconto,
           edtdatalancamentocr.text,'Desconto em lote',Codigolancamentofilho,false)=False) then
         begin
           ObjLancamento.Rolback;
           Messagedlg('Lan�amentos Cancelados!',mterror,[mbok],0);
           ObjLancamento.Status:=dsInactive;
           exit;
         end;
      except
      end;
    end;
  end;

  ObjLancamento.Commit;
  Messagedlg('Desconto(s) efetuado(s) com Sucesso!',mtinformation,[mbok],0);
  edtvencimentocR.text:=VencimentoCR;
  btpesquisacR.OnClick(nil);
end;

procedure TFrLancamento_novo.strgcrKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key=#13) then
    STRGCR.OnDblClick(nil);

  if (key=#32) then
  begin
    if (STRGCR.cells[0,1]='') then
      exit;

    chkExatamenteIgual.visible:=true;
    chkContenha.visible:=true;
    PreparaPesquisa_StringGrid(STRGCR,edtpesquisa_STRG_GRID_CR,LbtipoCampoCR.Items);
  end;
end;

procedure TFrLancamento_novo.edtcodigohistoricosimplesKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (not (key in ['0'..'9',#8])) then
    key:=#0;
end;

procedure TFrLancamento_novo.AtualizaLabelsCp;
var
  cont:integer;
  valor:Currency;
  pvalorlancamento,psaldo,TotalCP:Currency;
begin
  TotalCP:=0;

  for cont:=1 to STRGCP.RowCount-1 do
  begin
    valor:=0;
    try
      if (trim(strgcp.Cells[8,cont]) <> '') then
        valor:=strtofloat(tira_ponto(strgcp.Cells[8,cont]));
    except
          valor:=0;
    end;
    TotalCp:=TotalCP+valor;
  end;

  try
    PvalorLancamento:=0;
    if (edtvalorlancamentoCP.Text<>'') then
      PvalorLancamento:=strtofloat(edtvalorlancamentoCP.Text);
  except
    PvalorLancamento:=0;
  end;

  try
    PSaldo:=Pvalorlancamento-TOTALCP;
  except
    PSaldo:=0;
  end;

  lbvalorlanctoCP.caption:=CompletaPalavra_a_Esquerda(formata_valor(pvalorlancamento),15,' ');
  lbvalorpagamento.caption:=CompletaPalavra_a_Esquerda(formata_valor(TOTALCP),15,' ');

  if (pvalorlancamento<>0) then
    LbSaldoCP          .caption:=CompletaPalavra_a_Esquerda(formata_valor(psaldo),15,' ')
  else LbSaldoCP          .caption:=CompletaPalavra_a_Esquerda(formata_valor('0'),15,' ');

end;

procedure TFrLancamento_novo.AtualizaLabelsCR;
var
  cont:integer;
  valor:Currency;
  pvalorlancamento,psaldo,TotalCR:Currency;
begin

  TOTALCR:=0;
  for cont:=1 to STRGCR.RowCount-1 do
  begin
    valor:=0;
    try
      if (Trim(strgcr.Cells[8,cont])<>'') then
        valor:=strtofloat(tira_ponto(strgcr.Cells[8,cont]));
    except
      valor:=0;
    end;
    TotalCR:=TotalCR+Valor;
  end;

  try
    PvalorLancamento := 0;
    if (edtvalorlancamentoCR.Text <> '') then
      PvalorLancamento:=strtofloat(edtvalorlancamentoCR.Text);
  except
    PvalorLancamento:=0;
  end;

  try
    PSaldo:=Pvalorlancamento-TOTALCR;
  except
    PSaldo:=0;
  end;

  LbValorLancamentoCR.caption := CompletaPalavra_a_Esquerda(formata_valor(pvalorlancamento),15,' ');
  lbrecebe.caption := CompletaPalavra_a_Esquerda(formata_valor(TOTALCR),15,' ');

  if (pvalorlancamento<>0) then
    LbSaldoCR.caption := CompletaPalavra_a_Esquerda(formata_valor(psaldo),15,' ')
  else LbSaldoCR.caption := CompletaPalavra_a_Esquerda(formata_valor('0'),15,' ')
end;

procedure TFrLancamento_novo.btchequecaixaClick(Sender: TObject);
begin
  ObjLancamento.EmiteChequeCaixa;
end;

procedure TFrLancamento_novo.edtcodigocredordevedorCPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin
  if (key <>vk_f9) then
    exit;

  if (combocredordevedorCP.ItemIndex = -1) then
  begin
    MensagemErro('Selecione primeiro o Credor/Devedor antes de realizar a pesquisa!');
    combocredordevedorCP.SetFocus;
    exit;
  end;

  try
    Fpesquisalocal := Tfpesquisa.create(Self);

    if (FpesquisaLocal.PreparaPesquisaN(ObjLancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigocp.text),'Pesquisa de '+Combocredordevedorcp.text,ObjLancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigocp.text))=True) then
    begin
      try
        if (FpesquisaLocal.showmodal=mrok) then
        begin
          edtcodigocredordevedorcp.text:=edtcodigocredordevedorcp.text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
          LbNomeCredorDevedorcp.caption:=ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocp.text,edtcodigocredordevedorcp.Text);
        end;
      finally
        FpesquisaLocal.QueryPesq.close;
      end;
    end;
  finally
    FreeandNil(FPesquisaLocal);
  end;
end;

procedure TFrLancamento_novo.edtcodigocredordevedorCPExit(Sender: TObject);
begin
  LbNomeCredorDevedorcp.caption:='';

  if (edtcodigocredordevedorcp.text='') then //or (combocredordevedorcp.ItemIndex=-1)
    exit;

  LbNomeCredorDevedorcp.caption:=ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocp.text,edtcodigocredordevedorcp.Text);
end;

procedure TFrLancamento_novo.combocredordevedorCPChange(Sender: TObject);
begin
  combocredordevedorcodigocp.itemindex:=combocredordevedorcp.itemindex;
end;

procedure TFrLancamento_novo.combocredordevedorcrChange(Sender: TObject);
begin
  combocredordevedorcodigocr.itemindex:=combocredordevedorcr.itemindex;
end;

procedure TFrLancamento_novo.edtcodigocredordevedorcrExit(Sender: TObject);
begin
  LbNomeCredorDevedorcr.caption:='';
  if (edtcodigocredordevedorcr.text='') then//or (combocredordevedorcr.ItemIndex=-1)
    exit;
  LbNomeCredorDevedorcr.caption:=ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocr.text,edtcodigocredordevedorcr.Text);
end;

procedure TFrLancamento_novo.edtcodigocredordevedorcrKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin
  if (key <> vk_f9) then
    exit;

  if (combocredordevedorcr.itemindex = -1) then
  begin
    MensagemErro('Selecione primeiro o Credor/Devedor antes de realizar a Pesquisa!');
    combocredordevedorcr.SetFocus;
    exit;
  end;

  try
    Fpesquisalocal := Tfpesquisa.create(Self);
    if (FpesquisaLocal.PreparaPesquisaN(ObjLancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigocr.text),'Pesquisa de ' + Combocredordevedorcr.text,ObjLancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigocr.text)) = True) then
    begin
      try
        if (FpesquisaLocal.showmodal = mrok) then
        begin
          edtcodigocredordevedorcr.text := edtcodigocredordevedorcr.text + ';' + FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
          LbNomeCredorDevedorcr.caption := ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocr.text,edtcodigocredordevedorcr.Text);
        end;
      finally
        FpesquisaLocal.QueryPesq.close;
      end;
    end;
  finally
    FreeandNil(FPesquisaLocal);
  end;
end;


procedure TFrLancamento_novo.BtApagaLoteClick(Sender: TObject);
begin
  ObjLancamento.ExcluiLancamento;
end;

procedure TFrLancamento_novo.edtcodigohistoricoCRKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:TFpesquisa;
  FHistoricoSimples:TFHistoricoSimples;
begin
  if (key<>vk_f9) then
    exit;

  try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FHistoricoSimples:=TFHistoricoSimples.create(nil);

    if (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,
      ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples) = True) then
    begin
      try
        if (FpesquisaLocal.showmodal = mrok) then
        begin
          TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
          if (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False) then
          begin
            TEdit(Sender).text:='';
            exit;
          end;
          ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
          edthistoricoCR.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
        end;
      finally
        FpesquisaLocal.QueryPesq.close;
      end;
    end;
  finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FHistoricoSimples);
  end;
end;

procedure TFrLancamento_novo.edtcodigohistoricoCRKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (not (key in ['0'..'9',#8])) then
    key:=#0;
end;

procedure TFrLancamento_novo.edtcodigohistoricoCRExit(Sender: TObject);
begin
  if (TEdit(Sender).text='') then
    exit;

  If (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False) then
  begin
    TEdit(Sender).text:='';
    exit;
  end;

  ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
  edthistoricoCR.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
  TEdit(Sender).text:='';
end;

procedure TFrLancamento_novo.edtvalorlancamentoCRExit(Sender: TObject);
begin
  AtualizaLabelsCR;
end;

procedure TFrLancamento_novo.edtcodigohistoricoCPExit(Sender: TObject);
begin
  if (TEdit(Sender).text='') then
    exit;

  if (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False) then
  begin
    TEdit(Sender).text:='';
    exit;
  end;

  ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
  edthistoricoCP.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
  TEdit(Sender).text:='';
end;

procedure TFrLancamento_novo.edtcodigohistoricoCPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:TFpesquisa;
  FHistoricoSimples:TFHistoricoSimples;
begin
  if (key<>vk_f9) then
    exit;

  try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FHistoricoSimples:=TFHistoricoSimples.create(nil);

    if (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,
      ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples) = True) then
    begin
      try
        if (FpesquisaLocal.showmodal=mrok) then
        begin
          TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
          if (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text) = False) then
          begin
            TEdit(Sender).text:='';
            exit;
          end;

          ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
          edthistoricoCP.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
        end;
      finally
        FpesquisaLocal.QueryPesq.close;
      end;
    end;

  finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FHistoricoSimples);
  end;
end;

procedure TFrLancamento_novo.edtvalorlancamentoCPExit(Sender: TObject);
begin
  AtualizaLabelsCp;
end;

procedure TFrLancamento_novo.edtvalorlancamentoCPKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8,',']) then
  begin
    if key='.' then
      key:=','
    else Key:=#0;
  end;
end;

procedure TFrLancamento_novo.STRGCPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (STRGCP.cells[0,1]='') then
    exit;

  if (key=vk_f12) then
    Ordena_StringGrid(STRGCP,LbtipoCampoPG.Items);

  if ((ssctrl in shift) and (key=ord('P'))) or
    ((ssctrl in shift) and (key=ord('p'))) then
    Self.ImprimeGridPagamento;
end;

procedure TFrLancamento_novo.edtpesquisa_STRG_GRID_CPKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key =#27 then //ESC
  begin
    edtpesquisa_STRG_GRID_CP.Visible := false;
    STRGCP.SetFocus;
    exit;
  end;

  if key=#13 then
  begin
    Pesquisa_StringGrid(STRGCP,edtpesquisa_STRG_GRID_CP,LbtipoCampoPG.Items);
    if (edtpesquisa_STRG_GRID_CP.Visible=False) then
      STRGCP.SetFocus;
  end;
end;

procedure TFrLancamento_novo.STRGCPEnter(Sender: TObject);
begin
  edtpesquisa_STRG_GRID_CP.Visible:=false;
end;

procedure TFrLancamento_novo.strgcrEnter(Sender: TObject);
begin
  edtpesquisa_STRG_GRID_CR.Visible:=false;
end;

procedure TFrLancamento_novo.strgcrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (STRGCR.cells[0,1] = '') then
    exit;

  if (key = vk_f12) then
    Ordena_StringGrid(STRGCR,LbtipoCampoCR.Items);

  if ((ssctrl in shift) and (key=ord('P'))) or
   ((ssctrl in shift) and (key=ord('p'))) then
    Self.ImprimeGridRecebimento;
end;

procedure TFrLancamento_novo.edtpesquisa_STRG_GRID_CRKeyPress(Sender: TObject;
  var Key: Char);
begin
  if key =#27 then//ESC
  begin
    edtpesquisa_STRG_GRID_CR.Visible := false;
    STRGCR.SetFocus;
    exit;
  end;

  if key=#13 then
  begin
    Pesquisa_StringGrid(STRGCR,edtpesquisa_STRG_GRID_CR,LbtipoCampoCR.Items);
    if (edtpesquisa_STRG_GRID_CR.Visible=False) then
      STRGCR.SetFocus;
  end;
end;

procedure TFrLancamento_novo.chkExatamenteIgualKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (chkExatamenteIgual.Checked=true) then
  begin
    chkContenha.Checked:=false;
    edtpesquisa_STRG_GRID_CR.setfocus;
  end;
end;

procedure TFrLancamento_novo.btlancaboletoClick(Sender: TObject);
var
  PValorBoleto:Currency;
  cont:integer;
  StrPend,StrValorPend:TStringList;
begin
  PvalorBoleto:=0;
  //Primeiro Somo os Valores anotados em cada pendencia
  for cont:=1 to STRGCR.RowCount-1 do
  begin
    if (trim(StrgCR.Cells[8,cont])<>'') then //verificando a coluna de valor a ser recebido
      PValorBoleto:=PvalorBoleto+strtofloat(tira_ponto(StrgCR.Cells[8,cont]));
  end;


  if (PValorBoleto=0) then
  begin
    Messagedlg('Nenhum boleto foi selecionado',mtinformation,[mbok],0);
    exit;
  end;

  if (Messagedlg('Valor do Boleto R$ '+formata_valor(Pvalorboleto)+#13+'Confirma?',mtconfirmation,[mbyes,mbno],0)=mrno) then
    exit;

  try
    StrPend:=TStringList.Create;
    StrValorPend:=TStringList.Create;
    StrValorPend.clear;
    StrPend.clear;
  except
    Messagedlg('Erro na Tentativa de Criar a StringList STRPEND',mterror,[mbok],0);
    exit;
  end;

  try
    //guardando as pendencias escolhidas
    for cont:=1 to StrgCR.RowCount-1 do
    begin
      if (trim(StrgCR.Cells[8,cont]) <> '') then
      begin
                //a coluna 3 � o numero da pend�ncia
                strPend.add(StrgCR.cells[3,cont]);
                StrValorPend.add(tira_ponto(STRGCR.cells[8,cont]));
      end;
    end;

    for cont:=0 to StrPend.Count-1 do
    begin
      ObjLancamento.Pendencia.ZerarTabela;
      if (ObjLancamento.Pendencia.LocalizaCodigo(StrPend[cont])=False) then
      begin
        Messagedlg('Pend�ncia '+strpend[cont]+' n�o encontrado',mterror,[mbok],0);
        exit;
      end;

      ObjLancamento.Pendencia.TabelaparaObjeto;
      if (ObjLancamento.Pendencia.BoletoBancario.Get_CODIGO <> '') then
      begin
        Messagedlg('A pend�ncia '+ObjLancamento.Pendencia.get_codigo+' j� possui um boleto',mterror,[mbok],0);
        exit;
      end;
    end;

    //Passo as Pendencias, e o Valor que o Boleto sera gerado
    ObjLancamento.Pendencia.LancaBoleto_Varias_Pendencias(StrPend,StrValorPend,PValorBoleto);
  finally
    Freeandnil(StrPend);
    Freeandnil(StrValorPend);
  end;
end;

procedure TFrLancamento_novo.btimprimecomprovantepagamentoClick(Sender: TObject);
var
  PCodigo, pTitulo:String;
Begin
  Pcodigo := '';
  pTitulo := '';

  if (InputQuery('Recibo','Digite o C�digo do Lan�amento',pcodigo)=False) then
    exit;

  if Trim( pCodigo ) = '' then
    Exit;  

  pTitulo := ObjLancamento.RetornaTitulolancamentoUnicoCredor( PCodigo );

  Self.ObjLancamento.ImprimeReciboPagamento( Pcodigo, pTitulo );
end;

procedure TFrLancamento_novo.BtSelecionaTodas_PGClick(Sender: TObject);
var
  cont:integer;
begin
  //seleciona todas com o valor do saldo
  for cont:=1 to STRGCP.RowCount-1 do
  begin
    if (trim(STRGCP.cells[8,Cont])='') then
      STRGCP.cells[8,Cont]:=STRGCP.cells[7,Cont];
  end;
  atualizalabelscp;
  STRGCP.SetFocus;
end;

procedure TFrLancamento_novo.BtSelecionaTodas_CRClick(Sender: TObject);
var
  cont:integer;
begin
  //seleciona todas com o valor do saldo
  for cont:=1 to strgcr.RowCount-1 do
  begin
    if (trim(STRGCR.cells[8,Cont])='') then
      STRGCR.cells[8,Cont]:=STRGCR.cells[7,Cont];
  end;
  atualizalabelscr;
  STRGCR.SetFocus;
end;

procedure TFrLancamento_novo.edtcodigoboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  Self.ObjLancamento.Pendencia.edtBoletoUsadoKeyDown(sender,key,shift);
end;

procedure TFrLancamento_novo.SalvaContasReceber_UmaPendencia(ppendencia,pvalor:string);
var
  Phistorico:String;
  Pcodigo:string;
begin
  phistorico:='';

  if (edthistoricoCR.Text='') then
  begin
    Self.ObjLancamento.Pendencia.LocalizaCodigo(ppendencia);
    Self.ObjLancamento.Pendencia.TabelaparaObjeto;
    Phistorico:='Recebimento '+Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
  end
  else Phistorico:=edthistoricoCR.text;

  Pcodigo:=Self.ObjLancamento.Get_NovoCodigo;
     
  //chamo essa versao do NovoLancamento pois o codigo retorna
  if (Self.ObjLancamento.NovoLancamento(ppendencia,'R',pvalor,pcodigo,phistorico,False,true,true,'','',datetostr(now))=False) then
  begin
    FdataModulo.IBTransaction.RollbackRetaining;
    Messagedlg('Erro na tentativa de Salvar',mterror,[mbok],0);
    //desabilita_campos(self);
    exit;
  end;

  FdataModulo.IBTransaction.CommitRetaining;

  EdtvencimentocR.text:=VencimentoCR;
  BtpesquisacR.OnClick(nil);
end;

procedure TFrLancamento_novo.SalvaContasPagar_UmaPendencia(ppendencia,pvalor:string);
var
  Phistorico:String;
  Pcodigo:string;
begin
  phistorico:='';

  if (edthistoricoCP.Text='') then
  begin
    Self.ObjLancamento.Pendencia.LocalizaCodigo(ppendencia);
    Self.ObjLancamento.Pendencia.TabelaparaObjeto;
    Phistorico:='Pagamento '+Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCREDORDevedor(Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
  end
  else Phistorico:=edthistoricoCP.text;

  Pcodigo:=Self.ObjLancamento.Get_NovoCodigo;

  //chamo essa versao do NovoLancamento pois o codigo retorna
  if (Self.ObjLancamento.NovoLancamento(ppendencia,'P',pvalor,pcodigo,phistorico,False,true,true,'','',datetostr(now))=False) then
  begin
    FdataModulo.IBTransaction.RollbackRetaining;
    Messagedlg('Erro na tentativa de Salvar',mterror,[mbok],0);
    //desabilita_campos(self);
    exit;
  end;

  FdataModulo.IBTransaction.CommitRetaining;

  EdtvencimentoCP.text:=VencimentoCP;
  BtpesquisaCP.OnClick(nil);
end;

procedure TFrLancamento_novo.ImprimeGridRecebimento;
var
  cont:integer;
  psomasaldo,psomaareceber,psomavalor:currency;
begin
  with FreltxtRDPRINT do
  begin
    ConfiguraImpressao;
    RDprint.TamanhoQteColunas:=130;
    RDprint.FonteTamanhoPadrao:=S17cpp;
    LinhaLocal:=3;
    rdprint.Abrir;

    if (rdprint.setup=False) then
    begin
      rdprint.Fechar;
      exit;
    end;

    RDprint.impc(linhalocal,65,'DEMONSTRATIVO DE CONTAS A RECEBER',[negrito]);
    IncrementaLinha(2);

    RDprint.impf(linhalocal,1,CompletaPalavra('Hist�rico',50,' ')+' '+
                              CompletaPalavra('Vencimento',10,' ')+' '+
                              CompletaPalavra_a_Esquerda('Valor',12,' ')+' '+
                              CompletaPalavra_a_Esquerda('Saldo',12,' ')+' '+
                              CompletaPalavra_a_Esquerda('Vl.a.Receber',12,' ')+' '+
                              CompletaPalavra('N� Documento',29,' '),[negrito]);
    IncrementaLinha(1);
    DesenhaLinha;

    psomavalor:=0;
    psomasaldo:=0;
    psomaareceber:=0;

    for cont:=1 to strgcr.rowcount-1 do
    begin
      VerificaLinha;
      RDprint.imp(linhalocal,1,CompletaPalavra(strgcr.Cells[1,cont],50,' ')+' '+
                              CompletaPalavra(strgcr.Cells[5,cont],10,' ')+' '+
                              CompletaPalavra_a_Esquerda(strgcr.Cells[6,cont],12,' ')+' '+
                              CompletaPalavra_a_Esquerda(strgcr.Cells[7,cont],12,' ')+' '+
                              CompletaPalavra_a_Esquerda(strgcr.Cells[8,cont],12,' ')+' '+
                              CompletaPalavra(strgcr.Cells[9,cont],29,' '));
      IncrementaLinha(1);

      psomavalor:=psomavalor+strtocurr(tira_ponto(strgcr.Cells[6,cont]));
      psomasaldo:=psomasaldo+strtocurr(tira_ponto(strgcr.Cells[7,cont]));

      try
        if (trim(strgcr.Cells[8,cont])<>'') then
          psomaareceber:=psomaareceber+strtocurr(tira_ponto(strgcr.Cells[8,cont]));
      except

      end;
    end;

    DesenhaLinha;
    VerificaLinha;
    RDprint.impf(linhalocal,1,CompletaPalavra('SOMA',50,' ')+' '+
                             CompletaPalavra('',10,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(PSOMAVALOR),12,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(PSOMASALDO),12,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(PSOMAARECEBER),12,' ')+' '+
                             CompletaPalavra('',29,' '),[negrito]);
    IncrementaLinha(1);
    
    rdprint.fechar;
  end;
end;


procedure TFrLancamento_novo.ImprimeGridPagamento;
var
cont:integer;
psomasaldo,psomaapagar,psomavalor:currency;
begin
  with FreltxtRDPRINT do
  begin
    ConfiguraImpressao;
    RDprint.TamanhoQteColunas:=130;
    RDprint.FonteTamanhoPadrao:=S17cpp;
    LinhaLocal:=3;
    rdprint.Abrir;

    if (rdprint.setup=False) then
    begin
      rdprint.Fechar;
      exit;
    end;

    RDprint.impc(linhalocal,65,'DEMONSTRATIVO DE CONTAS A PAGAR',[negrito]);
    IncrementaLinha(2);

    RDprint.impf(linhalocal,1,CompletaPalavra('Pend�ncia',9,' ')+' '+
                              CompletaPalavra('Hist�rico',50,' ')+' '+
                              CompletaPalavra('Vencimento',10,' ')+' '+
                              CompletaPalavra_a_Esquerda('Valor',12,' ')+' '+
                              CompletaPalavra_a_Esquerda('Saldo',12,' ')+' '+
                              CompletaPalavra_a_Esquerda('Vl.a.Pagar',12,' ')+' '+
                              CompletaPalavra('N� Documento',19,' '),[negrito]);
    IncrementaLinha(1);
    DesenhaLinha;

    psomavalor:=0;
    psomasaldo:=0;
    psomaapagar:=0;

    for cont:=1 to STRGCP.rowcount-1 do
    begin
      VerificaLinha;
      RDprint.imp(linhalocal,1,CompletaPalavra(STRGCP.Cells[3,cont],9,' ')+' '+
                              CompletaPalavra(STRGCP.Cells[1,cont],50,' ')+' '+
                              CompletaPalavra(STRGCP.Cells[5,cont],10,' ')+' '+
                              CompletaPalavra_a_Esquerda(STRGCP.Cells[6,cont],12,' ')+' '+
                              CompletaPalavra_a_Esquerda(STRGCP.Cells[7,cont],12,' ')+' '+
                              CompletaPalavra_a_Esquerda(STRGCP.Cells[8,cont],12,' ')+' '+
                              CompletaPalavra(STRGCP.Cells[9,cont],19,' '));
      IncrementaLinha(1);

      psomavalor:=psomavalor+strtocurr(tira_ponto(STRGCP.Cells[6,cont]));
      psomasaldo:=psomasaldo+strtocurr(tira_ponto(STRGCP.Cells[7,cont]));

      try
        if (trim(STRGCP.Cells[8,cont])<>'') then
          psomaapagar:=psomaapagar+strtocurr(tira_ponto(STRGCP.Cells[8,cont]));
      except

      end;
    end;

    DesenhaLinha;
    VerificaLinha;
    RDprint.impf(linhalocal,1,CompletaPalavra('SOMA',60,' ')+' '+
                             CompletaPalavra('',10,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(PSOMAVALOR),12,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(PSOMASALDO),12,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(psomaapagar),12,' ')+' '+
                             CompletaPalavra('',19,' '),[negrito]);
    IncrementaLinha(1);

    rdprint.fechar;
  end;
end;

procedure TFrLancamento_novo.btdescontoReaisClick(Sender: TObject);
begin
  Self.LancaDesconto('R');
end;

procedure TFrLancamento_novo.btDescontoPercClick(Sender: TObject);
begin
  Self.LancaDesconto('%');
end;

procedure TFrLancamento_novo.strgcrDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  temp:string;
  pvalor:currency;
begin

  if (Arow=0) then
    exit;

  temp:=trim((Sender as TStringGrid).Cells[(Sender as TStringGrid).tag,Arow]);

  if (temp<>'') then
  begin
    try
      pvalor:=strtofloat(come(temp,'.'));
    except
      pvalor:=0;
    end;
  end
  else Pvalor:=0;

  if (pvalor>0) then
  begin
    (Sender as TStringGrid).canvas.brush.color :=RGB(255,255,153);
    (Sender as TStringGrid).canvas.Font.Color:=clblack;

    (Sender as TStringGrid).canvas.FillRect(rect);
    (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
  end
  else
  begin
    (Sender as TStringGrid).canvas.brush.color :=clwhite;
    (Sender as TStringGrid).canvas.Font.Color:=clblack;

    (Sender as TStringGrid).canvas.FillRect(rect);
    (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
  end;
end;

procedure TFrLancamento_novo.btAlterarHistoricoLancamentoClick(Sender: TObject);
begin
  objLancamento.AlterarHistoricoLancamento;
end;

procedure TFrLancamento_novo.edtcontagerencial_CPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin
  if (key <> vk_f9) then
    exit;

  try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FpesquisaLocal.NomeCadastroPersonalizacao:='edtcontagerencialKeyDown_LANCAMENTOFR';

    if (FpesquisaLocal.PreparaPesquisaN(ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Pesquisa2('D'),'Contas Gerenciais - T�tulos a Pagar','')=True) then
    begin
      try
        if (FpesquisaLocal.showmodal=mrok) then
          edtcontagerencial_CP.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
      finally
        FpesquisaLocal.QueryPesq.close;
      end;
    end;
  finally
    FreeandNil(FPesquisaLocal);
  end;
end;

procedure TFrLancamento_novo.edtsubcontagerencial_CPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin
  if (key <>vk_f9) then
    exit;

  if (edtcontagerencial_CP.text='') then
  begin
    MensagemSucesso('Escolha primeiro sua Conta Gerencial');
    exit;
  end;

  try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FpesquisaLocal.NomeCadastroPersonalizacao:='edtsubcontagerencialKeyDown_LANCAMENTOFR';

    if (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Pendencia.Titulo.SubContaGerencial.Get_Pesquisa(edtcontagerencial_CP.Text),'Sub-Contas Gerenciais - T�tulos a Pagar',nil)=True) then
    begin
      try
        if (FpesquisaLocal.showmodal=mrok) then
          edtsubcontagerencial_CP.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
      finally
        FpesquisaLocal.QueryPesq.close;
      end;
    end;
  finally
    FreeandNil(FPesquisaLocal);
  end;
end;

procedure TFrLancamento_novo.chkSelecionaTodasCPClick(Sender: TObject);
var
cont:integer;
begin
  //seleciona todas com o valor do saldo
  if CheckSelecionaPagamento.Checked=False then
  begin
    for cont:=1 to STRGCP.RowCount-1 do
    begin
         STRGCP.cells[8,Cont]:='';
    end;
  end
  else
  begin
    for cont:=1 to STRGCP.RowCount-1 do
    begin
      if (trim(STRGCP.cells[8,Cont])='') then
        STRGCP.cells[8,Cont]:=STRGCP.cells[7,Cont];
    end;
  end;
  atualizalabelscp;
  STRGCP.SetFocus;
end;

procedure TFrLancamento_novo.chkSelecionaTodasCRClick(Sender: TObject);
var
  cont:integer;
begin
  //seleciona todas com o valor do saldo
  if chkSelecionaTodasCR.Checked=False then
  begin
    for cont:=1 to STRGCR.RowCount-1 do
    begin
      STRGCR.cells[8,Cont]:='';
    end;
  end
  else
  begin
    for cont:=1 to STRGCR.RowCount-1 do
    begin
      if (trim(STRGCR.cells[8,Cont])='') then
        STRGCR.cells[8,Cont]:=STRGCR.cells[7,Cont];
    end;
  end;

  atualizalabelscr;
  STRGCR.SetFocus;
end;

procedure TFrLancamento_novo.LimpaLabels;
begin
  lbsaldocp.Caption:='0,00';
  LbSaldocr.Caption:='0,00';
  lbvalorlanctoCP.caption:='0,00';
  LbValorLancamentocr.caption:='0,00';
  lbrecebe.caption:='0,00';
  lbvalorpagamento.caption:='0,00'
end;

procedure TFrLancamento_novo.CheckSelecionaPagamentoClick(Sender: TObject);
  var cont:integer;
begin
  if (CheckSelecionaPagamento.Checked=False) then
  begin
    LimpaLabels;
    STRGCP.RowCount:=-1;
    BtpesquisaCPClick(Sender);

    //For cont:=1 to STRGCP.RowCount-1 do
    //Begin
    //     STRGCP.cells[8,Cont]:='';
    //end;
  end
  else
  begin
    for cont:=1 to STRGCP.RowCount-1 do
    begin
      if (trim(STRGCP.cells[8,Cont])='') then
        STRGCP.cells[8,Cont]:=STRGCP.cells[7,Cont];
    end;
    atualizalabelscp;
    STRGCP.SetFocus;
  end;
end;

procedure TFrLancamento_novo.CheckSelecionaRecebimentoClick(
  Sender: TObject);
var
  cont : integer;
begin

  if (CheckSelecionaRecebimento.Checked=False) then
  begin
    LimpaLabels;
    strgcr.RowCount:=-1;
    btpesquisacrClick(Sender);

    //For cont:=1 to strgcr.RowCount-1 do
    // Begin
    //      strgcr.cells[8,Cont]:='';
    //end;
  end
  //seleciona todas com o valor do saldo
  else
  begin
    for cont:=1 to strgcr.RowCount-1 do
    begin
      if (trim(STRGCR.cells[8,Cont])='') then
        STRGCR.cells[8,Cont]:=STRGCR.cells[7,Cont];
    end;
    atualizalabelscr;
    STRGCR.SetFocus;

    {For cont:=1 to strgcr.RowCount-1 do
     Begin
          if (trim(STRGCR.cells[8,Cont])='')
          Then STRGCR.cells[8,Cont]:=STRGCR.cells[7,Cont];
     end;
     atualizalabelscr;
     STRGCR.SetFocus;
    }
  end;
end;

procedure TFrLancamento_novo.chkContenhaKeyPress(Sender: TObject;
  var Key: Char);
begin
  if (chkContenha.Checked=true) then
  begin
    edtpesquisa_STRG_GRID_CR.text:='%';
    edtpesquisa_STRG_GRID_CR.setfocus;
    chkExatamenteIgual.Checked:=false;
  end
  else
  begin
    edtpesquisa_STRG_GRID_CR.Text:='';
    edtpesquisa_STRG_GRID_CR.setfocus;
  end;
end;

procedure TFrLancamento_novo.chkExatamenteIgualClick(Sender: TObject);
begin
  if (chkExatamenteIgual.Checked=true) then
  begin
    if strgcr.RowCount<=1 then
    begin
      MensagemAviso('Escolha primeiro o Credor/Devedor, ap�s clique em Pesquisar');
      edtpesquisa_STRG_GRID_CR.Visible:=false;
      exit;
    end
    else
    begin
      edtpesquisa_STRG_GRID_CR.Visible:=true;
      chkContenha.Checked:=false;
      edtpesquisa_STRG_GRID_CR.setfocus;
    end;
  end;
end;

procedure TFrLancamento_novo.chkContenhaClick(Sender: TObject);
begin
  if strgcr.RowCount<=1 then
  begin
    MensagemAviso('Escolha primeiro o Credor/Devedor ap�s clique em Pesquisar');
    exit;
  end
  else
  begin
    if (chkContenha.Checked=true) then
    begin
      edtpesquisa_STRG_GRID_CR.Visible:=true;
      edtpesquisa_STRG_GRID_CR.text:='%';
      edtpesquisa_STRG_GRID_CR.setfocus;
      chkExatamenteIgual.Checked:=false;
    end
    else
    begin
      edtpesquisa_STRG_GRID_CR.Text:='';
      edtpesquisa_STRG_GRID_CR.setfocus;
    end;
  end;
end;

procedure TFrLancamento_novo.chkigualaPGClick(Sender: TObject);
begin
  if (chkigualaPG.Checked=true) then
  begin
    edtpesquisa_STRG_GRID_CP.Visible:=true;
    chkContenhaPG.Checked:=false;
    edtpesquisa_STRG_GRID_CP.setfocus;
  end;
end;

procedure TFrLancamento_novo.chkContenhaPGClick(Sender: TObject);
begin
  if (chkContenhaPG.Checked=true) then
  begin
    edtpesquisa_STRG_GRID_CP.Visible:=true;
    edtpesquisa_STRG_GRID_Cp.text:='%';
    edtpesquisa_STRG_GRID_Cp.setfocus;
    chkigualaPG.Checked:=false;
  end
  else
  begin
    edtpesquisa_STRG_GRID_CP.Text:='';
    edtpesquisa_STRG_GRID_CP.setfocus;
  end;
end;

procedure TFrLancamento_novo.chkContenhaExit(Sender: TObject);
begin
  edtpesquisa_STRG_GRID_CR.setfocus;
end;

procedure TFrLancamento_novo.chkExatamenteIgualExit(Sender: TObject);
begin
  edtpesquisa_STRG_GRID_CR.setfocus;
end;

procedure TFrLancamento_novo.chkigualaPGExit(Sender: TObject);
begin
  edtpesquisa_STRG_GRID_CP.setfocus;
end;

procedure TFrLancamento_novo.chkContenhaPGExit(Sender: TObject);
begin
  edtpesquisa_STRG_GRID_CP.setfocus;
end;

end.


