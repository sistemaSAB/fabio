unit UobjCREDITOVALORES;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJCREDORDEVEDOR,UOBJvalores;

Type
   TObjCREDITOvalores=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               credordevedor:TOBJCREDORDEVEDOR;
               valores:TOBJvalores;
//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaValores(Parametro:string) :boolean;
                
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_codigocredordevedor(parametro: string);
                Function Get_codigocredordevedor: string;
                Procedure Submit_valorusado(parametro: string);
                Function Get_valorusado: string;
                procedure EdtcredordevedorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtcredordevedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtvaloresExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtvaloresKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               codigocredordevedor:string;
               valorusado:string;
//CODIFICA VARIAVEIS PRIVADAS





               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Function  DebitaSaldoCredito:boolean;
                Function  CreditaSaldoCredito:boolean;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UCredorDevedor;





Function  TObjCREDITOvalores.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('credordevedor').asstring<>'')
        Then Begin
                 If (Self.credordevedor.LocalizaCodigo(FieldByName('credordevedor').asstring)=False)
                 Then Begin
                          Messagedlg('Credor/Devedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.credordevedor.TabelaparaObjeto;
        End;
        Self.codigocredordevedor:=fieldbyname('codigocredordevedor').asstring;
        If(FieldByName('valores').asstring<>'')
        Then Begin
                 If (Self.valores.LocalizaCodigo(FieldByName('valores').asstring)=False)
                 Then Begin
                          Messagedlg('C�digo do Lan�amento N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.valores.TabelaparaObjeto;
        End;
        Self.valorusado:=fieldbyname('valorusado').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjCREDITOvalores.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('credordevedor').asstring:=Self.credordevedor.GET_CODIGO;
        ParamByName('codigocredordevedor').asstring:=Self.codigocredordevedor;
        ParamByName('valores').asstring:=Self.valores.GET_CODIGO;
        ParamByName('valorusado').asstring:=virgulaparaponto(Self.valorusado);
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjCREDITOvalores.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 if (Self.Status=dsinsert)
 Then Begin//esta inserindo entao tenho q debitar o saldo o cliente
      // Essa fun��o foi tiradas por f�bio. Porque os Saldo est� sesndo debitado em outra fun��o 
      {    if (Self.DebitaSaldoCredito=False)
          then Begin
                    mensagemerro('N�o poss�vel debitar o saldo do cr�dito');
                    exit;
          End;
      }
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCREDITOvalores.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        credordevedor.ZerarTabela;
        codigocredordevedor:='';
        valores.ZerarTabela;
        valorusado:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjCREDITOvalores.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (credordevedor.get_Codigo='')
      Then Mensagem:=mensagem+'/Credor/Devedor';
      If (codigocredordevedor='')
      Then Mensagem:=mensagem+'/C�digo do Credor/Devedor';
      If (valores.get_Codigo='')
      Then Mensagem:=mensagem+'/C�digo do Lan�amento';
      If (valorusado='')
      Then Mensagem:=mensagem+'/Valor Usado';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCREDITOvalores.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.credordevedor.LocalizaCodigo(Self.credordevedor.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Credor/Devedor n�o Encontrado!';
      If (Self.valores.LocalizaCodigo(Self.valores.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ C�digo do Lan�amento n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCREDITOvalores.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.credordevedor.Get_Codigo<>'')
        Then Strtoint(Self.credordevedor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Credor/Devedor';
     End;
     try
        Strtoint(Self.codigocredordevedor);
     Except
           Mensagem:=mensagem+'/C�digo do Credor/Devedor';
     End;
     try
        If (Self.valores.Get_Codigo<>'')
        Then Strtoint(Self.valores.Get_Codigo);
     Except
           Mensagem:=mensagem+'/C�digo do Lan�amento';
     End;
     try
        Strtofloat(Self.valorusado);
     Except
           Mensagem:=mensagem+'/Valor Usado';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCREDITOvalores.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCREDITOvalores.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCREDITOvalores.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CREDITOvalores vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,credordevedor,codigocredordevedor,valores,valorusado');
           SQL.ADD(' ');
           SQL.ADD(' from  TabCreditovalores');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjCREDITOvalores.LocalizaValores(parametro: string): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro de localiza��o de valores vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,credordevedor,codigocredordevedor,valores,valorusado');
           SQL.ADD(' ');
           SQL.ADD(' from  TabCreditovalores');
           SQL.ADD(' WHERE valores='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjCREDITOvalores.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCREDITOvalores.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 If(self.CreditaSaldoCredito=False)
                 Then Begin
                          result:=False;
                          exit;
                 End;

                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCREDITOvalores.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.credordevedor:=TOBJCREDORDEVEDOR.create;
        Self.valores:=TOBJvalores.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabCreditovalores(CODIGO,credordevedor');
                InsertSQL.add(' ,codigocredordevedor,valores,valorusado)');
                InsertSQL.add('values (:CODIGO,:credordevedor,:codigocredordevedor');
                InsertSQL.add(' ,:valores,:valorusado)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabCreditovalores set CODIGO=:CODIGO,credordevedor=:credordevedor');
                ModifySQL.add(',codigocredordevedor=:codigocredordevedor,valores=:valores');
                ModifySQL.add(',valorusado=:valorusado');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabCreditovalores where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCREDITOvalores.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCREDITOvalores.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCREDITOvalores');
     Result:=Self.ParametroPesquisa;
end;

function TObjCREDITOvalores.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CREDITOvalores ';
end;


function TObjCREDITOvalores.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCREDITOvalores,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCREDITOvalores,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCREDITOvalores.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.credordevedor.FREE;
    Self.valores.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCREDITOvalores.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCREDITOvalores.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCreditovalores.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjCreditovalores.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjCreditovalores.Submit_codigocredordevedor(parametro: string);
begin
        Self.codigocredordevedor:=Parametro;
end;
function TObjCreditovalores.Get_codigocredordevedor: string;
begin
        Result:=Self.codigocredordevedor;
end;
procedure TObjCreditovalores.Submit_valorusado(parametro: string);
begin
        Self.valorusado:=Parametro;
end;
function TObjCreditovalores.Get_valorusado: string;
begin
        Result:=Self.valorusado;
end;
//CODIFICA GETSESUBMITS


procedure TObjCREDITOvalores.EdtcredordevedorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.credordevedor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.credordevedor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.credordevedor.GET_NOME;
End;
procedure TObjCREDITOvalores.EdtcredordevedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCREDORDEVEDOR:TFCREDORDEVEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCREDORDEVEDOR:=TFCREDORDEVEDOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.credordevedor.Get_Pesquisa,Self.credordevedor.Get_TituloPesquisa,Fcredordevedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCREDORDEVEDOR);
     End;
end;
procedure TObjCREDITOvalores.EdtvaloresExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.valores.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.valores.tabelaparaobjeto;
End;
procedure TObjCREDITOvalores.EdtvaloresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.valores.Get_Pesquisa,Self.valores.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjCREDITOvalores.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCREDITOVALORES';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjCREDITOvalores.DebitaSaldoCredito: boolean;
begin
     result:=False;

     Self.credordevedor.LocalizaCodigo(Self.credordevedor.get_codigo);
     Self.credordevedor.TabelaparaObjeto;

     if (Self.credordevedor.Get_CampoCredito='')
     then Begin
               Mensagemerro('N�o foi configurado o campo de cr�dito para este credor/devedor');
               exit;
     End;

     With Self.Objquery do
     begin
          close;
          SQL.clear;
          sql.add('Select sum('+trim(Self.credordevedor.Get_CampoCredito)+') as soma from '+self.credordevedor.Get_Tabela);
          sql.add('where codigo='+self.codigocredordevedor);
          Try
             open;

             if (Fieldbyname('soma').AsCurrency<Strtocurr(self.valorusado))
             Then Begin
                       Mensagemerro('O saldo de cr�dito n�o � suficiente. Saldo atual '+formata_valor(Fieldbyname('soma').AsCurrency));
                       exit;
             End;
          except
                on e:exception do
                Begin
                     mensagemerro('N�o foi poss�vel somar o cr�dito do credor/devedor'+#13+E.message);
                     exit;
                End;
          End;

          close;
          SQL.clear;
          sql.add('Update '+self.credordevedor.Get_Tabela+' set '+trim(Self.credordevedor.Get_CampoCredito)+'='+trim(Self.credordevedor.Get_CampoCredito)+'-'+virgulaparaponto(Self.valorusado));
          sql.add('where codigo='+self.codigocredordevedor);
          Try
             open;
             result:=True;
          except
                on e:exception do
                Begin
                     mensagemerro('N�o foi poss�vel Debitar o cr�dito do credor/devedor'+#13+E.message);
                     exit;
                End;
          End;

     End;

end;

function TObjCREDITOvalores.CreditaSaldoCredito: boolean;
begin
     result:=False;

     Self.credordevedor.LocalizaCodigo(Self.credordevedor.get_codigo);
     Self.credordevedor.TabelaparaObjeto;

     if (Self.credordevedor.Get_CampoCredito='')
     then Begin
               Mensagemerro('N�o foi configurado o campo de cr�dito para este credor/devedor');
               exit;
     End;

     With Self.Objquery do
     begin
          close;
          SQL.clear;
          sql.add('Update '+self.credordevedor.Get_Tabela+' set '+trim(Self.credordevedor.Get_CampoCredito)+'='+trim(Self.credordevedor.Get_CampoCredito)+'+'+virgulaparaponto(Self.valorusado));
          sql.add('where codigo='+self.codigocredordevedor);
          Try
             open;
             result:=True;
          except
                on e:exception do
                Begin
                     mensagemerro('N�o foi poss�vel Aumentar o cr�dito do credor/devedor'+#13+E.message);
                     exit;
                End;
          End;

     End;

end;


end.



