object Fhistorico: TFhistorico
  Left = 207
  Top = 174
  Width = 436
  Height = 291
  Caption = 'Cadastro de Hist'#243'rico - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 3
    Top = 7
    Width = 424
    Height = 257
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'MS Sans Serif'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      object Label1: TLabel
        Left = 3
        Top = 14
        Width = 39
        Height = 14
        Caption = 'Codigo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 3
        Top = 56
        Width = 54
        Height = 14
        Caption = 'Descri'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 3
        Top = 99
        Width = 84
        Height = 14
        Caption = 'D'#233'bito&&Cr'#233'dito'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 3
        Top = 28
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object edtdescricao: TEdit
        Left = 3
        Top = 70
        Width = 350
        Height = 19
        MaxLength = 100
        TabOrder = 1
      end
      object combodebitocredito: TComboBox
        Left = 3
        Top = 113
        Width = 145
        Height = 21
        ItemHeight = 13
        TabOrder = 2
        Text = ' '
        Items.Strings = (
          'D'#233'bito'
          'Cr'#233'dito')
      end
      object BtCancelar: TBitBtn
        Left = 314
        Top = 151
        Width = 100
        Height = 38
        Caption = '&Cancelar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = BtCancelarClick
      end
      object Btgravar: TBitBtn
        Left = 210
        Top = 151
        Width = 100
        Height = 38
        Caption = '&Gravar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnClick = BtgravarClick
      end
      object btalterar: TBitBtn
        Left = 106
        Top = 151
        Width = 100
        Height = 38
        Caption = '&Alterar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        OnClick = btalterarClick
      end
      object BtNovo: TBitBtn
        Left = 2
        Top = 151
        Width = 100
        Height = 38
        Caption = '&Novo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        OnClick = BtNovoClick
      end
      object btpesquisar: TBitBtn
        Left = 2
        Top = 191
        Width = 100
        Height = 38
        Caption = '&Pesquisar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        OnClick = btpesquisarClick
      end
      object btrelatorios: TBitBtn
        Left = 106
        Top = 191
        Width = 100
        Height = 38
        Caption = '&Relat'#243'rios'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
      end
      object btexcluir: TBitBtn
        Left = 210
        Top = 191
        Width = 100
        Height = 38
        Caption = 'E&xcluir'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        OnClick = btexcluirClick
      end
      object btsair: TBitBtn
        Left = 314
        Top = 191
        Width = 100
        Height = 38
        Caption = '&Sair'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
        OnClick = btsairClick
      end
    end
  end
end
