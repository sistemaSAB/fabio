unit UObjLancamento;
Interface
Uses grids,rdprint,classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UOBjPendencia,UObjTipoLancto,
UobjExportaContabilidade,ibquery,uobjgeralancamento;

Type
  TObjLancamento=class

  Public
    //ObjDatasource                               :TDataSource;
    Status                                      :TDataSetState;
    SqlInicial                                  :String[200];
    Pendencia       :TobjPendencia;
    TipoLancto      :TobjTipoLancto;
    ObjExportaContabilidade:TobjExportaContabilidade;
    ObjGeraLancamento  :TObjGeraLancamento;
    NumeroRelatorio:string;
    ObjDataSourceLancto:TDataSource;
    ObjQueryLancto:TIBQuery;

    Constructor Create;
    Destructor  Free;
    Function    Salvar(ComCommit,ExportaContabilidade:Boolean;PCodContabilNGeraVl:string;VerificaSaldo:Boolean;LancaValoresquitacao:boolean):Boolean;overload;
    Function    Salvar(ComCommit,ExportaContabilidade:Boolean;PCodContabilNGeraVl:string;VerificaSaldo:Boolean;LancaValoresquitacao:boolean;out PcodigoLancamentoJurosAutomatico:string):Boolean;overload;
    Function    SalvarSemPendencia(ComCommit: boolean): Boolean;
    Function    LocalizaCodigo(Parametro:string) :boolean;
    Function    LocalizaPai(Parametro:string) :boolean;

    Function    ExcluiTitulo(Ptitulo:string;ComCommit:Boolean):Boolean;
    Function    ExcluiLancamentosPendencia(Ppendencia:string;ComCommit:Boolean):boolean;
    Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
    Function    Get_Pesquisa                    :string;
    Function    Get_TituloPesquisa              :string;
    Function    Get_PesquisaPendencia           :string;
    Function    Get_TituloPesquisaPendencia     :string;
    Function    Get_PesquisaTipoLancto          :string;
    Function    Get_TituloPesquisaTipoLancto    :string;
    Function    Get_PesquisaTipoLanctoNaoGeravalor:string;
    Function    Get_TituloPesquisaTalaodeCheques:string;

    Procedure   TabelaparaObjeto;
    Procedure   ZerarTabela;
    Procedure   Cancelar;
    Procedure   Commit;
    Procedure   Rolback;


    Function Get_CODIGO          :string;
    Function Get_Pendencia       :string;
    Function Get_TipoLancto      :string;
    Function Get_TipoLanctoNome  :string;
    Function Get_Valor           :string;
    Function Get_Historico       :String;
    Function Get_Data            :string;
    Function Get_lancamentoPai   :string;

    Procedure Submit_CODIGO          (parametro:string);
    Procedure Submit_Pendencia       (parametro:string);
    Procedure Submit_TipoLancto      (parametro:string);
    Procedure Submit_Valor           (parametro:string);
    Procedure Submit_Historico       (parametro:String);
    Procedure Submit_Data            (parametro:string);
    Procedure Submit_LancamentoPai   (parametro:string);

    Procedure Get_ListaTipoLancto    (ParametroL:Tstrings;ParametroPendencia:string);
    Procedure Get_ListaValor         (ParametroL:Tstrings;ParametroPendencia:string);
    Procedure Get_ListaHistorico     (ParametroL:Tstrings;ParametroPendencia:string);
    Procedure Get_ListaData          (ParametroL:Tstrings;ParametroPendencia:string);
    Function  Get_NovoCodigo:string;
    Function  Get_QuantRegs          (ParametroPendencia: string): INteger;
    procedure Get_ListaPortadores    (ParametroL: Tstrings;ParametroPendencia:string);

    procedure Imprime2(parametrocodigo:string);
    procedure ImprimeReciboMaster(parametrocodigo:string);
    //procedure ImprimeRecibo(Pcodigo,PcodigoJuros,PsaldoPendencia:string);
    procedure ImprimeRecibo(Pcodigo,PcodigoJuros,PsaldoPendencia:string; ptitulo : string = ''); //Rodolfo
    procedure ImprimeRecibo_Simples_TXT(pcodigo,pcodigojuros,PsaldoPendencia:string);
    procedure ImprimeComissaoProfessores(ParametroCodGerador,ParametroCodProfessor,ParametroValorComissao:string;Data1,Data2:string);
    //procedure ImprimeReciboPagamento(PcodigoLancamento:string);
    procedure ImprimeReciboPagamento(PcodigoLancamento:string; pTitulo : string = ''); //Rodolfo

    procedure ImprimeContaGerencial(); //Rodolfo
    function SaldoAnterior(pData : String) : Currency; //Rodolfo

    // F�bio 23/09/2009
    //procedure ImprimeReciboPersonalizadoReportBuilder(pcodigo:String);
    procedure ImprimeReciboPersonalizadoReportBuilder(pcodigo: String; pTitulo : string = ''); //Rodolfo

    procedure Imprime_Recebimento_por_data_portadornofinal;

    Function PegaNometipolancto(ParametroCodigo:string):string;
    Function PreencheChequeSemComprovante(out Par_NumChequeTalao:string;ParametroDescricao:string;out COMPCHEQUE:STRING): boolean;
    Function SomaporPai(ParametroPai:string):Currency;
    Function GravaTransferencia(out PnumeroTransf:string):Boolean;
    Function LancaValores(out PortadorRecebimento:string):boolean;
    Function EmiteChequeCaixa: boolean;

    procedure Get_ListaCodigo(ParametroL: Tstrings;ParametroPendencia: string);
    Function  RetornaRecebimento_lancamento(Plancamento:string;ComCOmmit:Boolean):boolean;
    Function  ApagaLancamento(Pcodigo:string;ComCommit:Boolean):boolean;

    Function  PagaPendencia(ExportaContabilidade:boolean):boolean;
    procedure Imprime(parametro: string);

    Function  NovoLancamento(Ppendencia:string;PPagarReceber:string;PValorLancamento:string;PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean):Boolean;overload;
    Function  NovoLancamento(Ppendencia:string;PPagarReceber:string;PValorLancamento:string;PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;PvalorMaior:currency):Boolean;overload;

    Function  NovoLancamento(Ppendencia: string;PPagarReceber:string;PValorLancamento:string;var PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;PcredorDevedorLote:string;PcodigoCredorDevedorLote:string;Pdata:string;PvalorMaior:currency):Boolean;overload;
    Function  NovoLancamento(Ppendencia: string;PPagarReceber:string;PValorLancamento:string;var PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;PcredorDevedorLote:string;PcodigoCredorDevedorLote:string;Pdata:string):Boolean;overload;

    Function  PreencheCheque(Pportador:string;PSaldo:string;PcredorDevedorLote,PcodigoCredorDevedorLote:string):Boolean;overload;
    Function  PreencheCheque(Pportador:string;PSaldo:string):Boolean;overload;

    Function  PreencheCheque:boolean;overload;

    Function  PreencheCheque(Pportador:string;PSaldo:string;PcredorDevedorLote,PcodigoCredorDevedorLote:string; auxLabel: String):Boolean; overload; {Rodolfo}


    Function  ExportaContabilidade_Recebimento:boolean;
    Function  ExportaContabilidade_Pagamento:boolean;

    Function  ExportaContabilidade_Pagamento_Lote(PlancamentoPai:string):boolean;
    function  ExportaContabilidade_Recebimento_Lote(PlancamentoPai: string): boolean;

    function  Retorna_contabilidade_pagamento_por_banco(Plancamento: string;Pbanco, PcodigoPlanoContas, Pvalor,Phistorico,PNumDcto: TStringList;PEmlote:boolean): boolean;
    function  Retorna_contabilidade_recebimento_por_banco(Plancamento: string;Pbanco, PcodigoPlanoContas, Pvalor,Phistorico: TStringList): boolean;

    Function  Retorna_Contabilidade_Pendencias_lote(Plancamento:string;PLancamentofilho,PcodigoPlanoContas, Pvalor,Phistorico: TStringList): boolean;


    Function ExcluiLancamento:Boolean;overload;
    Function ExcluiLancamento(Plancamento:string;ComCommit:boolean):boolean;overload;
    Function ExcluiLancamento(Plancamento:string;ComCommit:boolean;pperguntadesejaexcluirlote:boolean):boolean;overload;

    Procedure Abregaveta;
    Function  LancamentoAutomatico(Ptipo:string;Ppendencia:string;Pvalor:string;Pdata:string;Phistorico:String;var PCodigo:string;ComCommit:Boolean):Boolean;

    Function ExecutaSQL(Psql:String):boolean;
    Procedure RetornaLancamentoSemContabilidade(PdataInicial,PdataFinal:string;PGrid:TStringGrid);
    Function RetornaLancamentoBaixaBoletoemLote(Ppendencia:string;var PRegistroRetornoCobranca:string):integer;
    Procedure AlterarHistoricoLancamento;
    Function LancamentoPagamento(PPendencia: string): Boolean;
    procedure ImprimeReciboPagamento_Modelo3_DetalhamentoLancamento(PcodigoLancamento: string);
    procedure InicializaImpressoraNaoFiscal;
    procedure retornaLancto(Plancamento:string);

    function VerificaPermissaoDescontoAcrescimo(pLancamento:tobjlancamento;pTipo:string):boolean;

    Function LancaJuros_NovoObjeto(PPendencia:string;ValorJuroDesconto:Currency;PData:string;Plancamentopai:string;out PCodigolancamentoJuros:string;PExportaContabilidade:Boolean;Phistorico:String):boolean;
    Function LancaJuros(PPendencia:string;ValorJuroDesconto:Currency;PData:string;Plancamentopai:string;PlancaContaBilidade:Boolean;Phistorico:String):boolean;
    Function LancaDesconto_NovoObjeto(PPendencia:string;ValorJuroDesconto:Currency;PData:string;Plancamentopai:string;out PCodigolancamentoDesconto:string;PExportaContabilidade:Boolean;Phistorico:String):boolean;
    Function LancaDesconto(PPendencia: string; ValorJuroDesconto: Currency;PData: string; Plancamentopai: string;PExportaContabilidade:Boolean): boolean;

    procedure AtualizaTelaNovoLancamento(Ppendencia: string;PPagarReceber:string;PValorLancamento:string;var PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;PcredorDevedorLote:string;PcodigoCredorDevedorLote:string;Pdata:string;PvalorMaior:currency);

    function RetornaTitulolancamentoUnicoCredor( pLancamento:string ):string;
  Private
    ObjDataset:Tibdataset;
    ObjQueryLocal:tibquery;

    CODIGO          :string;
    Valor           :string;
    Historico       :String;
    Data            :string;
    LancamentoPai   :string;

    strLabel        :String; {Rodolfo}

    Function  VerificaBrancos:Boolean;
    Function  VerificaRelacionamentos:Boolean;
    Function  VerificaNumericos:Boolean;
    Function  VerificaData:Boolean;
    Function  VerificaFaixa:boolean;
    Procedure ObjetoparaTabela;
    Function  GeraTransferencia:boolean;//quando CP lan�o uma Transfer�ncia


    Procedure edttalaodechequesKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
    Procedure edttalaodechequesDblClick(Sender: TObject); // Rodolfo
    Procedure edtTalaodechequesKeyPress(Sender: TObject; var Key: Char);  //Rodolfo
    procedure buscaNumChequeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState); //Rodolfo
    Procedure buscaNumChequeDblClick(Sender: TObject); // Rodolfo

    Procedure edtPortadorKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);

    procedure edtPortadorComChequeKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState); //Rodolfo
    Procedure edtPortadorComChequeDblClick(Sender: TObject); //Rodolfo
    Procedure edtPortadorComChequeExit(Sender: TObject); //Rodolfo
    Procedure edtPortadorComChequeKeyPress(Sender: TObject; var Key: Char); //Rodolfo

    Procedure edtlancamentoKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);

    procedure edtValorChange(Sender: TObject); //Rodolfo

    Function  Get_PesquisaTalaodeChequesAbertos(Pportador:string):string;overload;
    Function  Get_PesquisaTalaodeChequesAbertos:string;overload;

    Procedure ImprimeLancamentos;
    function  AtualizaGerador: boolean;
    Function  ExportaContabilidade_NaoGeraValor(PCodContabilNGeraVl:string):Boolean;

    Function  PegaCodigoPlanodeContas:string;
    procedure EDTCODIGOPLANODECONTASKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    Function GravaRecebimento(ExportaContabilidade:boolean):boolean;
    Function GeraExclusaoContabilidade(Plancamento:string;ComCommit:Boolean):Boolean;

    {
    Function LancaJuros_NovoObjeto(PPendencia:string;ValorJuroDesconto:Currency;PData:string;Plancamentopai:string;out PCodigolancamentoJuros:string;PExportaContabilidade:Boolean;Phistorico:String):boolean;
    Function LancaJuros(PPendencia:string;ValorJuroDesconto:Currency;PData:string;Plancamentopai:string;PlancaContaBilidade:Boolean;Phistorico:String):boolean;
    Function LancaDesconto_NovoObjeto(PPendencia:string;ValorJuroDesconto:Currency;PData:string;Plancamentopai:string;out PCodigolancamentoDesconto:string;PExportaContabilidade:Boolean;Phistorico:String):boolean;
    Function LancaDesconto(PPendencia: string; ValorJuroDesconto: Currency;PData: string; Plancamentopai: string;PExportaContabilidade:Boolean): boolean;
    }

    function  ApagaRecebimento(PLancamento: string): boolean;
    function  ApagaPagamento(Plancamento: string): boolean;
    Procedure Imprime_Recebimento_Portador_Analitico;
    Procedure Imprime_Recebimento_Portador_Sintetico;
    function  ExcluiPendencias(Ptitulo: string; ComCommit: boolean): Boolean;

    Function  RetornaNf_HistoricoPendencia_Titulo(Pcodigolancamento:string):String;
    Procedure ImprimeTitulosPagos;
    Procedure ImprimeLancamentos_CredorDevedor;
    Procedure Imprime_ContasPagas_por_Dia;overload;
    procedure Imprime_ContasPagas_por_Dia(Ptipo:string);overload;
    PRocedure ImprimeReciboPagamento_Modelo1(PcodigoLancamento:string);
    procedure ImprimeReciboPagamento_Modelo2(PcodigoLancamento: string);
    //Procedure ImprimeReciboPagamento_ReportBuilder(PcodigoLancamento:String);
    procedure ImprimeReciboPagamento_ReportBuilder(PcodigoLancamento: String; pTitulo : string = ''); //Rodolfo
    Procedure Imprime_ContasaReceber_Juros;

    procedure ImprimeTitulosPagosEChequesACompensar;  //Rodolfo

  end;


implementation
uses stdctrls,SysUtils,Uobjvalores,Dialogs,UDatamodulo,
  Controls,Uvalores, Uobjtransferenciaportador,Uobjgeratransferencia,
  Utransferenciaportador,UopcaoRel,UfiltraImp,UobjcredorDevedor,
  Urelatorio,UobjTalaodeCheques,Uobjchequesportador,
  Upesquisa,Windows,Utalaodecheques,forms,UobjTitulo,UPLANODECONTAS,UobjcomprovanteCheque,
  UcomprovantepagamentoTMP,UobjConfRelatorio,Uobjlanctoportador,
  UReltxtRDPRINT, UObjValoresTransferenciaPortador,UMenuRelatorios, ULancaNovoLancamento,
  Urecibo, URelPedidoRdPrint, UObjContaGer, UMostraBarraProgresso,
  ExtCtrls,ACBrECFClass, UobjRELPERSREPORTBUILDER,
  Uportador, UobjPortador,ACBrECF; {Rodolfo}


Procedure  TObjLancamento.TabelaparaObjeto;//ok
begin
  with ObjDataset do
  begin
    Self.CODIGO           :=FieldByName('CODIGO').asstring;

    if (FieldByName('Pendencia').asstring<>'') then
    begin
      if (Self.Pendencia.LocalizaCodigo(FieldByName('Pendencia').asstring)=False) then
      begin
        Messagedlg('Pend�ncia Financeira N�o encontrada!',mterror,[mbok],0);
        Self.ZerarTabela;
        exit;
      end
      else Self.Pendencia.TabelaparaObjeto;
    end;

    if (Self.TipoLancto.LocalizaCodigo(FieldByName('TipoLancto').asstring)=False) then
    begin
      Messagedlg('Tipo de Lan�amento n�o encontrado!',mterror,[mbok],0);
      Self.ZerarTabela;
      exit;
    end
    else Self.tipolancto.TabelaparaObjeto;

    Self.Valor                      :=Fieldbyname('Valor').asstring;
    Self.Historico                  :=Fieldbyname('Historico').asstring;
    Self.Data                       :=Fieldbyname('Data').asstring;
    Self.LancamentoPai              :=Fieldbyname('LancamentoPai').asstring;

  end;
end;


procedure TObjLancamento.ObjetoparaTabela;//ok
begin
  with ObjDataset do
  begin
    FieldByName('CODIGO').asstring           :=        Self.CODIGO                    ;
    FieldByName('Pendencia').asstring        :=        Self.Pendencia.Get_Codigo      ;
    FieldByName('TipoLancto').asstring       :=        Self.TipoLancto.Get_codigo     ;
    FieldByName('Valor').asstring            :=        Self.Valor                     ;
    FieldByName('Historico').asstring        :=        Self.Historico                 ;
    FieldByName('Data').asstring             :=        Self.Data                      ;
    Fieldbyname('LancamentoPai').asstring    :=        Self.LancamentoPai             ;
  end;
end;

//***********************************************************************
function TObjLancamento.Salvar(ComCommit,ExportaContabilidade:boolean;PCodContabilNGeraVl:string;VerificaSaldo:Boolean;LancaValoresquitacao:boolean): Boolean;//Ok
var
  temp:string;
begin
  //nesse salvar nao se preocupa com o lancamento de juro automatico
  //que � aquele gerado em casos que o valor for maior
  Result:=Self.Salvar(ComCommit,ExportaContabilidade,PCodContabilNGeraVl,VerificaSaldo,LancaValoresquitacao,temp);
end;

function TObjLancamento.Salvar(ComCommit,ExportaContabilidade:boolean;PCodContabilNGeraVl:string;VerificaSaldo:Boolean;LancaValoresquitacao:boolean;out PcodigoLancamentoJurosAutomatico:string): Boolean;//Ok
var
  Resultado             :Boolean;
  PvalorLancamento,PValorPendencia:Currency;
  PCODIGO:string;
  PValor                :string;
  PHistorico            :String;
  PData                 :string;
  PLancamentoPai        :string;
  PPendencia            :string;
  PTipoLancto           :string;
  PComCommit,PExportaContabilidade:boolean;
  PPCodContabilNGeraVl:string;
  PVerificaSaldo:Boolean;
  ////AQUI Q ESTA O QUE PRECISO P FAZER A PARADA DE LAN�AR DESCONTOS AUTOMATICOS
begin

  Result:=False;

  if (Self.VerificaBrancos=True)
  Then Exit;

  if (Self.VerificaNumericos=False)
  Then Exit;


  if (Self.VerificaData=False)
  Then Exit;


  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;

  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
    if(Self.Status=dsedit)
    Then Begin
      Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
      result:=False;
      exit;
    End;
  End
  Else Begin
    if(Self.Status=dsinsert)
    Then Begin
      Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
      result:=False;
      exit;
    End;
  End;

  If (VerificaSaldo=True) and (Self.TipoLancto.Get_Classificacao='Q')
  Then Begin
    PCODIGO          :=Self.Codigo;
    PValor           :=Self.Valor;
    PHistorico       :=Self.HIstorico;
    PData            :=Self.Data;
    PLancamentoPai   :=Self.LancamentoPai;
    PPendencia       :=Self.Pendencia.Get_codigo;
    PTipoLancto      :=Self.TipoLancto.Get_codigo;
    PComCommit:=ComCommit;
    PExportaContabilidade:=ExportaContabilidade;
    PPCodContabilNGeraVl:=PCodContabilNGeraVl;
    PVerificaSaldo:=VerificaSaldo;
    //*************************
    Self.pendencia.LocalizaCodigo(self.pendencia.get_codigo);
    Self.pendencia.TabelaparaObjeto;
            
    PvalorLancamento:=strtofloat(Self.Valor);
    PvalorPendencia:=strtofloat(Self.pendencia.Get_Saldo);

    {
     Quando o valor do lancamento � maior que o saldo da pendencioa
     subtende-se que existe um juro
    }
    If (PvalorLancamento>PvalorPendencia)
    Then Begin
      if (Self.LancaJuros_NovoObjeto(PPendencia,(PvalorLancamento-PValorPendencia),PData,Self.LancamentoPai,PcodigoLancamentoJurosAutomatico,true,phistorico)=false)
      Then Begin
        Messagedlg('Erro na tentativa de lan�ar os juros!',mterror,[mbok],0);
        exit;
      End;
      //antes de eu chamar o lancajuros
      //eu tenho que salvar os dados, agora restauro estes dados
      Self.Codigo                   :=Self.get_novocodigo    ;
      Self.Valor                    :=PValor                 ;
      Self.HIstorico                :=PHistorico             ;
      Self.Data                     :=PData                  ;
      Self.LancamentoPai            :=PLancamentoPai         ;
      Self.Pendencia.Submit_codigo(PPendencia)               ;
      Self.TipoLancto.Submit_codigo(PTipoLancto)             ;
      ComCommit                     :=PComCommit             ;
      ExportaContabilidade          :=PExportaContabilidade  ;
      PCodContabilNGeraVl           :=PPCodContabilNGeraVl   ;
      VerificaSaldo                 :=PVerificaSaldo         ;
      //**************}
    End;

{
    //************alterado por celio 22/08/08*****
    //nao havia verificacao de desconto
    If (PvalorLancamento<PvalorPendencia)
    Then Begin
              if (Self.LancaDesconto_NovoObjeto(PPendencia,(PValorPendencia-PvalorLancamento),PData,Self.LancamentoPai,PcodigoLancamentoJurosAutomatico,true,phistorico)=false)
              Then Begin
                        Messagedlg('Erro na tentativa de lan�ar os juros!',mterror,[mbok],0);
                        exit;
              End;
              //antes de eu chamar o lancajuros
              //eu tenho que salvar os dados, agora restauro estes dados
              Self.Codigo                   :=Self.get_novocodigo    ;
              Self.Valor                    :=PValor                 ;
              Self.HIstorico                :=PHistorico             ;
              Self.Data                     :=PData                  ;
              Self.LancamentoPai            :=PLancamentoPai         ;
              Self.Pendencia.Submit_codigo(PPendencia)               ;
              Self.TipoLancto.Submit_codigo(PTipoLancto)             ;
              ComCommit                     :=PComCommit             ;
              ExportaContabilidade          :=PExportaContabilidade  ;
              PCodContabilNGeraVl           :=PPCodContabilNGeraVl   ;
              VerificaSaldo                 :=PVerificaSaldo         ;

    End;}


    //********************************************
  End;

  if Self.status=dsinsert
  then Self.ObjDataset.Insert//libera para insercao
  Else
    if (Self.Status=dsedit)
    Then Self.ObjDataset.edit//se for edicao libera para tal
    else Begin
      mensagemerro('Status Inv�lido na Grava��o');
      exit;
    End;

  Self.ObjetoParaTabela;

  Try
    Self.ObjDataset.Post;
  Except
    on e:exception do
    Begin
      if (ComCommit=True)
      Then Self.Rolback;

      Messagedlg('Erro na tentativa de Salvar o lan�amento'+#13+e.message,mterror,[mbok],0);
      result:=False;
      exit;
    End;
  End;
  
  //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  If (Self.TipoLancto.LocalizaCodigo(Self.TipoLancto.Get_codigo)=False)
  Then Begin
    Messagedlg('Tipo de Lan�amento N�o Localizado para Atualizar a Pend�ncia!',mterror,[mbok],0);
    if (ComCommit=True)
    Then Self.Rolback;
    result:=False;
    exit;
  End;

  Self.TipoLancto.TabelaparaObjeto;
  //Aumentando ou diminuindo o  Valor do Saldo
  //na pendencia selecionada, sem dar commit
  //de Acordo com o Tipo de Lancamento, se for um lan�amento de D�bito eu
  //Diminuo o Saldo da PEnd�ncia, se For um Lan�amento de Cr�dito
  //Eu aumento o Saldo da Pend�ncia
  If (Self.TipoLancto.Get_Tipo='D')
  Then Resultado:=Self.Pendencia.DiminuiSaldo(Self.Pendencia.Get_codigo,Self.Valor,False)
  Else Resultado:=Self.Pendencia.AumentaSaldo(Self.Pendencia.Get_codigo,Self.Valor,False);

  If (UPPERCASE(ExtractFileName(Application.exename))='ANALICE.EXE')
  Then Begin
    Resultado:=Self.AtualizaGerador;
    If (resultado=False)
    Then Begin
      Messagedlg('Gerador N�o Atualizado. Lan�amento de Pend�ncia Cancelado!',mterror,[mbok],0);
      if (ComCommit=True)
      Then Self.Rolback;
      result:=False;
      exit;
    End;
  End;

  If (resultado=False)
  Then Begin
    Messagedlg('Pend�ncia N�o Atualizada!',mterror,[mbok],0);
    if (ComCommit=True)
    Then Self.Rolback;
    result:=False;
    exit;
  End;
  
   //Se o tipo de lancto geravalor='S'(Exemplo=Quita��o) entao
   //tenho que lan�ar conforme o tipo, por exemplo se for um Contas
   //a Receber preciso fazer os lan�amento do que entrou
   //se for um a pagar preciso fazer o lan�amento do que vai sair
   //se � em cheque ou dinheiro, ou se cheque de 3�
  
  If Self.TipoLancto.Get_GeraValor='S'
  Then Begin
    If (LancaValoresquitacao=True)
    Then Begin
      Self.LocalizaCodigo(Self.codigo);
      Self.TabelaparaObjeto;

      if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C') then
      Begin//contas a receber gero Tabvalor
        If (Self.GravaRecebimento(ExportaContabilidade)=False)
        Then Begin
          Messagedlg('O Lan�amento N�o foi conclu�do. Verifique o Lan�amento dos Valores Recebidos!',mterror,[mbok],0);
          Result:=False;
          if (ComCommit=True)
          Then Self.Rolback;
          exit;
        End;
      End
      Else Begin//contas a pagar, gero transferencia OU ESCOLHO UM CHEQUE DO PORTADOR
        If (Self.PagaPendencia(exportacontabilidade)=False)
        Then Begin
          Messagedlg('O Pagamento n�o foi conclu�do!!',mterror,[mbok],0);
          result:=False;
          if (ComCommit=True)
          Then Self.Rolback;
          exit;
        End;
      End;
    End;
  End
  Else Begin
    //Lan�amento que n�o geram valor no sistema, ou seja
    //nao geram pendencias em outras tabelas nem creditam
    //nem debitam saldo do banco
    //s�o lancamento de juros e descontos
    //estes lancamentos tem que ser exportados para a contabilidade
    //tendo o fornecedor/cliente e a conta para serem exportados
    If (ExportaContabilidade=True)
    Then Begin
      If (self.ExportaContabilidade_NaoGeraValor(PCodContabilNGeraVl)=False)
      Then Begin
        Messagedlg('O lan�amento n�o foi conclu�do devido erro na exporta��o da Contabilidade!',mterror,[mbok],0);
        result:=False;
        if (ComCommit=True)
        Then Self.Rolback;
        exit;
      End;
    End;

   End;
  //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
  If ComCommit=True
  Then FDataModulo.IBTransaction.CommitRetaining;

  Self.status          :=dsInactive;
  result:=True;
End;

function TObjLancamento.SalvarSemPendencia(ComCommit:boolean): Boolean;//Ok
var
Resultado:Boolean;
begin

  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
    if(Self.Status=dsedit)
    Then Begin
      Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
      result:=False;
      exit;
    End;
  End
  Else Begin
    if(Self.Status=dsinsert)
    Then Begin
      Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
      result:=False;
      exit;
    End;
  End;


  if Self.status=dsinsert then
    Self.ObjDataset.Insert//libera para insercao
  Else
    if (Self.Status=dsedit) then
      Self.ObjDataset.edit//se for edicao libera para tal
    else
    Begin
      mensagemerro('Status Inv�lido na Grava��o');
      exit;
    End;

  Self.ObjetoParaTabela;
  Self.ObjDataset.Post;

  If ComCommit=True
  Then FDataModulo.IBTransaction.CommitRetaining;

  Self.status          :=dsInactive;
  result:=True;

End;


procedure TObjLancamento.ZerarTabela;//Ok
Begin
  With Self do
  Begin
    CODIGO           :='';
    Pendencia.ZerarTabela;
    TipoLancto.ZerarTabela;
    Valor            :='';
    Historico        :='';
    Data             :='';
    LancamentoPai    :='';
  End;
end;

Function TObjLancamento.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
    If (codigo='')
    Then Mensagem:=mensagem+'/C�digo';

    If (Pendencia.Get_codigo='')
    Then Mensagem:=mensagem+'/Pend�ncia';

    If (TipoLancto.Get_codigo='')
    Then Mensagem:=mensagem+'/Tipo de Lan�amento';


    If (Valor='')
    Then Mensagem:=mensagem+'/Valor';

    If (Historico='')
    Then Mensagem:=mensagem+'/Hist�rico';

    If (data='')
    Then Mensagem:=mensagem+'/Data';

  End;

  if mensagem <>''
  Then Begin //mostra mensagem de erro caso existam cpos requeridos em branco
    messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
    result:=true;
    exit;
  End;
   result:=false;
end;


function TObjLancamento.VerificaRelacionamentos: Boolean;
var
  mensagem:string;
Begin
  mensagem:='';
  If (Self.Pendencia.LocalizaCodigo(Self.Pendencia.Get_CODIGO)=False)
  Then Mensagem:=mensagem+'/ Pend�ncia n�o Encontrada!';

  If (Self.TipoLancto.LocalizaCodigo(Self.TipoLancto.Get_CODIGO)=False)
  Then Mensagem:=mensagem+'/ Tipo de Lan�amento n�o Encontrado!'
  Else Self.TipoLancto.TabelaparaObjeto;

  IF (Self.lancamentopai<>'')
  Then Begin
    If (Self.LocalizaCodigo(Self.LancamentoPai)=False)
    Then Mensagem:=mensagem+'LancamentoPai n�o encontrado!';
  End;

  If (mensagem<>'')
  Then Begin
    Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
    result:=False;
    exit;
  End;
  result:=true;
End;

function TObjLancamento.VerificaNumericos: Boolean;
var
  Inteiros:Integer;
  Reais:Currency;
  Mensagem:string;
begin
  Mensagem:='';

  try
    Inteiros:=Strtoint(Self.codigo);
  Except
    Mensagem:=mensagem+'/C�digo';
  End;

  try
    Inteiros:=Strtoint(Self.Pendencia.Get_codigo);
  Except
    Mensagem:=mensagem+'/Pend�ncia';
  End;

  try
    Inteiros:=Strtoint(Self.TipoLancto.Get_codigo);
  Except
    Mensagem:=mensagem+'/Tipo de Lan�amento';
  End;

  try
    Reais:=StrtoFLOAT(Self.Valor);
    if (Reais<0)
    Then Mensagem:='/O Valor n�o pode ser menor que zero';
  Except
    Mensagem:=mensagem+'/Valor';
  End;

  If Mensagem<>''
  Then Begin
    Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
    result:=false;
    exit;
  End;
  result:=true;

end;

function TObjLancamento.VerificaData: Boolean;
var
  Datas:Tdate;
  Horas:Ttime;
  Mensagem:string;
begin
  mensagem:='';

  Try
    Datas:=strtodate(Self.data);
  Except
    Mensagem:=mensagem+'/Data';
  End;


  If Mensagem<>''
  Then Begin
    Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
    result:=false;
    exit;
  End;
  result:=true;
end;

function TObjLancamento.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

  Try
    Mensagem:='';

    If mensagem<>''
    Then Begin
      Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
      result:=false;
      exit;
    End;

    result:=true;
  Finally
  end;
end;

function TObjLancamento.LocalizaCodigo(parametro: string): boolean;//ok
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSql.Clear;
    SelectSql.add('Select CODIGO,Pendencia,TipoLancto,Valor,Historico,Data,lancamentopai');
    SelectSql.add('from TabLancamento where codigo='+Parametro);
    Open;
    If (recordcount>0)
    Then Result:=True
    Else Result:=False;
  End;
end;

function TObjLancamento.LocalizaPai(Parametro: string): boolean;
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSql.Clear;
    SelectSql.add('Select CODIGO,Pendencia,TipoLancto,Valor,Historico,Data,lancamentopai');
    SelectSql.add('from TabLancamento where lancamentopai='+Parametro);
    Open;
    If (recordcount>0)
    Then Result:=True
    Else Result:=False;
  End;
end;


procedure TObjLancamento.Cancelar;
begin
  FDataModulo.IBTransaction.rollbackretaining;
  Self.status:=dsInactive;
end;

Function TObjLancamento.Exclui(pcodigo: string;ComCommit:boolean): Boolean;
begin

  Try
    result:=true;
    Self.ZerarTabela;
    If (Self.LocalizaCodigo(Pcodigo)=True)
    Then Begin
      Self.TabelaparaObjeto;
      if (Self.Pendencia.Get_CODIGO<>'')
      Then Begin
        if (self.TipoLancto.Get_Tipo='D')
        Then Self.Pendencia.AumentaSaldo(Self.Pendencia.Get_CODIGO,Self.Valor,False)
        Else Begin
          if (Self.Pendencia.DiminuiSaldo(Self.Pendencia.Get_CODIGO,Self.Valor,False)=False)
          Then Begin
            result:=False;
            exit;
          End;
        End;
      End;
      //verifica o objeto gerador e codgerador
      //se encontrar lancar o inverso
      If (Self.ObjExportaContabilidade.LocalizaGerador('OBJLANCAMENTO',PCODIGO)=tRUE)
      Then Begin
        Self.ObjExportaContabilidade.TabelaparaObjeto;
        if (Self.ObjExportaContabilidade.Get_Exportado='N')
        Then Begin
          If (Self.ObjExportaContabilidade.excluiporgerador('OBJLANCAMENTO',PCODIGO)=False)
          Then Begin
            Messagedlg('Erro na exclus�o da Contabilidade!',mterror,[mbok],0);
            result:=False;
            exit;
          End;
        End
        Else Begin//ja foi exportado, entao gero o inverso
          If (Self.GeraExclusaoContabilidade(pcodigo,comcommit)=False)
          Then Begin
            Messagedlg('Erro no Lan�amento da Contabilidade, o Lan�amento n�o foi exclu�do!',mterror,[mbok],0);
            result:=False;
            exit;
          End;
        End;
      End;
      Self.ObjDataset.delete;
      If (Comcommit=True)
      Then FDataModulo.IBTransaction.CommitRetaining;
      //ShowMessage();
    End
    Else result:=false;
  Except
    on E:exception do
    Begin
      result:=false;
      MensagemErro(e.message);
    End;
  End;
end;


constructor TObjLancamento.create;
begin
  Self.ObjDataset:=Tibdataset.create(nil);
  Self.ObjDataset.Database:=FDataModulo.IbDatabase;

  Self.ObjDataSourceLancto:=TDataSource.Create(nil);


  Self.Pendencia:=TObjPendencia.Create;
  Self.TipoLancto:=TObjTipoLancto.Create;
  Self.ObjExportaContabilidade:=TobjExportaContabilidade.create;
  //Self.Objparametros:=Tobjparametros.create;
  Self.ObjQueryLocal:=tibquery.create(nil);
  Self.ObjQueryLocal.Database:=FDataModulo.IbDatabase;

  Self.ObjQueryLancto:=tibquery.create(nil);
  Self.ObjQueryLancto.Database:=FDataModulo.IbDatabase;

  Self.ObjDataSourceLancto.DataSet:=ObjQueryLancto;

  Self.ObjGeraLancamento  :=TObjGeraLancamento.create;
  ZerarTabela;

  With Self.ObjDataset do
  Begin
    SelectSQL.clear;
    SelectSql.add('Select CODIGO,Pendencia,TipoLancto,Valor,Historico,Data,lancamentopai');
    SelectSql.add('from TabLancamento where codigo=0');

    Self.SqlInicial:=SelectSQL.text;

    InsertSQL.clear;
    InsertSQL.add(' Insert Into TabLancamento (CODIGO,Pendencia,TipoLancto,Valor,Historico,Data,lancamentopai)');
    InsertSql.Add('  values (:CODIGO,:Pendencia,:TipoLancto,:Valor,:Historico,:Data,:lancamentopai)');

    ModifySQL.clear;
    ModifySQL.add('Update Tablancamento set CODIGO=:CODIGO,Pendencia=:Pendencia,TipoLancto=:TipoLancto,Valor=:Valor,Historico=:Historico,Data=:Data,lancamentopai=:lancamentopai where codigo=:codigo');


    DeleteSQL.clear;
    DeleteSQL.add('Delete from tablancamento where codigo=:codigo');

    RefreshSQL.clear;
    RefreshSQL.add('Select CODIGO,Pendencia,TipoLancto,Valor,Historico,Data,lancamentopai');
    RefreshSQL.add('from TabLancamento where codigo=0');

    open;

    Self.ObjDataset.First ;
    Self.status          :=dsInactive;
  End;
end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjLancamento.Get_CODIGO: string;
begin
  Result:=Self.Codigo;
end;

procedure TObjLancamento.Submit_CODIGO(parametro: string);
begin
  Self.Codigo:=parametro;
end;

procedure TObjLancamento.Commit;
begin
  FDataModulo.IBTransaction.CommitRetaining;
end;

Procedure TObjLancamento.Rolback;
Begin
  FDataModulo.IBTransaction.RollbackRetaining;
End;

function TObjLancamento.Get_Pesquisa: string;
begin
  Result:=' Select * from TabLancamento ';
end;

function TObjLancamento.Get_TituloPesquisa: string;
begin
  Result:=' Pesquisa de Lan�amento em Pend�ncias Financeiras ';
end;

function TObjLancamento.Get_Data: string;
begin
  Result:=Self.data;
end;

function TObjLancamento.Get_Historico: String;
begin
  Result:=Self.Historico;
end;

function TObjLancamento.Get_Pendencia: string;
begin
  Result:=Self.Pendencia.get_codigo;
end;

function TObjLancamento.Get_TipoLancto: string;
begin
  Result:=Self.TipoLancto.get_codigo;
end;

function TObjLancamento.Get_Valor: string;
begin
  Result:=Self.Valor;
end;

procedure TObjLancamento.Submit_Data(parametro: string);
begin
  Self.data:=parametro;
end;

procedure TObjLancamento.Submit_Historico(parametro: String);
begin
  Self.Historico:=parametro;
end;

procedure TObjLancamento.Submit_Pendencia(parametro: string);
begin
  Self.pendencia.Submit_CODIGO(parametro);
end;

procedure TObjLancamento.Submit_TipoLancto(parametro: string);
begin
  Self.TipoLancto.Submit_CODigo(parametro);
end;

procedure TObjLancamento.Submit_Valor(parametro: string);
begin
  Self.Valor:=Parametro;
end;

function TObjLancamento.Get_PesquisaPendencia: string;
begin
  Result:=Self.Pendencia.Get_Pesquisa;
end;

function TObjLancamento.Get_PesquisaTipoLancto: string;
begin
  Result:=Self.TipoLancto.Get_Pesquisa;
end;

function TObjLancamento.Get_TipoLanctoNome: string;
begin
  Result:=Self.TipoLancto.Get_nome;
end;

function TObjLancamento.Get_TituloPesquisaPendencia: string;
begin
  Result:=Self.Pendencia.Get_TituloPesquisa;
end;

function TObjLancamento.Get_TituloPesquisaTipoLancto: string;
begin
  Result:=Self.TipoLancto.Get_TituloPesquisa;
end;


procedure TObjLancamento.Get_ListaData(ParametroL: Tstrings;
  ParametroPendencia: string);
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add(' Select data from TabLancamento where Pendencia='+ParametroPendencia+'  order by Codigo');
    open;
    first;
    ParametroL.Clear;
    ParametroL.add('*DATA*');

    While not(eof) do
    Begin
      ParametroL.add(fieldbyname('Data').asstring);
      next;
    End;
    close;
  End;
End;

procedure TObjLancamento.Get_ListaHistorico(ParametroL: Tstrings;
  ParametroPendencia: string);
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add(' Select historico from TabLancamento where Pendencia='+ParametroPendencia+'  order by Codigo');
    open;
    first;
    ParametroL.Clear;
    ParametroL.add('*HIST�RICO*');

    While not(eof) do
    Begin
      ParametroL.add(fieldbyname('Historico').asstring);
      next;
    End;
    close;
  End;
end;

procedure TObjLancamento.Get_ListaTipoLancto(ParametroL: Tstrings;
  ParametroPendencia: string);
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add(' Select TabLancamento.TipoLancto,TabtipoLancto.nome from TabLancamento left outer join TabTipoLancto on ');
    SelectSQL.add(' Tablancamento.TipoLancto=TabTipoLancto.codigo  where TabLancamento.Pendencia='+ParametroPendencia+'  order by TabLancamento.Codigo');
    open;
    first;
    ParametroL.Clear;
    ParametroL.add('*TIPO DE LAN�AMENTO*');

    While not(eof) do
    Begin
      ParametroL.add(fieldbyname('TipoLancto').asstring+'*-*'+fieldbyname('nome').asstring);
      next;
    End;
    close;
  End;
end;

procedure TObjLancamento.Get_ListaCodigo(ParametroL: Tstrings;
  ParametroPendencia: string);
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add(' Select TabLancamento.codigo from TabLancamento ');
    SelectSQL.add(' where TabLancamento.Pendencia='+ParametroPendencia+'  order by TabLancamento.Codigo');
    open;
    first;
    ParametroL.Clear;
    ParametroL.add('*C�DIGO*');

    While not(eof) do
    Begin
      ParametroL.add(fieldbyname('CODIGO').asstring);
      next;
    End;
    close;
  End;
end;

procedure TObjLancamento.Get_ListaPortadores(ParametroL: Tstrings;
  ParametroPendencia: string);
var
  Qlocal:tibquery;
  portadores,Plancamento:string;
begin
  try
    Qlocal:=tibquery.create(nil);
    Qlocal.Database:=ObjDataset.Database;
  except
    mensagemerro('Erro ao criar query tempor�ria');
    exit;
  end;

  try

    With Self.ObjDataset do
    Begin
      close;
      SelectSQL.clear;
      SelectSQL.add(' Select TabLancamento.codigo from TabLancamento ');
      SelectSQL.add(' where TabLancamento.Pendencia='+ParametroPendencia+'  order by TabLancamento.Codigo');
      open;
      first;
      ParametroL.Clear;
      ParametroL.add('*PORTADORES*');

      While not(eof) do
      Begin
        Plancamento:=fieldbyname('CODIGO').asstring;
        Qlocal.Close;
        Qlocal.SQL.Clear;
        Qlocal.SQL.Add('select distinct(TABLANCTOPORTADOR.portador) as portador from TABLANCAMENTO');
        Qlocal.SQL.Add('join TABLANCTOPORTADOR on TABLANCTOPORTADOR.lancamento=tablancamento.codigo');
        Qlocal.SQL.Add('where tablancamento.codigo='+Plancamento+' order by TABLANCTOPORTADOR.portador');
        Qlocal.Open;
        Qlocal.First;
        portadores:=Qlocal.fieldbyname('portador').asstring;
        Qlocal.Next;
        While not(Qlocal.Eof) do
        begin
          portadores:=portadores+'/'+Qlocal.fieldbyname('portador').asstring;
          Qlocal.Next;
        end;
        ParametroL.add(portadores);
        next;
      End;
      close;
    End;
  finally
    FreeAndNil(Qlocal); //25/06/2011 celio nao tinha free
  end;
end;


procedure TObjLancamento.Get_ListaValor(ParametroL: Tstrings;
  ParametroPendencia: string);
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add(' Select Valor from TabLancamento where Pendencia='+ParametroPendencia+'  order by Codigo');
    open;
    first;
    ParametroL.Clear;
    ParametroL.add('*VALOR*');

    While not(eof) do
    Begin
      ParametroL.add(formata_valor(fieldbyname('Valor').asstring));
      next;
    End;
    close;
  End;
end;

function TObjLancamento.Get_QuantRegs(ParametroPendencia: string): INteger;
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add(' Select count(codigo) as CNT from TabLancamento where Pendencia='+ParametroPendencia);
    open;
    first;
    Result:=Fieldbyname('CNT').asinteger;
    close;
  End;
end;

//Chama o Form de Valores e  verifica quando ele
//volta se os valores estao fechando
//Este procedimento � usado quando estou com
//uma conta a receber e preciso lan�ar os valores
//que entraram

function TObjLancamento.GravaRecebimento(ExportaContabilidade:boolean): boolean;
var
  ObjValores:TobjValores;
  Saida:Boolean;
  ValorObj,ValorRetorno:Currency;
  FValores:TFValores;
begin
  Try
    FValores:=TFValores.create(nil);
    Try
      //Crio um Objeto para depois verificar
      //se retornou os valores corretos
      ObjValores:=TobjValores.create;
      saida:=false;
      //Preenche os valores
      //para o form de Valores
      //Quando tenho que chamar um form
      //que tenha que prencher alguns edits
      //entao uso um registro fora do objeto para
      //a transferencia dos dados
      LimpaRegistroValores;//Limpo este registro
      //Preencho os Valores do registro
      RegistroValores.Historico:=Self.Historico;
      {mando apenas o valor que restou, pois posso
      utilizar esta funcao no recebimento em lote, desta forma
      chamo ele apois ter gravado um det. valor em dinheir}
      RegistroValores.Valor:=Self.valor;
      RegistroValores.Lancamento:=Self.codigo;
      RegistroValores.lancamentoexterno:=True;
      //**********************
      Repeat
        //Chamo o Form
        FValores.Showmodal;
        //Na hora que volta do form, chama-se o Objeto Valore
        //e passo o codigo de lan�amento de parametro para fazer um select e retorna
        //a soma do que foi lan�ado

        ValorRetorno:=0;
        ValorRetorno:=ObjValores.Get_SomaValores(Self.CODIGO);
        ValorObj:=StrTofloat(Self.valor);

        If (ValorRetorno<>ValorObj)
        Then Begin
          If (Messagedlg('Os Valores N�o Fecham com O lan�amento no Valor de R$ '+Self.Valor+#13+' Deseja voltar para Corrigir?',mtconfirmation,[mbyes,mbno],0)=MrYes)
          Then saida:=False
          Else Begin
            //ele n�o volta para o form mas
            //nao conclui o lancamento
            //pois o result=false

            saida:=True;
            Result:=False;
          End;
        End
        Else Begin
           //Os valore fecharam com o lan�amento
           //entao retorno=true
          saida:=True;
          result:=True;
          //Self.ImprimeReciboPequeno(Self.Codigo);
        End;

      Until(saida=True);

      //Desativando o campo do registro para evitar
      //que quando o form de lan�amento foi chamdo
      //do menu principal nao esteja com os edits
      //preenchidos
      RegistroValores.lancamentoexterno:=False;
      LimpaRegistroValores;
    Except
      result:=False;
    End;
  Finally
    ObjValores.free;
    Freeandnil(FValores);

    If (result=True) and (ExportaContabilidade=True)//Tenho que exportar para a contabilidade o que aconteceu
    Then Result:=Self.ExportaContabilidade_Recebimento;
  End;
End;


function TObjLancamento.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
  Try
    Try
      StrTemp:=TIBStoredProc.create(nil);
      StrTemp.database:=ObjDataset.Database;
      StrTemp.StoredProcName:='PROC_GERA_LANCAMENTO';
      StrTemp.ExecProc;
      Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
    Except
      Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Lan�amento',mterror,[mbok],0);
      result:='0';
      exit;
    End;
  Finally
    FreeandNil(StrTemp);
  End;

end;


//Gera transferencia quando for contas a pagar
//Neste caso s� � usado para transferencia em dinheiro e cheque de 3�
Function TObjLancamento.GeraTransferencia: boolean;
var
  TempObjTransferenciaPortador:TObjTransferenciaPortador;
  SomadosLancamentos:Currency;
  ValorTmp:Currency;
  Ftransferenciaportador:TFtransferenciaportador;
  TempObjgeraTransferencia:TObjGeraTransferencia;
begin
  result:=False;

  try
    TempObjgeraTransferencia:=TObjGeraTransferencia.create;
  Except
    Messagedlg('Erro na Tentativa de gerar o Objeto GeraTransfer�ncia',mterror,[mbok],0);
    exit;
  End;

  Try
    TempObjTransferenciaPortador:=TObjTransferenciaPortador.create;
  Except
    Messagedlg('Erro Na Cria��o do Objeto de Transfer�ncia de valores entre Portadores',mterror,[mbok],0);
    result:=False;
    exit;
  End;


  Try

    If (TempObjgeraTransferencia.LocalizaHistorico('PAGAMENTO DE CONTAS COM DINHEIRO')=False)
    Then Begin
      Messagedlg('O gerador de Transfer�ncia "PAGAMENTO DE CONTAS COM DINHEIRO" n�o foi encontrado',mterror,[mbok],0);
      exit;
    End;

    TempObjgeraTransferencia.TabelaparaObjeto;

    Ftransferenciaportador:=TFtransferenciaportador.create(nil);
        
    Uobjtransferenciaportador.RegTransferenciaPortador.Origem:='OBJLANCAMENTO';
    Uobjtransferenciaportador.RegTransferenciaPortador.CodigoLancamento:=Self.codigo;
    Uobjtransferenciaportador.RegTransferenciaPortador.PortadorDestino:=TempObjgeraTransferencia.Get_Portadordestino;
    Uobjtransferenciaportador.RegTransferenciaPortador.Valor:=Self.Valor;
    Uobjtransferenciaportador.RegTransferenciaPortador.Data:=Self.Data;
    Uobjtransferenciaportador.RegTransferenciaPortador.Historico:=Self.Historico;

    Ftransferenciaportador.showmodal;

    Uobjtransferenciaportador.RegTransferenciaPortador.Origem:='';
    Uobjtransferenciaportador.RegTransferenciaPortador.CodigoLancamento:='';
    Uobjtransferenciaportador.RegTransferenciaPortador.PortadorDestino:='';
    Uobjtransferenciaportador.RegTransferenciaPortador.Valor:='';
    Uobjtransferenciaportador.RegTransferenciaPortador.Data:='';
    Uobjtransferenciaportador.RegTransferenciaPortador.Historico:='';

  Finally
    Freeandnil(Ftransferenciaportador);
    TempObjgeraTransferencia.free;
    TempObjTransferenciaPortador.free;
  End;
end;

procedure TObjLancamento.Imprime(parametro: string);
begin
  with FmenuRelatorios do
  begin
    //era no titulo antes
    NomeObjeto:='UOBJTITULO';

    with RgOpcoes do
    begin
      items.clear;
      items.add('T�tulos a Pagar com Saldo(Anteriores a uma Data)');//0
      items.add('T�tulos a Pagar com Saldo(Intervalo de refer�ncia)');//1
      items.add('T�tulos a Receber com Saldo(Anteriores a uma Data)');//2
      items.add('T�tulos a Receber com Saldo(Intervalo de refer�ncia)');//3
      items.add('Previs�o Financeira');//4
      items.add('T�tulos por N�mero');//5
      items.add('T�tulos Lan�ados');//6
      items.add('Rela��o D�bitos/Cr�ditos por Credor/Devedor sem t�tulos Gerados pelo Saldo ');//7
      items.add('T�tulos a Pagar com Saldo (Anteriores a uma Data) 02');//8
      items.add('T�tulos a Receber com Saldo (Anteriores a uma Data) 02');//9
      items.add('Lan�amentos efetuados em T�tulos');//10
      items.add('DESATIVADO');
      items.add('Resumo por Conta Gerencial de Lan�amentos(quita��o,desconto...) ');//12
      items.add('Resumo por Conta Gerencial de T�tulos Lan�ados');//13
      items.add('T�tulos Lan�ados separados por Conta Gerencial sem "T�tulos gerados pelo Saldo de outros T�tulos"');//14
      items.add('T�tulos Lan�ados separados por Conta Gerencial com "T�tulos gerados pelo Saldo de outros T�tulos"');//15
      items.add('DESATIVADO');//16

      items.add('DESATIVADO');
      items.add('DESATIVADO');
      items.add('DESATIVADO');
      items.add('DESATIVADO');
      items.add('DESATIVADO');
      items.add('DESATIVADO');
                
      items.add('Ficha Financeira');//23
      items.add('T�tulos a Receber com Saldo 02 (incluso fone)');//24
      items.add('Total Pago por Conta Gerencial e Fornecedor/Cliente (contas a pagar)');//25
      items.add('Total Recebido por Conta Gerencial e Fornecedor/Cliente (contas a receber)');//26
      items.add('Contas a Receber e a pagar por Credor/Devedor');//27
      items.add('Saldo Total por Credor/Devedor');//28
      items.add('T�tulos a Receber com Saldo (imprime cidade)');//29
      items.add('T�tulos a Pagar por Intervalo (com Fornecedor impresso)');//30
      items.add('T�tulos  a pagar   (total por dia)');//31
      items.add('T�tulos  a receber (total por dia)');//32
      items.add('T�tulos  a receber (totalizado por Cidade e dia)');//33
      items.add('Recebimentos por portador Sint�tico');//34
      items.add('Recebimentos por portador Anal�tico');//35
      items.add('Total a pagar (T�tulos e Cheques a descontar) por dia');//36
      items.add('T�tulos a Receber separados por Conta Gerencial');//37
      items.add('T�tulos a Pagar separados por Conta Gerencial');//38
      items.add('Titulos Pagos');//39
      items.add('Titulos a pagar (Excluir Conta Gerencial)');//40
      items.add('Titulos a receber (Excluir Conta Gerencial)');//41
      items.add('Cheque por Cliente e Portador');//42
      items.add('Parcelas por Vencimento e Conta gerencial (com e sem saldo)');//43
      items.add('Lan�amentos por Credor/Devedor');//44
      items.add('Contas a Receber separado por Cadastro - com hist�rico');//45
      items.add('Contas a Pagar separado por Cadastro - com hist�rico');//46
      items.add('T�tulos a Receber com Cheques por Portador');//47
      items.add('T�tulos  a pagar   (total por dia) - com nome do Credor/Devedor');//48
      items.add('T�tulos  a receber (total por dia) - com nome do Credor/Devedor');//49
      items.add('Total pago por dia com descontos e juros');//50
      items.add('Total pago por Conta Gerencial e Sub-Conta Gerencial');//51
      items.add('Total recebido por Conta Gerencial e Sub-Conta Gerencial');//52
      items.add('Total pago por Conta Gerencial e Sub-Conta Gerencial com data de Lancamento');//53
      items.add('Total recebido por Conta Gerencial e Sub-Conta Gerencial com data de Lancamento');//54


      items.add('T�tulos a Receber (Filtro por Conta Cont�bil)');//55
      items.add('T�tulos a Pagar (Filtro por Conta Cont�bil)');//56
      items.add('Total Recebido por dia com descontos e juros');//57

      items.add('T�tulos a Receber com Juros por Cliente'); //58
      //Items.Add('RESUMO DE NFS.FATURA/CUPONS FISCAIS');

      Items.Add('Conta Gerencial'); //59   -   Rodolfo
      Items.Add('Pend�ncias Pagas e Cheques a Compensar'); //60   -   Rodolfo
    end;

    showmodal;

    Self.Pendencia.Titulo.NumeroRelatorio:=IntToStr(RgOpcoes.ItemIndex+1)+' - ';
    Self.Pendencia.NumeroRelatorio:=IntToStr(RgOpcoes.ItemIndex+1)+' - ';
    Self.NumeroRelatorio:=IntToStr(RgOpcoes.ItemIndex+1)+' - ';


    if (Tag=0)//indica botao cancel ou fechar
    then exit;

    case RgOpcoes.ItemIndex of

      00:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso('D',false);
      01:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso('D',true);
      02:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso('C',false);
      03:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso('C',True);
      04:Self.Pendencia.Titulo.ImprimePrevisaoFinanceira;
      05:Self.Pendencia.Titulo.ImprimeTitulosPorNumero;
      06:Self.Pendencia.Titulo.ImprimetitulosLancados;
      07:Self.Pendencia.Titulo.ImprimeRelacaoDebitoCreditoFornecedor('N');
      08:Self.Pendencia.Titulo.ImprimeTitulosEmAtrasoDOIS('D');
      09:Self.Pendencia.Titulo.ImprimeTitulosEmAtrasoDOIS('C');
      10:Self.Pendencia.Titulo.ImprimeLancamentosTitulo(parametro);
      11:MensagemAviso('Relat�rio Desativado');//Self.Pendencia.Titulo.ImprimetitulosLancados_com_Impostos;
      12:Self.Pendencia.Titulo.ImprimeResumoContaGerencialLancamentos;
      13:Self.Pendencia.Titulo.ImprimeResumoContaGerencialTitulos; // FFFF
      14:Self.Pendencia.Titulo.ImprimetitulosLancados_por_ContaGerencial(False);
      15:Self.Pendencia.Titulo.ImprimetitulosLancados_por_ContaGerencial(True);
      16:MensagemAviso('Relat�rio Desativado');
      17:MensagemAviso('Relat�rio Desativado');
      18:MensagemAviso('Relat�rio Desativado');
      19:MensagemAviso('Relat�rio Desativado');
      20:MensagemAviso('Relat�rio Desativado');
      21:MensagemAviso('Relat�rio Desativado');
      22:MensagemAviso('Relat�rio Desativado');
      23:Self.Pendencia.Titulo.ImprimeFichaFinanceira;

      24:Self.Pendencia.Titulo.ImprimeTitulosEmAtrasoDOISCOMFONES('C');
      25:Self.Pendencia.Titulo.ImprimeTotalporContaGerencial('D');
      26:Self.Pendencia.Titulo.ImprimeTotalporContaGerencial('C');
      27:Self.Pendencia.Titulo.ImprimeRelacaoDebitoCreditoFornecedor('');
      28:Self.Pendencia.Titulo.ImprimeSaldoTotalCredorDevedor;
      29:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso_NomeCidade('C',True);
      30:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso_COMFORNECEDOR;
      31:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso_somadoporDia('D',True,false);
      32:Self.Pendencia.Titulo.ImprimeTitulosEmAtraso_somadoporDia('C',True,false);
      33:Self.Pendencia.Titulo.Imprime_Titulos_Em_Atraso_somado_por_Cidade_e_Dia('C',True);
      34:Self.Imprime_Recebimento_Portador_Sintetico;
      35:Self.Imprime_Recebimento_Portador_Analitico;
      36:Self.pendencia.titulo.Imprime_Pagar_por_Dia_com_Cheque;

      37:Self.Pendencia.Imprime_Titulos_por_ContaGerencial('C');
      38:Self.Pendencia.Imprime_Titulos_por_ContaGerencial('D');
      39:Self.ImprimeTitulosPagos;
      40:Self.Pendencia.Imprime_titulos_Aberto_ExcluirCG('D');
      41:Self.Pendencia.Imprime_titulos_Aberto_ExcluirCG('C');
      42:Self.Pendencia.titulo.Imprime_Cheques_Clientes_portadores;
      43:Self.Pendencia.Imprime_Pendencia_por_vencimento_ContaGerencial;
      44:Self.ImprimeLancamentos_CredorDevedor;
      45:Self.Pendencia.Titulo.Imprime_TotalaReceber_Pagar_PorFornecedor('C');
      46:Self.Pendencia.Titulo.Imprime_TotalaReceber_Pagar_PorFornecedor('D');
      47:Self.pendencia.titulo.Imprime_Contas_a_Receber_e_Cheques_por_portador;
      48:Self.pendencia.titulo.ImprimeTitulosEmAtraso_somadoporDia('D',True,True);

      49:Self.pendencia.titulo.ImprimeTitulosEmAtraso_somadoporDia('C',True,True);
      50:Self.Imprime_ContasPagas_por_Dia;

      51:
      Begin
        FOpcaorel.RgOpcoes.items.clear;
        FOpcaorel.RgOpcoes.items.add('Anal�tico por T�tulo');
        FOpcaorel.RgOpcoes.items.add('Agrupado  por Cliente/Fornecedor');
        FOpcaorel.RgOpcoes.items.add('Sint�tico');
        FOpcaorel.showmodal;

        if (FOpcaorel.tag=0)
        then exit;

        case FOpcaorel.rgopcoes.itemindex of
          0:Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubConta('D');
          1:Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubConta_agrupado_credordevedor('D');
          2:Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubConta_Sintetico('D');
        end;
      end;

      52:
      begin
        FOpcaorel.RgOpcoes.items.clear;
        FOpcaorel.RgOpcoes.items.add('Anal�tico por T�tulo');
        FOpcaorel.RgOpcoes.items.add('Agrupado  por Cliente/Fornecedor');
        FOpcaorel.RgOpcoes.items.add('Sint�tico');
        FOpcaorel.showmodal;

        if (FOpcaorel.tag=0)
        then exit;

        case FOpcaorel.rgopcoes.itemindex of
          0:Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubConta('C');
          1:Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubConta_agrupado_credordevedor('C');
          2:Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubConta_Sintetico('C');
        end;
      end;

      53: Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubContaComDataDeLancamento('D');
      54: Self.pendencia.titulo.ImprimeTotalporContaGerencialeSubContaComDataDeLancamento('C');
      55: Self.pendencia.Imprime_Titulos_por_ContaContabil('C');

      56:
      begin
        Self.NumeroRelatorio:=IntToStr(RgOpcoes.ItemIndex+2)+' - ';
        Self.pendencia.Imprime_Titulos_por_ContaContabil('D');
      end;

      57:Self.Imprime_ContasPagas_por_Dia('C');
      58:Self.Imprime_ContasaReceber_Juros;

      59:Self.ImprimeContaGerencial(); //Rodolfo
      60:Self.ImprimeTitulosPagosEChequesACompensar(); //Rodolfo
    end;
  end;
end;

procedure TObjLancamento.Imprime2(parametrocodigo: string);
begin
     With FMenuRelatorios do
     Begin
          NomeObjeto:='UOBJLANCAMENTO';
               With RgOpcoes do
                Begin
                     items.clear;
                     items.add('Recibo de Lan�amento');//0
                     items.add('Lan�amentos por Data');//1
                End;
          showmodal;
          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of

                0:Self.ImprimeReciboMaster(parametrocodigo);
                1:Self.ImprimeLancamentos;
          End;

     end;
end;


procedure TObjLancamento.ImprimeRecibo(Pcodigo,PcodigoJuros,PsaldoPendencia:string; ptitulo : string = '');
begin
  if (ObjParametroGlobal.ValidaParametro('MODELO DE RECIBO DE LAN�AMENTO')=False) then
  begin
    Messagedlg('O par�metro "MODELO DE RECIBO DE LAN�AMENTO" n�o foi encontrado!',mterror,[mbok],0);
    exit;
  end;

  {if ((pcodigojuros<>'') and (PsaldoPendencia <> ''))
  then Begin}
    if (ObjParametroGlobal.Get_Valor='SIMPLES EM TXT') then
      Self.imprimerecibo_Simples_TXT(pcodigo,pcodigojuros,PsaldoPendencia);
  //end;

  if (ObjParametroGlobal.Get_Valor='MEIA FOLHA A4') then
    Self.ImprimeReciboPagamento(pcodigo);

  if (ObjParametroGlobal.Get_Valor='PERSONALIZADO REPORTBUILDER') then
    Self.ImprimeReciboPersonalizadoReportBuilder(pcodigo, ptitulo); //Adicionado ptitulo para casos de lote, cujo credor/devedor � unico
end;


procedure TObjLancamento.ImprimeRecibomaster(parametrocodigo: string);
var
Tdia,Tmes,Tano:word;
Codaluno:Integer;
TempGerador:String[100];
TempCredorDevedor:String[09];
TempCodigoCredorDevedor:String[09];
Tempobjcredordevedor:Tobjcredordevedor;
TempSqlCredorDevedor:String;
FreciboX:TFrecibo;
Begin

    Try
       If (Self.Localizacodigo(parametrocodigo)=True)
       Then Self.TabelaparaObjeto;
    Except
          exit;
    End;



    Try
       TempOBjCredorDevedor:=Tobjcredordevedor.create;
    Except
          Messagedlg('Erro ao criar o Objeto de Credor/Devedor para pesquisa da origem(Titulo) do Lan�amento',mterror,[mbok],0);
          exit;
    End;


Try

       FreciboX:=TFrecibo.create(nil);
       Uessencialglobal.limpaedit(FreciboX);

   //Preciso localizar o Titulo que se refere este lancamento;
   //com o titulo eu sei o gerador, e o credor/devedor
   //recupero o codigo do credor/devedor
   //busco no objeto temporario, assim eu tenho a sql e o campo a ser buscado
   //que vai no recibo como recebi do sr....

   TempGerador:=Self.Pendencia.Get_GeradorTitulo;
   TempCredorDevedor:=Self.Pendencia.Get_CredorDevedorTitulo;
   TempCodigoCredorDevedor:=Self.Pendencia. Get_CodigoCredorDevedorTitulo;

   try
      If (Tempobjcredordevedor.LocalizaCodigo(TempCredorDevedor)=False)
      Then Begin
                Messagedlg('O Credor/Devedor n�o foi localizado!',mterror,[mbok],0);
                exit;
           End;
      Tempobjcredordevedor.TabelaparaObjeto;
      TempSqlCredorDevedor:='';
      TempSqlCredorDevedor:=Tempobjcredordevedor.Get_InstrucaoSQL+' where codigo='+TempCodigoCredorDevedor;
      //Ja tem o Sql pronto agora vou pegar o nome do Credor/Devedor
      Self.ObjDataset.close;
      Self.ObjDataset.SelectSQL.clear;
      Self.ObjDataset.SelectSql.add(TempSqlCredorDevedor);
      Self.ObjDataset.open;
      //Preenchedo o Edit de Recebos do Recibo
      FreciboX.edtrecebemos.text:=Self.ObjDataset.Fieldbyname(Tempobjcredordevedor.Get_CampoNome).asstring;
      FreciboX.edtcorrespondente1.text:=TempGerador;
      Self.ObjDataset.close;
      Self.LocalizaCodigo(Self.codigo);

   Finally
          Tempobjcredordevedor.free;
   End;



   FreciboX.EDTVALOR.text:=Self.Valor;
   FreciboX.edtcorrespondente2.text:=Self.Pendencia.Get_Historico;

   FreciboX.edtcidade.text:='ITAPOR�';
   DecodeDate(now,Tano,Tmes,Tdia);
   FreciboX.edtdia.text:=Inttostr(Tdia);
   FreciboX.edtmes.text:=Uessencialglobal.MesExtenso(Tmes);
   FreciboX.edtano.text:=Inttostr(Tano);
   FreciboX.showmodal;

Finally
            Freeandnil(FreciboX);
End;

end;

procedure TObjLancamento.ImprimeComissaoProfessores(ParametroCodGerador,
  ParametroCodProfessor,ParametroValorComissao: string;Data1,Data2:string);
begin

{

Seleciona os dados dos lancamentos e a comissao calculada do professor
********So que os Lancamentos tem que ser filtrados atraves das matriculas
********que tem que ser fitradas pelo curso.....
********O view foi criado para minizar o SQL, o ViewTituloLancamento
********seleciona o titulo,gerador e dos dados da pendencia no lancamento

select ViewtituloLancamento.*,(ViewtituloLancamento.valor*Comissao)/100 as ValorComissao from ViewtituloLancamento left join tabtipolancto on
ViewtituloLancamento.tipolancto=Tabtipolancto.codigo where
/*Aqui Indica o tipo de lancto que e valido*/
TabtipoLancto.geravalor='S' and TabtipoLancto.tipo='D'
/*Aqui Indica qual o Gerador dos Titulo no nosso caso o de mensalidade*/
and  ViewtituloLancamento.gerador=XXX
/*Aqui escolhe a data dos lancamentos
and (ViewtituloLancamento.Data>=XXXXX and ViewtituloLancamento.Data<=XXXX)
/*Aqui Escolhe Todos da Mensalidade do Professor escolhido*/
and  ViewtituloLancamento.codigogerador in

/*Este Select seleciona todas as matriculas que estejam em turmas do professor escolhido*/
(select tabalunoturma.codigo from tabalunoturma left join tabturmas
on Tabalunoturma.codturma=tabturmas.codigo left join Tabprofessores
on tabturmas.codprofessor=tabprofessores.codigo
where Tabprofessores.codigo=XXX and Tabturmas.ativo='S')

 }
     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('select ViewtituloLancamento.*,((ViewtituloLancamento.valor*'+ParametroValorComissao+')/100) as ValorComissao ');
          SelectSql.add('from ViewtituloLancamento left join tabtipolancto on');
          SelectSql.add('ViewtituloLancamento.tipolancto=Tabtipolancto.codigo where');
          SelectSql.add('TabtipoLancto.geravalor=''S'' and TabtipoLancto.tipo=''D'' ');
          SelectSql.add('and  ViewtituloLancamento.gerador='+ParametroCodGerador);
          SelectSql.add('and  (ViewtituloLancamento.Data>='+#39+formatDatetime('mm/dd/yyyy',strtodate(Data1))+#39+' and  ViewtituloLancamento.Data<='+#39+formatDatetime('mm/dd/yyyy',strtodate(Data2))+#39+') ');
          SelectSql.add('and  ViewtituloLancamento.codigogerador in');

          //Aqui  Eu Seleciona as matriculas que tenha o professor escolhido
          SelectSql.add('(select tabalunoturma.codigo from tabalunoturma left join tabturmas');
          SelectSql.add('on Tabalunoturma.codturma=tabturmas.codigo left join Tabprofessores');
          SelectSql.add('on tabturmas.codprofessor=tabprofessores.codigo');
          SelectSql.add('where Tabprofessores.codigo='+ParametroCodProfessor+' and Tabturmas.ativo=''S'' )');
          //**********************//
          SelectSql.add(' order by ViewtituloLancamento.data');
          //Showmessage(SelectSql.text);
          open;

     End;
     //aqui eu envio para o relat�rio de Lan�amentos

     Frelatorio.QR.DataSet:=Self.ObjDataset;

          //configurando os campos

     With Frelatorio do
     Begin
          Frelatorio.desativacampos;
          Frelatorio.titulo.caption:='RELAT�RIO DE COMISS�O DO PROFESSOR -> '+ParametroCodProfessor+' - de '+DATA1+' a '+data2 ;

          ColTitulo01.Enabled:=True;
          ColTitulo01.Caption:='T�TULO';
          Campo01.Enabled:=True;
          Campo01.DataSet:=Self.ObjDataset;
          Campo01.DataField:='Titulo';

          ColTitulo02.Enabled:=True;
          ColTitulo02.Caption:='HIST�RICO LAN�AMENTO';
          Campo02.Enabled:=True;
          Campo02.DataSet:=Self.ObjDataset;
          Campo02.DataField:='Historico';


          ColTitulo05.Enabled:=True;
          ColTitulo05.Caption:='DATA';
          Campo05.Enabled:=True;
          Campo05.DataSet:=Self.ObjDataset;
          Campo05.DataField:='data';

          ColTitulo06.Enabled:=True;
          ColTitulo06.Caption:='TIPO LANCTO';
          Campo06.Enabled:=True;
          Campo06.DataSet:=Self.ObjDataset;
          Campo06.DataField:='tipolancto';

          ColTitulo07.Enabled:=True;
          ColTitulo07.Caption:='VALOR';
          Campo07.Enabled:=True;
          Campo07.mask:='0.00';
          Campo07.DataSet:=Self.ObjDataset;
          Campo07.DataField:='VALOR';
          Campo07.Alignment:=taRightJustify;
          Campo07.Left:=Campo07.left+15;
          


          ColTitulo08.Enabled:=True;
          ColTitulo08.Caption:='COMISS�O';
          Campo08.Enabled:=True;
          Campo08.DataSet:=Self.ObjDataset;
          Campo08.mask:='0.00';
          Campo08.DataField:='valorcomissao';

          CampoExpr01.enabled:=True;
          CampoExpr01.Left:=Campo07.left;
          CampoExpr01.mask:='0.00';
          CampoExpr01.Expression:='sum(Valor)';

          CampoExpr02.enabled:=True;
          CampoExpr02.Left:=Campo08.left;
          CampoExpr02.mask:='0.00';
          CampoExpr02.Expression:='sum(Valorcomissao)';

          QR.preview;

          Campo07.Alignment:=taLeftJustify;
          Campo07.Left:=Campo07.left-15;

     End;

end;


Function TObjLancamento.PagaPendencia(ExportaContabilidade:boolean): boolean;
var
ValorLancamento:Currency;
TempObjTransferenciaPortador:TObjTransferenciaPortador;
begin
     Try
        TempObjTransferenciaPortador:=TObjTransferenciaPortador.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o do Objeto de Transfer�ncia!',mterror,[mbok],0);
           result:=False;                                                                    
           exit;
     End;
Try

    Repeat
          //Escolhendo enTre o Pagamento com Transfer�ncia ou Cheque
          With FopcaoRel do
          Begin
              With RgOpcoes do
              Begin
                      items.clear;
                      items.add('Pagamento com Transfer�ncia (Cheques de 3� e Dinheiro)');//0
                      items.add('Cheque');
              End;
              BtCancelar.Caption:='Sair';
              showmodal;

              If (Tag=0)
              Then Begin
                      //Pego o Valor do Lan�amento
                      ValorLancamento:=0;
                      ValorLancamento:=Strtofloat(Self.Valor);
                      //Pego o Valor das Transferencias onde o Lan�amento Tenha Sido este e verifico com o valor do lan�amento
                      If (TempObjTransferenciaPortador.get_somaporlancamento(Self.codigo)<>ValorLancamento)
                      Then Begin
                                Messagedlg('Os Valores de Transfer�ncia e Cheques n�o conferem com o Lan�amento!',mterror,[mbok],0);
                                result:=False;
                      End
                      Else result:=True;
                      exit;
              End;

              Case RgOpcoes.ItemIndex of
                      0:Begin
                                Result:=Self.GeraTransferencia;//Gerando uma transferencia em dinheiro e cheques de 3�
                                tag:=1;
                      End;
                      1:Begin
                                Result:=Self.PreencheCheque;
                                tag:=1;
                      End;
              End;
              //Pego o Valor do Lan�amento
              ValorLancamento:=0;
              ValorLancamento:=Strtofloat(Self.Valor);
              //Pego o Valor das Transferencias onde o Lan�amento Tenha Sido este e verifico com o valor do lan�amento
              If (TempObjTransferenciaPortador.get_somaporlancamento(Self.codigo)<>ValorLancamento)
              Then tag:=1
              Else Begin
                      result:=True;
                      exit;
              End;
          End;
    Until(FopcaoRel.Tag=0);//O Tag Retorna Zero quando concluo os lan�amentos

    //Voltando o Caption do Bot�o
    FopcaoRel.BtCancelar.Caption:='Cancelar';

Finally
            TempObjTransferenciaPortador.free;

            If (Result=True) and (ExportaContabilidade=True)
            Then Result:=Self.ExportaContabilidade_Pagamento;
End;

end;


Function TObjLancamento.PreencheChequeSemComprovante(out Par_NumChequeTalao:string;ParametroDescricao:string;out COMPCHEQUE:STRING): boolean;
var
  TempObjTalaodecheques:tobjtalaodecheques;
  TempObjChequePortador:tobjChequesportador;
  TempObjTransferenciaPortador:TObjTransferenciaPortador;
  TempObjGeraTransferencia:TObjGeraTransferencia;
  TempObjComprovanteCheque:TObjComprovanteCheque;
  TempTStrings:TstringList;
  saida:Boolean;
  TempCodigoChequeTalao,TempCodigoChequePortador:string;
  ident:byte;
  vencimentocheque:Tdate;
  ValorCheque,Somaporlancamento:Currency;
  FcomprovantepagamentoTmp:TFcomprovantepagamentoTmp;
  Pcidade:String;
begin
Try

        Pcidade:='';
        Pcidade:='Dourados';
        if (ObjParametroGlobal.ValidaParametro('CIDADE A SER IMPRESSA NO CHEQUE')=true)
        Then Begin
                 
                 Pcidade:=Objparametroglobal.Get_Valor;
        End;

        FcomprovantepagamentoTmp:=TFcomprovantepagamentoTmp.create(nil);
      //Procedimento usado no lancamento de pagamento e lote
     //escolho um cheque e gravo com o valor da pendencia
     //atual
     //Tenho que escolher o cheque e o valor que vai ser preenchido
     //Preencho com valor 0, crio um registro em ChequesPortador
     //com valor 0 para n�o influenciar no saldo, depois altero
     //o mesmo com outro valor, pois na altera��o n�o atualiza o saldo
     //depois disto faco uma transferencia do portador do cheque para ele mesmo
     //assim eu tenho um titulo comprovando o pagamento da divida, quando o cheque
     //cair no banco eu transfiro do meu portador para o portador de contas pagas
     //ou de cheques do portador
     //******************************

    try
       ident:=0;
       TempObjTalaodeCheques:=Tobjtalaodecheques.create;
       ident:=1;
       TempObjChequePortador:=Tobjchequesportador.create;
       ident:=2;
       TempObjTransferenciaPortador:=TObjTransferenciaPortador.create;
       ident:=3;
       TempObjgeraTransferencia:=TObjgeraTransferencia.create;
       ident:=4;
       TempTStrings:=TStringList.create;
       ident:=5;
       TempObjComprovanteCheque:=TObjComprovanteCheque.create;

    Except
          If (ident=0)
          Then Messagedlg('N�o foi poss�vel gerar o Objeto de Tal�o de Cheques',mterror,[mbok],0);
          If (ident=1)
          Then Messagedlg('N�o foi poss�vel gerar o Objeto de ChequePortador',mterror,[mbok],0);
          If (ident=2)
          Then Messagedlg('N�o foi poss�vel gerar o Objeto de Transfer�ncia de Portador',mterror,[mbok],0);
          if (ident=3)
          then Messagedlg('Erro Na Cria��o do Objeto de Gera��o de Transfer�ncia para Lan�amento da Transfer�ncia',mterror,[mbok],0);
          if (ident=4)
          then Messagedlg('Erro na tentativa de Cria��o da TStrings para enviar o C�digo do Cheque para Transfer�ncia',mterror,[mbok],0);

          exit;
    End;

    //Encontrando o cheque no talao de cheques
    Try
       limpaedit(Ffiltroimp);
       With Ffiltroimp do
       Begin
                DesativaGrupos;
                Grupo01.Enabled:=True;
                Grupo02.Enabled:=True;

                edtgrupo01.color:=$005CADFE; //laranja
                edtgrupo01.EditMask:='';
                edtgrupo02.EditMask:='!99/99/9999;1;_';
                edtgrupo02.text:=datetostr(now);

                lbGrupo01.Caption:='Cheque';
                lbGrupo02.Caption:='Vencimento';
                lbGrupo03.Caption:='Valor R$';
                

                Repeat

                       saida:=False;
                       edtgrupo01.OnKeyDown:=Self.edttalaodechequesKeyDown;
                       showmodal;

                       If tag=0
                       Then Begin
                                result:=False;
                                exit;
                            End;
                       Saida:=true;

                       Try
                          ValorCheque:=0;
                          ValorCheque:=Strtofloat(Self.Valor);
                       Except
                             Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                             saida:=False;
                       End;

                       Try
                          If (Saida=True)//passou pela verificacao do valor
                          Then Begin
                                        VencimentoCheque:=Strtodate(edtgrupo02.text);

                                        If (TempObjTalaodecheques.LocalizaCodigo(edtgrupo01.text)=False)
                                        Then Begin
                                                Messagedlg('Cheque n�o localizado na Tabela de Tal�o de Cheques',mterror,[mbok],0);
                                                saida:=False;
                                            End
                                        Else Begin
                                                TempObjTalaodecheques.TabelaparaObjeto;
                                                TempCodigoChequeTalao:=TempObjTalaodecheques.Get_CODIGO;
                                                Par_NumChequeTalao:=TempCodigoChequeTalao;
                                                If TempObjTalaodecheques.Get_usado='S'
                                                Then Begin
                                                        Messagedlg('Cheque J� Usado',mterror,[mbok],0);
                                                        saida:=False;
                                                End
                                                Else saida:=True;
                                             End;
                               End;
                       Except
                             saida:=False;
                       End;
                Until(saida=True);
       End;

       //ja tenho o cheque agora tenho que gerar um cheque na TabChequePortador
       //depois localiza-lo e trocar o valor
       //Vou lanca-lo com valor zero, depois vou localiza-lo e alterar o valor
       //porque isso?? Porque quando eu insiro � alterado o valor da Tabchequesportador
       //e quando eu edito n�o

       //Gerando na TabChequesPortador e guardando o codigo gerado
       TempObjChequePortador.status:=dsinsert;
       TempCodigoChequePortador:=TempObjChequePortador.Get_NovoCodigo;
       TempObjChequePortador.Submit_CODIGO(TempCodigoChequePortador);
       TempObjChequePortador.Submit_Portador(TempObjTalaodecheques.Get_Portador);
       TempObjChequePortador.Submit_NumCheque(TempObjTalaodecheques.Get_Numero);
       TempObjChequePortador.Submit_Agencia(TempObjTalaodecheques.Portador.Get_Agencia);
       TempObjChequePortador.Submit_Conta(TempObjTalaodecheques.Portador.Get_NumeroConta);
       TempObjChequePortador.Submit_Valor('0');
       TempObjChequePortador.Submit_Vencimento(datetostr(Vencimentocheque));
       TempObjChequePortador.Submit_Chequedoportador('S');

       If TempObjChequePortador.Salvar(False,False,False,'')=False
       Then Begin
                 Messagedlg('N�o foi poss�vel Gerar o Cheque na TabChequeportador!',mterror,[mbok],0);
                 result:=False;
                 exit;
            End;

       //Agora Altero o mesmo com o valor correto
       If (TempObjChequePortador.LocalizaCodigo(TempCodigoChequePortador)=False)
       Then Begin
                 Messagedlg('Ap�s Salvar o ChequePortador criado atrav�s da Tal�o de Cheque, n�o foi poss�vel localizado para alterar o valor!',mterror,[mbok],0);
                 result:=False;
                 exit;
            End
       Else TempObjChequePortador.TabelaparaObjeto;

       TempObjChequePortador.status:=dsedit;
       TempObjChequePortador.Submit_Valor(FloatToStr(ValorCheque));

       If TempObjChequePortador.Salvar(False,False,false,'')=False
       Then Begin
                 Messagedlg('N�o foi poss�vel Alterar o Cheque na TabChequeportador!',mterror,[mbok],0);
                 result:=False;
                 exit;
            End;

       //NO TALAO DE CHEQUES altero o campo usado do cheque para S e guardo o codigo da chequeportador
       If (TempObjTalaodecheques.LocalizaCodigo(TempObjTalaodecheques.get_codigo)=False)
       Then Begin
                Messagedlg('Cheque n�o localizado na Tabela de Tal�o de Cheques para atualizar o campo USADO para ''S'' ',mterror,[mbok],0);
                saida:=False;
                exit;
            End;

       TempObjTalaodecheques.TabelaparaObjeto;
       TempObjTalaodecheques.Status:=dsedit;
       TempObjTalaodecheques.Submit_USado('S');
       TempObjTalaodecheques.Submit_CodigoChequePortador(TempCodigoChequePortador);
       TempObjTalaodecheques.Submit_Valor(FloattoStr(ValorCheque));
       TempObjTalaodecheques.Submit_Vencimento(datetostr(Vencimentocheque));
       TempObjTalaodecheques.Submit_HistoricoPagamento(Self.Historico+' CH'+TempObjTalaodecheques.Get_Numero);

       If (TempObjTalaodecheques.Salvar(false)=False)
       Then Begin
                 Messagedlg('A Opera��o de Salvar as altera��es no campo USADO e CodigoChequePortador da tabela de Tal�o de Cheques n�o foi bem sucedida!',mterror,[mbok],0);
                 result:=False;
                 exit;
            End;
      {Lanco uma transferencia deste cheque do seu portador para ele mesmo}

      //Preciso saber qual o historico e o grupo para a transferencia
      //criar campos na tabgeratransferencia e localizar pelo historico
      If (TempObjGeraTransferencia.LocalizaHistorico('CHEQUE DO PORTADOR PAGANDO CONTAS')=False)
      Then Begin
                Messagedlg('N�o foi encontrado o gerador de Transfer�ncia com Hist�rico->#CHEQUE DO PORTADOR PAGANDO CONTAS#',mterror,[mbok],0);
                result:=False;
                exit;
           End;

       TempObjGeraTransferencia.TabelaparaObjeto;
       {Uobjtransferenciaportador.RegTransferenciaPortador.Origem:='OBJLANCAMENTO';
       Uobjtransferenciaportador.RegTransferenciaPortador.CodigoLancamento:=Self.codigo;
       Uobjtransferenciaportador.RegTransferenciaPortador.PortadorDestino:=TempObjTalaodecheques.Get_Portador;
       //USado para a LanctoPortador gerar o historico do que aconteceu
       Uobjtransferenciaportador.RegTransferenciaPortador.Historico:='Cheque '+TempObjTalaodecheques.Get_Numero+' Emitido-'+Self.Get_Historico;}

       TempObjTransferenciaPortador.status:=dsinsert;
       TempObjTransferenciaPortador.Submit_CODIGO(TempObjTransferenciaPortador.Get_NovoCodigo);
       TempObjTransferenciaPortador.Submit_Data(Self.Data);
       TempObjTransferenciaPortador.Submit_Valor('0');
       TempObjTransferenciaPortador.Submit_Documento('0');
       TempObjTransferenciaPortador.Submit_Complemento('0');
       TempObjTransferenciaPortador.Submit_Grupo(TempObjGeraTransferencia.Get_Grupo);
       TempObjTransferenciaPortador.Submit_PortadorOrigem(TempObjTalaodecheques.Get_Portador);
       TempObjTransferenciaPortador.Submit_PortadorDestino(TempObjTalaodecheques.Get_Portador);
       TempObjTransferenciaPortador.CodigoLancamento.Submit_codigo(Self.codigo);
       //Na transferencia se envia um Tstrings com a lista de cheques
       //no meu caso tenho que mandar apenas uma string
       //para isto tenho que criar uma Tstrings s� para ele

       TempTStrings.clear;
       TempTStrings.add(TempCodigoChequePortador);
       TempObjTransferenciaPortador.Submit_ListaChequesTransferencia(TempTStrings);
       IF(TempObjTransferenciaPortador.Salvar(False,False,False,False,False)=False)
       Then Begin
                 result:=False;
                 exit;
            End;

       //AQUI COMECA A VERIFICACAO DO COMPROVANTE E DA IMPRESSAO DO CHEQUE

       Self.TabelaparaObjeto;
       //Preenchendo o Comprovante
       //FcomprovantepagamentoTmp.showmodal;
       //Pegando os dados do form
       TempObjComprovanteCheque.Submit_CODIGO(TempObjComprovanteCheque.Get_NovoCodigo);

       limpaedit(FcomprovantepagamentoTMP);
       FcomprovantepagamentoTmp.edtpagoa.text:='VARIOS TITULOS';
       FcomprovantepagamentoTmp.edtnominador.text:='';
       FcomprovantepagamentoTmp.edtdescricao.text:=Self.Get_Historico;
       FcomprovantepagamentoTmp.edtcomprovante.text:=TempObjComprovanteCheque.Get_CODIGO;
       FcomprovantepagamentoTmp.edtprovidencias.text:='Contabilizar e Arquivar';
       FcomprovantepagamentoTmp.edtcidade.text:=Pcidade;
       FcomprovantepagamentoTmp.edtdata.text:=Datetostr(now);
       FcomprovantepagamentoTmp.edtnomerecebedor.text:='';
       FcomprovantepagamentoTmp.edtcpfcnpjrecebedor.text:='';

       If (ObjParametroGlobal.ValidaParametro('PREENCHE COMPROVANTE CHEQUE?')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "PREENCHE COMPROVANTE CHEQUE?" "VALOR=SIM ou N�O" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;
       

       If (ObjParametroGlobal.Get_Valor='SIM')
       Then FcomprovantepagamentoTmp.showmodal;

       //Apos voltar do form envia os dados do form para o objeto

       TempObjComprovanteCheque.Status:=Dsinsert;

       TempObjComprovanteCheque.Submit_Cheque(TempCodigoChequePortador);
       TempObjComprovanteCheque.Submit_PagoA(FcomprovantepagamentoTmp.edtpagoa.text);
       TempObjComprovanteCheque.Submit_Nominador(FcomprovantepagamentoTmp.edtnominador.text);
       TempObjComprovanteCheque.Submit_descricaorecibo(FcomprovantepagamentoTmp.edtdescricao.text);
       TempObjComprovanteCheque.Submit_descricaocomprovante('CONFORME ANEXO');
       TempObjComprovanteCheque.Submit_Comprovante(FcomprovantepagamentoTmp.edtcomprovante.text);//uso o mesmo do primario
       TempObjComprovanteCheque.Submit_Providencias(FcomprovantepagamentoTmp.edtprovidencias.text);
       TempObjComprovanteCheque.Submit_Cidade(FcomprovantepagamentoTmp.edtcidade.text);
       TempObjComprovanteCheque.Submit_Data(FcomprovantepagamentoTmp.edtdata.text);
       TempObjComprovanteCheque.Submit_NomeRecebedor(FcomprovantepagamentoTmp.edtnomerecebedor.text);
       TempObjComprovanteCheque.Submit_CPFCNPJRecebedor(FcomprovantepagamentoTmp.edtcpfcnpjrecebedor.text);

       If (TempObjComprovanteCheque.Salvar(false)=False)
       Then Begin
                 result:=False;
                 exit;
            End;
       TempObjComprovanteCheque.TabelaparaObjeto;

       //Antes de Imprimir o Cheque e o Comprovante
       //Vou Atualizar o Cheque na Talao de Cheques acrescentando o Numero do Cheque no Pagamento e do comprovante

       If (TempObjTalaodecheques.Localizacodigo(TempCodigoChequeTalao)=True)
       Then Begin
                 COMPCHEQUE:='';
                 COMPCHEQUE:=' CP'+TempObjComprovanteCheque.Get_Comprovante;
                 TempObjTalaodecheques.TabelaparaObjeto;
                 TempObjTalaodecheques.Status:=dsedit;
                 TempObjTalaodecheques.Submit_HistoricoPagamento(TempObjTalaodecheques.Get_HistoricoPagamento+COMPCHEQUE);
                 TempObjTalaodecheques.Salvar(False);

       End;
       //******************************************

        //VERIFICANDO SE VAI IMPRIMIR O CHEQUE OU NAUM
        if (ObjParametroGlobal.ValidaParametro('IMPRIME CHEQUE NO PAGAMENTO?')=FALSE)
        THEN BEGIN
               Messagedlg('O Par�metro "IMPRIME CHEQUE NO PAGAMENTO?" n�o foi encontrado!',mterror,[mbok],0);
               exit;

        END;
        
        if (ObjParametroGlobal.get_valor='SIM')
        then TempObjTalaodecheques.ImprimeChequeRDPRINT(TempObjTalaodecheques.Get_codigo);

        //VERIFICANDO SE VAI IMPRIMIR O COMPROVANTE OU NAUM
        if (ObjParametroGlobal.ValidaParametro('IMPRIME C�PIA DE CHEQUE NO PAGAMENTO?')=FALSE)
        THEN BEGIN
               Messagedlg('O Par�metro "IMPRIME C�PIA DE CHEQUE NO PAGAMENTO?" n�o foi encontrado!',mterror,[mbok],0);
               exit;

        END;
        

        if (ObjParametroGlobal.get_valor='SIM')
        then TempObjComprovanteCheque.ImprimeComprovanteCheque(TempObjComprovanteCheque.Get_CODIGO);


       //Imprimindo um Relat�rio dos T�tulos Pagos
       

       Result:=True;

    Finally
           TempObjtalaodecheques.free;
           TempObjChequePortador.free;
           TempObjTransferenciaPortador.free;
           TempObjGeraTransferencia.free;
           TempObjComprovanteCheque.free;
           Freeandnil(TempTStrings);
           //Uobjtransferenciaportador.RegTransferenciaPortador.Origem:='';
           //Uobjtransferenciaportador.RegTransferenciaPortador.Historico:='';
    End;

Finally
       Freeandnil(FcomprovantepagamentoTmp);
End;



End;

Function TObjLancamento.EmiteChequeCaixa: boolean;
var
  TempObjTalaodecheques:tobjtalaodecheques;
  TempObjChequePortador:tobjChequesportador;
  TempObjComprovanteCheque:TObjComprovanteCheque;
  TempObjGeraTransferencia:TObjGeraTransferencia;
  saida:Boolean;
  TempPortadorDestino,TempCodigoChequeTalao,TempCodigoChequePortador:string;
  ident:byte;
  vencimentocheque:Tdate;
  ValorCheque,Somaporlancamento:Currency;
  PCCREDITO,PCDEBITO,PTransferencia:string;
  NOMECREDITO,NOMEDEBITO:string;
  FcomprovantepagamentoTmp:TFcomprovantepagamentoTmp;
  Pcidade:string;
  TempTStrings:TstringList;
  TempObjTransferenciaPortador:TobjtransferenciaPortador;
begin

  Pcidade:='';
  Pcidade:='Dourados';
  if (ObjParametroGlobal.ValidaParametro('CIDADE A SER IMPRESSA NO CHEQUE')=true) then
  begin
    Pcidade:=Objparametroglobal.Get_Valor;
  end;

  try
    FcomprovantepagamentoTmp:=TFcomprovantepagamentoTmp.create(nil);

    result:=FALSE;
    //Tenho que escolher o cheque e o valor que vai ser preenchido
    //Preencho com valor 0, crio um registro em ChequesPortador
    //com valor 0 para n�o influenciar no saldo, depois altero
    //o mesmo com outro valor, pois na altera��o n�o atualiza o saldo
    //Dou uma entrada com algum tipo de lanctoportador
    //exporta para a contabilidade o credito no portador do cheque
    //e o debito no caixa
    //******************************

    try
      ident:=0;
      TempObjTalaodeCheques:=Tobjtalaodecheques.create;
      ident:=1;
      TempObjChequePortador:=Tobjchequesportador.create;
      ident:=2;
      TempObjComprovanteCheque:=TObjComprovanteCheque.create;
      ident:=3;
      ident:=4;
      TempObjGeraTransferencia:=TObjGeraTransferencia.create;
      ident:=5;
      TempObjTransferenciaPortador:=TobjtransferenciaPortador.Create;
      ident:=6;
      TempTStrings:=TstringList.create;

    except
      if (ident=0)
      then Messagedlg('N�o foi poss�vel gerar o Objeto de Tal�o de Cheques',mterror,[mbok],0);

      if (ident=1)
      then Messagedlg('N�o foi poss�vel gerar o Objeto de ChequePortador',mterror,[mbok],0);

      if (ident=2)
      then Messagedlg('N�o foi poss�vel gerar o Objeto de Comprovante de Cheque',mterror,[mbok],0);

      if (ident=3)
      then Messagedlg('N�o foi poss�vel gerar o Objeto de Lancto em Portadores',mterror,[mbok],0);

      if (ident=4)
      then Messagedlg('N�o foi poss�vel gerar o Objeto de Gera��o de Transfer�ncia',mterror,[mbok],0);

      if (ident=5)
      then Messagedlg('N�o foi poss�vel gerar o Objeto de Transfer�ncia de Portador',mterror,[mbok],0);

      if (ident=6)
      then Messagedlg('N�o foi poss�vel gerar a String List',mterror,[mbok],0);

      exit;
    end;

    //Encontrando o cheque no talao de cheques
    try
      limpaedit(Ffiltroimp);
      with Ffiltroimp do
      begin
        DesativaGrupos;

        Grupo01.Enabled:=True;
        Grupo02.Enabled:=True;
        Grupo03.Enabled:=True;

        edtgrupo01.color:=$005CADFE; //laranja
        edtgrupo01.EditMask:='';
        edtgrupo01.MaxLength := 9;

        edtgrupo02.EditMask:='!99/99/9999;1;_';
        edtgrupo02.text:=datetostr(now);

        edtgrupo03.EditMask:='';
        edtgrupo03.MaxLength := 10;

        lbGrupo01.Caption:='Cheque';
        lbGrupo02.Caption:='Vencimento';
        lbGrupo03.Caption:='Valor R$';

        repeat
          tag := 0;

          saida:=False;
          edtgrupo01.OnKeyDown := Self.edttalaodechequesKeyDown;
          edtgrupo01.OnDblClick := Self.edttalaodechequesDblClick;
          edtgrupo01.OnKeyPress := Self.edttalaodechequesKeyPress; //Rodolfo

          edtgrupo03.OnKeyPress := Self.edttalaodechequesKeyPress; //Rodolfo
          edtgrupo03.OnChange := Self.edtValorChange; //Rodolfo
          showmodal;

          if tag=0 then
          begin
            result:=False;
            exit;
          end;
          Saida:=true;

          try
            ValorCheque:=0;
            ValorCheque:=Strtofloat(edtgrupo03.Text);
          except
            Messagedlg('Valor Inv�lido',mterror,[mbok],0);
            saida:=False;
          end;

          try
            if (Saida=True) then //passou pela verificacao do valor
            begin
              VencimentoCheque:=Strtodate(edtgrupo02.text);

              if (TempObjTalaodecheques.LocalizaCodigo(edtgrupo01.text) = False) then
              begin
                Messagedlg('Cheque n�o localizado na Tabela de Tal�o de Cheques',mterror,[mbok],0);
                saida:=False;
              end
              else
              begin
                TempObjTalaodecheques.TabelaparaObjeto;
                TempCodigoChequeTalao := TempObjTalaodecheques.Get_CODIGO;
                if TempObjTalaodecheques.Get_usado = 'S' then
                begin
                  Messagedlg('Cheque J� Usado',mterror,[mbok],0);
                  saida:=False;
                end
                else saida:=True;
              end;
            end;
          except
               saida := False;
          end;
        until(saida = True);
      end;

      //ja tenho o cheque agora tenho que gerar um cheque na TabChequePortador
      //depois localiza-lo e trocar o valor
      //Vou lanca-lo com valor zero, depois vou localiza-lo e alterar o valor
      //porque isso?? Porque quando eu insiro � alterado o valor da Tabchequesportador
      //e quando eu edito n�o

      //Gerando na TabChequesPortador e guardando o codigo gerado
      TempObjChequePortador.status:=dsinsert;
      TempCodigoChequePortador:=TempObjChequePortador.Get_NovoCodigo;
      TempObjChequePortador.Submit_CODIGO(TempCodigoChequePortador);
      TempObjChequePortador.Submit_Portador(TempObjTalaodecheques.Get_Portador);
      TempObjChequePortador.Submit_NumCheque(TempObjTalaodecheques.Get_Numero);
      TempObjChequePortador.Submit_Agencia(TempObjTalaodecheques.Portador.Get_Agencia);
      TempObjChequePortador.Submit_Conta(TempObjTalaodecheques.Portador.Get_NumeroConta);
      TempObjChequePortador.Submit_Valor('0');
      TempObjChequePortador.Submit_Vencimento(datetostr(Vencimentocheque));
      TempObjChequePortador.Submit_Chequedoportador('S');

      if TempObjChequePortador.Salvar(False,False,False,'') = False then
      begin
        Messagedlg('N�o foi poss�vel Gerar o Cheque na TabChequeportador!',mterror,[mbok],0);
        result:=False;
        exit;
      end;

      //Agora Altero o mesmo com o valor correto
      if (TempObjChequePortador.LocalizaCodigo(TempCodigoChequePortador)=False) then
      begin
        Messagedlg('Ap�s Salvar o ChequePortador criado atrav�s da Tal�o de Cheque, n�o foi poss�vel localizado para alterar o valor!',mterror,[mbok],0);
        result:=False;
        exit;
      end
      else TempObjChequePortador.TabelaparaObjeto;

      TempObjChequePortador.status:=dsedit;
      TempObjChequePortador.Submit_Valor(FloatToStr(ValorCheque));

      if TempObjChequePortador.Salvar(False,False,false,'') = False then
      begin
        Messagedlg('N�o foi poss�vel Alterar o Cheque na TabChequeportador!',mterror,[mbok],0);
        result:=False;
        exit;
      end;

      //Fazendo a transferencia do portador pra ele mesmo, assim tenho
      //uma data para ser usada e faz com o cheque apareca no relatorio
      //de cheques emitidos

      if (TempObjGeraTransferencia.LocalizaHistorico('CHEQUE DO PORTADOR PAGANDO CONTAS')=False) then
      begin
        Messagedlg('N�o foi encontrado o gerador de Transfer�ncia com Hist�rico->#CHEQUE DO PORTADOR PAGANDO CONTAS#',mterror,[mbok],0);
        result:=False;
        exit;
      End;

      Ptransferencia:=TempObjTransferenciaPortador.Get_NovoCodigo;
      TempObjGeraTransferencia.TabelaparaObjeto;
      TempObjTransferenciaPortador.status:=dsinsert;
      TempObjTransferenciaPortador.Submit_CODIGO(Ptransferencia);
      TempObjTransferenciaPortador.Submit_Data(datetostr(now));
      TempObjTransferenciaPortador.Submit_Valor('0');
      TempObjTransferenciaPortador.Submit_Documento('0');
      TempObjTransferenciaPortador.Submit_Complemento('0');
      TempObjTransferenciaPortador.Submit_Grupo(TempObjGeraTransferencia.Get_Grupo);
      TempObjTransferenciaPortador.Submit_PortadorOrigem(TempObjTalaodecheques.Get_Portador);
      TempObjTransferenciaPortador.Submit_PortadorDestino(TempObjTalaodecheques.Get_Portador);
      TempObjTransferenciaPortador.CodigoLancamento.Submit_codigo('');

      //Na transferencia se envia um Tstrings com a lista de cheques
      //no meu caso tenho que mandar apenas uma string
      //para isto tenho que criar uma Tstrings s� para ele

      TempTStrings.clear;
      TempTStrings.add(TempCodigoChequePortador);
      TempObjTransferenciaPortador.Submit_ListaChequesTransferencia(TempTStrings);

      if (TempObjTransferenciaPortador.Salvar(false,False,False,False,False,False,'')=False) then
      begin
        result:=False;
        exit;
      End;
      //*********************************************
        
      //NO TALAO DE CHEQUES altero o campo usado do cheque para S e guardo o codigo da chequeportador
      if (TempObjTalaodecheques.LocalizaCodigo(TempObjTalaodecheques.get_codigo)=False) then
      begin
        Messagedlg('Cheque n�o localizado na Tabela de Tal�o de Cheques para atualizar o campo USADO para ''S'' ',mterror,[mbok],0);
        saida:=False;
        exit;
      end;

      TempObjTalaodecheques.TabelaparaObjeto;
      TempObjTalaodecheques.Status:=dsedit;
      TempObjTalaodecheques.Submit_USado('S');
      TempObjTalaodecheques.Submit_Descontado('N');
      TempObjTalaodecheques.Submit_CodigoChequePortador(TempCodigoChequePortador);
      TempObjTalaodecheques.Submit_Valor(FloattoStr(ValorCheque));
      TempObjTalaodecheques.Submit_Vencimento(datetostr(Vencimentocheque));
      TempObjTalaodecheques.Submit_HistoricoPagamento('CHEQUE VALE CAIXA');

      if (TempObjTalaodecheques.Salvar(false)=False) then
      begin
        Messagedlg('A Opera��o de Salvar as altera��es no campo USADO e CodigoChequePortador da tabela de Tal�o de Cheques n�o foi bem sucedida!',mterror,[mbok],0);
        result:=False;
        exit;
      End;

      //Preenchendo o Comprovante
      //FcomprovantepagamentoTmp.showmodal;
      //Pegando os dados do form
      TempObjComprovanteCheque.Submit_CODIGO(TempObjComprovanteCheque.Get_NovoCodigo);

      limpaedit(FcomprovantepagamentoTMP);
      FcomprovantepagamentoTmp.edtpagoa.text:='CHEQUE VALE CAIXA';
      FcomprovantepagamentoTmp.edtnominador.text:='';
      FcomprovantepagamentoTmp.edtdescricao.text:='CHEQUE VALE CAIXA';
      FcomprovantepagamentoTmp.edtcomprovante.text:=TempObjComprovanteCheque.Get_CODIGO;
      FcomprovantepagamentoTmp.edtprovidencias.text:='Contabilizar e Arquivar';
      FcomprovantepagamentoTmp.edtcidade.text:=Pcidade;
      FcomprovantepagamentoTmp.edtdata.text:=Datetostr(now);
      FcomprovantepagamentoTmp.edtnomerecebedor.text:='';
      FcomprovantepagamentoTmp.edtcpfcnpjrecebedor.text:='';

      if (ObjParametroGlobal.ValidaParametro('PREENCHE COMPROVANTE CHEQUE?') = False) then
      begin
        Messagedlg('O Parametro "PREENCHE COMPROVANTE CHEQUE?" "VALOR=SIM ou N�O" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
        exit;
      End;


      if (ObjParametroGlobal.Get_Valor='SIM') then
        FcomprovantepagamentoTmp.showmodal;
      //Apos voltar do form envia os dados do form para o objeto

      TempObjComprovanteCheque.Status:=Dsinsert;

      TempObjComprovanteCheque.Submit_Cheque(TempCodigoChequePortador);
      TempObjComprovanteCheque.Submit_PagoA(FcomprovantepagamentoTmp.edtpagoa.text);
      TempObjComprovanteCheque.Submit_Nominador(FcomprovantepagamentoTmp.edtnominador.text);
      TempObjComprovanteCheque.Submit_descricaorecibo(FcomprovantepagamentoTmp.edtdescricao.text);
      TempObjComprovanteCheque.Submit_descricaocomprovante('CHEQUE VALE CAIXA');
      TempObjComprovanteCheque.Submit_Comprovante(FcomprovantepagamentoTmp.edtcomprovante.text);//uso o mesmo do primario
      TempObjComprovanteCheque.Submit_Providencias(FcomprovantepagamentoTmp.edtprovidencias.text);
      TempObjComprovanteCheque.Submit_Cidade(FcomprovantepagamentoTmp.edtcidade.text);
      TempObjComprovanteCheque.Submit_Data(FcomprovantepagamentoTmp.edtdata.text);
      TempObjComprovanteCheque.Submit_NomeRecebedor(FcomprovantepagamentoTmp.edtnomerecebedor.text);
      TempObjComprovanteCheque.Submit_CPFCNPJRecebedor(FcomprovantepagamentoTmp.edtcpfcnpjrecebedor.text);

      if (TempObjComprovanteCheque.Salvar(false)=False) then
      begin
        result:=False;
        exit;
      end;
      TempObjComprovanteCheque.TabelaparaObjeto;

      if (TempObjTalaodecheques.Localizacodigo(TempCodigoChequeTalao)=True) then
      begin
        TempObjTalaodecheques.TabelaparaObjeto;
        TempObjTalaodecheques.Status:=dsedit;
        TempObjTalaodecheques.Submit_HistoricoPagamento(TempObjTalaodecheques.Get_HistoricoPagamento+' CP'+TempObjComprovanteCheque.Get_Comprovante);
        TempObjTalaodecheques.Salvar(False);
      End;

       //VERIFICANDO SE VAI IMPRIMIR O CHEQUE OU NAUM
      if (ObjParametroGlobal.ValidaParametro('IMPRIME CHEQUE NO PAGAMENTO?')=FALSE) then
      begin
        Messagedlg('O Par�metro "IMPRIME CHEQUE NO PAGAMENTO?" n�o foi encontrado!',mterror,[mbok],0);
        exit;
      end;


      if (ObjParametroGlobal.get_valor='SIM') then
        TempObjTalaodecheques.ImprimeChequeRDPRINT(TempObjTalaodecheques.Get_codigo);

      //VERIFICANDO SE VAI IMPRIMIR O COMPROVANTE OU NAUM
      if (ObjParametroGlobal.ValidaParametro('IMPRIME C�PIA DE CHEQUE NO PAGAMENTO?')=FALSE) then
      begin
        Messagedlg('O Par�metro "IMPRIME C�PIA DE CHEQUE NO PAGAMENTO?" n�o foi encontrado!',mterror,[mbok],0);
        exit;
      end;

      if (ObjParametroGlobal.get_valor='SIM') then
        TempObjComprovanteCheque.ImprimeComprovanteCheque(TempObjComprovanteCheque.Get_CODIGO);

      //AGORA PRECISO LANCAR NO CAIXA O VALOR DO CHEQUE
      //PEGANDO O PORTADOR DO CAIXA E O TIPO DE LANCTOCDDD
      if (TempObjGeraTransferencia.LocalizaHistorico('TROCA DE CHEQUE')=False) then
      begin
        Messagedlg('O Gerador de Transfer�ncia "TROCA DE CHEQUE" n�o foi encontrado!'+#13+'N�o � poss�vel Gerar uma Troca de Cheque sem este Gerador!',mterror,[mbok],0);
        exit;
      end;

      TempObjGeraTransferencia.TabelaparaObjeto;
      TempPortadorDestino:=TempObjGeraTransferencia.PORTADORDESTINO.GET_CODIGO;

      ObjlanctoportadorGlobal.Submit_Historico('CHEQUE VALE CAIXA N�'+TempObjTalaodecheques.Get_Numero+' PORTADOR '+TempObjTalaodecheques.Portador.get_nome);
      ObjlanctoportadorGlobal.Submit_Data(datetostr(Now));
      ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABTRANSFERENCIAPORTADOR');
      ObjlanctoportadorGlobal.Submit_ObjetoGerador('TABTRANSFERENCIAPORTADOR');
      ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
      ObjlanctoportadorGlobal.Submit_ValordoCampo(Ptransferencia);
      ObjlanctoportadorGlobal.Submit_ImprimeRel(True);
      ObjlanctoportadorGlobal.TransferenciaPortador.Submit_CODIGO(ptransferencia);

      if(ObjlanctoportadorGlobal.AumentaSaldo(TempPortadorDestino,TempObjTalaodecheques.Get_Valor,False)=False) then
        exit;

      //exportando contabilidade
      result:=False;
      Self.objExportaContabilidade.ZerarTabela;
      Self.ObjExportaContabilidade.Submit_Exportado('N');
      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJTALAODECHEQUES');
      Self.ObjExportaContabilidade.Submit_CodGerador(TempCodigoChequeTalao);
      Self.ObjExportaContabilidade.Submit_NumDocto(TempObjTalaodecheques.Get_Numero);
      //preciso localizar se tenho que solicitar o c�digo do plano de contas para o desconto ou juros
      self.ObjExportaContabilidade.Submit_Exporta('N');
      PCCREDITO:='';
      NOMECREDITO:='';
      NOMEDEBITO:='';
      PCDEBITO:='';

      if (tempObjTalaodeCheques.Portador.Get_CodigoPlanodeContas <> '') then
        PCCREDITO:=tempObjTalaodeCheques.Portador.Get_CodigoPlanodeContas;

      if (TempObjGeraTransferencia.PORTADORDESTINO.Get_CodigoPlanodeContas <> '') then
        PCDEBITO:=TempObjGeraTransferencia.PORTADORDESTINO.Get_CodigoPlanodeContas;

      if (PCDEBITO='') then
        PCDEBITO:='0'
      else
      begin
        if (Self.Pendencia.Titulo.Objplanodecontas.LocalizaCodigo(PCDEBITO) = False) then
          PCDEBITO:='0'
        else
        begin
          Self.Pendencia.Titulo.Objplanodecontas.TabelaparaObjeto;
          NOMEDEBITO:=Self.Pendencia.Titulo.Objplanodecontas.Get_Nome;
          self.ObjExportaContabilidade.Submit_Exporta('S');
        end;
      end;

      if (PCCREDITO<>'') then
      begin
        if (Self.Pendencia.Titulo.Objplanodecontas.LocalizaCodigo(PCCREDITO)=False) then
          ObjExportaContabilidade.Submit_Exporta('N')
        else
        begin
          Self.Pendencia.Titulo.Objplanodecontas.TabelaparaObjeto;
          NOMECREDITO:=Self.Pendencia.Titulo.Objplanodecontas.Get_Nome;
        end;
      end
      else ObjExportaContabilidade.Submit_Exporta('N');

      //**************************************
      //preciso gravar os dados principais na exporta��o
      //Inserindo os dados
      ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
      ObjExportaContabilidade.Submit_Data(datetostr(now));

      ObjExportaContabilidade.Submit_ContaCredite(PCCREDITO);
      ObjExportaContabilidade.Submit_ContaDebite(PCDEBITO);
      ObjExportaContabilidade.Submit_Valor(TempObjTalaodeCheques.Get_Valor);
      ObjExportaContabilidade.Submit_Historico('CHEQUE VALE CAIXA N� '+TempObjTalaodecheques.Get_Numero+' PORTADOR '+TempObjTalaodecheques.Portador.get_nome+' CP'+TempObjComprovanteCheque.Get_Comprovante);
      ObjExportaContabilidade.Status:=dsinsert;
      Result:=ObjExportaContabilidade.Salvar(False);
      ObjExportaContabilidade.Status:=dsInactive;
    finally
      TempObjtalaodecheques.free;
      TempObjChequePortador.free;
      TempObjComprovanteCheque.free;
      TempObjGeraTransferencia.free;

      if (result=TRUE) then
      begin
        Self.Commit;
        showmessage('Cheque emitido com sucesso!');
      end
      else
      begin
        Self.Rolback;
        ShowMessage('Erro na emiss�o do Cheque!');
      end;
    end;

  finally
    Freeandnil(TempTStrings);
    Freeandnil(FcomprovantepagamentoTmp);
    TempObjTransferenciaPortador.Free;
  end;

end;


{Rodolfo}
Function TObjLancamento.PreencheCheque(Pportador:string; PSaldo:string; PcredorDevedorLote,PcodigoCredorDevedorLote:string; auxLabel: String):Boolean;
begin
   self.strLabel := auxLabel;
   Result:= self.PreencheCheque(Pportador, PSaldo, PcredorDevedorLote, PcodigoCredorDevedorLote);
end;


function TObjLancamento.PreencheCheque: boolean;
Begin
     Result:=Self.PreencheCheque('','');
End;


Function TObjLancamento.PreencheCheque(Pportador:string;PSaldo:string):Boolean;
Begin
     result:=Self.PreencheCheque(pportador,psaldo,'','');
End;


Function TObjLancamento.PreencheCheque(Pportador:string;PSaldo:string;PcredorDevedorLote,PcodigoCredorDevedorLote:string):Boolean;
var
  TempObjTalaodecheques:tobjtalaodecheques;
  TempObjChequePortador:tobjChequesportador;
  TempObjTransferenciaPortador:TObjTransferenciaPortador;
  TempObjGeraTransferencia:TObjGeraTransferencia;
  TempTStrings:TstringList;
  TempObjComprovanteCheque:TObjComprovanteCheque;
  TempObjConfRelatorio:TObjConfRelatorio;
  saida:Boolean;
  TempCodigoChequeTalao,TempCodigoChequePortador:string;
  ident:byte;
  vencimentocheque:Tdate;
  ValorCheque,Somaporlancamento:Currency;
  FcomprovantepagamentoTmp:TFcomprovantepagamentoTmp;
  pcodigo,Pcidade:String;
begin
  result:=False;
  FcomprovantepagamentoTmp:=TFcomprovantepagamentoTmp.create(nil);

  Pcidade:='Dourados';
  if (ObjParametroGlobal.ValidaParametro('CIDADE A SER IMPRESSA NO CHEQUE')=true) then
  begin
    Pcidade:=Objparametroglobal.Get_Valor;
  End;

  Try
     //Tenho que escolher o cheque e o valor que vai ser preenchido
     //Preencho com valor 0, crio um registro em ChequesPortador
     //com valor 0 para n�o influenciar no saldo, depois altero
     //o mesmo com outro valor, pois na altera��o n�o atualiza o saldo
     //depois disto faco uma transferencia do portador do cheque para ele mesmo
     //assim eu tenho um titulo comprovando o pagamento da divida, quando o cheque
     //cair no banco eu transfiro do meu portador para o portador de contas pagas
     //ou de cheques do portador
     //******************************
    
    try
      ident:=0;
      TempObjTalaodeCheques:=Tobjtalaodecheques.create;
      ident:=1;
      TempObjChequePortador:=Tobjchequesportador.create;
      ident:=2;
      TempObjTransferenciaPortador:=TObjTransferenciaPortador.create;
      ident:=3;
      TempObjgeraTransferencia:=TObjgeraTransferencia.create;
      ident:=4;
      TempTStrings:=TStringList.create;
      ident:=5;
      TempObjComprovanteCheque:=TObjComprovanteCheque.create;
      ident:=6;
      TempObjConfRelatorio:=TObjConfRelatorio.create;
    Except
      If (ident=0)
      Then Messagedlg('N�o foi poss�vel gerar o Objeto de Tal�o de Cheques',mterror,[mbok],0);
      If (ident=1)
      Then Messagedlg('N�o foi poss�vel gerar o Objeto de ChequePortador',mterror,[mbok],0);
      If (ident=2)
      Then Messagedlg('N�o foi poss�vel gerar o Objeto de Transfer�ncia de Portador',mterror,[mbok],0);
      if (ident=3)
      then Messagedlg('Erro Na Cria��o do Objeto de Gera��o de Transfer�ncia para Lan�amento da Transfer�ncia',mterror,[mbok],0);
      if (ident=4)
      then Messagedlg('Erro na tentativa de Cria��o da TStrings para enviar o C�digo do Cheque para Transfer�ncia',mterror,[mbok],0);
      if (ident=5)
      then Messagedlg('Erro na tentativa de Cria��o do Objeto de Comprovante de Cheque',mterror,[mbok],0);
      if (ident=6)
      then Messagedlg('Erro na tentativa de Cria��o do Objeto de Configura��o de Relat�rio',mterror,[mbok],0);
      result:=False;
      exit;
    End;

    //Encontrando o cheque no talao de cheques
    Try
      limpaedit(Ffiltroimp);
      With Ffiltroimp do
      Begin
        DesativaGrupos;
        Grupo01.Enabled := True;   //Rodolfo
        Grupo02.Enabled := True;
        Grupo03.Enabled := True;
        Grupo04.Enabled := True;

        edtgrupo02.EditMask := '';
        edtgrupo03.EditMask := '!99/99/9999;1;_';
        edtgrupo03.text := datetostr(now);

        edtgrupo01.Color := $005CADFE; //Rodolfo
        edtgrupo02.Color := $005CADFE; //Rodolfo

        {Rodolfo==}
        Try
          edtgrupo01.Text := Pportador;
          lb2grupo01.Caption := strLabel;
        Except
          edtgrupo01.Text := '';
        End;

        edtgrupo01.Tag:=0;
        {==Rodolfo}

        Try
          edtgrupo02.Tag := 0;
          if (Pportador <> '') then
            strtoint(pportador);
          edtgrupo02.Tag:=strtoint(pportador);
        Except
          edtgrupo02.Tag:=0;
        End;

        edtgrupo04.EditMask:='';

        if (PSaldo = '') then
          edtgrupo04.text := Self.Valor
        else edtgrupo04.text := Psaldo;

        lbGrupo01.Caption:='Portador';  //Rodolfo
        lbGrupo02.Caption:='Cheque';
        lbGrupo03.Caption:='Vencimento';
        lbGrupo04.Caption:='Valor R$';

        edtgrupo01.OnExit     := Self.edtportadorComChequeExit;      //Rodolfo
        edtgrupo01.OnKeyDown  := Self.edtportadorComChequeKeyDown;   //Rodolfo
        edtgrupo01.OnDblClick := Self.edtportadorComChequeDblClick;  //Rodolfo
        edtgrupo01.OnKeyPress := Self.edtportadorComChequeKeyPress;  //Rodolfo
        edtgrupo01.MaxLength  := 9; //Rodolfo

        edtgrupo02.OnKeyDown  := Self.buscaNumChequeKeyDown; //Acha o numero do cheque ao inves do codigo - rodolfo
        edtgrupo02.OnDblClick := Self.buscaNumChequeDblClick; //Rodolfo
        edtgrupo02.OnKeyPress := Self.edttalaodechequesKeyPress; //Rodolfo
        edtgrupo02.MaxLength  := 9;   //Rodolfo

        edtgrupo04.OnChange := Self.edtValorChange; //Rodolfo
        edtgrupo04.MaxLength := 9; //Valor Maximo: 99999,99    //Rodolfo

        Repeat

          saida:=False;
          showmodal;

          if tag=0 then
          begin
            result:=False;
            exit;
          end;

          Saida:=true;

          try
            ValorCheque:=0;
            ValorCheque:=StrtoFloat(edtgrupo04.text);
          except
            Messagedlg('Valor Inv�lido',mterror,[mbok],0);
            saida:=False;
          end;

          try
            if (Saida=True) then//passou pela verificacao do valor
            begin
              VencimentoCheque:=Strtodate(edtgrupo03.text);

              //If (TempObjTalaodecheques.LocalizaCodigo(edtgrupo02.text)=False)  //Anterior
              if (TempObjTalaodecheques.LocalizaChequeporPortador(edtgrupo02.Text, edtgrupo01.text) = False) then //novo - Rodolfo
              begin
                Messagedlg('Cheque n�o localizado na Tabela de Tal�o de Cheques',mterror,[mbok],0);
                saida:=False;
              end
              else
              begin
                TempObjTalaodecheques.TabelaparaObjeto;
                TempCodigoChequeTalao:=TempObjTalaodecheques.Get_CODIGO;
                if TempObjTalaodecheques.Get_usado='S' then
                begin
                  Messagedlg('Cheque J� Usado',mterror,[mbok],0);
                  saida:=False;
                end
                else saida:=True;
              end;
            end;
          except
            saida:=False;
          end;
        until(saida=True);
      end;

      //ja tenho o cheque agora tenho que gerar um cheque na TabChequePortador
      //depois localiza-lo e trocar o valor
      //Vou lanca-lo com valor zero, depois vou localiza-lo e alterar o valor
      //porque isso?? Porque quando eu insiro � alterado o valor da Tabchequesportador
      //e quando eu edito n�o
      //Gerando na TabChequesPortador e guardando o codigo gerado
      TempObjChequePortador.status:=dsinsert;
      TempCodigoChequePortador:=TempObjChequePortador.Get_NovoCodigo;
      TempObjChequePortador.Submit_CODIGO(TempCodigoChequePortador);
      TempObjChequePortador.Submit_Portador(TempObjTalaodecheques.Get_Portador);
      TempObjChequePortador.Submit_NumCheque(TempObjTalaodecheques.Get_Numero);
      TempObjChequePortador.Submit_Agencia(TempObjTalaodecheques.Portador.Get_Agencia);
      TempObjChequePortador.Submit_Conta(TempObjTalaodecheques.Portador.Get_NumeroConta);
      TempObjChequePortador.Submit_Valor('0');
      TempObjChequePortador.Submit_Vencimento(datetostr(Vencimentocheque));
      TempObjChequePortador.Submit_Chequedoportador('S');

      if TempObjChequePortador.Salvar(true,False,False,'')=False then
      begin
        Messagedlg('N�o foi poss�vel Gerar o Cheque na TabChequeportador!',mterror,[mbok],0);
        result:=False;
        exit;
      end;

      //Agora Altero o mesmo com o valor correto
      if (TempObjChequePortador.LocalizaCodigo(TempCodigoChequePortador)=False) then
      begin
        Messagedlg('Ap�s Salvar o ChequePortador criado atrav�s da Tal�o de Cheque, n�o foi poss�vel localizado para alterar o valor!',mterror,[mbok],0);
        result:=False;
        exit;
      end
      else TempObjChequePortador.TabelaparaObjeto;

      TempObjChequePortador.status:=dsedit;
      TempObjChequePortador.Submit_Valor(FloatToStr(ValorCheque));

      if TempObjChequePortador.Salvar(true,False,false,'') = False then
      begin
        Messagedlg('N�o foi poss�vel Alterar o Cheque na TabChequeportador!',mterror,[mbok],0);
        result:=False;
        exit;
      End;

      //NO TALAO DE CHEQUES altero o campo usado do cheque para S e guardo o codigo da chequeportador
      if (TempObjTalaodecheques.LocalizaCodigo(TempObjTalaodecheques.get_codigo)=False) then
      begin
        Messagedlg('Cheque n�o localizado na Tabela de Tal�o de Cheques para atualizar o campo USADO para ''S'' ',mterror,[mbok],0);
        saida:=False;
        exit;
      end;

      TempObjTalaodecheques.TabelaparaObjeto;
      TempObjTalaodecheques.Status:=dsedit;
      TempObjTalaodecheques.Submit_USado('S');
      TempObjTalaodecheques.Submit_Descontado('N');
      TempObjTalaodecheques.Submit_CodigoChequePortador(TempCodigoChequePortador);
      TempObjTalaodecheques.Submit_Valor(FloattoStr(ValorCheque));
      TempObjTalaodecheques.Submit_Vencimento(datetostr(Vencimentocheque));
      TempObjTalaodecheques.Submit_HistoricoPagamento(Self.Historico+' CH'+TempObjTalaodecheques.Get_Numero);
      TempObjTalaodecheques.Lancamento.Submit_CODIGO(Self.codigo);

      if (TempObjTalaodecheques.Salvar(True)=False) then
      begin
        Messagedlg('A Opera��o de Salvar as altera��es no campo USADO e CodigoChequePortador da tabela de Tal�o de Cheques n�o foi bem sucedida!',mterror,[mbok],0);
        result:=False;
        exit;
      End;

      {Lanco uma transferencia deste cheque do seu portador para ele mesmo}
      //Preciso saber qual o historico e o grupo para a transferencia
      //criar campos na tabgeratransferencia e localizar pelo historico
      if (TempObjGeraTransferencia.LocalizaHistorico('CHEQUE DO PORTADOR PAGANDO CONTAS')=False) then
      begin
        Messagedlg('N�o foi encontrado o gerador de Transfer�ncia com Hist�rico->#CHEQUE DO PORTADOR PAGANDO CONTAS#',mterror,[mbok],0);
        result:=False;
        exit;
      End;

      TempObjGeraTransferencia.TabelaparaObjeto;

      {Uobjtransferenciaportador.RegTransferenciaPortador.Origem:='OBJLANCAMENTO';
      Uobjtransferenciaportador.RegTransferenciaPortador.CodigoLancamento:=Self.codigo;
      Uobjtransferenciaportador.RegTransferenciaPortador.PortadorDestino:=TempObjTalaodecheques.Get_Portador;
      Uobjtransferenciaportador.RegTransferenciaPortador.Historico:='Cheque '+TempObjTalaodecheques.Get_Numero+' Emitido-'+Self.Get_Historico;}

      TempObjTransferenciaPortador.status:=dsinsert;
      TempObjTransferenciaPortador.Submit_CODIGO(TempObjTransferenciaPortador.Get_NovoCodigo);
      TempObjTransferenciaPortador.Submit_Data(Self.Data);
      TempObjTransferenciaPortador.Submit_Valor('0');
      TempObjTransferenciaPortador.Submit_Documento('0');
      TempObjTransferenciaPortador.Submit_Complemento('0');
      TempObjTransferenciaPortador.Submit_Grupo(TempObjGeraTransferencia.Get_Grupo);
      TempObjTransferenciaPortador.Submit_PortadorOrigem(TempObjTalaodecheques.Get_Portador);
      TempObjTransferenciaPortador.Submit_PortadorDestino(TempObjTalaodecheques.Get_Portador);
      TempObjTransferenciaPortador.CodigoLancamento.Submit_codigo(Self.codigo);

      //Na transferencia se envia um Tstrings com a lista de cheques
      //no meu caso tenho que mandar apenas uma string
      //para isto tenho que criar uma Tstrings s� para ele

      TempTStrings.clear;
      TempTStrings.add(TempCodigoChequePortador);
      TempObjTransferenciaPortador.Submit_ListaChequesTransferencia(TempTStrings);
      If (TempObjTransferenciaPortador.Salvar(true,False,False,False,False,False,'')=False) then
      begin
        result:=False;
        exit;
      end;

      Pcodigo:=self.codigo;
      Self.ZerarTabela;

      if (Self.LocalizaCodigo(pcodigo)=false) then
      begin
        MensagemErro('Erro ao tentar localizar o lan�amento');
        result:=False;
        exit;
      End;
      Self.TabelaparaObjeto;


      //Preenchendo o Comprovante
      //Pegando os dados do form
      TempObjComprovanteCheque.Status:=Dsinsert;
      TempObjComprovanteCheque.Submit_CODIGO(TempObjComprovanteCheque.Get_NovoCodigo);
      TempObjComprovanteCheque.Submit_Cheque(TempCodigoChequePortador);
      //Preenche o form  antes de chamar
      limpaedit(FcomprovantepagamentoTMP);

      FcomprovantepagamentoTmp.edtpagoa.text:='Pago a';
      FcomprovantepagamentoTmp.edtnominador.text:='Nominador';
      FcomprovantepagamentoTmp.edtdescricao.text:='Descricao';
      FcomprovantepagamentoTmp.edtcomprovante.text:='0';
      FcomprovantepagamentoTmp.edtprovidencias.text:='Contabilizar e Arquivar';
      FcomprovantepagamentoTmp.edtcidade.text:='Dourados';
      FcomprovantepagamentoTmp.edtdata.text:=Datetostr(now);
      FcomprovantepagamentoTmp.edtnomerecebedor.text:='Recebedor';
      FcomprovantepagamentoTmp.edtcpfcnpjrecebedor.text:='000.000.000/0000-00';

      if (ObjParametroGlobal.ValidaParametro('PREENCHE COMPROVANTE CHEQUE?') = False) then
      begin
        Messagedlg('O Parametro "PREENCHE COMPROVANTE CHEQUE?" "VALOR=SIM ou N�O" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
        exit;
      End;

      if (ObjParametroGlobal.Get_Valor='SIM') then
      begin
        try
          if (Self.Pendencia.get_codigo='') then
          begin
            //preencho apenas para chamar abaixo resgatando o nome
            //essa situacao existe quando � um pagamento em lote
            //nesse caso o lancamento nao tem pendencia
            //portanto nao tem titulo nem credor devedor
            self.pendencia.Titulo.CREDORDEVEDOR.Submit_CODIGO(PcredorDevedorLote);
            self.pendencia.Titulo.Submit_CODIGOCREDORDEVEDOR(PcodigocredorDevedorLote);
          end;

          FcomprovantepagamentoTmp.edtpagoa.text:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.pendencia.titulo.get_credordevedorCODIGO,Self.pendencia.titulo.get_codigocredordevedor);
          FcomprovantepagamentoTmp.edtnominador.text:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_RazaoSocialCredorDevedor(Self.pendencia.titulo.get_credordevedorCODIGO,Self.pendencia.titulo.get_codigocredordevedor);
          FcomprovantepagamentoTmp.edtdescricao.text:='PG. '+Self.pendencia.titulo.get_historico;
          FcomprovantepagamentoTmp.edtcomprovante.text:=TempObjComprovanteCheque.Get_codigo;
          FcomprovantepagamentoTmp.edtprovidencias.text:='Contabilizar e Arquivar';
          FcomprovantepagamentoTmp.edtcidade.text:=Pcidade;
          FcomprovantepagamentoTmp.edtdata.text:=Datetostr(now);
          FcomprovantepagamentoTmp.edtnomerecebedor.text:=TempObjComprovanteCheque.Get_Nominador;
          FcomprovantepagamentoTmp.edtcpfcnpjrecebedor.text:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_CPFCNPJCredorDevedor(Self.pendencia.titulo.get_credordevedorCODIGO,Self.pendencia.titulo.get_codigocredordevedor);
        except
          on e:exception do
          begin
               Mensagemerro(e.message);
          end;
        end;
        FcomprovantepagamentoTmp.showmodal;
      end;

      //AQUI COMECA A VERIFICACAO DO COMPROVANTE E DA IMPRESSAO DO CHEQUE

      //Apos voltar do form envia os dados do form para o objeto
      TempObjComprovanteCheque.Submit_PagoA(FcomprovantepagamentoTmp.edtpagoa.text);
      TempObjComprovanteCheque.Submit_Nominador(FcomprovantepagamentoTmp.edtnominador.text);
      TempObjComprovanteCheque.Submit_descricaorecibo(FcomprovantepagamentoTmp.edtdescricao.text);
      TempObjComprovanteCheque.Submit_descricaocomprovante(TempObjComprovanteCheque.Get_descricaorecibo);
      TempObjComprovanteCheque.Submit_Comprovante(FcomprovantepagamentoTmp.edtcomprovante.text);
      TempObjComprovanteCheque.Submit_Providencias(FcomprovantepagamentoTmp.edtprovidencias.text);
      TempObjComprovanteCheque.Submit_Cidade(FcomprovantepagamentoTmp.edtcidade.text);
      TempObjComprovanteCheque.Submit_Data(FcomprovantepagamentoTmp.edtdata.text);
      TempObjComprovanteCheque.Submit_NomeRecebedor(FcomprovantepagamentoTmp.edtnomerecebedor.text);
      TempObjComprovanteCheque.Submit_CPFCNPJRecebedor(FcomprovantepagamentoTmp.edtcpfcnpjrecebedor.text);

      if (TempObjComprovanteCheque.Salvar(true)=False) then
      begin
        result:=False;
        exit;
      end;
      TempObjComprovanteCheque.TabelaparaObjeto;
    
      //Antes de Imprimir o Cheque e o Comprovante
      //Vou Atualizar o Cheque na Talao de Cheques acrescentando o Numero do Cheque no Pagamento e do comprovante

      if (TempObjTalaodecheques.LocalizaCodigo(TempCodigoChequeTalao)=True) then
      begin
        TempObjTalaodecheques.TabelaparaObjeto;
        TempObjTalaodecheques.Status:=dsedit;
        TempObjTalaodecheques.Submit_HistoricoPagamento(TempObjTalaodecheques.Get_HistoricoPagamento+' -CP'+TempObjComprovanteCheque.Get_Comprovante);
        TempObjTalaodecheques.Salvar(False);
      end;

      //VERIFICANDO SE VAI IMPRIMIR O CHEQUE OU NAUM
      if (ObjParametroGlobal.ValidaParametro('IMPRIME CHEQUE NO PAGAMENTO?')=FALSE) then
      begin
        Messagedlg('O Par�metro "IMPRIME CHEQUE NO PAGAMENTO?" n�o foi encontrado!',mterror,[mbok],0);
        exit;
      end;
            
      if (ObjParametroGlobal.get_valor='SIM')
      then TempObjTalaodecheques.ImprimeChequeRDPRINT(TempObjTalaodecheques.get_codigo);

      //VERIFICANDO SE VAI IMPRIMIR O COMPROVANTE OU NAUM
      if (ObjParametroGlobal.ValidaParametro('IMPRIME C�PIA DE CHEQUE NO PAGAMENTO?')=FALSE) then
      begin
        Messagedlg('O Par�metro "IMPRIME C�PIA DE CHEQUE NO PAGAMENTO?" n�o foi encontrado!',mterror,[mbok],0);
        exit;
      end;


      if (ObjParametroGlobal.get_valor='SIM')
      then TempObjComprovanteCheque.ImprimeComprovanteCheque(TempObjComprovanteCheque.Get_CODIGO);

      result:=true;

    finally
      TempObjtalaodecheques.free;
      TempObjChequePortador.free;
      TempObjTransferenciaPortador.free;
      TempObjGeraTransferencia.free;
      TempObjComprovanteCheque.free;
      Freeandnil(TempObjConfRelatorio);
      Freeandnil(TempTStrings);
    end;

  finally
    Freeandnil(FcomprovantepagamentoTmp);
  end;
end;

function TObjLancamento.PegaNometipolancto(ParametroCodigo: string): string;
begin
     If Self.TipoLancto.LocalizaCodigo(ParametroCodigo)=false
     Then result:=''
     Else Begin
               Self.TipoLancto.TabelaparaObjeto;
               result:=Self.TipoLancto.Get_Nome;
          End;

end;

function TObjLancamento.Get_PesquisaTipoLanctoNaoGeravalor: string;
begin
     Result:=Self.TipoLancto.Get_PesquisaNaoGeraValor;
end;

{Procedure TObjLancamento.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FPagamento: TFrPagamento;
begin

end; }

{Rodolfo}
Procedure TObjLancamento.edttalaodechequesDblClick(Sender: TObject);
var
  Shift : TShiftState;
  key : Word;
begin
  key := VK_F9;
  edttalaodechequesKeyDown(Sender, Key, Shift);
end;

procedure TObjLancamento.edttalaodechequesKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Ftalao:TFtalaodeCheques;
begin

  if (key <>vk_f9)
  then exit;

  try
    Fpesquisalocal:=Tfpesquisa.create(nil);
    Ftalao:=TFtalaodeCheques.create(nil);
    FpesquisaLocal.NomeCadastroPersonalizacao:='UOBJLANCAMENTO.EDTTALAODEQUEQUES';

    if (Tedit(Sender).tag <> 0) then
    begin

      if (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaTalaodeChequesAbertos(inttostr(Tedit(Sender).tag)),Self.Get_titulopesquisatalaodecheques,FTalao)=True) then
      begin
        if (Fpesquisalocal.showmodal=mrok) then
        begin
          Tedit(Sender).text:=Fpesquisalocal.querypesq.fieldbyname('codigo').asstring;
        end;
      end;
    end
    else
    begin
      if (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaTalaodeChequesAbertos,Self.Get_titulopesquisatalaodecheques,Ftalao) = True) then
      begin
        if (Fpesquisalocal.showmodal = mrok) then
        begin
          Tedit(Sender).text:=Fpesquisalocal.querypesq.fieldbyname('codigo').asstring;   
        end;
      end;
    end;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FTalao);
  end;
end;


function TObjLancamento.Get_PesquisaTalaodeChequesAbertos: string;
Begin
  Result:=Self.Get_PesquisaTalaodeChequesAbertos('');
End;


function TObjLancamento.Get_PesquisaTalaodeChequesAbertos(pportador:string): string;
var
ObjCheques:TObjTalaodeCheques;
begin
     Try
        ObjCheques:=TObjTalaodeCheques.create;
     Except
           Messagedlg('Erro ao tentar criar Objeto de Tal�o de Cheques!',mterror,[mbok],0);
           result:='';
           exit;
     End;

     Try
        Result:=ObjCheques.Get_PesquisaAbertos(Pportador);
     Finally
            ObjCheques.free;
     End;

end;

function TObjLancamento.Get_TituloPesquisaTalaodeCheques: string;
var
ObjCheques:TObjTalaodeCheques;
begin
     Try
        ObjCheques:=TObjTalaodeCheques.create;
     Except
           Messagedlg('Erro ao tentar criar Objeto de Tal�o de Cheques!',mterror,[mbok],0);
           result:='';
           exit;
     End;

     Try
        Result:=ObjCheques.Get_TituloPesquisa;
     Finally
            ObjCheques.free;
     End;
end;

procedure TObjLancamento.ImprimeLancamentos;
var
TData1,TData2:Tdate;
Erro:boolean;
begin
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;

          LbGrupo01.caption:='Origem';
          LbGrupo02.caption:='Destino';

          ShowModal;

          If tag=0
          Then exit;

          Try
             Tdata1:=StrToDate(Edtgrupo01.text);
             Tdata2:=StrToDate(Edtgrupo02.text);
          Except
                Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
                exit;
          End;

     End;

     With Self.ObjDataset do
     Begin
          Close;
          SelectSQL.clear;
          SelectSQL.add('select * From ViewTituloLancamento where');
          SelectSQL.add('(Data>='+#39+Formatdatetime('mm/dd/yyyy',Tdata1)+#39+ ' and ');
          SelectSQL.add('Data<='+#39+Formatdatetime('mm/dd/yyyy',Tdata2)+#39+ ')');
          SelectSQL.add('Order by Data,Titulo,Pendencia,TipoLancto');
          Open;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('N�o h� dados para serem impressos!',mtinformation,[mbok],0);
                    exit;
               End;
     End;

     With FRelatorio do
     Begin
          DesativaCampos;
          QR.DataSet:=Self.ObjDataset;

          titulo.caption:='LAN�AMENTOS NO INTERVALO DE '+DatetoStr(Tdata1)+' a '+DatetoStr(Tdata2);

          ColTitulo01.Enabled:=true;
          Campo01.Enabled:=true;
          Campo01.DataSet:=Self.ObjDataset;
          ColTitulo01.Caption:='COD';
          Campo01.DataField:='CODIGO';

          ColTitulo0102.Enabled:=true;
          Campo0102.Enabled:=true;
          Campo0102.DataSet:=Self.ObjDataset;
          ColTitulo0102.Caption:='DATA';
          Campo0102.DataField:='DATA';

          ColTitulo0202.Enabled:=true;
          Campo0202.Enabled:=true;
          Campo0202.DataSet:=Self.ObjDataset;
          ColTitulo0202.Caption:='TITULO';
          Campo0202.DataField:='TITULO';

          ColTitulo03.Enabled:=true;
          Campo03.Enabled:=true;
          Campo03.DataSet:=Self.ObjDataset;
          ColTitulo03.Caption:='PEND�NCIA';
          Campo03.DataField:='PENDENCIA';

          ColTitulo04.Enabled:=true;
          Campo04.Enabled:=true;
          Campo04.DataSet:=Self.ObjDataset;
          ColTitulo04.Caption:='HIST�RICO';
          Campo04.DataField:='HISTORICO';
          Campo04.AutoSize:=False;
          Campo04.Width:=campo08.left-5-campo04.left;

          ColTitulo08.Enabled:=true;
          Campo08.Enabled:=true;
          Campo08.DataSet:=Self.ObjDataset;
          ColTitulo08.Caption:='VALOR';
          Campo08.DataField:='VALOR';
          Campo08.Mask:='0.00';
          Campo08.Alignment:=taRightJustify;
          Campo08.Left:=Campo08.left+15;

          Qr.preview;
          Campo04.AutoSize:=True;
          Campo08.Alignment:=taLeftJustify;
          Campo08.Left:=Campo07.left-15;

     End;



end;
destructor TObjLancamento.Free;
begin
     Freeandnil(Self.ObjDataset);
     FreeAndNil(ObjDataSourceLancto);
     FreeAndNil(objqueryLancto);

     Self.Pendencia.free;
     Self.TipoLancto.free;
     Self.ObjExportaContabilidade.free;
     Self.objgeralancamento.free;
     Self.ObjQueryLocal.free;
end;
//Usados No Analice
//chamado no momento que eu salvo o lancamento
function TObjLancamento.AtualizaGerador: boolean;
var
   titulotemp:TobjTitulo;
   QTemp:Tibquery;
   strtemp:string;
begin
     If (Self.pendencia.LocalizaCodigo(Self.pendencia.get_codigo)=False)
     Then Begin
               Messagedlg('Analise Erro-> Ap�s Salvar o lan�amento de uma Pend�ncia a Pend�ncia do mesmo n�o foi localizada!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
     Self.Pendencia.TabelaparaObjeto;

     Try
        Titulotemp:=Tobjtitulo.create;
     except
           Messagedlg('Erro na Cria��o do Objeto de T�tulo para Atualiza��o do Gerador',mterror,[mbok],0);
           result:=False;
           exit;
     End;
     Try

     //Unico exclusivamente para Titulos de Documentos
     //porque temos o campo saldo..., na Tabdctos
     //que precisam ser atualizados
     TituloTemp.LocalizaCodigo(Self.pendencia.get_titulo);
     TituloTemp.TabelaparaObjeto;

     If (TituloTemp.Get_GERADOR='DOCUMENTOS')
     Then Begin

               Try
                  Try

                        Qtemp:=Tibquery.create(nil);
                        Qtemp.database:=Self.ObjDataset.database;
                        Qtemp.close;
                        With QTemp do
                        Begin
                                close;
                                sql.clear;
                                strtemp:='Select count(codigo) as CNT from  '+titulotemp.get_geradortabela+' where TituloFinanceiro='+titulotemp.get_codigo+' and (Estado_dcto>0 or baixado=1)';
                                sql.add(strtemp);
                                open;
                                If (Fieldbyname('CNT').asinteger>0)
                                Then Begin
                                          Messagedlg('N�o � poss�vel Lan�ar em Pend�ncias de Doctos Baixados ou Devolvidos!',mterror,[mbok],0);
                                          result:=False;
                                          exit;
                                     End;

                                close;
                                sql.clear;
                                If (Self.TipoLancto.Get_Tipo='D')
                                Then sql.add('Update '+titulotemp.get_geradortabela+' set  baixado=2 where TituloFinanceiro='+titulotemp.Get_CODIGO)
                                Else sql.add('Update '+titulotemp.get_geradortabela+' set  baixado=2 where TituloFinanceiro='+titulotemp.Get_CODIGO);
                                execsql;

                                //seleciona na tabpendencias deste titulo para saber se
                                //tem algum saldo ainda, senao, seto o baixado dos dctos para o valor 1

                                close;
                                sql.clear;
                                sql.add('Select SUM(saldo) AS SOMA_SALDO  from TabPendencia where Titulo='+titulotemp.Get_CODIGO);
                                open;


                                if (FieldByname('SOMA_Saldo').asinteger=0)
                                Then Begin
                                          close;
                                          sql.clear;
                                          sql.add('Update '+titulotemp.get_geradortabela+' set baixado=1 where TituloFinanceiro='+titulotemp.Get_CODIGO);
                                          execsql;
                                     End;
                        End;
                  Except
                        Messagedlg('Erro durante a Cria��o da Query de Atualiza��o de Dctos',mterror,[mbok],0);
                        result:=False;
                        exit;
                  End;

               Finally
                      If (qtemp<>nil)
                      Then freeandnil(qtemp);
               End;

          End;

     Finally
            Titulotemp.free;

     End;

result:=true;

end;

//Usado para exportar para a contabilidade quando faco um lancto
//que nao gera valor como � o caso de desconto e juros
//tenho que usar a conta do plano de contas
//que estiver cadastrado no GeraLancamento que chamou
Function TObjLancamento.ExportaContabilidade_NaoGeraValor(PCodContabilNGeraVl:string): Boolean;
var
PCCredorDevedor,PCLancto:string;
begin
     result:=False;
     Self.TabelaparaObjeto;
     Self.objExportaContabilidade.ZerarTabela;
     Self.ObjExportaContabilidade.Submit_Exportado('N');
     Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
     Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);


     PCLancto:=PCodContabilNGeraVl;


     //preciso localizar se tenho que solicitar o c�digo do plano de contas para o desconto ou juros

     if (PCLancto='')//nao veio junto o codigo
     Then begin
             If (ObjParametroGlobal.ValidaParametro('PLANO DE CONTAS PARA LAN�AMENTOS QUE N�O GERAM VL')=False)
             Then Begin
                     Messagedlg('"PLANO DE CONTAS PARA LAN�AMENTOS QUE N�O GERAM VL" "VALOR=SIM OU N�O" n�o encontrado na tabela de parametros!',mterror,[mbok],0);
                     exit;
             End
             Else Begin
                     
                     If (ObjParametroGlobal.Get_Valor='SIM')
                     Then Begin
                                //se o parametro que vem junto   PCodContabilNGeraVl=''
                                //eu pego o codigo contabil por um form
                                //senao � porque o codigo da contabilidade veio junto
                                //na chamada do procedimento
                                If PCodContabilNGeraVl=''
                                Then PCLancto:=Self.PegaCodigoPlanodeContas
                                Else PcLancto:=PCodContabilNGeraVl;
                     End;
             End;
     End;

     PCCredorDevedor:='';
     self.ObjExportaContabilidade.Submit_Exporta('N');


     if (Self.Pendencia.Titulo.Get_ExportaLanctoContaGer='N')
     Then Begin
              If (Self.Pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
              Then PCCredorDevedor:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.Pendencia.Titulo.credordevedor.get_codigo,Self.Pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
              
              If (PCCredorDevedor='')
              Then PCCredorDevedor:='0'
              Else Begin
                      If (Self.Pendencia.Titulo.Objplanodecontas.LocalizaCodigo(PCCredorDevedor)=False)
                      Then PCCredorDevedor:='0'
                      Else Begin
                              Self.Pendencia.Titulo.Objplanodecontas.TabelaparaObjeto;
                              self.ObjExportaContabilidade.Submit_Exporta('S');
                      End;
              End;
     End
     Else Begin//utilizo a conta gerencial e nao o fornecedor quando o Titulo tem o campo exporta lancto conta ger='S'
               PCCredorDevedor:=Self.Pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
     End;



     If (PCLancto<>'') //codigo do lancamento de juros ou desconto
     Then Begin
             If (Self.Pendencia.Titulo.Objplanodecontas.LocalizaCodigo(PCLancto)=False)
             Then ObjExportaContabilidade.Submit_Exporta('N')
             Else Self.Pendencia.Titulo.Objplanodecontas.TabelaparaObjeto;
          End
     Else ObjExportaContabilidade.Submit_Exporta('N');

     
     //**************************************
     //preciso gravar os dados principais na exporta��o
     //Inserindo os dados
     ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
     ObjExportaContabilidade.Submit_Data(Self.Data);

     If(Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
     Then Begin//CONTAS A PAGAR
               //Debito o fornecedor e credito o banco ou a COnta Cont. X
               If (Self.TipoLancto.Get_Tipo='D')
               Then Begin
                     ObjExportaContabilidade.Submit_ContaCredite(PCLancto);
                     ObjExportaContabilidade.Submit_ContaDebite(PCCredorDevedor);
                    End
               Else Begin
                         ObjExportaContabilidade.Submit_ContaCredite(PCCredorDevedor);
                         ObjExportaContabilidade.Submit_ContaDebite(PCLancto);
                    End;

          End
     Else Begin//CONTAS A RECEBER
               If (Self.TipoLancto.Get_Tipo='D')//Desconto
               Then Begin
                         //Debita Conta Juros e Credito o Cliente
                         ObjExportaContabilidade.Submit_ContaDebite(PCLancto);
                         ObjExportaContabilidade.Submit_ContaCredite(PCCredorDevedor);
               End
               Else Begin//Juros
                         //Debita Cliente e Credita Juros
                         ObjExportaContabilidade.Submit_ContaDebite(PCCredorDevedor);
                         ObjExportaContabilidade.Submit_ContaCredite(PCLancto);
               End;
          End;

     ObjExportaContabilidade.Submit_Valor(Self.VALOR);
     ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);

     ObjExportaContabilidade.Status:=dsinsert;
     Result:=ObjExportaContabilidade.Salvar(False);
     ObjExportaContabilidade.Status:=dsInactive;

end;


//Usado para Exportar o que foi recebido de algum lancamento
function TObjLancamento.ExportaContabilidade_Recebimento: boolean;
Var
UsaPartidaSimples:boolean;
QuantPort:integer;
PcontaTransicao,PContaCredito,PContaDebito:string;
ST_Banco_CP,St_valor_CP,St_CodigoPlanoContas_CP,ST_historico_CP:TStringlist;
ST_NUMDCTO_CP,ST_Banco_CR,St_valor_CR,St_CodigoPlanoContas_CR,ST_historico_CR:TStringlist;
Begin
     Result:=False;

     If (ObjParametroGlobal.ValidaParametro('1-PARTIDA SIMPLES OU 2-CONTA DE TRANSI��O PARA TROCO?')=False)
     Then exit;

     UsaPartidaSimples:=False;
     IF (ObjParametroGlobal.Get_Valor='1')
     Then UsaPartidaSimples:=True
     Else Begin
               if (ObjParametroGlobal.ValidaParametro('CONTA TRANSI��O NO TROCO A RECEBER')=False)
               Then exit;

               PcontaTransicao:=ObjParametroGlobal.Get_Valor;
     End;


     Try
        ST_Banco_CP:=TStringList.create;
        St_valor_CP:=TStringList.create;
        St_CodigoPlanoContas_CP:=TStringList.create;
        ST_historico_CP:=TStringList.create;
        ST_NUMDCTO_CP:=TStringList.create;

        ST_Banco_CR:=TStringList.create;
        St_valor_CR:=TStringList.create;
        St_CodigoPlanoContas_CR:=TStringList.create;
        ST_historico_CR:=TStringList.create;

     Except
         Messagedlg('Erro na tentativa de Cria��o das StringLists',mterror,[mbok],0);
         exit;
     End;


     Try

        //Resgatando os portadores e valores recebidos, assim como
        //os pagos em caso de troco
        
        If (Self.Retorna_contabilidade_recebimento_por_banco(Self.codigo,ST_Banco_CR,St_CodigoPlanoContas_CR,St_valor_CR,ST_historico_CR)=False)
        Then Begin
                  Messagedlg('Erro na tentativa de Resgatar os Valores recebidos por portador, para gerar a contabilidade',mterror,[mbok],0);
                  exit;
        End;

        //troco (a pagar)
        if (Self.Retorna_contabilidade_pagamento_por_banco(Self.Codigo,ST_Banco_CP,St_CodigoPlanoContas_CP,St_valor_CP,ST_historico_CP,ST_NUMDCTO_CP,false)=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel retornar os lan�amentos de TROCO para gerar a Contabilidade!',mterror,[mbok],0);
                  exit;
        End;


        if (ST_Banco_CP.Count=0)
        Then Begin//nao tem troco
                  //* * * * * * * * * * * * * * * * * * * * * * * * * * * *
                  //Se nao existir o Troco, lanco partida dobrada
                  //Exemplo
                  //Credito o Fornecedor/Cliente
                  //Debito  o Banco
                  //* * * * * * * * * * * * * * * * * * * * * * * * * * * *


                  PcontaCredito:='';

                  if (Self.Pendencia.Titulo.Get_ExportaLanctoContaGer='N')
                  Then Begin
                            If (Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
                            Then PcontaCredito:=Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.pendencia.Titulo.credordevedor.get_codigo,Self.pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
                  End
                  Else Begin
                            //o campo exportalanctocontager indica que no momento da quitacao
                            //nao devo usar uma conta do credor/devedor e sim da contagerencial
                            //isso � utilizado em cheques devolvidos, onde os lanctos sao feitos manual
                            //e no momento de quitacao � utilizado a contagerencial para acertar
                            PcontaCredito:=Self.Pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
                  End;

                  //Dados basicos iniciais para a exportacao
                  Self.objExportaContabilidade.ZerarTabela;
                  Self.ObjExportaContabilidade.Submit_Exportado('N');
                  Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                  Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                  self.ObjExportaContabilidade.Submit_Exporta('N');
                  self.ObjexportaContabilidade.Submit_ContaCredite(PContaCredito);

                  For  quantport:=0 to ST_Banco_CR.Count-1 do
                  Begin
                       PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                       //preciso gravar os dados principais na exporta��o
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                      Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,false)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;

        End
        Else Begin//tem troco
                 //* * * * * * * * * * * * * * * * * * * * * * * * * * * *
                 //Se Existir o troco, tenho duas possibilidades
                 //1� - Partida Simples
                       //Lancamento 50,00
                       //Lanco 60,00 de entrada no banco
                       //Lanco 10,00 de saida no caixa

                       //Debito      Credito
                       //60 BANCO
                       //             50 CLIENTE
                       //             10 Caixa

                 //2� - Com caixa compensacao
                        //Debito    Credito
                        //60 BANCO   60 CONTA COMP
                        //10 CONTA   10 CAIXA
                        //50 CONTA   50 CLIENTE
                 //* * * * * * * * * * * * * * * * * * * * * * * * * * * *

                 if (UsaPartidaSimples=true)
                 Then begin//usa partida simples
                           //Lancando os Recebimentos nos bancos sem os creditos
                           //Lancando o Credito no Cliente/Fornecedor sem Debito
                           //Lancando o Troco sem Debito
                           //Exemplo
                           //DEBITo           CREDITO
                           //BANCO1               0
                           //banco2               0
                           //banco3               0
                           //                   CLIENTE
                           //                   BANCOTROCO
                           //                   BANCOTROCO

                           PcontaCredito:='';

                           if (Self.Pendencia.Titulo.Get_ExportaLanctoContaGer='N')
                           Then Begin
                                    If (Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
                                    Then PcontaCredito:=Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.pendencia.Titulo.credordevedor.get_codigo,Self.pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
                           End
                           Else Begin
                                     PcontaCredito:=Self.pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
                           End;

                           //EXPORTANDO OS DEBITOS DOS BANCOS, SEM A CONTRA PARTIDA DO CREDITO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           //exportando apenas os Debitos (Recebimento nos Bancos)
                           For  quantport:=0 to ST_Banco_CR.Count-1 do
                           Begin
                                PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                                Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                                Self.ObjExportaContabilidade.Status:=dsinsert;

                                If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;

                           //EXPORTANDO O LANCAMENTO NO FORNECEDOR/CLIENTE
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                           Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                           Self.ObjExportaContabilidade.Submit_Valor(Self.Valor);//valor do lancamento
                           Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           self.ObjexportaContabilidade.Submit_ContaCredite(PContaCredito);
                           Self.ObjExportaContabilidade.Status:=dsinsert;
                           If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                           Then Begin
                                       result:=False;
                                       exit;
                           End;
                           Self.ObjExportaContabilidade.Status:=dsInactive;

                           //EXPORTANDO OS DEBITOS DOS BANCOS (TROCO) SEM A CONTRA PARTIDA DO DEBITO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');

                           For  quantport:=0 to ST_Banco_CP.Count-1 do
                           Begin
                                PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                                Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                                Self.ObjExportaContabilidade.Submit_NumDocto(ST_NUMDCTO_CP[quantport]);
                                Self.ObjExportaContabilidade.Status:=dsinsert;



                                If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;
  
                 End
                 Else Begin//usando a conta transicao
                            //* * * * * * *Conta compensacao* * * * * * * * *
                            //Debito    Credito
                            //60 BANCO   60 CONTA COMP
                            //10 CONTA   10 CAIXA
                            //50 CONTA   50 CLIENTE
                            //* * * * * * * * * * * * * * * * * * * * * * * * * *

                           PcontaCredito:='';
                           if (Self.Pendencia.Titulo.Get_ExportaLanctoContaGer='N')
                           Then Begin
                                    If (Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
                                    Then PcontaCredito:=Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.pendencia.Titulo.credordevedor.get_codigo,Self.pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
                           End
                           Else Begin
                                     PcontaCredito:=Self.pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
                           End;

                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           Self.ObjExportaContabilidade.Submit_ContaCredite(PcontaTransicao);
                           //exportando apenas os Debitos (Recebimento nos Bancos)
                           For  quantport:=0 to ST_Banco_CR.Count-1 do
                           Begin
                                PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                                Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                                Self.ObjExportaContabilidade.Status:=dsinsert;

                                If (Self.ObjExportaContabilidade.Salvar(False,false)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;

                           //EXPORTANDO O LANCAMENTO NO FORNECEDOR/CLIENTE Com Conta TRANSICAO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                           Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                           Self.ObjExportaContabilidade.Submit_Valor(Self.Valor);//valor do lancamento
                           Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           self.ObjexportaContabilidade.Submit_ContaCredite(PContaCredito);
                           Self.ObjExportaContabilidade.Submit_ContaDebite(PcontaTransicao);
                           Self.ObjExportaContabilidade.Status:=dsinsert;
                           If (Self.ObjExportaContabilidade.Salvar(False,false)=False)
                           Then Begin
                                       result:=False;
                                       exit;
                           End;
                           Self.ObjExportaContabilidade.Status:=dsInactive;

                           //EXPORTANDO OS CREDITOS DOS BANCOS (TROCO) SEM A CONTRA PARTIDA DO DEBITO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           //exportando apenas os Debitos (Recebimento nos Bancos)
                           For  quantport:=0 to ST_Banco_CP.Count-1 do
                           Begin
                                PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                                Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                                Self.ObjExportaContabilidade.Submit_ContaDebite(PcontaTransicao);
                                Self.ObjExportaContabilidade.Submit_NumDocto(ST_NUMDCTO_CP[quantport]);

                                Self.ObjExportaContabilidade.Status:=dsinsert;

                                If (Self.ObjExportaContabilidade.Salvar(False,false)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;
                 End;//conta de transicao
        End;//troco

        result:=true;
        exit;
     Finally
            Freeandnil(ST_Banco_CP);
            Freeandnil(St_valor_CP);
            Freeandnil(St_CodigoPlanoContas_CP);
            Freeandnil(ST_historico_CP);
            Freeandnil(ST_NUMDCTO_CP);

            Freeandnil(ST_Banco_CR);
            Freeandnil(St_valor_CR);
            Freeandnil(St_CodigoPlanoContas_CR);
            Freeandnil(ST_historico_CR);
     End;
End;


//Usado para Exportar o que foi pago de algum lancamento
function TObjLancamento.ExportaContabilidade_Pagamento: boolean;
Var
UsaPartidaSimples:boolean;
QuantPort:integer;
PcontaTransicao,PContaCredito,PContaDebito:string;
St_NUMDCTO_CP,ST_Banco_CP,St_valor_CP,St_CodigoPlanoContas_CP,ST_historico_CP:TStringlist;
ST_Banco_CR,St_valor_CR,St_CodigoPlanoContas_CR,ST_historico_CR:TStringlist;
Begin

     Result:=False;


     If (ObjParametroGlobal.ValidaParametro('1-PARTIDA SIMPLES OU 2-CONTA DE TRANSI��O PARA TROCO?')=False)
     Then exit;

     UsaPartidaSimples:=False;
     IF (ObjParametroGlobal.Get_Valor='1')
     Then UsaPartidaSimples:=True
     Else Begin
               if (ObjParametroGlobal.ValidaParametro('CONTA TRANSI��O NO TROCO A PAGAR')=False)
               Then exit;
               PcontaTransicao:=ObjParametroGlobal.Get_Valor;
     End;

     Try
        ST_Banco_CP:=TStringList.create;
        St_valor_CP:=TStringList.create;
        St_CodigoPlanoContas_CP:=TStringList.create;
        ST_historico_CP:=TStringList.create;
        St_NUMDCTO_CP:=TStringList.create;
        

        ST_Banco_CR:=TStringList.create;
        St_valor_CR:=TStringList.create;
        St_CodigoPlanoContas_CR:=TStringList.create;
        ST_historico_CR:=TStringList.create;

     Except
         Messagedlg('Erro na tentativa de Cria��o das StringLists',mterror,[mbok],0);
         exit;
     End;


     Try

        //Resgatando os portadores e valores recebidos, assim como
        //os pagos em caso de troco
        
        If (Self.Retorna_contabilidade_recebimento_por_banco(Self.codigo,ST_Banco_CR,St_CodigoPlanoContas_CR,St_valor_CR,ST_historico_CR)=False)
        Then Begin
                  Messagedlg('Erro na tentativa de Resgatar os Valores recebidos por portador (troco) para gerar a contabilidade',mterror,[mbok],0);
                  exit;
        End;

        if (Self.Retorna_contabilidade_pagamento_por_banco(Self.Codigo,ST_Banco_CP,St_CodigoPlanoContas_CP,St_valor_CP,ST_historico_CP,St_NUMDCTO_CP,false)=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel retornar os lan�amentos dos bancos utilizados!',mterror,[mbok],0);
                  exit;
        End;


        if (ST_Banco_CR.Count=0)
        Then Begin//nao tem recebimento em banco nenhum entao nao tem troco
        
                  //* * * * * * * * * * * * * * * * * * * * * * * * * * * *
                  //Lanco partida dobrada
                  //Debito   o Fornecedor/Cliente
                  //Credito  o Banco
                  //* * * * * * * * * * * * * * * * * * * * * * * * * * * *

                  PcontaDebito:='';

                  if (Self.Pendencia.Titulo.Get_ExportaLanctoContaGer='N')
                  Then Begin
                            If (Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
                            Then PContaDebito:=Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.pendencia.Titulo.credordevedor.get_codigo,Self.pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
                  End
                  Else Begin
                            //nesse caso utiliza a conta gerencial e n�o o fornecedor
                            PContaDebito:=Self.pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
                  End;

                  //Dados basicos iniciais para a exportacao
                  Self.objExportaContabilidade.ZerarTabela;
                  Self.ObjExportaContabilidade.Submit_Exportado('N');
                  Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                  Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                  self.ObjExportaContabilidade.Submit_Exporta('N');
                  self.ObjexportaContabilidade.Submit_ContaDebite(PContaDebito);

                  For  quantport:=0 to ST_Banco_CP.Count-1 do
                  Begin
                      PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                               
                      {1 - No momento da quitacao Debito Fornecedor e credito o portador do cheque, ai no momento
                      da baixa do cheque (compensacao) nao tem lancto contabil
                      2 - No momento da quitacao Debito o Fornecedor e Credito uma conta de cheques a compensar
                      no momento da baixa, debito cheques a compensar e credito o banco do cheque}

                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(ST_historico_CP[quantport]);
                      Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                      Self.ObjExportaContabilidade.Submit_NumDocto(St_NUMDCTO_CP[quantport]);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,False)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                      
                  End;
        End
        Else Begin//tem troco
                 //* * * * * * * * * * * * * * * * * * * * * * * * * * * *
                 //Se Existir o troco, tenho duas possibilidades
                 //1� - Partida Simples
                       //Lancamento 50,00
                       //Lanco 60,00 de entrada no banco
                       //Lanco 10,00 de saida no caixa

                       //Debito      Credito
                       //60 BANCO
                       //             50 CLIENTE
                       //             10 Caixa
                 //ccccccccccccccccccCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCcccccccccccc
                 //2� - Com caixa compensacao
                        //Debito    Credito
                        //60 BANCO   60 CONTA 
                        //10 CONTA   10 CAIXA
                        //50 CONTA   50 CLIENTE
                 //* * * * * * * * * * * * * * * * * * * * * * * * * * * *

                 if (UsaPartidaSimples=true)
                 Then begin//usa partida simples
                           //Lancando os Recebimentos nos bancos sem os creditos
                           //Lancando o Credito no Cliente/Fornecedor sem Debito
                           //Lancando o Troco sem Debito
                           //Exemplo
                           //DEBITO           CREDITO
                           //BANCO1               0
                           //banco2               0
                           //banco3               0
                           //                   CLIENTE
                           //                   BANCOTROCO
                           //                   BANCOTROCO

                           PcontaDebito:='';

                           if (Self.Pendencia.Titulo.Get_ExportaLanctoContaGer='N')
                           Then Begin
                                    If (Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
                                    Then PcontaDebito:=Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.pendencia.Titulo.credordevedor.get_codigo,Self.pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
                           End
                           Else Begin
                                    PcontaDebito:=Self.pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
                           End;

                           //EXPORTANDO OS DEBITOS DOS BANCOS, SEM A CONTRA PARTIDA DO DEBITo
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           //exportando apenas os Creditos (Pagamento nos Bancos)
                           For  quantport:=0 to ST_Banco_CP.Count-1 do
                           Begin
                                PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico(ST_historico_CP[quantport]);
                                Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                                Self.ObjExportaContabilidade.Submit_NumDocto(St_NUMDCTO_CP[quantport]);
                                Self.ObjExportaContabilidade.Status:=dsinsert;

                                If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;

                           //EXPORTANDO O LANCAMENTO NO FORNECEDOR/CLIENTE
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                           Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                           Self.ObjExportaContabilidade.Submit_Valor(Self.Valor);//valor do lancamento
                           Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           self.ObjexportaContabilidade.Submit_ContaDebite(PContaDebito);
                           Self.ObjExportaContabilidade.Status:=dsinsert;
                           If (Self.ObjExportaContabilidade.Salvar(False)=False)
                           Then Begin
                                       result:=False;
                                       exit;
                           End;
                           Self.ObjExportaContabilidade.Status:=dsInactive;

                           //EXPORTANDO OS DEBITOS DOS BANCOS (TROCO) SEM A CONTRA PARTIDA DO CREDITO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');

                           For  quantport:=0 to ST_Banco_CR.Count-1 do
                           Begin
                                PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                                Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                                Self.ObjExportaContabilidade.Status:=dsinsert;

                                If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;
  
                 End
                 Else Begin//usando a conta transicao
                            //* * * * * * *Conta compensacao* * * * * * * * *
                            //Credito    Debito
                            //60 BANCO   60 CONTA COMP
                            //10 CONTA   10 CAIXA TROCo
                            //50 CONTA   50 CLIENTE
                            //* * * * * * * * * * * * * * * * * * * * * * * * * *

                           PcontaDebito:='';
                           if (Self.Pendencia.Titulo.Get_ExportaLanctoContaGer='N')
                           Then Begin
                                    If (Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
                                    Then PcontaDebito:=Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.pendencia.Titulo.credordevedor.get_codigo,Self.pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
                           End
                           Else Begin
                                     PcontaDebito:=Self.pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
                           End;

                           //EXPORTANDO OS DEBITOS DOS BANCOS, COM A CONTRA PARTIDA CONTA COMPENSACAO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           Self.ObjExportaContabilidade.Submit_ContaDebite(PcontaTransicao);
                           //exportando apenas os Debitos (Recebimento nos Bancos)
                           For  quantport:=0 to ST_Banco_CP.Count-1 do
                           Begin
                                PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico(ST_historico_CP[quantport]);
                                Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                                Self.ObjExportaContabilidade.Submit_NumDocto(St_NUMDCTO_CP[quantport]);
                                Self.ObjExportaContabilidade.Status:=dsinsert;

                                If (Self.ObjExportaContabilidade.Salvar(False,false)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;

                           //EXPORTANDO O LANCAMENTO NO FORNECEDOR/CLIENTE Com Conta TRANSICAO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                           Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                           Self.ObjExportaContabilidade.Submit_Valor(Self.Valor);//valor do lancamento
                           Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           self.ObjexportaContabilidade.Submit_ContaDebite(PContaDebito);//cliente
                           Self.ObjExportaContabilidade.Submit_ContaCredite(PcontaTransicao);//conta transicao
                           Self.ObjExportaContabilidade.Status:=dsinsert;
                           If (Self.ObjExportaContabilidade.Salvar(False,False)=False)
                           Then Begin
                                       result:=False;
                                       exit;
                           End;
                           Self.ObjExportaContabilidade.Status:=dsInactive;

                           //EXPORTANDO OS CREDITOS DOS BANCOS (TROCO) SEM A CONTRA PARTIDA DO DEBITO
                           Self.objExportaContabilidade.ZerarTabela;
                           Self.ObjExportaContabilidade.Submit_Exportado('N');
                           Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                           Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                           self.ObjExportaContabilidade.Submit_Exporta('S');
                           For  quantport:=0 to ST_Banco_CR.Count-1 do
                           Begin
                                PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                                //preciso gravar os dados principais na exporta��o
                                Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                                Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                                Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                                Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                                Self.ObjExportaContabilidade.Submit_ContaCredite(PcontaTransicao);
                                Self.ObjExportaContabilidade.Submit_ContaDebite(PcontaDebito);
                                Self.ObjExportaContabilidade.Status:=dsinsert;

                                If (Self.ObjExportaContabilidade.Salvar(False,False)=False)
                                Then Begin
                                       result:=False;
                                       exit;
                                End;
                                Self.ObjExportaContabilidade.Status:=dsInactive;
                           End;
                 End;
        End;

        result:=true;
        exit;
     Finally
            Freeandnil(ST_Banco_CP);
            Freeandnil(St_valor_CP);
            Freeandnil(St_CodigoPlanoContas_CP);
            Freeandnil(ST_historico_CP);
            freeandnil(St_NUMDCTO_CP);

            Freeandnil(ST_Banco_CR);
            Freeandnil(St_valor_CR);
            Freeandnil(St_CodigoPlanoContas_CR);
            Freeandnil(ST_historico_CR);
     End;
End;

function TObjLancamento.ExportaContabilidade_Pagamento_Lote(PlancamentoPai: string): boolean;
Var
UsaPartidaSimplesPagamento:boolean;
QuantPort:integer;
PcontaTransicaoPagamento,PContaCredito,PContaDebito:string;
ST_NUMDCTO_CP,ST_Banco_CP,St_valor_CP,St_CodigoPlanoContas_CP,ST_historico_CP:TStringlist;
ST_Banco_CR,St_valor_CR,St_CodigoPlanoContas_CR,ST_historico_CR:TStringlist;
St_Lancamento_Filho,St_CodigoPlanoContas_Pend,ST_Valor_Pend,St_Historico_Pend:TStringlist;
Begin
     
     Result:=False;


     UsaPartidaSimplesPagamento:=False;
     If (ObjParametroGlobal.ValidaParametro('EXPORTA CONTABILIDADE EM LOTE COM CONTA DE TRANSI��O?')=False)
     Then exit;

     UsaPartidaSimplesPagamento:=True;
     if (ObjParametroGlobal.Get_Valor='SIM')
     Then Begin
               //usando conta de transicao
               UsaPartidaSimplesPagamento:=False;
               If (ObjParametroGlobal.ValidaParametro('CONTA DE TRANSI��O PARA LOTES A PAGAR')=False)
               Then exit;


               PcontaTransicaoPagamento:=ObjParametroGlobal.Get_Valor;
     End;


     Try
        ST_Banco_CP:=TStringList.create;
        St_valor_CP:=TStringList.create;
        St_CodigoPlanoContas_CP:=TStringList.create;
        ST_historico_CP:=TStringList.create;
        ST_NUMDCTO_CP:=TStringList.create;

        ST_Banco_CR:=TStringList.create;
        St_valor_CR:=TStringList.create;
        St_CodigoPlanoContas_CR:=TStringList.create;
        ST_historico_CR:=TStringList.create;

        St_CodigoPlanoContas_Pend:=TStringlist.Create;
        ST_Valor_Pend:=TStringlist.Create;
        St_Historico_Pend:=TStringlist.Create;
        St_Lancamento_Filho:=TStringlist.Create;
     Except
         Messagedlg('Erro na tentativa de Cria��o das StringLists',mterror,[mbok],0);
         exit;
     End;


     Try


        If (Self.Retorna_contabilidade_recebimento_por_banco(PlancamentoPai,ST_Banco_CR,St_CodigoPlanoContas_CR,St_valor_CR,ST_historico_CR)=False)
        Then Begin
                  Messagedlg('Erro na tentativa de Resgatar os recebimentos por portador, para gerar a contabilidade',mterror,[mbok],0);
                  exit;
        End;

        if (Self.Retorna_contabilidade_pagamento_por_banco(PlancamentoPai,ST_Banco_CP,St_CodigoPlanoContas_CP,St_valor_CP,ST_historico_CP,ST_NUMDCTO_CP,true)=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel retornar os pagamentos para gerar a Contabilidade!',mterror,[mbok],0);
                  exit;
        End;

        if (Self.Retorna_Contabilidade_Pendencias_lote(PlancamentoPai,St_Lancamento_Filho,St_CodigoPlanoContas_Pend,ST_Valor_Pend,St_Historico_Pend)=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel retornar as pend�ncias envolvidas no pagamento em Lote',mterror,[mbok],0);
                  exit;
        end;

        If (self.LocalizaCodigo(PlancamentoPai)=false)
        Then Begin
                  Messagedlg('O Lan�amento que originou a quita��o n�o foi encontrado!',mterror,[mbok],0);
                  exit;
        end;
        Self.TabelaparaObjeto;


        //* * * * * * * * * * * * * * * * * * * * * * * * * * * *
        //TENHO DUAS POSSIBILIDADES, PARTIDA SIMPLES OU COM CONTA DE TRANSICAO
        //* * * * * * * * * * * * * * * * * * * * * * * * * * * *

        IF (UsaPartidaSimplesPagamento=true)//partida simples
        Then Begin
                  //* * * * * * * * * * * * * * * * * * * * * * * * *
                  //Debito    Credito
                  //            BANCOS
                  //CLIENTES
                  //* * * * * * * * * * * * * * * * * * * * * * * * *

                  //exportando os pagamentos (Credito na contabilidade)
                  For  quantport:=0 to ST_Banco_CP.Count-1 do
                  Begin
                      PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);//Lancamento Pai
                      Self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                      Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                      Self.ObjExportaContabilidade.Submit_NumDocto(ST_NUMDCTO_CP[quantport]);

                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;

                  //exportando os lancamentos filhos (debito na contabilidade)
                  For  quantport:=0 to St_Lancamento_Filho.Count-1 do
                  Begin
                      PContaDebito:=St_CodigoPlanoContas_Pend[quantport];
                       //preciso gravar os dados principais na exporta��o
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(St_Lancamento_Filho[quantport]);//Lancamento Pai
                      self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(ST_Valor_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(St_Historico_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
                  
                  //TROCO, LANCO O BANCO COMO DEBITO (o dinheiro entrou)
                  For  quantport:=0 to ST_Banco_CR.Count-1 do
                  Begin
                       PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                       Self.objExportaContabilidade.ZerarTabela;
                       Self.ObjExportaContabilidade.Submit_Exportado('N');
                       Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                       Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                       self.ObjExportaContabilidade.Submit_Exporta('S');
                       Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                       Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                       Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                       Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                       Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                       Self.ObjExportaContabilidade.Status:=dsinsert;

                       If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                       Then Begin
                              result:=False;
                              exit;
                       End;
                       Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
        end
        Else Begin//conta de transicao
                  //* * * * * * * * * * * * * * * * * * * *
                  {
                   Debito     Credito
                   CONTA_TR   BANCO
                   CLIENTE    CONTA_TR
                  }
                  //* * * * * * * * * * * * * * * * * * * *

                  //exportando os pagamentos (Credito na contabilidade)
                  For  quantport:=0 to ST_Banco_CP.Count-1 do
                  Begin
                      PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);//Lancamento Pai
                      Self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                      Self.ObjExportaContabilidade.Submit_ContaDebite(PcontaTransicaoPagamento);
                      Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                      Self.ObjExportaContabilidade.Submit_Numdocto(ST_NUMDCTO_CP[quantport]);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;

                  //exportando os lancamentos filhos (debito na contabilidade)
                  For  quantport:=0 to St_Lancamento_Filho.Count-1 do
                  Begin
                      PContaDebito:=St_CodigoPlanoContas_Pend[quantport];
                       //preciso gravar os dados principais na exporta��o
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(St_Lancamento_Filho[quantport]);//Lancamento Pai
                      self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(ST_Valor_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(St_Historico_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                      Self.ObjExportaContabilidade.Submit_ContaCredite(PcontaTransicaoPagamento);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
                  //TROCO, LANCO O BANCO COMO DEBITO (o dinheiro entrou)
                  For  quantport:=0 to ST_Banco_CR.Count-1 do
                  Begin
                       PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                       Self.objExportaContabilidade.ZerarTabela;
                       Self.ObjExportaContabilidade.Submit_Exportado('N');
                       Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                       Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                       self.ObjExportaContabilidade.Submit_Exporta('S');
                       Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                       Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                       Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                       Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                       Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                       Self.ObjExportaContabilidade.Submit_ContaCredite(PcontaTransicaoPagamento);
                       Self.ObjExportaContabilidade.Status:=dsinsert;

                       If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                       Then Begin
                              result:=False;
                              exit;
                       End;
                       Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
        End;

        result:=true;
        exit;
     Finally
            Freeandnil(ST_Banco_CP);
            Freeandnil(St_valor_CP);
            Freeandnil(St_CodigoPlanoContas_CP);
            Freeandnil(ST_historico_CP);
            Freeandnil(ST_NUMDCTO_CP);

            Freeandnil(ST_Banco_CR);
            Freeandnil(St_valor_CR);
            Freeandnil(St_CodigoPlanoContas_CR);
            Freeandnil(ST_historico_CR);

            Freeandnil(St_CodigoPlanoContas_Pend);
            Freeandnil(ST_Valor_Pend);
            Freeandnil(St_Historico_Pend);
            Freeandnil(St_Lancamento_Filho);
     End;
End;

function TObjLancamento.ExportaContabilidade_Recebimento_Lote(PlancamentoPai: string): boolean;
Var
UsaPartidaSimplesRecebimento:boolean;
QuantPort:integer;
PcontaTransicaoRecebimento,PContaCredito,PContaDebito:string;
ST_NUMDCTO_CP,ST_Banco_CP,St_valor_CP,St_CodigoPlanoContas_CP,ST_historico_CP:TStringlist;
ST_Banco_CR,St_valor_CR,St_CodigoPlanoContas_CR,ST_historico_CR:TStringlist;
St_Lancamento_Filho,St_CodigoPlanoContas_Pend,ST_Valor_Pend,St_Historico_Pend:TStringlist;
Begin
     
     Result:=False;


     {If (ObjParametroGlobal.ValidaParametro('1-PARTIDA SIMPLES OU 2-CONTA DE TRANSI��O PARA TROCO?')=False)
     Then exit;

     UsaPartidaSimplesTroco:=False;
     If (ObjParametroGlobal.Get_Valor='1')
     Then UsaPartidaSimplesTroco:=True
     Else Begin
               if (ObjParametroGlobal.ValidaParametro('CONTA TRANSI��O NO TROCO A PAGAR')=False)
               Then exit;

               PcontaTransicaoTroco:=ObjParametroGlobal.Get_Valor;
     End;}


     UsaPartidaSimplesRecebimento:=False;
     If (ObjParametroGlobal.ValidaParametro('EXPORTA CONTABILIDADE EM LOTE COM CONTA DE TRANSI��O?')=False)
     Then exit;

     UsaPartidaSimplesRecebimento:=True;
     if (ObjParametroGlobal.Get_Valor='SIM')
     Then Begin
               //usando conta de transicao
               UsaPartidaSimplesRecebimento:=False;
               If (ObjParametroGlobal.ValidaParametro('CONTA DE TRANSI��O PARA LOTES A RECEBER')=False)
               Then exit;
               PcontaTransicaoRecebimento:=ObjParametroGlobal.Get_Valor;
     End;

     Try
        ST_Banco_CP:=TStringList.create;
        St_valor_CP:=TStringList.create;
        St_CodigoPlanoContas_CP:=TStringList.create;
        ST_historico_CP:=TStringList.create;
        ST_NUMDCTO_CP:=TStringList.create;
        

        ST_Banco_CR:=TStringList.create;
        St_valor_CR:=TStringList.create;
        St_CodigoPlanoContas_CR:=TStringList.create;
        ST_historico_CR:=TStringList.create;

        St_CodigoPlanoContas_Pend:=TStringlist.Create;
        ST_Valor_Pend:=TStringlist.Create;
        St_Historico_Pend:=TStringlist.Create;
        St_Lancamento_Filho:=TStringlist.Create;
     Except
         Messagedlg('Erro na tentativa de Cria��o das StringLists',mterror,[mbok],0);
         exit;
     End;


     Try

        //Resgatando os portadores e valores recebidos, assim como
        //os pagos em caso de troco
        
        If (Self.Retorna_contabilidade_recebimento_por_banco(PlancamentoPai,ST_Banco_CR,St_CodigoPlanoContas_CR,St_valor_CR,ST_historico_CR)=False)
        Then Begin
                  Messagedlg('Erro na tentativa de Resgatar os Valores recebidos por portador, para gerar a contabilidade',mterror,[mbok],0);
                  exit;
        End;

        if (Self.Retorna_contabilidade_pagamento_por_banco(PlancamentoPai,ST_Banco_CP,St_CodigoPlanoContas_CP,St_valor_CP,ST_historico_CP,ST_NUMDCTO_CP,true)=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel retornar os lan�amentos de TROCO para gerar a Contabilidade!',mterror,[mbok],0);
                  exit;
        End;

        if (Self.Retorna_Contabilidade_Pendencias_lote(PlancamentoPai,St_Lancamento_Filho,St_CodigoPlanoContas_Pend,ST_Valor_Pend,St_Historico_Pend)=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel retornar as pend�ncias envolvidas no pagamento em Lote',mterror,[mbok],0);
                  exit;
        end;

        If (self.LocalizaCodigo(PlancamentoPai)=false)
        Then Begin
                  Messagedlg('O Lan�amento que originou a quita��o n�o foi encontrado!',mterror,[mbok],0);
                  exit;
        end;
        Self.TabelaparaObjeto;


        //* * * * * * * * * * * * * * * * * * * * * * * * * * * *
        //TENHO DUAS POSSIBILIDADES, PARTIDA SIMPLES OU COM CONTA DE TRANSICAO
        //* * * * * * * * * * * * * * * * * * * * * * * * * * * *

        IF (UsaPartidaSimplesRecebimento=true)//partida simples
        Then Begin
                  //* * * * * * * * * * * * * * * * * * * * * * * * *
                  //Debito    Credito
                  //            BANCOS
                  //CLIENTES
                  //* * * * * * * * * * * * * * * * * * * * * * * * *

                  //exportando os recebimentos (Debito na contabilidade)
                  For  quantport:=0 to ST_Banco_CR.Count-1 do
                  Begin
                      PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);//Lancamento Pai
                      Self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                      Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;

                  //exportando os lancamentos filhos (credito na contabilidade)
                  For  quantport:=0 to St_Lancamento_Filho.Count-1 do
                  Begin
                      PContaCredito:=St_CodigoPlanoContas_Pend[quantport];
                       //preciso gravar os dados principais na exporta��o
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(St_Lancamento_Filho[quantport]);//Lancamento Pai
                      self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(ST_Valor_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(St_Historico_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
                  //TROCO, LANCO O BANCO COMO CREDITO (o dinheiro saiu)
                  For  quantport:=0 to ST_Banco_CP.Count-1 do
                  Begin
                       PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                       Self.objExportaContabilidade.ZerarTabela;
                       Self.ObjExportaContabilidade.Submit_Exportado('N');
                       Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                       Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                       self.ObjExportaContabilidade.Submit_Exporta('S');
                       Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                       Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                       Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                       Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                       Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                       Self.ObjExportaContabilidade.Submit_NumDocto(ST_NUMDCTO_CP[quantport]);
                       Self.ObjExportaContabilidade.Status:=dsinsert;

                       If (Self.ObjExportaContabilidade.Salvar(False,true)=False)
                       Then Begin
                              result:=False;
                              exit;
                       End;
                       Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
        end
        Else Begin//conta de transicao
                  //* * * * * * * * * * * * * * * * * * * *
                  {
                   Credito     Debito
                   CONTA_TR    BANCO
                   CLIENTE    CONTA_TR
                  }
                  //* * * * * * * * * * * * * * * * * * * *
                  //exportando os recebimentos (Debito na contabilidade)
                  For  quantport:=0 to ST_Banco_CR.Count-1 do
                  Begin
                      PContaDebito:=St_CodigoPlanoContas_CR[quantport];
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);//Lancamento Pai
                      Self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(St_valor_CR[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);
                      Self.ObjExportaContabilidade.Submit_ContaDebite(PContaDebito);
                      Self.ObjExportaContabilidade.Submit_ContaCredite(PcontaTransicaoRecebimento);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,False)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;

                  //exportando os lancamentos filhos (credito na contabilidade)
                  For  quantport:=0 to St_Lancamento_Filho.Count-1 do
                  Begin
                      PContaCredito:=St_CodigoPlanoContas_Pend[quantport];
                       //preciso gravar os dados principais na exporta��o
                      Self.objExportaContabilidade.ZerarTabela;
                      Self.ObjExportaContabilidade.Submit_Exportado('N');
                      Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                      Self.ObjExportaContabilidade.Submit_CodGerador(St_Lancamento_Filho[quantport]);//Lancamento Pai
                      self.ObjExportaContabilidade.Submit_Exporta('N');
                      Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                      Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                      Self.ObjExportaContabilidade.Submit_Valor(ST_Valor_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_Historico(St_Historico_Pend[quantport]);
                      Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                      Self.ObjExportaContabilidade.Submit_ContaDebite(PcontaTransicaoRecebimento);
                      Self.ObjExportaContabilidade.Status:=dsinsert;

                      If (Self.ObjExportaContabilidade.Salvar(False,False)=False)
                      Then Begin
                              result:=False;
                              exit;
                      End;
                      Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
                  //TROCO, LANCO O BANCO COMO CREDITO (o dinheiro saiu)
                  For  quantport:=0 to ST_Banco_CP.Count-1 do
                  Begin
                       PContaCredito:=St_CodigoPlanoContas_CP[quantport];
                       Self.objExportaContabilidade.ZerarTabela;
                       Self.ObjExportaContabilidade.Submit_Exportado('N');
                       Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
                       Self.ObjExportaContabilidade.Submit_CodGerador(Self.codigo);
                       self.ObjExportaContabilidade.Submit_Exporta('S');
                       Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
                       Self.ObjExportaContabilidade.Submit_Data(Self.Data);
                       Self.ObjExportaContabilidade.Submit_Valor(St_valor_CP[quantport]);
                       Self.ObjExportaContabilidade.Submit_Historico('TROCO '+Self.HISTORICO);
                       Self.ObjExportaContabilidade.Submit_ContaCredite(PContaCredito);
                       Self.ObjExportaContabilidade.Submit_ContaDebite(PcontaTransicaoRecebimento);
                       Self.ObjExportaContabilidade.Submit_NumDocto(ST_NUMDCTO_CP[quantport]);
                       Self.ObjExportaContabilidade.Status:=dsinsert;

                       If (Self.ObjExportaContabilidade.Salvar(False,False)=False)
                       Then Begin
                              result:=False;
                              exit;
                       End;
                       Self.ObjExportaContabilidade.Status:=dsInactive;
                  End;
        End;

        result:=true;
        exit;
     Finally
            Freeandnil(ST_Banco_CP);
            Freeandnil(St_valor_CP);
            Freeandnil(St_CodigoPlanoContas_CP);
            Freeandnil(ST_historico_CP);
            Freeandnil(ST_NUMDCTO_CP);

            Freeandnil(ST_Banco_CR);
            Freeandnil(St_valor_CR);
            Freeandnil(St_CodigoPlanoContas_CR);
            Freeandnil(ST_historico_CR);

            Freeandnil(St_CodigoPlanoContas_Pend);
            Freeandnil(ST_Valor_Pend);
            Freeandnil(St_Historico_Pend);
            Freeandnil(St_Lancamento_Filho);
     End;
End;


function TObjLancamento.Retorna_contabilidade_recebimento_por_banco(
  Plancamento: string; Pbanco, PcodigoPlanoContas, Pvalor,
  Phistorico: TStringList): boolean;
Var
  ObjValores:TobjValores;
  QuantPort:integer;
  STlistPortador,StListValorPortador:TStringList;
Begin
     Result:=False;

     if (Self.LocalizaCodigo(plancamento)=False)
     Then Begin
               Messagedlg('Lan�amento n�o localizado',mterror,[mbok],0);
               exit;
     end;
     Self.TabelaparaObjeto;

     Try
        QuantPort:=0;
        ObjValores:=TObjValores.create;
        QuantPort:=1;
        STlistPortador:=TStringList.create;
        QuantPort:=2;
        StListValorPortador:=TStringList.create;
     Except
           Case quantport of
                0:Begin
                       Messagedlg('Erro na Cria��o do Objeto de Valores!',mterror,[mbok],0);
                       exit;
                End;
                1:Begin
                       ObjValores.free;
                       Messagedlg('Erro na Cria��o da lista de Portadores!',mterror,[mbok],0);
                       exit;
                End;
           End;
           exit;
     End;


     Try
        {funciona assim eu recebi 10,00 no caixa,20,00 no banco atraves de deposito
        uso o mesmo historico, porem divido o valor do lancamento
        conforme os valores recebidos em cada portador}

        If (ObjValores.Get_Valores_Portadores_Diferentes(Plancamento,STlistPortador,StListValorPortador)=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel resgatar os valores recebidos por portador!'+#13+'A Contabilidade n�o ser� exportada!',mterror,[mbok],0);
                  result:=True;
                  exit;
        End;

        Pbanco.clear;
        PcodigoPlanoContas.clear;
        Pvalor.clear;
        Phistorico.clear;

        ObjValores.PORTADOR.ZerarTabela;
        For  quantport:=0 to STlistPortador.Count-1 do
        Begin
              ObjValores.PORTADOR.ZerarTabela;
              ObjValores.PORTADOR.localizacodigo(STlistPortador.Strings[quantport]);
              ObjValores.PORTADOR.TabelaparaObjeto;

              Pbanco.add(ObjValores.PORTADOR.get_codigo);
              PcodigoPlanoContas.Add(ObjValores.Portador.Get_CodigoPlanodeContas);
              Pvalor.add(StListValorPortador[quantport]);
              Phistorico.Add(Self.historico);
        End;
        result:=True;
     Finally
            ObjValores.free;
            Freeandnil(STlistPortador);
            Freeandnil(StListValorPortador);
     End;
end;

function TObjLancamento.Retorna_contabilidade_pagamento_por_banco(Plancamento:string;Pbanco,PcodigoPlanoContas,Pvalor,Phistorico,PNumDcto:TStringList;PEmlote:boolean): boolean;
Var
ObjtransferenciaPortador:TObjTransferenciaPortador;
ObjtalaodeCheques:TObjTalaodeCheques;
QuantPort:integer;
ListaPortador_Din_ChTerc,Lista_Dinh_ChTerc,ListaPortador_Ch_Port,ListaChequePort,ListaCodigoCheque:TStringList;
PExportaChequedoPortadorPagamento:boolean;
PContaTransicaochequeportador:string;
Begin
     Result:=False;
     PExportaChequedoPortadorPagamento:=true;
     PContaTransicaochequeportador:='';

     if (ObjParametroGlobal.ValidaParametro('CONTA DE TRANSICAO NO CHEQUE DO PORTADOR')=true)
     Then Begin
               PContaTransicaochequeportador:=ObjParametroGlobal.get_valor;
               if (ObjLanctoPortadorGlobal.Portador.PlanodeContas.LocalizaCodigo(PContaTransicaochequeportador)=true)
               Then PExportaChequedoPortadorPagamento:=False
               Else PContaTransicaochequeportador:='';
     End;
     
    {este procedimento, recebe um lancamento de parametro, e procura todos os pagamentos
     que aconteceram neste lancamento, isto pode ser usado tanto em contas
     a receber quanto em contas a pagar, pois no a receber pode existir o troco
     e o troco usa-se transferencia de algum portador (caixa,banco...) para
     um portador de TROCO
     ele retorna, o banco, o codigo do plano de contas, valor e o historico que sera usado na contabilidade}


     if (Self.LocalizaCodigo(plancamento)=False)
     Then Begin
               Messagedlg('Lan�amento n�o localizado',mterror,[mbok],0);
               exit;
     end;
     Self.TabelaparaObjeto;

     Try
        ObjtransferenciaPortador:=TObjTransferenciaPortador.create;
        ObjtalaodeCheques:=TObjTalaodeCheques.Create;


        ListaPortador_Din_ChTerc:=TStringList.create;
        Lista_Dinh_ChTerc:=TStringList.create;

        ListaPortador_Ch_Port:=TStringList.create;
        ListaChequePort:=TStringList.create;
        ListaCodigoCheque:=TStringList.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Transfer�ncia e StringLists!',mterror,[mbok],0);
           exit;
     End;

     Try
        //**********************
        //Preciso dividir a quantia paga em cada portador
        // e se foi pago com dinheiro ou cheque de 3�
        ObjtransferenciaPortador.LocalizaLancamentoporPortador(Plancamento,ListaPortador_Din_ChTerc,Lista_Dinh_ChTerc,ListaPortador_Ch_Port,ListaChequePort,ListaCodigoCheque);

        Pbanco.clear;
        PCodigoPlanoContas.clear;
        Pvalor.clear;
        Phistorico.clear;
        PNumDcto.clear;

        //dinheiro e cheques de terceiros  totalizados por portador
        for QuantPort:=0 to ListaPortador_Din_ChTerc.count-1 do
        Begin
             If (strtofloat(Lista_Dinh_ChTerc.strings[QUANTPORT])<>0)
             Then Begin
                       If (Self.Pendencia.titulo.PORTADOR.LocalizaCodigo(ListaPortador_Din_ChTerc.strings[QUANTPORT])=False)
                       Then Begin
                                 Messagedlg('Os Portadores de Origem na Tabela de Transfer�ncia referentes a este Lan�amento n�o foram Encontrados!'+#13+'Eles deveriam ser usados para localizar o portador para exportar para a contabilidade!',mterror,[mbok],0);
                                 result:=False;
                                 exit;
                       End;
                       Self.Pendencia.titulo.PORTADOR.TabelaparaObjeto;
                       //Adicionando aos Parametro
                       Pbanco.add(Self.pendencia.titulo.portador.get_codigo);
                       PcodigoplanoContas.add(Self.pendencia.titulo.portador.get_codigoplanodecontas);
                       Pvalor.add(Lista_Dinh_ChTerc.strings[QUANTPORT]);
                       Phistorico.add(Self.historico);
                       PNumDcto.add('');
             End;
        End;

        //cheques do portador
        for QuantPort:=0 to ListaPortador_Ch_Port.count-1 do
        Begin
             If (Self.Pendencia.titulo.PORTADOR.LocalizaCodigo(ListaPortador_Ch_Port.strings[QUANTPORT])=False)
             Then Begin
                       Messagedlg('Os Portadores de Origem na Tabela de Transfer�ncia referentes a este Lan�amento n�o foram Encontrados!'+#13+'Eles deveriam ser usados para localizar o portador para exportar para a contabilidade!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;
             Self.Pendencia.titulo.PORTADOR.TabelaparaObjeto;
             Pbanco.add(Self.Pendencia.titulo.PORTADOR.get_codigo);
             
             //Exportar debito fornecedor e credito banco no momento que emito o cheque
             //ou usar uma Conta de Transicao e baixar apenas no momento que o cheque compensar
             if (PExportaChequedoPortadorPagamento=True)
             Then PCodigoPlanoContas.add(Self.Pendencia.titulo.PORTADOR.get_codigoplanodecontas)
             Else PCodigoPlanoContas.add(PContaTransicaochequeportador);

             Pvalor.add(ListaChequePort.strings[QUANTPORT]);

             If (OBjtalaodecheques.LocalizaChequePortador(ListaCodigoCheque.strings[quantport])=True)
             Then Begin
                       OBjtalaodecheques.TabelaparaObjeto;
                       Phistorico.add(OBjtalaodecheques.Get_HistoricoPagamento);
                       PNumDcto.add(OBjtalaodecheques.Get_Numero);
             End
             Else begin
                        Phistorico.add(Self.HISTORICO+' Pago com Cheque');
                        PNumDcto.add('');
             End;


        End;

        result:=true;

     Finally
            ObjtransferenciaPortador.free;
            ObjtalaodeCheques.free;

            Freeandnil(ListaPortador_Din_ChTerc);
            Freeandnil(Lista_Dinh_ChTerc);
            Freeandnil(ListaPortador_Ch_Port);
            Freeandnil(ListaChequePort);
            Freeandnil(ListaCodigoCheque);
     End;
End;


function TObjLancamento.Retorna_Contabilidade_Pendencias_lote(
  Plancamento: string; PLancamentofilho,PcodigoPlanoContas, Pvalor,
  Phistorico: TStringList): boolean;
var
PConta:string;
begin

  Result := false;

     PLancamentofilho.clear;
     PcodigoPlanoContas.clear;
     Phistorico.Clear;
     Pvalor.Clear;
     Pconta:='';

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select tablancamento.codigo,tablancamento.pendencia,tablancamento.valor,tablancamento.historico from TabLancamento ');
          SelectSQL.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
          SelectSQL.add('where tabtipolancto.classificacao=''Q'' ');
          SelectSQL.add('and tablancamento.LancamentoPai='+Plancamento);
          open;
          first;
          While not(eof) do
          Begin
               Self.Pendencia.ZerarTabela;
               Self.Pendencia.LocalizaCodigo(fieldbyname('pendencia').asstring);
               Self.Pendencia.TabelaparaObjeto;

               Pconta:='';

               If (Self.pendencia.Titulo.Get_ExportaLanctoContaGer='N')
               Then Begin
                       If (Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabil<>'')//Valor do CampoContabil do Fornecedor ou Cliente...
                       Then Pconta:=Self.pendencia.Titulo.CREDORDEVEDOR.Get_CampoContabilCredorDevedor(Self.pendencia.Titulo.credordevedor.get_codigo,Self.pendencia.Titulo.GET_CODIGOCREDORDEVEDOR);
               End
               Else Begin
                         Pconta:=Self.pendencia.Titulo.CONTAGERENCIAL.Get_CodigoPlanodeContas;
               End;

               PLancamentofilho.add(Fieldbyname('codigo').asstring);
               PcodigoPlanoContas.add(Pconta);
               Phistorico.add(fieldbyname('historico').AsString);
               Pvalor.add(fieldbyname('valor').asstring);
               
               next;
          End;
     End;

     result := True;
End;


function TObjLancamento.PegaCodigoPlanodeContas: string;
begin
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;

          edtgrupo01.EditMask:='';
          edtgrupo01.OnKeyDown:=Self.EDTCODIGOPLANODECONTASKeyDown;
          LbGrupo01.caption:='COD PCTS';

          showmodal;

          If (tag=0)
          Then result:=''
          Else result:=edtgrupo01.text;
     End;
end;

procedure TObjLancamento.EDTCODIGOPLANODECONTASKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(NIL);
            FplanodeCOntas:=TFplanodeCOntas.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjExportaContabilidade.ObjPlanodeContas.Get_Pesquisa,Self.ObjExportaContabilidade.ObjPlanodeContas.Get_TITULOPesquisa,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;


function TObjLancamento.Get_lancamentoPai: string;
begin
     Result:=Self.LancamentoPai;
end;

procedure TObjLancamento.Submit_LancamentoPai(parametro: string);
begin
     Self.LancamentoPai:=Parametro;
end;

function TObjLancamento.SomaporPai(ParametroPai: string): Currency;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select sum(valor)as Soma from Tablancamento where lancamentopai='+ParametroPai);
          open;
          result:=Fieldbyname('soma').asfloat;
     End;


end;

function TObjLancamento.GravaTransferencia(out PnumeroTransf:string): Boolean;
var
TempObjGeraTransferencia:TObjGeraTransferencia;
TmpPortador:string;
TempObjTransferenciaPortador:TObjTransferenciaPortador;
cont:integer;
TempTStrings:TStringList;
begin

     //Procedimento Usado para efetuar uma transferencia do Portador
     //que ser� escolhido para o contas pagas, com o valor da pendencia
     //atual, usado no pagamento em lote
     //resgatando o portador que deseja fazer a transferencia
     result:=False;
     limpaedit(Ffiltroimp);
     With Ffiltroimp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.EditMask:='';
          lbGrupo01.Caption:='Portador';
          edtgrupo01.OnKeyDown:=self.edtPortadorKeyDown;

          showmodal;

          If tag=0
          Then exit;


          Try
             strtoint(edtgrupo01.text);
             TmpPortador:=edtgrupo01.text;
             If (Self.Pendencia.PORTADOR.LocalizaCodigo(TmpPortador)=False)
             Then strtoint('a');
          Except
                Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
                exit;
          End;
     End;

     //ja tenho o portador
     //agora preciso localizar o gerador de transferencia
     //para pegar dados como portador destino, historicos....
     Try
        cont:=1;
        TempObjGeraTransferencia:=TObjGeraTransferencia.create;
        cont:=2;
        TempObjTransferenciaPortador:=TObjTransferenciaPortador.create;
        cont:=3;
        TempTStrings:=TStringList.create;

     Except
           Messagedlg('Erro na Tentativa de Criar o Objeto Gerador de Transfer�ncia ou Objeto de Transfer�ncia!',mterror,[mbok],0);
           If (cont>1)
           Then TempObjGeraTransferencia.free;
           If (cont=3)
           Then TempObjTransferenciaPortador.free;
           exit;
     End;


     Try
        If (TempObjGeraTransferencia.LocalizaHistorico('PAGAMENTO DE CONTAS EM LOTE')=False)
        Then Begin
                Messagedlg('N�o foi encontrado o gerador de Transfer�ncia com Hist�rico->"PAGAMENTO DE CONTAS EM LOTE"',mterror,[mbok],0);
                exit;
        End;
        TempObjGeraTransferencia.TabelaparaObjeto;

        {Uobjtransferenciaportador.RegTransferenciaPortador.Origem:='OBJLANCAMENTO';
        Uobjtransferenciaportador.RegTransferenciaPortador.CodigoLancamento:=Self.codigo;
        Uobjtransferenciaportador.RegTransferenciaPortador.PortadorDestino:=TempObjGeraTransferencia.Get_Portadordestino;
        Uobjtransferenciaportador.RegTransferenciaPortador.Historico:=Self.historico;}

        TempObjTransferenciaPortador.status:=dsinsert;
        PnumeroTransf:=TempObjTransferenciaPortador.Get_NovoCodigo;
        TempObjTransferenciaPortador.Submit_CODIGO(PnumeroTransf);
        TempObjTransferenciaPortador.Submit_Data(Self.Data);
        TempObjTransferenciaPortador.Submit_Valor(Self.Valor);
        TempObjTransferenciaPortador.Submit_Documento('0');
        TempObjTransferenciaPortador.Submit_Complemento('0');
        TempObjTransferenciaPortador.Submit_Grupo(TempObjGeraTransferencia.Get_Grupo);
        TempObjTransferenciaPortador.Submit_PortadorOrigem(TmpPortador);
        TempObjTransferenciaPortador.Submit_PortadorDestino(TempObjGeraTransferencia.Get_Portadordestino);
        TempObjTransferenciaPortador.CodigoLancamento.Submit_codigo(Self.codigo);

        TempTStrings.clear;
        TempObjTransferenciaPortador.Submit_ListaChequesTransferencia(TempTStrings);
        
        If (TempObjTransferenciaPortador.Salvar(False,False,False,True,True)=False)
        Then exit;

        Result:=True;

     Finally
            TempObjGeraTransferencia.free;
            TempObjTransferenciaPortador.free;
            freeandnil(TempTStrings);
     End;

end;

{Rodolfo}
Procedure TObjLancamento.edtPortadorComChequeDblClick(Sender: TObject);
var
  Shift : TShiftState;
  key : Word;
begin
  key := VK_F9;
  edtPortadorComChequeKeyDown(Sender, Key, Shift);
end;

{Rodolfo}
procedure TObjLancamento.edtPortadorComChequeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador; {Rodolfo}
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            Fportador := TFportador.Create(nil);   {Rodolfo}
            FpesquisaLocal.NomeCadastroPersonalizacao:='UOBJLANCAMENTO.EDTPORTADORCOMCHEQUE';
            If (FpesquisaLocal.PreparaPesquisa(Self.Pendencia.Titulo.Get_PesquisaPortadorComCheque, Self.Pendencia.Titulo.Get_TituloPesquisaPortador, Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)Then begin
                            Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                            FfiltroImp.lb2grupo01.Caption := FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring; //Rodolfo
                        end;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
End;

procedure TObjLancamento.edtPortadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='UOBJLANCAMENTO.EDTPORTADOR';
            If (FpesquisaLocal.PreparaPesquisa(Self.Pendencia.Titulo.Get_Pesquisa, Self.Pendencia.Titulo.Get_TituloPesquisaPortador, nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
End;

function TObjLancamento.LancaValores(out PortadorRecebimento: string): boolean;
var
numerorecebimento,TmpPortador:string;
ObjValores:TobjValores;
ValorDinheiro,ValorRetorno,ValorObj:Currency;
begin
     //resgatando o portador que deseja lancar o valor
     result:=False;
     {pergunto o valor em dinheiro e o portador
     se o valor for menor gravo este valor e cham
     o modulo de lancamento de valores}
     limpaedit(Ffiltroimp);
     With Ffiltroimp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.EditMask:='';
          lbGrupo01.Caption:='Portador';
          edtgrupo01.OnKeyDown:=self.edtPortadorKeyDown;
          Grupo02.Enabled:=True;
          edtgrupo02.EditMask:='';
          edtgrupo02.text:=Self.Valor;
          lbGrupo02.Caption:='Dinheiro';
          showmodal;
          If tag=0
          Then exit;

          Try
             strtoint(edtgrupo01.text);
             TmpPortador:=edtgrupo01.text;
             If (Self.Pendencia.PORTADOR.LocalizaCodigo(TmpPortador)=False)
             Then Begin
                       Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
                       exit;
             End;
             PortadorRecebimento:=TmpPortador;
          Except
                Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
                exit;
          End;

          Try
             valordinheiro:=strtofloat(edtgrupo02.text);
             if (strtofloat(floattostr(valordinheiro))>StrToFloat(Self.Valor))
             Then Begin
                       Messagedlg('O valor n�o pode ser maior que a soma!',mterror,[mbok],0);
                       exit;
             end;
          Except
                ValorDinheiro:=0;
          End;
     End;


     Try
        ObjValores:=TobjValores.create;
     Except
           Messagedlg('Erro na Tentativa de Cria��o do Objeto de Valores!',mterror,[mbok],0);
           exit;
     End;

     Try
        if (ValorDinheiro>0)
        Then Begin
                  ObjValores.ZerarTabela;
                  ObjValores.Status:=dsInsert;
                  NumeroRecebimento:=ObjValores.Get_NovoCodigo;
                  Objvalores.Submit_CODIGO(NumeroRecebimento);
                  Objvalores.Submit_Historico(Self.historico);
                  ObjValores.Submit_Tipo('D');
                  ObjValores.Submit_Valor(floattostr(valordinheiro));
                  ObjValores.Submit_Lancamento(Self.codigo);
                  objvalores.Submit_Portador(TmpPortador);
                  If (Objvalores.Salvar(false,true)=False)
                  Then exit;
        End;

        //se o valor lancado naum for o valor total,
        //chama-se o form dce lancamento

        if (strtofloat(floattostr(ValorDinheiro))<strtofloat(Self.Valor))//valores menor, tenho que lan�ar em cheque ou em outro portador
        Then Begin
                If (Self.GravaRecebimento(false)=False)
                Then exit;
        End;

        //Verificando se os valores fecham
        ValorRetorno:=0;
        ValorRetorno:=ObjValores.Get_SomaValores(Self.CODIGO);
        ValorObj:=StrTofloat(Self.valor);

        If (strtofloat(floattostr(ValorRetorno))<>strtofloat(floattostr(ValorObj)))
        Then Begin
                  Messagedlg('Os Valores N�o Fecham com o lan�amento!',mterror,[mbok],0);
                  exit;
        End;
        RegistroValores.lancamentoexterno:=False;
        LimpaRegistroValores;
        Result:=True;

     Finally
            ObjValores.free;
     End;
End;


function TObjLancamento.GeraExclusaoContabilidade(Plancamento:string;ComCommit:Boolean): Boolean;
VAR
CC,NCC,CD,NCD:STRING;
begin

     //ANTES DESTE PROCEDIMENTO SER CHAMADO O OBJEXPORTACONTABILIDADE
     //JA FOI PREENCHIDO COM OS DADOS DO LANCAMENTO INICIAL
     //AGORA DEVO APENAS INVERTER ESTE LANCAMENTO
     Self.ObjExportaContabilidade.Submit_Exportado('N');
     Self.ObjExportaContabilidade.Submit_ObjGerador('OBJLANCAMENTO');
     Self.ObjExportaContabilidade.Submit_CodGerador(Plancamento);
     Self.ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
     Self.ObjExportaContabilidade.Submit_Data(datetostr(now));
     Self.ObjExportaContabilidade.Submit_Historico('EXCLUS�O '+Self.ObjExportaContabilidade.Get_Historico);
     Self.ObjExportaContabilidade.Submit_NomedoArquivo('');

     CC:='';
     CC:=Self.ObjExportaContabilidade.Get_ContaCredite;
     NCC:='';
     NCC:=Self.ObjExportaContabilidade.Get_NomeCredite;
     CD:='';
     CD:=Self.ObjExportaContabilidade.Get_ContaDebite;
     NCD:='';
     NCD:=Self.ObjExportaContabilidade.Get_NomeDebite;
     
     Self.ObjExportaContabilidade.Submit_ContaCredite(CD);
     Self.ObjExportaContabilidade.Submit_ContaDebite(CC);

     Self.ObjExportaContabilidade.Status:=dsinsert;
     RESULT:=Self.ObjExportaContabilidade.Salvar(Comcommit);
     Self.ObjExportaContabilidade.Status:=dsInactive;
     
end;




Function TObjLancamento.LancaJuros(PPendencia: string;ValorJuroDesconto: Currency;PData:string;Plancamentopai:string;PlancaContaBilidade:Boolean;Phistorico:String): boolean;
var
  LancamentoPaitemp:string;
begin
     result:=False;
     Self.Pendencia.LocalizaCodigo(Ppendencia);
     Self.Pendencia.TabelaparaObjeto;
     Self.ObjGeraLancamento.ZerarTabela;

     if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
     Then Begin//contas a Pagar
               If (Self.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','JUROS A PAGAR')=False)
               Then Begin
                       Messagedlg('N�o foi encontrado o Gerador de Lan�amento, dados: '+#13+'Objeto:"OBJLANCAMENTO" /HISTORICOGERADOR="JUROS A PAGAR"',mterror,[mbok],0);
                       exit;
               End;
     End
     Else Begin//Contas a receber
               If (Self.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','JUROS A RECEBER')=False)
               Then Begin
                       Messagedlg('N�o foi encontrado o Gerador de Lan�amento, dados: '+#13+'Objeto:"OBJLANCAMENTO" /HISTORICOGERADOR="JUROS A RECEBER"',mterror,[mbok],0);
                       exit;
               End;
     End;
     Self.ObjGeraLancamento.TabelaparaObjeto;

     //No proprio registro do GeraLancamento eu tenho o Codigo do Plano de COntas
     //seja de Juro a Pagar ou a receber
     //assim no momento de gravar este lancamento, eu passo o codigo
     //E o comando para exportar a contabilidade

     //agora � s� lancar
     Self.ZerarTabela;
     Self.Submit_CODIGO(Self.Get_NovoCodigo);
     Self.Submit_Pendencia(Ppendencia);
     Self.Submit_TipoLancto(Self.ObjGeraLancamento.Get_TipoLAncto);
     Self.Submit_Valor(floattostr(ValorJuroDesconto));
     Self.Submit_Data(Pdata);
     Self.Submit_LancamentoPai(Plancamentopai);
     Self.Historico:=Self.ObjGeraLancamento.Get_historico+'-'+Phistorico;
     Self.Status:=dsinsert;
     Result:=Self.Salvar(False,PlancaContaBilidade,Self.ObjGeraLancamento.Get_CodigoPlanodeContas,False,true);
end;

Function TObjLancamento.LancaDesconto(PPendencia: string;
  ValorJuroDesconto: Currency;PData:string;Plancamentopai:string;PExportaContabilidade:Boolean): boolean;
var
  LancamentoPaitemp,NfTemp:string;
begin
     result:=False;

             Self.Pendencia.LocalizaCodigo(Ppendencia);
             Self.Pendencia.TabelaparaObjeto;
             NFTemp:=Self.Pendencia.Titulo.Get_NUmdcto;
             Self.ObjGeraLancamento.ZerarTabela;

             if (Self.Pendencia.Titulo.CONTAGERENCIAL.get_tipo='D')
             Then Begin//contas a pagar
                     If (Self.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO A PAGAR')=False)
                     Then Begin
                             Messagedlg('N�o foi encontrado o Gerador de Lan�amento, dados: '+#13+'Objeto:"OBJLANCAMENTO" /HISTORICOGERADOR="DESCONTO A PAGAR"',mterror,[mbok],0);
                             exit;
                     End;
             End
             Else Begin//contas a receber
                     If (Self.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO A RECEBER')=False)
                     Then Begin
                             Messagedlg('N�o foi encontrado o Gerador de Lan�amento, dados: '+#13+'Objeto:"OBJLANCAMENTO" /HISTORICOGERADOR="DESCONTO A RECEBER"',mterror,[mbok],0);
                             exit;
                     End;
             End;

             Self.ObjGeraLancamento.TabelaparaObjeto;

             //agora � s� lancar
             Self.ZerarTabela;
             Self.Submit_CODIGO(Self.Get_NovoCodigo);
             Self.Submit_Pendencia(Ppendencia);
             Self.Submit_TipoLancto(Self.ObjGeraLancamento.Get_TipoLAncto);
             Self.Submit_Valor(floattostr(ValorJuroDesconto));
             Self.Submit_Data(Pdata);
             Self.Submit_LancamentoPai(Plancamentopai);
             Self.Status:=dsinsert;
             Self.Historico:=Self.ObjGeraLancamento.Get_historico+NFTemp;
             Result:=Self.Salvar(False,PExportaContabilidade,Self.ObjGeraLancamento.Get_CodigoPlanodeContas,False,true);
end;



function TObjLancamento.ExcluiLancamentosPendencia(Ppendencia: string;
  ComCommit: Boolean): boolean;
var
TmpQuery:tibquery;
begin
     Try
        TmPQuery:=Tibquery.create(nil);
        TmPQuery.database:=FDataModulo.IBDatabase;
     Except
           Messagedlg('Erro na tentativa de Criar a Query Tempor�ria',mterror,[mbok],0);
           exit;
     End;
Try

     //seleciona todos os lancamentos da pendencia em parametro
     //e apaga-os,fazendo o caminho de volta

     result:=False;
     With tmpquery do
     Begin
          //selecionando todos os lancamentos da pendencia do parametro
          close;
          SQL.clear;
          SQL.add('Select codigo from tablancamento where pendencia='+Ppendencia);
          sql.add('order by codigo desc');

          //� ordenado descendente
          //explicacao
          //Saldo Inicial -           100,00
          //1 - Desconto 50,00  saldo 50
          //2 - Quitacao 20,00  saldo 30
          //3 - Juros 30,00     saldo 60
          //4 - Desconto 60,00  saldo 0,00

          //excluindo
          //saldo final         0,00
          //exclui 4-desconto   60,00
          //exclui 3-juros      30,00
          //exclui 2-quita      50,00
          //exclui 1-desconto   100,00
          //saldo final          100,00

          open;
          if (recordcount=0)
          Then Begin//se naum tiver nenhum ok
                    if ComCommit=True
                    Then FDataModulo.IBTransaction.CommitRetaining;
                    result:=True;
                    exit;
          End;
          last;
          first;

          While not (eof) do
          Begin
               //localizando os dados do objeto
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               Result:=Self.ApagaLancamento(Self.codigo,ComCommit);
               if (result=False)
               Then exit;
               next;
          End;//while

          if ComCommit=True
          Then FDataModulo.IBTransaction.CommitRetaining;

          result:=True;
     End;//with query

Finally
       freeandnil(TmPQuery);
End;

end;

function TObjLancamento.RetornaRecebimento_lancamento(
  Plancamento: string;ComCOmmit:Boolean): boolean;
var
ObjValores:TobjValores;
begin
     result:=False;
     Try
        ObjValores:=TObjValores.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Valores Recebido!',mterror,[mbok],0);
           exit;
     End;
     Try
        //tem que fazer o caminho inverso
        //primeiro retornar o saldo do portador
        //apagar os lancamentos na tabvalores
        //se tiver cheque na tabchequeportador tmbm
        //apagar o lancamento

        If (ObjValores.ExcluiporLancamento(plancamento,comcommit)=True)
        Then Begin
                  result:=Self.Exclui(plancamento,ComCommit);
                  exit;
        End
        Else Begin
                  result:=False;
                  exit;
        End;
        
     Finally
            Objvalores.free;
     End;

end;


function TObjLancamento.ApagaPagamento(Plancamento: string): boolean;
var
ObjValoresTransferencia:TObjValoresTransferenciaPortador;
OBjValores:TobjValores;
tmpvalordinheiro:currency;
begin
     result:=False;
     Try
        ObjValoresTransferencia:=TObjValoresTransferenciaPortador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Valores da Transfer�ncia entre Portadores!',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjValores:=TObjValores.create;
     Except
           ObjValores.Free;
           Messagedlg('Erro na Cria��o do Objeto de Valores Recebidos!',mterror,[mbok],0);
           exit;
     End;


     Try
        if (ObjValoresTransferencia.RetornaPagamentoLancamento(Plancamento)=True)
        Then Begin
                  Result:=ObjValores.ExcluiporLancamento(plancamento,False);
        End
        Else exit;
     Finally
            ObjValoresTransferencia.free;
            ObjValores.free;
     End;
end;


Function tobjLancamento.ApagaRecebimento(PLancamento:string):boolean;
var
ObjValores:TobjValores;
ObjValoresTransferencia:TObjValoresTransferenciaPortador;
Query:TIBQuery;
CodigoTit, CodigoPend, CodigoLanc:string;  //Usando quando tiver lan�amento de cr�dito nas pendecias
begin
     result:=False;

     query:=TIBQuery.Create(nil);
     Query.Database:=FDataModulo.IBDatabase;
     Try
        ObjValoresTransferencia:=TObjValoresTransferenciaPortador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Valores da Transfer�ncia entre Portadores!',mterror,[mbok],0);
           exit;
     End;



     Try
        ObjValores:=TObjValores.create;
     Except
           ObjValoresTransferencia.Free;
           ObjValores.Free;
           Messagedlg('Erro na Cria��o do Objeto de Valores Recebidos!',mterror,[mbok],0);
           exit;
     End;
     
     Try
        //tem que fazer o caminho inverso
        //primeiro retornar o saldo do portador
        //apagar os lancamentos na tabvalores
        //se tiver cheque na tabchequeportador tmbm
        //se esses cheques sofreram transfer�ncia, apagar estes registros
        //assim como seus lan�amentos na tablanctoportador
        //apagar o lancamento
        //retornar o saldo da pend�ncia

        {
            Jonatan Medina  18/07/2011
            Como foi acrescentado a op��o de gerar cr�dito com o valor do cr�dito
            � preciso ao excluir um lan�amento
            *excluir da tablanctoportador o valor do lan�amento
            *lan�ar na tablancamentocredito o valor negativo do cr�dito que essa pendencia tinha gerado
            *ou excluir esse cr�dito
            *para ter o controle do que entra � criado um titulo quitado com o valor do cr�dito
            *ao estornar preciso excluir esse titulo, a pendencia e o lan�amento do mesmo...
        }

        with Query do
        begin
              CodigoTit:='';
              Close;
              sql.Clear;
              sql.Add('delete from tablanctoportador where lancamento ='+PLancamento);
              sql.Add('and tipolancto=''12'' ');
              ExecSQL;

              //Encontro na Tabcredito qual titulo foi criado por esse lan�amento
              //lembrando que o mesmo so � criado na gera��o de credito automatico pelo cliente
              Close;
              sql.Clear;
              SQL.Add('select * from tabcredito where lancamento='+PLancamento);
              Open;
              CodigoTit:=fieldbyname('titulo').AsString;

              if(CodigoTit<>'') then
              begin
                      //Encontro qual a pendencia que esta ligada com esse t�tulo
                      Close;
                      sql.Clear;
                      SQL.Add('select * from tabpendencia where titulo='+CodigoTit);
                      Open;
                      CodigoPend:=fieldbyname('Codigo').AsString;

                      //Encontro o lan�amento de quita��o nesse titulo
                      Close;
                      sql.Clear;
                      SQL.Add('select * from tablancamento where pendencia='+CodigoPend);
                      Open;
                      CodigoLanc:=fieldbyname('Codigo').AsString;

                      //Agora preciso apagar o titulo/pendencia/lan�amento que
                      //foi gerado quando lan�ou um cr�dito pro cliente ao inv�s de
                      //dar o troco
                      //excluindo registro da tabcredito
                      Close;
                      sql.Clear;
                      sql.Add('delete from tabcredito where lancamento ='+PLancamento);
                      ExecSQL;

                      //Excluindo o lan�amento
                      Close;
                      sql.Clear;
                      sql.Add('delete from tablancamento where codigo ='+CodigoLanc);
                      ExecSQL;
                      Close;
                      //excluindo a pendencia
                      sql.Clear;
                      sql.Add('delete from tabpendencia where codigo='+CodigoPend);
                      ExecSQL;
                      Close;
                      //excluindo o titulo
                      sql.Clear;
                      sql.Add('delete from tabtitulo where codigo ='+CodigoTit);
                      ExecSQL;


                      
              end;

        end;

        If (ObjValores.ExcluiporLancamento(plancamento,False)=True)
        Then Begin
                  //se tiver troco, tem transferencia entao mando apagar
                  result:=ObjValoresTransferencia.RetornaPagamentoLancamento(Plancamento);
                  exit;
        End
        Else exit;
     Finally
            Objvalores.free;
            ObjValoresTransferencia.free;
            FreeAndNil(Query);
     End;

end;


function TObjLancamento.ApagaLancamento(Pcodigo: string;ComCommit:Boolean): boolean;
var
TipoConta:string;
begin

     if (ObjPermissoesUsoGlobal.ValidaPermissao('APAGAR LAN�AMENTO FINANCEIRO')=False)
     Then exit;


     result:=False;
     if (Self.LocalizaCodigo(pcodigo)=false)
     Then Begin
               Messagedlg('Lan�amento n�o encontrado!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     if (Self.LancamentoPai<>'')
     Then Begin
               if (Messagedlg('Esse lan�amento pertence a um lan�amento em Lote. Deseja excluir o Lan�amento em Lote?',mtConfirmation,[mbyes,mbno],0)=mrno)
               Then exit;

               PCodigo:=Self.LancamentoPai;

               Self.ZerarTabela;
               if (Self.LocalizaCodigo(pcodigo)=false)
               Then Begin
                       Messagedlg('Lan�amento Lote n�o encontrado!',mterror,[mbok],0);
                       exit;
               End;
               Self.TabelaparaObjeto;
     End;

     if (Self.Pendencia.get_codigo<>'')
     Then Begin
               if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C')
               Then TipoConta:='C'
               Else TipoConta:='D';
     End
     Else Begin//� em lote???? preciso pegar apenas um ja � o suficiente

               with Self.ObjQueryLocal do
               Begin

                  close;
                  SQL.clear;
                  SQL.add('Select * from TabLancamento where LancamentoPai='+Pcodigo);
                  sql.add('order by codigo desc');
                  open;
                  first;
                  Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                  Self.TabelaparaObjeto;
                  If (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C')
                  Then TipoConta:='C'
                  Else TipoConta:='D';
               End;
     End;

     Self.LocalizaCodigo(pcodigo);
     Self.TabelaparaObjeto;

     if (TipoConta='C')
     Then Begin
               If (Self.ApagaRecebimento(Pcodigo)=False)
               Then Begin
                         if (ComCommit=true)
                         Then FDataModulo.IBTransaction.RollbackRetaining;
                         Messagedlg('Erro na tentativa de Apagar o Recebimento!',mterror,[mbok],0);
                         exit;
               End;
     End
     Else Begin
               If (Self.ApagaPagamento(Pcodigo)=False)
               Then Begin
                         if (ComCommit=true)
                         Then FDataModulo.IBTransaction.RollbackRetaining;
                         Messagedlg('Erro na tentativa de Apagar o Recebimento!',mterror,[mbok],0);
                         exit;
               End;
     End;

     //verificando se eu tenho filhos

     with Self.ObjQueryLocal do
     Begin
          close;
          SQL.clear;
          SQL.add('Select * from TabLancamento where LancamentoPai='+Pcodigo);
          SQL.add('order by codigo desc');
          open;
          first;
          While not(eof) do
          Begin
               //verificar o saldo no lote
               if (Self.Exclui(fieldbyname('codigo').asstring,false)=False)
               Then Begin
                         if (ComCommit=true)
                         Then FDataModulo.IBTransaction.RollbackRetaining;
                         Messagedlg('Erro na tentativa de Excluir o Lan�amento '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                         exit;
               End;
               next;
          End;
    End;

    if (ObjLanctoPortadorGlobal.zeracampolancamento(Pcodigo)=False)
    Then begin
              if (ComCommit=true)
              Then FDataModulo.IBTransaction.RollbackRetaining;
              Messagedlg('Erro na tentativa de Zerar o campo Lan�amento na TabLanctoPortador',mterror,[mbok],0);
              exit;
    End;


    if (Self.Exclui(pcodigo,False)=False)
    then Begin
              if (ComCommit=true)
              Then FDataModulo.IBTransaction.RollbackRetaining;
              Messagedlg('Erro na tentativa de Excluir o Lan�amento '+pcodigo,mterror,[mbok],0);
              exit;
    End;
    result:=True;
    if (ComCommit=true)
    Then FDataModulo.IBTransaction.CommitRetaining;
end;


procedure TObjLancamento.Imprime_Recebimento_Portador_Analitico;
var
complemento:string;
data1,data2:string;
pportador:string;
linha:integer;
TotalPortador,totaldata,somageral:Currency;
pdataanterior,pportadoranterior:string;
begin
     //Conforme solicitado pela Camila
     //Filtrar somente os lancamentos que ocorreram
     //na data diferente da de criacao
     //por isso em opcaorel

     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Todos');
          RgOpcoes.Items.add('Lan�amentos em Datas diferentes da data de cria��o da pend�ncia');
          showmodal;

          if (tag=0)
          Then exit;

          case RgOpcoes.itemindex of
              0:complemento:='';
              1:Complemento:='and tablancamento.data<>cast(tabpendencia.datac as date)';
          End;
     End;

     //filtrando
     with Ffiltroimp do
     Begin
          limpaedit(FfiltroImp);
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          LbGrupo01.caption:='Lan�amento Inicial';
          LbGrupo02.caption:='Lan�amento Final';
          LbGrupo03.caption:='Portador do Recebimento';
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          edtgrupo03.EditMask:='';
          edtgrupo03.OnKeyDown:=Self.edtPortadorKeyDown;
          edtgrupo03.Color:=$005CADFE;

          showmodal;

          if (tag=0)
          Then exit;

          Try
             strtodate(edtgrupo01.Text);
             data1:=edtgrupo01.Text;
          Except
                data1:='';
          End;

          Try
             strtodate(edtgrupo02.Text);
             data2:=edtgrupo02.Text;
          Except
                data2:='';
          End;

          Try
             StrToInt(edtgrupo03.text);
             pportador:=edtgrupo03.text;
             if (Self.Pendencia.Titulo.PORTADOR.LocalizaCodigo(pportador)=False)
             Then Begin
                       Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
                       exit;
             End;
             Self.Pendencia.Titulo.PORTADOR.TabelaparaObjeto;

          Except
                pportador:='';
          End;

     End;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;

          if (FOpcaorel.RgOpcoes.ItemIndex=1)//Somente os fora data de criacao
          Then Begin
                  SelectSQL.add('select Plancamento,Pdatalancamento,pvalorlancamento,ptitulo,phistoricotitulo,ppendencia,pportadorvalores,pnomeportadorvalores,pvalorvalores');
                  SelectSQL.add('from PROCLANCAMENTOS_REC_FORADATA');
          End
          Else BEgin//todos
                    SelectSQL.add('select Plancamento,Pdatalancamento,pvalorlancamento,ptitulo,phistoricotitulo,ppendencia,pportadorvalores,pnomeportadorvalores,pvalorvalores');
                    SelectSQL.add('from PROCLANCAMENTOS_REC_TODOS');
          End;

          SelectSQL.add('where plancamento<>-100');//soh para ter o where


          if (data1<>'')
          Then SelectSQL.add('and Pdatalancamento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(data1))+#39);

          if (data2<>'')
          Then SelectSQL.add('and Pdatalancamento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(data2))+#39);

          if (pportador<>'')
          Then SelectSQL.add('and pportadorvalores='+pportador);

          SelectSQL.add('order by PdataLancamento,PPortadorValores');
          open;
          first;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum informa��o foi selecionada!',mtinformation,[mbok],0);
                    exit;
          End;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.fechar;
                    exit;
          End;
          linha:=3;

          FreltxtRDPRINT.RDprint.ImpC(linha,45,'RECEBIMENTOS POR PORTADOR',[negrito]);
          inc(linha,2);

          if (Data1<>'')
          Then Begin
                  if (data2<>'')
                  Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos entre '+data1+' e '+data2,[negrito])
                  Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos com data maior ou igual a '+data1,[negrito]);
                  inc(linha,2);
          End
          Else Begin
                   if (data2<>'')
                   Then Begin
                            FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos com data menor ou igual a '+data2,[negrito]);
                            inc(linha,2);
                   End;
          End;

          if (complemento<>'')
          Then Begin
                    FreltxtRDPRINT. RDprint.ImpF(linha,1,'OBS: SOMENTE LAN�AMENTOS OCORRIDOS EM DATAS DIFERENTES DA CRIA��O DO T�TULO',[negrito]);
                    inc(linha,2);
          end;

          if (pportador<>'')
          Then Begin
                    FreltxtRDPRINT. RDprint.ImpF(linha,1,'Portador: '+pportador+' - '+Self.Pendencia.Titulo.PORTADOR.Get_Nome,[negrito]);
                    inc(linha,2);
          End;

          //colunas
          //Pdatalancamento,pvalorlancamento,ptitulo,phistoricotitulo,ppendencia,pportadorvalores,pnomeportadorvalores,pvalorvalores');
          FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('DATA',10,' ')+' '+
                                              CompletaPalavra('VALOR LAN�.',12,' ')+' '+
                                              CompletaPalavra('TITULO',27,' ')+' '+
                                              CompletaPalavra('PEND�NCIA',09,' ')+' '+
                                              CompletaPalavra('PORTADOR',15,' ')+' '+
                                              CompletaPalavra('VALOR REC.',12,' '),[NEGRITO]);

          inc(linha,1);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,2);
          somageral:=0;
          pdataanterior:='';
          pportadoranterior:='';
          TotalPortador:=0;
          totaldata:=0;

          
          While not(eof) do
          Begin
               if (pdataanterior<>fieldbyname('pdatalancamento').asstring)
               Then Begin
                         if (pdataanterior<>'')//se for vazio � o 1� registro
                         Then Begin
                                   //totaliza portador
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,62,CompletaPalavra('TOTAL PORTADOR',15,' ')+' '+
                                                                       CompletaPalavra_a_Esquerda(formata_valor(floattostr(TotalPortador)),12,' '),[negrito]);
                                   inc(linha,2);
                                   //totaliza data
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('TOTAL DATA',15,' ')+' '+
                                                                       CompletaPalavra_a_Esquerda(formata_valor(floattostr(TotalData)),12,' '),[negrito]);
                                   inc(linha,2);
                         End;

                         pdataanterior:=fieldbyname('pdatalancamento').asstring;
                         pportadoranterior:=fieldbyname('pportadorvalores').asstring;
                         TotalPortador:=0;
                         totaldata:=0;
               end
               Else Begin//n�o mudou a datas, mas pode ter mudado o portador
                        if (pportadoranterior<>fieldbyname('pportadorvalores').asstring)
                        Then Begin
                                   //totaliza portador
                                  FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                  FreltxtRDPRINT.RDprint.Impf(linha,62,CompletaPalavra('TOTAL PORTADOR',15,' ')+' '+
                                                                      CompletaPalavra_a_Esquerda(formata_valor(floattostr(TotalPortador)),12,' '),[negrito]);
                                  inc(linha,2);
                                  pportadoranterior:=fieldbyname('pportadorvalores').asstring;
                                  TotalPortador:=0;
                        End;
               End;


               if (fieldbyname('ppendencia').asstring<>'')
               Then Begin
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Imp(linha,1,
                                              CompletaPalavra(fieldbyname('pDATAlancamento').asstring,10,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pvalorlancamento').asstring),12,' ')+' '+
                                              CompletaPalavra(fieldbyname('ptitulo').asstring+'-'+fieldbyname('phistoricotitulo').asstring,27,' ')+' '+
                                              CompletaPalavra_a_Esquerda(fieldbyname('ppendencia').asstring,09,' ')+' '+
                                              CompletaPalavra(fieldbyname('pportadorvalores').asstring+'-'+fieldbyname('pnomeportadorvalores').asstring,15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pvalorvalores').asstring),12,' '));
                         inc(linha,1);

               End
               Else Begin
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Imp(linha,1,
                                              CompletaPalavra(fieldbyname('pDATAlancamento').asstring,10,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pvalorlancamento').asstring),12,' ')+' '+
                                              CompletaPalavra(fieldbyname('ptitulo').asstring+'-'+'****RECEBIMENTO EM LOTE****',27,' ')+' '+
                                              CompletaPalavra_a_Esquerda(fieldbyname('ppendencia').asstring,09,' ')+' '+
                                              CompletaPalavra(fieldbyname('pportadorvalores').asstring+'-'+fieldbyname('pnomeportadorvalores').asstring,15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pvalorvalores').asstring),12,' '));
                         inc(linha,1);

                         Self.ObjQueryLocal.close;
                         Self.ObjQueryLocal.sql.clear;
                         Self.ObjQueryLocal.sql.add('select');
                         Self.ObjQueryLocal.sql.add('TabTitulo.codigo as TITULO,');
                         Self.ObjQueryLocal.sql.add('TabTitulo.Historico as HISTORICOTITULO,');
                         Self.ObjQueryLocal.sql.add('tablancamento.pendencia,');
                         Self.ObjQueryLocal.sql.add('cast(tabpendencia.datac as date) as DATAPENDENCIA,');
                         Self.ObjQueryLocal.sql.add('tabpendencia.valor as VALORPENDENCIA,');
                         Self.ObjQueryLocal.sql.add('tabpendencia.saldo as SALDOPENDENCIA,');
                         Self.ObjQueryLocal.sql.add('tablancamento.valor as VALORLANCAMENTO');
                         Self.ObjQueryLocal.sql.add('from tablancamento');
                         Self.ObjQueryLocal.sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                         Self.ObjQueryLocal.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                         Self.ObjQueryLocal.sql.add('Where TabLancamento.LancamentoPai='+Fieldbyname('plancamento').asstring);
                         Self.ObjQueryLocal.sql.add('order by tablancamento.lancamentopai');
                         Self.ObjQueryLocal.open;
                         Self.ObjQueryLocal.first;
                         While not(Self.ObjQueryLocal.eof) do
                         Begin
                              //imprimindo as pendencias do lote com o respectivo valor de quitacao de cada uma
                              FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                              FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(' ',10,' ')+' '+
                                                                 CompletaPalavra_a_Esquerda(formata_valor(Self.ObjQueryLocal.fieldbyname('valorlancamento').asstring),12,' ')+' '+
                                                                 CompletaPalavra(Self.ObjQueryLocal.fieldbyname('titulo').asstring+'-'+Self.ObjQueryLocal.fieldbyname('historicotitulo').asstring,27,' ')+' '+
                                                                 CompletaPalavra_a_Esquerda(Self.ObjQueryLocal.fieldbyname('pendencia').asstring,09,' '),[italico]);
                              inc(linha,1);
                              Self.ObjQueryLocal.next;
                         End;
               End;


               
               somageral:=somageral+fieldbyname('pvalorvalores').asfloat;
               TotalPortador:=TotalPortador+fieldbyname('pvalorvalores').asfloat;
               totaldata:=totaldata+fieldbyname('pvalorvalores').asfloat;
               next;
          End;

          //totaliza portador
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,62,CompletaPalavra('TOTAL PORTADOR',15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(floattostr(TotalPortador)),12,' '),[negrito]);
          inc(linha,2);
          //totaliza data
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('TOTAL DATA',15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(floattostr(TotalData)),12,' '),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.ImpF(linha,62,CompletaPalavra_a_Esquerda(formata_valor(floattostr(somageral)),12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.fechar;


     End;
end;
//******************************************************************************
procedure TObjLancamento.Imprime_Recebimento_por_data_portadornofinal;
var
data1,data2:string;
linha:integer;
PsomaTroco,PsomaTudo,Psoma_a_Vista,totaldata,somageral:Currency;
pdataanterior:string;

Pprazoatual,PPrazoAnterior:integer;
begin
     //filtrando
     with Ffiltroimp do
     Begin
          limpaedit(FfiltroImp);
          DesativaGrupos;

          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          
          LbGrupo01.caption:='Lan�amento Inicial';
          LbGrupo02.caption:='Lan�amento Final';

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          showmodal;

          if (tag=0)
          Then exit;

          Try
             strtodate(edtgrupo01.Text);
             data1:=edtgrupo01.Text;
          Except
                data1:='';
          End;

          Try
             strtodate(edtgrupo02.Text);
             data2:=edtgrupo02.Text;
          Except
                data2:='';
          End;

     End;

     
     With Self.ObjDataset do
     Begin
          //Esse relatorio seleciona todos os lancamento de pendencias a receber
          //recebidos num periodo
          //em caso de lote ja vem as filhas, nao vem o pai
          //pois o pai nao esta ligado a nenhuma pendencia

          close;
          SelectSQL.clear;
          SelectSQL.add('select Tablancamento.codigo as CODIGOLANCAMENTO,Tablancamento.data as DATALANCAMENTO,');
          SelectSQL.add('tablancamento.valor as VALORLANCAMENTO,');
          SelectSQL.add('tablancamento.lancamentopai,');
          SelectSQL.add('tablancamento.pendencia,tabpendencia.vencimento as VENCIMENTOPENDENCIA,');
          SelectSQL.add('tabpendencia.titulo,tabtitulo.historico as HISTORICOTITULO');

          SelectSQL.add('from tablancamento');
          SelectSQL.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
          SelectSQL.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
          SelectSQL.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSQL.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSQL.add('where tabcontager.tipo=''C''');
          SelectSQL.add('and tabtipolancto.classificacao=''Q''');

          if (data1<>'')
          Then SelectSQL.add('and tablancamento.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(data1))+#39);

          if (data2<>'')
          Then SelectSQL.add('and tablancamento.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(data2))+#39);

          SelectSQL.add('order by tablancamento.data,tabpendencia.vencimento');

          open;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Gerando relat�rio';
          first;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum informa��o foi selecionada!',mtinformation,[mbok],0);
                    exit;
          End;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.fechar;
                    exit;
          End;
          linha:=3;

          FreltxtRDPRINT.RDprint.ImpC(linha,45,'RECEBIMENTOS POR DATA',[negrito]);
          inc(linha,2);

          if (Data1<>'')
          Then Begin
                  if (data2<>'')
                  Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos entre '+data1+' e '+data2,[negrito])
                  Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos com data maior ou igual a '+data1,[negrito]);
                  inc(linha,2);
          End
          Else Begin
                   if (data2<>'')
                   Then Begin
                            FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos com data menor ou igual a '+data2,[negrito]);
                            inc(linha,2);
                   End;
          End;

          //Pdatalancamento,pvalorlancamento,ptitulo,phistoricotitulo,ppendencia,pportadorvalores,pnomeportadorvalores,pvalorvalores');
          FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('DATA',10,' ')+' '+
                                              CompletaPalavra('LANCTO',9,' ')+' '+
                                              CompletaPalavra('VALOR LANCTO',12,' ')+' '+
                                              CompletaPalavra('TITULO',27,' ')+' '+
                                              CompletaPalavra('PEND�NCIA',09,' ')+' '+
                                              CompletaPalavra('VENC.PEND.',10,' ')+' '+
                                              CompletaPalavra('LOTE',9,' '),[NEGRITO]);

          inc(linha,1);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,2);
          
          somageral:=0;
          pdataanterior:='';
          
          totaldata:=0;
          Psoma_a_Vista:=0;


          //Atrasados
          //A vista
          //Adiantados

          //como esta do menor pro Maior
          //entao posso trabalhar com numeros
          //-1 - atrasado
          //0  - a vista
          //1 - adiantado
          Pprazoatual:=-2;
          PPrazoAnterior:=-2;

          While not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.Show;
               Application.ProcessMessages;


               if (fieldbyname('vencimentopendencia').asdatetime<fieldbyname('datalancamento').asdatetime)
               Then Pprazoatual:=-1
               else Begin
                          if (fieldbyname('vencimentopendencia').asdatetime=fieldbyname('datalancamento').asdatetime)
                          Then Pprazoatual:=0
                          Else Pprazoatual:=1;
               End;

               if (pdataanterior<>fieldbyname('datalancamento').asstring)
               Then Begin
                         if (pdataanterior<>'')//se for vazio � o 1� registro
                         Then Begin
                                   inc(linha,1);

                                   //Totalizando os recebimentos a Vista
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('RECEBIMENTOS A VISTA DATA',40,' ')+' '+
                                                                        CompletaPalavra_a_Esquerda(formata_valor(PSoma_a_Vista),12,' '),[negrito]);
                                   inc(linha,1);

                                   //Totalizando os recebimentos que n�o s�o a Vista
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('ADIANTADOS E ATRASADOS',40,' ')+' '+
                                                                        CompletaPalavra_a_Esquerda(formata_valor(Totaldata-Psoma_a_Vista),12,' '),[negrito]);
                                   inc(linha,1);

                                   //totaliza data
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('TOTAL DATA',40,' ')+' '+
                                                                        CompletaPalavra_a_Esquerda(formata_valor(floattostr(TotalData)),12,' '),[negrito]);
                                   inc(linha,1);

                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra('_',90,'_'));
                                   inc(linha,1);

                                   

                                   //Somando os portadores
                                   Self.ObjQueryLocal.Close;
                                   Self.ObjQueryLocal.SQL.clear;
                                   Self.ObjQueryLocal.SQL.add('Select portador,tabportador.nome,sum(valor) as SOMA');
                                   Self.ObjQueryLocal.SQL.add('from ProcRetornaRecebimentoData ('+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+','+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+') A');
                                   Self.ObjQueryLocal.SQL.add('join tabportador on A.portador=tabportador.codigo');
                                   Self.ObjQueryLocal.SQL.add('group by portador,tabportador.nome');
                                   Self.ObjQueryLocal.open;
                                   Self.ObjQueryLocal.first;
                                   while not(Self.ObjQueryLocal.eof) do
                                   begin
                                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                        FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra(Self.ObjQueryLocal.fieldbyname('portador').asstring+'-'+Self.ObjQueryLocal.fieldbyname('nome').asstring,40,' ')+' '+
                                                                             CompletaPalavra_a_Esquerda(formata_valor(Self.ObjQueryLocal.fieldbyname('soma').asstring),12,' '),[negrito]);
                                        inc(linha,1);
                                        Self.ObjQueryLocal.next;
                                   End;

                                   //Somando o Troco
                                   Self.ObjQueryLocal.Close;
                                   Self.ObjQueryLocal.SQL.clear;
                                   Self.ObjQueryLocal.SQL.add('Select sum(valor) as SOMA from ProcRetornaTrocoRecebimentoData('+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+','+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+')');
                                   Self.ObjQueryLocal.open;
                                   PsomaTroco:=Self.ObjQueryLocal.fieldbyname('soma').asfloat;

                                   if (PsomaTroco>0)
                                   then begin
                                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                             FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('TROCO',40,' ')+' '+
                                                                                  CompletaPalavra_a_Esquerda(formata_valor(Self.ObjQueryLocal.fieldbyname('soma').asstring),12,' '),[negrito]);
                                             inc(linha,1);
                                   End;

                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra('_',90,'_'));
                                   inc(linha,2);
                                   
                         End;

                         pdataanterior:=fieldbyname('datalancamento').asstring;
                         totaldata:=0;
                         Psoma_a_Vista:=0;
                         PPrazoAnterior:=-2;
                         
               end;

               if (Pprazoatual<>PPrazoAnterior)
               Then Begin
                         if (fieldbyname('vencimentopendencia').asdatetime<fieldbyname('datalancamento').asdatetime)
                         Then Begin
                                   PPrazoAnterior:=-1;
                         
                                   FreltxtRDPRINT.RDprint.Impf(linha,1,'ATRASADO',[negrito]);
                                   inc(linha,2);
                         End
                         Else Begin
                              if (fieldbyname('vencimentopendencia').asdatetime=fieldbyname('datalancamento').asdatetime)
                              Then begin
                                         PPrazoAnterior:=0;
                                         FreltxtRDPRINT.RDprint.Impf(linha,1,'A VISTA',[negrito]);
                                         inc(linha,2);
                              End
                              Else begin
                                       PPrazoAnterior:=1;
                                       FreltxtRDPRINT.RDprint.Impf(linha,1,'ADIANTADO',[negrito]);
                                       inc(linha,2);
                              End;
                         End;
               End;


               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra_a_Esquerda(fieldbyname('CODIGOLANCAMENTO').asstring,9,' ')+' '+
                                    CompletaPalavra(fieldbyname('DATAlancamento').asstring,10,' ')+' '+
                                    CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorlancamento').asstring),12,' ')+' '+
                                    CompletaPalavra(fieldbyname('titulo').asstring+'-'+fieldbyname('historicotitulo').asstring,27,' ')+' '+
                                    CompletaPalavra_a_Esquerda(fieldbyname('pendencia').asstring,09,' ')+' '+
                                    CompletaPalavra_a_Esquerda(fieldbyname('vencimentopendencia').asstring,10,' ')+' '+
                                    CompletaPalavra_a_Esquerda(fieldbyname('lancamentopai').asstring,9,' '));
               inc(linha,1);

               somageral:=somageral+fieldbyname('valorlancamento').asfloat;
               totaldata:=totaldata+fieldbyname('valorlancamento').asfloat;

               if (Fieldbyname('vencimentopendencia').asstring=fieldbyname('datalancamento').asstring)
               Then Psoma_a_Vista:=Psoma_a_Vista+fieldbyname('valorlancamento').asfloat;
               next;
          End;
          //********************************************************************
          //totalizando o ultimo

          inc(linha,1);

          //Totalizando os recebimentos a Vista
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('RECEBIMENTOS A VISTA DATA',40,' ')+' '+
                                               CompletaPalavra_a_Esquerda(formata_valor(PSoma_a_Vista),12,' '),[negrito]);
          inc(linha,1);

          //Totalizando os recebimentos que n�o s�o a Vista
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('ADIANTADOS E ATRASADOS',40,' ')+' '+
                                               CompletaPalavra_a_Esquerda(formata_valor(Totaldata-Psoma_a_Vista),12,' '),[negrito]);
          inc(linha,1);

          //totaliza data
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('TOTAL DATA',40,' ')+' '+
                                               CompletaPalavra_a_Esquerda(formata_valor(floattostr(TotalData)),12,' '),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra('_',90,'_'));
          inc(linha,1);

          

          //Somando os portadores
          Self.ObjQueryLocal.Close;
          Self.ObjQueryLocal.SQL.clear;
          Self.ObjQueryLocal.SQL.add('Select portador,tabportador.nome,sum(valor) as SOMA');
          Self.ObjQueryLocal.SQL.add('from ProcRetornaRecebimentoData ('+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+','+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+') A');
          Self.ObjQueryLocal.SQL.add('join tabportador on A.portador=tabportador.codigo');
          Self.ObjQueryLocal.SQL.add('group by portador,tabportador.nome');
          Self.ObjQueryLocal.open;
          Self.ObjQueryLocal.last;
          FMostraBarraProgresso.ConfiguracoesIniciais(Self.ObjQueryLocal.recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Gerando relat�rio';
          Self.ObjQueryLocal.first;

          while not(Self.ObjQueryLocal.eof) do
          begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               application.processmessages;

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra(Self.ObjQueryLocal.fieldbyname('portador').asstring+'-'+Self.ObjQueryLocal.fieldbyname('nome').asstring,40,' ')+' '+
                                                    CompletaPalavra_a_Esquerda(formata_valor(Self.ObjQueryLocal.fieldbyname('soma').asstring),12,' '),[negrito]);
               inc(linha,1);
               Self.ObjQueryLocal.next;
          End;

          //Somando o Troco
          Self.ObjQueryLocal.Close;
          Self.ObjQueryLocal.SQL.clear;
          Self.ObjQueryLocal.SQL.add('Select sum(valor) as SOMA from ProcRetornaTrocoRecebimentoData('+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+','+#39+formatdatetime('mm/dd/yyyy',strtodate(pdataanterior))+#39+')');
          Self.ObjQueryLocal.open;
          PsomaTroco:=Self.ObjQueryLocal.fieldbyname('soma').asfloat;

          if (PsomaTroco>0)
          then begin
                    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                    FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('TROCO',40,' ')+' '+
                                                         CompletaPalavra_a_Esquerda(formata_valor(Self.ObjQueryLocal.fieldbyname('soma').asstring),12,' '),[negrito]);
                    inc(linha,1);
          End;

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra('_',90,'_'));
          inc(linha,2);

          //********************************************************************
          FMostraBarraProgresso.close;
          FreltxtRDPRINT.RDprint.fechar;

     End;
end;



//******************************************************************************
procedure TObjLancamento.Imprime_Recebimento_Portador_Sintetico;
var
complemento:string;
data1,data2:string;
pportador:string;
linha:integer;
somageral:Currency;
begin
     //Conforme solicitado pela Camila
     //Filtrar somente os lancamentos que ocorreram
     //na data diferente da de criacao
     //por isso em opcaorel

     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Todos');
          RgOpcoes.Items.add('Lan�amentos em Datas diferentes da data de cria��o da pend�ncia');
          showmodal;

          if (tag=0)
          Then exit;

          case RgOpcoes.itemindex of
              0:complemento:='';
              1:Complemento:='and tablancamento.data<>cast(tabpendencia.datac as date)';
          End;
     End;

     //filtrando
     with Ffiltroimp do
     Begin
          limpaedit(FfiltroImp);
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          LbGrupo01.caption:='Lan�amento Inicial';
          LbGrupo02.caption:='Lan�amento Final';
          LbGrupo03.caption:='Portador do Recebimento';
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          edtgrupo03.EditMask:='';
          edtgrupo03.OnKeyDown:=Self.edtPortadorKeyDown;
          edtgrupo03.Color:=$005CADFE;

          showmodal;

          if (tag=0)
          Then exit;

          Try
             strtodate(edtgrupo01.Text);
             data1:=edtgrupo01.Text;
          Except
                data1:='';
          End;

          Try
             strtodate(edtgrupo02.Text);
             data2:=edtgrupo02.Text;
          Except
                data2:='';
          End;

          Try
             StrToInt(edtgrupo03.text);
             pportador:=edtgrupo03.text;
             if (Self.Pendencia.Titulo.PORTADOR.LocalizaCodigo(pportador)=False)
             Then Begin
                       Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
                       exit;
             End;
             Self.Pendencia.Titulo.PORTADOR.TabelaparaObjeto;

          Except
                pportador:='';
          End;

     End;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;

          if (FOpcaorel.RgOpcoes.ItemIndex=1)//Somente os fora data de criacao
          Then Begin
                  SelectSQL.add('select Pdatalancamento as data,pportadorvalores as portador,pnomeportadorvalores as nomeportador,sum(pvalorvalores) as SOMAPORTADOR');
                  SelectSQL.add('from PROCLANCAMENTOS_REC_FORADATA');
          End
          Else BEgin//todos
                    SelectSQL.add('select Pdatalancamento as data,pportadorvalores as portador,pnomeportadorvalores as nomeportador,sum(pvalorvalores) as SOMAPORTADOR');
                    SelectSQL.add('from PROCLANCAMENTOS_REC_TODOS');
          End;

          SelectSQL.add('where plancamento<>-100');//soh para ter o where


          if (data1<>'')
          Then SelectSQL.add('and Pdatalancamento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(data1))+#39);

          if (data2<>'')
          Then SelectSQL.add('and Pdatalancamento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(data2))+#39);

          if (pportador<>'')
          Then SelectSQL.add('and pportadorvalores='+pportador);

          SelectSQL.add('group by Pdatalancamento,pportadorvalores,pnomeportadorvalores');
          SelectSQL.add('order by PdataLancamento,PPortadorValores');
          open;
          first;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum informa��o foi selecionada!',mtinformation,[mbok],0);
                    exit;
          End;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.fechar;
                    exit;
          End;
          linha:=3;

          FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'TOTAL SINT�TICO RECEBIDO POR PORTADOR',[negrito]);
          inc(linha,2);

          if (Data1<>'')
          Then Begin
                  if (data2<>'')
                  Then FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos entre '+data1+' e '+data2,[negrito])
                  Else FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos com data maior ou igual a '+data1,[negrito]);
                  inc(linha,2);
          End
          Else Begin
                   if (data2<>'')
                   Then Begin
                            FreltxtRDPRINT.RDprint.ImpF(linha,1,'Lan�amentos com data menor ou igual a '+data2,[negrito]);
                            inc(linha,2);
                   End;
          End;

          if (complemento<>'')
          Then Begin
                    FreltxtRDPRINT. RDprint.ImpF(linha,1,'OBS: SOMENTE LAN�AMENTOS OCORRIDOS EM DATAS DIFERENTES DA CRIA��O DO T�TULO',[negrito]);
                    inc(linha,2);
          end;

          if (pportador<>'')
          Then Begin
                    FreltxtRDPRINT. RDprint.ImpF(linha,1,'Portador: '+pportador+' - '+Self.Pendencia.Titulo.PORTADOR.Get_Nome,[negrito]);
                    inc(linha,2);
          End;

          //colunas
          FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('DATA',10,' ')+' '+
                                              CompletaPalavra('PORTADOR',50,' ')+' '+
                                              CompletaPalavra_a_Esquerda('SOMA',12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,2);
          somageral:=0;
          While not(eof) do
          Begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('DATA').asstring,10,' ')+' '+
                                              CompletaPalavra(fieldbyname('PORTADOR').asstring+'-'+fieldbyname('nomeportador').asstring,50,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SOMAportador').asstring),12,' '));
               inc(linha,1);
               somageral:=somageral+fieldbyname('somaportador').asfloat;
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.ImpF(linha,62,CompletaPalavra_a_Esquerda(formata_valor(floattostr(somageral)),12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.fechar;


     End;
end;

Function TObjLancamento.NovoLancamento(Ppendencia: string;PPagarReceber:string;PValorLancamento:string;PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean):Boolean;
Begin
     //esse procedimento nao vem o credor devedor nem seu codigo, esses parametros s� s�o preenchidos quando
     //� um recebimento em lote, assim pode ser usado no recebimento de cheques de terceiro caso o cliente
     //seja apenas um
     Result:=Self.NovoLancamento(Ppendencia,PPagarReceber,PValorLancamento,PcodigoLancamento,PhistoricoLancamento,PComCommit,PExportaContabilidade,PLancaAutomatico,'','',datetostr(RetornaDataServidor),0);
End;



Function TObjLancamento.NovoLancamento(Ppendencia: string;PPagarReceber:string;PValorLancamento:string;PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;PvalorMaior:currency):Boolean;
Begin
     //esse tem o valor maior a mais
     Result:=Self.NovoLancamento(Ppendencia,PPagarReceber,PValorLancamento,PcodigoLancamento,PhistoricoLancamento,PComCommit,PExportaContabilidade,PLancaAutomatico,'','',datetostr(RetornaDataServidor),PvalorMaior);
End;



Function TObjLancamento.NovoLancamento(Ppendencia: string;PPagarReceber:string;PValorLancamento:string;var PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;PcredorDevedorLote:string;PcodigoCredorDevedorLote:string;Pdata:string):Boolean;
Begin
     //chama o novo lancamento passando 0 no valor a mais, que funciona da segunte maneira
     //O CLiente paga 100 de uma conta de 70 (frente de caixa amanda), portandfo
     //preciso fazer um lancamento de 70 e na hora de receber digitar 100 para forcar o troco
     //esse 100 que eh diferente do val�or do lancamento vai como Pvalormaior
     Result:=Self.NovoLancamento(Ppendencia,PPagarReceber,PValorLancamento,PcodigoLancamento,PhistoricoLancamento,PComCommit,PExportaContabilidade,PLancaAutomatico,PcredorDevedorLote,PcodigoCredorDevedorLote,Pdata,0);
End;

Function TObjLancamento.NovoLancamento(Ppendencia: string;PPagarReceber:string;PValorLancamento:string;var PcodigoLancamento:string;PhistoricoLancamento:String;PComCommit,PExportaContabilidade,PLancaAutomatico:Boolean;PcredorDevedorLote:string;PcodigoCredorDevedorLote:string;Pdata:string;PvalorMaior:currency):Boolean;

begin
     result:=False;

     Flancanovolancamento.FrValorLancto.Visible:=False;


     self.AtualizaTelaNovoLancamento(Ppendencia,PPagarReceber,PValorLancamento,PcodigoLancamento,PhistoricoLancamento,PComCommit,PExportaContabilidade,PLancaAutomatico,PcredorDevedorLote,PcodigoCredorDevedorLote,Pdata,PvalorMaior);

     (*
     //Se vir a pendencia de parametro � um lancamento normal
     //caso nao venha � um lancamento em lote
     //Vindo a pendencia, as variaveis PreceberPagar e Valor lancamento
     //� preenchido com base na pendencia

     if (Ppendencia<>'')
     Then Begin
             if (Self.pendencia.LocalizaCodigo(ppendencia)=False)
             Then Begin
                      Messagedlg('Pend�ncia n�o encontrada!',mterror,[mbok],0);
                      exit;
             End;

             Self.Pendencia.TabelaparaObjeto;

             if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
             Then PPagarReceber:='P'
             Else PPagarReceber:='R';

             if (PValorLancamento='')
             Then Begin //soh calcula juros se eu nao coloquei o valor do lancamento antes de chamar

                        PValorLancamento:=Self.Pendencia.Get_Saldo;

                        if (ObjParametroGlobal.ValidaParametro('CALCULA JUROS NO LAN�AMENTO?')=False)
                        Then Begin
                                    Messagedlg('O Par�metro "CALCULA JUROS NO LAN�AMENTO?" n�o foi encontrado!',mterror,[mbok],0);
                                    exit;
                        End;

                        
                        if (ObjParametroGlobal.Get_valor='SIM')
                        then Begin
                                  if (ObjParametroGlobal.ValidaParametro('TAXA DE JUROS AO M�S NO LAN�AMENTO (%)')=False)
                                  Then Begin
                                           Messagedlg('O Par�metro "TAXA DE JUROS AO M�S NO LAN�AMENTO (%)" n�o foi encontrado!',mterror,[mbok],0);
                                           exit;
                                  End;


                                  Try
                                     Ptaxa:=strtofloat(ObjParametroGlobal.Get_Valor);
                                     Ptaxa:=Ptaxa/30;//ao dia
                                     ptaxa:=strtofloat(tira_ponto(formata_valor(ptaxa)));
                                  Except
                                     Ptaxa:=0;
                                  End;

                                  Try
                                     Pnumerodias:=0;
                                     if (now>strtodate(Self.Pendencia.Get_VENCIMENTO))//ta vencido
                                     then Begin
                                               Pnumerodias:=strtoint(floattostr(strtodate(datetostr(now))-strtodate(Self.Pendencia.Get_VENCIMENTO)));
                        
                                     End;
                                  Except
                                        Pnumerodias:=0;
                                  End;

                                  Try
                                     Pvalorfinal:=strtofloat(Self.Pendencia.Get_Saldo)+((strtofloat(Self.Pendencia.Get_Saldo)*ptaxa*Pnumerodias)/100);
                                  Except
                                     Pvalorfinal:=0;
                                  End;

                                  FLancaNovoLancamento.FrValorLancto.Visible:=True;
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbSaldoPendencia.Caption:=formata_valor(Self.Pendencia.Get_Saldo);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbTaxaDia.caption:=formata_valor(ptaxa);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbdiaatraso.caption:=inttostr(Pnumerodias);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbSaldoComJuros.caption:=formata_valor(Pvalorfinal);
                                  FLancaNovoLancamento.FrValorLancto.pj_vencimento.caption:=Self.pendencia.get_vencimento;
                                  FLancaNovoLancamento.FrValorLancto.UpDown.Max:=Pnumerodias;
                                  FLancaNovoLancamento.FrValorLancto.edtcarencia.Text:='0';
                                  FLancaNovoLancamento.FrValorLancto.edtdatalancamento.text:=datetostr(now);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbValorJuros.Caption := formata_valor(Pvalorfinal - StrToCurr(Self.Pendencia.Get_Saldo));
                                  PValorLancamento:=tira_ponto(formata_valor(floattostr(pvalorfinal)));
                        End;
             End;//valor lancamento=''
     End
     Else Self.Pendencia.ZerarTabela;

     limpaedit(FLancaNovoLancamento);
     FLancaNovoLancamento.FrValorLancto.edtdatalancamento.text:=datetostr(now);

     FLancaNovoLancamento.PcodigoLancamentoLocal:='';
     FlancaNOvoLancamento.PassaObjeto(self,PPagarReceber,PExportaContabilidade,PComCommit,PLancaAutomatico,PcredorDevedorLote,PcodigoCredorDevedorLote);

     FLancaNovoLancamento.Rgopcoes.ItemIndex:=0;
     FlancaNOvoLancamento.edtpendencia.Text:=Self.pendencia.get_codigo;//Se for em Lote nao vai existir a pendencia
     FLancaNovoLancamento.edtdata.text:=Pdata;


     FLancaNovoLancamento.edtvalor.text:=PValorLancamento;

     if (PvalorMaior>0)
     Then FLancaNovoLancamento.ValorDinheiro:=currtostr(PvalorMaior)
     Else FLancaNovoLancamento.ValorDinheiro:=PValorLancamento;

     FlancaNOvoLancamento.PanelLancamento.Enabled:=True;
     FLancaNovoLancamento.edthistorico.text:=PhistoricoLancamento;

     FlancaNOvoLancamento.PcodigoLancamentoLocal:=PcodigoLancamento;

     *)
     FlancaNOvoLancamento.showmodal;

     PcodigoLancamento:=FlancaNOvoLancamento.PcodigoLancamentoLocal;

     if (FLancaNovoLancamento.Tag=1)
     Then result:=True;

     Self.Status:=dsinactive;

end;

Function TObjLancamento.ExcluiLancamento:boolean;
begin
     result:=Self.ExcluiLancamento('',True,true);
End;

Function TObjLancamento.ExcluiLancamento(Plancamento:string;ComCommit:boolean):boolean;
begin
     result:=Self.ExcluiLancamento(plancamento,ComCommit,true);
End;

function TObjLancamento.ExcluiLancamento(Plancamento:string;ComCommit:boolean;pperguntadesejaexcluirlote:boolean):boolean;
begin
     result:=False;

     if (Plancamento='')
     then Begin
               limpaedit(FfiltroImp);
               With FfiltroImp do
               Begin
     
                    DesativaGrupos;
                    Grupo01.Enabled:=true;
                    LbGrupo01.caption:='Lan�amento';
                    edtgrupo01.EditMask:='';
                    edtgrupo01.OnKeyDown:=Self.edtlancamentokeydown;
                    showmodal;
                    if (tag=0)
                    Then exit;
                    Plancamento:=edtgrupo01.text;
          End;
     End;


     try
        strtoint(Plancamento);
     Except
           Messagedlg('Lan�amento Inv�lido!',mterror,[mbok],0);
           exit;
     End;

     Self.ZerarTabela;
     if (Self.LocalizaCodigo(Plancamento)=False)
     Then Begin
               Messagedlg('Lan�amento n�o localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;
     
     if (Self.LancamentoPai<>'')
     Then Begin
               Messagedlg('O lan�amento escolhido faz parte de uma opera��o em lote, exclua o lan�amento gerador do lote!',mterror,[mbok],0);
               exit;
     End;
     

     With Self.ObjQueryLocal do
     Begin
          close;
          sql.clear;
          sql.add('Select count(codigo) as CONTA from tabLancamento where lancamentopai='+Self.codigo);
          open;
          if (fieldbyname('conta').asinteger>0) and (pperguntadesejaexcluirlote=True)
          then Begin
                    if (Messagedlg('Este lan�amento � um lan�amento em lote, se ele for exclu�do todos as pend�ncias envolvidas neste lote ser�o afetadas. '+#13+'Certeza que deseja exclu�-lo?',mtconfirmation,[mbyes,mbno],0)=mrno)
                    Then exit;
          End;
     End;

     if (Self.ApagaLancamento(Self.codigo,false)=False)
     then Begin
              Messagedlg('Erro na tentativa de Excluir o lan�amento!',mterror,[mbok],0);
              FDataModulo.IBTransaction.RollbackRetaining;
              exit;
     End;

     if (ComCommit=true)
     Then FDataModulo.IBTransaction.CommitRetaining;
     Result:=true;
     Messagedlg('Lan�amento Exclu�do com Sucesso!',mtInformation,[mbok],0);
end;


//Exclui todas as pendencias e seus lancamentos
//do titulo em parametro
function TObjLancamento.ExcluiPendencias(Ptitulo:string;ComCommit:boolean): Boolean;
begin
     Result:=False;
     //este procedimento � responsavel por excluir todas as pendencias
     //do titulo em parametro e seus lancamentos
     With Self.Pendencia.ObjQlocal do
     Begin
          //selecionando as pendencias
          Close;
          Sql.Clear;
          Sql.add('Select codigo from Tabpendencia where titulo='+Ptitulo+' order by codigo');
          open;
          If (RecordCount=0)
          Then Begin//naum tem pendencia � s� excluir o titulo
                    result:=True;
                    exit;
          End;

          first;

          While Not(eof) do
          Begin
             //para cada pendendica exclui os lancamentos delas
             If (Self.ExcluiLancamentosPendencia(Fieldbyname('codigo').asstring,Comcommit)=False)
             Then exit;

             //depois exclui a pendencia
             If (Self.Pendencia.Exclui(fieldbyname('codigo').asstring,ComCOmmit)=False)
             Then Begin
                       Messagedlg('Erro na exclus�o da pend�ncia '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                       exit;
             End;
             next;
          End;
     End;
     result:=True;
end;



function TObjLancamento.ExcluiTitulo(Ptitulo: string;ComCommit: Boolean): Boolean;
begin
     result:=False;
     //Excluindo as pendencias deste titulo
     If (Self.ExcluiPendencias(Ptitulo,ComCommit)=False)
     Then Begin
               If (ComCommit=True)
               Then FDataModulo.IBTransaction.RollbackRetaining;
               exit;
     End;

     Try
        //gerando a contabilidade da exclusao
        Self.Pendencia.Titulo.LocalizaCodigo(Ptitulo);
        Self.Pendencia.Titulo.TabelaparaObjeto;

        if (Self.ObjExportaContabilidade.LocalizaGerador('OBJTITULO',Self.Pendencia.Titulo.get_codigo)=False)
        Then Begin
                  //se nao encontrou o titulo na contabilidade
                  //nao gero nada
                  //Um exemplo:
                  //Nos cheques devolvidos os titulos nao exportam contabilidade
                  //somente os recebimentos e pagamentos deles,
                  //porque a contabilidade � gerada antes do titulo de acordo
                  //com outros detalhes.
                  
                  {If (Self.Pendencia.titulo.GeraContabilidadeExclusao(ComCommit)=False)
                  Then Begin
                          If (ComCommit=True)
                          Then FDataModulo.IBTransaction.RollbackRetaining;
                          Result:=False;
                          exit;
                  End;}
        End
        Else Begin
                  Self.ObjExportaContabilidade.TabelaparaObjeto;
                  if (Self.ObjExportaContabilidade.Get_Exportado='S')
                  Then Begin//ja foi exportado
                            If (Self.Pendencia.titulo.GeraContabilidadeExclusao(ComCommit)=False)
                            Then Begin
                                    If (ComCommit=True)
                                    Then FDataModulo.IBTransaction.RollbackRetaining;
                                    Result:=False;
                                    exit;

                            End;
                  End
                  Else Begin
                            If (Self.ObjExportaContabilidade.excluiporgerador('OBJTITULO',Self.Pendencia.Titulo.Get_CODIGO)=False)
                            Then begin
                                      If (ComCommit=True)
                                      Then FDataModulo.IBTransaction.RollbackRetaining;
                                      Result:=False;
                                      exit;
                            end;
                  end;
        end;



        //apagando o titulo
        if (Self.Pendencia.Titulo.Exclui(Ptitulo,ComCOmmit)=False)
        Then Begin
                  If (ComCommit=True)
                  Then FDataModulo.IBTransaction.RollbackRetaining;
                  exit;
        End;

        If (ComCommit=True)
        Then FDataModulo.IBTransaction.CommitRetaining;

        result:=true;

     Except
           If (ComCommit=True)
           Then FDataModulo.IBTransaction.RollbackRetaining;
     End;
end;



procedure TObjLancamento.edtlancamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_titulopesquisa,nil)=True)
            Then Begin
                      If (Fpesquisalocal.showmodal=mrok)
                      Then Begin
                                Tedit(Sender).text:=Fpesquisalocal.querypesq.fieldbyname('codigo').asstring;
                           End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;



function TObjLancamento.LancaJuros_NovoObjeto(PPendencia: string;
ValorJuroDesconto: Currency; PData: string;Plancamentopai: string;
out PCodigolancamentoJuros:string;PExportaContabilidade:Boolean;Phistorico:String): boolean;

var
objlancamentotmp:tobjlancamento;
begin
     //esse procedimento cria um novo objeto de lancamento
     //para lancar juros


     result:=False;
     PCodigolancamentoJuros:='';

     Try
        objlancamentotmp:=TObjLancamento.Create;
     Except
           Messagedlg('Erro na tentativa de Cria��o do Objeto de Lan�amento!',mterror,[mbok],0);
           exit;
     End;
     Try
          result:=objlancamentotmp.LancaJuros(PPendencia,ValorJuroDesconto,pdata,Plancamentopai,PExportaContabilidade,Phistorico);
          //guardando o c�digo do lancamento de juros
          PCodigolancamentoJuros:=objlancamentotmp.Get_CODIGO;
     Finally
            objlancamentotmp.Free;
     End;
end;

procedure TObjLancamento.ImprimeRecibo_SIMPLES_TXT(pcodigo,pcodigojuros,PsaldoPendencia: string);
var
pcabecalho:String;
Arq:textfile;
PORTAIMPRESSAO,modeloimpressora:String;
linhatexto,pvalorjuros,temp1,temp2:string;
contlinha,cont:integer;
PImprimeDuasCopas:boolean;
quantidadecaracteres:integer;
comando:string;
begin

     pvalorjuros:='0,00';

     if (pcodigojuros<>'')
     Then Begin
               if (Self.LocalizaCodigo(pcodigojuros)=True)
               then Begin
                         Self.TabelaparaObjeto;
                         pvalorjuros:=formata_valor(self.Get_Valor);
               end;
     End;

     Self.ZerarTabela;
     if (Self.LocalizaCodigo(pcodigo)=False)
     then Begin
               Messagedlg('Lan�amento n� '+pcodigo+' n�o encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;


     if (ObjParametroGlobal.ValidaParametro('PORTA DE IMPRESS�O RECIBO DE LANCTO SIMPLES EM TXT')=FALSE)
     Then portaimpressao:='C:\recibo.txt'
     else begin
               if ObjParametroGlobal.Get_Valor='INI'
               then begin
                       LeChaveIni('impressora.ini','IMPRESSORA','MODELO',modeloimpressora);
                       LeChaveIni('impressora.ini','IMPRESSORA','PORTA',PORTAIMPRESSAO);
               end
               else PORTAIMPRESSAO:=ObjParametroGlobal.Get_Valor;
     End;




     if (ObjParametroGlobal.ValidaParametro('ABRE GAVETA NO RECIBO DE LAN�AMENTO?')=True)
     Then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then Begin
                         try
                              AssignFile(arq,PORTAIMPRESSAO);
                              rewrite(arq);
                              if modeloimpressora='BEMATECH'
                              then begin
                                    comando:=#27+#118+#100;
                                    Writeln(ARQ,' ');
                                    Writeln(ARQ,comando);
                              end
                              else begin
                                  comando:=#027+'v';
                                  Writeln(ARQ,comando);
                                  closefile(arq);
                              end;
                         Except
                                on e:exception do
                                Begin
                                    Messagedlg('Erro na tentativa de abrir a porta de impress�o'+#13+'Erro: '+e.message,mterror,[mbok],0);
                                    exit;
                                End;
                         End;
               End;
     End;

     if (ObjParametroGlobal.ValidaParametro('PERGUNTA SE DESEJA IMPRIMIR O RECIBO AP�S LAN�AMENTO A RECEBER?')=False)
     Then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then Begin
               if (messagedlg('Pressione <YES> para Imprimir ou <NO> para n�o imprimir o recibo',mtinformation,[mbyes,mbno],0)=mrno)
               Then exit;
     End;//se for nao vai direto pra impressao



     try
       AssignFile(arq,PORTAIMPRESSAO);
       rewrite(arq);
     Except
           Messagedlg('Erro na tentativa de abrir a porta de impress�o!',mterror,[mbok],0);
           exit;
     End;

     Try


        
        //o recibo soh tem 50 caracteres (NAO, O RECIBO TINHA 50, AGORA O MERCADO DOURADINA PRECISA DE 48 CARACTERES, VOU PEGAR DE UM PARAMETRO)
        If (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE CARACTERES NO RECIBO SIMPLES EM TXT')=False)
        Then quantidadecaracteres:=50
        Else Begin
             quantidadecaracteres:=StrToInt(ObjParametroGlobal.Get_Valor);
        End;

        //Pensar numa forma de por isso num parametro
        If (ObjParametroGlobal.ValidaParametro('CABECALHO NO RECIBO DE LANCTO ("SEPARAR LINHA COM |" E "MAXIMO 50 CARACTERES POR LINHA")')=False)
        Then PCabecalho:='SEM CABECALHO DEFINIDO'
        Else begin
                  
                  PCabecalho:=ObjParametroGlobal.Get_Valor;
        End;

        //imprimindo o cabecalho
        temp1:='';
        for cont:=1 to length(pcabecalho) do
        Begin
             if (pcabecalho[cont]='|')
             Then Begin
                       Writeln(arq,completapalavra(temp1,quantidadecaracteres,' '));
                       temp1:='';
             End
             Else temp1:=temp1+pcabecalho[cont];
        End;

        if (temp1<>'')
        Then Writeln(arq,completapalavra(temp1,quantidadecaracteres-2,' '));

        Writeln(arq,' ');
        Writeln(arq,centraliza_string('RECIBO DE PAGAMENTO',quantidadecaracteres-2));
        Writeln(arq,' ');
        Writeln(arq,completapalavra('_',quantidadecaracteres-2,'_'));
        //dados do cliente
        temp1:='';
        temp2:='';
        temp1:=Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
        temp2:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        //nome
        Writeln(arq,completapalavra(temp1+' - '+temp2,quantidadecaracteres-2,' '));
        //endereco
        temp1:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        Writeln(arq,completapalavra(temp1,quantidadecaracteres-2,' '));
        //bairro
        temp1:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_BairroCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        Writeln(arq,completapalavra(temp1,quantidadecaracteres-2,' '));
        //cidade
        temp1:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_CidadeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        temp2:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_EstadoCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        Writeln(arq,completapalavra(temp1,quantidadecaracteres-5,' ')+' '+completapalavra(temp2,02,' '));
        //cpf
        temp1:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_CPFCNPJCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        Writeln(arq,'CNPJ/CPF: '+completapalavra(temp1,quantidadecaracteres-12,' '));
        //RG
        temp1:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_RGIECredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        Writeln(arq,'IE/RG: '+completapalavra(temp1,quantidadecaracteres-9,' '));
        //telefone
        temp1:=Self.Pendencia.Titulo.CREDORDEVEDOR.Get_TelefoneCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
        Writeln(arq,'Telefone: '+completapalavra(temp1,quantidadecaracteres-12,' '));
        Writeln(arq,completapalavra('_',quantidadecaracteres-2,'_'));
        //Titulo e Pendencia
        temp1:=Self.Pendencia.Titulo.get_codigo;
        temp2:=Self.Pendencia.Get_codigo;
        Writeln(arq,completapalavra('T�tulo : '+temp1+'  Pend�ncia: '+temp2,quantidadecaracteres-2,' '));
        //dados da parcela
        temp2:=Self.Pendencia.Titulo.Get_EMISSAO;
        temp1:=Self.Pendencia.Get_Historico;
        Writeln(arq,'Parcela   : '+completapalavra(temp1,quantidadecaracteres-40,' ')+'   Data Fatura: '+completapalavra(temp2,10,' '));
        temp1:=Self.Pendencia.Get_VENCIMENTO;
        Writeln(arq,'Vencimento: '+completapalavra(temp1,10,' '));
        temp1:=PsaldoPendencia;
        Writeln(arq,'Saldo     :'+completapalavra_a_esquerda(formata_valor(temp1),quantidadecaracteres-13,' '));
        temp1:=Self.Get_Data;
        Writeln(arq,'Data Pgto : '+temp1);
        Writeln(arq,'Juros     :'+completapalavra_a_esquerda(pvalorjuros,quantidadecaracteres-13,' '));
        temp1:=Self.Get_Valor;
        Writeln(arq,'Valor Pago:'+completapalavra_a_esquerda(formata_valor(temp1),quantidadecaracteres-13,' '));
        If(Self.Pendencia.Get_CODIGO<>'')
        Then Begin
              temp1:= FloatToStr( StrToFloat(PsaldoPendencia) - StrToFloat(Self.Get_Valor) );
              Writeln(arq,'Saldo Restante:'+completapalavra_a_esquerda(formata_valor(temp1),quantidadecaracteres-17,' '));
        End;
        Writeln(arq,completapalavra('_',quantidadecaracteres-2,'_'));
        Writeln(arq,' ');
        Writeln(arq,completapalavra('Assinatura:',quantidadecaracteres-2,'_'));
        Writeln(arq,' ');
        Writeln(arq,completapalavra('_',quantidadecaracteres-2,'_'));
        Writeln(arq,centraliza_string('N�O TEM VALOR FISCAL',quantidadecaracteres-2));
        Writeln(arq,completapalavra('_',quantidadecaracteres-2,'_'));

        temp1:=formatdatetime('dd/mm/yyyy hh:mm',now);
        temp2:=Self.Pendencia.Get_CODIGO+Self.CODIGO+formatdatetime('ddmmyyyyhhmm',strtodatetime(temp1));
        temp2:=CalculaDigitoVerificador(TEMP2);

        Writeln(arq,completapalavra('Aut.: '+Self.Pendencia.Get_CODIGO+'|'+Self.CODIGO+'|'+temp1+'|'+temp2,quantidadecaracteres-2,' '));
        //***para poder destacar****
        Writeln(arq,' ');
        Writeln(arq,' ');
        Writeln(arq,' ');
        Writeln(arq,' ');
     Finally
            closefile(arq);
     End;
     //*******APOS A IMPRESSAO NO ARQUIVO POSSO USAR O RDPRINT POARA IMPRIMIR, ISSO SOH FUNCIONA
     //*******SE A PORTA DE IMRPESSAO FOR UM ARQUIVO TEXTO
         
     If (ObjParametroGlobal.ValidaParametro('IMPRIME RECIBO DE LANCTO SIMPLES EM TXT NO RDPRINT?')=False)
     Then exit;

     if (ObjParametroGlobal.get_valor<>'SIM')
     then exit;

     if (ObjParametroGlobal.ValidaParametro('IMPRIME DUAS C�PIAS NO A4 DO RECIBO DE LANCTO SIMPLES EM TXT NO RDPRINT?')=False)
     Then exit;

     PImprimeDuasCopas:=false;
     if (ObjParametroGlobal.Get_Valor='SIM')
     Then PImprimeDuasCopas:=true;

     Try
        //abrindo o arquivo e mandando para o Rdprint
        AssignFile(arq,PORTAIMPRESSAO);
        Reset(arq);
     Except
           Messagedlg('N�o foi poss�vel abrir o arquivo '+PORTAIMPRESSAO,mterror,[mbok],0);
           exit;
     End;

     Try
        //*****************************************
        FreltxtRDPRINT.ConfiguraImpressao;
        FreltxtRDPRINT.ImprimeCabecalho:=False;
        FreltxtRDPRINT.LinhaLocal:=1;
        FreltxtRDPRINT.RDprint.Abrir;

        FreltxtRDPRINT.RDprint.OpcoesPreview.Preview:=False;

        if (ObjParametroGlobal.ValidaParametro('MOSTRA TELA DE ESCOLHA DE IMPRESSORA NO RECIBO SIMPLES EM TXT COM RDPRINT?')=True)
        Then Begin
                  if (Objparametroglobal.get_valor='SIM')
                  Then Begin
                            if (FreltxtRDPRINT.RDprint.Setup=False)
                            Then Begin
                                      FreltxtRDPRINT.RDprint.fechar;
                                      exit;
                            End;
                  End;
        End;
        
        contlinha:=1;
        While not eof(arq) do
        begin
             linhatexto:='';
             Readln(arq,linhatexto);
             FreltxtRDPRINT.VerificaLinha;
             FreltxtRDPRINT.RDprint.Imp(FreltxtRDPRINT.LinhaLocal,1,linhatexto);
             inc(FreltxtRDPRINT.LinhaLocal,1);
             inc(contlinha,1);
        End;

        if (PImprimeDuasCopas=true)
        Then Begin
                  closefile(arq);

                  //foi colocando duas linhas a mais pela questao de ficar no meio da folha
                  //o segundo recibo, para poder destacar
                  FreltxtRDPRINT.LinhaLocal:=FreltxtRDPRINT.LinhaLocal+2;

                  Try
                    //abrindo o arquivo e mandando para o Rdprint
                    AssignFile(arq,PORTAIMPRESSAO);
                    Reset(arq);
                  Except
                        Messagedlg('N�o foi poss�vel abrir o arquivo '+PORTAIMPRESSAO,mterror,[mbok],0);
                        exit;
                  End;

                  While not eof(arq) do
                  begin
                      linhatexto:='';
                      Readln(arq,linhatexto);
                      //FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                      FreltxtRDPRINT.RDprint.Imp(FreltxtRDPRINT.LinhaLocal,1,linhatexto);
                      inc(FreltxtRDPRINT.LinhaLocal,1);
                      dec(contlinha,1);
                      if (ContLinha=3)//3 ultimas linhas em branco
                      Then Begin
                                for cont:=contlinha downto 1 do
                                Begin
                                     Readln(arq,linhatexto);
                                end;
                      End;
                  End;
        End;


        FreltxtRDPRINT.RDprint.fechar;
     Finally
            closefile(arq);
     End;

end;


procedure TObjLancamento.Abregaveta;
var
portaimpressao:String;
modeloimpressora:string;
arq:textfile;
comando:string;
begin

     if (ObjParametroGlobal.ValidaParametro('PORTA DE IMPRESS�O RECIBO DE LANCTO SIMPLES EM TXT')=FALSE)
     Then portaimpressao:='C:\recibo.txt'
     else begin
               if ObjParametroGlobal.Get_Valor='INI'
               then begin
                       LeChaveIni('impressora.ini','IMPRESSORA','MODELO',modeloimpressora);
                       LeChaveIni('impressora.ini','IMPRESSORA','PORTA',PORTAIMPRESSAO);
               end
               else PORTAIMPRESSAO:=ObjParametroGlobal.Get_Valor;
     End;
     
     try
       AssignFile(arq,PORTAIMPRESSAO);
       rewrite(arq);
     Except
           Messagedlg('Erro na tentativa de abrir a porta de impress�o!',mterror,[mbok],0);
           exit;
     End;

     Try
                  if modeloimpressora='BEMATECH'
                  then begin
                          comando:=#27+#118+#100;
                          Writeln(ARQ,' ');
                          Writeln(ARQ,comando);
                  end
                  else begin
                        Writeln(ARQ,' ');
                        Writeln(ARQ,#027+'v');
                        sleep(500);
                  end;
     finally
            closefile(arq);
     End;
end;

procedure TObjLancamento.InicializaImpressoraNaoFiscal;
var
portaimpressao:String;
arq:textfile;
comando:string;
Acbr:TACBrECF;
begin

     if (ObjParametroGlobal.ValidaParametro('PORTA DE IMPRESS�O RECIBO DE LANCTO SIMPLES EM TXT')=FALSE)
     Then portaimpressao:='C:\recibo.txt'
     else begin
               if ObjParametroGlobal.Get_Valor='INI'
               then  LeChaveIni('impressora.ini','IMPRESSORA','PORTA',PORTAIMPRESSAO)
               else PORTAIMPRESSAO:=ObjParametroGlobal.Get_Valor;
     End;

     try
        if (Copy(PORTAIMPRESSAO,1,3)<>'COM')
        then exit;
     Except
           exit;
     end;

     IF (ObjParametroGlobal.ValidaParametro('INICIALIZA IMPRESSORA NAO FISCAL?')=True)
     Then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then begin
                   try
                      Acbr:=TACBrECF.Create(nil);
                      Acbr.MemoParams.Add('Aviso_Legal=N�O');
                      Acbr.Modelo:= TACBrECFModelo(1);
                      Acbr.Porta:=portaimpressao;
                      Acbr.Ativar;
                      Acbr.Desativar;
                   Except

                   end;
               end;
     end;


end;

//begin alterado jonas
procedure TObjLancamento.retornaLancto(Plancamento:string);
begin

  if (Plancamento <> '') then
  begin

      try
        StrToInt(Plancamento);
      except
        MensagemAviso('Par�metro Plancamento inv�lido');
        Exit;
      end;

  end
  else
     Exit;



  with ObjQueryLancto do
  begin

    Close;
    SQL.Clear;

    SQL.Add('select *');
    SQL.Add('from view_lancto_valores v');
    SQL.Add('where v.lancamento='+Plancamento);

    Open;

  end;


end;
//end 

function TObjLancamento.LancamentoAutomatico(Ptipo: string;Ppendencia: string; Pvalor: string; Pdata: string; Phistorico: String;
  var PCodigo: string;ComCommit:Boolean): Boolean;
var
TmpGerador:String;
begin
     //Usado para lancar, juros, desconto e troco automaticamente
     //apenas passo os dados o resto � com ele, localizar o tipo
     //de lancto e gravar
     result:=False;
     Ptipo:=uppercase(PTipo);

     if (Self.Pendencia.LocalizaCodigo(Ppendencia)=False)
     Then exit;
     
     Self.Pendencia.TabelaparaObjeto;


     if (PTipo='J')//juros
     Then Begin
              if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
              Then TmpGerador:='JUROS A PAGAR'
              Else TmpGerador:='JUROS A RECEBER';
     End
     Else
        if(Ptipo='D')
        Then Begin
                  if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
                  Then TmpGerador:='DESCONTO A PAGAR'
                  Else TmpGerador:='DESCONTO A RECEBER';
        End
        {Else
           if (Ptipo='T')
           Then TmpGerador:='TROCO'}
            Else Begin
                     Messagedlg('Tipo de Lan�amento Inv�lido!',mterror,[mbok],0);
                     exit;
            End;

     If (Self.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO',TmpGerador)=False)
     Then Begin
               Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="'+TmpGerador+'", n�o foi encontrado!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjGeraLancamento.TabelaparaObjeto;
     Self.ZerarTabela;
     Self.Status:=dsInsert;
     PCodigo:=Self.Get_NovoCodigo;
     Self.Submit_CODIGO(Pcodigo);
     Self.Pendencia.submit_codigo(Ppendencia);
     Self.TipoLancto.Submit_CODIGO(Self.ObjGeraLancamento.Get_TipoLAncto);
     Self.Submit_Valor(pvalor);
     Self.Submit_Historico(Phistorico);
     Self.Submit_Data(Pdata);
     Self.Submit_LancamentoPai('');
     if (PTipo='J')//juros
     Then if not(VerificaPermissaoDescontoAcrescimo(self,'A'))
           then exit;

     if(Ptipo='D')
     Then if not(VerificaPermissaoDescontoAcrescimo(self,'D'))
          then exit;

     Result:=Self.Salvar(ComCommit,false,'0',false,false);
end;

procedure TObjLancamento.ImprimeReciboPagamento(PcodigoLancamento: string; pTitulo : string = '');
begin

  if (ObjParametroGlobal.ValidaParametro('MULTIPLOS RECIBO DE PAGAMENTO?')=True) then
  begin
    if (ObjParametroGlobal.Get_Valor='SIM') then
    begin
      with FOpcaorel do
      begin
        RgOpcoes.Items.clear;
        RgOpcoes.Items.add('Modelo 1');
        RgOpcoes.Items.add('Modelo 2 - Nome e CPF 2 do cheque de terceiro');
        RgOpcoes.Items.add('Modelo 3 - Colorido com Logotipo');

        //RgOpcoes.Items.add('Modelo 3 - Detalhamento de Lan�amento');
        showmodal;
        if Tag=0 then
          Exit;

        case RgOpcoes.ItemIndex of
          0 : Self.ImprimeReciboPagamento_Modelo1(PcodigoLancamento);
          1 : Self.ImprimeReciboPagamento_Modelo2(PcodigoLancamento);
          2 : Self.ImprimeReciboPagamento_ReportBuilder(PcodigoLancamento, pTitulo);
          //3 : Self.ImprimeReciboPagamento_Modelo3_DetalhamentoLancamento(PcodigoLancamento);
        end;

      end;
      Exit;    
    end;
  end;

  if not(ObjParametroGlobal.ValidaParametro('MODELO DE RECIBO DE LAN�AMENTO')) then
    Exit;

  if (ObjParametroGlobal.Get_Valor='PERSONALIZADO REPORTBUILDER') then
    Self.ImprimeReciboPagamento_ReportBuilder(PcodigoLancamento, pTitulo);

  //Self.ImprimeReciboPagamento_Modelo1(PcodigoLancamento);
end;

procedure TObjLancamento.ImprimeReciboPagamento_Modelo1(PcodigoLancamento: string);
var
  pcredordevedor,pcodigocredordevedor:string;
  PImprimeDuasCopas:boolean;
  copias,cont:integer;

begin

     Self.ZerarTabela;
     if (Self.LocalizaCodigo(PCodigoLancamento)=False)
     Then Begin
               Messagedlg('Lan�amento n�o localizado',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;


     FRelPedidoRdPrint.ConfiguraImpressao;
     FRelPedidoRdPrint.RDprint1.Abrir;

     if (FRelPedidoRdPrint.RDprint1.Setup=False)
     Then Begin
               FRelPedidoRdPrint.RDprint1.Fechar;
               exit;
     End;

     If (ObjParametroGlobal.ValidaParametro('IMPRIME DUAS C�PIAS NO A4 DO RECIBO DE LANCTO SIMPLES EM TXT NO RDPRINT?')=False)
     Then exit;

     copias:=1;

     PImprimeDuasCopas:=false;
     If (ObjParametroGlobal.Get_Valor='SIM')
     Then Begin
          PImprimeDuasCopas:=true;
          copias:=2;
     End;

     For cont:=1 to copias do
     Begin
          If(cont=1)
          Then FRelPedidoRdPrint.LinhaLocal:=3
          Else FRelPedidoRdPrint.LinhaLocal:=FRelPedidoRdPrint.LinhaLocal+13;

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.ImpC(FRelPedidoRdPrint.LinhaLocal,45,'RECIBO DE PAGAMENTO',[negrito]);
          inc(FRelPedidoRdPrint.LinhaLocal,2);

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Lan�amento N�:'+Self.Get_CODIGO);
          inc(FRelPedidoRdPrint.LinhaLocal,1);

          if (Self.LancamentoPai<>'')
          Then Begin
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'O Lan�amento pertence a um lan�amento em lote. Lan�amento Principal N�:'+Self.LancamentoPai,[negrito]);
                    inc(FRelPedidoRdPrint.LinhaLocal,1);
          end;

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Data         :'+Self.Get_Data);
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Hist�rico    :'+Self.Get_Historico);
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Valor        :'+formata_valor(Self.valor));
          inc(FRelPedidoRdPrint.LinhaLocal,1);

          if (Self.Pendencia.Get_CODIGO<>'')
          Then Begin//nao � em lote
                   FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia  N�:'+completapalavra(Self.Pendencia.Get_CODIGO,10,' ')+' Valor Lan�amento: '+CompletaPalavra_a_Esquerda(formata_valor(Self.Valor),12,' ')+' NF: '+completapalavra(Self.Pendencia.Titulo.Get_NotaFiscal,9,' ')+' Parcela: '+completapalavra(Self.Pendencia.Get_Historico,10,' '));
                   inc(FRelPedidoRdPrint.LinhaLocal,1);

                   FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                   inc(FRelPedidoRdPrint.LinhaLocal,1);
                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'CNPJ/CPF     : '+CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.Get_CPFCNPJCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR),14,' '));
                    inc(FRelPedidoRdPrint.LinhaLocal,1);
          End
          Else Begin
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia(s) do lote');
                    inc(FRelPedidoRdPrint.LinhaLocal,1);

                    self.ObjQueryLocal.Close;
                    self.ObjQueryLocal.sql.clear;
                    self.ObjQueryLocal.sql.add('Select TabLancamento.codigo,TabLancamento.pendencia,TabLancamento.valor,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
                    self.ObjQueryLocal.sql.add('from TabLancamento');
                    self.ObjQueryLocal.sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                    self.ObjQueryLocal.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                    self.ObjQueryLocal.sql.add('where LancamentoPai='+PcodigoLancamento);
                    self.ObjQueryLocal.sql.add('order by');
                    self.ObjQueryLocal.sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor,tabpendencia.codigo');
                    self.ObjQueryLocal.open;
                    self.ObjQueryLocal.First;

                    Self.LocalizaCodigo(self.ObjQueryLocal.fieldbyname('codigo').asstring);
                    self.TabelaparaObjeto;

                    pcredordevedor:=self.ObjQueryLocal.Fieldbyname('credordevedor').asstring;
                    pcodigocredordevedor:=self.ObjQueryLocal.Fieldbyname('codigocredordevedor').asstring;
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                    inc(FRelPedidoRdPrint.LinhaLocal,1);

                    While not(self.ObjQueryLocal.eof) do
                    Begin
                         Self.LocalizaCodigo(self.ObjQueryLocal.fieldbyname('codigo').asstring);
                         self.TabelaparaObjeto;

                         if (Pcredordevedor<>self.ObjQueryLocal.fieldbyname('credordevedor').asstring)
                         or (pcodigocredordevedor<>self.ObjQueryLocal.fieldbyname('codigocredordevedor').asstring)
                         Then Begin
                                   pcredordevedor:=self.ObjQueryLocal.Fieldbyname('credordevedor').asstring;
                                   pcodigocredordevedor:=self.ObjQueryLocal.Fieldbyname('codigocredordevedor').asstring;
                                   FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                                   inc(FRelPedidoRdPrint.LinhaLocal,1);
                         End;


                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia  N�:'+completapalavra(Self.Pendencia.Get_CODIGO,10,' ')+' Valor Lan�amento: '+CompletaPalavra_a_Esquerda(formata_valor(Self.Valor),12,' ')+' NF: '+completapalavra(Self.Pendencia.Titulo.Get_NotaFiscal,9,' ')+' Parcela: '+completapalavra(Self.Pendencia.Get_Historico,10,' '));
                         inc(FRelPedidoRdPrint.LinhaLocal,1);

                         self.ObjQueryLocal.next;
                    end;

          End;


          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra_a_Esquerda('_',93,'_'));
          inc(FRelPedidoRdPrint.LinhaLocal,1);

          With Self.ObjDataset do
          Begin
               //Imprimindo os Pagamentos em DInheiro
               close;
               SelectSQL.clear;
               SelectSQL.add('Select sum(Pvalor) as SOMA from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''D'' ');
               open;
               if (Fieldbyname('soma').asfloat>0)
               Then begin
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'PAGAMENTO EM DINHEIRO     ',[negrito]);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,27,CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('soma').asfloat),67,' '));
                         inc(FRelPedidoRdPrint.LinhaLocal,2);
               End;

               //Imprimindo os Pagamentos em Cheques de Terceiro
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''C'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'CHEQUES DE 3�',[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CLIENTE/FORNECEDOR',30,' ')+' '+
                                                       CompletaPalavra('BANCO',05,' ')+' '+
                                                       CompletaPalavra('AGENCIA',10,' ')+' '+
                                                       CompletaPalavra('CONTA',10,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra(fieldbyname('pcliente').asstring,30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pBANCO').asstring,05,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pAGENCIA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pCONTA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pNUMCHEQUE').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                         End;
               End;

               //Imprimindo os Pagamentos em Cheques do Portador
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''CP'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CHEQUE(S) EMITIDO(S)',30,' ')+' '+
                                                       CompletaPalavra('BANCO',05,' ')+' '+
                                                       CompletaPalavra('AGENCIA',10,' ')+' '+
                                                       CompletaPalavra('CONTA',10,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CHEQUE EMITIDO',30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pBANCO').asstring,05,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pAGENCIA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pCONTA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pNUMCHEQUE').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                         End;
               End;
               //************TROCO******************
               close;
               SelectSQL.clear;
               SelectSQL. add('Select sum(Pvalor) as SOMA from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''T'' and PEspecie=''D'' ');
               open;
               if (Fieldbyname('soma').asfloat>0)
               Then begin
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'TROCO RECEBIDO EM DINHEIRO:',[negrito]);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,27,CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('soma').asfloat),67,' '));
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
               End;
               //***em cheque****
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''T'' and PEspecie=''C'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'TROCO RECEBIDO EM CHEQUE',[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CLIENTE/FORNECEDOR',30,' ')+' '+
                                                       CompletaPalavra('BANCO',05,' ')+' '+
                                                       CompletaPalavra('AGENCIA',10,' ')+' '+
                                                       CompletaPalavra('CONTA',10,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra(fieldbyname('pcliente').asstring,30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pBANCO').asstring,05,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pAGENCIA').asstring,10,' ')+' '+
                                                  CompletaPalavra(fieldbyname('pCONTA').asstring,10,' ')+' '+
                                                  CompletaPalavra(fieldbyname('pNUMCHEQUE').asstring,10,' ')+' '+
                                                  CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                          End;
               End;
          End;
          inc(FRelPedidoRdPrint.LinhaLocal,3);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,60,'______________________________');
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,60,'          Assinatura');
     End;

     FRelPedidoRdPrint.RDprint1.Fechar;
End;


procedure TObjLancamento.ImprimeReciboPagamento_Modelo3_DetalhamentoLancamento(PcodigoLancamento: string);
var
  pcredordevedor,pcodigocredordevedor:string;
  copias,cont:integer;
  PImprimeDuasCopas:boolean;
begin

     Self.ZerarTabela;
     if (Self.LocalizaCodigo(PCodigoLancamento)=False)
     Then Begin
               Messagedlg('Lan�amento n�o localizado',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     If (ObjParametroGlobal.ValidaParametro('IMPRIME DUAS C�PIAS NO A4 DO RECIBO DE LANCTO SIMPLES EM TXT NO RDPRINT?')=False)
     Then exit;

     copias:=1;
     PImprimeDuasCopas:=False;
     If (ObjParametroGlobal.Get_Valor='SIM')
     Then begin
          copias:=2;
          PImprimeDuasCopas:=True;
     end;

     cont:=1;
     While (cont <= copias) do
     Begin

          FRelPedidoRdPrint.ConfiguraImpressao;
          FRelPedidoRdPrint.RDprint1.Abrir;

          if (FRelPedidoRdPrint.RDprint1.Setup=False)
          Then Begin
                    FRelPedidoRdPrint.RDprint1.Fechar;
                    exit;
          End;

          FRelPedidoRdPrint.LinhaLocal:=3;

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.ImpC(FRelPedidoRdPrint.LinhaLocal,45,'RECIBO',[negrito]);
          inc(FRelPedidoRdPrint.LinhaLocal,2);

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Lan�amento N�:'+Self.Get_CODIGO);
          inc(FRelPedidoRdPrint.LinhaLocal,1);

          if (Self.LancamentoPai<>'')
          Then Begin
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'O Lan�amento pertence a um lan�amento em lote. Lan�amento Principal N�:'+Self.LancamentoPai,[negrito]);
                    inc(FRelPedidoRdPrint.LinhaLocal,1);
          end;

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Data         :'+Self.Get_Data);
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Hist�rico    :'+Self.Get_Historico);
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Valor        :'+formata_valor(Self.valor));
          inc(FRelPedidoRdPrint.LinhaLocal,1);

          if (Self.Pendencia.Get_CODIGO<>'')
          Then Begin//nao � em lote
                   FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia  N�:'+completapalavra(Self.Pendencia.Get_CODIGO,10,' ')+' Valor Lan�amento: '+CompletaPalavra_a_Esquerda(formata_valor(Self.Valor),12,' ')+' NF: '+completapalavra(Self.Pendencia.Titulo.Get_NotaFiscal,9,' ')+' Parcela: '+completapalavra(Self.Pendencia.Get_Historico,10,' '));
                   inc(FRelPedidoRdPrint.LinhaLocal,1);

                   FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                   inc(FRelPedidoRdPrint.LinhaLocal,1);
          End
          Else Begin
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia(s) do lote');
                    inc(FRelPedidoRdPrint.LinhaLocal,1);

                    self.ObjQueryLocal.Close;
                    self.ObjQueryLocal.sql.clear;
                    self.ObjQueryLocal.sql.add('Select TabLancamento.codigo,TabLancamento.pendencia,TabLancamento.valor,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
                    self.ObjQueryLocal.sql.add('from TabLancamento');
                    self.ObjQueryLocal.sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                    self.ObjQueryLocal.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                    self.ObjQueryLocal.sql.add('where LancamentoPai='+PcodigoLancamento);
                    self.ObjQueryLocal.sql.add('order by');
                    self.ObjQueryLocal.sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor,tabpendencia.codigo');
                    self.ObjQueryLocal.open;
                    self.ObjQueryLocal.First;

                    Self.LocalizaCodigo(self.ObjQueryLocal.fieldbyname('codigo').asstring);
                    self.TabelaparaObjeto;

                    pcredordevedor:=self.ObjQueryLocal.Fieldbyname('credordevedor').asstring;
                    pcodigocredordevedor:=self.ObjQueryLocal.Fieldbyname('codigocredordevedor').asstring;
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                    inc(FRelPedidoRdPrint.LinhaLocal,1);

                    While not(self.ObjQueryLocal.eof) do
                    Begin
                         Self.LocalizaCodigo(self.ObjQueryLocal.fieldbyname('codigo').asstring);
                         self.TabelaparaObjeto;

                         if (Pcredordevedor<>self.ObjQueryLocal.fieldbyname('credordevedor').asstring)
                         or (pcodigocredordevedor<>self.ObjQueryLocal.fieldbyname('codigocredordevedor').asstring)
                         Then Begin
                                   pcredordevedor:=self.ObjQueryLocal.Fieldbyname('credordevedor').asstring;
                                   pcodigocredordevedor:=self.ObjQueryLocal.Fieldbyname('codigocredordevedor').asstring;
                                   FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                                   inc(FRelPedidoRdPrint.LinhaLocal,1);


                         End; 

                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia  N�:'+completapalavra(Self.Pendencia.Get_CODIGO,10,' ')+' Valor Lan�amento: '+CompletaPalavra_a_Esquerda(formata_valor(Self.Valor),12,' ')+' NF: '+completapalavra(Self.Pendencia.Titulo.Get_NotaFiscal,9,' ')+' Parcela: '+completapalavra(Self.Pendencia.Get_Historico,10,' '));
                         inc(FRelPedidoRdPrint.LinhaLocal,1);

                         self.ObjQueryLocal.next;
                    end;

          End;


          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra_a_Esquerda('_',93,'_'));
          inc(FRelPedidoRdPrint.LinhaLocal,1);

          With Self.ObjDataset do
          Begin
               //Imprimindo os Pagamentos em DInheiro
               close;
               SelectSQL.clear;
               SelectSQL.add('Select PValor, PPortador  from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''D'' ');
               open;
               //ver com Ronnei, pois todos os pagamentos estao como T(troco)
               
               While not (eof) do
               Begin
                   if (Fieldbyname('PValor').asfloat>0)
                   Then begin
                             FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                             FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'DINHEIRO     ',[negrito]);
                             FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,32,fieldbyname('PPortador').AsString);
                             FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,79,CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('PValor').asfloat),15,' '));
                             inc(FRelPedidoRdPrint.LinhaLocal,1);
                   End;
                   Next;
               end;
               inc(FRelPedidoRdPrint.LinhaLocal,1);

               //Imprimindo os Pagamentos em Cheques de Terceiro
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''C'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'CHEQUES DE 3�',[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CLIENTE/FORNECEDOR',30,' ')+' '+
                                                       CompletaPalavra('PORTADOR',27,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra(fieldbyname('pcliente').asstring,30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pportador').asstring,27,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pnumcheque').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                         End;
               End;

               //Imprimindo os Pagamentos em Cheques do Portador
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''CP'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CHEQUE EMITIDO',30,' ')+' '+
                                                       CompletaPalavra('PORTADOR',27,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('Cheque Emitido',30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pportador').asstring,27,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pnumcheque').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                         End;
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
               End;
               //************TROCO******************
               close;
               SelectSQL.clear;
               SelectSQL. add('Select * from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where PEspecie=''D'' ');

               if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo = 'D') // A Pagar
               then SelectSQL.add('And Pagamento_troco=''T'' ') // Nao MUDA
               else SelectSQL.add('And Pagamento_troco=''P'' ');

               open;

               While not (eof) do
               Begin
                   if (Fieldbyname('PValor').asfloat>0)
                   Then begin
                             FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                             FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'TROCO EM DINHEIRO     ',[negrito]);
                             FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,32,fieldbyname('PPortador').AsString);
                             FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,79,CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('PValor').asfloat),15,' '));
                             inc(FRelPedidoRdPrint.LinhaLocal,1);
                   End;
                   Next;
               end;
              // inc(FRelPedidoRdPrint.LinhaLocal,1);

               
               //***em cheque****
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from RET_PAGTO_COMP_LANCAMENTO('+PcodigoLancamento+')');
               SelectSQL.add('Where PEspecie=''C'' ');

               if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo = 'D') // A Pagar
               then SelectSQL.add('And Pagamento_troco=''T'' ') // Nao MUDA
               else SelectSQL.add('And Pagamento_troco=''P'' ');

               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'TROCO EM CHEQUE',[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CLIENTE/FORNECEDOR',30,' ')+' '+
                                                       CompletaPalavra('PORTADOR',27,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra(fieldbyname('pcliente').asstring,30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pportador').asstring,27,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pnumcheque').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                          End;
               End;
          End;
          inc(FRelPedidoRdPrint.LinhaLocal,3);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,60,'______________________________');
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,60,'          Assinatura');

          FRelPedidoRdPrint.RDprint1.Fechar;
          inc(cont,1);
     End;

End;     

procedure TObjLancamento.ImprimeReciboPagamento_Modelo2(PcodigoLancamento: string);
var
  pcredordevedor,pcodigocredordevedor:string;
  PImprimeDuasCopas:boolean;
  copias,cont:integer;
  ptemp2,pcliente2,ptemp,PAGARRECEBER:string;

begin
     (*
     Solicitado pela Glassbox dia 23/04/2009
     Neste Modelo no lugar do nome do cliente vai o nome e o cpf2 dos cheques de terceiro
     e na hora de imprimir os cheques vai o nome e o cpf2 tmbm

     Foi solicitado tmbm emitir esse recibo tanto de pagamento quanto de recebimento

     N�o emitir em hipotese alguma o credor devedor, apenas o cliente 2
     em casos de recebimento ou pagamento sem o nome2 deixar em branco

     *)

     Self.ZerarTabela;
     if (Self.LocalizaCodigo(PCodigoLancamento)=False)
     Then Begin
               Messagedlg('Lan�amento n�o localizado',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     if (Self.Pendencia.get_codigo='')//� o principal do lote
     Then Begin
               Self.ObjQueryLocal.Close;
               Self.ObjQueryLocal.sql.clear;
               Self.ObjQueryLocal.sql.add('Select pendencia from tablancamento where lancamentopai='+Self.CODIGO);
               Self.ObjQueryLocal.open;

               Self.Pendencia.LocalizaCodigo(Self.ObjQueryLocal.fieldbyname('pendencia').asstring);
               Self.Pendencia.TabelaparaObjeto;
     End;

     if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C')
     Then PAGARRECEBER:='R'
     Else PAGARRECEBER:='P';

     Self.LocalizaCodigo(PcodigoLancamento);
     Self.TabelaparaObjeto;




     FRelPedidoRdPrint.ConfiguraImpressao;
     FRelPedidoRdPrint.RDprint1.Abrir;

     if (FRelPedidoRdPrint.RDprint1.Setup=False)
     Then Begin
               FRelPedidoRdPrint.RDprint1.Fechar;
               exit;
     End;

     If (ObjParametroGlobal.ValidaParametro('IMPRIME DUAS C�PIAS NO A4 DO RECIBO DE LANCTO SIMPLES EM TXT NO RDPRINT?')=False)
     Then exit;

     copias:=1;

     PImprimeDuasCopas:=false;
     If (ObjParametroGlobal.Get_Valor='SIM')
     Then Begin
          PImprimeDuasCopas:=true;
          copias:=2;
     End;

     For cont:=1 to copias do
     Begin
          If(cont=1)
          Then FRelPedidoRdPrint.LinhaLocal:=3
          Else FRelPedidoRdPrint.LinhaLocal:=FRelPedidoRdPrint.LinhaLocal+13;

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          if (PAGARRECEBER='P')
          Then FRelPedidoRdPrint.RDprint1.ImpC(FRelPedidoRdPrint.LinhaLocal,45,'RECIBO DE PAGAMENTO',[negrito])
          Else FRelPedidoRdPrint.RDprint1.ImpC(FRelPedidoRdPrint.LinhaLocal,45,'RECIBO DE RECEBIMENTO',[negrito]);


          inc(FRelPedidoRdPrint.LinhaLocal,2);

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Lan�amento N�:'+Self.Get_CODIGO);
          inc(FRelPedidoRdPrint.LinhaLocal,1);

          if (Self.LancamentoPai<>'')
          Then Begin
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'O Lan�amento pertence a um lan�amento em lote. Lan�amento Principal N�:'+Self.LancamentoPai,[negrito]);
                    inc(FRelPedidoRdPrint.LinhaLocal,1);
          end;






          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Data         :'+Self.Get_Data);
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Hist�rico    :'+Self.Get_Historico);
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Valor        :'+formata_valor(Self.valor));
          inc(FRelPedidoRdPrint.LinhaLocal,1);


          With self.ObjQueryLocal do
          Begin
               close;
               SQL.clear;
               SQL.add('Select Pcliente2,pcpfcliente2 from PROC_RECIBO_LANCTO('+PcodigoLancamento+','+#39+PAGARRECEBER+#39+')');
               SQL.add('Where Pagamento_troco=''P'' and PEspecie=''C'' ');
               open;
               first;
               pCliente2:='';
               While not(eof) do
               begin
                     if (pcliente2<>'')
                     Then Pcliente2:=Pcliente2+' / ';

                     Pcliente2:=Pcliente2+Fieldbyname('Pcliente2').asstring+' CPF: '+Fieldbyname('Pcpfcliente2').asstring;
                     next;
               End;
          End;



          if (Self.Pendencia.Get_CODIGO<>'')
          Then Begin//nao � em lote
                   FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                   FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia  N�:'+completapalavra(Self.Pendencia.Get_CODIGO,10,' ')+' Valor Lan�amento: '+CompletaPalavra_a_Esquerda(formata_valor(Self.Valor),12,' ')+' NF: '+completapalavra(Self.Pendencia.Titulo.Get_NotaFiscal,9,' ')+' Parcela: '+completapalavra(Self.Pendencia.Get_Historico,10,' '));
                   inc(FRelPedidoRdPrint.LinhaLocal,1);

                   {if (pcliente2='')
                   Then Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                   End;}
          End
          Else Begin
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia(s) do lote');
                    inc(FRelPedidoRdPrint.LinhaLocal,1);

                    self.ObjQueryLocal.Close;
                    self.ObjQueryLocal.sql.clear;
                    self.ObjQueryLocal.sql.add('Select TabLancamento.codigo,TabLancamento.pendencia,TabLancamento.valor,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');
                    self.ObjQueryLocal.sql.add('from TabLancamento');
                    self.ObjQueryLocal.sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                    self.ObjQueryLocal.sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                    self.ObjQueryLocal.sql.add('where LancamentoPai='+PcodigoLancamento);
                    self.ObjQueryLocal.sql.add('order by');
                    self.ObjQueryLocal.sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor,tabpendencia.codigo');
                    self.ObjQueryLocal.open;
                    self.ObjQueryLocal.First;

                    Self.LocalizaCodigo(self.ObjQueryLocal.fieldbyname('codigo').asstring);
                    self.TabelaparaObjeto;

                    pcredordevedor:=self.ObjQueryLocal.Fieldbyname('credordevedor').asstring;
                    pcodigocredordevedor:=self.ObjQueryLocal.Fieldbyname('codigocredordevedor').asstring;
                    FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                    FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                    inc(FRelPedidoRdPrint.LinhaLocal,1);

                    While not(self.ObjQueryLocal.eof) do
                    Begin
                         Self.LocalizaCodigo(self.ObjQueryLocal.fieldbyname('codigo').asstring);
                         self.TabelaparaObjeto;

                         {If (Pcliente2='')
                         Then Begin
                                 if (Pcredordevedor<>self.ObjQueryLocal.fieldbyname('credordevedor').asstring)
                                 or (pcodigocredordevedor<>self.ObjQueryLocal.fieldbyname('codigocredordevedor').asstring)
                                 Then Begin
                                           pcredordevedor:=self.ObjQueryLocal.Fieldbyname('credordevedor').asstring;
                                           pcodigocredordevedor:=self.ObjQueryLocal.Fieldbyname('codigocredordevedor').asstring;
                                           FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                                           FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra(Self.Pendencia.Titulo.CREDORDEVEDOR.get_nome,14,' ')+':'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                                           inc(FRelPedidoRdPrint.LinhaLocal,1);
                                 End;
                         End;}


                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,'Pend�ncia  N�:'+completapalavra(Self.Pendencia.Get_CODIGO,10,' ')+' Valor Lan�amento: '+CompletaPalavra_a_Esquerda(formata_valor(Self.Valor),12,' ')+' NF: '+completapalavra(Self.Pendencia.Titulo.Get_NotaFiscal,9,' ')+' Parcela: '+completapalavra(Self.Pendencia.Get_Historico,10,' '));
                         inc(FRelPedidoRdPrint.LinhaLocal,1);

                         self.ObjQueryLocal.next;
                    end;

          End;


          if (pcliente2<>'')
          Then Begin
                    ptemp2:=pcliente2;
                    //tem Cliente e Cpf 2 dividir em N Linhas
                    While (pcliente2<>'') do
                    Begin
                         ptemp:='';
                         if (length(pcliente2)>90)
                         Then Begin
                                    DividirValorCOMSEGUNDASEMTAMANHO(pcliente2,90,ptemp,ptemp2);
                                    pcliente2:=ptemp2;
                         End
                         Else Begin
                                   ptemp:=ptemp2;
                                   ptemp2:='';
                                   pcliente2:='';
                         End;


                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,ptemp);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                    End;
          End;

          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,CompletaPalavra_a_Esquerda('_',93,'_'));
          inc(FRelPedidoRdPrint.LinhaLocal,1);



          With Self.ObjDataset do
          Begin
               //Imprimindo os Pagamentos em DInheiro
               close;
               SelectSQL.clear;
               SelectSQL.add('Select sum(Pvalor) as SOMA from PROC_RECIBO_LANCTO('+PcodigoLancamento+','+#39+PAGARRECEBER+#39+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''D'' ');
               open;
               if (Fieldbyname('soma').asfloat>0)
               Then begin
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'DINHEIRO     ',[negrito]);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,27,CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('soma').asfloat),67,' '));
                         inc(FRelPedidoRdPrint.LinhaLocal,2);
               End;

               //Imprimindo os Pagamentos em Cheques de Terceiro
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from PROC_RECIBO_LANCTO('+PcodigoLancamento+','+#39+PAGARRECEBER+#39+')');
               SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''C'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'CHEQUES DE 3�',[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CLIENTE/FORNECEDOR',30,' ')+' '+
                                                       CompletaPalavra('BANCO',05,' ')+' '+
                                                       CompletaPalavra('AGENCIA',10,' ')+' '+
                                                       CompletaPalavra('CONTA',10,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra(fieldbyname('pcliente2').asstring,30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pBANCO').asstring,05,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pAGENCIA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pCONTA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pNUMCHEQUE').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                         End;
               End;




               //Imprimindo Cheques do Portador
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from PROC_RECIBO_LANCTO('+PcodigoLancamento+','+#39+PAGARRECEBER+#39+')');
               if (PAGARRECEBER='P')//se for pagamento � P senao � troco
               Then SelectSQL.add('Where Pagamento_troco=''P'' and PEspecie=''CP'' ')
               Else SelectSQL.add('Where Pagamento_troco=''T'' and PEspecie=''CP'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         if (PAGARRECEBER='P')
                        Then Begin
                                   FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                                CompletaPalavra('CHEQUE(S) EMITIDO(S)',30,' ')+' '+
                                                                CompletaPalavra('BANCO',05,' ')+' '+
                                                                CompletaPalavra('AGENCIA',10,' ')+' '+
                                                                CompletaPalavra('CONTA',10,' ')+' '+
                                                                CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                                CompletaPalavra('VENCTO',10,' ')+' '+
                                                                CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                        End
                        Else Begin//troco (A RECEBER)
                                  FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                                CompletaPalavra('TROCO COM CHEQUE(S) EMITIDO(S)',30,' ')+' '+
                                                                CompletaPalavra('BANCO',05,' ')+' '+
                                                                CompletaPalavra('AGENCIA',10,' ')+' '+
                                                                CompletaPalavra('CONTA',10,' ')+' '+
                                                                CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                                CompletaPalavra('VENCTO',10,' ')+' '+
                                                                CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                        End;

                        inc(FRelPedidoRdPrint.LinhaLocal,1);
                        first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CHEQUE EMITIDO',30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pBANCO').asstring,05,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pAGENCIA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pCONTA').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pNUMCHEQUE').asstring,10,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                         End;
               End;


               
               //************TROCO******************
               close;
               SelectSQL.clear;
               SelectSQL. add('Select sum(Pvalor) as SOMA from PROC_RECIBO_LANCTO('+PcodigoLancamento+','+#39+PAGARRECEBER+#39+')');
               SelectSQL.add('Where Pagamento_troco=''T'' and PEspecie=''D'' ');
               open;
               if (Fieldbyname('soma').asfloat>0)
               Then begin
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'TROCO DINHEIRO:',[negrito]);
                         FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,27,CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('soma').asfloat),67,' '));
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
               End;
               //***em cheque****
               close;
               SelectSQL.clear;
               SelectSQL.add('Select * from PROC_RECIBO_LANCTO('+PcodigoLancamento+','+#39+PAGARRECEBER+#39+')');
               SelectSQL.add('Where Pagamento_troco=''T'' and PEspecie=''C'' ');
               open;
               if (Recordcount>0)
               Then Begin
                         //****titulo das colunas*****
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,'TROCO RECEBIDO EM CHEQUE',[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                         FRelPedidoRdPrint.RDprint1.Impf(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra('CLIENTE/FORNECEDOR',30,' ')+' '+
                                                       CompletaPalavra('BANCO',05,' ')+' '+
                                                       CompletaPalavra('AGENCIA',10,' ')+' '+
                                                       CompletaPalavra('CONTA',10,' ')+' '+
                                                       CompletaPalavra('NUMCHEQUE',10,' ')+' '+
                                                       CompletaPalavra('VENCTO',10,' ')+' '+
                                                       CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
                         inc(FRelPedidoRdPrint.LinhaLocal,1);
                         first;
                         While not(eof) do
                         Begin
                              FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
                              FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,1,
                                                       CompletaPalavra(fieldbyname('pcliente2').asstring,30,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pBANCO').asstring,05,' ')+' '+
                                                       CompletaPalavra(fieldbyname('pAGENCIA').asstring,10,' ')+' '+
                                                  CompletaPalavra(fieldbyname('pCONTA').asstring,10,' ')+' '+
                                                  CompletaPalavra(fieldbyname('pNUMCHEQUE').asstring,10,' ')+' '+
                                                  CompletaPalavra(fieldbyname('pVENCimenTO').asstring,10,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('pVALOR').asstring),12,' '));
                              inc(FRelPedidoRdPrint.LinhaLocal,1);
                              next;
                          End;
               End;
          End;
          inc(FRelPedidoRdPrint.LinhaLocal,3);
          FreltxtRDPRINT.VerificaLinha(FRelPedidoRdPrint.RDprint1,FRelPedidoRdPrint.LinhaLocal);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,60,'______________________________');
          inc(FRelPedidoRdPrint.LinhaLocal,1);
          FRelPedidoRdPrint.RDprint1.Imp(FRelPedidoRdPrint.LinhaLocal,60,'          Assinatura');

          (*if (cont<copias)
          Then begin
                    FRelPedidoRdPrint.LinhaLocal:=3;
                    FRelPedidoRdPrint.RDprint1.Novapagina;
          End;*)

     End;

     FRelPedidoRdPrint.RDprint1.Fechar;
End;


function TObjLancamento.RetornaNf_HistoricoPendencia_Titulo(
  Pcodigolancamento: string): String;
var
temp:string;
begin
     temp:='';
     result:='';

     Self.ZerarTabela;

     if (Self.LocalizaCodigo(Pcodigolancamento)=false)
     then exit;

     Self.TabelaparaObjeto;

     if (Self.Pendencia.Get_CODIGO<>'')//nao foi em lote
     Then begin
              result:=Self.Pendencia.Titulo.Get_NotaFiscal+' - '+Self.Pendencia.Get_Historico;
              exit;
     End;

     With Self.ObjQueryLocal do
     begin
          close;
          sql.clear;
          sql.add('Select Tabtitulo.notafiscal,tabpendencia.historico');
          sql.add('from tablancamento tl');
          sql.add('join tabpendencia on tl.pendencia=tabpendencia.codigo');
          sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          sql.add('where tl.lancamentopai='+Pcodigolancamento);
          open;
          first;
          While not(eof) do
          Begin
              if (fieldbyname('notafiscal').asstring<>'')
              then temp:=temp+fieldbyname('notafiscal').asstring+' - '+fieldbyname('historico').asstring+' | '
              Else temp:=temp+fieldbyname('historico').asstring+' | ';
              next;
          end;
          close;
          result:=temp;
     end;
end;

procedure TObjLancamento.ImprimeTitulosPagos;
var
  DataLimite,DataLimite2:Tdate;
  CredorDevedorTemp,CodigoCredorDevedorTemp:string;
  ptotalpago:currency;
  TipoDataPesquisa:string;
  ImprimeDescricao:string;
begin

  with FfiltroImp do
  begin
    DesativaGrupos;
    Grupo01.enabled:=true;
    Grupo02.enabled:=true;
    Grupo07.Enabled:=True;
    Grupo15.Enabled:=True;

    lbGrupo01.caption:='Data Inicial';
    lbGrupo02.caption:='Data Final';
    edtGrupo01.EditMask:='!99/99/9999;1;_';
    edtGrupo02.EditMask:='!99/99/9999;1;_';

    LbGrupo07.caption:='Credor/Devedor';
    ComboGrupo07.Items.clear;
    Combo2Grupo07.Items.clear;
    Self.pendencia.titulo.Get_ListacredorDevedor(ComboGrupo07.items);
    Self.pendencia.titulo.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
    edtgrupo07.onkeydown:=Self.pendencia.titulo.edtcodigocredordevedor_SPV_KeyDown;
    edtgrupo07.color:=$005CADFE;

    ComboGrupo15.Items.Clear;
    lbgrupo15.Caption:='Tipo de Data';
    ComboGrupo15.Items.Add('Por data de vencimento');
    ComboGrupo15.Items.Add('Por data de pagamento');

    Grupo06.Enabled:=True;
    LbGrupo06.Caption:='Mostrar Descri��o do T�tulo?';
    Combo2Grupo06.Visible:=True;
    ComboGrupo06.Visible:=False;
    Combo2Grupo06.Items.Clear;
    Combo2Grupo06.Items.Add('SIM');
    Combo2Grupo06.Items.Add('N�O');

    showmodal;

    if tag=0
    then exit;

    //Conferindo o que foi lancado
    ImprimeDescricao:='N';

    if(Combo2Grupo06.Text='SIM')
    then ImprimeDescricao:='S';

    try
      DataLimite:=Strtodate(edtgrupo01.text);
      DataLimite2:=Strtodate(edtgrupo02.text);
    except
      Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
      exit;
    end;
    TipoDataPesquisa :='V';

    lbgrupo15.Caption:='Filtrar por';
    if(ComboGrupo15.Text='Por data de vencimento')
    then  TipoDataPesquisa :='V';

    if(ComboGrupo15.Text='Por data de pagamento')
    then TipoDataPesquisa := 'P';

    try
      CredorDevedortemp:='';
      strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
      CredorDevedorTemp:=Combo2Grupo07.Items[ComboGrupo07.itemindex];
    except
      CredorDevedorTemp:='';
    end;

    try
      CodigoCredorDevedorTemp:='';
      if (EdtGrupo07.text<>'')then
      begin
        Strtoint(EdtGrupo07.text);
        CodigoCredorDevedorTemp:=EdtGrupo07.text
      end;
    except
      CodigoCredorDevedorTemp:='';
    end;
         
  end;//with das opcoes


  with Self.ObjQueryLocal do
  begin
    close;
    sql.clear;
    sql.add('Select Tablancamento.Pendencia,');
    sql.add('Tabpendencia.vencimento,Tabpendencia.valor as VALORPENDENCIA,');
    sql.add('Tabpendencia.Boletoapagar,');
    sql.add('tabtitulo.notafiscal,tablancamento.data as DATAPAGAMENTO,tablancamento.valor as VALORPAGAMENTO,');
    sql.add('tabtitulo.credordevedor,tabtitulo.codigocredordevedor,tabtitulo.historico');
    sql.add('from tablancamento');
    sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
    sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
    sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
    sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
    sql.add('Where tabcontager.tipo=''D'' and tabtipolancto.classificacao=''Q'' ');

    if(TipoDataPesquisa='V') then
    begin
      sql.add('and tabpendencia.vencimento>='+#39+Formatdatetime('mm/dd/yyyy',DataLimite)+#39);
      sql.add('and tabpendencia.vencimento<='+#39+Formatdatetime('mm/dd/yyyy',DataLimite2)+#39);
    end;

    if(TipoDataPesquisa='P') then
    begin
      sql.add('and tablancamento.data>='+#39+Formatdatetime('mm/dd/yyyy',DataLimite)+#39);
      sql.add('and tablancamento.data<='+#39+Formatdatetime('mm/dd/yyyy',DataLimite2)+#39);
    end;

    if (CredorDevedorTemp<>'')
    then sql.add('and TabTitulo.CredorDevedor='+CredorDevedorTemp);

    if (CodigoCredorDevedorTemp<>'')
    then sql.add('and TabTitulo.CodigoCredorDevedor='+CodigoCredorDevedorTemp);

    sql.add('order by Tablancamento.data,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');

    //InputBox('','',SQL.Text);
    open;

    if (Recordcount=0) then
    begin
      MensagemAviso('Nenhuma informa��o foi selecionada');
      exit;
    end;

    with FreltxtRDPRINT do
    begin
      ConfiguraImpressao;
      LinhaLocal:=3;
      RDprint.Abrir;
      if (RDprint.Setup=False) then
      begin
        RDprint.Fechar;
        exit;
      end;

      rdprint.impc(LinhaLocal,45,Self.NumeroRelatorio+'PEND�NCIAS PAGAS',[negrito]);
      IncrementaLinha(2);

      rdprint.impf(linhalocal,1,'DATA    : '+datetostr(DataLimite)+' a '+datetostr(DataLimite2),[negrito]);
      IncrementaLinha(1);

      if (CodigoCredorDevedorTemp<>'') and (CredorDevedorTemp<>'') then
      begin
        rdprint.impf(linhalocal,1,CompletaPalavra('CREDOR/DEVEDOR: '+CodigoCredorDevedorTemp+'-'+Self.pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(CredorDevedorTemp,CodigoCredorDevedorTemp),90,' '),[negrito]);
        IncrementaLinha(1);
      end;
      IncrementaLinha(1);

      VerificaLinha;
      rdprint.impf(linhalocal,1,CompletaPalavra('PEND.',6,' ')+' '+
        CompletaPalavra('VENCTO',8,' ')+' '+
        CompletaPalavra_a_Esquerda('VALOR PEND',10,' ')+' '+
        CompletaPalavra('BOLETO',6,' ')+' '+
        CompletaPalavra('NF',6,' ')+' '+
        CompletaPalavra('DT PAGTO',8,' ')+' '+
        CompletaPalavra_a_Esquerda('VALOR PAGTO',11,' ')+' '+
        CompletaPalavra('CREDOR/DEVEDOR',28,' '),[negrito]);

      IncrementaLinha(1);
      DesenhaLinha;

      ptotalpago:=0;
      while not(Self.ObjQueryLocal.eof) do
      begin
        VerificaLinha;
        rdprint.imp(linhalocal,1,CompletaPalavra(fieldbyname('pendencia').asstring,6,' ')+' '+
          CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('vencimento').asdatetime),8,' ')+' '+
          CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorpendencia').asstring),10,' ')+' '+
          CompletaPalavra(fieldbyname('boletoapagar').asstring,6,' ')+' '+
          CompletaPalavra(fieldbyname('notafiscal').asstring,6,' ')+' '+
          CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('datapagamento').asdatetime),8,' ')+' '+
          CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorpagamento').asstring),11,' ')+' '+
          CompletaPalavra(fieldbyname('codigocredordevedor').asstring+'-'+Self.pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),28,' '));

        IncrementaLinha(1);
        ptotalpago:=ptotalpago+fieldbyname('valorpagamento').ascurrency;

        if (ImprimeDescricao='S') then
        begin
          rdprint.imp(linhalocal,1,'Descri��o '+fieldbyname('historico').asstring);
          IncrementaLinha(1);
        end;
        
        Self.ObjQueryLocal.next;
      end;

      DesenhaLinha;

      verificalinha;
      rdprint.impf(linhalocal,1,'TOTAL: R$ '+formata_valor(ptotalpago),[negrito]);

      rdprint.Fechar;
    end;
  end;
end;

procedure TObjLancamento.ImprimeLancamentos_CredorDevedor;
var
PtipoContager:string;
PdataInicial,PdataFinal:Tdate;
pcredordevedor,pcodigocredordevedor:string;
Psomaquitacao,psomadesconto,psomajuro:currency;

begin
     With FOpcaorel do
     Begin
          RgOpcoes.items.clear;
          RgOpcoes.items.add('T�tulos a pagar');
          RgOpcoes.items.add('T�tulos a receber');
          RgOpcoes.items.add('Todos');
          Showmodal;

          if (Tag=0)
          then exit;

          case RgOpcoes.itemindex of
            0:PtipoContager:='D';
            1:PtipoContager:='C';
            2:PtipoContager:='';
          End;
     End;

     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.enabled:=True;
          Grupo02.enabled:=True;
          LbGrupo01.caption:='Lancto Inicial';
          LbGrupo02.caption:='Lancto Final  ';
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;

          Showmodal;

          if (tag=0)
          then exit;

          if (Validadata(1,pdatainicial,false)=False)
          then exit;

          if (Validadata(2,pdatafinal,false)=False)
          then exit;
     End;

     With Self.ObjQueryLocal do
     begin
          close;
          SQL.clear;
          sql.add('Select tabtipolancto.Classificacao,');
          sql.add('Tabcontager.tipo as Tipo,');
          sql.add('Tabpendencia.codigo as PENDENCIA,');
          sql.add('Tabtitulo.codigo as TITULO,');
          sql.add('Tabtitulo.historico,');
          sql.add('Tabtitulo.credordevedor,');
          sql.add('Tabtitulo.codigocredordevedor,');
          sql.add('tabcontager.codigo as CG,');
          sql.add('tabcontager.nome as nomecg,');
          sql.add('tablancamento.data as datalancto,');
          sql.add('tablancamento.valor as VALORLANCTO');
          sql.add('');
          sql.add('from tablancamento');
          sql.add('join  tabpendencia on tabpendencia.codigo=tablancamento.pendencia');
          sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
          sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          sql.add('where Tablancamento.Data>='+#39+formatdatetime('mm/dd/yyyy',pdatainicial)+#39);
          sql.add('and tablancamento.Data<='+#39+formatdatetime('mm/dd/yyyy',pdatafinal)+#39);

          if (PtipoContager<>'')
          then sql.add('and TabContager.tipo='+#39+PtipoContager+#39);

          sql.add('order by TabContager.tipo,Tabtitulo.credordevedor,tabtitulo.codigocredordevedor,');
          sql.add('tablancamento.data');
          open;
          last;

          if (recordcount=0)
          then begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          end;
          first;
          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.Abrir;
               LinhaLocal:=3;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;

               if (rdprint.Setup=False)
               then begin
                         rdprint.Fechar;
                         exit;
               End;

               VerificaLinha;
               rdprint.ImpC(linhalocal,65,Self.NumeroRelatorio+'LAN�AMENTOS EFETUADOS NO PER�ODO',[NEGRITO]);
               incrementalinha(2);

               VerificaLinha;
               rdprint.ImpF(linhalocal,1,'Per�odo: '+datetostr(PdataInicial)+' a '+datetostr(pdatafinal),[negrito]);
               incrementalinha(2);

               //titulo das colunas

               VerificaLinha;
               RDprint.Impf(linhalocal,10,CompletaPalavra('CONTA GERENCIAL',30,' ')+' '+
                                          CompletaPalavra('DATA',10,' ')+' '+
                                          CompletaPalavra('Q/D/J',5,' ')+' '+
                                          CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                          CompletaPalavra('TITULO',6,' ')+' '+
                                          CompletaPalavra('PEND.',6,' ')+' '+
                                          CompletaPalavra('HISTORICO',40,' '),[negrito]);
               IncrementaLinha(1);


               PtipoContager:=Fieldbyname('tipo').asstring;
               if (PtipoContager='C')
               Then Begin
                         RDprint.ImpF(linhalocal,1,'CONTAS A RECEBER',[negrito]);
                         incrementalinha(2);
               End
               Else Begin
                         RDprint.ImpF(linhalocal,1,'CONTAS A PAGAR',[negrito]);
                         incrementalinha(2);
               End;

               pcredordevedor:=Fieldbyname('credordevedor').asstring;
               pcodigocredordevedor:=Fieldbyname('codigocredordevedor').asstring;

               Psomaquitacao:=0;
               psomadesconto:=0;
               psomajuro:=0;

               While not(Self.ObjQueryLocal.eof) do
               begin
                    if (PtipoContager<>fieldbyname('tipo').asstring)
                    then Begin//SE MUDOU � PORQUE ANTES ERA CREDITO, A ORDEM ALFABETICA ME GARANTE ISSO
                              //totalizando Quitacao, desconto e juros
                              incrementalinha(1);
                              VerificaLinha;
                              RDprint.ImpF(linhalocal,1,'TOTAL EM QUITA��O  R$ '+formata_valor(Psomaquitacao),[negrito]);
                              incrementalinha(1);

                              VerificaLinha;
                              RDprint.ImpF(linhalocal,1,'TOTAL EM DESCONTOS R$ '+formata_valor(Psomadesconto),[negrito]);
                              incrementalinha(1);

                              VerificaLinha;
                              RDprint.ImpF(linhalocal,1,'TOTAL EM JUROS     R$ '+formata_valor(Psomajuro),[negrito]);
                              incrementalinha(1);
                              
                              rdprint.Novapagina;
                              linhalocal:=3;

                              RDprint.ImpF(linhalocal,1,'CONTAS A PAGAR',[negrito]);
                              incrementalinha(2);

                              
                              //toda vez que mudo de receber para pagar ele tem q mostrar
                              //o credordevedor de novo,por isso limpo
                              pcredordevedor:='';
                              pcodigocredordevedor:='';
                              Psomaquitacao:=0;
                              psomadesconto:=0;
                              psomajuro:=0;
                              PtipoContager:=Fieldbyname('tipo').asstring;
                    end;

                    if(pcredordevedor<>Fieldbyname('credordevedor').asstring) or
                      (pcodigocredordevedor<>Fieldbyname('codigocredordevedor').asstring)
                    Then Begin
                              pcredordevedor:=Fieldbyname('credordevedor').asstring;
                              pcodigocredordevedor:=Fieldbyname('codigocredordevedor').asstring;
                              incrementalinha(1);
                              
                              VerificaLinha;
                              RDprint.Imp(linhalocal,1,pcodigocredordevedor+'-'+Self.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(pcredordevedor,pcodigocredordevedor));
                              incrementalinha(2);
                    End;
                    //**********************************************************
                    VerificaLinha;
                    RDprint.Impf(linhalocal,10,CompletaPalavra(Fieldbyname('cg').asstring+'-'+Fieldbyname('nomecg').asstring,30,' ')+' '+
                                             CompletaPalavra(Fieldbyname('datalancto').asstring,10,' ')+' '+
                                             CompletaPalavra('  '+Fieldbyname('classificacao').asstring,5,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valorlancto').asstring),12,' ')+' '+
                                             CompletaPalavra(Fieldbyname('titulo').asstring,6,' ')+' '+
                                             CompletaPalavra(Fieldbyname('pendencia').asstring,6,' ')+' '+
                                             CompletaPalavra(Fieldbyname('historico').asstring,40,' '),[italico]);
                    IncrementaLinha(1);
                    //**********************************************************

                    if (Fieldbyname('classificacao').asstring='Q')
                    Then PSomaQuitacao:=Psomaquitacao+Fieldbyname('valorlancto').asfloat
                    Else
                        if (Fieldbyname('classificacao').asstring='D')
                        Then PSomaDesconto:=Psomadesconto+Fieldbyname('valorlancto').asfloat
                        Else
                           if (Fieldbyname('classificacao').asstring='J')
                          Then PSomaJuro:=PsomaJuro+Fieldbyname('valorlancto').asfloat;

                    Self.ObjQueryLocal.next;
               End;
               //***********************************************************
               incrementalinha(1);
               
               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'TOTAL EM QUITA��O  R$ '+formata_valor(Psomaquitacao),[negrito]);
               incrementalinha(1);

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'TOTAL EM DESCONTOS R$ '+formata_valor(Psomadesconto),[negrito]);
               incrementalinha(1);

               VerificaLinha;
               RDprint.ImpF(linhalocal,1,'TOTAL EM JUROS     R$ '+formata_valor(Psomajuro),[negrito]);
               incrementalinha(1);


               rdprint.Fechar;
          end;
     End;
end;

procedure TObjLancamento.Imprime_ContasPagas_por_Dia;
begin
  Self.Imprime_ContasPagas_por_Dia('D');
end;

procedure TObjLancamento.Imprime_ContasPagas_por_Dia(Ptipo:string);
var
datainicial,datafinal:Tdate;
CredorDevedortemp,codigocredordevedortemp:string;
pdata:string;
pvalorperiodo,pdescontos,pjuros,pvalordia:currency;
pvalorpendenciadia,pdescontodia,pjurosdia,totalpendencias,totaldescontos,totaljuros,totalpago:currency;

begin
    With FfiltroImp do
    Begin
         DesativaGrupos;
         Grupo01.Enabled:=True;
         Grupo02.Enabled:=True;
         Grupo07.Enabled:=True;

         LbGrupo01.caption:='Pagamento Inicial';
         LbGrupo02.caption:='Pagamento Final';

         edtGrupo01.EditMask:=MascaraData;
         edtGrupo02.EditMask:=MascaraData;


         LbGrupo07.caption:='Credor/Devedor';
         ComboGrupo07.Items.clear;
         Combo2Grupo07.Items.clear;
         Self.pendencia.titulo.Get_ListacredorDevedor(ComboGrupo07.items);
         Self.pendencia.titulo.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
         edtgrupo07.onkeydown:=Self.pendencia.titulo.edtcodigocredordevedor_SPV_KeyDown;
         edtgrupo07.Color:=$005CADFE;

         showmodal;

         If tag=0
         Then exit;

         //Conferindo o que foi lancado
         Try
               datainicial:=Strtodate(edtgrupo01.text);
               DataFinal:=Strtodate(edtgrupo02.text);
         Except
               Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
               exit;
         End;


         Try
               CredorDevedortemp:='';
               strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
               CredorDevedorTemp:=Combo2Grupo07.Items[ComboGrupo07.itemindex];

               if (Self.pendencia.titulo.CREDORDEVEDOR.LocalizaCodigo(CredorDevedortemp)=False)
               Then Begin
                         MensagemErro('Credor Devedor n�o encontrado');
                         exit;
               End;
               Self.pendencia.titulo.CREDORDEVEDOR.TabelaparaObjeto;
         Except
               CredorDevedorTemp:='';
         End;

         Try
            CodigoCredorDevedorTemp:='';
            if (EdtGrupo07.text<>'')
            Then begin
                    Strtoint(EdtGrupo07.text);
                    CodigoCredorDevedorTemp:=EdtGrupo07.text;
            End;
         Except
             CodigoCredorDevedorTemp:='';
         End;

    End;//with das opcoes

    With Self.ObjQueryLocal do
    Begin
         close;
         sql.clear;
         sql.add('Select Tablancamento.Pendencia, Tablancamento.data,');
         sql.add('sum(tablancamento.valor) as VALORPAGAMENTO,tabtitulo.codigo as titulo');
         sql.add('from tablancamento');
         sql.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
         sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
         sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
         sql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
         sql.add('Where tabtipolancto.classificacao=''Q'' ');
         sql.add('and tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',datainicial)+#39);
         sql.add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',datafinal)+#39);

         if (CredorDevedortemp<>'')
         then sql.add('and tabtitulo.credordevedor='+CredorDevedortemp);

         if (codigocredordevedortemp<>'')
         then sql.add('and tabtitulo.codigocredordevedor='+codigocredordevedortemp);

         sql.add('and tabcontager.tipo='+#39+ptipo+#39);

         sql.add('group by Tablancamento.Pendencia, Tablancamento.data,tabtitulo.codigo');
         sql.add('order by Tablancamento.data');

         open;

         if (recordcount=0)
         Then Begin
                   MEnsagemAviso('Nenhuma Informa��o foi selecionada');
                   exit;
         End;
         last;
         FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
         FMostraBarraProgresso.Lbmensagem.caption:='Gerando Relat�rio';
         first;

         With FreltxtRDPRINT do
         Begin
              ConfiguraImpressao;
              linhalocal:=3;
              RDprint.FonteTamanhoPadrao:=S20cpp;
              RDprint.TamanhoQteColunas:=160;
              rdprint.Abrir;
              if (RDprint.setup=False)
              Then Begin
                        rdprint.fechar;
                        exit;
              End;

              if Ptipo='D'
              then RDprint.impc(linhalocal,80,Self.NumeroRelatorio+'TITULOS PAGOS POR DIA',[negrito])
              else RDprint.impc(linhalocal,80,Self.NumeroRelatorio+'TITULOS RECEBIDOS POR DIA',[negrito]);

              IncrementaLinha(2);

              RDprint.impf(linhalocal,1,datetostr(datainicial)+' a '+datetostr(datafinal),[negrito]);
              IncrementaLinha(1);

              if (CredorDevedortemp<>'')
              Then Begin
                        if (codigocredordevedortemp<>'')
                        Then RDprint.impf(linhalocal,1,completapalavra(Self.pendencia.titulo.CREDORDEVEDOR.Get_Nome,30,' ')+' '+completapalavra(Self.pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(CredorDevedortemp,codigocredordevedortemp),99,' '),[negrito])
                        else RDprint.impf(linhalocal,1,completapalavra(Self.pendencia.titulo.CREDORDEVEDOR.Get_Nome,30,' '),[negrito]);

                        IncrementaLinha(1);
              End;

              pdata:=fieldbyname('data').asstring;
              pvalordia:=0;
              pvalorperiodo:=0;

              pvalorpendenciadia:=0;
              pdescontodia:=0;
              pjurosdia:=0;

              totalpendencias:=0;
              totaldescontos:=0;
              totaljuros:=0;
              totalpago:=0;

               (*N� Doc  Emiss�o  Vencto  Fornecedor  Hist�rico Valor Desconto Juros Total*)
               VerificaLinha;
               rdprint.impf(linhalocal,1,CompletaPalavra('NUM DCTO/TITULO',20,' ')+' '+
                                        CompletaPalavra('EMISSAO',8,' ')+' '+
                                        CompletaPalavra('VENCTO',8,' ')+' '+
                                        CompletaPalavra('FORNECEDOR/CLIENTE',38,' ')+' '+
                                        CompletaPalavra('HISTORICO',30,' ')+' '+
                                        CompletaPalavra_a_Esquerda('VL PEND',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('DESCONTOS',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('JUROS',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('VALOR PAGO',12,' '),[negrito]);
              IncrementaLinha(1);

              DesenhaLinha;


              VerificaLinha;
              rdprint.impf(linhalocal,1,pdata,[negrito]);
              IncrementaLinha(2);


              While not(Self.ObjQueryLocal.eof)do
              Begin
                   FMostraBarraProgresso.IncrementaBarra1(1);
                   FMostraBarraProgresso.show;
                   Application.ProcessMessages;

                   if (Pdata<>fieldbyname('data').asstring) Then
                   Begin
                      VerificaLinha;

                      rdprint.impf(linhalocal,110,CompletaPalavra_a_Esquerda(formata_valor(pvalorpendenciadia),12,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_Valor(pdescontodia),12,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_Valor(pjurosdia),12,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_Valor(pvalordia),12,' '),[negrito]);
                     IncrementaLinha(1);
                     desenhalinha;
                     IncrementaLinha(1);

                     pdata:=fieldbyname('data').asstring;
                     VerificaLinha;
                     rdprint.impf(linhalocal,1,pdata,[negrito]);
                     IncrementaLinha(2);


                     pvalordia:=0;
                     pvalorpendenciadia:=0;
                     pdescontodia:=0;
                     pjurosdia:=0;

                   End;


                   (*Somando os descontos e juros do dia da parcela atual*)
                   Self.ObjDataset.close;
                   Self.ObjDataset.SelectSQL.clear;
                   Self.ObjDataset.SelectSQL.add('Select sum(tablancamento.valor) as soma');
                   Self.ObjDataset.SelectSQL.add('from tablancamento');
                   Self.ObjDataset.SelectSQL.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
                   Self.ObjDataset.SelectSQL.add('Where tabtipolancto.classificacao=''D'' ');
                   Self.ObjDataset.SelectSQL.add('and tablancamento.pendencia='+fieldbyname('pendencia').asstring);
                   Self.ObjDataset.SelectSQL.add('and tablancamento.data='+#39+formatdatetime('mm/dd/yyyy',fieldbyname('data').asdatetime)+#39);
                   Self.ObjDataset.open;
                   pdescontos:=Self.ObjDataset.fieldbyname('soma').ascurrency;

                   Self.ObjDataset.close;
                   Self.ObjDataset.SelectSQL.clear;
                   Self.ObjDataset.SelectSQL.add('Select sum(tablancamento.valor) as soma');
                   Self.ObjDataset.SelectSQL.add('from tablancamento');
                   Self.ObjDataset.SelectSQL.add('join tabtipolancto on tablancamento.tipolancto=tabtipolancto.codigo');
                   Self.ObjDataset.SelectSQL.add('Where tabtipolancto.classificacao=''J'' ');
                   Self.ObjDataset.SelectSQL.add('and tablancamento.pendencia='+fieldbyname('pendencia').asstring);
                   Self.ObjDataset.SelectSQL.add('and tablancamento.data='+#39+formatdatetime('mm/dd/yyyy',fieldbyname('data').asdatetime)+#39);
                   Self.ObjDataset.open;
                   pjuros:=Self.ObjDataset.fieldbyname('soma').ascurrency;


                   Self.Pendencia.localizacodigo(fieldbyname('pendencia').asstring);
                   Self.Pendencia.TabelaparaObjeto;

                   (*N� Doc  Emiss�o  Vencto  Fornecedor  Hist�rico Valor Desconto Juros Total*)
                   VerificaLinha;
                   rdprint.imp(linhalocal,1,CompletaPalavra(Self.pendencia.titulo.Get_NUMDCTO+'/'+ObjQueryLocal.fieldbyname('TITULO').AsString,20,' ')+' '+
                                        CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(Self.pendencia.titulo.Get_EMISSAO)),8,' ')+' '+
                                        CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(Self.pendencia.Get_VENCIMENTO)),8,' ')+' '+
                                        CompletaPalavra(Self.pendencia.titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.pendencia.titulo.CREDORDEVEDOR.get_codigo,Self.pendencia.titulo.Get_CODIGOCREDORDEVEDOR),38,' ')+' '+
                                        CompletaPalavra(Self.pendencia.titulo.Get_HISTORICO,30,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_valor(Self.pendencia.get_valor),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_Valor(Pdescontos),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_Valor(Pjuros),12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_Valor(fieldbyname('valorpagamento').asstring),12,' '));
                   IncrementaLinha(1);

                   pvalordia:=pvalordia+fieldbyname('valorpagamento').ascurrency;
                   pvalorperiodo:=pvalorperiodo+fieldbyname('valorpagamento').ascurrency;

                   pvalorpendenciadia:=pvalorpendenciadia+StrToCurr(Self.pendencia.get_valor);
                   pdescontodia:=pdescontodia+pdescontos;
                   pjurosdia:=pjurosdia+pjuros;

                   totalpendencias:=totalpendencias+StrToCurr(Self.pendencia.get_valor);
                   totaldescontos:=totaldescontos+pdescontos;
                   totaljuros:=totaljuros+pjuros;
                   totalpago:=totalpago+fieldbyname('valorpagamento').ascurrency;

                   Self.ObjQueryLocal.next;
              End;
              VerificaLinha;
              rdprint.impf(linhalocal,110,CompletaPalavra_a_Esquerda(formata_valor(pvalorpendenciadia),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_Valor(pdescontodia),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_Valor(pjurosdia),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_Valor(pvalordia),12,' '),[negrito]);
              IncrementaLinha(2);
              DesenhaLinha;

              VerificaLinha;
              rdprint.impf(linhalocal,1,CompletaPalavra_a_Esquerda('TOTAL GERAL DO PER�ODO ',108,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(totalpendencias),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_Valor(totaldescontos),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_Valor(totaljuros),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_Valor(totalpago),12,' '),[negrito]);
              IncrementaLinha(2);

              FMostraBarraProgresso.close;
              rdprint.Fechar;
         End;
    End;
end;

function TObjLancamento.ExecutaSQL(Psql: String): boolean;
begin
     result:=False;

     with Self.ObjQueryLocal do
     Begin
          close;
          sql.clear;
          sql.add(psql);
          Try
             execsql;
             result:=true;
          Except
                on e:exception do
                begin
                     mensagemerro(E.message);
                End;
          End;

     End;
end;

procedure TObjLancamento.RetornaLancamentoSemContabilidade(PdataInicial,
  PdataFinal: string; PGrid: TStringGrid);
begin

     Try
        strtodate(pdatainicial);
        strtodate(pdatafinal);
     Except
           MensagemErro('Datas Inv�lidas');
           exit;
     End;

     Pgrid.FixedCols:=0;
     Pgrid.Fixedrows:=0;

     Pgrid.colcount:=4;
     Pgrid.rowcount:=1;


     Pgrid.cells[1,0]:='CODIGO';
     Pgrid.cells[2,0]:='TABELA';
     Pgrid.cells[3,0]:='TIPO';


     With Self.ObjQueryLocal do
     Begin

          close;
          sql.clear;
          sql.add('Select codigo,pendencia from tablancamento');
          sql.add('where tablancamento.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);
          sql.add('and tablancamento.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatafinal))+#39);
          sql.add('and tablancamento.LANCAMENTOPAI IS NULL');//pega o lancamento pai ou os simples
          open;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Buscando lan�amentos';
          FMostraBarraProgresso.btcancelar.Visible:=True;
          first;
          while not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               Application.ProcessMessages;

               if (FMostraBarraProgresso.Cancelar=True)
               then Begin
                         FMostraBarraProgresso.close;
                         close;
                         exit;
               End;

               Self.ObjDataset.close;
               Self.ObjDataset.selectsql.clear;
               Self.ObjDataset.selectsql.add('Select count(codigo) as conta from tabexportacontabilidade where objgerador=''OBJLANCAMENTO'' ');
               Self.ObjDataset.selectsql.add('and codgerador='+fieldbyname('codigo').asstring);
               Self.ObjDataset.open;
               if (Self.ObjDataset.fieldbyname('conta').asinteger=0)
               Then Begin
                         pgrid.RowCount:=pgrid.RowCount+1;
                         pgrid.Cells[1,pgrid.rowcount-1]:=fieldbyname('codigo').asstring;
                         pgrid.Cells[2,pgrid.rowcount-1]:='TABLANCAMENTO';
                         if (fieldbyname('pendencia').asstring='')
                         Then pgrid.Cells[3,pgrid.rowcount-1]:='LOTE'
                         else pgrid.Cells[3,pgrid.rowcount-1]:='SIMPLES';
               End;
               next;
          End;
          FMostraBarraProgresso.btcancelar.Visible:=false;

          if (pgrid.rowcount>1)
          Then Begin
                    pgrid.FixedRows:=1;
                    pgrid.FixedCols:=1;
          End;

          AjustaLArguraColunaGrid(pgrid);
          FMostraBarraProgresso.close;
     End;
     
end;


function TObjLancamento.LancaDesconto_NovoObjeto(PPendencia: string;
  ValorJuroDesconto: Currency; PData: string; Plancamentopai: string;
  out PCodigolancamentoDesconto: string; PExportaContabilidade: Boolean;
  Phistorico: String): boolean;
var
objlancamentotmp:tobjlancamento;
begin
     //esse procedimento cria um novo objeto de lancamento
     //para lancar desconto


     result:=False;
     PCodigolancamentoDesconto:='';

     Try
        objlancamentotmp:=TObjLancamento.Create;
     Except
           Messagedlg('Erro na tentativa de Cria��o do Objeto de Lan�amento!',mterror,[mbok],0);
           exit;
     End;
     Try
          result:=objlancamentotmp.LancaDesconto(PPendencia,ValorJuroDesconto,pdata,Plancamentopai,PExportaContabilidade);
          //guardando o c�digo do lancamento de desconto
          PCodigolancamentoDesconto:=objlancamentotmp.Get_CODIGO;
     Finally
            objlancamentotmp.Free;
     End;

end;

function TObjLancamento.RetornaLancamentoBaixaBoletoemLote(
  Ppendencia: string;var PRegistroRetornoCobranca:string): integer;
begin
      If(Ppendencia='')
      Then result:=0;

      With ObjQueryLocal do
      Begin
            Close;
            sql.Clear;
            sql.add('select trrc.codigo as codigoregistro,tl1.codigo as lancamentopai,tl2.codigo as lanctofilho  from tabregistroretornocobranca trrc');
            sql.add('join tablancamento tl1 on tl1.codigo=trrc.lancamento');
            sql.add('join tablancamento tl2 on tl2.lancamentopai=tl1.codigo');
            sql.add('where tl2.pendencia='+Ppendencia);
            open;
            If(recordcount=0)
            Then Begin
                  result:=0;
            End
            Else Begin
                  PRegistroRetornoCobranca:=fieldbyname('codigoregistro').AsString;
                  result:=fieldbyname('lancamentopai').asinteger;
            End;
      End;
end;

procedure TObjLancamento.AlterarHistoricoLancamento;
var FpesquisaLocal:TFpesquisa;
    PLancamentoLocal:string;
    PNovoHistorico:String;
    SomenteLancamento:boolean;
begin
       SomenteLancamento:=False;
       With FOpcaorel do
       Begin
            RgOpcoes. Items.clear;
            RgOpcoes. Items.add('Alterar Tudo (Lan�amento, Extrato e Contabilidade');
            RgOpcoes. Items.add('Somente o Hist�rico do Lan�amento');
            Showmodal;

            IF (TAG=0)
            THEN EXIT;

            if (RgOpcoes.ItemIndex=1)
            Then SomenteLancamento:=true;
       End;


       Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        if (FpesquisaLocal.showmodal=mrOk)
                        Then Begin
                                PLancamentoLocal:=FpesquisaLocal.querypesq.fieldbyname('Codigo').AsString;
                        End;

                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

       Finally
           FreeandNil(FPesquisaLocal);
       End;


        if (Self.LocalizaCodigo(PLancamentoLocal)=false)
        then Begin
                MensagemErro('Lan�amento n�o encontrado');
                exit;
        end;
        Self.TabelaparaObjeto;

        PNovoHistorico:=Self.Get_Historico;

        if (InputQuery('Hist�rico', 'Novo Hist�rico',pnovohistorico )=False)
        Then Begin
                  MensagemAviso('Cancelado');
                  exit;
        End;

        if (SomenteLancamento=False)
        Then Begin
                if (MessageDlg('O Novo Hist�rico ser� alterado tamb�m no lanc�amento em portador e na contabilidade. Tem Certeza que deseja Alterar?',
                    mtConfirmation, [mbYes, mbNo],0) = mrNo)
                then exit;
        End;
        

        With Self.ObjDataset do
        Begin
            try
                // Primeiro eu altero na TabLancamento
                Close;
                SelectSQL.Clear;
                SelectSQL.Add('Update TabLancamento Set Historico = '+#39+PNovoHistorico+#39);
                SelectSQL.Add('Where Codigo = '+PLancamentoLocal);
                ExecSQL;

                if (SomenteLancamento=False)
                Then BEgin
                        // Segundo  eu altero na TabLanctoPortador
                        Close;
                        SelectSQL.Clear;
                        SelectSQL.Add('Update TabLanctoPortador Set Historico = '+#39+PNovoHistorico+#39);
                        SelectSQL.Add('Where Lancamento = '+PLancamentoLocal);
                        ExecSQL;

                        // Terceiro eu altero na TABEXPORTACONTABILIDADE
                        Close;
                        SelectSQL.Clear;
                        SelectSQL.Add('Update TABEXPORTACONTABILIDADE Set Historico = '+#39+PNovoHistorico+#39);
                        SelectSQL.Add('Where ObjGerador = ''OBJLANCAMENTO''  ');
                        SelectSQL.Add('and CodGerador = '+PLancamentoLocal);
                        ExecSQL;
                End;
            except
                MensagemErro('Erro ao tentar Alterar os Hist�ricos');
                FDataModulo.IBTransaction.RollbackRetaining;
                exit;
            end;
        end;

        FDataModulo.IBTransaction.CommitRetaining;
        MensagemAviso('Hist�ricos Alterados com Sucesso !');
end;

Function TObjLancamento.LancamentoPagamento(PPendencia:string):Boolean;
Begin
     Result:=false;
     Self.Pendencia.LocalizaCodigo(PPendencia);
     Self.Pendencia.TabelaparaObjeto;

     if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo = 'D')
     then Result:=true
     else Result:=false;
end;

procedure TObjLancamento.ImprimeReciboPersonalizadoReportBuilder(pcodigo: String; pTitulo : string = '');
var
  ObjRelPersReportBuilder : TObjRELPERSREPORTBUILDER;
  strSql1, strSql2, strSql3 : string;
begin
  try
    // Feito por F�bio
    // por Enquanto para atender a urgencia do Sindicato Rural
    // vou fazer apenas o recebimento normal,
    // Quando for em Lote precisa descutir com Sadol ou Ronnei uma nova
    // forma de Gerar esse recibo. Portanto s� funciona para recebimento comum

    if (PCodigo = '') then
      exit;

    try
      ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
    except
      MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
      exit;
    end;

    ObjRelPersReportBuilder.ZerarTabela;
    //Self.ObjDataset.Close;
    //Self.ObjDataset.SelectSQL.Clear;
    //Self.ObjDataset.SelectSQL.Add('Select * from PROCTIPORECIBOREPORTBUILDER('+pcodigo+')' );
    //Self.ObjDataset.Open;

    //if ((Self.ObjDataset.fieldbyname('TIPO').AsString = 'COMUM COM CHEQUE') OR
    //  (Self.ObjDataset.fieldbyname('TIPO').AsString = 'COMUM')) then
    //begin
      if (ObjRelPersReportBuilder.LocalizaNOme('RECIBO DE LANCAMENTO A RECEBER REPORTBUILDER') = false) then
      begin
        MensagemErro('O Relatorio de ReporteBuilder "RECIBO DE LANCAMENTO A RECEBER REPORTBUILDER" n�o foi econtrado');
        exit;
      end;

      ObjRelPersReportBuilder.TabelaparaObjeto;
      strSql1 := StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PCODIGO',PCodigo);
      strSql2 := StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PCODIGO',PCodigo);
      strSql3 := StrReplace(ObjRelPersReportBuilder.Get_SQLRepeticao_2,':PCODIGO',PCodigo);

      ObjRelPersReportBuilder.SQLCabecalhoPreenchido:= strSql1;
      ObjRelPersReportBuilder.SQLRepeticaoPreenchido:= strSql2;
      ObjRelPersReportBuilder.SQLRepeticao_2Preenchido:= strSql3;

      //Rodolfo
      //Adicionado ptitulo para casos de lote com o mesmo credor/devedor
      //Com isso sera buscado os dados do credor/devedor
      if (pTitulo <> '') then
      begin
        ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(strSql1,':PTITULO',pTitulo);
        ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(strSql2,':PTITULO',pTitulo);
        ObjRelPersReportBuilder.SQLRepeticao_2Preenchido:=StrReplace(strSql3,':PTITULO',pTitulo);
      end;

      ObjRelPersReportBuilder.ChamaRelatorio(False);
    //end;

  finally
    ObjRelPersreportBuilder.Free;
  end;
end;

procedure TObjLancamento.ImprimeReciboPagamento_ReportBuilder(PcodigoLancamento: String; pTitulo : string = '');
var
  ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
  strSql1, strSql2, strSql3 : string;
begin
  if (PcodigoLancamento = '') then
    exit;

  if (ObjParametroGlobal.ValidaParametro('MODELO DE RECIBO DE LAN�AMENTO')=False) then
  begin
    Messagedlg('O par�metro "MODELO DE RECIBO DE LAN�AMENTO" n�o foi encontrado!',mterror,[mbok],0);
    exit;
  end;

  if (ObjParametroGlobal.Get_Valor<>'PERSONALIZADO REPORTBUILDER') then
    exit;

  try
    try
      ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
    except
      MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
      exit;
    end;

    if (ObjRelPersReportBuilder.LocalizaNOme('RECIBO A PAGAR REPORTBUILDER COMUM') = False) then
    begin
      MensagemErro('O Relatorio de ReporteBuilder "RECIBO A PAGAR REPORTBUILDER COMUM" n�o foi econtrado');
      exit;
    end;

    ObjRelPersReportBuilder.TabelaparaObjeto;

    strSql1 := StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PCODIGO',PcodigoLancamento);
    strSql2 := StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PCODIGO',PcodigoLancamento);
    strSql3 := StrReplace(ObjRelPersReportBuilder.Get_SQLRepeticao_2,':PCODIGO',PcodigoLancamento);

    ObjRelPersReportBuilder.SQLCabecalhoPreenchido:= strSql1;
    ObjRelPersReportBuilder.SQLRepeticaoPreenchido:= strSql2;
    ObjRelPersReportBuilder.SQLRepeticao_2Preenchido:= strSql3;
    
    //Rodolfo
    //Adicionado ptitulo para casos de lote com o mesmo credor/devedor
    //Com isso sera buscado os dados do credor/devedor
    if (pTitulo <> '') then
    begin
      ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(strSql1,':PTITULO',pTitulo);
      ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(strSql2,':PTITULO',pTitulo);
      ObjRelPersReportBuilder.SQLRepeticao_2Preenchido:=StrReplace(strSql3,':PTITULO',pTitulo);
    end;

    ObjRelPersReportBuilder.ChamaRelatorio(False);

  finally
    ObjRelPersreportBuilder.Free;
  end;
end;


procedure TObjLancamento.Imprime_ContasaReceber_Juros;
var datainicial,datafinal,datavencimento:Tdate;
CredorDevedortemp,codigocredordevedortemp:string;
pvalor,pjuros:currency;
pjurosdia,totalpendencias,totaljuros,totalsaldo:currency;
LINHA:Integer;
nomecredordevedor:string;
qtdedias,valorjuros:Currency;

begin

   With FfiltroImp do
    Begin
         DesativaGrupos;
         Grupo01.Enabled:=True;
         Grupo02.Enabled:=True;
         Grupo07.Enabled:=True;

         LbGrupo01.caption:='Data Inicial';
         LbGrupo02.caption:='Data Final';

         edtGrupo01.EditMask:=MascaraData;
         edtGrupo02.EditMask:=MascaraData;


         LbGrupo07.caption:='Cliente';
         ComboGrupo07.Items.clear;
         Combo2Grupo07.Items.clear;
         Self.pendencia.titulo.Get_ListacredorDevedor(ComboGrupo07.Items);
         Self.pendencia.titulo.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
         edtgrupo07.onkeydown:=Self.pendencia.titulo.edtcodigocredordevedor_SPV_KeyDown;
         edtgrupo07.Color:=$005CADFE;
         showmodal;

         If tag=0
         Then exit;

         //Conferindo o que foi lancado
         Try
               datainicial:=Strtodate(edtgrupo01.text);
               DataFinal:=Strtodate(edtgrupo02.text);
         Except
               Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
               exit;
         End;


         Try
               CredorDevedortemp:='';
               strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
               CredorDevedorTemp:=Combo2Grupo07.Items[ComboGrupo07.itemindex];

               if (Self.pendencia.titulo.CREDORDEVEDOR.LocalizaCodigo(CredorDevedortemp)=False)
               Then Begin
                         MensagemErro('Credor Devedor n�o encontrado');
                         exit;
               End;
               Self.pendencia.titulo.CREDORDEVEDOR.TabelaparaObjeto;
         Except
               CredorDevedorTemp:='';
         End;

         Try
            CodigoCredorDevedorTemp:='';
            if (EdtGrupo07.text<>'')
            Then begin
                    Strtoint(EdtGrupo07.text);
                    CodigoCredorDevedorTemp:=EdtGrupo07.text;
            End;
         Except
             CodigoCredorDevedorTemp:='';
         End;
    End;

    if (ObjParametroGlobal.ValidaParametro('TAXA DE JUROS AO M�S NO LAN�AMENTO (%)')=False)
    then Exit;

    pjuros:=StrToCurr(ObjParametroGlobal.get_valor);
    pjurosdia:=(pjuros/30);
    pjurosdia:=strtofloat(tira_ponto(formata_valor(pjurosdia)));

    With Self.ObjQueryLocal do
    Begin


         close;
         sql.clear;
         sql.add('SELECT TP.TITULO, TP.CODIGO AS PENDENCIA, TP.CODIGO, TABTITULO.CREDORDEVEDOR,TABTITULO.HISTORICO,');
         SQL.Add('TABTITULO.EMISSAO,TP.VENCIMENTO,TP.VALOR,TP.SALDO, tabtitulo.codigocredordevedor');
         SQL.Add('FROM TABPENDENCIA TP');
         Sql.Add('JOIN TABTITULO ON TP.TITULO=TABTITULO.CODIGO');
         SQL.Add('JOIN TABCONTAGER ON TABTITULO.CONTAGERENCIAL=TABCONTAGER.CODIGO');
         SQL.Add('WHERE TABCONTAGER.TIPO = '#39+'C'+#39' AND TP.SALDO>0');
         sql.add('and tabtitulo.emissao>='+#39+formatdatetime('mm/dd/yyyy',datainicial)+#39);
         sql.add('and tabtitulo.emissao<='+#39+formatdatetime('mm/dd/yyyy',datafinal)+#39);

         if (CredorDevedortemp<>'')
         then sql.add('and tabtitulo.credordevedor='+CredorDevedortemp);

         if (codigocredordevedortemp<>'')
         then sql.add('and tabtitulo.codigocredordevedor='+codigocredordevedortemp);

           SQL.Add('ORDER BY TP.VENCIMENTO');
         Open;

         if (recordcount=0)
         Then Begin
                   MEnsagemAviso('Nenhuma Informa��o foi selecionada');
                   exit;
         End;
         last;
         InicializaBarradeProgressoRelatorio(RecordCount, 'Carregando Relat�rio');
         first;

        if AbrirImpressaoPaisagem = False
        then Exit;
        linha:=3;

        ImprimirNegrito(LINHA,50,'59 - RELAT�RIO DE CONTAS A RECEBER COM JUROS');
        Inc(LINHA,1);
        ImprimirNegrito(LINHA,1,'__________________________________________________________________________________________________________________________________');
        Inc(LINHA,1);
        ImprimirNegrito(LINHA,1,CompletaPalavra('T�TULO',6,' ')+' '+
                                   CompletaPalavra('PENDENCIA',6,' ')+' '+
                                 CompletaPalavra('CREDOR/DEVEDOR',20,' ')+' '+
                                 CompletaPalavra('HIST�RICO',25,' ')+' '+
                                 CompletaPalavra_a_Esquerda('EMISS�O',10,' ')+' '+
                                 CompletaPalavra_a_Esquerda('VENCIMENTO',10,' ')+' '+
                                 CompletaPalavra_a_Esquerda('VLR PENDENCIA',13,' ')+' '+
                                 CompletaPalavra_a_Esquerda('JUROS',10,' ')+' '+
                                 CompletaPalavra_a_Esquerda('DIAS ATRAZO',11,' ')+' '+
                                 CompletaPalavra_a_Esquerda('SALDO',5,' '));
        Inc(LINHA,1);

        WHILE not (eof)  do
        Begin
              IncrementaBarraProgressoRelatorio;
              nomecredordevedor:= Self.Pendencia.Titulo.RetornaNomesCredoresDevedores(fieldbyname('credordevedor').AsString,fieldbyname('codigocredordevedor').asstring);
              pvalor:=fieldbyname('saldo').ascurrency;
              datavencimento:=fieldbyname('vencimento').AsDateTime;

              qtdedias:=Int(now-datavencimento);
              if qtdedias<=0 then
              qtdedias:=0;

              valorjuros:=((PValor*pjurosdia)*(qtdedias))/100;

              ImprimirNegrito(LINHA,1,CompletaPalavra(fieldbyname('TITULO').asstring,6,' ')+' '+
                             CompletaPalavra(FieldByName('PENDENCIA').asstring,6,' ')+' '+
                             CompletaPalavra(nomecredordevedor,20,' ')+' '+
                             CompletaPalavra(FieldByName('historico').asstring,25,' ')+' '+
                             CompletaPalavra_a_Esquerda(FieldByName('emissao').asstring ,10,' ')+' '+
                             CompletaPalavra_a_Esquerda(fieldbyname('vencimento').asstring,10,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(FieldByName('valor').asstring),13,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(valorjuros),10,' ')+' '+
                             CompletaPalavra_a_Esquerda(CurrToStr(qtdedias),10,' ')+' '+
                             CompletaPalavra_a_Esquerda(formata_valor(pvalor+valorjuros),10,' '));
             Inc(LINHA,1);
               
             totalpendencias:=totalpendencias+fieldbyname('valor').AsCurrency;
             totaljuros:=totaljuros+valorjuros;
             totalsaldo:=totalsaldo+(pvalor+valorjuros);

             Next;
        end;

     Inc(LINHA,1);
     ImprimirNegrito(LINHA,1,'__________________________________________________________________________________________________________________________________');
     Inc(LINHA,1);
       ImprimirNegrito(LINHA,1,CompletaPalavra('TOTAL GERAL DO PER�ODO',86,' ')+' '+
                             CompletaPalavra(formata_valor(totalpendencias),10,' ')+' '+
                             CompletaPalavra(formata_valor(totaljuros),10,' ')+' '+
                             CompletaPalavra(' ',10,' ')+' '+
                             CompletaPalavra(formata_valor(totalsaldo),10,' '));
     Inc(LINHA,2);

    FechaBarraProgressoRelatorio;
    FecharImpressao;
   end;
end;

function TObjLancamento.VerificaPermissaoDescontoAcrescimo(
  pLancamento: tobjlancamento; pTipo: string): boolean;
var
      ValorMaximo,ValorPendencia,ValorLancamento,PercentualLancamento:Currency;
begin
      result := false;
      ValorMaximo := 0;
      ValorPendencia := 0;
      ValorLancamento := 0;
      PercentualLancamento := 0;

      if not(ObjParametroGlobal.ValidaParametro('VALIDA DESCONTO E ACR�SCIMO NO RECEBIMENTO'))
      then begin
            result := true;
            exit;
      end
      else if(UpperCase(ObjParametroGlobal.Get_Valor)='N�O')
            then begin
                  Result := true;
                  exit;
            end;

      pLancamento.Pendencia.LocalizaCodigo(pLancamento.Pendencia.Get_CODIGO);
      pLancamento.Pendencia.TabelaparaObjeto;
      try
            ValorPendencia := strtocurr(pLancamento.Pendencia.Get_VALOR);
      except
            MensagemErro('Valor inv�lido para a pend�ncia '+pLancamento.Pendencia.Get_CODIGO);
            Exit;
      end;

      try
            ValorLancamento := strtocurr(pLancamento.Get_Valor);
      except
            MensagemErro('Valor inv�lido para o lancamento '+pLancamento.Get_CODIGO);
            Exit;
      end;

      //calculando o percentual
      PercentualLancamento := (ValorLancamento * 100) / ValorPendencia;

      if(pTipo='D')
      then begin  //desconto
            if (ObjParametroGlobal.ValidaParametro('DESCONTO M�XIMO NO RECEBIMENTO EM %'))
            then ValorMaximo := strtocurr(ObjParametroGlobal.Get_Valor);

            if not(ObjPermissoesUsoGlobal.ValidaPermissao('PERMISS�O PARA DESCONTO OU ACRESCIMO NO RECEBIMENTO'))
            then exit;

            //tem pemissao, agora precisa verificar se o valor nao ultrapassa o estipulado no parametro
            if(ValorMaximo=0)
            then begin
                  MensagemErro('Desconto n�o permitido .');
                  exit;
            end
            else if(PercentualLancamento > ValorMaximo)
                  then begin
                        MensagemErro('Valor de desconto ultrapassa o valor m�ximo estipulado para desconto');
                        exit;
                  end;

            result := true;
      end
      else if(pTipo='A')
            then begin  //acr�scimo
                  if (ObjParametroGlobal.ValidaParametro('ACR�SCIMO M�XIMO NO RECEBIMENTO EM %'))
                  then ValorMaximo := strtocurr(ObjParametroGlobal.Get_Valor);

                  if not(ObjPermissoesUsoGlobal.ValidaPermissao('PERMISS�O PARA DESCONTO OU ACRESCIMO NO RECEBIMENTO'))
                  then exit;

                  //tem pemissao, agora precisa verificar se o valor nao ultrapassa o estipulado no parametro
                  if(ValorMaximo=0)
                  then begin
                        MensagemErro('Acr�scimo n�o permitido.');
                        exit;
                  end
                  else if(PercentualLancamento > ValorMaximo)
                        then begin
                              MensagemErro('Valor de acr�xcimo ultrapassa o valor m�ximo estipulado para acr�scimo');
                              exit;
                        end;

                  Result := true;

            end
            else MensagemErro('Verifique par�metro de Desconto ou Acr�scimo');
end;

{Rodolfo}
Procedure TObjLancamento.edtPortadorComChequeExit(Sender: TObject);
begin
  with FfiltroImp do
  begin
    Try
      edtgrupo02.Tag:=0;
      if (edtgrupo01.Text <> '')
      then strtoint(edtgrupo01.Text);
      edtgrupo02.Tag:=strtoint(edtgrupo01.Text);
    Except
      edtgrupo02.Tag:=0;
    End;
  end;
end;

{Rodolfo}
Procedure TObjLancamento.edtPortadorComChequeKeyPress(Sender: TObject; var Key: Char);
begin
  if not (key in['0'..'9',Chr(8)])Then
    key:=#0;
end;

{Rodolfo}
Procedure TObjLancamento.edtTalaodechequesKeyPress(Sender: TObject; var Key: Char);
begin
  edtPortadorComChequeKeyPress(Sender, Key);
end;

{Rodolfo}
Procedure TObjLancamento.edtValorChange(Sender: TObject);
begin
  with FfiltroImp do
  begin
    TEdit(sender).Text := formata_valor_tempo_real_N_casas(TEdit(sender).text , Sender , 2);
  end;
end;

//Rodolfo
procedure TObjLancamento.ImprimeContaGerencial();
var
  pData1 : string;
  pData2 : string;
  portAnterior : string; //Portador Anterior

  SomaCreditos : Currency;
  SomaDebitos : Currency;
  SomaPortador : Currency;
  SomaTotalPortador : Currency;
  SAnterior : Currency;    //Saldo Anterior
  SAtual : Currency;       //Saldo Atual
  Movimento : Currency;

  i : Integer;
  FlagEntrada1 : Boolean;
  FlagEntrada2 : Boolean;

  ObjQueryLocal : Tibquery;
begin

  try
    ObjQueryLocal:=TIBQuery.create(nil);
    ObjQueryLocal.Database:=FDataModulo.IBDatabase;
  except
    Messagedlg('Erro na cria��o da query de pesquisa.',mterror,[mbok],0);
    exit;
  end;

  try

    with FfiltroImp do
    begin
      DesativaGrupos;

      grupo01.enabled := True;
      edtgrupo01.EditMask := '!99/99/9999;1;_';
      LbGrupo01.Caption := 'Lan�amento Inicial';

      grupo02.enabled := True;
      edtgrupo02.EditMask := '!99/99/9999;1;_';
      LbGrupo02.Caption := 'Lan�amento Final';

      showmodal;

      If tag=0     //clique no x de sair do form
      Then exit;

      try
        StrToDate(edtgrupo01.text);
        StrToDate(edtgrupo02.text);
        Pdata1:=edtgrupo01.text;
        Pdata2:=edtgrupo02.text;
      except
        Pdata1:='';
        Pdata2:='';
      end;
    end;

    SAnterior := Self.SaldoAnterior(pData1);

    with objquerylocal do
    begin
      close;
      Sql.clear;
      Sql.add('Select');
      Sql.add('t2.nome         as NomeContaGerencial,');
      Sql.add('t2.tipo         as tipo,   ');
      Sql.add('sum(t4.valor)   as SomaQuitacao');
      Sql.add('from tabtitulo t1');
      Sql.add('join tabcontager    t2       on t1.contagerencial = t2.codigo');
      Sql.add('join tabpendencia   t3       on t3.titulo = t1.codigo');
      Sql.add('join tablancamento  t4       on t3.codigo = t4.pendencia');
      Sql.add('join tabtipolancto  t5       on t4.tipolancto = t5.codigo');
      Sql.add('where');
      Sql.add('t5.classificacao = ''Q'' ');

      if (pData1 <> '')then
        SQL.add('and t4.data >= ' + #39 + formatdatetime('mm/dd/yyyy',strtodate(pData1)) + #39);

      if (pData2 <> '')then
        SQL.add('and t4.data <= ' + #39 + formatdatetime('mm/dd/yyyy',strtodate(pData2)) + #39);

      Sql.add('group by 1, 2');
      Sql.add('order by 2, 1');

      Open;

      last;
      FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
      FMostraBarraProgresso.Lbmensagem.caption := 'Gerando Relat�rio';
      first;

      With FreltxtRDPRINT do
      Begin
        ConfiguraImpressao;
        RDprint.FonteTamanhoPadrao:=S20cpp;
        RDprint.TamanhoQteColunas:=130;
        RDprint.Abrir;

        if (RDprint.Setup = False)then
        begin
          RDprint.Fechar;
          exit;
        end;

        linha:=3;
        RDprint.ImpC(linha,55,Self.NumeroRelatorio + ' CONTA GERENCIAL',[negrito]);
        inc(linha,2);

        RDprint.ImpF(linha,1,'Lan�amentos de ' + Pdata1 + ' a ' + Pdata2,[negrito]);
        inc(linha,2);

        RDprint.ImpF(linha,1,
          CompletaPalavra('',1,' ')+' '+
          CompletaPalavra('CONTAS',40,' ')+' '+
          CompletaPalavra('',2,' ')+' '+
          CompletaPalavra('TIPO',8,' ')+' '+
          CompletaPalavra('VALOR',9,' ')+' '+
          CompletaPalavra('',55,' '),[negrito, sublinhado]
        );

        inc(linha,2);

        SomaCreditos := 0;
        SomaDebitos := 0;

        FlagEntrada1 := True;
        FlagEntrada2 := True;
        while not(Eof) do
        begin

          FMostraBarraProgresso.IncrementaBarra1(1);
          FMostraBarraProgresso.show;
          Application.ProcessMessages;

          if(objquerylocal.Fields[1].AsString = 'C')then
          begin
            SomaCreditos := SomaCreditos + objquerylocal.Fields[2].AsCurrency;

            if(FlagEntrada1 = True)then
            begin
              RDprint.ImpF(linha,1, CompletaPalavra('CREDITOS',9,' '),[negrito]);
              inc(linha,1);
              FlagEntrada1 := False;
            end;
          end
          else
          begin
            SomaDebitos := SomaDebitos + objquerylocal.Fields[2].AsCurrency;

            if(FlagEntrada2 = True)then
            begin
              RDprint.ImpF(linha,38,
                CompletaPalavra('Total Creditos: ',17,' ')+' '+
                CompletaPalavra(formata_valor(CurrToStr(SomaCreditos)),15,' '),[negrito]
              );

              inc(linha,2);
              RDprint.ImpF(linha,1, CompletaPalavra('DEBITOS',9,' '),[negrito]);
              inc(linha,1);
              FlagEntrada2 := False;
            end;
          end;

          VerificaLinha(RDprint,linha);
          RDprint.Imp(linha,3,completapalavra(objquerylocal.fields[0].asstring,40,' '));
          RDprint.Imp(linha,48,completapalavra(objquerylocal.fields[1].asstring,7,' '));
          RDprint.Imp(linha,56,completapalavra(formata_valor(objquerylocal.fields[2].asstring) ,15,' '));
          inc(linha,1);

          objquerylocal.Next;
        end;

        RDprint.ImpF(linha,39, CompletaPalavra('Total Debitos: ',16,' ')+' '+
                                     CompletaPalavra(formata_valor(CurrToStr(SomaDebitos)),15,' '),[negrito]);

        Movimento := SomaCreditos - SomaDebitos; //Movimento do Periodo
        SAtual := SAnterior + Movimento;  //Saldo Atual eh o saldo anterior mais a movimentacao do periodo

        inc(linha,2);

        VerificaLinha(rdprint,linha);
        RDprint.imp(linha,1,CompletaPalavra('_',120,'_'));
        inc(linha,1);

        VerificaLinha(RDprint,linha);
        RDprint.ImpF(linha,1,
          CompletaPalavra('Saldo Anterior: ', 16, ' ')+' '+
          CompletaPalavra(formata_valor(CurrToStr(SAnterior)), 15, ' ')+' '+
          CompletaPalavra('', 3, ' ')+' '+
          CompletaPalavra('Movimento do Periodo: ', 23, ' ')+' '+
          CompletaPalavra(formata_valor(CurrToStr(Movimento)), 15, ' ')+' '+
          CompletaPalavra('', 3, ' ')+' '+
          CompletaPalavra('Saldo Atual: ', 14, ' ')+' '+
          CompletaPalavra(formata_valor(CurrToStr(SAtual)), 15, ' '),[negrito]
        );

        {RDprint.ImpF(linha,1, CompletaPalavra('', 41, ' ')+' '+
                              CompletaPalavra('Resultado: ', 12, ' ')+' '+
                              CompletaPalavra(formata_valor(CurrToStr(Movimento)), 15, ' '),[negrito]); }


        //Pesquisa de valores por portadores
        close;
        Sql.clear;
        Sql.add('Select');
        Sql.add('t3.debito_credito   as tipo,');
        Sql.add('t2.nome         as nome,');
        Sql.add('sum(t1.valor)   as SomaQuitacao');
        Sql.add('from tablanctoportador  t1');
        Sql.add('join tabportador  t2  on t2.codigo = t1.portador');
        Sql.add('join tabtipolanctoportador t3 on t3.codigo = t1.tipolancto');
        Sql.add('where 1=1');
        Sql.add('and (t2.apenas_uso_sistema = ''N'' or t2.nome = ''CHEQUES DEVOLVIDOS'') ');
        //Sql.add('and t2.apenas_uso_sistema = ''N'' ');

        if (pData1 <> '')then
        begin
          SQL.add('and t1.data >= ' + #39 + formatdatetime('mm/dd/yyyy',strtodate(pData1)) + #39);
          SQL.add('and t1.data <= ' + #39 + formatdatetime('mm/dd/yyyy',strtodate(pData2)) + #39);
        end;

        Sql.add('group by 2,1');
        Sql.add('order by 2,1');

        //InputBox('','',sql.text);

        Open;

        inc(linha,3);
        portAnterior := objquerylocal.fields[1].asstring;  //guarda o portador
        SomaPortador := 0;
        SomaTotalPortador := 0;
        while not(Eof) do
        begin
          //verifica se o portador que esta sendo visto eh igual ao anterior, a primeira vez sempre eh igual ao anterior
          if(objquerylocal.fields[1].asstring = portAnterior)then
          begin
            if(objquerylocal.fields[0].asString = 'C')then  //Eh de credito
            begin
              SomaPortador := SomaPortador + ObjQueryLocal.Fields[2].AsCurrency;
            end
            else // = 'D'  - Nao eh credito, eh debito
            begin
              SomaPortador := SomaPortador - ObjQueryLocal.Fields[2].AsCurrency;
            end;

            objquerylocal.Next; //incrementa pra ver o proximo portador
          end
          else
          begin
            VerificaLinha(RDprint,linha);
            RDprint.ImpF(linha,1, CompletaPalavra(portAnterior, 40, ' ')+ ' '+
                                  CompletaPalavra(formata_valor(CurrToStr(SomaPortador)), 15, ' '),[negrito]);
            inc(linha,1);

            SomaTotalPortador := SomaTotalPortador + SomaPortador;
            SomaPortador := 0;
            portAnterior := objquerylocal.fields[1].asstring;
          end;
        end;

        SomaTotalPortador := SomaTotalPortador + SomaPortador;

        VerificaLinha(RDprint,linha);
        RDprint.ImpF(linha,1, CompletaPalavra(portAnterior, 40, ' ')+ ' '+
                              CompletaPalavra(formata_valor(CurrToStr(SomaPortador)), 15, ' '),[negrito]);

        inc(linha,2);
        VerificaLinha(RDprint,linha);
        RDprint.ImpF(linha,1, CompletaPalavra('Total dos Portadores', 40, ' ')+ ' '+
                              CompletaPalavra(formata_valor(CurrToStr(SomaTotalPortador)), 15, ' '),[negrito]);

        RDprint.Fechar;
      end;
    end;

  finally
    FreeAndNil(ObjqueryLocal);
    FMostraBarraProgresso.close;
  end;
end;

//Rodolfo
function TObjLancamento.SaldoAnterior (pdata : string) : Currency;
var
  ObjQueryLocal : Tibquery;
  SomaCreditos : Currency;
  SomaDebitos : Currency;
begin
  try
    ObjQueryLocal:=TIBQuery.create(nil);
    ObjQueryLocal.Database:=FDataModulo.IBDatabase;
  except
    Messagedlg('Erro na cria��o da query de pesquisa.',mterror,[mbok],0);
    exit;
  end;

  try
    with objquerylocal do
    begin
      close;
      Sql.clear;
      Sql.add('Select');
      Sql.add('t2.nome         as NomeContaGerencial,');
      Sql.add('t2.tipo         as tipo,   ');
      Sql.add('sum(t4.valor)   as SomaQuitacao');
      Sql.add('from tabtitulo t1');
      Sql.add('join tabcontager    t2       on t1.contagerencial = t2.codigo');
      Sql.add('join tabpendencia   t3       on t3.titulo = t1.codigo');
      Sql.add('join tablancamento  t4       on t3.codigo = t4.pendencia');
      Sql.add('join tabtipolancto  t5       on t4.tipolancto = t5.codigo');
      Sql.add('where');
      Sql.add('t5.classificacao = ''Q'' ');

      SQL.add('and t4.data < ' + #39 + formatdatetime('mm/dd/yyyy',strtodate(pData)) + #39);

      Sql.add('group by 1, 2');
      Sql.add('order by 2, 1');

      Open;

      SomaCreditos := 0;
      SomaDebitos := 0;
      while not(Eof) do
      begin
        if(Fields[1].AsString = 'C')then
        begin
          SomaCreditos := SomaCreditos + Fields[2].AsCurrency;
        end
        else
        begin
          SomaDebitos := SomaDebitos + Fields[2].AsCurrency;
        end;
        Next;
      end;
      
      result := SomaCreditos - SomaDebitos;
    end;
  finally
    FreeAndNil(ObjqueryLocal);
  end;
end;

{Rodolfo}
Procedure TObjLancamento.buscaNumChequeDblClick(Sender: TObject);
var
  Shift : TShiftState;
  key : Word;
begin
  key := VK_F9;
  buscaNumChequeKeyDown(Sender, Key, Shift);
end;

//Rodolfo
procedure TObjLancamento.buscaNumChequeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
  Ftalao:TFtalaodeCheques;
begin

  if (key <> vk_f9)
  then exit;

  try
    Fpesquisalocal:=Tfpesquisa.create(nil);
    Ftalao:=TFtalaodeCheques.create(nil);
    FpesquisaLocal.NomeCadastroPersonalizacao := 'UOBJLANCAMENTO.EDTTALAODEQUEQUES';

    if (Tedit(Sender).tag <> 0) then
    begin

      if (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaTalaodeChequesAbertos(inttostr(Tedit(Sender).tag)),Self.Get_titulopesquisatalaodecheques,FTalao)=True) then
      begin
        if (Fpesquisalocal.showmodal = mrok) then
        begin
          Tedit(Sender).text := Fpesquisalocal.querypesq.fieldbyname('numero').asstring;  //pega o numero do cheque
        end;
      end;
    end
    else
    begin
      if (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaTalaodeChequesAbertos,Self.Get_titulopesquisatalaodecheques,Ftalao) = True) then
      begin
        if (Fpesquisalocal.showmodal = mrok) then
        begin
          Tedit(Sender).text := Fpesquisalocal.querypesq.fieldbyname('numero').asstring; //pega o numero do cheque
        end;
      end;
    end;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FTalao);
  end;
end;

//Rodolfo
procedure TObjLancamento.ImprimeTitulosPagosEChequesACompensar;
var
  DataLimite,DataLimite2 :Tdate;
  CredorDevedorTemp, CodigoCredorDevedorTemp :string;
  ptotalpago :Currency;
  TipoDataPesquisa :String;
  ImprimeDescricao :String;
  Flag : Boolean;
begin

  with FfiltroImp do
  begin
    DesativaGrupos;
    Grupo01.enabled:=true;
    Grupo02.enabled:=true;
    Grupo07.Enabled:=True;

    lbGrupo01.caption:='Data Inicial';
    lbGrupo02.caption:='Data Final';
    edtGrupo01.EditMask:='!99/99/9999;1;_';
    edtGrupo02.EditMask:='!99/99/9999;1;_';

    LbGrupo07.caption:='Credor/Devedor';
    ComboGrupo07.Items.clear;
    Combo2Grupo07.Items.clear;
    Self.pendencia.titulo.Get_ListacredorDevedor(ComboGrupo07.items);
    Self.pendencia.titulo.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
    edtgrupo07.onkeydown:=Self.Pendencia.Titulo.EdtCodigoCredorDevedor_SPV_KeyDown;
    edtgrupo07.color:=$005CADFE;

    showmodal;

    if tag=0
    then exit;

    try
      DataLimite := Strtodate(edtgrupo01.text);
      DataLimite2 := Strtodate(edtgrupo02.text);
    except
      Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
      exit;
    end;

    try
      CredorDevedortemp := '';
      strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
      CredorDevedorTemp := Combo2Grupo07.Items[ComboGrupo07.itemindex];
    except
      CredorDevedorTemp:='';
    end;

    try
      CodigoCredorDevedorTemp := '';
      if (EdtGrupo07.text <> '')then
      begin
        Strtoint(EdtGrupo07.text);
        CodigoCredorDevedorTemp := EdtGrupo07.text
      end;
    except
      CodigoCredorDevedorTemp := '';
    end;
         
  end;//with das opcoes

  //TipoDataPesquisa := 'P'; //Por data do Lancamento (pagamento)
  //ImprimeDescricao := 'S'; //Imprime a Descricao do Titulo
  Flag := True; //Indica se algum registro foi encontrado

  with FreltxtRDPRINT do
  begin

    ConfiguraImpressao;
    LinhaLocal:=3;
    RDprint.Abrir;
    if (RDprint.Setup=False) then
    begin
      RDprint.Fechar;
      exit;
    end;

    rdprint.impc(LinhaLocal,45,Self.NumeroRelatorio+'PEND�NCIAS PAGAS E CHEQUES N�O DEBITADOS',[negrito]);
    IncrementaLinha(2);

    rdprint.impf(linhalocal,1,'DATA    : ' + datetostr(DataLimite)+' a ' + datetostr(DataLimite2),[negrito]);
    IncrementaLinha(1);

    if (CodigoCredorDevedorTemp <> '') and (CredorDevedorTemp <> '') then
    begin
      rdprint.impf(linhalocal,1,CompletaPalavra('CREDOR/DEVEDOR: '+ CodigoCredorDevedorTemp+'-'+Self.pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(CredorDevedorTemp,CodigoCredorDevedorTemp),90,' '),[negrito]);
      IncrementaLinha(1);
    end;
    IncrementaLinha(1);

    //Puxa os titulos
    with Self.ObjQueryLocal do
    begin
      close;
      sql.clear;
      sql.add('Select tabLancamento.pendencia,');
      sql.add('tabPendencia.vencimento, tabPendencia.valor as VALORPENDENCIA,');
      sql.add('tabPendencia.boletoapagar,');
      sql.add('tabTitulo.notafiscal, tabLancamento.data as DATAPAGAMENTO, tabLancamento.valor as VALORPAGAMENTO,');
      sql.add('tabTitulo.credordevedor, tabTitulo.codigocredordevedor, tabTitulo.historico');
      sql.add('from tabLancamento');
      sql.add('join tabTipoLancto on tabLancamento.tipolancto=tabtipolancto.codigo');
      sql.add('join tabPendencia on tabLancamento.pendencia=tabPendencia.codigo');
      sql.add('join tabTitulo on tabPendencia.titulo=tabTitulo.codigo');
      sql.add('join tabContager on tabTitulo.contagerencial=tabContager.codigo');
      sql.add('where tabContager.tipo=''D'' and tabTipoLancto.classificacao=''Q'' ');

      //TipoDataPesquisa='P'
      sql.add('and tablancamento.data>=' + #39 + Formatdatetime('mm/dd/yyyy',DataLimite) + #39);
      sql.add('and tablancamento.data<=' + #39 + Formatdatetime('mm/dd/yyyy',DataLimite2) + #39);
      //--

      if (CredorDevedorTemp <> '') then
        sql.add('and tabTitulo.CredorDevedor=' + CredorDevedorTemp);

      if (CodigoCredorDevedorTemp <> '') then
        sql.add('and tabTitulo.CodigoCredorDevedor=' + CodigoCredorDevedorTemp);

      sql.add('order by tabLancamento.data, tabTitulo.credordevedor, tabTitulo.codigocredordevedor');

      //InputBox('','',SQL.Text);
      open;

      if (Recordcount <> 0) then
      begin
        VerificaLinha;
        rdprint.impf(linhalocal,1,CompletaPalavra('>> PEND�NCIAS PAGAS',30,' '),[negrito]);
        IncrementaLinha(1);

        VerificaLinha;
        rdprint.impf(linhalocal,1,CompletaPalavra('PEND.',6,' ')+' '+
          CompletaPalavra('VENCTO',8,' ')+' '+
          CompletaPalavra_a_Esquerda('VALOR PEND',10,' ')+' '+
          CompletaPalavra('BOLETO',6,' ')+' '+
          CompletaPalavra('NF',6,' ')+' '+
          CompletaPalavra('DATAPAGO',8,' ')+' '+
          CompletaPalavra_a_Esquerda('VALOR PAGTO',11,' ')+' '+
          CompletaPalavra('CREDOR/DEVEDOR',28,' '),[negrito]);

        IncrementaLinha(1);
        DesenhaLinha;

        ptotalpago := 0;
        while not(Self.ObjQueryLocal.eof) do
        begin
          VerificaLinha;
          rdprint.imp(linhalocal,1,CompletaPalavra(fieldbyname('pendencia').asstring,6,' ')+' '+
            CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('vencimento').asdatetime),8,' ')+' '+
            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorpendencia').asstring),10,' ')+' '+
            CompletaPalavra(fieldbyname('boletoapagar').asstring,6,' ')+' '+
            CompletaPalavra(fieldbyname('notafiscal').asstring,6,' ')+' '+
            CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('datapagamento').asdatetime),8,' ')+' '+
            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorpagamento').asstring),11,' ')+' '+
            CompletaPalavra(fieldbyname('codigocredordevedor').asstring+'-'+Self.pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),28,' '));

          IncrementaLinha(1);
          pTotalPago:= pTotalPago + fieldbyname('valorpagamento').ascurrency;

          //ImprimeDescricao='S'
          //rdprint.imp(linhalocal,1,'Descri��o '+fieldbyname('historico').asstring);
          //IncrementaLinha(1);
          //--

          Self.ObjQueryLocal.next;
        end;

        DesenhaLinha;

        verificalinha;
        rdprint.impf(linhalocal,1,'TOTAL: R$ '+formata_valor(ptotalpago),[negrito]);
      end
      else Flag := False;

    end;

    pTotalPago := 0;
    //puxa os cheques
    with Self.ObjQueryLocal do
    begin
      close;
      sql.clear;

      sql.Text :=

      'select   ' +
      'tablancamento.pendencia, ' +
      'tabpendencia.valor as VALORPENDENCIA, ' +
      'ttc.numero as NUMEROCHEQUE, ' +
      'ttc.valor as VALORPAGAMENTO, ' +
      'tabportador.nome as NOMEPORTADOR, ' +
      'tabLancamento.data as DATAPAGAMENTO, ' +
      'ttc.vencimento,  ' +
      'tabtitulo.credordevedor, '+
      'tabtitulo.codigocredordevedor, '+
      'tabtitulo.historico  ' +

      'from tabTalaoDeCheques ttc '  +

      'join tabportador        on ttc.portador             = tabportador.codigo  ' +
      'join tablancamento      on ttc.lancamento           = tablancamento.codigo '  +
      'left join tabpendencia  on tablancamento.pendencia  = tabpendencia.codigo  ' +
      'left join tabtitulo     on tabpendencia.titulo      = tabtitulo.codigo  ' +

      'where ttc.descontado = ' + QuotedStr('N')  +
      'and tablancamento.data >= ' + QuotedStr( FormatDateTime('mm/dd/yyyy',DataLimite) )   +
      'and tablancamento.data <= ' + QuotedStr( FormatDateTime('mm/dd/yyyy',DataLimite2) )  +

      'order by ttc.vencimento ';
      //InputBox('cheks','',SQL.Text);

      Open;

      if (RecordCount <> 0) then
      begin
        IncrementaLinha(2);

        VerificaLinha;
        rdprint.impf(linhalocal,1,CompletaPalavra('>> CHEQUES A COMPENSAR',30,' '),[negrito]);
        IncrementaLinha(1);

        VerificaLinha;
        rdprint.impf(linhalocal,1,
          CompletaPalavra('PEND.',6,' ')+' '+
          CompletaPalavra_a_Esquerda('VALOR PEND',10,' ')+' '+
          CompletaPalavra('',1,' ')+' '+
          CompletaPalavra('NOME PORTADOR',16,' ')+' '+
          CompletaPalavra_a_Esquerda('N�CHEQUE',8,' ')+' '+
          CompletaPalavra('DATAPGTO',8,' ')+' '+
          CompletaPalavra_a_Esquerda('VALOR PAGO',10,' ')+' '+
          CompletaPalavra_a_Esquerda('VENCIMENTO',10,' ')+' '+
          CompletaPalavra_a_Esquerda('CREDOR/DEVEDOR',18,' '),[negrito]);

        IncrementaLinha(1);
        DesenhaLinha;

        ptotalpago := 0;
        while not(Self.ObjQueryLocal.eof) do
        begin
          VerificaLinha;
          rdprint.imp(linhalocal,1,
            CompletaPalavra(fieldbyname('pendencia').asstring,6,' ')+' '+
            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorpendencia').asstring),10,' ')+' '+
            CompletaPalavra_a_Esquerda(fieldbyname('nomeportador').asstring,17,' ')+' '+
            CompletaPalavra_a_Esquerda(fieldbyname('numerocheque').asstring,8,' ')+' '+
            CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('datapagamento').asdatetime),8,' ')+' '+
            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorpagamento').asstring),10,' ')+' '+
            CompletaPalavra_a_Esquerda(formatdatetime('dd/mm/yy',fieldbyname('vencimento').asdatetime),10,' ')+' '+
            CompletaPalavra_a_Esquerda(fieldbyname('codigocredordevedor').asstring + '-' + Self.Pendencia.Titulo.CredorDevedor.Get_NomeCredorDevedor(fieldbyname('credordevedor').asstring,fieldbyname('codigocredordevedor').asstring),18,' '));

          IncrementaLinha(1);
          pTotalPago:= pTotalPago + fieldbyname('valorpagamento').ascurrency;

          //ImprimeDescricao='S'
          //rdprint.imp(linhalocal,1,'Descri��o '+fieldbyname('historico').asstring);
          //IncrementaLinha(1);
          //--

          Self.ObjQueryLocal.next;
        end;

        DesenhaLinha;

        verificalinha;
        rdprint.impf(linhalocal,1,'TOTAL: R$ '+ Formata_Valor(pTotalPago),[negrito]);
      end
      else
      begin
        //Flag false indica que ja n�o havia sido encontrado registro na busca de t�tulos
        //E se n�o encontrou nenhum registro de cheques ent�o n�o encontrou registro nenhum
        if(Flag = False)then
        begin
          ShowMessage('Nenhum registro foi encontrado.');
          RDprint.Abortar;
        end;
      end;
    end;

    rdprint.Fechar;
  end;
end;

procedure TObjLancamento.AtualizaTelaNovoLancamento(Ppendencia,
  PPagarReceber, PValorLancamento: string; var PcodigoLancamento: string;
  PhistoricoLancamento: String; PComCommit, PExportaContabilidade,
  PLancaAutomatico: Boolean; PcredorDevedorLote, PcodigoCredorDevedorLote,
  Pdata: string; PvalorMaior: currency);
var
  Pvalorfinal,ptaxa:Currency;
  Pnumerodias:Integer;
  pDiasUltimoJuro:Integer;
  dataultimojuros:string;
begin
     //Se vir a pendencia de parametro � um lancamento normal
     //caso nao venha � um lancamento em lote
     //Vindo a pendencia, as variaveis PreceberPagar e Valor lancamento
     //� preenchido com base na pendencia

     if (Ppendencia<>'')
     Then Begin
             if (Self.pendencia.LocalizaCodigo(ppendencia)=False)
             Then Begin
                      Messagedlg('Pend�ncia n�o encontrada!',mterror,[mbok],0);
                      exit;
             End;

             Self.Pendencia.TabelaparaObjeto;

             if (Self.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
             Then PPagarReceber:='P'
             Else PPagarReceber:='R';

             if (PValorLancamento='')
             Then Begin //soh calcula juros se eu nao coloquei o valor do lancamento antes de chamar

                        PValorLancamento:=Self.Pendencia.Get_Saldo;

                        if (ObjParametroGlobal.ValidaParametro('CALCULA JUROS NO LAN�AMENTO?')=False)
                        Then Begin
                                    Messagedlg('O Par�metro "CALCULA JUROS NO LAN�AMENTO?" n�o foi encontrado!',mterror,[mbok],0);
                                    exit;
                        End;

                        
                        if (ObjParametroGlobal.Get_valor='SIM')
                        then Begin
                                  if (ObjParametroGlobal.ValidaParametro('TAXA DE JUROS AO M�S NO LAN�AMENTO (%)')=False)
                                  Then Begin
                                           Messagedlg('O Par�metro "TAXA DE JUROS AO M�S NO LAN�AMENTO (%)" n�o foi encontrado!',mterror,[mbok],0);
                                           exit;
                                  End;


                                  Try
                                     Ptaxa:=strtofloat(ObjParametroGlobal.Get_Valor);
                                     Ptaxa:=Ptaxa/30;//ao dia
                                     ptaxa:=strtofloat(tira_ponto(formata_valor(ptaxa)));
                                  Except
                                     Ptaxa:=0;
                                  End;

                                  Try
                                     Pnumerodias:=0;
                                     if (now>strtodate(Self.Pendencia.Get_VENCIMENTO))//ta vencido
                                     then Begin
                                       Pnumerodias:=strtoint(floattostr(strtodate(datetostr(now))-strtodate(Self.Pendencia.Get_VENCIMENTO)));

                                       dataultimojuros := get_campoTabela('data','codigo','tablancamento',
                                        get_campoTabela('max(codigo) codigo','','tablancamento','where pendencia='+self.Pendencia.Get_CODIGO+' and tipolancto=5','codigo')
                                       );
                                       try
                                         pDiasUltimoJuro := StrToInt(floattostr(strtodate(datetostr(now)) - strtodate(dataultimojuros)));
                                       except
                                         pDiasUltimoJuro := Pnumerodias;
                                       end;
                                     End;
                                  Except
                                        Pnumerodias:=0;
                                  End;

                                  Try
                                     //Pvalorfinal:=strtofloat(Self.Pendencia.Get_Saldo)+((strtofloat(Self.Pendencia.Get_Saldo)*ptaxa*Pnumerodias)/100);
                                     Pvalorfinal:=strtofloat(Self.Pendencia.Get_Saldo)+((strtofloat(Self.Pendencia.Get_Saldo)*ptaxa*pDiasUltimoJuro)/100);
                                  Except
                                     Pvalorfinal:=0;
                                  End;

                                  FLancaNovoLancamento.FrValorLancto.Visible:=True;
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbSaldoPendencia.Caption:=formata_valor(Self.Pendencia.Get_Saldo);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbTaxaDia.caption:=formata_valor(ptaxa);
                                  //FLancaNovoLancamento.FrValorLancto.PJ_lbdiaatraso.caption:=inttostr(Pnumerodias);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbdiaatraso.caption:=inttostr(pDiasUltimoJuro);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbSaldoComJuros.caption:=formata_valor(Pvalorfinal);
                                  FLancaNovoLancamento.FrValorLancto.pj_vencimento.caption:=Self.pendencia.get_vencimento;
                                  //FLancaNovoLancamento.FrValorLancto.UpDown.Max:=Pnumerodias;
                                  FLancaNovoLancamento.FrValorLancto.UpDown.Max:=pDiasUltimoJuro;
                                  FLancaNovoLancamento.FrValorLancto.edtcarencia.Text:='0';
                                  FLancaNovoLancamento.FrValorLancto.edtdatalancamento.text:=datetostr(now);
                                  FLancaNovoLancamento.FrValorLancto.PJ_lbValorJuros.Caption := formata_valor(Pvalorfinal - StrToCurr(Self.Pendencia.Get_Saldo));
                                  PValorLancamento:=tira_ponto(formata_valor(floattostr(pvalorfinal)));
                        End;
             End;//valor lancamento=''
     End
     Else Self.Pendencia.ZerarTabela;

     limpaedit(FLancaNovoLancamento);
     FLancaNovoLancamento.FrValorLancto.edtdatalancamento.text:=datetostr(now);

     FLancaNovoLancamento.PcodigoLancamentoLocal:='';
     FlancaNOvoLancamento.PassaObjeto(self,PPagarReceber,PExportaContabilidade,PComCommit,PLancaAutomatico,PcredorDevedorLote,PcodigoCredorDevedorLote);

     FLancaNovoLancamento.Rgopcoes.ItemIndex:=0;
     FlancaNOvoLancamento.edtpendencia.Text:=Self.pendencia.get_codigo;//Se for em Lote nao vai existir a pendencia
     FLancaNovoLancamento.edtdata.text:=Pdata;


     FLancaNovoLancamento.edtvalor.text:=PValorLancamento;

     if (PvalorMaior>0)
     Then FLancaNovoLancamento.ValorDinheiro:=currtostr(PvalorMaior)
     Else FLancaNovoLancamento.ValorDinheiro:=PValorLancamento;

     FlancaNOvoLancamento.PanelLancamento.Enabled:=True;
     FLancaNovoLancamento.edthistorico.text:=PhistoricoLancamento;

     FlancaNOvoLancamento.PcodigoLancamentoLocal:=PcodigoLancamento;
end;


function TObjLancamento.RetornaTitulolancamentoUnicoCredor(
  pLancamento: string): string;
var
  sqlTemp, pTituloTemp: string;
  lote: Boolean;
begin
  {Este m�todo ser� utilizado na impress�o do recibo de lancamento
  onde � necess�rio obter os dados do credordevedor atrav�s do t�tulo, ent�o:

  Caso o lancamento seja de 1 pendencia, ent�o � s� pegar o titulo desse cara, pois
  � de 1 unico cliente

  Agora Caso seja em lote, pode ser que tenha pendencia de v�rios clientes, nesse caso
  n�o vai retornar o titulo.

  Somente se todos os titulos forem do mesmo cliente

  }
  Result := '';

  {checando se esse lancamento foi em lote, ou seja caso exista algum registro
  com o campo lancamentopai preenchido entao � em lote
  }

  ObjQueryLancto.Close;
  sqlTemp := ObjQueryLancto.SQL.Text;
  ObjQueryLancto.SQL.Text := 'SELECT * ' +
                         ' FROM TABLANCAMENTO L' +
                         ' WHERE L.LANCAMENTOPAI = :PLANCAMENTOPAI';                
  ObjQueryLancto.Params[0].AsString := pLancamento;

  ObjQueryLancto.Open;
  lote := true;
  if ObjQueryLancto.IsEmpty then
    lote := False;

  {se � lote precisa saber se � de um unico cliente
  }
  if lote then
  begin
    ObjQueryLancto.Close;
    ObjQueryLancto.SQL.Text := 'SELECT DISTINCT(T.CODIGOCREDORDEVEDOR)' +
                           ' FROM TABLANCAMENTO L' +
                           ' JOIN TABPENDENCIA P ON P.CODIGO = L.PENDENCIA' +
                           ' JOIN TABTITULO T ON T.CODIGO=P.TITULO' +
                           ' WHERE L.LANCAMENTOPAI = :PLANCAMENTO';
    ObjQueryLancto.Params[0].AsString := pLancamento;
    ObjQueryLancto.Open;
    ObjQueryLancto.Last;
    //se for retorno o titulo
    if ObjQueryLancto.RecordCount = 1 then
    begin
      {como o credor devedor � o mesmo em todos os titulos, entao qualquer
      um pode ser utilizado
      }
      ObjQueryLancto.Close;
      ObjQueryLancto.SQL.Text := 'SELECT FIRST 1 T.CODIGO' +
                             ' FROM TABLANCAMENTO L' +
                             ' JOIN TABPENDENCIA P ON P.CODIGO = L.PENDENCIA' +
                             ' JOIN TABTITULO T ON T.CODIGO=P.TITULO' +
                             ' WHERE L.LANCAMENTOPAI = :PLANCAMENTO';
      ObjQueryLancto.Params[0].AsString := pLancamento;
      ObjQueryLancto.Open;

      Result := ObjQueryLancto.Fields[0].AsString;
    end;
    //se tiver v�rios credores ent�o n�o volta nenhum titulo
  end
  else
  begin
    ObjQueryLancto.Close;
    ObjQueryLancto.SQL.Text := 'SELECT FIRST 1 T.CODIGO' +
                           ' FROM TABLANCAMENTO L' +
                           ' JOIN TABPENDENCIA P ON P.CODIGO = L.PENDENCIA' +
                           ' JOIN TABTITULO T ON T.CODIGO=P.TITULO' +
                           ' WHERE L.CODIGO = :PLANCAMENTO';
    ObjQueryLancto.Params[0].AsString := pLancamento;
    ObjQueryLancto.Open;

    Result := ObjQueryLancto.Fields[0].AsString;
  end;
end;

end.
