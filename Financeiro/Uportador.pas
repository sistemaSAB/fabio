unit Uportador;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPortador,
  Grids, ULanctoPortadorFR, UfrBaixaCheque,UObjChequesPortador;

type
  TFportador = class(TForm)
    Guia: TTabbedNotebook;
    STRChequePortador: TStringGrid;
    STRGEmitidos: TStringGrid;
    FrBaixaCheque1: TFrBaixaCheque;
    panelbotoes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    btopcoes: TBitBtn;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    BtCancelar: TBitBtn;
    btsair: TBitBtn;
    Panel2: TPanel;
    lbAtivo: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    Label18: TLabel;
    Label11: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    edtnome: TEdit;
    edtagencia: TEdit;
    edtnumeroconta: TEdit;
    edtendereco: TEdit;
    edttelefone: TEdit;
    ComboPermiteEmissaoCheque: TComboBox;
    edtCodigoPlanodeContas: TMaskEdit;
    comboapenas_uso_sistema: TComboBox;
    comboPermite_Saldo_Negativo: TComboBox;
    ComboClassificacao: TComboBox;
    FrLanctoPortador1: TFrLanctoPortador;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    ImagemFundo: TImage;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lbSaldoPortador: TLabel;
    lbCheque: TLabel;
    lbDinheiro: TLabel;
    lbPortador: TLabel;
    shp3: TShape;
    checkativo: TCheckBox;
    Shape1: TShape;
    Panel1: TPanel;
    ImagemFundoBaixa: TImage;
    Label1: TLabel;
    panelPesquisa: TPanel;
    edtPesquisa: TEdit;
    btAjuda: TSpeedButton;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FrLanctoPortador1BtNovoClick(Sender: TObject);
    procedure edtCodigoPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoPlanodeContasKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FrLanctoPortador1btexcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FrLanctoPortador1BtgravarClick(Sender: TObject);
    procedure FrLanctoPortador1BtCancelarClick(Sender: TObject);
    procedure ComboClassificacaoKeyPress(Sender: TObject; var Key: Char);
    procedure FrLanctoPortador1Label7Click(Sender: TObject);
    procedure checkativoClick(Sender: TObject);
    procedure STRChequePortadorDblClick(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure STRChequePortadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAjudaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edtCodigoPlanodeContasDblClick(Sender: TObject);
    procedure FrBaixaCheque1BtbaixaClick(Sender: TObject);

  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure PreparaAlteracao;
    procedure LimpaLabel;
    function AtualizaQuantidade:string;
    procedure controlChange(sender: TObject);

  public
    { Public declarations }

  end;

var
  Fportador: TFportador;
  ObjPortador:TObjPortador;
  OBjChequesPortador:TObjChequesPortador;
  linha,coluna:Integer;

implementation

uses
  UessencialGlobal, Upesquisa, UPLanodeContas, UTitulo, UDataModulo,
  UescolheImagemBotao, UObjValores, UTitulo_novo, Uprincipal, UAjuda;


{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFportador.ControlesParaObjeto: Boolean;
begin
  try
    with ObjPortador do
    begin

      Submit_CODIGO           (lbcodigo.caption);
      Submit_Nome             (edtNome.text);
      Submit_Agencia          (edtAgencia.text);
      Submit_NumeroConta      (edtNumeroConta.text);
      Submit_Endereco         (edtEndereco.text);
      Submit_Telefone         (edtTelefone.text);

      if ( checkativo.Checked=True) then
        Submit_Ativo('S')
      else Submit_Ativo('N');

      //Submit_Saldo            (tira_ponto(lbsaldoportador.caption));
      Submit_PermiteEmissaoCheque(ComboPermiteEmissaoCheque.text);
      Submit_CodigoPlanodeContas(edtCodigoPlanodeContas.text);
      Submit_Apenas_Uso_Sistema(comboapenas_uso_sistema.text);
      Submit_Permite_Saldo_Negativo(comboPermite_Saldo_Negativo.text);
      Submit_Classificacao(ComboClassificacao.Text);

      result:=true;
    end;
  except
    result:=False;
  end;
end;

function TFportador.ObjetoParaControles: Boolean;
var
  SomaTudo,SomaCheques:Currency;
  TempPortador:integer;
Begin
  Try
    with ObjPortador do
    begin
      lbcodigo.caption                         :=  Get_CODIGO          ;
      edtNome.text                             :=  Get_Nome            ;
      edtAgencia.text                          :=  Get_Agencia         ;
      edtNumeroConta.text                      :=  Get_NumeroConta     ;
      edtEndereco.text                         :=  Get_Endereco        ;
      edtTelefone.text                         :=  Get_Telefone        ;
      lbsaldoportador.caption                  :=  Get_SaldopelosLancamentos(lbcodigo.caption);

      if (Get_Permite_Saldo_Negativo='S') then
        comboPermite_Saldo_Negativo.ItemIndex:=1
      else comboPermite_Saldo_Negativo.ItemIndex:=0;

      //pegando os cheques de 3
      SomaCheques:=OBjChequesPortador.Soma_Cheques_Terceiro_Portador(lbcodigo.caption);
      Try
        somatudo:=strtocurr(lbsaldoportador.caption);
      Except
        somatudo:=0;
      End;
      lbdinheiro.caption:=Formata_Valor(currtostr(SomaTudo-SomaCheques));
      lbcheque.caption:=Formata_Valor(currtostr(SomaCheques));
      lbsaldoportador.caption:=formata_valor(lbsaldoportador.caption);

      if (get_Ativo='S') then
      begin
        checkativo.Checked:=True;
        lbAtivo.Caption:='ATIVO';
      end
      else
      begin
        checkativo.Checked:=False;
        lbAtivo.Caption:='INATIVO';
      end;
      checkativo.Enabled:=False;

      if (Get_PermiteEmissaoCheque='S') then
        ComboPermiteEmissaoCheque.itemindex:=1
      else ComboPermiteEmissaoCheque.itemindex:=0;

      edtCodigoPlanodeContas.text:=Get_CodigoPlanodeContas;

      if (Get_Apenas_Uso_Sistema='N') then
        comboapenas_uso_sistema.itemindex:=0
      else comboapenas_uso_sistema.itemindex:=1;

      if (Get_Classificacao = 'C') then
        ComboClassificacao.ItemIndex:=0
      else
        if (Get_Classificacao = 'B') then
          ComboClassificacao.ItemIndex:=1
        else ComboClassificacao.ItemIndex:=2;

      lbPortador.Caption:=Get_Nome;
      //***********************************fun��o para preencher o grid******************/////////
      STRChequePortador.ColCount:=2;
      STRChequePortador.RowCount:=2;
      STRChequePortador.Rows[0].clear;
      STRChequePortador.Rows[1].clear;
      STRChequePortador.FixedCols:=1;
      STRChequePortador.fixedrows:=1;

      Try
        TempPortador:=Strtoint(lbcodigo.caption);
      Except
        exit;
      End;

      OBjChequesPortador.get_ListaporPortador(lbcodigo.caption,STRChequePortador,False,'');
      AjustaLArguraColunaGrid(STRChequePortador);
      Guia.PageIndex:=0;
      //***************************************FIM********************************************//

      result:=True;
    End;
  Except
    Result:=False;
  End;
End;

function TFportador.TabelaParaControles: Boolean;
begin
  ObjPortador.TabelaparaObjeto;
  ObjetoParaControles;
end;

//****************************************
//****************************************

procedure TFportador.FormActivate(Sender: TObject);
begin
  If (Tag=1)
  Then exit;

  Self.Tag:=1;//evitar que se chame o form ativate mais de uma vez
  //p tag passa a ser zero somente quando sai do form

  limpaedit(Self);
  desabilita_campos(Self);
  Self.LimpaLabel;
  checkativo.Enabled:=False;

end;

procedure TFportador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (ObjPortador<>Nil) then
  begin
    If (ObjPortador.status<>dsinactive) then
    begin
      Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
      abort;
      exit;
    End;
  End;

  if (ObjLanctoPortadorGlobal <> nil) then
  begin

    if (ObjLanctoPortadorGlobal.Status<>dsinactive) then
    begin
      Messagedlg('Cancele ou salve o lan�amento antes de sair!',mterror,[mbok],0);
      abort;
      exit;
    End;

  end;

  if (ObjPortador<>nil)
  Then ObjPortador.free;

  if (OBjChequesPortador<>nil)
  Then OBjChequesPortador.Free;

  FrLanctoPortador1.Destruir;
  FrBaixaCheque1.Destroi;

  Self.tag:=0;
  Screen.OnActiveControlChange:=nil;
end;

procedure TFportador.btsairClick(Sender: TObject);
begin
  Close;
end;

procedure TFportador.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
    Perform(Wm_NextDlgCtl,0,0);
  end;
end;


procedure TFportador.BtNovoClick(Sender: TObject);
begin
  limpaedit(Self);
  habilita_campos(Self);
  esconde_botoes(Self);
  Guia.pageindex:=0;

  //lbcodigo.caption:='0';

  Self.LimpaLabel;
  lbcodigo.caption:=ObjPortador.get_novocodigo;
  lbAtivo.Caption:='ATIVO';
  checkativo.Enabled:=True;

  lbdinheiro.caption:='0,00';
  lbcheque.caption:='0,00';
  lbsaldoportador.caption:='0,00';//o saldo tem que ser colocado via lancto em portador
  Btgravar.Visible:=True;
  BtCancelar.Visible:=True;
  comboapenas_uso_sistema.ItemIndex:=0;

  ObjPortador.status:=dsInsert;
  Edtnome.setfocus;
end;


procedure TFportador.BtCancelarClick(Sender: TObject);
begin
  guia.PageIndex:=0;
  objPortador.cancelar;
  Self.LimpaLabel;
  limpaedit(Self);
  desabilita_campos(Self);
  mostra_botoes(Self);
  checkativo.Enabled:=False;
  ObjPortador.Status:=dsInactive;

  STRChequePortador.ColCount:=2;
  STRChequePortador.RowCount:=2;
  STRChequePortador.Rows[0].clear;
  STRChequePortador.Rows[1].clear;
end;


procedure TFportador.BtgravarClick(Sender: TObject);
begin

  If ObjPortador.Status=dsInactive
  Then exit;

  If ControlesParaObjeto=False then
  begin
    Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
    exit;
  End;

  //Mando Salva e Mando Dar o Commit
  If (ObjPortador.salvar(True)=False)
  Then exit;

  mostra_botoes(Self);
  limpaedit(Self);
  desabilita_campos(Self);
  ObjetoParaControles;
  lbquantidade.Caption:=AtualizaQuantidade;
  Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFportador.PreparaAlteracao;
begin
  habilita_campos(Self);
  esconde_botoes(Self);
  BtCancelar.Visible:=True;
  Btgravar.Visible:=True;
  ObjPortador.Status:=dsEdit;
  checkativo.Enabled:=True;
  Guia.pageindex:=0;
  edtNome.setfocus;
end;

procedure TFportador.btpesquisarClick(Sender: TObject);
var
  FpesquisaLocal:TFpesquisa;
begin

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FpesquisaLocal.NomeCadastroPersonalizacao:='FPORTADOR.PESQUISA';
    If (FpesquisaLocal.PreparaPesquisa(ObjPortador.Get_Pesquisa,ObjPortador.Get_TituloPesquisa,Nil)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok) then
        begin
          If ObjPortador.status<>dsinactive
          then exit;

          If (ObjPortador.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False) then
          begin
            Messagedlg('Dados n�o encontrados na tabela de Portador !',mterror,[mbok],0);
            exit;
          End;

          TabelaParaControles;
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
  End;
end;

procedure TFportador.btalterarClick(Sender: TObject);
begin
  If (ObjPortador.Status=dsinactive) and (lbcodigo.caption<>'')
  Then PreparaAlteracao;
end;

procedure TFportador.btexcluirClick(Sender: TObject);
begin
  If (ObjPortador.status<>dsinactive) or (lbcodigo.caption='')
  Then exit;

  If (ObjPortador.LocalizaCodigo(lbcodigo.caption)=False) then
  begin
    Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
    exit;
  End;

  If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
  Then exit;

  If (ObjPortador.exclui(lbcodigo.caption,True)=False) then
  begin
    Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
    exit;
  End;

  limpaedit(Self);
  lbquantidade.Caption:=AtualizaQuantidade;
  Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;


procedure TFportador.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
var
  TempPortador:integer;
Begin

  (*If (NewTab=0)
  Then Begin
         If (objPortador.status<>dsinactive)
         Then exit;


         If (lbcodigo.caption<>'')
         Then Begin
                   IF (ObjPortador.LocalizaCodigo(lbcodigo.caption)=False)
                   Then Begin
                             ObjPortador.ZerarTabela;
                             limpaedit(self);
                             exit;
                   End;
                   ObjPortador.TabelaparaObjeto;
                   ObjetoParaControles;
                   exit;
        End;

  End;  *)

  If NewTab=0 then
  begin
    If (ObjPortador.Status=dsinactive) and (lbcodigo.caption='')
    Then exit;

    STRChequePortador.ColCount:=2;
    STRChequePortador.RowCount:=2;

    STRChequePortador.Rows[0].clear;
    STRChequePortador.Rows[1].clear;

    STRChequePortador.FixedCols:=1;
    STRChequePortador.fixedrows:=1;

    Try
      TempPortador:=Strtoint(lbCodigo.Caption);
    Except
      exit;
    End;
    //O portador por sua vezes cria um objeto de Chequesportador que
    //faz uma pesquisa relacionada a tabvalores buscando os dados dos cheques
    //o ultimo parametro � a data de vencimento dos cheques
    //usado em dep�sitos, como deixei nulo pega todos
    OBjChequesPortador.get_ListaporPortador(lbcodigo.caption,STRChequePortador,False,'');
    AjustaLArguraColunaGrid(STRChequePortador);
  end;

  If NewTab=1 then
  begin
    If (ObjPortador.Status=dsinactive)and(lbcodigo.caption='')
    Then exit;

    STRChequePortador.ColCount:=2;
    STRChequePortador.RowCount:=2;

    STRChequePortador.Rows[0].clear;
    STRChequePortador.Rows[1].clear;

    STRChequePortador.FixedCols:=1;
    STRChequePortador.fixedrows:=1;

    Try
      TempPortador:=Strtoint(lbcodigo.caption);
    Except
      exit;
    End;
    //busca no Objeto Transferencia a Lista de Cheques por Portador
    //O portador por sua vezes cria um objeto de Chequesportador que
    //faz uma pesquisa relacionada a tabvalores buscando os dados dos cheques
    ObjPortador.Get_ListaCheques_do_Portador(lbcodigo.caption,Strgemitidos);
    AjustaLArguraColunaGrid(STRGemitidos);
  End;

  If (Newtab=2) then//lancto em portador
  begin
    iF (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR CADASTRO DE LAN�AMENTOS DIRETOS NO PORTADOR')=False)
    Then AllowChange:=False;
  End;

  If (Newtab=3) then//baixa de cheque
  begin
    iF (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR CADASTRO DE BAIXA DE CHEQUE')=False)
    Then AllowChange:=False;
  End;
end;

procedure TFportador.btrelatoriosClick(Sender: TObject);
begin
  ObjPortador.Imprime(lbcodigo.caption);
end;

procedure TFportador.FrLanctoPortador1BtNovoClick(Sender: TObject);
begin
  FrLanctoPortador1.BtNovoClick(Sender);
  FrLanctoPortador1.edtportador.text:=Self.lbcodigo.caption;
end;

procedure TFportador.edtCodigoPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
  FplanodeCOntas:TFplanodeCOntas;
begin

  If (key <>vk_f9)
  Then exit;

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FplanodeCOntas:=TFplanodeCOntas.create(NIl);
    FpesquisaLocal.NomeCadastroPersonalizacao:='FPORTADOR.EDTCODIGOPLANODECONTAS';

    If (FpesquisaLocal.PreparaPesquisa(ObjPortador.Get_PesquisaPlanodeContas,ObjPortador.Get_TituloPesquisaPlanodeContas,FplanodeCOntas)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok)
        Then edtCodigoPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FplanodeCOntas);
  End;
end;

procedure TFportador.edtCodigoPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
  If not(key in['0'..'9',#8])
  Then key:=#0;
end;

procedure TFportador.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FTitulo:TFtitulo;
begin

  If ( key=VK_F12) then
  begin
    Try
      Ftitulo:=TFtitulo.create(nil);
      Ftitulo.Showmodal;
    Finally
      Freeandnil(Ftitulo);
    End;
  End
  else
    if (Key = vk_f1) then
    begin
      Fprincipal.chamaPesquisaMenu();
    end;

  if (Key = VK_F2) then
  begin
    FAjuda.PassaAjuda('CADASTRO DE PORTADOR');
    FAjuda.ShowModal;
  end;
end;

procedure TFportador.FrLanctoPortador1btexcluirClick(Sender: TObject);
begin
  FrLanctoPortador1.btexcluirClick(Sender);
end;

procedure TFportador.FormShow(Sender: TObject);
begin
  Uessencialglobal.PegaCorForm(Self);
  Screen.OnActiveControlChange:=Self.controlChange;

  Try
    ObjPortador:=TObjPortador.create;
    ObjChequesPortador:=TObjChequesPortador.Create;

    If FrLanctoPortador1.Criar=False
    Then close;

    iF (FrBaixaCheque1.Cria=False)
    Then Close;

  Except
    Messagedlg('Erro na Inicializa��o do Objeto Portador e Cheques do Portador!',mterror,[mbok],0);
    Self.close;
  End;

  FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
  FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
  FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
  FescolheImagemBotao.PegaFiguraImagem(ImagemFundoBaixa,'FUNDO');
  FescolheImagemBotao.PegaFiguraBotaopequeno(FrBaixaCheque1.Btbaixa,'BOTAOBAIXACHEQUES.BMP');
  FescolheImagemBotao.PegaFiguraBotaopequeno(FrBaixaCheque1.btestornabaixa,'BOTAOESTORNABAIXACHEQUES.BMP');


  retira_fundo_labels(self);
  Self.Color:=clwhite;
  lbquantidade.Caption:=AtualizaQuantidade;

  If (ObjPortador.VerificaPermissao=False) then
    esconde_botoes(Self)
  Else mostra_botoes(Self);

  Guia.PageIndex:=0;
  //Guia.PageIndex:=0;
end;

procedure TFportador.FrLanctoPortador1BtgravarClick(Sender: TObject);
begin
  FrLanctoPortador1.BtgravarClick(Sender);
end;

procedure TFportador.FrLanctoPortador1BtCancelarClick(Sender: TObject);
begin
  FrLanctoPortador1.BtCancelarClick(Sender);
end;

procedure TFportador.ComboClassificacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  Abort;
end;

procedure TFportador.FrLanctoPortador1Label7Click(Sender: TObject);
begin
  FrLanctoPortador1.Label7Click(Sender);
end;

procedure TFportador.checkativoClick(Sender: TObject);
begin
  if (checkativo.Checked=True)
  then lbAtivo.Caption:='ATIVO'
  else lbAtivo.Caption:='INATIVO';
end;

procedure TFportador.LimpaLabel;
begin
  lbCodigo.Caption:='';
  lbDinheiro.Caption:='';
  lbCheque.Caption:='';
  lbSaldoPortador.Caption:='';
  lbPortador.Caption:='';
  lbAtivo.Caption:='';
  lbDinheiro.Caption:='0,00';
  lbCheque.Caption:='0,00';
  lbSaldoPortador.Caption:='0,00';
  lbPortador.Caption:='';
  FrLanctoPortador1.LbTipoLanctoNome.Caption:='';
end;

function TFportador.AtualizaQuantidade;
begin
  Result:='Existem '+ContaRegistros('tabportador','codigo')+' Portadores cadastrados';
end;

procedure TFportador.controlChange(sender: TObject);
begin
  controlChange_focus(sender,Self);
end;

procedure TFportador.STRChequePortadorDblClick(Sender: TObject);
var
  ObjValores:TObjValores;
  codCheque:string;
  numTitulo:string;
  FtituloNovo:TFtitulo_novo;
begin

  //jonas
  try

    ObjValores:=TObjValores.Create;
    
    codCheque:=STRChequePortador.Cells[19,STRChequePortador.Row];

    if (codCheque <> '') then
    begin

      ObjValores.Cheque.LocalizaCodigo(codCheque);
      Objvalores.localizacheque(codCheque);

      ObjValores.TabelaparaObjeto;

      numTitulo := ObjValores.Lancamento.Pendencia.Titulo.get_codigo;

      if (numTitulo <> '') then
      begin

        try
          FtituloNovo:=TFtitulo_novo.Create(nil);
          FtituloNovo.LocalizaCodigoInicial(numTitulo);
          FtituloNovo.ShowModal;
        finally
          FreeAndNil(FtituloNovo);
        end;
        
      end;

    end;
  finally
    ObjValores.Free;
  end;
end;

procedure TFportador.btn1Click(Sender: TObject);
begin

  {if (panelPesquisa.Visible) then
      panelPesquisa.Visible:=False
  else
      panelPesquisa.Visible:=True;}

end;

procedure TFportador.STRChequePortadorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  if (Key = VK_SPACE) then
  begin
    panelPesquisa.Visible:=True;
    edtPesquisa.Enabled:=True;
    edtPesquisa.SetFocus;
    linha:=1;
    coluna:=STRChequePortador.Col;
  end;
end;

procedure TFportador.edtPesquisaKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  indice:Integer;
  //linha,coluna:Integer;
begin

  if (Key = 27) then //27 - ESC
  begin
    panelPesquisa.Visible:=False;
  end
  else
    if (Key = 13) then // 13 - enter
    begin

      for indice:=linha to STRChequePortador.RowCount - 1 do
      begin

        if Pos( UpperCase(edtPesquisa.Text), UpperCase(STRChequePortador.Cells[coluna,indice])) > 0  then
        begin

          STRChequePortador.Col := coluna;
          STRChequePortador.Row := indice;
          guia.PageIndex:=0;
          STRChequePortador.Setfocus;

          linha:=indice+1;
          exit;
          {if (indice = STRChequePortador.RowCount-1)
          then begin

          ShowMessage('N�o h� mais itens');
          linha:=1;
          panelPesquisa.Visible:=False;
          STRChequePortador.SetFocus;
          Exit;

          end;}

        end
        else
        begin

        end;

      end;

      ShowMessage('N�o h� mais itens');
      linha:=1;
      panelPesquisa.Visible:=False;
      STRChequePortador.SetFocus;
      Exit;
    end;
end;

procedure TFportador.btAjudaClick(Sender: TObject);
begin
  FAjuda.PassaAjuda('CADASTRO DE PORTADOR');
  FAjuda.ShowModal;
end;

//Rodolfo
procedure TFportador.FormCreate(Sender: TObject);
var
  addDuploClique : TDuploClique;
begin
  addDuploClique.AdicionaDuploClique(Sender); //Chamada da adi�ao do evento duplo clique nos edits
end;

//Rodolfo
procedure TFportador.edtCodigoPlanodeContasDblClick(Sender: TObject);
var
  key : Word;
  shift : TShiftState;
begin
  key := VK_F9;
  edtCodigoPlanodeContasKeyDown(Sender, key, shift);
end;

procedure TFportador.FrBaixaCheque1BtbaixaClick(Sender: TObject);
begin
  FrBaixaCheque1.BtbaixaClick(Sender);

end;

end.

