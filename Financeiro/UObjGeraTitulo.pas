unit UObjGeraTitulo;
Interface
Uses ibquery,windows,stdctrls,UobjSubContagerencial,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjGerador,UObjCredorDevedor,UobjPortador,UObjContaGer,UobjPrazoPagamento;

Type
   TObjGeraTitulo=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ContaGerencial           :TobjContaGer;
                SubContaGerencial        :TobjSubContagerencial;
                Gerador                  :Tobjgerador;
                CredorDevedor            :TOBjCredorDevedor;
                Prazo                    :TObjPRazoPagamento;
                Portador                 :TobjPortador;
                
                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaHistorico(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaGerador             :TStringList;
                Function    Get_TituloPesquisaGerador       :string;
                Function    Get_PesquisaCredorDevedor       :TStringList;
                Function    Get_TituloPesquisaCredorDevedor :string;
                Function    Get_PesquisaPortador            :TStringList;
                Function    Get_TituloPesquisaPortador      :string;
                Function    Get_PesquisaPrazoPagamento      :TStringList;
                Function    Get_TituloPesquisaPrazoPagamento:string;
                Function    Get_PesquisaContaGerencial            :TStringList;
                Function    Get_TituloPesquisaContaGerencial      :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO             :string;
                Function Get_Historico          :string;
                Function Get_HistoricoContaFacil:string;
                Function Get_MostraSubConta_ContaFacil:string;
                Function Get_Gerador            :string;
                Function Get_CodigoGerador      :string;
                Function Get_CredorDevedor      :string;
                Function Get_CodigoCredorDevedor:string;
                Function Get_Prazo              :string;
                Function Get_Portador           :string;
                Function Get_ContaGerencial     :string;
                Function Get_ContaFacil:string;

                Procedure Submit_CODIGO             (parametro:string);
                Procedure Submit_Historico          (parametro:string);
                Procedure Submit_HistoricoContaFacil          (parametro:string);
                Procedure Submit_MostraSubConta_ContaFacil          (parametro:string);

                Procedure Submit_Gerador            (parametro:string);
                Procedure Submit_CodigoGerador      (parametro:string);
                Procedure Submit_CredorDevedor      (parametro:string);
                Procedure Submit_CodigoCredorDevedor(parametro:string);
                Procedure Submit_Prazo              (parametro:string);
                Procedure Submit_Portador           (parametro:string);
                Procedure Submit_ContaGerencial     (parametro:string);
                Procedure Submit_ContaFacil(Parametro:string);

                Function Get_PesquisaGeradorInstrucaoSQL(parametro:string): string;
                Function Get_PesquisaCredorDevedorInstrucaoSQL(parametro: string): string;
                Function  Get_NovoCodigo:string;

                procedure EdtSubcontagerencialExit(Sender: TObject; LabelNome: Tlabel);
                procedure edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; lbnome: Tlabel; PContager: string);
                procedure Pesquisa(campo:string;valor:string;ordenar:string);
                procedure edtgeradorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edtcodigogeradorKeyDown(Pgerador:String;Sender: TObject;var Key: Word; Shift: TShiftState);
                procedure edtcredordevedorKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
                procedure edtcodigocredordevedorKeyDown(pcredordevedor:string;Sender: TObject;var Key: Word; Shift: TShiftState);
                procedure edtprazoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

                procedure edtcontagerencialKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);







         Private
               ObjDataset:Tibdataset;
               ObjqueryPesquisa:TIbquery;

               CODIGO                   :String[09];
               Historico                :String;
               HistoricoContaFacil      :string;
               MostraSubConta_ContaFacil      :string;
               CodigoGerador            :String[09];
               CodigoCredorDevedor      :String[09];
               ContaFacil:string;




               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls, Upesquisa, UGerador,
  UCredorDevedor, UPrazoPagamento, Uportador, Ucontager;


{ TTabTitulo }


Procedure  TObjGeraTitulo.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO             :=FieldByName('CODIGO').asstring;
        Self.Historico          :=FieldByName('Historico').asstring;
        Self.historicocontafacil          :=FieldByName('historicocontafacil').asstring;
        Self.MostraSubConta_ContaFacil          :=FieldByName('MostraSubConta_ContaFacil').asstring;
        Self.CodigoGerador      :=FieldByName('CodigoGerador').asstring;
        Self.CodigoCredorDevedor:=FieldByName('CodigoCredorDevedor').asstring;
        Self.ContaFacil:=FieldByName('ContaFacil').asstring;


        If (FieldByName('Gerador').asstring<>'')
        Then Begin
                If (Self.Gerador.LocalizaCodigo(FieldByName('Gerador').asstring)=False)
                Then Begin
                          Messagedlg('Gerador N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.Gerador.TabelaparaObjeto;
        End;


        If (FieldByName('subcontagerencial').asstring<>'')
        Then Begin
                If (Self.SubContaGerencial.LocalizaCodigo(FieldByName('SubContaGerencial').asstring)=False)
                Then Begin
                          Messagedlg('Sub-Conta Gerencial N�o encontrada!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.SubContaGerencial.TabelaparaObjeto;
        End;

        If (FieldByName('CredorDevedor').asstring<>'')
        Then Begin
                If (Self.CredorDevedor.LocalizaCodigo(FieldByName('CredorDevedor').asstring)=False)
                Then Begin
                          Messagedlg('C�digo do Credor_Devedor N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.CredorDevedor.TabelaparaObjeto;
        End;
        IF (FieldByName('prazo').asstring<>'')
        Then Begin
                If (Self.Prazo.LocalizaCodigo(FieldByName('prazo').asstring)=False)
                Then Begin
                          Messagedlg('Prazo N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.Prazo.TabelaparaObjeto;
        End;
        If (FieldByName('Portador').asstring<>'')
        Then Begin
                If (Self.Portador.LocalizaCodigo(FieldByName('Portador').asstring)=False)
                Then Begin
                          Messagedlg('Portador N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.Portador.TabelaparaObjeto;
        End;
        If (FieldByName('ContaGerencial').asstring<>'')
        Then Begin
                If (Self.ContaGerencial.LocalizaCodigo(FieldByName('ContaGerencial').asstring)=False)
                Then Begin
                          Messagedlg('Conta Gerencial N�o encontrada!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.ContaGerencial.TabelaparaObjeto;
        End;
     End;
     
End;


Procedure TObjGeraTitulo.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring                    := Self.CODIGO                   ;
      FieldByName('Historico').asstring                 := Self.Historico                ;
      FieldByName('historicocontafacil').asstring                 := Self.historicocontafacil                ;
      FieldByName('MostraSubConta_ContaFacil').asstring                 := Self.MostraSubConta_ContaFacil                ;
      FieldByName('Gerador').asstring                   := Self.Gerador.Get_CODIGO       ;
      FieldByName('CodigoGerador').asstring             := Self.CodigoGerador            ;
      FieldByName('CredorDevedor').asstring             := Self.CredorDevedor.Get_CODIGO ;
      FieldByName('CodigoCredorDevedor').asstring       := Self.CodigoCredorDevedor      ;
      FieldByName('Prazo').asstring                     := Self.Prazo.Get_CODIGO         ;
      FieldByName('Portador').asstring                  := Self.Portador.Get_CODIGO      ;
      FieldByName('ContaGerencial').asstring            := Self.ContaGerencial.Get_CODIGO;
      Fieldbyname('subcontagerencial').asstring         :=Self.SubContaGerencial.Get_CODIGO;
      Fieldbyname('contafacil').asstring                :=Self.contafacil;
             
  End;
End;

//***********************************************************************

function TObjGeraTitulo.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjGeraTitulo.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        CODIGO          :='';
        Historico       :='';
        historicocontafacil       :='';
        MostraSubConta_ContaFacil       :='';
        Gerador.ZerarTabela;
        CodigoGerador   :='';
        CredorDevedor.ZerarTabela;
        CodigoCredorDevedor:='';
        Prazo.ZerarTabela;
        Portador.ZerarTabela;
        ContaGerencial.ZerarTabela;
        SubContaGerencial.ZerarTabela;
        contafacil:='';
     End;
end;

Function TObjGeraTitulo.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Historico='')
       Then Mensagem:=mensagem+'/HIst�rico';

       If (Gerador.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Gerador';

       If (CodigoGerador='')
       Then Mensagem:=mensagem+'/C�digo do gerador';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjGeraTitulo.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     If (Self.Gerador.LocalizaCodigo(Self.Gerador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Gerador n�o Encontrado!';

     If (Self.CredorDevedor.Get_CODIGO<>'')
     Then Begin
             If (Self.CredorDevedor.LocalizaCodigo(Self.CredorDevedor.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Credor_Devedor n�o Encontrado!';
     End;

     If (Self.Prazo.Get_CODIGO<>'')
     Then Begin
             If (Self.Prazo.LocalizaCodigo(Self.Prazo.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Prazo n�o Encontrado!';
     End;
     If (Self.Portador.Get_CODIGO<>'')
     Then Begin
             If (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';
     End;
     If (Self.ContaGerencial.Get_CODIGO<>'')
     Then Begin
             If (Self.ContaGerencial.LocalizaCodigo(Self.ContaGerencial.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Conta Gerencial n�o Encontrado!';
     End;

     If (Self.SubContaGerencial.Get_CODIGO<>'')
     Then Begin
             If (Self.SubContaGerencial.LocalizaCodigo(Self.SubContaGerencial.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Sub-Conta Gerencial n�o Encontrado!';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjGeraTitulo.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.Gerador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Gerador';
     End;

     try
        Inteiros:=Strtoint(Self.CodigoGerador);
     Except
           Mensagem:=mensagem+'/C�digo do Gerador';
     End;

     try
        if (Self.CredorDevedor.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.CredorDevedor.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Credor_Devedor';
     End;


     try
        If (Self.CodigoCredorDevedor<>'')
        Then  Inteiros:=Strtoint(Self.CodigoCredorDevedor);
     Except
           Mensagem:=mensagem+'/C�digo do Credor Devedor';
     End;

     try
        If (Self.Prazo.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.Prazo.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Prazo';
     End;

     try
        If (Self.Portador.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.Portador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Portador';
     End;

     try
        If (Self.ContaGerencial.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.ContaGerencial.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Conta Gerencial';
     End;

     try
        If (Self.SubContaGerencial.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.SubContaGerencial.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Sub-Conta Gerencial';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjGeraTitulo.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjGeraTitulo.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     if ((Self.ContaFacil<>'S') and (Self.ContaFacil<>'N'))
     then Mensagem:=Mensagem+'\O Campo Conta F�cil cont�m valores inv�lidos';

     if ((Self.MostraSubConta_ContaFacil<>'S') and (Self.MostraSubConta_ContaFacil<>'N'))
     then Mensagem:=Mensagem+'\O Campo Mostra Sub-Conta no Conta F�cil cont�m valores inv�lidos';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;
function TObjGeraTitulo.LocalizaHistorico(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Gerador,CodigoGerador,CredorDevedor,');
           SelectSql.add('CodigoCredorDevedor,Prazo,Portador,ContaGerencial,SubContagerencial,contafacil');
           SelectSql.add(',historicocontafacil,MostraSubConta_ContaFacil from TabGeraTitulo where historico='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjGeraTitulo.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Gerador,CodigoGerador,CredorDevedor,');
           SelectSql.add('CodigoCredorDevedor,Prazo,Portador,ContaGerencial,SubContagerencial,contafacil,HistoricoContaFacil,MostraSubConta_ContaFacil from TabGeraTitulo where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjGeraTitulo.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjGeraTitulo.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjGeraTitulo.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.Gerador:=TObjGerador.create;
        Self.CredorDevedor:=TObjCredorDevedor.create;
        Self.Prazo:=TObjPrazoPagamento.create;
        Self.Portador:=TObjPortador.create;
        Self.ContaGerencial:=TObjContaGer.create;
        Self.ParametroPesquisa:=TStringList.create;
        Self.SubContaGerencial:=TobjSubContagerencial.create;

        Self.objquerypesquisa:=Tibquery.create(nil);
        Self.objquerypesquisa.database:=FdataModulo.IBDatabase;
        Self.objdatasource:=TdataSource.create(nil);
        Self.objdatasource.dataset:=Self.objquerypesquisa;


        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Historico,Gerador,CodigoGerador,CredorDevedor,');
                SelectSql.add('CodigoCredorDevedor,Prazo,Portador,ContaGerencial,SubContagerencial,contafacil,historicocontafacil,MostraSubConta_ContaFacil from TabGeraTitulo where codigo=0');


                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert Into TabgeraTitulo (CODIGO,Historico,Gerador,CodigoGerador,CredorDevedor,');
                InsertSQL.add(' CodigoCredorDevedor,Prazo,Portador,ContaGerencial,SubContagerencial,contafacil,historicocontafacil,MostraSubConta_ContaFacil)');
                InsertSQL.add(' values (:CODIGO,:Historico,:Gerador,:CodigoGerador,:CredorDevedor,');
                InsertSQL.add(' :CodigoCredorDevedor,:Prazo,:Portador,:ContaGerencial,:SubContagerencial,:contafacil,:historicocontafacil,:MostraSubConta_ContaFacil)');

                ModifySQL.clear;
                ModifySQL.add(' UPdate TabGeratitulo set');
                ModifySQL.add(' CODIGO=:CODIGO,Historico=:Historico,Gerador=:Gerador,CodigoGerador=:CodigoGerador,CredorDevedor=:CredorDevedor,');
                ModifySQL.add(' CodigoCredorDevedor=:CodigoCredorDevedor,Prazo=:Prazo,Portador=:Portador,ContaGerencial=:ContaGerencial,SubContagerencial=:SubContagerencial');
                ModifySQL.add(',contafacil=:contafacil,historicocontafacil=:historicocontafacil,MostraSubConta_ContaFacil=:MostraSubConta_ContaFacil where codigo=:codigo'); 

                DeleteSQL.clear;
                DeleteSQL.add('Delete from TabGeraTitulo where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Historico,Gerador,CodigoGerador,CredorDevedor,');
                RefreshSQL.add('CodigoCredorDevedor,Prazo,Portador,ContaGerencial,SubContagerencial,contafacil,historicocontafacil,MostraSubConta_ContaFacil from TabGeraTitulo where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjGeraTitulo.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjGeraTitulo.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjGeraTitulo.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjGeraTitulo.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabGeraTitulo');
     Result:=Self.ParametroPesquisa;
end;

function TObjGeraTitulo.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de GeraTitulo ';
end;



{
function TObjGeraTitulo.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjGeraTitulo.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjGeraTitulo.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_GERATITULO';
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GeraTitulo',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            freeandnil(StrTemp);
     End;


end;


function TObjGeraTitulo.Get_CodigoCredorDevedor: string;
begin
     Result:=Self.CodigoCredorDevedor;
end;

function TObjGeraTitulo.Get_CodigoGerador: string;
begin
     Result:=Self.CodigoGerador;
end;

function TObjGeraTitulo.Get_ContaGerencial: string;
begin
     Result:=Self.ContaGerencial.Get_CODIGO;
end;

function TObjGeraTitulo.Get_CredorDevedor: string;
begin
     Result:=Self.CredorDevedor.get_codigo;
end;

function TObjGeraTitulo.Get_PesquisaCredorDevedor: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add(Self.CredorDevedor.get_pesquisa);
     Result:=Parametropesquisa;
end;

function TObjGeraTitulo.Get_Gerador: string;
begin
     Result:=Self.Gerador.Get_CODIGO;
end;

function TObjGeraTitulo.Get_Historico: string;
begin
     Result:=self.historico;
end;


function TObjGeraTitulo.Get_historicocontafacil: string;
begin
     Result:=self.historicocontafacil;
end;


function TObjGeraTitulo.Get_MostraSubConta_ContaFacil: string;
begin
     Result:=self.MostraSubConta_ContaFacil;
end;

function TObjGeraTitulo.Get_PesquisaContaGerencial: TStringList;
begin
        Self.ParametroPesquisa.clear;
        Self.ParametroPesquisa.add(Self.ContaGerencial.Get_Pesquisa);
        Result:=Self.ParametroPesquisa;
end;

function TObjGeraTitulo.Get_PesquisaGerador: TStringList;
begin
        Self.ParametroPesquisa.clear;
        Self.ParametroPesquisa.add(Self.Gerador.Get_Pesquisa);
        Result:=Self.ParametroPesquisa;
end;

function TObjGeraTitulo.Get_PesquisaPortador: TStringList;
begin
        Self.ParametroPesquisa.clear;
        Self.ParametroPesquisa.add(Self.Portador.Get_Pesquisa);
        Result:=Self.ParametroPesquisa;
End;
function TObjGeraTitulo.Get_PesquisaPrazoPagamento: TStringList;
begin
        Self.ParametroPesquisa.clear;
        Self.ParametroPesquisa.add(Self.Prazo.Get_Pesquisa);
        Result:=Self.ParametroPesquisa;

end;

function TObjGeraTitulo.Get_Portador: string;
begin
     result:=Self.portador.Get_CODIGO;
end;

function TObjGeraTitulo.Get_Prazo: string;
begin
     Result:=Self.Prazo.Get_CODIGO;
end;

function TObjGeraTitulo.Get_TituloPesquisaCredorDevedor: string;
begin
     Result:=Self.CredorDevedor.Get_TituloPesquisa;
end;

function TObjGeraTitulo.Get_TituloPesquisaGerador: string;
begin
     Result:=Self.Gerador.Get_TituloPesquisa;
end;

function TObjGeraTitulo.Get_TituloPesquisaPortador: string;
begin
     Result:=Self.Portador.Get_TituloPesquisa;
end;

function TObjGeraTitulo.Get_TituloPesquisaPrazoPagamento: string;
begin
     Result:=Self.Prazo.Get_TituloPesquisa;
end;

procedure TObjGeraTitulo.Submit_CodigoCredorDevedor(parametro: string);
begin
     Self.CodigoCredorDevedor:=Parametro;
end;

procedure TObjGeraTitulo.Submit_CodigoGerador(parametro: string);
begin
     Self.CodigoGerador:=Parametro;
end;

procedure TObjGeraTitulo.Submit_ContaGerencial(parametro: string);
begin
     Self.ContaGerencial.Submit_CODIGO(parametro);
end;

procedure TObjGeraTitulo.Submit_CredorDevedor(parametro: string);
begin
     Self.CredorDevedor.Submit_CODIGO(parametro);
end;

procedure TObjGeraTitulo.Submit_Gerador(parametro: string);
begin
     Self.Gerador.Submit_CODIGO(parametro);
end;

procedure TObjGeraTitulo.Submit_Historico(parametro: string);
begin
     Self.historico:=parametro;
end;


procedure TObjGeraTitulo.Submit_historicocontafacil(parametro: string);
begin
     Self.historicocontafacil:=parametro;
end;

procedure TObjGeraTitulo.Submit_MostraSubConta_ContaFacil(parametro: string);
begin
     Self.MostraSubConta_ContaFacil:=parametro;
end;



procedure TObjGeraTitulo.Submit_Portador(parametro: string);
begin
     Self.Portador.Submit_CODIGO(parametro);
end;

procedure TObjGeraTitulo.Submit_Prazo(parametro: string);
begin
     Self.prazo.Submit_CODIGO(parametro);
end;

function TObjGeraTitulo.Get_TituloPesquisaContaGerencial: string;
begin
     Result:=Self.ContaGerencial.Get_TituloPesquisa;
end;

function TObjGeraTitulo.Get_PesquisaGeradorInstrucaoSQL(
  parametro: string): string;
begin
     If (Self.gerador.LocalizaCodigo(parametro)=true)
     Then Begin
             Self.gerador.TabelaparaObjeto;
             Result:=Self.gerador.Get_InstrucaoSQL;
          End
     Else result:='';
end;

function TObjGeraTitulo.Get_PesquisaCredorDevedorInstrucaoSQL(
  parametro: string): string;
begin

     If (Self.CREDORDEVEDOR.LocalizaCodigo(parametro)=true)
     Then Begin
             Self.CREDORDEVEDOR.TabelaparaObjeto;
             Result:=Self.CREDORDEVEDOR.Get_InstrucaoSQL;
          End;

end;


destructor TObjGeraTitulo.Free;
begin
    freeandnil(ObjDataset);
    Self.Gerador.free;
    Self.CredorDevedor.free;
    Self.Prazo.free;
    Self.Portador.free;
    Self.ContaGerencial.free;
    Self.SubContaGerencial.free;
    Freeandnil(Self.ParametroPesquisa);
    freeandnil(objdatasource);
    freeandnil(objquerypesquisa);

end;

procedure TObjGeratitulo.EdtSubcontagerencialExit(Sender: TObject;
  LabelNome: Tlabel);
begin
     if (LabelNome<>nil)
     then labelnome.caption:='';

     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SubContaGerencial.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SUBCONTAGERENCIAL.tabelaparaobjeto;
     
     if (LabelNome<>nil)
     then LABELNOME.CAPTION:=Self.SUBCONTAGERENCIAL.get_mascara+' '+Self.SUBCONTAGERENCIAL.GET_NOME;

end;

procedure TObjGeratitulo.edtsubcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState;lbnome:Tlabel;PContager:string);

var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            if (lbnome<>nil)
            then lbnome.caption:='';

            If (FpesquisaLocal.PreparaPesquisa(Self.SubContaGerencial.Get_Pesquisa(Pcontager),self.SubContaGerencial.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then begin
                                  TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                  if (lbnome<>nil)
                                  then lbnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




procedure TObjGeraTitulo.Pesquisa(campo, valor, ordenar: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select * from ViewGeraTitulo');
          sql.add('where ContaFacil=''S'' ');

          if (campo<>'')
          then sql.add('and upper('+campo+') like '+#39+uppercase(valor)+'%'+#39);

          if (ordenar='')
          Then sql.add('order by historico')
          Else sql.add('order by '+ordenar);

          open;
     End;

end;

function TObjGeraTitulo.Get_ContaFacil: string;
begin
     Result:=Self.ContaFacil;
end;

procedure TObjGeraTitulo.Submit_ContaFacil(Parametro: string);
begin
     Self.ContaFacil:=Parametro;
end;


procedure TObjGeraTitulo.edtgeradorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fgerador:TFgerador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            Fgerador:=TFgerador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaGerador,Self.get_titulopesquisagerador,Fgerador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Fgerador.Close;
           Freeandnil(Fgerador);
     End;
End;     
procedure TObjGeraTitulo.edtcodigogeradorKeyDown(Pgerador: String;Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaGeradorinstrucaoSql(pgerador),'',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
End;

procedure TObjGeraTitulo.edtcredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fcredordevedor:TFcredordevedor;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            Fcredordevedor:=TFcredordevedor.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaCredorDevedor,Self.get_titulopesquisacredordevedor,Fcredordevedor)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fcredordevedor);
     End;

end;

procedure TObjGeraTitulo.edtcodigocredordevedorKeyDown(
  pcredordevedor: string; Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(self.Get_PesquisacredordevedorinstrucaoSql(pcredordevedor),'',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 tedit(sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;

procedure TObjGeraTitulo.edtprazoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

var
   FpesquisaLocal:Tfpesquisa;
   FPrazopagamento:TFPrazopagamento;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FPrazopagamento:=TFPrazopagamento.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(self.Get_PesquisaPrazoPagamento,self.get_titulopesquisaPrazopagamento,FPrazopagamento)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPrazopagamento);
     End;

end;
procedure TObjGeraTitulo.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            Fportador:=TFportador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(self.Get_PesquisaPortador,self.get_titulopesquisaPortador,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 tedit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;

end;

procedure TObjGeraTitulo.edtcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

var
   FpesquisaLocal:Tfpesquisa;
   Fcontager:TFcontager;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            Fcontager:=TFcontager.create(NIL);
            If (FpesquisaLocal.PreparaPesquisa(self.Get_PesquisaContaGerencial,self.get_titulopesquisaContaGerencial,Fcontager)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 tedit(sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fcontager);
     End;

end;



end.
