unit UobjMOEDA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc;


Type
   TObjMOEDA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaNome(Parametro:String) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: String);
                Function Get_CODIGO: String;
                Procedure Submit_NOME(parametro: String);
                Function Get_NOME: String;
                Procedure Submit_MoedaPadrao(parametro:String);
                Function Get_MoedaPadrao:String;
                Procedure Submit_SimboloMoedaPadrao(parametro:String);
                Function Get_SimboloMoedaPadrao:String;

                function ResgataMoedaPadrao:boolean;
                function RetornaNomeMoedaPadrao: String;
                function AtualizaMoedaPadrao(PCODIGO: String): Boolean;
                function CriaPrimeiraCotacao(PCodigo: String): Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:String;
               NOME:String;
               MOEDAPADRAO:String;
               SIMBOLOMOEDAPADRAO:String;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;



   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UobjCOTACAOMOEDA;





Function  TObjMOEDA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.NOME:=fieldbyname('NOME').asstring;
        Self.MOEDAPADRAO:=fieldbyname('MoedaPadrao').AsString;
        Self.SIMBOLOMOEDAPADRAO:=fieldbyname('SIMBOLOMOEDAPADRAO').AsString;
        result:=True;
     End;
end;


Procedure TObjMOEDA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NOME').asstring:=Self.NOME;
        ParamByName('MOEDAPADRAO').AsString:=Self.MOEDAPADRAO;
        ParamByName('SIMBOLOMOEDAPADRAO').AsString:=Self.SIMBOLOMOEDAPADRAO;
  End;
End;

//***********************************************************************

function TObjMOEDA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjMOEDA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NOME:='';
        MOEDAPADRAO:='';
        SIMBOLOMOEDAPADRAO:='';
//CODIFICA ZERARTABELA


     End;
end;

Function TObjMOEDA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjMOEDA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjMOEDA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjMOEDA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjMOEDA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjMOEDA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MOEDA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select * from  TABMOEDA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjMOEDA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjMOEDA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjMOEDA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjMOEDA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABMOEDA(CODIGO,NOME,MOEDAPADRAO,SIMBOLOMOEDAPADRAO)');
                InsertSQL.add('values (:CODIGO,:NOME,:MOEDAPADRAO,:SIMBOLOMOEDAPADRAO)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABMOEDA set CODIGO=:CODIGO,NOME=:NOME, MOEDAPADRAO=:MOEDAPADRAO, SIMBOLOMOEDAPADRAO=:SIMBOLOMOEDAPADRAO');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABMOEDA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjMOEDA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjMOEDA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabMOEDA');
     Result:=Self.ParametroPesquisa;
end;

function TObjMOEDA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de MOEDA ';
end;


function TObjMOEDA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENMOEDA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENMOEDA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjMOEDA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjMOEDA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjMOEDA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjMOEDA.Submit_CODIGO(parametro: String);
begin
        Self.CODIGO:=Parametro;
end;
function TObjMOEDA.Get_CODIGO: String;
begin
        Result:=Self.CODIGO;
end;
procedure TObjMOEDA.Submit_NOME(parametro: String);
begin
        Self.NOME:=Parametro;
end;
function TObjMOEDA.Get_NOME: String;
begin
        Result:=Self.NOME;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjMOEDA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJMOEDA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjMOEDA.Get_MoedaPadrao: String;
begin
    Result:=Self.MOEDAPADRAO;
end;

function TObjMOEDA.Get_SimboloMoedaPadrao: String;
begin
    Result:=Self.SIMBOLOMOEDAPADRAO;
end;

procedure TObjMOEDA.Submit_MoedaPadrao(parametro: String);
begin
    Self.MOEDAPADRAO:=parametro;
end;

procedure TObjMOEDA.Submit_SimboloMoedaPadrao(parametro: String);
begin
    Self.SIMBOLOMOEDAPADRAO:=parametro;
end;

function TObjMOEDA.ResgataMoedaPadrao: boolean;
begin
    With Self.Objquery do
    Begin
         Close;
         SQL.Clear;
         SQL.Add('Select * from TabMoeda Where MoedaPadrao = ''S'' ');
         Open;

         if (RecordCount = 0)
         then Result:=false
         else Result:=true;
    end;
end;


function TObjMOEDA.LocalizaNome(Parametro: String): boolean;
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MOEDA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select * from  TABMOEDA');
           SQL.ADD(' WHERE nome='+#39+parametro+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

Function TObjMoeda.RetornaNomeMoedaPadrao:String;
Begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select Nome from  TABMOEDA');
           SQL.ADD(' WHERE MoedaPadrao = ''S'' ');
           Open;

           Result:=fieldbyname('Nome').asString;
       End;

end;


Function TObjMoeda.AtualizaMoedaPadrao(PCODIGO:String):Boolean;
Begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           SQL.Clear;
           SQL.Add('Update TabMoeda Set MoedaPadrao = ''N''  Where Codigo <> '+PCODIGO);
           try
               ExecSQL;
           except
               exit;
           end;
     end;
     Result:=true;
end;

Function TObjMoeda.CriaPrimeiraCotacao(PCodigo:String):Boolean;
Var ObjCotacaoMoeda:TOBJCotacaoMoeda;
Begin
try
     Result:=false;
     try
         ObjCotacaoMoeda:=TOBJCotacaoMoeda.create;
     except
         MensagemErro('Erro ao tentar criar o Objeto Cotacao Moeda');
         exit;
     end;

     ObjCotacaoMoeda.ZerarTabela;
     ObjCotacaoMoeda.Status:=dsInsert;
     ObjCotacaoMoeda.Submit_CODIGO('0');
     ObjCotacaoMoeda.MOEDA.Submit_CODIGO(PCodigo);
     ObjCotacaoMoeda.Submit_VALOR('0');
     ObjCotacaoMoeda.Submit_DATA(DateToStr(Now));
     if (ObjCotacaoMoeda.Salvar(true)=false)
     then exit;   

     Result:=true;
finally
     ObjCotacaoMoeda.Free;
end;     
end;
end.





