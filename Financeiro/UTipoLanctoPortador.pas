unit UTipoLanctoPortador;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjTipoLanctoPortador;

type
  TFtipolanctoportador = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    BtNovo: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btpesquisar: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label18: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    combodebito_credito: TComboBox;
    edtCodigoPlanodeContas: TMaskEdit;
    combolancatitulo: TComboBox;
    comboImprimeRecibo: TComboBox;
    lbplanodeContas: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtCodigoPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoPlanodeContasKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure edtCodigoPlanodeContasExit(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function atualizaquantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Ftipolanctoportador: TFtipolanctoportador;
  ObjTipoLanctoPortador:TObjTipoLanctoPortador;

implementation

uses UessencialGlobal, Upesquisa, UPLanodeContas, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFtipolanctoportador.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjTipoLanctoPortador do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Historico       ( edthistorico.text);
         Submit_Debito_Credito  ( combodebito_credito.text[1]);
         Submit_codigoplanodecontas(edtcodigoplanodecontas.text);
         Submit_LancaTitulo(combolancatitulo.text[1]);
         Submit_ImprimeRecibo(Submit_ComboBox(comboImprimeRecibo));


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFtipolanctoportador.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjTipoLanctoPortador do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edthistorico.text       :=Get_Historico        ;
        if Get_Debito_Credito='D'
        Then combodebito_credito.itemindex:=0
        Else combodebito_credito.itemindex:=1;

        if Get_LancaTitulo='S'
        Then combolancatitulo.itemindex:=0
        Else combolancatitulo.itemindex:=1;

        edtcodigoplanodecontas.text:=Get_codigoplanodecontas;

        if (Get_ImprimeRecibo='S')
        Then comboImprimeRecibo.ItemIndex:=1
        Else comboImprimeRecibo.ItemIndex:=0;

        if ObjTipoLanctoPortador.PlanodeContas.LocalizaCodigo(Get_CodigoPlanodecontas)
        then begin
              ObjTipoLanctoPortador.PlanodeContas.TabelaparaObjeto;
              lbplanodeContas.Caption:=ObjTipoLanctoPortador.PlanodeContas.Get_Nome;
        end
        else lbplanodeContas.Caption:='';

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFtipolanctoportador.TabelaParaControles: Boolean;
begin
     ObjTipoLanctoPortador.TabelaparaObjeto;
     ObjetoParaControles;
end;

procedure TFtipolanctoportador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjTipoLanctoPortador=Nil)
     Then exit;

    If (ObjTipoLanctoPortador.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjTipoLanctoPortador.free;
end;

procedure TFtipolanctoportador.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFtipolanctoportador.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
          Perform(Wm_NextDlgCtl,0,0);
          //key:=#0;
      end;
end;


procedure TFtipolanctoportador.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);
     lbplanodeContas.Caption:='';

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjTipoLanctoPortador.Get_novocodigo;
     edtcodigo.enabled:=False;
     Btgravar.visible:=True;
     BtCancelar.visible:=True;

     ObjTipoLanctoPortador.status:=dsInsert;
     Edthistorico.setfocus;
end;

procedure TFtipolanctoportador.BtCancelarClick(Sender: TObject);
begin
     ObjTipoLanctoPortador.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);
     lbplanodeContas.caption:='';
end;

procedure TFtipolanctoportador.BtgravarClick(Sender: TObject);
begin

     If ObjTipoLanctoPortador.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjTipoLanctoPortador.salvar(true)=False)
     Then exit;


     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     Self.TabelaParaControles;

     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFtipolanctoportador.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     EdtCodigo.enabled:=False;
     ObjTipoLanctoPortador.Status:=dsEdit;
     edthistorico.setfocus;
     combodebito_credito.Enabled:=False;
end;

procedure TFtipolanctoportador.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjTipoLanctoPortador.Get_pesquisa,ObjTipoLanctoPortador.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjTipoLanctoPortador.status<>dsinactive
                                  then exit;

                                  If (ObjTipoLanctoPortador.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFtipolanctoportador.btalterarClick(Sender: TObject);
begin
    If (ObjTipoLanctoPortador.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFtipolanctoportador.btexcluirClick(Sender: TObject);
begin
     If (ObjTipoLanctoPortador.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjTipoLanctoPortador.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjTipoLanctoPortador.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFtipolanctoportador.edtCodigoPlanodeContasKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeContas:TFplanodeContas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FplanodeContas:=TfplanodeContas.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjTipoLanctoPortador.PlanodeContas.Get_Pesquisa,ObjTipoLanctoPortador.PlanodeContas.Get_TituloPesquisa,FplanodeContas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtCodigoPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeContas);
     End;


end;

procedure TFtipolanctoportador.edtCodigoPlanodeContasKeyPress(
  Sender: TObject; var Key: Char);
begin
     If not(key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFtipolanctoportador.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);
     lbplanodeContas.Caption:='';

     Try
        ObjTipoLanctoPortador:=TObjTipoLanctoPortador.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

function TFtipolanctoportador.atualizaquantidade: string;
begin
     result:='Existem '+ContaRegistros('tabtipolanctoportador', 'codigo')+' Tipos de Lan�amento em Portador Cadastrados';
end;

procedure TFtipolanctoportador.edtCodigoPlanodeContasExit(Sender: TObject);
begin
     if edtCodigoPlanodeContas.Text <> ''
     then begin
            if ObjTipoLanctoPortador.PlanodeContas.LocalizaCodigo(edtCodigoPlanodeContas.Text)
            then begin
                    ObjTipoLanctoPortador.PlanodeContas.TabelaparaObjeto;
                    lbplanodeContas.Caption:=ObjTipoLanctoPortador.PlanodeContas.Get_Nome;
            end
            else MensagemErro('C�digo do plano de contas n�o encontrado!');
     end;
end;

end.
