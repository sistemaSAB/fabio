object FrValorLanctoLote: TFrValorLanctoLote
  Left = 0
  Top = 0
  Width = 501
  Height = 89
  TabOrder = 0
  object paneljuros: TPanel
    Left = 0
    Top = 1
    Width = 498
    Height = 86
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object label150: TLabel
      Left = 8
      Top = 3
      Width = 147
      Height = 13
      Caption = 'Saldo da Pend'#234'ncia R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 20
      Width = 161
      Height = 13
      Caption = 'Taxa de Juros(%) ao dia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 8
      Top = 37
      Width = 138
      Height = 13
      Caption = 'N'#186' de dias em Atraso'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object PJ_lbSaldoPendencia: TLabel
      Left = 176
      Top = 3
      Width = 71
      Height = 13
      Caption = '585,5650,00'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object PJ_lbTaxaDia: TLabel
      Left = 176
      Top = 20
      Width = 71
      Height = 13
      Caption = '585,5650,00'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object PJ_lbdiaatraso: TLabel
      Left = 176
      Top = 37
      Width = 71
      Height = 13
      Caption = '585,5650,00'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label9: TLabel
      Left = 8
      Top = 71
      Width = 125
      Height = 13
      Caption = 'Saldo com juros R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object PJ_lbSaldoComJuros: TLabel
      Left = 176
      Top = 71
      Width = 71
      Height = 13
      Caption = '585,5650,00'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label11: TLabel
      Left = 316
      Top = 51
      Width = 180
      Height = 13
      Caption = 'Car'#234'ncia                        dias'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 316
      Top = 27
      Width = 76
      Height = 13
      Caption = 'Vencimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Shape2: TShape
      Left = 308
      Top = 2
      Width = 1
      Height = 82
    end
    object pj_vencimento: TLabel
      Left = 407
      Top = 27
      Width = 71
      Height = 13
      Caption = '585,5650,00'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 316
      Top = 3
      Width = 77
      Height = 13
      Caption = 'Data Lancto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 54
      Width = 92
      Height = 13
      Caption = 'Valor juros R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object PJ_lbValorJuros: TLabel
      Left = 176
      Top = 54
      Width = 71
      Height = 13
      Caption = '585,5650,00'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
    end
    object btLancaJuros: TSpeedButton
      Left = 256
      Top = 51
      Width = 18
      Height = 18
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        E0EEE17BB47ECDE3CEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF83AF94247240196B
        3724724083AF94FFFFFFFFFFFFFCFDFC95C59861A7666BAE6F4F9953DBEADCE7
        F1E7FFFFFF87B297288C5364BA8D95D2B264BA8D288C5380AD90FFFFFFE9F3EA
        7DBC82B7DEBB67AC6C75B67A4E985182B785AACEAC22713F62BA8B60BA87FFFF
        FF60B98767BC8F20703DFFFFFFABD3AE9CCDA06FB2738DC792AADCAF76B67B51
        9B5577B77B317B4C9CD4B6FFFFFFFFFFFFFFFFFF95D2B2196B37F2F9F37BBB80
        77B77C91CB97ABDEB19CD7A2AADDB077B77C60AC65478A6090D3B192D6B1FFFF
        FF65BC8C67BC8F20703DAED5B17FBD8497CE9CADDFB36FB37496D59D9DD8A3AA
        DDB078B87C61A46F61AB8195D4B4BAE6D06ABB8F2D8F5770A4807BBB808EC893
        AFDFB5A1DAA798D79F97D69E7EC08382C187ABDDB079B97D5EA16B5E98734F8E
        6646895E3F894EF3F8F3C7E5CA7DBB828FC894B0E0B6A2DAA87FC185A4D0A7DD
        EEDF80B883ABDEB17AB97F569F5AC4E7C878B87CA3CAA5FFFFFFFFFFFFEFF7EF
        7FBD8490C995B0E0B685C28AF7FCF895C297DDEEDF82C287ABDEB17BBA7F58A0
        5C59A15DFCFDFCFFFFFFFFFFFFFFFFFFFFFFFF82BF8791CA96B1E0B6D9F3DDF7
        FCF8A4D0A77EC0849FD9A5ACDEB27BBB8059A15DFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFF84C18A92CB97B1E1B685C38A80C18599D7A098D79F9FD9A5ACDF
        B27DBB815DA361FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF86C28B93CC98B1
        E1B7A3DBA99BD8A273B477AFDFB487C38C65AA69F5FAF6FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFF87C28C94CC99B2E2B7A3DCAAB0E0B68CC6926EB1
        73F6FAF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF88
        C38D94CD9AB3E2B793CB9877B77CF7FAF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF89C48F96CD9B80BE85F7FBF7FFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFF8BC590F8FBF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object btHistorico: TSpeedButton
      Left = 283
      Top = 51
      Width = 18
      Height = 18
      Flat = True
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFCCCCCCCCCCCCF4F4F4FFFFFFDCDCDCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
        CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC396EA13B6E9EAEB9C3FFFFFFC09F7A
        B88956B88A57B88955B68550B6854EB98751BD8B54BF8C55C08D55CA8F4F2C6C
        A929B2FF44C8FF3A83CCCCCCCCBC884EFFDFAABC8D58B5844FFFFBEEFFF8EAD4
        CDC574777C5E636D60646D68686B7F736C3AC6FF55DBFF3880CB8D8D90C5CAD1
        57616EC6975EB4834CFFF7E8D1C5B9696869E4C58FFFEDA7FFF5B1E8D5A37671
        6D9D90882A7FD7FFFFFFFFFFFFBF894DAB7A3ECB9F65B4824CFFF8E978777AE3
        BF86FFE6A5FFE7A6FFEFB3FFF9BBE8D6A378787BC88C4BFFFFFFCCCCCCBD874C
        FFDFA4D0A568B4824BFFF9EA6A6D73FFE19EFFEFCAFFE7B3FFE9ABFFEFB2FFF4
        B071757DBF8C53FFFFFF8E8D8FC5C9D1565F6DD9AE6FB4814BFFF9EB707278FF
        DD98FFF7E4FFEDC8FFE7B2FFE6A5FFEBA5757982BD8B54FFFFFFFFFFFFBF884D
        A67B3ADEB576B2804AFFF9EA868585E7BC7DFFF5DCFFF7E4FFEECAFFE5A4E8C9
        938B8E93BB8952FFFFFFCCCCCCBD874CFFE09EE3BB7AB17F49FFF7EAD3C0AC82
        8181E8BC7EFFDC97FFDF9CE8C48B838284DED7D0B88750FFFFFF8E8D90C5C9D1
        555D6CEBC47FB17E49FFF6EBF7D9B9D9D1C992959B81848B80848B8F8E8FD4C0
        AAFFFBEFB5844DFFFFFFFFFFFFBF884DA57C35ECC581B07E49FFF8EEF2D4B2F5
        D8B6F9DCBBFADDBCF9DCBBF7D9B8F4D6B4FFFAF0B4834CFFFFFFCCCCCCBD874C
        F9D08BEBC47FB07D48FFFBF2EFD1ADF1D5B2AC7940D0A87BCEA679F1D4B1EFD1
        ADFFFBF3B4824CFFFFFF8D8D90C3C8CF525A67EBC37BB07D46FFFDF8EECCA6EF
        CEA9F0D1ACF0D1ACF0CFABEECDA8EECCA6FFFEF9B4834CFFFFFFFFFFFFBC864B
        FFF2D4FEF1D4B27F49FFFFFFFFFEFAFFFEFAFFFFFBFFFFFBFFFEFAFFFEFAFFFE
        FAFFFFFFB5844FFFFFFFFFFFFFD0AF8AB78752B68652B78752B5844EB4824CB4
        824BB4824BB4824BB4824BB4824BB4824CB5844FCEAD89FFFFFF}
    end
    object UpDown: TUpDown
      Left = 454
      Top = 48
      Width = 15
      Height = 19
      Associate = edtcarencia
      TabOrder = 0
      OnChangingEx = UpDownChangingEx
    end
    object edtcarencia: TMaskEdit
      Left = 407
      Top = 48
      Width = 47
      Height = 19
      TabStop = False
      TabOrder = 1
      Text = '0'
      OnExit = edtcarenciaExit
      OnKeyPress = edtcarenciaKeyPress
    end
    object edtdatalancamento: TMaskEdit
      Left = 407
      Top = 3
      Width = 72
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
  end
end
