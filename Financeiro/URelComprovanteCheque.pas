unit URelComprovanteCheque;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, Db, IBCustomDataSet, IBQuery;

type
  TFrelComprovanteCheque = class(TForm)
    QR: TQuickRep;
    QRBand1: TQRBand;
    LbValor: TQRLabel;
    LbExtenso1: TQRLabel;
    LbExtenso2: TQRLabel;
    LbNominal: TQRLabel;
    LbCidadeCheque: TQRLabel;
    LbDiaCheque: TQRLabel;
    LbMesCheque: TQRLabel;
    LbAnoCheque: TQRLabel;
    Lbcomprovante: TQRLabel;
    LbNumeroCheque: TQRLabel;
    LbBanco: TQRLabel;
    LbAgencia: TQRLabel;
    LbConta: TQRLabel;
    LbProvidencias: TQRLabel;
    LbValorComprovante: TQRLabel;
    LbExtensoComprovante: TQRLabel;
    LbDescricaoRecibo: TQRLabel;
    LbCidadeComprovante: TQRLabel;
    LbDiaComprovante: TQRLabel;
    LbMEsComprovante: TQRLabel;
    LbAnoComprovante: TQRLabel;
    LbNomeRecebedor: TQRLabel;
    lbCPFCNPJrecebedor: TQRLabel;
    MemoDescricaoComprovante: TQRMemo;
    LBCONFERIDO: TQRLabel;
    lbdataconferido: TQRLabel;
    LBLINHACONFERIDO: TQRLabel;
    LBASSINATURACONFERIDO: TQRLabel;
    IBQuery1: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrelComprovanteCheque: TFrelComprovanteCheque;

implementation

{$R *.DFM}

end.
