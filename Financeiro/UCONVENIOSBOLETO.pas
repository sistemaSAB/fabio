unit UCONVENIOSBOLETO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjCONVENIOSBOLETO,FileCtrl;

type
  TFCONVENIOSBOLETO = class(TForm)
    Guia: TTabbedNotebook;
    Label11: TLabel;
    MemoSacado: TMemo;
    Bevel1: TBevel;
    Label10: TLabel;
    Label12: TLabel;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    Btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    LbCODIGO: TLabel;
    Label6: TLabel;
    Lbnomeportador: TLabel;
    EdtCODIGO: TEdit;
    edtportador: TEdit;
    LbNome: TLabel;
    LbNomeBanco: TLabel;
    LbCodigoBAnco: TLabel;
    LbDgBanco: TLabel;
    LbLocalPagamento: TLabel;
    LbNomecedente: TLabel;
    LbCarteira: TLabel;
    LbCompCarteira: TLabel;
    LbAgencia: TLabel;
    LbDGAgencia: TLabel;
    LbContaCorrente: TLabel;
    LbDgContaCorrente: TLabel;
    Lbnumeroconvenio: TLabel;
    LbEspecieDoc: TLabel;
    LbAceite: TLabel;
    LbNumContaResponsavel: TLabel;
    LbEspecieMoeda: TLabel;
    LbQuantidadeMoeda: TLabel;
    LbValorMoeda: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdtNome: TEdit;
    EdtNomeBanco: TEdit;
    EdtCodigoBAnco: TEdit;
    EdtDgBanco: TEdit;
    EdtLocalPagamento: TEdit;
    EdtNomecedente: TEdit;
    EdtCarteira: TEdit;
    EdtCompCarteira: TEdit;
    EdtAgencia: TEdit;
    EdtDGAgencia: TEdit;
    EdtContaCorrente: TEdit;
    EdtDgContaCorrente: TEdit;
    Edtnumeroconvenio: TEdit;
    EdtEspecieDoc: TEdit;
    EdtAceite: TEdit;
    EdtNumContaResponsavel: TEdit;
    EdtEspecieMoeda: TEdit;
    EdtQuantidadeMoeda: TEdit;
    EdtValorMoeda: TEdit;
    Memoinstrucoes: TMemo;
    edttaxamensal: TEdit;
    edtquantdiasprotesto: TEdit;
    edttaxabanco: TEdit;
    edtcodigocedente: TEdit;
    edtQtdeDigitosNossoNumero: TEdit;
    LboxVariaveis: TListBox;
    ImagemFundo: TImage;
    Label2: TLabel;
    checkpreimpresso: TCheckBox;
    edtCodigoSICOB_caixa: TEdit;
    edtCodigoCarteira_Caixa: TEdit;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label18: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    edtposicao20_sicred: TEdit;
    edtposicao31_34_sicred: TEdit;
    edtposicao35_36_sicred: TEdit;
    edtposicao21_sicred: TEdit;
    edtposicao37_41_sicred: TEdit;
    edtposicao42_sicred: TEdit;
    edtposicao43_sicred: TEdit;
    Label13: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label28: TLabel;
    edtcodigotransacao_unibanco: TEdit;
    edtnumerocliente_UNIBANCO: TEdit;
    edtposicoes_28_a_29_UNIBANCO: TEdit;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    edtTipoIdentificador_HSBC: TEdit;
    edtCodigoCedente_HSBC: TEdit;
    edtCodigoCNR_HSBC: TEdit;
    lbvariaveissacado: TListBox;
    Label14: TLabel;
    edtBYTE_NOSSONUMERO_SICRED: TEdit;
    GuiaCobrebem: TTabbedNotebook;
    Label30: TLabel;
    EdtCobreBemCodigoAgencia: TEdit;
    Label29: TLabel;
    EdtCobreBemNumeroContaCorrente: TEdit;
    Label31: TLabel;
    EdtCobreBemCodigoCedente: TEdit;
    Label32: TLabel;
    EdtCobreBemInicioNossoNumero: TEdit;
    Label33: TLabel;
    EdtCobreBemFimNossoNumero: TEdit;
    Label34: TLabel;
    EdtCobreBemProximoNossoNumero: TEdit;
    Label35: TLabel;
    EdtCobreBemOutroDadoConfiguracao1: TEdit;
    Label37: TLabel;
    EdtCobreBemArquivoLicenca: TEdit;
    Label38: TLabel;
    EdtCobreBemDiretorioRemessa: TEdit;
    Label39: TLabel;
    EdtCobreBemDiretorioRetorno: TEdit;
    Label40: TLabel;
    Label41: TLabel;
    edtCobreBemArquivoLogotipo: TEdit;
    EdtCobreBemImagensCodigoBarras: TEdit;
    Label42: TLabel;
    Label43: TLabel;
    EdtCobreBemLayoutRemessa: TEdit;
    EdtCobreBemArquivoRemessa: TEdit;
    Label36: TLabel;
    EdtCobreBemArquivoRetorno: TEdit;
    Label44: TLabel;
    EdtCobreBemLayoutRetorno: TEdit;
    Label45: TLabel;
    CheckCobreBem: TCheckBox;
    btOpenDialogArquivoLicenca: TSpeedButton;
    btOpenDialogArquivoLogotipo: TSpeedButton;
    btOpenDialogImagensCodigoBarras: TSpeedButton;
    btOpenDialogDiretorioRemessa: TSpeedButton;
    btOpenDialogDiretorioRetorno: TSpeedButton;
    OpenDialog: TOpenDialog;
    LayoutBoleto: TLabel;
    ComboCobreBemLayOutBoleto: TComboBox;
    Label46: TLabel;
    EdtPercentualJurosDiasAtraso: TEdit;
    label90: TLabel;
    EdtPercentualMultaAtraso: TEdit;
    CobreBemPercentualDesconto: TLabel;
    EdtPercentualDesconto: TEdit;
    Label47: TLabel;
    ComboCobreBemBancoImprimeBoleto: TComboBox;
    Label48: TLabel;
    EdtCobreBemDiasProtesto: TEdit;
    Label49: TLabel;
    EdtLimiteVencimento: TEdit;
    Label50: TLabel;
    EdtCobreBemNumeroRemessa: TEdit;
    Label51: TLabel;
    EdtCobreBemOutroDadoConfiguracao2: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorExit(Sender: TObject);
    procedure BtopcoesClick(Sender: TObject);
    procedure LboxVariaveisDblClick(Sender: TObject);
    procedure lbvariaveissacadoDblClick(Sender: TObject);
    procedure btOpenDialogArquivoLicencaClick(Sender: TObject);
    procedure btOpenDialogArquivoLogotipoClick(Sender: TObject);
    procedure btOpenDialogImagensCodigoBarrasClick(Sender: TObject);
    procedure btOpenDialogDiretorioRemessaClick(Sender: TObject);
    procedure btOpenDialogDiretorioRetornoClick(Sender: TObject);
    procedure ComboCobreBemBancoImprimeBoletoKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         function  atualizaQuantidade:string;
         procedure controlChange(sender: TObject);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCONVENIOSBOLETO: TFCONVENIOSBOLETO;
  ObjCONVENIOSBOLETO:TObjCONVENIOSBOLETO;

implementation

uses UessencialGlobal, Upesquisa, UobjPortador, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCONVENIOSBOLETO.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjCONVENIOSBOLETO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
        Submit_NomeBanco(edtNomeBanco.text);
        Submit_CodigoBAnco(edtCodigoBAnco.text);
        Submit_DgBanco(edtDgBanco.text);
        Submit_LocalPagamento(edtLocalPagamento.text);
        Submit_Nomecedente(edtNomecedente.text);
        Submit_Carteira(edtCarteira.text);
        Submit_CompCarteira(edtCompCarteira.text);
        Submit_Agencia(edtAgencia.text);
        Submit_DGAgencia(edtDGAgencia.text);
        Submit_ContaCorrente(edtContaCorrente.text);
        Submit_DgContaCorrente(edtDgContaCorrente.text);
        Submit_numeroconvenio(edtnumeroconvenio.text);
        Submit_EspecieDoc(edtEspecieDoc.text);
        Submit_Aceite(edtAceite.text);
        Submit_NumContaResponsavel(edtNumContaResponsavel.text);
        Submit_EspecieMoeda(edtEspecieMoeda.text);
        Submit_QuantidadeMoeda(edtQuantidadeMoeda.text);
        Submit_ValorMoeda(edtValorMoeda.text);
        Submit_Instrucoes(Memoinstrucoes.text);
        Submit_taxamensal(edttaxamensal.text);
        Submit_Quantdiasprotesto(edtquantdiasprotesto.text);
        Submit_TaxaBanco(Edttaxabanco.Text);
        Portador.Submit_CODIGO(edtportador.Text);
        Submit_codigocedente(edtcodigocedente.text);
        Submit_QtdeDigitosNossoNumero(edtQtdeDigitosNossoNumero.text);

        if checkpreimpresso.Checked=True //mauricio 29/7/2009
        then Submit_PreImpresso('S')
        else Submit_PreImpresso('N');

        Submit_Sacado(memosacado.Text);
        Submit_CodigoSICOB_caixa(edtCodigoSICOB_caixa.text);
        Submit_CodigoCarteira_Caixa(edtCodigoCarteira_Caixa.text);

        Submit_codigotransacao_unibanco(edtcodigotransacao_unibanco.text);
        Submit_posicoes_28_a_29_UNIBANCO(edtposicoes_28_a_29_UNIBANCO.text);
        Submit_numerocliente_UNIBANCO(edtnumerocliente_UNIBANCO.text);

        //***sicred****

        submit_posicao20_sicred(edtposicao20_sicred.text);
        submit_posicao21_sicred(edtposicao21_sicred.text);
        submit_posicao31_34_sicred(edtposicao31_34_sicred.text);
        submit_posicao35_36_sicred(edtposicao35_36_sicred.text);
        submit_posicao37_41_sicred(edtposicao37_41_sicred.text);
        submit_posicao42_sicred(edtposicao42_sicred.text);
        submit_posicao43_sicred(edtposicao43_sicred.text);
        Submit_BYTE_NOSSONUMERO_SICRED(edtBYTE_NOSSONUMERO_SICRED.Text);
        //*****HSBC*****

        Submit_codigocedente_HSBC(edtCodigoCedente_HSBC.Text);
        Submit_codigoCNR_HSBC(edtCodigoCNR_HSBC.Text);
        Submit_tipoidentificador_HSBC(edtTipoIdentificador_HSBC.Text);


        //****** Cobrebem ******
        if (CheckCobreBem.Checked = true)
        then Submit_CobreBem('S')
        else Submit_CobreBem('N');

        Submit_CobreBemCodigoAgencia(EdtCobreBemCodigoAgencia.Text);
        Submit_CobreBemNumeroContaCorrente(EdtCobreBemNumeroContaCorrente.Text);
        //Submit_CobreBemCodigoCedente(CompletaPalavra_a_Esquerda(EdtCobreBemCodigoCedente.Text,9,'0'));

        Submit_CobreBemCodigoCedente(EdtCobreBemCodigoCedente.Text);

        Submit_CobreBemInicioNossoNumero(EdtCobreBemInicioNossoNumero.Text);
        Submit_CobreBemFimNossoNumero(EdtCobreBemFimNossoNumero.Text);
        Submit_CobreBemProximoNossoNumero(EdtCobreBemProximoNossoNumero.Text);
        Submit_CobreBemOutroDadoConfiguracao1(EdtCobreBemOutroDadoConfiguracao1.Text);
        Submit_CobreBemOutroDadoConfiguracao2(EdtCobreBemOutroDadoConfiguracao2.Text);
        Submit_CobreBemArquivoLicenca(EdtCobreBemArquivoLicenca.Text);
        Submit_CobreBemArquivoLogotipo(edtCobreBemArquivoLogotipo.Text);
        Submit_CobreBemCaminhoImagensCodBarras(EdtCobreBemImagensCodigoBarras.Text);
        Submit_CobreBemDiretorioRemessa(EdtCobreBemDiretorioRemessa.Text);
        Submit_CobreBemArquivoRemessa(EdtCobreBemArquivoRemessa.Text);
        Submit_CobreBemLayoutRemessa(EdtCobreBemLayoutRemessa.Text);
        Submit_CobreBemDiretorioRetorno(EdtCobreBemDiretorioRetorno.Text);
        Submit_CobreBemArquivoRetorno(EdtCobreBemArquivoRetorno.Text);
        Submit_CobreBemLayoutRetorno(EdtCobreBemLayoutRetorno.Text);
        Submit_CobreBemLayoutBoleto(ComboCobreBemLayOutBoleto.Text);
        Submit_CobreBemPercJurosDiaAtraso(EdtPercentualJurosDiasAtraso.Text);
        Submit_CobreBemPercMultaAtraso(EdtPercentualMultaAtraso.Text);
        Submit_CobreBemPercentualDesconto(EdtPercentualDesconto.Text);
        
        if (ComboCobreBemBancoImprimeBoleto.ItemIndex = 0)
        then Submit_CobrebemBancoImprimeBoleto('S')
        else Submit_CobrebemBancoImprimeBoleto('N');

        Submit_CobreBemDiasProtesto(EdtCobreBemDiasProtesto.Text);
        Submit_CobreBemLimiteVencimento(EdtLimiteVencimento.Text);
        Submit_CobreBemNumeroRemessa(EdtCobreBemNumeroRemessa.Text);

        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCONVENIOSBOLETO.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjCONVENIOSBOLETO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtNomeBanco.text:=Get_NomeBanco;
        EdtCodigoBAnco.text:=Get_CodigoBAnco;
        EdtDgBanco.text:=Get_DgBanco;
        EdtLocalPagamento.text:=Get_LocalPagamento;
        EdtNomecedente.text:=Get_Nomecedente;
        EdtCarteira.text:=Get_Carteira;
        EdtCompCarteira.text:=Get_CompCarteira;
        EdtAgencia.text:=Get_Agencia;
        EdtDGAgencia.text:=Get_DGAgencia;
        EdtContaCorrente.text:=Get_ContaCorrente;
        EdtDgContaCorrente.text:=Get_DgContaCorrente;
        Edtnumeroconvenio.text:=Get_numeroconvenio;
        EdtEspecieDoc.text:=Get_EspecieDoc;
        EdtAceite.text:=Get_Aceite;
        EdtNumContaResponsavel.text:=Get_NumContaResponsavel;
        EdtEspecieMoeda.text:=Get_EspecieMoeda;
        EdtQuantidadeMoeda.text:=Get_QuantidadeMoeda;
        EdtValorMoeda.text:=Get_ValorMoeda;
        Memoinstrucoes.text:=Get_Instrucoes;
        edttaxamensal.text:=Get_taxamensal;
        edtquantdiasprotesto.Text:=Get_Quantdiasprotesto;
        edttaxabanco.Text:=Get_TaxaBanco;
        edtportador.Text:=Portador.Get_CODIGO;
        edtQtdeDigitosNossoNumero.text:=get_QtdeDigitosNossoNumero;
        edtcodigocedente.text:=get_codigocedente;
        //combopreimpresso.ItemIndex:=0;

        if Get_PreImpresso='S'
        then checkpreimpresso.Checked:=True //combopreimpresso.ItemIndex:=1;
        else checkpreimpresso.Checked:=False;

        memoSacado.text:=Get_Sacado;
        edtCodigoSICOB_caixa.text:=Get_CodigoSICOB_caixa;
        edtCodigoCarteira_Caixa.text:=Get_CodigoCarteira_Caixa;
        edtcodigotransacao_unibanco.text:=get_codigotransacao_unibanco;
        edtposicoes_28_a_29_UNIBANCO.text:=get_posicoes_28_a_29_UNIBANCO;
        edtnumerocliente_UNIBANCO.text:=Get_numerocliente_UNIBANCO;
        edtposicao20_sicred.text:=get_posicao20_sicred;
        edtposicao21_sicred.text:=get_posicao21_sicred;
        edtposicao31_34_sicred.text:=get_posicao31_34_sicred;
        edtposicao35_36_sicred.text:=get_posicao35_36_sicred;
        edtposicao37_41_sicred.text:=get_posicao37_41_sicred;
        edtposicao42_sicred.text:=get_posicao42_sicred;
        edtposicao43_sicred.text:=get_posicao43_sicred;
        edtBYTE_NOSSONUMERO_SICRED.text:=Get_BYTE_NOSSONUMERO_SICRED;
        //****HSBC***********

        edtCodigoCedente_HSBC.Text := get_codigocedente_HSBC;
        edtCodigoCNR_HSBC.Text := get_codigoCNR_HSBC;
        edtTipoIdentificador_HSBC.Text := get_tipoidentificador_HSBC;



        //********** CobreBem *********************************
        if (Get_CobreBem = 'S')
        then CheckCobreBem.Checked:=true
        else CheckCobreBem.Checked:=false;

        EdtCobreBemCodigoAgencia.Text:=Get_CobreBemCodigoAgencia;
        EdtCobreBemNumeroContaCorrente.Text:=Get_CobreBemNumeroContaCorrente;
        EdtCobreBemCodigoCedente.Text:=Get_CobreBemCodigoCedente;
        EdtCobreBemInicioNossoNumero.Text:=Get_CobreBemInicioNossoNumero;
        EdtCobreBemFimNossoNumero.Text:=Get_CobreBemFimNossoNumero;
        EdtCobreBemProximoNossoNumero.Text:=Get_CobreBemProximoNossoNumero;
        EdtCobreBemOutroDadoConfiguracao1.Text:=Get_CobreBemOutroDadoConfiguracao1;
        EdtCobreBemOutroDadoConfiguracao2.Text:=Get_CobreBemOutroDadoConfiguracao2;
        EdtCobreBemArquivoLicenca.Text:=Get_CobreBemArquivoLicenca;
        edtCobreBemArquivoLogotipo.Text:=Get_CobreBemArquivoLogotipo;
        EdtCobreBemImagensCodigoBarras.Text:=Get_CobreBemCaminhoImagensCodBarras;
        EdtCobreBemDiretorioRemessa.Text:=Get_CobreBemDiretorioRemessa;
        EdtCobreBemArquivoRemessa.Text:=Get_CobreBemArquivoRemessa;
        EdtCobreBemLayoutRemessa.Text:=Get_CobreBemLayoutRemessa;
        EdtCobreBemDiretorioRetorno.Text:=Get_CobreBemDiretorioRetorno;
        EdtCobreBemArquivoRetorno.Text:=Get_CobreBemArquivoRetorno;
        EdtCobreBemLayoutRetorno.Text:=Get_CobreBemLayoutRetorno;
        ComboCobreBemLayOutBoleto.Text:=Get_CobreBemLayoutBoleto;
        EdtPercentualJurosDiasAtraso.Text:=Get_CobreBemPercJurosDiaAtraso;
        EdtPercentualDesconto.Text:=Get_CobreBemPercentualDesconto;
        EdtPercentualMultaAtraso.Text:=Get_CobreBemPercMultaAtraso;

        if (Get_CobreBemBancoImprimeBoleto = 'S')
        then ComboCobreBemBancoImprimeBoleto.ItemIndex:=0
        else ComboCobreBemBancoImprimeBoleto.ItemIndex:=1;

        EdtCobreBemDiasProtesto.Text:=Get_CobreBemDiasProtesto;
        EdtLimiteVencimento.Text:=Get_CobreBemLimiteVencimento;
        EdtCobreBemNumeroRemessa.Text:=Get_CobreBemNumeroRemessa;
        //*******************

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCONVENIOSBOLETO.TabelaParaControles: Boolean;
begin
     If (ObjCONVENIOSBOLETO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCONVENIOSBOLETO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCONVENIOSBOLETO=Nil)
     Then exit;

      If (ObjCONVENIOSBOLETO.status<>dsinactive)
      Then Begin
                Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
                abort;
                exit;
      End;

      ObjCONVENIOSBOLETO.free;
      Screen.OnActiveControlChange:=nil;
end;

procedure TFCONVENIOSBOLETO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCONVENIOSBOLETO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     checkpreimpresso.enabled:=True;
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjCONVENIOSBOLETO.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     ObjCONVENIOSBOLETO.status:=dsInsert;
     Guia.pageindex:=0;
     Edtportador.setfocus;

end;


procedure TFCONVENIOSBOLETO.btalterarClick(Sender: TObject);
begin
    If (ObjCONVENIOSBOLETO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                checkpreimpresso.enabled:=True;
                EdtCodigo.enabled:=False;
                ObjCONVENIOSBOLETO.Status:=dsEdit;
                guia.pageindex:=0;
                edtportador.setfocus;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
          End;


end;

procedure TFCONVENIOSBOLETO.btgravarClick(Sender: TObject);
begin

     If ObjCONVENIOSBOLETO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCONVENIOSBOLETO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjCONVENIOSBOLETO.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     checkpreimpresso.enabled:=False;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCONVENIOSBOLETO.btexcluirClick(Sender: TObject);
begin
     If (ObjCONVENIOSBOLETO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjCONVENIOSBOLETO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCONVENIOSBOLETO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCONVENIOSBOLETO.btcancelarClick(Sender: TObject);
begin
     ObjCONVENIOSBOLETO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     checkpreimpresso.enabled:=False;
     mostra_botoes(Self);

end;

procedure TFCONVENIOSBOLETO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCONVENIOSBOLETO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCONVENIOSBOLETO.Get_pesquisa,ObjCONVENIOSBOLETO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCONVENIOSBOLETO.status<>dsinactive
                                  then exit;

                                  If (ObjCONVENIOSBOLETO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCONVENIOSBOLETO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCONVENIOSBOLETO.LimpaLabels;
begin
     lbnomeportador.caption:='';
end;

procedure TFCONVENIOSBOLETO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
     {lbvariaveis.caption:=
     '&&taxamensal'+#13+
     '&&valoraodia'+#13+
     '&&dataprotesto'+#13+
     '&&datavencimento'+#13+
     '&&taxabanco'+#13+
     '&&propriedade';}

     LboxVariaveis.Clear;
     LboxVariaveis.AddItem('&taxamensal',nil);
     LboxVariaveis.AddItem('&valoraodia',nil);
     LboxVariaveis.AddItem('&dataprotesto',nil);
     LboxVariaveis.AddItem('&datavencimento',nil);
     LboxVariaveis.AddItem('&taxabanco',nil);
     LboxVariaveis.AddItem('&propriedade',nil);
     LboxVariaveis.AddItem('&desconto',nil);
     LboxVariaveis.AddItem('&vencimentodesconto',nil);

     lbvariaveissacado.AddItem('&CNPJ',nil);
     lbvariaveissacado.AddItem('&CODIGOCLIENTE',nil);
     lbvariaveissacado.AddItem('&NOMECLIENTE',nil);
     lbvariaveissacado.AddItem('&ENDERECO',nil);
     lbvariaveissacado.AddItem('&NUMEROCASA',nil);
     lbvariaveissacado.AddItem('&BAIRRO',nil);
     lbvariaveissacado.AddItem('&CIDADE',nil);
     lbvariaveissacado.AddItem('&ESTADO',nil);
     lbvariaveissacado.AddItem('&CEP',nil);
     Screen.OnActiveControlChange:=Self.controlChange;

     Guia.PageIndex:=0;
     GuiaCobrebem.PageIndex:=0;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     checkpreimpresso.enabled:=False;
     Guia.PageIndex:=0;

     Try
        ObjCONVENIOSBOLETO:=TObjCONVENIOSBOLETO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
      FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
      FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
      FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
      Self.Color:=clwhite;
      lbquantidade.caption:=atualizaQuantidade;

end;

procedure TFCONVENIOSBOLETO.edtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCONVENIOSBOLETO.edtPortadorKeyDown(sender,key,shift,lbnomeportador);
end;

procedure TFCONVENIOSBOLETO.edtportadorExit(Sender: TObject);
begin
     ObjCONVENIOSBOLETO.edtPortadorExit(sender,Lbnomeportador);
end;

procedure TFCONVENIOSBOLETO.BtopcoesClick(Sender: TObject);
begin
     ObjCONVENIOSBOLETO.opcoes(edtcodigo.text);
end;

procedure TFCONVENIOSBOLETO.LboxVariaveisDblClick(Sender: TObject);
begin
      Memoinstrucoes.Text:=Memoinstrucoes.Text+LboxVariaveis.Items[LboxVariaveis.Itemindex]
end;

function TFCONVENIOSBOLETO.atualizaQuantidade: string;
begin
      Result:='Existem '+ContaRegistros('tabconveniosboleto','codigo')+' Conv�nios de Boleto Cadastrados';
end;

procedure TFCONVENIOSBOLETO.controlChange(sender: TObject);
begin
     controlChange_focus(sender,Self);
end;


procedure TFCONVENIOSBOLETO.lbvariaveissacadoDblClick(Sender: TObject);
begin
         MemoSacado.Text:=MemoSacado.Text+lbvariaveissacado.Items[lbvariaveissacado.Itemindex]
end;

procedure TFCONVENIOSBOLETO.btOpenDialogArquivoLicencaClick(Sender: TObject);
begin
     OpenDialog.Execute;
     EdtCobreBemArquivoLicenca.Text:=OpenDialog.FileName;
end;

procedure TFCONVENIOSBOLETO.btOpenDialogArquivoLogotipoClick(
  Sender: TObject);
begin
     OpenDialog.Execute;
     edtCobreBemArquivoLogotipo.Text:=OpenDialog.FileName;
end;

procedure TFCONVENIOSBOLETO.btOpenDialogImagensCodigoBarrasClick(  Sender: TObject);
Var pdiretorioraiz:String;
begin

      If (selectdirectory('Escolha o diret�rio','',pdiretorioraiz)=False)
      Then exit;

      EdtCobreBemImagensCodigoBarras.Text:=pdiretorioraiz;
end;

procedure TFCONVENIOSBOLETO.btOpenDialogDiretorioRemessaClick( Sender: TObject);
Var pdiretorioraiz:String;
begin

      If (selectdirectory('Escolha o diret�rio','',pdiretorioraiz)=False)
      Then exit;

      EdtCobreBemDiretorioRemessa.Text:=pdiretorioraiz;
end;

procedure TFCONVENIOSBOLETO.btOpenDialogDiretorioRetornoClick( Sender: TObject);
Var pdiretorioraiz:String;
begin
     If (selectdirectory('Escolha o diret�rio','',pdiretorioraiz)=False)
     Then exit;

     EdtCobreBemDiretorioRetorno.Text:=pdiretorioraiz;
end;

procedure TFCONVENIOSBOLETO.ComboCobreBemBancoImprimeBoletoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  Abort;
end;

end.
