object FCONTAGER: TFCONTAGER
  Left = 210
  Top = 213
  Width = 707
  Height = 490
  Caption = 'Cadastro de Contas Gerenciais - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelConta: TPanel
    Left = 0
    Top = 48
    Width = 691
    Height = 135
    Align = alTop
    Color = clSilver
    TabOrder = 1
    DesignSize = (
      691
      135)
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 689
      Height = 133
      Align = alClient
      Stretch = True
    end
    object LbCODIGO: TLabel
      Left = 8
      Top = 14
      Width = 44
      Height = 13
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNOME: TLabel
      Left = 8
      Top = 37
      Width = 37
      Height = 13
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbTIPO: TLabel
      Left = 8
      Top = 83
      Width = 28
      Height = 13
      Caption = 'Tipo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbCODIGOPLANODECONTAS: TLabel
      Left = 8
      Top = 107
      Width = 104
      Height = 13
      Caption = 'Plano de Contas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNomePlanodeContas: TLabel
      Left = 201
      Top = 107
      Width = 145
      Height = 13
      Caption = 'lbNomePlanodeContas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 8
      Top = 60
      Width = 54
      Height = 13
      Caption = 'M'#225'scara'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Shape1: TShape
      Left = 0
      Top = 129
      Width = 693
      Height = 1
      Anchors = [akLeft, akRight, akBottom]
      Pen.Color = clWhite
    end
    object EdtCODIGO: TEdit
      Left = 120
      Top = 11
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object EdtNOME: TEdit
      Left = 120
      Top = 34
      Width = 300
      Height = 19
      MaxLength = 200
      TabOrder = 1
    end
    object EdtCODIGOPLANODECONTAS: TEdit
      Left = 120
      Top = 104
      Width = 72
      Height = 19
      Color = 6073854
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 4
      OnExit = EdtCODIGOPLANODECONTASExit
      OnKeyDown = EdtCODIGOPLANODECONTASKeyDown
    end
    object ComboTipo: TComboBox
      Left = 120
      Top = 80
      Width = 300
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 3
      Items.Strings = (
        'D'#233'bito (CONTAS A PAGAR)'
        'Cr'#233'dito (CONTAS A RECEBER)')
    end
    object edtmascara: TMaskEdit
      Left = 120
      Top = 57
      Width = 300
      Height = 19
      BiDiMode = bdRightToLeft
      CharCase = ecUpperCase
      EditMask = '999.999.999.999.999.999.999;1;_'
      MaxLength = 27
      ParentBiDiMode = False
      TabOrder = 2
      Text = '   .   .   .   .   .   .   '
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 691
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 480
      Top = 0
      Width = 211
      Height = 48
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Conta Gerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btOpcoes: TBitBtn
      Left = 346
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object Btnovo: TBitBtn
      Left = -3
      Top = -3
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 47
      Top = -3
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 97
      Top = -3
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 147
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 247
      Top = -3
      Width = 49
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 296
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
    end
    object btexcluir: TBitBtn
      Left = 197
      Top = -3
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 396
      Top = -3
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
  object PanelSubConta: TPanel
    Left = 0
    Top = 183
    Width = 691
    Height = 70
    Align = alTop
    Color = 14024703
    TabOrder = 2
    object Label5: TLabel
      Left = 6
      Top = 49
      Width = 104
      Height = 13
      Caption = 'Plano de Contas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbnomeplanodecontasSUBCONTA: TLabel
      Left = 198
      Top = 49
      Width = 145
      Height = 13
      Caption = 'lbNomePlanodeContas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbMascara: TLabel
      Left = 199
      Top = 5
      Width = 54
      Height = 13
      Caption = 'M'#225'scara'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 6
      Top = 5
      Width = 44
      Height = 13
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 6
      Top = 27
      Width = 37
      Height = 13
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtnomesubconta: TEdit
      Left = 118
      Top = 24
      Width = 370
      Height = 19
      MaxLength = 100
      TabOrder = 4
    end
    object EdtCODIGOPLANODECONTASsubconta: TEdit
      Left = 118
      Top = 46
      Width = 72
      Height = 19
      Color = 6073854
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentFont = False
      TabOrder = 5
      OnExit = EdtCODIGOPLANODECONTASsubcontaExit
      OnKeyDown = EdtCODIGOPLANODECONTASsubcontaKeyDown
    end
    object edtmascarasubconta: TMaskEdit
      Left = 258
      Top = 2
      Width = 230
      Height = 19
      TabOrder = 1
      Text = 'edtmascara'
    end
    object edtcodigosubconta: TEdit
      Left = 118
      Top = 2
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object BtGravarSubConta: TBitBtn
      Left = 553
      Top = 16
      Width = 40
      Height = 38
      Caption = '&i'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtGravarSubContaClick
    end
    object Btexcluirsubconta: TBitBtn
      Left = 607
      Top = 16
      Width = 40
      Height = 38
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BtexcluirsubcontaClick
    end
  end
  object DbGridSubConta: TDBGrid
    Left = 0
    Top = 253
    Width = 691
    Height = 149
    TabStop = False
    Align = alClient
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Verdana'
    TitleFont.Style = []
    OnDblClick = DbGridSubContaDblClick
    OnKeyPress = DbGridSubContaKeyPress
  end
  object panelrodape: TPanel
    Left = 0
    Top = 402
    Width = 691
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 4
    DesignSize = (
      691
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 691
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 393
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 862
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 862
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
end
