unit URelComprovanteChequeSRecibo;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls;

type
  TFrelComprovanteChequeSRecibo = class(TForm)
    QR: TQuickRep;
    QRBand1: TQRBand;
    LbValor: TQRLabel;
    LbExtenso1: TQRLabel;
    LbExtenso2: TQRLabel;
    LbNominal: TQRLabel;
    LbCidadeCheque: TQRLabel;
    LbDiaCheque: TQRLabel;
    LbMesCheque: TQRLabel;
    LbAnoCheque: TQRLabel;
    LbNumeroCheque: TQRLabel;
    LbConta: TQRLabel;
    LbLinha1Desc: TQRLabel;
    LbLinha2Desc: TQRLabel;
    LbLinha3Desc: TQRLabel;
    lbdobanco: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrelComprovanteChequeSRecibo: TFrelComprovanteChequeSRecibo;

implementation

{$R *.DFM}

end.
