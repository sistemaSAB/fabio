unit uConfiguraBoleto;

interface

uses
  UessencialGlobal, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, Buttons,UobjConfRelatorio,
  RDprint,inifiles;

type
  TFConfiguraBoleto = class(TForm)
    LbParcela: TLabel;
    LbDataEmissao: TLabel;
    LbNumeroDocumento: TLabel;
    LbValor: TLabel;
    PanelPosicoes: TPanel;
    edtleft: TMaskEdit;
    edttop: TMaskEdit;
    LBLEFT: TLabel;
    LBTOP: TLabel;
    Btsalvar: TBitBtn;
    btimprime: TBitBtn;
    LbVencimento: TLabel;
    LbInstrucoes: TLabel;
    LbSacado: TLabel;
    LBQUANTIDADE: TLabel;
    edtquantidade: TMaskEdit;
    CheckUSA: TCheckBox;
    BitBtn1: TBitBtn;
    LbOutroValor: TLabel;
    procedure LbDataEmissaoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LbDataEmissaoMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure edttopKeyPress(Sender: TObject; var Key: Char);
    procedure edtquantidadeKeyPress(Sender: TObject; var Key: Char);
    procedure CheckUSAClick(Sender: TObject);
    procedure BtsalvarClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btimprimeClick(Sender: TObject);
  private
         PcodigobancoGlobal:string;
    procedure PassaLABELPRETO;
    Procedure SalvaPosicoesINI;
    { Private declarations }
  public
    { Public declarations }
    Function  AbrePosicoesIni(Pcodigobanco:string):boolean;overload;
    Function  AbrePosicoesIni:boolean;overload;

  end;





var
  FConfiguraBoleto: TFConfiguraBoleto;
  ObjConfiguraRel:TObjConfRelatorio;



implementation

uses UoBJConfiguraFolhaRdprint, UDataModulo;



{$R *.DFM}

procedure TFConfiguraBoleto.LbDataEmissaoClick(Sender: TObject);
begin
     PassaLABELPRETO;
     PanelPOsicoes.Caption:=TLabel(Sender).Name;
     TLabel(Sender).Font.Color:=clBlue;
     edtleft.text:=floattostr(int(TLabel(Sender).left/5));
     edttop.text:=floattostr(int((TLabel(Sender).Top+self.VertScrollBar.Position)/5));
     edtquantidade.text:=inttostr(TLabel(Sender).tag);
     CheckUSA.Checked:=TLabel(Sender).ParentShowHint;//estou usando como se fosse algo que indicasse se  � para usar ou naum
     edtleft.setfocus;
     VertScrollBar.Position:=0;
end;

procedure TFConfiguraboleto.PassaLABELPRETO;//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1
   do Begin
        if uppercase(Self.Components [int_habilita].ClassName) = 'TLABEL'
        then Tedit(Self.Components [int_habilita]).font.color:=clblack;
   End;

end;
procedure TFConfiguraBoleto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     tag:=0;
end;

procedure TFConfiguraBoleto.LbDataEmissaoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  TLabel(Sender).showhint:=True;
     TLabel(Sender).hint:='Linha->'+inttostr(TLabel(Sender).top)+' - Coluna'+inttostr(TLabel(Sender).left);
end;

procedure TFConfiguraBoleto.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     AbrePosicoesIni;
     LbDataEmissao.onclick(LbDataEmissao);
end;

procedure TFConfiguraBoleto.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
keyc:char;
begin
     keyc:=#13;
     if key=vk_left
     Then begin
                if (strtoint(edtleft.text)>=1)
                Then  edtleft.text:=inttostr(strtoint(edtleft.text)-1);
                self.edtleftKeyPress(Sender,keyc);
     End
     Else
        if key=vk_right
        Then Begin
                edtleft.text:=inttostr(strtoint(edtleft.text)+1);
                self.edtleftKeyPress(Sender,keyc);
        End
        Else
            if key=vk_up
            Then Begin
                if (strtoint(edttop.text)>=1)
                Then edttop.text:=inttostr(strtoint(edttop.text)-1);
                self.edtleftKeyPress(Sender,keyc);
            End
            Else
                if key=vk_down
                Then Begin
                        edttop.text:=inttostr(strtoint(edttop.text)+1);
                        self.edtleftKeyPress(Sender,keyc);
                End;
end;

procedure TFConfiguraBoleto.edtleftKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=5*(strtoint(edttop.text));
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edttop.setfocus;
        End
        Else key:=#0;
end;

procedure TFConfiguraBoleto.edttopKeyPress(Sender: TObject; var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=(5*strtoint(edttop.text))-VertScrollBar.position;
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edtquantidade.setfocus;
        End
        Else key:=#0;end;

procedure TFConfiguraBoleto.edtquantidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=5*(strtoint(edttop.text));
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edtleft.setfocus;
        End
        Else key:=#0;
end;

procedure TFConfiguraBoleto.CheckUSAClick(Sender: TObject);
begin
    TLabel(Self.FindComponent(PanelPosicoes.caption)).ParentShowHint:=CheckUSA.Checked;
end;



Function TFConfiguraBoleto.AbrePosicoesIni(Pcodigobanco:string):boolean;
var
int_habilita:integer;
NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
ObjConfiguracoesRDPRINT:TObjConfiguraFolhaRdPrint;
NomeArquivoBoleto:String;
begin
     result:=False;
     NomeArquivoBoleto:='RELBOLETOBANCARIO'+pcodigobanco+'.INI';
     PcodigobancoGlobal:=pcodigobanco;

     Try
        ObjConfiguracoesRDPRINT:=TObjConfiguraFolhaRdPrint.create;
     Except
           Messagedlg('Erro na tentativa de abrir o arquivo de Configura��es!',mterror,[mbok],0);
           exit;
     End;

     Try
        if (ObjConfiguracoesRDPRINT.ResgataConfiguracoesFolha(FdataModulo.ComponenteRdPrint,NomeArquivoBoleto)=False)
        Then exit;
     Finally
            freeandnil(ObjConfiguracoesRDPRINT);  //nesse caso pode ser freeandnil, pois o objeto n�o instancia nenhum tipo
     End;



     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'CONFRELS\'+NomeArquivoBoleto);
     Except
           Messagedlg('Erro na Abertura do Arquivo CONFRELS\'+NomeArquivoBoleto,mterror,[mbok],0);
           exit;
     End;
Try
     Try

        for int_habilita:=0 to self.ComponentCount -1
        do Begin
                if (uppercase(self.Components [int_habilita].ClassName) = 'TLABEL')
                and (
                (uppercase(self.Components [int_habilita].Name)<>'LBLEFT')
                AND (uppercase(self.Components [int_habilita].Name)<>'LBTOP')
                AND (uppercase(self.Components [int_habilita].Name)<>'LBQUANTIDADE')
                )
                then BEGIN
                          temp:=arquivo_ini.ReadString('CONFIGURACOES_'+UPPERCASE(self.Components [int_habilita].Name),'LINHA','');
                          try
                             strtoint(temp);
                             TLabel(self.FindComponent(self.Components [int_habilita].NAME)).top:=(5*strtoint(temp));
                          except

                          end;
                          temp:=arquivo_ini.ReadString('CONFIGURACOES_'+UPPERCASE(self.Components [int_habilita].Name),'COLUNA','');
                          try
                             strtoint(temp);
                             TLabel(self.FindComponent(self.Components [int_habilita].NAME)).left:=(5*strtoint(temp));
                          except

                          end;

                          temp:=arquivo_ini.ReadString('CONFIGURACOES_'+UPPERCASE(self.Components [int_habilita].Name),'QUANTIDADE','');
                          try
                             strtoint(temp);
                             TLabel(self.FindComponent(self.Components [int_habilita].NAME)).tag:=(strtoint(temp));
                          except

                          end;

                          temp:=arquivo_ini.ReadString('CONFIGURACOES_'+UPPERCASE(self.Components [int_habilita].Name),'USAR','');
                          try
                             if uppercase(temp)='SIM'
                             Then TLabel(self.FindComponent(self.Components [int_habilita].NAME)).ParentShowHint:=True
                             Else TLabel(self.FindComponent(self.Components [int_habilita].NAME)).ParentShowHint:=False;
                          except

                          end;
                END;
        End;


        {memoinstrucoes.lines.clear;
        temp:=arquivo_ini.ReadString('CONFIGURACOES_INSTRUCOES','QUANTIDADELINHAS','');
        try
           strtoint(temp);
           int_habilita:=strtoint(temp);
        except
           Messagedlg('Erro na Propriedade "QUANTIDADEDELINHAS" na chave "CONFIGURACOES_INSTRUCOES" do INI',mterror,[mbok],0);
           exit;
        end;
        //pegando as linhas do INI
        for cont:=1 to int_habilita do
        Begin
            temp:=arquivo_ini.ReadString('CONFIGURACOES_INSTRUCOES','LINHA'+inttostr(cont),'');
            memoinstrucoes.lines.add(temp);
        End;
        //pegando o valor da mora
        temp:=arquivo_ini.ReadString('CONFIGURACOES_INSTRUCOES','VALORMORA','');
        try
           strtofloat(pontoparavirgula(temp));
           RegConfiguracoes.ValorMora:=strtofloat(pontoparavirgula(temp));
        except
           Messagedlg('Erro na Propriedade "VALORMORA" na chave "CONFIGURACOES_INSTRUCOES" do INI ser� usado "0.3"',mterror,[mbok],0);
           RegConfiguracoes.ValorMora:=0.3;
        end;

        temp:=arquivo_ini.ReadString('CONFIGURACOES_INSTRUCOES','TIPOMORA','');
        if (uppercase(temp)='PORCENTO')
        Then RegConfiguracoes.TIpoMora:='PORCENTO'
        ELSE
             if (uppercase(temp)='VALOR')
             Then RegConfiguracoes.TIpoMora:='VALOR'
             Else Begin
                       RegConfiguracoes.TIpoMora:='PORCENTO';
                       Messagedlg('Erro na Propriedade "TIPOMORA" na chave "CONFIGURACOES_INSTRUCOES" do INI ser� usado "PORCENTO"',mterror,[mbok],0);
             End;
        temp:=arquivo_ini.ReadString('CONFIGURACOES_INSTRUCOES','IMPRIMETELEFONECLIENTE','');
        if (uppercase(temp)='SIM')
        Then RegConfiguracoes.ImprimeTelefoneCliente:=True
        Else RegConfiguracoes.ImprimeTelefoneCliente:=False;

        temp:=arquivo_ini.ReadString('CONFIGURACOES_VALORES','IMPRIMEOUTROVALOR','');
        if (uppercase(temp)='SIM')
        Then RegConfiguracoes.ImprimeOUtrovalor:=True
        Else RegConfiguracoes.ImprimeOUtrovalor:=False;


        temp:=arquivo_ini.ReadString('CONFIGURACOES_VALORES','OUTROVALOR','');
        RegConfiguracoes.OUtrovalor:=temp;
        }
        //*****************************************************
        result:=true;
     Except
           Messagedlg('Erro na leitura da CHAVE '+NOMECAMPO,mterror,[mbok],0);
           exit;
     End;
Finally
     arquivo_ini.Free;
End;
end;

procedure TFConfiguraBoleto.SalvaPosicoesINI;
var
int_habilita:integer;
NomeArquivoBoleto,NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     NomeArquivoBoleto:='RELBOLETOBANCARIO'+PcodigobancoGlobal+'.INI';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'CONFRELS\'+NomeArquivoBoleto);
     Except
           Messagedlg('Erro na Abertura do Arquivo CONFRELS\'+NomeArquivoBoleto,mterror,[mbok],0);
           exit;
     End;
Try
     Try

        for int_habilita:=0 to self.ComponentCount -1
        do Begin
                if (uppercase(self.Components [int_habilita].ClassName) = 'TLABEL')
                and (
                (uppercase(self.Components [int_habilita].Name)<>'LBLEFT')
                AND (uppercase(self.Components [int_habilita].Name)<>'LBTOP')
                AND (uppercase(self.Components [int_habilita].Name)<>'LBQUANTIDADE')
                )
                then BEGIN
                          arquivo_ini.WriteString('CONFIGURACOES_'+UPPERCASE(self.Components [int_habilita].Name),'LINHA',FLOATTOSTR(INT((TLabel(self.FindComponent(self.Components [int_habilita].NAME)).TOP/5))));
                          arquivo_ini.WriteString('CONFIGURACOES_'+UPPERCASE(self.Components [int_habilita].Name),'COLUNA',FLOATTOSTR(INT((TLabel(self.FindComponent(self.Components [int_habilita].NAME)).left/5))));
                          arquivo_ini.WriteString('CONFIGURACOES_'+UPPERCASE(self.Components [int_habilita].Name),'QUANTIDADE',FLOATTOSTR(INT((TLabel(self.FindComponent(Self.Components [int_habilita].NAME)).TAG))));
                          if (TLabel(Self.Components [int_habilita]).ParentShowHint=true)
                          Then arquivo_ini.WriteString('CONFIGURACOES_'+UPPERCASE(Self.Components [int_habilita].Name),'USAR','SIM')
                          ELSE arquivo_ini.WriteString('CONFIGURACOES_'+UPPERCASE(Self.Components [int_habilita].Name),'USAR','N�O');
                END;
        End;
        Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);
     Except
           Messagedlg('Erro na escrita da CHAVE '+NOMECAMPO,mterror,[mbok],0);
           exit;
     End;
Finally
     FreeAndNil(arquivo_ini);
End;

end;

procedure TFConfiguraBoleto.BtsalvarClick(Sender: TObject);
begin
     Self.SalvaPosicoesINI;
end;

procedure TFConfiguraBoleto.BitBtn1Click(Sender: TObject);
var
cont:integer;
cont2:integer;
begin
     FdataModulo.ComponenteRdPrint.Abrir;
     for cont:=1 to FdataModulo.ComponenteRdPrint.TamanhoQteColunas do
     Begin
         FdataModulo.ComponenteRdPrint.Imp(1,cont,inttostr(cont)[length(inttostr(cont))]);
     End;

     for cont:=2 to FdataModulo.ComponenteRdPrint.TamanhoQteLinhas do
     Begin
         FdataModulo.ComponenteRdPrint.Imp(cont,1,inttostr(cont));
     End;
     FdataModulo.ComponenteRdPrint.Novapagina;
     FdataModulo.ComponenteRdPrint.Imp(1,1,'NOVA FOLHA');
     FdataModulo.ComponenteRdPrint.fechar;
end;

procedure TFConfiguraBoleto.btimprimeClick(Sender: TObject);
var
cont:integer;
PInstrucoes,PSacado:TStringList;
valorsaldo,moradiaria:currency;
ValorTXT:string;
Begin
//este procedimento apenas envia para o rdprint as informacoes
//do boleto, ele so funciona porque antes de chamado
//foi chamado os procedimentos abrir e setup do rdprint
//e apos enviar tudo chama-se o fechar para dar inicio
//a impressao ou a visualizacao


Try
     

     FdataModulo.ComponenteRdPrint.abrir;
     
     PInstrucoes:=TStringList.create;
     PSacado:=TStringList.create;


     PInstrucoes.clear;
     PInstrucoes.add('At� o Vencimento, pag�vel em qualquer ag. banc�ria');
     PInstrucoes.add('Ap�s o Vencimento, cobrar mora di�ria de R$ 0,333');
     PInstrucoes.add('Protestar ap�s 5 dias de vencimento');
     PInstrucoes.add('Telefone do Cliente: 99012748');

     PSacado.clear;
     Psacado.add('RAZAO');
     Psacado.add('ENDERECO');
     Psacado.add('CNPJ');

     With FConfiguraBoleto do
     Begin

           if (lbvencimento.ParentShowHint=True)//Propriedade usada indicar se vai imprimir ou naum
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(LbVencimento.top/5))),strtoint(floattostr(int(lbvencimento.left/5))),completapalavra(formatdatetime('dd/mm/yy',strtodate('13/03/1980')),strtoint(floattostr(int(lbvencimento.Tag))),' '));

           if (LbDataEmissao.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(LbDataEmissao.top/5))),strtoint(floattostr(int(LbDataEmissao.left/5))),completapalavra(formatdatetime('dd/mm/yy',now),strtoint(floattostr(int(LbDataEmissao.Tag))),' '));//data
           //FdataModulo.ComponenteRdPrint.impf(08,01, datetostr(date),[comp12]);//data

           //o sdadol pediu pra mudar em vez de por o numero do doumento
           //por o numero do titulo
           //impf(08,12, Self.Titulo.Get_NUMDCTO,[comp12]);//N� do Docto
           if (LbNumeroDocumento.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbnumerodocumento.top/5))),strtoint(floattostr(int(Lbnumerodocumento.left/5))),completapalavra('NUMDCTO',strtoint(floattostr(int(Lbnumerodocumento.Tag))),' '));
           //FdataModulo.ComponenteRdPrint.impf(08,12, Self.Titulo.Get_Codigo,[comp12]);//N� do titulo


           if (LbValor.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbvalor.top/5))),strtoint(floattostr(int(Lbvalor.left/5))),CompletaPalavra_a_Esquerda('VALOR',strtoint(floattostr(int(Lbvalor.Tag))),' '));
           //FdataModulo.ComponenteRdPrint.impf(10,65 - length(valortxt), valortxt,[negrito,comp12]);//valor

           if (LbOutroValor.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(LbOutroValor.top/5))),strtoint(floattostr(int(LbOutroValor.left/5))),CompletaPalavra_a_Esquerda('OUTRO VALOR',strtoint(floattostr(int(LbOutroValor.Tag))),' '));

           //informacoes complementares
           for cont:=0 to PInstrucoes.Count-1 do
           Begin
                if (LbInstrucoes.ParentShowHint=True)
                Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbinstrucoes.top/5)))+cont,strtoint(floattostr(int(Lbinstrucoes.left/5))),CompletaPalavra(PInstrucoes[cont],strtoint(floattostr(int(Lbinstrucoes.Tag))),' '));
                //FdataModulo.ComponenteRdPrint.imp(12+cont,01, PInstrucoes[cont]);
           End;

           //Sacado
           for cont:=0 to Psacado.Count-1 do
           Begin
                if (LbSacado.ParentShowHint=True)
                Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbsacado.top/5)))+cont,strtoint(floattostr(int(Lbsacado.left/5))),CompletaPalavra(Psacado[cont],strtoint(floattostr(int(Lbsacado.Tag))),' '));
                //FdataModulo.ComponenteRdPrint.imp(21+cont,01,PSacado[cont]);
           End;
     End;
          FdataModulo.ComponenteRdPrint.fechar;

Finally
       freeandnil(pinstrucoes);
       freeandnil(psacado);
End;

End;

Function TFConfiguraBoleto.AbrePosicoesIni:boolean;
var
Temp:string;
begin
     //pegando o banco para abrir o arquivo

     if (InputQuery('BANCO','DIGITE O C�DIGO DO BANCO DO BOLETO',temp)=False)
     Then temp:='';

     Result:=Self.AbrePosicoesIni(temp);
end;



end.
