unit UFrPagamento;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db,
  UessencialGlobal, Grids, DBGrids,UobjValoresTransferenciaportador,uobjlancamento,IBQuery;

type
  TFrPagamento = class(TFrame)
    Label19: TLabel;
    edtvalor: TEdit;
    btinserir: TBitBtn;
    Label21: TLabel;
    edtportador: TEdit;
    lbportador: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    BtLancaCheque: TBitBtn;
    STRGChequePortador: TStringGrid;
    Label3: TLabel;
    BtGridCheque: TBitBtn;
    DBGrid1: TDBGrid;
    LbtotalDinheiro: TLabel;
    LbTotalCheque: TLabel;
    LbTotalChequeProprio: TLabel;
    Bevel8: TBevel;
    LbSaldo: TLabel;
    Label5: TLabel;
    btexcluir: TBitBtn;
    Label14: TLabel;
    LbTotalselecionado: TLabel;
    Bevel1: TBevel;
    Bevel2: TBevel;
    procedure btinserirClick(Sender: TObject);
    procedure BtGridChequeClick(Sender: TObject);
    procedure STRGChequePortadorDblClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtportadorExit(Sender: TObject);
    procedure edtportadorEnter(Sender: TObject);
    procedure BtLancaChequeClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure STRGChequePortadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure STRGChequePortadorKeyPress(Sender: TObject; var Key: Char);
    procedure edtportadorKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure edtportadorDblClick(Sender: TObject); //Rodolfo
  private
    { Private declarations }
    procedure SeparaListaCheques(TempStr:TStringList);
    Procedure LocalizaporData;
    Procedure LocalizaporCodigodeBarras;
    Procedure LocalizaporNumero;
    Procedure MarcaDesmarcaTudo;
    Procedure SomaCheques;

  public
    //**variaveis que serao preenchidas pelo FLancaNovoLancamento***
    PValorLancamento:Currency;
    PdataLancamento:string;
    PHistoricoLancamento:String;
    PCodigoLancamento:string;
    PportadorDestino:string;
    CredorDevedorLote:string;
    CodigoCredorDevedorLote:string;
    //**************************************************************
    Procedure PassaObjeto(Objeto:TobjValoresTransferenciaportador;PObjetoLancamento:Tobjlancamento;PCredorDevedor,PcodigoCredorDevedor:string);
    procedure Atualizagrid;

  end;

var
  ObjValoresTransferenciaPortador:TObjValoresTransferenciaPortador;
  ObjLancamento:Tobjlancamento;
  ValorTemp:String;
  colunaprocura:integer;
implementation

uses Upesquisa, UObjTransferenciaPortador, UObjTalaodeCheques,
  UobjComprovanteCheque, UobjCmC7, UDataModulo;



{$R *.DFM}



{ TFrPagamento }

procedure TFrPagamento.PassaObjeto(Objeto:TobjValoresTransferenciaportador;PObjetoLancamento:Tobjlancamento;PCredorDevedor,PcodigoCredorDevedor:string);
begin
    ObjValoresTransferenciaPortador:=Objeto;
    ObjLancamento:=Pobjetolancamento;
    DBGrid1.DataSource:=ObjValoresTransferenciaPortador.ObjDatasource;
    Self.Atualizagrid;
    ValorTemp:='';
    colunaprocura:=2;
    LbTotalselecionado.caption:='';
    Self.CredorDevedorLote:=PCredorDevedor;
    Self.CodigoCredorDevedorLote:=PCodigoCredorDevedor;
end;

procedure TFrPagamento.btinserirClick(Sender: TObject);
var
PlistaCheques:TStringList;
cont:integer;
Objquery: TIBQuery;
begin

    cont:=0;

   //um pagamento nada mais � que a transferencia de dinheiro ou cheques de 3�
   //de um portador para outro, geralmente o portador destino � um portador lixo
   //como por exemplo o CONTAS PAGAS
   if (edtportador.text='')
   Then Begin
             Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
             exit;
   End;
   
   Try
      strtoint(edtportador.text);
      if (ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.LocalizaCodigo(edtportador.text)=False)
      Then Begin
                Messagedlg('Portador n�o encontrado!',mterror,[mbok],0);
                exit;
      End;

      ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.TabelaparaObjeto;

      if (ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.Get_Apenas_Uso_Sistema='S')
      Then Begin
                Messagedlg('Esse portador n�o poder� ser utilizado, ele � de uso exclusivo do sistema!',mterror,[mbok],0);
                exit;
      End;

   Except

         Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
         exit;
         
   End;



   Try
      PlistaCheques:=TStringList.create;
   Except
         Messagedlg('Erro na Tentativa de Criar a Lista de Cheque!',mterror,[mbok],0);
         exit;
   End;

   Try
       Self.SeparaListaCheques(PlistaCheques);
        //altera��o 07/02/2011 *jonatan
        //quando o usuario usar um cheque de terceiro para pagar uma conta
        //o mesmo deve ser baixado automaticamente
       while(cont<PlistaCheques.Count) do
       begin
             try
                   try
                        Objquery:=TIBQuery.Create(nil);
                        Objquery.Database:=FDataModulo.IBDatabase;
                        ObjQuery.Close;
                        ObjQuery.SQL.Clear;
                        ObjQuery.SQL.Add('update tabchequesportador set baixado = ''S'' ' ) ;
                        ObjQuery.SQL.Add('where CODIGO ='+PlistaCheques.Strings[Cont]);
                        ObjQuery.ExecSQL;
                        FDataModulo.IBTransaction.CommitRetaining;
                        Inc(cont,1);
                   except

                   end;

             finally
                 FreeAndNil(Objquery);
             end;


       end;

       ObjValoresTransferenciaPortador.ZerarTabela;
       With ObjValoresTransferenciaPortador.TransferenciaPortador do
       Begin 
            If (PlistaCheques.count>0)
            Then Begin
                      //salvo com Valor Zero,
                      //depois salvo o Valor em Dinheiro
                      status:=dsinsert;
                      Submit_CODIGO(Get_NovoCodigo);
                      Submit_PortadorOrigem(edtportador.text);
                      Submit_Data(PdataLancamento);
                      Submit_Grupo('');
                      Submit_Valor('0');
                      Submit_Documento('');
                      Submit_Complemento('');
                      Submit_PortadorDestino(PPortadorDestino);
                      CodigoLancamento.Submit_codigo(PCodigoLancamento);
                      Submit_ListaChequesTransferencia(PlistaCheques);
                      //salvo, sem commitar e sem exportar contabilidade
                      //porque a contabilidade sera exportada depois
                      if (Salvar(True,true,True,True,True,False,'',PHistoricoLancamento)=False)
                      Then Begin
                                Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
                                exit;
                      End;
                      PlistaCheques.Clear;
            End;
            Try
               if (edtvalor.text='')
               Then edtvalor.text:='0';
               strtofloat(edtvalor.Text);
            Except
                  edtvalor.Text:='0';
            End;
            if (strtofloat(edtvalor.Text)>0)
            Then Begin
                    status:=dsinsert;
                    Submit_CODIGO(Get_NovoCodigo);
                    Submit_PortadorOrigem(edtportador.text);
                    Submit_Data(PdataLancamento);
                    Submit_Grupo('');
                    Submit_Valor(edtvalor.Text);
                    Submit_Documento('');
                    Submit_Complemento('');
                    Submit_PortadorDestino(PPortadorDestino);
                    CodigoLancamento.Submit_CODIGO(PCodigoLancamento);
                    Submit_ListaChequesTransferencia(PlistaCheques);
                    //salvo, sem commitar e sem exportar contabilidade

                    if (Salvar(True,true,True,True,True,False,'',PhistoricoLancamento)=False)
                    Then Begin
                              Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
                              exit;
                    End;
            End;
            STRGChequePortador.cols[0].clear;
            STRGChequePortador.ColCount:=2;
            STRGChequePortador.rowcount:=2;
            STRGChequePortador.cols[0].clear;
            STRGChequePortador.cols[1].clear;

            Self.Atualizagrid;
            LbTotalselecionado.caption:='';
            edtvalor.text:='';
            edtportador.text:='';
            lbportador.Caption:='';
            edtvalor.setfocus;
            DBGrid1.Visible:=True;
            STRGChequePortador.Visible:=False;
            Self.BtGridCheque.caption:='&Cheques de terceiros';
            ValorTemp:='';
            colunaprocura:=2;
       End;
   Finally
          FreeAndNil(PlistaCheques);
   End;
End;

procedure TFrPagamento.SeparaListaCheques(TempStr: TStringList);
var
cont:integer;
begin
         tempStr.clear;
         for cont:=0 to STRGChequePortador.Cols[1].Count-1 do
         Begin
              if STRGChequePortador.Cols[0].Strings[cont]='X'
              Then tempstr.add(STRGChequePortador.Cells[19,cont]);

         End;
end;

procedure TFrPagamento.Atualizagrid;
var
TMPDinheiro,TmpCheque,TmpChequePortador:string;
begin

     ObjValoresTransferenciaPortador.Seleciona_Pagamentos_Lancamento(PCodigoLancamento,TmpCheque,TMPDinheiro,TmpChequePortador);
     lbtotalcheque.caption:=TmpCheque;
     lbtotaldinheiro.caption:=TMPDinheiro;
     lbtotalchequeProprio.caption:=TmpChequePortador;
     Try
        LbSaldo.caption:=Floattostr(PValorLancamento-StrToFloat(LbTotalChequeProprio.caption)
                                    -StrToFloat(LbTotalCheque.caption)
                                    -StrToFloat(LbTotalDinheiro.caption));
     Except
           LbSaldo.caption:='0';
     End;
     Try
        LbtotalDinheiro.caption:=formata_valor(lbtotaldinheiro.Caption);
        LbtotalCheque.caption:=formata_valor(lbtotalcheque.Caption);
        LbtotalChequeProprio.caption:=formata_valor(lbtotalchequeproprio.Caption);
        lbsaldo.caption:=formata_valor(lbsaldo.caption);
     Except

     End;
     formatadbgrid(dbgrid1);
end;

procedure TFrPagamento.BtGridChequeClick(Sender: TObject);
begin
     if (edtportador.text='')
     and (STRGChequePortador.Visible=False)
     Then exit;

     DBGrid1.Visible:=not(DBGrid1.Visible);
     STRGChequePortador.Visible:=not(STRGChequePortador.Visible);

     if (DBGrid1.Visible=True)
     Then BtGridCheque.caption:='&Cheques de terceiros'
     Else BtGridCheque.caption:='&Dados Lan�ados';


     {if (edtportador.Text<>'')
     and (STRGChequePortador.Visible=True)
     Then Begin
               //a ultima coluna � o portador
               //se ja selecionado o portador
               //nao precisa pesquisar novo
               if (STRGChequePortador.Cells[STRGChequePortador.ColCount-1,1]<>edtportador.text)
               Then ObjValoresTransferenciaPortador.TransferenciaPortador.Get_ListaChequesPortador(Edtportador.text,STRGChequePortador,False,'');
               Self.SomaCheques;
     End;}

     AjustaLArguraColunaGrid(STRGChequePortador);
end;

procedure TFrPagamento.STRGChequePortadorDblClick(Sender: TObject);
begin

     If(STRGChequePortador.cells[1,0]='') or (STRGChequePortador.Row=0)
     Then exit;

     If (STRGChequePortador.Cells[0,STRGChequePortador.Row]='X')
     Then STRGChequePortador.Cells[0,STRGChequePortador.Row]:=''
     Else STRGChequePortador.Cells[0,STRGChequePortador.Row]:='X';

     Self.SomaCheques;

end;

procedure TFrPagamento.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.Get_PesquisaApenasUsoSistema,ObjValoresTransferenciaPortador.TransferenciaPortador.get_titulopesquisaPortadorOrigem,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbPortador.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFrPagamento.edtportadorExit(Sender: TObject);
begin
     lbportador.caption:='';

     if (edtportador.Text='')
     Then Begin
              //zerando o grid
              STRGChequePortador.cols[0].clear;
              STRGChequePortador.ColCount:=2;
              STRGChequePortador.rowcount:=2;
              STRGChequePortador.cols[0].clear;
              STRGChequePortador.cols[1].clear;
              Self.SomaCheques;
              exit;
     End;

     if (ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.LocalizaCodigo(edtportador.text)=False)
     Then Begin
               edtportador.Text:='';
               //zerando o grid de cheques
               STRGChequePortador.cols[0].clear;
               STRGChequePortador.ColCount:=2;
               STRGChequePortador.rowcount:=2;
               STRGChequePortador.cols[0].clear;
               STRGChequePortador.cols[1].clear;
               Self.SomaCheques;
               exit;
     End;
     ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.TabelaparaObjeto;
     lbportador.caption:=ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.Get_nome;

     //quando o foco entrou no edit, foi anotado o numero do portador
     //na variavel tag, se na saida o portador foi diferente do tag
     //busca para o novo portador por causa das selecoes, senaum
     //mantem o grid
     if (strtoint(edtportador.Text)<>edtportador.Tag)
     Then ObjValoresTransferenciaPortador.TransferenciaPortador.Get_ListaChequesPortador(Edtportador.text,STRGChequePortador,False,'');

     Self.SomaCheques;
end;

procedure TFrPagamento.edtportadorEnter(Sender: TObject);
begin
     {DBGrid1.Visible:=True;
     STRGChequePortador.Visible:=False;
     Self.BtGridCheque.caption:='&Cheques do Portador';}

     //Quando eu entro no edit eu anoto o portador que esta no momento
     //se naum tem nenhum eu zero o StringGrid e coloco -1 no tag do edit
     //na saida eu verifico o portador
     //se tiver vazio zero a stringgrid, caso contrario
     //verifico se era igual a entrada pelo tag, senaum zero o grid
     //se sim mantenho, pois pode ter cheque selecionado no grid

     Try
        if (edtportador.Text='')
        Then Begin
                  STRGChequePortador.cols[0].clear;
                  STRGChequePortador.ColCount:=2;
                  STRGChequePortador.rowcount:=2;
                  STRGChequePortador.cols[0].clear;
                  STRGChequePortador.cols[1].clear;
                  edtportador.Tag:=-1;
        End
        Else edtportador.Tag:=strtoint(edtportador.Text);
        
     Except
           edtportador.tag:=-1;
     End;
end;

procedure TFrPagamento.BtLancaChequeClick(Sender: TObject);
begin

     if (ObjLancamento.LocalizaCodigo(PCodigoLancamento)=False)
     Then Begin
               Messagedlg('Lan�amento n�o encontrado!',mterror,[mbok],0);
               exit;
     End;
     ObjLancamento.TabelaparaObjeto;


     if (ObjLancamento.PreencheCheque(edtportador.text, tira_ponto(LbSaldo.caption), self.CredorDevedorLote, self.CodigoCredorDevedorLote, lbportador.caption)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Preencher cheque!',mterror,[mbok],0);
               exit;
     End;
     
     Self.Atualizagrid;
end;

procedure TFrPagamento.btexcluirClick(Sender: TObject);
var
ChequeTalao:TObjTalaodeCheques;
ObjComprovanteCheque:TObjComprovanteCheque;
begin
     if (STRGChequePortador.visible=True)
     Then Begin
               Messagedlg('Escolha qual Informa��o deseja excluir!',mtInformation,[mbok],0);
               exit;
     End;

     if (DBGrid1.DataSource.DataSet.RecordCount=0)
     Then exit;

     if (DBGrid1.DataSource.DataSet.FieldByName('especie').asstring='D')//dinheiro
     Then Begin
               if (ObjValoresTransferenciaPortador.TransferenciaPortador.exclui(DBGrid1.DataSource.DataSet.FieldByName('codigo').asstring,False)=False)
               Then Begin
                         Messagedlg('Erro na Tentativa de Excluir!',mterror,[mbok],0);
                         exit;
               End;
     End
     Else Begin
               if (DBGrid1.DataSource.DataSet.FieldByName('especie').asstring='C')//cheque
               Then Begin

                         //****celio***170209***** ao excluir um cheque lan�ado
                         //como pagamento o mesmo nao retornava ao portador
                         //de origem.
                         ObjValoresTransferenciaPortador.LocalizaCodigo(DBGrid1.DataSource.DataSet.FieldByName('codigo').asstring);
                         ObjValoresTransferenciaPortador.TabelaparaObjeto;
                         ObjValoresTransferenciaPortador.Valores.Submit_Portador(ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.Get_CODIGO);
                         ObjValoresTransferenciaPortador.Valores.Status := dsedit;
                         ObjValoresTransferenciaPortador.Valores.Salvar(false,false,false,'');
                         //************************
                         if (ObjValoresTransferenciaPortador.exclui(DBGrid1.DataSource.DataSet.FieldByName('codigo').asstring,False)=False)
                         Then Begin
                                   Messagedlg('Erro na Tentativa de Excluir!',mterror,[mbok],0);
                                    exit;
                         End;
               End
               Else Begin
                         if (DBGrid1.DataSource.DataSet.FieldByName('especie').asstring='CP')
                         Then Begin
                                   Try
                                      ChequeTalao:=TObjTalaodeCheques.create;
                                      ObjComprovanteCheque:=TObjComprovanteCheque.create;
                                   Except
                                         Messagedlg('Erro na tentativa de gerar o objetos de tal�o e comprovante de cheques',mterror,[mbok],0);
                                         exit;
                                   End;

                                   Try
                                       ObjValoresTransferenciaPortador.LocalizaCodigo(DBGrid1.DataSource.DataSet.fieldbyname('codigo').asstring);
                                       ObjValoresTransferenciaPortador.tabelaparaobjeto;
                                       If (ObjValoresTransferenciaPortador.Exclui(ObjValoresTransferenciaPortador.get_codigo,True)=False)
                                       Then Begin
                                                 Messagedlg('Erro na tentativa de Excluir!',mterror,[mbok],0);
                                                 exit;
                                       End;
                                       if (ChequeTalao.LocalizaChequePortador(ObjValoresTransferenciaPortador.valores.get_codigo)=False)
                                       Then Begin
                                                Messagedlg('Cheque n�o localizado no Tal�o!',mterror,[mbok],0);
                                                 exit;
                                       End;
                                       ChequeTalao.TabelaparaObjeto;
                                       ChequeTalao.Status:=dsEdit;
                                       ChequeTalao.Submit_Valor('0');
                                       ChequeTalao.Submit_USado('N');
                                       ChequeTalao.CodigoChequePortador.Submit_CODIGO('');
                                       ChequeTalao.Submit_HistoricoPagamento('');
                                       ChequeTalao.Lancamento.ZerarTabela;
                                       if (ChequeTalao.Salvar(true)=False)
                                       Then Begin
                                                 Messagedlg('Erro na tentativa de Reaproveitar o Cheque do Portador',mterror,[mbok],0);
                                                 exit;
                                       End;
                                       //exclui o comprovante deste cheque
                                       if (objcomprovantecheque.LocalizaCheque(ObjValoresTransferenciaPortador.Valores.get_codigo)=True)
                                       Then Begin
                                                 objcomprovantecheque.TabelaparaObjeto;
                                                 if (objcomprovantecheque.exclui(objcomprovantecheque.get_codigo,True)=False)
                                                 Then Begin
                                                           Messagedlg('Erro na Tentativa de Excluir o Comprovante de Cheque N�'+objcomprovantecheque.get_codigo,mterror,[mbok],0);
                                                           exit;
                                                 End;
                                       End;
                                       //exclui da TabChequesPortador
                                       If (ObjValoresTransferenciaPortador.Valores.Exclui(ObjValoresTransferenciaPortador.valores.get_codigo,True)=False)
                                       Then Begin
                                                 Messagedlg('Erro na tentativa de excluir o cheque '+ObjValoresTransferenciaPortador.Valores.get_codigo+' da TabChequesPortador',mterror,[mbok],0);
                                                  exit;
                                       End;
                                   Finally
                                          ChequeTalao.free;
                                          ObjComprovanteCheque.Free;
                                   End;
                         End;//then
               End;//else
     End;

     Self.Atualizagrid;
end;

procedure TFrPagamento.STRGChequePortadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
cont:integer;
Valortemp2:Currency;
begin

     if (STRGChequePortador.rowCount<1)
     Then exit;

     case key of
             VK_F5:Self.LocalizaporData;
             VK_F6:Self.LocalizaporNumero;
             VK_F7:Self.MarcaDesmarcaTudo;
             VK_F8:Self.LocalizaporCodigodeBarras;
     End;


     If (ValorTemp='')
     Then exit;


     If (Key<>vk_F3)
     Then exit;

     for cont:=STRGChequePortador.row+1 to STRGChequePortador.rowCount-1 do
     Begin
          if (colunaprocura=2)
          Then Begin
                  Try
                     Valortemp2:=strtofloat(tira_ponto(STRGChequePortador.cells[colunaprocura,cont]));
                  Except
                        ValorTemp2:=0;
                  End;
                  If (Valortemp=floattostr(Valortemp2))
                  Then Begin
                            STRGChequePortador.row:=cont;
                            exit;
                  End;
          End
          Else
          if (colunaprocura=3)//por data
          Then Begin
                  Try
                     strtodate(STRGChequePortador.cells[colunaprocura,cont]);
                     If (Valortemp=STRGChequePortador.cells[colunaprocura,cont])
                     Then Begin
                            STRGChequePortador.row:=cont;
                            exit;
                     End;
                  Except
                  End;
          End
          else
          if (colunaprocura=1)//por numero
          Then Begin
                     If (Valortemp=STRGChequePortador.cells[colunaprocura,cont])
                     Then Begin
                            STRGChequePortador.row:=cont;
                            exit;
                     End;
          End
          else
          if (colunaprocura=16)//codigo de barras
          Then Begin
                    If (Valortemp=RetornaSoNUmeros(STRGChequePortador.cells[colunaprocura,cont]))
                     Then Begin
                              STRGChequePortador.row:=cont;
                              exit;
                     End;
          End;
     End;
end;

procedure TFrPagamento.STRGChequePortadorKeyPress(Sender: TObject;
  var Key: Char);
var
tmpstr:String;
cont:integer;
ValorTemp2:Currency;
begin
     If key=#13
     Then Begin
                STRGChequePortador.OnDblClick(sender);
                STRGChequePortador.setfocus;
     End
     Else Begin
               if (key=#32)
               Then Begin
                         tmpstr:='';
                         
                         colunaprocura:=1;
                         colunaprocura:=STRGChequePortador.Rows[0].IndexOf('VALOR');

                         if (UPPERCASE(STRGChequePortador.Cells[colunaprocura,0])<>'VALOR')
                         THEN EXIT;
                         
                         If (Inputquery('Procura Cheque','Digite o Valor',tmpstr)=False)
                         Then exit;

                         if (tmpstr='')
                         Then exit;

                         Try
                            strtofloat(tmpstr);
                            valortemp:=tmpstr;
                         Except
                               exit;
                         End;

                         //procurando em todas as linhas
                         for cont:=1 to STRGChequePortador.rowCount-1 do
                         Begin
                              Try
                                 Valortemp2:=strtofloat(tira_ponto(STRGChequePortador.cells[colunaprocura,cont]));
                              Except
                                    ValorTemp2:=0;
                              End;
                              If (floattostr(strtofloat(Valortemp))=floattostr(Valortemp2))
                              Then Begin
                                        STRGChequePortador.row:=cont;
                                        exit;

                              End;

                         End;

               End;
     End;

end;

procedure TFrPagamento.LocalizaporData;
var
tmpstr:String;
cont:integer;
begin
     tmpstr:='';
     colunaprocura:=03;

     if (UPPERCASE(STRGChequePortador.Cells[colunaprocura,0])<>'VENCIMENTO')
     THEN EXIT;

     If (Inputquery('Procura Cheque','Digite o Vencimento',tmpstr)=False)
     Then exit;

     if (tmpstr='')
     Then exit;

     Try
        strtodate(tmpstr);
        valortemp:=tmpstr;
     Except
           exit;
     End;

     //procurando em todas as linhas
     for cont:=1 to STRGChequePortador.rowCount-1 do
     Begin
          Try
             strtodate(STRGChequePortador.cells[colunaprocura,cont]);
             If (Valortemp=STRGChequePortador.cells[colunaprocura,cont])
             Then Begin
                    STRGChequePortador.row:=cont;
                    exit;
             End;
          Except

          End;

     End;

end;

procedure TFrPagamento.LocalizaporNumero;
var
tmpstr:String;
cont:integer;
begin
     tmpstr:='';
     colunaprocura:=1;

     if (UPPERCASE(STRGChequePortador.Cells[colunaprocura,0])<>'NUMCHEQUE')
     THEN EXIT;
     
     If (Inputquery('Procura Cheque','Digite o N�',tmpstr)=False)
     Then exit;

     if (tmpstr='')
     Then exit;

     valortemp:=tmpstr;

     //procurando em todas as linhas
     for cont:=1 to STRGChequePortador.rowCount-1 do
     Begin
          If (Valortemp=STRGChequePortador.cells[colunaprocura,cont])
          Then Begin
                    STRGChequePortador.row:=cont;
                    exit;
          End;
     End;

end;

procedure TFrPagamento.MarcaDesmarcaTudo;
var
cont:Integer;
marca:boolean;
begin
     if (STRGChequePortador.Cells[0,1]='')
     Then Marca:=True
     Else Marca:=False;

     for cont:=1 to STRGChequePortador.RowCount-1 do
     Begin
          if marca=true
          Then STRGChequePortador.Cells[0,cont]:='X'
          Else STRGChequePortador.Cells[0,cont]:='';
     End;

     Self.SomaCheques;

end;

procedure TFrPagamento.SomaCheques;
var
cont:integer;
SomaGeral:Currency;
begin
     somageral:=0;

     for cont:=1 to STRGChequePortador.RowCount-1 do
     Begin
          if (STRGChequePortador.Cells[0,cont]='X')
          Then SomaGeral:=SomaGeral+strtofloat(tira_ponto(STRGChequePortador.Cells[2,cont]));
     End;
     LbTotalselecionado.caption:=Floattostr(SomaGeral);
     LbTotalselecionado.caption:=formata_valor(LbTotalselecionado.caption);
     LbTotalselecionado.caption:='Selecionado R$ '+LbTotalselecionado.caption; 

end;

procedure TFrPagamento.edtportadorKeyPress(Sender: TObject; var Key: Char);
begin
     if not (key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFrPagamento.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then if key='.'
          Then key:=','
          Else key:=#0;
end;

procedure TFrPagamento.LocalizaporCodigodeBarras;
var
tmpstr,temp:String;
cont:integer;
ObjCmc7:TObjCMC7;
begin
     Try
        ObjCmc7:=TObjCMC7.create;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto de CMC7',mterror,[mbok],0);
           exit;
     End;

     Try
       tmpstr:='';
       colunaprocura:=16;

       if (UPPERCASE(STRGChequePortador.Cells[colunaprocura,0])<>'CODIGODEBARRAS')
       THEN EXIT;

       If (Inputquery('Procura Cheque','Digite o C�digo de Barras',tmpstr)=False)
       Then exit;

       if (tmpstr='')
       Then exit;

       if (ObjCmc7.ValidaCmc7(tmpstr)=False)
       then exit;

       ValorTemp:=ObjCmc7.Get_NumeroCodigoBarras;//sem os caracteres de controle


     Finally
            ObjCmc7.free;
     End;

     //procurando em todas as linhas
     for cont:=1 to STRGChequePortador.rowCount-1 do
     Begin
          Try
             If (Valortemp=RetornaSoNUmeros(STRGChequePortador.cells[colunaprocura,cont]))
             Then Begin
                    STRGChequePortador.row:=cont;
                    exit;
             End;
          Except

          End;

     End;

end;

{Rodolfo}
procedure TFrPagamento.edtportadorDblClick(Sender: TObject);
var
  Shift : TShiftState;
  key : Word;
begin
  key := VK_F9;
  edtportadorKeyDown(Sender, Key, Shift);
end;

end.
