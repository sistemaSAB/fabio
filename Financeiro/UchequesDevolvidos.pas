unit UchequesDevolvidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, UFrLancachequeDevolvido;

type
  TFchequesDevolvidos = class(TForm)
    FrLancaChequeDevolvido1: TFrlancaChequeDevolvido;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FrLancaChequeDevolvido1BtGeraContasClick(Sender: TObject);
    procedure FrLancaChequeDevolvido1BtGeraConta_CPClick(Sender: TObject);
    procedure FrLancaChequeDevolvido1BtConsultarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FchequesDevolvidos: TFchequesDevolvidos;

implementation

{$R *.dfm}

procedure TFchequesDevolvidos.FormShow(Sender: TObject);
begin
     FrLancaChequeDevolvido1.Cria;
end;

procedure TFchequesDevolvidos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     FrLancaChequeDevolvido1.Destroi;
end;

procedure TFchequesDevolvidos.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFchequesDevolvidos.FrLancaChequeDevolvido1BtGeraContasClick(
  Sender: TObject);
begin
  FrLancaChequeDevolvido1.BtGeraContasClick(Sender);

end;

procedure TFchequesDevolvidos.FrLancaChequeDevolvido1BtGeraConta_CPClick(
  Sender: TObject);
begin
  FrLancaChequeDevolvido1.BtGeraConta_CPClick(Sender);

end;

procedure TFchequesDevolvidos.FrLancaChequeDevolvido1BtConsultarClick(
  Sender: TObject);
begin
  FrLancaChequeDevolvido1.BtConsultarClick(Sender);

end;

end.
