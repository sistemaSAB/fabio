object FrchequeDevolvido: TFrchequeDevolvido
  Left = 0
  Top = 0
  Width = 327
  Height = 134
  TabOrder = 0
  object Label3: TLabel
    Left = 8
    Top = 27
    Width = 40
    Height = 13
    Caption = 'Portador'
  end
  object Lbportador: TLabel
    Left = 124
    Top = 27
    Width = 60
    Height = 15
    Caption = 'Lbportador'
    Font.Charset = ANSI_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Bevel1: TBevel
    Left = 4
    Top = 19
    Width = 315
    Height = 32
  end
  object Bevel2: TBevel
    Left = 4
    Top = 19
    Width = 315
    Height = 101
  end
  object Label2: TLabel
    Left = 8
    Top = 62
    Width = 74
    Height = 13
    Caption = 'Data do Extrato'
  end
  object Label1: TLabel
    Left = 8
    Top = 93
    Width = 88
    Height = 13
    Caption = 'Cheque Devolvido'
  end
  object edtportador: TEdit
    Left = 64
    Top = 24
    Width = 57
    Height = 21
    TabOrder = 0
  end
  object edtdata: TMaskEdit
    Left = 120
    Top = 59
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object edtcheque: TEdit
    Left = 120
    Top = 89
    Width = 73
    Height = 21
    TabOrder = 2
  end
  object Btbaixa: TBitBtn
    Left = 194
    Top = 52
    Width = 124
    Height = 66
    Caption = '&Baixar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clActiveCaption
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
  end
end
