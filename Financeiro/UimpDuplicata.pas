unit UimpDuplicata;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls;

type
  TFImpDuplicata = class(TForm)
    QR: TQuickRep;
    ValorFatura: TQRLabel;
    NumeroFatura: TQRLabel;
    ValorDuplicata: TQRLabel;
    numeroduplicata: TQRLabel;
    vencimento: TQRLabel;
    dataemissao: TQRLabel;
    nome: TQRLabel;
    endereco: TQRLabel;
    cep: TQRLabel;
    estado: TQRLabel;
    cidade: TQRLabel;
    cnpj: TQRLabel;
    ie: TQRLabel;
    valorextenso1: TQRLabel;
    valorextenso2: TQRLabel;
    dataaceite: TQRLabel;
    pracapagamento: TQRLabel;
    descontode: TQRLabel;
    ate: TQRLabel;
    condicoesespeciais: TQRLabel;
    notafiscal: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImpDuplicata: TFImpDuplicata;

implementation

{$R *.DFM}

end.
