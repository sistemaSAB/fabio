unit UPagamento_e_geracao_emprestimo;

interface

uses
  db,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ExtCtrls, Buttons, StdCtrls, Grids, Mask, uobjlancamento,UessencialGlobal;

type
  TFPagamento_e_geracao_emprestimo = class(TForm)
    guia: TPageControl;
    GuiaContasaPagar: TTabSheet;
    TabSheet2: TTabSheet;
    RichEdit1: TRichEdit;
    Panel_Gera_Conta_a_Receber: TPanel;
    Label4: TLabel;
    comboCredordevedorCR: TComboBox;
    edtcodigocredordevedorCR: TEdit;
    Label5: TLabel;
    Panel_Gera_Conta_a_pagar: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label10: TLabel;
    Label7: TLabel;
    LbValorLancamentoCP: TLabel;
    lbpaga: TLabel;
    LbSaldoCP: TLabel;
    Bevel1: TBevel;
    LbNomeCredorDevedorCP: TLabel;
    combocredordevedorcodigoCP: TComboBox;
    LbtipoCampoCP: TListBox;
    edtpesquisa_STRG_GRID_CP: TMaskEdit;
    strgCP: TStringGrid;
    BtSelecionaTodas_CP: TButton;
    edtvalorlancamentoCP: TMaskEdit;
    edtcodigohistoricoCP: TEdit;
    edthistoricoCP: TEdit;
    btexecutaCP: TBitBtn;
    btpesquisaCP: TBitBtn;
    edtdatalancamentoCP: TMaskEdit;
    edtvencimentoCP: TMaskEdit;
    edtcodigocredordevedorcp: TEdit;
    combocredordevedorCP: TComboBox;
    Label3: TLabel;
    combocredordevedorcodigoCR: TComboBox;
    LbNomeCredorDevedorCR: TLabel;
    procedure btpesquisaCPClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtcodigocredordevedorcpKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtcodigocredordevedorcpExit(Sender: TObject);
    procedure combocredordevedorCPChange(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalorlancamentoCPExit(Sender: TObject);
    procedure edtvalorlancamentoCPKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigohistoricoCPExit(Sender: TObject);
    procedure edtcodigohistoricoCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigohistoricoCPKeyPress(Sender: TObject; var Key: Char);
    procedure guiaChange(Sender: TObject);
    procedure BtSelecionaTodas_CPClick(Sender: TObject);
    procedure strgCPDblClick(Sender: TObject);
    procedure strgCPEnter(Sender: TObject);
    procedure strgCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure strgCPKeyPress(Sender: TObject; var Key: Char);
    procedure edtpesquisa_STRG_GRID_CPKeyPress(Sender: TObject;
      var Key: Char);
    procedure btexecutaCPClick(Sender: TObject);
    procedure comboCredordevedorCRChange(Sender: TObject);
    procedure edtcodigocredordevedorCRExit(Sender: TObject);
    procedure edtcodigocredordevedorCRKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
         ObjLancamento:TobjLancamento;
         procedure AtualizaLabelsCP;
         procedure limpalabels;
         procedure SalvaContasPagar;
         Procedure SalvaContasPagar_UmaPendencia(ppendencia,pvalor:String);
         Function  GeraContaaReceber(Plancamentoapagar:string):boolean;
  public
    { Public declarations }
  end;

var
  FPagamento_e_geracao_emprestimo: TFPagamento_e_geracao_emprestimo;
  VencimentoCP:string;

implementation

uses Uformata_String_Grid, Upesquisa, UHistoricoSimples,
  UDataModulo, UObjTitulo, UFiltraImp,
  UObjGeraTitulo, UObjchequesPortador;



{$R *.dfm}

procedure TFPagamento_e_geracao_emprestimo.btpesquisaCPClick(Sender: TObject);
begin
     VencimentoCP:=EDTVENCIMENTOCP.text;
     Objlancamento.Pendencia.Titulo.Get_PendenciaseTitulo('D',EDTVENCIMENTOCP.TEXT,STRGCP,comboCredordevedorcodigoCP.Items[comboCredordevedorCP.itemindex],edtcodigoCredordevedorCP.text,'',LbtipoCampoCP.Items);
     edtdatalancamentoCP.text:=datetostr(now);

     If (STRGCP.ROWCOUNT<=1)
     Then STRGCP.FixedRows:=0
     Else STRGCP.FixedRows:=1;
     
     Formata_StringGrid(strgCP,LbtipoCampoCP.Items);
     AjustaLArguraColunaGrid(STRGCP);
     Self.AtualizaLabelsCP;
end;

procedure TFPagamento_e_geracao_emprestimo.FormShow(Sender: TObject);
begin
     guia.ActivePageIndex:=0;

     limpaedit(Panel_Gera_Conta_a_Receber);
     limpaedit(Panel_Gera_Conta_a_pagar);

     self.limpalabels;
     
     Try
        ObjLancamento:=TobjLancamento.Create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           exit;
     End;

     ObjLancamento.Pendencia.Titulo.Get_ListaCRedorDevedor(comboCredordevedorCP.items);
     ObjLancamento.Pendencia.Titulo.Get_ListaCRedorDevedorCodigo(comboCredordevedorcodigoCP.items);

     ObjLancamento.Pendencia.Titulo.Get_ListaCRedorDevedor(comboCredordevedorCR.items);
     ObjLancamento.Pendencia.Titulo.Get_ListaCRedorDevedorCodigo(comboCredordevedorcodigoCR.items);

     PegaFiguraBotao(BtpesquisaCP,'BotaoPesquisar.Bmp');
     PegaFiguraBotao(BtexecutaCP,'BotaoGravar.Bmp');
     
     Self.guiaChange(guia);
     comboCredordevedorCR.SetFocus;
end;

procedure TFPagamento_e_geracao_emprestimo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Objlancamento.free;
end;

procedure TFPagamento_e_geracao_emprestimo.edtcodigoCredordevedorcpKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (comboCredordevedorCP.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.Create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(Self.objlancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(comboCredordevedorcodigoCP.text),'Pesquisa de '+comboCredordevedorCP.text,Self.objlancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(comboCredordevedorcodigoCP.text))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigoCredordevedorCP.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedorCP.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.objlancamento.Pendencia.titulo.Get_CampoNomeCredorDevedor(comboCredordevedorcodigoCP.text)).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFPagamento_e_geracao_emprestimo.edtcodigoCredordevedorCpExit(
  Sender: TObject);
begin
     LbNomeCredorDevedorCP.caption:='';
     If (edtcodigoCredordevedorCP.text='') or (comboCredordevedorCP.ItemIndex=-1)
     Then exit;
     LbNomeCredorDevedorCP.caption:=Self.objlancamento.Pendencia.Titulo.Get_NomeCredorDevedor(comboCredordevedorcodigoCP.text,edtcodigoCredordevedorCP.text);

end;

procedure TFPagamento_e_geracao_emprestimo.comboCredordevedorCpChange(
  Sender: TObject);
begin
 comboCredordevedorcodigoCP.itemindex:=comboCredordevedorCP.itemindex;
end;

procedure TFPagamento_e_geracao_emprestimo.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFPagamento_e_geracao_emprestimo.edtvalorlancamentoCPExit(Sender: TObject);
begin
 AtualizaLabelsCP;
end;

procedure TFPagamento_e_geracao_emprestimo.edtvalorlancamentoCPKeyPress(
  Sender: TObject; var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then Begin
                if key='.'
                Then key:=','
                Else Key:=#0;
     End;
end;

procedure TFPagamento_e_geracao_emprestimo.AtualizaLabelsCP;
var
cont:integer;
valor:Currency;
pvalorlancamento,psaldo,TotalCP:Currency;
begin
    TOTALCP:=0;
     for cont:=1 to STRGCP.RowCount-1 do
     Begin
          valor:=0;
          Try
             If (Trim(strgCP.Cells[8,cont])<>'')
             Then valor:=strtofloat(tira_ponto(strgCP.Cells[8,cont]));
          Except
                valor:=0;
          End;
          TotalCP:=TotalCP+Valor;
     End;

     Try
        PvalorLancamento:=0;
        if (edtvalorlancamentoCP.Text<>'')
        Then PvalorLancamento:=strtofloat(edtvalorlancamentoCP.Text);
     Except
        PvalorLancamento:=0;
     End;

     Try
        PSaldo:=Pvalorlancamento-TOTALCP;
     Except
        PSaldo:=0;
     End;

     LbValorLancamentoCP.caption:='VALOR DO PAGAMENTO   R$ '+CompletaPalavra_a_Esquerda(formata_valor(pvalorlancamento),15,' ');
     lbpaga.             caption:='TOTAL SELECIONADO    R$ '+CompletaPalavra_a_Esquerda(formata_valor(TOTALCP),15,' ');
     if (pvalorlancamento<>0)
     Then LbSaldoCP     .caption:='SALDO DO PAGAMENTO   R$ '+CompletaPalavra_a_Esquerda(formata_valor(psaldo),15,' ')
     Else LbSaldoCP     .caption:='SALDO DO PAGAMENTO   R$ '+CompletaPalavra_a_Esquerda(formata_valor('0'),15,' ')


end;

procedure TFPagamento_e_geracao_emprestimo.edtcodigohistoricoCPExit(Sender: TObject);
begin
     IF (TEdit(Sender).text='')
     Then exit;

     If (Self.objlancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.objlancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
     edthistoricoCP.text:=Self.objlancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
     TEdit(Sender).text:='';
end;

procedure TFPagamento_e_geracao_emprestimo.edtcodigohistoricoCPKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   FHistoricoSimples:TFHistoricoSimples;
begin
        If (key<>vk_f9)
        Then exit;

        Try
           Fpesquisalocal:=Tfpesquisa.Create(Self);
           FHistoricoSimples:=TFHistoricoSimples.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.objlancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,Self.objlancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                                  If (Self.objlancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
                                  Then Begin
                                            TEdit(Sender).text:='';
                                            exit;
                                  End;
                                  Self.objlancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
                                  edthistoricoCP.text:=Self.objlancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FHistoricoSimples);
        End;

end;

procedure TFPagamento_e_geracao_emprestimo.edtcodigohistoricoCPKeyPress(
  Sender: TObject; var Key: Char);
begin
   If (not (key in ['0'..'9',#8]))
     Then key:=#0;

end;

procedure TFPagamento_e_geracao_emprestimo.limpalabels;
begin
     LbNomeCredorDevedorCP.caption:='';
end;

procedure TFPagamento_e_geracao_emprestimo.guiaChange(Sender: TObject);
begin
     If Guia.ActivePage=GuiaContasaPagar
     Then Begin
               EDTVENCIMENTOCP.enabled:=True;
               edtdatalancamentoCP.enabled:=True;
               comboCredordevedorCP.enabled:=True;
               edtcodigoCredordevedorCP.enabled:=True;
               edtvalorlancamentoCP.enabled:=True;
               edtcodigohistoricoCP.enabled:=True;
               edthistoricoCP.enabled:=True;

               edtpesquisa_STRG_GRID_CP.enabled:=True;
               edtpesquisa_STRG_GRID_CP.Visible:=False;

               LbNomeCredorDevedorCP.caption:='';
               //comboCredordevedorCP.SetFocus;

               STRGCP.ColCount:=1;
               STRGCP.rowcount:=1;
               STRGCP.cols[0].clear;
               EDTVENCIMENTOCP.text:='';
               VencimentoCP:='';
               lbpaga.caption:='';
               LbValorLancamentoCP.caption:='';
               LbSaldoCP.caption:='';

               comboCredordevedorCP.Text:='';
               edtcodigoCredordevedorCP.Text:='';
               AtualizaLabelsCP;
     End;
end;

procedure TFPagamento_e_geracao_emprestimo.BtSelecionaTodas_CPClick(Sender: TObject);
var
cont:integer;
begin
     //seleciona todas com o valor do saldo
     For cont:=1 to STRGCP.RowCount-1 do
     Begin
          if (trim(STRGCP.cells[8,Cont])='')
          Then STRGCP.cells[8,Cont]:=STRGCP.cells[7,Cont];
     end;
     atualizalabelsCP;
     STRGCP.SetFocus;
end;

procedure TFPagamento_e_geracao_emprestimo.strgCPDblClick(Sender: TObject);
var
valor,valordogrid:currency;
begin

     //pegando o valor a ser pago
     limpaedit(Ffiltroimp);
     With Ffiltroimp do
     Begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.EditMask:='';
              edtgrupo01.text:=Trim(STRGCP.cells[7,STRGCP.row]);
              edtgrupo01.OnKeyPress:=nil;
              LbGrupo01.caption:='VALOR';
              showmodal;
              edtgrupo01.OnKeyPress:=nil;
              If Tag=0
              Then exit;  

              Try

                 valor:=strtofloat(tira_ponto(edtgrupo01.text));
                 valordogrid:=strtofloat(tira_ponto(STRGCP.cells[7,STRGCP.row]));
                 STRGCP.cells[8,STRGCP.row]:=formata_valor(floattostr(valor));
              Except
                    STRGCP.cells[8,STRGCP.row]:='';
              End;

     End;
     atualizalabelscp;
     STRGCP.SetFocus;
End;

procedure TFPagamento_e_geracao_emprestimo.strgCPEnter(Sender: TObject);
begin
edtpesquisa_STRG_GRID_CP.Visible:=false;
end;

procedure TFPagamento_e_geracao_emprestimo.strgCPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if (STRGCP.cells[0,1]='')
     Then exit;

     if (key=vk_f12)
     Then Ordena_StringGrid(STRGCP,LbtipoCampoCP.Items);

end;

procedure TFPagamento_e_geracao_emprestimo.strgCPKeyPress(Sender: TObject;
  var Key: Char);
begin
     If (Key=#13)
     Then STRGCP.OnDblClick(nil);

     if (key=#32)
     Then Begin
               if (STRGCP.cells[0,1]='')
               Then exit;
               PreparaPesquisa_StringGrid(STRGCP,edtpesquisa_STRG_GRID_CP,LbtipoCampoCP.Items);
     End;
end;

procedure TFPagamento_e_geracao_emprestimo.edtpesquisa_STRG_GRID_CPKeyPress(
  Sender: TObject; var Key: Char);
begin
 if key =#27//ESC
   then Begin
           edtpesquisa_STRG_GRID_CP.Visible := false;
           STRGCP.SetFocus;
           exit;
   End;

   if key=#13
   Then Begin
            Pesquisa_StringGrid(STRGCP,edtpesquisa_STRG_GRID_CP,LbtipoCampoCP.Items);
            if (edtpesquisa_STRG_GRID_CP.Visible=False)
            Then STRGCP.SetFocus;
   End;
end;

procedure TFPagamento_e_geracao_emprestimo.btexecutaCPClick(Sender: TObject);
begin
       If (STRGCP.RowCount<=1)
     Then exit;


     if (comboCredordevedorCR.ItemIndex=-1)
     then Begin
               mensagemErro('Escolha um fornecedor/cliente para gerar a conta a receber');
               exit;
     End;

     if (edtcodigocredordevedorCR.text='')
     then Begin
               mensagemErro('Escolha um fornecedor/cliente para gerar a conta a receber');
               exit;
     End;



     if (self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.LocalizaCodigo(combocredordevedorcodigoCR.text)=False)
     Then begin
               MensagemErro('Cliente/Fornecedor n�o encontrado para gerar a conta a receber');
               exit;
     End;
     self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.TabelaparaObjeto;

     if (self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.LocalizaCredorDevedor(edtcodigocredordevedorCR.text)=False)
     Then begin
               MensagemErro('Cliente/Fornecedor n�o encontrado para gerar a conta a receber');
               exit;
     End;

     Self.SalvaContasPagar;
     AtualizaLabelsCP;
end;

procedure TFPagamento_e_geracao_emprestimo.SalvaContasPagar;
var
Cont:Integer;
ContaPag:Integer;
ValorLanccomPai,ValorSoma:Currency;
HistoricoTodos:String;
PortadorUSadoNoPagamento,NFTEMP,PCCredorDevedor,CodigoLAncamentoPai,CodigoLancamentoFilho,TmptipoLancto,tmptipolanctoligado:string;
PCredorDevedorLote,PCodigoCredorDevedorLote:string;
Ppendencia,Pvalor:String;
Begin

     ContaPag:=0;

     for cont:=1 to STRGCP.RowCount-1 do
     Begin
          If (trim(StrgCP.Cells[8,cont])<>'')//verificando a coluna de valor a ser pago
          Then begin
                    Inc(ContaPag,1);
                    //guardando pendencia e o valor
                    Ppendencia:=StrgCP.cells[3,cont];
                    Pvalor:=trim(StrgCP.Cells[8,cont]);
          End;
     End;

     If (ContaPag=0)
     Then Begin
               Messagedlg('N�o foi escolhido nenhuma pend�ncia para ser paga!',mtinformation,[mbok],0);
               exit;
     End;

     Try
        strtodate(edtdatalancamentoCP.text);
     Except
          Messagedlg('Data Inv�lida para o lan�amento!',mterror,[mbok],0);
          exit;
     End;

     if (ContaPag=1)//somente uma pendencia
     then begin
               Self.SalvaContaspagar_UmaPendencia(ppendencia,tira_ponto(pvalor));
               exit;
     End;

     //Tenho Que Salvar um lancamento sem estar ligado a pendencia
     //nenhuma, apenas com o valor de todas elas
     //e um hist�rico PAG. DE PEND. DOS T�TULOS X,Y,Z....
     ValorSoma:=0;

     HistoricoTodos:='LOTE - ';
     PCredorDevedorLote:='';
     PCodigoCredorDevedorLote:='';

     for cont:=1 to StrgCP.RowCount-1 do
     Begin
          If (trim(StrgCP.Cells[8,cont])<>'')
          Then Begin
                    Try

                       Self.objlancamento.Pendencia.Titulo.LocalizaCodigo(trim(StrgCP.cells[0,cont]));
                       Self.objlancamento.Pendencia.Titulo.TabelaparaObjeto;

                       //******************************************************
                       {Se todas as pend�ncias forem do mesmo Credor devedor (cliente, fornecedor...)
                       entao as variaveis Credordevedorlote e codigoCredordevedorlote ficaram
                       com o codigo dele, assim quando mandar pra tela de pagamento, os cheques
                       de terceiro poderao resgatar automaticamente o nome o cpf do cadastro}
                       if (PCredorDevedorLote='')
                       Then Begin
                                 PCredorDevedorLote:=Self.objlancamento.Pendencia.Titulo.CredorDEVEDOR.Get_CODIGO;
                                 PCodigoCredorDevedorLote:=Self.objlancamento.Pendencia.Titulo.Get_CODIGOCredorDEVEDOR;
                       End
                       Else Begin

                                 if (PCredorDevedorLote<>Self.objlancamento.Pendencia.Titulo.CredorDEVEDOR.Get_CODIGO)
                                 or (PCodigoCredorDevedorLote<>Self.objlancamento.Pendencia.Titulo.Get_CODIGOCredorDEVEDOR)
                                 Then Begin
                                           //existem pendencias de Credordevedores diferentes
                                           PCredorDevedorLote:='-1';
                                           PcodigoCredorDevedorLote:='-1';
                                 End;
                       End;
                       //******************************************************
                       Strtofloat(TIRA_PONTO(trim(StrgCP.cells[8,cont])));
                       ValorSoma:=ValorSoma+Strtofloat(Tira_ponto(trim(StrgCP.cells[8,cont])));
                       if (edthistoricoCP.text='')
                       Then HistoricoTodos:=HistoricoTodos+' / '+StrgCP.cells[0,cont]+' NF '+Self.objlancamento.Pendencia.Titulo.Get_NUMDCTO;
                    Except

                    End;
          End;
     End;

     if (PCredorDevedorlote='-1')
     Then Begin
               PCredorDevedorLote:='';
               PCodigoCredorDevedorLote:='';
     End;

     if (edthistoricoCP.Text<>'')
     Then historicotodos:=historicotodos+edthistoricoCP.Text;

     If (ValorSoma=0)
     Then exit;

     //USado para os lancamentos filhos
     If (Self.objlancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO')=fALSE)
     Then Begin
                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO", n�o foi encontrado!',mterror,[mbok],0);
                  exit;
     End;
     Self.objlancamento.ObjGeraLancamento.TabelaparaObjeto;
     TmptipoLanctoLigado:=Self.objlancamento.ObjGeraLancamento.Get_TipoLAncto;
     //*******************************
      CodigoLAncamentoPai:=Self.objlancamento.Get_NovoCodigo;
      Self.objlancamento.ZerarTabela;

     //***********************************************************************
     //CHAMANDO A TELA DE QUITACAO
     if (Self.objlancamento.NovoLancamento('','P',floattostr(ValorSoma),codigolancamentopai,HistoricoTodos,False,False,True,PCredorDevedorLote,PCodigoCredorDevedorLote,edtdatalancamentoCP.text)=False)
     Then Begin
               FDataModulo.IBTransaction.RollbackRetaining;
               Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
               exit;
     End;

     //****************************************************************
     //salvando as pendencias sem gerar valor
     for cont:=1 to StrgCP.RowCount-1 do
     Begin
          If (Trim(StrgCP.Cells[8,cont])<>'')
          Then Begin
                    Try
                       Self.objlancamento.Pendencia.Titulo.LocalizaCodigo(StrgCP.cells[0,cont]);
                       Self.objlancamento.Pendencia.Titulo.TabelaparaObjeto;
                       NFTEMP:=Self.objlancamento.Pendencia.Titulo.get_numdcto;

                       Self.objlancamento.ZerarTabela;
                       CodigoLancamentoFilho:=Self.objlancamento.get_novocodigo;
                       Self.objlancamento.Submit_CODIGO(CodigoLancamentoFilho);
                       Self.objlancamento.Submit_Pendencia(StrgCP.cells[3,cont]);
                       Self.objlancamento.Submit_TipoLancto(tmptipolanctoligado);
                       Self.objlancamento.Submit_Valor(TIRA_PONTO(trim(StrgCP.Cells[8,cont])));
                       Self.objlancamento.Submit_Historico('RC.LOTE-'+StrgCP.Cells[1,cont]+' - '+NFTEMP);
                       Self.objlancamento.Submit_Data(edtdatalancamentoCP.text);
                       Self.objlancamento.Submit_LancamentoPai(CodigoLAncamentoPai);
                       Self.objlancamento.Status:=dsinsert;
                       If (Self.objlancamento.Salvar(False,False,'',true,true)=False)
                       Then Begin
                                 Self.objlancamento.Rolback;
                                 Messagedlg('Lan�amentos Cancelados!',mterror,[mbok],0);
                                 Self.objlancamento.Status:=dsInactive;
                                 exit;
                       End;
                       Codigolancamentofilho:=Self.objlancamento.Get_CODIGO;
                       //GUARDANDO O CODIGO DO LANCAMENTO FILHO NA LINHA DO STRING GRID
                       StrgCP.cells[13,cont]:=Codigolancamentofilho;
                    Except

                    End;
          End;
     End;


     if (Self.objlancamento.ExportaContabilidade_Pagamento_Lote(CodigoLAncamentoPai)=False)
     Then  Begin
                Self.objlancamento.Rolback;
                Messagedlg('Erro na tentativa de exportar a contabilidade do Pagamento',mterror,[mbok],0);
                exit;
     End;

     if (Self.GeraContaAReceber(CodigolancamentoPai)=False)
     Then Begin
                Self.objlancamento.Rolback;
                Messagedlg('Erro na tentativa de gerar o t�tulo a receber',mterror,[mbok],0);
                exit;
     End;

     FdataModulo.IBTransaction.CommitRetaining;

     edtvencimentoCP.text:=VencimentoCP;
     btpesquisaCP.OnClick(nil);
 end;

procedure TFPagamento_e_geracao_emprestimo.SalvaContaspagar_UmaPendencia(ppendencia,pvalor:String);
var
Phistorico:String;
Pcodigo:string;
begin
 //    Showmessage(Pvalor);
     phistorico:='';

     if (edthistoricoCP.Text='')
     Then Begin
              Self.objlancamento.Pendencia.LocalizaCodigo(ppendencia);
              Self.objlancamento.Pendencia.TabelaparaObjeto;
              Phistorico:='Pagamento '+Self.objlancamento.Pendencia.Titulo.CredorDEVEDOR.Get_NomeCredorDevedor(Self.objlancamento.Pendencia.Titulo.CredorDEVEDOR.get_codigo,Self.objlancamento.Pendencia.Titulo.Get_CODIGOCredorDEVEDOR);
     end
     Else Phistorico:=edthistoricoCP.text;
     
     Pcodigo:=Self.objlancamento.Get_NovoCodigo;

     
     //chamo essa versao do NovoLancamento pois o codigo retorna
     if (Self.objlancamento.NovoLancamento(ppendencia,'P',pvalor,pcodigo,phistorico,False,true,true,'','',datetostr(now))=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar',mterror,[mbok],0);
               desabilita_campos(self);
               exit;
     End;


     if (Self.GeraContaAReceber(PCodigo)=False)
     then exit;

     FdataModulo.IBTransaction.CommitRetaining;
     
     EdtvencimentoCP.text:=VencimentoCP;
     BtpesquisaCP.OnClick(nil);
end;


procedure TFPagamento_e_geracao_emprestimo.comboCredordevedorCRChange(
  Sender: TObject);
begin
 comboCredordevedorcodigoCR.itemindex:=comboCredordevedorCR.itemindex;
end;

procedure TFPagamento_e_geracao_emprestimo.edtcodigocredordevedorCRExit(
  Sender: TObject);
begin
     LbNomeCredorDevedorCR.caption:='';
     If (edtcodigoCredordevedorCR.text='') or (comboCredordevedorCR.ItemIndex=-1)
     Then exit;
     LbNomeCredorDevedorCR.caption:=Self.objlancamento.Pendencia.Titulo.Get_NomeCredorDevedor(comboCredordevedorcodigoCR.text,edtcodigoCredordevedorCR.text);

end;

procedure TFPagamento_e_geracao_emprestimo.edtcodigocredordevedorCRKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (comboCredordevedorCr.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.Create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(Self.objlancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(comboCredordevedorcodigoCR.text),'Pesquisa de '+comboCredordevedorCR.text,Self.objlancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(comboCredordevedorcodigoCr.text))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigoCredordevedorCr.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedorCr.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.objlancamento.Pendencia.titulo.Get_CampoNomeCredorDevedor(comboCredordevedorcodigoCr.text)).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


function TFPagamento_e_geracao_emprestimo.GeraContaaReceber(
  Plancamentoapagar: string): boolean;
var
   pdata:Tdate;
   ptituloaReceber:string;
   PsomaCobrador:Currency;
   Pcodigos:TStringList;
   ObjGeraTitulo:TobjgeraTitulo;
   cont:integer;
   Pcheque,Tmp1,tmp2,tmp3,tmp4:TStringList;
   PnumChequeUsado:String;
begin
     Result:=False;

     Try
         Pcheque:=TStringList.Create;
         Tmp1:=TStringList.Create;
         tmp2:=TStringList.Create;
         tmp3:=TStringList.Create;
         tmp4:=TStringList.Create;
     Except

     End;

     Try
         PnumChequeUsado:='';
         //localizando o cheque do portador que  foi usado na conta a pagar
         //para  mostrar no historico do titulo a receber e no numdocto
         ObjLanctoPortadorGlobal.TransferenciaPortador.LocalizaLancamentoporPortador(plancamentoapagar,tmp1,tmp2,tmp3,tmp4,Pcheque);
         if (Pcheque.Count>0)
         Then Begin
                   ObjLanctoPortadorGlobal.ValoresTransferenciaPortador.Valores.LocalizaCodigo(Pcheque[0]);
                   ObjLanctoPortadorGlobal.ValoresTransferenciaPortador.Valores.TabelaparaObjeto;
                   PnumChequeUsado:='CH '+ObjLanctoPortadorGlobal.ValoresTransferenciaPortador.Valores.Get_NumCheque;
                   for cont:=1 to Pcheque.count-1 do
                   Begin
                        ObjLanctoPortadorGlobal.ValoresTransferenciaPortador.Valores.LocalizaCodigo(Pcheque[cont]);
                        ObjLanctoPortadorGlobal.ValoresTransferenciaPortador.Valores.TabelaparaObjeto;
                        PnumChequeUsado:=PnumChequeUsado+'/'+'CH '+ObjLanctoPortadorGlobal.ValoresTransferenciaPortador.Valores.Get_NumCheque;
                   End;

         End;
         
     Finally
            Freeandnil(Pcheque);
            Freeandnil(Tmp1);
            Freeandnil(tmp2);
            Freeandnil(tmp3);
            Freeandnil(tmp4);
     End;






     if(Self.ObjLancamento.LocalizaCodigo(Plancamentoapagar)=False)
     then begin
               MensagemErro('Lan�amento a pagar n�mero '+plancamentoapagar+' n�o encontrado');
               exit;
     End;
     Self.ObjLancamento.TabelaparaObjeto;

     try
        ObjGeraTitulo:=TobjgeraTitulo.create;
     Except
        MensagemErro('N�o foi poss�vel criar a Query');
        exit;
     End;

     Try
        Objgeratitulo.ZerarTabela;
        if (ObjGeraTitulo.LocalizaHistorico('TITULO DE EMPR�STIMO A RECEBER')=False)
        then Begin
                  Mensagemerro('Gerador de T�tulo n�o encontrado "T�TULO DE EMPR�STIMO A RECEBER"');
                  exit;
        End;
        ObjGeraTitulo.TabelaparaObjeto;

        Self.ObjLancamento.Pendencia.Titulo.ZerarTabela;
        Self.ObjLancamento.Pendencia.Titulo.Status:=dsinsert;
        Self.ObjLancamento.Pendencia.Titulo.Submit_CODIGO(Self.ObjLancamento.Pendencia.Titulo.Get_NovoCodigo);
        Self.ObjLancamento.Pendencia.Titulo.Submit_HISTORICO(Self.objlancamento.Get_historico+' '+PnumChequeUsado);
        Self.ObjLancamento.Pendencia.Titulo.Submit_GERADOR(ObjGeraTitulo.Get_Gerador);
        Self.ObjLancamento.Pendencia.Titulo.Submit_CODIGOGERADOR(ObjGeraTitulo.Get_CodigoGerador);
        Self.ObjLancamento.Pendencia.Titulo.Submit_CREDORDEVEDOR(combocredordevedorcodigoCR.text);
        Self.ObjLancamento.Pendencia.Titulo.Submit_CODIGOCREDORDEVEDOR(edtcodigocredordevedorCR.text);
        Self.ObjLancamento.Pendencia.Titulo.Submit_EMISSAO(Self.ObjLancamento.get_data);
        Self.ObjLancamento.Pendencia.Titulo.Submit_PRAZO(ObjGeraTitulo.Get_Prazo);
        Self.ObjLancamento.Pendencia.Titulo.Submit_PORTADOR(ObjGeraTitulo.Get_Portador);
        Self.ObjLancamento.Pendencia.Titulo.Submit_VALOR(Self.ObjLancamento.get_valor);
        Self.ObjLancamento.Pendencia.Titulo.Submit_CONTAGERENCIAL(ObjGeraTitulo.Get_ContaGerencial);
        Self.ObjLancamento.Pendencia.Titulo.Submit_NUMDCTO(PnumChequeUsado);
        Self.ObjLancamento.Pendencia.Titulo.Submit_NotaFiscal('');
        Self.ObjLancamento.Pendencia.Titulo.Submit_GeradoPeloSistema('N');
        Self.ObjLancamento.Pendencia.Titulo.Submit_ExportaLanctoContaGer('N');
        Self.ObjLancamento.Pendencia.Titulo.Submit_ParcelasIguais(true);

        if (Self.ObjLancamento.Pendencia.Titulo.Salvar(False,True)=False)
        then Begin
                  mensagemerro('Erro na tentativa de salvar o t�tulo a receber');
                  exit; 
        End;
        result:=true;
     Finally
            ObjGeraTitulo.free;
     End;

end;

end.
