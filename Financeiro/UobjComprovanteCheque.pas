unit UobjComprovanteCheque;
Interface
Uses windows,ibquery,rdprint,printers,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjchequesPortador;

Type
   TObjComprovanteCheque=class

          Public
                //ObjDatasource                               :TDataSource;
                Cheque           :TobjChequesPortador;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaCheque(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Cheque           :string; 
                Function Get_PagoA            :string;
                Function Get_Nominador        :string;
                Function Get_descricaorecibo  :string;
                Function Get_descricaocomprovante:string;


                Function Get_Comprovante      :string;
                Function Get_Providencias     :string;
                Function Get_Cidade           :string;
                Function Get_Data             :string;
                Function Get_NomeRecebedor    :string;
                Function Get_CPFCNPJRecebedor :string;

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Cheque           (parametro:string);
                Procedure Submit_PagoA            (parametro:string);
                Procedure Submit_Nominador        (parametro:string);
                Procedure Submit_descricaorecibo        (parametro:string);
                Procedure Submit_descricaocomprovante(parametro:string);
                Procedure Submit_Comprovante      (parametro:string);
                Procedure Submit_Providencias     (parametro:string);
                Procedure Submit_Cidade           (parametro:string);
                Procedure Submit_Data             (parametro:string);
                Procedure Submit_NomeRecebedor    (parametro:string);
                Procedure Submit_CPFCNPJRecebedor (parametro:string);

                Function  Get_NovoCodigo:string;
                Procedure Imprime(PParametro:string);
                Procedure ImprimeComprovanteCheque(parametrocodigo:string);
                Procedure AlteraSequencia;

         Private
               ObjDataset:Tibdataset;

               CODIGO           :string;

               PagoA            :string;
               Nominador        :string;
               descricaorecibo  :string;
               descricaocomprovante:string;
               Comprovante      :string;
               Providencias     :string;
               Cidade           :string;
               Data             :string;
               NomeRecebedor    :string;
               CPFCNPJRecebedor :string;

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure ImprimeComprovanteChequeC_Recibo(parametrocodigo: string);
               Procedure ImprimeComprovanteChequeS_Recibo(parametrocodigo: string);
               procedure ImprimeComprovanteCheque_modelo_2(parametrocodigo:string);
               procedure ImprimeComprovanteCheque_Sequencia;
               Procedure EdtKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);


End;


implementation
uses stdctrls,SysUtils,Dialogs,UDatamodulo,Controls,
Uobjconfrelatorio,Urelcomprovantecheque,UrelComprovanteChequeSRecibo,
  UMenuRelatorios, URelPedidoRdPrint, UObjTalaodeCheques, UFiltraImp,
  Upesquisa;


{ TTabTitulo }


Function  TObjComprovanteCheque.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        
        If (Self.Cheque.LocalizaCodigo(FieldByName('Cheque').asstring)=False)
        Then Begin
                  Messagedlg('Cheque N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  result:=False;
                  exit;
        End
        Else Self.Cheque.TabelaparaObjeto;
        
        Self.PagoA:=FieldByName('PAGOA').asstring;
        Self.Nominador:=FieldByName('Nominador').asstring;
        Self.descricaorecibo:=FieldByName('descricaorecibo').asstring;
        Self.Comprovante:=FieldByName('Comprovante').asstring;
        Self.Providencias:=FieldByName('Providencias').asstring;
        Self.Cidade:=FieldByName('Cidade').asstring;
        Self.Data:=FieldByName('Data').asstring;
        Self.NomeRecebedor:=FieldByName('NomeRecebedor').asstring;
        Self.CPFCNPJRecebedor:=FieldByName('CPFCNPJRecebedor').asstring;
        Self.descricaocomprovante:=FieldByName('descricaocomprovante').asstring;

        
        result:=True;
     End;
end;


Procedure TObjComprovanteCheque.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring            :=Self.CODIGO                   ;
      FieldByName('Cheque').asstring            :=Self.Cheque.get_codigo        ;
      FieldByName('PAGOA').asstring             :=Self.PagoA                    ;
      FieldByName('Nominador').asstring         :=Self.Nominador                ;
      FieldByName('descricaorecibo').asstring   :=Self.descricaorecibo                ;
      FieldByName('descricaocomprovante').asstring:=Self.descricaocomprovante;
      FieldByName('Comprovante').asstring       :=Self.Comprovante              ;
      FieldByName('Providencias').asstring      :=Self.Providencias             ;
      FieldByName('Cidade').asstring            :=Self.Cidade                   ;
      FieldByName('Data').asstring              :=Self.Data                     ;
      FieldByName('NomeRecebedor').asstring     :=Self.NomeRecebedor            ;
      FieldByName('CPFCNPJRecebedor').asstring  :=Self.CPFCNPJRecebedor         ;

  End;
End;

//***********************************************************************

function TObjComprovanteCheque.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjComprovanteCheque.ZerarTabela;//Ok
Begin
     Self.CODIGO             :='';
     Self.Cheque.ZerarTabela;
     Self.PagoA              :='';
     Self.Nominador          :='';
     Self.descricaorecibo          :='';
     Self.descricaocomprovante:='';
     Self.Comprovante        :='';
     Self.Providencias       :='';
     Self.Cidade             :='';
     Self.Data               :='';
     Self.NomeRecebedor      :='';
     Self.CPFCNPJRecebedor   :='';
end;

Function TObjComprovanteCheque.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Cheque.Get_Codigo='')
       Then Mensagem:=mensagem+'/Cheque';

       If (Cidade='')
       Then Mensagem:=mensagem+'/Cidade';

       If (Data='')
       Then Mensagem:=mensagem+'/Data';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjComprovanteCheque.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.Cheque.LocalizaCodigo(Self.Cheque.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Cheque n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjComprovanteCheque.VerificaNumericos: Boolean;
var
   Mensagem:string;
begin
     Mensagem:='';

     try
        Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjComprovanteCheque.VerificaData: Boolean;
var
Mensagem:string;
begin
     mensagem:='';

     Try
        strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     {Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjComprovanteCheque.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjComprovanteCheque.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Cheque,PagoA,Nominador,descricaorecibo,descricaocomprovante,Comprovante,');
           SelectSql.add('Providencias,Cidade,Data,NomeRecebedor,CPFCNPJRecebedor From TabComprovanteCheque');
           SelectSql.add('Where codigo='+Parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjComprovanteCheque.LocalizaCheque(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Cheque,PagoA,Nominador,descricaorecibo,descricaocomprovante,Comprovante,');
           SelectSql.add('Providencias,Cidade,Data,NomeRecebedor,CPFCNPJRecebedor From TabComprovanteCheque');
           SelectSql.add('Where cheque='+Parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

procedure TObjComprovanteCheque.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjComprovanteCheque.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjComprovanteCheque.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        //Self.ObjDatasource:=TDataSource.Create(nil);
        //Self.CodCurso:=TObjCursos.create;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Cheque:=TObjChequesPortador.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Cheque,PagoA,Nominador,descricaorecibo,descricaocomprovante,Comprovante,');
                SelectSql.add('Providencias,Cidade,Data,NomeRecebedor,CPFCNPJRecebedor From TabComprovanteCheque');
                SelectSql.add('Where codigo=-1');


                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabComprovanteCheque (CODIGO,Cheque,PagoA,Nominador,descricaorecibo,descricaocomprovante,Comprovante,');
                InsertSQL.add('Providencias,Cidade,Data,NomeRecebedor,CPFCNPJRecebedor) values');
                InsertSQL.add('(:CODIGO,:Cheque,:PagoA,:Nominador,:descricaorecibo,:descricaocomprovante,:Comprovante,');
                InsertSQL.add(':Providencias,:Cidade,:Data,:NomeRecebedor,:CPFCNPJRecebedor)');


                ModifySQL.clear;
                ModifySQL.add('Update TabComprovanteCheque set CODIGO=:CODIGO,Cheque=:Cheque,PagoA=:PagoA,Nominador=:Nominador,descricaorecibo=:descricaorecibo,descricaocomprovante=:descricaocomprovante,Comprovante=:Comprovante,');
                ModifySQL.add('Providencias=:Providencias,Cidade=:cidade,Data=:Data,NomeRecebedor=:NomeRecebedor,CPFCNPJRecebedor=:CPFCNPJRecebedor');
                ModifySQL.add('Where codigo=:codigo');

                DeleteSQL.clear;
                DeleteSQL.add('Delete from TabCOmprovanteCheque where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Cheque,PagoA,Nominador,descricaorecibo,descricaocomprovante,Comprovante,');
                RefreshSQL.add('Providencias,Cidade,Data,NomeRecebedor,CPFCNPJRecebedor From TabComprovanteCheque');
                RefreshSQL.add('Where codigo=-1');


                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjComprovanteCheque.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjComprovanteCheque.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjComprovanteCheque.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjComprovanteCheque.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabComprovanteCheque');
     Result:=Self.ParametroPesquisa;
end;

function TObjComprovanteCheque.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ComprovanteCheque ';
end;



{
function TObjComprovanteCheque.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjComprovanteCheque.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjComprovanteCheque.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_COMPROVANTECHEQUE';
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o ComprovanteCheque',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjComprovanteCheque.Free;
begin
Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);
   Self.Cheque.Free;

end;

function TObjComprovanteCheque.Get_Cheque: string;
begin
     Result:=Self.Cheque.Get_codigo;
end;

function TObjComprovanteCheque.Get_Comprovante: string;
begin
     Result:=Self.Comprovante;
end;

function TObjComprovanteCheque.Get_CPFCNPJRecebedor: string;
begin
     Result:=Self.CPFCNPJRecebedor;
end;

function TObjComprovanteCheque.Get_Data: string;
begin
     Result:=Self.Data;
end;

function TObjComprovanteCheque.Get_descricaorecibo: string;
begin
     Result:=Self.descricaorecibo;
end;

function TObjComprovanteCheque.Get_NomeRecebedor: string;
begin
     Result:=Self.NomeRecebedor;
end;

function TObjComprovanteCheque.Get_Nominador: string;
begin
     Result:=Self.Nominador;
end;

function TObjComprovanteCheque.Get_PagoA: string;
begin
     Result:=Self.PagoA;
end;

function TObjComprovanteCheque.Get_Providencias: string;
begin
     Result:=Self.Providencias;
end;

procedure TObjComprovanteCheque.Submit_Cheque(parametro: string);
begin
     Self.Cheque.Submit_codigo(parametro);
end;

procedure TObjComprovanteCheque.Submit_Comprovante(parametro: string);
begin
     Self.Comprovante:=parametro;
end;

procedure TObjComprovanteCheque.Submit_CPFCNPJRecebedor(parametro: string);
begin
     Self.CPFCNPJRecebedor:=parametro;
end;

procedure TObjComprovanteCheque.Submit_Data(parametro: string);
begin
     Self.Data:=parametro;
end;

procedure TObjComprovanteCheque.Submit_descricaorecibo(parametro: string);
begin
     Self.descricaorecibo:=parametro;
end;

procedure TObjComprovanteCheque.Submit_NomeRecebedor(parametro: string);
begin
     Self.NomeRecebedor:=parametro;
end;

procedure TObjComprovanteCheque.Submit_Nominador(parametro: string);
begin
     Self.Nominador:=parametro;
end;

procedure TObjComprovanteCheque.Submit_PagoA(parametro: string);
begin
     Self.PagoA:=parametro;
end;

procedure TObjComprovanteCheque.Submit_Providencias(parametro: string);
begin
     Self.Providencias:=parametro;
end;

function TObjComprovanteCheque.Get_Cidade: string;
begin
     Result:=Self.Cidade;
end;

procedure TObjComprovanteCheque.Submit_Cidade(parametro: string);
begin
     Self.cidade:=Parametro;
end;

procedure TObjComprovanteCheque.Imprime(PParametro: string);
begin
     With FMenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOMPROVANTECHEQUE';
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Comprovante de Emiss�o de Cheque');//0
                items.add('Comprovante de Emiss�o de Cheque - Sequ�ncia');//0

          End;
          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0:Self.ImprimeComprovanteCheque(PParametro);
                1:Self.ImprimeComprovanteCheque_Sequencia;
          End;

     end;
End;

Procedure TObjComprovanteCheque.ImprimeComprovanteCheque_Sequencia;
var
ObjQueryTemp:tibquery;
PcomprovanteInicial,PcomprovanteFinal:string;
begin

     With ffiltroimp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          LbGrupo01.caption:='Comprovante Inicial';
          LbGrupo02.caption:='Comprovante Final';
          edtgrupo01.OnKeyDown:=Self.edtkeydown;
          edtgrupo02.OnKeyDown:=Self.edtkeydown;


          Showmodal;

          if (edtgrupo01.Text='')
          then Begin
                    MensagemErro('� necess�rio escolher o comprovante inicial');
                    exit;
          End
          Else Begin
                    Try
                       Strtoint(edtgrupo01.Text);
                       PcomprovanteInicial:=edtgrupo01.Text;
                    Except
                          mensagemErro('Valor Inv�lido para o Comprovante Inicial');
                          exit;
                    End;

          End;

          if (edtgrupo02.Text='')
          then Begin
                    MensagemErro('� necess�rio escolher o comprovante final');
                    exit;
          End
          Else Begin
                    Try
                       Strtoint(edtgrupo02.Text);
                       PcomprovanteFinal:=edtgrupo02.Text;
                    Except
                          mensagemErro('Valor Inv�lido para o Comprovante Final');
                          exit;
                    End;

          End;
     End;

     Try
        ObjQueryTemp:=TIBQuery.create(nil);
        ObjQueryTemp.Database:=FDataModulo.IBDatabase;
     Except
        MensagemErro('Erro na tentativa de criar a QueryTemp');
        exit;
     End;

     Try
        Objquerytemp.close;
        Objquerytemp.sql.clear;
        Objquerytemp.sql.add('Select codigo from TabComprovanteCheque where codigo>='+Pcomprovanteinicial+' and codigo<='+pComprovantefinal);
        Objquerytemp.sql.add('order by codigo');
        Objquerytemp.open;
        if (Objquerytemp.recordcount=0)
        then Begin
                  mensagemAviso('N�o existem comprovantes na faixa dos n�meros escolhidos');
                  exit;
        End;
        Objquerytemp.first;

        while not(Objquerytemp.eof) do
        Begin
            Self.ImprimeComprovanteCheque(ObjQueryTemp.fieldbyname('codigo').asstring);
            Objquerytemp.next;
        end;
        MensagemAviso('Impress�o conclu�da');
     Finally
            Freeandnil(Objquerytemp);
     End;


End;


procedure TObjComprovanteCheque.ImprimeComprovanteCheque(parametrocodigo: string);
Begin
     If (ObjParametroGlobal.ValidaParametro('MODELO DE COPIA DE CHEQUE')=False)
     Then exit;

     

     if (ObjParametroGlobal.Get_Valor='1')
     Then Begin
               If (ObjParametroGlobal.Validaparametro('COMPROVANTE COM RECIBO')=fALSE)
               Then Begin
                         Messagedlg('O Par�metro "Comprovante com Recibo" com valores "SIM" ou  "N�O" n�o foi encontrado!'+#13+'Ser� usado o modelo com recibo',mterror,[mbok],0);
                         Self.ImprimeComprovanteChequeC_Recibo(parametrocodigo);
                         exit;
               End;

               If (ObjParametroGlobal.Get_Valor='SIM')
               Then Self.ImprimeComprovanteChequeC_Recibo(parametrocodigo)
               Else Self.ImprimeComprovanteChequeS_Recibo(parametrocodigo);
     end
     Else Begin
               Self.ImprimeComprovanteCheque_modelo_2(parametrocodigo);
     End;

End;


Procedure TobjComprovanteCheque.ImprimeComprovanteCheque_modelo_2(parametrocodigo:string);
var
ValorExtensoCheque,extenso1,extenso2:String;
quantcaractereschequelinha:Integer;
Tmpano,TmpMes,TmpDia:word;
ObjTalaodeCheque:TObjTalaodeCheques;
Begin

     If (Self.LocalizaCodigo(parametrocodigo)=False)
     Then Begin
               Messagedlg('Comprovante n�o Localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     Try
        ObjTalaodeCheque:=TObjTalaodeCheques.create;
     Except
        MensagemErro('Erro na tentativa de criar o objeto de tal�o de cheques');
        exit;
     End;

     Try
         if (ObjTalaodeCheque.LocalizaChequePortador(Self.Get_cheque)=False)
         Then Begin
                   MensagemErro('Cheque n�o localizado no tal�o de cheques');
                   exit;
         End;
         ObjTalaodeCheque.TabelaparaObjeto;

        With FRelPedidoRdPrint do
        Begin
                //preparando a impress�o
                ConfiguraImpressao;
                imprimecabecalho:=false;
                ImprimeNumeroPagina:=false;
                rdprint1.Abrir;
                RDprint1.Orientacao:=poPortrait;

                if (rdprint1.OpcoesPreview.Preview=False)
                Then Begin
                          rdprint1.Fechar;
                          exit;
                End;
                linhalocal:= 4;

                //FreltxtRDPRINT.verificalinha(RDprint1,linhalocal);

                rdprint1.Box(2,1,15,93,false);
                rdprint1.Box(15,1,31,93,false);
                rdprint1.imp(linhalocal,2,completapalavra_a_esquerda('R$ '+formata_valor(Self.Cheque.Get_Valor),90,' '));
                Inc(linhalocal,1);
                //******************************
                ValorExtensoCheque:='';
                extenso1:='';
                extenso2:='';
                ValorExtensoCheque:=valorextenso(Strtofloat(Self.Cheque.Get_Valor));
                DividirValor(ValorExtensoCheque,quantcaractereschequelinha,90,extenso1,extenso2);
                //*******************************
                rdprint1.impf(linhalocal,2,completapalavra(extenso1,90,' '),[sublinhado]);
                Inc(linhalocal,2);
                rdprint1.impf(linhalocal,2,completapalavra(extenso2,90,' '),[sublinhado]);
                Inc(linhalocal,2);
                rdprint1.impf(linhalocal,2,completapalavra(self.Nominador,90,' '),[sublinhado]);
                Inc(linhalocal,2);

                DecodeDate(strtodate(Self.Data),Tmpano,TmpMes,TmpDia);
                
                rdprint1.imp(linhalocal,2,CompletaPalavra_a_Esquerda(self.cidade+' '+Inttostr(TmpDia)+' de '+MesExtenso(tmpmes)+' de '+inttostr(tmpano),90,' '));
                Inc(linhalocal,2);

                rdprint1.imp(linhalocal,2,CompletaPalavra_a_Esquerda('____________________________' ,90,' '));
                Inc(linhalocal,4);

                rdprint1.imp(linhalocal,2,CompletaPalavra('C�pia de Cheque n� '+Self.Get_CODIGO,40,' ')+
                                          CompletaPalavra('N� CH '+Self.Cheque.Get_NumCheque,20,' ')+
                                          CompletaPalavra_a_Esquerda(' [ ] Visado  [ ] Cruzado',30,' '));
                Inc(linhalocal,1);
                imprimelinha(2,91);
                rdprint1.imp(linhalocal,2,CompletaPalavra('do Banco: '+ObjTalaodeCheque.Portador.Get_Nome,90,' '));
                Inc(linhalocal,1);
                imprimelinha(2,91);
                rdprint1.imp(linhalocal,2,CompletaPalavra('Utilizado Para: '+ObjTalaodeCheque.Lancamento.Get_Historico,90,' '));
                Inc(linhalocal,1);
                imprimelinha(2,91);
                rdprint1.imp(linhalocal,10,CompletaPalavra('Vistos',9,' '));
                rdprint1.imp(linhalocal,20,CompletaPalavra('Contador',9,' '));
                rdprint1.imp(linhalocal,30,CompletaPalavra('Caixa',15,' '));
                rdprint1.imp(linhalocal,46,CompletaPalavra('          1',23,' '));
                rdprint1.imp(linhalocal,70,CompletaPalavra('Cheque recebido por',20,' '));

                RDprint1.LinhaV(19,linhalocal,linhalocal+7,0,false);
                RDprint1.LinhaV(29,linhalocal,linhalocal+7,0,false);
                RDprint1.LinhaV(45,linhalocal,linhalocal+7,0,false);
                RDprint1.LinhaV(69,linhalocal,linhalocal+7,0,false);  // Mexi aki

                Inc(linhalocal,1);
                imprimelinha(2,91);
                RDprint1.LinhaV(11,linhalocal,linhalocal+5,0,false);
                RDprint1.imp(linhalocal,30,CompletaPalavra('C/ Corrente',15,' '));
                RDprint1.imp(linhalocal,46,CompletaPalavra('    '+ObjTalaodeCheque.Portador.Get_NumeroConta,23,' '));
                Inc(linhalocal,1);
                RDprint1.imp(linhalocal,73,'___ / ___ / ______');

                imprimelinha(30,39);
                RDprint1.imp(linhalocal,30,CompletaPalavra('Tal�o',15,' '));
                Inc(linhalocal,1);
                imprimelinha(30,39);

                {

       //*******************************************
       FrelComprovanteCheque.memodescricaoComprovante.lines.text:=Self.descricaorecibo+'CONFORME ANEXO';
       {FrelComprovanteCheque.memodescricaoComprovante.lines.text:='';
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');

       If (length(Self.descricaocomprovante)>(quantcaractereslinhadescricao*quantidadelinhasdescricao))
       Then FrelComprovanteCheque.memodescricaoComprovante.lines.text:=Self.descricaorecibo+' - CONFORME ANEXO'
       Else Begin
                 FrelComprovanteCheque.memodescricaoComprovante.lines.clear;
                 //criando as linhas
                 for cont:=1 to quantidadelinhasdescricao do
                        FrelComprovanteCheque.memodescricaoComprovante.lines.add('');

                 linhaatual:=0;
                 //dividindo em varias linhas

                 for cont:=1 to length(Self.descricaocomprovante) do
                 Begin
                      if (cont mod quantcaractereslinhadescricao=0)
                      Then inc(linhaatual,1);
                      iF (LINHAATUAL<QUANTIDADELINHASDESCRICAO)
                      tHEN FrelComprovanteCheque.memodescricaoComprovante.lines[linhaatual]:=FrelComprovanteCheque.memodescricaoComprovante.lines[linhaatual]+Self.descricaocomprovante[cont];
                 End;

       End;
       //********8***********************************
       FrelComprovanteCheque.Lbdescricaorecibo.caption:=Self.descricaorecibo;
       FrelComprovanteCheque.Lbcomprovante.caption:=Self.Comprovante;
       //Dados do Cheque
       FrelComprovanteCheque.LbAgencia.caption:=Self.Cheque.portador.get_agencia;
       FrelComprovanteCheque.LbBanco.caption:=Self.Cheque.portador.Get_Nome;
       FrelComprovanteCheque.LbNumeroCheque.caption:=Self.Cheque.Get_NumCheque;
       FrelComprovanteCheque.LbConta.caption:=Self.Cheque.portador.Get_NumeroConta;
       //*******
       FrelComprovanteCheque.LbProvidencias.caption:=Self.Providencias;
       FrelComprovanteCheque.LbNomeRecebedor.caption:=Self.NomeRecebedor;
       FrelComprovanteCheque.LbCPFCNPJRecebedor.caption:=Self.CPFCNPJRecebedor;
       FrelComprovanteCheque.lbconferido.caption:='CONFERIDO';
       FrelComprovanteCheque.lbdataconferido.caption:= '___/___/___';
       FrelComprovanteCheque.lblinhaconferido.caption:='___________';
       FrelComprovanteCheque.lbassinaturaconferido.caption:='ASSINATURA';


       FrelComprovanteCheque.IBQuery1.close;
       FrelComprovanteCheque.IBQuery1.database:=FdataModulo.IBDatabase;
       FrelComprovanteCheque.IBQuery1.SQL.clear;
       FrelComprovanteCheque.IBQuery1.sql.add('Select codigo from Tabparametros where codigo=-1');
       FrelComprovanteCheque.IBQuery1.open;

       If (messagedlg('Pressione <OK> para imprimir ou <CANCELAR> para visualizar a impress�o do Comprovante!',mtinformation,[mbok,mbcancel],0)=MrCAncel)
       Then FrelComprovanteCheque.Qr.preview
       Else FrelComprovanteCheque.Qr.print;}

        End;
     Finally
            FRelPedidoRdPrint.RDprint1.Fechar;
            FRelPedidoRdPrint.imprimecabecalho:=True;
            ObjTalaodeCheque.free;
     End;

End;


procedure TObjComprovanteCheque.ImprimeComprovanteChequeC_Recibo(
  parametrocodigo: string);
//usado pela Gesil
var
TempObjConfRelatorio:tobjconfrelatorio;
ValorExtensoCheque,extenso1,extenso2:String;
linhaatual,cont,quantcaractereschequelinha:Integer;
quantcaractereslinhadescricao,quantidadelinhasdescricao:integer;
Tmpano,TmpMes,TmpDia:word;

Begin


     If (Self.LocalizaCodigo(parametrocodigo)=False)
     Then Begin
               Messagedlg('Comprovante n�o Localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;
     Try
        quantcaractereschequelinha:=1;
        TempObjConfRelatorio:=TObjConfRelatorio.create;
        quantcaractereschequelinha:=2;
     Except
           If (quantcaractereschequelinha=1)
           Then Messagedlg('Erro na cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0)
           Else Begin
                     TempObjConfRelatorio.Free;
                     If (quantcaractereschequelinha=2)
                     Then Messagedlg('Erro na cria��o do Objeto de Parametros!',mterror,[mbok],0);
                End;
           exit;
     End;
     Try

       If (ObjParametroGlobal.Validaparametro('QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;


       Try
          quantcaractereschequelinha:=StrToInt(ObjParametroGlobal.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres no valor extenso do Cheque!',mterror,[mbok],0);
             exit;
       End;
       //***********************************************
       //Referente ao campo descricao do comprovante de pagamento

       If (ObjParametroGlobal.Validaparametro('QUANTIDADE DE CARACTERES LINHA 1 DESC. COMPROVANTE')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 1 DESC. COMPROVANTE" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;


       Try
          quantcaractereslinhadescricao:=StrToInt(ObjParametroGlobal.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres na linha da descri��o do comprovante!',mterror,[mbok],0);
             exit;
       End;
       If (ObjParametroGlobal.Validaparametro('QUANTIDADE DE LINHAS NA DESCRICAO DO COMPROVANTE')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE LINHAS NA DESCRICAO DO COMPROVANTE" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;


       Try
          quantidadelinhasdescricao:=StrToInt(ObjParametroGlobal.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de linhas na descri��o do comprovante!',mterror,[mbok],0);
             exit;
       End;
       //***********************************************
       TempObjConfRelatorio.RecuperaArquivoRelatorio(FrelcomprovanteCheque,'IMPRIMECOMPROVANTE');
       FrelComprovanteCheque.LbValor.caption:='R$ '+formatfloat('###,##0.00',Strtofloat(Self.Cheque.Get_Valor));
       FrelComprovanteCheque.LbValorComprovante.caption:='R$ '+formatfloat('###,##0.00',Strtofloat(Self.Cheque.Get_Valor));
       //**************************
       ValorExtensoCheque:='';
       extenso1:='';
       extenso2:='';
       ValorExtensoCheque:=valorextenso(Strtofloat(Self.Cheque.Get_Valor));
       DividirValor(ValorExtensoCheque,quantcaractereschequelinha,200,extenso1,extenso2);
       //**************************
       FrelComprovanteCheque.LbExtenso1.caption:=extenso1;
       FrelComprovanteCheque.LbExtenso2.caption:=extenso2;
       FrelComprovanteCheque.LbExtensoComprovante.caption:=ValorExtensoCheque;
       FrelComprovanteCheque.LbNominal.caption:=Self.Nominador;
       FrelComprovanteCheque.LbCidadeCheque.caption:=Self.Cidade;
       FrelComprovanteCheque.LbCidadeComprovante.caption:=Self.Cidade;
       //****************************************
       DecodeDate(strtodate(Self.Data),Tmpano,TmpMes,TmpDia);
       FrelComprovanteCheque.LbDiaCheque.caption:=Inttostr(TmpDia);
       FrelComprovanteCheque.LbDiaComprovante.caption:=Inttostr(TmpDia);
       FrelComprovanteCheque.LbMesCheque.caption:=MesExtenso(TmpMes);
       FrelComprovanteCheque.LbMesComprovante.caption:=MesExtenso(TmpMes);
       FrelComprovanteCheque.LbAnoCheque.caption:=Inttostr(TmpAno);
       FrelComprovanteCheque.LbAnoComprovante.caption:=Inttostr(TmpAno);
       //*******************************************
       FrelComprovanteCheque.memodescricaoComprovante.lines.text:=Self.descricaorecibo+'CONFORME ANEXO';
       {FrelComprovanteCheque.memodescricaoComprovante.lines.text:='';
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');
       FrelComprovanteCheque.memodescricaoComprovante.lines.add('123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X');}

       If (length(Self.descricaocomprovante)>(quantcaractereslinhadescricao*quantidadelinhasdescricao))
       Then FrelComprovanteCheque.memodescricaoComprovante.lines.text:=Self.descricaorecibo+' - CONFORME ANEXO'
       Else Begin
                 FrelComprovanteCheque.memodescricaoComprovante.lines.clear;
                 //criando as linhas
                 for cont:=1 to quantidadelinhasdescricao do
                        FrelComprovanteCheque.memodescricaoComprovante.lines.add('');

                 linhaatual:=0;
                 //dividindo em varias linhas

                 for cont:=1 to length(Self.descricaocomprovante) do
                 Begin
                      if (cont mod quantcaractereslinhadescricao=0)
                      Then inc(linhaatual,1);
                      iF (LINHAATUAL<QUANTIDADELINHASDESCRICAO)
                      tHEN FrelComprovanteCheque.memodescricaoComprovante.lines[linhaatual]:=FrelComprovanteCheque.memodescricaoComprovante.lines[linhaatual]+Self.descricaocomprovante[cont];
                 End;

       End;
       //********8***********************************
       FrelComprovanteCheque.Lbdescricaorecibo.caption:=Self.descricaorecibo;
       FrelComprovanteCheque.Lbcomprovante.caption:=Self.Comprovante;
       //Dados do Cheque
       FrelComprovanteCheque.LbAgencia.caption:=Self.Cheque.portador.get_agencia;
       FrelComprovanteCheque.LbBanco.caption:=Self.Cheque.portador.Get_Nome;
       FrelComprovanteCheque.LbNumeroCheque.caption:=Self.Cheque.Get_NumCheque;
       FrelComprovanteCheque.LbConta.caption:=Self.Cheque.portador.Get_NumeroConta;
       //*******
       FrelComprovanteCheque.LbProvidencias.caption:=Self.Providencias;
       FrelComprovanteCheque.LbNomeRecebedor.caption:=Self.NomeRecebedor;
       FrelComprovanteCheque.LbCPFCNPJRecebedor.caption:=Self.CPFCNPJRecebedor;
       FrelComprovanteCheque.lbconferido.caption:='CONFERIDO';
       FrelComprovanteCheque.lbdataconferido.caption:= '___/___/___';
       FrelComprovanteCheque.lblinhaconferido.caption:='___________';
       FrelComprovanteCheque.lbassinaturaconferido.caption:='ASSINATURA';


       FrelComprovanteCheque.IBQuery1.close;
       FrelComprovanteCheque.IBQuery1.database:=FdataModulo.IBDatabase;
       FrelComprovanteCheque.IBQuery1.SQL.clear;
       FrelComprovanteCheque.IBQuery1.sql.add('Select codigo from Tabparametros where codigo=-1');
       FrelComprovanteCheque.IBQuery1.open;

       If (messagedlg('Pressione <OK> para imprimir ou <CANCELAR> para visualizar a impress�o do Comprovante!',mtinformation,[mbok,mbcancel],0)=MrCAncel)
       Then FrelComprovanteCheque.Qr.preview
       Else FrelComprovanteCheque.Qr.print;
       
     Finally
            freeandnil(TempObjconfrelatorio);
     End;

End;


Procedure TObjComprovanteCheque.ImprimeComprovanteChequeS_Recibo(
  parametrocodigo: string);
//usado pelo Sintraesul
var
TempObjConfRelatorio:tobjconfrelatorio;
ValorExtensoCheque,extenso1,extenso2:String;
linhaatual,cont,quantcaractereschequelinha:Integer;
quantcaractereslinhadescricao,quantidadelinhasdescricao:integer;
Tmpano,TmpMes,TmpDia:word;
FrelComprovanteChequeSReciboX:TFrelComprovanteChequeSRecibo;
Begin
Try
     FrelComprovanteChequeSReciboX:=TFrelComprovanteChequeSRecibo.create(nil);

     If (Self.LocalizaCodigo(parametrocodigo)=False)
     Then Begin
               Messagedlg('Comprovante n�o Localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     Try
        quantcaractereschequelinha:=1;
        TempObjConfRelatorio:=TObjConfRelatorio.create;
        quantcaractereschequelinha:=2;
     Except
           If (quantcaractereschequelinha=1)
           Then Messagedlg('Erro na cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0)
           Else Begin
                     freeandnil(TempObjConfRelatorio);
                     If (quantcaractereschequelinha=2)
                     Then Messagedlg('Erro na cria��o do Objeto de Parametros!',mterror,[mbok],0);
                End;
           exit;
     End;
     Try

       If (ObjParametroGlobal.Validaparametro('QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;


       Try
          quantcaractereschequelinha:=StrToInt(ObjParametroGlobal.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres no valor extenso do Cheque!',mterror,[mbok],0);
             exit;
       End;
       //***********************************************
       //Referente ao campo descricao do comprovante de pagamento

       If (ObjParametroGlobal.Validaparametro('QUANTIDADE DE CARACTERES LINHA 1 DESC. COMPROVANTE')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 1 DESC. COMPROVANTE" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;


       Try
          quantcaractereslinhadescricao:=StrToInt(ObjParametroGlobal.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres na linha da descri��o do comprovante!',mterror,[mbok],0);
             exit;
       End;
       If (ObjParametroGlobal.Validaparametro('QUANTIDADE DE LINHAS NA DESCRICAO DO COMPROVANTE')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE LINHAS NA DESCRICAO DO COMPROVANTE" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;


       Try
          quantidadelinhasdescricao:=StrToInt(ObjParametroGlobal.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de linhas na descri��o do comprovante!',mterror,[mbok],0);
             exit;
       End;
       //***********************************************
       TempObjConfRelatorio.RecuperaArquivoRelatorio(FrelComprovanteChequeSReciboX,'IMPRIMECOMPROVANTES_RECIBO');
       FrelComprovanteChequeSReciboX.LbValor.caption:='R$ '+formatfloat('###,##0.00',Strtofloat(Self.Cheque.Get_Valor));
       //**************************IMPRIMINDO A C�PIA DO CHEQUE*****************
       ValorExtensoCheque:='';
       extenso1:='';
       extenso2:='';
       ValorExtensoCheque:=valorextenso(Strtofloat(Self.Cheque.Get_Valor));
       DividirValor(ValorExtensoCheque,quantcaractereschequelinha,200,extenso1,extenso2);
       FrelComprovanteChequeSReciboX.LbExtenso1.caption:=extenso1;
       FrelComprovanteChequeSReciboX.LbExtenso2.caption:=extenso2;
       FrelComprovanteChequeSReciboX.LbNominal.caption:=Self.Nominador;
       FrelComprovanteChequeSReciboX.LbCidadeCheque.caption:=Self.Cidade;
       DecodeDate(strtodate(Self.Data),Tmpano,TmpMes,TmpDia);
       FrelComprovanteChequeSReciboX.LbDiaCheque.caption:=Inttostr(TmpDia);
       FrelComprovanteChequeSReciboX.LbMesCheque.caption:=MesExtenso(TmpMes);
       FrelComprovanteChequeSReciboX.LbAnoCheque.caption:=Inttostr(TmpAno);
       //*********IMPRIMINDO OS DADOS ABAIXO DA C�PIA DO CHEQUE*****************
       FrelComprovanteChequeSReciboX.LbNumeroCheque.caption:=Self.Cheque.Get_NumCheque;
       FrelComprovanteChequeSReciboX.LbConta.caption:=Self.Cheque.portador.Get_NumeroConta;

       FrelComprovanteChequeSReciboX.LbdoBanco.caption:=Self.Cheque.Portador.Get_nome;

       //*******************************************
       //Tenho apenas 3 linhas neste tipo de comprovante, se for um unico pagamento
       //utilizo as 3, se for mais de um tenho que imprimir em anexo a listagem dos t�tulos pagos

       FrelComprovanteChequeSReciboX.LbLinha1Desc.caption:='';
       FrelComprovanteChequeSReciboX.LbLinha2Desc.caption:='';
       FrelComprovanteChequeSReciboX.LbLinha3Desc.caption:='';
       
       linhaatual:=1;
       //dividindo em varias linhas
       for cont:=1 to length(Self.descricaocomprovante) do
       Begin

            if (cont mod quantcaractereslinhadescricao=0)
            Then inc(linhaatual,1);

            Case linhaatual of
            1:FrelComprovanteChequeSReciboX.LbLinha1Desc.caption:=FrelComprovanteChequeSReciboX.LbLinha1Desc.caption+Self.descricaocomprovante[cont];
            2:FrelComprovanteChequeSReciboX.LbLinha2Desc.caption:=FrelComprovanteChequeSReciboX.LbLinha2Desc.caption+Self.descricaocomprovante[cont];
            3:FrelComprovanteChequeSReciboX.LbLinha3Desc.caption:=FrelComprovanteChequeSReciboX.LbLinha3Desc.caption+Self.descricaocomprovante[cont];
            Else FrelComprovanteChequeSReciboX.LbLinha3Desc.caption:=FrelComprovanteChequeSReciboX.LbLinha3Desc.caption+'...';
            End;
       End;

       If (messagedlg('Pressione <OK> para imprimir ou <CANCELAR> para visualizar a impress�o do Comprovante!',mtinformation,[mbok,mbcancel],0)=MrCAncel)
       Then FrelComprovanteChequeSReciboX.Qr.preview
       Else FrelComprovanteChequeSReciboX.Qr.print;

     Finally
            freeandnil(TempObjconfrelatorio);
     End;
Finally
	Freeandnil(FrelComprovanteChequeSReciboX);
End;

end;

procedure TObjComprovanteCheque.AlteraSequencia;
var
ValorGen:String;
ValorGenInt:Integer;
begin


     Repeat

           If (InputQuery('ALTERA SEQU�NCIA','DIGITE O N� DO �LTIMO COMPROVANTE',ValorGen)=False)
           Then exit;
           Try
              ValorGenInt:=strtoint(ValorGen);
           Except
                 valorgenint:=-1;
           End;
     Until(valorgenint<>-1);

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.Add('set generator gencomprovantecheque to '+inttostr(valorgenint));
          Try
          ExecSQL;
          Except
                Messagedlg('Erro na tentativa de alterar a sequ�ncia!',mterror,[mbok],0);
                exit;
          End;
     End;
end;

function TObjComprovanteCheque.Get_descricaocomprovante: string;
begin
     Result:=Self.descricaocomprovante;
end;

procedure TObjComprovanteCheque.Submit_descricaocomprovante(
  parametro: string);
begin
     Self.descricaocomprovante:=parametro;
end;

procedure TObjComprovanteCheque.EdtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
begin
     if key<>vk_f9
     Then exit;
     
     Try
        Fpesquisalocal:=Tfpesquisa.create(nil);

        If (FpesquisaLocal.PreparaPesquisa(Self.Get_pesquisa,Self.Get_TituloPesquisa,Nil)=True)
        Then Begin
                  Try
                    If (FpesquisaLocal.showmodal=mrok)
                    Then Begin
                              TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                    End;
                  Finally
                    FpesquisaLocal.QueryPesq.close;
                  End;
        End;

     Finally
        FreeandNil(FPesquisaLocal);
     End;
end;

end.




