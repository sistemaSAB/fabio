unit ULancamentoFR;

interface

uses
  UessencialGlobal, rdprint,Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls, ComCtrls,UObjLancamento,db,Upesquisa,
  Grids;

type
  TFrLancamento = class(TFrame)
    Guia: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    strgcr: TStringGrid;
    btchequecaixa: TBitBtn;
    btpesquisa: TBitBtn;
    BtApagaLote: TBitBtn;
    btimprimecomprovantepagamento: TBitBtn;
    Panel1: TPanel;
    STRGCP: TStringGrid;
    Panel3: TPanel;
    edtpesquisa_STRG_GRID_CR: TMaskEdit;
    LbtipoCampoCR: TListBox;
    combocredordevedorcodigocr: TComboBox;
    Panel4: TPanel;
    btlancaboleto: TBitBtn;
    BtSelecionaTodas_CR: TButton;
    Bevel1: TBevel;
    LbValorLancamentocr: TLabel;
    lbrecebe: TLabel;
    LbSaldocr: TLabel;
    edthistoricoCR: TEdit;
    Label2: TLabel;
    edtcodigohistoricoCR: TEdit;
    edtvalorlancamentoCR: TMaskEdit;
    Label1: TLabel;
    LbNomeCredorDevedorcr: TLabel;
    Label10: TLabel;
    edtcodigocredordevedorcr: TEdit;
    edtvencimentocr: TMaskEdit;
    Label15: TLabel;
    Label14: TLabel;
    edtdatalancamentocr: TMaskEdit;
    edtcodigoboleto: TEdit;
    Label5: TLabel;
    btpesquisacr: TBitBtn;
    btexecutacr: TBitBtn;
    Label7: TLabel;
    combocredordevedorcr: TComboBox;
    Panel2: TPanel;
    Bevel2: TBevel;
    Label4: TLabel;
    Label3: TLabel;
    LbNomeCredorDevedorCP: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    BtSelecionaTodas_PG: TButton;
    edthistoricoCP: TEdit;
    edtcodigohistoricoCP: TEdit;
    edtvalorlancamentoCP: TMaskEdit;
    combocredordevedorCP: TComboBox;
    edtcodigocredordevedorCP: TEdit;
    edtvencimentocp: TMaskEdit;
    edtdatalancamentoCP: TMaskEdit;
    BtpesquisaCP: TBitBtn;
    BtexecutaCP: TBitBtn;
    lbvalorlanctoCP: TLabel;
    lbvalorpagamento: TLabel;
    lbsaldocp: TLabel;
    combocredordevedorcodigoCP: TComboBox;
    edtpesquisa_STRG_GRID_CP: TMaskEdit;
    LbtipoCampoPG: TListBox;
    Button1: TButton;
    Label6: TLabel;
    Button2: TButton;
    Label13: TLabel;
    btAlterarHistoricoLancamento: TBitBtn;
    Label16: TLabel;
    edtcontagerencial_CP: TEdit;
    Label17: TLabel;
    edtsubcontagerencial_CP: TEdit;
    procedure BtpesquisaCPClick(Sender: TObject);
    procedure BtexecutaCPClick(Sender: TObject);
    procedure STRGCPDblClick(Sender: TObject);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure STRGCPKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaChange(Sender: TObject);
    procedure btpesquisacrClick(Sender: TObject);
    procedure strgcrDblClick(Sender: TObject);
    procedure btexecutacrClick(Sender: TObject);
    procedure strgcrKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigohistoricosimplesKeyPress(Sender: TObject;var Key: Char);
    procedure btchequecaixaClick(Sender: TObject);
    procedure edtcodigocredordevedorCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorCPExit(Sender: TObject);
    procedure combocredordevedorCPChange(Sender: TObject);
    procedure combocredordevedorcrChange(Sender: TObject);
    procedure edtcodigocredordevedorcrExit(Sender: TObject);
    procedure edtcodigocredordevedorcrKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure BtApagaLoteClick(Sender: TObject);
    procedure edtcodigohistoricoCRKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigohistoricoCRKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigohistoricoCRExit(Sender: TObject);
    procedure edtvalorlancamentoCRExit(Sender: TObject);
    procedure edtcodigohistoricoCPExit(Sender: TObject);
    procedure edtcodigohistoricoCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtvalorlancamentoCPExit(Sender: TObject);
    procedure edtvalorlancamentoCPKeyPress(Sender: TObject; var Key: Char);
    procedure STRGCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpesquisa_STRG_GRID_CPKeyPress(Sender: TObject;
      var Key: Char);
    procedure STRGCPEnter(Sender: TObject);
    procedure strgcrEnter(Sender: TObject);
    procedure strgcrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpesquisa_STRG_GRID_CRKeyPress(Sender: TObject;
      var Key: Char);
    procedure btlancaboletoClick(Sender: TObject);
    procedure btimprimecomprovantepagamentoClick(Sender: TObject);
    procedure BtSelecionaTodas_PGClick(Sender: TObject);
    procedure BtSelecionaTodas_CRClick(Sender: TObject);
    procedure edtcodigoboletoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure strgcrDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure btAlterarHistoricoLancamentoClick(Sender: TObject);
    procedure edtcontagerencial_CPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtsubcontagerencial_CPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorCPDblClick(Sender: TObject);
    procedure edtcontagerencial_CPDblClick(Sender: TObject);
    procedure edtsubcontagerencial_CPDblClick(Sender: TObject);
    procedure edtcodigocredordevedorcrDblClick(Sender: TObject);
    procedure edtcodigoboletoDblClick(Sender: TObject);

    private
    { Private declarations }
         Procedure SalvaLotePagar;
         Procedure SalvaContasReceber;
         Procedure AtualizaLabelsCp;
         procedure AtualizaLabelsCR;
         procedure SalvaPagamento;
         procedure SalvaContasReceber_UmaPendencia(ppendencia, pvalor: string);
         procedure SalvaContasPagar_UmaPendencia(ppendencia, pvalor: string);
         procedure LancaDesconto(PTipo:string);
         Procedure ImprimeGridRecebimento;
         procedure ImprimeGridPagamento;
         
  public
    { Public declarations }
    ObjLancamento:TobjLancamento;
    Function Criar:boolean;
    Procedure Destruir;
  end;


var

Vencimentocp,VencimentocR:STring[10];

implementation

uses UFiltraImp,
  UObjGeraLancamento, 
  UObjTransferenciaPortador, UHistoricoSimples, 
  UDataModulo, UReltxtRDPRINT, UObjPendencia,
  UValorLanctoLote_rec, Uformata_String_Grid,
  UObjTitulo;
{$R *.DFM}


//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************


//****************************************
//****************************************

Function TFrLancamento.Criar:boolean;
begin
     limpaedit(Self);
     desabilita_campos(Self);
     Guia.ActivePageIndex:=0;

     Try
        ObjLancamento:=TObjLancamento.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Result:=False;
           exit;
     End;

     ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedorcp.items);
     ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigocp.items);
     ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedorcr.items);
     ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigocr.items);


     PegaFiguraBotao(BtpesquisaCP,'BotaoPesquisar.Bmp');
     PegaFiguraBotao(BtexecutaCP,'BotaoGravar.Bmp');
     PegaFiguraBotao(BtpesquisaCR,'BotaoPesquisar.Bmp');
     PegaFiguraBotao(BtexecutaCR,'BotaoGravar.Bmp');
     PegaFiguraBotao(btchequecaixa,'botaogravar.bmp');
     result:=True;

end;

procedure TFrLancamento.Destruir;
begin
     if (Self.objLancamento<>nil)
     Then ObjLancamento.free;
end;



procedure TFrLancamento.BtpesquisaCPClick(Sender: TObject);
begin
     edtdatalancamentoCP.text:=datetostr(now);
     VencimentoCP:=EDTVENCIMENTOCP.text;

     objlancamento.Pendencia.Titulo.Get_PendenciaseTitulo('D',EDTVENCIMENTOCP.TEXT,STRGCP,combocredordevedorcodigocp.Items[combocredordevedorcp.itemindex],edtcodigocredordevedorcp.text,'',LbtipoCampoPG.Items,edtcontagerencial_CP.text,edtsubcontagerencial_CP.Text);

     IF (STRGCP.ROWCOUNT<=1)
     Then STRGCP.FixedRows:=0
     Else STRGCP.FixedRows:=1;

     Formata_StringGrid(STRGCP,LbtipoCampoPG.Items);
     AjustaLArguraColunaGrid(STRGCP);

end;

procedure TFrLancamento.BtexecutaCPClick(Sender: TObject);
begin
     If (STRGCP.RowCount<=1)
     Then exit;
     Self.salvaLotePagar;
     btpesquisacp.OnClick(nil);
     AtualizaLabelsCP;

end;

procedure TFrLancamento.STRGCPDblClick(Sender: TObject);
var
valor,valordogrid:currency;
cont:integer;
begin

     //pegando o valor a ser pago
     limpaedit(Ffiltroimp);
     With Ffiltroimp do
     Begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.EditMask:='';
              edtgrupo01.text:=STRGCP.cells[7,STRGCP.row];
              edtgrupo01.OnKeyPress:=nil;
              LbGrupo01.caption:='VALOR';
              showmodal;
              edtgrupo01.OnKeyPress:=nil;
              If Tag=0
              Then exit;


              Try

                 valor:=strtofloat(tira_ponto(edtgrupo01.text));
                 valordogrid:=strtofloat(tira_ponto(STRGCP.cells[7,STRGCP.row]));
                 STRGCP.cells[8,STRGCP.row]:=formata_valor(floattostr(valor));
              Except
                    STRGCP.cells[8,STRGCP.row]:='';
              End;

              for cont:=0 to STRGCP.ColCount-1 do
              Begin
                   STRGCP.Cells[cont,STRGCP.row]:=STRGCP.Cells[cont,STRGCP.row];
              End;

     End;
     atualizalabelscp;
     STRGCP.SetFocus;

End;
procedure TFrLancamento.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
    If not(key in ['0'..'9',#8,','])
     Then Begin
        If (key='.')
        Then Key:=','
        Else Key:=#0;
     End;
end;

procedure TFrLancamento.STRGCPKeyPress(Sender: TObject; var Key: Char);
begin
     If key=#13
     Then STRGCP.OnDblClick(sender);


     if (key=#32)
     Then Begin
               if (STRGCP.cells[0,1]='')
               Then exit;

               PreparaPesquisa_StringGrid(STRGCP,edtpesquisa_STRG_GRID_CP,LbtipoCampoPG.Items);
     End;

end;

procedure TFrLancamento.SalvaLotePagar;
var
Cont:Integer;
tvalor:currency;
Begin
     tvalor:=0;
     for cont:=1 to STRGCP.RowCount-1 do
     begin
          Try
             if (trim(STRGCP.cells[8,cont])<>'')
             Then Begin
                       strtofloat(tira_ponto(STRGCP.cells[8,cont]));
                       tvalor:=tvalor+strtofloat(tira_ponto(STRGCP.cells[8,cont]));
             End;
          except
          end;
     End;

     if (Tvalor=0)
     Then Begin
               Messagedlg('Nenhuma pend�ncia foi selecionada para pagamento!',mtinformation,[mbok],0);
               exit;
     End;

     Try
        strtodate(edtdatalancamentocp.text);
     Except
           Messagedlg('Data de Lan�amento Inv�lida!',mterror,[mbok],0);
           exit;
     End;

     Self.SalvaPagamento;
End;

PRocedure TfrLancamento.SalvaPagamento;
var
contapag,cont:Integer;
ValorLanccomPai,ValorSoma:Currency;
HistoricoTodos:String;
ObjTransferenciaPortador:TObjTransferenciaPortador;
PCCredorDevedor,CodigoLAncamentoPai,CodigoLancamentoFilho,TmptipoLancto,tmptipolanctoligado:string;
NFTEMP,NumeroTransferencia:string;
ppendencia,pvalor,PCredorDevedorLote,PcodigoCredorDevedorLote:string;
Begin
     //Tenho Que Salvar um lancamento sem estar ligado a pendencia
     //nenhuma, apenas com o valor de todas elas
     //e um hist�rico PAGAMENTO DE PEND. DOS T�TULOS X,Y,Z....

     ValorSoma:=0;
     ContaPag:=0;
     HistoricoTodos:='PAG.LOTE - ';
     PCredorDevedorLote:='';
     PcodigoCredorDevedorLote:='';

     for cont:=1 to StrgCP.RowCount-1 do
     Begin
          Try
             if (trim(StrgCP.cells[8,cont])<>'')
             Then Begin
                       Inc(ContaPAG,1);
                       //guardando pendencia e o valor
                       Ppendencia:=StrgCP.cells[3,cont];
                       Pvalor:=tira_ponto(trim(StrgCP.Cells[8,cont]));
                       
                       //******************************************************
                       ObjLancamento.Pendencia.Titulo.LocalizaCodigo(StrgCP.cells[0,cont]);
                       ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
                       //******************************************************
                       {Se todas as pend�ncias forem do mesmo credor devedor (cliente, fornecedor...)
                       entao as variaveis credordevedorlote e codigocredordevedorlote ficaram
                       com o codigo dele, assim quando mandar pra tela de recebimento, os cheques
                       de terceiro poderao resgatar automaticamente o nome o cpf do cadastro}
                       if (PcredorDevedorLote='')
                       Then Begin
                                 PcredorDevedorLote:=ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO;
                                 PCodigoCredorDevedorLote:=ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
                       End
                       Else Begin

                                 if (PcredorDevedorLote<>ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO)
                                 or (PCodigoCredorDevedorLote<>ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR)
                                 Then Begin
                                           //existem pendencias de credordevedores diferentes
                                           PcredorDevedorLote:='-1';
                                           PcodigoCredorDevedorLote:='-1';
                                 End;
                       End;
                       //******************************************************
                       Strtofloat(TIRA_PONTO(StrgCP.cells[8,cont]));
                       ValorSoma:=ValorSoma+Strtofloat(Tira_ponto(StrgCP.cells[8,cont]));
                       IF (edthistoricoCP.text='')
                       Then HistoricoTodos:=HistoricoTodos+' / '+StrgCP.cells[0,cont]+' NF '+ObjLancamento.Pendencia.Titulo.Get_NUMDCTO;
             End;
          Except
          End;
     End;

     if (PCredorDevedorLote='-1')//tem mais de um credor/devedor
     Then begin
               PCredorDevedorLote:='';
               PcodigoCredorDevedorLote:='';
     End;

     if (edthistoricocp.Text<>'')
     Then Historicotodos:=EdthistoricoCp.text;//Historicotodos:=HistoricoTodos+edthistoricoCP.text;

     If (ValorSoma=0)
     Then exit;


     if (ContaPag=1)//somente uma pendencia
     then begin
               Self.SalvaContasPagar_UmaPendencia(ppendencia,pvalor);
               exit;
     End;


     //lan�amento das pendencias filhos
     If (Objlancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO')=fALSE)
     Then Begin
               Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO", n�o foi encontrado!',mterror,[mbok],0);
               exit;
     End;
     Objlancamento.ObjGeraLancamento.TabelaparaObjeto;
     TmptipoLanctoLigado:=Objlancamento.ObjGeraLancamento.Get_TipoLAncto;

     //Salvando o Lan�amento Pai
     CodigoLAncamentoPai:=ObjLancamento.Get_NovoCodigo;
     if (ObjLancamento.NovoLancamento('','P',floattostr(valorsoma),CodigoLAncamentoPai,HistoricoTodos,False,False,True,PCredorDevedorLote,PcodigoCredorDevedorLote,edtdatalancamentocp.text)=False)
     Then Begin
               FDataModulo.IBTransaction.RollbackRetaining;
               Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
               exit;
     End;
              
     //****************************************************************
     //salvando as pendencias sem gerar valor
     for cont:=1 to StrgCP.RowCount-1 do
     Begin
          if (trim(StrgCP.Cells[8,cont])<>'')
          Then Begin
                    Try
                       ObjLancamento.Pendencia.Titulo.LocalizaCodigo(StrgCP.cells[0,cont]);
                       ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
                       NFTEMP:=ObjLancamento.Pendencia.Titulo.Get_NUMDCTO;

                       ObjLancamento.ZerarTabela;
                       CodigoLancamentoFilho:=ObjLancamento.get_novocodigo;
                       ObjLancamento.Submit_CODIGO(CodigoLancamentoFilho);
                       ObjLancamento.Submit_Pendencia(StrgCP.cells[3,cont]);
                       ObjLancamento.Submit_TipoLancto(tmptipolanctoligado);
                       ObjLancamento.Submit_Valor(TIRA_PONTO(StrgCP.Cells[8,cont]));
                       ObjLancamento.Submit_Historico('PAG.'+StrgCP.Cells[1,cont]+'-NF '+NFTEMP);
                       ObjLancamento.Submit_Data(edtdatalancamentocp.text);
                       ObjLancamento.Submit_LancamentoPai(CodigoLAncamentoPai);
                       ObjLancamento.Status:=dsinsert;
                       If (Objlancamento.Salvar(False,False,'',true,true)=False)
                       Then Begin
                                 Messagedlg('Lan�amentos Cancelados!',mterror,[mbok],0);
                                 ObjLancamento.Rolback;
                                 ObjLancamento.Status:=dsInactive;
                                 exit;
                       End;
                       CodigoLancamentoFilho:=ObjLancamento.get_codigo;
                       //lancamento filho
                       StrgCP.cells[13,cont]:=Codigolancamentofilho;
                    Except

                    End;
          End;
     End;
     //****************************************************************
     ObjLancamento.Commit;
     
     if (ObjLancamento.ExportaContabilidade_Pagamento_Lote(CodigoLAncamentoPai)=False)
     Then Begin
               ObjLancamento.Rolback;
               Messagedlg('Erro na tentativa de Exportar a contabilidade do Pagamento. O lan�amento ser� exclu�do',mterror,[mbok],0);
               if (Objlancamento.LocalizaCodigo(codigolancamentopai)=True)
               Then Objlancamento.ExcluiLancamento(CodigoLAncamentoPai,true,false);
               exit;
     End
     Else ObjLancamento.Commit;
     
     //******IMPRIMIR RECIBO DE PAGAMENTO**************************************
     if (ObjParametroGlobal.ValidaParametro('IMPRIME RECIBO DE PAGAMENTO AP�S LAN�AMENTO')=False)
     then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then Objlancamento.ImprimeReciboPagamento(CodigoLAncamentoPai);
     //*************************************************************************

     Messagedlg('Pagamento efetuado com Sucesso!',mtinformation,[mbok],0);
     edtvencimentocp.text:=VencimentoCP;
End;


procedure TFrLancamento.GuiaChange(Sender: TObject);
begin
     If (Guia.ActivePage=TabSheet2) or
        (Guia.ActivePage=TabSheet3)
     Then Begin
               If (objlancamento.status<>dsinactive)
               Then Begin
                        Guia.ActivePageIndex:=0;
                        exit;
               End;
     End;

     If Guia.ActivePage=TabSheet2
     Then Begin
               EDTVENCIMENTOcp.enabled:=True;
               edtdatalancamentoCP.enabled:=True;
               combocredordevedorcp.enabled:=True;
               edtcodigocredordevedorcp.enabled:=True;
               edtvalorlancamentoCP.enabled:=True;
               edtcodigohistoricoCP.enabled:=True;
               edthistoricoCP.enabled:=True;
               edtpesquisa_STRG_GRID_CP.enabled:=True;
               edtpesquisa_STRG_GRID_CP.Visible:=False;

               edtcontagerencial_CP.enabled:=True;
               edtsubcontagerencial_CP.enabled:=True;

               LbNomeCredorDevedorcp.caption:='';
               combocredordevedorcp.SetFocus;

               STRGCP.ColCount:=1;
               STRGCP.rowcount:=1;
               STRGCP.cols[0].clear;
               EDTVENCIMENTOcp.text:='';
               VencimentoCP:='';
               LbValorPagamento.caption:='';
               combocredordevedorcp.Text:='';
               edtcodigocredordevedorcp.Text:='';
               lbvalorlanctoCP.caption:='';
               lbsaldocp.caption:='';
               AtualizaLabelsCp;
     End;

     If Guia.ActivePage=TabSheet3
     Then Begin
               EDTVENCIMENTOcr.enabled:=True;
               edtdatalancamentoCR.enabled:=True;
               combocredordevedorcr.enabled:=True;
               edtcodigocredordevedorcr.enabled:=True;
               edtvalorlancamentoCR.enabled:=True;
               edtcodigohistoricoCR.enabled:=True;
               edthistoricoCR.enabled:=True;
               edtcodigoboleto.enabled:=True;

               edtpesquisa_STRG_GRID_CR.enabled:=True;
               edtpesquisa_STRG_GRID_CR.Visible:=False;

               LbNomeCredorDevedorcr.caption:='';
               combocredordevedorcr.SetFocus;

               STRGCR.ColCount:=1;
               STRGCR.rowcount:=1;
               STRGCR.cols[0].clear;
               EDTVENCIMENTOcR.text:='';
               VencimentoCR:='';
               lbrecebe.caption:='';
               LbValorLancamentocr.caption:='';
               LbSaldocr.caption:='';

               combocredordevedorcr.Text:='';
               edtcodigocredordevedorcr.Text:='';
               edtcodigoboleto.Text:='';

               AtualizaLabelsCR;
     End;



end;

procedure TFrLancamento.btpesquisacrClick(Sender: TObject);
begin
     VencimentoCR:=EDTVENCIMENTOCR.text;
     objlancamento.Pendencia.Titulo.Get_PendenciaseTitulo('C',EDTVENCIMENTOCR.TEXT,STRGCR,combocredordevedorcodigocr.Items[combocredordevedorcr.itemindex],edtcodigocredordevedorcr.text,edtcodigoboleto.text,LbtipoCampoCR.Items);
     edtdatalancamentoCR.text:=datetostr(now);
//sdfsdfsd
     If (STRGCR.ROWCOUNT<=1)
     Then STRGCR.FixedRows:=0
     Else STRGCR.FixedRows:=1;
     Formata_StringGrid(strgcr,LbtipoCampoCR.Items);
     AjustaLArguraColunaGrid(STRGCR);
     Self.AtualizaLabelsCR;
end;

procedure TFrLancamento.strgcrDblClick(Sender: TObject);
var
Ptaxa,valor:currency;
cont:integer;
begin
     if (FvalorLanctoLote_REC=nil)    //criando form ****ALEX
     then Application.CreateForm(TFvalorLanctoLote_REC,FvalorLanctoLote_REC);

     FValorLanctoLote_REC.FrValorLanctoLote.Visible:=False;

     if (ObjParametroGlobal.Validaparametro('CALCULA JUROS NO LAN�AMENTO?')=False)
     Then exit;

     if (ObjParametroGlobal.Get_valor='SIM')
     then Begin
               if (ObjParametroGlobal.Validaparametro('TAXA DE JUROS AO M�S NO LAN�AMENTO (%)')=False)
               Then exit;


               Try
                  Ptaxa:=strtofloat(ObjParametroGlobal.Get_Valor);
                  Ptaxa:=Ptaxa/30;//ao dia
                  ptaxa:=strtofloat(tira_ponto(formata_valor(ptaxa)));
               Except
                  Ptaxa:=0;
               End;


               //saldo e vencimento
               FValorLanctoLote_REC.FrValorLanctoLote.visible:=true;
               FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbSaldoPendencia.Caption:=formata_valor(STRGCR.cells[7,STRGCR.row]);
               FValorLanctoLote_REC.FrValorLanctoLote.edtdatalancamento.text:=datetostr(now);
               FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbTaxaDia.caption:=formata_valor(ptaxa);
               FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbdiaatraso.caption:='0';
               FValorLanctoLote_REC.FrValorLanctoLote.PJ_lbSaldoComJuros.caption:=formata_valor(STRGCR.cells[7,STRGCR.row]);
               FValorLanctoLote_REC.FrValorLanctoLote.pj_vencimento.caption:=STRGCR.cells[5,STRGCR.row];
               FValorLanctoLote_REC.FrValorLanctoLote.edtcarencia.Text:='0';
     End;

     FValorLanctoLote_REC.edtvalor.text:=trim(STRGCR.cells[7,STRGCR.row]);
     FValorLanctoLote_REC.ShowModal;     //ABRINDO FORM ******ALEX*********


     If FValorLanctoLote_REC.Tag=0
     Then exit;

     Try
        valor:=strtofloat(tira_ponto(FvalorLanctoLote_REC.edtvalor.text));
        STRGCR.cells[8,STRGCR.row]:=formata_valor(floattostr(valor));
     Except
           STRGCR.cells[8,STRGCR.row]:='';
     End;

     for cont:=0 to strgcr.ColCount-1 do
     Begin
          strgcr.Cells[cont,STRGCR.row]:=strgcr.Cells[cont,STRGCR.row];
     End;


    

     {//pegando o valor a ser pago
     limpaedit(Ffiltroimp);
     With Ffiltroimp do
     Begin
              DesativaGrupos;
              Grupo01.Enabled:=True;
              edtgrupo01.EditMask:='';
              edtgrupo01.text:=STRGCR.cells[7,STRGCR.row];
              edtgrupo01.OnKeyPress:=nil;
              LbGrupo01.caption:='VALOR';
              showmodal;
              edtgrupo01.OnKeyPress:=nil;
              If Tag=0
              Then exit;

              Try
                 valor:=strtofloat(tira_ponto(edtgrupo01.text));
                 STRGCR.cells[8,STRGCR.row]:=formata_valor(floattostr(valor));
              Except
                    STRGCR.cells[8,STRGCR.row]:='';
              End;
     End;}
     STRGCR.SetFocus;
     AtualizaLabelsCR;
     FreeAndNil(FvalorLanctoLote_REC);
End;

procedure TFrLancamento.btexecutacrClick(Sender: TObject);
begin
     If (STRGCR.RowCount<=1)
     Then exit;

     Self.SalvaContasReceber;
     AtualizaLabelsCR;
end;

procedure TFrLancamento.SalvaContasReceber;
var
Cont:Integer;
ContaRec:Integer;
ValorLanccomPai,ValorSoma:Currency;
HistoricoTodos:String;
PortadorUSadoNoRecebimento,NFTEMP,PCCredorDevedor,CodigoLAncamentoPai,CodigoLancamentoFilho,TmptipoLancto,tmptipolanctoligado:string;
ppendencia,PCredorDevedorLote,PCodigoCredorDevedorLote:string;
pvalor:string;
Begin

     ContaRec:=0;

     for cont:=1 to STRGCR.RowCount-1 do
     Begin
          If (trim(StrgCR.Cells[8,cont])<>'')//verificando a coluna de valor a ser recebido
          Then Begin
                    Inc(ContaRec,1);
                    //guardando pendencia e o valor
                    Ppendencia:=StrgCR.cells[3,cont];
                    Pvalor:=tira_ponto(trim(StrgCR.Cells[8,cont]));
          End;
     End;

     If (ContaRec=0)
     Then Begin
               Messagedlg('N�o foi definido o valor de nenhum T�tulo!',mtinformation,[mbok],0);
               exit;
     End;

     Try
        strtodate(edtdatalancamentocr.text);
     Except
          Messagedlg('Data Inv�lida para o lan�amento!',mterror,[mbok],0);
          exit;
     End;

     if (ContaRec=1)//somente uma pendencia
     then begin
               Self.SalvaContasReceber_UmaPendencia(ppendencia,pvalor);
               exit;
     End;


     
     //Tenho Que Salvar um lancamento sem estar ligado a pendencia
     //nenhuma, apenas com o valor de todas elas
     //e um hist�rico REC. DE PEND. DOS T�TULOS X,Y,Z....
     ValorSoma:=0;
     
     HistoricoTodos:='LOTE - ';
     PCredorDevedorLote:='';
     PCodigoCredorDevedorLote:='';
     
     for cont:=1 to StrgCR.RowCount-1 do
     Begin
          If (trim(StrgCR.Cells[8,cont])<>'')
          Then Begin
                    Try
                       ObjLancamento.Pendencia.Titulo.LocalizaCodigo(trim(StrgCR.cells[0,cont]));
                       ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;

                       //******************************************************
                       {Se todas as pend�ncias forem do mesmo credor devedor (cliente, fornecedor...)
                       entao as variaveis credordevedorlote e codigocredordevedorlote ficaram
                       com o codigo dele, assim quando mandar pra tela de recebimento, os cheques
                       de terceiro poderao resgatar automaticamente o nome o cpf do cadastro}
                       if (PcredorDevedorLote='')
                       Then Begin
                                 PcredorDevedorLote:=ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO;
                                 PCodigoCredorDevedorLote:=ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
                       End
                       Else Begin

                                 if (PcredorDevedorLote<>ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CODIGO)
                                 or (PCodigoCredorDevedorLote<>ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR)
                                 Then Begin
                                           //existem pendencias de credordevedores diferentes
                                           PcredorDevedorLote:='-1';
                                           PcodigoCredorDevedorLote:='-1';
                                 End;
                       End;
                       //******************************************************
                       Strtofloat(TIRA_PONTO(trim(StrgCR.cells[8,cont])));
                       ValorSoma:=ValorSoma+Strtofloat(Tira_ponto(trim(StrgCR.cells[8,cont])));
                       if (edthistoricoCR.text='')
                       Then HistoricoTodos:=HistoricoTodos+' / '+StrgCR.cells[0,cont]+' NF '+ObjLancamento.Pendencia.Titulo.Get_NUMDCTO;
                    Except

                    End;
          End;
     End;

     if (PCredorDevedorlote='-1')
     Then Begin
               PCredorDevedorLote:='';
               PCodigoCredorDevedorLote:='';
     End;

     if (edthistoricoCR.Text<>'')
     Then historicotodos:=historicotodos+edthistoricoCR.Text;

     If (ValorSoma=0)
     Then exit;

     //USado para os lancamentos filhos
     If (ObjLancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO')=fALSE)
     Then Begin
                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO", n�o foi encontrado!',mterror,[mbok],0);
                  exit;
     End;
     ObjLancamento.ObjGeraLancamento.TabelaparaObjeto;
     TmptipoLanctoLigado:=ObjLancamento.ObjGeraLancamento.Get_TipoLAncto;
     //*******************************
      CodigoLAncamentoPai:=ObjLancamento.Get_NovoCodigo;
      ObjLancamento.ZerarTabela;



     
     //***********************************************************************
     //CHAMANDO A TELA DE QUITACAO
     if (ObjLancamento.NovoLancamento('','R',floattostr(ValorSoma),codigolancamentopai,HistoricoTodos,False,False,True,PcredorDevedorLote,PCodigoCredorDevedorLote,edtdatalancamentocr.text)=False)
     Then Begin
               FDataModulo.IBTransaction.RollbackRetaining;
               Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
               exit;
     End;

     //****************************************************************
     //salvando as pendencias sem gerar valor
     for cont:=1 to StrgCr.RowCount-1 do
     Begin
          If (Trim(StrgCR.Cells[8,cont])<>'')
          Then Begin
                    Try
                       ObjLancamento.Pendencia.Titulo.LocalizaCodigo(StrgCR.cells[0,cont]);
                       ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
                       NFTEMP:=ObjLancamento.Pendencia.Titulo.get_numdcto;

                       ObjLancamento.ZerarTabela;
                       CodigoLancamentoFilho:=ObjLancamento.get_novocodigo;
                       ObjLancamento.Submit_CODIGO(CodigoLancamentoFilho);
                       ObjLancamento.Submit_Pendencia(StrgCR.cells[3,cont]);
                       ObjLancamento.Submit_TipoLancto(tmptipolanctoligado);
                       ObjLancamento.Submit_Valor(TIRA_PONTO(trim(StrgCR.Cells[8,cont])));
                       ObjLancamento.Submit_Historico('RC.LOTE-'+StrgCR.Cells[1,cont]+' - '+NFTEMP);
                       ObjLancamento.Submit_Data(edtdatalancamentocr.text);
                       ObjLancamento.Submit_LancamentoPai(CodigoLAncamentoPai);
                       ObjLancamento.Status:=dsinsert;
                       If (Objlancamento.Salvar(False,False,'',true,true)=False)
                       Then Begin
                                 ObjLancamento.Rolback;
                                 Messagedlg('Lan�amentos Cancelados!',mterror,[mbok],0);
                                 ObjLancamento.Status:=dsInactive;
                                 exit;
                       End;
                       Codigolancamentofilho:=Objlancamento.Get_CODIGO;
                       //GUARDANDO O CODIGO DO LANCAMENTO FILHO NA LINHA DO STRING GRID
                       StrgCR.cells[13,cont]:=Codigolancamentofilho;
                    Except

                    End;
          End;
     End;
     ObjLancamento.Commit;

     if (ObjLancamento.ExportaContabilidade_Recebimento_Lote(CodigoLAncamentoPai)=False)
     Then  Begin
                ObjLancamento.Rolback;
               Messagedlg('Erro na tentativa de exportar a contabilidade do recebimento. O lan�amento ser� exclu�do',mterror,[mbok],0);
               if (Objlancamento.LocalizaCodigo(codigolancamentopai)=True)
               Then Objlancamento.ExcluiLancamento(CodigoLAncamentoPai,true,false);
               exit;
     End
     Else ObjLancamento.Commit;


     // F�bio 30/09/2009 vou colocar o recibo do reportbuilder
     // quando � feito recebimento em lote
     if (ObjParametroGlobal.ValidaParametro('IMPRIME RECIBO DE RECEBIMENTO AP�S LAN�AMENTO')=true)
     then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then ObjLancamento.ImprimeRecibo(CodigoLAncamentoPai,'','');
     end;

     Messagedlg('Recebimento efetuado com Sucesso!',mtinformation,[mbok],0);
     edtvencimentocR.text:=VencimentoCR;
     btpesquisacR.OnClick(nil);
end;


procedure TFrLancamento.LancaDesconto(PTipo:string);
var
  Cont:Integer;
  ContaRec:Integer;
  ValorLanccomPai,ValorSoma,percentualdesconto:Currency;
  HistoricoTodos:String;
  PortadorUSadoNoRecebimento,NFTEMP,PCCredorDevedor,CodigoLAncamentoPai,CodigoLancamentoFilho,TmptipoLancto,tmptipolanctoligado:string;
  ppendencia,PCredorDevedorLote,PCodigoCredorDevedorLote:string;
  pvalor,pvalordesconto:String;
Begin

     ContaRec:=0;
     valorsoma:=0;

     for cont:=1 to STRGCR.RowCount-1 do
     Begin
          If (trim(StrgCR.Cells[8,cont])<>'')//verificando a coluna de valor a ser recebido
          Then Begin
                    Inc(ContaRec,1);
                    //somando o valor do saldo das linhas selecionadas
                    Ppendencia:=StrgCR.cells[3,cont];
                    Pvalor:=tira_ponto(trim(StrgCR.Cells[7,cont]));
                    VALORSOMA:=VALORSOMA+strtocurr(PVALOR);

          End;
     End;

     If (ContaRec=0)
     Then Begin
               Messagedlg('N�o foi definido o valor de nenhum T�tulo!',mtinformation,[mbok],0);
               exit;
     End;

     Try
        strtodate(edtdatalancamentocr.text);
     Except
          Messagedlg('Data Inv�lida para o lan�amento!',mterror,[mbok],0);
          exit;
     End;

     pvalordesconto:='0';

     if (Ptipo='%')
     Then Begin
               if (inputquery('Desconto','Digite o valor do desconto em (%)',pvalordesconto)=False)
               Then exit;
     End;

     if (Ptipo='R')
     Then Begin
               if (inputquery('Desconto','Digite o valor do desconto em R$',pvalordesconto)=False)
               Then exit;
     End;
     
     Try
        strtocurr(pvalordesconto);

        if (Ptipo='R')
        Then Begin
                  if (strtocurr(Pvalordesconto)>valorsoma)
                  Then Begin
                            MensagemErro('O valor do desconto n�o pode ser superior ao saldo das pend�ncias selecionadas');
                            exit;
                  End;
        End
        Else BEgin
                  if (strtocurr(Pvalordesconto)>100)//100%
                  Then Begin
                            MensagemErro('O desconto n�o pode ser superior a 100%');
                            exit;
                  End;
        End;

        if (strtocurr(Pvalordesconto)=0)
        Then Begin
                  MensagemErro('O desconto n�o pode ser zero');
                  exit;
        End;

        //calculando quantos % sera dado de desconto
        //esse calculo � feito sobre  o valor de saldo das linhas selecionadas

        (*ValorSoma    100%
        Pvalordesconto   X*)

        if (ptipo='R')
        Then percentualdesconto:=(strtocurr(pvalordesconto)*100)/valorsoma
        else percentualdesconto:=STRTOCURR(pvalordesconto);
        
        percentualdesconto:=strtocurr(formata_valor(percentualdesconto));

     Except
           Mensagemerro('Valor Inv�lido para o Desconto');
           exit;
     End;

     for cont:=1 to StrgCr.RowCount-1 do
     Begin
          If (Trim(StrgCR.Cells[8,cont])<>'')
          Then Begin
                    Try
                       Pvalor:=tira_ponto(trim(StrgCR.Cells[7,cont]));//pego o saldo

                       pvalordesconto:=currtostr((percentualdesconto*strtocurr(pvalor))/100);
                       pvalordesconto:=tira_ponto(formata_valor(pvalordesconto));

                       if (ObjLancamento.LancamentoAutomatico('D',StrgCR.cells[3,cont],pvalordesconto,edtdatalancamentocr.text,'Desconto em lote',Codigolancamentofilho,false)=False)
                       Then Begin
                                 ObjLancamento.Rolback;
                                 Messagedlg('Lan�amentos Cancelados!',mterror,[mbok],0);
                                 ObjLancamento.Status:=dsInactive;
                                 exit;
                       End;
                    Except

                    End;
          End;
     End;
     ObjLancamento.Commit;
     Messagedlg('Desconto(s) efetuado(s) com Sucesso!',mtinformation,[mbok],0);
     edtvencimentocR.text:=VencimentoCR;
     btpesquisacR.OnClick(nil);
 end;


procedure TFrLancamento.strgcrKeyPress(Sender: TObject; var Key: Char);
begin
     If (Key=#13)
     Then STRGCR.OnDblClick(nil);

     if (key=#32)
     Then Begin
               if (STRGCR.cells[0,1]='')
               Then exit;
               PreparaPesquisa_StringGrid(STRGCR,edtpesquisa_STRG_GRID_CR,LbtipoCampoCR.Items);
     End;

end;

procedure TFrLancamento.edtcodigohistoricosimplesKeyPress(Sender: TObject;
  var Key: Char);
begin
     If (not (key in ['0'..'9',#8]))
     Then key:=#0;
end;


procedure TFrLancamento.AtualizaLabelsCp;
var
  cont:integer;
  valor:Currency;
  pvalorlancamento,psaldo,TotalCP:Currency;
begin
     TotalCP:=0;
     for cont:=1 to STRGCP.RowCount-1 do
     Begin
          valor:=0;
          Try
             If (trim(strgcp.Cells[8,cont])<>'')
             Then valor:=strtofloat(tira_ponto(strgcp.Cells[8,cont]));
          Except
                valor:=0;
          End;
          TotalCp:=TotalCP+valor;
     End;

     Try
        PvalorLancamento:=0;
        if (edtvalorlancamentoCP.Text<>'')
        Then PvalorLancamento:=strtofloat(edtvalorlancamentoCP.Text);
     Except
        PvalorLancamento:=0;
     End;

     Try
        PSaldo:=Pvalorlancamento-TOTALCP;
     Except
        PSaldo:=0;
     End;

     lbvalorlanctoCP    .caption:='VALOR DO PAGAMENTO R$ '+CompletaPalavra_a_Esquerda(formata_valor(pvalorlancamento),15,' ');
     lbvalorpagamento   .caption:='TOTAL SELECIONADO  R$ '+CompletaPalavra_a_Esquerda(formata_valor(TOTALCP),15,' ');
     if (pvalorlancamento<>0)
     Then LbSaldoCP          .caption:='SALDO DO PAGAMENTO R$ '+CompletaPalavra_a_Esquerda(formata_valor(psaldo),15,' ')
     Else LbSaldoCP          .caption:='SALDO DO PAGAMENTO R$ '+CompletaPalavra_a_Esquerda(formata_valor('0'),15,' ');


end;

procedure TFrLancamento.AtualizaLabelsCR;
var
cont:integer;
valor:Currency;
pvalorlancamento,psaldo,TotalCR:Currency;
begin
    TOTALCR:=0;
     for cont:=1 to STRGCR.RowCount-1 do
     Begin
          valor:=0;
          Try
             If (Trim(strgcr.Cells[8,cont])<>'')
             Then valor:=strtofloat(tira_ponto(strgcr.Cells[8,cont]));
          Except
                valor:=0;
          End;
          TotalCR:=TotalCR+Valor;
     End;

     Try
        PvalorLancamento:=0;
        if (edtvalorlancamentoCR.Text<>'')
        Then PvalorLancamento:=strtofloat(edtvalorlancamentoCR.Text);
     Except
        PvalorLancamento:=0;
     End;

     Try
        PSaldo:=Pvalorlancamento-TOTALCR;
     Except
        PSaldo:=0;
     End;

     LbValorLancamentoCR.caption:='VALOR DO RECEBIMENTO R$ '+CompletaPalavra_a_Esquerda(formata_valor(pvalorlancamento),15,' ');
     lbrecebe.           caption:='TOTAL SELECIONADO    R$ '+CompletaPalavra_a_Esquerda(formata_valor(TOTALCR),15,' ');
     if (pvalorlancamento<>0)
     Then LbSaldoCR          .caption:='SALDO DO RECEBIMENTO R$ '+CompletaPalavra_a_Esquerda(formata_valor(psaldo),15,' ')
     Else LbSaldoCR          .caption:='SALDO DO RECEBIMENTO R$ '+CompletaPalavra_a_Esquerda(formata_valor('0'),15,' ')


end;



procedure TFrLancamento.btchequecaixaClick(Sender: TObject);
begin
     ObjLancamento.EmiteChequeCaixa;
end;


procedure TFrLancamento.edtcodigocredordevedorCPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combocredordevedorcp.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(ObjLancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigocp.text),'Pesquisa de '+Combocredordevedorcp.text,ObjLancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigocp.text))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedorcp.text:=edtcodigocredordevedorcp.text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedorcp.caption:=ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocp.text,edtcodigocredordevedorcp.Text);
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFrLancamento.edtcodigocredordevedorCPExit(Sender: TObject);
begin
     LbNomeCredorDevedorcp.caption:='';
     If (edtcodigocredordevedorcp.text='') or (combocredordevedorcp.ItemIndex=-1)
     Then exit;
     LbNomeCredorDevedorcp.caption:=ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocp.text,edtcodigocredordevedorcp.Text);
end;

procedure TFrLancamento.combocredordevedorCPChange(Sender: TObject);
begin
     combocredordevedorcodigocp.itemindex:=combocredordevedorcp.itemindex;
end;

procedure TFrLancamento.combocredordevedorcrChange(Sender: TObject);
begin
     combocredordevedorcodigocr.itemindex:=combocredordevedorcr.itemindex;
end;

procedure TFrLancamento.edtcodigocredordevedorcrExit(Sender: TObject);
begin
     LbNomeCredorDevedorcr.caption:='';
     If (edtcodigocredordevedorcr.text='') or (combocredordevedorcr.ItemIndex=-1)
     Then exit;
     LbNomeCredorDevedorcr.caption:=ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocr.text,edtcodigocredordevedorcr.Text);
end;

procedure TFrLancamento.edtcodigocredordevedorcrKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combocredordevedorcr.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(ObjLancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigocr.text),'Pesquisa de '+Combocredordevedorcr.text,ObjLancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigocr.text))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedorcr.text:=edtcodigocredordevedorcr.text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedorcr.caption:=ObjLancamento.Pendencia.Titulo.RetornaNomesCredoresDevedores(combocredordevedorcodigocr.text,edtcodigocredordevedorcr.Text);
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFrLancamento.BtApagaLoteClick(Sender: TObject);
begin
     ObjLancamento.ExcluiLancamento;
end;

procedure TFrLancamento.edtcodigohistoricoCRKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   FHistoricoSimples:TFHistoricoSimples;
begin
        If (key<>vk_f9)
        Then exit;

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           FHistoricoSimples:=TFHistoricoSimples.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                                  If (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
                                  Then Begin
                                            TEdit(Sender).text:='';
                                            exit;
                                  End;
                                  ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
                                  edthistoricoCR.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FHistoricoSimples);
        End;
end;

procedure TFrLancamento.edtcodigohistoricoCRKeyPress(Sender: TObject;
  var Key: Char);
begin
     If (not (key in ['0'..'9',#8]))
     Then key:=#0;

end;

procedure TFrLancamento.edtcodigohistoricoCRExit(Sender: TObject);
begin
     IF (TEdit(Sender).text='')
     Then exit;

     If (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
     edthistoricoCR.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
     TEdit(Sender).text:='';
end;

procedure TFrLancamento.edtvalorlancamentoCRExit(Sender: TObject);
begin
    AtualizaLabelsCR;
end;

procedure TFrLancamento.edtcodigohistoricoCPExit(Sender: TObject);
begin
     IF (TEdit(Sender).text='')
     Then exit;

     If (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
     edthistoricoCP.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
     TEdit(Sender).text:='';
end;

procedure TFrLancamento.edtcodigohistoricoCPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

var
   FpesquisaLocal:TFpesquisa;
   FHistoricoSimples:TFHistoricoSimples;
begin
        If (key<>vk_f9)
        Then exit;

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           FHistoricoSimples:=TFHistoricoSimples.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                                  If (ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
                                  Then Begin
                                            TEdit(Sender).text:='';
                                            exit;
                                  End;
                                  ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
                                  edthistoricoCP.text:=ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FHistoricoSimples);
        End;
end;



procedure TFrLancamento.edtvalorlancamentoCPExit(Sender: TObject);
begin
    AtualizaLabelsCp;
end;

procedure TFrLancamento.edtvalorlancamentoCPKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then Begin
                if key='.'
                Then key:=','
                Else Key:=#0;
     End;
end;

procedure TFrLancamento.STRGCPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (STRGCP.cells[0,1]='')
     Then exit;

     if (key=vk_f12)
     Then Ordena_StringGrid(STRGCP,LbtipoCampoPG.Items);

     if ((ssctrl in shift) and (key=ord('P'))) or
       ((ssctrl in shift) and (key=ord('p')))
     Then Self.ImprimeGridPagamento;
end;

procedure TFrLancamento.edtpesquisa_STRG_GRID_CPKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key =#27//ESC
   then Begin
           edtpesquisa_STRG_GRID_CP.Visible := false;
           STRGCP.SetFocus;
           exit;
   End;

   if key=#13
   Then Begin
            Pesquisa_StringGrid(STRGCP,edtpesquisa_STRG_GRID_CP,LbtipoCampoPG.Items);
            if (edtpesquisa_STRG_GRID_CP.Visible=False)
            Then STRGCP.SetFocus;
   End;

end;

procedure TFrLancamento.STRGCPEnter(Sender: TObject);
begin
     edtpesquisa_STRG_GRID_CP.Visible:=false;
end;

procedure TFrLancamento.strgcrEnter(Sender: TObject);
begin
     edtpesquisa_STRG_GRID_CR.Visible:=false;
end;

procedure TFrLancamento.strgcrKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (STRGCR.cells[0,1]='')
     Then exit;

     if (key=vk_f12)
     Then Ordena_StringGrid(STRGCR,LbtipoCampoCR.Items);

     if ((ssctrl in shift) and (key=ord('P'))) or
       ((ssctrl in shift) and (key=ord('p')))
     Then Self.ImprimeGridRecebimento;
end;

procedure TFrLancamento.edtpesquisa_STRG_GRID_CRKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key =#27//ESC
   then Begin
           edtpesquisa_STRG_GRID_CR.Visible := false;
           STRGCR.SetFocus;
           exit;
   End;

   if key=#13
   Then Begin
            Pesquisa_StringGrid(STRGCR,edtpesquisa_STRG_GRID_CR,LbtipoCampoCR.Items);
            if (edtpesquisa_STRG_GRID_CR.Visible=False)
            Then STRGCR.SetFocus;
   End;

end;

procedure TFrLancamento.btlancaboletoClick(Sender: TObject);
var
PValorBoleto:Currency;
cont:integer;
StrPend,StrValorPend:TStringList;
Begin
     PvalorBoleto:=0;
     //Primeiro Somo os Valores anotados em cada pendencia
     for cont:=1 to STRGCR.RowCount-1 do
     Begin
          If (trim(StrgCR.Cells[8,cont])<>'')//verificando a coluna de valor a ser recebido
          Then PValorBoleto:=PvalorBoleto+strtofloat(tira_ponto(StrgCR.Cells[8,cont]));
     End;


     if (PValorBoleto=0)
     then begin
               Messagedlg('Nenhum boleto foi selecionado',mtinformation,[mbok],0);
               exit;
     End;

     if (Messagedlg('Valor do Boleto R$ '+formata_valor(Pvalorboleto)+#13+'Confirma?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     Try
        StrPend:=TStringList.Create;
        StrValorPend:=TStringList.Create;
        StrValorPend.clear;
        StrPend.clear;
     Except
           Messagedlg('Erro na Tentativa de Criar a StringList STRPEND',mterror,[mbok],0);
           exit; 

     End;
     Try
         //guardando as pendencias escolhidas
         for cont:=1 to StrgCR.RowCount-1 do
         Begin
              If (trim(StrgCR.Cells[8,cont])<>'')
              Then Begin
                        //a coluna 3 � o numero da pend�ncia
                        strPend.add(StrgCR.cells[3,cont]);
                        StrValorPend.add(tira_ponto(STRGCR.cells[8,cont]));
              End;
         End;

         for cont:=0 to StrPend.Count-1 do
         Begin
              ObjLancamento.Pendencia.ZerarTabela;
              if (ObjLancamento.Pendencia.LocalizaCodigo(StrPend[cont])=False)
              Then Begin
                        Messagedlg('Pend�ncia '+strpend[cont]+' n�o encontrado',mterror,[mbok],0);
                        exit;
              End;
              ObjLancamento.Pendencia.TabelaparaObjeto;
              if (ObjLancamento.Pendencia.BoletoBancario.Get_CODIGO<>'')
              Then begin
                        Messagedlg('A pend�ncia '+ObjLancamento.Pendencia.get_codigo+' j� possui um boleto',mterror,[mbok],0);
                        exit;
              End;
         End;

         //Passo as Pendencias, e o Valor que o Boleto sera gerado
         ObjLancamento.Pendencia.LancaBoleto_Varias_Pendencias(StrPend,StrValorPend,PValorBoleto);
     Finally
            Freeandnil(StrPend);
            Freeandnil(StrValorPend);
     End;
end;

procedure TFrLancamento.btimprimecomprovantepagamentoClick(Sender: TObject);
var
PCodigo:String;
Begin
     Pcodigo:='';

     if (InputQuery('Recibo','Digite o C�digo do Lan�amento',pcodigo)=False)
     Then exit;
     
     Self.ObjLancamento.ImprimeReciboPagamento(Pcodigo);
end;

procedure TFrLancamento.BtSelecionaTodas_PGClick(Sender: TObject);
var
cont:integer;
begin
     //seleciona todas com o valor do saldo
     For cont:=1 to STRGCP.RowCount-1 do
     Begin
          if (trim(STRGCP.cells[8,Cont])='')
          Then STRGCP.cells[8,Cont]:=STRGCP.cells[7,Cont];
     end;
     atualizalabelscp;
     STRGCP.SetFocus;
end;

procedure TFrLancamento.BtSelecionaTodas_CRClick(Sender: TObject);
var
cont:integer;
begin
     //seleciona todas com o valor do saldo
     For cont:=1 to STRGCR.RowCount-1 do
     Begin
          if (trim(STRGCR.cells[8,Cont])='')
          Then STRGCR.cells[8,Cont]:=STRGCR.cells[7,Cont];
     end;
     atualizalabelscr;
     STRGCR.SetFocus;
end;

procedure TFrLancamento.edtcodigoboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjLancamento.Pendencia.edtBoletoUsadoKeyDown(sender,key,shift);
end;

procedure TFrlancamento.SalvaContasReceber_UmaPendencia(ppendencia,pvalor:string);
var
Phistorico:String;
Pcodigo:string;
begin
     phistorico:='';

     if (edthistoricoCR.Text='')
     Then Begin
              Self.ObjLancamento.Pendencia.LocalizaCodigo(ppendencia);
              Self.ObjLancamento.Pendencia.TabelaparaObjeto;
              Phistorico:='Recebimento '+Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
     end
     Else Phistorico:=edthistoricoCR.text;
     
     Pcodigo:=Self.ObjLancamento.Get_NovoCodigo;

     
     //chamo essa versao do NovoLancamento pois o codigo retorna
     if (Self.ObjLancamento.NovoLancamento(ppendencia,'R',pvalor,pcodigo,phistorico,False,true,true,'','',datetostr(now))=False)
     Then Begin
               FdataModulo.IBTransaction.RollbackRetaining;
               Messagedlg('Erro na tentativa de Salvar',mterror,[mbok],0);
               desabilita_campos(self);
               exit;
     End;

     FdataModulo.IBTransaction.CommitRetaining;
     
     EdtvencimentocR.text:=VencimentoCR;
     BtpesquisacR.OnClick(nil);
end;


procedure TFrlancamento.SalvaContasPagar_UmaPendencia(ppendencia,pvalor:string);
var
Phistorico:String;
Pcodigo:string;
begin
     phistorico:='';

     if (edthistoricoCP.Text='')
     Then Begin
              Self.ObjLancamento.Pendencia.LocalizaCodigo(ppendencia);
              Self.ObjLancamento.Pendencia.TabelaparaObjeto;
              Phistorico:='Pagamento '+Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCREDORDevedor(Self.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,Self.ObjLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
     end
     Else Phistorico:=edthistoricoCP.text;
     
     Pcodigo:=Self.ObjLancamento.Get_NovoCodigo;

     
     //chamo essa versao do NovoLancamento pois o codigo retorna
     if (Self.ObjLancamento.NovoLancamento(ppendencia,'P',pvalor,pcodigo,phistorico,False,true,true,'','',datetostr(now))=False)
     Then Begin
               FdataModulo.IBTransaction.RollbackRetaining;
               Messagedlg('Erro na tentativa de Salvar',mterror,[mbok],0);
               desabilita_campos(self);
               exit;
     End;

     FdataModulo.IBTransaction.CommitRetaining;
     
     EdtvencimentoCP.text:=VencimentoCP;
     BtpesquisaCP.OnClick(nil);
end;


procedure TFrLancamento.ImprimeGridRecebimento;
var
cont:integer;
psomasaldo,psomaareceber,psomavalor:currency;
begin
     With FreltxtRDPRINT do
     Begin
          ConfiguraImpressao;
          RDprint.TamanhoQteColunas:=130;
          RDprint.FonteTamanhoPadrao:=S17cpp;
          LinhaLocal:=3;
          rdprint.Abrir;

          if (rdprint.setup=False)
          then Begin
                    rdprint.Fechar;
                    exit;
          End;


          RDprint.impc(linhalocal,65,'DEMONSTRATIVO DE CONTAS A RECEBER',[negrito]);
          IncrementaLinha(2);


          RDprint.impf(linhalocal,1,CompletaPalavra('Hist�rico',50,' ')+' '+
                                    CompletaPalavra('Vencimento',10,' ')+' '+
                                    CompletaPalavra_a_Esquerda('Valor',12,' ')+' '+
                                    CompletaPalavra_a_Esquerda('Saldo',12,' ')+' '+
                                    CompletaPalavra_a_Esquerda('Vl.a.Receber',12,' ')+' '+
                                    CompletaPalavra('N� Documento',29,' '),[negrito]);
          IncrementaLinha(1);
          DesenhaLinha;

          psomavalor:=0;
          psomasaldo:=0;
          psomaareceber:=0;

          for cont:=1 to strgcr.rowcount-1 do
          Begin
               VerificaLinha;
               RDprint.imp(linhalocal,1,CompletaPalavra(strgcr.Cells[1,cont],50,' ')+' '+
                                        CompletaPalavra(strgcr.Cells[5,cont],10,' ')+' '+
                                        CompletaPalavra_a_Esquerda(strgcr.Cells[6,cont],12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(strgcr.Cells[7,cont],12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(strgcr.Cells[8,cont],12,' ')+' '+
                                        CompletaPalavra(strgcr.Cells[9,cont],29,' '));
               IncrementaLinha(1);

               psomavalor:=psomavalor+strtocurr(tira_ponto(strgcr.Cells[6,cont]));
               psomasaldo:=psomasaldo+strtocurr(tira_ponto(strgcr.Cells[7,cont]));
               Try
                  if (trim(strgcr.Cells[8,cont])<>'')
                  Then psomaareceber:=psomaareceber+strtocurr(tira_ponto(strgcr.Cells[8,cont]));
               Except

               End;
          end;
          DesenhaLinha;
          VerificaLinha;
          RDprint.impf(linhalocal,1,CompletaPalavra('SOMA',50,' ')+' '+
                                   CompletaPalavra('',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(PSOMAVALOR),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(PSOMASALDO),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(PSOMAARECEBER),12,' ')+' '+
                                   CompletaPalavra('',29,' '),[negrito]);
          IncrementaLinha(1);
          
          rdprint.fechar;
     End;
end;


procedure TFrLancamento.ImprimeGridPagamento;
var
cont:integer;
psomasaldo,psomaapagar,psomavalor:currency;
begin
     With FreltxtRDPRINT do
     Begin
          ConfiguraImpressao;
          RDprint.TamanhoQteColunas:=130;
          RDprint.FonteTamanhoPadrao:=S17cpp;
          LinhaLocal:=3;
          rdprint.Abrir;

          if (rdprint.setup=False)
          then Begin
                    rdprint.Fechar;
                    exit;
          End;


          RDprint.impc(linhalocal,65,'DEMONSTRATIVO DE CONTAS A PAGAR',[negrito]);
          IncrementaLinha(2);


          RDprint.impf(linhalocal,1,CompletaPalavra('Pend�ncia',9,' ')+' '+
                                    CompletaPalavra('Hist�rico',50,' ')+' '+
                                    CompletaPalavra('Vencimento',10,' ')+' '+
                                    CompletaPalavra_a_Esquerda('Valor',12,' ')+' '+
                                    CompletaPalavra_a_Esquerda('Saldo',12,' ')+' '+
                                    CompletaPalavra_a_Esquerda('Vl.a.Pagar',12,' ')+' '+
                                    CompletaPalavra('N� Documento',19,' '),[negrito]);
          IncrementaLinha(1);
          DesenhaLinha;

          psomavalor:=0;
          psomasaldo:=0;
          psomaapagar:=0;

          for cont:=1 to STRGCP.rowcount-1 do
          Begin
               VerificaLinha;
               RDprint.imp(linhalocal,1,CompletaPalavra(STRGCP.Cells[3,cont],9,' ')+' '+
                                        CompletaPalavra(STRGCP.Cells[1,cont],50,' ')+' '+
                                        CompletaPalavra(STRGCP.Cells[5,cont],10,' ')+' '+
                                        CompletaPalavra_a_Esquerda(STRGCP.Cells[6,cont],12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(STRGCP.Cells[7,cont],12,' ')+' '+
                                        CompletaPalavra_a_Esquerda(STRGCP.Cells[8,cont],12,' ')+' '+
                                        CompletaPalavra(STRGCP.Cells[9,cont],19,' '));
               IncrementaLinha(1);

               psomavalor:=psomavalor+strtocurr(tira_ponto(STRGCP.Cells[6,cont]));
               psomasaldo:=psomasaldo+strtocurr(tira_ponto(STRGCP.Cells[7,cont]));
               Try
                  if (trim(STRGCP.Cells[8,cont])<>'')
                  Then psomaapagar:=psomaapagar+strtocurr(tira_ponto(STRGCP.Cells[8,cont]));
               Except

               End;
          end;
          DesenhaLinha;
          VerificaLinha;
          RDprint.impf(linhalocal,1,CompletaPalavra('SOMA',60,' ')+' '+
                                   CompletaPalavra('',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(PSOMAVALOR),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(PSOMASALDO),12,' ')+' '+
                                   CompletaPalavra_a_Esquerda(formata_valor(psomaapagar),12,' ')+' '+
                                   CompletaPalavra('',19,' '),[negrito]);
          IncrementaLinha(1);

          rdprint.fechar;
     End;
end;

procedure TFrLancamento.Button1Click(Sender: TObject);
begin
     Self.LancaDesconto('R');
end;

procedure TFrLancamento.Button2Click(Sender: TObject);
begin
    Self.LancaDesconto('%');
end;

procedure TFrLancamento.strgcrDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
temp:string;
pvalor:currency;
begin

    if (Arow=0)
    Then exit;

    temp:=trim((Sender as TStringGrid).Cells[(Sender as TStringGrid).tag,Arow]);

    If (temp<>'')
    then begin
              Try
                 pvalor:=strtofloat(come(temp,'.'));
              Except
                    pvalor:=0;
              End;
    End
    Else Pvalor:=0;



    if (pvalor>0)
    Then Begin
          (Sender as TStringGrid).canvas.brush.color :=RGB(255,255,153);
          (Sender as TStringGrid).canvas.Font.Color:=clblack;

          (Sender as TStringGrid).canvas.FillRect(rect);
          (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
    end
    Else Begin
              (Sender as TStringGrid).canvas.brush.color :=clwhite;
              (Sender as TStringGrid).canvas.Font.Color:=clblack;

              (Sender as TStringGrid).canvas.FillRect(rect);
              (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
    End;


end;

procedure TFrLancamento.btAlterarHistoricoLancamentoClick(Sender: TObject);
begin
    objLancamento.AlterarHistoricoLancamento;
end;

procedure TFrLancamento.edtcontagerencial_CPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FpesquisaLocal.NomeCadastroPersonalizacao:='edtcontagerencialKeyDown_LANCAMENTOFR';
            
            If (FpesquisaLocal.PreparaPesquisaN(ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Pesquisa2('D'),'Contas Gerenciais - T�tulos a Pagar','')=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtcontagerencial_CP.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFrLancamento.edtsubcontagerencial_CPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FpesquisaLocal.NomeCadastroPersonalizacao:='edtsubcontagerencialKeyDown_LANCAMENTOFR';
            
            If (FpesquisaLocal.PreparaPesquisa(ObjLancamento.Pendencia.Titulo.SubContaGerencial.Get_Pesquisa(edtcontagerencial_CP.Text),'Sub-Contas Gerenciais - T�tulos a Pagar',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtsubcontagerencial_CP.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;
     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

{Rodolfo}
procedure TFrLancamento.edtcodigocredordevedorCPDblClick(Sender: TObject);
var
   key : Word;
   shift : TShiftState;
begin
    key := VK_F9;
    edtcodigocredordevedorCPKeyDown(Sender, Key, Shift);
end;

{Rodolfo}
procedure TFrLancamento.edtcontagerencial_CPDblClick(Sender: TObject);
var
   key : Word;
   shift : TShiftState;
begin
    key := VK_F9;
    edtcontagerencial_CPKeyDown(Sender, Key, Shift);
end;

{Rodolfo}
procedure TFrLancamento.edtsubcontagerencial_CPDblClick(Sender: TObject);
var
   key : Word;
   shift : TShiftState;
begin
    key := VK_F9;
    edtsubcontagerencial_CPKeyDown(Sender, Key, Shift);
end;

{rodolfo}
procedure TFrLancamento.edtcodigocredordevedorcrDblClick(Sender: TObject);
var
   key : Word;
   shift : TShiftState;
begin
    key := VK_F9;
    edtcodigocredordevedorcrKeyDown(Sender, Key, Shift);
end;

{Rodolfo}
procedure TFrLancamento.edtcodigoboletoDblClick(Sender: TObject);
var
   key : Word;
   shift : TShiftState;
begin
    key := VK_F9;
    edtcodigoboletoKeyDown(Sender, Key, Shift);
end;

end.
