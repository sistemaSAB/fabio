unit UObjGeraTransferencia;
Interface
Uses Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjPortador,UobjGrupo;

Type
   TObjGeraTransferencia=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                PortadorDestino  :TobjPortador;
                Grupo            :TobjGrupo;
                PortadorOrigem   :TobjPortador;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaHistorico(Parametro:string):boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Function    Get_Pesquisaportadordestino     :string;
                Function    Get_TituloPesquisaportadordestino:string;
                Function    Get_PesquisaGrupo                 :string;
                Function    Get_TituloPesquisaGrupo           :string;




                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Historico        :string;
                Function Get_Portadordestino  :string;
                Function Get_Grupo            :string;
                Function Get_PortadorOrigem   :string;





                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Historico        (parametro:string);
                Procedure Submit_Portadordestino  (parametro:string);
                Procedure Submit_Grupo            (parametro:string);
                Procedure Submit_PortadorOrigem   (parametro:string);


                Function  Get_NovoCodigo:string;



         Private
               ObjDataset:Tibdataset;

               CODIGO           :String[09];
               HISTORICO        :String[50];






                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Classes,SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Procedure  TObjGeraTransferencia.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.HISTORICO        :=FieldByName('HISTORICO').asstring;

        if (FieldByName('Grupo').asstring<>'')
        Then Begin
                If (Self.Grupo.LocalizaCodigo(FieldByName('Grupo').asstring)=False)
                Then Begin
                        Messagedlg('Grupo N�o encontrado!',mterror,[mbok],0);
                        Self.ZerarTabela;
                        exit;
                End
                Else Self.Grupo.TabelaparaObjeto;
        End;

        If (Self.PortadorDestino.LocalizaCodigo(FieldByName('portadordestino').asstring)=False)
        Then Begin
                  Messagedlg('Portador de Destino N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.PortadorDestino.TabelaparaObjeto;

        If (Self.PortadorOrigem.LocalizaCodigo(FieldByName('portadorOrigem').asstring)=False)
        Then Begin
                  Messagedlg('Portador de Origem N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.PortadorOrigem.TabelaparaObjeto;

     End;
end;


Procedure TObjGeraTransferencia.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring           :=        Self.CODIGO                     ;
      FieldByName('historico').asstring        :=        Self.historico                  ;
      FieldByName('portadordestino').asstring  :=        Self.portadorDestino.Get_codigo ;
      FieldByName('Grupo').asstring            :=        Self.grupo.Get_codigo ;
      FieldByName('portadorOrigem').asstring   :=        Self.portadorOrigem.Get_codigo ;
  End;
End;

//***********************************************************************

function TObjGeraTransferencia.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjGeraTransferencia.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.historico        :='';
        Self.grupo.ZerarTabela;
        Self.PortadorDestino.ZerarTabela;
        Self.PortadorOrigem.ZerarTabela;
     End;
end;

Function TObjGeraTransferencia.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (historico='')
       Then Mensagem:=mensagem+'/Historico';

       If (PortadorOrigem.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Portador de Origem';

       If (Grupo.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Grupo';

       If (PortadorDestino.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Portador de Destino';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjGeraTransferencia.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     If (Self.Grupo.LocalizaCodigo(Self.grupo.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Grupo n�o Encontrado!';

     If (Self.PortadorOrigem.LocalizaCodigo(Self.PortadorOrigem.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador de Origem n�o Encontrado!';

     If (Self.PortadorDestino.LocalizaCodigo(Self.PortadorDestino.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador de Destino n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjGeraTransferencia.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.Grupo.get_codigo);
     Except
           Mensagem:=mensagem+'/Grupo';
     End;

     try
        Inteiros:=Strtoint(Self.PortadorOrigem.get_codigo);
     Except
           Mensagem:=mensagem+'/Portador de Origem';
     End;

     try
        Inteiros:=Strtoint(Self.PortadorDestino.get_codigo);
     Except
           Mensagem:=mensagem+'/Portador de Destino';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjGeraTransferencia.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjGeraTransferencia.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjGeraTransferencia.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,portadordestino,grupo,portadororigem from Tabgeratransferencia where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjGeraTransferencia.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjGeraTransferencia.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjGeraTransferencia.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.Grupo:=TObjGrupo.create;
        Self.PortadorDestino:=TObjPortador.create;
        Self.PortadorOrigem:=TObjPortador.create;
        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,historico,portadordestino,grupo,portadororigem from Tabgeratransferencia where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert Into TabGeraTransferencia (codigo,historico,portadordestino,grupo,portadororigem) values (:codigo,:historico,:portadordestino,:grupo,:portadororigem)');

                ModifySQL.clear;
                ModifySQL.add(' Update TabgeraTransferencia set codigo=:codigo,historico=:historico,portadordestino=:portadordestino,grupo=:Grupo,portadororigem=:PortadorOrigem where codigo=:codigo ');


                DeleteSQL.clear;
                DeleteSQL.add(' Delete from TabGeraTransferencia where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,historico,portadordestino,grupo,portadororigem from Tabgeratransferencia where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjGeraTransferencia.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjGeraTransferencia.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjGeraTransferencia.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjGeraTransferencia.Get_Pesquisa: string;
begin
     Result:=' Select * from TabGeraTransferencia ';
end;

function TObjGeraTransferencia.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Geradores de Transfer�ncia ';
end;




function TObjGeraTransferencia.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERATRANSFERENCIA' ;
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Gerador de Transfer�ncia',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


function TObjGeraTransferencia.Get_Historico: string;
begin
     Result:=Self.HISTORICO;
end;

function TObjGeraTransferencia.Get_Pesquisaportadordestino: string;
begin
     Result:=Self.PortadorDestino.Get_Pesquisa;
end;

function TObjGeraTransferencia.Get_Portadordestino: string;
begin
     Result:=Self.PortadorDestino.Get_codigo;
end;

function TObjGeraTransferencia.Get_TituloPesquisaportadordestino: string;
begin
          Result:=Self.PortadorDestino.Get_TituloPesquisa;
end;

procedure TObjGeraTransferencia.Submit_Historico(parametro: string);
begin
     Self.HISTORICO:=parametro;
end;

procedure TObjGeraTransferencia.Submit_Portadordestino(parametro: string);
begin
          Self.PortadorDestino.Submit_CODIGO(parametro);
end;

function TObjGeraTransferencia.LocalizaHistorico(
  Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,portadordestino,grupo,portadororigem from Tabgeratransferencia where historico='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjGeraTransferencia.Get_Grupo: string;
begin
     Result:=Self.Grupo.Get_codigo;
end;


function TObjGeraTransferencia.Get_PesquisaGrupo: string;
begin
     Result:=Self.grupo.Get_Pesquisa;
end;

function TObjGeraTransferencia.Get_PortadorOrigem: string;
begin
     Result:=Self.PortadorOrigem.Get_CODIGO;
end;


procedure TObjGeraTransferencia.Submit_Grupo(parametro: string);
begin
     Self.Grupo.Submit_CODIGO(parametro);
end;


procedure TObjGeraTransferencia.Submit_PortadorOrigem(parametro: string);
begin
     Self.PortadorOrigem.Submit_CODIGO(parametro)
end;

function TObjGeraTransferencia.Get_TituloPesquisaGrupo: string;
begin
     Result:=Self.grupo.Get_TituloPesquisa;
end;

destructor TObjGeraTransferencia.Free;
begin
     Freeandnil(Self.ObjDataset);
     Self.Grupo.free;
     Self.PortadorDestino.free;
     Self.PortadorOrigem.free;

end;

end.
