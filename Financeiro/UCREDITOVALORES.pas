unit UCREDITOVALORES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjCREDITOvalores;

type
  TFCREDITOVALORES = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btopcoes: TBitBtn;
    Panel2: TPanel;
    LbCODIGO: TLabel;
    Lbcredordevedor: TLabel;
    LbNomecredordevedor: TLabel;
    Lbcodigocredordevedor: TLabel;
    lbvalores: TLabel;
    Lbvalorusado: TLabel;
    EdtCODIGO: TEdit;
    Edtcredordevedor: TEdit;
    Edtcodigocredordevedor: TEdit;
    edtvalores: TEdit;
    Edtvalorusado: TEdit;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
//DECLARA COMPONENTES
    procedure edtcredordevedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtcredordevedorExit(Sender: TObject);
    procedure edtvaloresKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtvaloresExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCREDITOvalores:TObjCREDITOvalores;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCREDITOVALORES: TFCREDITOVALORES;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCREDITOVALORES.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCREDITOvalores do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        credordevedor.Submit_codigo(edtcredordevedor.text);
        Submit_codigocredordevedor(edtcodigocredordevedor.text);
        valores.Submit_codigo(edtvalores.text);
        Submit_valorusado(edtvalorusado.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCREDITOVALORES.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCREDITOvalores do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        Edtcredordevedor.text:=credordevedor.Get_codigo;
        Edtcodigocredordevedor.text:=Get_codigocredordevedor;
        Edtvalores.text:=valores.Get_codigo;
        Edtvalorusado.text:=Get_valorusado;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCREDITOVALORES.TabelaParaControles: Boolean;
begin
     If (Self.ObjCREDITOvalores.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCREDITOVALORES.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCREDITOvalores=Nil)
     Then exit;

    If (Self.ObjCREDITOvalores.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    Self.ObjCREDITOvalores.free;
end;

procedure TFCREDITOVALORES.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCREDITOVALORES.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCREDITOvalores.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjCREDITOvalores.status:=dsInsert;
     edtcredordevedor.setfocus;

end;


procedure TFCREDITOVALORES.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCREDITOvalores.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCREDITOvalores.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtcredordevedor.setfocus;
                
          End;


end;

procedure TFCREDITOVALORES.btgravarClick(Sender: TObject);
begin

     If Self.ObjCREDITOvalores.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCREDITOvalores.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCREDITOvalores.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCREDITOVALORES.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCREDITOvalores.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCREDITOvalores.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCREDITOvalores.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCREDITOVALORES.btcancelarClick(Sender: TObject);
begin
     Self.ObjCREDITOvalores.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCREDITOVALORES.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCREDITOVALORES.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCREDITOvalores.Get_pesquisa,Self.ObjCREDITOvalores.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCREDITOvalores.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCREDITOvalores.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCREDITOvalores.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCREDITOVALORES.LimpaLabels;
begin
//LIMPA LABELS
    lbnomecredordevedor.caption:='';
end;

procedure TFCREDITOVALORES.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCREDITOvalores:=TObjCREDITOvalores.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;

     lbquantidade.caption:=atualizaQuantidade;

end;
procedure TFCREDITOVALORES.edtcredordevedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCREDITOvalores.edtcredordevedorkeydown(sender,key,shift,lbnomecredordevedor);
end;
 
procedure TFCREDITOVALORES.edtcredordevedorExit(Sender: TObject);
begin
    ObjCREDITOvalores.edtcredordevedorExit(sender,lbnomecredordevedor);
end;
procedure TFCREDITOVALORES.edtvaloresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCREDITOvalores.edtvaloreskeydown(sender,key,shift,nil);
end;
 
procedure TFCREDITOVALORES.edtvaloresExit(Sender: TObject);
begin
    ObjCREDITOvalores.edtvaloresExit(sender,nil);
end;
//CODIFICA ONKEYDOWN E ONEXIT


function TFCREDITOVALORES.atualizaQuantidade: string;
begin
        result:='Existem '+ContaRegistros('TABCREDITOVALORES','codigo')+' Valores de Cr�ditos Cadastrados'
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjCREDITOvalores.OBJETO.Get_Pesquisa,Self.ObjCREDITOvalores.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCREDITOvalores.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjCREDITOvalores.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCREDITOvalores.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
