object FEmiteCartaCobranca: TFEmiteCartaCobranca
  Left = 339
  Top = 158
  Width = 763
  Height = 574
  Caption = 'Emite carta de cobran'#231'a'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 747
    Height = 536
    Align = alClient
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'MS Sans Serif'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object Label1: TLabel
        Left = 4
        Top = 54
        Width = 117
        Height = 14
        Caption = 'Credor / Devedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 4
        Width = 90
        Height = 14
        Caption = 'Vencto Inicial'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 106
        Top = 4
        Width = 81
        Height = 14
        Caption = 'Vencto Final'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbNomeCredorDevedor: TLabel
        Left = 4
        Top = 97
        Width = 524
        Height = 13
        AutoSize = False
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clTeal
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 163
        Top = 54
        Width = 158
        Height = 14
        Caption = 'C'#243'digo Credor/Devedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object StrGrid: TStringGrid
        Left = 0
        Top = 181
        Width = 739
        Height = 285
        Align = alBottom
        RowCount = 2
        TabOrder = 7
        OnDblClick = StrGridDblClick
        OnKeyPress = StrGridKeyPress
        ColWidths = (
          31
          64
          64
          64
          64)
        RowHeights = (
          24
          24)
      end
      object ComboNomeCredorDevedor: TComboBox
        Left = 4
        Top = 69
        Width = 157
        Height = 22
        BevelKind = bkSoft
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        ItemHeight = 14
        ParentFont = False
        TabOrder = 3
        OnClick = ComboNomeCredorDevedorClick
        OnExit = ComboNomeCredorDevedorExit
        OnKeyDown = ComboNomeCredorDevedorKeyDown
        OnKeyPress = ComboNomeCredorDevedorKeyPress
        OnKeyUp = ComboNomeCredorDevedorKeyUp
      end
      object btPesquisar: TBitBtn
        Left = 641
        Top = 156
        Width = 105
        Height = 30
        Caption = 'Pesquisar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        OnClick = btPesquisarClick
      end
      object edtVencimentoInicial: TMaskEdit
        Left = 4
        Top = 20
        Width = 79
        Height = 20
        EditMask = '!99/99/9999;1;_'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 0
        Text = '  /  /    '
      end
      object edtVencimentoFinal: TMaskEdit
        Left = 105
        Top = 20
        Width = 80
        Height = 20
        EditMask = '!99/99/9999;1;_'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 1
        Text = '  /  /    '
      end
      object ComboCodigoTabelaCredorDevedor: TComboBox
        Left = 196
        Top = 21
        Width = 64
        Height = 21
        BevelKind = bkSoft
        ItemHeight = 13
        TabOrder = 2
        Visible = False
      end
      object Panel1: TPanel
        Left = 0
        Top = 466
        Width = 739
        Height = 42
        Align = alBottom
        Color = 3355443
        TabOrder = 8
        DesignSize = (
          739
          42)
        object btsair: TBitBtn
          Left = 645
          Top = 3
          Width = 100
          Height = 38
          Anchors = [akRight, akBottom]
          Caption = '&Sair'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btSairClick
        end
        object btOpcoes: TBitBtn
          Left = 544
          Top = 3
          Width = 100
          Height = 38
          Anchors = [akRight, akBottom]
          Caption = 'Visualizar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btOpcoesClick
        end
        object ChecMarcarDesmarcarTodos: TCheckBox
          Left = 8
          Top = 3
          Width = 211
          Height = 17
          Caption = 'Marcar / Desmarcar todos'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -12
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = ChecMarcarDesmarcarTodosClick
        end
      end
      object edtcodigocredordevedor: TEdit
        Left = 165
        Top = 69
        Width = 39
        Height = 20
        Color = clWhite
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 10
        ParentFont = False
        TabOrder = 4
        OnExit = edtcodigocredordevedorExit
        OnKeyDown = edtcodigocredordevedorKeyDown
        OnKeyPress = edtcodigocredordevedorKeyPress
      end
      object RgOpcoes: TRadioGroup
        Left = 1
        Top = 115
        Width = 185
        Height = 71
        Caption = 'Op'#231#245'es de Pesquisa'
        ItemIndex = 0
        Items.Strings = (
          'Agrupa Pend'#234'ncias por T'#237'tulo'
          'Lista Pend'#234'ncias')
        TabOrder = 5
        OnClick = RgOpcoesClick
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Instru'#231#245'es'
      object TabbedNotebook1: TTabbedNotebook
        Left = 0
        Top = 0
        Width = 739
        Height = 508
        Align = alClient
        TabFont.Charset = DEFAULT_CHARSET
        TabFont.Color = clBtnText
        TabFont.Height = -11
        TabFont.Name = 'MS Sans Serif'
        TabFont.Style = []
        TabOrder = 0
        object TTabPage
          Left = 4
          Top = 24
          Caption = 'Um carta por T'#237'tulo'
          object Label6: TLabel
            Left = 0
            Top = 0
            Width = 731
            Height = 17
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            Caption = 'VARI'#193'VEIS DISPON'#205'VEIS'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
          end
          object Memo1: TMemo
            Left = 0
            Top = 17
            Width = 731
            Height = 463
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            Lines.Strings = (
              'Vari'#225'veis relacionadas ao t'#237'tulo, podem ser usadas at'#233' 10 c'#243'pias'
              ''
              '[DATA]                  - data atual'
              '[TITULO]                - titulo'
              '[VALORTITULO]           - valor do titulo'
              '[EMISSAO]               - Data de Emiss'#227'o do Titulo'
              '[TOTAL_DA_DIVIDA]       - total das pendencias'
              '[NOMECLIENTE]           - nome do cliente'
              '[VENCIMENTOS]           - linha com vencimentos'
              ''
              
                'Vari'#225'veis relacionadas a pend'#234'ncias, s'#227'o impressas de acordo com' +
                ' o n'#250'mero de pend'#234'ncias'
              ''
              '[PENDENCIA]             - numero da pendencia'
              
                '[PENDENCIA&ENTER]       - numero da pendencia seguido de um ENTE' +
                'R'
              '[PENDENCIA&VENCIMENTO&SALDO]- pendencia,vencimento e saldo'
              
                '[TITULO&PENDENCIA&VENCIMENTO&SALDO]- t'#237'tulo,pendencia,vencimento' +
                ' e saldo da pendencia'
              ''
              ''
              '[SALDO]                 - saldo da pendencia'
              '[SALDO&ENTER]           - saldo da pendencia seguido de um ENTER'
              '[VENCIMENTO]            - vencimento da pendencia'
              
                '[VENCIMENTO&ENTER]      - vencimento da pendencia seguido de um ' +
                'ENTER'
              '[VENCIMENTO&SALDO]      - vencimento e saldo da pendencia'
              
                '[VENCIMENTO&SALDO&ENTER]- vencimento e saldo da pendencia seguid' +
                'o de um ENTER'
              
                '[PENDENCIA&VENCIMENTO&SALDO&ENTER]- pendencia,vencimento e saldo' +
                ' da pendencia seguido de um ENTER'
              
                '[TITULO&PENDENCIA&VENCIMENTO&SALDO&ENTER]- t'#237'tulo,pendencia,venc' +
                'imento e saldo da pendencia seguido de '
              'um '
              'ENTER')
            ParentFont = False
            TabOrder = 0
          end
        end
        object TTabPage
          Left = 4
          Top = 24
          Caption = 'Uma carta por Cliente'
          object Label7: TLabel
            Left = 0
            Top = 0
            Width = 731
            Height = 17
            Align = alTop
            Alignment = taCenter
            AutoSize = False
            Caption = 'VARI'#193'VEIS DISPON'#205'VEIS'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
            Transparent = True
          end
          object Memo2: TMemo
            Left = 0
            Top = 17
            Width = 731
            Height = 463
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            Lines.Strings = (
              'Vari'#225'veis relacionadas ao t'#237'tulo, podem ser usadas at'#233' 10 c'#243'pias'
              ''
              '[DATA]                  - data atual'
              
                '[TITULO]                - titulo em sequ'#234'ncia separados por v'#237'rg' +
                'ula'
              
                '[VALORTITULO]           - valor do titulo em sequ'#234'ncia separado ' +
                'por v'#237'rgula'
              
                '[EMISSAO]               - Data de Emiss'#227'o do Titulo sem sequ'#234'nci' +
                'a separado por v'#237'rgula'
              
                '[TOTAL_DA_DIVIDA]       - total das pendencias de cada t'#237'tulo se' +
                'parado por v'#237'rgula'
              '[NOMECLIENTE]           - nome do cliente'
              
                '[VENCIMENTOS]           - linha com vencimentos separados por v'#237 +
                'rgula'
              ''
              
                'Vari'#225'veis relacionadas a pend'#234'ncias, s'#227'o impressas de acordo com' +
                ' o n'#250'mero de pend'#234'ncias'
              '[PENDENCIA]             - numero da pendencia de cada t'#237'tulo'
              
                '[PENDENCIA&ENTER]       - numero da pendencia de cada t'#237'tulo seg' +
                'uido de um ENTER'
              
                '[PENDENCIA&VENCIMENTO&SALDO]- pendencia,vencimento e saldo de ca' +
                'da t'#237'tulo'
              '[SALDO]                 - saldo da pendencia de cada t'#237'tulo'
              
                '[SALDO&ENTER]           - saldo da pendencia de cada t'#237'tulo segu' +
                'ido de um ENTER'
              '[VENCIMENTO]            - vencimento da pendencia de cada t'#237'tulo'
              
                '[VENCIMENTO&ENTER]      - vencimento da pendencia de cada t'#237'tulo' +
                ' seguido de um ENTER'
              
                '[VENCIMENTO&SALDO]      - vencimento e saldo da pendencia de cad' +
                'a t'#237'tulo'
              
                '[VENCIMENTO&SALDO&ENTER]- vencimento e saldo da pendencia de cad' +
                'a t'#237'tulo seguido de um ENTER'
              
                '[PENDENCIA&VENCIMENTO&SALDO&ENTER]- pendencia,vencimento e saldo' +
                ' de cada t'#237'tulo da pendencia seguido de '
              'um ENTER'
              
                '[TITULO&PENDENCIA&VENCIMENTO&SALDO&ENTER]- t'#237'tulo,pendencia,venc' +
                'imento e saldo da pendencia seguido de '
              'um '
              'ENTER')
            ParentFont = False
            TabOrder = 0
          end
        end
      end
    end
  end
end
