object FImpDuplicata: TFImpDuplicata
  Left = 341
  Top = 185
  Width = 705
  Height = 540
  Caption = 'FImpDuplicata'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object QR: TQuickRep
    Left = 0
    Top = 24
    Width = 595
    Height = 842
    Frame.Color = clBlack
    Frame.DrawTop = False
    Frame.DrawBottom = False
    Frame.DrawLeft = False
    Frame.DrawRight = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    Functions.Strings = (
      'PAGENUMBER'
      'COLUMNNUMBER'
      'REPORTTITLE')
    Functions.DATA = (
      '0'
      '0'
      #39#39)
    Options = [FirstPageHeader, LastPageFooter]
    Page.Columns = 1
    Page.Orientation = poPortrait
    Page.PaperSize = A4
    Page.Values = (
      100.000000000000000000
      2970.000000000000000000
      100.000000000000000000
      2100.000000000000000000
      100.000000000000000000
      100.000000000000000000
      0.000000000000000000)
    PrinterSettings.Copies = 1
    PrinterSettings.Duplex = False
    PrinterSettings.FirstPage = 0
    PrinterSettings.LastPage = 0
    PrinterSettings.OutputBin = Auto
    PrintIfEmpty = True
    SnapToGrid = True
    Units = MM
    Zoom = 75
    object ValorFatura: TQRLabel
      Left = 58
      Top = 36
      Width = 51
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        204.611111111111100000
        127.000000000000000000
        179.916666666666700000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'ValorFatura'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object NumeroFatura: TQRLabel
      Left = 70
      Top = 60
      Width = 62
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        246.944444444444400000
        211.666666666666700000
        218.722222222222200000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'NumeroFatura'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object ValorDuplicata: TQRLabel
      Left = 150
      Top = 60
      Width = 64
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        529.166666666666700000
        211.666666666666700000
        225.777777777777800000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'ValorDuplicata'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object numeroduplicata: TQRLabel
      Left = 302
      Top = 84
      Width = 72
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        1065.388888888889000000
        296.333333333333300000
        254.000000000000000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'numeroduplicata'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object vencimento: TQRLabel
      Left = 394
      Top = 44
      Width = 50
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        1389.944444444444000000
        155.222222222222200000
        176.388888888888900000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'vencimento'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object dataemissao: TQRLabel
      Left = 600
      Top = 30
      Width = 56
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        2116.666666666670000000
        105.833333333333000000
        197.555555555556000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'dataemissao'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object nome: TQRLabel
      Left = 60
      Top = 329
      Width = 25
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        211.666666666666700000
        1160.638888888889000000
        88.194444444444440000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'nome'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object endereco: TQRLabel
      Left = 60
      Top = 360
      Width = 41
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        211.666666666666700000
        1270.000000000000000000
        144.638888888888900000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'endereco'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object cep: TQRLabel
      Left = 250
      Top = 375
      Width = 17
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        881.944444444444400000
        1322.916666666667000000
        59.972222222222220000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'cep'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object estado: TQRLabel
      Left = 380
      Top = 375
      Width = 30
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        1340.555555555556000000
        1322.916666666667000000
        105.833333333333300000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'estado'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object cidade: TQRLabel
      Left = 60
      Top = 375
      Width = 29
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        211.666666666666700000
        1322.916666666667000000
        102.305555555555600000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'cidade'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object cnpj: TQRLabel
      Left = 60
      Top = 405
      Width = 19
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        211.666666666666700000
        1428.750000000000000000
        67.027777777777780000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'cnpj'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object ie: TQRLabel
      Left = 280
      Top = 405
      Width = 8
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        987.777777777777800000
        1428.750000000000000000
        28.222222222222220000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'ie'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object valorextenso1: TQRLabel
      Left = 60
      Top = 435
      Width = 60
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        211.666666666666700000
        1534.583333333333000000
        211.666666666666700000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'valorextenso1'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object valorextenso2: TQRLabel
      Left = 60
      Top = 450
      Width = 60
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        211.666666666666700000
        1587.500000000000000000
        211.666666666666700000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'valorextenso2'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object dataaceite: TQRLabel
      Left = 600
      Top = 45
      Width = 38
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111100000
        2116.666666666670000000
        158.750000000000000000
        134.055555555556000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'emissao'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object pracapagamento: TQRLabel
      Left = 60
      Top = 390
      Width = 73
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        211.666666666666700000
        1375.833333333333000000
        257.527777777777800000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'pracapagamento'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object descontode: TQRLabel
      Left = 82
      Top = 106
      Width = 51
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        289.277777777777800000
        373.944444444444400000
        179.916666666666700000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'descontode'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object ate: TQRLabel
      Left = 250
      Top = 90
      Width = 14
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        881.944444444444400000
        317.500000000000000000
        49.388888888888890000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'ate'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object condicoesespeciais: TQRLabel
      Left = 42
      Top = 129
      Width = 86
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        148.166666666666700000
        455.083333333333300000
        303.388888888888900000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'condicoesespeciais'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
    object notafiscal: TQRLabel
      Left = 302
      Top = 116
      Width = 72
      Height = 13
      Frame.Color = clBlack
      Frame.DrawTop = False
      Frame.DrawBottom = False
      Frame.DrawLeft = False
      Frame.DrawRight = False
      Size.Values = (
        45.861111111111110000
        1065.388888888889000000
        409.222222222222200000
        254.000000000000000000)
      Alignment = taLeftJustify
      AlignToBand = False
      AutoSize = True
      AutoStretch = False
      Caption = 'numeroduplicata'
      Color = clWhite
      Transparent = False
      WordWrap = True
      FontSize = 10
    end
  end
end
