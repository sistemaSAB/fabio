unit UGerador;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjGerador;

type
  TFgerador = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    btsair: TBitBtn;
    BtCancelar: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    edttabela: TEdit;
    edtobjeto: TEdit;
    edtinstrucaosql: TEdit;
    EdtContagerencial: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure EdtContagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function atualizaQuantidade:string;


    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fgerador: TFgerador;
  ObjGerador:TObjGerador;

implementation

uses UessencialGlobal, Upesquisa, UContaGer, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFgerador.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjGerador do
    Begin
      Submit_CODIGO         ( edtCODIGO      .text);
      Submit_Nome           ( edtNome        .text);
      Submit_Tabela         ( edtTabela      .text);
      Submit_Objeto         ( edtObjeto      .text);
      Submit_InstrucaoSQL   ( edtInstrucaoSQL.text);
      Submit_ContaGerencial ( EdtContagerencial.text);

      result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFgerador.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjGerador do
     Begin
        edtCODIGO      .text    :=Get_CODIGO            ;
        edtNome        .text    :=Get_Nome              ;
        edtTabela      .text    :=Get_Tabela            ;
        edtObjeto      .text    :=Get_Objeto            ;
        edtInstrucaoSQL.text    :=Get_InstrucaoSQL      ;
        EdtContagerencial.text  :=Get_ContaGerencial    ;
        
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFgerador.TabelaParaControles: Boolean;
begin
     ObjGerador.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFgerador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjGerador=Nil)
     Then exit;

      If (ObjGerador.status<>dsinactive)
      Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
      End;

      ObjGerador.free;
end;

procedure TFgerador.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFgerador.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFgerador.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjGerador.Get_NovoCodigo;
     edtcodigo.enabled:=False;
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.Visible:=True;
     
     ObjGerador.status:=dsInsert;
     Edtnome.setfocus;
end;

procedure TFgerador.BtCancelarClick(Sender: TObject);
begin
     ObjGerador.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFgerador.BtgravarClick(Sender: TObject);
begin

     If ObjGerador.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjGerador.salvar=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFgerador.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     EdtCodigo.enabled:=False;
     ObjGerador.Status:=dsEdit;
     edtNome.setfocus;

end;

procedure TFgerador.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjGerador.Get_pesquisa,ObjGerador.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjGerador.status<>dsinactive
                                  then exit;

                                  If (ObjGerador.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados na tabela de Geradores!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFgerador.btalterarClick(Sender: TObject);
begin
    If (ObjGerador.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFgerador.btexcluirClick(Sender: TObject);
begin
     If (ObjGerador.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjGerador.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjGerador.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFgerador.EdtContagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fcontager:TFcontager;
begin

     If (key <>vk_f9)
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fcontager:=TFcontager.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjGerador.Get_PesquisaContaGerencial,ObjGerador.Get_TituloPesquisaContaGerencial,Fcontager)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then EdtContagerencial.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fcontager);
     End;
end;

procedure TFgerador.FormShow(Sender: TObject);
begin

limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjGerador:=TObjGerador.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
     Uessencialglobal.PegaCorForm(Self);
end;

function TFgerador.atualizaQuantidade: string;
begin
     Result:='Existem '+ContaRegistros('TABGERADOR','codigo')+' Geradores de T�tulo Cadastrados';
end;

end.
