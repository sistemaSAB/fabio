unit UACERTOPENDENCIAS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjACERTOPENDENCIAS,
  jpeg;

type
  TFACERTOPENDENCIAS = class(TForm)
    Guia: TTabbedNotebook;
    Image1: TImage;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    btcancelar: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbPendencia: TLabel;
    EdtPendencia: TEdit;
    LbData: TLabel;
    EdtData: TMaskEdit;
    LbTituloResultante: TLabel;
    EdtTituloResultante: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FACERTOPENDENCIAS: TFACERTOPENDENCIAS;
  ObjACERTOPENDENCIAS:TObjACERTOPENDENCIAS;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFACERTOPENDENCIAS.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjACERTOPENDENCIAS do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Pendencia.Submit_CODIGO(edtPendencia.text);
        Submit_Data(edtData.text);
        TituloResultante.Submit_CODIGO(edtTituloResultante.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFACERTOPENDENCIAS.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjACERTOPENDENCIAS do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtPendencia.text:=Pendencia.get_codigo;
        EdtData.text:=Get_Data;
        EdtTituloResultante.text:=TituloResultante.Get_CODIGO;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFACERTOPENDENCIAS.TabelaParaControles: Boolean;
begin
     If (ObjACERTOPENDENCIAS.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFACERTOPENDENCIAS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjACERTOPENDENCIAS=Nil)
     Then exit;

    If (ObjACERTOPENDENCIAS.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjACERTOPENDENCIAS.free;
end;

procedure TFACERTOPENDENCIAS.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFACERTOPENDENCIAS.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjACERTOPENDENCIAS.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjACERTOPENDENCIAS.status:=dsInsert;
     Guia.pageindex:=0;
     EdtPendencia.setfocus;

end;


procedure TFACERTOPENDENCIAS.btalterarClick(Sender: TObject);
begin
    If (ObjACERTOPENDENCIAS.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjACERTOPENDENCIAS.Status:=dsEdit;
                guia.pageindex:=0;
                EdtPendencia.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;


end;

procedure TFACERTOPENDENCIAS.btgravarClick(Sender: TObject);
begin

     If ObjACERTOPENDENCIAS.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjACERTOPENDENCIAS.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFACERTOPENDENCIAS.btexcluirClick(Sender: TObject);
begin
     If (ObjACERTOPENDENCIAS.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjACERTOPENDENCIAS.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjACERTOPENDENCIAS.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFACERTOPENDENCIAS.btcancelarClick(Sender: TObject);
begin
     ObjACERTOPENDENCIAS.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFACERTOPENDENCIAS.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFACERTOPENDENCIAS.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjACERTOPENDENCIAS.Get_pesquisa,ObjACERTOPENDENCIAS.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjACERTOPENDENCIAS.status<>dsinactive
                                  then exit;

                                  If (ObjACERTOPENDENCIAS.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjACERTOPENDENCIAS.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFACERTOPENDENCIAS.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFACERTOPENDENCIAS.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        ObjACERTOPENDENCIAS:=TObjACERTOPENDENCIAS.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);

end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(ObjACERTOPENDENCIAS.OBJETO.Get_Pesquisa,ObjACERTOPENDENCIAS.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(OBJACERTOPENDENCIAS.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If OBJACERTOPENDENCIAS.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(OBJACERTOPENDENCIAS.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
