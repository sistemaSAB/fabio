unit Urecibo;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UObjRecibo;

type
  TFrecibo = class(TForm)
    Shape1: TShape;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EDTVALOR: TEdit;
    edtrecebemos: TEdit;
    edtquantia1: TEdit;
    edtquantia2: TEdit;
    edtcorrespondente1: TEdit;
    edtcidade: TEdit;
    Image1: TImage;
    edtcorrespondente2: TEdit;
    edtdia: TEdit;
    labr8: TLabel;
    edtmes: TEdit;
    Label9: TLabel;
    edtano: TEdit;
    btrelatorios: TBitBtn;
    BtNovo: TBitBtn;
    edtcodigo: TEdit;
    procedure btrelatoriosClick(Sender: TObject);
    procedure EDTVALORExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtNovoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Frecibo: TFrecibo;
  ObjRecibo:TObjrecibo;

implementation

uses UimpReciboLancamento, UessencialGlobal, UobjConfRelatorio,
  UescolheImagemBotao;

{$R *.DFM}

procedure TFrecibo.btrelatoriosClick(Sender: TObject);
var
ObjConfRelatorio:TObjConfRelatorio;
FimpReciboLancamentoX:TFimpReciboLancamento;
begin
Try
FimpReciboLancamentoX:=TFimpReciboLancamento.create(nil);
     edtcodigo.text:=ObjRecibo.Get_NovoCodigo;
     Try
        ObjConfRelatorio:=TObjConfRelatorio.create;
     Except
           Messagedlg('Erro na cria��o do Objeto Configurador de Relat�rio!',mterror,[mbok],0);
           exit;
     End;


     With FimpReciboLancamentoX do
     Begin
          Try
             ObjConfRelatorio.RecuperaArquivoRelatorio(FimpReciboLancamentoX,'RECIBO');
          Finally
             ObjConfRelatorio.free;
          End;


          codigo.caption:=edtcodigo.text;
          valor.Caption:=formatfloat('0.00',strtofloat(edtvalor.text));
          recebi.Caption:=edtrecebemos.text;
          quantia1.Caption:=edtquantia1.text;
          quantia2.Caption:=edtquantia2.text;
          correspondente1.Caption:=edtcorrespondente1.text;
          correspondente2.Caption:=edtcorrespondente2.text;
          cidade.Caption:=edtcidade.text;
          dia.Caption:=edtdia.text;
          mes.Caption:=edtmes.text;
          ano.caption:=edtano.text;
          Qr.Preview;
     End;
Finally
       Freeandnil(FimpReciboLancamentoX);
End;

end;


procedure TFrecibo.EDTVALORExit(Sender: TObject);
var
valor:Currency;
apoio:string;
cont:integer;
valor1,valor2:string;
Begin
        apoio:='';
        valor:=0;
        Try
           valor:=strtofloat(edtvalor.text);
        Except
              edtquantia1.text:='';
              edtquantia2.text:='';
              exit;
        End;

        if (Valor>0)
        Then Begin
                apoio:=Uessencialglobal.valorextenso(valor);
                IF (apoio[1]=' ') and (apoio[2]='e')
                then delete(apoio,1,3);

                IF LENGTH(APOIO)>50
                Then Begin
                          valor1:='';
                          valor2:='';
                          DividirValor(apoio,50,valor1,valor1);
                          edtquantia1.text:=valor1;
                          edtquantia2.text:=valor2;
                          exit;
                     End;

                edtquantia1.text:=apoio;
                exit;
             End;
        edtquantia1.text:='';
        edtquantia2.text:='';
end;

procedure TFrecibo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     objrecibo.Free;
end;

procedure TFrecibo.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     EDTVALOR.setfocus;
end;

procedure TFrecibo.FormShow(Sender: TObject);
var
Valor:Currency;
begin
     Uessencialglobal.PegaCorForm(Self);

     Objrecibo:=TObjrecibo.create;
     edtvalor.setfocus;

     Try
        Valor:=StrToFloat(edtvalor.text);
        edtrecebemos.setfocus;
     Except
     End;
     FescolheImagemBotao.PegaFiguraBotoes(btnovo,nil,nil,nil,btrelatorios,nil,nil,nil);
     PegaFigura(Image1,'Logo200x52x24b.jpg');
end;

end.
