unit UObjValores;
Interface
Uses UobjCmc7,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjLancamento,UObjPortador,Uobjchequesportador,ibquery
;

Type
   TRegistroValores=Record//USado para preencher de outro form

               Historico        :String[60];
               Valor            :String[9];
               Lancamento       :String[9];
               LancamentoExterno:Boolean;

               End;

   TObjValores=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Portador         :TobjPortador;
                Lancamento       :TobjLancamento;
                Cheque           :TobjChequesPortador;
                ObjCMC7          :tobjCmc7;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean;ImprimeLAncto:Boolean;PhistoricoLancamento:String):Boolean;overload;
                Function    Salvar(ComCommit:Boolean;ImprimeLAncto:Boolean):Boolean;overload;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaLancamento(Parametro:string) :boolean;
                Function    LocalizaCheque(Parametro:string) :boolean;

                Function    exclui(Pcodigo:string;ComCommit:boolean):Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaLancamento          :string;
                Function    Get_TituloPesquisaLancamento    :string;
                Function    Get_PesquisaPortador            :string;
                Function    Get_TituloPesquisaPortador      :string;
                Function    Get_PesquisaCheque              :string;
                Function    Get_TituloPesquisaCheque        :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Historico        :string;
                Function Get_Tipo             :string;
                Function Get_Valor            :string;
                Function Get_Lancamento       :string;
                Function Get_Portador         :string;
                Function Get_Cheque           :string;



                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Historico        (parametro:string);
                Procedure Submit_Tipo             (parametro:string);
                Procedure Submit_Valor            (parametro:string);
                Procedure Submit_Lancamento       (parametro:string);
                Procedure Submit_Portador         (parametro:string);
                Procedure Submit_Cheque           (parametro:string);

                Function  Get_SomaValores(ParametroLancamento:string):Currency;
                Function  Get_Conta_Portadores_Diferentes(ParametroLancamento: string): Integer;
                Function  Get_NovoCodigo:string;
                function Get_Valores_Portadores_Diferentes(ParametroLancamento: string;Lista1, Lista2: TStringList): Boolean;
                Function ExcluiporLancamento(Plancamento:string;ComCommit:boolean):boolean;
                Procedure Pesquisa_Recebimentos_por_Lancamento(Plancamento:string;out PTotalDinheiro:string;out PtotalCheque:string);overload;
                Procedure Pesquisa_Recebimentos_por_Lancamento(Plancamento:string;out PTotalDinheiro:string;out PtotalCheque:string;out PtotalCredito:string);overload;

                // Feito por F�bio
                function ExcluiCredito(PValores: String): Boolean;
                function RetornaSaldoCliente(PCliente: String): Currency;
                function DiminuiCreditoComValores(PValores, PCliente, PValor,  PData: String): Boolean;

         Private
               ObjDataset:Tibdataset;
               ObjQlocal:Tibquery;

               CODIGO           :String[09];
               Historico        :String[60];
               Tipo             :String[1];//*D=Dinheiro,C=Cheque,N=Nenhum dos dois Valores*/
               Valor            :String[9];

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Function  AtualizaPortador(ImprimeLAncto:Boolean;PhistoricoEnviado:String):Boolean;overload;
                Function  AtualizaPortador(ImprimeLAncto:Boolean):Boolean;overload;


   End;
//************************************************
//Usado para lancamento externo
Procedure LimpaRegistroValores;
var
RegistroValores:TRegistroValores;
//************************************************

implementation
uses Uobjlanctoportador,SysUtils,Dialogs,UDatamodulo,Controls;


Procedure  TObjValores.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.Historico        :=FieldByName('Historico').asstring;
        Self.Tipo             :=FieldByName('Tipo').asstring;
        Self.Valor            :=FieldByName('Valor').asstring;

        If (Self.Lancamento.LocalizaCodigo(fieldbyname('Lancamento').asstring)=False)
        Then Begin
                  Messagedlg('Lan�amento n�o Localizado na Tabela de Lan�amentos Financeiros',mterror,[mbok],0);
                  Self.zerarTabela;
                  Exit;
             End
        Else Self.Lancamento.TabelaparaObjeto;

        If (Self.Portador.LocalizaCodigo(fieldbyname('Portador').asstring)=False)
        Then Begin
                  Messagedlg('Portador n�o Localizado na Tabela de Portadores',mterror,[mbok],0);
                  Self.zerarTabela;
                  Exit;
             End
        Else Self.Portador.TabelaparaObjeto;

        If (Self.Tipo='C')
        Then Begin
                If (Self.Cheque.LocalizaCodigo(fieldbyname('Cheque').asstring)=False)
                Then Begin
                          Messagedlg('Cheque n�o Localizado na Tabela de Cheques',mterror,[mbok],0);
                          Self.zerarTabela;
                          Exit;
                     End
                Else Self.Cheque.TabelaparaObjeto;
             End
        Else Self.Cheque.ZerarTabela;

     End;
end;


Procedure TObjValores.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
       FieldByName('CODIGO').asstring           :=Self.CODIGO           ;
       FieldByName('Historico').asstring        :=Self.Historico        ;
       FieldByName('Tipo').asstring             :=Self.Tipo             ;
       FieldByName('Valor').asstring            :=Self.Valor            ;
       FieldByName('Lancamento').asstring       :=Self.Lancamento.Get_Codigo;
       FieldByName('Portador').asstring         :=Self.Portador.Get_Codigo;
       FieldByName('cheque').asstring           :=Self.Cheque.Get_Codigo;
   End;
End;

//***********************************************************************
function TObjValores.Salvar(ComCommit:Boolean;ImprimeLAncto:Boolean): Boolean;//Ok
Begin
     Result:=Self.Salvar(ComCommit,ImprimeLAncto,'');
End;

function TObjValores.Salvar(ComCommit:Boolean;ImprimeLAncto:Boolean;PhistoricoLancamento:String): Boolean;//Ok
begin
     result:=false;

     if (Self.VerificaBrancos=True)
     then Exit;
     
     if (Self.VerificaNumericos=False)
     Then Exit;
     
     if (Self.VerificaData=False)
     Then Exit;
     
     if (Self.VerificaFaixa=False)
     Then Exit;
     
     if (Self.VerificaRelacionamentos=False)
     Then Exit;


     If Self.LocalizaCodigo(Self.CODIGO)=False
     Then Begin
               if(Self.Status=dsedit)
               Then Begin
                         Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                         exit;
                    End;
     End
     Else Begin
               if(Self.Status=dsinsert)
               Then Begin
                         Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                         exit;
                    End;
     End;
     
     if (Self.Status<>dsinsert) and (Self.Status<>dsedit)
     Then Begin
               Messagedlg('O Status n�o est� nem como edi��o nem como inser��o',mterror,[mbok],0);
               exit;
     end;

    if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

     
     Self.ObjetoParaTabela;
     Self.ObjDataset.Post;
     
     If (Self.status=dsinsert)
     Then Begin
             //Chamo a Atualiza��o do Portador
             If (Self.AtualizaPortador(ImprimeLancto,PhistoricoLancamento)=True)
             Then Begin
                      If (ComCommit=True)
                      Then FDataModulo.IBTransaction.CommitRetaining;
             End
             Else Begin
                       If (ComCommit=True)
                       Then FDataModulo.IBTransaction.RollbackRetaining;
                       result:=False;
                       exit;
             End;
     End;
     Self.status          :=dsInactive;
     result:=True;
     
end;

procedure TObjValores.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Historico        :='';
        Self.Tipo             :='';
        Self.Valor            :='';
        Self.Lancamento.zerarTabela;
        Self.Portador.zerarTabela;
        Self.Cheque.zerartabela;

     End;
end;

Function TObjValores.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Tipo='')
       Then Mensagem:=mensagem+'/Tipo'
       Else Begin
                 If (tipo='C')
                 Then Begin
                         If (Cheque.get_codigo='')
                         Then Mensagem:=mensagem+'/Cheque';
                      End;
            End;

       If (Valor='')
       Then Mensagem:=mensagem+'/Valor';

       If (Lancamento.get_codigo='')
       Then Mensagem:=mensagem+'/Lancamento';

       If (Portador.Get_Codigo='')
       Then Mensagem:=mensagem+'/Portador';


  End;
  If mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjValores.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.Lancamento.LocalizaCodigo(Self.Lancamento.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Lan�amento n�o Encontrado!';

     If (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';

     If (Tipo='C')
     Then Begin
               If (Self.Cheque.LocalizaCodigo(Self.Cheque.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Cheque n�o Encontrado!';
          End;

     
     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjValores.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Reais:=Strtofloat(Self.Valor);
        If (Reais<0)
        Then mensagem:=mensagem+'/ O Valor n�o pode ser negativo!';
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

end;

function TObjValores.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjValores.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';
     Self.tipo:=uppercase(Self.tipo);
     
     If ((Self.Tipo<>'D') and (Self.Tipo<>'N') and (Self.Tipo<>'C') and (Self.Tipo<>'R'))
     Then mensagem:=mensagem+'\Esp�cie desconhecido';
  
     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjValores.LocalizaCodigo(parametro: string): boolean;//ok
Begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Tipo,Valor,Lancamento,Portador,');
           SelectSql.add('cheque from TabValores where codigo='+Parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjValores.LocalizaLancamento(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Tipo,Valor,Lancamento,Portador,');
           SelectSql.add('cheque from TabValores where Lancamento='+Parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


procedure TObjValores.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjValores.Exclui(Pcodigo: string;ComCommit:boolean): Boolean;
var
codcheque:string;
TmpEncontrou:boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                  Self.TabelaparaObjeto;
                  codcheque:=Self.Cheque.get_codigo;

                  If (Self.Cheque.LocalizaCodigo(codcheque)=True)
                  Then Begin
                               //como vou excluir, preciso
                               //antes atualizar a tabvalores colocando
                               //null no codigo do cheque
                               Self.Status:=dsedit;
                               Self.Tipo:='D';//para poder gravar com cheque=NULL
                               Self.Cheque.Submit_CODIGO('');//para naum da erro de chave estrangeira;
                               If (Self.Salvar(ComCommit,False)=False)
                               Then Begin
                                         Messagedlg('Erro na atualizacao do campo cheque na TabValores para poder excluir o cheque!',mterror,[mbok],0);
                                         result:=False;
                                         exit;
                               End;
                               
                               Self.Cheque.LocalizaCodigo(codcheque);
                               Self.Cheque.TabelaparaObjeto;
                               {ShowMessage(Self.Cheque.get_codigo);}
                               If (Self.Cheque.Exclui(Self.Cheque.get_codigo,ComCommit)=False)
                               Then Begin
                                         Messagedlg('N�o foi poss�vel excluir o Cheque da Tabela de Cheques de Portador',mterror,[mbok],0);
                                         result:=False;
                                         exit;
                               End;
                  End;

                  ObjlanctoportadorGlobal.ZerarTabela;
                  if (ObjlanctoportadorGlobal.LocalizaValores(Self.CODIGO)=True)
                  Then Begin
                            //no caso do credito eu terei dois lancamentos
                            //na lancto portador um de debito e outro de credito
                            //para o mesmo codigo na tabvalores
                            //sendo assim o while me garantira a exclusao dos 2 registros 
                            Tmpencontrou:=ObjlanctoportadorGlobal.LocalizaValores(Self.CODIGO);
                            While (Tmpencontrou=True) do
                            Begin
                                //se eu encontrar o lancto eu posso exclui-lo senaum lanco um contra partida
                                ObjlanctoportadorGlobal.TabelaparaObjeto;
                                result:=ObjlanctoportadorGlobal.exclui(ObjlanctoportadorGlobal.get_codigo,false);
                                if (result=false)
                                then exit;
                                
                                Tmpencontrou:=ObjlanctoportadorGlobal.LocalizaValores(Self.CODIGO);
                            End;
                  End
                  Else Begin
                            //este procedimento s� tem sentido porque existem lancamentos antigos que naum obedeciam
                            //aos apdroes atuais como por exemplo o Numero do Lancamento da TabLanctoPortador
                            ObjlanctoportadorGlobal.Submit_Historico('EXCLUS�O.LANCTO.DINH.'+Self.Lancamento.Get_Historico);
                            ObjlanctoportadorGlobal.Submit_Data(Datetostr(date));
                            ObjlanctoportadorGlobal.Submit_TabelaGeradora('');
                            ObjlanctoportadorGlobal.Submit_ObjetoGerador('');
                            ObjlanctoportadorGlobal.Submit_CampoPrimario('');
                            ObjlanctoportadorGlobal.Submit_ValordoCampo('');
                            ObjlanctoportadorGlobal.Submit_ImprimeRel(True);
                            If (ObjlanctoportadorGlobal.DiminuiSaldo(Self.Portador.Get_CODIGO,Self.Valor,ComCommit)=False)
                            Then Begin
                                        Messagedlg('N�o foi poss�vel lan�ar o lancto de exclus�o devido a problemas no acerto de saldo do portador !',mterror,[mbok],0);
                                        Result:=False;
                                        exit;
                            End;
                  End;

                  if (ObjCreditoValoresGlobal<>nil) //existem sistemas que nao usam este oobjeto
                  Then Begin
                            if (ObjCreditoValoresGlobal.LocalizaValores(pcodigo)=True)
                            then Begin
                                    ObjCreditoValoresGlobal.TabelaparaObjeto;
                                    if (ObjCreditoValoresGlobal.Exclui(ObjCreditoValoresGlobal.get_codigo,ComCommit)=False)
                                    Then Begin
                                              Mensagemerro('Erro na tentativa de Excluir o Cr�dito ligado a este valor');
                                              result:=False;
                                              exit;
                                    End;
                            End;
                  End;


                 //Excluo o Valor Lan�ado
                 //idenpendente se era dinheiro ou cheque
                 //pois os dois possuem um registro na TabValores
                 Self.ObjDataset.delete;

                 //Se n�o for um lancamento externo dou commit
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;

                 Result:=True;
             End//Localiza Objeto
         Else result:=false;
     Except
           on e:exception do
           Begin
                mensagemerro(e.message);
                result:=false;
           End;
     End;
end;


constructor TObjValores.create;
begin
        Self.ObjCMC7:=tobjCmc7.Create;

        Self.ObjDataset         :=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.Lancamento         :=TObjLancamento.Create;
        Self.Portador           :=TObjPortador.create;
        Self.cheque             :=Tobjchequesportador.create;
        Self.ObjQlocal          :=tibquery.create(nil);
        Self.ObjQlocal.Database :=FDataModulo.IbDatabase;
        Self.ObjDatasource      :=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQlocal;
        Self.ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Historico,Tipo,Valor,Lancamento,Portador,');
                SelectSql.add('cheque from TabValores where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert into TabValores(CODIGO,Historico,Tipo,Valor,Lancamento,Portador,cheque) values');
                InsertSQL.add('(:CODIGO,:Historico,:Tipo,:Valor,:Lancamento,:Portador,:cheque)');

                ModifySQL.clear;
                ModifySQL.Add('Update TabValores set  CODIGO=:CODIGO,Historico=:Historico,Tipo=:Tipo,Valor=:Valor,Lancamento=:Lancamento,Portador=:Portador,Cheque=:cheque');
                ModifySQL.Add('where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add(' Delete from TabValores where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Historico,Tipo,Valor,Lancamento,Portador,cheque');
                RefreshSQL.add('from TabValores where codigo=0');

                Open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjValores.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjValores.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjValores.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjValores.Get_Pesquisa: string;
begin
     If (RegistroValores.LancamentoExterno=True)
     Then Result:=' Select * from TabValores where Lancamento='+RegistroValores.Lancamento
     Else Result:=' Select * from TabValores ';
end;

function TObjValores.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Valores ';
end;

function TObjValores.Get_Historico: string;
begin
     Result:=Self.historico;
end;

function TObjValores.Get_Lancamento: string;
begin
     Result:=Self.Lancamento.Get_Codigo;
end;


function TObjValores.Get_Portador: string;
begin
     Result:=Self.Portador.Get_Codigo;
end;


function TObjValores.Get_Tipo: string;
begin
     Result:=Self.tipo;
end;

function TObjValores.Get_Valor: string;
begin
     Result:=Self.valor;
end;


procedure TObjValores.Submit_Historico(parametro: string);
begin
     Self.historico:=Parametro;
end;

procedure TObjValores.Submit_Lancamento(parametro: string);
begin
     Self.lancamento.Submit_CODIGO(Parametro);
end;

procedure TObjValores.Submit_Portador(parametro: string);
begin
     Self.portador.Submit_CODIGO(Parametro);
end;


procedure TObjValores.Submit_Tipo(parametro: string);
begin
     Self.tipo:=Parametro;
end;

procedure TObjValores.Submit_Valor(parametro: string);
begin
     Self.valor:=Parametro;
end;


Function TObjValores.Get_SomaValores(ParametroLancamento: string): Currency;
begin

     With Self.ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSQL.add('Select sum(valor) as SOMA from Tabvalores where lancamento='+ParametroLancamento);
          open;

          Result:=Fieldbyname('SOMA').asfloat;
          close;
     End;

End;

Function TObjValores.Get_Conta_Portadores_Diferentes(ParametroLancamento: string): Integer;
begin

     With Self.ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSQL.add('Select distinct(portador) from Tabvalores where lancamento='+ParametroLancamento);
          open;
          last;
          Result:=recordcount;
          close;
     End;

End;

Function TObjValores.Get_Valores_Portadores_Diferentes(ParametroLancamento: string;Lista1,Lista2:TStringList): Boolean;
begin


   //Este procedimento retorna o valor recebido e cada portador referente
   //ao lancamento em parametro
   //utilizado para poder gerar os registros de exportacao
   //para a contabilidade, assim eu gero um registro
   //para cada portador diferente em que recebeu o valor


     Result:=False;
     lista1.Clear;
     Lista2.Clear;

     With Self.ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSQL.add('Select portador,sum(valor) as VALOR from Tabvalores where lancamento='+ParametroLancamento);
          SelectSQL.add('group by portador');
          Try
             open;
             first;
             While not(eof) do
             Begin
                  Lista1.ADD(fieldbyname('portador').asstring);
                  lista2.add(fieldbyname('valor').asstring);
                  next;
             End;
             close;
             result:=True;
          Except
                exit;
          End;

     End;

End;

Procedure LimpaRegistroValores;
BEgin
          RegistroValores.Historico:='';
          RegistroValores.Valor:='';
          RegistroValores.Lancamento:='';
          RegistroValores.LancamentoExterno:=False;
End;

function TObjValores.Get_PesquisaLancamento: string;
begin
     Result:=Self.Lancamento.Get_Pesquisa;
end;

function TObjValores.Get_TituloPesquisaLancamento: string;
begin
     Result:=Self.Lancamento.Get_TituloPesquisa;
end;

function TObjValores.Get_PesquisaPortador: string;
begin
     Result:=Self.Portador.Get_Pesquisa;
end;

function TObjValores.Get_TituloPesquisaPortador: string;
begin
     Result:=Self.Portador.Get_TituloPesquisa;
end;

function TObjValores.get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_CHAVEVALORES';
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Valor a ser Lan�ado',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


function TObjValores.Get_PesquisaCheque: string;
begin
     Result:=Self.Cheque.get_pesquisa;
end;

function TObjValores.Get_TituloPesquisaCheque: string;
begin
     Result:=Self.cheque.Get_TituloPesquisa;
end;

function TObjValores.Get_Cheque: string;
begin
     Result:=Self.Cheque.Get_CODIGO;
end;

procedure TObjValores.Submit_Cheque(parametro: string);
begin
     Self.Cheque.Submit_CODIGO(parametro);
end;

Function TObjValores.AtualizaPortador(ImprimeLAncto:Boolean): Boolean;
Begin
     Result:=Self.AtualizaPortador(ImprimeLAncto,'');
end;

//Atualiza o Saldo do Portador Escolhido para lan�ar o valor
Function TObjValores.AtualizaPortador(ImprimeLAncto:Boolean;PhistoricoEnviado:String): Boolean;
var
SaldoInicial:string;
SaldoInicialF:Currency;
Resultado:boolean;
Phistorico:String;
begin
     If (Self.Portador.LocalizaCodigo(self.portador.get_codigo)=False)
     Then Begin
             result:=False;
             Messagedlg('Portador N�o encontrado para atualizar o Saldo',mterror,[mbok],0);
             exit;
     End;

     Self.Portador.TabelaparaObjeto;

     If Self.LANCAMENTO.LocalizaCodigo(Self.Lancamento.get_codigo)=false
     Then Begin
               Messagedlg('Erro na Localiza��o dos dados do Lan�amento na Atualiza��o dos Valores no Portador!',mterror,[mbok],0);
               result:=False;
               exit;
     End;
     Self.Lancamento.TabelaparaObjeto;

     ObjlanctoportadorGlobal.ZerarTabela;


     if (PhistoricoEnviado<>'')
     Then Phistorico:=phistoricoenviado
     Else PHistorico:=Self.Lancamento.GET_HISTORICO;

     If (Self.tipo='C')//Cheque
     Then Begin
              //ObjlanctoportadorGlobal.Submit_Historico('CH.REC.'+Phistorico);
                //*******CELIO*************************************************
                If(copy(Phistorico,1,5)='VENDA')
                Then Phistorico := 'REC. CHQ '+Phistorico;
                //**************************************************************

              ObjlanctoportadorGlobal.Submit_Historico(Phistorico);
              {if (Length(Phistorico)>=4)
              then Begin
                        if (copy(Phistorico,1,4)='REC.')
                        Then ObjlanctoportadorGlobal.Submit_Historico('CH.'+Phistorico);
              End;}
     End
     Else Begin
                //ObjlanctoportadorGlobal.Submit_Historico('REC.'+Phistorico);

                //*******CELIO*************************************************
                If(copy(Phistorico,1,5)='VENDA')
                Then Phistorico := 'REC. '+Phistorico;
                //**************************************************************

                ObjlanctoportadorGlobal.Submit_Historico(Phistorico);
                {if (length(Phistorico)>=4)
                then Begin
                          if (copy(phistorico,1,4)='REC.')
                          Then ObjlanctoportadorGlobal.Submit_Historico(Phistorico);
                End;}
     End;
     
     ObjlanctoportadorGlobal.Submit_Data(Self.LANCAMENTO.Get_Data);
     ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABVALORES');
     ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJVALORES');
     ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
     ObjlanctoportadorGlobal.Submit_ValordoCampo(Self.Codigo);
     ObjlanctoportadorGlobal.Submit_ImprimeRel(ImprimeLAncto);
     ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(Self.Lancamento.Get_CODIGO);
     ObjlanctoportadorGlobal.Valores.Submit_CODIGO(Self.Codigo);

     resultado:=ObjlanctoportadorGlobal.AumentaSaldo(Self.Portador.get_codigo,Self.valor,False);

     If (Resultado=False)
     Then Begin
             Messagedlg('Erro na tentativa de Atualizar o Saldo do Portador!',mterror,[mbok],0);
             result:=False;
             exit;
     End;

     if (Self.Tipo='R')//cr�dito
     then Begin
               ObjlanctoportadorGlobal.Submit_historico('CR�DITO USADO - '+ObjlanctoportadorGlobal.get_historico);
               resultado:=ObjlanctoportadorGlobal.DiminuiSaldo(Self.Portador.get_codigo,Self.valor,False);
     End;

     Result:=True;
     //
End;

destructor TObjValores.Free;
begin
    Self.Lancamento.free;
    Self.Portador.free;
    Self.cheque.free;
    Freeandnil(Self.ObjDataset);
    freeandnil(Self.objqlocal);
    freeandnil(Self.objdatasource);
    ObjCMC7.free;
end;



function TObjValores.LocalizaCheque(Parametro: string): boolean;
begin

     Result:=False;

     With Self.ObjDataset do
     Begin
          SelectSQL.clear;
          SelectSQL.add('Select * from tabvalores where cheque='+parametro);
          open;

          If Recordcount=0
          Then exit;

          Self.TabelaparaObjeto;
          result:=True;
     End;
end;

function TObjValores.ExcluiporLancamento(Plancamento: string;ComCommit:boolean): boolean;
begin
     With Self.objqlocal do
     Begin
          Close;
          SQL.clear;
          sql.add('Select * from TabValores where lancamento='+plancamento);
          open;
          if (recordcount=0)
          Then Begin
                    result:=true;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
               //aqui tenho q excluir  o credito caso existia. Essa fun��o exclui o lancamento de
               // de credito que � negativo por isso que Aumenta o credito
               if (Self.ExcluiCredito(fieldbyname('codigo').asstring)=false)
               then Begin
                       MensagemErro('Erro ao tentar excluir o credito');
                       exit;
               end;

               //tem que fazer o caminho inverso
                //primeiro retornar o saldo do portador
                //apagar os lancamentos na tabvalores
                //se tiver cheque na tabchequeportador tmbm
                //apagar o lancamento
               If (Self.Exclui(fieldbyname('codigo').asstring,ComCommit)=False)
               Then begin
                         result:=False;
                         exit;
               end;
               next;
          End;

          result:=True;
          
     End;


end;

{4Os passos ja foram feitos
quando lanco um chequre na tabvalores agora na TABLANCTOPORTAdor
eh lancado um registro ligado a tabvalores
assim eu coinsigo retornar um lancamento
quando tento excluir um lancamento em cheque
ele verifica se esses cheques sofreram transferencia
caso sim, ele vai tentar retornar o processo
transferencisa a transferencia
por isso preciso codificar o excluir da transferencia
para retornar os  cheque para os portadores originais
e apagar os lancamentos na tablanctoportador
se naum conseguiur lancar uma contrapartida
acertar depois o pagamento que envolve transferencia e cheque do poirtador
assim como baixa de cheque}

Procedure TobjValores.Pesquisa_Recebimentos_por_Lancamento(Plancamento:string;out PTotalDinheiro:string;out PtotalCheque:string);
var
temp:string;
Begin
     Self.Pesquisa_Recebimentos_por_Lancamento(Plancamento,PTotalDinheiro,PtotalCheque,temp);
End;

Procedure TobjValores.Pesquisa_Recebimentos_por_Lancamento(Plancamento:string;out PTotalDinheiro:string;out PtotalCheque:string;out PtotalCredito:string);
begin
     With Self.ObjQlocal do
     Begin

          PTotalDinheiro:='0,00';
          PtotalCheque:='0,00';
          //somando o total em dinheiro e cheque
          close;
          SQL.clear;
          sql.add('Select tabvalores.tipo,sum(TabVAlores.valor) as SOMA');
          sql.add('from TabVAlores');
          sql.add('where lancamento='+plancamento);
          sql.add('group by tabvalores.tipo');
          open;
          first;
          While not(eof) do
          Begin
              if (fieldbyname('tipo').asstring='D')
              Then PTotalDinheiro:=floattostr(fieldbyname('soma').asfloat)
              else
                 if (fieldbyname('tipo').asstring='C')
                 Then PtotalCheque:=floattostr(fieldbyname('soma').asfloat)
                 Else PtotalCredito:=floattostr(fieldbyname('soma').asfloat);
              next;
          End;


          //selecionando os lancamentos na tabvalores
          close;
          SQL.clear;
          sql.add('Select TabVAlores.tipo as Especie,TabVAlores.valor,TabVAlores.portador,');
          sql.add('tabportador.nome as Nomeportador,TabVAlores.codigo,');
          sql.add('tabchequesportador.vencimento,tabchequesportador.numcheque');
          sql.add('from TabVAlores');
          sql.add('join tabportador on TabVAlores.portador=tabportador.codigo');
          sql.add('left join tabchequesportador on Tabvalores.cheque=tabchequesportador.codigo');
          sql.add('where lancamento='+plancamento);
          open;
     End;
end;

Function TobjValores.ExcluiCredito(PValores:String):Boolean;
Var  QueryLocal :TIBQuery;
Begin

try

       // N�o posso criar o Objeto LancamentoCredito aki em Valores
       // por isso fa�o as Inser�oes e Remo��es direto no SQL

       Result:=false;
       try
           QueryLocal := nil;
           QueryLocal :=TIBQuery.Create(nil);
           QueryLocal.Database:=FDataModulo.IBDatabase;
       except
           MensagemErro('Erro ao tentar criar a QueryLocal');
           exit;
       end;

       // Se Excluiu os Valores � s� excluir o registro
       // que o Credito ser� Aumentado, porque todos lancamento de credito
       // vindo da tabela de Valores � NEGATIVO


       //*****OBSERVACAO RONNEI 25/01/2010********************

       (*Essa tabela pode n�o existir em outros sistemas
       como Bia, Safira, Aline entre outros, portanto precisa
       tomar cuidado quando for trabalhar com qq coisa que envolva
       o financeiro, no sql abaixo tive erro em Clientes
       do Safira porque a tabela n�o existe
       pensa numa solu��o
       *)



       QueryLocal.Close;
       QueryLocal.SQL.Clear;
       QueryLocal.SQL.Add('Delete from TabLancamentoCredito Where Valores ='+PValores);
       try

           QueryLocal.ExecSQL;
       except
           on e:exception do
           Begin
                MensagemErro('Erro ao tentar Excluir o Cr�dito'+#13+e.message);
                exit;
           End;
       end;

       Result:=true;

finally
     if (QueryLocal <> nil)
     then FreeAndNil(QueryLocal);
end;

end;

Function TobjValores.RetornaSaldoCliente(PCliente:String):Currency;
Var  QueryLocal :TIBQuery;
Begin
try
       // N�o posso criar o Objeto LancamentoCredito aki em Valores
       // por isso fa�o as Inser�oes e Remo��es direto no SQL

       Result:=0;
       try
           QueryLocal := nil;
           QueryLocal :=TIBQuery.Create(nil);
           QueryLocal.Database:=FDataModulo.IBDatabase;
       except
           MensagemErro('Erro ao tentar criar a QueryLocal');
           exit;
       end;

       QueryLocal.Close;
       QueryLocal.SQL.Clear;
       QueryLocal.SQL.Add('Select SUM(Valor) as Valor from TabLancamentoCredito Where Cliente = '+PCliente);
       QueryLocal.Open;

       Result:=QueryLocal.fieldbyname('Valor').AsCurrency;

finally
     if (QueryLocal <> nil)
     then FreeAndNil(QueryLocal);
end;

end;


Function TobjValores.DiminuiCreditoComValores(PValores, PCliente,PValor, PData: String): Boolean;
Var  QueryLocal :TIBQuery;
     PValorNegativo:Currency;
Begin
try
       // N�o posso criar o Objeto LancamentoCredito aki em Valores
       // por isso fa�o as Inser�oes e Remo��es direto no SQL


       Result:=false;
       try
           QueryLocal := nil;
           QueryLocal :=TIBQuery.Create(nil);
           QueryLocal.Database:=FDataModulo.IBDatabase;
       except
           MensagemErro('Erro ao tentar criar a QueryLocal');
           exit;
       end;

       PValorNegativo:= StrToCurr(tira_ponto(PValor))*-1;

       // Inserir o Valor Negativo
       QueryLocal.Close;
       QueryLocal.SQL.Clear;
       QueryLocal.SQL.Add('Insert Into TabLancamentoCredito(Codigo, Data, Valor, Valores, Cliente)');
       QueryLocal.SQL.Add('values (0,'+#39+FormatDateTime('mm/dd/yyyy',StrToDate(PData))+#39+','+virgulaparaponto(CurrToStr(PValorNegativo))+','+PValores+','+PCliente+')');

       try
           QueryLocal.ExecSQL;
       except
           MensagemErro('Erro ao tentar Inserir o Lancamento de Credito');
           exit;
       end;

       Result:=true;

finally
     if (QueryLocal <> nil)
     then FreeAndNil(QueryLocal);
end;

end;

end.

