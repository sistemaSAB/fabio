object FconfrontoContabilidade: TFconfrontoContabilidade
  Left = 194
  Top = 154
  Width = 870
  Height = 500
  Caption = 'Confronto de Dados da Contabilidade com Backup'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DbGridAtual: TDBGrid
    Left = 0
    Top = 41
    Width = 862
    Height = 187
    Align = alTop
    DataSource = DSAtual
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object DbGridbackup: TDBGrid
    Left = 0
    Top = 269
    Width = 862
    Height = 204
    Align = alClient
    DataSource = DSBackup
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object FconfrontoContabilidade: TPanel
    Left = 0
    Top = 228
    Width = 862
    Height = 41
    Align = alTop
    Caption = 'Dados do backup'
    TabOrder = 2
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 41
    Align = alTop
    Caption = 'Dados Atuais'
    TabOrder = 3
  end
  object DSAtual: TDataSource
    Left = 440
    Top = 112
  end
  object DSBackup: TDataSource
    Left = 568
    Top = 440
  end
end
