unit UobjPlanodeContas;
Interface
Uses Classes,Db,UessencialGlobal,Ibcustomdataset;

Type
   TObjPlanodeContas=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaCodigo2(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;
                Procedure   Rollback;
                Function    Get_NovoCodigo(var Pcodigo:string):boolean;

                Function Get_CODIGO           :string;
                Function Get_CODIGO2           :string;
                Function Get_Tipo             :string;
                Function Get_Classificacao    :string;
                Function Get_Nome             :string;
                function Get_ImprimeBalancoSS:string;

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_CODIGO2           (parametro:string);
                Procedure Submit_Tipo             (parametro:string);
                Procedure Submit_Classificacao    (parametro:string);
                Procedure Submit_Nome             (parametro:string);
                Procedure Submit_ImprimeBalancoSS(Parametro:string);
                //Function  Get_NovoCodigo:string;
                Function  ImportaDados(PArq:String):Boolean;
                Function  ExportaDados(PArq:String):Boolean;
                Function  ExcluiTodos:Boolean;
                Function RetornaCampoNome:string;
                Function RetornaCampoCodigo:string;
                Procedure Imprimir;

                Procedure fieldbyname(PCampo,PValor:String);overload;
                Function fieldbyname(PCampo:String):String;overload;


         Private
               ObjDataset:Tibdataset;

               CODIGO           :string;
               CODIGO2           :string;
               Tipo             :string;//varchar(1) check (Tipo='T' or Tipo='A'),
               Classificacao    :string;//Varchar(30),
               Nome             :string;//Varchar(100),
               ImprimeBalancoSS:string;
               
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Function  GravaLinha(linha:String):Boolean;
                Procedure ImprimePlanodeContas;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Ibquery,Controls, 
  UReltxtRDPRINT, UMenuRelatorios,rdprint;


{ TTabTitulo }


Function  TObjPlanodeContas.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO             :=FieldByName('CODIGO').asstring;
        Self.CODIGO2             :=FieldByName('CODIGO2').asstring;
        Self.Tipo               :=FieldByName('Tipo').asstring;
        Self.Classificacao      :=FieldByName('Classificacao').asstring;
        Self.Nome               :=FieldByName('Nome').asstring;
        Self.ImprimeBalancoSS:=fieldbyname('ImprimeBalancoSS').asstring;
        result:=True;
     End;
end;


Procedure TObjPlanodeContas.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring            :=Self.CODIGO           ;
      FieldByName('CODIGO2').asstring            :=Self.CODIGO2;
      FieldByName('Tipo').asstring              :=Self.Tipo             ;
      FieldByName('Classificacao').asstring     :=Self.Classificacao    ;
      FieldByName('ImprimeBalancoSS').asstring:=Self.ImprimeBalancoSS;
      FieldByName('Nome').asstring              :=Self.Nome             ;
  End;
End;

//***********************************************************************

function TObjPlanodeContas.Salvar(ComCommit:Boolean): Boolean;//Ok
begin
  Result:=false;
     
  if (Self.VerificaBrancos=True)
  Then Exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
            if(Self.Status=dsedit)
            Then Begin
                      Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                      exit;
            End;
  End
  Else Begin
            if(Self.Status=dsinsert)
            Then Begin
                      Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                      exit;
            End;
  End;

  if Self.status=dsinsert
  then Begin
             if (Self.codigo='') or (self.codigo='0')
             Then Begin
                        if (Self.get_novocodigo(Self.codigo)=false)
                        Then Begin
                                  Messagedlg('Erro na tentativa de Gerar um Novo C�digo',mterror,[mbok],0);
                                  exit; 
                        end;
             end;

             Self.ObjDataset.Insert;//libera para insercao
  End
  Else if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;


  Self.ObjetoParaTabela;
  Self.ObjDataset.Post;

  If ComCommit=True
  Then FDataModulo.IBTransaction.CommitRetaining;

  Self.status          :=dsInactive;
  result:=True;

end;

procedure TObjPlanodeContas.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        CODIGO           :='';
        CODIGO2           :='';
        Tipo             :='';
        Classificacao    :='';
        Nome             :='';
        ImprimeBalancoSS:='';
     End;
end;

Function TObjPlanodeContas.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       and (Self.Status=dsedit)
       Then Mensagem:=mensagem+'/C�digo';


       If (Tipo='')
       Then Mensagem:=mensagem+'/Tipo';
       If (Classificacao='')
       Then Mensagem:=mensagem+'/Classifica��o';
       If (Nome='')
       Then Mensagem:=mensagem+'/Nome';
       if (ImprimeBalancoSS='')
       Then Mensagem:=Mensagem+'/Imprime no Balanco quando sem saldo?';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
  End;
  result:=false;
end;


function TObjPlanodeContas.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Curso n�o Encontrado!';}

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjPlanodeContas.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        if (Self.Codigo<>'')
        Then Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjPlanodeContas.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjPlanodeContas.VerificaFaixa: boolean;
var
   Mensagem,Tmascara2:String;
   Cont:Integer;
begin

Try

     Mensagem:='';
     If (Tipo<>'T') and (Tipo<>'A')
     Then MEnsagem:=Mensagem+'O Tipo de Conta deve ser "Titulo" ou "Analitica"'
     Else Begin
               //Vou apagar os espa�os em branco da String da MAscara
                //e verificar aonde termina os numeros para cortar os pontos
                Tmascara2:='';
                for cont:=1 to length(self.Classificacao) do
                Begin
                        If (Self.Classificacao[cont]<>' ')
                        Then TMascara2:=Tmascara2+Self.Classificacao[cont];
                End;
                Self.Classificacao:='';
                for cont:=1 to length(Tmascara2) do
                Begin
                        If (Tmascara2[cont]='.')
                        Then Begin
                                If  (cont<>length(Tmascara2))//n�o � a ultima
                                Then Begin
                                        If (not (TMascara2[cont+1] in ['0'..'9']))
                                        Then break
                                        Else Self.Classificacao:=Self.Classificacao+Tmascara2[cont];
                                     End;
                              End
                        Else Self.Classificacao:=Self.Classificacao+Tmascara2[cont];
                End;
     End;

     if (Self.ImprimeBalancoSS<>'S') and (Self.ImprimeBalancoSS<>'N')
     Then Mensagem:=Mensagem+'O campo "Imprime Balan�o quando Sem Saldo" cont�m um valor inv�lido';
     
     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjPlanodeContas.LocalizaCodigo(parametro: string): boolean;//ok
begin
       result:=False;
       If (Parametro='')
       Then Exit;
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,codigo2,tipo,classificacao,nome,ImprimeBalancoSS from tabplanodecontas where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjPlanodeContas.LocalizaCodigo2(parametro: string): boolean;//ok
begin
       result:=False;
       If (Parametro='')
       Then Exit;
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,codigo2,tipo,classificacao,nome,ImprimeBalancoSS from tabplanodecontas where codigo2='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjPlanodeContas.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPlanodeContas.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPlanodeContas.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IBDatabase;
        //Self.ObjDatasource:=TDataSource.Create(nil);
        //Self.CodCurso:=TObjCursos.create;
        Self.ParametroPesquisa:=TStringList.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,codigo2,tipo,classificacao,nome,ImprimeBalancoSS from tabplanodecontas where codigo =-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert into TabPLanodeContas (codigo,codigo2,tipo,classificacao,nome,ImprimeBalancoSS) values (:codigo,:codigo2,:tipo,:classificacao,:nome,:ImprimeBalancoSS) ');

                ModifySQL.clear;
                ModifySQL.add(' Update tabplanodecontas set codigo=:codigo,codigo2=:codigo2,tipo=:tipo,classificacao=:classificacao,nome=:nome,ImprimeBalancoSS=:ImprimeBalancoSS where codigo=:codigo ');


                DeleteSQL.clear;
                DeleteSQL.add('Delete from TabPlanodeContas where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,codigo2,tipo,classificacao,nome,ImprimeBalancoSS from tabplanodecontas where codigo =-1');

                open;

                Self.ObjDataset.First ;

                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjPlanodeContas.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjPlanodeContas.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


function TObjPlanodeContas.Get_CODIGO2: string;
begin
        Result:=Self.Codigo2;
end;

procedure TObjPlanodeContas.Submit_CODIGO2(parametro: string);
begin
        Self.Codigo2:=parametro;
end;

procedure TObjPlanodeContas.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

procedure TObjPlanodeContas.Rollback;
begin
     Fdatamodulo.IBTransaction.RollbackRetaining;
end;

function TObjPlanodeContas.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPlanodeContas');
     Result:=Self.ParametroPesquisa;
end;

function TObjPlanodeContas.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Plano de Contas ';
end;


destructor TObjPlanodeContas.Free;
begin
   //Freeandnil(Self.ObjDataset);
   //Freeandnil(Self.ParametroPesquisa);     
   freeandnil(ObjDataset);
   freeandnil(self.ParametroPesquisa);
end;

function TObjPlanodeContas.Get_Classificacao: string;
begin
        Result:=Self.Classificacao;
end;

function TObjPlanodeContas.Get_Nome: string;
begin
     Result:=Self.Nome;
end;

function TObjPlanodeContas.Get_Tipo: string;
begin
     Result:=Self.Tipo;
end;

procedure TObjPlanodeContas.Submit_Classificacao(parametro: string);
begin
     Self.Classificacao:=Parametro;
end;

procedure TObjPlanodeContas.Submit_Nome(parametro: string);
begin
     Self.Nome:=parametro;
end;

procedure TObjPlanodeContas.Submit_Tipo(parametro: string);
begin
     Self.Tipo:=Parametro;
end;

function TObjPlanodeContas.ImportaDados(PArq: String): Boolean;
var
Arq:TextFile;
LInha:String;
begin
     Result:=False;
     //Tenho o nome do Arquivo Texto como Parametro
     //Tento Abrir o Arquivo
     Try
        AssignFile(Arq,Parq);
        Reset(Arq);
     Except
           Messagedlg('Erro na Tentativa de Abertura do Arquivo:'+PArq,mterror,[mbok],0);
           exit;
     End;
Try
     While Not (eof(arq)) do
     Begin
          readln(arq,linha);
          If (Self.GravaLinha(linha)=False)
          Then Begin
                    FdataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Erro na Grava��o da Linha!',mterror,[mbok],0);
                    exit;
               End;
     End;
     Result:=True;
     FDataModulo.IBTransaction.CommitRetaining;
Finally
       Closefile(arq);
End;


end;

function TObjPlanodeContas.GravaLinha(linha: String): Boolean;
var
Primeiro,Fim:Integer;
SubString:String;
begin
     result:=False;
     //Recebe a Linha de Parametro separa o necessario
     //e tenta gravar no banco de dados
     //Modelo de linha:

     //"17","T","1","A T I V O"
     //"Codigoreduzido","Titulo ou Analitica","Classificacao","Nome"

     Self.ZerarTabela;
     Primeiro:=Pos('"',linha);
     If (primeiro=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     SubString:=copy(linha,primeiro+1,length(linha));
     Fim:=Pos('"',substring);
     If (Fim=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     //Copiando o Conteudo
     Self.Codigo:=copy(substring,1,fim-1);

     SubString:=copy(substring,fim+1,length(substring));

     If (substring='')
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;

     Primeiro:=Pos('"',substring);
     If (primeiro=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     SubString:=copy(substring,primeiro+1,length(substring));
     Fim:=Pos('"',substring);
     If (Fim=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     //Copiando o Conteudo
     Self.Tipo:=UpperCase(copy(substring,1,fim-1));

     SubString:=copy(substring,fim+1,length(substring));

     If (substring='')
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;

     Primeiro:=Pos('"',substring);
     If (primeiro=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     SubString:=copy(substring,primeiro+1,length(substring));
     Fim:=Pos('"',substring);
     If (Fim=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     //Copiando o Conteudo
     Self.Classificacao:=copy(substring,1,fim-1);

     SubString:=copy(substring,fim+1,length(substring));

     If (substring='')
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;

     Primeiro:=Pos('"',substring);
     If (primeiro=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     SubString:=copy(substring,primeiro+1,length(substring));
     Fim:=Pos('"',substring);
     If (Fim=0)
     Then Begin
               Messagedlg('Linha com estruturada Inv�lida!',mterror,[mbok],0);
               exit;
          End;
     //Copiando o Conteudo
     Self.Nome:=copy(substring,1,fim-1);
     Self.ImprimeBalancoSS:='S';
     Self.Status:=dsInsert;
     Result:=Self.Salvar(False);
end;

function TObjPlanodeContas.ExcluiTodos: Boolean;
begin
     result:=False;
     //apago tudo
     With Self.ObjDataset do
     Begin
          SelectSQL.clear;
          SelectSQL.add('Delete from tabplanodecontas');
          Try
            ExecSQL;
            result:=true;
          Except
                Exit;
          End;
     End;
     



end;


function TObjPlanodeContas.RetornaCampoCodigo: string;
begin
     result:='codigo';
end;

function TObjPlanodeContas.RetornaCampoNome: string;
begin
     result:='nome';
end;


function TObjPlanodeContas.ExportaDados(PArq: String): Boolean;
var
Arq:TextFile;
LInha:String;
begin
     Result:=False;
     //Tenho o nome do Arquivo Texto como Parametro
     //Tento Abrir o Arquivo
     Try
        AssignFile(Arq,Parq);
        Rewrite(Arq);
     Except
           Messagedlg('Erro na Tentativa de Criar o  Arquivo:'+PArq,mterror,[mbok],0);
           exit;
     End;
Try
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select * from TabPlanodeContas order by codigo');
          open;
          first;
          While Not (eof) do
          Begin
               Writeln(arq,completapalavra(fieldbyname('codigo').asstring,9,' ')+','+completapalavra(fieldbyname('tipo').asstring,1,' ')+','+
                           completapalavra(fieldbyname('classificacao').asstring,30,' ')+','+
                           completapalavra(fieldbyname('nome').asstring,100,' '));
               Flush(arq);
               next;
          End;
          result:=True;
     End;
Finally
       Closefile(arq);
End;


end;

function TObjPlanodeContas.Get_ImprimeBalancoSS: string;
begin
     Result:=Self.ImprimeBalancoSS;
end;

procedure TObjPlanodeContas.Submit_ImprimeBalancoSS(
  Parametro: string);
begin
     Self.ImprimeBalancoSS:=Parametro;
end;

function TObjPlanodeContas.Get_NovoCodigo(var Pcodigo: string):boolean;
var
QueryNovoCodigo:TIbquery;
begin
     result:=False;
     //procurando um novo codigo
     Try
        QueryNovoCodigo:=TIBQuery.create(nil);
        QueryNovoCodigo.Database:=Self.ObjDataset.Database;
     Except
           Messagedlg('Erro na tentativa de Criar a Query de Gera��o de Novo C�digo',mterror,[mbok],0);
           exit;
     End;

     Try
        With QueryNovoCodigo do
        begin
             close;
             SQL.clear;
             SQL.add('Select max(codigo) as maximo from TabPlanodeContas');
             try
                open;
                Pcodigo:=inttostr(fieldbyname('maximo').asinteger+1);
             except
                   on E:Exception do
                   Begin
                        Messagedlg(E.message,mterror,[mbok],0);
                        exit;
                   end;
             end;

             result:=true;
        end;
     finally
            freeandnil(QueryNovoCodigo);
     end;
end;

procedure TObjPlanodeContas.Imprimir;
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPLANODECONTAS';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Plano de Contas');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               0:begin
                      Self.ImprimePlanodeContas;
               end;
               -1 : exit;
          End;
     end;

end;

procedure TObjPlanodeContas.ImprimePlanodeContas;
var
linha:integer;
begin
     With Self.ObjDataset do
     begin
        close;
        SelectSQL.clear;
        SelectSQL.add('Select * from TabPlanodeContas order by classificacao');
        open;
        if (recordcount=0)
        then Begin
                  Messagedlg('Nenhum dado foi selecionado na pesquisa',mtinformation,[mbok],0);
                  exit;
        end;
        FreltxtRDPRINT.ConfiguraImpressao;
        FreltxtRDPRINT.RDprint.Abrir;
        if (FreltxtRDPRINT.RDprint.Setup=False)
        then begin
                  FreltxtRDPRINT.RDprint.Fechar;
                  exit;
        End;
        linha:=3;
        FreltxtRDPRINT.RDprint.ImpC(linha,45,'PLANO DE CONTAS',[negrito]);
        inc(linha,2);
        FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                                CompletaPalavra('',1,' ')+' '+
                                                CompletaPalavra('CLASSIFICACAO',30,' ')+' '+
                                                CompletaPalavra('NOME',50,' '),[negrito]);
        inc(linha,2);

        While not(eof) do

        Begin
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);

             if (fieldbyname('tipo').asstring='T')
             Then begin
                        FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                                CompletaPalavra(fieldbyname('tipo').asstring,1,' ')+' '+
                                                CompletaPalavra(fieldbyname('classificacao').asstring,30,' ')+' '+
                                                CompletaPalavra(fieldbyname('nome').asstring,50,' '),[negrito]);
             End
             Else Begin
                       FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                                CompletaPalavra(fieldbyname('tipo').asstring,1,' ')+' '+
                                                CompletaPalavra(fieldbyname('classificacao').asstring,30,' ')+' '+
                                                CompletaPalavra(fieldbyname('nome').asstring,50,' '));
             End;
             inc(linha,1);
             next;

        end;
        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
        FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
        FreltxtRDPRINT.RDprint.Fechar;
     End;
end;

procedure TObjPlanodeContas.fieldbyname(PCampo, PValor: String);
begin
     PCampo:=Uppercase(pcampo);

     If (Pcampo='CODIGO')
     Then Begin
              Self.Submit_CODIGO(pVALOR);
              exit;
     End;
     If (Pcampo='TIPO')
     Then Begin
              Self.Submit_Tipo(pVALOR);
              exit;
     End;

     If (Pcampo='CLASSIFICACAO')
     Then Begin
              Self.Submit_Classificacao(pVALOR);
              exit;
     End;


     If (Pcampo='NOME')
     Then Begin
              Self.Submit_Nome(pVALOR);
              exit;
     End;

     If (Pcampo='IMPRIMEBALANCOSS')
     Then Begin
              Self.Submit_ImprimeBalancoSS(pVALOR);
              exit;
     End;

     If (Pcampo='CODIGO2')
     Then Begin
              Self.Submit_CODIGO2(pVALOR);
              exit;
     End;
end;

function TObjPlanodeContas.fieldbyname(PCampo: String): String;
begin
     PCampo:=Uppercase(pcampo);
     Result:='';
     If (Pcampo='CODIGO')
     Then Begin
              Result:=Self.Get_CODIGO;
              exit;
     End;
     If (Pcampo='TIPO')
     Then Begin
              Result:=Self.Get_Tipo;
              exit;
     End;

     If (Pcampo='CLASSIFICACAO')
     Then Begin
              Result:=Self.Get_Classificacao;
              exit;
     End;


     If (Pcampo='NOME')
     Then Begin
              Result:=Self.Get_Nome;
              exit;
     End;

     If (Pcampo='IMPRIMEBALANCOSS')
     Then Begin
              Result:=Self.Get_ImprimeBalancoSS;
              exit;
     End;

     If (Pcampo='CODIGO2')
     Then Begin
              Result:=Self.Get_CODIGO2;
              exit;
     End;

end;

end.
