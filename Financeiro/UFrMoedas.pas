unit UFrMoedas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFrMoedas = class(TFrame)
  private
    procedure rbClick(Sender: TObject);
    { Private declarations }
  public
    Procedure PreencheMoedas(PStrList:TStringList);
  end;

implementation

{$R *.dfm}

{ TFrMoedas }

procedure TFrMoedas.PreencheMoedas(PStrList:TStringList);
Var Cont, Topo:Integer;
begin
    Topo:=0;
    for Cont:=0 to PStrList.Count-1 do
    Begin   // Radio Button
        with TRadioButton.Create(Self)  do
        begin
               Parent:=Self;
               Caption:=PStrList[Cont];
               Left:=5;
               Top:=Topo+5; //17
               Checked:=false;
               Name:='RB'+PStrList[Cont];
               font.Size:=14;
               onClick:=rbClick;
               Inc(Topo,25);
         end;
    end;
end;

procedure TFrMoedas.rbClick(Sender: TObject);
begin
     if (TRadioButton(Sender).checked = true)
     then Begin
           ShowMessage(TRadioButton(Sender).Caption);
     end;  
end;

end.
