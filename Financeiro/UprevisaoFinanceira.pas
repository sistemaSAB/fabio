unit UprevisaoFinanceira;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPrevisaoFinanceira;

type
  TFprevisaoFinanceira = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btopcoes: TBitBtn;
    BtLancaTitulo: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    btpesquisar: TBitBtn;
    Btnovo: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LbPortador: TLabel;
    Label7: TLabel;
    lbnomecontagerencial: TLabel;
    Label9: TLabel;
    lbnomesubcontagerencial: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    LbNomeCredorDevedor: TLabel;
    Label12: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    edtdata: TMaskEdit;
    edtvalor: TEdit;
    combodebito_credito: TComboBox;
    edtportador: TEdit;
    edtcontagerencial: TEdit;
    edtsubcontagerencial: TEdit;
    comboconsiderarmesessubsequentes: TComboBox;
    edtdatalimite: TMaskEdit;
    combocredordevedor: TComboBox;
    combocredordevedorcodigo: TComboBox;
    edtcodigocredordevedor: TEdit;
    combosomatoriaprevisao: TComboBox;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalorExit(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorExit(Sender: TObject);
    procedure LancaTitulo;
    procedure FormShow(Sender: TObject);
    procedure edtcontagerencialExit(Sender: TObject);
    procedure edtsubcontagerencialExit(Sender: TObject);
    procedure edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure combocredordevedorChange(Sender: TObject);
    procedure combocredordevedorcodigoChange(Sender: TObject);
    procedure edtcodigocredordevedorExit(Sender: TObject);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorKeyPress(Sender: TObject;
      var Key: Char);
    procedure BtLancaTituloClick(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    procedure controlChange(Sender: TObject);
    { Private declarations }
  public
    { Public declarations }
        { Public declarations }
        Function Criar:boolean;
        Procedure Destruir;

  end;

var
  FprevisaoFinanceira: TFprevisaoFinanceira;
  ObjPrevisaoFinanceira:TObjPrevisaoFinanceira;

implementation

uses UessencialGlobal, Upesquisa, Uportador, UDataModulo,
  UescolheImagemBotao, UObjContaGer, UObjCredorDevedor, UopcaoRel;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFprevisaoFinanceira.ControlesParaObjeto: Boolean;
Begin
       Try
        With ObjPrevisaoFinanceira do
        Begin
             Submit_CODIGO(edtCODIGO.text);
             Submit_Historico(edtHistorico.text);
             Submit_Data(edtData.text);
             Submit_Valor(tira_ponto(edtValor.text));
             Submit_Debito_Credito(comboDebito_Credito.text[1]);
             Portador.Submit_CODIGO(edtportador.text);
             ContaGerencial.Submit_codigo(edtcontagerencial.text);
             SubContaGerencial.Submit_codigo(edtsubcontagerencial.text);
             Submit_considerarmesessubsequentes(Submit_ComboBox(comboconsiderarmesessubsequentes));
             Submit_datalimite(edtdatalimite.Text);

             combocredordevedorcodigo.ItemIndex:=combocredordevedor.ItemIndex;
             CREDORDEVEDOR.Submit_codigo(comboCREDORDEVEDORcodigo.text);
             Submit_CodigoCredorDevedor(edtcodigocredordevedor.text);
             Submit_somatoriaprevisao(submit_Combobox(combosomatoriaprevisao));


             result:=true;
        End;
     Except
          result:=False;
     End;

End;

function TFprevisaoFinanceira.ObjetoParaControles: Boolean;
var
auxilio:string;
cont:integer;
Begin
     Try
        With ObjPrevisaoFinanceira do
        Begin
             edtCODIGO.text:=Get_CODIGO;
             edtHistorico.text:=Get_Historico;
             edtData.text:=Get_Data;
             edtValor.text:=formata_valor(Get_Valor);
             If (Get_Debito_Credito='D')
             Then combodebito_credito.itemindex:=0
             Else combodebito_credito.itemindex:=1;
             edtportador.text:=Portador.get_codigo;
             LbPortador.Caption:=Portador.Get_Nome;
             edtcontagerencial.text:=ContaGerencial.get_codigo;
             lbnomecontagerencial.Caption:=ContaGerencial.Get_Nome;
             edtsubcontagerencial.text:=SubContaGerencial.get_codigo;
             lbnomesubcontagerencial.Caption:=SubContaGerencial.Get_Nome;
             if (get_considerarmesessubsequentes='N')
             Then comboconsiderarmesessubsequentes.ItemIndex:=0
             Else comboconsiderarmesessubsequentes.ItemIndex:=1;
             edtdatalimite.Text:=Get_datalimite;
             edtcodigocredordevedor.text:=Get_CODIGOCREDORDEVEDOR;

             if (Get_somatoriaprevisao='S')
             Then combosomatoriaprevisao.itemindex:=1
             Else combosomatoriaprevisao.ItemIndex:=0;

             combocredordevedor.itemindex:=-1;

             auxilio:= CREDORDEVEDOR.get_codigo;
             for cont:=0 to combocredordevedorcodigo.items.Count-1 do
             Begin
                if combocredordevedorcodigo.Items[cont]=auxilio
                Then Begin
                           combocredordevedor.itemindex:=cont;
                           combocredordevedorcodigo.itemindex:=cont;
                           LbNomeCredorDevedor.caption:=ObjPrevisaoFinanceira.CredorDevedor.Get_NomeCredorDevedor(combocredordevedorcodigo.Text,edtcodigocredordevedor.text);
                           break;
                End;
             End;



             result:=true;
        End;
     Except
              result:=False;
     End;
End;

function TFprevisaoFinanceira.TabelaParaControles: Boolean;
begin
     If (ObjPrevisaoFinanceira.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFprevisaoFinanceira.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjPrevisaoFinanceira=Nil)
     Then exit;

      If (ObjPrevisaoFinanceira.status<>dsinactive)
      Then Begin
                Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
                abort;
                exit;
           End;

      ObjPrevisaoFinanceira.free;
      Screen.OnActiveControlChange:=nil;
end;

procedure TFprevisaoFinanceira.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
      end;
end;

procedure TFprevisaoFinanceira.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.LimpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjPrevisaoFinanceira.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjPrevisaoFinanceira.status:=dsInsert;
     combodebito_credito.setfocus;

end;


procedure TFprevisaoFinanceira.btalterarClick(Sender: TObject);
begin
    If (ObjPrevisaoFinanceira.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjPrevisaoFinanceira.Status:=dsEdit;
                combodebito_credito.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
          End;


end;

procedure TFprevisaoFinanceira.btgravarClick(Sender: TObject);
begin

     If ObjPrevisaoFinanceira.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPrevisaoFinanceira.salvar(True)=False)
     Then exit;

     mostra_botoes(Self);
     desabilita_campos(Self);

end;

procedure TFprevisaoFinanceira.btexcluirClick(Sender: TObject);
begin
     If (ObjPrevisaoFinanceira.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjPrevisaoFinanceira.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjPrevisaoFinanceira.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     
     Self.LimpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFprevisaoFinanceira.btcancelarClick(Sender: TObject);
begin
     ObjPrevisaoFinanceira.cancelar;

     limpaedit(Self);
     Self.LimpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFprevisaoFinanceira.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFprevisaoFinanceira.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPrevisaoFinanceira.Get_pesquisa,ObjPrevisaoFinanceira.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPrevisaoFinanceira.status<>dsinactive
                                  then exit;

                                  If (ObjPrevisaoFinanceira.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.LimpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

function TFprevisaoFinanceira.Criar: boolean;
begin


end;

procedure TFprevisaoFinanceira.Destruir;
begin

end;

procedure TFprevisaoFinanceira.edtvalorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If (not(key in ['0'..'9',#8,',']))
     Then Begin
                If (Key='.')
                Then Key:=','
                Else Key:=#0;
     End;
end;

procedure TFprevisaoFinanceira.edtvalorExit(Sender: TObject);
begin
     if (Tedit(Sender).text<>'')
     Then Tedit(Sender).text:=formata_valor(Tedit(Sender).text);
end;

procedure TFprevisaoFinanceira.edtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador1:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador1:=TFportador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjPrevisaoFinanceira.Portador.Get_Pesquisa,ObjPrevisaoFinanceira.Portador.Get_TituloPesquisa,Fportador1)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 ObjPrevisaoFinanceira.Portador.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                                 ObjPrevisaoFinanceira.Portador.TabelaparaObjeto;
                                 LbPortador.caption:=ObjPrevisaoFinanceira.Portador.Get_Nome;
                                //Pegando o nome, no caso de ter uma label para isso
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador1);
     End;

end;

procedure TFprevisaoFinanceira.edtportadorExit(Sender: TObject);
begin
        If (Tedit(sender).text<>'')
        Then Begin
                  If (ObjPrevisaoFinanceira.Portador.LocalizaCodigo(Tedit(Sender).text)=false)
                  Then Begin
                            Tedit(Sender).text:='';
                            Self.LimpaLabels;
                            exit;
                  End;
                  ObjPrevisaoFinanceira.Portador.TabelaparaObjeto;
                  LbPortador.caption:=ObjPrevisaoFinanceira.Portador.Get_Nome;
        End;
end;

procedure TFprevisaoFinanceira.LancaTitulo;
begin
     If (ObjPrevisaoFinanceira.status<>dsinactive)or(edtcodigo.text='')
     Then exit;

     If (ObjPrevisaofinanceira.LancaTitulo(edtcodigo.text)=True)
     Then btcancelar.onclick(nil);
end;

procedure TFprevisaoFinanceira.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     limpaedit(Self);
     Self.LimpaLabels;
     desabilita_campos(Self);

     Try
        ObjPrevisaoFinanceira:=TObjPrevisaoFinanceira.create;
        ObjPrevisaoFinanceira.CredorDevedor.Get_listaNomeCredorDevedor(combocredordevedor.items);
        ObjPrevisaoFinanceira.CredorDevedor.Get_listaCodigoCredorDevedor(combocredordevedorcodigo.items);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes,BtLancaTitulo,nil,nil,nil);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PREVIS�O FINANCEIRA')=False)
     Then Begin
               esconde_botoes(Self);
               exit;
     End
     Else mostra_botoes(Self);
     
     Screen.OnActiveControlChange:=Self.controlChange;
end;

procedure TFprevisaoFinanceira.LimpaLabels;
begin
     lbnomecontagerencial.caption:='';
     lbnomesubcontagerencial.caption:='';
     LbPortador.caption:='';
     LbNomeCredorDevedor.caption:='';

end;

procedure TFprevisaoFinanceira.edtcontagerencialExit(Sender: TObject);
begin
     ObjPrevisaoFinanceira.EdtContaGerencialExit(sender,lbnomecontagerencial);
end;

procedure TFprevisaoFinanceira.edtsubcontagerencialExit(Sender: TObject);
begin
    ObjPrevisaoFinanceira.EdtSubContaGerencialExit(sender,lbnomesubcontagerencial);
end;

procedure TFprevisaoFinanceira.edtcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjPrevisaoFinanceira.EdtContaGerencialKeyDown(sender,key,shift,lbnomecontagerencial,Submit_ComboBox(combodebito_credito));
end;

procedure TFprevisaoFinanceira.edtsubcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

     if (edtcontagerencial.Text <> '')
     then ObjPrevisaoFinanceira.EdtSubContaGerencialKeyDown(sender,key,shift,lbnomesubcontagerencial,edtcontagerencial.Text);
end;

procedure TFprevisaoFinanceira.combocredordevedorChange(Sender: TObject);
begin
    combocredordevedorcodigo.itemindex:=combocredordevedor.itemindex;
end;

procedure TFprevisaoFinanceira.combocredordevedorcodigoChange(
  Sender: TObject);
begin
    combocredordevedor.itemindex:=Combocredordevedorcodigo.itemindex;
end;

procedure TFprevisaoFinanceira.edtcodigocredordevedorExit(Sender: TObject);
begin
     //preciso sair com o nome do credordevedor escolhido
     //atraves do FrLancamento1.ObjLancamento.Pendencia.Titulo
     LbNomeCredorDevedor.caption:='';

     If (edtcodigocredordevedor.text='') or (combocredordevedor.ItemIndex=-1)
     Then exit;

     LbNomeCredorDevedor.caption:=ObjPrevisaoFinanceira.CredorDevedor.Get_NomeCredorDevedor(combocredordevedorcodigo.Text,edtcodigocredordevedor.text);

end;

procedure TFprevisaoFinanceira.edtcodigocredordevedorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combocredordevedor.itemindex=-1)
     Then exit;


     If (ObjPrevisaoFinanceira.CREDORDEVEDOR.LocalizaCodigo(combocredordevedorcodigo.Text)=true)
     Then ObjPrevisaoFinanceira.CredorDevedor.TabelaparaObjeto
     Else exit;



     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(ObjPrevisaoFinanceira.CredorDevedor.Get_InstrucaoSQL,'Pesquisa de '+Combocredordevedor.text,ObjPrevisaoFinanceira.CredorDevedor.Get_NomeFormulario)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedor.caption:=FpesquisaLocal.QueryPesq.fieldbyname(ObjPrevisaoFinanceira.CredorDevedor.Get_CampoNome).asstring;
                             End;

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFprevisaoFinanceira.edtcodigocredordevedorKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (key in['0'..'9',#8])
    Then Key:=#0;
end;

procedure TFprevisaoFinanceira.BtLancaTituloClick(Sender: TObject);
begin
      self.LancaTitulo;
end;

procedure TFprevisaoFinanceira.btopcoesClick(Sender: TObject);
begin
    with FOpcaorel do
    begin
           RgOpcoes.Items.clear;
           RgOpcoes.Items.add('Lan�ar T�tulo');                   //0

          showmodal;
          if (tag=0)
          Then exit;

          case RgOpcoes.ItemIndex of
              0:self.LancaTitulo;
          end;
    end;
end;

procedure TFprevisaoFinanceira.controlChange(Sender: TObject);
begin
    controlChange_focus(sender,Self);
end;

//Rodolfo
procedure TFprevisaoFinanceira.FormCreate(Sender: TObject);
var
  addDuploClique : TDuploClique;
begin
  addDuploClique.AdicionaDuploClique(Sender);   //adicao de duplo clique em edits que possuem onkeydown
end;

end.

