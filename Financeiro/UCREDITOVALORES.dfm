object FCREDITOVALORES: TFCREDITOVALORES
  Left = 226
  Top = 286
  Width = 654
  Height = 279
  Caption = 'Cadastro de Cr'#233'dito no Lan'#231'amento - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 638
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 508
      Top = 0
      Width = 130
      Height = 49
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Valores de Cr'#233'dito'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object Btnovo: TBitBtn
      Left = 2
      Top = -3
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 52
      Top = -3
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 102
      Top = -3
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 152
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 252
      Top = -3
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 302
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btexcluir: TBitBtn
      Left = 202
      Top = -3
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 402
      Top = -3
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 638
    Height = 142
    Align = alClient
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 636
      Height = 140
      Align = alClient
      Stretch = True
    end
    object LbCODIGO: TLabel
      Left = 24
      Top = 20
      Width = 44
      Height = 13
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Lbcredordevedor: TLabel
      Left = 24
      Top = 42
      Width = 107
      Height = 13
      Caption = 'Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNomecredordevedor: TLabel
      Left = 344
      Top = 42
      Width = 107
      Height = 13
      Caption = 'Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Lbcodigocredordevedor: TLabel
      Left = 24
      Top = 64
      Width = 175
      Height = 13
      Caption = 'C'#243'digo do Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbvalores: TLabel
      Left = 24
      Top = 86
      Width = 212
      Height = 13
      Caption = 'C'#243'digo do Lan'#231'amento (valores)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Lbvalorusado: TLabel
      Left = 24
      Top = 108
      Width = 78
      Height = 13
      Caption = 'Valor Usado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCODIGO: TEdit
      Left = 240
      Top = 20
      Width = 100
      Height = 19
      Color = clWhite
      MaxLength = 9
      TabOrder = 0
    end
    object Edtcredordevedor: TEdit
      Left = 240
      Top = 42
      Width = 100
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 1
      OnExit = edtcredordevedorExit
      OnKeyDown = edtcredordevedorKeyDown
    end
    object Edtcodigocredordevedor: TEdit
      Left = 240
      Top = 64
      Width = 100
      Height = 19
      Color = clWhite
      MaxLength = 9
      TabOrder = 2
    end
    object edtvalores: TEdit
      Left = 240
      Top = 86
      Width = 100
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 3
      OnExit = edtvaloresExit
      OnKeyDown = edtvaloresKeyDown
    end
    object Edtvalorusado: TEdit
      Left = 240
      Top = 108
      Width = 100
      Height = 19
      MaxLength = 9
      TabOrder = 4
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 191
    Width = 638
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    DesignSize = (
      638
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 638
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 340
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 809
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 809
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
end
