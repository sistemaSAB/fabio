unit UpendenciasATRASO;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons;

type
  TFpendenciasAtraso = class(TForm)
    Grid: TStringGrid;
    BtImprime: TBitBtn;
    Label1: TLabel;
    lbtotalpendencias: TLabel;
    lbcliente: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FpendenciasAtraso: TFpendenciasAtraso;

implementation

uses rdprint,UessencialGlobal, URelTXT, UReltxtRDPRINT;

{$R *.DFM}

procedure TFpendenciasAtraso.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     PegaFiguraBotao(btimprime,'botaorelatorios.bmp');
end;

procedure TFpendenciasAtraso.BtImprimeClick(Sender: TObject);
var
Cont,linha:integer;
begin
     FreltxtRDPRINT.ConfiguraImpressao;
     FreltxtRDPRINT.RDprint.Abrir;
     if (FreltxtRDPRINT.RDprint.setup=False)
     Then Begin
               FreltxtRDPRINT.RDprint.Fechar;
               exit;
     End;
     linha:=3;
     FreltxtRDPRINT.RDprint.ImpC(linha,45,'PEND�NCIAS EM ATRASO',[negrito]);
     inc(linha,2);
     FreltxtRDPRINT.RDprint.Impf(linha,1,lbcliente.caption,[negrito]);
     inc(linha,2);

     FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('T�TULO',9,' ')+' '+
                                        CompletaPalavra('PEND�NCIAS',10,' ')+' '+
                                        CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                        CompletaPalavra('SALDO',12,' '),[negrito]);
     inc(linha,2);
     for cont:=1 to Grid.RowCount -1 do
     Begin
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(grid.cells[0,cont],9,' ')+' '+
                                            CompletaPalavra(grid.cells[1,cont],9,' ')+' '+
                                            CompletaPalavra(grid.cells[2,cont],10,' ')+' '+
                                            CompletaPalavra_a_Esquerda(grid.cells[3,cont],9,' '));
          inc(linha,1);
     End;
     inc(linha,1);
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
     FreltxtRDPRINT.RDprint.Imp(linha,1,label1.caption+' '+lbtotalpendencias.caption);
     FreltxtRDPRINT.RDprint.Fechar;
end;

end.
