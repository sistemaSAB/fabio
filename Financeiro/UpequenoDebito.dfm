object FpequenoDebito: TFpequenoDebito
  Left = 312
  Top = 155
  Width = 303
  Height = 132
  Caption = 'Dados do D'#233'bito - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 3
    Top = 12
    Width = 49
    Height = 14
    Caption = 'Hist'#243'rico'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 3
    Top = 44
    Width = 28
    Height = 14
    Caption = 'Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 3
    Top = 76
    Width = 23
    Height = 14
    Caption = 'Data'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edthistorico: TEdit
    Left = 88
    Top = 9
    Width = 201
    Height = 19
    MaxLength = 60
    TabOrder = 0
  end
  object edtvalor: TEdit
    Left = 88
    Top = 41
    Width = 72
    Height = 19
    MaxLength = 9
    TabOrder = 1
  end
  object edtdata: TMaskEdit
    Left = 88
    Top = 73
    Width = 72
    Height = 19
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object BitBtn1: TBitBtn
    Left = 167
    Top = 37
    Width = 124
    Height = 33
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 167
    Top = 71
    Width = 124
    Height = 33
    TabOrder = 4
    Kind = bkCancel
  end
end
