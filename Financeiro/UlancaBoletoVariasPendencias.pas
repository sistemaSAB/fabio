unit UlancaBoletoVariasPendencias;

interface

uses
  db,UessencialGlobal, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, Buttons, Mask, ExtCtrls,UobjPendencia,ComObj;

type
  TFlancaBoletoVariasPendencias = class(TForm)
    StrgPendencias: TStringGrid;
    v: TPanel;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LbNomeCredorDevedor: TLabel;
    edtvencimento1: TMaskEdit;
    edtvencimento2: TMaskEdit;
    combocredordevedor: TComboBox;
    combocredordevedorcodigo: TComboBox;
    edtcodigocredordevedor: TEdit;
    Label6: TLabel;
    EdtContaGerencial: TEdit;
    LbNomeContaGerencial: TLabel;
    btimpenderecos: TBitBtn;
    Label7: TLabel;
    edtsubconta: TEdit;
    lbnomesubconta: TLabel;
    Label28: TLabel;
    Btpesquisar: TBitBtn;
    Panel2: TPanel;
    ImagemFundo: TImage;
    Label10: TLabel;
    Label9: TLabel;
    CHB_UtilizaNumDcto: TCheckBox;
    Label8: TLabel;
    ChkSelecionaTodos: TCheckBox;
    checkpreview: TCheckBox;
    Label5: TLabel;
    LbtotalSelecionado: TLabel;
    BTRenegocia: TBitBtn;
    Label11: TLabel;
    procedure BtpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorExit(Sender: TObject);
    procedure StrgPendenciasDblClick(Sender: TObject);
    procedure StrgPendenciasKeyPress(Sender: TObject; var Key: Char);
    procedure BTRenegociaClick(Sender: TObject);
    procedure edtSomenteNumerosKeyPress(Sender: TObject;var Key: Char);
    procedure EdtContaGerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtContaGerencialExit(Sender: TObject);
    procedure SelecinaTodos(Sender: TObject);
    procedure btimpenderecosClick(Sender: TObject);
    procedure edtsubcontaExit(Sender: TObject);
    procedure edtsubcontaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    PcredorDevedor,PcodigoCredorDevedor:string;
    ObjPendenciaLocal:Tobjpendencia;
    procedure SomaSelecionadas;
    function AdiquiraOuCrieObjeto(const ClasseNome: String): IDispatch;

  public
    { Public declarations }
    Procedure PassaObjeto(Pobjeto:TobjPendencia);
    

  end;

var
  FlancaBoletoVariasPendencias: TFlancaBoletoVariasPendencias;

implementation

uses Upesquisa, UObjTitulo, IBQuery,
  UobjREGISTRORETORNOCOBRANCA, UescolheImagemBotao;

{$R *.dfm}

procedure TFlancaBoletoVariasPendencias.BtpesquisarClick(Sender: TObject);
var
cont:integer;
begin

     StrgPendencias.ColCount:=9;
     StrgPendencias.Cols[0].clear;
     StrgPendencias.Cols[1].clear;
     StrgPendencias.Cols[2].clear;
     StrgPendencias.Cols[3].clear;
     StrgPendencias.Cols[4].clear;
     StrgPendencias.Cols[5].clear;
     StrgPendencias.Cols[6].clear;
     StrgPendencias.Cols[7].clear;
     StrgPendencias.Cols[8].clear;


     StrgPendencias.Cells[0,0]:='..';
     StrgPendencias.Cells[1,0]:='T�TULO';
     StrgPendencias.Cells[2,0]:='PEND.';
     StrgPendencias.Cells[3,0]:='VENCIMENTO';
     StrgPendencias.Cells[4,0]:='VALOR';
     StrgPendencias.Cells[5,0]:='SALDO';
     StrgPendencias.Cells[6,0]:='HIST�RICO';
     StrgPendencias.Cells[7,0]:='NUMDCTO';
     StrgPendencias.Cells[8,0]:='NOME';


     With Self.ObjPendenciaLocal.ObjQlocal do
     Begin
          close;
          SQL.clear;
          sql.add('Select tabpendencia.titulo,tabpendencia.codigo as pendencia,tabpendencia.vencimento,');
          sql.add('tabpendencia.valor,tabpendencia.saldo,tabtitulo.historico,');
          sql.add('tabtitulo.credordevedor, tabtitulo.codigocredordevedor,');
          sql.add('tabtitulo.numdcto from tabpendencia');
          SQL.add('join TabTitulo on TabPendencia.Titulo=TabTitulo.codigo');
          sql.add('join tabcontager on TabTitulo.ContaGerencial=TabContaGer.codigo');
          sql.add('where tabcontager.tipo=''C'' and tabpendencia.saldo>0 and (tabpendencia.BoletoBancario is null)');

          if (combocredordevedor.ItemIndex>=0)
          Then sql.add('and TabTitulo.credordevedor='+combocredordevedorcodigo.Items[combocredordevedor.itemindex]);

          if (edtcodigocredordevedor.Text<>'')
          Then sql.add('and TabTitulo.CodigocredorDevedor='+edtcodigocredordevedor.Text);

          if (EdtContaGerencial.Text<>'')
          Then sql.add('And tabTitulo.ContaGerencial='+EdtContaGerencial.text);

          if (edtsubconta.Text<>'')
          Then sql.add('And tabTitulo.SubContaGerencial='+edtsubconta.text);

          Try
             StrToDate(edtvencimento1.text);
             StrToDate(edtvencimento2.text);
             sql.add('and TabPendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento1.text))+#39);
             sql.add('and TabPendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento2.text))+#39);
          Except
                on e:exception do
                Begin
                     mensagemerro(e.message);
                     exit;
                End;

          End;
          Open;
          PcredorDevedor:=combocredordevedorcodigo.Items[combocredordevedor.itemindex];
          PcodigoCredorDevedor:=edtcodigocredordevedor.Text;
          last;

          if (RecordCount>0)
          Then StrgPendencias.RowCount:=Recordcount+1
          Else StrgPendencias.RowCount:=2;

          first;

          cont:=1;
          While not(eof) do
          Begin
               StrgPendencias.Cells[0,cont]:='';
               StrgPendencias.Cells[1,cont]:=fieldbyname('titulo').asstring;
               StrgPendencias.Cells[2,cont]:=fieldbyname('pendencia').asstring;
               StrgPendencias.Cells[3,cont]:=fieldbyname('vencimento').asstring;
               StrgPendencias.Cells[4,cont]:=formata_valor(fieldbyname('valor').asstring);
               StrgPendencias.Cells[5,cont]:=formata_valor(fieldbyname('saldo').asstring);
               StrgPendencias.Cells[6,cont]:=fieldbyname('historico').asstring;
               StrgPendencias.Cells[7,cont]:=fieldbyname('numdcto').asstring;
               StrgPendencias.Cells[8,cont]:=Self.ObjPendenciaLocal.Titulo.Get_NomeCredorDevedor(fieldbyname('CredorDevedor').asstring,fieldbyname('CodigoCredorDevedor').asstring);
               next;
               inc(cont,1);
          End;

          LbtotalSelecionado.caption:='0,00';
          AjustaLArguraColunaGrid(StrgPendencias);
          StrgPendencias.SetFocus;
     End;
End;

procedure TFlancaBoletoVariasPendencias.FormShow(Sender: TObject);
begin
     Btpesquisar.Enabled:=False;
     PcredorDevedor:='';
     PcodigoCredorDevedor:='';
     LbtotalSelecionado.caption:='0,00';
     LbNomeCredorDevedor.caption:='';
     LbNomeContaGerencial.caption:='';
     lbnomesubconta.caption:='';
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     retira_fundo_labels(Self);
     
     Try
        Btpesquisar.Enabled:=True;
        edtvencimento1.setfocus;
        Self.ObjPendenciaLocal.Titulo.Get_ListacredorDevedor(combocredordevedor.items);
        Self.ObjPendenciaLocal.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigo.items);
     Except
           mensagemerro('Erro na tentativa de Criar o Objeto de Pend�ncia');
           exit;
     End;
     
end;

procedure TFlancaBoletoVariasPendencias.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then begin
           Perform(Wm_NextDlgCtl,0,0);
      end;
end;

procedure TFlancaBoletoVariasPendencias.edtcodigocredordevedorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combocredordevedor.itemindex=-1)
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(Self.ObjPendenciaLocal.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigo.items[combocredordevedor.itemindex]),'Pesquisa de '+Combocredordevedor.text,Self.ObjPendenciaLocal.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex]))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedor.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjPendenciaLocal.Titulo.Get_CampoNomeCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex])).asstring;
                             End;

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFlancaBoletoVariasPendencias.edtcodigocredordevedorExit(
  Sender: TObject);
begin
      //preciso sair com o nome do credordevedor escolhido
     //atraves do FrLancamento1.ObjLancamento.Pendencia.Titulo
     LbNomeCredorDevedor.caption:='';

     If (edtcodigocredordevedor.text='') or (combocredordevedor.ItemIndex=-1)
     Then exit;

     LbNomeCredorDevedor.caption:=Self.ObjPendenciaLocal.Titulo.Get_NomeCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex],edtcodigocredordevedor.text);
end;

procedure TFlancaBoletoVariasPendencias.StrgPendenciasDblClick(Sender: TObject);
begin
     if (StrgPendencias.Cells[1,StrgPendencias.row]='')
     Then exit;

     if (StrgPendencias.Cells[0,StrgPendencias.row]='X')
     then StrgPendencias.Cells[0,StrgPendencias.row]:=''
     Else StrgPendencias.Cells[0,StrgPendencias.row]:='X';
     Self.SomaSelecionadas;
end;

procedure TFlancaBoletoVariasPendencias.StrgPendenciasKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     then Begin
                StrgPendencias.setfocus;
                StrgPendenciasDblClick(sender);
     End;
end;

procedure TFlancaBoletoVariasPendencias.BTRenegociaClick(Sender: TObject);
var
Cont:integer;
StrlPendencias:TStringList;
StrHistoricoExtra:TStringList;
begin
     Try
        StrlPendencias:=TStringList.Create;
        StrHistoricoExtra:=TStringList.Create;
     Except
        MensagemErro('Erro na tentativa de Criar a String List para Pend�ncias Escolhidas');
        exit;
     End;

     try
         StrlPendencias.Clear;
         StrHistoricoExtra.Clear;


         For cont:=1 to StrgPendencias.RowCount-1 do
         Begin
              //********************************************************************
              if (StrgPendencias.Cells[0,cont]='X')
              Then begin
                        if (Self.ObjPendenciaLocal.LocalizaCodigo(StrgPendencias.Cells[2,cont])=True)
                        Then Begin
                                  Self.ObjPendenciaLocal.TabelaparaObjeto;
                                  StrlPendencias.add(StrgPendencias.Cells[2,cont]);

                                  if (CHB_UtilizaNumDcto.Checked=True)
                                  Then StrHistoricoExtra.Add(Self.ObjPendenciaLocal.Titulo.Get_NUMDCTO)
                                  Else StrHistoricoExtra.Add('');
                        End;
              End;
         End;

         if (StrlPendencias.Count=0)
         then Begin
                   MensagemAviso('Nenhuma pend�ncia foi selecionada');
                   exit;
         End;




         if (Self.ObjPendenciaLocal.LancaVariosBoletos(StrlPendencias,StrHistoricoExtra,'V',checkpreview.checked)=False)
         Then Begin
                  mensagemerro('Erro');
         End;

        BtpesquisarClick(sender);
     Finally
         FreeAndNil(StrlPendencias);
         Freeandnil(StrHistoricoExtra);
     End;
     
end;

procedure TFlancaBoletoVariasPendencias.SomaSelecionadas;
var
cont:integer;
Psoma:Currency;
begin
     Psoma:=0;
     for cont:=1 to StrgPendencias.RowCount-1 do
     begin
          if (StrgPendencias.Cells[0,cont]='X')
          then Psoma:=Psoma+strtofloat(tira_ponto(StrgPendencias.Cells[5,cont]));
     End;
     LbtotalSelecionado.caption:=formata_valor(psoma);
end;

function TFlancaBoletoVariasPendencias.AdiquiraOuCrieObjeto(const ClasseNome: String): IDispatch;
var Classifique_Id : TGUID;
begin
  Classifique_ID:=ProgIdToClassId(ClasseNome);
  Result:=CreateOleObject(ClasseNome);
end;


procedure TFlancaBoletoVariasPendencias.edtSomenteNumerosKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     then Begin
               if Key='.'
               Then Key:=','
               Else key:=#0;
     End;
end;

procedure TFlancaBoletoVariasPendencias.PassaObjeto(
  Pobjeto: TobjPendencia);
begin
     Self.ObjPendenciaLocal:=Pobjeto;
end;

procedure TFlancaBoletoVariasPendencias.EdtContaGerencialKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin

     Self.ObjPendenciaLocal.Titulo.EdtcontagerencialRECEBERKeyDown_spv(sender,key,shift,lbnomecontagerencial);
end;

procedure TFlancaBoletoVariasPendencias.EdtContaGerencialExit(
  Sender: TObject);
begin
     Self.ObjPendenciaLocal.Titulo.EdtcontagerencialExit(sender,lbnomecontagerencial);
end;

procedure TFlancaBoletoVariasPendencias.SelecinaTodos(Sender: TObject);
var
cont:integer;
begin
     if ChkSelecionaTodos.Checked=True
     then begin
           for cont:=1 to StrgPendencias.RowCount-1 do
           Begin
              StrgPendencias.Cells[0,cont]:='X';
           End;
           Self.SomaSelecionadas;
     end
     else begin     
           for cont:=1 to StrgPendencias.RowCount-1 do
           Begin
              StrgPendencias.Cells[0,cont]:='';
           End;
           Self.SomaSelecionadas;
     end;

end;

procedure TFlancaBoletoVariasPendencias.btimpenderecosClick(
  Sender: TObject);
var
Objregistro:TObjREGISTRORETORNOCOBRANCA;
begin
     Try
        Objregistro:=TObjREGISTRORETORNOCOBRANCA.create;
     Except
           Mensagemerro('Erro na tentativa de criar o objeto de registro');
           exit;
     End;

     Try
        Objregistro.imprimeenderecos;
     Finally
            Objregistro.Free;
     end;
end;

procedure TFlancaBoletoVariasPendencias.edtsubcontaExit(Sender: TObject);
begin
     Self.ObjPendenciaLocal.Titulo.EdtSubcontagerencialExit(sender,lbnomesubconta);
end;

procedure TFlancaBoletoVariasPendencias.edtsubcontaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjPendenciaLocal.Titulo.edtsubcontagerencialKeyDown(sender,key,shift,lbnomesubconta,EdtContaGerencial.text);
end;

end.
