object FprevisaoFinanceira: TFprevisaoFinanceira
  Left = 412
  Top = 213
  Width = 702
  Height = 398
  Caption = 'Cadastro de Previs'#245'es Financeiras- EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 686
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 564
      Top = 0
      Width = 122
      Height = 50
      Align = alRight
      Caption = 'Previs'#227'o Financeira'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btopcoes: TBitBtn
      Left = 402
      Top = -2
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btopcoesClick
    end
    object BtLancaTitulo: TBitBtn
      Left = 352
      Top = -2
      Width = 50
      Height = 52
      Caption = '&L'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = BtLancaTituloClick
    end
    object btcancelar: TBitBtn
      Left = 152
      Top = -2
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btsair: TBitBtn
      Left = 452
      Top = -2
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 9
      OnClick = btsairClick
    end
    object btexcluir: TBitBtn
      Left = 202
      Top = -2
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btgravar: TBitBtn
      Left = 102
      Top = -2
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btalterar: TBitBtn
      Left = 52
      Top = -2
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btrelatorios: TBitBtn
      Left = 302
      Top = -2
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btpesquisar: TBitBtn
      Left = 252
      Top = -2
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object Btnovo: TBitBtn
      Left = 2
      Top = -2
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 310
    Width = 686
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clMedGray
    TabOrder = 2
    DesignSize = (
      686
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 686
      Height = 50
      Align = alClient
      Stretch = True
    end
    object BitBtn9: TBitBtn
      Left = 834
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 834
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 50
    Width = 686
    Height = 260
    Align = alClient
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 684
      Height = 258
      Align = alClient
      Stretch = True
    end
    object Label1: TLabel
      Left = 23
      Top = 21
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 23
      Top = 46
      Width = 49
      Height = 14
      Caption = 'Hist'#243'rico'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 23
      Top = 121
      Width = 57
      Height = 14
      Caption = 'Data Inicial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 23
      Top = 145
      Width = 28
      Height = 14
      Caption = 'Valor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 220
      Top = 21
      Width = 27
      Height = 14
      Caption = 'Tipo '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 23
      Top = 171
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbPortador: TLabel
      Left = 220
      Top = 171
      Width = 62
      Height = 14
      Caption = 'LbPortador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 23
      Top = 196
      Width = 87
      Height = 14
      Caption = 'Conta Gerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbnomecontagerencial: TLabel
      Left = 220
      Top = 196
      Width = 98
      Height = 14
      Caption = 'LbContaGerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 23
      Top = 221
      Width = 112
      Height = 14
      Caption = 'Sub-Conta Gerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbnomesubcontagerencial: TLabel
      Left = 220
      Top = 221
      Width = 119
      Height = 14
      Caption = 'LbSubContaGerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 292
      Top = 121
      Width = 166
      Height = 14
      Caption = 'Incluir Meses Subsequentes?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 23
      Top = 71
      Width = 50
      Height = 14
      Caption = 'Cadastro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 23
      Top = 96
      Width = 88
      Height = 14
      Caption = 'Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbNomeCredorDevedor: TLabel
      Left = 220
      Top = 97
      Width = 127
      Height = 14
      Caption = 'lbNomeCredorDevedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 218
      Top = 146
      Width = 131
      Height = 14
      Caption = 'Somat'#243'rio na previs'#227'o?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EdtCodigo: TEdit
      Left = 137
      Top = 18
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object edthistorico: TEdit
      Left = 137
      Top = 44
      Width = 414
      Height = 19
      MaxLength = 100
      TabOrder = 2
    end
    object edtdata: TMaskEdit
      Left = 137
      Top = 118
      Width = 70
      Height = 19
      EditMask = '!99/99/9999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 6
      Text = '  /  /    '
    end
    object edtvalor: TEdit
      Left = 137
      Top = 143
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 9
      OnExit = edtvalorExit
      OnKeyPress = edtvalorKeyPress
    end
    object combodebito_credito: TComboBox
      Left = 337
      Top = 18
      Width = 212
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'D - Contas a Pagar'
        'C - Contas a Receber')
    end
    object edtportador: TEdit
      Left = 137
      Top = 167
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 11
      OnExit = edtportadorExit
      OnKeyDown = edtportadorKeyDown
    end
    object edtcontagerencial: TEdit
      Left = 137
      Top = 191
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 12
      OnExit = edtcontagerencialExit
      OnKeyDown = edtcontagerencialKeyDown
    end
    object edtsubcontagerencial: TEdit
      Left = 137
      Top = 215
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 13
      OnExit = edtsubcontagerencialExit
      OnKeyDown = edtsubcontagerencialKeyDown
    end
    object comboconsiderarmesessubsequentes: TComboBox
      Left = 464
      Top = 118
      Width = 89
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 8
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
    object edtdatalimite: TMaskEdit
      Left = 220
      Top = 118
      Width = 70
      Height = 19
      EditMask = '!99/99/9999;1;_'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 10
      ParentFont = False
      TabOrder = 7
      Text = '  /  /    '
    end
    object combocredordevedor: TComboBox
      Left = 137
      Top = 68
      Width = 145
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 4
      OnChange = combocredordevedorChange
      Items.Strings = (
        'Clientes'
        'Fornecedores'
        'Alunos')
    end
    object combocredordevedorcodigo: TComboBox
      Left = 137
      Top = 68
      Width = 70
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 3
      Visible = False
      OnChange = combocredordevedorcodigoChange
    end
    object edtcodigocredordevedor: TEdit
      Left = 137
      Top = 94
      Width = 70
      Height = 19
      Color = 6073854
      TabOrder = 5
      OnExit = edtcodigocredordevedorExit
      OnKeyDown = edtcodigocredordevedorKeyDown
      OnKeyPress = edtcodigocredordevedorKeyPress
    end
    object combosomatoriaprevisao: TComboBox
      Left = 463
      Top = 143
      Width = 90
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 10
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
  end
end
