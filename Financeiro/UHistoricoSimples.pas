unit UHistoricoSimples;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjHistoricoSimples;

type
  TFhistoricosimples = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function  atualizaQuantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fhistoricosimples: TFhistoricosimples;
  ObjHistoricoSimples:TObjHistoricoSimples;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFhistoricosimples.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjHistoricoSimples do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Nome       ( edtnome.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFhistoricosimples.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjHistoricoSimples do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtnome.text       :=Get_nome       ;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFhistoricosimples.TabelaParaControles: Boolean;
begin
     ObjHistoricoSimples.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFhistoricosimples.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjHistoricoSimples=Nil)
     Then exit;

If (ObjHistoricoSimples.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

ObjHistoricoSimples.free;
end;

procedure TFhistoricosimples.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFhistoricosimples.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFhistoricosimples.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     //edtcodigo.enabled:=False;
     edtcodigo.text:=ObjHistoricoSimples.get_novocodigo;
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;

     ObjHistoricoSimples.status:=dsInsert;
     Edtcodigo.setfocus;
end;

procedure TFhistoricosimples.BtCancelarClick(Sender: TObject);
begin
     ObjHistoricoSimples.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFhistoricosimples.BtgravarClick(Sender: TObject);
begin

     If ObjHistoricoSimples.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjHistoricoSimples.salvar=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFhistoricosimples.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     EdtCodigo.enabled:=False;
     ObjHistoricoSimples.Status:=dsEdit;
     edtnome.setfocus;
end;

procedure TFhistoricosimples.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjHistoricoSimples.Get_pesquisa,ObjHistoricoSimples.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjHistoricoSimples.status<>dsinactive
                                  then exit;

                                  If (ObjHistoricoSimples.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFhistoricosimples.btalterarClick(Sender: TObject);
begin
    If (ObjHistoricoSimples.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFhistoricosimples.btexcluirClick(Sender: TObject);
begin
     If (ObjHistoricoSimples.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjHistoricoSimples.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjHistoricoSimples.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFhistoricosimples.btrelatoriosClick(Sender: TObject);
begin
    ObjHistoricoSimples.imprime;
end;

procedure TFhistoricosimples.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjHistoricoSimples:=TObjHistoricoSimples.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

function TFhistoricosimples.atualizaQuantidade: string;
begin
     Result:='Existem '+ContaRegistros('TABHISTORICOSIMPLES','CODIGO')+' Hist�ricos Cadastrados';
end;

end.
