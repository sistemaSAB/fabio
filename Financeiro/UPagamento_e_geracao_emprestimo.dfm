object FPagamento_e_geracao_emprestimo: TFPagamento_e_geracao_emprestimo
  Left = 199
  Top = 167
  Width = 687
  Height = 532
  Caption = 
    'Pagamento de conta com gera'#231#227'o de conta a receber autom'#225'tico  (E' +
    'MPR'#201'STIMOS)'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object guia: TPageControl
    Left = 0
    Top = 0
    Width = 671
    Height = 494
    ActivePage = GuiaContasaPagar
    Align = alClient
    TabOrder = 0
    OnChange = guiaChange
    object GuiaContasaPagar: TTabSheet
      Caption = '&1 - Quita'#231#227'o'
      object Panel_Gera_Conta_a_Receber: TPanel
        Left = 0
        Top = 0
        Width = 663
        Height = 73
        Align = alTop
        TabOrder = 0
        object Label4: TLabel
          Left = 6
          Top = 20
          Width = 94
          Height = 13
          Caption = 'Credor/Devedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 151
          Top = 20
          Width = 40
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 1
          Top = 1
          Width = 661
          Height = 15
          Align = alTop
          Alignment = taCenter
          Caption = 'Conta a Receber a ser Gerada para'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbNomeCredorDevedorCR: TLabel
          Left = 6
          Top = 57
          Width = 537
          Height = 13
          AutoSize = False
          Caption = 'ascdasuidyauisdy uiay sduioay sdoiuay soiuda'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object comboCredordevedorCR: TComboBox
          Left = 6
          Top = 36
          Width = 139
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          OnChange = comboCredordevedorCRChange
          Items.Strings = (
            'Clientes'
            'Fornecedores'
            'Alunos')
        end
        object edtcodigocredordevedorCR: TEdit
          Left = 151
          Top = 36
          Width = 54
          Height = 21
          Color = clSkyBlue
          TabOrder = 2
          OnExit = edtcodigocredordevedorCRExit
          OnKeyDown = edtcodigocredordevedorCRKeyDown
        end
        object combocredordevedorcodigoCR: TComboBox
          Left = 86
          Top = 35
          Width = 62
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Text = ' '
          Visible = False
        end
      end
      object Panel_Gera_Conta_a_pagar: TPanel
        Left = 0
        Top = 73
        Width = 663
        Height = 393
        Align = alClient
        Caption = 'Panel_Gera_Conta_a_pagar'
        TabOrder = 1
        object Bevel1: TBevel
          Left = 334
          Top = 119
          Width = 326
          Height = 56
          Shape = bsFrame
        end
        object Label1: TLabel
          Left = 6
          Top = 80
          Width = 184
          Height = 13
          Caption = 'Valor do Lan'#231'amento (Opcional)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 198
          Top = 80
          Width = 201
          Height = 13
          Caption = 'Hist'#243'rico do Lan'#231'amento (opcional)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 291
          Top = 19
          Width = 86
          Height = 13
          Caption = 'Data do Lancto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 211
          Top = 19
          Width = 67
          Height = 13
          Caption = 'Vencimento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 151
          Top = 19
          Width = 40
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 6
          Top = 19
          Width = 94
          Height = 13
          Caption = 'Credor/Devedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbValorLancamentoCP: TLabel
          Left = 569
          Top = 125
          Width = 88
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'aaaaaaaaaaa'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object lbpaga: TLabel
          Left = 569
          Top = 141
          Width = 88
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'aaaaaaaaaaa'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object LbSaldoCP: TLabel
          Left = 569
          Top = 157
          Width = 88
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'aaaaaaaaaaa'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object LbNomeCredorDevedorCP: TLabel
          Left = 6
          Top = 57
          Width = 537
          Height = 13
          AutoSize = False
          Caption = 'ascdasuidyauisdy uiay sduioay sdoiuay soiuda'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object combocredordevedorcodigoCP: TComboBox
          Left = 86
          Top = 37
          Width = 62
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Text = ' '
          Visible = False
        end
        object LbtipoCampoCP: TListBox
          Left = 632
          Top = 348
          Width = 27
          Height = 33
          ItemHeight = 13
          TabOrder = 13
          Visible = False
        end
        object edtpesquisa_STRG_GRID_CP: TMaskEdit
          Left = 0
          Top = 360
          Width = 115
          Height = 21
          TabOrder = 12
          Text = 'edtpesquisa_STRG_GRID_CP'
          OnKeyPress = edtpesquisa_STRG_GRID_CPKeyPress
        end
        object strgCP: TStringGrid
          Left = 0
          Top = 177
          Width = 661
          Height = 185
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 11
          OnDblClick = strgCPDblClick
          OnEnter = strgCPEnter
          OnKeyDown = strgCPKeyDown
          OnKeyPress = strgCPKeyPress
          ColWidths = (
            64
            64
            64
            64
            64)
          RowHeights = (
            24
            24
            24
            24
            24)
        end
        object BtSelecionaTodas_CP: TButton
          Left = 0
          Top = 152
          Width = 123
          Height = 24
          Caption = 'Seleciona Todas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
          OnClick = BtSelecionaTodas_CPClick
        end
        object edtvalorlancamentoCP: TMaskEdit
          Left = 6
          Top = 96
          Width = 81
          Height = 21
          TabOrder = 5
          OnExit = edtvalorlancamentoCPExit
          OnKeyPress = edtvalorlancamentoCPKeyPress
        end
        object edtcodigohistoricoCP: TEdit
          Left = 200
          Top = 96
          Width = 27
          Height = 21
          TabOrder = 6
          OnExit = edtcodigohistoricoCPExit
          OnKeyDown = edtcodigohistoricoCPKeyDown
          OnKeyPress = edtcodigohistoricoCPKeyPress
        end
        object edthistoricoCP: TEdit
          Left = 240
          Top = 96
          Width = 416
          Height = 21
          TabOrder = 7
        end
        object btexecutaCP: TBitBtn
          Left = 551
          Top = 57
          Width = 106
          Height = 38
          Caption = '&Executar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
          OnClick = btexecutaCPClick
        end
        object btpesquisaCP: TBitBtn
          Left = 551
          Top = 19
          Width = 106
          Height = 38
          Caption = 'Pesqu&isar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          OnClick = btpesquisaCPClick
        end
        object edtdatalancamentoCP: TMaskEdit
          Left = 291
          Top = 35
          Width = 70
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 4
          Text = '  /  /    '
        end
        object edtvencimentoCP: TMaskEdit
          Left = 211
          Top = 35
          Width = 73
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 3
          Text = '  /  /    '
        end
        object edtcodigocredordevedorcp: TEdit
          Left = 151
          Top = 35
          Width = 54
          Height = 21
          Color = clSkyBlue
          TabOrder = 2
          OnExit = edtcodigocredordevedorcpExit
          OnKeyDown = edtcodigocredordevedorcpKeyDown
        end
        object combocredordevedorCP: TComboBox
          Left = 6
          Top = 35
          Width = 131
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          OnChange = combocredordevedorCPChange
          Items.Strings = (
            'Clientes'
            'Fornecedores'
            'Alunos')
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = '&2 - Explica'#231#227'o'
      ImageIndex = 1
      object RichEdit1: TRichEdit
        Left = 0
        Top = 0
        Width = 663
        Height = 466
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        Lines.Strings = (
          
            '                                                                ' +
            '   SISTEMA DE EMPR'#201'STIMO'
          ''
          
            'Esse M'#243'dulo '#233' respons'#225'vel por lan'#231'ar quita'#231#227'o em uma conta a pag' +
            'ar gerando automaticamente uma conta '
          'a '
          'receber para um cliente/fornecedor no valor do lan'#231'amento. '
          ''
          
            #201' utilizado quando a empresa necessita pagar uma conta de tercei' +
            'ros e depois receber desse '
          'cliente/fornecedor.')
        ParentFont = False
        TabOrder = 0
      end
    end
  end
end
