unit UObjDUPLICATA;
Interface
Uses windows,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,IBQuery
//USES
;

Type
   TObjDUPLICATA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Function  Get_NovoCodigo:string;
                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Historico(parametro: string);
                Function Get_Historico: string;
                PRocedure Submit_ObservacaoPedido(Parametro:String);
                Function Get_ObservacaoPedido:String;
                Function LocalizaPendencia(Parametro:string):string;

                Procedure Submit_Sequencia(parametro:string);
                Function  Get_Sequencia:string;

         Private
               ObjDataset:Tibdataset;
               CODIGO:string;
               Historico:string;
               Sequencia:string;
               ObservacaoPedido:String;


               
//CODIFICA VARIAVEIS PRIVADAS


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjDUPLICATA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Historico:=fieldbyname('Historico').asstring;
        Self.sequencia:=fieldbyname('sequencia').asstring;
        Self.ObservacaoPedido:=fieldbyname('ObservacaoPedido').AsString;
        result:=True;
     End;
end;


Procedure TObjDUPLICATA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('Historico').asstring:=Self.Historico;
        fieldbyname('sequencia').asstring:=Self.sequencia;
        fieldbyname('ObservacaoPedido').AsString:=Self.ObservacaoPedido;
  End;
End;

//***********************************************************************

function TObjDUPLICATA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjDUPLICATA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Historico:='';
        Sequencia:='';
        ObservacaoPedido:='';
     End;
end;

Function TObjDUPLICATA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjDUPLICATA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjDUPLICATA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjDUPLICATA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjDUPLICATA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjDUPLICATA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Historico,sequencia,ObservacaoPedido');
           SelectSQL.ADD(' from  TABDUPLICATA');
           SelectSQL.ADD(' WHERE CODIGO='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjDUPLICATA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjDUPLICATA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjDUPLICATA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

//CODIFICA CRIACAO DE OBJETOS

        ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select CODIGO,Historico,sequencia,ObservacaoPedido');
                SelectSQL.ADD(' from  TABDUPLICATA');
                SelectSQL.ADD(' WHERE CODIGO=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TABDUPLICATA(CODIGO,Historico,sequencia,ObservacaoPedido)');
                InsertSQL.add('values (:CODIGO,:Historico,:sequencia,:ObservacaoPedido)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABDUPLICATA set CODIGO=:CODIGO,Historico=:Historico,sequencia=:sequencia,ObservacaoPedido=:ObservacaoPedido');
                ModifySQL.add('');
                ModifySQL.add('where CODIGO=:CODIGO');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABDUPLICATA where CODIGO=:CODIGO ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,Historico,sequencia,ObservacaoPedido');
                RefreshSQL.ADD(' from  TABDUPLICATA');
                RefreshSQL.ADD(' WHERE CODIGO=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjDUPLICATA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjDUPLICATA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabDUPLICATA');
     Result:=Self.ParametroPesquisa;
end;

function TObjDUPLICATA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Duplicata ';
end;




destructor TObjDUPLICATA.Free;
begin
  Freeandnil(Self.ObjDataset);
  Freeandnil(Self.ParametroPesquisa);
  //CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjDUPLICATA.RetornaCampoCodigo: string;
begin
      result:='CODIGO';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjDUPLICATA.RetornaCampoNome: string;
begin
      result:='HISTORICO';
//CODIFICA RETORNACAMPONOME

end;

procedure TOBJDUPLICATA.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TOBJDUPLICATA.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TOBJDUPLICATA.Submit_Historico(parametro: string);
begin
        Self.Historico:=Parametro;
end;
function TOBJDUPLICATA.Get_Historico: string;
begin
        Result:=Self.Historico;
end;
//CODIFICA GETSESUBMITS

//CODIFICA EXITONKEYDOWN



function TObjDUPLICATA.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_DUPLICATA';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
           result:='0';
           exit;

        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;

function TObjDUPLICATA.Get_Sequencia: string;
begin
     Result:=Self.Sequencia;
end;

procedure TObjDUPLICATA.Submit_Sequencia(parametro: string);
begin
    Self.Sequencia:=parametro;
end;

function TObjDUPLICATA.Get_ObservacaoPedido: String;
begin
    Result:=Self.CODIGO;
end;

procedure TObjDUPLICATA.Submit_ObservacaoPedido(Parametro: String);
begin
    Self.ObservacaoPedido:=Parametro;
end;

function TObjDUPLICATA.LocalizaPendencia(parametro:string):string;
var
    Query:TIBQuery;
begin
     result:='';
     query:=TIBQuery.Create(nil);
     query.Database:=FDataModulo.IBDatabase;
     try
           with Query do
           begin
               Close;
               sql.Clear;
               sql.add('select duplicata from tabpendencia');
               sql.Add('where codigo='+parametro);
               Open;
               Result:=fieldbyname('duplicata').AsString;
           end;


     finally
           FreeAndNil(Query);
     end;


end;

end.


