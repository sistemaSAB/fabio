unit UARQUIVOREMESSARETORNO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjARQUIVOREMESSARETORNO,
  jpeg;

type
  TFARQUIVOREMESSARETORNO = class(TForm)
    Guia: TTabbedNotebook;
    panelbotes: TPanel;
    painel_imagem: TPanel;
    imcesta: TImage;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    ImagemFundo: TImage;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbAquivoRemessa: TLabel;
    EdtArquivoRemessa: TEdit;
    LbAquivoRetorno: TLabel;
    EdtArquivoRetorno: TEdit;
    LbData: TLabel;
    EdtData: TMaskEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjARQUIVOREMESSARETORNO:TObjARQUIVOREMESSARETORNO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FARQUIVOREMESSARETORNO: TFARQUIVOREMESSARETORNO;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFARQUIVOREMESSARETORNO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjARQUIVOREMESSARETORNO do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_ArquivoRemessa(edtArquivoRemessa.text);
        Submit_ArquivoRetorno(edtArquivoRetorno.text);
        Submit_Data(edtData.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFARQUIVOREMESSARETORNO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjARQUIVOREMESSARETORNO do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtArquivoRemessa.text:=Get_ArquivoRemessa;
        EdtArquivoRetorno.text:=Get_ArquivoRetorno;
        EdtData.text:=Get_Data;
     
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFARQUIVOREMESSARETORNO.TabelaParaControles: Boolean;
begin
     If (Self.ObjARQUIVOREMESSARETORNO.TabelaparaObjeto(false)=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFARQUIVOREMESSARETORNO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjARQUIVOREMESSARETORNO=Nil)
     Then exit;

     If (Self.ObjARQUIVOREMESSARETORNO.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjARQUIVOREMESSARETORNO.free;
end;

procedure TFARQUIVOREMESSARETORNO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFARQUIVOREMESSARETORNO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjARQUIVOREMESSARETORNO.status:=dsInsert;
     Guia.pageindex:=0;
     edtArquivoRemessa.setfocus;

end;


procedure TFARQUIVOREMESSARETORNO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjARQUIVOREMESSARETORNO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjARQUIVOREMESSARETORNO.Status:=dsEdit;
                guia.pageindex:=0;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtArquivoRemessa.setfocus;
    End;                                   

end;

procedure TFARQUIVOREMESSARETORNO.btgravarClick(Sender: TObject);
begin

     If Self.ObjARQUIVOREMESSARETORNO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjARQUIVOREMESSARETORNO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjARQUIVOREMESSARETORNO.Get_codigo;
     Self.ObjARQUIVOREMESSARETORNO.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFARQUIVOREMESSARETORNO.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjARQUIVOREMESSARETORNO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjARQUIVOREMESSARETORNO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjARQUIVOREMESSARETORNO.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFARQUIVOREMESSARETORNO.btcancelarClick(Sender: TObject);
begin
     Self.ObjARQUIVOREMESSARETORNO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFARQUIVOREMESSARETORNO.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFARQUIVOREMESSARETORNO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjARQUIVOREMESSARETORNO.Get_pesquisa,Self.ObjARQUIVOREMESSARETORNO.Get_TituloPesquisa,Nil)
=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjARQUIVOREMESSARETORNO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjARQUIVOREMESSARETORNO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjARQUIVOREMESSARETORNO.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFARQUIVOREMESSARETORNO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFARQUIVOREMESSARETORNO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

          limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjARQUIVOREMESSARETORNO:=TObjARQUIVOREMESSARETORNO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

