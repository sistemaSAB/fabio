object FrelPrevisaoFinanceira: TFrelPrevisaoFinanceira
  Left = 243
  Top = 173
  Width = 1032
  Height = 748
  Caption = 'M'#243'dulo de Previs'#227'o Financeira'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TPageControl
    Left = 0
    Top = 0
    Width = 1016
    Height = 690
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '&1-Di'#225'rio'
      object StrgPrevisao: TStringGrid
        Left = 0
        Top = 164
        Width = 1008
        Height = 394
        Align = alClient
        Ctl3D = False
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        ParentCtl3D = False
        TabOrder = 0
        OnEnter = StrgPrevisaoEnter
        OnKeyDown = StrgPrevisaoKeyDown
        OnKeyPress = StrgPrevisaoKeyPress
      end
      object PaneIRodape: TPanel
        Left = 0
        Top = 558
        Width = 1008
        Height = 104
        Align = alBottom
        TabOrder = 1
        object memorodape: TMemo
          Left = 1
          Top = 1
          Width = 1006
          Height = 102
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            'memorodape')
          ParentFont = False
          TabOrder = 0
        end
      end
      object PanelSuperior: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 164
        Align = alTop
        TabOrder = 2
        DesignSize = (
          1008
          164)
        object Label1: TLabel
          Left = 4
          Top = 53
          Width = 70
          Height = 14
          Caption = 'Portador(es)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 142
          Top = 53
          Width = 141
          Height = 14
          Caption = 'Excluir Conta(s) Gerencial'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 142
          Top = 8
          Width = 61
          Height = 14
          Caption = 'Data Limite'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label4: TLabel
          Left = 4
          Top = 8
          Width = 31
          Height = 14
          Caption = 'Inicial'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 302
          Top = 54
          Width = 166
          Height = 14
          Caption = 'Excluir Sub-Conta(s) Gerencial'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label6: TLabel
          Left = 478
          Top = 54
          Width = 77
          Height = 14
          Caption = 'Excluir T'#237'tulos'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdtPortador: TEdit
          Left = 4
          Top = 69
          Width = 121
          Height = 21
          TabOrder = 2
          Text = 'EdtPortador'
          OnKeyDown = EdtPortadorKeyDown
        end
        object EdtExcluirContasGerenciais: TEdit
          Left = 142
          Top = 69
          Width = 121
          Height = 21
          TabOrder = 3
          Text = 'Edit1'
          OnKeyDown = EdtExcluirContasGerenciaisKeyDown
        end
        object edtdatalimite: TMaskEdit
          Left = 142
          Top = 24
          Width = 120
          Height = 21
          EditMask = '99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 1
          Text = '  /  /    '
        end
        object edtdatainicial: TMaskEdit
          Left = 4
          Top = 24
          Width = 120
          Height = 21
          EditMask = '99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
        end
        object edtexcluirsubconta: TEdit
          Left = 302
          Top = 68
          Width = 121
          Height = 21
          TabOrder = 4
          Text = 'edtexcluirsubconta'
          OnKeyDown = edtexcluirsubcontaKeyDown
        end
        object MemoCabecalho: TMemo
          Left = 1
          Top = 96
          Width = 1006
          Height = 67
          Align = alBottom
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            
              '123456789X123456789X123456789X123456789X123456789X123456789X1234' +
              '56789X123456789X123456789X')
          ParentFont = False
          TabOrder = 5
        end
        object btGerarDiario: TButton
          Left = 892
          Top = 70
          Width = 121
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = '&Gerar'
          TabOrder = 6
          OnClick = btGerarDiarioClick
        end
        object edtexcluititulo: TEdit
          Left = 478
          Top = 68
          Width = 121
          Height = 21
          TabOrder = 7
          OnKeyDown = edtexcluitituloKeyDown
        end
        object btsalvarcsv: TButton
          Left = 893
          Top = 44
          Width = 121
          Height = 25
          Anchors = [akRight, akBottom]
          Caption = '&Salvar CSV'
          TabOrder = 8
          OnClick = btsalvarcsvClick
        end
      end
      object edtpesquisa: TMaskEdit
        Left = 6
        Top = 582
        Width = 121
        Height = 21
        TabOrder = 3
        Visible = False
        OnKeyPress = edtpesquisaKeyPress
      end
      object lbtipocoluna: TListBox
        Left = 813
        Top = 422
        Width = 33
        Height = 33
        ItemHeight = 13
        TabOrder = 4
        Visible = False
      end
    end
    object TabSheet2: TTabSheet
      Caption = '&2-Mensal'
      ImageIndex = 1
      object StrgPrevisaoMensal: TStringGrid
        Left = 0
        Top = 59
        Width = 1008
        Height = 603
        TabStop = False
        Align = alClient
        Color = clWhite
        Ctl3D = False
        FixedColor = clWhite
        FixedCols = 0
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
        ParentCtl3D = False
        TabOrder = 0
        OnDrawCell = StrgPrevisaoMensalDrawCell
        OnKeyDown = StrgPrevisaoMensalKeyDown
        OnKeyPress = StrgPrevisaoMensalKeyPress
        ColWidths = (
          64
          64
          64
          64
          64)
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 59
        Align = alTop
        TabOrder = 1
        object lbdatas: TLabel
          Left = 512
          Top = 32
          Width = 49
          Height = 13
          Caption = 'LBDATAS'
        end
        object BtGerarmensal: TButton
          Left = 7
          Top = 27
          Width = 121
          Height = 25
          Caption = '&Gerar'
          TabOrder = 1
          OnClick = BtGerarmensalClick
        end
        object btsalvardados: TButton
          Left = 373
          Top = 27
          Width = 121
          Height = 25
          Caption = '&Salvar Dados CSV'
          TabOrder = 4
          OnClick = btsalvardadosClick
        end
        object memosalvadados: TMemo
          Left = 1064
          Top = 8
          Width = 185
          Height = 89
          Lines.Strings = (
            'memosalvadados')
          TabOrder = 5
          Visible = False
        end
        object CheckPrevisao: TCheckBox
          Left = 11
          Top = 8
          Width = 161
          Height = 17
          Caption = 'Incluir T'#237'tulos de Previs'#227'o?'
          TabOrder = 0
        end
        object btatualizasaldo: TButton
          Left = 129
          Top = 27
          Width = 121
          Height = 25
          Caption = '&Atualiza Saldos'
          TabOrder = 2
          OnClick = btatualizasaldoClick
        end
        object btadicionalinha: TBitBtn
          Left = 251
          Top = 27
          Width = 121
          Height = 25
          Caption = '+ &Linha'
          TabOrder = 3
          OnClick = btadicionalinhaClick
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = '&3-Realizado'
      ImageIndex = 2
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 59
        Align = alTop
        TabOrder = 0
        object lbdatasrealizado: TLabel
          Left = 512
          Top = 32
          Width = 49
          Height = 13
          Caption = 'LBDATAS'
        end
        object BTGerar_realizado: TButton
          Left = 7
          Top = 27
          Width = 121
          Height = 25
          Caption = '&Gerar'
          TabOrder = 0
          OnClick = BTGerar_realizadoClick
        end
        object Btsalvadados_realizado: TButton
          Left = 373
          Top = 27
          Width = 121
          Height = 25
          Caption = '&Salvar Dados CSV'
          TabOrder = 3
          OnClick = Btsalvadados_realizadoClick
        end
        object Memo1: TMemo
          Left = 1064
          Top = 8
          Width = 185
          Height = 89
          Lines.Strings = (
            'memosalvadados')
          TabOrder = 4
          Visible = False
        end
        object BTAtualizaSaldo_Realizado: TButton
          Left = 129
          Top = 27
          Width = 121
          Height = 25
          Caption = '&Atualiza Saldos'
          TabOrder = 1
          OnClick = BTAtualizaSaldo_RealizadoClick
        end
        object btadicionalinha_realizado: TBitBtn
          Left = 251
          Top = 27
          Width = 121
          Height = 25
          Caption = '+ &Linha'
          TabOrder = 2
          OnClick = btadicionalinha_realizadoClick
        end
      end
      object STRG_Realizado: TStringGrid
        Left = 0
        Top = 59
        Width = 1008
        Height = 603
        TabStop = False
        Align = alClient
        Color = clWhite
        Ctl3D = False
        FixedColor = clWhite
        FixedCols = 0
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
        ParentCtl3D = False
        TabOrder = 1
        OnDrawCell = StrgPrevisaoMensalDrawCell
        OnKeyDown = STRG_RealizadoKeyDown
        OnKeyPress = StrgPrevisaoMensalKeyPress
        ColWidths = (
          64
          64
          64
          64
          64)
      end
    end
  end
  object barrastatus: TPanel
    Left = 0
    Top = 690
    Width = 1016
    Height = 20
    Align = alBottom
    Alignment = taLeftJustify
    Caption = 'barrastatus'
    TabOrder = 1
  end
  object SaveDialog: TSaveDialog
    Left = 616
    Top = 88
  end
  object QueryRealizado: TIBQuery
    Left = 604
    Top = 48
  end
end
