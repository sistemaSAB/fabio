object FGeraTitulo: TFGeraTitulo
  Left = 274
  Top = 252
  Width = 691
  Height = 346
  Caption = 'Cadastro de Geradores de T'#237'tulo - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 675
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 572
      Top = 0
      Width = 103
      Height = 51
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Gerador de T'#237'tulo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btOpcoes: TBitBtn
      Left = 352
      Top = -1
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
    end
    object BtCancelar: TBitBtn
      Left = 152
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btsair: TBitBtn
      Left = 402
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object btexcluir: TBitBtn
      Left = 202
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object Btgravar: TBitBtn
      Left = 102
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object btalterar: TBitBtn
      Left = 52
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btrelatorios: TBitBtn
      Left = 302
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object BtNovo: TBitBtn
      Left = 2
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btpesquisar: TBitBtn
      Left = 252
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 258
    Width = 675
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    DesignSize = (
      675
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 675
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 377
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 846
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 846
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 51
    Width = 675
    Height = 207
    Align = alClient
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 673
      Height = 205
      Align = alClient
      Stretch = True
    end
    object Label1: TLabel
      Left = 19
      Top = 19
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 19
      Top = 42
      Width = 49
      Height = 14
      Caption = 'Hist'#243'rico'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 19
      Top = 88
      Width = 45
      Height = 14
      Caption = 'Gerador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 19
      Top = 111
      Width = 87
      Height = 14
      Caption = 'C'#243'digo Gerador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 19
      Top = 134
      Width = 88
      Height = 14
      Caption = 'Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 19
      Top = 157
      Width = 130
      Height = 14
      Caption = 'C'#243'digo Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label7: TLabel
      Left = 322
      Top = 88
      Width = 31
      Height = 14
      Caption = 'Prazo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 322
      Top = 111
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 322
      Top = 134
      Width = 87
      Height = 14
      Caption = 'Conta Gerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label10: TLabel
      Left = 322
      Top = 157
      Width = 112
      Height = 14
      Caption = 'Sub-Conta Gerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label11: TLabel
      Left = 322
      Top = 19
      Width = 59
      Height = 14
      Caption = 'Conta F'#225'cil'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label12: TLabel
      Left = 19
      Top = 65
      Width = 118
      Height = 14
      Caption = 'Hist'#243'rico - Conta F'#225'cil'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label13: TLabel
      Left = 38
      Top = 178
      Width = 168
      Height = 14
      Caption = 'Mostra Sub-Conta em Branco?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCodigo: TEdit
      Left = 155
      Top = 17
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object edthistorico: TEdit
      Left = 155
      Top = 40
      Width = 410
      Height = 19
      TabOrder = 2
    end
    object edtgerador: TEdit
      Left = 155
      Top = 86
      Width = 155
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 4
      OnKeyDown = edtgeradorKeyDown
    end
    object edtcodigogerador: TEdit
      Left = 155
      Top = 109
      Width = 155
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 6
      OnKeyDown = edtcodigogeradorKeyDown
    end
    object edtcredordevedor: TEdit
      Left = 155
      Top = 132
      Width = 155
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 8
      OnKeyDown = edtcredordevedorKeyDown
    end
    object edtcodigocredordevedor: TEdit
      Left = 155
      Top = 154
      Width = 155
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 10
      OnKeyDown = edtcodigocredordevedorKeyDown
    end
    object edtprazo: TEdit
      Left = 442
      Top = 86
      Width = 123
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 5
      OnKeyDown = edtprazoKeyDown
    end
    object edtportador: TEdit
      Left = 442
      Top = 109
      Width = 123
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 7
      OnKeyDown = edtportadorKeyDown
    end
    object edtcontagerencial: TEdit
      Left = 442
      Top = 132
      Width = 123
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 9
      OnKeyDown = edtcontagerencialKeyDown
    end
    object edtsubcontagerencial: TEdit
      Left = 442
      Top = 154
      Width = 123
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 11
      OnExit = edtsubcontagerencialExit
      OnKeyDown = edtsubcontagerencialKeyDown
    end
    object combocontafacil: TComboBox
      Left = 442
      Top = 17
      Width = 123
      Height = 19
      BevelKind = bkSoft
      Style = csOwnerDrawFixed
      ItemHeight = 13
      TabOrder = 1
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
    object edthistoricocontafacil: TEdit
      Left = 155
      Top = 63
      Width = 410
      Height = 19
      MaxLength = 200
      TabOrder = 3
    end
    object ChMostraSubConta_ContaFacil: TCheckBox
      Left = 19
      Top = 178
      Width = 13
      Height = 13
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 12
    end
  end
end
