unit UComprovantePagamentoTMP;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask;

type
  TFcomprovantepagamentoTMP = class(TForm)
    edtpagoa: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtnominador: TEdit;
    edtdescricao: TEdit;
    Label5: TLabel;
    Label6: TLabel;
    edtcomprovante: TEdit;
    edtprovidencias: TEdit;
    Label7: TLabel;
    edtdata: TMaskEdit;
    Label8: TLabel;
    edtnomerecebedor: TEdit;
    edtcpfcnpjrecebedor: TEdit;
    Label10: TLabel;
    Btsair: TBitBtn;
    Label1: TLabel;
    Label9: TLabel;
    edtcidade: TEdit;
    Label2: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure BtsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FcomprovantepagamentoTMP: TFcomprovantepagamentoTMP;

implementation

uses UessencialGlobal;

{$R *.DFM}

procedure TFcomprovantepagamentoTMP.FormActivate(Sender: TObject);
begin
     PegaFiguraBotao(btsair,'botaosair.bmp');
     edtpagoa.setfocus;
end;

procedure TFcomprovantepagamentoTMP.BtsairClick(Sender: TObject);
begin
     Close;
end;

procedure TFcomprovantepagamentoTMP.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFcomprovantepagamentoTMP.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
end;

end.
