unit UimpReciboLancamento;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls;

type
  TFimpReciboLancamento = class(TForm)
    QR: TQuickRep;
    QRBand1: TQRBand;
    QRImage1: TQRImage;
    lbtitulo: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    correspondente1: TQRLabel;
    correspondente2: TQRLabel;
    cidade: TQRLabel;
    dia: TQRLabel;
    mes: TQRLabel;
    ano: TQRLabel;
    recebi: TQRLabel;
    quantia2: TQRLabel;
    quantia1: TQRLabel;
    valor: TQRLabel;
    QRShape1: TQRShape;
    QRLabel10: TQRLabel;
    CODIGO: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    procedure QRBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FimpReciboLancamento: TFimpReciboLancamento;

implementation

uses UessencialGlobal;

{$R *.DFM}

procedure TFimpReciboLancamento.QRBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
pegafigura(qrimage1,'Logo200x52x24b.jpg');
end;

procedure TFimpReciboLancamento.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
end;

end.
