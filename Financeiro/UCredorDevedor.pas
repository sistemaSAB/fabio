unit UCredorDevedor;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjCredorDevedor;

type
  TFcredordevedor = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    BtCancelar: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    Btgravar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    btpesquisar: TBitBtn;
    BtNovo: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    label25: TLabel;
    label26: TLabel;
    label27: TLabel;
    label28: TLabel;
    label29: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    edttabela: TEdit;
    edtobjeto: TEdit;
    edtinstrucaosql: TEdit;
    edtcamponome: TEdit;
    edtnomeformulario: TEdit;
    edtcampocontabil: TEdit;
    edtcamporazaosocial: TEdit;
    edtcampocpfcnpj: TEdit;
    edtcamporgie: TEdit;
    edtcampocontagerencial: TEdit;
    edttelefone: TEdit;
    edtendereco: TEdit;
    edtbairro: TEdit;
    edtcampocep: TEdit;
    edtcampoestado: TEdit;
    edtcampocidade: TEdit;
    edtcelular: TEdit;
    EdtCamponumeroCasa: TEdit;
    edtcampocredito: TEdit;
    edtTelefoneCobranca: TEdit;
    edtEnderecocobranca: TEdit;
    edtNumeroCasacobranca: TEdit;
    edtBairrocobranca: TEdit;
    edtCEPCobranca: TEdit;
    edtEstadoCobranca: TEdit;
    edtCidadeCobranca: TEdit;
    edtCelularCobranca: TEdit;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function  atualizaQuantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fcredordevedor: TFcredordevedor;
  ObjCredorDevedor:TObjCredorDevedor;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFcredordevedor.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjCredorDevedor do
    Begin
      Submit_CODIGO             (edtCODIGO        .text);
      Submit_Nome               (edtNome          .text);
      Submit_Tabela             (edtTabela        .text);
      Submit_Objeto             (edtObjeto        .text);
      Submit_InstrucaoSQL       (edtInstrucaoSQL  .text);
      Submit_CampoNome          (edtcamponome     .text);
      Submit_NomeFormulario     (edtnomeformulario.text);
      Submit_CampoContabil      (edtcampocontabil .text);
      Submit_CampoRazaoSocial   (edtcamporazaosocial.text);
      Submit_CAMPOCPFCNPJ       (edtcampocpfcnpj.text);
      Submit_CAMPORGIE          (edtcamporgie.text);
      Submit_CampoContaGerencial(edtcampocontagerencial.text);
      Submit_endereco           (edtendereco.text);
      Submit_CampoNumeroCasa    (edtcamponumerocasa.text);
      Submit_bairro             (edtbairro.text);
      Submit_telefone           (edttelefone.text);
      Submit_CampoCep(edtcampocep.text);
      Submit_campoestado(edtcampoestado.text);
      Submit_Campocidade(edtcampocidade.text);
      Submit_Celular(edtcelular.text);
      Submit_CampoCredito(edtcampocredito.Text);

      Submit_EnderecoCobranca           (edtEnderecocobranca.text);
      Submit_CampoNumeroCasaCobranca    (edtNumeroCasacobranca.text);
      Submit_BairroCobranca             (edtBairrocobranca.text);
      Submit_TelefoneCobranca           (edtTelefoneCobranca.text);
      Submit_CampoCepCobranca           (edtCEPCobranca.text);
      Submit_CampoEstadoCobranca        (edtEstadoCobranca.text);
      Submit_CampoCidadeCobranca        (edtCidadeCobranca.text);
      Submit_CelularCobranca            (edtCelularCobranca.text);

      result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFcredordevedor.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjCredorDevedor do
     Begin
        edtCODIGO      .text            :=Get_CODIGO            ;
        edtNome        .text            :=Get_Nome              ;
        edtTabela      .text            :=Get_Tabela            ;
        edtObjeto      .text            :=Get_Objeto            ;
        edtInstrucaoSQL.text            :=Get_InstrucaoSQL      ;
        edtcamponome   .text            :=Get_CampoNome         ;
        edtnomeformulario.text          :=Get_NomeFormulario    ;
        edtcampocontabil.text           :=Get_CampoContabil     ;
        edtcamporazaosocial.text        :=Get_CampoRazaoSocial  ;
        edtcampocpfcnpj.text            :=Get_CAMPOCPFCNPJ      ;
        edtcamporgie.text               :=Get_CAMPORGIE         ;
        edtcampocontagerencial.text     :=Get_CampoContaGerencial;
        edttelefone.text                :=Get_telefone;
        edtbairro.text                  :=Get_bairro;
        edtendereco.text                :=Get_endereco;
        edtcamponumerocasa.text         :=Get_CampoNumeroCasa;
        edtcampocep.text                :=Get_CampoCep;
        edtcampoestado.text             :=Get_campoEstado;
        edtcampocidade.text             :=Get_campoCidade;
        edtcelular.text                 :=Get_Celular;

        edtTelefoneCobranca.text        :=Get_TelefoneCobranca;
        edtBairrocobranca.text          :=Get_BairroCobranca;
        edtEnderecocobranca.text        :=Get_EnderecoCobranca;
        edtNumeroCasacobranca.text      :=Get_CampoNumeroCasaCobranca;
        edtCEPCobranca.text             :=Get_CampoCepCobranca;
        edtEstadoCobranca.text          :=Get_CampoEstadoCobranca;
        edtCidadeCobranca.text          :=Get_CampoCidadeCobranca;
        edtCelularCobranca.text         :=Get_CelularCobranca;

        edtcampocredito.Text:=Get_CampoCredito;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFcredordevedor.TabelaParaControles: Boolean;
begin
     ObjCredorDevedor.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFcredordevedor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCredorDevedor=Nil)
     Then exit;

    If (ObjCredorDevedor.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjCredorDevedor.free;
end;

procedure TFcredordevedor.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFcredordevedor.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFcredordevedor.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjCredorDevedor.Get_NovoCodigo;
     edtcodigo.enabled:=False;
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.Visible:=True;

     ObjCredorDevedor.status:=dsInsert;
     Edtnome.setfocus;
end;

procedure TFcredordevedor.BtCancelarClick(Sender: TObject);
begin
     ObjCredorDevedor.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFcredordevedor.BtgravarClick(Sender: TObject);
begin

     If ObjCredorDevedor.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCredorDevedor.salvar=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFcredordevedor.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     ObjCredorDevedor.Status:=dsEdit;
     edtNome.setfocus;

end;

procedure TFcredordevedor.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCredorDevedor.Get_pesquisa,ObjCredorDevedor.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCredorDevedor.status<>dsinactive
                                  then exit;

                                  If (ObjCredorDevedor.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFcredordevedor.btalterarClick(Sender: TObject);
begin
    If (ObjCredorDevedor.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFcredordevedor.btexcluirClick(Sender: TObject);
begin
     If (ObjCredorDevedor.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjCredorDevedor.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjCredorDevedor.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFcredordevedor.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjCredorDevedor:=TObjCredorDevedor.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

function TFcredordevedor.atualizaQuantidade: string;
begin
     result:='Existem '+ContaRegistros('TABCREDORDEVEDOR','CODIGO')+' Credores/Devedores Cadastrados'; 
end;

end.
