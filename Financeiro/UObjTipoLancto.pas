unit UObjTipoLancto;
Interface
Uses Classes,Ibcustomdataset,IBStoredProc,Db,UessencialGlobal,UObjPlanodeContas;

Type
   TObjTipoLancto=class
          Public
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ObjPlanodeContas                            :Tobjplanodecontas;
                

                Function    Salvar                          :Boolean;
                Procedure   TabelaparaObjeto                        ;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string)            :Boolean;
                Procedure   ZerarTabela;
                Procedure  Cancelar;
                Procedure  Commit;
                Function   Get_Pesquisa                         :string;overload;
                Function   Get_Pesquisa(parametro:string)        :string;overload;
                Function   Get_TituloPesquisa                   :string;
                Function   Get_PesquisaNaoGeraValor             :string;
                Function  Get_pesquisaPlanodeContas             :TstringList;
                Function   Get_TituloPesquisaPlanodeContas      :string;
                Constructor Create;
                Destructor Free;

                Function Get_CODIGO           :string;
                Function Get_Nome             :string;
                Function Get_Tipo             :string;
                Function Get_GeraValor        :string;
                Function Get_Classificacao    :string;

                Procedure Submit_CODIGO         (parametro:string);
                Procedure Submit_NoME           (parametro:string);
                Procedure Submit_Tipo           (parametro:string);
                Procedure Submit_GeraValor      (parametro:string);
                Procedure Submit_Classificacao  (parametro:string);
                function Get_NovoCodigo: string;


         Private
               ObjDataset:Tibdataset;

               CODIGO           :String[09];
               NOME             :String[40];
               Tipo             :String[01];
               GeraValor        :String[01];
               //esse campo teve que ser adicionado, pois quando precisava
               //fazer relatorios que totalizasse o que foi recebido, dava pau,
               //porque havia varios tiposlanctos de quitacao e alguns nem geravam
               //valor, desta forma eu tinha que totalizar por tipo e naum por quitacao
               //com esse campo o que for juro eu coloco 'J', o que for quitacao 'Q'
               //e o que for desconto 'D'
               Classificacao    :string;




                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses SysUtils,Dialogs,UDatamodulo;


{ TTabTitulo }


Procedure  TObjTipoLancto.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.NOME             :=FieldByName('NOME').asstring;
        Self.Tipo             :=FieldByName('Tipo').asstring;
        Self.GeraValor        :=FieldByName('GeraValor').asstring;
        Self.Classificacao    :=FieldByName('Classificacao').asstring;
     End;
end;


Procedure TObjTipoLancto.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring    := Self.CODIGO           ;
      FieldByName('NOME').asstring      :=Self.NOME              ;
      FieldByName('Tipo').asstring      :=Self.Tipo              ;
      FieldByName('GeraValor').asstring :=Self.GeraValor         ;
      FieldByName('Classificacao').asstring:=Self.Classificacao  ;
  End;
End;

//***********************************************************************

function TObjTipoLancto.Salvar: Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  If Self.LocalizaCodigo(Self.CODIGO)=False
  Then Begin
              if(Self.Status=dsedit)
              Then Begin
                        Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                        result:=False;
                        exit;
                   End
       End
  Else Begin
            If (Self.Status=dsinsert)
            Then Begin
                        Messagedlg('O registro j� est� cadastrado!',mterror,[mbok],0);
                        result:=False;
                        exit;
                 End;
       End;


if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;
 FDataModulo.IBTransaction.CommitRetaining;
 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjTipoLancto.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Nome             :='';
        Self.Tipo             :='';
        Self.GeraValor        :='';

     End;
end;

Function TObjTipoLancto.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (nome='')
       Then Mensagem:=mensagem+'/Nome';
       If (Tipo='')
       Then Mensagem:=mensagem+'/Tipo';
       If (GeraValor='')
       Then Mensagem:=mensagem+'/Gera Valor';

       If (Classificacao='')
       Then Mensagem:=Mensagem+'/Classifica��o';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjTipoLancto.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     Try
        {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
        Then Mensagem:='/ Curso n�o Encontrado!'
        Else Begin
                  Self.CODCURSO.TabelaparaObjeto;
                  If Self.CODCURSO.Get_Ativo='N'
                  Then Mensagem:='/Situa��o do Curso Inv�lida para a Turma!';
             End;}

        If (mensagem<>'')
        Then Begin
                  Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
                  result:=False;
                  exit;
             End;
        result:=true;
     Finally

     End;
End;

function TObjTipoLancto.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     Try
        Inteiros:=Strtoint(Self.Codigo);

     Except
           mensagem:=mensagem+'/C�digo';
     End;



     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjTipoLancto.VerificaData: Boolean;
var
Datas:Tdatetime;
Mensagem:string;
begin
     mensagem:='';

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m datas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjTipoLancto.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     If (Self.Tipo<>'D')and(Self.Tipo<>'C')
     Then Mensagem:=mensagem+'/Tipo de Lan�amento Inv�lido';

     If (Self.GeraValor<>'S')and(Self.GeraValor<>'N')
     Then Mensagem:=mensagem+'/Gera Valor Inv�lido';

     If ((Self.Classificacao<>'Q')and(Self.Classificacao<>'J') and (Self.Classificacao<>'D'))
     Then Mensagem:=mensagem+'/Classifica��o Inv�lida';

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjTipoLancto.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,tipo,geravalor,classificacao from tabTipoLancto where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjTipoLancto.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjTipoLancto.Exclui(Pcodigo: string): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjTipoLancto.create;
begin

        ZerarTabela;
        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ObjPlanodeContas:=Tobjplanodecontas.create;

        //Self.ObjDatasource:=TDataSource.Create(nil);
        //Self.CodCurso:=TObjCursos.create;



        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,nome,tipo,geravalor,classificacao  from tabTipoLancto where codigo=0');


                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' insert into tabTipoLancto (codigo,nome,tipo,geravalor,classificacao) values (:codigo,:nome,:tipo,:geravalor,:classificacao)');

                ModifySQL.clear;
                ModifySQL.add('  Update TabTipoLancto set codigo=:codigo,nome=:nome,tipo=:tipo,geravalor=:geravalor,classificacao=:classificacao where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add('delete from tabTipoLancto where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,nome,tipo,geravalor,classificacao from tabTipoLancto where codigo=1');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;

//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjTipoLancto.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjTipoLancto.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;

procedure TObjTipoLancto.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjTipoLancto.Get_Pesquisa: string;
begin
     Result:='Select * from TabTipoLancto';
end;

function TObjTipoLancto.Get_TituloPesquisa: string;
begin
     Result:='Pesquisa de Tipos de Lan�amentos ';
end;

function TObjTipoLancto.Get_NOme: string;
begin
     Result:=Self.Nome;
end;

procedure TObjTipoLancto.Submit_NOME(parametro: string);
begin
     Self.nome:=parametro;
end;

function TObjTipoLancto.Get_Pesquisa(parametro: string): string;
begin
     result:=Self.Get_Pesquisa+'  where codigo= '+parametro;
end;

function TObjTipoLancto.Get_Tipo: string;
begin
     Result:=Self.tipo;
end;

procedure TObjTipoLancto.Submit_Tipo(parametro: string);
begin
     Self.tipo:=parametro;
end;

function TObjTipoLancto.Get_GeraValor: string;
begin
     Result:=Self.GeraValor;
end;

procedure TObjTipoLancto.Submit_GeraValor(parametro: string);
begin
     Self.GeraValor:=parametro;
end;

function TObjTipoLancto.Get_PesquisaNaoGeraValor: string;
begin
    Result:='Select * from TabTipoLancto where geravalor=''N'' ';
end;

destructor TObjTipoLancto.Free;
begin
    freeandnil(ObjDataset);
    Self.ObjPlanodeContas.free;
end;



function TObjTipoLancto.Get_pesquisaPlanodeContas: TstringList;
begin
     Result:=Self.objplanodecontas.get_pesquisa;
end;

function TObjTipoLancto.Get_TituloPesquisaPlanodeContas: string;
begin
     Result:=Self.objplanodecontas.get_titulopesquisa;
end;

function TObjTipoLancto.Get_Classificacao: string;
begin
     Result:=Self.Classificacao;
end;

procedure TObjTipoLancto.Submit_Classificacao(parametro: string);
begin
     Self.Classificacao:=parametro;
end;

function TObjTipoLancto.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_TIPOLANCTO';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
           result:='0';
           exit;

        End;
     Finally
            freeandnil(StrTemp);
     End;


end;

end.

