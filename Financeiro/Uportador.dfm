object Fportador: TFportador
  Left = 591
  Top = 159
  Width = 843
  Height = 600
  Caption = 'Cadastro de Portadores  - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotoes: TPanel
    Left = 0
    Top = 0
    Width = 827
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      827
      49)
    object lbnomeformulario: TLabel
      Left = 464
      Top = 0
      Width = 145
      Height = 32
      Caption = 'Portadores'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 631
      Top = 0
      Width = 116
      Height = 32
      Align = alCustom
      Alignment = taRightJustify
      Caption = 'lbcodigo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btAjuda: TSpeedButton
      Left = 779
      Top = -18
      Width = 53
      Height = 80
      Cursor = crHandPoint
      Hint = 'Ajuda'
      Anchors = [akTop, akRight]
      Flat = True
      Glyph.Data = {
        4A0D0000424D4A0D0000000000003600000028000000240000001F0000000100
        180000000000140D0000D8030000D80300000000000000000000FEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFAF6FBF9F2FEFEF4FFFFFAFF
        FCFDFFF9FFFEFEFDFCFFFFFEFEFFFDFEFCFEFEF8FFFFFBFEFCFFFFFCFFFDF6FB
        FFF7F8FFF7FBFFFBFBFDF7FFFFFCFFFFFEF7FDFCFBFFFFFFFBFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFF9FD
        F8FDFFFFFDFBFFFFFBFFFFFDFEFFFCFCFFFEFFFEFDFFFCF9FBF0EFF1F0EBEDF9
        F1F2F6F8F9F1FFFFF4FFFFFEFCFFFFFDFFFFFAFFFBFCFFFEFDFFFFFAFDFAFCFD
        F7FFFFFFFCFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFFFCFFFFFCFEFFFDFCFFFFFFFFFDFFF2E4E6DEBCBCCF
        9F99EB8688E68081EA7E7DF68686FE9092FDA2A5F7C3C3FFF0EDFFFAFFF8FFFF
        FFFDFFFFF5FDFCFDFFF6FFFFFBF8FAFFFDFEFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFBFFFCFEFFFBFFFCE7
        E1DCC29E9EBF7579DC7677F07B76FF8783FF8F88FD9388FB8F84FF837BFF7977
        F77576F98084FBB6B9F2F2ECE8FFFAF8FDFCFFFEFFFAFCFDFFFCFEFCFEFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFEFDFFFAFCD7B8BBB17879C87772F08E88FE9893F58E8CE7797DD76F70
        D26867DC716EE88581F69A95F99591FE8686FF7173F28484FFD9D4FFFFFBFDF8
        F7FFFCFFFFFEFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFF9B99D9CA76B6CDA8786F99B96E17D79
        B6484C98222FAB1122AA0F1EA5131FA41D25AA2226C53938E6625BFF9085FD9B
        91FF7B7CF2777BF4C8C7F8FFFEF9FEFDFFFEFFFFFBFCFEFDFFFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFC0A0A1A7706D
        E6928CFC9691B3484A87192589112398182BA32E31DC6F71EB8082CC5050BB28
        26D32A22EB2F24FE3727F36D69FF9D97FB7D83F7707EFEC5CDFFFEFFFFFDFDED
        FFFFFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFCFBFDFFFDFFFFFBFFFAFFFFF2FAFA
        FFFEFEDCBDBEA66D6BDB9092ED909996303C770E1B8D1E2C9B1C2BA11425D96F
        75FDA8B0FFA2B0FFA0AAC83F43BC2F26D73927FF3929FF2B1CEC5650FF9996FE
        7D7AFD7F7EFADFDBFFFEFFF4FEFEFEFDFFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFF
        FDF9FEFFFDFFF7FAFEFAFFFFF4E6E7A67F81CF8B8CEC9A9F92323C7B121D9123
        2F921C27A5232EA71C27DE837EFFB7B5F9A9AEFDB0B3BF4343B42821C9372BDA
        3326E33626CB2728CA5556FF9893FB7777DE9A9BFFFEFEFFFCFDFEFDFFFEFDFF
        FEFDFFFEFDFFFEFEFEFEFDFFFFFBFFFFFBFFFAFBFFFFFFFFC3AEADA77678F8A4
        A9A9515776131B8D202898242BA3282CA72629A82627B55047EBADA2FFBCB5CD
        7974A92E2AA13026A5372BBD332DBF3528BD2B2DA51E26DF7372FF918FE27377
        F0CCCCFFFFFEFEFDFFFEFDFFFEFDFFFEFDFFFFFFFFFAF9FBFFFDFFFFFAFFFFFE
        FFF5F0EFA38384DB9FA0E78A91791D22852428992E309E2827A72C28AC3028AB
        3229A3271FA13E36B4413E9E332CB03129A73126A23127B0322EA93026AD3131
        A91925B63239F2948FFD7C7FE9989BFBFFF9FEFDFFFEFDFFFEFDFFFEFDFFFFFE
        FEFCFBFDFFFEFFFFFDFFFFFEFFDACECEAE8687F0ABAEB156597E24248B2C2992
        2A25A93730A93328AE3B2EA33222D35E59EA928BF28C87E27D74AC362BA93A2A
        A23729A5342AA3342CA62728B02931A71821D8716FFF8A8ADE7D7FEFECE7FEFD
        FFFEFDFFFEFDFFFEFDFFFFFDFDFFFEFFFDFCFEFFFCFEFFFEFFC0B3B1BF9392F0
        A5A78F3B35842A2396352B9F372AA93C2EAC3D2DAC3C2AA1311FD16D69F8CABF
        FBC0B7FCABA3AA3A2EA23827A33928A9372AA6352BAE302BA72C2EAA1922C550
        51FF9692DC7475E0D2D3FFFEFFFEFDFFFEFDFFFEFDFFFFFDFEFEFDFFFEFDFFFF
        FDFFFFFEFEB6A9A7CB9D9CEC9E9F8335288E3527A0392AAD4130A83C2AA73B29
        A93927AE3827CE6C66FFCDC1FABAB5FFBFB6D46B62AD3126B43427B13A2BA933
        27A93327A02A25AC1F28AF373BFC9D94E7797BD1B8BCFFFEFFFFFEFFFEFDFFFE
        FDFFFFFDFEFCFCFCFDFCFEFFFEFFFDFBFBB5A8A6DBAEABE19192813522983C29
        A73B29AF3E2AA83B26AA3F2AAD3B2AAF3224B05A4EFFE0D0FFCEC6F7BEB5FFBE
        B3DE8C7BA44434A33628AD3C2CAD3726A8342DA7262DAE2E33FA9B92E27A7BD4
        ABB3FFFEFFFFFEFFFEFDFFFEFDFFFBFCFFF9FEFFFFFCFDFBFFFFFCF9FBC9AAAB
        DEB1ADDF9A91813628B03C2BAB3D25A7412AA6362AB3362EA13726AE3C25A833
        24D79987FFF4E4F9D3CEFEBEBEFFC9C3EC9E91AD4436B137299C3B27AA3526B1
        2F28A4302FF7969AE67F86C7AAA5FFFEFFFFFEFFFEFDFFFEFDFFFAFBFFFCFEFF
        FFFEFFFCFEFFFCFEFFCEB4B4E6B6B2E6A99F98392AA13426B2382C9E3E2EAA3E
        33AD4338A74539A24536AB443B9B3D38CF8A87FFEBE6FEDED9F1C0B8FFC9BDEC
        8B81AA372AA73B2AA83F2CA52B1FA6403BF9A1A1E57E86C9B3AEFFFEFFFFFEFF
        FEFDFFFEFDFFFAFDFFFDFCFEFFFEFFFDFCFEFAFFFFD3BEC0ECB5B2F5C2B8AB40
        329F3D31B84D49A24F47AD554EAE574DAE5D55B1655FAD5654B34E50A13B40B9
        7B7BFFE1DBFED7CFF9CAC2FFBCB5BD5146AD3627A3402AA83526AD544AFEACAB
        D5787FD4C7C5FFFEFFFEFDFFFEFDFFFEFDFFFBFFFFFFFBFDFFFDFDFFFDFFF9FE
        FFDFD1D2E7B1B0FFDACEC26B61A74C45A55C54A85A54D3958FF2B8B3F9C2BFF4
        B4B3AD5352AB5C59AC5553A54343F7C0BBFDE0D9FFCCCAFBC9C9CC6A60B03429
        9F3923A63624C9776BFFADACC67E84E4E0DFFEFDFFFEFDFFFEFDFFFEFDFFFAFE
        FFFFFCFFFCFCFCFFFEFFFBFEFFF4ECEDE2B7B4FFDBD2DAB0ABA6504AA86153AB
        514AD59594FFF8F7FEF1EFFAD4D2C469649C5148A85950A14B45ECBBB3FBE1DA
        FFCED0FFCFD6D06E66AB352AAD3B2A9F3726F7A89DFBA4A7BB8E91F2F7F6FEFD
        FFFEFDFFFEFDFFFEFDFFFCFBFDFFFEFFFCFEFEFEFDFFFDFCFFFFFEFFE2C7C3F3
        CAC1EDDEDCBD7672AD5144CE6459D7797AF6F5F1F4FFFEF9F3EEE9B4AAB16860
        B45E58C99087FAD7CDFFDCD6F8D5D2FECBCFBE5A55A23127A12F22CB7264FFC3
        BBD28B8ECFB7B9FAFFFFFEFDFFFEFDFFFEFDFFFEFDFFFEFBFDFFFFFFFCFEFEFB
        FDFEFFFAFFFFFEFFEBE5DEF2C4BDFFE5E8EFC8C0AE6053E17164F47773F0BBB8
        FDFEFAFDFFFFFFF8F1FCD6D4FFCDCCFFDDD8FFDED8FFE0DAFFDDD7E7A1A2A339
        32A7352EAC4A3EF8B8ADECB2ADB68E90FAECEEFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFFFDFFFCFCFCFAFFFEFAFDFFFFFBFFFFFDFDFBFFF9FFCFC9FFC7CCFFF9EF
        E8BFB0D66D5FFF7C70F9807EEACECDFFFBFFFDFCFFF9FEFFFDFFFFFFE4E5FFE2
        DFF6E7DEEDB6AFB34648A1312BB03E37FCAA9FF7CDC1BC8F8CD5C7C8FFFDFFFC
        FEFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFFFEFFFFFEFF
        FDFCFEFCFBFDFCC8C8F6D7D6FFFCF6F0C9C1E57F7AFF7673F98481ECA5A1E6D6
        D7FCF1F3F9F1F1ECD7D9F0C1C3D78A8DA84646A03232A54B4BECABAAFFD1CFC9
        A09EC9ADACFEF9F8FBFFFFFCFCFCFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFF
        FDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFDFCFEFFF4EEF8CDCAFDD8D4FFFBF4FDE6
        E4FBA1A6FB7375FD6F68EA6E5CCE6855B76958AE6458A5514B993E3AA04D4BC8
        7F7BFCC3C1FFCBCCDAA4A4BFAEABF3F9F4FFFFFEFFFCFDFCFEFFFEFDFFFEFDFF
        FEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFEFDFFFFFEFFF4FF
        FFFBF2EFF6D6D1FDD9D3FFEFE8FCFBF1FFE6DCF6BBB2EE9285D07C70B76F65B2
        6F66C0807BDDA39EF3C1BBFFD6CFF6C6C5D4ACADCEBBB8F9FAF6F7FFFDFAFCFC
        FFFEFFFCF9FBFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFDFC
        FEFDFCFEFEFDFFFFFEFFFFFAFCFBFFFFF9F9F9FCE0DFFFD3CCFEDDD4F8ECE6F8
        F6F5F5F8F6F4EFEEF6E4E5FADBDCFFDBDCFFD9D8FBCAC8F0C0BCD7B5B6E1D8D5
        FAFFFEFFFFFFFEFCFCFAFFFFF8FDFFFFFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFDFCFEFDFCFEFDFCFEFEFDFFFFFFFBFFFBFCFDFDFFFD
        FBFFFFEEF1FFE2E1FFD3D2FFD0D2FFD2CFFFD1CDFFD2CFFFCFCFFFC9C9F2BFC3
        E9C4C6EED6D8F5F7F7FFFBFCFFFEFFFFFCFEFDFFFFF9FCFFFCFBFFFFFDFFFEFD
        FFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFFFEFDFFFEFDFFFDFCFEFDFCFEFD
        FCFEFFFBFFF0FDFBF1FFFBFFFEFFFFFDFFF9FEFFF9FCFAFEF2EEF9EAE1F4E5DC
        F3E3DCF3E3DDF6E7E5FCF1F3FEFAFFF9FCFFF4FEFEFFFEFFFFF7FAFFFDFFFCF9
        FBFFFDFFFFFCFFF5F8FCFEFDFFFEFDFFFEFDFFFEFDFFFDFCFEFEFDFFFEFDFFFE
        FDFFFEFDFFFEFDFFFEFDFFFEFDFFF7F9FFFDFEFFFFFBF8FFFDF9FDFBFAF1FBFB
        F6FFFFFBFDFEFBFEFFFFFCFFFFFDFFFFFEFFFBFFFFF8FFFFFDFEFFFFFAFFFFFD
        FFFAFEFFF8FFFFF9FBFCFFFDFFFFF9FCFBFAFCFBFFFFFEFDFFFEFDFFFEFDFFFE
        FDFFFDFCFEFDFCFEFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFEFDFFFFFEFCFFFEFF
        F7FDFFF5FEFFFBFFFFFFFFFEFFFCFDFFFCFFFFFAFFFFFCFFF9FEFDF9FFFDFBFF
        FEF9FCFAFBFBFBFFFFFFFFFCFEFEFDFFFBFAFCFBFFFFF5FDFDF7FFFFFDFFFFFF
        FCFFFEFDFFFEFDFFFEFDFFFEFDFF}
      OnClick = btAjudaClick
    end
    object btopcoes: TBitBtn
      Left = 351
      Top = -2
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object BtNovo: TBitBtn
      Left = 1
      Top = -2
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -2
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -2
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -2
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object Btgravar: TBitBtn
      Left = 101
      Top = -2
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -2
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object BtCancelar: TBitBtn
      Left = 151
      Top = -2
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btsair: TBitBtn
      Left = 401
      Top = -2
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 49
    Width = 827
    Height = 139
    Align = alTop
    Color = clSilver
    TabOrder = 1
    DesignSize = (
      827
      139)
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 825
      Height = 137
      Align = alClient
      Stretch = True
    end
    object lbAtivo: TLabel
      Left = 625
      Top = 115
      Width = 53
      Height = 19
      Caption = 'lbAtivo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 440
      Top = 42
      Width = 48
      Height = 14
      Caption = 'Telefone'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 11
      Top = 68
      Width = 52
      Height = 14
      Caption = 'Endere'#231'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 213
      Top = 44
      Width = 95
      Height = 14
      Caption = 'Numero da Conta'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 11
      Top = 44
      Width = 44
      Height = 14
      Caption = 'Ag'#234'ncia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 11
      Top = 20
      Width = 100
      Height = 14
      Caption = 'Nome do Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 10
      Top = 115
      Width = 117
      Height = 14
      Caption = 'Emiss'#227'o de Cheque?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 213
      Top = 115
      Width = 131
      Height = 14
      Caption = 'C'#243'digo Plano de Contas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label11: TLabel
      Left = 440
      Top = 90
      Width = 155
      Height = 14
      Caption = 'Apenas de Uso do Sistema?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 213
      Top = 90
      Width = 134
      Height = 14
      Caption = 'Permite Saldo Negativo?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 11
      Top = 90
      Width = 72
      Height = 14
      Caption = 'Classifica'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Shape1: TShape
      Left = 0
      Top = 134
      Width = 826
      Height = 1
      Anchors = [akLeft, akRight, akBottom]
      Pen.Color = clWhite
    end
    object ComboPermiteEmissaoCheque: TComboBox
      Left = 130
      Top = 111
      Width = 80
      Height = 21
      BevelKind = bkSoft
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 8
      Items.Strings = (
        'N - N'#227'o'
        'S - Sim')
    end
    object edtnome: TEdit
      Left = 130
      Top = 18
      Width = 550
      Height = 19
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 250
      ParentFont = False
      TabOrder = 0
    end
    object edtagencia: TEdit
      Left = 130
      Top = 41
      Width = 80
      Height = 19
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 15
      ParentFont = False
      TabOrder = 1
    end
    object edtnumeroconta: TEdit
      Left = 354
      Top = 41
      Width = 80
      Height = 19
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 15
      ParentFont = False
      TabOrder = 2
    end
    object edtendereco: TEdit
      Left = 130
      Top = 64
      Width = 550
      Height = 19
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 50
      ParentFont = False
      TabOrder = 4
    end
    object edttelefone: TEdit
      Left = 599
      Top = 41
      Width = 80
      Height = 19
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 20
      ParentFont = False
      TabOrder = 3
    end
    object edtCodigoPlanodeContas: TMaskEdit
      Left = 354
      Top = 112
      Width = 80
      Height = 19
      Hint = 
        'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
        #225'bil)'
      BiDiMode = bdRightToLeft
      CharCase = ecUpperCase
      Color = 6073854
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      OnDblClick = edtCodigoPlanodeContasDblClick
      OnKeyDown = edtCodigoPlanodeContasKeyDown
      OnKeyPress = edtCodigoPlanodeContasKeyPress
    end
    object comboapenas_uso_sistema: TComboBox
      Left = 599
      Top = 87
      Width = 80
      Height = 21
      BevelKind = bkSoft
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 7
      Items.Strings = (
        'N - N'#227'o'
        'S - Sim')
    end
    object comboPermite_Saldo_Negativo: TComboBox
      Left = 354
      Top = 87
      Width = 80
      Height = 21
      BevelKind = bkSoft
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 6
      Items.Strings = (
        'N - N'#227'o'
        'S - Sim')
    end
    object ComboClassificacao: TComboBox
      Left = 130
      Top = 87
      Width = 80
      Height = 21
      BevelKind = bkSoft
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ItemHeight = 13
      ParentFont = False
      TabOrder = 5
      OnKeyPress = ComboClassificacaoKeyPress
      Items.Strings = (
        'C - Caixa'
        'B - Banco'
        'O - Outros')
    end
    object checkativo: TCheckBox
      Left = 576
      Top = 118
      Width = 13
      Height = 13
      TabOrder = 10
      OnClick = checkativoClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 188
    Width = 827
    Height = 271
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'MS Sans Serif'
    TabFont.Style = []
    TabOrder = 2
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Cheques de Terceiros'
      object STRChequePortador: TStringGrid
        Left = 0
        Top = 0
        Width = 819
        Height = 243
        Align = alClient
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing]
        TabOrder = 0
        OnDblClick = STRChequePortadorDblClick
        OnKeyDown = STRChequePortadorKeyDown
        ColWidths = (
          64
          64
          64
          64
          64)
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Cheques Emitidos'
      object STRGEmitidos: TStringGrid
        Left = 0
        Top = 0
        Width = 819
        Height = 243
        Align = alClient
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSizing, goColSizing]
        TabOrder = 0
        ColWidths = (
          64
          64
          64
          64
          64)
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&3 - Lan'#231'amentos (Concilia'#231#227'o Banc'#225'ria)'
      inline FrLanctoPortador1: TFrLanctoPortador
        Left = 0
        Top = 0
        Width = 819
        Height = 243
        Align = alClient
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        inherited Guia: TPageControl
          Width = 819
          Height = 193
          inherited TabSheet1: TTabSheet
            inherited Label1: TLabel
              Left = 8
            end
            inherited Label2: TLabel
              Left = 143
              Top = 5
            end
            inherited Label5: TLabel
              Left = 8
              Top = 29
            end
            inherited Label3: TLabel
              Left = 8
              Top = 53
            end
            inherited Label6: TLabel
              Left = 8
              Top = 77
            end
            inherited Label4: TLabel
              Left = 143
              Top = 75
            end
            inherited LbTipoLanctoNome: TLabel
              Left = 345
              Top = 5
              Width = 92
              Caption = 'lbLancamento'
            end
            inherited lbportador: TLabel
              Left = 143
              Top = 29
            end
            inherited EdtCodigo: TEdit
              Left = 63
            end
            inherited edttipolancto: TEdit
              Left = 263
              Top = 3
            end
            inherited edtportador: TEdit
              Left = 63
              Top = 26
            end
            inherited edthistorico: TEdit
              Left = 143
              Top = 49
            end
            inherited edtvalor: TEdit
              Left = 63
              Top = 72
            end
            inherited edtdata: TMaskEdit
              Left = 263
              Top = 73
              Width = 70
            end
            inherited edtcodigohistoricosimples: TEdit
              Left = 63
              Top = 49
            end
          end
        end
        inherited panelbotes: TPanel
          Width = 819
          inherited lbnomeformulario: TLabel
            Left = 581
          end
          inherited BtNovo: TBitBtn
            OnClick = FrLanctoPortador1BtNovoClick
          end
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&4 - Baixas de Cheque (Concilia'#231#227'o de Cheques)'
      inline FrBaixaCheque1: TFrBaixaCheque
        Left = 0
        Top = 37
        Width = 819
        Height = 136
        Align = alTop
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        inherited Panel1: TPanel
          Width = 819
          Height = 136
          Color = 14024703
          inherited Label2: TLabel
            Top = 38
          end
          inherited Label1: TLabel
            Left = 216
            Top = 37
          end
          inherited edtportador: TEdit
            Color = 6073854
          end
          inherited edtdata: TMaskEdit
            Top = 34
          end
          inherited edtcheque: TEdit
            Left = 342
            Top = 34
            Color = 6073854
          end
          inherited btestornabaixa: TBitBtn [7]
            Left = 563
            Top = 18
            Width = 64
            Height = 42
            HelpType = htKeyword
            Caption = '&T'
            Font.Color = clWhite
            Font.Height = -1
            Font.Name = 'Verdana'
          end
          inherited Btbaixa: TBitBtn [8]
            Left = 488
            Top = 18
            Width = 64
            Height = 42
            HelpType = htKeyword
            Caption = '&B'
            Font.Color = clWhite
            Font.Height = -1
            Font.Name = 'Verdana'
            OnClick = FrBaixaCheque1BtbaixaClick
          end
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 819
        Height = 37
        Align = alTop
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -24
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        object ImagemFundoBaixa: TImage
          Left = 1
          Top = 1
          Width = 817
          Height = 35
          Align = alClient
          Stretch = True
        end
        object Label1: TLabel
          Left = 1
          Top = 1
          Width = 817
          Height = 35
          Align = alClient
          Alignment = taCenter
          Caption = 'Concilia'#231#227'o Banc'#225'ria'
          Transparent = True
        end
      end
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 500
    Width = 827
    Height = 62
    Align = alBottom
    BevelOuter = bvNone
    Color = clSilver
    TabOrder = 3
    DesignSize = (
      827
      62)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 827
      Height = 62
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 514
      Top = 16
      Width = 303
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Portadores cadastrados'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lb11: TLabel
      Left = 174
      Top = 3
      Width = 61
      Height = 18
      Caption = 'Dinheiro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb12: TLabel
      Left = 174
      Top = 22
      Width = 64
      Height = 18
      Caption = 'Cheques'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb13: TLabel
      Left = 174
      Top = 42
      Width = 38
      Height = 19
      Caption = 'Total'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbSaldoPortador: TLabel
      Left = 223
      Top = 42
      Width = 128
      Height = 19
      Alignment = taRightJustify
      Caption = 'lbSaldoPortador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -17
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbCheque: TLabel
      Left = 282
      Top = 22
      Width = 69
      Height = 18
      Alignment = taRightJustify
      Caption = 'lbCheque'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbDinheiro: TLabel
      Left = 277
      Top = 3
      Width = 74
      Height = 18
      Alignment = taRightJustify
      Caption = 'lbDinheiro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbPortador: TLabel
      Left = 8
      Top = 2
      Width = 161
      Height = 48
      AutoSize = False
      Caption = 'lbportador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object shp3: TShape
      Left = 168
      Top = 40
      Width = 191
      Height = 1
      Pen.Color = clWhite
    end
  end
  object panelPesquisa: TPanel
    Left = 0
    Top = 459
    Width = 827
    Height = 41
    Align = alBottom
    Color = 8539648
    TabOrder = 4
    Visible = False
    object edtPesquisa: TEdit
      Left = 12
      Top = 8
      Width = 365
      Height = 26
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnKeyDown = edtPesquisaKeyDown
    end
  end
end
