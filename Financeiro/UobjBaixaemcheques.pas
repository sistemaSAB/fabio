//UNIT PARA PESQUISA E BAIXA EM CHEQUES **JONATAN MEDINA

unit UobjBaixaemcheques;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,Upesquisa,UobjPortador,db, Ibquery,UessencialGlobal,
  DateUtils,IBCustomDataSet, ExtCtrls,UescolheImagemBotao, Grids, DBGrids,
  Mask;

type
  TFBaixaEmCheques = class(TForm)
    LbCodigo: TLabel;
    LbNome: TLabel;
    LbPortador: TLabel;
    LbValor: TLabel;
    LbCliente: TLabel;
    LbPortador1: TLabel;
    LbValor1: TLabel;
    lblVenc: TLabel;
    LbNumCheq2: TLabel;
    PanPanel2: TPanel;
    LbTipoCheque: TLabel;
    LbTipocHEQ: TLabel;
    lbdatvenc: TLabel;
    lblnomPort: TLabel;
    btnSair: TBitBtn;
    rbPagos: TRadioButton;
    rbRecebidos: TRadioButton;
    pnl2: TPanel;
    StrGridBaixa1: TStringGrid;
    dscheques: TDataSource;
    bvl1: TBevel;
    lbl1: TLabel;
    pnl1: TPanel;
    pnl3: TPanel;
    lblQuantsel: TLabel;
    lblquant: TLabel;
    lblval: TLabel;
    lblvalsel: TLabel;
    lblBaixarCheque: TLabel;
    lbl2: TLabel;
    edtpesquisaport: TEdit;
    edtpesquisa: TMaskEdit;
    lblobs: TLabel;
    bvl2: TBevel;

    //procedure btnBaixarClick(Sender: TObject);
    //procedure btnPesquisaClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure rbRecebidosClick(Sender: TObject);
    procedure rbPagosClick(Sender: TObject);
    procedure StrGridBaixa1Click(Sender: TObject);
    procedure StrGridBaixa1dblClick (Sender: TObject);
    procedure StrGridBaixa1KeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
    procedure edtpesquisaKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
    procedure lblBaixarChequeClick(Sender: TObject);
    procedure edtpesquisaportKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
    procedure StrGridBaixa1DrawCell(Sender: TObject; ACol,ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure edtpesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure edtpesquisaportKeyPress(Sender: TObject; var Key: Char);
    procedure edtpesquisaportDblClick(Sender: TObject);


  private
    ObjQuery  : TIBQuery;
    objquery2 : TIBQuery;
    InsertSql,DeleteSql,ModifySQl:TStringList;
    codigo            : string;
    codigo2           : string;
    Valor             : String;
    Portador          : String;
    Cliente           : String;
    Data              : string;
    Teste             : string;
    NomePortador      :string;
    quantSel          :Integer;
    linha             :Integer;
    coluna            :Integer;
    valortotalsel     :Currency ;
    selecao           :Integer;


  public
    constructor Create;
    procedure Dar_baixas();
    procedure Carregar_Figuras();
    function Get_Pesquisa                               : string;
    function Get_TituloPesquisa                         : string;
    function LocalizaCodigo(parametro: string)           : boolean;
    procedure ObjetoparaTabela;
    procedure TabelaparaObjetos;
    procedure ObjetoparaControles;
    procedure Busca_Nome_Portador;
    function Get_Codigo                                 :String;
    function Get_Valor                                  :String;
    function Get_Portador                               :String;
    procedure CarregaChequesPagos();
    procedure CarregaChequesRecebidos();


  end;

var
  FBaixaEmCheques: TFBaixaEmCheques;

implementation

uses UDataModulo;

{$R *.dfm}

constructor TFBaixaEmCheques.Create;
begin
       Objquery:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
       Objquery.Database:= FDataModulo.IBDatabase;         //conectando ao banco
       Objquery2:= TIBQuery.Create(nil);                    //criando um query pra executar SQL
       Objquery2.Database:= FDataModulo.IBDatabase;         //conectando ao banco


       LbCliente.Caption := '';
       LbValor1.Caption := '';
       LbPortador1.Caption:= '';
       LbNumCheq2.Caption := '';
       LbTipocHEQ.Caption := '';
       lbdatvenc.Caption := '';
       Self.lblnomPort.Caption :='';
       edtpesquisaport.text := '';

       with StrGridBaixa1 do
       begin
            RowCount:= 1;
            ColCount:= 9;
            ColWidths[0] := 35;
            ColWidths[1] := 100;
            ColWidths[2] := 300;
            ColWidths[3] := 100;
            ColWidths[4] := 150;
            ColWidths[5] := 300;
            ColWidths[6] := 150;
            ColWidths[7] := 300;
            ColWidths[8] := 100;
            Cells[0,0] := 'X';
            Cells[1,0] := 'Num Cheque';
            Cells[2,0] := 'Nome do Cliente';
            Cells[3,0] := 'Valor';
            Cells[4,0] := 'Cod Portador';
            Cells[5,0] := 'Portador';
            Cells[6,0] := 'Vencimento';
            Cells[7,0] := 'Tipo de Cheque';
            Cells[8,0] := 'Codigo';


       end;
       self.Carregar_Figuras();

       Self.quantSel :=0;  //quantidade de chueques selecionados
       Self.valortotalsel  :=0;//valor total selecionado
       edtPesquisa.Visible := False;
       pnl3.Visible :=False;



end;

procedure TFBaixaEmCheques.Carregar_Figuras();
begin
      FescolheImagemBotao.PegaFiguraBotaopequeno(BtnSair,'BOTAOSAIR.BMP');

end;

procedure TFBaixaEmCheques.Dar_baixas();
begin

end;


Function TFBaixaEmCheques.Get_Pesquisa : string;
begin
  result:= 'Select * from TabChequesPortador where baixado = ''N''';
end;

Function TFBaixaEmCheques.Get_TituloPesquisa : string;
begin
  result:='Pesquisa de Cheques';
end;

function TFBaixaEmCheques.LocalizaCodigo(parametro: string): boolean;//ok
begin
       result:=False;

       if (parametro='')
       then exit;

       With Self.ObjQuery do
       Begin


           close;
           SQL.Clear;
           SQL.add('select * from tabportador');
           SQL.add('from tabportador');
           //SQL.add('and chequedoportador = ''N'' ');
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TFBaixaEmCheques.TabelaparaObjetos;
begin
   With Objquery do
     Begin
        Self.codigo            := FieldByName('numcheque').asstring;
        Self.valor             := FieldByName('valor').asstring;
        Self.Portador          := FieldByName('portador').asstring;
        Self.Cliente           := FieldByname('CLIENTE1').AsString;
        Self.codigo2           := FieldByName('CODIGO').AsString;
        Self.Data              := FieldByname('vencimento').AsString;
        Self.Teste             := FieldByname('chequedoportador').AsString;
        Self.Busca_Nome_Portador();
        Self.lblnomPort.Caption  := Self.NomePortador;


     End;

     if(Teste ='N')
     then begin
       LbTipocHEQ.Caption := 'Cheque de Terceiro (Recebido)';
     end;
     if(teste = 'S')
     then begin
       LbTipocHEQ.Caption := 'Cheque pr�prio (Pago)';
     end;
end;


procedure TFBaixaEmCheques.ObjetoparaTabela;
begin
  With ObjQuery do
  Begin
      FieldByName('numcheque').asstring           :=         Self.Get_codigo  ;
      FieldByName('Portador').asstring            :=         Self.Get_Portador;
      FieldByName('valor').asstring               :=         Self.Get_Valor;
      FieldByName('cliente1').AsString            :=         Self.Cliente;

  End;
end;

procedure TFBaixaEmCheques.ObjetoparaControles;
begin

        LbNumCheq2.Caption := Self.Codigo;
        LbCliente.Caption := Self.Cliente;
        LbValor1.Caption := Self.Valor +' R$';
        LbPortador1.Caption:= Self.Portador;
        lbdatvenc.Caption := Self.Data;


end;

function TFBaixaEmCheques.Get_Codigo : String;
begin
  result := Self.codigo;
end;

function TFBaixaEmCheques.Get_Valor :String;
begin
  result := Self.Valor;
end;

function TFBaixaEmCheques.Get_Portador :String;
begin
  result := Self.Portador;
end;

procedure TFBaixaEmCheques.Busca_Nome_Portador();
begin
          objquery2.Close;
          objquery2.SQL.Clear;
          objquery2.SQL.Add('select nome from tabportador where codigo =' +self.Portador );

          objquery2.Open;
          Self.NomePortador :=  objquery2.FieldByName('nome').asstring;
end;

procedure TFBaixaEmCheques.btnSairClick(Sender: TObject);
begin
  if(rbPagos.Checked = True)
  then begin
    rbPagos.Checked := False;
  end;
  if(rbRecebidos.Checked = True)
  then begin
    rbRecebidos.Checked:=False;
  end;
     FBaixaEmCheques.Close;
end;

procedure TFBaixaEmCheques.rbRecebidosClick(Sender: TObject);
begin

  Self.CarregaChequesRecebidos();

end;

procedure TFBaixaEmCheques.rbPagosClick(Sender: TObject);
begin

  Self.CarregaChequesPagos();

end;

procedure TFBaixaEmCheques.StrGridBaixa1KeyDown(Sender: TObject; var Key: Word;
Shift: TShiftState);
var
  cont : Integer;
  
begin
    if(key = vk_space)
    then begin
        if(edtPesquisa.Visible=False)
        then begin
              pnl3.Visible :=True;
              edtPesquisa.Visible:=True;
              edtpesquisa.Text:='';
              linha := 1;
              coluna := StrGridBaixa1.Col;
              if(coluna = 6)
              then begin
                 edtpesquisa.EditMask := '!99/99/9999;1;_';
              end
              else
              begin
                 edtpesquisa.EditMask := '';
              end;
              edtpesquisa.SetFocus;

        end
        else
        begin
              pnl3.Visible :=False;
              edtPesquisa.Visible :=false;
              edtPesquisa.Text:='';
        end;

    end;
    if(Key =13)
    then begin
        if(StrGridBaixa1.Row <> 0)
        then begin


           try
                if(StrGridBaixa1.Cells[0,StrGridBaixa1.row] <> 'X')
                then begin
                    StrGridBaixa1.Cells[0,StrGridBaixa1.row] := 'X';
                    Inc(quantSel,1);
                    valortotalsel := valortotalsel +(StrToCurr(tira_ponto(StrGridBaixa1.Cells[3,StrGridBaixa1.row])))


                end
                else begin
                    StrGridBaixa1.Cells[0,StrGridBaixa1.row] := '';
                    Dec(quantSel,1);
                    valortotalsel := valortotalsel -(StrToCurr(tira_ponto(StrGridBaixa1.Cells[3,StrGridBaixa1.row])))

                end;
                lblquant.caption := IntToStr(quantSel);
                lblvalsel.Caption := formata_valor(CurrToStr(valortotalsel));
           except
              ShowMessage('Selecione uma linha');

           end;
        end;
    end;



end;

procedure TFBaixaEmCheques.StrGridBaixa1Click(Sender: TObject);
var
  mouse : TMouseButton;
  key :word;
begin
  if(StrGridBaixa1.Row <>0)
  then begin
    LbCliente.Caption := '';
    LbValor1.Caption := '';
    LbPortador1.Caption:= '';
    LbNumCheq2.Caption := '';
    LbTipocHEQ.Caption := '';
    lbdatvenc.Caption := '';
    Self.lblnomPort.Caption :='';
    LbNumCheq2.Caption := StrGridBaixa1.Cells[1,StrGridBaixa1.row] ;
    LbCliente.Caption  := StrGridBaixa1.Cells[2,StrGridBaixa1.row] ;
    LbValor1.Caption   := StrGridBaixa1.Cells[3,StrGridBaixa1.row] +' R$';
    lblnomPort.Caption := StrGridBaixa1.Cells[5,StrGridBaixa1.row];
    LbPortador1.Caption:= StrGridBaixa1.Cells[4,StrGridBaixa1.row];
    lbdatvenc.Caption  := StrGridBaixa1.Cells[6,StrGridBaixa1.row];
    LbTipocHEQ.Caption := StrGridBaixa1.Cells[7,StrGridBaixa1.row];
    lblquant.caption := IntToStr(quantSel);
  end
  else
  begin
     LbCliente.Caption := '';
    LbValor1.Caption := '';
    LbPortador1.Caption:= '';
    LbNumCheq2.Caption := '';
    LbTipocHEQ.Caption := '';
    lbdatvenc.Caption := '';
    Self.lblnomPort.Caption :='';
  end;
end;

procedure TFBaixaEmCheques.StrGridBaixa1dblClick (Sender: TObject);
var
  mouse : TMouseButton;
begin
   if(StrGridBaixa1.Row <> 0)
   then begin


       try
            if(StrGridBaixa1.Cells[0,StrGridBaixa1.row] <> 'X')
            then begin
                StrGridBaixa1.Cells[0,StrGridBaixa1.row] := 'X';
                Inc(quantSel,1);
                valortotalsel := valortotalsel +(StrToCurr(tira_ponto(StrGridBaixa1.Cells[3,StrGridBaixa1.row])));


            end
            else begin
                StrGridBaixa1.Cells[0,StrGridBaixa1.row] := '';
                Dec(quantSel,1);
                valortotalsel := valortotalsel -(StrToCurr(tira_ponto(StrGridBaixa1.Cells[3,StrGridBaixa1.row])));

            end;
            lblquant.caption := IntToStr(quantSel);
            lblvalsel.Caption := formata_valor(CurrToStr(valortotalsel));
       except
          ShowMessage('Selecione uma linha');

       end;
    end;


end;

procedure TFBaixaEmCheques.edtpesquisaKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);

var
  indice : Integer;
begin


    if (Key = 27) then //27 - ESC
    begin

      pnl3.Visible :=False; //panel pesquisa
      edtPesquisa.Text:='';
      StrGridBaixa1.SetFocus;

    end
    else if (Key = 13) then // 13 - enter
    begin

       for indice:=linha to StrGridBaixa1.RowCount do
       begin

           if Pos( UpperCase(edtPesquisa.Text), UpperCase(StrGridBaixa1.Cells[coluna,indice])) > 0
           then begin


              StrGridBaixa1.Col := coluna;
              StrGridBaixa1.Row := indice;
              StrGridBaixa1.Setfocus;

              linha:=indice+1;
              edtpesquisa.SetFocus;

              Exit;


           end;

       end;

        ShowMessage('N�o h� mais itens');
        linha:=1;
        pnl3.Visible:=False;
        //StrGridBaixa1.SetFocus;
        Exit;


    end;

end;


procedure TFBaixaEmCheques.lblBaixarChequeClick(Sender: TObject);
var
  cont, cont2:Integer;
  cheques : Integer;
begin
  cont:=1;
  cheques :=0;
  try
       while (Cont<StrGridBaixa1.RowCount) do
       begin
         if(StrGridBaixa1.Cells[0,Cont] = 'X')
         then begin
             ObjQuery.Close;
             ObjQuery.SQL.Clear;
             ObjQuery.SQL.Add('update tabchequesportador set baixado = ''S'' ' ) ;
             ObjQuery.SQL.Add('where CODIGO ='+StrGridBaixa1.Cells[8,Cont]);
             ObjQuery.ExecSQL;
             LbNumCheq2.Caption := ' ';
             LbCliente.Caption := ' ';
             LbValor1.Caption := ' ';
             LbPortador1.Caption:= ' ';
             lbdatvenc.Caption := ' ';
             Self.lblnomPort.Caption := ' ';
             LbTipocHEQ.Caption := ' ';
             FDataModulo.IBTransaction.CommitRetaining;
             StrGridBaixa1.Cells[0,Cont] := '';
             Inc(cheques,1);
         end;
         Inc(cont,1);
       end;

       if(cheques =0)
       then begin
            MensagemErro('Voc� precisa selecionar um cheque a ser baixado');
            FDataModulo.IBTransaction.RollbackRetaining;
       end;
       if(cheques = 1)
       then begin
            ShowMessage('Cheque baixado com sucesso');
       end;
       if(cheques > 1)
       then begin
            ShowMessage('Cheque(s) baixado(s) com sucesso');
       end;
            cheques :=0;
         Self.quantSel :=0;  //quantidade de cheques selecionados
         Self.valortotalsel := 0;
         edtpesquisaport.text := '';
         edtPesquisa.Text:='';
       if(rbPagos.Checked = True) then
       begin

           Self.CarregaChequesPagos();

       end;
       if(rbRecebidos.Checked = True)
       then begin

          Self.CarregaChequesRecebidos();

       end;



  except
      MensagemErro('Voc� precisa selecionar um cheque a ser baixado');
      FDataModulo.IBTransaction.RollbackRetaining;

  end;
end;

procedure TFBaixaEmCheques.edtpesquisaportKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
    If (key =vk_f9)
     Then begin
         Try

            Fpesquisalocal:=Tfpesquisa.create(nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FCLIENTE.EDTCLIENTE';

            If (FpesquisaLocal.PreparaPesquisa('select * from tabportador','Pesquisa de Portadores',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtpesquisaport.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;
            if(rbPagos.Checked = True)
            then begin
              rbPagos.Checked := False;
            end;
            if(rbRecebidos.Checked = True)
            then begin
              rbRecebidos.Checked:=False;
            end;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
     end;

    if (Key = 13)
    THEN begin
      if(rbPagos.Checked = True)
      then begin
          rbRecebidos.Checked:=true;
          Exit;
      end;
      if(rbRecebidos.Checked = True)
      then begin
           rbPagos.Checked := true ;
           Exit;
      end;
      if(rbRecebidos.Checked = False) and (rbPagos.Checked = False)
      then begin
          rbRecebidos.Checked:=true;
          Exit;
      end;
    end;




end;

procedure TFBaixaEmCheques.StrGridBaixa1DrawCell(Sender: TObject; ACol,ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
    {if (selecao = 1)
    then begin
      StrGridBaixa1.Canvas.Brush.Color := clBlue;
    end;  }

end;

procedure TFBaixaEmCheques.edtpesquisaKeyPress(Sender: TObject; var Key: Char);
begin
   if(StrGridBaixa1.Col = 1) or (StrGridBaixa1.Col = 4) or(StrGridBaixa1.Col = 8)
   then begin
       if not (Key in['0'..'9',Chr(8)]) then
        Key:= #0;
   end;
   if(coluna <> StrGridBaixa1.Col)
   then begin
     coluna := StrGridBaixa1.Col;
     edtpesquisa.Text:='';
     edtpesquisa.Visible:=False;
     StrGridBaixa1.SetFocus;
   end;


end;

procedure TFBaixaEmCheques.CarregaChequesPagos();
var
  cont : Integer;
begin
    cont := 1;
    quantSel := 0;
    valortotalsel :=0;
    StrGridBaixa1.RowCount:= 1;
    LbCliente.Caption := '';
    LbValor1.Caption := '';
    LbPortador1.Caption:= '';
    LbNumCheq2.Caption := '';
    LbTipocHEQ.Caption := '';
    lbdatvenc.Caption := '';
    Self.lblnomPort.Caption :='';
    lblquant.caption := IntToStr(quantSel);
    lblvalsel.Caption := formata_valor(CurrToStr(valortotalsel));
    edtPesquisa.Text:='';
    With ObjQuery do
    Begin
      Close;
      SQL.Clear;
      SQL.Add('Select * from TabChequesPortador ');
      SQL.Add('join tabvalores on tabvalores.cheque = tabchequesportador.codigo');
      SQL.Add('where baixado = ''N''');
      SQL.Add('and chequedoportador =''S''');
      //SQL.Add('and tabvalores.cheque = tabchequesportador.codigo');
      if (edtpesquisaport.text <> '')
      then begin
       SQL.Add('and tabchequesportador.portador ='+edtpesquisaport.text);
      end;
      Open;
      while not (Eof) do
      begin
           StrGridBaixa1.Cells[0,cont] := '';
           StrGridBaixa1.Cells[1,cont] := FieldByName('numcheque').asstring   ;
           StrGridBaixa1.Cells[2,cont] := FieldByName('cliente1').asstring    ;
           StrGridBaixa1.Cells[3,cont] := formata_valor(FieldByName('valor').asstring)       ;
           StrGridBaixa1.Cells[4,cont] := FieldByName('portador').asstring    ;
           Self.Portador:= FieldByName('portador').asstring;
           Self.Busca_Nome_Portador();
           StrGridBaixa1.Cells[5,cont] := Self.NomePortador;
           StrGridBaixa1.Cells[6,cont] := FieldByName('vencimento').asstring  ;
           StrGridBaixa1.Cells[7,cont] := 'Cheques Pagos ';
           StrGridBaixa1.Cells[8,cont] := FieldByName('codigo').asstring  ;

           StrGridBaixa1.RowCount:= StrGridBaixa1.RowCount+1;
           Inc(cont,1);
           Next;;
      end;


    End;

end;

procedure TFBaixaEmCheques.CarregaChequesRecebidos();
var
  cont, cont2 : Integer;
  key  : Word;
begin
    cont := 1;
    cont2 :=0;
    StrGridBaixa1.RowCount:= 1;
   LbCliente.Caption := '';
    LbValor1.Caption := '';
    LbPortador1.Caption:= '';
    LbNumCheq2.Caption := '';
    LbTipocHEQ.Caption := '';
    lbdatvenc.Caption := '';
    Self.lblnomPort.Caption :='';
    edtPesquisa.Text:='';
    lblquant.caption := IntToStr(quantSel);
    lblvalsel.Caption := formata_valor(CurrToStr(valortotalsel));
    With ObjQuery do
    Begin
      Close;
      SQL.Clear;
      SQL.Add('Select * from TabChequesPortador ');
      SQL.Add('join tabvalores on tabvalores.cheque = tabchequesportador.codigo');
      SQL.Add('where baixado = ''N''');
      SQL.Add('and chequedoportador =''N''');
      //SQL.Add('and tabvalores.cheque = tabchequesportador.codigo');
      if (edtpesquisaport.text <> '')
      then begin
       SQL.Add('and tabchequesportador.portador ='+edtpesquisaport.text);
      end;
      Open;
      while not (Eof) do
      begin
           StrGridBaixa1.Cells[0,cont] := '';
           StrGridBaixa1.Cells[1,cont] := FieldByName('numcheque').asstring   ;
           StrGridBaixa1.Cells[2,cont] := FieldByName('cliente1').asstring    ;
           StrGridBaixa1.Cells[3,cont] := formata_valor(FieldByName('valor').asstring)       ;
           StrGridBaixa1.Cells[4,cont] := FieldByName('portador').asstring    ;
           Self.Portador:= FieldByName('portador').asstring;
           Self.Busca_Nome_Portador();
           StrGridBaixa1.Cells[5,cont] := Self.NomePortador;
           StrGridBaixa1.Cells[6,cont] := FieldByName('vencimento').asstring  ;
           StrGridBaixa1.Cells[7,cont] := 'Cheques Recebidos'  ;
           StrGridBaixa1.Cells[8,cont] := FieldByName('codigo').asstring  ;
           StrGridBaixa1.RowCount:= StrGridBaixa1.RowCount+1;
           Inc(cont,1);
           Next;
      end;



     end;

End;

procedure TFBaixaEmCheques.edtpesquisaportKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
        Key:= #0;
end;

{Rodolfo}
procedure TFBaixaEmCheques.edtpesquisaportDblClick(Sender: TObject);
var
  key : Word;
  shift : TShiftState;
begin
    key := VK_F9;
    edtpesquisaportKeyDown(Sender, key, shift);
end;

end.
