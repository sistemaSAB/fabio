object FconfiguraComprovanteCheque: TFconfiguraComprovanteCheque
  Left = -4
  Top = -4
  Width = 808
  Height = 580
  Caption = 'Configura��o das Posi��es do Comprovante de Cheque'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object LbDescricao: TLabel
    Left = 35
    Top = 201
    Width = 56
    Height = 13
    Caption = 'Descri��o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbComprovante: TLabel
    Left = 35
    Top = 260
    Width = 78
    Height = 13
    Caption = 'Comprovante'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbProvidencias: TLabel
    Left = 27
    Top = 326
    Width = 72
    Height = 13
    Caption = 'Provid�ncias'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbNomeRecebedor: TLabel
    Left = 35
    Top = 443
    Width = 33
    Height = 13
    Caption = 'Nome'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbCPFCNPJRecebedor: TLabel
    Left = 35
    Top = 469
    Width = 56
    Height = 13
    Caption = 'CPF/CNPJ'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbValor: TLabel
    Left = 592
    Top = 16
    Width = 36
    Height = 13
    Caption = 'LbValor'
    OnClick = LBpagoAClick
  end
  object LBExtenso1: TLabel
    Left = 40
    Top = 64
    Width = 57
    Height = 13
    Caption = 'LBExtenso1'
    OnClick = LBpagoAClick
  end
  object LBExtenso2: TLabel
    Left = 40
    Top = 88
    Width = 57
    Height = 13
    Caption = 'LBExtenso2'
    OnClick = LBpagoAClick
  end
  object LbNominal: TLabel
    Left = 24
    Top = 120
    Width = 50
    Height = 13
    Caption = 'LbNominal'
    OnClick = LBpagoAClick
  end
  object LbCidadeCheque: TLabel
    Left = 376
    Top = 152
    Width = 33
    Height = 13
    Caption = 'Cidade'
    OnClick = LBpagoAClick
  end
  object LbDiaCheque: TLabel
    Left = 464
    Top = 152
    Width = 16
    Height = 13
    Caption = 'Dia'
    OnClick = LBpagoAClick
  end
  object LbMesCheque: TLabel
    Left = 544
    Top = 152
    Width = 20
    Height = 13
    Caption = 'Mes'
    OnClick = LBpagoAClick
  end
  object LbAnoCheque: TLabel
    Left = 648
    Top = 152
    Width = 19
    Height = 13
    Caption = 'Ano'
    OnClick = LBpagoAClick
  end
  object LbNumeroCheque: TLabel
    Left = 27
    Top = 284
    Width = 62
    Height = 13
    Caption = 'N� Cheque'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbBAnco: TLabel
    Left = 139
    Top = 284
    Width = 35
    Height = 13
    Caption = 'Banco'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LBAgencia: TLabel
    Left = 243
    Top = 284
    Width = 45
    Height = 13
    Caption = 'Ag�ncia'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbConta: TLabel
    Left = 379
    Top = 284
    Width = 36
    Height = 13
    Caption = 'N� CC'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbValorComprovante: TLabel
    Left = 448
    Top = 352
    Width = 36
    Height = 13
    Caption = 'LbValor'
    OnClick = LBpagoAClick
  end
  object LbExtensoComprovante: TLabel
    Left = 32
    Top = 400
    Width = 101
    Height = 13
    Caption = 'ExtensoComprovante'
    OnClick = LBpagoAClick
  end
  object LbDescricao2: TLabel
    Left = 35
    Top = 417
    Width = 56
    Height = 13
    Caption = 'Descri��o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    OnClick = LBpagoAClick
  end
  object LbCidadeComprovante: TLabel
    Left = 352
    Top = 432
    Width = 33
    Height = 13
    Caption = 'Cidade'
    OnClick = LBpagoAClick
  end
  object lbDiaComprovante: TLabel
    Left = 440
    Top = 432
    Width = 16
    Height = 13
    Caption = 'Dia'
    OnClick = LBpagoAClick
  end
  object LbMesComprovante: TLabel
    Left = 520
    Top = 432
    Width = 20
    Height = 13
    Caption = 'Mes'
    OnClick = LBpagoAClick
  end
  object LbAnoComprovante: TLabel
    Left = 624
    Top = 432
    Width = 19
    Height = 13
    Caption = 'Ano'
    OnClick = LBpagoAClick
  end
  object Panel: TPanel
    Left = 0
    Top = 496
    Width = 800
    Height = 57
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenu
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object LBLEFT: TLabel
      Left = 8
      Top = 8
      Width = 61
      Height = 13
      Caption = 'Esquerda'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object LBTOP: TLabel
      Left = 8
      Top = 32
      Width = 32
      Height = 13
      Caption = 'Topo'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object edtleft: TMaskEdit
      Left = 72
      Top = 4
      Width = 108
      Height = 21
      TabOrder = 0
      OnExit = edtleftExit
      OnKeyPress = edtleftKeyPress
    end
    object edttop: TMaskEdit
      Left = 72
      Top = 28
      Width = 108
      Height = 21
      TabOrder = 1
      OnExit = edtleftExit
      OnKeyPress = edtleftKeyPress
    end
    object Btsalvar: TBitBtn
      Left = 182
      Top = 3
      Width = 127
      Height = 52
      Caption = 'Gravar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtsalvarClick
    end
    object btimprime: TBitBtn
      Left = 310
      Top = 3
      Width = 127
      Height = 52
      Caption = 'Imprimir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btimprimeClick
    end
  end
end
