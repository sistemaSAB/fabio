unit UObjchequesPortador;
Interface
Uses windows,ibquery,Grids,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,
UobjPortador,Uobjmotivodevolucao,graphics;
Type
   TObjChequesPortador=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Portador         :TObjPortador;
                MotivoDevolucao  :Tobjmotivodevolucao;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit,ComHistoriconaLAnctoPortador,ImprimeLancto:boolean;Plancamento:string):Boolean;
                Function    SalvarChequeRecebido(ComCommit:boolean):Boolean;


                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:Boolean)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaChequeTerceiro      :string;


                Function    Get_PesquisaPortador            :string;
                Function    Get_TituloPesquisaPortador      :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Portador         :string;
                //**********************************************
                Function Get_Comp             :string;
                Function Get_Banco            :string;
                Function Get_Agencia          :string;
                Function Get_C1               :string;
                Function Get_Conta            :string;
                Function Get_C2               :string;
                Function Get_Serie            :string;
                Function Get_NumCheque        :string;
                Function Get_C3               :string;
                Function Get_Cliente1         :string;
                Function Get_CPFCliente1      :string;
                Function Get_Cliente2         :string;
                Function Get_CPFCliente2      :string;
                Function Get_CodigodeBarras   :string;
                Function Get_Valor            :string;
                Function Get_Vencimento       :string;
                Function Get_Chequedoportador :string;
                Function Get_MotivoDevolucao  :string;
                Function Get_Temp             :string;

                //*******************************************
                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Portador         (parametro:string);
                //****************************************************
                Procedure Submit_Comp             (parametro:string);
                Procedure Submit_Banco            (parametro:string);
                Procedure Submit_Agencia          (parametro:string);
                Procedure Submit_C1               (parametro:string);
                Procedure Submit_Conta            (parametro:string);
                Procedure Submit_C2               (parametro:string);
                Procedure Submit_Serie            (parametro:string);
                Procedure Submit_NumCheque        (parametro:string);
                Procedure Submit_C3               (parametro:string);
                Procedure Submit_Cliente1         (parametro:string);
                Procedure Submit_CPFCliente1      (parametro:string);
                Procedure Submit_Cliente2         (parametro:string);
                Procedure Submit_CPFCliente2      (parametro:string);
                Procedure Submit_CodigodeBarras   (parametro:string);
                Procedure Submit_Valor            (parametro:string);
                Procedure Submit_Vencimento       (parametro:string);
                Procedure Submit_Chequedoportador (parametro:string);
                Procedure Submit_MotivoDevolucao  (parametro:string);
                Procedure Submit_Temp             (parametro:string);

                //*******************************************************
                procedure Get_Lista_do_Portador(ParametroPortador: string;ParametroListaGeral: TstringGrid);
                procedure Get_ListaPorPortador     (ParametroPortador: string;ParametroListaGeral: TstringGrid;ComChequedoPortador:boolean;PVencimento:string);overload;
                procedure Get_ListaPorPortador     (ParametroPortador: string;ParametroListaGeral: TstringGrid;ComChequedoPortador:boolean;PVencimentoinicial,PvencimentoFinal:string);overload;

                Function  AtualizaChequePortador(ParametroPortador,ParametroCheque,ParametroPortadorDestino:string;HistoricoLanctoPortador:string;ImprimeOrigem,ImprimeDestino:boolean;PData:string;CodigoLancamentotransferencia:string):boolean;
                Function  Get_NovoCodigo:string;
                Function  BaixaCheque(Parametro:string;ParametroPortador:string;ParametroData:string;PnumeroCheque:string;HistoricoPagamento:string;Plancamento:string;out PcodigoTransferencia:string):Boolean;
                Procedure Imprime_Conciliacao(ParametroPortador:string;Ordenacao:string; PNumeroRelatorio:string);
                Procedure ImprimeListaCheques(PPortador:string;Pvencimentoinicial,Pvencimentofinal:string;PNumeroRelatorio:string);

                Function  Soma_Cheques_Terceiro_Portador(Pportador:string):currency;
                //********************************************************
                procedure EdtMotivoDevolucaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

         Private
               ObjDataset:Tibdataset;
               ObjqueryLocal:TIbquery;

               CODIGO           :String[09];

               //*************************************
               Comp             :String[5];
               Banco            :String[5];
               Agencia          :String[10];
               C1               :String[5];
               Conta            :String[25];
               C2               :String[5];
               Serie            :String[5];
               NumCheque        :String[25];
               C3               :String[5];
               Cliente1         :String[50];
               CPFCliente1      :String[20];
               Cliente2         :String[50];
               CPFCliente2      :String[20];
               CodigodeBarras   :String[50];
               Valor            :String[9];
               Vencimento       :String[10];
               Chequedoportador :String[01];
               Temp:string;  // Criado por F�biop
                           // Campo espec�fico para o Sitema BIA
                           // Criado para Resolver um problema da Imobiliaria contato
                           // Ele me indicar� se o cheque j� foi compensado


               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Function  AtualizaPortador(ComHistoriconaLAnctoPortador,ImprimeLancto:Boolean;Plancamento:string):boolean;
               Function  VerificaSeSofreuTransferencia(Pcodigo:string;out NumTransf:String):boolean;
               Function  ExcluiTransferencias(Pcodigo:string;ComCommit:boolean):boolean;


   End;


implementation
uses UobjLanctoPortador,rdprint,stdctrls,SysUtils,Dialogs,UDatamodulo,Controls,
Uobjgeratransferencia,Uobjtransferenciaportador,UObjlancamento,
ufiltraimp,ureltxtrdprint,uobjtitulo,UObjvaloresTransferenciaPortador,
  Upesquisa, UMotivoDevolucao;


Procedure  TObjChequesPortador.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;

        If (Self.Portador.LocalizaCodigo(FieldByName('Portador').asstring)=False)
        Then Begin
                  Messagedlg('Portador N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.Portador.TabelaparaObjeto;

        If (FieldByName('MotivoDevolucao').asstring<>'')
        Then Begin
                If (Self.MotivoDevolucao.LocalizaCodigo(FieldByName('MotivoDevolucao').asstring)=False)
                Then Begin
                        Messagedlg('Motivo de Devolu��o n�o encontrado!',mterror,[mbok],0);
                        Self.ZerarTabela;
                        exit;
                End
                Else Self.MotivoDevolucao.TabelaparaObjeto;
        End;


        Self.Comp             :=FieldByName('Comp').asstring;
        Self.Banco            :=FieldByName('Banco').asstring;
        Self.Agencia          :=FieldByName('Agencia').asstring;
        Self.C1               :=FieldByName('C1').asstring;
        Self.Conta            :=FieldByName('Conta').asstring;
        Self.C2               :=FieldByName('C2').asstring;
        Self.Serie            :=FieldByName('Serie').asstring;
        Self.NumCheque        :=FieldByName('NumCheque').asstring;
        Self.C3               :=FieldByName('C3').asstring;
        Self.Cliente1         :=FieldByName('Cliente1').asstring;
        Self.CPFCliente1      :=FieldByName('CPFCliente1').asstring;
        Self.Cliente2         :=FieldByName('Cliente2').asstring;
        Self.CPFCliente2      :=FieldByName('CPFCliente2').asstring;
        Self.CodigodeBarras   :=FieldByName('CodigodeBarras').asstring;
        Self.Valor            :=FieldByName('Valor').asstring;
        Self.Vencimento       :=FieldByName('Vencimento').asstring;
        Self.Chequedoportador :=FieldByName('Chequedoportador').asstring;
        Self.Temp             :=fieldbyname('Temp').AsString;
    End;
end;


Procedure TObjChequesPortador.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring           :=        Self.CODIGO                    ;
      FieldByName('Portador').asstring         :=        Self.Portador.Get_codigo       ;
      FieldByName('Comp').asstring             :=Self.Comp             ;
      FieldByName('Banco').asstring            :=Self.Banco            ;
      FieldByName('Agencia').asstring          :=Self.Agencia          ;
      FieldByName('C1').asstring               :=Self.C1               ;
      FieldByName('Conta').asstring            :=Self.Conta            ;
      FieldByName('C2').asstring               :=Self.C2               ;
      FieldByName('Serie').asstring            :=Self.Serie            ;
      FieldByName('NumCheque').asstring        :=Self.NumCheque        ;
      FieldByName('C3').asstring               :=Self.C3               ;
      FieldByName('Cliente1').asstring         :=Self.Cliente1         ;
      FieldByName('CPFCliente1').asstring      :=Self.CPFCliente1      ;
      FieldByName('Cliente2').asstring         :=Self.Cliente2         ;
      FieldByName('CPFCliente2').asstring      :=Self.CPFCliente2      ;
      FieldByName('CodigodeBarras').asstring   :=Self.CodigodeBarras   ;
      FieldbyName('Valor').asstring            :=Self.Valor            ;
      FieldbyName('Vencimento').asstring       :=Self.Vencimento       ;
      FieldbyName('Chequedoportador').asstring :=Self.Chequedoportador ;
      FieldbyName('MotivoDevolucao').asstring  :=Self.MotivoDeVolucao.Get_codigo;
      FieldbyName('Temp').AsString             :=Self.Temp;
  End;

End;

//***********************************************************************

function TObjChequesPortador.Salvar(ComCommit,ComHistoriconaLAnctoPortador,ImprimeLancto:boolean;Plancamento:string): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;


if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If (Self.Status=dsInsert)
 Then Begin
        If (Self.AtualizaPortador(ComHistoriconaLAnctoPortador,ImprimeLancto,Plancamento)=False)
        Then Begin
                FDataModulo.IBTransaction.RollbackRetaining;
                result:=false;
                exit;
        End;
 End;

 If (ComCommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjChequesPortador.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Portador.ZerarTabela;
        Self.Valor            :='';
        Self.Comp             :='';
        Self.Banco            :='';
        Self.Agencia          :='';
        Self.C1               :='';
        Self.Conta            :='';
        Self.C2               :='';
        Self.Serie            :='';
        Self.NumCheque        :='';
        Self.C3               :='';
        Self.Cliente1         :='';
        Self.CPFCliente1      :='';
        Self.Cliente2         :='';
        Self.CPFCliente2      :='';
        Self.CodigodeBarras   :='';
        Self.vencimento       :='';
        Self.Chequedoportador :='';
        Self.MotivoDevolucao.ZerarTabela;
        Self.temp:='';
     End;
end;

Function TObjChequesPortador.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Portador.Get_codigo='')
       Then Mensagem:=mensagem+'/Portador';

       If (Valor='')
       Then Mensagem:=mensagem+'/Valor';

       If (Vencimento='')
       Then Mensagem:=mensagem+'/Vencimento';



       If (ChequedoPortador='')
       Then Begin
                 Chequedoportador:='N';
            End;    

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjChequesPortador.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     If (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';

     If (MotivoDevolucao.Get_codigo<>'')
     Then Begin
               If (Self.MotivoDevolucao.LocalizaCodigo(Self.MotivoDevolucao.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Motivo de Devolu��o n�o Encontrado!';
     End;


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjChequesPortador.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;


     try
        Inteiros:=Strtoint(Self.Portador.get_codigo);
     Except
           Mensagem:=mensagem+'/Portador';
     End;

     If (Self.MotivoDevolucao.Get_CODIGO<>'')
     Then BEgin
                try
                   Inteiros:=Strtoint(Self.MotivoDevolucao.get_codigo);
                Except
                        Mensagem:=mensagem+'/Motivo de Devolu��o';
                End;
     End;


     try
        Reais:=StrtoFloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjChequesPortador.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     Try
        datas:=Strtodate(Self.vencimento);
     Except
           mensagem:=mensagem+'\Vencimento';
     End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjChequesPortador.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;
     

function TObjChequesPortador.LocalizaCodigo(parametro: string): boolean;//ok
begin
       result:=false;
       if parametro=''
       then exit;
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Portador,Comp,Banco,Agencia,C1,');
           SelectSql.add('Conta,C2,Serie,NumCheque,C3,Cliente1,');
           SelectSql.add('CPFCliente1,Cliente2,CPFCliente2,CodigodeBarras,Valor,Vencimento,ChequedoPortador,MotivoDevolucao,Temp');
           SelectSql.add('from Tabchequesportador where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjChequesPortador.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

Function TObjChequesPortador.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
SaldoInicial:string;
SaldoInicialF,SaldofinalF:Currency;
NumTransf:String;
begin
     Try
        result:=true;

        If (Self.LocalizaCodigo(Pcodigo)=False)
        Then Begin
                  Messagedlg('C�digo N�o Encontrado na Tabela de Cheques por Portador para ser exclu�do',mterror,[mbok],0);
                  result:=False;
                  Exit;
             End
        Else Self.TabelaparaObjeto;

        If (self.VerificaSeSofreuTransferencia(Pcodigo,NumTransf)=True)
        Then Begin
                  If (Messagedlg('O Cheque N� '+Self.NumCheque+' de valor R$ '+Self.Valor+' sofreu a(s) transfer�ncia(s) N�(s) '+NumTransf+' deseja excluir este cheque desta(s) transfer�ncia(s)?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  Then Begin
                            result:=False;
                            exit;
                  End
                  Else Begin
                          If (Self.ExcluiTransferencias(Pcodigo,ComCommit)=false)
                          Then Begin
                                    result:=False;
                                    exit;
                          End;
                  End;
        End;

        Self.ObjDataset.delete;

        If (ComCommit=True)
        Then FDataModulo.IBTransaction.CommitRetaining;
        
     Except
           result:=false;
     End;
end;


constructor TObjChequesPortador.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ObjqueryLocal:=TIbquery.create(nil);
        Self.ObjqueryLocal.Database:=FDataModulo.IbDatabase;
        Self.Portador:=TObjPortador.create;

        Self.MotivoDevolucao:=TObjMotivoDevolucao.create;
        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Portador,Comp,Banco,Agencia,C1,');
                SelectSql.add('Conta,C2,Serie,NumCheque,C3,Cliente1,');
                SelectSql.add('CPFCliente1,Cliente2,CPFCliente2,CodigodeBarras,Valor,Vencimento,ChequedoPortador,MotivoDevolucao, Temp from TabchequesPortador where codigo=0');


                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert Into Tabchequesportador (CODIGO,Portador,Comp,Banco,Agencia,C1,');
                InsertSQL.add('Conta,C2,Serie,NumCheque,C3,Cliente1,');
                InsertSQL.add('CPFCliente1,Cliente2,CPFCliente2,CodigodeBarras,Valor,Vencimento,ChequedoPortador,MotivoDevolucao,Temp) values ');
                InsertSQL.add('(:CODIGO,:Portador,:Comp,:Banco,:Agencia,:C1,');
                InsertSQL.add(':Conta,:C2,:Serie,:NumCheque,:C3,:Cliente1,');
                InsertSQL.add(':CPFCliente1,:Cliente2,:CPFCliente2,:CodigodeBarras,:Valor,:Vencimento,:ChequedoPortador,:MotivoDevolucao,:Temp)');


                ModifySQL.clear;
                ModifySQL.add('Update TabchequesPortador set ');
                ModifySQL.add('CODIGO=:CODIGO,Portador=:Portador,Comp=:Comp,Banco=:Banco,Agencia=:Agencia,C1=:C1,');
                ModifySQL.add('Conta=:Conta,C2=:C2,Serie=:Serie,NumCheque=:NumCheque,C3=:C3,Cliente1=:Cliente1,');
                ModifySQL.add('CPFCliente1=:CPFCliente1,Cliente2=:Cliente2,CPFCliente2=:CPFCliente2,CodigodeBarras=:CodigodeBarras,Valor=:valor,Vencimento=:Vencimento,ChequedoPortador=:ChequedoPortador,MotivoDevolucao=:MotivoDevolucao,Temp=:Temp where codigo=:codigo');



                DeleteSQL.clear;
                DeleteSQL.add(' Delete from TabchequesPortador where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Portador,Comp,Banco,Agencia,C1,');
                RefreshSQL.add('Conta,C2,Serie,NumCheque,C3,Cliente1,');
                RefreshSQL.add('CPFCliente1,Cliente2,CPFCliente2,CodigodeBarras,Valor,Vencimento,ChequedoPortador,MotivoDevolucao,Temp from TabchequesPortador where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjChequesPortador.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjChequesPortador.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjChequesPortador.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjChequesPortador.Get_Pesquisa: string;
begin
     Result:=' Select * from TabChequesPortador ';
end;

function TObjChequesPortador.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Cheques de Portador ';
end;


function TObjChequesPortador.Get_Portador: string;
begin
     Result:=Self.Portador.Get_CODIGO;
end;


procedure TObjChequesPortador.Submit_Portador(parametro: string);
begin
     Self.portador.Submit_CODIGO(parametro);
end;


function TObjChequesPortador.Get_PesquisaPortador: string;
begin
     Result:=Self.Portador.Get_Pesquisa;
end;


function TObjChequesPortador.Get_TituloPesquisaPortador: string;
begin
     Result:=Self.Portador.Get_TituloPesquisa;
end;


Function TObjChequesPortador.AtualizaPortador(ComHistoriconaLAnctoPortador,ImprimeLancto: Boolean;Plancamento:string):boolean;
var
SaldoInicial:string;
SaldoInicialF,SaldoFinalF:Currency;
HistoricoLanc:string;
ObjLancamento:tobjlancamento;
begin
        If (Plancamento<>'')
        Then Begin
                  Try
                     ObjLancamento:=TobjLancamento.Create;
                  Except
                        Messagedlg('Erro na Tentativa de Resgate do hist�rico do Lan�amento!',mterror,[mbok],0);
                        Result:=False;
                        exit;
                  End;
                  Try
                      If (objlancamento.LocalizaCodigo(Plancamento)=False)
                      Then Begin
                                Messagedlg('Lan�amento n�o Localizado : '+Plancamento,mterror,[mbok],0);
                                Result:=False;
                                exit;
                           End;
                      objlancamento.TabelaparaObjeto;
                      HistoricoLanc:='';
                      HistoricoLanc:=ObjLancamento.get_Historico;
                  Finally
                         ObjLancamento.free;
                  End;
        End;


        If (Self.Portador.LocalizaCodigo(self.portador.get_codigo)=False)
        Then Begin
                  result:=False;
                  Messagedlg('Portador N�o encontrado para atualizar o Saldo',mterror,[mbok],0);
                  exit;
             End;


        Self.Portador.TabelaparaObjeto;
        //Mando Aumentar o Saldo mas n�o dou Commit, pois vou dar aqui
        If Self.Status=dsinsert
        Then Begin
                If (Plancamento='')
                Then ObjlanctoportadorGlobal.Submit_Historico('CHEQUE LAN�ADO-> '+Self.codigo)
                Else ObjlanctoportadorGlobal.Submit_Historico('CH.REC.'+HistoricoLanc);
        End;

        ObjlanctoportadorGlobal.Submit_Data(Datetostr(date));
        ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABCHEQUESPORTADOR');
        ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJCHEQUESPORTADOR');
        ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
        ObjlanctoportadorGlobal.Submit_ValordoCampo(Self.codigo);
        ObjlanctoportadorGlobal.Submit_ImprimeRel(ImprimeLancto);
        ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(plancamento);

        If ComHistoriconaLAnctoPortador=False
        Then Begin
                ObjlanctoportadorGlobal.status:=dsinactive;
                //NO caso de pagamento de contas com cheque proprio
                //tenho que lancar um cheque na tabchequesportador
                //mas naoum tenho necessidade de gerar um objlantoportador pois o valor=0
                //depois editado para passar para o valor real
                (*If (Self.Portador.CreditaSaldo(Self.Portador.Get_codigo,Self.valor,False)=False)
                Then Begin
                        result:=False;
                        exit;
                     End;*)
             End
        Else Begin

                If (ObjlanctoportadorGlobal.AumentaSaldo(Self.Portador.Get_codigo,Self.valor,False)=False)
                Then Begin
                        result:=False;
                        exit;
                     End;
             End;

        result:=True;
End;



procedure TObjChequesPortador.Get_ListaPorPortador(
  ParametroPortador: string; ParametroListaGeral: TstringGrid;
  ComChequedoPortador: boolean;PVencimento:string);
var
Cont,linha:Integer;
TmpData:Tdate;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('Select NumCheque,Valor,Vencimento,Cliente1,Comp,Banco,Agencia,C1,');
          SelectSql.add('Conta,C2,Serie,C3,');
          SelectSql.add('CPFCliente1,Cliente2,CPFCliente2,CodigodeBarras,ChequedoPortador,MotivoDevolucao,CODIGO,Portador');
          SelectSql.add('from Tabchequesportador');

          if (ComChequedoPortador=False)
          Then SelectSQL.add(' where tabchequesportador.portador='+ParametroPortador+' and not ChequedoPortador=''S''')
          Else SelectSQL.add(' where tabchequesportador.portador='+ParametroPortador+' and ChequedoPortador=''S''');

          Try
             If (Pvencimento<>'')
             Then Begin
                     TmpData:=StrToDate(PVencimento);
                     SelectSQL.add(' and vencimento<='+#39+FormatDateTime('mm/dd/yyyy',TmpData)+#39);
             End;
          Except
          End;

          open;
          last;



          //somo um a mais nas colunas para a coluna 0 onde ficara o check
          //e um a mais nas linhas para linha 0 do T�tulo
          ParametroListaGeral.ColCount:=Self.ObjDataset.FieldList.Count+1;
          if (RecordCount=0)
          Then ParametroListaGeral.RowCount:=RecordCount+2
          Else ParametroListaGeral.RowCount:=RecordCount+1;
          ParametroListaGeral.cols[0].Clear;
          if (recordcount=0)
          Then Begin
                    ParametroListaGeral.Rows[0].clear;
                    ParametroListaGeral.Rows[1].clear;
                    exit;
          End;



          //dando nome as colunas
          for cont:=0 to FieldCount-1 do
          Begin
               ParametroListaGeral.Cells[cont+1,0]:=Self.ObjDataset.Fields[cont].DisplayName;
          End;


          
          first;

          linha:=1;
          While not(eof) do
          Begin
               for cont:=0 to FieldCount-1 do
               Begin
                    If (UPPERCASE(Fields[cont].DisplayName)='VALOR')
                    Then ParametroListaGeral.Cells[cont+1,linha]:=formata_valor(Fields[cont].AsString)
                    Else ParametroListaGeral.Cells[cont+1,linha]:=Fields[cont].AsString;
               End;
               inc(linha,1);
               next;
          End;
          
     End;



end;


procedure TObjChequesPortador.Get_ListaPorPortador(
  ParametroPortador: string; ParametroListaGeral: TstringGrid;
  ComChequedoPortador: boolean;PvencimentoInicial,PVencimentoFinal:string);
var
Cont,linha:Integer;
TmpData:Tdate;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('Select NumCheque,Valor,Vencimento,Cliente1,Comp,Banco,Agencia,C1,');
          SelectSql.add('Conta,C2,Serie,C3,');
          SelectSql.add('CPFCliente1,Cliente2,CPFCliente2,CodigodeBarras,ChequedoPortador,MotivoDevolucao,CODIGO,Portador');
          SelectSql.add('from Tabchequesportador');

          if (ComChequedoPortador=False)
          Then SelectSQL.add(' where tabchequesportador.portador='+ParametroPortador+' and not ChequedoPortador=''S''')
          Else SelectSQL.add(' where tabchequesportador.portador='+ParametroPortador+' and ChequedoPortador=''S''');

          Try
             If (PvencimentoInicial<>'')
             Then Begin
                     TmpData:=StrToDate(PVencimentoInicial);
                     SelectSQL.add(' and vencimento>='+#39+FormatDateTime('mm/dd/yyyy',TmpData)+#39);
             End;
          Except
          End;


          Try
             If (PvencimentoFinal<>'')
             Then Begin
                     TmpData:=StrToDate(PVencimentoFinal);
                     SelectSQL.add(' and vencimento<='+#39+FormatDateTime('mm/dd/yyyy',TmpData)+#39);
             End;
          Except
          End;

          SelectSQL.add(' order by vencimento,valor');

          open;
          last;



          //somo um a mais nas colunas para a coluna 0 onde ficara o check
          //e um a mais nas linhas para linha 0 do T�tulo
          ParametroListaGeral.ColCount:=Self.ObjDataset.FieldList.Count+1;
          if (RecordCount=0)
          Then ParametroListaGeral.RowCount:=RecordCount+2
          Else ParametroListaGeral.RowCount:=RecordCount+1;
          ParametroListaGeral.cols[0].Clear;
          if (recordcount=0)
          Then Begin
                    ParametroListaGeral.Rows[0].clear;
                    ParametroListaGeral.Rows[1].clear;
                    exit;
          End;



          //dando nome as colunas
          for cont:=0 to FieldCount-1 do
          Begin
               ParametroListaGeral.Cells[cont+1,0]:=Self.ObjDataset.Fields[cont].DisplayName;
          End;


          
          first;

          linha:=1;
          While not(eof) do
          Begin
               for cont:=0 to FieldCount-1 do
               Begin
                    If (UPPERCASE(Fields[cont].DisplayName)='VALOR')
                    Then ParametroListaGeral.Cells[cont+1,linha]:=formata_valor(Fields[cont].AsString)
                    Else ParametroListaGeral.Cells[cont+1,linha]:=Fields[cont].AsString;
               End;
               inc(linha,1);
               next;
          End;
          
     End;



end;


procedure TObjChequesPortador.Get_Lista_do_Portador(ParametroPortador: string;ParametroListaGeral: TstringGrid);
var
Cont,linha:Integer;
begin

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add(' Select * from tabchequesportador');
          SelectSQL.add(' where tabchequesportador.portador='+ParametroPortador);
          SelectSQL.add(' and tabchequesportador.chequedoportador=''S'' ');
                    
          open;
          last;

          //somo um a mais nas colunas para a coluna 0 onde ficara o check
          //e um a mais nas linhas para linha 0 do T�tulo
          ParametroListaGeral.ColCount:=Self.ObjDataset.FieldList.Count+1;
          ParametroListaGeral.RowCount:=RecordCount+1;

          //dando nome as colunas
          for cont:=0 to FieldCount-1 do
          Begin
               ParametroListaGeral.Cells[cont+1,0]:=Self.ObjDataset.Fields[cont].DisplayName;
          End;
          
          first;

          linha:=1;
          While not(eof) do
          Begin
               for cont:=0 to FieldCount-1 do
               Begin
                    If (UPPERCASE(Fields[cont].DisplayName)='VALOR')
                    Then ParametroListaGeral.Cells[cont+1,linha]:=formata_valor(Fields[cont].AsString)
                    Else ParametroListaGeral.Cells[cont+1,linha]:=Fields[cont].AsString;
               End;
               inc(linha,1);
               next;
          End;
          
     End;
end;



//Passo o cheque da Origem para o destino e atualizo o Saldo
function TObjChequesPortador.AtualizaChequePortador(ParametroPortador,ParametroCheque, ParametroPortadorDestino: string;HistoricoLanctoPortador:string;ImprimeOrigem,ImprimeDestino:boolean;PData:string;CodigoLancamentotransferencia:string): boolean;
begin

    //Verificando o Registro na TabchequePortador
    If (Self.LocalizaCodigo(ParametroCheque)=False)
    Then Begin
              Messagedlg('Cheque n�o encontrado->'+ParametroCheque+' Na Tabela de Cheques por Portador',mterror,[mbok],0);
              result:=False;
              exit;
         End;
    Self.TabelaparaObjeto;

    //Antes de qualquer coisa verificar se o cheque naum � um ch. devolvido

    If(Self.MotivoDevolucao.get_codigo<>'')
    Then Begin
                If (Self.MotivoDevolucao.Get_Reapresenta='N')
                Then Begin
                          Messagedlg('O Cheque n�o pode ser transferido, pois foi devolvido por motivo que n�o aceita reapresenta��o!',mterror,[mbok],0);
                          Result:=False;
                          exit;
                End;
    End;

    //Preciso Verificar se existe o Portador Destino e Atualizar Saldo creditando o Valor do Cheque
    If (Self.Portador.LocalizaCodigo(ParametroPortadorDestino)=False)
    Then Begin
              Messagedlg('Portador Destino n�o encontrado->'+ParametroPortadorDestino+' na tabela de Portadores!',mterror,[mbok],0);
              result:=False;
              exit;
         End;
    Self.Portador.TabelaparaObjeto;

    //Self.ObjLanctoPortador.Submit_Historico(HistoricoLanctoPortador+'-CR CH N� '+Self.codigo);
    ObjlanctoportadorGlobal.Submit_Historico(HistoricoLanctoPortador);
    ObjlanctoportadorGlobal.Submit_Data(PData);
    ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABCHEQUESPORTADOR');
    ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJCHEQUESPORTADOR');
    ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
    ObjlanctoportadorGlobal.Submit_ValordoCampo(Self.Codigo);
    ObjlanctoportadorGlobal.Submit_ImprimeRel(ImprimeDestino);
    ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(CodigoLancamentotransferencia);

    If (ObjlanctoportadorGlobal.AumentaSaldo(Self.Portador.Get_codigo,Self.Valor,False)=False)
    Then Begin
              Messagedlg('Erro ao tentar Creditar do Saldo Destino',mterror,[mbok],0);
              result:=False;
              exit;
    End;
    //**************************************
    //Diminuindo o Saldo do Portador Origem
    If (Self.Portador.LocalizaCodigo(ParametroPortador)=False)
    Then Begin
              Messagedlg('Portador n�o encontrado->'+Self.Portador.Get_CODIGO+' na tabela de Portadores!',mterror,[mbok],0);
              result:=False;
              exit;
         End;
    Self.Portador.TabelaparaObjeto;

    //ObjlanctoportadorGlobal.Submit_Historico(HistoricoLanctoPortador+'-DB CH N� '+Self.codigo);
    ObjlanctoportadorGlobal.Submit_Historico(HistoricoLanctoPortador);
    ObjlanctoportadorGlobal.Submit_Data(PData);
    ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABCHEQUESPORTADOR');
    ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJCHEQUESPORTADOR');
    ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
    ObjlanctoportadorGlobal.Submit_ValordoCampo(Self.Codigo);
    ObjlanctoportadorGlobal.Submit_ImprimeRel(ImprimeOrigem);
    ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(CodigoLancamentotransferencia);
    
    If (ObjlanctoportadorGlobal.DiminuiSaldo(Self.Portador.Get_codigo,Self.Valor,false)=False)
    Then Begin
              Messagedlg('Erro ao tentar debitar do Saldo Origem',mterror,[mbok],0);
              result:=False;
              exit;
         End;
    //*********************************************
    //E por final trocando na Tabela de ChequePorPortador a Origem e o Destino
    Self.Portador.Submit_CODIGO(ParametroPortadorDestino);
    Self.Status:=dsedit;
    If (Self.Salvar(False,False,False,'')=False)
    Then Begin
              Messagedlg('Erro Ao Tentar Trocar o Portador Origem para Destino na Tabela de Cheque por Portadores',mterror,[mbok],0);
              result:=False;
              exit;
    End;

    result:=True;
end;




function TObjChequesPortador.Get_Agencia: string;
begin
     Result:=Self.Agencia;
end;

function TObjChequesPortador.Get_Banco: string;
begin
     Result:=Self.Banco;
end;

function TObjChequesPortador.Get_C1: string;
begin
     Result:=Self.C1;
end;

function TObjChequesPortador.Get_C2: string;
begin
     Result:=Self.C2;
end;

function TObjChequesPortador.Get_C3: string;
begin
     Result:=Self.c3;
end;

function TObjChequesPortador.Get_Cliente1: string;
begin
     Result:=Self.cliente1;
end;

function TObjChequesPortador.Get_Cliente2: string;
begin
     Result:=Self.cliente2;
end;

function TObjChequesPortador.Get_CodigodeBarras: string;
begin
     Result:=Self.codigodebarras;
end;

function TObjChequesPortador.Get_Comp: string;
begin
     Result:=Self.comp;
end;

function TObjChequesPortador.Get_Conta: string;
begin
     Result:=Self.conta;
end;

function TObjChequesPortador.Get_CPFCliente1: string;
begin
     Result:=Self.cpfcliente1;
end;

function TObjChequesPortador.Get_CPFCliente2: string;
begin
     Result:=Self.CPFCliente2;
end;

function TObjChequesPortador.Get_NumCheque: string;
begin
     Result:=Self.NumCheque;
end;

function TObjChequesPortador.Get_Serie: string;
begin
     Result:=Self.Serie
end;

procedure TObjChequesPortador.Submit_Agencia(parametro: string);
begin
     Self.Agencia:=Parametro
end;

procedure TObjChequesPortador.Submit_Banco(parametro: string);
begin
     Self.Banco:=Parametro
end;

procedure TObjChequesPortador.Submit_C1(parametro: string);
begin
     Self.C1:=Parametro
end;

procedure TObjChequesPortador.Submit_C2(parametro: string);
begin
     Self.c2:=Parametro
end;

procedure TObjChequesPortador.Submit_C3(parametro: string);
begin
     Self.c3:=Parametro
end;

procedure TObjChequesPortador.Submit_Cliente1(parametro: string);
begin
     Self.cliente1:=Parametro
end;

procedure TObjChequesPortador.Submit_Cliente2(parametro: string);
begin
     Self.Cliente2:=Parametro
end;

procedure TObjChequesPortador.Submit_CodigodeBarras(parametro: string);
begin
     Self.CodigodeBarras:=Parametro
end;

procedure TObjChequesPortador.Submit_Comp(parametro: string);
begin
     Self.Comp:=Parametro
end;

procedure TObjChequesPortador.Submit_Conta(parametro: string);
begin
     Self.Conta:=Parametro
end;

procedure TObjChequesPortador.Submit_CPFCliente1(parametro: string);
begin
     Self.cpfcliente1:=Parametro
end;

procedure TObjChequesPortador.Submit_CPFCliente2(parametro: string);
begin
     Self.cpfcliente2:=Parametro
end;

procedure TObjChequesPortador.Submit_NumCheque(parametro: string);
begin
     Self.numcheque:=Parametro
end;

procedure TObjChequesPortador.Submit_Serie(parametro: string);
begin
     Self.serie:=Parametro
end;

function TObjChequesPortador.Get_Valor: string;
begin
     Result:=Self.valor;
end;

procedure TObjChequesPortador.Submit_Valor(parametro: string);
begin
     Self.valor:=Parametro
end;

function TObjChequesPortador.Get_Vencimento: string;
begin
     Result:=Self.Vencimento;
end;

procedure TObjChequesPortador.Submit_Vencimento(parametro: string);
begin
     Self.Vencimento:=parametro;
end;


function TObjChequesPortador.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_CHEQUESPORTADOR';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Modelo',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;



function TObjChequesPortador.BaixaCheque(Parametro: string;ParametroPortador:string;ParametroData:string;PnumeroCheque:string;HistoricoPagamento:string;Plancamento:string;out PcodigoTransferencia:string): Boolean;
var
TempTStrings:TStrings;
TempObjGeraTransferencia:TObjGeraTransferencia;
TempObjTransferenciaPortador:TObjTransferenciaPortador;
ident:word;
tmpcomplemento:string;
PusaFraseBxCheque:boolean;

begin
     result:=False;
     
     try
       ident:=0;
       TempObjTransferenciaPortador:=TObjTransferenciaPortador.create;
       ident:=1;
       TempObjgeraTransferencia:=TObjgeraTransferencia.create;
       ident:=2;
       TempTStrings:=TStringList.create;
    Except
          If (ident=0)
          Then Messagedlg('N�o foi poss�vel gerar o Objeto de Transfer�ncia de Portador',mterror,[mbok],0);
          if (ident=1)
          then Begin
                    TempObjTransferenciaPortador.free;
                    Messagedlg('Erro Na Cria��o do Objeto de Gera��o de Transfer�ncia para Lan�amento da Transfer�ncia',mterror,[mbok],0);
          End;
          if (ident=2)
          then Begin
                  TempObjTransferenciaPortador.free;
                  TempObjgeraTransferencia:=TObjgeraTransferencia.create;
                  Messagedlg('Erro na tentativa de Cria��o da TStrings para enviar o C�digo do Cheque para Transfer�ncia',mterror,[mbok],0);
          End;
          result:=False;
          exit;
    End;

    Try
    
        //tenho que ver se o cheque n�o est� em uma conta diferente do portador original, ou seja
        //portador de contas pagas
        //se ja tiver n�o h� jeito, caso contrario mando para ela
        //Localizando o Cheque

        If Self.LocalizaCodigo(Parametro)=False
        Then Begin
                  Messagedlg('Cheque N�o localizado na Tabchequesportador',mterror,[mbok],0);
                  exit;
        End;

        Self.TabelaparaObjeto;

        If (Self.Portador.Get_CODIGO<>ParametroPortador)
        Then Begin
               Messagedlg('O Cheque n�o se encontra em seu portador original, provavelmente ja foi baixado ou o cheque foi devolvido!',mterror,[mbok],0);
               exit;
        End;

        {Lanco uma transferencia deste cheque do seu portador para uma Conta de Cheques Debitados}
        //Preciso saber qual o grupo para a transferencia
        //criar campos na tabgeratransferencia e localizar pelo historico
        If (TempObjGeraTransferencia.LocalizaHistorico('CHEQUE DEBITADO NO BANCO')=False)
        Then Begin
                  Messagedlg('N�o foi encontrado o gerador de Transfer�ncia com Hist�rico->#CHEQUE DEBITADO NO BANCO#',mterror,[mbok],0);
                  exit;
        End;

        TempObjGeraTransferencia.TabelaparaObjeto;
        TempObjTransferenciaPortador.status:=dsinsert;

        PCodigoTransferencia:=TempObjTransferenciaPortador.Get_NovoCodigo;
        
        TempObjTransferenciaPortador.Submit_CODIGO(Pcodigotransferencia);
        TempObjTransferenciaPortador.Submit_Data(ParametroData);
        TempObjTransferenciaPortador.Submit_Valor('0');
        TempObjTransferenciaPortador.Submit_Documento('0');
        //aqui eu posso usar o parametro de se na baixa do cheque aparece o historico
        //do lancamento que o gerou ou se aparece o HistoricoGrupo+Numero do cheque
        //na tabtransferencia ele usa o historico do grupo + campo complemento
        //basta deixar o complemento como quiser
        //e zerar o historico do grupo se necessario
        //O Historico do Lancamento vem e parametro, pois � ObjTalaodeCheques que chamou essa funcao
        //e passou os parametros
        tmpcomplemento:='';


        //*****VERIFICANDO SE � PARA APARECER A FRASE BX CH N� XXXXX******
        PusaFraseBxCheque:=true;
        //usa sim como padrao caso o parametro nao tenha sido encontrado
        If (ObjParametroGlobal.ValidaParametro('UTILIZA FRASE "BX CH N�" NA COMPENSACAO DO CHEQUE')=true)
        then Begin
                  if (ObjParametroGlobal.Get_Valor<>'SIM')
                  Then PusaFraseBxCheque:=False;
        End;
        //*****************************************
        
        If (ObjParametroGlobal.ValidaParametro('UTILIZA HISTORICO DO LANCAMENTO NA BAIXA DO CHEQUE')=True)
        Then Begin
                if (ObjParametroGlobal.get_valor='SIM')
                Then Begin
                          tmpcomplemento:='';
                
                          if (PusaFraseBxCheque=True)
                          Then tmpcomplemento:='BX CH N� '+PNumeroCheque+' - ';
                
                          tmpcomplemento:=tmpcomplemento+HistoricoPagamento;
                End
                Else Begin                                                                    
                          //Se nao USar o Historico do Lan�amento � Usado o Hist�rico do grupo e o n�mero do cheque
                          tmpcomplemento:=TempObjGeraTransferencia.Grupo.Get_Descricao;
                
                          if (PusaFraseBxCheque=True)
                          Then tmpcomplemento:= tmpcomplemento+' - BX CH N� '+PnumeroCheque;
                End;
        End;
        //essa variavel ira ser usado no lanctoportador
        TempObjTransferenciaPortador.Submit_HistoricoLancamentoPortador(tmpcomplemento);
        //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
        TempObjTransferenciaPortador.Submit_Complemento(tmpcomplemento);
        TempObjTransferenciaPortador.Submit_Grupo(TempObjGeraTransferencia.Get_Grupo);
        TempObjTransferenciaPortador.Submit_PortadorOrigem(Self.Portador.Get_CODIGO);
        TempObjTransferenciaPortador.Submit_PortadorDestino(TempObjGeraTransferencia.Get_Portadordestino);
        TempObjTransferenciaPortador.CodigoLancamento.Submit_Codigo('');
        //Na transferencia se envia um Tstrings com a lista de cheques
        //no meu caso tenho que mandar apenas uma string
        //para isto tenho que criar uma Tstrings s� para ele
        TempTStrings.clear;
        TempTStrings.add(Self.codigo);
        TempObjTransferenciaPortador.Submit_ListaChequesTransferencia(TempTStrings);
        If (TempObjTransferenciaPortador.Salvar(False,True,True,False,False,False,Plancamento)=False)
        Then result:=False
        Else result:=True;
    Finally
           TempObjGeraTransferencia.free;
           TempObjTransferenciaPortador.free;
           Freeandnil(TempTStrings);
    End;
End;

function TObjChequesPortador.Get_Chequedoportador: string;
begin
     Result:=Self.ChequedoPortador;
end;

procedure TObjChequesPortador.Submit_Chequedoportador(parametro: string);
begin
     Self.ChequedoPortador:=Parametro;
end;


destructor TObjChequesPortador.Free;
begin

     Freeandnil(Self.ObjDataset);
     Freeandnil(Self.ObjqueryLocal);

     Self.Portador.free;
     Self.MotivoDevolucao.free;


end;

procedure TObjChequesPortador.Imprime_Conciliacao(
  ParametroPortador: string;Ordenacao:string; PNumeroRelatorio:string);
var
somadoscheques:CuRrency;
SALDOCONTABIL:CURRENCY;
datatmp:Tdate;
comdata:boolean;
SALDOBANCO:Currency;
linha:integer;
begin

     If (ParametroPortador='')
     Then Begin
               Messagedlg('Escolha um Portador para poder Imprimir a concilia��o Banc�ria',mtinformation,[mbok],0);
               exit;
          End;
     If (Self.Portador.LocalizaCodigo(ParametroPortador)=False)
     Then Begin
               Messagedlg('Portador n�o encontrado para gerar a Concilia��o!',mterror,[mbok],0);
               exit;
          End;
     Self.Portador.TabelaparaObjeto;

     //pegando a data se for necessario

     limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          LbGrupo01.caption:='Data Limite';
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          showmodal;
          If tag=0
          Then exit;
          Try
             Comdata:=True;
             DataTMP:=sTrtodate(edtgrupo01.text);
          Except
                Comdata:=False;
          End;
     end;



     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add(' select sum(tabchequesportador.Valor) as SOMA');
          SelectSQL.add(' from tablancamento');
          SelectSQL.add(' join TabTransferenciaPortador on TabTransferenciaPortador.codigolancamento=Tablancamento.codigo');
          SelectSQL.add(' join TabValoresTransferenciaPortador');
          SelectSQL.add(' on Tabvalorestransferenciaportador.transferenciaportador=Tabtransferenciaportador.codigo');
          SelectSQL.add(' join tabchequesportador on TabValoresTransferenciaPortador.Valores=TabChequesportador.codigo');
          SelectSQL.add(' where tabchequesportador.portador='+ParametroPortador);
          SelectSQL.add(' and tabchequesportador.chequedoportador=''S'' ');

          if (ComData=True)
          Then SelectSQL.add(' and tabchequesportador.Vencimento<='+#39+Formatdatetime('mm/dd/yyyy',datatmp)+#39);

          open;
          somadoscheques:=fieldbyname('soma').asfloat;



          close;
          SelectSQL.clear;
          SelectSQL.add(' select cast(tabchequesportador.numcheque as integer) as NUMCHEQUENUMERICO,');
          SelectSQL.add(' tablancamento.codigo AS CODIGOLANCAMENTO,tablancamento.historico AS HISTORICOLANCAMENTO,');
          SelectSQL.add(' tablancamento.data as EMISSAO,');
          SelectSQL.add(' tabchequesportador.Vencimento,tabchequesportador.Valor as VALORCHEQUE');
          SelectSQL.add(' from tablancamento');
          SelectSQL.add(' join TabTransferenciaPortador on TabTransferenciaPortador.codigolancamento=Tablancamento.codigo');
          SelectSQL.add(' join TabValoresTransferenciaPortador');
          SelectSQL.add(' on Tabvalorestransferenciaportador.transferenciaportador=Tabtransferenciaportador.codigo');
          SelectSQL.add(' join tabchequesportador on TabValoresTransferenciaPortador.Valores=TabChequesportador.codigo');
          SelectSQL.add(' where tabchequesportador.portador='+ParametroPortador);
          SelectSQL.add(' and tabchequesportador.chequedoportador=''S'' ');

          if (ComData=True)
          Then SelectSQL.add(' and tabchequesportador.Vencimento<='+#39+Formatdatetime('mm/dd/yyyy',datatmp)+#39);

          If (ordenacao='V')
          Then SelectSQL.add(' order by tabchequesportador.vencimento')
          Else
               If (Ordenacao='C')
               Then SelectSQL.add(' order by 1');//para ordenar numerico

          open;

          If (RecordCount=0)
          Then Begin
                  Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                  exit;
          End;

          SALDOBANCO:=strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo));
          SALDOCONTABIL:=strtofloat(Self.Portador.Get_SaldopelosLancamentos(Self.portador.get_codigo))-somadoscheques;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,PNumeroRelatorio+'CONCILIA��O BANC�RIA',[negrito]);
          inc(linha,1);

          if (comdata=True)
          Then begin
                    FreltxtRDPRINT.RDprint.Impf(linha,1,'LIMITE '+Datetostr(datatmp),[negrito]);
                    inc(linha,1);
          End;

          FreltxtRDPRINT.RDprint.Impf(linha,1,'PORTADOR   = '+Self.Portador.Get_Nome,[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'SALDO ATUAL= '+formata_valor(Self.Portador.Get_SaldopelosLancamentos(Self.portador.Get_CODIGO))+'         CHEQUES: R$ '+formata_valor(floattostr(somadoscheques))+'      SALDO CONT�BIL R$ '+formata_valor(floattostr(SALDOCONTABIL)),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,1,COMPLETAPALAVRA('_',95,'_'),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('LAN�AMENTO',50,' ')+' '+CompletaPalavra('CHEQUE',09,' ')+' '+CompletaPalavra('VENCIMENTO',10,' ')+' '+CompletaPalavra('VALOR',10,' ')+' '+CompletaPalavra('SD BANCO',10,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,1,COMPLETAPALAVRA('_',95,'_'),[negrito]);
          inc(linha,1);

          While not(eof) do
          Begin
               SALDOBANCO:=SALDOBANCO-fieldbyname('valorcheque').asfloat;
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigolancamento').asstring+'-'+fieldbyname('historicolancamento').asstring,50,' ')+
                          ' '+CompletaPalavra(fieldbyname('NUMCHEQUENUMERICO').asstring,09,' ')+
                          ' '+CompletaPalavra(fieldbyname('VENCIMENTO').asstring,10,' ')+' '+
                          CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORcheque').asstring),10,' ')+' '+
                          CompletaPalavra_a_Esquerda(formata_valor(floattostr(SALDOBANCO)),10,' '));
               inc(linha,1);
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,COMPLETAPALAVRA('_',95,'_'),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SALDO CONT�BIL: R$ '+formata_valor(floattostr(SALDOCONTABIL)),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'TOTAL DE REGISTROS: '+inttostr(Self.ObjDataset.recordcount),[negrito]);
          FreltxtRDPRINT.RDprint.Fechar;

     End;

end;




function TObjChequesPortador.Get_MotivoDevolucao: string;
begin
     Result:=Self.MotivoDevolucao.get_codigo;
end;

procedure TObjChequesPortador.Submit_MotivoDevolucao(parametro: string);
begin
     Self.MotivoDevolucao.Submit_CODIGO(parametro);
end;

function TObjChequesPortador.Get_PesquisaChequeTerceiro: string;
begin
     result:='Select * From tabchequesportador where Chequedoportador=''N'' ';
end;

procedure TObjChequesPortador.ImprimeListaCheques(PPortador: string;
  Pvencimentoinicial,Pvencimentofinal: string; PNumeroRelatorio:string);
var
linha:Integer;
ObjTitulo:TObjTitulo;
TmpNomeCliente:String;
ValorTotalCheques:currency;
begin
     Try
        ObjTitulo:=TObjTitulo.create;
     Except
           Messagedlg('Erro na tentativa de Cria��o do Objeto de T�tulo!',mterror,[mbok],0);
           exit;
     End;

     Try
        //ja tenho o portador e o vencimento, basta selecionar e imprimir
        With Self.ObjDataset do
        Begin

             close;
             SelectSQL.clear;
             SelectSQL.add('Select tabchequesportador.portador,tabchequesportador.codigo,tabchequesportador.vencimento,tabchequesportador.valor,tabchequesportador.numcheque,');
             SelectSQL.add('tabvalores.codigo as VALORES,tabvalores.lancamento,tablancamento.pendencia,tabpendencia.titulo,tablancamento.historico as HISTORICOLANCAMENTO');
             SelectSQL.add('from tabchequesportador');
             SelectSQL.add('left join tabvalores on tabvalores.cheque=tabchequesportador.codigo');
             SelectSQL.add('left join tablancamento on Tabvalores.lancamento=tablancamento.codigo');
             SelectSQL.add('left join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
             SelectSQL.add('where not(ChequedoPortador=''S'')');
             if (Pvencimentoinicial<>'')
             Then  SelectSQL.add('and (TabChequesPortador.vencimento>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pvencimentoinicial))+#39+')');
             SelectSQL.add('and (TabChequesPortador.vencimento<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pvencimentofinal))+#39+')');
             SelectSQL.add('and tabchequesportador.portador='+PPortador);
             SelectSQL.add('order by tabchequesportador.vencimento');
             open;
             if (Recordcount=0)
             Then Begin
                       Messagedlg('Nenhum dado foi selecionado na pesquisa!',mtinformation,[mbok],0);
                       exit;
             End;
             first;
             FrelTXTrdprint.ConfiguraImpressao;
             FrelTXTrdprint.RDprint.Abrir;
             linha:=3;
             //titulo
             FrelTXTrdprint.RDprint.ImpC(linha,40,PNumeroRelatorio+'RELAT�RIO DE CHEQUES DE 3� - VENCIMENTO  '+PvencimentoInicial+' - '+PvencimentoFINAL,[NEGRITO]);
             inc(linha,2);
             //cabecalho das colunas
             FrelTXTrdprint.RDprint.ImpF(linha,1,CompletaPalavra('CLIENTE',50,' ')+' '+
                                                 CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                                 CompletaPalavra('N� CHEQUE',15,' ')+' '+
                                                 CompletaPalavra_a_Esquerda('VALOR',12,' '),[NEGRITO]);
             inc(linha,1);
             ValorTotalCheques:=0;
             While not(eof) do
             Begin
                  if (linha>=(FreltxtRDPRINT.RDprint.TamanhoQteLinhas-3))
                  Then Begin
                            linha:=3;
                            FreltxtRDPRINT.RDprint.Novapagina;
                  End;
                  if (fieldbyname('titulo').asstring<>'')
                  then Begin
                        If (ObjTitulo.LocalizaCodigo(fieldbyname('titulo').asstring)=True)
                        Then Begin
                                ObjTitulo.TabelaparaObjeto;
                                TmpNomeCliente:=ObjTitulo.CREDORDEVEDOR.Get_NomeCredorDevedor(ObjTitulo.CREDORDEVEDOR.get_codigo,ObjTitulo.Get_CODIGOCREDORDEVEDOR);
                        End
                        Else TmpNomeCliente:='';
                  End
                  Else Begin
                          if (fieldbyname('valores').asstring='')
                          Then TmpNomeCliente:='Prov�vel lan�amento manual'
                          Else TmpNomeCliente:=fieldbyname('historicolancamento').asstring;
                  End;

                  FrelTXTrdprint.RDprint.Imp(linha,1,CompletaPalavra(TmpNomeCliente,50,' ')+' '+
                                                 CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                                 CompletaPalavra(fieldbyname('numcheque').asstring,15,' ')+' '+
                                                 CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),12,' '));
                  ValorTotalCheques:=ValorTotalCheques+fieldbyname('valor').asfloat;
                  inc(linha,1);
                  next;
             End;//while
             if (linha>=(FreltxtRDPRINT.RDprint.TamanhoQteLinhas-3))
             Then Begin
                       linha:=3;
                       FreltxtRDPRINT.RDprint.Novapagina;
             End;
             FreltxtRDPRINT.RDprint.ImpF(linha,1,'VALOR TOTAL R$ '+formata_valor(floattostr(ValorTotalCheques)),[negrito]);

             FrelTXTrdprint.RDprint.Fechar;
        End;//with
     Finally
            ObjTitulo.free;
     End;

end;

function TObjChequesPortador.SalvarChequeRecebido(ComCommit: Boolean): Boolean;
begin
  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;


if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If (ComCommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

function TObjChequesPortador.VerificaSeSofreuTransferencia(Pcodigo: string;
  out NumTransf: String): boolean;
begin
     result:=False;
     NumTransf:='';
     //preciso apenas procurar na TabValoresTransferenciaPortador este cheque
     //e anotar o numero das transferencias
     With Self.ObjqueryLocal do
     Begin
          close;
          SQL.clear;
          SQL.add('select distinct(transferenciaportador)');
          SQL.add('from tabvalorestransferenciaportador');
          SQL.add('where valores='+Pcodigo);
          open;
          if Recordcount=0
          Then exit;
          result:=True;
          first;
          While not(eof) do
          Begin
               NumTransf:=NumTransf+','+fieldbyname('transferenciaportador').asstring;
               next;
          end;
     End;
end;

function TObjChequesPortador.ExcluiTransferencias(Pcodigo: string;ComCommit:Boolean): boolean;
var
ObjValTransferenciaPortador:TObjvaloresTransferenciaPortador;
Begin
     result:=False;
     Try
        ObjValTransferenciaPortador:=TObjValoresTransferenciaPortador.create;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto de TRANSFERENCIA PORTADOR para apagar as transfer�ncia!',mterror,[mbok],0);
           exit;
     End;
  Try

     //preciso apenas excluir os registros que tem este cheque e seus respectivos
     //lan�amentos na TabLanctoPortador
     With Self.ObjqueryLocal do
     Begin
          close;
          SQL.clear;
          SQL.add('select codigo,transferenciaportador');
          SQL.add('from tabvalorestransferenciaportador');
          SQL.add('where valores='+Pcodigo);
          open;
          if Recordcount=0
          Then exit;
          first;
          While not(eof) do
          Begin
               If (ObjValTransferenciaPortador.exclui(fieldbyname('codigo').asstring,ComCommit)=False)
               Then Begin
                         Messagedlg('Erro na Tentativa de excluir o cheque '+pcodigo+'da Transfer�ncia '+fieldbyname('transferenciaportador').asstring,mterror,[mbok],0);
                         exit;
               End;
               next;
          end;
          result:=True;
     End;

  Finally
       ObjValTransferenciaPortador.free;
  End;

end;

function TObjChequesPortador.Soma_Cheques_Terceiro_Portador(
  Pportador: string): currency;
var
Cont,linha:Integer;
TmpData:Tdate;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('Select sum(Valor) as SOMA');
          SelectSql.add('from Tabchequesportador');
          SelectSQL.add('where tabchequesportador.portador='+Pportador+' and not ChequedoPortador=''S''');
          open;
          result:=0;
          result:=Fieldbyname('soma').asfloat;
     End;
end;


procedure TObjChequesPortador.EdtMotivoDevolucaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FmotivoDevolucao:TFMotivoDevolucao;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FmotivoDevolucao:=TFMotivoDevolucao.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.MotivoDevolucao.Get_Pesquisa,Self.MotivoDevolucao.Get_titulopesquisa,FmotivoDevolucao)=True)
            Then Begin
                      If (Fpesquisalocal.showmodal=mrok)
                      Then Begin
                                Tedit(Sender).text:=Fpesquisalocal.querypesq.fieldbyname('codigo').asstring;
                           End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FmotivoDevolucao);
     End;


end;


function TObjChequesPortador.Get_Temp: string;
begin
    Result:=Self.Temp;
end;

procedure TObjChequesPortador.Submit_Temp(parametro: string);
begin
    Self.Temp:=parametro;
end;

end.


