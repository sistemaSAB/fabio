unit UObjGeraLancamento;
Interface
Uses Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjTipoLancto;

Type
   TObjGeraLancamento=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                TipoLAncto       :TobjTipoLancto;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;overload;
                Function    LocalizaCodigo(ParametroObjetoGerador,ParametroHistorico:string) :boolean;overload;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaTipoLancto          :string;
                Function    Get_TituloPesquisaTipoLancto    :string;//Function    Get_PesquisaTurma              :string;
               // Function    Get_TituloPesquisaTurma        :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_TipoLAncto       :string;
                Function Get_Historico        :string;
                Function Get_ObjetoGerador    :string;
                Function Get_HistoricoGerador :string;
                Function Get_CodigoPlanodeContas:string;

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_TipoLAncto       (parametro:string);
                Procedure Submit_Historico        (parametro:string);
                Procedure Submit_ObjetoGerador    (parametro:string);
                Procedure Submit_HistoricoGerador (parametro:string);
                Procedure Submit_CodigoPlanodeContas(parametro:string);
                Function  Get_NovoCodigo:string;
                Procedure Imprime;



         Private
               ObjDataset:Tibdataset;

               CODIGO           :String[09];
               Historico        :String[60];
               ObjetoGerador    :String[50];
               HistoricoGerador :String[50];
               CodigoPlanodeContas:string;


               //CodTurma         :TobjTurma;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Classes,SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Procedure  TObjGeraLancamento.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.Historico        :=FieldByName('Historico').asstring;
        Self.ObjetoGerador    :=FieldByName('ObjetoGerador').asstring;
        Self.HistoricoGerador :=FieldByName('HistoricoGerador').asstring;
        Self.CodigoPlanodeContas :=FieldByName('CodigoPlanodeContas').asstring;
        If (Self.tipolancto.LocalizaCodigo(FieldByName('tipolancto').asstring)=False)
        Then Begin
                  Messagedlg('Tipo de Lancto N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.tipolancto.TabelaparaObjeto;
     End;
end;


Procedure TObjGeraLancamento.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
       FieldByName('CODIGO').asstring           :=Self.CODIGO                   ;
       FieldByName('Historico').asstring        :=Self.Historico                ;
       FieldByName('ObjetoGerador').asstring    :=Self.ObjetoGerador            ;
       FieldByName('HistoricoGerador').asstring :=Self.HistoricoGerador         ;
       FieldByName('TipoLancto').asstring       :=Self.TipoLancto.Get_codigo    ;
       FieldByName('CodigoPlanodeContas').asstring:=Self.CodigoPlanodeContas    ;
  End;
End;

//***********************************************************************

function TObjGeraLancamento.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;



if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjGeraLancamento.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Historico        :='';
        Self.ObjetoGerador    :='';
        Self.HistoricoGerador :='';
        Self.TipoLAncto.ZerarTabela;
        Self.CodigoPlanodeContas:='';
     End;
end;

Function TObjGeraLancamento.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Historico='')
       Then Mensagem:=mensagem+'/Historico';

       If (ObjetoGerador='')
       Then Mensagem:=mensagem+'/Objeto Gerador do Lan�amento';

       If (HistoricoGerador='')
       Then Mensagem:=mensagem+'/Hist�rico para localiza��o do Gerador';

       If (TipoLancto.get_codigo='')
       Then Mensagem:=mensagem+'/Tipo de Lancto';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjGeraLancamento.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.TipoLAncto.LocalizaCodigo(Self.TipoLancto.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Tipo de LAncto n�o Encontrado!';


     If (Self.CodigoPlanodeContas<>'0')
     Then Begin
                IF (self.TipoLAncto.ObjPlanodeContas.LocalizaCodigo(self.CodigoPlanodeContas)=False)
                Then mensagem:=mensagem+' / C�digo do Plano de Contas n�o encontrado - Utilize 0 para utilizar o plano de contas';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjGeraLancamento.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.tipolancto.get_codigo);
     Except
           Mensagem:=mensagem+'/Tipo de Lancto';
     End;
     try
        Inteiros:=Strtoint(Self.CodigoPlanodeContas);
     Except
           Mensagem:=mensagem+'/C�digo do Plano de Contas';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjGeraLancamento.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjGeraLancamento.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjGeraLancamento.LocalizaCodigo(ParametroObjetoGerador,
  ParametroHistorico: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,ObjetoGerador,HistoricoGerador,TipoLAncto,CodigoPlanodeContas from TabGeraLancamento where ');
           SelectSql.add('ObjetoGerador='+#39+ParametroObjetoGerador+#39+' and HistoricoGerador='+#39+ParametroHistorico+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


function TObjGeraLancamento.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,ObjetoGerador,HistoricoGerador,TipoLAncto,CodigoPlanodeContas from TabGeraLancamento where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjGeraLancamento.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjGeraLancamento.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjGeraLancamento.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.TipoLancto:=TObjTipoLancto.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Historico,ObjetoGerador,HistoricoGerador,TipoLAncto,CodigoPlanodeContas from TabgeraLancamento where codigo=0');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert into TabgeraLAncamento (CODIGO,Historico,ObjetoGerador,HistoricoGerador,TipoLAncto,CodigoPlanodeContas) values (:CODIGO,:Historico,:ObjetoGerador,:HistoricoGerador,:TipoLAncto,:CodigoPlanodeContas)');

                ModifySQL.clear;
                ModifySQL.add('UPdate tabgeralancamento set  CODIGO=:CODIGO,Historico=:Historico,ObjetoGerador=:ObjetoGerador,HistoricoGerador=:HistoricoGerador,TipoLAncto=:TipoLAncto,CodigoPlanodeContas=:CodigoPlanodeContas where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add('Delete from tabgeralancamento where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Historico,ObjetoGerador,HistoricoGerador,TipoLAncto,CodigoPlanodeContas from TabgeraLancamento where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjGeraLancamento.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjGeraLancamento.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjGeraLancamento.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjGeraLancamento.Get_Pesquisa: string;
begin
     Result:=' Select * from TabGeraLancamento ';
end;

function TObjGeraLancamento.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de GeraLancamento ';
end;



{
function TObjGeraLancamento.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjGeraLancamento.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjGeraLancamento.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_GERALANCAMENTO' ;
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GeraLancamento',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


function TObjGeraLancamento.Get_Historico: string;
begin
     Result:=Self.Historico;
end;

function TObjGeraLancamento.Get_HistoricoGerador: string;
begin
     Result:=Self.HistoricoGerador;
end;

function TObjGeraLancamento.Get_ObjetoGerador: string;
begin
     Result:=Self.ObjetoGerador;
end;

function TObjGeraLancamento.Get_TipoLAncto: string;
begin
     Result:=Self.TipoLAncto.Get_CODIGO;
end;

procedure TObjGeraLancamento.Submit_Historico(parametro: string);
begin
     Self.historico:=parametro;
end;

procedure TObjGeraLancamento.Submit_HistoricoGerador(parametro: string);
begin
     Self.HistoricoGerador:=parametro;
end;

procedure TObjGeraLancamento.Submit_ObjetoGerador(parametro: string);
begin
     Self.ObjetoGerador:=parametro;
end;

procedure TObjGeraLancamento.Submit_TipoLAncto(parametro: string);
begin
     Self.tipolancto.submit_codigo(parametro);
end;

function TObjGeraLancamento.Get_PesquisaTipoLancto: string;
begin
     Result:=Self.TipoLAncto.Get_Pesquisa;
end;

function TObjGeraLancamento.Get_TituloPesquisaTipoLancto: string;
begin
     Result:=Self.TipoLAncto.Get_TituloPesquisa;
end;


destructor TObjGeraLancamento.Free;
begin
    Freeandnil(Self.ObjDataset);
    Self.TipoLancto.free;
end;

function TObjGeraLancamento.Get_CodigoPlanodeContas: string;
begin
     Result:=Self.CodigoPlanodeContas;
end;

procedure TObjGeraLancamento.Submit_CodigoPlanodeContas(parametro: string);
begin
     Self.CodigoPlanodeContas:=parametro;
end;

procedure TObjGeraLancamento.Imprime;
begin


end;

end.
