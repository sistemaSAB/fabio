unit UlancaTroco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UFrPagamento,UFrRecebimento;

type
  TFlancaTroco = class(TForm)
    Label1: TLabel;
    lbvalortroco: TLabel;
    Label3: TLabel;
    FrPagamento1: TFrPagamento;
    FrRecebimento1: TFrRecimento;
    procedure FrPagamento1btinserirClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FlancaTroco: TFlancaTroco;

implementation

uses UObjValores;



{$R *.dfm}

procedure TFlancaTroco.FrPagamento1btinserirClick(Sender: TObject);
begin
  FrPagamento1.btinserirClick(Sender);
end;


procedure TFlancaTroco.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);

     IF (kEY=#27)//esc
     Then Self.Close;

end;

procedure TFlancaTroco.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
PvalorLancadoTroco:Currency;
begin
     if (FrPagamento1.Visible=True)
     Then Begin
               //verificando se os valores fecham
               PvalorLancadoTroco:=ObjValoresTransferenciaPortador.TransferenciaPortador.get_somaporlancamento(Self.FrPagamento1.PCodigoLancamento);
               if (Pvalorlancadotroco<>Self.FrPagamento1.PValorLancamento)
               Then Begin
                         if (Messagedlg('A soma dos valores lan�ados para troco n�o � igual ao valor total do troco'+#13+'Deseja Voltar e Acertar? Pressione SIM para retornar e acertar e N�O para cancelar',mtconfirmation,[mbyes,mbno],0)=mryes)
                         Then Begin
                                  Abort;
                                  exit;
                         End;
               End;
     End
     Else Begin
               //verificando se os valores fecham
               PvalorLancadoTroco:=ObjValores.Get_SomaValores(Self.FrRecebimento1.PCodigoLancamento);
               if (Pvalorlancadotroco<>Self.FrRecebimento1.PValorLancamento)
               Then Begin
                         if (Messagedlg('A soma dos valores lan�ados para troco n�o � igual ao valor total do troco'+#13+'Deseja Voltar e Acertar? Pressione SIM para retornar e acertar e N�O para cancelar',mtconfirmation,[mbyes,mbno],0)=mryes)
                         Then Begin
                                  Abort;
                                  exit;
                         End;
               End;
     End;
end;

procedure TFlancaTroco.FormShow(Sender: TObject);
begin
     Self.FrPagamento1.lbportador.caption:='';
     Self.FrPagamento1.edtvalor.Text:=floattostr(Self.FrPagamento1.PValorLancamento);
end;

end.
