
unit UMostraValorMoedas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, UObjCotacaoMoeda, DBCtrls, ExtCtrls;

type
  TFMostraValorMoedas = class(TForm)
    StrGrid: TStringGrid;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    EdtValor: TEdit;
    Bevel1: TBevel;
    Bevel2: TBevel;
    lbFrase1: TLabel;
    lbFrase2: TLabel;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtValorExit(Sender: TObject);
    procedure EdtValorKeyPress(Sender: TObject; var Key: Char);
  private
      ObjCotacaoMoeda : TObjCotacaoMoeda;
    procedure PreencheMoedas(PStrList: TStringList);
    procedure rbClick(Sender: TObject);

  public
    { Public declarations }
  end;

var
  FMostraValorMoedas: TFMostraValorMoedas;

implementation

uses UessencialLocal, UessencialGlobal;

{$R *.dfm}

procedure TFMostraValorMoedas.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

procedure TFMostraValorMoedas.FormShow(Sender: TObject);
Var StrList:TStringList;
begin
try
     try
         Self.ObjCotacaoMoeda := TObjCotacaoMoeda.Create;
         StrList:=TStringList.Create();
     except
         MensagemErro('Erro ao tentar criar o Obejeto Cotacao Moeda');
     end;

     Self.ObjCotacaoMoeda.PreencheMoedas(StrList);
     Self.PreencheMoedas(StrList);

finally
     FreeAndNil(StrList);
end;

end;

procedure TFMostraValorMoedas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (ObjCotacaoMoeda <> nil)
     then  ObjCotacaoMoeda.Free;
end;

procedure TFMostraValorMoedas.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
            //KEY:=#0;
      end;

end;

procedure TFMostraValorMoedas.PreencheMoedas(PStrList:TStringList);
Var Cont, Topo, Esquerda, Cont3 :Integer;
begin
    Topo:=35; // 20 Form
    Esquerda:=180;
    Cont3:=0;
    lbFrase1.Visible:=false;
    lbFrase2.Visible:=false;

    for Cont:=0 to PStrList.Count-1 do
    Begin   // Radio Button

        with TRadioButton.Create(Self)  do
        begin
               Parent:=Self;
               Self.ObjCotacaoMoeda.MOEDA.LocalizaCodigo(PStrList[Cont]);
               Self.ObjCotacaoMoeda.MOEDA.TabelaparaObjeto;
               Caption:=Self.ObjCotacaoMoeda.MOEDA.Get_NOME;

               if (Cont3>=3)
               then Esquerda:=300
               else Esquerda:=180;

               left:=Esquerda;

               if (Cont3=3)
               then Topo:=35;

               Top:=Topo+5; //17
               Checked:=false;
               Name:='RB'+PStrList[Cont];
               font.Size:=10;
               font.Name:='Courier New';
               Width:=100;
               Enabled:=true;

               onClick:=rbClick;

               if ((Topo=35)and(Cont3<>3))
               then Checked:=true; // Somenete  o primeiro

               // Se nuam tem cota��o para essa moeda eu desabilito o RadioButton
               if ((Self.ObjCotacaoMoeda.VerificaSeExisteCotacao(PStrList[Cont])=false) and (Self.ObjCotacaoMoeda.MOEDA.Get_MoedaPadrao <> 'S'))
               then Begin
                      Enabled:=false;
                      lbFrase1.Visible:=true;
                      lbFrase2.Visible:=true;
               end;

               // Se for moeda padr�o fica em Negrito
               if (Self.ObjCotacaoMoeda.MOEDA.Get_MoedaPadrao = 'S')
               then Begin
                       font.Style:=[fsBold];
                       font.Color:= clGreen;
               end;



               Inc(Topo,18);
               inc(Cont3,1);
         end;
    end;

end;

procedure TFMostraValorMoedas.rbClick(Sender: TObject);
begin
     if (EdtValor.Text='')
     then exit;

     try
         StrToCurr(Tira_Ponto(EdtValor.Text));
     except
         MensagemErro('Valor Incorreto');
         exit;
     end;


     if (TRadioButton(Sender).checked = true)
     then Begin
             ObjCotacaoMoeda.CalculaMoeda(StrToCurr(Tira_Ponto(EdtValor.Text)),TRadioButton(Sender).Caption,StrGrid);
     end;
end;

procedure TFMostraValorMoedas.EdtValorExit(Sender: TObject);
Var Cont:Integer;
begin
   for Cont:=0 to Self.ComponentCount -1   do
   Begin
        if Self.Components [Cont].ClassName = 'TRadioButton'
        then Begin
                if (TRadioButton(Self.Components [Cont]).Checked = true)
                then Begin
                       TRadioButton(Self.Components[Cont]).OnClick(TRadioButton(Self.Components[Cont]));
                       Break;
                end;        

        end;
    end;    
end;

procedure TFMostraValorMoedas.EdtValorKeyPress(Sender: TObject; var Key: Char);
begin
    If not (key in ['0'..'9',',',#8])
    then key:=#0
    Else
    If Key='.'
    Then key:=',';
end;

end.
