{Unit Criada por F�bio 03/09/2009. Anderson que pediu pra um cliente do Paraguai
 Como j� tenho isso no BIA apenas adaptei ao Amanda}

unit UobjCOTACAOMOEDA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UOBJMOEDA, Grids, Forms;

Type
   TObjCOTACAOMOEDA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                MOEDA:TOBJMOEDA;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    SalvarGrid(PStrgrid:TStringGrid; PData:String; ComCommit:Boolean):Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_PesquisaView                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: String);
                Function Get_CODIGO: String;
                Procedure Submit_VALOR(parametro: String);
                Function Get_VALOR: String;
                Procedure Submit_DATA(parametro: String);
                Function Get_DATA: String;
                procedure EdtMOEDAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtMOEDAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure MostraCotacaodoDia(var PStrGrid: TStringGrid; Var PDataRetorno:String; PData: String);
                Procedure PreencheMoedas(Var PStrList:TStringList);
                procedure CalculaMoeda(PValor: Currency; PNomeMoeda: String; var PStrGrid: TStringGrid);
                Function VerificaSeExisteCotacao(PMoeda:String):Boolean;


         Private
                Objquery:Tibquery;
                InsertSql,DeleteSql,ModifySQl:TStringList;

                CODIGO:String;
                VALOR:String;
                DATA:String;
                ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function RetornaCotacao(PNomeMoeda, PData: String): Currency;


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UMOEDA, UMostraValorMoedas, UessencialLocal;





Function  TObjCOTACAOMOEDA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('MOEDA').asstring<>'')
        Then Begin
                 If (Self.MOEDA.LocalizaCodigo(FieldByName('MOEDA').asstring)=False)
                 Then Begin
                          Messagedlg('MOEDA N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.MOEDA.TabelaparaObjeto;
        End;
        Self.VALOR:=fieldbyname('VALOR').asstring;
        Self.DATA:=fieldbyname('DATA').asstring;
        result:=True;
     End;
end;


Procedure TObjCOTACAOMOEDA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('MOEDA').asstring:=Self.MOEDA.GET_CODIGO;
        ParamByName('VALOR').asstring:=virgulaparaponto(Self.VALOR);
        ParamByName('DATA').asstring:=Self.DATA;
//CODIFICA OBJETOPARATABELA






  End;
End;

//***********************************************************************

function TObjCOTACAOMOEDA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOTACAOMOEDA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        MOEDA.ZerarTabela;
        VALOR:='';
        DATA:='';
     End;
end;

Function TObjCOTACAOMOEDA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCOTACAOMOEDA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.MOEDA.LocalizaCodigo(Self.MOEDA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ MOEDA n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCOTACAOMOEDA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        If (Self.MOEDA.Get_Codigo<>'')
        Then Strtoint(Self.MOEDA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/MOEDA';
     End;

     try
        Strtofloat(Self.VALOR);
     Except
           Mensagem:=mensagem+'/VALOR';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCOTACAOMOEDA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATA);
     Except
           Mensagem:=mensagem+'/DATA';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCOTACAOMOEDA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCOTACAOMOEDA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COTACAOMOEDA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select * from  TABCOTACAOMOEDA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOTACAOMOEDA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCOTACAOMOEDA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCOTACAOMOEDA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCOTACAOMOEDA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.MOEDA:=TOBJMOEDA.create;

//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCOTACAOMOEDA(CODIGO,MOEDA,VALOR');
                InsertSQL.add(',DATA)');
                InsertSQL.add('values (:CODIGO,:MOEDA,:VALOR,:DATA');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCOTACAOMOEDA set CODIGO=:CODIGO,MOEDA=:MOEDA');
                ModifySQL.add(',VALOR=:VALOR,DATA=:DATA');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCOTACAOMOEDA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOTACAOMOEDA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOTACAOMOEDA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOTACAOMOEDA');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOTACAOMOEDA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de COTACAOMOEDA ';
end;


function TObjCOTACAOMOEDA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCOTACAOMOEDA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCOTACAOMOEDA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOTACAOMOEDA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.MOEDA.FREE;

//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOTACAOMOEDA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOTACAOMOEDA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCOTACAOMOEDA.Submit_CODIGO(parametro: String);
begin
        Self.CODIGO:=Parametro;
end;
function TObjCOTACAOMOEDA.Get_CODIGO: String;
begin
        Result:=Self.CODIGO;
end;
procedure TObjCOTACAOMOEDA.Submit_VALOR(parametro: String);
begin
        Self.VALOR:=Parametro;
end;
function TObjCOTACAOMOEDA.Get_VALOR: String;
begin
        Result:=Self.VALOR;
end;
procedure TObjCOTACAOMOEDA.Submit_DATA(parametro: String);
begin
        Self.DATA:=Parametro;
end;
function TObjCOTACAOMOEDA.Get_DATA: String;
begin
        Result:=Self.DATA;
end;
//CODIFICA GETSESUBMITS


procedure TObjCOTACAOMOEDA.EdtMOEDAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.MOEDA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.MOEDA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.MOEDA.GET_NOME;
End;
procedure TObjCOTACAOMOEDA.EdtMOEDAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FMOEDA:TFMOEDA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FMOEDA:=TFMOEDA.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.MOEDA.Get_Pesquisa,Self.MOEDA.Get_TituloPesquisa,FMOEDA)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.MOEDA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.MOEDA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.MOEDA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FMOEDA);
     End;
end;


procedure TObjCOTACAOMOEDA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOTACAOMOEDA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjCOTACAOMOEDA.Get_PesquisaView: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewPesquisaCotacaoMoeda');
     Result:=Self.ParametroPesquisa;

end;

procedure TObjCOTACAOMOEDA.MostraCotacaodoDia(var PStrGrid: TStringGrid; Var PDataRetorno:String; PData: String);
Var PrimeiraLinha:Boolean;
    Cont:Integer;
begin

     PrimeiraLinha:=true;
     PStrGrid.RowCount:=2;
     LimpaStringGrid(PStrGrid);

     if (PData = '')
     then exit;

     With Self.Objquery do
     Begin
          Close;
          SQL.Clear;
          SQL.Add('Select TabMoeda.Codigo, TabMoeda.Nome, TabCotacaoMoeda.VALOR, ');
          SQL.Add('TabCotacaoMoeda.Data from TabCotacaoMoeda');
          SQL.Add('join TabMoeda on TabMoeda.Codigo = TabCotacaoMoeda.MOEDA');
          SQL.Add('Where TabCotacaoMoeda.Codigo in (Select  MAX(TabCotacaoMoeda.Codigo) from TabCotacaoMoeda');
          SQL.Add('                                 Where TabCotacaoMoeda.Data = '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PData))+#39);
          SQL.Add('                                 Group By TabCotacaoMoeda.Moeda)');
          SQL.Add('And TabMoeda.MoedaPadrao <> ''S''  ');
          open;

          // Se n�o econtrou nada com a data Informada, entao retorna a ultima data encontrada
          if (RecordCount = 0)
          then Begin
                    Close;
                    SQL.Clear;
                    SQL.Add('Select TabMoeda.Codigo, TabMoeda.Nome, TabCotacaoMoeda.VALOR,');
                    SQL.Add('TabCotacaoMoeda.Data from TabCotacaoMoeda');
                    SQL.Add('join TabMoeda on TabMoeda.Codigo = TabCotacaoMoeda.MOEDA');
                    SQL.Add('Where TabCotacaoMoeda.Codigo in (Select  MAX(TabCotacaoMoeda.Codigo) from TabCotacaoMoeda');
                    SQL.Add('                                 Where TabCotacaoMoeda.Data in (Select MAX(TabCotacaoMoeda.Data) from TabCotacaoMoeda)');
                    SQL.Add('                                 Group By TabCotacaoMoeda.Moeda)');
                    SQL.Add('And TabMoeda.MoedaPadrao <> ''S''  ');
                    Open;
          end;

          if (RecordCount=0)
          then exit;

          Cont:=1;
          While not (eof) do
          Begin
               if (PrimeiraLinha=false)
               then PStrGrid.RowCount:=PStrGrid.RowCount+1;
               PrimeiraLinha:=false;

               PStrGrid.Cells[0,Cont]:=fieldbyname('Codigo').AsString;
               PStrGrid.Cells[1,Cont]:=fieldbyname('Nome').AsString;
               PStrGrid.Cells[2,Cont]:=formata_valor(fieldbyname('Valor').AsString);
               PDataRetorno:=fieldbyname('Data').AsString;
               next;
               inc(Cont,1);
          end;

     end;

end;

function TObjCOTACAOMOEDA.SalvarGrid(PStrgrid: TStringGrid;PData:String; ComCommit: Boolean): Boolean;
Var Cont:Integer;
begin
    Result:=false;
    For Cont:=1 to PStrgrid.RowCount-1 do
    Begin
         Self.ZerarTabela;
         Self.Status:=dsInsert;
         Self.Submit_CODIGO('0');
         Self.Submit_DATA(PData);
         Self.MOEDA.Submit_CODIGO(PStrGrid.Cells[0,Cont]);
         Self.Submit_VALOR(tira_ponto(PStrGrid.Cells[2,Cont]));
         if (Self.Salvar(false)=false)
         then Begin
                 MensagemErro('Erro ao tentar salvar');
                 FDataModulo.IBTransaction.RollbackRetaining;
                 exit;
         end;
    end;

    if (ComCommit = true)
    then Self.Commit;

    Result:=true;
end;

Procedure TObjCOTACAOMOEDA.PreencheMoedas(Var PStrList:TStringList);
Begin
     With Self.Objquery do
     Begin
          Close;
          SQL.Clear;
          SQL.Add('Select Codigo from TabMoeda Order By MoedaPadrao Desc');
          Open;

          PStrList.Clear;
          While not (eof) do
          Begin
               PStrList.Add(fieldbyname('Codigo').AsString);
               next;
          end;
     end;           

end;

Procedure TObjCOTACAOMOEDA.CalculaMoeda(PValor :Currency; PNomeMoeda:String; Var PStrGrid:TStringGrid);
Var  Pdatatemp, NomeMoedaPadao:String;
     Cont:Integer;
     ValorCotacao:Currency;
Begin
     // Se a Moeda for a  Moeda Padraao entaum eu consigo dar todos as
     // outras moedas, Sen�o eu s� posso mostrar a moeda seleciona e a moeda padrao

     PStrGrid.Cells[0,0]:='COD';
     PStrGrid.Cells[1,0]:='MOEDA';
     PStrGrid.Cells[2,0]:='VALOR';

     if (Self.MOEDA.LocalizaNome(PNomeMoeda)=false)
     then Begin
              MensagemErro('Moeda n�o encontrada');
              exit;
     end;
     Self.MOEDA.TabelaparaObjeto;

     if (Self.MOEDA.Get_MoedaPadrao = 'S')
     then Begin
             // Preenche o Grid com as Moedas e sua cotacoes
             NomeMoedaPadao:=Self.MOEDA.Get_Nome;
             Self.MostraCotacaodoDia(PStrGrid,Pdatatemp, DateToStr(Now));
             For Cont:=1 to PStrGrid.RowCount-1 do
             Begin
                 ValorCotacao:=StrToCurr(Tira_ponto(PStrGrid.Cells[2,Cont]));
                 PStrGrid.Cells[2,Cont]:=Formata_Valor(PValor*ValorCotacao);
             end
     end else
     Begin
          // Se a moeda escolhida n�o for a moeda padr�o eu s� posso mostrar o valor em Real
          PStrGrid.RowCount:=2;
          ValorCotacao:=Self.RetornaCotacao(PNomeMoeda, DateToStr(Now));
          PStrGrid.Cells[1,1]:=Self.MOEDA.RetornaNomeMoedaPadrao;
          PStrGrid.Cells[2,1]:=Formata_Valor(PValor/ValorCotacao);
     end;

end;

Function TObjCotacaoMoeda.RetornaCotacao(PNomeMoeda, PData:String ):Currency;
Begin
     Result:=0;
     With Self.Objquery do
     Begin
          Close;
          SQL.Clear;
          SQL.Add('Select TabCotacaoMoeda.VALOR from TabCotacaoMoeda');
          SQL.Add('join TabMoeda on TabMoeda.Codigo = TabCotacaoMoeda.MOEDA');
          SQL.Add('Where TabMoeda.Nome = '+#39+PNomeMoeda+#39);
          SQL.Add('and TabCotacaoMoeda.Codigo in (Select  MAX(TabCotacaoMoeda.Codigo) from TabCotacaoMoeda');
          SQL.Add('                                 Where TabCotacaoMoeda.Data = '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PData))+#39);
          SQL.Add('                                 Group By TabCotacaoMoeda.Moeda)');
          open;

          // Se n�o econtrou nada com a data Informada, entao retorna a ultima data encontrada
          if (RecordCount = 0)
          then Begin
                    Close;
                    SQL.Clear;
                    SQL.Add('Select TabCotacaoMoeda.VALOR from TabCotacaoMoeda');
                    SQL.Add('join TabMoeda on TabMoeda.Codigo = TabCotacaoMoeda.MOEDA');
                    SQL.Add('Where TabMoeda.Nome = '+#39+PNomeMoeda+#39);
                    SQL.Add('AND TabCotacaoMoeda.Codigo in (Select  MAX(TabCotacaoMoeda.Codigo) from TabCotacaoMoeda');
                    SQL.Add('                                 Where TabCotacaoMoeda.Data in (Select MAX(TabCotacaoMoeda.Data) from TabCotacaoMoeda)');
                    SQL.Add('                                 Group By TabCotacaoMoeda.Moeda)');
                    Open;
          end;

          Result:=fieldbyname('Valor').AsCurrency;
     end;
end;

function TObjCOTACAOMOEDA.VerificaSeExisteCotacao(PMoeda: String): Boolean;
begin
     With Self.Objquery do
     Begin
          Close;
          SQL.Clear;
          SQL.Add('Select TabCotacaoMoeda.Codigo from TabCotacaoMoeda');
          SQL.Add('Where Moeda = '+PMoeda);
          SQL.Add('and TabCotacaoMoeda.Valor > 0');
          open;

          if (recordCount = 0)
          then Result:=false
          else Result:=true;
     end;     
end;

end.




