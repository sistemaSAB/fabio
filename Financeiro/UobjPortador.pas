unit UobjPortador;
Interface
Uses Ibquery,grids,Ibcustomdataset,IBStoredProc,db,Classes,UessencialGlobal,
Uobjplanodecontas,UobjHistoricoSimples;

Type
   TObjPortador=class

          Public

                PlanodeContas:TobjPlanodeContas;
                HIstoricoContabil:TobjHistoricoSimples;

                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                NumeroRelatorio:string;

                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Procedure   TabelaparaObjeto                        ;
                Function    LocalizaCodigo(Parametro:string) :boolean;

                Function    exclui(Pcodigo:string;ComCommit:Boolean)            :Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;
                Function    Get_Pesquisa                               :string;
                Function    Get_PesquisaApenasUsoSistema               :String;
                Function    Get_PesquisaPermiteEmissao                 :string;
                Function    Get_TituloPesquisa                         :string;
                Function    Get_PesquisaPlanodeContas                  :TStringList;
                Function    Get_TituloPesquisaPlanodeContas            :string;
                Function    Get_Classificacao:string;
                function    Get_PesquisaPortadorComCheque             :string; //Rodolfo

                Constructor Create;
                Destructor Free;

                Function Get_CODIGO           :string;
                Function Get_Nome             :string;
                Function Get_Agencia          :string;
                Function Get_NumeroConta      :string;
                Function Get_Endereco         :string;
                Function Get_Telefone         :string;
                Function Get_Ativo            :string;
                //Function Get_Saldo            :string;
                Function Get_PermiteEmissaoCheque:string;
                Function Get_CodigoPlanodeContas:string;
                Function Get_Apenas_Uso_Sistema  :string;
                Function Get_Permite_Saldo_Negativo:string;

                Procedure Submit_Permite_Saldo_Negativo(parametro:string);
                Procedure Submit_CODIGO            (parametro:string);
                Procedure Submit_Nome              (parametro:string);
                Procedure Submit_Agencia           (parametro:string);
                Procedure Submit_NumeroConta       (parametro:string);
                Procedure Submit_Endereco          (parametro:string);
                Procedure Submit_Telefone          (parametro:string);
                Procedure Submit_Ativo             (parametro:string);
                //Procedure Submit_Saldo             (parametro:string);
                Procedure Submit_PermiteEmissaoCheque(parametro:string);
                Procedure Submit_CodigoPlanodeContas(parametro:string);
                Procedure Submit_Apenas_Uso_Sistema  (parametro:string);
                Procedure Submit_Classificacao(parametro:string);

                //Function CreditaSaldo(ParametroPortador:string;ParametroValor:string;ComCommit:boolean):Boolean;
                //Function DebitaSaldo(ParametroPortador:string;ParametroValor:string;ComCommit:boolean):Boolean;


                Function AtualizaChequePortador(ParametroPortador,ParametroCheque,ParametroPortadorDestino:string;HistoricoLanctoPortador:string;ImprimeOrigem,ImprimeDestino:boolean;PData:string;Plancamentotransferencia:string):boolean;
                procedure Get_ListaCheques_do_Portador(ParametroPortador: string;ParametroListaGeral: TstringGrid);
                Procedure Get_ListaChequesPortador(ParametroPortador:string;ParametroListaGeral:TstringGrid;ComChequedoPortador:boolean;PVencimento:string);overload;
                Procedure Get_ListaChequesPortador(ParametroPortador:string;ParametroListaGeral:TstringGrid;ComChequedoPortador:boolean;PVencimentoInicial,PvencimentoFinal:string);overload;

                Procedure Imprime(parametrocodigo:string);
                Function VerificaPermissao:Boolean;
                function Get_NovoCodigo: string;
                procedure edtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edtportadorKeyDown_PV(Sender: TObject; var Key: Word;Shift: TShiftState);
                Function  Get_SaldopelosLancamentos(Pportador:string):string;
                Function Proc_SaldoAnterior(PPortador: string;PDataLimite: string): string;
                Procedure ImprimeExtratoPortador_FechamentoCaixa_3();

         Private
               ObjDataset        :Tibdataset;
               QueryLocal        :tibquery;
               QueryLocal2       :tibquery;

               CODIGO            :String[09];
               Nome              :String[250];
               Agencia           :String[15];
               NumeroConta       :String[15];
               Endereco          :String[50];
               Telefone          :String[20];
               Ativo             :String[1];
               //Saldo             :String[9];
               PermiteEmissaoCheque:String[1];
               CodigoPlanodeContas  :string;
               Apenas_Uso_Sistema  :string[1];   // corrigido    - Rodolfo
               Permite_Saldo_Negativo:string[1];   // corrigido  - Rodolfo
               Classificacao:string;
               //****

               Procedure ImprimeLancamento_por_Data(parametrocodigo:string);
               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Procedure ImprimeConciliacaoBancaria(Parametrocodigo:string;Ordenacao:string);
               Procedure ImprimeFluxodeCaixa(parametroCodigo:string;SA:string);

               Function  GeraFluxoAnalitico(PQlocal:tibquery;Pportador:Integer;PdataInicial,PdataFinal:Tdatetime;ComPrevisao:boolean):boolean;
               Function  GeraFluxoSinteticoTotalizado(PQlocal:tibquery;PdataInicial,PdataFinal:Tdatetime;PNumdias:Integer):boolean;
               Function  PegaSaldoAnteriorTotal(Pdata:Tdatetime):Currency;
               Procedure ImprimePrevisto(PPortador:string;PDataInicial,PDataFinal:TdateTime;PSaldoInicial:Currency);
               function  GeraFluxoSinteticoNovo(PQlocal: tibquery; Pportador: Integer;PdataInicial, PdataFinal: Tdatetime; PNumdias: Integer): boolean;
               Function  SomaCreditos(Pportador:string;Pdata:Tdatetime):Currency;
               Function  SomaDebitos(Pportador:string;Pdata:Tdatetime):Currency;
               procedure ImprimeChequesPortador_periodo(ParametroPortador: string);
               Procedure ImprimeExtratoPortador(parametroCodigo:string);
               Procedure ImprimeExtratoPortador_Separado_Usuario(parametroCodigo:string);
               Function  LocalizaTituloGERADOR(TMPOBJETOGERADOR,TMPvalordocampo:string):string;
               Procedure ImprimeRelacaodeChequesTERCEIRO(parametrocodigo:string);

               procedure edtUsuarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
               procedure edtTipoLanctoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
               procedure edtTipoLanctoKeyDown_PV(Sender: TObject; var Key: Word;Shift: TShiftState);

               Procedure GeraFluxoporLancto(Pportador:string;AnaliticoSintetico:string);
               function LocalizaTitulo_Lancamento(Plancamento: string): string;

               // Funcao feita por F�bio
               function RetornaNomeCredorDevedor(PLancamentoPortador: string): string;
               Procedure ImprimeExtratoPortador_NAO_Separado_Usuario(parametroCodigo:string);
               Procedure ImprimeChequesEmitidos(PPortador:string);
               procedure ImprimeContasGerenciaisFechamentoCaixa3(Var PLinha:Integer; PUsuario, PPortador:String; Pdata1, PData2:TDateTime);
               procedure ImprimeTransferenciaFechamentoCaixa3(var PLinha: Integer; PUsuario : String; Pdata1, PData2: TDateTime);

   End;


implementation
uses rdprint,stdctrls,SysUtils,Dialogs,UDatamodulo,UobjChequesPortador,
     windows,Upesquisa,Controls,UfiltraImp,Urelatorio,Uobjlanctoportador,URelTXT,UrelTxtRdPrint,printers,
  Forms,Uobjtipolanctoportador, UObjPendencia, UMenuRelatorios, Mask,
  UObjTitulo;


{ TTabTitulo }


Procedure  TObjPortador.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin                                   
        Self.CODIGO            :=FieldByName('CODIGO').asstring;
        Self.Nome              :=FieldByName('Nome').asstring;
        Self.Agencia           :=FieldByName('Agencia').asstring;
        Self.NumeroConta       :=FieldByName('NumeroConta').asstring;
        Self.Endereco          :=FieldByName('Endereco').asstring;
        Self.Telefone          :=FieldByName('Telefone').asstring;
        Self.Ativo             :=FieldByName('Ativo').asstring;
        //Self.Saldo             :=FieldByName('Saldo').asstring;
        Self.PermiteEmissaoCheque:=FieldByName('PermiteEmissaoCheque').AsString;
        Self.CodigoPlanodeCOntas:=Fieldbyname('CodigoPlanodeContas').asstring;
        Self.Apenas_Uso_Sistema:=Fieldbyname('Apenas_Uso_Sistema').asstring;
        Self.Permite_Saldo_Negativo:=Fieldbyname('permite_saldo_negativo').asstring;
        Self.Classificacao:=fieldbyname('Classificacao').asString;
     End;
end;


Procedure TObjPortador.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
     FieldByName('CODIGO').asstring             :=Self.CODIGO            ;
     FieldByName('Nome').asstring               :=Self.Nome              ;
     FieldByName('Agencia').asstring            :=Self.Agencia           ;
     FieldByName('NumeroConta').asstring        :=Self.NumeroConta       ;
     FieldByName('Endereco').asstring           :=Self.Endereco          ;
     FieldByName('Telefone').asstring           :=Self.Telefone          ;
     FieldByName('Ativo').asstring              :=Self.Ativo             ;
     //FieldByName('Saldo').asstring              :=Self.Saldo             ;
     FieldByName('PermiteEmissaoCheque').AsString :=Self.PermiteEmissaoCheque;
     FieldByName('CodigoPlanodeContas').AsString:=Self.CodigoPlanodeContas;
     Fieldbyname('Apenas_Uso_Sistema').asstring:=Self.Apenas_Uso_Sistema;
     Fieldbyname('permite_saldo_negativo').asstring:=Self.Permite_Saldo_Negativo;
     Fieldbyname('Classificacao').AsString:=Self.Classificacao;
  End;
End;

//***********************************************************************

function TObjPortador.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if(Self.Status=dsedit)
  Then Begin
            If Self.LocalizaCodigo(Self.CODIGO)=False
            Then Begin
                      Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                      result:=False;
                      exit;
            End;
       End;

  //Self.ObjDataset.Active := True; //Rodolfo
  //Self.ObjDataset.First;    //rodolfo

  if Self.status=dsinsert
      then Self.ObjDataset.Insert//libera para insercao
  Else
       if (Self.Status=dsedit)
          Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
            mensagemerro('Status Inv�lido na Grava��o');
            exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If (ComCommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status := dsInactive;
 result:=True;

end;

procedure TObjPortador.ZerarTabela;//Ok
Begin
     With Self do
     Begin
          Self.CODIGO            :='';
          Self.Nome              :='';
          Self.Agencia           :='';
          Self.NumeroConta       :='';
          Self.Endereco          :='';
          Self.Telefone          :='';
          Self.Ativo             :='';
          //Self.Saldo             :='';
          Self.PermiteEmissaoCheque:='';
          Self.CodigoPlanodeContas:='';
          Self.Apenas_Uso_Sistema:='';
          Self.Permite_Saldo_Negativo:='';
          Self.Classificacao:='';
     End;
end;

Function TObjPortador.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (nome='')
       Then Mensagem:=mensagem+'/Nome';
       If (Ativo='')
       Then Mensagem:=mensagem+'/Situa��o do Portador';
       If (PermiteEmissaoCheque='')
       Then Mensagem:=mensagem+'/Cheque Credita em Dinheiro?';
       If (Apenas_Uso_Sistema='')
       Then Mensagem:=Mensagem+'/Apenas Uso do Sistema?';
       if (Permite_Saldo_Negativo='')
       then mensagem:=Mensagem+'/Permite Saldo Negativo?';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjPortador.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     Try
        {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
        Then Mensagem:='/ Curso n�o Encontrado!'
        Else Begin
                  Self.CODCURSO.TabelaparaObjeto;
                  If Self.CODCURSO.Get_Ativo='N'
                  Then Mensagem:='/Situa��o do Curso Inv�lida para a Turma!';
             End;}

        If (mensagem<>'')
        Then Begin
                  Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
                  result:=False;
                  exit;
             End;
        result:=true;
     Finally

     End;
End;

function TObjPortador.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:='C�digo';
     End;

     try
        //Reais:=Strtofloat(Self.Saldo);
     Except
           Mensagem:=mensagem+'/Saldo da Conta';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjPortador.VerificaData: Boolean;
var
Datas:Tdatetime;
Mensagem:string;
begin
     mensagem:='';

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m datas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjPortador.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

  Try

    Mensagem:='';
    If ((Self.Ativo <> 'S') and (Self.Ativo <> 'N'))
    Then mensagem:=mensagem+'/Ativo';

    If ((Self.PermiteEmissaoCheque <> 'S') and (Self.PermiteEmissaoCheque <> 'N'))
    Then mensagem:=mensagem+'/Cheque Credita em Dinheiro?';

    If ((Self.Apenas_Uso_Sistema <> 'S') and (Self.Apenas_Uso_Sistema <> 'N'))
    Then mensagem:=mensagem+'/Apenas Uso do Sistema?';

    If ((Self.Permite_Saldo_Negativo <> 'S') and (Self.Permite_Saldo_Negativo <> 'N'))
    Then mensagem:=mensagem+'/Permite Saldo Negativo?';

    If mensagem <> '' then
    Begin
      Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
      result:=false;
      exit;
    End;

    Result:=true;
  Finally

  end;

end;

function TObjPortador.LocalizaCodigo(parametro: string): boolean;//ok
begin
       result:=False;

       if (parametro='')
       then exit;

       With Self.ObjDataset do
       Begin


           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,nome,agencia,NumeroConta,Endereco,Telefone,Ativo,');
           SelectSql.add('PermiteEmissaoCheque,CodigoPlanodeContas,apenas_uso_sistema,Permite_Saldo_Negativo,Classificacao');
           SelectSql.add('from tabportador where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjPortador.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjPortador.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPortador.create;
begin
        ZerarTabela;

        Self.ObjDataset:=Tibdataset.create(nil);

        Self.ObjDataset.Database:=FDataModulo.IbDatabase;

        Self.PlanodeContas:=TobjPlanodeContas.create;

        Self.HIstoricoContabil:=TobjHistoricoSimples.create;

        Self.QueryLocal:=tibquery.create(nil);

        Self.QueryLocal.database:=FDataModulo.IbDatabase;

        Self.QueryLocal2:=tibquery.create(nil);

        Self.QueryLocal2.database:=FDataModulo.IbDatabase;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,nome,agencia,NumeroConta,Endereco,Telefone,Ativo,PermiteEmissaoCheque,CodigoPlanodeContas,apenas_uso_sistema,Permite_Saldo_Negativo,Classificacao from tabportador where codigo=-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' Insert Into TabPortador (CODIGO,nome,agencia,NumeroConta,Endereco,Telefone,Ativo,PermiteEmissaoCheque,CodigoPlanodeContas,apenas_uso_sistema,Permite_Saldo_Negativo,Classificacao) ');
                InsertSQL.add(' values (:CODIGO,:nome,:agencia,:NumeroConta,:Endereco,:Telefone,:Ativo,:PermiteEmissaoCheque,:CodigoPlanodeContas,:apenas_uso_sistema,:Permite_Saldo_Negativo,:Classificacao)');

                ModifySQL.clear;
                ModifySQL.add(' UPdate Tabportador set ');
                ModifySQL.add(' CODIGO=:Codigo, ');
                ModifySQL.add(' nome=:nome, ');
                ModifySQL.add(' agencia=:agencia ,');
                ModifySQL.add(' NumeroConta=:NumeroConta,');
                ModifySQL.add(' Endereco=:Endereco,');
                ModifySQL.add(' Telefone=:Telefone, ');
                ModifySQL.add(' Ativo=:Ativo,PermiteEmissaoCheque=:PermiteEmissaoCheque,CodigoPlanodeContas=:CodigoPlanodeContas, ');
                ModifySQL.add(' apenas_uso_sistema=:apenas_uso_sistema ,Permite_Saldo_Negativo=:Permite_Saldo_Negativo,Classificacao=:Classificacao where codigo=:codigo');

                DeleteSQL.clear;
                DeleteSQL.add(' Delete from Tabportador where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,nome,agencia,NumeroConta,Endereco,Telefone,Ativo,PermiteEmissaoCheque,CodigoPlanodeContas,apenas_uso_sistema,Permite_Saldo_Negativo,Classificacao from tabportador where codigo=-1');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjPortador.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjPortador.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjPortador.Commit;
begin
     FdataModulo.IbTransaction.commitretaining;   
end;

function TObjPortador.Get_Agencia: string;
begin
        Result:=Self.Agencia
end;

function TObjPortador.Get_Ativo: string;
begin
        Result:=Self.Ativo
end;

function TObjPortador.Get_Endereco: string;
begin
        Result:=Self.Endereco
end;

function TObjPortador.Get_Nome: string;
begin
        Result:=Self.Nome
end;

function TObjPortador.Get_NumeroConta: string;
begin
        Result:=Self.NumeroConta
end;

function TObjPortador.Get_Telefone: string;
begin
        Result:=Self.telefone;
end;

procedure TObjPortador.Submit_Agencia(parametro: string);
begin
     Self.Agencia:=parametro;
end;

procedure TObjPortador.Submit_Ativo(parametro: string);
begin
     Self.Ativo:=parametro;
end;

procedure TObjPortador.Submit_Endereco(parametro: string);
begin
     Self.Endereco:=parametro;
end;

procedure TObjPortador.Submit_Nome(parametro: string);
begin
     Self.Nome:=parametro;
end;

procedure TObjPortador.Submit_NumeroConta(parametro: string);
begin
     Self.NumeroConta:=parametro;
end;

procedure TObjPortador.Submit_Telefone(parametro: string);
begin
     Self.Telefone:=parametro;
end;

//Rodolfo
function TObjPortador.Get_PesquisaPortadorComCheque: string;
begin
    Result:=' Select * from TabPortador where PermiteEmissaoCheque=''S'' ';
end;

function TObjPortador.Get_Pesquisa: string;
begin
    Result:=' Select * from TabPortador ';
end;

function TObjPortador.Get_PesquisaApenasUsoSistema: String;
begin
     Result:=' Select * from TabPortador where apenas_uso_sistema=''N'' ';
end;

function TObjPortador.Get_TituloPesquisa: string;
begin
    Result:='Pesquisa de Portadores';
end;

{function TObjPortador.Get_Saldo: string;
begin
     Result:=Self.Saldo;
end;}

(*procedure TObjPortador.Submit_Saldo(parametro: string);
begin
     Self.Saldo:=Parametro;
end;*)

function TObjPortador.Get_PermiteEmissaoCheque: string;
begin
     Result:=Self.PermiteEmissaoCheque;
end;

procedure TObjPortador.Submit_PermiteEmissaoCheque(parametro: string);
begin
     Self.PermiteEmissaoCheque:=parametro;
end;

procedure TObjPortador.Get_ListaChequesPortador(ParametroPortador: string;
  ParametroListaGeral: TstringGrid; ComChequedoPortador: boolean;PVencimento:string);
var
   ChequesPortadorTemp:TObjChequesPortador;
begin

     Try
        ChequesPortadorTemp:=TObjChequesPortador.create;
     Except
           Messagedlg('Erro Durante a Cria��o do Objeto de Cheques por Portador!',mterror,[mbok],0);
           exit;
     End;

     Try
        ChequesPortadorTemp.get_ListaporPortador(ParametroPortador,ParametroListaGeral,ComChequedoPortador,Pvencimento);
     Finally
            ChequesPortadorTemp.free;
     End;

End;


procedure TObjPortador.Get_ListaChequesPortador(ParametroPortador: string;
  ParametroListaGeral: TstringGrid; ComChequedoPortador: boolean;PvencimentoInicial,PVencimentoFinal:string);
var
   ChequesPortadorTemp:TObjChequesPortador;
begin

     Try
        ChequesPortadorTemp:=TObjChequesPortador.create;
     Except
           Messagedlg('Erro Durante a Cria��o do Objeto de Cheques por Portador!',mterror,[mbok],0);
           exit;
     End;

     Try
        ChequesPortadorTemp.get_ListaporPortador(ParametroPortador,ParametroListaGeral,ComChequedoPortador,PvencimentoInicial,PvencimentoFinal);
     Finally
            ChequesPortadorTemp.free;
     End;

End;


procedure TObjPortador.Get_ListaCheques_do_Portador(ParametroPortador: string;
  ParametroListaGeral: TstringGrid);
var
   ChequesPortadorTemp:TObjChequesPortador;
begin

     Try
        ChequesPortadorTemp:=TObjChequesPortador.create;
     Except
           Messagedlg('Erro Durante a Cria��o do Objeto de Cheques por Portador!',mterror,[mbok],0);
           exit;
     End;

     Try
        ChequesPortadorTemp.get_Lista_do_Portador(ParametroPortador,ParametroListaGeral);
     Finally
            ChequesPortadorTemp.free;
     End;

End;


//Este procedimento � chamado quando tenho que transferir
//um cheque de um portador para outro

function TObjPortador.AtualizaChequePortador(ParametroPortador,
  ParametroCheque,ParametroPortadorDestino: string;HistoricoLanctoPortador:string;ImprimeOrigem,ImprimeDestino:Boolean;PData:string;Plancamentotransferencia:string): boolean;
var
   ChequesPortadorTemp:TObjChequesPortador;
begin

     Try
        ChequesPortadorTemp:=TObjChequesPortador.create;
     Except
           Messagedlg('Erro Durante a Cria��o do Objeto de Cheques por Portador!',mterror,[mbok],0);
           result:=False;
           exit;
     End;

     Try
        If (ChequesPortadorTemp.AtualizaChequePortador(ParametroPortador,ParametroCheque,ParametroPortadorDestino,HistoricoLanctoPortador,ImprimeOrigem,ImprimeDestino,PData,Plancamentotransferencia)=False)
        Then Begin
                 result:=False;
                 exit;
             End;
        result:=true;
     Finally
            ChequesPortadorTemp.free;
     End;

End;

procedure TObjPortador.Imprime(parametrocodigo: string);
begin
     With FMenuRelatorios do
     Begin
          NomeObjeto:='UOBJPORTADOR';
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Concilia��o Banc�ria (ordenada por Vencimento)');//0
                items.add('Concilia��o Banc�ria (ordenada por N� de Cheque)');//1
                items.add('Fluxo de Caixa Sint�tico Realizado');//2
                items.add('Fluxo de Caixa Anal�tico Realizado');//3
                items.add('Fluxo de Caixa(Todos Portadores Agrupados) Realizado');//4
                items.add('Fluxo de Caixa Anal�tico Realizado e Previsto');//5
                items.add('Rela��o de Cheques Emitidos por Vencimento');//6
                items.add('Extrato por portador');//7
                items.add('Rela��o de Cheques de 3� por Vencimento');//8
                items.add('Lan�amentos por Tipo de Lan�amento Anal�tico');//9
                items.add('Lan�amentos por Tipo de Lan�amento Sint�tico');//10
                items.add('Extrato por portador (Separado por Usuario)');//11
                items.add('Extrato por portador (N�O Separado por Usuario)');//12
                Items.add('Lista de Cheques Emitidos (Credor/Devedor)');//13
                //items.add('Rela��o de Cheques por Vencimento');//8
          End;
          showmodal;
          Self.NumeroRelatorio:=IntToStr(RgOpcoes.ItemIndex+1)+' - ';

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                //0:Self.ImprimeLancamento_por_Data(parametrocodigo);
                00:Self.ImprimeConciliacaoBancaria(Parametrocodigo,'V');
                01:Self.ImprimeConciliacaoBancaria(Parametrocodigo,'C');
                02:Self.ImprimeFluxodeCaixa(parametroCodigo,'S');
                03:Self.ImprimeFluxodeCaixa(parametroCodigo,'A');
                04:Self.ImprimeFluxodeCaixa(parametroCodigo,'T');
                05:Self.ImprimeFluxodeCaixa(parametroCodigo,'P');
                 06:Self.ImprimeChequesPortador_periodo(parametrocodigo);
                07:Self.ImprimeExtratoPortador(parametroCodigo);
                08:Self.ImprimeRelacaodeChequesTERCEIRO(parametrocodigo);
                09:Self.GeraFluxoporLancto(parametrocodigo,'A');
                10:Self.GeraFluxoporLancto(parametrocodigo,'S');
                11:Self.ImprimeExtratoPortador_Separado_Usuario(parametroCodigo);
                12:Self.ImprimeExtratoPortador_NAO_Separado_Usuario(parametroCodigo);
                13:Self.ImprimeChequesEmitidos(parametroCodigo);
          End;

     end;

end;


procedure TObjPortador.ImprimeLancamento_por_Data(parametrocodigo: string);
var
   codigoTmp:integer;
   saida:Boolean;
   comandosomasaldoanterior,complemento,Comando,comandoSoma:String;

   ant:byte;
   TmpData1,TmpData2:Tdate;


   SomaDebito,Somacredito:string[10];
   SaldoAnterior,SOmaDebitoAnterior,SomaCreditoAnterior:Currency;



begin

     ant:=0;

     comando:='';
     comandosoma:='';
     complemento:='';
     comandosomasaldoanterior:='';
     comandosoma:='Select sum(TabLanctoPortador.valor) as SomaTemp  from TabLanctoPortador';
     comandosoma:=comandosoma+' left join Tabtipolanctoportador on TabLanctoPortador.tipolancto=Tabtipolanctoportador.codigo ';
     comandosomasaldoanterior:=comandosoma;

     comando:=comando+'Select tablanctoportador.portador,tablanctoportador.data,';
     comando:=comando+' tablanctoportador.historico,tablanctoportador.valor,tabtipolanctoportador.debito_credito ';
     comando:=comando+' from tablanctoportador left join Tabtipolanctoportador on ';
     comando:=comando+' tablanctoportador.tipolancto=tabtipolanctoportador.codigo ';
     comando:=comando+' where imprimerel=''S'' ';

     Try
        codigoTmp:=strtoint(parametrocodigo);
        complemento:=complemento+' and portador='+Parametrocodigo;
        ant:=1;
        Self.LocalizaCodigo(parametrocodigo);
        self.TabelaparaObjeto;
     Except
           codigoTmp:=0;
     End;

     limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo01.caption:='Data 01';
          LbGrupo02.caption:='Data 02';
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          saida:=False;
          showmodal;
          If tag=0
          Then exit;
          Try
                TmpData1:=StrToDate(edtgrupo01.text);
                Tmpdata2:=Strtodate(edtgrupo02.text);
                complemento:=complemento+' and (data>='+#39+formatdatetime('mm/dd/yyyy',tmpdata1)+#39+' and data<='+#39+formatdatetime('mm/dd/yyyy',tmpdata2)+#39+')';
          Except
                If ant=0
                Then Begin
                        Messagedlg('N�o � poss�vel gerar Relat�rio sem escolher a Data ou o Portador',mterror,[mbok],0);
                        exit;
                     End;
          End;

          comando:=comando+complemento+' order by Tablanctoportador.portador,tablanctoportador.data,tablanctoportador.codigo';
     End;

     FRelatorio.QR.DataSet:=Self.ObjDataset;

     //Saldo Anterior
     If (codigoTMP<>0)
     Then Begin
               comandosomasaldoanterior:=comandosomasaldoanterior+' where data<'+#39+formatdatetime('mm/dd/yyyy',tmpdata1)+#39;
               comandosomasaldoanterior:=comandosomasaldoanterior+' and portador='+Parametrocodigo;

               //Cr�dito
               Self.ObjDataset.Close;
               Self.ObjDataset.SelectSQL.Clear;
               Self.ObjDataset.SelectSQL.add(comandosomasaldoanterior+'and imprimerel=''S'' and  debito_credito=''C''');
               Self.ObjDataset.Open;
               Try
                        SOmaCreditoAnterior:=strtofloat(Self.objdataset.fieldbyname('somatemp').asstring);
               Except
                     SOmaCreditoAnterior:=0;
               End;

               Self.ObjDataset.Close;
               Self.ObjDataset.SelectSQL.Clear;
               Self.ObjDataset.SelectSQL.add(comandosomasaldoanterior+'and imprimerel=''S'' and  debito_credito=''D''');
               Self.ObjDataset.Open;
               Try
                        SomaDebitoAnterior:=strtofloat(Self.objdataset.fieldbyname('somatemp').asstring);
               Except
                     SOmaDebitoAnterior:=0;
               End;
               SaldoAnterior:=SomaCreditoAnterior-SOmaDebitoAnterior;
     End;


     //cr�dito
     Self.ObjDataset.Close;
     Self.ObjDataset.SelectSQL.Clear;
     Self.ObjDataset.SelectSQL.add(comandosoma+'where imprimerel=''S'' and debito_credito=''C'''+complemento);
     Self.ObjDataset.Open;
     somacredito:='';
     somacredito:=Self.ObjDataset.fieldbyname('SomaTemp').asstring;
     if somacredito=''
     then somacredito:='0';
     //D�bito
     Self.ObjDataset.Close;
     Self.ObjDataset.SelectSQL.Clear;
     Self.ObjDataset.SelectSQL.add(comandosoma+'where imprimerel=''S'' and  debito_credito=''D'''+complemento);
     Self.ObjDataset.Open;
     somadebito:='';
     somadebito:=Self.ObjDataset.fieldbyname('SomaTemp').asstring;
     if somadebito=''
     then somadebito:='0';

     //comando final, onde filtrara os dados
     Self.ObjDataset.Close;
     Self.ObjDataset.SelectSQL.Clear;
     Self.ObjDataset.SelectSQL.add(comando);
     Self.ObjDataset.Open;

     //configurando os campos
     With Frelatorio do
     Begin
          Frelatorio.desativacampos;
          Frelatorio.titulo.caption:='RELAT�RIO DE MOVIMENTA��O DE CONTA -  DE '+datetostr(tmpdata1)+' A '+datetostr(tmpdata2);


          Campo01.Enabled:=True;
          ColTitulo01.Enabled:=True;
          ColTitulo01.Caption:='PT';
          Campo01.DataSet:=Self.ObjDataset;
          Campo01.DataField:='PORTADOR';

          ColTitulo0101.Enabled:=True;
          ColTitulo0101.Caption:='    DATA';
          Campo0101.Enabled:=True;
          Campo0101.DataSet:=Self.ObjDataset;
          Campo0101.DataField:='DATA';

          ColTitulo0201.Enabled:=True;
          ColTitulo0201.Caption:='                                                 HIST�RICO';
          Campo0201.Enabled:=True;
          Campo0201.DataSet:=Self.ObjDataset;
          Campo0201.DataField:='historico';
          campo0201.AutoSize:=False;
          campo0201.Width:=campo07.left-15-campo0201.left;


          ColTitulo07.Enabled:=True;
          ColTitulo07.Caption:='VALOR';
          ColTitulo07.Alignment:=taRightJustify;
          ColTitulo07.Left:=ColTitulo07.Left+30;
          Campo07.Enabled:=True;
          Campo07.DataSet:=Self.ObjDataset;
          Campo07.DataField:='VALOR';
          Campo07.Mask:='0.00';
          Campo07.Alignment:=taRightJustify;
          Campo07.left:=Campo07.left+30;


          ColTitulo08.Enabled:=True;
          ColTitulo08.Caption:='DC';
          Campo08.Enabled:=True;
          Campo08.DataSet:=Self.ObjDataset;
          Campo08.DataField:='DEBITO_CREDITO';

          If (codigoTmp<>0)
          Then Begin
                    LbCampoExpr01.enabled:=True;
                    LbCampoExpr01.Left:=01;
                    LbCampoExpr01.Top:=08;
                    LbCampoExpr01.Caption:='Saldo Anterior ao Per�odo: '+formata_valor(floattostr(SaldoAnterior));
          End;
          LbCampoExpr02.enabled:=True;
          LbCampoExpr02.Left:=01;
          LbCampoExpr02.Top:=28;
          LbCampoExpr02.Caption:='Total de Entradas no Per�odo: '+formatfloat('0.00',strtofloat(Somacredito));

          LbCampoExpr03.enabled:=True;
          lbCampoExpr03.Left:=01;
          lbCampoExpr03.Top:=48;
          LbCampoExpr03.caption:='Total de Sa�das no Per�odo: '+formatfloat('0.00',strtofloat(SomaDebito));

          If (parametrocodigo<>'')
          Then Begin//acrescentando o saldo do caixa
                        LbCampoExpr04.enabled:=True;
                        lbCampoExpr04.Left:=01;
                        lbCampoExpr04.Top:=68;
                        //LbCampoExpr04.caption:='SALDO ATUAL '+Self.Nome+' : R$ '+formata_valor(Self.Saldo);
                        LbCampoExpr04.caption:='SALDO ATUAL '+Self.Nome+' : R$ '+formata_valor('-99999');
          End;



    End;

     If Self.ObjDataset.RecordCount>0
     Then  Begin
                Frelatorio.qr.preview;

                Frelatorio.campo0201.AutoSize:=True;
                Frelatorio.Campo07.Alignment:=taLeftJustify;
                Frelatorio.Campo07.left:=Frelatorio.Campo07.left-25;
                Frelatorio.ColTitulo07.left:=Frelatorio.ColTitulo07.left-25;
                Frelatorio.ColTitulo07.Alignment:=taLeftJustify;
           End
     Else Messagedlg('N�o H� Movimenta��o',mtinformation,[mbok],0);



End;


(*function TObjPortador.CreditaSaldo(ParametroPortador,
  ParametroValor: string; ComCommit: boolean): Boolean;
var
ValorTemp:Currency;
begin

     If (Self.LocalizaCodigo(ParametroPortador)=False)
     Then Begin
               Messagedlg('Portador N�o Encontrado Para Atualizar Saldo',mterror,[mbok],0);
               result:=False;
               exit;
          End
     Else Self.TabelaparaObjeto;

     Try
        ValorTemp:=Strtofloat(ParametroValor);
     Except
           Messagedlg('Valor Inv�lido para Atualizar o Saldo',mterror,[mbok],0);
           result:=False;
           exit;
     End;


     try
       Self.Saldo:=floattostr(StrToFloat(Self.Saldo)+strtofloat(floattostr(ValorTemp)));
     Except
           Messagedlg('Erro durante a Tentativa de Atualiza��o dos Valores do Saldo',mterror,[mbok],0);
           result:=False;
           exit;
     End;

     Self.Status:=dsedit;
     If (Self.Salvar(ComCommit)=False)
     Then Begin
                Messagedlg('Erro durante a Tentativa de Salvar a Atualiza��o dos Valores do Saldo',mterror,[mbok],0);
                result:=False;
                exit;
          End;
     Result:=True;

end;*)

(*function TObjPortador.DebitaSaldo(ParametroPortador, ParametroValor: string;
  ComCommit: boolean): Boolean;
var
  Valortemp:Currency;
begin
     result:=False;
     If (Self.LocalizaCodigo(ParametroPortador)=False)
     Then Begin
               Messagedlg('Portador N�o Encontrado Para Atualizar Saldo',mterror,[mbok],0);
               exit;
          End
     Else Self.TabelaparaObjeto;

     Try
        ValorTemp:=Strtofloat(ParametroValor);
     Except
           Messagedlg('Valor Inv�lido para Atualizar o Saldo',mterror,[mbok],0);
           exit;
     End;


     try
       Self.Saldo:=floattostr(StrToFloat(Self.Saldo)-strtofloat(floattostr(ValorTemp)));
     Except
           Messagedlg('Erro durante a Tentativa de Atualiza��o dos Valores do Saldo',mterror,[mbok],0);
           exit;
     End;

     if (strtofloat(Self.Saldo)<0)
     Then Begin
               if (ObjPermissoesUsoGlobal.ValidaPermissao('TORNAR O SALDO DO PORTADOR NEGATIVO')=False)
               Then exit;

               if (Self.permite_saldo_negativo='N')
               Then Begin
                         Messagedlg('O portador '+Self.codigo+' - '+Self.nome+' n�o permite Saldo Negativo',mterror,[mbok],0);
                         exit;
               End;
     End;

     Self.Status:=dsedit;
     If (Self.Salvar(ComCommit)=False)
     Then Begin
                Messagedlg('Erro durante a Tentativa de Salvar a Atualiza��o dos Valores do Saldo',mterror,[mbok],0);
                exit;
     End;

     Result:=true;
end;*)


destructor TObjPortador.Free;
begin
     freeandnil(Self.ObjDataset);
     freeandnil(Self.QueryLocal);
     freeandnil(Self.QueryLocal2);

     Self.PlanodeContas.free;
     Self.HIstoricoContabil.free;
end;

function TObjPortador.Get_CodigoPlanodeContas: string;
begin
     Result:=self.codigoplanodecontas;
end;

procedure TObjPortador.Submit_CodigoPlanodeContas(parametro: string);
begin
     Self.codigoplanodecontas:=Parametro;
end;

function TObjPortador.Get_PesquisaPlanodeContas: TStringList;
begin
     result:=Self.PlanodeContas.Get_Pesquisa;
end;

function TObjPortador.Get_TituloPesquisaPlanodeContas: string;
begin
     result:=self.PlanodeContas.Get_TituloPesquisa;
end;

procedure TObjPortador.ImprimeChequesPortador_periodo(ParametroPortador:string);
var
TmpData1,TmpData2:TDate;
portadortemporario:string;
QueryTemp:Tibquery;
ValorTotalTMp:Currency;
linha:integer;
Begin
     Try
        QueryTemp:=TIBQuery.create(nil);
     Except
           Messagedlg('Erro na cria��o da Query Tempor�ria!',mterror,[mbok],0);
           exit;
     End;
     ValorTotalTMp:=0;
     QueryTemp.database:=Self.ObjDataset.Database;
     QueryTemp.close;
     QueryTemp.sql.clear;

Try
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          showmodal;
          If tag=0
          Then exit;

          Try
                TmpData1:=StrToDate(edtgrupo01.text);
                Tmpdata2:=Strtodate(edtgrupo02.text);
                If (TmpData2<TmpData1)
                Then Begin
                          Messagedlg('A Data Final n�o pode ser menor que a Data Inicial!',mtinformation,[mbok],0);
                          exit;
                End;
                
          Except
                Messagedlg('N�o � poss�vel gerar Relat�rio sem escolher a Data ou o Portador',mterror,[mbok],0);
                exit;
          End;
     End;
     With QueryTemp do
     Begin
          close;
          SQL.clear;
          Sql.add('select distinct(TabChequesPortador.codigo) as CodigoCheque,tabtransferenciaportador.Data as DATAEMISSAOCHEQUE,');
          Sql.add('TabTransferenciaPortador.PortadorOrigem as PORTADORTRANSFERENCIA,TabTalaodeCheques.NUMERO as NUMEROCHEQUE,');
          Sql.add('TabTransferenciaPortador.PortadorDestino as PORTADORDESTINOTRANSFERENCIA,');
          Sql.add('TabTalaodeCheques.Portador as PORTADORCHEQUE,TabTalaodeCheques.HistoricoPagamento,TabTalaodeCheques.Valor as VALORDOCHEQUE,');
          Sql.add('TabChequesPortador.Portador as PortadorAtual,');
          Sql.add('TabTransferenciaPortador.CodigoLancamento,');
          //celio
          Sql.add('TabTalaodeCheques.vencimento');
          //fim celio
          Sql.add('from TabTalaodeCheques');
          Sql.add('join TabChequesPortador on TabTalaodeCheques.codigochequeportador=TabChequesPortador.codigo');
          Sql.add('Join TabValoresTransferenciaPortador on TabChequesPortador.codigo=TabValoresTransferenciaPortador.valores');
          Sql.add('join TabTransferenciaPortador on TabValoresTransferenciaPOrtador.Transferenciaportador=TabTransferenciaPortador.codigo');
          Sql.add('where TabTransferenciaPortador.PortadorOrigem=TabTransferenciaPortador.PortadorDestino');
          //Sql.add('and not(TabTransferenciaPortador.codigolancamento is null)');
          Sql.add('and TabChequesPortador.chequedoportador=''S'' ');
          //Sql.add('and(tabtransferenciaportador.Data>='+#39+FormatDateTime('mm/dd/yyyy',Tmpdata1)+#39+' and tabtransferenciaportador.Data<='+#39+Formatdatetime('mm/dd/yyyy',Tmpdata2)+#39+')');
          Sql.add('and(TabTalaodeCheques.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',Tmpdata1)+#39+' and TabTalaodeCheques.vencimento<='+#39+Formatdatetime('mm/dd/yyyy',Tmpdata2)+#39+')');
          if (ParametroPortador<>'')
          Then SQL.add(' and TabTalaodeCheques.portador='+parametroportador);
          SQL.add(' ORDER BY TABTALAODECHEQUES.PORTADOR,TABTRANSFERENCIAPORTADOR.DATA');

          //InputBox('','',sql.Text);

          open;
          first;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhum dado foi selecionado na pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          FreltxtRDPRINT.rdprint.Setup;
          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'LISTAGEM DE CHEQUES EMITIDOS',[negrito]);
          inc(linha,2);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Intervalo de vencimento: '+datetostr(TmpData1)+' a '+datetostr(TmpData2),[negrito]);
          inc(linha,1);

          //FrelTXT.SB.Items.clear;
          FreltxtRDPRINT.RDprint.impf(linha,1,CompletaPalavra('Codigo',6,' ')+' '+CompletaPalavra('Hist�rico',50,' ')+' '+CompletaPalavra('N�Cheque',09,' ')+' '+CompletaPalavra('Vencimento',10,' ')+' '+CompletaPalavra('Valor',12,' '),[negrito]);
          inc(linha,1);
          Self.LocalizaCodigo(fieldbyname('PortadorCheque').asstring);
          Self.TabelaparaObjeto;
          FreltxtRDPRINT.RDprint.impf(linha,1,'Portador: '+Self.CODIGO+' - '+Self.Nome,[negrito]);
          inc(linha,2);

          portadortemporario:='';
          portadortemporario:=fieldbyname('PortadorCheque').asstring;

          While not(eof) do
          Begin
               ValorTotalTMp:=ValorTotalTMp+fieldbyname('VALORDOCHEQUE').asfloat;
               
               If (portadortemporario<>fieldbyname('PortadorCheque').asstring)
               Then Begin
                         Self.LocalizaCodigo(fieldbyname('PortadorCheque').asstring);
                         Self.TabelaparaObjeto;
                         FreltxtRDPRINT.RDprint.impf(linha,1,'Portador: '+Self.CODIGO+' - '+Self.Nome,[negrito]);
                         inc(linha,1);
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         portadortemporario:=fieldbyname('PortadorCheque').asstring;
               End;
               FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra(fieldbyname('CodigoCheque').asstring,6,' ')+' '+CompletaPalavra(fieldbyname('historicopagamento').asstring,50,' ')+' '+CompletaPalavra(fieldbyname('NumeroCheque').asstring,09,' ')+' '+CompletaPalavra(fieldbyname('VENCIMENTO').asstring,10,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORDOCHEQUE').asstring),12,' '));
               inc(linha,1);
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               next;
          End;
          FreltxtRDPRINT.RDprint.impf(linha,1,'Soma dos Cheques: '+formata_valor(floattostr(ValorTotalTMp)),[negrito]);
          FreltxtRDPRINT.rdprint.fechar;


     End;
     
Finally
       Freeandnil(QueryTemp);
End;

     

End;

procedure TObjPortador.ImprimeConciliacaoBancaria(Parametrocodigo: string;Ordenacao:string);
var
   ChequesPortadorTemp:TObjChequesPortador;
begin

     Try
        ChequesPortadorTemp:=TObjChequesPortador.create;
     Except
           Messagedlg('Erro Durante a Cria��o do Objeto de Cheques por Portador!',mterror,[mbok],0);
           exit;
     End;

     Try
        ChequesPortadorTemp.Imprime_Conciliacao(ParametroCodigo,Ordenacao, Self.NumeroRelatorio);
     Finally
            ChequesPortadorTemp.free;
     End;

End;


function TObjPortador.Get_PesquisaPermiteEmissao: string;
begin
     Result:='Select * from Tabportador where permiteemissaocheque=''S'' ';
end;

Procedure TObjPortador.ImprimeFluxodeCaixa(parametroCodigo: string;SA:string);
var
   TmpData1,TmpData2:Tdate;
   CodigoTmp,TmpNumDias:Integer;
   Qlocal:tibquery;
begin


     Self.ZerarTabela;
     If (Parametrocodigo<>'')
     Then Begin
               If (Self.LocalizaCodigo(parametroCodigo)=False)
               Then parametrocodigo:=''
               Else Self.TabelaparaObjeto;
     End;

     limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;

          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          showmodal;
          If tag=0
          Then exit;

          Try
                TmpData1:=StrToDate(edtgrupo01.text);
                Tmpdata2:=Strtodate(edtgrupo02.text);
                If (TmpData2<TmpData1)
                Then Begin
                          Messagedlg('A Data Final n�o pode ser menor que a Data Inicial!',mtinformation,[mbok],0);
                          exit;
                End;

          Except
                Messagedlg('N�o � poss�vel gerar Relat�rio sem escolher a Data ou o Portador',mterror,[mbok],0);
                exit;
          End;

          Try
                codigotmp:=strtoint(Parametrocodigo);
          Except
                codigotmp:=-1;
          End;

     End;

     Try
        QLocal:=Tibquery.create(nil);
        QLocal.close;
        QLocal.database:=Self.Objdataset.database;
     Except
        Messagedlg('Erro na Tentativa de Criar o Objeto de Lancto em Portadores ou da Query Local!',mterror,[mbok],0);
        exit;
     End;

     Try
        If (SA='S')//Sint�tico
        Then Self.GeraFluxoSinteticonovo(Qlocal,CodigoTmp,TmpData1,TmpData2,TmpNumDias)
        Else
             If (SA='A')//Analitico sem previsao
             Then Self.GeraFluxoAnalitico(qlocal,codigotmp,TmpData1,TmpData2,False)
             Else
                If (SA='T')//Totalizado por todos os portadores
                Then Self.GeraFluxoSinteticoTotalizado(Qlocal,TmpData1,TmpData2,TmpNumDias)
                Else
                    If (SA='P')//Analitico com Previs�o Futura
                    Then Self.GeraFluxoAnalitico(qlocal,codigotmp,TmpData1,TmpData2,True);
     Finally
            freeandnil(Qlocal);
     End;

End;


function TObjPortador.GeraFluxoSinteticoNovo(PQlocal: tibquery;
  Pportador: Integer; PdataInicial, PdataFinal: Tdatetime;PNumdias:Integer): boolean;
var
   PrimeiroSaldo,SaldoAnterior,SaldoFinal,SomaEntrada,SomaEntradaTotal,SomaSaida,SomaSaidaTotal:Currency;
   DataTmp:Tdatetime;
   quantdias:extended;
   cont:integer;
begin
     With PQlocal do
     Begin
          close;//seleciono os portadores
          SQL.clear;
          SQL.add(' Select codigo,nome from Tabportador');
          If ( Pportador<>-1)
          Then SQL.add(' where codigo='+inttostr(pportador))
          Else SQL.add(' where TabPortador.apenas_uso_sistema=''N'' ');

          SQL.add(' order by codigo');

          open;

          If (Recordcount=0)
          Then Begin
                    Messagedlg('N�o h� dados para serem impressos!',mterror,[mbok],0);
                    exit;
          End;
          first;

          FrelTXT.SB.Items.clear;
          //cabe�alho da colunas
          // o '?' � sinal de negrito para o relat�rio, eu que defini isto
          FrelTXT.SB.Items.add('?'+CompletaPalavra('PORT.',5,' ')+'|'+completapalavra('INTERVALO',17,' ')+'|'+CompletaPalavra_a_Esquerda('SALDO INICIAL',13,' ')+'|'+CompletaPalavra_a_Esquerda('ENTRADAS',13,' ')+'|'+CompletaPalavra_a_Esquerda('SA�DAS',13,' ')+'|'+CompletaPalavra_a_Esquerda('SALDO FINAL',13,' '));
          FrelTXT.SB.Items.add(CompletaPalavra('-',5,'-')+'-'+completapalavra('-',17,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));

          while not(eof) do
          Begin//PERCORRE TODOS OS PORTADORES

                SomaEntradaTotal:=0;
                SomaSaidaTotal:=0;
                //****NOME DO PORTADOR****
                FrelTXT.SB.Items.add('?'+fieldbyname('nome').asstring);
                //calculando quantos ira percorrer no for
                quantdias:=Pdatafinal-PdataInicial;
                //calculando o saldo inicial do primeiro dia (saldo anterior)
                SaldoAnterior:=strtofloat(Self.proc_SaldoAnterior(fieldbyname('codigo').asstring,datetostr(PdataInicial)));
                PrimeiroSaldo:=SaldoAnterior;
                //percorrendo todos os dias
                for cont:=0 to strtoint(floattostr(int(quantdias))) do
                Begin
                     DataTmp:=PdataInicial+cont;

                     SomaEntrada:=0;
                     SomaSaida:=0;
                     SomaEntrada:=Self.SomaCreditos(fieldbyname('codigo').asstring,DataTmp);
                     SomaSaida:=Self.SomaDebitos(fieldbyname('codigo').asstring,DataTmp);
                     SaldoFinal:=(SaldoAnterior+SomaEntrada)-SomaSaida;
                     SomaEntradaTotal:=SomaEntradaTotal+SomaEntrada;
                     SomaSaidaTotal:=SomaSaidaTotal+SomaSaida;
                     FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+completapalavra(formatdatetime('dd/mm',DataTmp)+' a '+formatdatetime('dd/mm/yy',DataTmp),17,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAnterior)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));
                     SaldoAnterior:=SaldoFinal;
                End;
                //totalizando as entradas e saidas
                FrelTXT.SB.Items.add(CompletaPalavra('?',5,' ')+' '+completapalavra('TOTALIZA��O',17,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PrimeiroSaldo)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntradaTotal)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaidaTotal)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(((PRIMEIROSALDO+SOMAENTRADATOTAL)-SOMASAIDATOTAL))),13,' '));
                FrelTXT.SB.Items.add(CompletaPalavra('-',5,'-')+'-'+completapalavra('-',17,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
                next;
          End;

     End;

         //configurando os campos
     With FRELTXT do
     Begin

             LimpaCampos;//LIMPA E DESATIVA

             LBTITULO.Enabled:=True;
             LBTITULO.caption:=Self.NumeroRelatorio+'FLUXO DE CAIXA SINT�TICO - DE '+datetostr(PdataInicial)+' A '+datetostr(PdataFinal);
             If (Pportador<>-1)
             Then Begin
                       LbSubtitulo1.enabled:=True;
                       LbSubtitulo1.Caption:='Portador '+Self.Get_Nome;
             End;

             Qr.Page.Orientation:=poPortrait;
             Qr.preview;
     End;


end;


function TObjPortador.GeraFluxoSinteticoTotalizado(PQlocal: tibquery;
  PdataInicial, PdataFinal: Tdatetime; PNumdias: Integer): boolean;
var
   SaldoAnterior,SaldoFinal,SomaEntrada,SomaSaida:Currency;
   quantdias:extended;
   cont:integer;
   DataTmp:Tdatetime;
begin
     With PQlocal do
     Begin
          FrelTXT.SB.Items.clear;
          //cabe�alho da colunas
          // o '?' � sinal de negrito para o relat�rio, eu que defini isto
          FrelTXT.SB.Items.add('?'+completapalavra('INTERVALO',17,' ')+'|'+CompletaPalavra_a_Esquerda('SALDO INICIAL',13,' ')+'|'+CompletaPalavra_a_Esquerda('ENTRADAS',13,' ')+'|'+CompletaPalavra_a_Esquerda('SA�DAS',13,' ')+'|'+CompletaPalavra_a_Esquerda('SALDO FINAL',13,' '));
          FrelTXT.SB.Items.add(completapalavra('-',17,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));

          quantdias:=Pdatafinal-PdataInicial;
          //calculando o saldo inicial do primeiro dia (saldo anterior)
          SaldoAnterior:=Self.PegaSaldoAnteriorTotal(PdataInicial);
          //percorrendo todos os dias
          for cont:=0 to strtoint(floattostr(int(quantdias))) do
          Begin
               DataTmp:=PdataInicial+cont;
               SomaEntrada:=0;
               SomaSaida:=0;
               SomaEntrada:=Self.SomaCreditos('',DataTmp);
               SomaSaida:=Self.SomaDebitos('',DataTmp);
               SaldoFinal:=(SaldoAnterior+SomaEntrada)-SomaSaida;
               FrelTXT.SB.Items.add(completapalavra(formatdatetime('dd/mm',DataTmp)+' a '+formatdatetime('dd/mm/yy',datatmp),17,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoAnterior)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));
               SaldoAnterior:=SaldoFinal;
          End;


     End;

    //configurando os campos
     With FRELTXT do
     Begin
             LimpaCampos;//LIMPA E DESATIVA
             LBTITULO.Enabled:=True;
             LBTITULO.caption:=Self.NumeroRelatorio+'FLUXO DE CAIXA SINT�TICO AGRUPANDO TODOS OS PORTADORES - DE '+datetostr(PdataInicial)+' A '+datetostr(PdataFinal);
             Qr.Page.Orientation:=poPortrait;
             Qr.preview;
     End;


end;

function TObjPortador.PegaSaldoAnteriorTotal(Pdata: Tdatetime): Currency;
var
valorcredito,valordebito,SaldoFInal:currency;
begin
     Try

        With Self.ObjDataset do
        Begin
                  close;
                  SelectSQL.clear;
                  SelectSQL.add('SELECT sum(tablanctoportador.valor) as soma');
                  SelectSQL.add('from tablanctoportador left join Tabtipolanctoportador on');
                  SelectSQL.add('tablanctoportador.tipolancto=tabtipolanctoportador.codigo');
                  SelectSQL.add('where imprimerel=''S'' and tablanctoportador.data<'+#39+FormatDateTime('mm/dd/yyyy',Pdata)+#39);
                  SelectSQL.add('AND TABTIPOLANCTOPORTADOR.DEBITO_CREDITO=''C'' ');
                  open;

                  If (Fieldbyname('soma').asstring='')
                  Then valorcredito:=0
                  Else valorcredito:=fieldbyname('soma').asfloat;

                  close;
                  SelectSQL.clear;
                  SelectSQL.add('SELECT sum(tablanctoportador.valor) as soma');
                  SelectSQL.add('from tablanctoportador left join Tabtipolanctoportador on');
                  SelectSQL.add('tablanctoportador.tipolancto=tabtipolanctoportador.codigo');
                  SelectSQL.add('where imprimerel=''S'' and tablanctoportador.data<'+#39+FormatDateTime('mm/dd/yyyy',Pdata)+#39);
                  SelectSQL.add('AND TABTIPOLANCTOPORTADOR.DEBITO_CREDITO=''D'' ');
                  open;

                  If (Fieldbyname('soma').asstring='')
                  Then valordebito:=0
                  Else valordebito:=fieldbyname('soma').asfloat;

                  SaldoFInal:=valorcredito-valordebito;

                  result:=SaldoFinal;

        End;

     Except
           Messagedlg('Erro no retorno do Saldo Anterior de todos os portadores!',mterror,[mbok],0);
           result:=0;
           exit;
     End;
end;
//    fazer o relatorio com a previsao e fazer o cadastro de previs�o

function TObjPortador.Get_Apenas_Uso_Sistema: string;
begin
     Result:=Self.Apenas_Uso_Sistema;
end;

procedure TObjPortador.Submit_Apenas_Uso_Sistema(parametro: string);
begin
     Self.Apenas_Uso_Sistema:=parametro;
end;


procedure TObjPortador.ImprimePrevisto(PPortador: string; PDataInicial,
  PDataFinal: Tdatetime;PSaldoInicial:Currency);
var
saldoinicial,saldofinal,somaentrada,somasaida:currency;
ultimoportador:string;
PrimeiroSaldo,SomaEntradaTotal,SomaSaidaTotal:Currency;
begin
     //esse procedimento � chamado quando no relatorio de fluxo de caixa analitico
     //com previsao acontece a mudanca de um portador para outro
     //antes de iniciar um novo portador � necess�rio totalizar o anterior com o previsto
     
     With Self.ObjDataset do
     Begin
          //primeiro seleciono o que esta previsto atraves do cadastro de t�tulo
          close;
          SelectSQL.clear;
          SelectSql.add('Select tabtitulo.codigo,tabtitulo.historico,tabpendencia.vencimento,tabcontager.tipo as DC,tabpendencia.saldo');
          SelectSql.add('from tabpendencia left join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');

          SelectSql.add('where (tabpendencia.vencimento>='+#39+formatDateTime('mm/dd/yyyy',PDataInicial)+#39+' and tabpendencia.vencimento<='+#39+formatDateTime('mm/dd/yyyy',PDataFinal)+#39+')');
          SelectSQL.add('and tabtitulo.portador='+Pportador+' and tabpendencia.saldo>0');
          SelectSQL.add('order by tabpendencia.vencimento,tabpendencia.saldo');
          open;
          SaldoInicial:=PSaldoInicial;
          SaldoFinal:=SaldoInicial;
          PrimeiroSaldo:=PSaldoInicial;
          SomaEntradaTotal:=0;
          SomaSaidatotal:=0;
          If (recordcount>0)
          Then Begin

                    SaldoInicial:=PSaldoInicial;//O Saldo inicial ja vem do procedimento
                                     //que o chamou

                    SomaEntrada:=0;
                    SomaSaida:=0;
                    If (FieldByName('dc').ASSTRING='C')
                    Then SomaEntrada:=Fieldbyname('saldo').asfloat
                    Else SomaSaida:=Fieldbyname('saldo').asfloat;

                    SomaEntradaTotal:=SomaEntradaTotal+SomaEntrada;
                    SomaSaidaTotal:=SomaSaidaTotal+SomaSaida;

                    SaldoFinal:=(SaldoInicial+SomaEntrada)-SomaSaida;
                    FrelTXT.SB.Items.add('********LAN�AMENTOS FUTUROS********');
                    FrelTXT.SB.Items.add(completapalavra(Pportador,5,' ')+'|'+CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,60,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('vencimento').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));
                    next;
          End;
          while not(eof) do
          Begin
               SaldoInicial:=saldofinal;
               SomaEntrada:=0;
               SomaSaida:=0;
               If (FieldByName('dc').ASSTRING='C')
               Then SomaEntrada:=Fieldbyname('saldo').asfloat
               Else SomaSaida:=Fieldbyname('saldo').asfloat;
               SomaEntradaTotal:=SomaEntradaTotal+SomaEntrada;
               SomaSaidaTotal:=SomaSaidaTotal+SomaSaida;

               SaldoFinal:=(SaldoInicial+SomaEntrada)-SomaSaida;
               FrelTXT.SB.Items.add(completapalavra(Pportador,5,' ')+'|'+CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,60,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('vencimento').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));
               next;
          End;
          //totalizando a previsao
          If (recordcount>0)
          Then FrelTXT.SB.Items.add(completapalavra('?',5,' ')+'|'+CompletaPalavra(' ',5,' ')+'|'+CompletaPalavra('TOTALIZA��O',60,' ')+'|'+completapalavra(' ',08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PRIMEIROSALDO)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntradatOTAL)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaidatOTAL)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr((Primeirosaldo+SomaEntradaTotal)-SomaSaidaTotal)),13,' '));


         //depois o que esta previsto atraves do cadastro de previsao
          close;
          SelectSQL.clear;
          SelectSql.add('Select codigo,historico,data as vencimento,valor as saldo,debito_credito as DC');
          SelectSql.add('from TabPrevisaoFinanceira');
          SelectSql.add('where (data>='+#39+formatDateTime('mm/dd/yyyy',PDataInicial)+#39+' and data<='+#39+formatDateTime('mm/dd/yyyy',PDataFinal)+#39+')');
          SelectSQL.add('and portador='+PPortador);
          SelectSQL.add('order by data');
          open;
          If (Recordcount=0)
          Then exit;
          SaldoInicial:=SaldoFinal;
          
          SomaEntrada:=0;
          SomaSaida:=0;
          If (FieldByName('dc').ASSTRING='C')
          Then SomaEntrada:=Fieldbyname('saldo').asfloat
          Else SomaSaida:=Fieldbyname('saldo').asfloat;
          SaldoFinal:=(SaldoInicial+SomaEntrada)-SomaSaida;
          FrelTXT.SB.Items.add('********PREVIS�O DE LAN�AMENTOS********');
          FrelTXT.SB.Items.add(completapalavra(Pportador,5,' ')+'|'+CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,60,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('vencimento').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));
          next;

          while not(eof) do
          Begin
               SaldoInicial:=saldofinal;
               SomaEntrada:=0;
               SomaSaida:=0;
               If (FieldByName('dc').ASSTRING='C')
               Then SomaEntrada:=Fieldbyname('saldo').asfloat
               Else SomaSaida:=Fieldbyname('saldo').asfloat;
               SaldoFinal:=(SaldoInicial+SomaEntrada)-SomaSaida;
               FrelTXT.SB.Items.add(completapalavra(Pportador,5,' ')+'|'+CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,60,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('vencimento').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));
               next;
          End;

     End;
End;


//Este procedimento soma todos os creditos que acontecerem na data de parametro
//e no portador de parametro, caso o portador seja='' � somado de todos os portadores
//de acordo com o imprimerel='S'
function TObjPortador.SomaCreditos(Pportador: string;
  Pdata: Tdatetime): Currency;
begin
     With Self.ObjDataset do
     Begin
          close;
          selectSQL.clear;
          selectSQL.add(' Select sum(tablanctoportador.valor) as soma');
          selectSQL.add(' from tablanctoportador left join Tabtipolanctoportador on');
          selectSQL.add(' tablanctoportador.tipolancto=tabtipolanctoportador.codigo left Join Tabportador');
          selectSQL.add(' on TabLanctoPortador.Portador=Tabportador.codigo');
          selectSQL.add(' where imprimerel=''S''');
          selectSQL.add(' and data='+#39+formatdatetime('mm/dd/yyyy',Pdata)+#39);
          If (pportador<>'')
          Then selectSQL.add(' and TabLanctoPortador.Portador='+pportador)
          Else selectSQL.add(' and TabPortador.apenas_uso_sistema=''N'' ');

          selectSQL.add(' and tabtipolanctoportador.debito_credito=''C'' ');

          open;
          If (fieldbyname('soma').asstring='')
          Then result:=0
          Else result:=fieldbyname('soma').asfloat;
     End;


end;

//Este procedimento soma todos os debitos que acontecerem na data de parametro
//e no portador de parametro, caso o portador seja='' � somado de todos os portadores
//de acordo com o imprimerel='S'
function TObjPortador.SomaDebitos(Pportador: string;
  Pdata: Tdatetime): Currency;
begin
     With Self.ObjDataset do
     Begin
          close;
          selectSQL.clear;
          selectSQL.add(' Select sum(tablanctoportador.valor) as soma');
          selectSQL.add(' from tablanctoportador left join Tabtipolanctoportador on');
          selectSQL.add(' tablanctoportador.tipolancto=tabtipolanctoportador.codigo left Join Tabportador');
          selectSQL.add(' on TabLanctoPortador.Portador=Tabportador.codigo');
          selectSQL.add(' where imprimerel=''S''');
          selectSQL.add(' and data='+#39+formatdatetime('mm/dd/yyyy',Pdata)+#39);
          If (pportador<>'')
          Then selectSQL.add(' and TabLanctoPortador.Portador='+pportador)
          Else selectSQL.add(' and TabPortador.apenas_uso_sistema=''N'' ');

          selectSQL.add(' and tabtipolanctoportador.debito_credito=''D'' ');

          open;
          If (fieldbyname('soma').asstring='')
          Then result:=0
          Else result:=fieldbyname('soma').asfloat;
     End;

end;

Function TObjPortador.GeraFluxoAnalitico(PQlocal:tibquery;Pportador: Integer; PdataInicial,
  PdataFinal: TdateTime;ComPrevisao:boolean):boolean;
var
PrimeiroSaldo,saldoinicial,saldofinal,entrada,saida,SomaEntrada,SomaSaida:currency;
TituloQueGerou,ultimoportador:string;
Begin

      With PQlocal do
      Begin
             close;
             SQL.clear;
             SQL.add(' Select tablanctoportador.portador,tablanctoportador.codigo,tablanctoportador.historico,tablanctoportador.data,tablanctoportador.valor,tabtipolanctoportador.debito_credito as DC,');
             SQL.add(' tablanctoportador.OBJETOGERADOR,tablanctoportador.CAMPOPRIMARIO,tablanctoportador.VALORDOCAMPO,tablanctoportador.lancamento');
             SQL.add(' from tablanctoportador left join Tabtipolanctoportador on');
             SQL.add(' tablanctoportador.tipolancto=tabtipolanctoportador.codigo left Join Tabportador');
             SQL.add(' on TabLanctoPortador.Portador=Tabportador.codigo');
             SQL.add(' where imprimerel=''S''');
             SQL.add(' and (data>='+#39+formatdatetime('mm/dd/yyyy',PdataInicial)+#39+' and data<='+#39+formatdatetime('mm/dd/yyyy',PdataFinal)+#39+')');
             If (Pportador<>-1)
             Then SQL.add(' and TabLanctoPortador.Portador='+Inttostr(Pportador))
             Else SQL.add(' and TabPortador.apenas_uso_sistema=''N'' ');
             SQL.add(' order by tablanctoportador.portador,tablanctoportador.data,tablanctoportador.codigo');
             open;
             If (Recordcount=0)
             Then Begin
                       Messagedlg('N�o h� dados para serem impressos!',mterror,[mbok],0);
                       exit;
             End;
             //tenho uma listagem assim
             //CODIGO/HISTORICO/data / VALOR/ Cr�dito/D�bito
             //preciso fazer algo assim
             first;
             entrada:=0;
             saida:=0;
             UltimoPortador:=fieldbyname('Portador').asstring;
             FrelTXT.SB.Items.clear;

             //*******CABE�ALHO***********
             //o 1� caracter='?' significa negrito nesta linha do relat�rio
             FrelTXT.SB.Items.add('?'+CompletaPalavra('CODIGO',6,' ')+'|'+completapalavra('HIST�RICO',60,' ')+'|'+completapalavra('TITULO',06,' ')+'|'+completapalavra('DATA',08,' ')+'|'+CompletaPalavra_a_Esquerda('SALDO INICIAL',13,' ')+'|'+CompletaPalavra_a_Esquerda('ENTRADAS',13,' ')+'|'+CompletaPalavra_a_Esquerda('SA�DAS',13,' ')+'|'+CompletaPalavra_a_Esquerda('SALDO FINAL',13,' '));
             FrelTXT.SB.Items.add(CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
             //nome do primeiro portador
             Self.LocalizaCodigo(fieldbyname('portador').asstring);
             Self.TabelaparaObjeto;
             FrelTXT.SB.Items.add('?'+completapalavra(fieldbyname('portador').asstring,5,' ')+' - '+Self.Nome);
             //************************

             //imprimo o primeiro registro com o saldo anterior correto dele
              SaldoInicial:=strtofloat(Self.proc_SaldoAnterior(fieldbyname('portador').asstring,fieldbyname('data').asstring));

              PrimeiroSaldo:=SaldoInicial;
              Entrada:=0;
              Saida:=0;
              SomaEntrada:=0;
              SomaSaida:=0;

              If (FieldByName('dc').ASSTRING='C')
              Then Begin
                        Entrada:=Fieldbyname('valor').asfloat;
                        SomaEntrada:=SomaEntrada+Entrada;
              End
              Else Begin
                        Saida:=Fieldbyname('valor').asfloat;
                        SomaSaida:=SomaSaida+Saida;
              End;


              SaldoFinal:=(SaldoInicial+Entrada)-Saida;

              TituloqueGerou:=Self.LocalizaTituloGERADOR(uppercase(fieldbyname('OBJETOGERADOR').asstring),fieldbyname('valordocampo').asstring);
              FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,60,' ')+'|'+completapalavra(TituloQueGerou,6,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('Data').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(Entrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(Saida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));

              ultimoportador:=fieldbyname('portador').asstring;
              //****vai para o segundo***** 
              next;
              while not(eof) do
              Begin

                  If (UltimoPortador<>fielDbyname('portador').asstring)
                  Then Begin
                            //trocou o portador, tenho que totalizar
                            //todas as entradas e saidas e o saldo final
                            FrelTXT.SB.Items.add(completapalavra('?',6,' ')+' '+CompletaPalavra('',5,' ')+' '+CompletaPalavra('TOTALIZA��O',60,' ')+' '+completapalavra(' ',08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PrimeiroSaldo)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))),13,' '));
                            FrelTXT.SB.Items.add(CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
                            If (ComPrevisao=True)
                            Then Begin
                                Self.ImprimePrevisto(UltimoPortador,PdataInicial,PdataFinal,SaldoFinal);
                            End;

                            Self.LocalizaCodigo(fieldbyname('portador').asstring);
                            Self.TabelaparaObjeto;
                            FrelTXT.SB.Items.add('?'+completapalavra(fieldbyname('portador').asstring,5,' ')+'-'+Self.Nome);
                            //acerto o saldo inicial quanto troco de portador, quando nao apenas pego o final e vou trabalhando
                            SaldoInicial:=strtofloat(Self.proc_SaldoAnterior(fieldbyname('portador').asstring,fieldbyname('data').asstring));
                            PrimeiroSaldo:=SaldoInicial;
                            SomaEntrada:=0;
                            SomaSaida:=0;
                  End
                  Else SaldoInicial:=saldofinal;

                  entrada:=0;
                  saida:=0;
                  If (FieldByName('dc').ASSTRING='C')
                  Then Begin
                        Entrada:=Fieldbyname('valor').asfloat;
                        SomaEntrada:=SomaEntrada+Entrada;
                  End
                  Else Begin
                        Saida:=Fieldbyname('valor').asfloat;
                        SomaSaida:=SomaSaida+Saida;
                  End;
                  SaldoFinal:=(SaldoInicial+Entrada)-Saida;


                  if (FieldByName('lancamento').asstring='')
                  Then TituloqueGerou:=Self.LocalizaTituloGERADOR(uppercase(fieldbyname('OBJETOGERADOR').asstring),fieldbyname('valordocampo').asstring)
                  Else Begin
                            ObjlanctoportadorGlobal.ZerarTabela;
                            ObjlanctoportadorGlobal.Lancamento.LocalizaCodigo(FieldByName('lancamento').asstring);
                            ObjlanctoportadorGlobal.Lancamento.TabelaparaObjeto;
                            if(ObjlanctoportadorGlobal.Lancamento.Pendencia.get_codigo<>'')
                            Then TituloqueGerou:=ObjlanctoportadorGlobal.Lancamento.Pendencia.Titulo.Get_CODIGO
                            Else TituloqueGerou:='LT';
                  End;



                  FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,60,' ')+'|'+CompletaPalavra(TituloQueGerou,6,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('Data').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(Entrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(Saida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));
                  ultimoportador:=fieldbyname('portador').asstring;
                  next;
             End;//while

             //trocou o portador, tenho que totalizar
             //todas as entradas e saidas e o saldo final
             FrelTXT.SB.Items.add(completapalavra('?',6,' ')+' '+CompletaPalavra('',5,' ')+' '+CompletaPalavra('TOTALIZA��O',60,' ')+' '+completapalavra(' ',08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PrimeiroSaldo)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))),13,' '));
             FrelTXT.SB.Items.add(CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));

             If (ComPrevisao=True)
             Then Begin
                       Self.ImprimePrevisto(UltimoPortador,PdataInicial,PdataFinal,SaldoFinal);
             End;

     End;//WITH



     //configurando os campos
     With FRELTXT do
     Begin

          LimpaCampos;//LIMPA E DESATIVA

          LBTITULO.Enabled:=True;
          LBTITULO.caption:=Self.NumeroRelatorio+'FLUXO DO CAIXA ANAL�TICO -  DE '+datetostr(PdataInicial)+' A '+datetostr(PdataFinal);

          If (Pportador<>-1)
          Then Begin
                    LbSubtitulo1.enabled:=True;
                    LbSubtitulo1.Caption:='Portador '+Self.Get_Nome;
          End;
          Qr.Page.Orientation:=polandscape;
          Qr.preview;
          //FreltxtRDPRINT.botaoimprimirClick(nil);
     End;

end;

Procedure TObjPortador.GeraFluxoporLancto(Pportador:string;AnaliticoSintetico:string);
var
totallancto, totallanctocredito, totallanctodebito:currency;
ultimoportador,ultimolancto:string;
cont,linha:integer;
PQlocal:tibquery;
PdataInicial,PDataFinal:Tdate;
PtipoLancto,PexcluiTipoLancto:TStringList;
temp:string;

Begin
     Try
        PQlocal:=TIBQuery.Create(nil);
        PQlocal.database:=FDataModulo.IBDatabase;
        PtipoLancto:=TStringList.Create;
        PexcluiTipoLancto:=TStringList.create;
     Except
           messagedlg('Erro Na Tentativa de Cria��o da Query Local!',mterror,[mbok],0);
           exit;
     End;
Try
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo03.Enabled:=True;
          Grupo04.Enabled:=True;
          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          LbGrupo03.caption:='Tipo Lancto';
          LbGrupo04.caption:='Excluir Tipo Lancto';
          edtgrupo01.editmask:='!99/99/9999;1;_';
          edtgrupo02.editmask:='!99/99/9999;1;_';
          edtgrupo03.editmask:='';
          edtgrupo03.OnKeyDown:=Self.edtTipoLanctoKeyDown_PV;
          edtgrupo04.OnKeyDown:=Self.edtTipoLanctoKeyDown_PV;

          ShowModal;

          if (tag=0)
          Then exit;
          
          Try
             PdataInicial:=StrToDate(edtgrupo01.text);
             PDataFinal:=StrToDate(edtgrupo02.text);
          Except
                Messagedlg('N�o � poss�vel gerar este relat�rio sem escolher a Data!',mterror,[mbok],0);
                exit;
          End;


             PtipoLancto.clear;
             temp:='';
             if (edtgrupo03.text<>'')
             Then begin
                       for cont:=1 to length(edtgrupo03.Text) do
                       Begin
                            if (edtgrupo03.Text[cont]=';')
                            Then Begin
                                      try
                                        if (temp<>';')
                                        and (temp<>'')
                                        Then Begin
                                                  strtoint(temp);
                                                  PtipoLancto.add(temp);
                                        End;
                                        temp:='';
                                      Except
                                            Messagedlg('Tipo de lancto inv�lido '+temp,mterror,[mbok],0);
                                            temp:='';
                                      End;
                            End
                            Else temp:=temp+edtgrupo03.Text[cont];
                       End;
                       if (temp<>'')
                       Then begin
                                 try
                                   if (temp<>';')
                                   and (temp<>'')
                                   Then Begin
                                             strtoint(temp);
                                             PtipoLancto.add(temp);
                                   End;
                                   temp:='';
                                 Except
                                       Messagedlg('Tipo de lancto inv�lido '+temp,mterror,[mbok],0);
                                       temp:='';
                                 End;
                       end;
             End;

             PexcluiTipoLancto.clear;
             temp:='';
             if (edtgrupo04.text<>'')
             Then begin
                       for cont:=1 to length(edtgrupo04.Text) do
                       Begin
                            if (edtgrupo04.Text[cont]=';')
                            Then Begin
                                      try
                                        if (temp<>';')
                                        and (temp<>'')
                                        Then Begin
                                                strtoint(temp);
                                                PexcluiTipoLancto.add(temp);
                                        End;
                                        temp:='';
                                      Except
                                            Messagedlg('Tipo de lancto inv�lido '+temp,mterror,[mbok],0);
                                            temp:='';
                                      End;
                            End
                            Else temp:=temp+edtgrupo04.Text[cont];
                       End;
                       if (temp<>'')
                       Then begin
                                 try
                                   if (temp<>';')
                                   and (temp<>'')
                                   Then Begin
                                             strtoint(temp);
                                             PexcluiTipoLancto.add(temp);
                                   End;
                                   temp:='';
                                 Except
                                       Messagedlg('Tipo de lancto inv�lido '+temp,mterror,[mbok],0);
                                       temp:='';
                                 End;
                       end;
             End;
             
     End;

     totallanctocredito:=0;
     totallanctodebito:=0;
     With PQlocal do
     Begin
             close;
             SQL.clear;

             SQL.add(' Select tablanctoportador.portador,Tabportador.nome as NOMEPORTADOR,tablanctoportador.codigo,');
             SQL.add(' tablanctoportador.historico,tablanctoportador.data,tablanctoportador.valor,');
             SQL.add(' tabtipolanctoportador.historico as NomeTipoLancto,tablanctoportador.tipolancto,');
             SQL.add(' tablanctoportador.OBJETOGERADOR,tablanctoportador.CAMPOPRIMARIO,tablanctoportador.VALORDOCAMPO,');
             SQL.Add(' tabtipolanctoportador.debito_credito');
             SQL.add(' from tablanctoportador join Tabtipolanctoportador on');
             SQL.add(' tablanctoportador.tipolancto=tabtipolanctoportador.codigo Join Tabportador');
             SQL.add(' on TabLanctoPortador.Portador=Tabportador.codigo');
             SQL.add(' where imprimerel=''S''');
             SQL.add(' and (data>='+#39+formatdatetime('mm/dd/yyyy',PdataInicial)+#39+' and data<='+#39+formatdatetime('mm/dd/yyyy',PdataFinal)+#39+')');
             If (Pportador<>'')
             Then SQL.add(' and TabLanctoPortador.Portador='+Pportador)
             Else SQL.add(' and TabPortador.apenas_uso_sistema=''N'' ');

             if (PtipoLancto.count>0)
             Then begin
                      sql.add('and (tablanctoportador.tipolancto='+PtipoLancto[0]);
                      for cont:=1 to PtipoLancto.Count-1 do
                      Begin
                           sql.add('or tablanctoportador.tipolancto='+PtipoLancto[cont]);
                      end;
                      sql.add(')');
             End;

             if (PexcluiTipoLancto.count>0)
             Then begin
                      sql.add('and not(tablanctoportador.tipolancto='+PexcluiTipoLancto[0]);
                      for cont:=1 to PexcluiTipoLancto.Count-1 do
                      Begin
                           sql.add('or tablanctoportador.tipolancto='+PexcluiTipoLancto[cont]);
                      end;
                      sql.add(')');
             End;

             SQL.add(' order by tablanctoportador.portador,tablanctoportador.tipolancto,tablanctoportador.data,tablanctoportador.codigo');
             open;
             If (Recordcount=0)
             Then Begin
                       Messagedlg('N�o h� dados para serem impressos!',mterror,[mbok],0);
                       exit;
             End;
             //tenho uma listagem assim
             //CODIGO/HISTORICO/data / VALOR/ Cr�dito/D�bito
             first;

             linha:=3;
             FreltxtRDPRINT.ConfiguraImpressao;
             FreltxtRDPRINT.RDprint.Abrir;
             if (FreltxtRDPRINT.RDprint.Setup=False)
             Then Begin
                       FreltxtRDPRINT.RDprint.Fechar;
                       exit;
             End;
             FreltxtRDPRINT.RDprint.ImpC(linha,45,Self.NumeroRelatorio+'RELAT�RIO DE LAN�AMENTOS EM PORTADOR POR TIPO DE LAN�AMENTO',[negrito]);
             inc(linha,2);


             if (AnaliticoSintetico='A')
             Then Begin
                        FreltxtRDPRINT.RDprint.Impf(linha,10,CompletaPalavra('LANCTO',6,' ')+' '+CompletaPalavra('DATA',10,' ')+' '+
                                                                CompletaPalavra('HIST�RICO',40,' ')+' '+
                                                                CompletaPalavra_a_Esquerda('VALOR',15,' '),[negrito]);
                        inc(linha,1);
                        FreltxtRDPRINT.RDprint.Imp(linha,10,CompletaPalavra('_',74,'_'));
                        inc(linha,2);
             End;



             //PORTADOR CAIXA
                  //LAN�AMENTO: JUROS
                      //se for anal�tico
                      //Codigo     Data               Hist�rico         Valor
                  //TOTAL R$ 200,00


             FreltxtRDPRINT.RDprint.ImpF(linha,1,'PORTADOR: '+CompletaPalavra(fieldbyname('nomeportador').asstring,50,' '),[negrito]);
             inc(linha,2);

             FreltxtRDPRINT.RDprint.ImpF(linha,5,'LAN�AMENTO: '+fieldbyname('tipolancto').asstring+'-'+CompletaPalavra(fieldbyname('nometipolancto').asstring,50,' '),[negrito]);
             inc(linha,1);

             ultimoportador:=fieldbyname('portador').asstring;
             ultimolancto:=fieldbyname('tipolancto').asstring;
             totallancto:=0;

             while not(eof) do
             Begin

                  If (UltimoPortador<>fielDbyname('portador').asstring)
                  Then Begin
                            //tenho que totalizar o tipo de lancto e come�ar de novo
                            FreltxtRDPRINT.RDprint.ImpF(linha,5,'TOTAL: R$ '+formata_valor(floattostr(TotalLancto)),[negrito]);
                            inc(linha,2);
                            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                            TotalLancto:=0;

                            FreltxtRDPRINT.RDprint.ImpF(linha,1,'PORTADOR: '+CompletaPalavra(fieldbyname('nomeportador').asstring,50,' '),[negrito]);
                            inc(linha,2);
                            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                            FreltxtRDPRINT.RDprint.ImpF(linha,5,'LAN�AMENTO: '+fieldbyname('tipolancto').asstring+'-'+CompletaPalavra(uppercase(fieldbyname('nometipolancto').asstring),50,' '),[negrito]);
                            inc(linha,1);
                            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);

                            ultimoportador:=fieldbyname('portador').asstring;
                            ultimolancto:=fieldbyname('tipolancto').asstring;
                  End
                  Else Begin
                            //n�o mudou o portador mas pode ter mudado o tipo de lancto
                            if (ultimolancto<>fieldbyname('tipolancto').asstring)
                            Then Begin
                                        //tenho que totalizar o tipo de lancto e come�ar de novo
                                        FreltxtRDPRINT.RDprint.ImpF(linha,5,'TOTAL: R$ '+formata_valor(floattostr(TotalLancto)),[negrito]);
                                        inc(linha,2);
                                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                        TotalLancto:=0;

                                        FreltxtRDPRINT.RDprint.ImpF(linha,5,'LAN�AMENTO: '+fieldbyname('tipolancto').asstring+'-'+CompletaPalavra(uppercase(fieldbyname('nometipolancto').asstring),50,' '),[negrito]);
                                        inc(linha,1);
                                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                        ultimolancto:=fieldbyname('tipolancto').asstring;
                            End;
                  End;
                  totallancto:=totallancto+fieldbyname('valor').asfloat;
                  if (fieldbyname('debito_credito').AsString = 'C')
                  then totallanctocredito:=totallanctocredito+fieldbyname('valor').asfloat
                  else totallanctodebito:=totallanctodebito+fieldbyname('valor').asfloat;

                  if (AnaliticoSintetico='A')
                  Then Begin
                            FreltxtRDPRINT.RDprint.Imp(linha,10,CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                                                CompletaPalavra(fieldbyname('data').asstring,10,' ')+' '+
                                                                CompletaPalavra(fieldbyname('historico').asstring,40,' ')+' '+
                                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),15,' '));
                            inc(linha,1);
                            FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                  End;
                  Next;
             End;//while
             //tenho que totalizar o tipo de lancto
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
             FreltxtRDPRINT.RDprint.ImpF(linha,5,'TOTAL: R$ '+formata_valor(floattostr(TotalLancto)),[negrito]);

             inc(linha,2);
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
             FreltxtRDPRINT.RDprint.ImpF(linha,5,'SALDO TOTAL: R$ '+formata_valor(floattostr(totallanctocredito-totallanctodebito)),[negrito]);
             FreltxtRDPRINT.RDprint.Fechar;
     End;//WITH

Finally
       FreeAndNil(PQlocal);
       FreeAndNil(PtipoLancto);
       FreeAndNil(PexcluiTipoLancto);
End;

end;



Function TObjPortador.Proc_SaldoAnterior(PPortador:string;PDataLimite:string):string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_RETORNASALDOANTERIOR';
           StrTemp.ParamByName('PORTADORENT').ASSTRING:=PPortador;
           StrTemp.ParamByName('DATALIMITE').ASSTRING:=PDataLimite;
           StrTemp.ExecProc;
           result:=StrTemp.ParamByName('VALORRETORNO').asstring;
        Except
           Messagedlg('Erro durante a Retorno do Saldo Anterior!',mterror,[mbok],0);
           result:='0';
        End;
     Finally
            FreeandNil(StrTemp);
     End;

end;

function TObjPortador.VerificaPermissao: Boolean;
begin
     result:=False;
     iF (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PORTADORES')=fALSE)
     Then exit
     Else result:=True;

end;

procedure TObjPortador.ImprimeExtratoPortador_Separado_Usuario(parametroCodigo: string);
var
   totaldata,totalusuarioentrada,totalusuariosaida,PrimeiroSaldo,saldoinicial,saldofinal,entrada,saida,SomaEntrada,SomaSaida:currency;
   tituloquegerou,ultimadata:string;
   ultimousuario,pusuario:String;
   tmpData1,TmpData2:Tdate;
   linha:integer;
   ZeraSaldo:Boolean;
Begin

     Self.ZerarTabela;
     If (Parametrocodigo='')
     Then Begin
                Messagedlg('Escolha o Portador',mtinformation,[mbok],0);
                exit;
     End;


     If (Self.LocalizaCodigo(parametroCodigo)=False)
     Then Begin
               Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
               exit;
     End;

     Self.TabelaparaObjeto;



     limpaedit(Ffiltroimp);

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo03.Enabled:=True;

          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          LbGrupo03.caption:='Usu�rio';

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          edtgrupo03.EditMask:='';

          edtgrupo03.OnKeyDown:=Self.edtusuariokeydown;

          showmodal;


          If tag=0
          Then exit;

          Pusuario:='';
          if (edtgrupo03.text<>'')
          Then Pusuario:=edtgrupo03.text;

          Try
                TmpData1:=StrToDate(edtgrupo01.text);
                Tmpdata2:=Strtodate(edtgrupo02.text);
                If (TmpData2<TmpData1)
                Then Begin
                          Messagedlg('A Data Final n�o pode ser menor que a Data Inicial!',mtinformation,[mbok],0);
                          exit;
                End;
          Except
                Messagedlg('N�o � poss�vel gerar Relat�rio sem escolher a Data ou o Portador',mterror,[mbok],0);
                exit;
          End;
     End;
     {
     Portador BANCO DO BRASIL

     DATA 01/01/2005

       Usuario Jose
         a
         b
         c
         d
       Total Usuario : XXX,XX

       Usuario Joao
         a
         b
         c
       Total Usuario : XXX,XX

     TOTAL DATA XXX,XX

     DATA 01/02/2005
     .
     .
     .
     TOTAL DATA  XXX,XX
     ----------------------------------------------------------------------
     TOTALIZACAO USUARIO

     JOSE
      ENTRADA X
      SAIDA Y
     JOAO
      ENTRADA X
      SAIDA Y
     }
     With Self.QueryLocal do
     Begin
             close;
             SQL.clear;
             SQL.add(' Select tablanctoportador.portador,tablanctoportador.codigo,tablanctoportador.userc,');
             SQL.add(' tablanctoportador.historico,tablanctoportador.data,tablanctoportador.valor,');
             SQL.add(' tabtipolanctoportador.debito_credito as DC,');
             SQL.add(' tablanctoportador.OBJETOGERADOR,tablanctoportador.CAMPOPRIMARIO,tablanctoportador.VALORDOCAMPO');
             SQL.add(' from tablanctoportador left join Tabtipolanctoportador on');
             SQL.add(' tablanctoportador.tipolancto=tabtipolanctoportador.codigo left Join Tabportador');
             SQL.add(' on TabLanctoPortador.Portador=Tabportador.codigo');
             SQL.add(' where imprimerel=''S''');
             SQL.add(' and (data>='+#39+formatdatetime('mm/dd/yyyy',TmpData1)+#39+' and data<='+#39+formatdatetime('mm/dd/yyyy',TmpData2)+#39+')');
             If (parametroCodigo<>'')
             Then SQL.add(' and TabLanctoPortador.Portador='+parametroCodigo)
             Else SQL.add(' and TabPortador.apenas_uso_sistema=''N'' ');

             if (pusuario<>'')
             Then SQL.add(' and TabLanctoPortador.userc='+#39+pusuario+#39);

             SQL.add(' order by tablanctoportador.portador,tablanctoportador.data,tablanctoportador.userc');

             open;
             If (Recordcount=0)
             Then Begin
                       Messagedlg('N�o h� dados para serem impressos!',mterror,[mbok],0);
                       exit;
             End;


             first;
             entrada:=0;
             saida:=0;

             FreltxtRDPRINT.RDprint.Abrir;
             if (FreltxtRDPRINT.RDprint.setup=False)
             Then Begin
                       FreltxtRDPRINT.RDprint.Fechar;
                       exit;
             End;
             FreltxtRDPRINT.ConfiguraImpressao;
             linha:=3;
             FreltxtRDPRINT.rdprint.ImpC(linha,45,Self.NumeroRelatorio+'EXTRATO POR PORTADOR  -  DE '+datetostr(TmpData1)+' A '+datetostr(TmpData2),[negrito]);
             inc(linha,2);
             FreltxtRDPRINT.rdprint.Impf(linha,01,'PORTADOR '+Self.Get_Nome,[negrito]);
             inc(linha,1);

             if (pusuario<>'')
             Then Begin
                       FreltxtRDPRINT.rdprint.Impf(linha,01,'USU�RIO FILTRADO: '+pusuario,[negrito]);
                       inc(linha,1);
             End;
             inc(linha,1);

             FreltxtRDPRINT.rdprint.Impf(linha,10,CompletaPalavra('CODIGO',5,' ')+'|'+completapalavra('HIST�RICO',59,' ')+'|'+completapalavra('TITULO',06,' ')+'|'+CompletaPalavra_a_Esquerda('VALOR',13,' '),[negrito]);
             inc(linha,1);
             FreltxtRDPRINT.rdprint.Imp(linha,01,CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
             inc(linha,1);
             //SALDO INICIAL
             SaldoInicial:=strtofloat(Self.proc_SaldoAnterior(fieldbyname('portador').asstring,fieldbyname('data').asstring));
             PrimeiroSaldo:=SaldoInicial;


             //*****PARAMETRO PARA ZERAR O SALDO POR PORTADOR*******
             //****A pedido da Qualifica*****
             ZeraSaldo:=False;
             if (ObjParametroGlobal.ValidaParametro('ZERAR SALDO INICIAL NO RELAT�RIO DE EXTRATO')=True)
             Then Begin
                        
                        if (ObjParametroGlobal.Get_Valor='SIM')
                        Then Begin
                                  ZeraSaldo:=True;
                                  saldoinicial:=0;
                                  PrimeiroSaldo:=0;
                        End;
              End;



             Entrada:=0;
             Saida:=0;
             SomaEntrada:=0;
             SomaSaida:=0;
             totaldata:=0;
             totalusuarioentrada:=0;
             totalusuariosaida:=0;


             //imprimindo o saldo inicial primeiro portador
             FreltxtRDPRINT.rdprint.Impf(linha,01,'Saldo Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' '),[negrito]);
             inc(linha,2);

             //****LABELS DE DATA E DE USUARIO*****
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.RDprint.ImpF(linha,1,'DATA '+fieldbyname('data').asstring,[negrito]);
             inc(linha,2);
             ultimadata:=fieldbyname('data').asstring;

             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.RDprint.ImpF(linha,5,'Usu�rio '+fieldbyname('userc').asstring,[negrito]);
             inc(linha,1);
             ultimousuario:=uppercase(trim(fieldbyname('userc').asstring));
             //****LABELS DE DATA E DE USUARIO*****
             while not(eof) do
             Begin

                   if (ultimadata<>fieldbyname('data').asstring)
                   Then Begin
                             //trocou a data
                             //totalizar o ultimo ususario , a data
                             //mostrara a nova data e o novo ususario
                             inc(linha,1);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.ImpF(linha,5,'Saldo do Usu�rio: '+completapalavra(formata_valor(floattostr(saldofinal)),15,' ')+'Entradas: '+completapalavra(formata_valor(floattostr(totalusuarioentrada)),15,' ')+'Sa�das: '+completapalavra(formata_valor(floattostr(totalusuariosaida)),15,' '),[negrito]);
                             inc(linha,1);

                             if (ZeraSaldo=True)
                             Then SaldoFinal:=0;

                             if (ZeraSaldo=False)
                             Then Begin
                                     inc(linha,1);
                                     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                     FreltxtRDPRINT.RDprint.ImpF(linha,1,'SALDO FINAL '+formata_valor(floattostr(saldofinal)),[negrito]);
                                     inc(linha,1);
                             End;

                             //*********************************
                             inc(linha,1);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.ImpF(linha,1,'DATA '+fieldbyname('data').asstring,[negrito]);
                             inc(linha,1);
                             ultimadata:=fieldbyname('data').asstring;
                             totaldata:=0;

                             inc(linha,1);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.ImpF(linha,5,'Usu�rio '+fieldbyname('userc').asstring,[negrito]);
                             inc(linha,1);
                             ultimousuario:=fieldbyname('userc').asstring;
                             totalusuarioentrada:=0;
                             totalusuariosaida:=0;
                   End
                   Else Begin
                             if (ultimousuario<>uppercase(trim(fieldbyname('userc').asstring)))//trocou o usuario
                             Then Begin
                                       inc(linha,1);
                                       FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                       FreltxtRDPRINT.RDprint.ImpF(linha,5,'Saldo do Usu�rio: '+completapalavra(formata_valor(floattostr(saldofinal)),15,' ')+'Entradas: '+completapalavra(formata_valor(floattostr(totalusuarioentrada)),15,' ')+'Sa�das: '+completapalavra(formata_valor(floattostr(totalusuariosaida)),15,' '),[negrito]);
                                       inc(linha,1);

                                       //****A pedido da Qualifica*****
                                       If (ZeraSaldo=True)
                                       Then SaldoFinal:=0;
                                       //*****************************
                                       inc(linha,1);
                                       FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                       FreltxtRDPRINT.RDprint.ImpF(linha,5,'Usu�rio '+fieldbyname('userc').asstring,[negrito]);
                                       inc(linha,1);
                                       ultimousuario:=uppercase(trim(fieldbyname('userc').asstring));
                                       totalusuarioentrada:=0;
                                       totalusuariosaida:=0;
                             End;
                   End;


                  entrada:=0;
                  saida:=0;
                  If (FieldByName('dc').ASSTRING='C')
                  Then Begin
                        Entrada:=Fieldbyname('valor').asfloat;
                        SomaEntrada:=SomaEntrada+Entrada;
                        totalusuarioentrada:=totalusuarioentrada+entrada;
                  End
                  Else Begin
                        Saida:=Fieldbyname('valor').asfloat;
                        SomaSaida:=SomaSaida+Saida;
                        totalusuariosaida:=totalusuariosaida+saida;
                  End;
                  if (ZeraSaldo=False)
                  Then SaldoFinal:=(SaldoInicial+SomaEntrada)-SomaSaida
                  Else SaldoFinal:=totalusuarioentrada-totalusuariosaida;

                  TituloqueGerou:=Self.LocalizaTituloGERADOR(uppercase(fieldbyname('OBJETOGERADOR').asstring),fieldbyname('valordocampo').asstring);
                  FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                  If (FieldByName('dc').ASSTRING='C')//imprimo com +                                                                //{Funcao RetornaNomeCredorDevedor feita por F�bio *********}
                  Then FreltxtRDPRINT.rdprint.Imp(linha,10,CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(Self.RetornaNomeCredorDevedor(fieldbyname('Codigo').asstring)+' - '+fieldbyname('historico').asstring,59,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'+',14,' '))
                  Else FreltxtRDPRINT.rdprint.Imp(linha,10,CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(Self.RetornaNomeCredorDevedor(fieldbyname('Codigo').asstring)+' - '+fieldbyname('historico').asstring,59,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'-',14,' '));
                  inc(linha,1);
                  next;

             End;//WHILE

             //totalizando o usuario e a data
             inc(linha,1);
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.RDprint.ImpF(linha,5,'Saldo do Usu�rio: '+completapalavra(formata_valor(floattostr(saldofinal)),15,' ')+'Entradas: '+completapalavra(formata_valor(floattostr(totalusuarioentrada)),15,' ')+'Sa�das: '+completapalavra(formata_valor(floattostr(totalusuariosaida)),15,' '),[negrito]);
             inc(linha,1);
             inc(linha,1);

             if (ZeraSaldo=False)
             Then Begin
                     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                     FreltxtRDPRINT.RDprint.ImpF(linha,1,'SALDO FINAL '+formata_valor(floattostr(saldofinal)),[negrito]);
                     inc(linha,1);
                      //************TOTALIZACAO GERAL*****************************
                      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                      FreltxtRDPRINT.rdprint.Imp(linha,01,CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
                      inc(linha,1);
                      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                      FreltxtRDPRINT.rdprint.Impf(linha,01,'Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PrimeiroSaldo)),13,' ')+'|Entradas R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|Sa�das R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|Final R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))),12,' '),[negrito]);
                      inc(linha,1);
                      FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                      FreltxtRDPRINT.rdprint.Imp(linha,01,CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
                      inc(linha,1);
             End;
             FreltxtRDPRINT.rdprint.Fechar;
     End;//WITH
end;


procedure TObjPortador.ImprimeExtratoPortador(parametroCodigo: string);
var
PrimeiroSaldo,saldoinicial,saldofinal,entrada,saida,SomaEntrada,SomaSaida:currency;
tituloquegerou,ultimoportador:string;
TmpData1,TmpData2:Tdate;
ListaPortadores:TStringList;
ListaValorPortador:TStringList;
ValorTotal:Real;
i:Integer;
Begin
    ListaPortadores:=TStringList.Create;
    ListaValorPortador:=TStringList.Create;

    try

             Self.ZerarTabela;
             If (Parametrocodigo<>'')
             Then Begin
                       If (Self.LocalizaCodigo(parametroCodigo)=False)
                       Then parametrocodigo:=''
                       Else Self.TabelaparaObjeto;
             End;

             limpaedit(Ffiltroimp);

             With FfiltroImp do
             Begin
                  DesativaGrupos;
                  Grupo01.Enabled:=True;
                  Grupo02.Enabled:=True;
                  //Grupo03.Enabled:=True;
                  LbGrupo01.caption:='Data Inicial';
                  LbGrupo02.caption:='Data Final';
                  LbGrupo03.caption:='Usu�rio';
                  edtgrupo01.EditMask:='!99/99/9999;1;_';
                  edtgrupo02.EditMask:='!99/99/9999;1;_';
                  edtgrupo03.EditMask:='';

                  showmodal;

                  If tag=0
                  Then exit;

                  Try
                        TmpData1:=StrToDate(edtgrupo01.text);
                        Tmpdata2:=Strtodate(edtgrupo02.text);
                        If (TmpData2<TmpData1)
                        Then Begin
                                  Messagedlg('A Data Final n�o pode ser menor que a Data Inicial!',mtinformation,[mbok],0);
                                  exit;
                        End;
                  Except
                        Messagedlg('N�o � poss�vel gerar Relat�rio sem escolher a Data ou o Portador',mterror,[mbok],0);
                        exit;
                  End;
             End;

            With Self.QueryLocal do
            Begin
                   close;
                   SQL.clear;
                   SQL.add(' Select tablanctoportador.portador,tablanctoportador.codigo,tablanctoportador.historico,tablanctoportador.data,tablanctoportador.valor,tabtipolanctoportador.debito_credito as DC,');
                   SQL.add(' tablanctoportador.OBJETOGERADOR,tablanctoportador.CAMPOPRIMARIO,tablanctoportador.VALORDOCAMPO,tablanctoportador.lancamento');
                   SQL.add(' from tablanctoportador left join Tabtipolanctoportador on');
                   SQL.add(' tablanctoportador.tipolancto=tabtipolanctoportador.codigo left Join Tabportador');
                   SQL.add(' on TabLanctoPortador.Portador=Tabportador.codigo');
                   SQL.add(' where imprimerel=''S''');
                   SQL.add(' and (data>='+#39+formatdatetime('mm/dd/yyyy',TmpData1)+#39+' and data<='+#39+formatdatetime('mm/dd/yyyy',TmpData2)+#39+')');

                   If (parametroCodigo<>'')
                   Then SQL.add(' and TabLanctoPortador.Portador='+parametroCodigo)
                   Else SQL.add(' and TabPortador.apenas_uso_sistema=''N'' ');

                   if (FfiltroImp.edtgrupo03.text<>'')
                   Then SQL.add(' and TabLanctoPortador.Userc='+#39+FfiltroImp.edtgrupo03.text+#39);

                   SQL.add(' order by tablanctoportador.portador,tablanctoportador.data,tablanctoportador.codigo');
                   //InputBox('','',sql.Text);
                   open;
                   If (Recordcount=0)
                   Then Begin
                             Messagedlg('N�o h� dados para serem impressos!',mterror,[mbok],0);
                             exit;
                   End;
                   //tenho uma listagem assim
                   //Portador / CODIGO/HISTORICO/data / VALOR/ Cr�dito/D�bito
                   //preciso fazer algo assim
                   first;
                   entrada:=0;
                   saida:=0;
                   UltimoPortador:=fieldbyname('Portador').asstring;
                   FrelTXT.SB.Items.clear;

                   //*******CABE�ALHO***********
                   //o 1� caracter='?' significa negrito nesta linha do relat�rio

                   FrelTXT.SB.Items.add('?'+CompletaPalavra('CODIGO',11,' ')+'|'+completapalavra('HIST�RICO',53,' ')+'|'+completapalavra('TITULO',06,' ')+'|'+completapalavra('DATA',08,' ')+'|'+CompletaPalavra_a_Esquerda('VALOR',13,' '));
                   FrelTXT.SB.Items.add(CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
                   //nome do primeiro portador
                   Self.LocalizaCodigo(fieldbyname('portador').asstring);
                   Self.TabelaparaObjeto;
                   FrelTXT.SB.Items.add('?'+fieldbyname('portador').asstring+'-'+Self.Nome);
                   //************************
                   //imprimo o primeiro registro com o saldo anterior correto dele
                    SaldoInicial:=strtofloat(Self.proc_SaldoAnterior(fieldbyname('portador').asstring,fieldbyname('data').asstring));
                    PrimeiroSaldo:=SaldoInicial;

                    if (ObjParametroGlobal.ValidaParametro('ZERAR SALDO INICIAL NO RELAT�RIO DE EXTRATO')=True)
                    Then Begin
                        
                              if (ObjParametroGlobal.Get_Valor='SIM')
                              Then Begin
                                        saldoinicial:=0;
                                        PrimeiroSaldo:=0;
                              End;
                    End;


                    Entrada:=0;
                    Saida:=0;

                    SomaEntrada:=0;
                    SomaSaida:=0;

                    If (FieldByName('dc').ASSTRING='C')
                    Then Begin
                              Entrada:=Fieldbyname('valor').asfloat;
                              SomaEntrada:=SomaEntrada+Entrada;
                    End
                    Else Begin
                              Saida:=Fieldbyname('valor').asfloat;
                              SomaSaida:=SomaSaida+Saida;
                    End;
                    SaldoFinal:=(SaldoInicial+Entrada)-Saida;
                    //imprimindo o saldo inicial primeiro portador
                    FrelTXT.SB.Items.add('?Saldo Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' '));
                    //imprimindo o primeiro registro

                    if (fieldbyname('lancamento').asstring<>'')
                    Then TituloqueGerou:=Self.LocalizaTitulo_Lancamento(fieldbyname('lancamento').asstring)
                    Else TituloqueGerou:=Self.LocalizaTituloGERADOR(uppercase(fieldbyname('OBJETOGERADOR').asstring),fieldbyname('valordocampo').asstring);
              
                    If (FieldByName('dc').ASSTRING='C')//imprimo com +
                    Then FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codigo').asstring,11,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,53,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('Data').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'+',14,' '))
                    Else FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codigo').asstring,11,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,53,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('Data').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'-',14,' '));
                    ListaValorPortador.Add(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))));
                    ListaPortadores.Add(Self.Nome);
                    ultimoportador:=fieldbyname('portador').asstring;
                    next;
                    while not(eof) do
                    Begin

                        If (UltimoPortador<>fielDbyname('portador').asstring)
                        Then Begin
                                  //trocou o portador, tenho que totalizar
                                  //todas as entradas e saidas e o saldo final
                                  FrelTXT.SB.Items.add('?Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PrimeiroSaldo)),13,' ')+'|Entradas R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|Sa�das R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|Final R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))),12,' '));
                                  FrelTXT.SB.Items.add(CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
                                  ListaValorPortador.Add(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))));
                                  ListaPortadores.Add(Self.Nome);

                                  Self.LocalizaCodigo(fieldbyname('portador').asstring);
                                  Self.TabelaparaObjeto;
                                  FrelTXT.SB.Items.add('?'+fieldbyname('portador').asstring+'-'+Self.Nome);
                                  //acerto o saldo inicial quanto troco de portador, quando nao apenas pego o final e vou trabalhando
                                  SaldoInicial:=strtofloat(Self.proc_SaldoAnterior(fieldbyname('portador').asstring,fieldbyname('data').asstring));
                                  PrimeiroSaldo:=SaldoInicial;
                                  //imprimindo o saldo inicial primeiro portador
                                  FrelTXT.SB.Items.add('?Saldo Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' '));
                                  SomaEntrada:=0;
                                  SomaSaida:=0;


                        End
                        Else SaldoInicial:=saldofinal;

                        entrada:=0;
                        saida:=0;
                        If (FieldByName('dc').ASSTRING='C')
                        Then Begin
                              Entrada:=Fieldbyname('valor').asfloat;
                              SomaEntrada:=SomaEntrada+Entrada;
                        End
                        Else Begin
                              Saida:=Fieldbyname('valor').asfloat;
                              SomaSaida:=SomaSaida+Saida;
                        End;

                        SaldoFinal:=(SaldoInicial+Entrada)-Saida;

                        if (fieldbyname('lancamento').asstring<>'')
                        Then TituloqueGerou:=Self.LocalizaTitulo_Lancamento(fieldbyname('lancamento').asstring)
                        Else TituloqueGerou:=Self.LocalizaTituloGERADOR(uppercase(fieldbyname('OBJETOGERADOR').asstring),fieldbyname('valordocampo').asstring);
                  
                        If (FieldByName('dc').ASSTRING='C')//imprimo com +
                        Then FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codigo').asstring,11,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,53,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('Data').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'+',14 ,' '))
                        Else FrelTXT.SB.Items.add(CompletaPalavra(fieldbyname('codigo').asstring,11,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,53,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('Data').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'-',14,' '));

                        //FrelTXT.SB.Items.add(completapalavra(fieldbyname('portador').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,60,' ')+'|'+completapalavra(formatdatetime('dd/mm/yy',strtodate(fieldbyname('Data').asstring)),08,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(Entrada)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(Saida)),13,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoFinal)),13,' '));

                        ultimoportador:=fieldbyname('portador').asstring;
                        next;
                   End;//while

                   //trocou o portador, tenho que totalizar
                   //todas as entradas e saidas e o saldo final
                   FrelTXT.SB.Items.add('?Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PrimeiroSaldo)),13,' ')+'|Entradas R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|Sa�das R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|Final R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))),12,' '));
                   FrelTXT.SB.Items.add(CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));

            End;//WITH
            ListaValorPortador.Add(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))));
            ListaPortadores.Add(Self.Nome);
            ValorTotal:=0;
            for i:=0  to ListaValorPortador.Count-1 do
            begin
                  //ShowMessage();
                  ValorTotal:=ValorTotal+StrToFloat(tira_ponto(ListaValorPortador[i]));
                  if(i<>ListaValorPortador.Count-1) then
                  begin
                      if(ListaPortadores[i]<>ListaPortadores[i+1])
                      then FrelTXT.SB.Items.add(CompletaPalavra(ListaPortadores[i],50,' ')+' - '+CompletaPalavra_a_Esquerda(ListaValorPortador[i],15,' '))
                      else ValorTotal:=ValorTotal-StrToFloat(tira_ponto(ListaValorPortador[i])); //se for igual, preciso desconsider o que tiver vindo antes pra esse portador
                  end
                  else FrelTXT.SB.Items.add(CompletaPalavra(ListaPortadores[i],50,' ')+' - '+CompletaPalavra_a_Esquerda(ListaValorPortador[i],15,' '));

            end;
            FrelTXT.SB.Items.add(CompletaPalavra('Valor Total',50,' ')+' - '+CompletaPalavra_a_Esquerda(formata_valor(ValorTotal),15,' '));

           //configurando os campos
           With FRELTXT do
           Begin

                LimpaCampos;//LIMPA E DESATIVA

                LBTITULO.Enabled:=True;
                LBTITULO.caption:=Self.NumeroRelatorio+'EXTRATO POR PORTADOR  -  DE '+datetostr(TmpData1)+' A '+datetostr(TmpData2);

                If (parametroCodigo<>'')
                Then Begin
                          LbSubtitulo1.enabled:=True;
                          LbSubtitulo1.Caption:='Portador '+Self.Get_Nome;
                End;

                FreltxtRDPRINT.preview(TRUE);
           End;
    
    finally
           ListaPortadores.Free;
           ListaValorPortador.Free;
    end;

end;


function TObjPortador.LocalizaTituloGERADOR(TMPOBJETOGERADOR,
TMPvalordocampo: string): string;
begin
     Result:='';
Try
     With QueryLocal2 do
     Begin
          Close;
          SQL.clear;
          //CONTA RECEBIDA COM OU SEM TIUTLO
          if (TMPOBJETOGERADOR='OBJVALORES')
          Then Begin
                    sql.add('select tabvalores.codigo as VALORES,');
                    sql.add('tablancamento.codigo as LANCAMENTO,');
                    sql.add('tabpendencia.codigo as PENDENCIA,');
                    sql.add('tabpendencia.titulo as TITULO from tabvalores');
                    sql.add('join tablancamento on tabvalores.lancamento=tablancamento.codigo');
                    sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                    SQL.add('where TabValores.codigo='+tmpvalordocampo);
                    sql.add('order by tabvalores.codigo');
                    open;
                    Result:=fieldbyname('titulo').asstring;
                    close;
        End
        Else
        if (TMPOBJETOGERADOR='OBJTRANSFERENCIAPORTADOR')
        Then Begin
                  sql.add('Select TabTransferenciaPOrtador.codigo as TRANSFERENCIAPORTADOR,');
                  sql.add('TabLAncamento.Codigo as LANCAMENTO,');
                  sql.add('TABPENDENCIA.CODIGO as PENDENCIA,');
                  sql.add('TABPENDENCIA.TITULO as TITULO');
                  sql.add('from TabTransferenciaPOrtador');
                  sql.add('join TabLancamento on TabTransferenciaPOrtador.CodigoLancamento=TabLancamento.codigo');
                  sql.add('join TabPendencia on TabLancamento.pendencia=tabpendencia.codigo');
                  sql.add('where TabTransferenciaportador.codigo='+tmpvalordocampo);
                  open;
                  Result:=fieldbyname('titulo').asstring;
        End
        else
        if (TMPOBJETOGERADOR='OBJCHEQUESPORTADOR')
        Then Begin
                  //isso � s� para pagamento com cheque do portador
                  SQL.ADD('Select TabTalaodecheques.codigo as CODIGOTALAO,tabchequesportador.codigo as CODIGOCHEQUEPORTADOR,');
                  SQL.ADD('tabvalorestransferenciaportador.codigo as VALTRFPORT,');
                  SQL.ADD('tabtransferenciaportador.codigo as TRFPORT,');
                  SQL.ADD('TABLANCAMento.codigo as Lancamento,');
                  SQL.ADD('Tabpendencia.codigo as PENDENCIA,');
                  SQL.ADD('Tabpendencia.titulo as TITULO');
                  SQL.ADD('from TabTalaodecheques');
                  SQL.ADD('join tabchequesportador');
                  SQL.ADD('on tabtalaodecheques.codigochequeportador=tabchequesportador.codigo');
                  SQL.ADD('join TabVAloresTRansferenciaPortador on tabchequesportador.codigo=tabvalorestransferenciaportador.valores');
                  SQL.ADD('join tabtransferenciaportador on TabVAloresTRansferenciaPortador.transferenciaportador=tabtransferenciaportador.codigo');
                  SQL.ADD('join TabLancamento on TabTransferenciaPOrtador.CodigoLancamento=TabLancamento.codigo');
                  SQL.ADD('join TabPendencia on TabLancamento.pendencia=tabpendencia.codigo');
                  SQL.ADD('where TabchequesPortador.codigo='+tmpvalordocampo);
                  open;
                  Result:=fieldbyname('titulo').asstring;
        End;
     End;
Except
      result:='';
End;

end;

function TObjPortador.LocalizaTitulo_Lancamento(Plancamento:string): string;
var
ppendencia:string;
begin
     Result:='';
Try
     With QueryLocal2 do
     Begin
          Close;
          SQL.clear;
          sql.add('Select tablancamento.codigo as lancamento,tabpendencia.titulo');
          sql.add('from tablancamento left join Tabpendencia');
          sql.add('on tablancamento.pendencia=tabpendencia.codigo');
          sql.add('where tablancamento.codigo='+Plancamento);
          open;
          if (fieldbyname('titulo').asstring='')
          Then Result:='LT '+plancamento
          Else Result:=fieldbyname('titulo').asstring;
     End;
Except
      result:='';
End;

end;



procedure TObjPortador.edtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,self.get_titulopesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjPortador.ImprimeRelacaodeChequesTERCEIRO(
  parametrocodigo: string);
var
   ChequesPortadorTemp:TObjChequesPortador;
   PdataInicial,PdataFinal:string;
begin
     //escolhendo o portador
     limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
             DesativaGrupos;
             LbGrupo01.caption:='Portador';
             edtgrupo01.EditMask:='';
             edtgrupo01.OnKeyDown:=Self.edtportadorKeyDown;
             if (parametrocodigo='')
             Then Grupo01.Enabled:=True
             Else edtgrupo01.text:=parametrocodigo;

             Grupo02.enabled:=True;
             LbGrupo02.caption:='Vencimento Inicial';
             edtgrupo02.EditMask:='!99/99/9999;1;_';

             Grupo03.enabled:=True;
             LbGrupo03.caption:='Vencimento Final';
             edtgrupo03.EditMask:='!99/99/9999;1;_';

             showmodal;

             If tag=0
             Then exit;

             pdatainicial:='';
             pdatafinal:='';

             Try
                if (comebarra(trim(edtgrupo02.text))<>'')
                Then Begin
                          strtodate(edtgrupo02.text);
                          pdatainicial:=edtgrupo02.text;
                End;
             Except
                   Messagedlg('Data Inicial Inv�lida!',mterror,[mbok],0);
                   exit;
             end;

             Try
                strtodate(edtgrupo03.text);
                pdatafinal:=edtgrupo03.text;
             Except
                   Messagedlg('Data Final Inv�lida!',mterror,[mbok],0);
                   exit;
             end;


             Try
                strtoint(edtgrupo01.text);
                if (Self.LocalizaCodigo(edtgrupo01.text)=False)
                Then Begin
                          Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
                          exit;
                End;
                parametrocodigo:=Edtgrupo01.text;
             Except
                   Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
                   exit;
             End;

             Try
                ChequesPortadorTemp:=TObjChequesPortador.create;
             Except
                Messagedlg('Erro Durante a Cria��o do Objeto de Cheques por Portador!',mterror,[mbok],0);
                exit;
             End;

             Try
                ChequesPortadorTemp.ImprimeListaCheques(ParametroCodigo,pdatainicial,pdatafinal, Self.NumeroRelatorio);
             Finally
                ChequesPortadorTemp.free;
             End;
     
     End;


End;

procedure TObjPortador.edtTipoLanctoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   ObjTipoLancto:Tobjtipolanctoportador;
begin            


     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            ObjTipoLancto:=TObjTipoLanctoPortador.Create;

            If (FpesquisaLocal.PreparaPesquisa(ObjTipoLancto.Get_Pesquisa,ObjTipoLancto.get_titulopesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           ObjTipoLancto.free;
     End;
end;

function TObjPortador.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_PORTADOR';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
           result:='0';
           exit;

        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


procedure TObjPortador.edtUsuarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjUsuarioGlobal.Get_Pesquisa,'Usu�rios do Sistema',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjPortador.edtTipoLanctoKeyDown_PV(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   ObjTipoLancto:Tobjtipolanctoportador;
begin            


     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            ObjTipoLancto:=TObjTipoLanctoPortador.Create;

            If (FpesquisaLocal.PreparaPesquisa(ObjTipoLancto.Get_Pesquisa,ObjTipoLancto.get_titulopesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           ObjTipoLancto.free;
     End;
end;

function TObjPortador.Get_Permite_Saldo_Negativo: string;
begin
     Result:=Self.Permite_Saldo_Negativo;
end;

procedure TObjPortador.Submit_Permite_Saldo_Negativo(parametro: string);
begin
     Self.Permite_Saldo_Negativo:=Parametro;
end;


function TObjPortador.Get_SaldopelosLancamentos(Pportador: string): string;
var
PsomaCredito,PsomaDebito:Currency;
begin
     Result:='0';
     PsomaCredito:=0;
     PsomaDebito:=0;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('SELECT sum(tablanctoportador.valor) as SOMA');
          SelectSQL.add('from tablanctoportador left join Tabtipolanctoportador on');
          SelectSQL.add('tablanctoportador.tipolancto=tabtipolanctoportador.codigo');
          SelectSQL.add('where imprimerel=''S'' and tablanctoportador.portador='+pportador);
          SelectSQL.add('AND TABTIPOLANCTOPORTADOR.DEBITO_CREDITO=''C''');

          Try
            open;
            PsomaCredito:=Fieldbyname('soma').ASCURRENCY;
          except
          End;

          close;
          SelectSQL.clear;
          SelectSQL.add('SELECT sum(tablanctoportador.valor) as SOMA');
          SelectSQL.add('from tablanctoportador left join Tabtipolanctoportador on');
          SelectSQL.add('tablanctoportador.tipolancto=tabtipolanctoportador.codigo');
          SelectSQL.add('where imprimerel=''S'' and tablanctoportador.portador='+pportador);
          SelectSQL.add('AND TABTIPOLANCTOPORTADOR.DEBITO_CREDITO=''D''');
          try
            open;
            PsomaDebito:=Fieldbyname('soma').ascurrency;
          Except
          End;

          close;

          Result:=floattostr(PsomaCredito-PsomaDebito);

     End;

end;

procedure TObjPortador.edtportadorKeyDown_PV(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,self.get_titulopesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

Function TObjPortador.RetornaNomeCredorDevedor(PLancamentoPortador:string):string;
Var QueryLocal:TIBquery;
    PTabela, PCampoNome:string;
    PCodigoCredorDevedor:string;
Begin
     Result:='';
try

     try
         QueryLocal:=TIBquery.Create(nil);
         QueryLocal.Database:=FDataModulo.IBDatabase;
     except
         MensagemErro('Erro ao tentar criar a Query Local');
         exit;
     end;

     if (PLancamentoPortador = '')
     then exit;

     With  QueryLocal do
     Begin
         Close;
         SQL.Clear;
         SQL.Add('Select TabCredorDevedor.Tabela, TabCredorDevedor.CampoNome,  Tabtitulo.CodigoCredorDevedor from TabLanctoPortador');
         SQL.Add('join TabLancamento on TabLancamento.Codigo = TabLanctoPortador.Lancamento');
         SQL.Add('join TabPendencia on TabPendencia.Codigo = TabLancamento.Pendencia');
         SQL.Add('Join TabTitulo on TabTitulo.Codigo = TabPendencia.Titulo');
         SQL.Add('Join TabCredorDevedor on TabCredorDevedor.Codigo = TabTitulo.CredorDevedor');
         SQL.Add('Where TabLanctoPortador.Codigo = '+PLancamentoPortador);
         Open;

         if (RecordCount = 0)
         then exit;

         PTabela:=fieldbyname('Tabela').AsString;
         PCodigoCredorDevedor:=fieldbyname('CodigoCredorDevedor').asString;
         PCampoNome:=fieldbyname('CampoNome').asString;

         Close;
         SQL.Clear;
         SQL.Add('Select  '+PCampoNome+' from '+PTabela+' Where Codigo = '+PCodigoCredorDevedor);
         Open;

         Result:=fieldbyname(PCampoNome).AsString;
     end;
finally
    FreeAndnil(QueryLocal);    

end;

end;

procedure TObjPortador.ImprimeExtratoPortador_NAO_Separado_Usuario(parametroCodigo: string);
var
   totaldata,PrimeiroSaldo,saldoinicial,saldofinal,entrada,saida,SomaEntrada,SomaSaida:currency;
   tituloquegerou,ultimadata:string;
   tmpData1,TmpData2:Tdate;
   linha:integer;
   ZeraSaldo:Boolean;
Begin

     Self.ZerarTabela;
     If (Parametrocodigo='')
     Then Begin
                Messagedlg('Escolha o Portador',mtinformation,[mbok],0);
                exit;
     End;


     If (Self.LocalizaCodigo(parametroCodigo)=False)
     Then Begin
               Messagedlg('Portador Inv�lido!',mterror,[mbok],0);
               exit;
     End;

     Self.TabelaparaObjeto;



     limpaedit(Ffiltroimp);

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;

          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';

          showmodal;


          If tag=0
          Then exit;

          Try
                TmpData1:=StrToDate(edtgrupo01.text);
                Tmpdata2:=Strtodate(edtgrupo02.text);
                If (TmpData2<TmpData1)
                Then Begin
                          Messagedlg('A Data Final n�o pode ser menor que a Data Inicial!',mtinformation,[mbok],0);
                          exit;
                End;
          Except
                Messagedlg('N�o � poss�vel gerar Relat�rio sem escolher a Data ou o Portador',mterror,[mbok],0);
                exit;
          End;
     End;
     With Self.QueryLocal do
     Begin
             close;
             SQL.clear;
             SQL.add(' Select tablanctoportador.portador,tablanctoportador.codigo,tablanctoportador.userc,');
             SQL.add(' tablanctoportador.historico,tablanctoportador.data,tablanctoportador.valor,');
             SQL.add(' tabtipolanctoportador.debito_credito as DC,');
             SQL.add(' tablanctoportador.OBJETOGERADOR,tablanctoportador.CAMPOPRIMARIO,tablanctoportador.VALORDOCAMPO');
             SQL.add(' from tablanctoportador left join Tabtipolanctoportador on');
             SQL.add(' tablanctoportador.tipolancto=tabtipolanctoportador.codigo left Join Tabportador');
             SQL.add(' on TabLanctoPortador.Portador=Tabportador.codigo');
             SQL.add(' where imprimerel=''S''');
             SQL.add(' and (data>='+#39+formatdatetime('mm/dd/yyyy',TmpData1)+#39+' and data<='+#39+formatdatetime('mm/dd/yyyy',TmpData2)+#39+')');
             If (parametroCodigo<>'')
             Then SQL.add(' and TabLanctoPortador.Portador='+parametroCodigo)
             Else SQL.add(' and TabPortador.apenas_uso_sistema=''N'' ');


             SQL.add(' order by tablanctoportador.portador,tablanctoportador.data,tablanctoportador.userc');

             open;
             If (Recordcount=0)
             Then Begin
                       Messagedlg('N�o h� dados para serem impressos!',mterror,[mbok],0);
                       exit;
             End;


             first;
             entrada:=0;
             saida:=0;

             FreltxtRDPRINT.RDprint.Abrir;
             if (FreltxtRDPRINT.RDprint.setup=False)
             Then Begin
                       FreltxtRDPRINT.RDprint.Fechar;
                       exit;
             End;
             FreltxtRDPRINT.ConfiguraImpressao;
             linha:=3;
             FreltxtRDPRINT.rdprint.ImpC(linha,45,Self.NumeroRelatorio+'EXTRATO POR PORTADOR  -  DE '+datetostr(TmpData1)+' A '+datetostr(TmpData2),[negrito]);
             inc(linha,2);
             FreltxtRDPRINT.rdprint.Impf(linha,01,'PORTADOR '+Self.Get_Nome,[negrito]);
             inc(linha,1);

             FreltxtRDPRINT.rdprint.ImpC(linha,1,CompletaPalavra('CODIGO',5,' ')+'|'+CompletaPalavra('CREDOR_DEVEDOR',17,' ')+'|'+CompletaPalavra('HISTORICO',50,' ')+'|'+completapalavra('TITULO',6,' ')+'|'+CompletaPalavra_a_Esquerda('VALOR',14,' '), [Negrito]);


             inc(linha,1);
             FreltxtRDPRINT.rdprint.Imp(linha,1,CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
             inc(linha,1);
             //SALDO INICIAL
             SaldoInicial:=strtofloat(Self.proc_SaldoAnterior(fieldbyname('portador').asstring,fieldbyname('data').asstring));
             PrimeiroSaldo:=SaldoInicial;

             Entrada:=0;
             Saida:=0;
             SomaEntrada:=0;
             SomaSaida:=0;
             totaldata:=0;


             //imprimindo o saldo inicial primeiro portador
             FreltxtRDPRINT.rdprint.Impf(linha,01,'Saldo Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SaldoInicial)),13,' '),[negrito]);
             inc(linha,2);

             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.RDprint.ImpF(linha,1,'DATA '+fieldbyname('data').asstring,[negrito]);
             inc(linha,2);
             ultimadata:=fieldbyname('data').asstring;

             //****LABELS DE DATA E DE USUARIO*****
             while not(eof) do
             Begin

                   if (ultimadata<>fieldbyname('data').asstring)
                   Then Begin
                             inc(linha,1);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.ImpF(linha,1,'SALDO FINAL '+formata_valor(floattostr(saldofinal)),[negrito]);
                             inc(linha,1);

                             //*********************************
                             inc(linha,1);
                             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                             FreltxtRDPRINT.RDprint.ImpF(linha,1,'DATA '+fieldbyname('data').asstring,[negrito]);
                             inc(linha,1);
                             ultimadata:=fieldbyname('data').asstring;
                             totaldata:=0;
                   End;

                  entrada:=0;
                  saida:=0;
                  If (FieldByName('dc').ASSTRING='C')
                  Then Begin
                        Entrada:=Fieldbyname('valor').asfloat;
                        SomaEntrada:=SomaEntrada+Entrada;
                  End
                  Else Begin
                        Saida:=Fieldbyname('valor').asfloat;
                        SomaSaida:=SomaSaida+Saida;
                  End;

                  SaldoFinal:=(SaldoInicial+SomaEntrada)-SomaSaida;


                  TituloqueGerou:=Self.LocalizaTituloGERADOR(uppercase(fieldbyname('OBJETOGERADOR').asstring),fieldbyname('valordocampo').asstring);
                  FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                  If (FieldByName('dc').ASSTRING='C')//imprimo com +                                                                //{Funcao RetornaNomeCredorDevedor feita por F�bio *********}
                  Then FreltxtRDPRINT.rdprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(Self.RetornaNomeCredorDevedor(fieldbyname('Codigo').asstring), 17,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,50,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'+',14,' '))
                  Else FreltxtRDPRINT.rdprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigo').asstring,5,' ')+'|'+CompletaPalavra(Self.RetornaNomeCredorDevedor(fieldbyname('Codigo').asstring), 17,' ')+'|'+CompletaPalavra(fieldbyname('historico').asstring,50,' ')+'|'+completapalavra(tituloquegerou,6,' ')+'|'+CompletaPalavra_a_Esquerda(formata_valor(Fieldbyname('valor').asstring)+'-',14,' '));
                  inc(linha,1);
                  next;

             End;//WHILE

             inc(linha,1);
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.RDprint.ImpF(linha,1,'SALDO FINAL '+formata_valor(floattostr(saldofinal)),[negrito]);
             inc(linha,1);
             //************TOTALIZACAO GERAL*****************************
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.rdprint.Imp(linha,01,CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
             inc(linha,1);
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.rdprint.Impf(linha,01,'Inicial R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(PrimeiroSaldo)),13,' ')+'|Entradas R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaEntrada)),13,' ')+'|Sa�das R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr(SomaSaida)),13,' ')+'|Final R$ '+CompletaPalavra_a_Esquerda(formata_valor(floattostr((PrimeiroSaldo+(SomaEntrada-somasaida)))),12,' '),[negrito]);
             inc(linha,1);
             FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
             FreltxtRDPRINT.rdprint.Imp(linha,01,CompletaPalavra('-',5,'-')+'-'+CompletaPalavra('-',6,'-')+'-'+completapalavra('-',60,'-')+'-'+completapalavra('-',08,'-')+'-'+CompletaPalavra_a_Esquerda('-',13,'-'));
             inc(linha,1);

             FreltxtRDPRINT.rdprint.Fechar;
     End;//WITH
end;

function TObjPortador.Get_Classificacao: string;
begin
    Result:=Self.Classificacao;
end;

procedure TObjPortador.Submit_Classificacao(parametro: string);
begin
    Self.Classificacao:=parametro;
end;

procedure TObjPortador.ImprimeChequesEmitidos(PPortador:string);
Var  PDataInicial, PDataFinal, pBaixado:string;
     QueryLocal:TIBquery;
     Linha:Integer;
     Objtitulo:TObjTitulo;
begin

Try
     try
         QueryLocal:=TIBquery.Create(nil);
         QueryLocal.Database:=FDataModulo.IBDatabase;
         Objtitulo:=TObjTitulo.Create;
     except
         MensagemErro('Erro ao tentar criar a Querylocal');
         exit;
     end;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          edtgrupo01.EditMask:=MascaraData;
          LbGrupo01.Caption:='Data Inicial';

          Grupo02.Enabled:=true;
          edtgrupo02.EditMask:=MascaraData;
          LbGrupo02.Caption:='Data Final';

          Grupo06.Enabled := True;
          LbGrupo06.Caption := 'Baixado';
          ComboGrupo06.Items.Clear;
          ComboGrupo06.Items.Add('TODOS');
          ComboGrupo06.Items.Add('SIM');
          ComboGrupo06.Items.Add('N�O');


          ShowModal;
          if (Tag = 0)
          then exit;

          PDataInicial:=edtgrupo01.Text;
          PDataFinal:=edtgrupo02.Text;
          pBaixado := Copy(ComboGrupo06.Text,1,1);//pega somente S ou N
    end;

    try
        if (PDataInicial <> '  /  /    ')
        then StrtoDate(PDataInicial);
    except
        MensagemErro('Data Incial inv�lida');
    end;

    try
        if (PDataFinal <> '  /  /    ')
        then StrToDate(PDataFinal);
    except
        MensagemErro('Data Incial inv�lida');
    end;

    try
        if (pBaixado <> 'S') and (pBaixado <> 'N') and (pBaixado <> 'T') then
          pBaixado := 'T';
    except
    end;

    With QueryLocal do
    Begin
           Close;
           SQL.Clear;
           SQL.Add('Select Tablancamento.Codigo as Lancamento, Tablancamento.Historico,');
           SQL.Add('TabTalaoDeCheques.numero, TabTalaodeCheques.Valor, TabTalaoDeCheques.Vencimento,');
           SQL.Add('TabTitulo.CredorDevedor, TabTitulo.CodigoCredorDevedor');
           SQL.Add(' from TabTalaoDeCheques');
           SQL.Add('Join TabLancamento on TabLancamento.codigo = TabTalaoDeCheques.Lancamento');
           SQL.Add('Join TabPendencia on TabPendencia.Codigo = TabLancamento.Pendencia');
           SQL.Add('join Tabtitulo on TabTitulo.Codigo  = TabPendencia.Titulo');
           SQL.Add('Where TabTalaoDeCheques.Codigo > -100');

           if (PPortador <> '')
           then SQL.Add('and TabTalaoDeCheques.POrtador = '+PPortador);

           if (PDataInicial <> '  /  /    ')
           then SQL.Add('and TabTalaoDeCheques.Vencimento >= '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PdataInicial))+#39);

           if (PDataFinal <> '  /  /    ')
           then SQL.Add('and   TabTalaoDeCheques.Vencimento <= '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PdataFinal))+#39);

           if pBaixado <> 'T' then
             sql.Add('and TabTalaoDeCheques.descontado='+QuotedStr(pBaixado));
             
           Open;

           if (RecordCount = 0)
           then Begin
                   MensagemErro('Nenhum dado encontrado na pesquisa');
                   exit;
           end;

           linha:=3;
           FreltxtRDPRINT.ConfiguraImpressao;
           FreltxtRDPRINT.RDprint.Abrir;
           FreltxtRDPRINT.RDprint.ImpC(Linha,30,Self.NumeroRelatorio+'RELAT�RIO DE CHEQUES EMITIDOS (CREDOR / DEVEDOR)',[negrito]);
           inc(linha,2);

           FreltxtRDPRINT.RDprint.ImpC(Linha,1,CompletaPalavra('CREDOR/DEVEDOR',30,' ')+' '+
                                               CompletaPalavra('HISTORICO',30,' ')+' '+
                                               CompletaPalavra_a_Esquerda('CHEQUE',10,' ')+' '+
                                               CompletaPalavra_a_Esquerda('VALOR',10,' ')+' '+
                                               CompletaPalavra_a_Esquerda('VENCIMENTO',10,' '),[Negrito]);
           inc(linha,1);

           While not (eof) do
           Begin
                FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha );
                FreltxtRDPRINT.RDprint.Imp(Linha,1,CompletaPalavra(Objtitulo.Get_NomeCredorDevedor(fieldbyname('CredorDevedor').AsString, fieldbyname('CodigoCredorDevedor').AsString),30,' ')+' '+
                                    CompletaPalavra(fieldbyname('Historico').asString,30,' ')+' '+
                                    CompletaPalavra_a_Esquerda(fieldbyname('Numero').asString,10,' ')+' '+
                                    CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Valor').asString),10,' ')+' '+
                                    CompletaPalavra_a_Esquerda(fieldbyname('Vencimento').asString,10,' '));
               inc(linha,1);
               Next;

           end;
    end;
    FreltxtRDPRINT.RDprint.Fechar;


finally
    FreeAndnil(QueryLocal);
    Objtitulo.Free;
end;

end;

procedure TObjPortador.ImprimeExtratoPortador_FechamentoCaixa_3();
var Linha:integer;
    TmpData1, TmpData2:TDate;
    Usuario, UsuarioAnterior, PortadorAnterior :String;
    EntradaPortador,SaidaPortador, SaldoPortador :Currency;
Begin
      {Portador X

      Transferencias
      	Entrada   X
      	Saida 	  Y
      Pagamentos de Titulos
      	Saiu 	X
      Receb. de Titulos
      	Entrou Y

      Total de Entrdadas  Y
      Total de Saidas     Z
      ----------------------------

      Portador Y

      Transferencias
      	Entrada   X
      	Saida 	  Y
      Pagamentos de Titulos
      	Saiu 	X
      Receb. de Titulos
      	Entrou Y
      
      Total de Entrdadas  Y
      Total de Saidas     Z
      ----------------------------
      
      Total por COnta gerencial
      
      Rec.
      CONTA A
      CONTA B
      CONTA C
      
      Pag.
      CONTA D
      CONTA E
      
      ----------------------------------
      }


     limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo03.Enabled:=True;

          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          LbGrupo03.caption:='Usu�rio';

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          edtgrupo02.EditMask:='!99/99/9999;1;_';
          edtgrupo03.EditMask:='';  
          edtgrupo03.OnKeyDown:=Self.edtusuariokeydown;
          edtgrupo03.CharCase:=ecUpperCase;

          showmodal;
          edtgrupo03.CharCase:=ecNormal;
          

          If tag=0
          Then exit;

          UsuarioAnterior:='';
          PortadorAnterior:='';
          Usuario:='';
          if (edtgrupo03.text<>'')
          Then Usuario:=edtgrupo03.text;

          Try
                TmpData1:=StrToDate(edtgrupo01.text);
                Tmpdata2:=Strtodate(edtgrupo02.text);
                If (TmpData2<TmpData1)
                Then Begin
                          Messagedlg('A Data Final n�o pode ser menor que a Data Inicial!',mtinformation,[mbok],0);
                          exit;
                End;
          Except
                Messagedlg('N�o � poss�vel gerar Relat�rio sem escolher as Datas ',mterror,[mbok],0);
                exit;
          End;
     End;

     EntradaPortador:=0;
     SaidaPortador:=0;
     SaldoPortador:=0;

     With Self.QueryLocal do
     Begin
         Close;
         Sql.Clear;
         SQL.Add('Select Distinct(TabLanctoPortador.UserC),TabPortador.Codigo as Portador, TabPortador.Nome from  Tablanctoportador');
         SQL.Add('join TabPortador on TabPortador.Codigo = TabLanctoPortador.Portador');
         SQL.Add('Where TabLanctoPortador.Data >= '+#39+FormatDateTime('mm/dd/yyyy', TmpData1)+#39);
         SQL.Add('and   TabLanctoPortador.Data <= '+#39+FormatDateTime('mm/dd/yyyy', TmpData2)+#39);
         SQL.Add('and   TabLanctoPortador.ImprimeRel = ''S''  ');
         SQL.add('and   TabPortador.apenas_uso_sistema = ''N'' ');

         if (Usuario <> '')
         then SQL.Add('and TabLanctoPortador.UserC = '+#39+Usuario+#39);

         SQL.Add('Order By TabLanctoPortador.UserC, TabPortador.Codigo ');
         Open;
         Last;
         InicializaBarradeProgressoRelatorio(RecordCount, 'Gerando Relatorio. Aguarde...');
         First;

         if (RecordCount = 0)
         then Begin
                  MensagemErro('Nenhum dado encontrado na pesquisa');
                  exit;
         end;

         linha:=3;
         if (AbrirImpressao=false)
         then exit;

         ImprimirNegrito(linha,20,'FECHAMENTO DE CAIXA TIPO III - DE '+datetostr(TmpData1)+' A '+datetostr(TmpData2));
         inc(linha,2);

         // Para cada Usuario e Portador eu procuro seus lancamenros

         ImprimirSimples(linha,1,LinhaVertical);
         inc(linha,1);
         ImprimirNegrito(Linha,1,'USU�RIO: '+fieldbyname('UserC').AsString);
         inc(linha,1);
         UsuarioAnterior := fieldbyname('UserC').AsString;
         PortadorAnterior:= fieldbyname('Portador').AsString;
         While not (eof) do
         Begin
                IncrementaBarraProgressoRelatorio;

                if (UsuarioAnterior <> fieldbyname('UserC').AsString)
                then Begin
                        Self.ImprimeTransferenciaFechamentoCaixa3(Linha, UsuarioAnterior, TmpData1, TmpData2);
                        Self.ImprimeContasGerenciaisFechamentoCaixa3(Linha, UsuarioAnterior, PortadorAnterior, TmpData1, TmpData2);


                        // Imprimindo Proximo Usuario
                        inc(linha,1);
                        ImprimirSimples(linha,1,LinhaVertical);
                        inc(linha,2);
                        ImprimirNegrito(Linha,1,'USU�RIO: '+fieldbyname('UserC').AsString);
                        inc(linha,1);
                        UsuarioAnterior := fieldbyname('UserC').AsString;
                        PortadorAnterior:= fieldbyname('Portador').AsString;
                end;


                Self.QueryLocal2.Close;
                Self.QueryLocal2.SQL.Clear;
                Self.QueryLocal2.SQL.Add('Select Coalesce(SUM(CASE When (TabTipoLanctoPortador.Debito_Credito = ''C'' ) then TabLanctoPortador.Valor end),0) as Entrada,');
                Self.QueryLocal2.SQL.Add('       Coalesce(SUM(CASE When (TabTipoLanctoPortador.Debito_Credito = ''D'' ) then TabLanctoPortador.Valor end),0) as Saida');
                Self.QueryLocal2.SQL.Add('from tablanctoportador');
                Self.QueryLocal2.SQL.Add('join TabTipoLanctoPortador on TabTipoLanctoPortador.Codigo = TabLanctoPortador.TipoLancto');
                Self.QueryLocal2.SQL.Add('where (TRANSFERENCIAPORTADOR is not null or  valorestransferenciaportador is not null)');
                Self.QueryLocal2.SQL.Add('and lancamento is null');
                Self.QueryLocal2.SQL.Add('and  tablanctoportador.UserC = '+#39+fieldbyname('UserC').asString+#39);
                Self.QueryLocal2.SQL.Add('and  TabLanctoPortador.Data >= '+#39+FormatDateTime('mm/dd/yyyy', TmpData1)+#39);
                Self.QueryLocal2.SQL.Add('and  TabLanctoPortador.Data <= '+#39+FormatDateTime('mm/dd/yyyy', TmpData2)+#39);
                Self.QueryLocal2.SQL.Add('and tablanctoportador.Portador = '+fieldbyname('Portador').asString);
                Self.QueryLocal2.Open;

                // Esse SQL sempre e retorna uma linha s�

                inc(linha,1);
                ImprimirSimples(Linha,10,'PORTADOR: '); ImprimirNegrito(linha,20,fieldbyname('Nome').AsString);
                inc(linha,1);
                ImprimirNegrito(Linha,20,'TRANSFERENCIAS ');
                inc(linha,1);
                ImprimirSimples(Linha,25,'ENTRADAS............: ');ImprimirSimples(Linha,48, CompletaPalavra_a_Esquerda(formata_valor(Self.QueryLocal2.fieldbyname('Entrada').AsString), 15,' '));
                inc(linha,1);
                ImprimirSimples(Linha,25,'SA�DAS..............: ');ImprimirSimples(Linha,48, CompletaPalavra_a_Esquerda(formata_valor(Self.QueryLocal2.fieldbyname('Saida').AsString), 15,' '));
                inc(linha,2);

                EntradaPortador:=EntradaPortador+Self.QueryLocal2.fieldbyname('Entrada').AsCurrency;
                SaidaPortador:=SaidaPortador+Self.QueryLocal2.fieldbyname('Saida').AsCurrency;

                // Pagamentos  e Recebimentos de Titulos
                Self.QueryLocal2.Close;
                Self.QueryLocal2.SQL.Clear;
                Self.QueryLocal2.SQL.Add('Select Coalesce(SUM(CASE When (TabTipoLanctoPortador.Debito_Credito = ''C'' ) then TabLanctoPortador.Valor end),0) as Entrada,');
                Self.QueryLocal2.SQL.Add('       Coalesce(SUM(CASE When (TabTipoLanctoPortador.Debito_Credito = ''D'' ) then TabLanctoPortador.Valor end),0) as Saida');
                Self.QueryLocal2.SQL.Add('from tablanctoportador');
                Self.QueryLocal2.SQL.Add('join TabTipoLanctoPortador on TabTipoLanctoPortador.Codigo = TabLanctoPortador.TipoLancto');
                Self.QueryLocal2.SQL.Add('where Lancamento is not null');
                Self.QueryLocal2.SQL.Add('and  tablanctoportador.UserC = '+#39+fieldbyname('UserC').asString+#39);
                Self.QueryLocal2.SQL.Add('and  TabLanctoPortador.Data >= '+#39+FormatDateTime('mm/dd/yyyy', TmpData1)+#39);
                Self.QueryLocal2.SQL.Add('and  TabLanctoPortador.Data <= '+#39+FormatDateTime('mm/dd/yyyy', TmpData2)+#39);
                Self.QueryLocal2.SQL.Add('and  Tablanctoportador.Portador = '+fieldbyname('Portador').asString);
                Self.QueryLocal2.Open;
                // Esse SQL senmpre e retorna uma linha s�


                ImprimirNegrito(Linha,20,'T�TULOS ');
                inc(linha,1);
                ImprimirSimples(Linha,25,'ENTRADAS............: ');ImprimirSimples(Linha,48, CompletaPalavra_a_Esquerda(formata_valor(Self.QueryLocal2.fieldbyname('Entrada').AsString), 15,' '));
                inc(linha,1);
                ImprimirSimples(Linha,25,'SA�DAS..............: ');ImprimirSimples(Linha,48, CompletaPalavra_a_Esquerda(formata_valor(Self.QueryLocal2.fieldbyname('Saida').AsString), 15,' '));
                inc(linha,2);

                EntradaPortador:=EntradaPortador+Self.QueryLocal2.fieldbyname('Entrada').AsCurrency;
                SaidaPortador:=SaidaPortador+Self.QueryLocal2.fieldbyname('Saida').AsCurrency;

                inc(linha,1);
                ImprimirSimples(Linha,20,'------------------------------------------- ');
                inc(linha,1);
                ImprimirNegrito(Linha,20,'TOTAL DE ENTRADAS........: ');ImprimirNegrito(Linha,48,CompletaPalavra_a_Esquerda(formata_valor(EntradaPortador), 15,' '));
                inc(linha,1);
                ImprimirNegrito(Linha,20,'TOTAL DE SA�DAS..........: ');ImprimirNegrito(Linha,48,CompletaPalavra_a_Esquerda(formata_valor(SaidaPortador),15,' '));
                inc(linha,2);

                EntradaPortador:=0;
                SaidaPortador:=0;

         Next;
         end;

         Self.ImprimeTransferenciaFechamentoCaixa3(Linha, UsuarioAnterior, TmpData1, TmpData2);
         Self.ImprimeContasGerenciaisFechamentoCaixa3(Linha, UsuarioAnterior, PortadorAnterior, TmpData1, TmpData2);

         FechaBarraProgressoRelatorio;
         FecharImpressao;
     end;
end;

procedure TObjPortador.ImprimeContasGerenciaisFechamentoCaixa3(Var PLinha:Integer; PUsuario, PPortador:String; Pdata1, PData2:TDateTime);
Begin
    // Imprime as contas gerenciais
    inc(PLinha,2);
    ImprimirNegrito(PLinha,20, 'TOTAL POR CONTA GERENCIAL:');


    Self.QueryLocal2.Close;
    Self.QueryLocal2.SQL.Clear;
    Self.QueryLocal2.SQL.Add('Select SUM(Valor) as Soma, ContaGerencial, NomeContaGerencial');
    Self.QueryLocal2.SQL.Add('from PROC_REL_FECHAMENTO_CAIXA('+#39+PUsuario+#39+',');
    Self.QueryLocal2.SQL.Add(                                 PPortador+',');
    Self.QueryLocal2.SQL.Add(#39+FormatDateTime('mm/dd/yyyy', PData1)+#39+', '+#39+FormatDateTime('mm/dd/yyyy', Pdata2)+#39+')');
    Self.QueryLocal2.SQL.Add('Where Tipo = ''C''  ');
    Self.QueryLocal2.SQL.Add('Group By ContaGerencial, NomeContaGerencial');
    Self.QueryLocal2.Open;

    inc(PLinha,1);
    ImprimirNegrito(PLinha,25,'RECEBIMENTO:');
    While not (Self.QueryLocal2.eof) do
    Begin
         inc(PLinha,1);
         ImprimirNegrito(PLinha,30,CompletaPalavra(Self.QueryLocal2.fieldbyname('NomeContaGerencial').AsString,40,' '));
         ImprimirSimples(PLinha,70,CompletaPalavra_a_Esquerda(formata_valor(Self.QueryLocal2.fieldbyname('Soma').AsString),15,' '));
         Self.QueryLocal2.Next;
    end;

    Self.QueryLocal2.Close;
    Self.QueryLocal2.SQL.Clear;
    Self.QueryLocal2.SQL.Add('Select SUM(Valor) as Soma, ContaGerencial, NomeContaGerencial');
    Self.QueryLocal2.SQL.Add('from PROC_REL_FECHAMENTO_CAIXA('+#39+PUsuario+#39+',');
    Self.QueryLocal2.SQL.Add(                                PPortador+',');
    Self.QueryLocal2.SQL.Add(#39+FormatDateTime('mm/dd/yyyy', Pdata1)+#39+', '+#39+FormatDateTime('mm/dd/yyyy', PData2)+#39+')');
    Self.QueryLocal2.SQL.Add('Where Tipo = ''D''  ');
    Self.QueryLocal2.SQL.Add('Group By ContaGerencial, NomeContaGerencial');
    Self.QueryLocal2.Open;

    inc(PLinha,1);
    ImprimirNegrito(PLinha,25,'PAGAMENTO:');
    While not (Self.QueryLocal2.eof) do
    Begin
         inc(PLinha,1);
         ImprimirNegrito(PLinha,30,CompletaPalavra(Self.QueryLocal2.fieldbyname('NOmeContaGerencial').AsString,40,' '));
         ImprimirSimples(PLinha,70,CompletaPalavra_a_Esquerda(formata_valor(Self.QueryLocal2.fieldbyname('Soma').AsString),15,' '));
         Self.QueryLocal2.Next;
    end;

end;

procedure TObjPortador.ImprimeTransferenciaFechamentoCaixa3(Var PLinha:Integer; PUsuario :String; Pdata1, PData2:TDateTime);
Begin
    // Imprime as contas gerenciais
    inc(PLinha,1);
    ImprimirNegrito(PLinha,20, 'TRANSFER�NCIAS:');

    Self.QueryLocal2.Close;
    Self.QueryLocal2.SQL.Clear;
    Self.QueryLocal2.SQL.Add('Select SUM(TabTransferenciaPortador.Valor)/2 as Valor, TabTransferenciaPortador.Codigo, TabGrupo.Descricao');
    Self.QueryLocal2.SQL.Add('from tablanctoportador');
    Self.QueryLocal2.SQL.Add('join TabTipoLanctoPortador on TabTipoLanctoPortador.Codigo = TabLanctoPortador.TipoLancto');
    Self.QueryLocal2.SQL.Add('join TabTransferenciaPortador on TabTransferenciaPortador.Codigo = Tablanctoportador.TransferenciaPortador');
    Self.QueryLocal2.SQL.Add('join TabGrupo on TabGrupo.Codigo = TabTransferenciaPortador.Grupo');
    Self.QueryLocal2.SQL.Add('where tablanctoportador.UserC = '+#39+PUsuario+#39);
    Self.QueryLocal2.SQL.Add('and  TabLanctoPortador.Data >= '+#39+FormatDateTime('mm/dd/yyyy', Pdata1)+#39);
    Self.QueryLocal2.SQL.Add('and  TabLanctoPortador.Data <= '+#39+FormatDateTime('mm/dd/yyyy', Pdata2)+#39);
    Self.QueryLocal2.SQL.Add('Group By  TabTransferenciaPortador.Codigo, TabGrupo.Descricao');
    Self.QueryLocal2.Open;

    While not (Self.QueryLocal2.eof) do
    Begin
         inc(PLinha,1);
         ImprimirNegrito(PLinha,30,CompletaPalavra(Self.QueryLocal2.fieldbyname('Descricao').AsString,40,' '));
         ImprimirSimples(PLinha,70,CompletaPalavra_a_Esquerda(formata_valor(Self.QueryLocal2.fieldbyname('Valor').AsString),15,' '));
         Self.QueryLocal2.Next;
    end;

end;



end.

