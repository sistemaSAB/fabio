unit UinfoCheque;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, UobjValores,
  UobjValoresTransferenciaPortador;

type
  TFinfocheque = class(TForm)
    memoinformacao: TMemo;
    Panel1: TPanel;
    edtnumcheque: TMaskEdit;
    Label1: TLabel;
    pnl3: TPanel;
    lb1: TLabel;
    pnlRodape: TPanel;
    Img1: TImage;
    btok: TSpeedButton;
    btimprimir: TSpeedButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtnumchequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtnumchequeKeyPress(Sender: TObject; var Key: Char);
    procedure btokClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btimprimirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Finfocheque: TFinfocheque;
  ObjValores:TobjValores;
  ObjValoresTransferenciaPortador:TobjValoresTransferenciaPortador;
implementation

uses UessencialGlobal, Upesquisa, UDataModulo,ibquery, UReltxtRDPRINT,
  UescolheImagemBotao;

{$R *.DFM}

procedure TFinfocheque.FormClose(Sender: TObject;var Action: TCloseAction);
begin

     if (Objvalores<>nil)
     then ObjValores.Free;

     if (ObjValoresTransferenciaPortador <> nil)
     then ObjValoresTransferenciaPortador.Free;


end;

procedure TFinfocheque.edtnumchequeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjValores.cheque.get_pesquisaCHEQUETERCEIRO,objvalores.cheque.get_titulopesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
End;

procedure TFinfocheque.edtnumchequeKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then btok.OnClick(sender)
     Else Begin
               If not(key in ['0'..'9',#8])
               Then key:=#0; 
     End;


end;

procedure TFinfocheque.btokClick(Sender: TObject);
var
Ibquery:Tibquery;
begin
     Try
         Ibquery:=Tibquery.create(nil);
         Ibquery.database:=FdataModulo.Ibdatabase;
     Except
           Messagedlg('Erro na tentativa de Criar a Query',mterror,[mbok],0);
           exit;
     End;
Try     

     

     if edtnumcheque.text=''
     Then exit;

     memoinformacao.lines.clear;
     memoinformacao.lines.add('* * * * * * * * * * * * * * * INFORMA��O DE CHEQUE RECEBIDO * * * * * * * * * * * * * * *');

     //******ENCONTRADO O CHEQUE NA TABELA DE VALORES*********
     If (Objvalores.Cheque.LocalizaCodigo(edtnumcheque.text)=False)
     Then Begin
               Messagedlg('Cheque N�o encontrado!',mterror,[mbok],0);
               exit;
     End;

     If (Objvalores.localizacheque(edtnumcheque.text)=False)
     Then Begin
               Messagedlg('Lan�amento n�o localizado para o cheque escolhido!',mtinformation,[mbok],0);
               exit;
     End;
     Objvalores.TabelaparaObjeto;

     //DADOS GERAIS SOBRE O CHEQUE

     Self.memoinformacao.Lines.add(completapalavra('COD.SISTEMA',20,' ')+'='+ObjValores.Cheque.Get_CODIGO);
     Self.memoinformacao.Lines.add('');
     Self.memoinformacao.Lines.add(completapalavra('DADOS DO CHEQUE',20,' '));
     Self.memoinformacao.Lines.add(completapalavra('   BANCO',20,' ')+':'+CompletaPalavra(ObjValores.Cheque.Get_Banco,10,' '));
     Self.memoinformacao.Lines.add(completapalavra('   AG�NCIA',20,' ')+':'+CompletaPalavra(ObjValores.Cheque.Get_Agencia,10,' '));
     Self.memoinformacao.Lines.add(completapalavra('   N�MERO',20,' ')+':'+CompletaPalavra(ObjValores.Cheque.Get_NumCheque,10,' '));
     Self.memoinformacao.Lines.add(completapalavra('   VENCTO',20,' ')+':'+CompletaPalavra(ObjValores.Cheque.Get_Vencimento,10,' '));
     Self.memoinformacao.Lines.add(completapalavra('   VALOR',20,' ')+':'+CompletaPalavra(formata_valor(ObjValores.Cheque.Get_Valor),10,' '));


     Self.memoinformacao.Lines.add('');
     Self.memoinformacao.Lines.add(completapalavra('LAN�AMENTO N�',20,' ')+'='+completapalavra(ObjValores.Lancamento.Get_CODIGO,7,' ')+'  HIST�RICO='+completapalavra(ObjValores.Lancamento.Get_Historico,49,' '));
     Self.memoinformacao.Lines.add(completapalavra('   VALOR',20,' ')+'=R$ '+formata_valor(ObjValores.Lancamento.get_valor));
     Self.memoinformacao.Lines.add(completapalavra('   DATA',20,' ')+'='+ObjValores.Lancamento.Get_Data);
     Self.memoinformacao.Lines.add('');

     if (ObjValores.Lancamento.Pendencia.Get_CODIGO='')
     then Self.memoinformacao.Lines.add (completapalavra('T�TULO',20,' ')+'="LOTE"')
     Else Begin
              Self.memoinformacao.Lines.add(completapalavra('T�TULO',20,' ')+'='+completapalavra(ObjValores.Lancamento.Pendencia.Titulo.get_codigo,7,' ')+'  HIST�RICO='+completapalavra(ObjValores.Lancamento.Pendencia.Titulo.Get_HISTORICO,49,' '));
              if (ObjValores.Lancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
              THEN Self.memoinformacao.Lines.add(completapalavra('   TIPO',20,' ')+'=CONTAS A PAGAR')
              eLSE Self.memoinformacao.Lines.add(completapalavra('   TIPO',20,' ')+'=CONTAS A RECEBER');
              Self.memoinformacao.Lines.add(completapalavra('   EMISS�O',20,' ')+'='+ObjValores.Lancamento.Pendencia.titulo.Get_emissao);
              Self.memoinformacao.Lines.add(completapalavra('   VALOR',20,' ')+'=R$'+formata_valor(ObjValores.Lancamento.Pendencia.titulo.Get_Valor));
              Self.memoinformacao.Lines.add(completapalavra('PEND�NCIA',20,' ')+'='+completapalavra(ObjValores.Lancamento.Pendencia.get_codigo,7,' ')+'  HIST�RICO='+completapalavra(ObjValores.Lancamento.Pendencia.Get_Historico,49,' '));
              Self.memoinformacao.Lines.add(completapalavra('   VENCIMENTO',20,' ')+'='+ObjValores.Lancamento.Pendencia.Get_VENCIMENTO);
              Self.memoinformacao.Lines.add(completapalavra('   VALOR',20,' ')+'=R$ '+formata_valor(ObjValores.Lancamento.Pendencia.Get_Valor));
              Self.memoinformacao.Lines.add(completapalavra(ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Nome,20,' ')+'='+ObjValores.Lancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR+' - '+ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,ObjValores.Lancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
     End;
     Self.memoinformacao.Lines.add(completapalavra('_',90,'_'));

     //Agora as Transferencias que foram efetuadas com esse cheque
     With ibquery do
     Begin
        close;
        sql.clear;
        sql.add('Select distinct(Transferenciaportador) as CODIGOTRANSFERENCIA,tabtransferenciaportador.data from tabValoresTransferenciaPortador TVP');
        sql.add('join tabchequesportador on TVP.valores=tabchequesportador.codigo');
        sql.add('join tabtransferenciaportador on TVP.transferenciaportador=tabtransferenciaportador.codigo');
        sql.add('where TVP.Valores='+ObjValores.Cheque.Get_CODIGO);
        sql.add('order by tabtransferenciaportador.data');
        open;
        if (recordcount=0)
        Then exit;

        While not(eof) do
        begin
            ObjValoresTransferenciaPortador.TransferenciaPortador.ZerarTabela;
            ObjValoresTransferenciaPortador.TransferenciaPortador.LocalizaCodigo(fieldbyname('CODIGOTRANSFERENCIA').asstring);
            ObjValoresTransferenciaPortador.TransferenciaPortador.TabelaparaObjeto;

            //Dois casos, transferencia normal ou pagamento de contas
            //que acaba transferindo para um portador especifico

            if (ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Get_CODIGO='')//Transferencia normal
            Then Begin
                      Self.memoinformacao.lines.add(CompletaPalavra('TRANSFER�NCIA',20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.Get_codigo);
                      Self.memoinformacao.lines.add(CompletaPalavra('   DATA',20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.Get_Data);
                      Self.memoinformacao.lines.add(CompletaPalavra('   PORTADOR ORIGEM',20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.Get_codigo+'-'+ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorOrigem.Get_Nome);
                      Self.memoinformacao.lines.add(CompletaPalavra('   PORTADOR DESTINO',20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorDestino.Get_codigo+'-'+ObjValoresTransferenciaPortador.TransferenciaPortador.PortadorDestino.Get_Nome);
            End
            Else Begin//pagamento de contas
                      Self.memoinformacao.lines.add(CompletaPalavra('PAGAMENTO DE CONTAS',20,' '));
                      Self.memoinformacao.Lines.add(completapalavra('  LAN�AMENTO N�',20,' ')+'='+completapalavra(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Get_CODIGO,7,' ')+'  HIST�RICO='+completapalavra(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Get_Historico,49,' '));
                      Self.memoinformacao.Lines.add(completapalavra('     VALOR',20,' ')+'=R$ '+formata_valor(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.get_valor));
                      Self.memoinformacao.Lines.add(completapalavra('     DATA',20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Get_Data);

                      if (ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Get_CODIGO='')
                      then Self.memoinformacao.Lines.add (completapalavra('  T�TULO',20,' ')+'="LOTE"')
                      Else Begin
                               Self.memoinformacao.Lines.add(completapalavra('  T�TULO',20,' ')+'='+completapalavra(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.get_codigo,7,' ')+'  HIST�RICO='+completapalavra(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.Get_HISTORICO,49,' '));
                               if (ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D')
                               THEN Self.memoinformacao.Lines.add(completapalavra('     TIPO',20,' ')+'=CONTAS A PAGAR')
                               eLSE Self.memoinformacao.Lines.add(completapalavra('     TIPO',20,' ')+'=CONTAS A RECEBER');
                               Self.memoinformacao.Lines.add(completapalavra('     EMISS�O',20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.titulo.Get_emissao);
                               Self.memoinformacao.Lines.add(completapalavra('     VALOR',20,' ')+'=R$'+formata_valor(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.titulo.Get_Valor));
                               Self.memoinformacao.Lines.add(completapalavra('  PEND�NCIA',20,' ')+'='+completapalavra(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.get_codigo,7,' ')+'  HIST�RICO='+completapalavra(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Get_Historico,49,' '));
                               Self.memoinformacao.Lines.add(completapalavra('     VENCIMENTO',20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Get_VENCIMENTO);
                               Self.memoinformacao.Lines.add(completapalavra('     VALOR',20,' ')+'=R$ '+formata_valor(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Get_Valor));
                               Self.memoinformacao.Lines.add('  '+completapalavra(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Nome,20,' ')+'='+ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR+' - '+ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,ObjValoresTransferenciaPortador.TransferenciaPortador.CodigoLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                      End;
            End;
            

            next;
        End;
     End;



     {
     strgrid.ColCount:=6;
     strgrid.RowCount:=2;
     strgrid.FixedRows:=1;
     strgrid.COLS[0].CLEAR;
     strgrid.Cells[0,0]:='T�tulo';
     strgrid.COLS[1].CLEAR;
     strgrid.Cells[1,0]:='Hist�rico do T�tulo';
     strgrid.COLS[2].CLEAR;
     strgrid.Cells[2,0]:='Pend�ncia';
     strgrid.COLS[3].CLEAR;
     strgrid.Cells[3,0]:='Lan�amento';
     strgrid.COLS[4].CLEAR;
     strgrid.Cells[4,0]:='Data Lan�amento';
     strgrid.COLS[5].CLEAR;
     strgrid.Cells[5,0]:='Hist�rico Lan�amento';

     strgrid.Cells[0,1]:=ObjValores.Lancamento.Pendencia.Titulo.get_codigo;
     strgrid.Cells[1,1]:=ObjValores.Lancamento.Pendencia.Titulo.get_historico;
     strgrid.Cells[2,1]:=ObjValores.Lancamento.Pendencia.Get_CODIGO;
     strgrid.Cells[3,1]:=ObjValores.Lancamento.get_codigo;
     strgrid.Cells[4,1]:=ObjValores.Lancamento.Get_Data;
     strgrid.Cells[5,1]:=ObjValores.Lancamento.Get_Historico;
     AjustaLArguraColunaGrid(strgrid);}
Finally
       freeandnil(ibquery);
End;

end;

procedure TFinfocheque.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
     PegaFiguraBotao(btimprimir,'botaorelatorios.bmp');
     memoinformacao.Text:='';

     Try
        ObjValores:=TobjValores.create;
        ObjValoresTransferenciaPortador:=TobjValoresTransferenciaPortador.create;
        btok.enabled:=True;
        edtnumcheque.SETFOCUS;
    Except
           Messagedlg('Erro na Cria��o do Objeto de Valores, o m�dulo est� desativado por seguran�a!',mterror,[mbok],0);
           btok.enabled:=False;
    End;

    if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE INFORMA��O DE CHEQUE')=False)
    Then Begin
               desab_botoes(Self);
               exit;
     End
    Else habilita_botoes(Self);
end;

procedure TFinfocheque.btimprimirClick(Sender: TObject);
var
cont,linha:integer;
begin

     FreltxtRDPRINT.ConfiguraImpressao;
     FreltxtRDPRINT.RDprint.Abrir;

     if (FreltxtRDPRINT.RDprint.Setup=False)
     Then begin
               FreltxtRDPRINT.RDprint.Fechar;
               exit;
     end;
     linha:=3;


     for cont:=0 to memoinformacao.Lines.count-1 do
     Begin
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,memoinformacao.lines[cont]);
          inc(linha,1);
     end;
     FreltxtRDPRINT.RDprint.Fechar;

end;

procedure TFinfocheque.btCancelarClick(Sender: TObject);
begin
    self.Close;
end;

//Rodolfo
procedure TFinfocheque.FormCreate(Sender: TObject);
var
  addDuploClique : TDuploClique;
begin
  addDuploClique.AdicionaDuploClique(Sender);   //adicao de duplo clique em edits que possuem onkeydown
end;

end.


