object FchequesPortador: TFchequesPortador
  Left = 273
  Top = 225
  Width = 640
  Height = 417
  Caption = 'Cadastro de Cheques de Portadores - EXCLAIM TECNOLOGIA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 517
      Top = 0
      Width = 107
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Cheques Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btopcoes: TBitBtn
      Left = 350
      Top = -1
      Width = 50
      Height = 52
      HelpType = htKeyword
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
    end
    object BtNovo: TBitBtn
      Left = 0
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btalterar: TBitBtn
      Left = 50
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object Btgravar: TBitBtn
      Left = 100
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object BtCancelar: TBitBtn
      Left = 150
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 250
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 300
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object btexcluir: TBitBtn
      Left = 200
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 400
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 329
    Width = 624
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 3
    DesignSize = (
      624
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 624
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 326
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 795
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 795
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 50
    Width = 624
    Height = 113
    Align = alTop
    BevelOuter = bvNone
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 0
      Top = 0
      Width = 624
      Height = 113
      Align = alClient
      Stretch = True
    end
    object Label1: TLabel
      Left = 11
      Top = 15
      Width = 39
      Height = 14
      Caption = 'Codigo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 11
      Top = 38
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 11
      Top = 61
      Width = 28
      Height = 14
      Caption = 'Valor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label22: TLabel
      Left = 11
      Top = 85
      Width = 66
      Height = 14
      Caption = 'Vencimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label13: TLabel
      Left = 173
      Top = 62
      Width = 91
      Height = 14
      Caption = 'N'#186' Cheque Temp'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label20: TLabel
      Left = 173
      Top = 85
      Width = 31
      Height = 14
      Caption = 'Temp'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label21: TLabel
      Left = 173
      Top = 38
      Width = 62
      Height = 14
      Caption = 'Motivo Dev.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Shape1: TShape
      Left = 0
      Top = 107
      Width = 2000
      Height = 1
      Pen.Color = clWhite
      Pen.Mode = pmMerge
    end
    object edtportador: TEdit
      Left = 88
      Top = 36
      Width = 70
      Height = 19
      Color = 6073854
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      OnKeyDown = edtportadorKeyDown
    end
    object edtvalor: TEdit
      Left = 88
      Top = 59
      Width = 70
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 4
      OnKeyPress = edtvalorKeyPress
    end
    object edtvencimento: TMaskEdit
      Left = 88
      Top = 82
      Width = 70
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 6
      Text = '  /  /    '
    end
    object edtChequedoPortador: TEdit
      Left = 173
      Top = 13
      Width = 41
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 1
      Visible = False
    end
    object EdtCodigo: TEdit
      Left = 88
      Top = 13
      Width = 70
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 0
    end
    object EdtNumeroChequeTemp: TEdit
      Left = 276
      Top = 59
      Width = 70
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 5
    end
    object ComboTemp: TComboBox
      Left = 276
      Top = 82
      Width = 70
      Height = 21
      BevelKind = bkSoft
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 7
      OnExit = ComboTempExit
      Items.Strings = (
        'SIM'
        'N'#195'O')
    end
    object edtmotivodevolucao: TEdit
      Left = 276
      Top = 36
      Width = 70
      Height = 19
      Ctl3D = False
      MaxLength = 9
      ParentCtl3D = False
      TabOrder = 3
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 163
    Width = 624
    Height = 166
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel2'
    Color = 14024703
    TabOrder = 2
    object Label17: TLabel
      Left = 407
      Top = 55
      Width = 24
      Height = 13
      Caption = 'CPF'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label16: TLabel
      Left = 257
      Top = 55
      Width = 40
      Height = 13
      Caption = 'Cliente'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label15: TLabel
      Left = 13
      Top = 83
      Width = 8
      Height = 13
      Caption = 'a'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label14: TLabel
      Left = 13
      Top = 52
      Width = 122
      Height = 13
      Caption = 'cheque a quantia de '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 13
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Comp'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 62
      Top = 8
      Width = 37
      Height = 13
      Caption = 'Banco'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 112
      Top = 8
      Width = 47
      Height = 13
      Caption = 'Ag'#234'ncia'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label7: TLabel
      Left = 169
      Top = 8
      Width = 18
      Height = 13
      Caption = 'DV'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 207
      Top = 8
      Width = 16
      Height = 13
      Caption = 'C1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 237
      Top = 8
      Width = 34
      Height = 13
      Caption = 'Conta'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label10: TLabel
      Left = 361
      Top = 8
      Width = 16
      Height = 13
      Caption = 'C2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label11: TLabel
      Left = 391
      Top = 8
      Width = 30
      Height = 13
      Caption = 'S'#233'rie'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label12: TLabel
      Left = 439
      Top = 8
      Width = 66
      Height = 13
      Caption = 'Cheque N.'#186
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label18: TLabel
      Left = 520
      Top = 8
      Width = 16
      Height = 13
      Caption = 'C3'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label19: TLabel
      Left = 149
      Top = 52
      Width = 17
      Height = 13
      Caption = 'R$'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label23: TLabel
      Left = 89
      Top = 74
      Width = 101
      Height = 13
      Caption = 'Portador e Cpf 01'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label24: TLabel
      Left = 89
      Top = 95
      Width = 101
      Height = 13
      Caption = 'Portador e Cpf 02'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label26: TLabel
      Left = 13
      Top = 119
      Width = 163
      Height = 13
      Caption = 'C'#243'digo de Barras do Cheque'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object edtcodigodebarras: TEdit
      Left = 13
      Top = 134
      Width = 532
      Height = 19
      Ctl3D = False
      MaxLength = 50
      ParentCtl3D = False
      TabOrder = 14
    end
    object edtcpfcliente2: TEdit
      Left = 394
      Top = 92
      Width = 151
      Height = 19
      Ctl3D = False
      MaxLength = 20
      ParentCtl3D = False
      TabOrder = 13
    end
    object edtcliente2: TEdit
      Left = 198
      Top = 92
      Width = 188
      Height = 19
      Ctl3D = False
      MaxLength = 50
      ParentCtl3D = False
      TabOrder = 12
    end
    object edtCliente1: TEdit
      Left = 198
      Top = 71
      Width = 188
      Height = 19
      Ctl3D = False
      MaxLength = 50
      ParentCtl3D = False
      TabOrder = 10
    end
    object edtcpfcliente1: TEdit
      Left = 394
      Top = 71
      Width = 151
      Height = 19
      Ctl3D = False
      MaxLength = 20
      ParentCtl3D = False
      TabOrder = 11
    end
    object edtcomp: TEdit
      Left = 13
      Top = 24
      Width = 33
      Height = 19
      Ctl3D = False
      MaxLength = 5
      ParentCtl3D = False
      TabOrder = 0
    end
    object edtbanco: TEdit
      Left = 56
      Top = 24
      Width = 49
      Height = 19
      Ctl3D = False
      MaxLength = 5
      ParentCtl3D = False
      TabOrder = 1
    end
    object edtagencia: TEdit
      Left = 115
      Top = 24
      Width = 41
      Height = 19
      Ctl3D = False
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 2
    end
    object edtdv: TEdit
      Left = 166
      Top = 24
      Width = 25
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 3
    end
    object edtc1: TEdit
      Left = 201
      Top = 24
      Width = 28
      Height = 19
      Ctl3D = False
      MaxLength = 5
      ParentCtl3D = False
      TabOrder = 4
    end
    object edtconta: TEdit
      Left = 239
      Top = 24
      Width = 108
      Height = 19
      Ctl3D = False
      MaxLength = 25
      ParentCtl3D = False
      TabOrder = 5
    end
    object edtc2: TEdit
      Left = 357
      Top = 24
      Width = 24
      Height = 19
      Ctl3D = False
      MaxLength = 5
      ParentCtl3D = False
      TabOrder = 6
    end
    object edtserie: TEdit
      Left = 391
      Top = 24
      Width = 31
      Height = 19
      Ctl3D = False
      MaxLength = 5
      ParentCtl3D = False
      TabOrder = 7
    end
    object edtnumcheque: TEdit
      Left = 432
      Top = 24
      Width = 81
      Height = 19
      Ctl3D = False
      MaxLength = 25
      ParentCtl3D = False
      TabOrder = 8
    end
    object edtc3: TEdit
      Left = 523
      Top = 24
      Width = 22
      Height = 19
      Ctl3D = False
      MaxLength = 5
      ParentCtl3D = False
      TabOrder = 9
    end
  end
end
