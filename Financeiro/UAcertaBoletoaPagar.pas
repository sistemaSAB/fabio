unit UAcertaBoletoaPagar;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Buttons;

type
  TFAcertaBoletoaPagar = class(TForm)
    Grid: TStringGrid;
    Btsair: TBitBtn;
    Btcancelar: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    lbcredordevedor: TLabel;
    lbnumtitulo: TLabel;
    lbvalortitulo: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BtsairClick(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: Char);
    procedure BtcancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FAcertaBoletoaPagar: TFAcertaBoletoaPagar;

implementation

uses UessencialGlobal;

{$R *.DFM}

procedure TFAcertaBoletoaPagar.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     Self.Tag:=0;
end;

procedure TFAcertaBoletoaPagar.BtsairClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;

end;

procedure TFAcertaBoletoaPagar.GridKeyPress(Sender: TObject;
  var Key: Char);
begin
     if (self.grid.Col<>5) and  (self.grid.Col<>2) and  (self.grid.Col<>6)  
     Then key:=#0;
end;

procedure TFAcertaBoletoaPagar.BtcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.Close;
end;

end.
