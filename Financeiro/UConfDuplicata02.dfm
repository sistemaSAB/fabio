object FConfDuplicata02: TFConfDuplicata02
  Left = 4
  Top = -9
  BorderStyle = bsDialog
  Caption = 'Configura'#231#227'o de Duplicatas Modelo 02'
  ClientHeight = 549
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnDragDrop = FormDragDrop
  OnDragOver = FormDragOver
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  DesignSize = (
    792
    549)
  PixelsPerInch = 96
  TextHeight = 13
  object ValorFatura: TLabel
    Left = 20
    Top = 125
    Width = 54
    Height = 13
    Caption = 'ValorFatura'
    OnClick = ValorFaturaClick
  end
  object NumeroFatura: TLabel
    Left = 128
    Top = 125
    Width = 67
    Height = 13
    Caption = 'NumeroFatura'
    OnClick = ValorFaturaClick
  end
  object ValorDuplicata: TLabel
    Left = 240
    Top = 125
    Width = 69
    Height = 13
    Caption = 'ValorDuplicata'
    OnClick = ValorFaturaClick
  end
  object numeroduplicata: TLabel
    Left = 328
    Top = 125
    Width = 78
    Height = 13
    Caption = 'numeroduplicata'
    OnClick = ValorFaturaClick
  end
  object vencimento: TLabel
    Left = 420
    Top = 127
    Width = 55
    Height = 13
    Caption = 'vencimento'
    OnClick = ValorFaturaClick
  end
  object nome: TLabel
    Left = 20
    Top = 200
    Width = 26
    Height = 13
    Caption = 'nome'
    OnClick = ValorFaturaClick
  end
  object endereco: TLabel
    Left = 20
    Top = 215
    Width = 45
    Height = 13
    Caption = 'endereco'
    OnClick = ValorFaturaClick
  end
  object estado: TLabel
    Left = 440
    Top = 230
    Width = 32
    Height = 13
    Caption = 'estado'
    OnClick = ValorFaturaClick
  end
  object cidade: TLabel
    Left = 20
    Top = 230
    Width = 32
    Height = 13
    Caption = 'cidade'
    OnClick = ValorFaturaClick
  end
  object cnpj: TLabel
    Left = 20
    Top = 260
    Width = 20
    Height = 13
    Caption = 'cnpj'
    OnClick = ValorFaturaClick
  end
  object ie: TLabel
    Left = 424
    Top = 260
    Width = 8
    Height = 13
    Caption = 'ie'
    OnClick = ValorFaturaClick
  end
  object valorextenso1: TLabel
    Left = 20
    Top = 275
    Width = 66
    Height = 13
    Caption = 'valorextenso1'
    OnClick = ValorFaturaClick
  end
  object valorextenso2: TLabel
    Left = 20
    Top = 290
    Width = 66
    Height = 13
    Caption = 'valorextenso2'
    OnClick = ValorFaturaClick
  end
  object emissao: TLabel
    Left = 488
    Top = 126
    Width = 38
    Height = 13
    Caption = 'emissao'
    OnClick = ValorFaturaClick
  end
  object FONE: TLabel
    Left = 524
    Top = 229
    Width = 29
    Height = 13
    Caption = 'FONE'
    OnClick = ValorFaturaClick
  end
  object BAIRRO: TLabel
    Left = 244
    Top = 214
    Width = 41
    Height = 13
    Caption = 'BAIRRO'
    OnClick = ValorFaturaClick
  end
  object pedidos1: TLabel
    Left = 552
    Top = 38
    Width = 43
    Height = 13
    Caption = 'pedidos1'
    OnClick = ValorFaturaClick
  end
  object pedidos2: TLabel
    Left = 552
    Top = 51
    Width = 43
    Height = 13
    Caption = 'pedidos2'
    OnClick = ValorFaturaClick
  end
  object pedidos3: TLabel
    Left = 552
    Top = 63
    Width = 43
    Height = 13
    Caption = 'pedidos3'
    OnClick = ValorFaturaClick
  end
  object pedidos4: TLabel
    Left = 552
    Top = 76
    Width = 43
    Height = 13
    Caption = 'pedidos4'
    OnClick = ValorFaturaClick
  end
  object pedidos5: TLabel
    Left = 552
    Top = 88
    Width = 43
    Height = 13
    Caption = 'pedidos5'
    OnClick = ValorFaturaClick
  end
  object pedidos6: TLabel
    Left = 552
    Top = 101
    Width = 43
    Height = 13
    Caption = 'pedidos6'
    OnClick = ValorFaturaClick
  end
  object pedidos7: TLabel
    Left = 552
    Top = 113
    Width = 43
    Height = 13
    Caption = 'pedidos7'
    OnClick = ValorFaturaClick
  end
  object pedidos8: TLabel
    Left = 552
    Top = 126
    Width = 43
    Height = 13
    Caption = 'pedidos8'
    OnClick = ValorFaturaClick
  end
  object observacoes1: TLabel
    Left = 16
    Top = 142
    Width = 67
    Height = 13
    Caption = 'observacoes1'
    OnClick = ValorFaturaClick
  end
  object observacoes2: TLabel
    Left = 16
    Top = 155
    Width = 67
    Height = 13
    Caption = 'observacoes2'
    OnClick = ValorFaturaClick
  end
  object observacoes3: TLabel
    Left = 16
    Top = 167
    Width = 67
    Height = 13
    Caption = 'observacoes3'
    OnClick = ValorFaturaClick
  end
  object observacoes4: TLabel
    Left = 16
    Top = 180
    Width = 67
    Height = 13
    Caption = 'observacoes4'
    OnClick = ValorFaturaClick
  end
  object Panel1: TPanel
    Left = 6
    Top = 4
    Width = 283
    Height = 81
    Anchors = []
    DragMode = dmAutomatic
    TabOrder = 0
    object LBTOP: TLabel
      Left = 5
      Top = 31
      Width = 79
      Height = 16
      Caption = 'Linha(Top)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LBLEFT: TLabel
      Left = 5
      Top = 53
      Width = 96
      Height = 16
      Caption = 'Coluna (Left)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LBNOMECOMPONENTE: TLabel
      Left = 6
      Top = 11
      Width = 79
      Height = 16
      Caption = 'Linha(Top)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edttop: TMaskEdit
      Left = 105
      Top = 29
      Width = 75
      Height = 21
      TabOrder = 0
      OnExit = edtleftExit
      OnKeyPress = edtleftKeyPress
    end
    object edtleft: TMaskEdit
      Left = 105
      Top = 51
      Width = 75
      Height = 21
      TabOrder = 1
      OnExit = edtleftExit
      OnKeyPress = edtleftKeyPress
    end
    object botaogravar: TBitBtn
      Left = 181
      Top = 28
      Width = 98
      Height = 23
      Caption = 'GRAVAR'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = botaogravarClick
    end
    object BitBtn2: TBitBtn
      Left = 181
      Top = 50
      Width = 98
      Height = 23
      Caption = 'IMPRIMIR'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BitBtn2Click
    end
    object BitBtn3: TBitBtn
      Left = 180
      Top = 5
      Width = 99
      Height = 23
      Caption = 'SAIR'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = BitBtn3Click
    end
  end
end
