object FConfiguraComprovanteS_recibo: TFConfiguraComprovanteS_recibo
  Left = -1
  Top = 4
  Width = 708
  Height = 344
  Caption = 'Configura��o das Posi��es de Impress�o do  Cheque'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object LbValor: TLabel
    Left = 400
    Top = 24
    Width = 36
    Height = 13
    Caption = 'LbValor'
    OnClick = LBpagoAClick
  end
  object LBExtenso1: TLabel
    Left = 109
    Top = 72
    Width = 57
    Height = 13
    Caption = 'LBExtenso1'
    OnClick = LBpagoAClick
  end
  object LBExtenso2: TLabel
    Left = 109
    Top = 96
    Width = 57
    Height = 13
    Caption = 'LBExtenso2'
    OnClick = LBpagoAClick
  end
  object LbNominal: TLabel
    Left = 93
    Top = 128
    Width = 50
    Height = 13
    Caption = 'LbNominal'
    OnClick = LBpagoAClick
  end
  object LbCidadeCheque: TLabel
    Left = 304
    Top = 176
    Width = 33
    Height = 13
    Caption = 'Cidade'
    OnClick = LBpagoAClick
  end
  object LbDiaCheque: TLabel
    Left = 392
    Top = 176
    Width = 16
    Height = 13
    Caption = 'Dia'
    OnClick = LBpagoAClick
  end
  object LbMesCheque: TLabel
    Left = 472
    Top = 176
    Width = 20
    Height = 13
    Caption = 'Mes'
    OnClick = LBpagoAClick
  end
  object LbAnoCheque: TLabel
    Left = 576
    Top = 176
    Width = 19
    Height = 13
    Caption = 'Ano'
    OnClick = LBpagoAClick
  end
  object lbdobanco: TLabel
    Left = 88
    Top = 248
    Width = 31
    Height = 13
    Caption = 'Banco'
    OnClick = LBpagoAClick
  end
  object LbNumeroCheque: TLabel
    Left = 272
    Top = 216
    Width = 52
    Height = 13
    Caption = 'N� Cheque'
    OnClick = LBpagoAClick
  end
  object LbLinha1Desc: TLabel
    Left = 88
    Top = 272
    Width = 86
    Height = 13
    Caption = 'Linha 1 Descri��o'
    OnClick = LBpagoAClick
  end
  object LbLinha2Desc: TLabel
    Left = 88
    Top = 288
    Width = 86
    Height = 13
    Caption = 'Linha 2 Descri��o'
    OnClick = LBpagoAClick
  end
  object LbLinha3Desc: TLabel
    Left = 88
    Top = 304
    Width = 86
    Height = 13
    Caption = 'Linha 3 Descri��o'
    OnClick = LBpagoAClick
  end
  object LbConta: TLabel
    Left = 232
    Top = 368
    Width = 43
    Height = 13
    Caption = 'N� Conta'
    OnClick = LBpagoAClick
  end
  object Panel: TPanel
    Left = 0
    Top = 381
    Width = 684
    Height = 57
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMenu
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object LBLEFT: TLabel
      Left = 8
      Top = 8
      Width = 61
      Height = 13
      Caption = 'Esquerda'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object LBTOP: TLabel
      Left = 8
      Top = 32
      Width = 32
      Height = 13
      Caption = 'Topo'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
    end
    object edtleft: TMaskEdit
      Left = 72
      Top = 4
      Width = 108
      Height = 21
      TabOrder = 0
      OnExit = edtleftExit
      OnKeyPress = edtleftKeyPress
    end
    object edttop: TMaskEdit
      Left = 72
      Top = 28
      Width = 108
      Height = 21
      TabOrder = 1
      OnExit = edtleftExit
      OnKeyPress = edtleftKeyPress
    end
    object Btsalvar: TBitBtn
      Left = 182
      Top = 3
      Width = 139
      Height = 52
      Caption = 'Gravar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtsalvarClick
    end
    object btimprime: TBitBtn
      Left = 323
      Top = 3
      Width = 126
      Height = 52
      Caption = 'Imprimir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clHighlight
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btimprimeClick
    end
  end
end
