unit UfrValorLanctoLote;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Mask, ComCtrls, ExtCtrls, Buttons;

type
  TFrValorLanctoLote = class(TFrame)
    paneljuros: TPanel;
    label150: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    PJ_lbSaldoPendencia: TLabel;
    PJ_lbTaxaDia: TLabel;
    PJ_lbdiaatraso: TLabel;
    Label9: TLabel;
    PJ_lbSaldoComJuros: TLabel;
    Label11: TLabel;
    Label4: TLabel;
    Shape2: TShape;
    pj_vencimento: TLabel;
    Label7: TLabel;
    UpDown: TUpDown;
    edtcarencia: TMaskEdit;
    edtdatalancamento: TMaskEdit;
    Label1: TLabel;
    PJ_lbValorJuros: TLabel;
    btLancaJuros: TSpeedButton;
    btHistorico: TSpeedButton;
    procedure edtcarenciaExit(Sender: TObject);
    procedure edtcarenciaKeyPress(Sender: TObject; var Key: Char);
    procedure UpDownChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Smallint; Direction: TUpDownDirection);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure AcertapelaCarencia(PEdtvalor:Tmaskedit);
  end;

implementation

uses UessencialGlobal;

{$R *.dfm}

{procedure TFLancaNovoLancamento.AcertapelaCarencia;


{ TFrValorLanctoLote }

procedure TFrValorLanctoLote.AcertapelaCarencia(PEdtvalor: Tmaskedit);
var
ptaxa,psaldo,pvalorfinal:currency;
pnumerodias,pcarencia:integer;
begin

     Try
        Ptaxa:=strtofloat(tira_ponto(PJ_lbTaxaDia.caption));
     Except
           Ptaxa:=0;
     End;

     Try
        Pnumerodias:=0;
        if (strtodate(edtdatalancamento.text)>strtodate(pj_vencimento.Caption))//ta vencido
        then Begin
                  Pnumerodias:=strtoint(floattostr(strtodate(edtdatalancamento.text)-strtodate(pj_vencimento.Caption)));

        End;
        PJ_lbdiaatraso.caption:=inttostr(Pnumerodias);
     Except
        Pnumerodias:=0;
     End;

     try
        psaldo:=strtofloat(tira_ponto(PJ_lbSaldoPendencia.caption));
     Except
           Psaldo:=0;
     End;

     Try
        pcarencia:=strtoint(edtcarencia.Text);
     Except
           pcarencia:=0;
           edtcarencia.Text:='0';
     End;

     try
        pnumerodias:=pnumerodias-pcarencia;
        if (pnumerodias<0)
        Then pnumerodias:=0;
     Except
        pnumerodias:=0;
     End;

     Try
        Pvalorfinal:=Psaldo+((psaldo*ptaxa*Pnumerodias)/100);
     Except
        Pvalorfinal:=0;
     End;

     Self.PJ_lbSaldoComJuros.caption:=formata_valor(Pvalorfinal);
     Pedtvalor.text:=tira_ponto(formata_valor(pvalorfinal));
end;

procedure TFrValorLanctoLote.edtcarenciaExit(Sender: TObject);
var
pcarencia:integer;
pvalormaximo:integer;
begin
     Try
        pcarencia:=strtoint(edtcarencia.Text);
     Except
           edtcarencia.Text:='0';
           exit;
     End;
     
     Try
        pvalormaximo:=strtoint(PJ_lbdiaatraso.caption);
     Except
        pvalormaximo:=0;
     End;

     if (pcarencia>pvalormaximo)
     then edtcarencia.text:=inttostr(pvalormaximo)
     Else
         if (Pcarencia<0)
         Then edtcarencia.text:='0';

end;

procedure TFrValorLanctoLote.edtcarenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in['0'..'9',#13,#8])
     Then key:=#0
     Else if key=#13
          Then edtcarenciaExit(sender);

end;

procedure TFrValorLanctoLote.UpDownChangingEx(Sender: TObject;
  var AllowChange: Boolean; NewValue: Smallint;
  Direction: TUpDownDirection);
begin
    edtcarencia.Text:=inttostr(NewValue);
end;

end.
