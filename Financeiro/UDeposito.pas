unit UDeposito;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ComCtrls, Grids, ExtCtrls,UObjTransferenciaPortador,
  Buttons,UobjgeraTransferencia,db, UObjchequesPortador;

type
  TFdeposito = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    LbNomePortador: TLabel;
    edtdata: TMaskEdit;
    edtportadordestino: TEdit;
    edtcomplemento: TEdit;
    edtdocumento: TEdit;
    edtvalordinheiro: TEdit;
    STRGChequePortador: TStringGrid;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btsair: TBitBtn;
    pnlrodape: TPanel;
    imagemRodape: TImage;
    lbPortador: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lbDinheiro: TLabel;
    lbCheque: TLabel;
    lb13: TLabel;
    shp3: TShape;
    lbTotalPortador: TLabel;
    lbSomaCheques: TLabel;
    shp4: TShape;
    lb14: TLabel;
    lbSomaDinheiro: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lbSomaTotal: TLabel;
    shp5: TShape;
    bt1: TBitBtn;
    bt2: TBitBtn;
    btcheques: TBitBtn;
    btgravar: TBitBtn;
    imagemFundo: TImage;
    Shape1: TShape;
    checkSelecionaTodos: TCheckBox;
    Label6: TLabel;
    btAjuda: TSpeedButton;
    procedure edtportadordestinoKeyPress(Sender: TObject; var Key: Char);
    procedure edtportadordestinoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtportadordestinoExit(Sender: TObject);
    procedure btchequesClick(Sender: TObject);
    procedure STRGChequePortadorDblClick(Sender: TObject);
    procedure edtvalordinheiroKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalordinheiroExit(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure STRGChequePortadorKeyPress(Sender: TObject; var Key: Char);
    procedure btselecionartodosClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure checkSelecionaTodosMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure btAjudaClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    Procedure PegaDadosDeposito;
    procedure ContaCheques;
    procedure SomaValoresPortador;
    procedure LimpaLabels;
  public
    { Public declarations }
  end;

var
  Fdeposito: TFdeposito;
  Objtransferenciaportador:tobjtransferenciaportador;
  ObjGeraTransferencia:TObjGeraTransferencia;
  LocalGrupo,LocalportadorOrigem:String[09];



implementation

uses Upesquisa, Uportador, UessencialGlobal, UDataN, UDataModulo,
  UescolheImagemBotao, UpesquisaMenu, UAjuda, Uprincipal;

var
  TempStr:Tstrings;

{$R *.DFM}

procedure TFdeposito.edtportadordestinoKeyPress(Sender: TObject;
  var Key: Char);
begin
     ValidaNumeros(tedit(Sender),Key,'int');
end;

procedure TFdeposito.edtportadordestinoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador:=TFportador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjTransferenciaPortador.Get_PesquisaPortadorOrigem,ObjTransferenciaPortador.get_titulopesquisaPortadorOrigem,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtportadordestino.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                ObjTransferenciaPortador.Submit_PortadorOrigem(edtportadordestino.text);
                                LbNomePortador.caption:=ObjTransferenciaPortador.Get_NomePortadorOrigem;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;
end;

procedure TFdeposito.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Objtransferenciaportador<>nil)
     Then ObjTransferenciaPortador.Free;

     If (ObjGeraTransferencia<>nil)
     Then ObjGeraTransferencia.Free;

     if OBjChequesPortador<>nil
     then OBjChequesPortador.Free;

     If (TempStr<>nil)
     Then Freeandnil(TempStr);
end;

procedure TFdeposito.edtportadordestinoExit(Sender: TObject);
begin
     ObjTransferenciaPortador.Submit_PortadorOrigem(edtportadordestino.text);
     LbNomePortador.caption:=ObjTransferenciaPortador.Get_NomePortadorOrigem;
end;

procedure TFdeposito.btchequesClick(Sender: TObject);
var
Tempportador:integer;
Data:Tdate;
DataStr:string;
FdataN:TFdataN;
begin
Try
     FdataN:=TFdataN.create(nil);
     STRGChequePortador.Cols[0].clear;
     STRGChequePortador.ColCount:=2;
     STRGChequePortador.RowCount:=2;

     STRGChequePortador.Rows[0].clear;
     STRGChequePortador.Rows[1].clear;

     STRGChequePortador.FixedCols:=1;
     STRGChequePortador.fixedrows:=1;

     Try
       TempPortador:=Strtoint(LocalportadorOrigem);
     Except
           exit;
     End;

     DataStr:='';
     FdataN.lbfrase.caption:='Vencimento dos Cheques';
     If (FdataN.ShowModal=mrok)
     Then DataStr:=FdataN.edtdata.text;
     FdataN.lbfrase.caption:='Entre com a data';
     Try
       Data:=StrtoDate(DataStr);
     Except
           Messagedlg('A Informa��o da Data de Vencimento foi Cancelada!'+#13+'Ser� pesquisado todos os Cheques!',mtinformation,[mbok],0);
     End;

     //busca no Objeto Transferencia a Lista de Cheques por Portador
     //O portador por sua vezes cria um objeto de Chequesportador que
     //faz uma pesquisa relacionada a tabvalores buscando os dados dos cheques
     ObjTransferenciaPortador.Get_ListaChequesPortador(LocalportadorOrigem,STRGChequePortador,False,DataStr);
     Uessencialglobal.AjustaLArguraColunaGrid(STRGChequePortador);
     LbSomaCheques.caption:='0,00';

Finally
       Freeandnil(FdataN);
End;

end;

procedure TFdeposito.PegaDadosDeposito;
begin
     limpaedit(Self);
     habilita_campos(Self);
     habilita_botoes(self);
     With ObjGeraTransferencia do
     Begin
          If (LocalizaHistorico('DEP�SITO')=False)
          Then Begin
                    Messagedlg('O Gerador de Transfer�ncia "DEP�SITO" n�o foi encontrado!'+#13+'N�o � poss�vel Gerar um Dep�sito sem este Gerador!',mterror,[mbok],0);
                    desabilita_campos(Self);
                    desab_botoes(Self);
                    exit;
               End;
          TabelaparaObjeto;
          LocalportadorOrigem:=Get_PortadorOrigem;
          LocalGrupo:=Get_Grupo;
    End;

end;

procedure TFdeposito.STRGChequePortadorDblClick(Sender: TObject);
begin
     If (STRGChequePortador.cells[1,0]='') or (STRGChequePortador.Row=0)
     Then exit;


     If (STRGChequePortador.Cells[0,STRGChequePortador.Row]='X')
     Then STRGChequePortador.Cells[0,STRGChequePortador.Row]:=''
     Else STRGChequePortador.Cells[0,STRGChequePortador.Row]:='X';

     Self.ContaCheques;
     STRGChequePortador.SetFocus;

end;
procedure TFdeposito.ContaCheques;
var
cont,colunavalor:Integer;
ValorT,ValorA:Currency;
begin
     ValorA:=0;
     //procurando a coluna do valor
     colunavalor:=-1;
     For cont:=0 to STRGChequePortador.ColCount-1 do
     Begin
          If (UPPERCASE(STRGChequePortador.Cells[cont,0])='VALOR')
          Then Begin
                    colunavalor:=cont;
                    break;
               End;
     End;
     If (colunavalor=-1)
     Then exit;

     ValorT:=0;
     For cont:=1 to STRGChequePortador.RowCount-1 do
     Begin
          Try
              If (STRGChequePortador.Cells[0,cont]='X')
             Then Begin
                        ValorA:=Strtofloat(tira_ponto(STRGChequePortador.Cells[colunavalor,cont]));
                        ValorT:=ValorT+ValorA;
                  End;
          Except
                ValorA:=0;
          End;


     End;
     LbSomaCheques.caption:=Floattostr(Valort);
     LbSomaCheques.caption:=formata_valor(LbSomaCheques.caption);

end;

procedure TFdeposito.edtvalordinheiroKeyPress(Sender: TObject;
  var Key: Char);
begin
     ValidaNumeros(tedit(sender),Key,'FLOAT');
end;

procedure TFdeposito.edtvalordinheiroExit(Sender: TObject);
begin

     if(edtvalordinheiro.text<>'') Then
     Begin
        edtvalordinheiro.text:=formata_valor(edtvalordinheiro.text);
        lbSomaDinheiro.Caption:=edtvalordinheiro.Text;
        lbSomaTotal.Caption:= formata_valor( FloatToStr( StrToFloat( tira_ponto( LbSomaCheques.Caption ) ) + StrToFloat( tira_ponto ( lbsomaDinheiro.Caption ) ) ) );
    end;
end;

procedure TFdeposito.btgravarClick(Sender: TObject);
var
CodigoDeposito:string;
cont:integer;
begin
     With Objtransferenciaportador do
     Begin
          Status:=dsinactive;
          CodigoDeposito:=Get_NovoCodigo;
          Submit_CODIGO(CodigoDeposito);
          Submit_PortadorOrigem(LocalportadorOrigem);
          Submit_Data(edtdata.text);
          Submit_Grupo(Localgrupo);
          Submit_Valor(tira_ponto(edtvalordinheiro.text));
          Submit_Documento(edtdocumento.text);
          Submit_Complemento(edtcomplemento.text);
          Submit_PortadorDestino(edtportadordestino.text);
          CodigoLancamento.Submit_codigo('');
          //A Coluna 01 supostamente vai conter os dados dos
         //c�digos da Tabela de Valores que ser�o transferidos
         //somente os que estiverem com X serao transferidos
         //TempStr:=TStringList.Create;
         tempStr.clear;

         for cont:=0 to STRGChequePortador.Cols[1].Count-1 do
         Begin
              if STRGChequePortador.Cols[0].Strings[cont]='X'
              Then tempstr.add(STRGChequePortador.Cols[19].Strings[cont]);
         End;

         Submit_ListaChequesTransferencia(TempStr);

         Status:=dsInsert;

         If (Salvar(true,true,true,true,true)=False)
         Then Begin
                   Status:=dsInactive;
              End
         Else Begin
                Messagedlg('Transfer�ncia Efetuada Com Sucesso!'+#13+'C�digo da Transfer�ncia: '+CodigoDeposito,mtinformation,[mbok],0);
                limpaedit(Self);
                STRGChequePortador.Cols[0].clear;
                STRGChequePortador.RowCount:=2;
                STRGChequePortador.Rows[1].clear;
                self.LimpaLabels;
              End;

     End;
     Self.SomaValoresPortador;
end;

procedure TFdeposito.STRGChequePortadorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If key=#13
     Then STRGChequePortador.OnDblClick(sender);
end;

procedure TFdeposito.btselecionartodosClick(Sender: TObject);
var
Cont:INteger;
begin


end;

procedure TFdeposito.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFdeposito.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     edtdata.SetFocus;

      Try
        ObjTransferenciaPortador:=TobjTransferenciaPortador.create;
        ObjGeraTransferencia:=TObjGeraTransferencia.create;
        OBjChequesPortador:=TObjChequesPortador.Create;
        TempStr:=TstringList.create;
     Except
           Messagedlg('Erro na tentativa de cria��o do Objeto de Transfer�ncia entre Portadores!',mterror,[mbok],0);
           Self.Close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(nil,nil,nil,btgravar,nil,nil,nil,btsair,nil);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;


     LocalportadorOrigem:='';
     Self.limpalabels;
     STRGChequePortador.Cols[0].clear;
     STRGChequePortador.RowCount:=2;
     STRGChequePortador.Rows[1].clear;
     PegaDadosDeposito;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE DEP�SITO')=False)
     Then Begin
               desab_botoes(Self);
               exit;
     End
     Else habilita_botoes(Self);

     SomaValoresPortador;
end;

procedure TFdeposito.SomaValoresPortador;
var
SomaTudo,SomaCheques:Currency;
PSALDO:STRING;
begin
        If LocalportadorOrigem=''
        Then Exit;

        somatudo:=0;
        SomaCheques:=0;

        PSALDO:=Objtransferenciaportador.PortadorOrigem.Get_SaldopelosLancamentos(LocalportadorOrigem);

        Try
           somatudo:=strtocurr(psaldo);
        Except
           somatudo:=0;
        End;

        Objtransferenciaportador.Submit_PortadorOrigem(LocalportadorOrigem);

        Somacheques:=OBjChequesPortador.Soma_Cheques_Terceiro_Portador(LocalportadorOrigem);

        lbPortador.Caption:=Objtransferenciaportador.Get_NomePortadorOrigem;
        lbCheque.Caption:=formata_valor(SomaCheques);
        lbDinheiro.Caption:= formata_valor( SomaTudo-SomaCheques );
        lbTotalPortador.Caption:=formata_valor( Somatudo );
end;

procedure TFdeposito.LimpaLabels;
begin

     LbNomePortador.caption:='';
     LbSomaCheques.caption:='0,00';
     lbSomaDinheiro.Caption:='0,00';
     lbSomaTotal.Caption:='0,00';
     lbDinheiro.caption:='0,00';
     lbCheque.Caption:='0,00';
     lbTotalPortador.caption:='0,00';
     lbPortador.Caption:='';
end;

procedure TFdeposito.btsairClick(Sender: TObject);
begin
     Self.close;
end;

procedure TFdeposito.checkSelecionaTodosMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  var
    cont:Integer;
begin
     if (STRGChequePortador.RowCount=0)
     Then exit;

     If  ( checkSelecionaTodos.Checked )
     Then begin
               For cont:=1 to STRGChequePortador.RowCount-1 do
               Begin
                    If (STRGChequePortador.Cells[0,Cont]='')
                    Then STRGChequePortador.Cells[0,Cont]:='X';
               End;
     End
     Else Begin
               For cont:=1 to STRGChequePortador.RowCount-1 do
               Begin
                    If (STRGChequePortador.Cells[0,Cont]='X')
                    Then STRGChequePortador.Cells[0,Cont]:='';
               End;
     End;
     Self.ContaCheques;
end;

procedure TFdeposito.btAjudaClick(Sender: TObject);
begin
      FAjuda.PassaAjuda('DEPOSITO');
      FAjuda.ShowModal;
end;

procedure TFdeposito.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('DEPOSITO');
         FAjuda.ShowModal;
    end;
end;

//Rodolfo
procedure TFdeposito.FormCreate(Sender: TObject);
var
  addDuploClique : TDuploClique;
begin
  addDuploClique.AdicionaDuploClique(Sender);   //adicao de duplo clique em edits que possuem onkeydown
end;

end.
