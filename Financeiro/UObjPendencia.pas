unit UObjPendencia;
Interface
Uses Grids, windows,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjTitulo,UobjPortador,ibquery,UobjBoletoBancario,rdprint,uobjduplicata,
     Messages, Graphics,  Forms, Buttons,ExtCtrls,Mask;

Type
   TObjPendencia=class

          Public
                ObjDataset:Tibdataset;
                  PORTADOR         :TobjPortador;//teste

                ObjQlocal:tibquery;

                Titulo           :TobjTitulo;

                BoletoBancario   :TobjBoletoBancario;
                Duplicata        :TobjDuplicata;

                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                NumeroRelatorio:string;

                Constructor Create;
                Destructor Free;
                Function    Salvar(ComCommit:Boolean):Boolean;

                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaporBoleto(pboleto: string): boolean;
                Function    LocalizaporTitulo(Ptitulo:string) :boolean;
                Function    LocalizaporDuplicata(Pduplicata:string) :boolean;


                Function    exclui(Pcodigo:string;ComCommit:Boolean)            :Boolean;
                Function    ExcluiSemLancamento(ParametroTitulo:string)  :Boolean;

                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaTiTulo              :TStringList;
                Function    Get_PesquisaporTitulo(Ptitulo:string):TStringList;
                Function    Get_TituloPesquisaTitulo        :string;
                Function    Get_PesquisaPortador                    :string;
                Function    Get_TituloPesquisaPortador      :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;
                Function    Get_CODIGO           :string;
                Function    Get_Historico        :string;
                Function    Get_Titulo           :string;
                Function    Get_VENCIMENTO       :string;
                Function    Get_PORTADOR         :string;
                Function    Get_VALOR            :string;
                Function    Get_Saldo            :string;
                Function    Get_BoletoaPagar     :string;
                Function    Get_Observacao       :string;
                Procedure   Submit_Observacao       (Parametro:string);
                Procedure   Submit_BoletoaPagar     (parametro:string);
                Procedure   Submit_CODIGO           (parametro:string);
                Procedure   Submit_Historico        (parametro:string);
                Procedure   Submit_Titulo           (parametro:string);
                Procedure   Submit_VENCIMENTO       (parametro:string);
                Procedure   Submit_PORTADOR         (parametro:string);
                Procedure   Submit_VALOR            (parametro:string);
                Procedure   Submit_Saldo            (parametro:string);
                Procedure   Get_ListaCodigo         (Parametro:TStrings;Parametrotitulo:string;Saldo_Todas_quitadas:char);
                Procedure   Get_ListaHistorico      (Parametro:TStrings;Parametrotitulo:string;Saldo_Todas_quitadas:char);
                Procedure   Get_ListaVencimento     (Parametro:TStrings;Parametrotitulo:string;Saldo_Todas_quitadas:char);
                Procedure   Get_ListaValor          (Parametro:TStrings;Parametrotitulo:string;Saldo_Todas_quitadas:char);
                Procedure   Get_ListaSaldo          (Parametro:TStrings;Parametrotitulo:string;Saldo_Todas_quitadas:char);
                procedure   Get_ListaNomeBoleto     (Parametro: TStrings;Parametrotitulo: string; Saldo_Todas_quitadas: char);
                procedure   Get_Listaboleto         (Parametro: TStrings; Parametrotitulo: string;Saldo_Todas_quitadas:char);
                procedure   Get_ListaDuplicata      (Parametro: TStrings;Parametrotitulo: string;Saldo_Todas_quitadas:char);
                procedure   Get_ListaBoletoaPagar   (Parametro: TStrings;Parametrotitulo: string; Saldo_Todas_quitadas: char);
                procedure   Get_ListaObservacao     (Parametro: TStrings;Parametrotitulo: string; Saldo_Todas_quitadas: char);

                Procedure Get_ListaLancamentos   (ParametroTipoLancto,ParametroValor,ParametroHistorico,ParametroData,ParametroCodigo: TStrings;
                                                  ParametroPendencia:string);
                Function Get_QuantRegsLancamento(ParametroPendencia:string):INteger;
                Function Get_QuantRegs(ParametroTitulo:string):Integer;
                Function AumentaSaldo(ParametroPendencia:string;ParametroValor:string;ComCommit:Boolean):Boolean;
                Function DiminuiSaldo(ParametroPendencia:string;ParametroValor:string;ComCommit:Boolean):Boolean;
                Function Get_GeradorTitulo:string;
                Function Get_CredorDevedorTitulo:string;
                Function Get_CodigoCredorDevedorTitulo:string;
                Function Desconta_Pendencias_de_Matricula(Parametrotitulo:string;ParametroData:string):Boolean;overload;
                Function Desconta_Pendencias_de_Matricula(Parametrotitulo:string):Boolean;overload;
                procedure Get_ListaCamposPorGerador(Pcodigo, Phistorico,PVencimento, PValor, PSaldo: TStrings; PGerador, PcodGerador: string;OpcoesSaldo:Integer;Pdata:string);overload;
                procedure edttituloKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edtpendenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edtduplicataKeydown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edtBoletoUsadoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);


                Function VerificaLancamentos(Ptitulo:string):integer;

                //USados no Analice
                Function  Get_PesquisaTituloInExterno(Ppesquisa: String): boolean;
                function  Eof: Boolean;
                procedure Next;
                procedure Prior;
                //**********************
                function LancaDescontoTodos(Parametrotitulo: string;PDataVencimento,PdataLancamento:string;Phistorico: string): Boolean;
                function ExcluiLancamentos(ParametroPendencia: string;Comcommit: boolean): Boolean;
                Function Get_NumpendenciaTitulo(PTitulo:string):string;
                Function Get_saldoporTitulo(Ptitulo:string):Currency;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome  :string;
                Procedure CancelaBoleto(ParametroPendencia:string);
                Procedure ExcluiDuplicata(PPendencia:string);

                Procedure EscolheDuplicatas(parametro:string;LancaIndividualTituloAtual:Boolean);
                Procedure RetornaProximascomSaldo(PTitulo,PPendenciaInicial:string;PStringList:TStringList);
                Function  Get_NovoCodigo:string;
                procedure Get_ListaPendenciaCampos(ParametroCodigo,ParametroHistorico, ParametroVencimento, ParametroValor,
                                                   ParametroSaldo,ParametroBoleto,ParametroDuplicata: TStrings; Parametrotitulo: string;Saldo_Todas_quitadas:char);
                function  Get_QuantRegsComSaldo(ParametroTitulo: string): Integer;
                Procedure Opcoes(PNumeroTitulo:string);
                function  Get_PesquisaporTituloSemSaldo(Ptitulo: string): TStringList;
                Function  PegaTitulo:string;
                Function  PegaPendencia:string;
                Function  PegaDuplicata:string;

                //*********************************************************************
                Procedure LancaBoletos(PnumeroTitulo:string);
                Function  LancaBoleto_Varias_Pendencias(PStrPend,PStrValorPend:TStringList;PValorBoleto:Currency):Boolean;
                Function  LancaVariosBoletos(PStrPend,PAdicionalObservacao:TStringList;Pvalor_Saldo:string):boolean;overload;
                Function  LancaVariosBoletos(PStrPend,PAdicionalObservacao:TStringList;Pvalor_Saldo:string;Ppreview:Boolean):boolean;overload;

                procedure Re_imprimeboleto(ppendencia: string;ppreview:Boolean);overload;
                procedure Re_imprimeboleto(ppendencia: string;ppreview:Boolean;modelo:integer);overload;

                procedure Re_imprimeduplicata(parametro: string); //celio 130108

                //*********************************************************************
                Function  Lanca_Numero_BoletoAPagar(Ptitulo:string):Boolean;
                function  Lanca_Observacao_titulo(Ptitulo: string): Boolean;
                Function  LocalizaBoleto_Convenio:Boolean;
                Procedure Imprime_Titulos_por_ContaGerencial(PtipoConta:string);
                Procedure Imprime_titulos_Aberto_ExcluirCG(PtipoConta:string);
                procedure Imprime_Pendencia_por_vencimento_ContaGerencial;
                procedure EmiteCartaCobranca(var PStrGrid: TStringGrid);
                procedure MostraClientesInadimplesPorContaGer(PCredorDevedor, PCodigoCredorDevedor,PdataInicial, PDataFinal: string; var PStrGrid: TStringGrid;OpcaoExtra:string);
                procedure fieldbyname(PCampo, PValor: String);
                procedure RetornaGrid_Receber(Parametro: TstringGrid;Parametrotitulo: string);
                function  Imprime_Boleto_SemRegistro(Ppendencia: string;ppreview:boolean;modelo:integer): Boolean;overload;
                procedure Alteravencimentosevalorespendencias(PNumeroTitulo: string);
                procedure Imprime_Titulos_por_ContaContabil(PtipoConta: string);

                //  Fun��o feita por Fabio
                Function VerificaFuturosLancamentoNaPendencia_SistemaBIA(PLocadorLocatario,PPendencia, PValor,PValorComissao:string):Boolean;

         Private
               ParametroPesquisa:TStringList;



               CODIGO           :String[09];
               Historico        :String[20];
               VENCIMENTO       :String[10];
               VALOR            :String[09];
               Saldo            :String[09];
               BoletoaPagar     :string;
               Observacao       :string;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

                Function  ImprimeDuplicataModelo01(PcodigoDuplicata: string;RecuaFolhaAntesdeImprimir:boolean):boolean;
                function  ImprimeDuplicataModelo02(PcodigoDuplicata: string;RecuaFolhaAntesdeImprimir: boolean): boolean;

                procedure edtboletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);

                procedure edtBoletoNaoUsadoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                //BOLETOS BANCO DO BRASIL***************************************
                Function  LancaBoleto_Sem_Registro_VariasPendencias(PcodigoConvenio:string;StrPend,PStrValorPend:TStringList;PvalorBoleto:Currency):Boolean;
                procedure Lancaboleto_Sem_registro(pnumerotitulo,PcodigoConvenio: string; StrLPendencias: TStringList);
                procedure lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio: string; StrLPendencias,StrLObservacaoAdicional: TStringList;PSaldo_Valor:string);overload;
                procedure lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio: string; StrLPendencias,StrLObservacaoAdicional: TStringList;PSaldo_Valor:string;Ppreview:boolean);overload;
                procedure lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio: string; StrLPendencias,StrLObservacaoAdicional: TStringList);overload;

                //**************************************************************
                procedure lancaboleto_PreImpresso(pnumerotitulo,PcodigoConvenio: string; StrLPendencias: TStringList);
                Function  lancaboleto_PreImpresso_VariasPendencias(PcodigoConvenio:string;StrPend,PStrValorPend:TStringList;PvalorBoleto:Currency):boolean;
                Function  lancaboleto_PreImpresso_VariosBoletos(PcodigoConvenio:string;StrPend,PStrObservacaoAdicional:TStringList):boolean;
                //**************************************************************

                function  Imprime_Boleto_SemRegistro(Ppendencia: string): Boolean;overload;
                function  Imprime_Boleto_SemRegistro(Ppendencia: string;ppreview:boolean): Boolean;overload;

                Procedure ImprimeBoletoPreImpresso(Saltar:boolean);
                //**************************************************************
                Function  SalvaInformacaoBoleto(Ppendencia,PBoleto:string;ComCommit:boolean):boolean;
                //************************************************************
                Function RetornaPendenciasTitulo(Ptitulo: string;Ptitulos,Ppendencias,PretornaPendencia:TStringList): boolean;
                function RetornaPropriedadeDoCliente(PPendencia: string): String;

                // **************** Feito por F�bio ****************************
                Function Lancaboleto_COM_registro_CobreBem(pnumerotitulo,PcodigoConvenio: string; StrLPendencias: TStringList):Boolean;
                procedure ImprimeDuplicataReportBuilder(PPendencia: String);
                function RetornaObservacaoPedido(PTitulo: String): String;
                function ReimprimeBoletoCobreBem(PBoletoBancario: String): Boolean;
                //**************************************************************



End;


implementation
uses stdctrls,SysUtils,Dialogs,UDatamodulo,Controls,Uobjlancamento,
Uobjgeralancamento,uescolhependencias,URDPRINT,ufiltraimp,
uimpduplicata,uimpduplicata02,uobjconfrelatorio,uconfiguraboleto,
  UopcaoRel, UAlteraPendencias_Titulo, UAcertoCreditoDebito,
  UobjBoletoSemRegistro, UobjCONVENIOSBOLETO,UObjCredorDevedor, Upesquisa,
  UAcertaBoletoaPagar, UReltxtRDPRINT, UacertaObservacaoPendencia,
  UBoletoBancario, UEmiteCartaCobranca, UlancaBoletoVariasPendencias,
  UrelPrevisaoFinanceira, UMostraBarraProgresso, UmostraStringList,
  UObjContaGer, UobjRELPERSREPORTBUILDER, UobjARQUIVOREMESSARETORNO,
  UobjCobreBem;


{ TTabTitulo }


Procedure  TObjPendencia.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO           :=FieldByName('CODIGO').asstring          ;
        Self.Historico        :=FieldByName('historico').asstring       ;
        Self.vencimento       :=FieldByName('vencimento').asstring      ;
        Self.valor            :=FieldByName('valor').asstring           ;
        Self.saldo            :=FieldByName('saldo').asstring            ;
        Self.BoletoaPagar     :=FieldByName('boletoapagar').asstring;
        Self.Observacao       :=FieldByname('Observacao').AsString;

        If (Self.titulo.LocalizaCodigo(FieldByName('titulo').asstring)=False)
        Then Begin
                  Messagedlg('T�tulo N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.titulo.TabelaparaObjeto;

        If (FieldByName('BoletoBancario').asstring<>'')
        Then Begin

                If (Self.BoletoBancario.LocalizaCodigo(FieldByName('BoletoBancario').asstring)=False)
                Then Begin
                          Messagedlg('Boleto Banc�rio N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.BoletoBancario.TabelaparaObjeto;
        End;

        If (FieldByName('Duplicata').asstring<>'')
        Then Begin
                If (Self.Duplicata.LocalizaCodigo(FieldByName('Duplicata').asstring)=False)
                Then Begin
                          Messagedlg('Duplicata N�o encontrada!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.Duplicata.TabelaparaObjeto;
        End;

        If (Self.portador.LocalizaCodigo(FieldByName('Portador').asstring)=False)
        Then Begin
                  Messagedlg('Portador N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.portador.TabelaparaObjeto;

     End;
end;


Procedure TObjPendencia.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring    :=Self.CODIGO                  ;
      FieldByName('Historico').asstring :=Self.Historico               ;
      FieldByName('Titulo').asstring    :=Self.Titulo.get_codigo       ;
      FieldByName('VENCIMENTO').asstring:=Self.VENCIMENTO              ;
      FieldByName('PORTADOR').asstring  :=Self.PORTADOR.get_codigo     ;
      FieldByName('VALOR').asstring     :=Self.VALOR                   ;
      FieldByName('Saldo').asstring     :=Self.Saldo                   ;
      FieldByName('BoletoBancario').asstring     :=Self.BoletoBancario.get_codigo;
      FieldByName('Duplicata').asstring     :=Self.Duplicata.get_codigo;
      FieldByName('boletoapagar').asstring:=Self.BoletoaPagar;
      Fieldbyname('Observacao').asstring:=self.Observacao;
  End;
End;

Procedure TObjPENDENCIA.fieldbyname(PCampo:String;PValor:String);
Begin                                                          
     PCampo:=Uppercase(pcampo);                                

     If (Pcampo='CODIGO')
     Then Self.Submit_CODIGO(pVALOR);
     If (Pcampo='HISTORICO')
     Then Self.Submit_HISTORICO(pVALOR);
     If (Pcampo='TITULO')
     Then Self.Submit_TITULO(pVALOR);
     If (Pcampo='VENCIMENTO')
     Then Self.Submit_VENCIMENTO(pVALOR);
     If (Pcampo='PORTADOR')
     Then Self.Submit_PORTADOR(pVALOR);
     If (Pcampo='VALOR')
     Then Self.Submit_VALOR(pVALOR);
     If (Pcampo='SALDO')
     Then Self.Submit_SALDO(pVALOR);
     If (Pcampo='BOLETOBANCARIO')
     Then Self.BOLETOBANCARIO.Submit_CODIGO(pVALOR);
     If (Pcampo='DUPLICATA')
     Then Self.DUPLICATA.Submit_CODIGO(pVALOR);
     If (Pcampo='BOLETOAPAGAR')
     Then Self.Submit_BOLETOAPAGAR(pVALOR);
     If (Pcampo='OBSERVACAO')
     Then Self.Submit_OBSERVACAO(pVALOR);
end;


//***********************************************************************

function TObjPendencia.Salvar(ComCommit:boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
  End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;


 if Self.status=dsinsert
 Then Begin
           If (Self.BoletoBancario.get_codigo<>'')//verificando se naum tem alguma pendencia com este boleto
           Then Begin
                     If (Self.LocalizaporBoleto(Self.BoletoBancario.get_codigo)=True)//ja existe alguma pendencia
                     Then Begin
                               Messagedlg('J� existe uma pend�ncia ligada a este boleto!'+#13+'Pend�ncia N� '+Self.Objdataset.fieldbyname('codigo').asstring,mterror,[mbok],0);
                               result:=false;
                               exit;
                     End;
           End;

        Self.ObjDataset.Insert;//libera para insercao
 End
 Else Begin
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;
 End;



 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If (ComCommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjPendencia.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Historico        :='';
        Self.Titulo.zerartabela;
        Self.BoletoBancario.zerartabela;
        Self.VENCIMENTO       :='';
        Self.PORTADOR.zerartabela;
        Self.Duplicata.zerartabela;
        Self.VALOR            :='';
        Self.Saldo            :='';
        Self.BoletoaPagar:='';
        Self.Observacao:='';
    End;
end;

Function TObjPendencia.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Historico='')
       Then Mensagem:=mensagem+'/Hist�rico';

       If (Titulo.get_codigo='')
       Then Mensagem:=mensagem+'/T�tulo';

       If (Vencimento='')
       Then Mensagem:=mensagem+'/Vencimento';

       If (Portador.get_codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Valor='')
       Then Mensagem:=mensagem+'/Valor';

       If (Saldo='')
       Then Mensagem:=mensagem+'/Saldo';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjPendencia.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.Titulo.LocalizaCodigo(Self.Titulo.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ T�tulo n�o Encontrado!';

     If (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';

     If (Self.BoletoBancario.get_codigo<>'')
     Then Begin
             If (Self.BoletoBancario.LocalizaCodigo(Self.BoletoBancario.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Boleto Banc�rio n�o Encontrado!'
             Else Begin
                       If (Self.status=dsinsert)
                       Then Begin
                               Self.BoletoBancario.TabelaparaObjeto;
                               If (Self.BoletoBancario.Get_situacao<>'N')
                               Then mensagem:=mensagem+'/ Situ��o Inv�lida do Boleto!';
                       End;
             End;

     End;

     If (Self.Duplicata.get_codigo<>'')
     Then Begin
             If (Self.Duplicata.LocalizaCodigo(Self.Duplicata.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Duplicata n�o Encontrada!';
     End;


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjPendencia.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Inteiros:=Strtoint(Self.titulo.get_codigo);
     Except
           Mensagem:=mensagem+'/T�tulo';
     End;

     try
        If (Self.BoletoBancario.get_codigo<>'')
        Then Inteiros:=Strtoint(Self.BoletoBancario.get_codigo);
     Except
           Mensagem:=mensagem+'/Boleto Banc�rio';
     End;

     try
        If (Self.Duplicata.get_codigo<>'')
        Then Inteiros:=Strtoint(Self.Duplicata.get_codigo);
     Except
           Mensagem:=mensagem+'/Duplicata';
     End;

     try
        Reais:=Strtofloat(Self.valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     try
        Reais:=Strtofloat(Self.Saldo);
     Except
           Mensagem:=mensagem+'/Saldo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjPendencia.VerificaData: Boolean;
var
Datas:Tdate;
Mensagem:string;
begin
     mensagem:='';

     Try
        Datas:=strtodate(Self.Vencimento);
     Except
           Mensagem:=mensagem+'/Vencimento';
     End;

    If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjPendencia.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try
     Mensagem:='';
     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;
Finally
end;

end;



function TObjPendencia.LocalizaporTitulo(Ptitulo:string): boolean;//ok
begin

       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Titulo,VENCIMENTO,PORTADOR,VALOR,Saldo,boletobancario,duplicata,BoletoaPagar,observacao');
           SelectSql.add('from TabPendencia where titulo='+ptitulo);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjPendencia.LocalizaporBoleto(pboleto: string): boolean;//ok
begin

       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Titulo,VENCIMENTO,PORTADOR,VALOR,Saldo,boletobancario,duplicata,BoletoaPagar,observacao');
           SelectSql.add('from TabPendencia where boletobancario='+pboleto);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjPendencia.LocalizaporDuplicata(Pduplicata: string): boolean;//ok
begin

       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Titulo,VENCIMENTO,PORTADOR,VALOR,Saldo,boletobancario,duplicata,BoletoaPagar,observacao');
           SelectSql.add('from TabPendencia where duplicata='+pduplicata);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjPendencia.LocalizaCodigo(parametro: string): boolean;//ok
begin

       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Historico,Titulo,VENCIMENTO,PORTADOR,VALOR,Saldo,boletobancario,duplicata,BoletoaPagar,observacao');
           SelectSql.add(' from TabPendencia where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjPendencia.Cancelar;
begin
     FDataModulo.IBTransaction.rollbackretaining;
     Self.status:=dsInactive;
end;

function TObjPendencia.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=true)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPendencia.create;
begin  
        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.Titulo:=TObjTitulo.create;
        Self.Portador:=TObjportador.create;
        Self.ObjQlocal:=tibquery.create(nil);
        Self.ObjQlocal.Database:=FDataModulo.IbDatabase;
        Self.BoletoBancario     :=TobjBoletoBancario.create;
        Self.Duplicata          :=TobjDuplicata.create;
        Self.ParametroPesquisa  :=TStringList.create;
        Self.ZerarTabela;
        

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Historico,Titulo,VENCIMENTO,PORTADOR,VALOR,Saldo,boletobancario,duplicata,BoletoaPagar');
                SelectSql.add(',observacao from TabPendencia where codigo=0');
                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert into Tabpendencia (CODIGO,Historico,Titulo,VENCIMENTO,PORTADOR,VALOR,Saldo,boletobancario,duplicata,BoletoaPagar,observacao) ');
                InsertSQL.add('values (:CODIGO,:Historico,:Titulo,:VENCIMENTO,:PORTADOR,:VALOR,:Saldo,:boletobancario,:duplicata,:BoletoaPagar,:observacao) ');

                ModifySQL.clear;
                ModifySQL.add(' Update Tabpendencia set CODIGO=:CODIGO,Historico=:Historico,Titulo=:Titulo,VENCIMENTO=:VENCIMENTO,');
                ModiFySql.add('PORTADOR=:PORTADOR,VALOR=:VALOR,Saldo=:Saldo,boletobancario=:boletobancario,duplicata=:duplicata,BoletoaPagar=:BoletoaPagar,observacao=:observacao where codigo=:codigo');

                DeleteSQL.clear;
                DeleteSQL.add(' Delete from tabpendencia where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Historico,Titulo,VENCIMENTO,PORTADOR,VALOR,Saldo,boletobancario,duplicata,BoletoaPagar,observacao from tabpendencia where codigo=0 ');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;

//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjPendencia.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjPendencia.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjPendencia.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;
function TObjPendencia.Get_PesquisaporTitulo(Ptitulo:string): TStringList;
begin
     Self.ParametroPesquisa.Clear;
     Self.ParametroPesquisa.add(' Select * from tabpendencia where titulo='+Ptitulo);
     Self.ParametroPesquisa.add(' and Saldo>0 order by vencimento');
     Result:=Self.ParametroPesquisa;
end;

function TObjPendencia.Get_PesquisaporTituloSemSaldo(Ptitulo:string): TStringList;
begin
     Self.ParametroPesquisa.Clear;
     Self.ParametroPesquisa.add(' Select * from tabpendencia where titulo='+Ptitulo);
     Self.ParametroPesquisa.add(' and Saldo=0 order by vencimento');
     Result:=Self.ParametroPesquisa;
end;


function TObjPendencia.Get_Pesquisa: string;
begin
     Result:=' Select * from TabPendencia ';
end;

function TObjPendencia.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Pend�ncia de T�tulos';
end;

function TObjPendencia.Get_Historico: string;
begin
     Result:=Self.historico;
end;

function TObjPendencia.Get_PORTADOR: string;
begin
     Result:=Self.portador.get_codigo
end;

function TObjPendencia.Get_Saldo: string;
begin
     Result:=Self.saldo
end;


function TObjPendencia.Get_Titulo: string;
begin
     Result:=Self.titulo.get_codigo;
end;

function TObjPendencia.Get_VALOR: string;
begin
     Result:=Self.valor;
end;

function TObjPendencia.Get_VENCIMENTO: string;
begin
     Result:=Self.vencimento;
end;

procedure TObjPendencia.Submit_Historico(parametro: string);
begin
     Self.Historico:=parametro;
end;

procedure TObjPendencia.Submit_PORTADOR(parametro: string);
begin
     Self.portador.submit_codigo(parametro);
end;

procedure TObjPendencia.Submit_Saldo(parametro: string);
begin
     Self.saldo:=parametro;
end;


procedure TObjPendencia.Submit_Titulo(parametro: string);
begin
     Self.titulo.submit_codigo(parametro);
end;

procedure TObjPendencia.Submit_VALOR(parametro: string);
begin
     Self.valor:=parametro;
end;

procedure TObjPendencia.Submit_VENCIMENTO(parametro: string);
begin
     Self.vencimento:=parametro;
end;

function TObjPendencia.Get_PesquisaTiTulo: TStringList;
begin
     Result:=Self.titulo.Get_Pesquisa;
end;

function TObjPendencia.Get_TituloPesquisaTitulo: string;
begin
    Result:=Self.titulo.Get_TituloPesquisa;
end;


function TObjPendencia.Get_PesquisaPortador: string;
begin
     Result:=Self.Portador.Get_Pesquisa;
end;

function TObjPendencia.Get_TituloPesquisaPortador: string;
begin
     Result:=Self.PORTADOR.Get_TituloPesquisa;
end;

procedure TObjPendencia.Get_ListaCodigo(Parametro: TStrings;Parametrotitulo:string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select codigo from Tabpendencia where titulo='+#39+Parametrotitulo+#39);

          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );

          open;
          Parametro.clear;
          Parametro.add('C�DIGO');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('codigo').asstring);
               next;
          End;
          close;
     End;

end;

procedure TObjPendencia.Get_ListaHistorico(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select historico from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );

          open;
          Parametro.clear;
          Parametro.add('HIST�RICO');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('historico').asstring);
               next;
          End;
          close;
     End;

end;

procedure TObjPendencia.Get_ListaSaldo(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select saldo from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('SALDO');
          first;
          While not(eof) do
          Begin
               Parametro.add(formata_valor(fieldbyname('saldo').asstring));
               next;
          End;
          close;
     End;

end;


procedure TObjPendencia.Get_ListaNomeBoleto(Parametro: TStrings;Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select Tabpendencia.codigo,Tabpendencia.boletobancario,tabboletobancario.numeroboleto from Tabpendencia');
          SelectSql.add('left join tabboletobancario on tabpendencia.boletobancario=tabboletobancario.codigo');
          SelectSql.add('where tabpendencia.titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('N� BOLETO');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('numeroboleto').asstring);
               next;
          End;
          close;
     End;

end;



procedure TObjPendencia.Get_Listaboleto(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select boletobancario from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('C�DIGO BOLETO');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('BOLETOBANCARIO').asstring);
               next;
          End;
          close;
     End;

end;
procedure TObjPendencia.Get_ListaDuplicata(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select Duplicata from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('DUPLICATA');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('DUPLICATA').asstring);
               next;
          End;
          close;
     End;

end;


procedure TObjPendencia.Get_ListaBoletoaPagar(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select BoletoaPagar from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('BOLETO A PAGAR');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('boletoapagar').asstring);
               next;
          End;
          close;
     End;

end;

procedure TObjPendencia.Get_ListaObservacao(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select Observacao from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('OBSERVA��O');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('observacao').asstring);
               next;
          End;
          close;
     End;

end;

procedure TObjPendencia.Get_ListaValor(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select valor from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('VALOR');
          first;
          While not(eof) do
          Begin
               Parametro.add(formata_valor(fieldbyname('valor').asstring));
               next;
          End;
          close;
     End;

end;

procedure TObjPendencia.Get_ListaVencimento(Parametro: TStrings;
  Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
     If (Parametro=NIl)
     Then exit;

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select vencimento from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          case Saldo_Todas_quitadas of
          'S':SelectSQL.Add('and Tabpendencia.saldo>0');
          'Q':SelectSQL.Add('and Tabpendencia.saldo=0');
          End;
          SelectSql.add('order by Tabpendencia.codigo ' );
          open;
          Parametro.clear;
          Parametro.add('VENCIMENTO');
          first;
          While not(eof) do
          Begin
               Parametro.add(fieldbyname('vencimento').asstring);
               next;
          End;
          close;
     End;

end;


procedure TObjPendencia.RetornaGrid_Receber(Parametro: TstringGrid;Parametrotitulo:string);
begin
    //titulo a receber



    Parametro.Colcount:=1;
    Parametro.cols[0].clear;

    //Self.Get_ListaPendenciaCampos(ParametroCodigo,ParametroHistorico, ParametroVencimento, ParametroValor,
   // ParametroSaldo,ParametroBoleto,nil,ParametroDuplicata,nil,nil,Parametrotitulo);

     If (Parametro=NIl)
     Then exit;

     With Self.ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select tabpendencia.codigo,tabpendencia.historico,tabpendencia.vencimento,tabpendencia.valor,');
          SelectSql.add('tabpendencia.saldo,tabpendencia.boletobancario,tabboletobancario.numeroboleto,tabpendencia.duplicata,');
          SelectSql.add('tabpendencia.observacao from Tabpendencia');
          SelectSql.add('left join tabboletobancario on tabpendencia.boletobancario=tabboletobancario.codigo');
          SelectSql.add('where tabpendencia.titulo='+#39+Parametrotitulo+#39);

          if (ObjParametroGlobal.ValidaParametro('ORDENA PENDENCIAS POR VENCIMENTO NA TELA DE TITULO')=true)
          then Begin
                  if (ObjParametroGlobal.Get_Valor = 'SIM')
                  then SelectSql.add('order by Tabpendencia.vencimento' )
                  else SelectSql.add('order by Tabpendencia.Codigo' );
          end else  SelectSql.add('order by Tabpendencia.vencimento' );

          open;
          if (recordcount=0)
          then exit;

          Parametro.rowcount:=1;
          Parametro.ColCount:=9;

          Parametro.cells[0,0]:='C�DIGO';
          Parametro.cells[1,0]:='HIST�RICO';
          Parametro.cells[2,0]:='VENCIMENTO';
          Parametro.cells[3,0]:='VALOR';
          Parametro.cells[4,0]:='SALDO';
          Parametro.cells[5,0]:='C�D.BOLETO';
          Parametro.cells[6,0]:='N�BOLETO';
          Parametro.cells[7,0]:='DUPLICATA';
          Parametro.cells[8,0]:='OBSERVA��O';

          first;
          While not(eof) do
          Begin
               parametro.rowcount:=parametro.rowcount+1;
               parametro.cells[0,parametro.rowcount-1]:=fieldbyname('codigo').asstring;
               parametro.cells[1,parametro.rowcount-1]:=fieldbyname('historico').asstring;
               parametro.cells[2,parametro.rowcount-1]:=fieldbyname('vencimento').asstring;
               parametro.cells[3,parametro.rowcount-1]:=formata_valor(fieldbyname('valor').asstring);
               parametro.cells[4,parametro.rowcount-1]:=formata_valor(fieldbyname('saldo').asstring);
               parametro.cells[5,parametro.rowcount-1]:=fieldbyname('boletobancario').asstring;
               parametro.cells[6,parametro.rowcount-1]:=fieldbyname('numeroboleto').asstring;
               parametro.cells[7,parametro.rowcount-1]:=fieldbyname('duplicata').asstring;
               parametro.cells[8,parametro.rowcount-1]:=fieldbyname('observacao').asstring;
               next;
          End;
          close;
          Parametro.FixedRows:=1;
     End;
end;


procedure TObjPendencia.Get_ListaLancamentos(ParametroTipoLancto,
  ParametroValor, ParametroHistorico, ParametroData,ParametroCodigo: TStrings;
  ParametroPendencia: string);
var
   ObjLancamentoTemp:TObjlancamento;
begin
     try
        ObjLancamentoTemp:=TobjLancamento.create;
        ObjLancamentoTemp.get_listacodigo(Parametrocodigo,parametropendencia);
        ObjLancamentoTemp.Get_ListaTipoLancto(ParametroTipoLancto,ParametroPendencia);
        ObjLancamentoTemp.Get_ListaValor(ParametroValor,ParametroPendencia);
        ObjLancamentoTemp.Get_ListaHistorico(ParametroHistorico,ParametroPendencia);
        ObjLancamentoTemp.Get_ListaData(ParametroData,ParametroPendencia);
     Finally
            Objlancamentotemp.free;

     End;
end;

function TObjPendencia.Get_QuantRegsLancamento(ParametroPendencia: string): INteger;
var
   ObjLancamentoTemp:TObjlancamento;
begin
     try
        ObjLancamentoTemp:=TobjLancamento.create;
        Result:=ObjLancamentoTemp.Get_QuantRegs(ParametroPendencia);
     Finally
            Objlancamentotemp.free;
     End;

end;

function TObjPendencia.Get_QuantRegs(ParametroTitulo: string): Integer;
begin
     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select count(codigo) as CNT from Tabpendencia where titulo='+#39+Parametrotitulo+#39);
          open;
          first;
          result:=Fieldbyname('CNT').asinteger;
          close;
     End;
end;

function TObjPendencia.Get_QuantRegsComSaldo(ParametroTitulo: string): Integer;
begin
     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select count(codigo) as CNT from Tabpendencia where titulo='+#39+Parametrotitulo+#39+' and saldo>0');
          open;
          first;
          result:=Fieldbyname('CNT').asinteger;
          close;
     End;
end;





function TObjPendencia.AumentaSaldo(ParametroPendencia,
  ParametroValor: string;ComCommit:Boolean): Boolean;
Var
SaldoAtual:Currency;
begin

     If (Self.LocalizaCodigo(ParametroPendencia)=False)
     Then Begin
             Messagedlg('Pend�ncia N�o Localizada Para ser Atualizada!',mterror,[mbok],0);
             If (ComCommit=True)
             Then FDataModulo.IBTransaction.RollbackRetaining;
             result:=False;
             exit;
     End;

     Self.TabelaparaObjeto;
     SaldoAtual:=StrToFloat(Self.Saldo)+StrToFloat(ParametroValor);

     If SaldoAtual<0
     Then Begin
               Messagedlg('Saldo Atual da Pend�ncia N�o pode Ficar Negativo',mterror,[mbok],0);
               If (ComCommit=True)
               Then FDataModulo.IBTransaction.RollbackRetaining;
               Result:=False;
               exit;
     End;

     Self.Status:=dsEdit;
     Self.Saldo:=FloatToStr(Saldoatual);

     If Self.Salvar(ComCommit)=False
     Then Begin
               Messagedlg('O Lan�amento N�o foi conclu�do, Erro na Atualiza��o da Pend�ncia!',mterror,[mbok],0);
               If (ComCommit=True)
               Then FDataModulo.IBTransaction.RollbackRetaining;
               Result:=False;
               exit;
          End
     Else Begin
               If (ComCommit=True)
               Then FDataModulo.IBTransaction.CommitRetaining;
               Result:=True;
     End;
end;

function TObjPendencia.DiminuiSaldo(ParametroPendencia,
  ParametroValor: string;ComCommit:Boolean): Boolean;
var
SaldoAtual:Double;
begin

     If (Self.LocalizaCodigo(ParametroPendencia)=False)
     Then Begin
             Messagedlg('Pend�ncia N�o Localizada Para ser Atualizada!',mterror,[mbok],0);
             If (ComCommit=True)
             Then FDataModulo.IBTransaction.RollbackRetaining;
             result:=False;
             exit;
     End;

     Self.TabelaparaObjeto;
     SaldoAtual:=StrToFloat(Self.Saldo)-StrToFloat(ParametroValor);
     If SaldoAtual<0
     Then Begin

               Messagedlg('Saldo Atual da Pend�ncia N�o pode Ficar Negativo',mterror,[mbok],0);
               If (ComCommit=True)
               Then FDataModulo.IBTransaction.RollbackRetaining;
               result:=False;
               exit;
     End;
     Self.Status:=dsEdit;
     Self.Saldo:=FloatToStr(Saldoatual);
     If Self.Salvar(ComCommit)=False
     Then Begin
               Messagedlg('O Lan�amento N�o foi conclu�do, Erro na Atualiza��o da Pend�ncia!',mterror,[mbok],0);
               If (ComCommit=True)
               Then FDataModulo.IBTransaction.RollbackRetaining;
               Result:=False;
               exit;
     End
     Else Begin
              If (ComCommit=True)
              Then FDataModulo.IBTransaction.CommitRetaining;
              Result:=True;
     End;

end;

function TObjPendencia.ExcluiLancamentos(ParametroPendencia: string;Comcommit:boolean): Boolean;
var
   ObjLancamentoTemp:TObjlancamento;
begin
     try
        ObjLancamentoTemp:=TobjLancamento.create;
        result:=ObjLancamentoTemp.ExcluiLancamentosPendencia(parametropendencia,comcommit);

     Finally
            Objlancamentotemp.free;
     End;


end;


function TObjPendencia.ExcluiSemLancamento(ParametroTitulo: string): Boolean;
var
   ObjLancamentoTemp:TObjlancamento;
begin
     try
        ObjLancamentoTemp:=TobjLancamento.create;

        With Self.ObjDataset do
        Begin
             Close;
             SelectSql.Clear;
             SelectSql.add('Select codigo from Tabpendencia where titulo='+parametroTitulo);
             open;
             If (RecordCount=0)
             Then Begin
                       Messagedlg('N�o H� Pend�ncias para ser Exclu�da!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;

             first;
             While Not(eof) do
             Begin
                  If (ObjLancamentoTemp.Get_QuantRegs(Fieldbyname('codigo').asstring)>0)
                  Then Begin
                        Messagedlg('N�o � poss�vel Excluir a Pend�ncias do T�tulo->'+ParametroTitulo+'  .Pois as mesmas possuem Lan�amentos',mterror,[mbok],0);
                        result:=False;
                        last;
                        exit;
                       End;
                  next;
             End;
       End;



     Finally
            Objlancamentotemp.free;
     End;
     //Caso n�o tenha nenhum lan�amento na pend�ncia

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add(' Delete from TabPendencia where titulo='+ParametroTitulo);
          Try
            ExecSQL;
           Except
                 close;
                 Messagedlg('Erro Ao Tentar Excluir as Pend�ncias do T�tulo'+Parametrotitulo,mterror,[mbok],0);
                 result:=False;
                 exit;
           End;
          close;
     End;

     result:=True;

end;


function TObjPendencia.Get_CodigoCredorDevedorTitulo: string;
begin
     If (Self.Titulo.LocalizaCodigo(Self.titulo.Get_CODIGO)=False)
     Then Begin
               result:='';
               exit;
          End;
     Self.Titulo.TabelaparaObjeto;
     Result:=Self.Titulo.Get_CODIGOCREDORDEVEDOR;
 
end;


function TObjPendencia.Get_CredorDevedorTitulo: string;
begin
     If (Self.Titulo.LocalizaCodigo(Self.titulo.Get_CODIGO)=False)
     Then Begin
               result:='';
               exit;
          End;
     Self.Titulo.TabelaparaObjeto;
     Result:=Self.Titulo.Get_CREDORDEVEDORCODIGO;

end;

function TObjPendencia.Get_GeradorTitulo: string;
begin
     If (Self.Titulo.LocalizaCodigo(Self.titulo.Get_CODIGO)=False)
     Then Begin
               result:='';
               exit;
          End;
     Self.Titulo.TabelaparaObjeto;
     Result:=Self.Titulo.Get_GERADOR;
end;

//Da um lancamento de desconto em todas as pendencias do titulo
//em parametro que possuem vencimento> que o parametro
function TObjPendencia.Desconta_Pendencias_de_Matricula(Parametrotitulo: string;
  ParametroData: string): Boolean;
var
ObjLancamento:TobjLancamento;
QuantLAncTmp:Integer;
QuantLAncGeramValor:Integer;
ObjGeraLancamento:Tobjgeralancamento;
begin
     Try
        ObjLancamento:=Tobjlancamento.create;
        ObjGeraLancamento:=TobjGeraLancamento.create;
     Except
           Messagedlg('Erro ao tentar Criar o Objeto de Lan�amento em Pend�ncias e Gerador de Lan�amento!',mterror,[mbok],0);
           result:=False;
           exit;
     End;
     Try
        If (ObjGeraLancamento.localizacodigo('OBJALUNOTURMA','CANCELA MATRICULA')=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel encontrar o Registro da TabgeraLancamento contendo "OBJALUNOTURMA" e "CANCELA MATRICULA" como dados!',mterror,[mbok],0);
                  result:=False;
                  exit;
             End;
        ObjGeraLancamento.TabelaparaObjeto;
        //PEgo os dados para gerar e gero o lancamento nas pendencias
        //com estes dados, assim elas terao saldo =0
        With Self.ObjDataset do
        Begin
             close;
             SelectSql.clear;
             SelectSql.add('Select codigo,saldo from Tabpendencia where titulo='+parametrotitulo);
             SelectSql.add(' and saldo>0 and vencimento>'+#39+formatdatetime('mm/dd/yyyy',strtodate(parametrodata))+#39);
             open;
             first;

             While not(eof) do
             Begin
                  ObjLancamento.Status:=dsInsert;
                  ObjLancamento.Submit_CODIGO(Objlancamento.Get_NovoCodigo);
                  ObjLancamento.Submit_Pendencia(fieldbyname('codigo').asstring);
                  ObjLancamento.Submit_TipoLancto(ObjGeraLancamento.Get_TipoLAncto);
                  ObjLancamento.Submit_Valor(fieldbyname('saldo').asstring);
                  ObjLancamento.Submit_Historico('CANCELAMENTO DE MATR�CULA');
                  ObjLancamento.Submit_Data(formatdatetime('dd/mm/yyyy',now));
                  If (ObjLancamento.Salvar(False,false,'',False,true)=False)
                  Then Begin
                            Messagedlg('Erro ao tentar Lan�ar desconto na Pend�ncia '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                            Result:=False;
                            exit;
                       End;
                  next;
             End;

        End;


      result:=true;
     Finally
            ObjLancamento.free;
            OBjgeralancamento.free;
     End;


End;


function TObjPendencia.Desconta_Pendencias_de_Matricula(
  Parametrotitulo: string): Boolean;
var
ObjLancamento:TobjLancamento;
QuantLAncTmp:Integer;
QuantLAncGeramValor:Integer;
ObjGeraLancamento:Tobjgeralancamento;
begin
     Try
        ObjLancamento:=Tobjlancamento.create;
        ObjGeraLancamento:=TobjGeraLancamento.create;
     Except
           Messagedlg('Erro ao tentar Criar o Objeto de Lan�amento em Pend�ncias e Gerador de Lan�amento!',mterror,[mbok],0);
           result:=False;
           exit;
     End;
     Try
        If (ObjGeraLancamento.localizacodigo('OBJALUNOTURMA','CANCELA MATRICULA')=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel encontrar o Registro da TabgeraLancamento contendo "OBJALUNOTURMA" e "CANCELA MATRICULA" como dados!',mterror,[mbok],0);
                  result:=False;
                  exit;
             End;
        ObjGeraLancamento.TabelaparaObjeto;
        //PEgo os dados para gerar e gero o lancamento nas pendencias
        //com estes dados, assim elas terao saldo =0
        With Self.ObjDataset do
        Begin
             close;
             SelectSql.clear;
             SelectSql.add('Select codigo,saldo from Tabpendencia where titulo='+parametrotitulo);
             SelectSql.add(' and saldo>0');
             open;
             first;

             While not(eof) do
             Begin
                  ObjLancamento.Status:=dsInsert;
                  ObjLancamento.Submit_CODIGO(Objlancamento.Get_NovoCodigo);
                  ObjLancamento.Submit_Pendencia(fieldbyname('codigo').asstring);
                  ObjLancamento.Submit_TipoLancto(ObjGeraLancamento.Get_TipoLAncto);
                  ObjLancamento.Submit_Valor(fieldbyname('saldo').asstring);
                  ObjLancamento.Submit_Historico('CANCELAMENTO DE MATR�CULA');
                  ObjLancamento.Submit_Data(formatdatetime('dd/mm/yyyy',now));
                  If (ObjLancamento.Salvar(False,false,'',false,true)=False)
                  Then Begin
                            Messagedlg('Erro ao tentar Lan�ar desconto na Pend�ncia '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                            Result:=False;
                            exit;
                       End;
                  next;
             End;
             
        End;


      result:=true;
     Finally
            ObjLancamento.free;
            OBjgeralancamento.free;
     End;
end;                                                                          

//Passo o titulo entre parenteses e uma data, se data='' entaum tenho
//que lancar desconto em tudo,
//o geralancamento "desconto" apenas serve para pegar o c�digo o tipolancamento=desconto
//� gerado um lancamento de desconto no valor total da pendencia com o historico entre parenteses
Function TObjPendencia.LancaDescontoTodos(Parametrotitulo: string;PDataVencimento,PdataLancamento:string;Phistorico:string): Boolean;
var
ObjLancamento:TobjLancamento;
QuantLAncTmp:Integer;
QuantLAncGeramValor:Integer;
ObjGeraLancamento:Tobjgeralancamento;
begin
     Try
        ObjLancamento:=Tobjlancamento.create;
        ObjGeraLancamento:=TobjGeraLancamento.create;
     Except
           Messagedlg('Erro ao tentar Criar o Objeto de Lan�amento em Pend�ncias e Gerador de Lan�amento!',mterror,[mbok],0);
           result:=False;
           exit;
     End;
     Try
        If (ObjGeraLancamento.localizacodigo('OBJTITULO','DESCONTO')=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel encontrar o Registro da TabgeraLancamento contendo "OBJTITULO" e "DESCONTO" como dados!',mterror,[mbok],0);
                  result:=False;
                  exit;
        End;
        ObjGeraLancamento.TabelaparaObjeto;
        //PEgo os dados para gerar e gero o lancamento nas pendencias
        //com estes dados, assim elas terao saldo =0
        With Self.ObjDataset do
        Begin
             close;
             SelectSql.clear;
             SelectSql.add('Select codigo,saldo from Tabpendencia where titulo='+parametrotitulo);
             SelectSql.add(' and saldo>0');
             if (PDataVencimento<>'')
             Then SelectSql.add(' and vencimento<'+#39+FormatDateTime('mm/dd/yyyy',strtodate(Pdatavencimento))+#39);

             open;
             first;

             While not(eof) do
             Begin
                  ObjLancamento.Status:=dsInsert;
                  ObjLancamento.Submit_CODIGO(Objlancamento.Get_NovoCodigo);
                  ObjLancamento.Submit_Pendencia(fieldbyname('codigo').asstring);
                  ObjLancamento.Submit_TipoLancto(ObjGeraLancamento.Get_TipoLAncto);
                  ObjLancamento.Submit_Valor(fieldbyname('saldo').asstring);
                  ObjLancamento.Submit_Historico(Phistorico);
                  ObjLancamento.Submit_Data(PdataLancamento);

                  If (ObjLancamento.Salvar(False,false,'',false,true)=False)
                  Then Begin
                            Messagedlg('Erro ao tentar Lan�ar desconto na Pend�ncia '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                            Result:=False;
                            exit;
                  End;
                  
                  next;
             End;
             
        End;


      result:=true;
     Finally
            ObjLancamento.free;
            OBjgeralancamento.free;
     End;
end;





{
procedure TObjPendencia.Get_ListaCamposPorGerador(Pcodigo, Phistorico,
  PVencimento, PValor, PSaldo: TStrings; PGerador, PcodGerador: string;OpcoesSaldo:Integer);
begin
     //este procedimento ser� utilizado quando por exemplo na mensalidade
     //eu precisar saber todas as mensalidades que este aluno possui
     //como uma matricula pode ter mais de um titulo, entao o mais facil neste
     //caso � pesquisar pelo gerador e se codigo que seria o c�digo da matr�cula

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select Tabpendencia.codigo,Tabpendencia.historico,Tabpendencia.vencimento,Tabpendencia.valor,Tabpendencia.saldo from Tabpendencia');
          SelectSql.add('left join Tabtitulo on tabpendencia.titulo=tabtitulo.codigo where ');
          SelectSql.add('tabtitulo.gerador='+Pgerador+' and codigogerador='+Pcodgerador);
          //USado de acordo com os parametros do RGOpcoesPendencias
          //no Ualunoturma, usado para indicar se quero pendencias
          //com saldo, sem saldo ou todas
          //0 todas
         // 1 quitadas
          //2 nao quitadas
          Case OpcoesSaldo of
                  0:Begin End;
                  1:SelectSQL.add('and Tabpendencia.saldo=0');
                  2:SelectSQL.add('and Tabpendencia.saldo>0');
          End;
          SelectSql.add(' order by vencimento,saldo');


          open;
          Pcodigo.clear;
          Phistorico.clear;
          Pvencimento.clear;
          Pvalor.clear;
          PSaldo.clear;

          PCodigo.add('C�DIGO');
          Phistorico.add('HIST�RICO');
          PVENCIMENTO.add('VENCIMENTO');
          PVALOR.add('VALOR');
          PSALDO.add('SALDO');

          first;
          While not(eof) do
          Begin
               Pcodigo          .add(fieldbyname('codigo').asstring);
               Phistorico       .add(fieldbyname('historico').asstring);
               Pvencimento      .add(fieldbyname('vencimento').asstring);
               Pvalor           .add(formata_valor(fieldbyname('valor').asstring));
               Psaldo           .add(formata_valor(fieldbyname('saldo').asstring));
               next;
          End;
          close;
     End;

end;}

procedure TObjPendencia.Get_ListaCamposPorGerador(Pcodigo, Phistorico,
  PVencimento, PValor, PSaldo: TStrings; PGerador, PcodGerador: string;OpcoesSaldo:Integer;Pdata:string);
begin
     //este procedimento ser� utilizado quando por exemplo na mensalidade
     //eu precisar saber todas as mensalidades que este aluno possui
     //como uma matricula pode ter mais de um titulo, entao o mais facil neste
     //caso � pesquisar pelo gerador e se codigo que seria o c�digo da matr�cula

     With ObjDataset do
     Begin
          close;
          SelectSql.clear;
          SelectSql.add('Select Tabpendencia.codigo,Tabpendencia.historico,Tabpendencia.vencimento,Tabpendencia.valor,Tabpendencia.saldo from Tabpendencia');
          SelectSql.add('left join Tabtitulo on tabpendencia.titulo=tabtitulo.codigo where ');
          SelectSql.add('tabtitulo.gerador='+Pgerador+' and codigogerador='+Pcodgerador);
          //USado de acordo com os parametros do RGOpcoesPendencias
          //no Ualunoturma, usado para indicar se quero pendencias
          //com saldo, sem saldo ou todas
          //0 todas
         // 1 quitadas
          //2 nao quitadas
          Case OpcoesSaldo of
                  0:Begin End;
                  1:SelectSQL.add('and Tabpendencia.saldo=0');
                  2:SelectSQL.add('and Tabpendencia.saldo>0');
          End;
          if (pdata<>'')
          Then SelectSQL.add(' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pdata))+#39);
          SelectSql.add(' order by Tabpendencia.vencimento,Tabpendencia.saldo');


          open;
          Pcodigo.clear;
          Phistorico.clear;
          Pvencimento.clear;
          Pvalor.clear;
          PSaldo.clear;

          PCodigo.add('C�DIGO');
          Phistorico.add('HIST�RICO');
          PVENCIMENTO.add('VENCIMENTO');
          PVALOR.add('VALOR');
          PSALDO.add('SALDO');

          first;
          While not(eof) do
          Begin
               Pcodigo          .add(fieldbyname('codigo').asstring);
               Phistorico       .add(fieldbyname('historico').asstring);
               Pvencimento      .add(fieldbyname('vencimento').asstring);
               Pvalor           .add(formata_valor(fieldbyname('valor').asstring));
               Psaldo           .add(formata_valor(fieldbyname('saldo').asstring));
               next;
          End;
          close;
     End;

end;



//USados no Analice
function TObjPendencia.Get_PesquisaTituloInExterno(
  Ppesquisa: String): boolean;
begin
     result:=False;
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select * from tabpendencia where titulo in');
          SelectSQL.add(PPesquisa);
          SelectSQL.add('and saldo>0 order by titulo,vencimento,saldo');

          Try
             open;
             first;
          Except
                Messagedlg('Erro no parametro da Pesquisa "Get_PesquisaTituloInExterno"',mterror,[mbok],0);
                exit;
          End;
          If (recordcount>0)
          Then Result:=True
          Else Exit;
     End;

end;
function TObjPendencia.Eof: Boolean;
begin
     Result:=Self.ObjDataset.eof;
end;

procedure TObjPendencia.Next;
begin
     Self.ObjDataset.Next;
end;

procedure TObjPendencia.Prior;
begin
     Self.ObjDataset.Prior;
end;
//********************************
destructor TObjPendencia.Free;
begin
     Freeandnil(Self.ObjDataset);
     Freeandnil(Self.ParametroPesquisa);
     Self.Titulo.free;
     Self.Portador.free;
     Self.ObjQlocal.free;
     Self.BoletoBancario.free;
     Self.Duplicata.free;
end;





function TObjPendencia.Get_NumpendenciaTitulo(PTitulo: string): string;
begin
     With Self.Objdataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select (codigo) from Tabpendencia where Titulo='+Ptitulo);
          open;
          Result:=fieldbyname('codigo').asstring;
     End;

end;

function TObjPendencia.Get_saldoporTitulo(Ptitulo: string): Currency;
begin
     With Self.ObjDataset do
     Begin
          result:=0;
          close;
          SelectSQL.clear;
          SelectSQL.add('Select sum(saldo) as SOMA from Tabpendencia where Titulo='+Ptitulo);
          open;
          result:=fieldbyname('soma').asfloat;
     End;
end;

procedure TObjPendencia.lancaboleto_Sem_registro(pnumerotitulo,PcodigoConvenio: string;StrLPendencias:TStringList);
var
  cont:integer;
  temp:string;
  Pinstrucoes:String;
  ptaxabanco,Ptaxamensal,pvaloraodia:Currency;
  pdiasprotesto:integer;
  ObjLancamentoTemp:TobjLancamento;
  TmpQuantConvenio:integer;
begin

  Try
    ObjLancamentoTemp:=TobjLancamento.create;
  Except
    Messagedlg('Erro na tentativa de Cria��o da StringList de Pend�ncias selecionadas!',mterror,[mbok],0);
    exit;
  End;

  Try

    if (Self.titulo.LocalizaCodigo(PNumeroTitulo)=False)
    Then Begin
              Messagedlg('T�tulo n�o localizado',mterror,[mbok],0);
              exit;
    end;
    Self.titulo.TabelaparaObjeto;

    if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
    Then begin
              Messagedlg('Conv�nio n�o localizado!',mterror,[mbok],0);
              exit;
    End;
    Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
    //********************************************************************

    Try
       ptaxabanco:=strtofloat(Self.BoletoBancario.Convenioboleto.Get_taxabanco);
    Except
       PtaxaBanco:=0;
    End;


    //imprimindo boleto a boleto atraves dos registros das pendencias
    for cont:=0 to StrLPendencias.count-1 do
    Begin
         //criando um novo boleto e gravando o nosso numero nesse boleto
         Self.ZerarTabela;
         Self.LocalizaCodigo(StrLPendencias[cont]);
         Self.TabelaparaObjeto;

         Self.BoletoBancario.Status:=dsinsert;
         Self.BoletoBancario.Submit_CODIGO('0');
         Self.BoletoBancario.Submit_Situacao('I');
         Self.BoletoBancario.Submit_NumeroBoleto('');//nosso numero
         Self.BoletoBancario.Submit_Vencimento(Self.VENCIMENTO);
         Self.BoletoBancario.Submit_DataDoc(datetostr(now));
         Self.BoletoBancario.Submit_NumeroDoc('');
         Self.BoletoBancario.Submit_DataProc(datetostr(now));

         //Era no valor que saia, o sadol pediu pra sair pelo saldo
         Self.BoletoBancario.Submit_ValorDoc(floattoStr(strtofloat(Self.Saldo)+ptaxabanco));
         Self.BoletoBancario.Submit_Desconto('');
         Self.BoletoBancario.Submit_OutrasDeducoes('');
         Self.BoletoBancario.Submit_Juros('');
         Self.BoletoBancario.Submit_outrosacrescimos('');
         Self.BoletoBancario.Submit_ValorCobrado('');
         //*******************************************
         Self.BoletoBancario.Convenioboleto.Submit_CODIGO(PcodigoConvenio);
         Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio);
         Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
         //*******************************************
         //as instrucoes possuem variaveis pre-determinada
         //que sao colocadas no texto, aqui apenas eu mando substituir
         //caso o texto as tenha usado ele substitue
         //senao nao tem erro nenhum
         //Ser� Protestado ap�s dia &dataprotesto
         Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;

         try
             //juros
             Ptaxamensal:=strtofloat(Self.BoletoBancario.ConvenioBoleto.get_taxamensal);
         Except
               Ptaxamensal:=0;
         End;

         try
            ptaxamensal:=((strtofloat(Self.VALOR)*Ptaxamensal)/100);
            pvaloraodia:=strtofloat(tira_ponto(formata_valor(Ptaxamensal/30)));
         Except
               pvaloraodia:=0;
         End;

         //taxamensal,Quantdiasprotesto

         Try
            pdiasprotesto:=strtoint(Self.BoletoBancario.Convenioboleto.get_Quantdiasprotesto);
         Except
            pdiasprotesto:=0;
         End;

         Pinstrucoes:=strreplace(pinstrucoes,'&taxamensal',formata_valor(ptaxamensal));
         Pinstrucoes:=strreplace(pinstrucoes,'&valoraodia',formata_valor(pvaloraodia));
         Pinstrucoes:=strreplace(pinstrucoes,'&dataprotesto',datetostr(strtodate(Self.VENCIMENTO)+pdiasprotesto));
         Pinstrucoes:=strreplace(pinstrucoes,'&datavencimento',Self.VENCIMENTO);
         Pinstrucoes:=strreplace(pinstrucoes,'&taxabanco',Formata_valor(PtaxaBanco));

         // Alterado por F�bio 30/06/2009 ordem Saldol
         if (ObjParametroGlobal.ValidaParametro('MOSTRA PROPRIEDADE DO CLIENTE EM BOLETO')=false)
         then
         else if (ObjParametroGlobal.Get_Valor = 'SIM')
              then  Pinstrucoes:=strreplace(pinstrucoes,'&propriedade',Self.RetornaPropriedadeDoCliente(StrLPendencias[Cont]));


         //Rodolfo 29-08-12
         if(Self.Titulo.Get_NotaFiscal <> '')then
         begin
           PInstrucoes := 'Nota Fiscal: '+ Self.Titulo.Get_NotaFiscal + #13 + Pinstrucoes;
         end;

         Self.BoletoBancario.Submit_Instrucoes(Pinstrucoes);

         if (Self.BoletoBancario.Salvar(False,'')=False)
         then begin
                   FDataModulo.IBTransaction.RollbackRetaining;
                   Messagedlg('Erro na tentativa de Criar um novo boleto para pend�ncia '+StrLPendencias[cont],mterror,[mbok],0);
                   exit;
         end;
         //passando o codigo do Boleto para o numero do boleto e numero do documento
         Self.BoletoBancario.Status:=DsEdit;

         if (Self.Boletobancario.GeraNossoNumero=False)//ele gera de acordo com o Banco do Convenio
         Then Begin
                  FDataModulo.IBTransaction.RollbackRetaining;
                  Messagedlg('Erro na tentativa de Gerar O Nosso N�mero do Boleto',mterror,[mbok],0);
                  exit;
         end;

         Self.BoletoBancario.Submit_NumeroDoc(Self.BoletoBancario.Get_CODIGO);

         if (Self.BoletoBancario.Salvar(False,'')=False)
         Then begin
                 Messagedlg('Erro na tentativa de Guardar o N� do Boleto e N� do Doc',mterror,[mbok],0);
                 exit;
         End;

         Self.Status:=dsedit;
         if (Self.Salvar(False)=False)
         Then Begin
                   Messagedlg('Erro na tentativa de Gravar o Numero do boleto na pend�ncia '+StrLPendencias[cont],mterror,[mbok],0);
                   exit;
         End;

         if (ptaxabanco>0)
         Then Begin
                   //lancando o juros para taxa do Boleto
                   temp:='';
                   If (ObjLancamentotemp.LancamentoAutomatico('J',StrLPendencias[cont],floattostr(PtaxaBanco),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',temp,False)=False)
                   Then Begin
                            Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                            exit;
                    End;
         End;

    End;
    FDataModulo.IBTransaction.CommitRetaining;

    for cont:=0 to StrLPendencias.count-1 do
    Begin
         Self.Imprime_Boleto_SemRegistro(StrLPendencias[cont]);
    End;

  Finally
             ObjLancamentoTemp.free;
  End;
End;

//********
procedure TObjPendencia.lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio: string;StrLPendencias,StrLObservacaoAdicional:TStringList);
Begin
     Self.lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio,StrLPendencias,StrLObservacaoAdicional,'V');
End;

procedure TObjPendencia.lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio: string;StrLPendencias,StrLObservacaoAdicional:TStringList;PSaldo_Valor:string);
Begin
     //chama com preview = true
     Self.lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio,StrLPendencias,StrLObservacaoAdicional,PSaldo_Valor,True);
End;

procedure TObjPendencia.lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio: string;StrLPendencias,StrLObservacaoAdicional:TStringList;PSaldo_Valor:string;Ppreview:boolean);
var
cont:integer;
temp:string;
Pinstrucoes:String;
ptaxabanco,Ptaxamensal,pvaloraodia:Currency;
tempint,pdiasprotesto:integer;
ObjLancamentoTemp:TobjLancamento;
TmpQuantConvenio:integer;
begin
     Try
        ObjLancamentoTemp:=TobjLancamento.create;
     Except
        Messagedlg('Erro na tentativa de Cria��o da StringList de Pend�ncias selecionadas!',mterror,[mbok],0);
        exit;
     End;


     Try

        if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
        Then begin
                  Messagedlg('Conv�nio n�o localizado!',mterror,[mbok],0);
                  exit;
        End;
        Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
        //********************************************************************
        
        Try
           ptaxabanco:=strtofloat(Self.BoletoBancario.Convenioboleto.Get_taxabanco);
        Except
           PtaxaBanco:=0;
        End;



        //imprimindo boleto a boleto atraves dos registros das pendencias
        for cont:=0 to StrLPendencias.count-1 do
        Begin
             //criando um novo boleto e gravando o nosso numero nesse boleto
             Self.ZerarTabela;
             Self.LocalizaCodigo(StrLPendencias[cont]);
             Self.TabelaparaObjeto;

             Self.BoletoBancario.Status:=dsinsert;
             Self.BoletoBancario.Submit_CODIGO('0');
             Self.BoletoBancario.Submit_Situacao('I');
             Self.BoletoBancario.Submit_NumeroBoleto('');//nosso numero
             Self.BoletoBancario.Submit_Vencimento(Self.VENCIMENTO);
             Self.BoletoBancario.Submit_DataDoc(datetostr(now));
             Self.BoletoBancario.Submit_NumeroDoc('');
             Self.BoletoBancario.Submit_DataProc(datetostr(now));

             if (PSaldo_Valor='V')
             Then Self.BoletoBancario.Submit_ValorDoc(floattoStr(strtofloat(Self.VALOR)+ptaxabanco))
             Else Self.BoletoBancario.Submit_ValorDoc(floattoStr(strtofloat(Self.Saldo)+ptaxabanco));
             
             Self.BoletoBancario.Submit_Desconto('');
             Self.BoletoBancario.Submit_OutrasDeducoes('');
             Self.BoletoBancario.Submit_Juros('');
             Self.BoletoBancario.Submit_outrosacrescimos('');
             Self.BoletoBancario.Submit_ValorCobrado('');
             //*******************************************
             Self.BoletoBancario.Convenioboleto.Submit_CODIGO(PcodigoConvenio);
             Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio);
             Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
             //*******************************************
             //as instrucoes possuem variaveis pre-determinada
             //que sao colocadas no texto, aqui apenas eu mando substituir
             //caso o texto as tenha usado ele substitue
             //senao nao tem erro nenhum
             //Ser� Protestado ap�s dia &dataprotesto
             Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;

             //Adicionando o complemento da observacao enviado em parametro
             PInstrucoes:=PInstrucoes+#13;
             for tempint:=1 to length(StrLObservacaoAdicional[cont]) do
             Begin
                  if (StrLObservacaoAdicional[cont][tempint]<>'|')
                  then PInstrucoes:=PInstrucoes+StrLObservacaoAdicional[cont][tempint]
                  Else PInstrucoes:=PInstrucoes+#13;
             End;


             try
                 //juros
                 Ptaxamensal:=strtofloat(Self.BoletoBancario.ConvenioBoleto.get_taxamensal);
             Except
                   Ptaxamensal:=0;
             End;

             try
                ptaxamensal:=((strtofloat(Self.VALOR)*Ptaxamensal)/100);
                pvaloraodia:=strtofloat(tira_ponto(formata_valor(Ptaxamensal/30)));
             Except
                   pvaloraodia:=0;
             End;

             //taxamensal,Quantdiasprotesto

             Try
                pdiasprotesto:=strtoint(Self.BoletoBancario.Convenioboleto.get_Quantdiasprotesto);
             Except
                pdiasprotesto:=0;
             End;

             Pinstrucoes:=strreplace(pinstrucoes,'&taxamensal',formata_valor(ptaxamensal));
             Pinstrucoes:=strreplace(pinstrucoes,'&valoraodia',formata_valor(pvaloraodia));
             Pinstrucoes:=strreplace(pinstrucoes,'&dataprotesto',datetostr(strtodate(Self.VENCIMENTO)+pdiasprotesto));
             Pinstrucoes:=strreplace(pinstrucoes,'&datavencimento',Self.VENCIMENTO);
             Pinstrucoes:=strreplace(pinstrucoes,'&taxabanco',Formata_valor(PtaxaBanco));
             Self.BoletoBancario.Submit_Instrucoes(Pinstrucoes);
             Self.BoletoBancario.Submit_ObservacoesSuperior('');


             if (Self.BoletoBancario.Salvar(False,'')=False)
             then begin
                       FDataModulo.IBTransaction.RollbackRetaining;
                       Messagedlg('Erro na tentativa de Criar um novo boleto para pend�ncia '+StrLPendencias[cont],mterror,[mbok],0);
                       exit;
             end;
             //passando o codigo do Boleto para o numero do boleto e numero do documento
             Self.BoletoBancario.Status:=DsEdit;

             if (Self.Boletobancario.GeraNossoNumero=False)
             Then Begin
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Messagedlg('Erro na tentativa de Gerar O Nosso N�mero do Boleto',mterror,[mbok],0);
                      exit;
             end;

             Self.BoletoBancario.Submit_NumeroDoc(Self.BoletoBancario.Get_CODIGO);

             if (Self.BoletoBancario.Salvar(False,'')=False)
             Then begin
                     Messagedlg('Erro na tentativa de Guardar o N� do Boleto e N� do Doc',mterror,[mbok],0);
                     exit;
             End;

             Self.Status:=dsedit;
             if (Self.Salvar(False)=False)
             Then Begin
                       Messagedlg('Erro na tentativa de Gravar o Numero do boleto na pend�ncia '+StrLPendencias[cont],mterror,[mbok],0);
                       exit;
             End;

             if (ptaxabanco>0)
             Then Begin
                       //lancando o juros para taxa do Boleto
                       temp:='';
                       If (ObjLancamentotemp.LancamentoAutomatico('J',StrLPendencias[cont],floattostr(PtaxaBanco),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',temp,False)=False)
                       Then Begin
                                Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                                exit;
                        End;
             End;

        End;
        FDataModulo.IBTransaction.CommitRetaining;

        for cont:=0 to StrLPendencias.count-1 do
        Begin
             Self.Imprime_Boleto_SemRegistro(StrLPendencias[cont],ppreview);
        End;

     Finally
                 ObjLancamentoTemp.free;
     End;
End;

Function TobjPendencia.Imprime_Boleto_SemRegistro(Ppendencia:string):Boolean;
Begin
     result:=Self.Imprime_Boleto_SemRegistro(Ppendencia,true);
End;

Function TobjPendencia.Imprime_Boleto_SemRegistro(Ppendencia:string;Ppreview:boolean):Boolean;
Begin
     result:=self.Imprime_Boleto_SemRegistro(Ppendencia,Ppreview,1);
End;

Function TobjPendencia.Imprime_Boleto_SemRegistro(Ppendencia:string;Ppreview:boolean;Modelo:integer):Boolean;
var
  objboletoSR:TObjBoletoSemRegistro;
Begin
     result:=False;

     Self.ZerarTabela;
     if (Self.LocalizaCodigo(Ppendencia)=False)
     Then Begin
               Messagedlg('Pend�ncia N� '+ppendencia+' n�o encontrada',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     if (Self.BoletoBancario.Get_CODIGO='')
     Then Begin
               Messagedlg('Essa pend�ncia n�o possui boleto ligado a ela',mterror,[mbok],0);
               exit;
     End;

     if (Self.BoletoBancario.Convenioboleto.Get_PreImpresso='S')
     then Begin
               Messagedlg('N�o � poss�vel imprimir novamente boletos pr�-impressos',mterror,[mbok],0);
               exit;
     End;

     Try
        objboletoSR:=TObjBoletoSemRegistro.create;
     Except
           Messagedlg('Erro na tentativa de Criar o Objeto OBJETOBOLETOSR',mterror,[mbok],0);
           exit;
     End;

     try

        objboletoSR.ZerarCampos;
        With objboletoSR do
        Begin
            NomeBanco:=Self.BoletoBancario.Convenioboleto.Get_NomeBanco;
            CodigoBAnco:=Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco;
            DgBanco:=Self.BoletoBancario.Convenioboleto.Get_DgBanco;
            LocalPagamento:=Self.BoletoBancario.Convenioboleto.Get_LocalPagamento;
            Nomecedente:=Self.BoletoBancario.Convenioboleto.Get_Nomecedente;
            Carteira:=Self.BoletoBancario.Convenioboleto.Get_Carteira;
            CompCarteira:=Self.BoletoBancario.Convenioboleto.Get_CompCarteira;
            Agencia:=Self.BoletoBancario.Convenioboleto.Get_Agencia;
            DGAgencia:=Self.BoletoBancario.Convenioboleto.Get_DGAgencia;
            ContaCorrente:=Self.BoletoBancario.Convenioboleto.Get_ContaCorrente;
            DgContaCorrente:=Self.BoletoBancario.Convenioboleto.Get_DgContaCorrente;
            numeroconvenio:=Self.BoletoBancario.Convenioboleto.Get_numeroconvenio;
            EspecieDoc:=Self.BoletoBancario.Convenioboleto.Get_EspecieDoc;
            Aceite:=Self.BoletoBancario.Convenioboleto.Get_Aceite;
            NumContaResponsavel:=Self.BoletoBancario.Convenioboleto.Get_NumContaResponsavel;
            EspecieMoeda:=Self.BoletoBancario.Convenioboleto.Get_EspecieMoeda;
            QuantidadeMoeda:=Self.BoletoBancario.Convenioboleto.Get_QuantidadeMoeda;
            ValorMoeda:=Self.BoletoBancario.Convenioboleto.Get_ValorMoeda;
            codigocedente:=Self.BoletoBancario.Convenioboleto.get_codigocedente;
            QtdeDigitosNossoNumero:=strtoint(Self.BoletoBancario.Convenioboleto.get_QtdeDigitosNossoNumero);
            CodigoCarteira_caixa:=Self.BoletoBancario.Convenioboleto.Get_CodigoCarteira_Caixa;
            CodigoSICOB_Caixa:=Self.BoletoBancario.Convenioboleto.Get_CodigoSICOB_caixa;
            codigotransacao_unibanco:=Self.BoletoBancario.Convenioboleto.Get_codigotransacao_unibanco;
            posicoes_28_a_29_UNIBANCO:=Self.BoletoBancario.Convenioboleto.Get_posicoes_28_a_29_UNIBANCO;
            numerocliente_UNIBANCO:=Self.BoletoBancario.Convenioboleto.Get_numerocliente_UNIBANCO;
            posicao20_sicred    :=Self.BoletoBancario.Convenioboleto.Get_posicao20_sicred;
            posicao21_sicred    :=Self.BoletoBancario.Convenioboleto.Get_posicao21_sicred;
            posicao31_34_sicred :=Self.BoletoBancario.Convenioboleto.Get_posicao31_34_sicred;
            posicao35_36_sicred :=Self.BoletoBancario.Convenioboleto.Get_posicao35_36_sicred;
            posicao37_41_sicred :=Self.BoletoBancario.Convenioboleto.Get_posicao37_41_sicred;
            posicao42_sicred    :=Self.BoletoBancario.Convenioboleto.Get_posicao42_sicred;
            posicao43_sicred    :=Self.BoletoBancario.Convenioboleto.Get_posicao43_sicred;

            tipoidentificador_HSBC :=Self.BoletoBancario.Convenioboleto.Get_tipoidentificador_HSBC;
            codigocedente_HSBC := Self.BoletoBancario.Convenioboleto.Get_codigocedente_HSBC;
            codigoCNR_HSBC :=Self.BoletoBancario.Convenioboleto.Get_codigoCNR_HSBC;


            ObservacoesSuperior:=Self.BoletoBancario.Get_ObservacoesSuperior;


            //*******************************************SACADO***********************************************************
            FisicajuridicaSacado:='F';
            NomeSacado    :=Self.titulo.CREDORDEVEDOR.Get_NomeCredorDevedor     (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
            CpfSacado     :=Self.titulo.CREDORDEVEDOR.Get_CPFCNPJCredorDevedor  (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
            RgSacado      :=Self.titulo.CREDORDEVEDOR.Get_RGIECredorDevedor     (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);

            If(UsaEnderecoEnderecoCobrancaGlobal =false)
            Then Begin
                  EnderecoSacado:=Self.titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedor (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
                  BairroSacado  :=Self.titulo.CREDORDEVEDOR.Get_BairroCredorDevedor   (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);;
                  CepSacado     :=Self.titulo.CREDORDEVEDOR.Get_CepCredorDevedor      (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);;
                  CidadeSacado  :=Self.titulo.CREDORDEVEDOR.Get_CidadeCredorDevedor   (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
                  EstadoSacado  :=Self.titulo.CREDORDEVEDOR.Get_EstadoCredorDevedor   (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);;
                  numerocasasacado:=Self.titulo.CREDORDEVEDOR.Get_NumeroCasaCredorDevedor(Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
            End
            Else Begin
                  EnderecoSacado:=Self.titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedorCobranca (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
                  BairroSacado  :=Self.titulo.CREDORDEVEDOR.Get_BairroCredorDevedorCobranca   (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);;
                  CepSacado     :=Self.titulo.CREDORDEVEDOR.Get_CepCredorDevedorCobranca      (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);;
                  CidadeSacado  :=Self.titulo.CREDORDEVEDOR.Get_CidadeCredorDevedorCobranca   (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
                  EstadoSacado  :=Self.titulo.CREDORDEVEDOR.Get_EstadoCredorDevedorCobranca   (Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);;
                  numerocasasacado:=Self.titulo.CREDORDEVEDOR.Get_NumeroCasaCredorDevedorCobranca(Self.titulo.CREDORDEVEDOR.Get_CODIGO,Self.titulo.get_CODIGOCREDORDEVEDOR);
            End;

            //***********************************************************************************************************

            NossoNumero_partedois:=Self.BoletoBancario.Get_NumeroBoleto;

            Vencimento:=strtodate(Self.BoletoBancario.get_vencimento);
            DataDoc:=Self.BoletoBancario.Get_datadoc;
            NumeroDoc:=Self.BoletoBancario.Get_numerodoc;
            DataProc:=Self.BoletoBancario.Get_dataproc;
            ValorDoc:=Self.BoletoBancario.Get_ValorDoc;
            Desconto:=Self.BoletoBancario.Get_Desconto;
            OutrasDeducoes:=Self.BoletoBancario.Get_OutrasDeducoes;
            Juros:=Self.BoletoBancario.Get_Juros;
            outrosacrescimos:=Self.BoletoBancario.Get_OutrosAcrescimos;
            ValorCobrado:=Self.BoletoBancario.Get_ValorCobrado;
            
            Instrucoes.Clear;
            Instrucoes.text:=Self.BoletoBancario.Get_Instrucoes;
            ImprimeBoleto(ppreview,modelo);
            result:=True;
        End;
     Finally
            objboletoSR.free;
     End;
End;

procedure TObjPendencia.lancaboleto_PreImpresso(pnumerotitulo,PcodigoConvenio: string; StrLPendencias: TStringList);
var
cont:integer;
StrBoleto:TStringList;
Saltar:boolean;
ObjLancamentoTemp:TobjLancamento;
pvalormensal,ptaxamensal,pvaloraodia,PTaxaBanco:Currency;
pdiasprotesto:integer;
PInstrucoes:String;
PcodigoLanctoJuros:string;
begin
     {Esse procedimento � usado para gravar os dados em um boleto pre-impresso e depois imprimir

     If (Pnumerotitulo='')
     Then Begin
               Messagedlg('Escolha um t�tulo antes de Tentar lan�ar os Boletos!',mtinformation,[mbok],0);
               exit;
     End;}


     if (PnumeroTitulo<>'')
     then Begin
               If (Self.Titulo.LocalizaCodigo(pnumerotitulo)=False)
               Then Begin
                         messagedlg('T�tulo n�o localizado!',mterror,[mbok],0);
                         exit;
               End;
               Self.Titulo.TabelaparaObjeto;
               
               If (Self.Titulo.CONTAGERENCIAL.get_tipo='D')
               Then Begin
                         Messagedlg('N�o � poss�vel enviar um boleto para uma Conta a Pagar',mtinformation,[mbok],0);
                         exit;
               End;
     End;

     if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
     then Begin
               Messagedlg('Conv�nio n�o localizado',mterror,[mbok],0);
               exit;
     End;
     Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;

     With Self.ObjQlocal do
     Begin
          Try
            StrBoleto:=TStringList.create;
          Except
                Messagedlg('Erro na tentativa de Cria��o da StringList de Pend�ncias selecionadas!',mterror,[mbok],0);
                exit;
          End;

          Try
             ObjLancamentotemp:=TObjLancamento.Create;
          except
             Freeandnil(StrBoleto);
             Messagedlg('Erro na tentativa de Criar o ObjLancamentoTEMP',mterror,[mbok],0);
             exit;
          End;


          Try
             Self.BoletoBancario.ZerarTabela;
             Self.BoletoBancario.Resgatanumeroboletos(StrLPendencias,StrBoleto,PcodigoConvenio);
             If (StrBoleto.count=0)
             Then Begin
                       Messagedlg('Sem escolher os Boletos n�o � poss�vel continuar!',mterror,[mbok],0);
                       FdataModulo.IBTransaction.RollbackRetaining;
                       exit;
             End;

             for cont:=0 to StrLPendencias.count-1 do
             Begin
             //***********************************************************
                  Self.ZerarTabela;
                  if (Self.LocalizaCodigo(StrLPendencias[cont])=False)
                  Then Begin
                          Messagedlg('Pend�ncia n�o localizada!',mtinformation,[mbok],0);
                          exit;
                  End;
                  Self.TabelaparaObjeto;

                  if (Self.Titulo.CONTAGERENCIAL.Get_Tipo='D')
                  Then Begin
                            Messagedlg('N�o � poss�vel gerar um boleto para uma conta a pagar'+#13+'Pend�ncia : '+Self.codigo,mtinformation,[mbok],0);
                            exit;
                  End;


                  if (Self.BoletoBancario.get_codigo<>'')
                  Then Begin
                            Messagedlg('Essa pend�ncia j� possui o Boleto '+Self.boletobancario.get_codigo+' n�o � poss�vel imprimir outro boleto para ela!',mtinformation,[mbok],0);
                            exit;
                  End;
                  
                  if (Self.BoletoBancario.LocalizaCodigo(StrBoleto[cont])=False)
                  Then Begin
                            Messagedlg('Boleto n�o localizado!',mtinformation,[mbok],0);
                            exit;
                  End;

                  Self.BoletoBancario.TabelaparaObjeto;
                  if (Self.BoletoBancario.Get_Situacao<>'N')
                  Then Begin
                            messagedlg('O boleto '+StrBoleto[cont]+' n�o pode ser usado!',mtinformation,[mbok],0);
                            exit;
                  End;
                  //como a pendencia ja esta com o numero do boleto correto
                  //� s� editar e salvar
                  Self.Status:=dsedit;
                  If (Self.Salvar(false)=False)
                  Then Begin
                            messagedlg('Erro na tentativa de Atualizar a Pend�ncia '+Self.codigo+' com o boleto '+Self.boletobancario.get_codigo,mterror,[mbok],0);
                            exit;
                 End;

                 //*******GRAVANDO AS INFORMACOES NO BOLETO******

                 //Adicionando o valor da taxa do boleto no saldo da pendencia como juros
                 Try
                    PTaxaBanco:=strtofloat(Self.BoletoBancario.Convenioboleto.Get_taxabanco);
                 Except
                    PTaxaBanco:=0;
                 End;

                 //as instrucoes possuem variaveis pre-determinada
                 //que sao colocadas no texto, aqui apenas eu mando substituir
                 //caso o texto as tenha usado ele substitue
                 //senao nao tem erro nenhum
                 //Ser� Protestado ap�s dia &dataprotesto
                 Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;

                 try
                     //juros
                     Ptaxamensal:=strtofloat(Self.BoletoBancario.ConvenioBoleto.get_taxamensal);
                 Except
                       Ptaxamensal:=0;
                 End;
    
                 try
                    PvalorMensal:=((strtofloat(Self.VALOR)*Ptaxamensal)/100);
                    pvaloraodia:=strtofloat(tira_ponto(formata_valor(Pvalormensal/30)));
                 Except
                       pvaloraodia:=0;
                 End;
    
                 //taxamensal,Quantdiasprotesto
                 Try
                    pdiasprotesto:=strtoint(Self.BoletoBancario.Convenioboleto.get_Quantdiasprotesto);
                 Except
                    pdiasprotesto:=0;
                 End;

                 Pinstrucoes:=strreplace(pinstrucoes,'&taxamensal',formata_valor(ptaxamensal));
                 Pinstrucoes:=strreplace(pinstrucoes,'&valoraodia',formata_valor(pvaloraodia));
                 Pinstrucoes:=strreplace(pinstrucoes,'&dataprotesto',datetostr(strtodate(Self.VENCIMENTO)+pdiasprotesto));
                 Pinstrucoes:=strreplace(pinstrucoes,'&datavencimento',Self.VENCIMENTO);
                 Pinstrucoes:=strreplace(pinstrucoes,'&taxabanco',Formata_valor(PtaxaBanco));

                 //Rodolfo 29-08-12
                 if(Self.Titulo.Get_NotaFiscal <> '')then
                 begin
                   PInstrucoes := 'Nota Fiscal: ' + Self.Titulo.Get_NotaFiscal + #13 + Pinstrucoes;
                 end;

                 //****************************************************************
                 Self.BoletoBancario.Submit_Instrucoes(Pinstrucoes);
                 Self.BoletoBancario.Status:=dsedit;
                 Self.BoletoBancario.Submit_Situacao('I');
                 Self.BoletoBancario.Submit_ValorDoc(floattostr(Strtofloat(Self.Get_VALOR)+PTaxaBanco));
                 Self.BoletoBancario.Submit_Vencimento(Self.VENCIMENTO);
                 Self.BoletoBancario.Submit_DataProc(datetostr(now));
                 Self.BoletoBancario.Submit_DataDoc(datetostr(now));
                 Self.BoletoBancario.Submit_NumeroDoc(Self.Titulo.Get_CODIGO);

                 If (Self.BoletoBancario.Salvar(False,'')=False)
                 Then Begin
                            messagedlg('Erro na tentativa de Atualizar o Boleto '+Self.boletobancario.get_codigo+' para impresso!',mterror,[mbok],0);
                            exit;
                 End;

                 if (PTaxaBanco>0)
                 Then Begin
                           PcodigoLanctoJuros:='';
                           If (ObjLancamentotemp.LancamentoAutomatico('J',StrLPendencias[cont],floattostr(PTaxaBanco),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',PcodigoLanctoJuros,False)=False)
                           Then Begin
                                     Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                                     exit;
                           End;
                 End;
             //***********************************************************
             End;//for STRLPENDENCIAS
             FdataModulo.IBTransaction.CommitRetaining;


             try
                //imprimindo os boletos

                if (FConfiguraBoleto.AbrePosicoesIni(Self.boletobancario.Convenioboleto.Get_CodigoBanco)=False)
                Then exit;


                FDataModulo.ComponenteRdPrint.abrir;

                if (FDataModulo.ComponenteRdPrint.setup=False)
                then begin
                         FDataModulo.ComponenteRdPrint.fechar;
                         exit;
                End;

                Try
                    Saltar:=False;
                    for cont:=0 to StrLPendencias.count-1 do
                    Begin
                         Self.ZerarTabela;
                         Self.LocalizaCodigo(StrLPendencias[cont]);
                         Self.TabelaparaObjeto;
                         Self.ImprimeBoletoPreImpresso(saltar);
                         saltar:=True;
                    End;
                    
                Finally
                  FDataModulo.ComponenteRdPrint.fechar;
                End;
             Except
                   Messagedlg('Erro na tentativa de imprimir os Boletos!',mterror,[mbok],0);
                   exit;
             End;

             Messagedlg('Gera��o Conclu�da com Sucesso!',mtInformation,[mbok],0);
          Finally
                freeandnil(StrBoleto);
                ObjLancamentoTemp.Free;
                FDataModulo.IBTransaction.RollbackRetaining;
          End;
     End;
end;

Function  TobjPendencia.lancaboleto_PreImpresso_VariasPendencias(PcodigoConvenio:string;StrPend,PStrValorPend:TStringList;PvalorBoleto:Currency):boolean;
var
cont:integer;
PVencimento:TDate;
Pboleto:string;
Pcodigobanco,PnumeroBoleto:string;
ObjLancamentotmp:tobjlancamento;
Pvalormensal,pvaloraodia,ptaxamensal,Ptaxabanco:Currency;
temp:string;
Pinstrucoes:String;
pdiasprotesto:integer;
begin
     result:=False;

     //Esse procedimento � usado para gravar os dados em um boleto pre-impresso e depois imprimir
     if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
     then Begin
               Messagedlg('Conv�nio n�o localizado',mterror,[mbok],0);
               exit;
     End;
     Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
     Pcodigobanco:=Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco;

     Try
        ObjLancamentotmp:=TObjLancamento.Create;
     except
        Messagedlg('Erro na tentativa de Criar o ObjLancamento para Lan�ar o juro referente a taxa dos Boletos',mterror,[mbok],0);
        exit;
     End;

Try



     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          edtGrupo01.EditMask:=MascaraData;
          LbGrupo01.caption:='Vencimento Boleto';
          LbGrupo02.caption:='Boleto';
          edtgrupo02.OnKeyDown:=Self.edtBoletoNaoUsadoKeyDown;

          showmodal;

          if (Tag=0)
          then exit;

          Try
             Pvencimento:=strtodate(edtgrupo01.text);
          Except
                Messagedlg('Vencimento Inv�lido',mterror,[mbok],0);
                exit;
          End;

             strtoint(edtgrupo02.text);

             if (Self.BoletoBancario.LocalizaCodigo(edtgrupo02.text)=False)
             then Begin
                       Messagedlg('Boleto n�o encontrado',mterror,[mbok],0);
                       exit;
             End;
             Self.BoletoBancario.TabelaparaObjeto;
             Pboleto:=Self.boletobancario.get_codigo;
     End;//Filtro Imp

     Try

        if (Self.BoletoBancario.Convenioboleto.get_codigo<>PcodigoConvenio)
        Then Begin
                  Messagedlg('O Conv�nio do Boleto escolhido n�o � o mesmo',mterror,[mbok],0);
                  exit;
        end;

        if (Self.BoletoBancario.Get_Situacao<>'N')
        then Begin
                  Messagedlg('A situa��o do boleto escolhido deve ser n�o usado',mterror,[mbok],0);
                  exit;
        End;

        //*******GRAVANDO AS INFORMACOES NO BOLETO******

        Try
           PTaxaBanco:=strtofloat(Self.BoletoBancario.Convenioboleto.Get_taxabanco);
        Except
           PTaxaBanco:=0;
        End;

        //as instrucoes possuem variaveis pre-determinada
        //que sao colocadas no texto, aqui apenas eu mando substituir
        //caso o texto as tenha usado ele substitue
        //senao nao tem erro nenhum
        //Ser� Protestado ap�s dia &dataprotesto
        Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;

        try
            //juros
            Ptaxamensal:=strtofloat(Self.BoletoBancario.ConvenioBoleto.get_taxamensal);
        Except
              Ptaxamensal:=0;
        End;

        try
           PvalorMensal:=((pValorBoleto*Ptaxamensal)/100);
           pvaloraodia:=strtofloat(tira_ponto(formata_valor(Pvalormensal/30)));
        Except
              pvaloraodia:=0;
        End;

        //taxamensal,Quantdiasprotesto
        Try
           pdiasprotesto:=strtoint(Self.BoletoBancario.Convenioboleto.get_Quantdiasprotesto);
        Except
           pdiasprotesto:=0;
        End;

        Pinstrucoes:=strreplace(pinstrucoes,'&taxamensal',formata_valor(ptaxamensal));
        Pinstrucoes:=strreplace(pinstrucoes,'&valoraodia',formata_valor(pvaloraodia));
        Pinstrucoes:=strreplace(pinstrucoes,'&dataprotesto',datetostr(PVencimento+pdiasprotesto));
        Pinstrucoes:=strreplace(pinstrucoes,'&datavencimento',datetostr(PVENCIMENTO));
        Pinstrucoes:=strreplace(pinstrucoes,'&taxabanco',Formata_valor(PtaxaBanco));
        //****************************************************************
        Self.BoletoBancario.Submit_Instrucoes(Pinstrucoes);
        Self.BoletoBancario.Status:=dsedit;
        Self.BoletoBancario.Submit_Situacao('I');
        Self.BoletoBancario.Submit_ValorDoc(floattostr(PvalorBoleto+PTaxaBanco));
        Self.BoletoBancario.Submit_Vencimento(Datetostr(PVencimento));
        Self.BoletoBancario.Submit_DataProc(datetostr(now));
        Self.BoletoBancario.Submit_DataDoc(datetostr(now));
        Self.BoletoBancario.Submit_NumeroDoc('LT');
        If (Self.BoletoBancario.Salvar(False,'')=False)
        Then Begin
                    messagedlg('Erro na tentativa de Atualizar o Boleto '+Self.boletobancario.get_codigo+' para impresso!',mterror,[mbok],0);
                    FDataModulo.IBTransaction.RollbackRetaining;
                    exit;
        End;

     Except
           Messagedlg('Boleto inv�lido',mterror,[mbok],0);
           exit;
     End;

     //colocando o numero do boleto nas pendencias
     for cont:=0 to StrPend.count-1 do
     Begin
           Self.ZerarTabela;
           Self.LocalizaCodigo(StrPend[cont]);
           Self.TabelaparaObjeto;
           Self.Status:=dsedit;
           Self.BoletoBancario.Submit_CODIGO(Pboleto);
           if (Self.Salvar(False)=False)
           Then Begin
                     Messagedlg('Erro na tentativa de Gravar o Numero do boleto na pend�ncia '+StrPend[cont],mterror,[mbok],0);
                     exit;
           End;

           if (strtofloat(Self.Saldo)<strtofloat(PStrValorPend[cont]))//foi colocado juros
           Then Begin
                     //lancando a diferenca como Juros
                     temp:='';
                     If (ObjLancamentotmp.LancamentoAutomatico('J',StrPend[cont],floattostr((strtofloat(PStrValorPend[cont])-strtofloat(Self.Saldo))),datetostr(now),'REFERENTE JUROS NO LAN�AMENTO DO BOLETO',temp,False)=False)
                     Then Begin
                               Messagedlg('N�o foi poss�vel lan�ar os Juros na Pend�ncia N� '+StrPend[cont],mterror,[mbok],0);
                               exit;
                     End;
           End;
     End;

     //lancar a taxa do boleto apenas na ultima pendencia
     if (PtaxaBanco<>0)
     Then Begin
               temp:='';
               If (ObjLancamentotmp.LancamentoAutomatico('J',StrPend[strpend.count-1],floattostr(PtaxaBanco),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',temp,False)=False)
               Then Begin
                         Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                         exit;
               End;
     End;

     FdataModulo.IBTransaction.CommitRetaining;

     Try
             //imprimindo os boletos

             if (FConfiguraBoleto.AbrePosicoesIni(PcodigoBanco)=False)
             Then exit;

             FDataModulo.ComponenteRdPrint.abrir;
             Try
               if (FDataModulo.ComponenteRdPrint.setup=False)
               then begin
                        FDataModulo.ComponenteRdPrint.fechar;
                        exit;
               End;
               Self.LocalizaCodigo(StrPend[0]);
               Self.TabelaparaObjeto;
               Self.ImprimeBoletoPreImpresso(false);
             Finally
              FDataModulo.ComponenteRdPrint.fechar;
             End;
     Except
           Messagedlg('Erro na tentativa de impress�o dos Boletos!',mterror,[mbok],0);
           exit;
     End;

     Messagedlg('Gera��o Conclu�da com Sucesso!',mtInformation,[mbok],0);

finally
       ObjLancamentotmp.Free;
End;

end;

Procedure TObjPendencia.ImprimeBoletoPreImpresso(Saltar:boolean);
var
PSacado,Pinstrucoes:TStringList;
cont:integer;
ValorTXT:string;
temp:String;
Begin
    //este procedimento apenas envia para o rdprint as informacoes
    //do boleto, ele so funciona porque antes de chamado
    //foi chamado os procedimentos abrir e setup do rdprint
    //e apos enviar tudo chama-se o fechar para dar inicio
    //a impressao ou a visualizacao

    Try
      PSacado:=TStringList.create;
      PInstrucoes:=TStringList.create;
    Except
          Messagedlg('Erro na tentativa de Criar as StringLists Psacado e Pinstrucoes',mterror,[mbok],0);
          exit;
    End;

Try
     {
     '&&CNPJ'+#13+
     '&&CODIGOCLIENTE'+#13+
     '&&NOMECLIENTE'+#13+
     '&&ENDERECO'+#13+
     '&&NUMEROCASA'+#13+
     '&&BAIRRO'+#13+
     '&&CIDADE'+#13+
     '&&ESTADO'+#13+
     '&&CEP';
     }

     PInstrucoes.clear;
     PSacado.clear;

     Temp:=Self.BoletoBancario.Convenioboleto.Get_sacado;

     temp:=strreplace(temp,'&CNPJ',Self.Titulo.CredorDevedor.Get_CPFCNPJCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
     temp:=strreplace(temp,'&CODIGOCLIENTE',Self.titulo.Get_CODIGOCREDORDEVEDOR);
     temp:=strreplace(temp,'&NOMECLIENTE',Self.Titulo.CredorDevedor.Get_RazaoSocialCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));

     If(UsaEnderecoEnderecoCobrancaGlobal=false)
     Then Begin
          temp:=strreplace(temp,'&ENDERECO',Self.Titulo.CredorDevedor.Get_EnderecoCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&NUMEROCASA',Self.Titulo.CredorDevedor.Get_NumeroCasaCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&BAIRRO',Self.Titulo.CredorDevedor.Get_BairroCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&CIDADE',Self.Titulo.CredorDevedor.Get_CidadeCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&ESTADO',Self.Titulo.CredorDevedor.Get_EstadoCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&CEP',Self.Titulo.CredorDevedor.Get_CepCredorDevedor(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
     End
     Else Begin
          temp:=strreplace(temp,'&ENDERECO',Self.Titulo.CredorDevedor.Get_EnderecoCredorDevedorCobranca(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&NUMEROCASA',Self.Titulo.CredorDevedor.Get_NumeroCasaCredorDevedorCobranca(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&BAIRRO',Self.Titulo.CredorDevedor.Get_BairroCredorDevedorCobranca(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&CIDADE',Self.Titulo.CredorDevedor.Get_CidadeCredorDevedorCobranca(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&ESTADO',Self.Titulo.CredorDevedor.Get_EstadoCredorDevedorCobranca(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
          temp:=strreplace(temp,'&CEP',Self.Titulo.CredorDevedor.Get_CepCredorDevedorCobranca(Self.titulo.credordevedor.get_codigo,Self.titulo.Get_CODIGOCREDORDEVEDOR));
     End;
     Psacado.text:=Temp;

     With FConfiguraBoleto do
     Begin
           if (saltar=True)
           then FdataModulo.ComponenteRdPrint.novapagina;  // Nao salta pagina no primeiro boleto

           if (lbvencimento.ParentShowHint=True)//Propriedade usada indicar se vai imprimir ou naum
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(LbVencimento.top/5))),strtoint(floattostr(int(lbvencimento.left/5))),completapalavra(formatdatetime('dd/mm/yy',strtodate(Self.BoletoBancario.get_VENCIMENTO)),strtoint(floattostr(int(lbvencimento.Tag))),' '));

           if (LbDataEmissao.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(LbDataEmissao.top/5))),strtoint(floattostr(int(LbDataEmissao.left/5))),completapalavra(Self.BoletoBancario.Get_DataProc,strtoint(floattostr(int(LbDataEmissao.Tag))),' '));//data

           if   (LbNumeroDocumento.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbnumerodocumento.top/5))),strtoint(floattostr(int(Lbnumerodocumento.left/5))),completapalavra(Self.BoletoBancario.Get_NumeroDoc,strtoint(floattostr(int(Lbnumerodocumento.Tag))),' '));

           if (LbParcela.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbparcela.top/5))),strtoint(floattostr(int(LbParcela.left/5))),completapalavra(Self.Get_Historico,strtoint(floattostr(int(LbParcela.Tag))),' '));
           
           valortxt := formatfloat('###,###,###,##0.00',strtofloat(Self.BoletoBancario.Get_ValorDoc));
           
           if (LbValor.ParentShowHint=True)
           Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbvalor.top/5))),strtoint(floattostr(int(Lbvalor.left/5))),CompletaPalavra_a_Esquerda(valortxt,strtoint(floattostr(int(Lbvalor.Tag))),' '));

           //informacoes complementares
           if (LbInstrucoes.ParentShowHint=True)
           Then Begin
                   Pinstrucoes.Text:=Self.BoletoBancario.Get_Instrucoes;
                   
                   for cont:=0 to Pinstrucoes.Count-1 do
                   Begin
                        FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbinstrucoes.top/5)))+cont,strtoint(floattostr(int(Lbinstrucoes.left/5))),CompletaPalavra(Pinstrucoes[cont],strtoint(floattostr(int(Lbinstrucoes.Tag))),' '));
                   End;
           End;

           //Sacado
           for cont:=0 to Psacado.Count-1 do
           Begin
                if (LbSacado.ParentShowHint=True)
                Then FdataModulo.ComponenteRdPrint.imp(strtoint(floattostr(int(Lbsacado.top/5)))+cont,strtoint(floattostr(int(Lbsacado.left/5))),CompletaPalavra(Psacado[cont],strtoint(floattostr(int(Lbsacado.Tag))),' '));
           End;
     End;

Finally

       freeandnil(psacado);
       freeandnil(Pinstrucoes);
End;

end;

function TObjPendencia.SalvaInformacaoBoleto(Ppendencia, PBoleto: string;ComCommit: boolean): boolean;
var
ObjLancamentotemp:TObjLancamento;
pvalor:Currency;
pcodigo:string;
begin
      //passa o codigo do boleto para a pendencia
      //e configura o boleto para ficar como impresso
      if (Self.LocalizaCodigo(Ppendencia)=False)
      Then Begin

                Messagedlg('Pend�ncia n�o localizada!',mtinformation,[mbok],0);
                result:=False;
                exit;
      End;
      Self.TabelaparaObjeto;
      if (Self.BoletoBancario.get_codigo<>'')
      Then Begin
                Messagedlg('Essa pend�ncia j� possui o Boleto '+Self.boletobancario.get_codigo+' n�o � poss�vel imprimir outro boleto para ela!',mtinformation,[mbok],0);
                result:=False;
                exit;
      End;

      if (Self.BoletoBancario.LocalizaCodigo(pboleto)=False)
      Then Begin
                Messagedlg('Boleto n�o localizado!',mtinformation,[mbok],0);
                result:=False;
                exit;
      End;
      Self.BoletoBancario.TabelaparaObjeto;
      if (Self.BoletoBancario.Get_Situacao<>'N')
      Then Begin
                messagedlg('Esse boleto n�o pode ser usado!',mtinformation,[mbok],0);
                result:=False;
                exit;
      End;

      //como a pendencia ja esta com o numero do boleto correto
      //� s� editar e salvar
      Self.Status:=dsedit;
      If (Self.Salvar(false)=False)
      Then Begin
                messagedlg('Erro na tentativa de Atualizar a Pend�ncia '+Self.codigo+' com o boleto '+Self.boletobancario.get_codigo,mterror,[mbok],0);
                If (ComCommit=True)
                Then FDataModulo.IBTransaction.RollbackRetaining;
                result:=False;
                exit;
     End;
     //passar o boleto para Impresso
     Self.BoletoBancario.Status:=dsedit;
     Self.BoletoBancario.Submit_Situacao('I');
     Self.BoletoBancario.Submit_ValorDoc(Self.Get_VALOR);
     Self.BoletoBancario.Submit_Vencimento(Self.VENCIMENTO);


     If (Self.BoletoBancario.Salvar(False,'')=False)
     Then Begin
                messagedlg('Erro na tentativa de Atualizar o Boleto '+Self.boletobancario.get_codigo+' para impresso!',mterror,[mbok],0);
                If (ComCommit=True)
                Then FDataModulo.IBTransaction.RollbackRetaining;
                result:=False;
                exit;
     End;
     //Adicionando o valor da taxa do boleto no saldo da pendencia como juros

     Try
        Pvalor:=strtofloat(Self.BoletoBancario.Convenioboleto.Get_taxabanco);
     Except
        Pvalor:=0;
     End;

     if (Pvalor>0)
     Then Begin
               Try
                   ObjLancamentotemp:=TObjLancamento.Create;
               except
                     Messagedlg('Erro na tentativa de Criar o ObjLancamentoTEMP',mterror,[mbok],0);
                     exit;
               End;

               Try
                  Pcodigo:='';
                  If (ObjLancamentotemp.LancamentoAutomatico('J',ppendencia,floattostr(pvalor),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',Pcodigo,False)=False)
                  Then Begin
                            Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                            exit;
                  End;
               finally
                      ObjLancamentotemp.Free;
               end;
     End;

     If (ComCommit=True)
     Then FDataModulo.IBTransaction.CommitRetaining;
     Result:=True;
end;


function TObjPendencia.RetornaCampoCodigo: string;
begin
     Result:='Codigo';
end;

function TObjPendencia.RetornaCampoNome: string;
begin
     Result:='';
end;


procedure TObjPendencia.CancelaBoleto(ParametroPendencia: string);
var
PcodigoBoleto:string;
begin
     If (parametropendencia='')
     Then Begin
               Messagedlg('Escolha uma Pend�ncia',mterror,[mbok],0);
               exit;
     End;
     If (Self.LocalizaCodigo(ParametroPendencia)=False)
     Then Begin
               Messagedlg('Pend�ncia N� '+ParametroPendencia+' n�o localizada!',mterror,[mbok],0);
               exit;
     End;

     If (ObjPermissoesUsoGlobal.ValidaPermissao('CANCELA BOLETO BANCARIO')=FALSE)
     THEN EXIT;


     If (Messagedlg('Certeza que deseja Cancelar este Boleto?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.TabelaparaObjeto;
     If (Self.BoletoBancario.get_codigo='')
     Then Begin
               Messagedlg('Esta pend�ncia n�o est� ligada a nenhum Boleto!',mtinformation,[mbok],0);
               exit;
     End;

     PcodigoBoleto:=Self.BoletoBancario.get_codigo;
     Self.BoletoBancario.Status:=dsedit;
     Self.BoletoBancario.Submit_Situacao('R');//rasurada
     Self.BoletoBancario.Submit_Historico('Boleto Rasurado, N� Pend�ncia '+Self.codigo+',T�tulo '+Self.Titulo.get_codigo);
     If (Self.BoletoBancario.Salvar(false,'')=false)
     Then Begin
               Messagedlg('Erro na tentativa de Rasurar o Boleto N� '+Self.BoletoBancario.Get_codigo,mterror,[mbok],0);
               FdataModulo.IBTransaction.RollbackRetaining;
               exit;
     End;
     Self.BoletoBancario.Submit_CODIGO('');
     Self.Status:=dsedit;
     If (Self.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Limpar o Boleto da Pend�ncia  N� '+Self.Codigo,mterror,[mbok],0);
               FdataModulo.IBTransaction.RollbackRetaining;
               exit;
     End;

     //Passando o campo boletobancario e todas as demais pend�ncia que tenham esse numero de boleto
     //para vazio

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Update TabPendencia set BoletoBancario=null where boletobancario='+PcodigoBoleto);
          Try
            execsql;
          Except
                on e:exception do
                Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Erro na tentativa de editar a pend�ncias do boleto atual'+#13+E.message,mterror,[mbok],0);
                    exit;
                End;
          End;
     End;

     FdataModulo.IBTransaction.CommitRetaining;
     Messagedlg('Boleto Cancelado!'+#13+'Exclua o lan�amento de Juros Referente a Taxa do banco manualmente',mtinformation,[mbok],0);

end;


procedure TObjPendencia.ExcluiDuplicata(PPendencia: string);
Var
      PCodigoDuplicata:string;
begin
     If (PPendencia='')
     Then Begin
               Messagedlg('Escolha uma Pend�ncia',mterror,[mbok],0);
               exit;
     End;
     If (Self.LocalizaCodigo(PPendencia)=False)
     Then Begin
               Messagedlg('Pend�ncia N� '+PPendencia+' n�o localizada!',mterror,[mbok],0);
               exit;
     End;

     If (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR DUPLICATA')=FALSE)
     THEN EXIT;


     If (Messagedlg('Certeza que deseja Excluir esta Duplicata?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;


     Self.TabelaparaObjeto;
     If (Self.Duplicata.get_codigo='')
     Then Begin
               Messagedlg('Esta pend�ncia n�o est� ligada a nenhuma Duplicata!',mtinformation,[mbok],0);
               exit;
     End;

     PCodigoDuplicata:=Self.Duplicata.get_codigo;

     //Passando o campo duplicata  da pend�ncia que tenham esse numero de duplicata
     //para vazio

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Update TabPendencia set Duplicata=null where Duplicata='+PCodigoDuplicata);
          Try
            execsql;
          Except
                on e:exception do
                Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Erro na tentativa de editar a pend�ncias da Duplicata atual'+#13+E.message,mterror,[mbok],0);
                    exit;
                End;
          End;
     End;
     //excluir a duplicata da tabduplicata
     If(Self.Duplicata.exclui(PCodigoDuplicata,false)=false)
     Then Exit;
     FdataModulo.IBTransaction.CommitRetaining;
     MensagemAviso('Duplicata Excluida com sucesso!');
     //Messagedlg('Boleto Cancelado!'+#13+'Exclua o lan�amento de Juros Referente a Taxa do banco manualmente',mtinformation,[mbok],0);

end;

Procedure Tobjpendencia.EscolheDuplicatas(parametro:string;LancaIndividualTituloAtual:Boolean);
var
duptemp,data1,data2:string;
cont,CredorDevedorTemp,codigoCredorDevedorTemp:integer;
StrDuplicata:TStringList;
NUMTITULOESCOLHIDO:string;
modeloduplicata:integer;
begin
     //primeiro escolho o credor/devedor
     //depois listo as pendencias para escolher quais entrar�o na Duplicata
     limpaedit(Ffiltroimp);

     if (parametro<>'')//veio algum titulo junto
     Then Begin
               if (Self.Titulo.LocalizaCodigo(parametro)=False)
               Then NUMTITULOESCOLHIDO:=''
               Else NUMTITULOESCOLHIDO:=parametro;
     End
     Else NUMTITULOESCOLHIDO:='';

     If (Numtituloescolhido='')
     Then Begin
             With FfiltroImp do
             Begin
                  DesativaGrupos;
                  Grupo01.enabled:=true;
                  Grupo02.enabled:=true;
                  Grupo07.Enabled:=True;
                  lbGrupo01.caption:='Emiss�o Inicial';
                  lbGrupo02.caption:='Emiss�o Final';
                  edtGrupo01.EditMask:='!99/99/9999;1;_';
                  edtGrupo02.EditMask:='!99/99/9999;1;_';
                  LbGrupo07.caption:='Credor/Devedor';
                  ComboGrupo07.Items.clear;
                  Combo2Grupo07.Items.clear;
                  Self.Titulo.Get_ListacredorDevedor(ComboGrupo07.items);
                  Self.Titulo.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
                  edtgrupo07.onkeydown:=Self.Titulo.edtcodigocredordevedor_SPV_KeyDown;
                  showmodal;
                  If tag=0
                  Then exit;
                  //Conferindo o que foi lancado
                  Try
                        Strtodate(edtgrupo01.text);
                        Data1:=edtgrupo01.text;
                        Strtodate(edtgrupo02.text);
                        Data2:=edtgrupo02.text;
                  Except
                        data1:='';
                        data2:='';
                  End;

                  Try
                        CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
                  Except
                        Messagedlg('� necess�rio escolher um Devedor!',mterror,[mbok],0);
                        exit;
                  End;

                  Try
                     CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
                  Except
                      Messagedlg('� necess�rio escolher um Devedor!',mterror,[mbok],0);
                      exit;
                  End;
             End;//with das opcoes
     End;//if (numtiutloescolhido='')

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('Select Tabpendencia.* from tabpendencia');
          SelectSql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add('where tabcontager.tipo=''C''');
          if (NUMTITULOESCOLHIDO='')
          Then Begin
                  if (data1<>'')
                  Then SelectSql.add(' and (Tabtitulo.emissao>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(data1))+#39+' and Tabtitulo.emissao<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(data2))+#39+')' );
                  SelectSQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));
                  SelectSQL.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));
          End
          Else SelectSQL.add(' and TabPendencia.Titulo='+NUMTITULOESCOLHIDO);
          SelectSQL.add(' and Tabpendencia.Saldo>0 and Tabpendencia.duplicata is null');
          SelectSql.add('order by tabpendencia.vencimento');
          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;

          last;
          //preenchendo uma lista de pendencias a serem escolhidas
          FescolhePendencias.grid.RowCount:=recordcount+1;
          first;
          cont:=1;
          FescolhePendencias.Grid.ColCount:=6;
          FescolhePendencias.Grid.Cells[0,0]:='Marca';
          FescolhePendencias.Grid.Cells[1,0]:='C�digo';
          FescolhePendencias.Grid.Cells[2,0]:='Hist�rico';
          FescolhePendencias.Grid.Cells[3,0]:='Vencimento';
          FescolhePendencias.Grid.Cells[4,0]:='Valor';
          FescolhePendencias.Grid.Cells[5,0]:='Saldo';
          while not(eof) do
          Begin
               FescolhePendencias.Grid.Cells[1,cont]:=fieldbyname('codigo').asstring;
               FescolhePendencias.Grid.Cells[2,cont]:=fieldbyname('historico').asstring;
               FescolhePendencias.Grid.Cells[3,cont]:=fieldbyname('vencimento').asstring;
               FescolhePendencias.Grid.Cells[4,cont]:=formata_valor(fieldbyname('Valor').asstring);
               FescolhePendencias.Grid.Cells[5,cont]:=formata_valor(fieldbyname('saldo').asstring);
               inc(cont,1);
               next;
          End;
          AjustaLArguraColunaGrid(FescolhePendencias.Grid);

          //If (LancaIndividualTituloAtual=False)
          //Then
          FescolhePendencias.showmodal;
{          Else Begin//neste caso, naum preciso escolher as pendencias
                  //porque � para imprimir uma duplicata para cada pend�ncia do
                  //t�tulo escolhido
                  //entaum marca a coluna zero como se tivesse escolhido
                  for cont:=1 to FescolhePendencias.Grid.rowcount-1 do
                  Begin
                          Fescolhependencias.Grid.cells[0,cont]:='X'
                  End;
          End;}

          //voltou, tenho que ver quais ficaram marcadas e delas gerar a duplicata

          Try
            StrDuplicata:=TStringList.create;
          Except
                Messagedlg('Erro na tentativa de Cria��o da StringList de Pend�ncias selecionadas!',mterror,[mbok],0);
                exit;
          End;
          Try
                  //passando o codigo das que ficaram com X para uma STringLIst
                  StrDuplicata.clear;
                  for cont:=1 to FescolhePendencias.Grid.rowcount-1 do
                  Begin
                          If (Fescolhependencias.Grid.cells[0,cont]='X')//marcada para gerar boleto
                          Then StrDuplicata.add(Fescolhependencias.Grid.cells[1,cont]);
                  End;
                  If (StrDuplicata.count=0)
                  Then Begin
                            Messagedlg('Nenhuma pend�ncia foi escolhida para gerar a Duplicata!',mtinformation,[mbok],0);
                            exit;
                  End;


                  If (ObjParametroGlobal.ValidaParametro('MODELO DE DUPLICATA')=FALSE)
                  Then Begin
                               Messagedlg('O Par�metro "MODELO DE DUPLICATA" n�o foi encontrado!',mterror,[mbok],0);
                               exit;
                  End;

                  Try
                        ModeloDuplicata:=strtoint(ObjParametroGlobal.Get_Valor);
                  Except
                        ModeloDuplicata:=1;//happy kids
                        Messagedlg('O Valor do Par�metro "MODELO DE DUPLICATA" est� incorreto, ser� usado o Modelo N� 01!',mterror,[mbok],0);
                  End;

                  Case ModeloDuplicata of
                          1:begin
                                  FRDPRINT.RDprint.TamanhoQteLinhas:=36;
                                  FRDPRINT.RDprint.TamanhoQteColunas:=80;
                                  FRDPRINT.RDprint.UsaGerenciadorImpr:=True;
                                  FRDPRINT.RDprint.TamanhoQteLPP:=Seis;
                                  FRDPRINT.RDprint.OpcoesPreview.Preview:=true;
                                  FRDPRINT.RDprint.abrir;
                          End;
                          2:begin
                                  FRDPRINT.RDprint.TamanhoQteLinhas:=33;
                                  FRDPRINT.RDprint.TamanhoQteColunas:=80;
                                  FRDPRINT.RDprint.UsaGerenciadorImpr:=True;
                                  FRDPRINT.RDprint.TamanhoQteLPP:=Seis;
                                  FRDPRINT.RDprint.OpcoesPreview.Preview:=true;
                                  FRDPRINT.RDprint.abrir;
                          End;

                          3:
                          Else Begin
                                    Messagedlg('O Modelo de Duplicata N� '+inttostr(modeloduplicata)+' n�o foi configurado no Sistema!',mterror,[mbok],0);
                                    exit;
                          End;
                  End;

                  If (LancaIndividualTituloAtual=False)
                  Then Begin
                          //ja tenho as duplicatas escolhidos
                          //crio uma nova duplicata
                          Self.Duplicata.ZerarTabela;
                          DupTemp:=Self.Duplicata.Get_NovoCodigo;
                          Self.Duplicata.Submit_CODIGO(duptemp);
                          Self.Duplicata.Status:=dsinsert;
                          Self.Duplicata.Submit_Sequencia('1/1');
                          //Feito por Fabio - O Sadol mandou colocar o codigo do pedido na duplicata e na nota fiscal
                          //porem eu vou pegar o camop NUNDCTO do Titulo
                          Self.Duplicata.Submit_ObservacaoPedido(RetornaObservacaoPedido(parametro)); // Parametro � o codigo do titulo


                          If (Self.Duplicata.Salvar(False)=False)
                          Then Begin
                                    Messagedlg('N�o foi poss�vel Criar uma Nova Duplicata!',mterror,[mbok],0);
                                    FdataModulo.IBTransaction.RollbackRetaining;
                                    FRDPRINT.RDprint.Abortar;
                                    exit;
                          End;

                          for cont:=0 to StrDuplicata.count-1 do
                          Begin
                               Self.ZerarTabela;
                               Self.LocalizaCodigo(strduplicata[cont]);
                               Self.TabelaparaObjeto;
                               Self.Status:=dsedit;
                               Self.Duplicata.submit_codigo(duptemp);
                               If (Self.Salvar(False)=False)
                               Then Begin
                                    Messagedlg('N�o foi poss�vel Alterar a Pend�ncia com o C�digo da Duplicata!',mterror,[mbok],0);
                                    FdataModulo.IBTransaction.RollbackRetaining;
                                    FRDPRINT.RDprint.Abortar;
                                    exit;
                               End;
                          End;
                          //imprimindo a duplicata
                          Case modeloduplicata of
                                1:begin
                                        If (Self.ImprimeDuplicataModelo01(DupTemp,False)=False)
                                        Then Begin
                                                  Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                                                  FdataModulo.IBTransaction.RollbackRetaining;
                                                  FRDPRINT.RDprint.Abortar;
                                                  exit;
                                        End;
                                End;
                                2:begin
                                        If (Self.ImprimeDuplicataModelo02(DupTemp,False)=False)
                                        Then Begin
                                                  Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                                                  FdataModulo.IBTransaction.RollbackRetaining;
                                                  FRDPRINT.RDprint.Abortar;
                                                  exit;
                                        End;
                                End;
                          End;//case
                  End
                  Else Begin
                            //neste caso para cada pendencia � uma duplicata
                            //UTILIZAR ESTA PARTE NO PEDIDO PRA CRIAR A DUPLICATA DIRETAMENTE
                            for cont:=0 to StrDuplicata.count-1 do
                            Begin
                                 //ja tenho as pendencias escolhidos
                                 //crio uma nova duplicata para cada pendencia
                                Self.Duplicata.ZerarTabela;
                                DupTemp:=Self.Duplicata.Get_NovoCodigo;
                                Self.Duplicata.Submit_CODIGO(duptemp);
                                Self.Duplicata.Status:=dsinsert;
                                Self.Duplicata.Submit_Sequencia(inttostr(cont+1)+'/'+inttostr(StrDuplicata.count));

                                If (Self.Duplicata.Salvar(False)=False)
                                Then Begin
                                        Messagedlg('N�o foi poss�vel Criar uma Nova Duplicata!',mterror,[mbok],0);
                                        FdataModulo.IBTransaction.RollbackRetaining;
                                        exit;
                                End;
                                Self.ZerarTabela;
                                Self.LocalizaCodigo(strduplicata[cont]);
                                Self.TabelaparaObjeto;
                                Self.Status:=dsedit;
                                Self.Duplicata.submit_codigo(duptemp);
                                If (Self.Salvar(False)=False)
                                Then Begin
                                        Messagedlg('N�o foi poss�vel Alterar a Pend�ncia com o C�digo da Duplicata!',mterror,[mbok],0);
                                        FdataModulo.IBTransaction.RollbackRetaining;
                                        FRDPRINT.RDprint.Abortar;
                                        exit;
                                End;
                                //imprimindo a duplicata
                                Case modeloduplicata of
                                  1:begin
                                         if (cont=0)
                                         Then Begin
                                                If (Self.ImprimeDuplicataModelo01(DupTemp,False)=False)
                                                Then Begin
                                                          Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                                                          FdataModulo.IBTransaction.RollbackRetaining;
                                                          FRDPRINT.RDprint.Abortar;
                                                          exit;
                                                End;
                                         End
                                         Else Begin
                                                If (Self.ImprimeDuplicataModelo01(DupTemp,True)=False)
                                                Then Begin
                                                          Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                                                          FdataModulo.IBTransaction.RollbackRetaining;
                                                          FRDPRINT.RDprint.Abortar;
                                                          exit;
                                                End;
                                         End;
                                         FRdPrint.Rdprint.fechar;
                                  end;
                                2:begin
                                         if (cont=0)
                                         Then Begin
                                                If (Self.ImprimeDuplicataModelo02(DupTemp,False)=False)
                                                Then Begin
                                                          Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                                                          FdataModulo.IBTransaction.RollbackRetaining;
                                                          FRDPRINT.RDprint.Abortar;
                                                          exit;
                                                End;
                                         End
                                         Else Begin
                                                If (Self.ImprimeDuplicataModelo02(DupTemp,True)=False)
                                                Then Begin
                                                          Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                                                          FdataModulo.IBTransaction.RollbackRetaining;
                                                          FRDPRINT.RDprint.Abortar;
                                                          exit;
                                                End;
                                         End;
                                         FRdPrint.Rdprint.fechar;
                                  end;//2

                                3: Begin
                                     //Self.ImprimeDuplicataReportBuilder(StrDuplicata[cont]);
                                     {alterado jonas 23092010 }
                                     Self.ImprimeDuplicataReportBuilder(duptemp);
                                end;

                                End;//case
                            End;//for
                  End;//else
                  //para concluir a impressao
                  FDataModulo.IBTransaction.CommitRetaining;
                  Messagedlg('Processo Finalizado com Sucesso!',mtinformation,[mbok],0);

         Finally
                 freeandnil(StrDuplicata);
         End;
         
     End;
End;


Function  TObjPendencia.ImprimeDuplicataModelo01(PcodigoDuplicata: string;RecuaFolhaAntesdeImprimir:boolean):boolean;
var
CredorDevedorTMP,CodigoCredorDevedorTmp:string;
PCOMPLEMENTO,extenso1,extenso2:string;
valorcurrency:Currency;
Pimprimependencia:boolean;
begin

     Pimprimependencia:=true;
     if (ObjParametroGlobal.ValidaParametro('IMPRIME NUMERO DA PENDENCIA NA DUPLICATA MODELO 1')=true)
     THEN Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               Then Pimprimependencia:=False;
     End;
     

     //Modelo Usado na Happy Kids
     //Neste modelo somente uma pend�ncia � mostrada na Duplicata
     //por isso n�o � poss�vel Imprimir v�rias nela
     result:=False;
     with Self.ObjQlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('Select codigo from Tabpendencia where Duplicata='+PCodigoDuplicata);
          open;
          if (Recordcount=0)
          Then Begin
               Messagedlg('N�o � poss�vel Imprimir a Duplicata '+PcodigoDuplicata+', nenhuma pend�ncia esta associada a ela!',mterror,[mbok],0);
               exit;
          End;
          last;
          If (Recordcount>1)
          Then Begin
                    Messagedlg('Este Modelo de Duplicata n�o permite a Impress�o de v�rias Pend�ncias na mesma Duplicata!'
                    +#13+'Escolha outro modelo ou imprima uma Duplicata para cada pend�ncia!',mterror,[mbok],0);
                    exit;
          End;
          first;
          Self.LocalizaCodigo(fieldbyname('codigo').asstring);
          Self.TabelaparaObjeto;
     End;
     //resgatando as configuracoes
     ObjConfiguraRelGlobal.RecuperaArquivoRelatorio(FImpDuplicata,'IMPRIMEDUPLICATA01');
     //imprimindo

     With FRDPRINT.RDprint do
     Begin
           if (RecuaFolhaAntesdeImprimir=True)
           then novapagina;
           // Nao salta pagina no primeiro boleto
           //todos os labels sao convertidos dividindo por 15 na linha e 10 na coluna
           //isso para poder usar no rdprint

           //dataemissao
           imp(strtoint(floattostr(int(FImpDuplicata.dataemissao.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataemissao.left/10))),datetostr(now));
           //dataaceite
           imp(strtoint(floattostr(int(FImpDuplicata.dataaceite.Top/15))),strtoint(floattostr(int(FImpDuplicata.dataaceite.left/10))),datetostr(now));
           //ValorFatura
           imp(strtoint(floattostr(int(FImpDuplicata.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorFatura.left/10))),formata_valor(Self.Saldo));
           //NumeroFatura,  uso o NUmerodaPendencia|NUmDcto do Titulo, que geralmente sera na venda Pedido/NF, e em outros casos depende do usuario que criou
           if (PimprimePendencia=True)
           then Pcomplemento:=Self.Codigo+'|'
           else pcomplemento:='';

           imp(strtoint(floattostr(int(FImpDuplicata.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata.NumeroFatura.left/10))),Pcomplemento+'-'+Self.Titulo.Get_NUMDCTO);


           //ValorDuplicata=ValorFatura, usado o Saldo
           imp(strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.ValorDuplicata.left/10))),formata_valor(Self.Saldo));
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata.numeroduplicata.left/10))),Self.Duplicata.Get_CODIGO+' - '+Self.Duplicata.Get_Sequencia);
           //NotaFiscal
           imp(strtoint(floattostr(int(FImpDuplicata.notafiscal.Top/15))),strtoint(floattostr(int(FImpDuplicata.notafiscal.left/10))),Self.Titulo.Get_NotaFiscal);
           //vencimento
           imp(strtoint(floattostr(int(FImpDuplicata.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata.vencimento.left/10))),Self.VENCIMENTO);
           //descontode             \
           //ate                     \  Estes dois naum foram usados ainda
           //condicoes especiais
           imp(strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.Top/15))),strtoint(floattostr(int(FImpDuplicata.condicoesespeciais.left/10))),Self.Titulo.PRAZO.Get_Nome);
           //estas informacoes do SACADO utilizam o Objeto Credor Devedor
           //guardo as informacoes essenciais para todos, para diminuir o tamanho do comando
           CredorDevedorTMP:=Self.Titulo.CredorDevedor.Get_codigo;
           CodigoCredorDevedorTmp:=Self.titulo.Get_CODIGOCREDORDEVEDOR;
           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata.nome.left/10))),Self.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));


           If(UsaEnderecoEnderecoCobrancaGlobal=false)
            Then Begin
                  //Endereco do Sacado
                  imp(strtoint(floattostr(int(FImpDuplicata.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata.endereco.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Cidade
                  imp(strtoint(floattostr(int(FImpDuplicata.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata.cidade.left/10))),Self.Titulo.CREDORDEVEDOR.Get_cidadeCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Cep
                  imp(strtoint(floattostr(int(FImpDuplicata.cep.Top/15))),strtoint(floattostr(int(FImpDuplicata.cep.left/10))),Self.Titulo.CREDORDEVEDOR.Get_cepCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //estado
                  imp(strtoint(floattostr(int(FImpDuplicata.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata.estado.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EstadoCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
            End
            Else Begin
                  //Endereco do Sacado
                  imp(strtoint(floattostr(int(FImpDuplicata.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata.endereco.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Cidade
                  imp(strtoint(floattostr(int(FImpDuplicata.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata.cidade.left/10))),Self.Titulo.CREDORDEVEDOR.Get_CidadeCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Cep
                  imp(strtoint(floattostr(int(FImpDuplicata.cep.Top/15))),strtoint(floattostr(int(FImpDuplicata.cep.left/10))),Self.Titulo.CREDORDEVEDOR.Get_CepCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //estado
                  imp(strtoint(floattostr(int(FImpDuplicata.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata.estado.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EstadoCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
            End;

           //PracaPagamento
           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata.cnpj.left/10))),Self.Titulo.CREDORDEVEDOR.Get_cpfcnpjCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata.ie.left/10))),Self.Titulo.CREDORDEVEDOR.Get_RGIECredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
           //valorextenso
           ValorCurrency:=strtofloat(Self.saldo);
           ValorExtenso_DivididoDois(valorcurrency,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso2.left/10))),extenso2);


           result:=True;
     End;

End;


Function  TObjPendencia.ImprimeDuplicataModelo02(PcodigoDuplicata: string;RecuaFolhaAntesdeImprimir:boolean):boolean;
var
tmpnumeroordem,CredorDevedorTMP,CodigoCredorDevedorTmp:string;
extenso1,extenso2:string;
valorcurrency:Currency;
cont:integer;
pcomplemento,LinhaPedidos:String;
ultimovencimento:string;
Linhapedido1a8:Vetor1a8de35;
Pimprimependencia:boolean;
begin
     //Modelo Usado na Tecmac
     //Neste modelo � poss�vel 08 pend�ncias na mesma duplicata


     Pimprimependencia:=true;
     if (ObjParametroGlobal.ValidaParametro('IMPRIME NUMERO DA PENDENCIA NA DUPLICATA MODELO 2')=true)
     THEN Begin
               if (ObjParametroGlobal.get_valor<>'SIM')
               Then Pimprimependencia:=False;
     End;


     result:=False;
     with Self.ObjQlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('Select Tabpendencia.codigo,Tabpendencia.saldo,tabpendencia.duplicata,tabpendencia.vencimento,');
          SQL.add('tabtitulo.notafiscal from Tabpendencia');
          SQL.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SQL.add('where Duplicata='+PCodigoDuplicata);
          sql.add('order by Tabpendencia.vencimento');
          open;
          if (Recordcount=0)
          Then Begin
               Messagedlg('N�o � poss�vel Imprimir a Duplicata '+PcodigoDuplicata+', nenhuma pend�ncia esta associada a ela!',mterror,[mbok],0);
               exit;
          End;
          If (RecordCount>8)
          Then Begin
                    Messagedlg('Neste Modelo de Duplicata s� pode ser impresso 8 Duplicatas',mterror,[mbok],0);
                    exit;
          End;
          last;
          ultimovencimento:=fieldbyname('vencimento').asstring;
          first;
          Self.LocalizaCodigo(fieldbyname('codigo').asstring);
          Self.TabelaparaObjeto;
          If (RecordCount=1)
          Then tmpnumeroordem:=Self.Historico
          Else tmpnumeroordem:='--';

          linhapedidos:='';
          valorcurrency:=0;
          cont:=1;
          While not(eof) do
          Begin

               if (PimprimePendencia=True)
               then Pcomplemento:='Pend. '+fieldbyname('codigo').asstring
               else pcomplemento:='';

               if (fieldbyname('notafiscal').asstring<>'')
               Then Linhapedido1a8[cont]:=pcomplemento+' '+fieldbyname('notafiscal').asstring+' '+formata_valor(fieldbyname('saldo').asstring)
               Else Linhapedido1a8[cont]:=pcomplemento+' '+formata_valor(fieldbyname('saldo').asstring);

               valorcurrency:=valorcurrency+fieldbyname('saldo').asfloat;
               inc(cont,1);
               next;
          End;

     End;

     //resgatando as configuracoes
     ObjConfiguraRelGlobal.RecuperaArquivoRelatorio(FImpDuplicata02,'IMPRIMEDUPLICATA02');
     //imprimindo

     With FRDPRINT.RDprint do
     Begin
           if (RecuaFolhaAntesdeImprimir=True)
           then novapagina;  // Nao salta pagina no primeiro boleto
           //todos os labels sao convertidos dividindo por 15 na linha e 10 na coluna
           //isso para poder usar no rdprint
           //dataemissao
           imp(strtoint(floattostr(int(FImpDuplicata02.emissao.Top/15))),strtoint(floattostr(int(FImpDuplicata02.emissao.left/10))),datetostr(now));
           //ValorFatura
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorFatura.left/10))),formata_valor(floattostr(valorcurrency)),[comp12]);
           //NEste modelo de duplocata o Numero da Fatiura eh gerado pelo sistema
           imp(strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.left/10))),PcodigoDuplicata);
           //ValorDuplicata=ValorFatura, usado o Saldo
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.left/10))),formata_valor(floattostr(valorcurrency)),[comp12]);
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.left/10))),tmpnumeroordem);
           //vencimento, usado o ultimo
           imp(strtoint(floattostr(int(FImpDuplicata02.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata02.vencimento.left/10))),ultimovencimento);
           //PEDIDOS
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos1.left/10))),linhapedido1a8[1]);
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos2.left/10))),linhapedido1a8[2]);
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos3.left/10))),linhapedido1a8[3]);
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos4.left/10))),linhapedido1a8[4]);
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos5.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos5.left/10))),linhapedido1a8[5]);
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos6.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos6.left/10))),linhapedido1a8[6]);
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos7.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos7.left/10))),linhapedido1a8[7]);
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos8.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos8.left/10))),linhapedido1a8[8]);
           //OBSERVACOES
           {imp(strtoint(floattostr(int(FImpDuplicata02.observacoes1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes1.left/10))),'Linha 1 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes2.left/10))),'Linha 2 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes3.left/10))),'Linha 3 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes4.left/10))),'Linha 4 Observa��es');}

           CredorDevedorTMP:=Self.Titulo.CredorDevedor.Get_codigo;
           CodigoCredorDevedorTmp:=Self.titulo.Get_CODIGOCREDORDEVEDOR;

           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata02.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata02.nome.left/10))),Self.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));

           If(UsaEnderecoEnderecoCobrancaGlobal=false)
            Then Begin

                  //Endereco do Sacado
                  imp(strtoint(floattostr(int(FImpDuplicata02.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata02.endereco.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Bairro
                  imp(strtoint(floattostr(int(FImpDuplicata02.BAIRRO.Top/15))),strtoint(floattostr(int(FImpDuplicata02.BAIRRO.left/10))),Self.Titulo.CREDORDEVEDOR.Get_BairroCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Cidade
                  imp(strtoint(floattostr(int(FImpDuplicata02.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cidade.left/10))),Self.Titulo.CREDORDEVEDOR.Get_cidadeCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //estado
                  imp(strtoint(floattostr(int(FImpDuplicata02.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata02.estado.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EstadoCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //fone
                  imp(strtoint(floattostr(int(FImpDuplicata02.FONE.Top/15))),strtoint(floattostr(int(FImpDuplicata02.FONE.left/10))),Self.Titulo.CREDORDEVEDOR.Get_telefoneCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
            End
            Else Begin
                  //Endereco do Sacado
                  imp(strtoint(floattostr(int(FImpDuplicata02.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata02.endereco.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Bairro
                  imp(strtoint(floattostr(int(FImpDuplicata02.BAIRRO.Top/15))),strtoint(floattostr(int(FImpDuplicata02.BAIRRO.left/10))),Self.Titulo.CREDORDEVEDOR.Get_BairroCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //Cidade
                  imp(strtoint(floattostr(int(FImpDuplicata02.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cidade.left/10))),Self.Titulo.CREDORDEVEDOR.Get_CidadeCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //estado
                  imp(strtoint(floattostr(int(FImpDuplicata02.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata02.estado.left/10))),Self.Titulo.CREDORDEVEDOR.Get_EstadoCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
                  //fone
                  imp(strtoint(floattostr(int(FImpDuplicata02.FONE.Top/15))),strtoint(floattostr(int(FImpDuplicata02.FONE.left/10))),Self.Titulo.CREDORDEVEDOR.Get_TelefoneCredorDevedorCobranca(CredorDevedorTMP,CodigoCredorDevedorTmp));
            End;

           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata02.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cnpj.left/10))),Self.Titulo.CREDORDEVEDOR.Get_cpfcnpjCredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata02.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ie.left/10))),Self.Titulo.CREDORDEVEDOR.Get_RGIECredorDevedor(CredorDevedorTMP,CodigoCredorDevedorTmp));
           //valorextensoa
           ValorExtenso_DivididoDois(valorcurrency,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata.valorextenso2.left/10))),extenso2);
           result:=True;
     End;

End;



procedure TObjPendencia.RetornaProximascomSaldo(PTitulo,
  PPendenciaInicial: string; PStringList: TStringList);
begin
     PStringList.Clear;
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select * from Tabpendencia where titulo='+PTitulo);
          SelectSQL.add('and Codigo>='+PPendenciaInicial+' and saldo>0 order by vencimento');
          open;
          first;
          While not(eof) do
          Begin
               PStringList.add(fieldbyname('codigo').asstring);
               next;
          End;
          close;
     End;

end;

function TObjPendencia.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_PENDENCIA';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
           result:='0';
           exit;

        end;
     Finally
            FreeandNil(StrTemp);
     End;


end;

procedure TObjPendencia.Get_ListaPendenciaCampos(ParametroCodigo,
  ParametroHistorico, ParametroVencimento, ParametroValor, ParametroSaldo,
  ParametroBoleto, ParametroDuplicata: TStrings; Parametrotitulo: string;Saldo_Todas_quitadas:char);
begin
        Self.Get_ListaCodigo(parametroCodigo,Parametrotitulo,Saldo_Todas_quitadas);
        Self.Get_ListaHistorico(parametroHistorico,Parametrotitulo,Saldo_Todas_quitadas);
        Self.Get_ListaVencimento(parametroVencimento,Parametrotitulo,Saldo_Todas_quitadas);
        Self.Get_ListaValor(parametroValor,Parametrotitulo,Saldo_Todas_quitadas);
        Self.Get_ListaSaldo(parametroSaldo,Parametrotitulo,Saldo_Todas_quitadas);
        Self.Get_ListaBoleto(parametroBoleto,Parametrotitulo,Saldo_Todas_quitadas);
        Self.Get_ListaDuplicata(parametroduplicata,Parametrotitulo,Saldo_Todas_quitadas);
end;


procedure TObjPendencia.Opcoes(PNumeroTitulo: string);
Var
  FEmiteCartaCobranca : TFEmiteCartaCobranca;
  FlancaBoletovariasPendenciasX:TflancaBoletovariasPendencias;
  FrelPrevisaoFinanceiraX:TFrelPrevisaoFinanceira;
begin
     With FopcaoRel do
     Begin

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Lan�ar T�tulo do Saldo de T�tulos a Pagar e Receber');//0
                items.add('Lan�ar Boletos Banc�rios do T�tulo Atual'); //1
                items.add('Lan�ar Uma Duplicata para mais de uma Pend�ncia '); //2
                items.add('Lan�ar uma duplicata para cada pend�ncia escolhida');  //3
                items.add('Lan�ar pend�ncias para t�tulos sem pend�ncias (somente SYSDBA)'); //4
                items.add('Alterar datas e Valores das Pend�ncias'); //5
                items.add('Acerto de Pend�ncias a Pagar e Receber');   //6
                items.add('Lan�a n� dos boletos a pagar para o t�tulo atual'); //7
                items.add('Lan�a Observa��o nas Pend�ncias do t�tulo atual'); //8
                Items.Add('Emite carta cobran�a'); //9
                items.add('Gera Boletos de V�rias Pend�ncias'); //10
                items.add('M�dulo de Previs�o Financeira'); //11

          End;

          showmodal;
          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0:Self.Titulo.GeratituloSaldoPagar_receber('');
                1:Self.LancaBoletos(PNumeroTitulo);
                2:Self.Titulo.LancaDuplicatas(PNumeroTitulo,False);
                3:Self.Titulo.LancaDuplicatas(PNumeroTitulo,True);
                4:Self.Titulo.LancapendenciasTitulos;
                5:Self.Alteravencimentosevalorespendencias(PNumeroTitulo);
                6:Begin
                       FAcertoCreditoDebito.ShowModal;
                End;
                7:Begin
                       if (Self.Lanca_Numero_BoletoAPagar(PNumeroTitulo)=False)
                       Then FDataModulo.IBTransaction.RollbackRetaining
                       Else FDataModulo.IBTransaction.CommitRetaining;
                End;
                8:Begin
                       if (Self.Lanca_Observacao_titulo(PNumeroTitulo)=False)
                       Then FDataModulo.IBTransaction.RollbackRetaining
                       Else FDataModulo.IBTransaction.CommitRetaining;
                End;
                9:Begin
                      try
                             FEmiteCartaCobranca := TFEmiteCartaCobranca.Create(nil);
                             FEmiteCartaCobranca.ShowModal;
                      finally
                             FreeAndNil(FEmiteCartaCobranca);
                      end;
                end;
                10:Begin
                        Try
                           FlancaBoletovariasPendenciasX:=TflancaBoletovariasPendencias.create(nil);
                           FlancaBoletovariasPendenciasX.PassaObjeto(Self);
                           FlancaBoletovariasPendenciasX.Showmodal;

                        Finally
                           Freeandnil(FlancaBoletovariasPendenciasX);
                        End;


                End;
                11:Begin
                        try
                            FrelPrevisaoFinanceiraX:=TFrelPrevisaoFinanceira.create(nil);
                        Except
                              mensagemerro('Erro na tentativa de criar o formul�rio de previs�o financeira');
                              exit;
                        End;
                        
                        Try
                              FrelPrevisaoFinanceiraX.ObjTitulo:=Self.Titulo;
                              FrelPrevisaoFinanceiraX.ShowModal;
                        finally
                               Freeandnil(FrelPrevisaoFinanceiraX);
                        End;
                End;
          End;
     end;

end;

procedure TObjPendencia.Alteravencimentosevalorespendencias( PNumeroTitulo: string );
var
  FAlteraPendencias_TituloXXX : TFAlteraPendencias_Titulo;
  QueryTemp : TIBQuery;
  cont : integer;
  PcodigoPendencia : string;
  pvencimento, pvalor : string;
  Temp : String;
begin
  if (ObjPermissoesUsoGlobal.ValidaPermissao('ALTERAR VALOR E VENCIMENTO DE PEND�NCIAS')=False) then
    Exit;

  try
    FAlteraPendencias_TituloXXX := TFAlteraPendencias_Titulo.Create(nil);
    QueryTemp:=TIBQuery.create(nil);
    QueryTemp.database:=FDataModulo.IBDatabase;

    if (PNumeroTitulo='') then
    begin
      Messagedlg('Escolha um T�tulo Financeiro',mtinformation,[mbok],0);
      exit;
    end;

    if (Self.Titulo.LocalizaCodigo(PNumeroTitulo)=False) then
    begin
      Messagedlg('T�tulo n�o localizado!',mtinformation,[mbok],0);
      exit;
    end;

    Self.Titulo.TabelaparaObjeto;

    with Self.ObjDataset do
    begin
      //primeiro preencho as informacoes do

      //Valor do T�tulo
      FAlteraPendencias_TituloXXX.lbvalortitulo.caption:='R$ '+formata_valor(Self.Titulo.Get_VALOR);

      //Quantidade de pendencias
      close;
      SelectSQL.clear;
      SelectSQL.add('Select count(codigo) as CONTA from Tabpendencia where titulo='+Self.titulo.Get_codigo);
      open;
      FAlteraPendencias_TituloXXX.lbquantparcelas.caption:=fieldbyname('conta').asstring;

      //Parcelas Quitadas
      close;
      SelectSQL.clear;
      SelectSQL.add('Select count(codigo) as CONTA from Tabpendencia where saldo=0 and titulo='+Self.titulo.Get_codigo);
      open;
      FAlteraPendencias_TituloXXX.lbparcelasquitadas.caption:=fieldbyname('conta').asstring;


      close;
      SelectSQL.clear;
      SelectSQL.add('Select sum(valor) as SOMA from Tabpendencia where saldo=0 and titulo='+Self.titulo.Get_codigo);
      open;
      FAlteraPendencias_TituloXXX.lbvalorparcelasquitadas.caption:='R$ '+formata_valor(fieldbyname('soma').asstring);

      //Parcelas Quitadas Parcialmente
      close;
      SelectSQL.clear;
      SelectSQL.add('Select count(codigo) as CONTA from Tabpendencia where saldo>0 and Saldo<>Valor and titulo='+Self.titulo.Get_codigo);
      open;
      FAlteraPendencias_TituloXXX.lbparcelasquitadasparcialmente.caption:=fieldbyname('conta').asstring;

      close;
      SelectSQL.clear;
      SelectSQL.add('Select sum(valor) as SOMA from Tabpendencia where saldo>0 and Saldo<>Valor and titulo='+Self.titulo.Get_codigo);
      open;
      FAlteraPendencias_TituloXXX.lbvalorparcelasquitadasparcialmente.caption:='R$ '+formata_valor(fieldbyname('soma').asstring);

      //Parcelas Abertas
      close;
      SelectSQL.clear;
      SelectSQL.add('Select count(codigo) as CONTA from Tabpendencia where saldo>0 and Saldo=Valor and titulo='+Self.titulo.Get_codigo);
      open;
      FAlteraPendencias_TituloXXX.lbparcelasabertas.caption:=fieldbyname('conta').asstring;

      close;
      SelectSQL.clear;
      SelectSQL.add('Select sum(valor) as SOMA from Tabpendencia where saldo>0 and Saldo=Valor and titulo='+Self.titulo.Get_codigo);
      open;
      FAlteraPendencias_TituloXXX.lbvalorparcelasabertas.caption:='R$ '+formata_valor(fieldbyname('soma').asstring);

      //*****Preenchendo a StringList com o que esta Aberto*****
      close;
      SelectSQL.clear;
      SelectSQL.add('Select codigo,vencimento,valor from Tabpendencia where saldo>0 and Saldo=Valor and titulo='+Self.titulo.Get_codigo);
      SelectSQL.add('order by vencimento');
      open;

      first;
      FAlteraPendencias_TituloXXX.LBVencimentos.Items.clear;
      FAlteraPendencias_TituloXXX.LBValores.Items.clear;
      FAlteraPendencias_TituloXXX.LBCodigos.Items.clear;

      while not(eof) do
      begin
        FAlteraPendencias_TituloXXX.LbCodigos.Items.add(fieldbyname('codigo').asstring);
        FAlteraPendencias_TituloXXX.LBVencimentos.Items.add(fieldbyname('vencimento').asstring);
        FAlteraPendencias_TituloXXX.LBValores.Items.add(fieldbyname('valor').asstring);
        next;
      end;

      FAlteraPendencias_TituloXXX.ShowModal;

      if FAlteraPendencias_TituloXXX.tag = 1 then
        exit;

      if (FAlteraPendencias_TituloXXX.LBVencimentos.Items.Count <
          FAlteraPendencias_TituloXXX.LbCodigos.Items.Count) then
      begin
        //alguma pendencia foi retirada
        //preciso ver qual foi e tentar excluir
        for cont:=FAlteraPendencias_TituloXXX.LBVencimentos.Items.Count to FAlteraPendencias_TituloXXX.LbCodigos.Items.Count-1 do
        begin
          close;
          SelectSQL.Clear;
          SelectSQL.Add('Delete from Tabpendencia where codigo='+FAlteraPendencias_TituloXXX.LbCodigos.Items[cont]);
          try
            execsql;
          except
            Messagedlg('A Pend�ncia de N� '+FAlteraPendencias_TituloXXX.LbCodigos.Items[cont]+' n�o pode ser exclu�da provavelmente ela sofreu algum lan�amento!',mterror,[mbok],0);
            FDataModulo.IBTransaction.RollbackRetaining;
            exit;
          end;
        end;

        for cont:=FAlteraPendencias_TituloXXX.LbCodigos.Items.Count-1 downto FAlteraPendencias_TituloXXX.LBVencimentos.Items.Count do
        begin
          FalteraPendencias_TituloXXX.LbCodigos.Items.Delete(cont);
        end;
      end;

      //criando novas pendencias
      if (FAlteraPendencias_TituloXXX.LBVencimentos.Items.Count>
          FAlteraPendencias_TituloXXX.LbCodigos.Items.Count) then
      begin
        //criado novas pend�ncias
        for cont:=FAlteraPendencias_TituloXXX.LbCodigos.Items.Count to FAlteraPendencias_TituloXXX.LBVencimentos.Items.Count-1 do
        begin
          Self.ZerarTabela;
          PcodigoPendencia:=Self.Get_NovoCodigo;
          FAlteraPendencias_TituloXXX.LbCodigos.Items.add(PcodigoPendencia);
          Self.Submit_CODIGO(PcodigoPendencia);
          Self.Titulo.LocalizaCodigo(PNumeroTitulo);
          Self.Titulo.TabelaparaObjeto;
          Self.PORTADOR.Submit_CODIGO(Self.Titulo.PORTADOR.Get_CODIGO);
          Self.Historico:='.';
          Self.VENCIMENTO:=FAlteraPendencias_TituloXXX.LBVencimentos.Items[cont];
          Self.VALOR:=FAlteraPendencias_TituloXXX.LBValores.Items[cont];
          Self.Saldo:=Self.VALOR;
          Self.Status:=dsinsert;

          if (Self.Salvar(False)=False) then
          begin
            FDataModulo.IBTransaction.RollbackRetaining;
            Messagedlg('N�o foi poss�vel criar uma nova pend�ncia!',mterror,[mbok],0);
            exit;
          end;
        end;
      end;

      //percorrendo as pendencias e alterando vencimento e valores
      for cont:=0 to FAlteraPendencias_TituloXXX.LbCodigos.count-1 do
      begin
        pvencimento:=FAlteraPendencias_TituloXXX.LBVencimentos.Items[cont];
        pvalor:=FAlteraPendencias_TituloXXX.LBValores.Items[cont];

        close;
        SelectSQL.clear;
        SelectSQL.add('Update TabPendencia set vencimento='+#39+FormatDateTime('mm/dd/yyyy',strtodate(pvencimento))+#39);
        SelectSQL.add(',Valor='+virgulaparaponto(pvalor)+' ,saldo='+virgulaparaponto(pvalor));
        SelectSQL.add('where codigo='+FAlteraPendencias_TituloXXX.LbCodigos.items[cont]);

        try
          execsql;
        except
          FDataModulo.IBTransaction.RollbackRetaining;
          Messagedlg('Erro na tentativa de Alterar o Valor e o Vencimento da pend�ncia '+FAlteraPendencias_TituloXXX.LbCodigos.items[cont],mterror,[mbok],0);
          exit;
        end;
      end;

      //alterando o valor do titulo
      close;
      SelectSQL.clear;
      SelectSQL.add('Select sum(valor) as SOMA from TabPendencia where titulo='+PNumeroTitulo);
      open;
      pvalor:=fieldbyname('soma').asstring;

      close;
      SelectSQL.clear;
      SelectSQL.add('Update TabTitulo Set Valor='+virgulaparaponto(pvalor)+' where codigo='+PNumeroTitulo);

      try
        ExecSQL;
      except
        FDataModulo.IBTransaction.RollbackRetaining;
        Messagedlg('Erro na tentativa de Alterar o Valor do T�tulo',mterror,[mbok],0);
        exit;
      end;

      //alterando o historico de todas as pendencias
      // Parametro Colocado por F�bio, para resolver um problema do Jucelino, ele precisa alterar o vencimento sem alterar o Historico
      if (ObjParametroGlobal.ValidaParametro('ATUALIZAR HISTORICO EM ALTERA��O DE PENDENCIAS')=false)
      then temp:='SIM';

      if (Temp = '') then
      begin
        if (ObjParametroGlobal.Get_Valor = 'SIM') then
          temp:='SIM'
        else temp:='NAO';
      end;

      if (Temp = 'SIM') then
      begin
        close;
        SelectSQL.clear;
        SelectSQL.add('Select count(codigo) as CONTA from Tabpendencia where titulo='+PNumeroTitulo);
        open;
        pvalor:=fieldbyname('conta').asstring;

        Close;
        SelectSQL.clear;
        SelectSQL.add('Select codigo from Tabpendencia where titulo='+PNumeroTitulo);
        SelectSQL.add('order by vencimento');
        open;
        first;
        cont:=1;

        while not(eof) do
        begin
          QueryTemp.close;
          QueryTemp.sql.clear;
          QueryTemp.sql.add('Update TabPendencia set historico='+#39+inttostr(cont)+'/'+pvalor+#39);
          QueryTemp.sql.add('where codigo='+Fieldbyname('codigo').asstring);

          try
            QueryTemp.ExecSQL;
          except
            FDataModulo.IBTransaction.RollbackRetaining;
            Messagedlg('Erro na tentativa de Alterar o Hist�rico da Pend�ncia '+fieldbyname('codigo').asstring,mterror,[mbok],0);
            exit;
          end;

          inc(cont,1);
          next;
        end;
      end;

      Self.titulo.LocalizaCodigo(PNumeroTitulo);
      Self.titulo.TabelaparaObjeto;

      Self.titulo.ObjExportaContabilidade.ZerarTabela;

      if (Self.titulo.ObjExportaContabilidade.LocalizaGerador('OBJTITULO',PNumeroTitulo)=True) then
      begin
        Self.titulo.ObjExportaContabilidade.TabelaparaObjeto;
        Self.titulo.ObjExportaContabilidade.Submit_Valor(self.Titulo.Get_VALOR);
        Self.titulo.ObjExportaContabilidade.Status:=dsedit;

        if (Self.titulo.ObjExportaContabilidade.Salvar(false)=False) then
        begin
          mensagemerro('Erro na tentativa de alterar a contabilidade');
          exit;
        end;
      end;

      FDataModulo.IBTransaction.CommitRetaining;
    end;
  finally
    Freeandnil(FAlteraPendencias_TituloXXX);
    Freeandnil(QueryTemp);
  end;

end;


function TObjPendencia.LancaBoleto_Varias_Pendencias(PStrPend,PStrValorPend: TStringList;
  PValorBoleto: Currency): Boolean;
var
   PcodigoConvenio:string;
begin
     Result:=False;

     //As pend�ncias j� vem entre parametro assim como Valor
     //basta escolher o convenio e enviar pro Procedimento correto


     if (Self.BoletoBancario.Convenioboleto.Pegaconvenio=False)
     Then exit;

     PcodigoConvenio:=Self.BoletoBancario.Convenioboleto.get_codigo;

     if (Self.BoletoBancario.Convenioboleto.Get_PreImpresso='S')
     Then Begin
               result:=Self.lancaboleto_PreImpresso_VariasPendencias(PcodigoConvenio,pstrpend,PStrValorPend,PValorBoleto);
     End
     Else Begin
               result:=Self.LancaBoleto_Sem_Registro_VariasPendencias(PcodigoConvenio,PStrPend,PStrValorPend,PValorBoleto);
     End;
end;

function TObjPendencia.LancaBoleto_Sem_Registro_VariasPendencias(PcodigoConvenio:string;StrPend,PStrValorPend: TStringList; PvalorBoleto: Currency): Boolean;
var
cont:integer;
temp:string;
pnumeroboleto,Pinstrucoes:String;
PtaxaBanco,Ptaxamensal,pvaloraodia:Currency;
pdiasprotesto:integer;
PDataVencimento:string;
ObjLancamentotemp:Tobjlancamento;

begin
     result:=False;

     Try
        ObjLancamentotemp:=TObjLancamento.Create;
     except
        Messagedlg('Erro na tentativa de Criar o ObjLancamentoTEMP',mterror,[mbok],0);
        exit;
     End;

Try     

     //Pegando o vencimento do Boleto

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Vencto';
          edtgrupo01.EditMask:=MascaraData;
          showmodal;
          if (tag=0)
          then exit;

          Try
              strtodate(edtgrupo01.text);
              PdataVencimento:=edtgrupo01.text;
          Except
                Messagedlg('Vencimento Inv�lido',mterror,[mbok],0);
                exit;
          End;

     End;

     if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
     Then Begin
               Messagedlg('Conv�nio n�o encontrado!',mterror,[mbok],0);
               exit;
     End;
     Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;

     Try
         PtaxaBanco:=Strtofloat(Self.BoletoBancario.Convenioboleto.get_taxabanco);
     Except
         PtaxaBanco:=0;
     End;

     Self.BoletoBancario.ZerarTabela;
     Self.BoletoBancario.Status:=dsinsert;
     Self.BoletoBancario.Submit_CODIGO('0');
     Self.BoletoBancario.Submit_Situacao('I');
     //Aqui vou usar o codigo do Boleto
     //Pego depois q salvar
     Self.BoletoBancario.Submit_NumeroBoleto('');//nosso numero
     Self.BoletoBancario.Submit_Vencimento(PDataVencimento);
     Self.BoletoBancario.Submit_DataDoc(datetostr(now));
     //Sera o Usado o Codigo do Boleto
     //Pego depois q salver
     Self.BoletoBancario.Submit_NumeroDoc('');
     Self.BoletoBancario.Submit_DataProc(datetostr(now));
     //No Valor que sera cobrado � acrescido a taxa que o banco cobra
     Self.BoletoBancario.Submit_ValorDoc(floattostr(PvalorBoleto+PtaxaBanco));
     Self.BoletoBancario.Submit_Desconto('');
     Self.BoletoBancario.Submit_OutrasDeducoes('');
     Self.BoletoBancario.Submit_Juros('');
     Self.BoletoBancario.Submit_outrosacrescimos('');
     Self.BoletoBancario.Submit_ValorCobrado('');
     //*******************************************
     Self.BoletoBancario.Convenioboleto.Submit_CODIGO(PcodigoConvenio);
     Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio);
     Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
     //*******************************************
     Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;
     //as instrucoes possuem variaveis pre-determinada
     //que sao colocadas no texto, aqui apenas eu mando substituir
     //caso o texto as tenha usado ele substitue
     //senao nao tem erro nenhum. Exemplo:
     //Ser� Protestado ap�s dia &dataprotesto
     Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;

     try
         //juros
         Ptaxamensal:=strtofloat(Self.BoletoBancario.ConvenioBoleto.get_taxamensal);
     Except
           Ptaxamensal:=0;
     End;

     try
        ptaxamensal:=((PvalorBoleto*Ptaxamensal)/100);
        pvaloraodia:=strtofloat(tira_ponto(formata_valor(Ptaxamensal/30)));
     Except
           pvaloraodia:=0;
     End;

     //taxamensal,Quantdiasprotesto

     Try
        pdiasprotesto:=strtoint(Self.BoletoBancario.Convenioboleto.get_Quantdiasprotesto);
     Except
        pdiasprotesto:=0;
     End;

     Pinstrucoes:=strreplace(pinstrucoes,'&taxamensal',formata_valor(ptaxamensal));
     Pinstrucoes:=strreplace(pinstrucoes,'&valoraodia',formata_valor(pvaloraodia));
     Pinstrucoes:=strreplace(pinstrucoes,'&dataprotesto',datetostr(strtodate(PDataVencimento)+pdiasprotesto));
     Pinstrucoes:=strreplace(pinstrucoes,'&datavencimento',PDataVencimento);
     Pinstrucoes:=strreplace(pinstrucoes,'&taxabanco',Formata_valor(PtaxaBanco));

     Self.BoletoBancario.Submit_Instrucoes(Pinstrucoes);

     if (Self.BoletoBancario.Salvar(False,'')=False)
     then begin
               Messagedlg('Erro na tentativa de Criar um novo boleto',mterror,[mbok],0);
               exit;
     end;
     pnumeroboleto:='';
     PnumeroBoleto:=Self.BoletoBancario.Get_codigo;
     Self.BoletoBancario.Status:=DsEdit;
     if (Self.Boletobancario.GeraNossoNumero=False)
     Then Begin
              FDataModulo.IBTransaction.RollbackRetaining;
              Messagedlg('Erro na tentativa de Gerar O Nosso N�mero do Boleto',mterror,[mbok],0);
              exit;
     end;

     Self.BoletoBancario.Submit_NumeroDoc(Self.BoletoBancario.Get_CODIGO);
     if (Self.BoletoBancario.Salvar(False,'')=False)
     then begin
               Messagedlg('Erro na tentativa de Guardar o N� do Boleto e N� do Doc',mterror,[mbok],0);
               exit;
     end;
     PnumeroBoleto:=Self.BoletoBancario.Get_CODIGO;

     //guardando o numero do boleto nas pendencias
     for cont:=0 to StrPend.count-1 do
     Begin
          Self.ZerarTabela;
          Self.LocalizaCodigo(StrPend[cont]);
          Self.TabelaparaObjeto;
          Self.Status:=dsedit;
          Self.BoletoBancario.Submit_CODIGO(PnumeroBoleto);
          if (Self.Salvar(False)=False)
          Then Begin
                    Messagedlg('Erro na tentativa de Gravar o Numero do boleto na pend�ncia '+StrPend[cont],mterror,[mbok],0);
                    exit;
          End;

          if (strtofloat(Self.Saldo)<strtofloat(PStrValorPend[cont]))//foi colocado juros
           Then Begin
                     //lancando a diferenca como Juros
                     temp:='';
                     If (ObjLancamentotemp.LancamentoAutomatico('J',StrPend[cont],floattostr((strtofloat(PStrValorPend[cont])-strtofloat(Self.Saldo))),datetostr(now),'REFERENTE JUROS NO LAN�AMENTO DO BOLETO',temp,False)=False)
                     Then Begin
                               Messagedlg('N�o foi poss�vel lan�ar os Juros na Pend�ncia N� '+StrPend[cont],mterror,[mbok],0);
                               exit;
                     End;
           End;


     End;

     //lancar a taxa do boleto apenas na ultima pendencia
     if (PtaxaBanco<>0)
     Then Begin
                  temp:='';
                  If (ObjLancamentotemp.LancamentoAutomatico('J',StrPend[strpend.count-1],floattostr(PtaxaBanco),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',temp,False)=False)
                  Then Begin
                            Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                            exit;
                  End;
     End;
     FDataModulo.IBTransaction.CommitRetaining;
     //imprimindo o boleto
     Self.Imprime_Boleto_SemRegistro(StrPend[0]);
     Result:=True;

finally
       ObjLancamentotemp.Free;
end;

End;

function TObjPendencia.Get_BoletoaPagar: string;
begin
     Result:=Self.BoletoaPagar;
end;

procedure TObjPendencia.Submit_BoletoaPagar(parametro: string);
begin
     Self.BoletoaPagar:=parametro;
end;

function TObjPendencia.Lanca_Numero_BoletoAPagar(Ptitulo: string): Boolean;
var
cont:integer;
begin
     Result:=False;

     If (Ptitulo='')
     Then Begin
               Messagedlg('Escolha um t�tulo antes de Tentar lan�ar o N� dos  Boletos a Pagar!',mtinformation,[mbok],0);
               exit;
     End;

     If (Self.Titulo.LocalizaCodigo(ptitulo)=False)
     Then Begin
               messagedlg('T�tulo n�o localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.Titulo.TabelaparaObjeto;
     If (Self.Titulo.CONTAGERENCIAL.get_tipo<>'D')
     Then Begin
               Messagedlg('S� � poss�vel cadastrar o n� do Boleto a Pagar para "T�tulos a Pagar"',mtinformation,[mbok],0);
               exit;
     End;

     With Self.ObjQlocal do
     Begin
          Close;
          Sql.clear;
          Sql.add('Select * from Tabpendencia where titulo='+ptitulo);
          open;
          If (recordcount=0)
          Then Begin
                    Messagedlg('Este t�tulo n�o possui pend�ncia com Saldo ou j� foi gerado boleto para todas as pend�ncias!',mtinformation,[mbok],0);
                    exit;
          End;

          last;
          FAcertaBoletoaPagar.lbnumtitulo.caption:=Self.Titulo.Get_CODIGO;
          FAcertaBoletoaPagar.lbvalortitulo.caption:=formata_valor(Self.Titulo.Get_VALOR);
          FAcertaBoletoaPagar.lbcredordevedor.caption:=Self.Titulo.CREDORDEVEDOR.Get_Nome+'='+Self.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.Titulo.Get_CODIGOCREDORDEVEDOR);



          //preenchendo uma lista de pendencias a serem escolhidas
          FAcertaBoletoaPagar.grid.RowCount:=recordcount+1;
          first;
          cont:=1;
          FAcertaBoletoaPagar.Grid.ColCount:=7;
          FAcertaBoletoaPagar.Grid.Cols[0].Clear;
          FAcertaBoletoaPagar.Grid.Cols[1].Clear;
          FAcertaBoletoaPagar.Grid.Cols[2].Clear;
          FAcertaBoletoaPagar.Grid.Cols[3].Clear;
          FAcertaBoletoaPagar.Grid.Cols[4].Clear;
          FAcertaBoletoaPagar.Grid.Cols[5].Clear;
          FAcertaBoletoaPagar.Grid.Cols[6].Clear;

          FAcertaBoletoaPagar.Grid.Cells[0,0]:='C�digo';
          FAcertaBoletoaPagar.Grid.Cells[1,0]:='Hist�rico';
          FAcertaBoletoaPagar.Grid.Cells[2,0]:='Vencimento';
          FAcertaBoletoaPagar.Grid.Cells[3,0]:='Valor';
          FAcertaBoletoaPagar.Grid.Cells[4,0]:='Saldo';
          FAcertaBoletoaPagar.Grid.Cells[5,0]:='Boleto a Pagar';
          FAcertaBoletoaPagar.Grid.Cells[6,0]:='Observa��o';

          while not(eof) do
          Begin
               FAcertaBoletoaPagar.Grid.Cells[0,cont]:=fieldbyname('codigo').asstring;
               FAcertaBoletoaPagar.Grid.Cells[1,cont]:=fieldbyname('historico').asstring;
               FAcertaBoletoaPagar.Grid.Cells[2,cont]:=fieldbyname('vencimento').asstring;
               FAcertaBoletoaPagar.Grid.Cells[3,cont]:=formata_valor(fieldbyname('Valor').asstring);
               FAcertaBoletoaPagar.Grid.Cells[4,cont]:=formata_valor(fieldbyname('saldo').asstring);
               FAcertaBoletoaPagar.Grid.Cells[5,cont]:=fieldbyname('boletoapagar').asstring;
               FAcertaBoletoaPagar.Grid.Cells[6,cont]:=fieldbyname('observacao').asstring;
               inc(cont,1);
               next;
          End;
          AjustaLArguraColunaGrid(FAcertaBoletoaPagar.Grid);
          FAcertaBoletoaPagar.Grid.Enabled:=true;
         
          FAcertaBoletoaPagar.showmodal;

          if (FAcertaBoletoaPagar.Tag=0)
          Then exit;

          for cont:=1 to FAcertaBoletoaPagar.Grid.rowcount-1 do
          Begin
               Self.ZerarTabela;
               //gravando o boleto a pagar nas pendencias
               if (Self.localizacodigo(FAcertaBoletoaPagar.Grid.Cells[0,cont])=False)
               Then Begin
                         Messagedlg('Pend�ncia '+FAcertaBoletoaPagar.Grid.Cells[0,cont]+' n�o localizada ',mterror,[mbok],0);
                         exit;
               End;
               Self.TabelaparaObjeto;
               Self.Status:=dsEdit;

               Self.Submit_VENCIMENTO  (FAcertaBoletoaPagar.Grid.Cells[2,cont]);
               Self.Submit_BoletoaPagar(FAcertaBoletoaPagar.Grid.Cells[5,cont]);
               Self.Submit_Observacao(FAcertaBoletoaPagar.Grid.Cells[6,cont]);

               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de gravar o n� do boleto a pagar na pend�ncia n� '+Self.codigo,mterror,[mbok],0);
                         exit;
               End;
          End;
          Result:=True;
          Messagedlg('N� de Boletos a Pagar Lan�ados com Sucesso!',mtinformation,[mbok],0);
     End;
End;

function TObjPendencia.Lanca_Observacao_titulo(Ptitulo: string): Boolean;
var
cont:integer;
begin
     Result:=False;

     If (Ptitulo='')
     Then Begin
               Messagedlg('Escolha um t�tulo antes de Tentar lan�ar a Observa��o das Parcelas(pend�ncias)!',mtinformation,[mbok],0);
               exit;
     End;

     If (Self.Titulo.LocalizaCodigo(ptitulo)=False)
     Then Begin
               messagedlg('T�tulo n�o localizado!',mterror,[mbok],0);
               exit;
     End;
     Self.Titulo.TabelaparaObjeto;

     With Self.ObjQlocal do
     Begin
          Close;
          Sql.clear;
          Sql.add('Select * from Tabpendencia where titulo='+ptitulo);
          open;
          If (recordcount=0)
          Then Begin
                    Messagedlg('Este t�tulo n�o possui pend�ncia com Saldo ou j� foi gerado boleto para todas as pend�ncias!',mtinformation,[mbok],0);
                    exit;
          End;

          last;
          FAcertaObservacaoPendencia.lbnumtitulo.caption:=Self.Titulo.Get_CODIGO;
          FAcertaObservacaoPendencia.lbvalortitulo.caption:=formata_valor(Self.Titulo.Get_VALOR);
          FAcertaObservacaoPendencia.lbcredordevedor.caption:=Self.Titulo.CREDORDEVEDOR.Get_Nome+'='+Self.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Self.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.Titulo.Get_CODIGOCREDORDEVEDOR);



          //preenchendo uma lista de pendencias a serem escolhidas
          FAcertaObservacaoPendencia.grid.RowCount:=recordcount+1;
          first;
          cont:=1;
          FAcertaObservacaoPendencia.Grid.ColCount:=6;
          FAcertaObservacaoPendencia.Grid.Cols[0].Clear;
          FAcertaObservacaoPendencia.Grid.Cols[1].Clear;
          FAcertaObservacaoPendencia.Grid.Cols[2].Clear;
          FAcertaObservacaoPendencia.Grid.Cols[3].Clear;
          FAcertaObservacaoPendencia.Grid.Cols[4].Clear;
          FAcertaObservacaoPendencia.Grid.Cols[5].Clear;

          FAcertaObservacaoPendencia.Grid.Cells[0,0]:='C�digo';
          FAcertaObservacaoPendencia.Grid.Cells[1,0]:='Hist�rico';
          FAcertaObservacaoPendencia.Grid.Cells[2,0]:='Vencimento';
          FAcertaObservacaoPendencia.Grid.Cells[3,0]:='Valor';
          FAcertaObservacaoPendencia.Grid.Cells[4,0]:='Saldo';
          FAcertaObservacaoPendencia.Grid.Cells[5,0]:='Observa��o                         ';

          while not(eof) do
          Begin
               FAcertaObservacaoPendencia.Grid.Cells[0,cont]:=fieldbyname('codigo').asstring;
               FAcertaObservacaoPendencia.Grid.Cells[1,cont]:=fieldbyname('historico').asstring;
               FAcertaObservacaoPendencia.Grid.Cells[2,cont]:=fieldbyname('vencimento').asstring;
               FAcertaObservacaoPendencia.Grid.Cells[3,cont]:=formata_valor(fieldbyname('Valor').asstring);
               FAcertaObservacaoPendencia.Grid.Cells[4,cont]:=formata_valor(fieldbyname('saldo').asstring);
               FAcertaObservacaoPendencia.Grid.Cells[5,cont]:=fieldbyname('observacao').asstring;
               inc(cont,1);
               next;
          End;
          AjustaLArguraColunaGrid(FAcertaObservacaoPendencia.Grid);
          FAcertaObservacaoPendencia.Grid.Enabled:=true;
         
          FAcertaObservacaoPendencia.showmodal;

          if (FAcertaObservacaoPendencia.Tag=0)
          Then exit;

          for cont:=1 to FAcertaObservacaoPendencia.Grid.rowcount-1 do
          Begin
               Self.ZerarTabela;
               //gravando o boleto a pagar nas pendencias
               if (Self.localizacodigo(FAcertaObservacaoPendencia.Grid.Cells[0,cont])=False)
               Then Begin
                         Messagedlg('Pend�ncia '+FAcertaObservacaoPendencia.Grid.Cells[0,cont]+' n�o localizada ',mterror,[mbok],0);
                         exit;
               End;
               Self.TabelaparaObjeto;
               Self.Status:=dsEdit;
               Self.Submit_Observacao(FAcertaObservacaoPendencia.Grid.Cells[5,cont]);
               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de gravar a observa��o na pend�ncia n� '+Self.codigo,mterror,[mbok],0);
                         exit;
               End;
          End;
          Result:=True;
     End;
End;

procedure TObjPendencia.edtboletoKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.BoletoBancario.Get_Pesquisa,Self.BoletoBancario.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('numeroboleto').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


function TObjPendencia.LocalizaBoleto_Convenio: Boolean;
var
PConvenio,PnumBoleto:string;
begin
     result:=False;
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;

          LbGrupo01.caption:='Conv�nio';
          LbGrupo02.caption:='N� Boleto';

          edtgrupo01.OnKeyDown:=Self.BoletoBancario.edtconvenioboletoKeyDown;
          edtgrupo02.OnKeyDown:=Self.edtboletoKeyDown;
          showmodal;

          if (tag=0)
          Then exit;

          PConvenio:='';
          PnumBoleto:='';
          
          Try
             strtoint(edtgrupo01.text);
             Pconvenio:=edtgrupo01.text;
          Except
                Messagedlg('N� do Conv�nio Inv�lido',mterror,[mbok],0);
                exit; 
          End;

          if (edtgrupo02.text='')
          Then Begin
                   Messagedlg('N� do Boleto Inv�lido',mterror,[mbok],0);
                   exit;
          End;
          PnumBoleto:=Edtgrupo02.text;

     End;

     With Self.ObjQlocal do
     Begin
          close;
          sql.clear;
          sql.add('Select tabpendencia.titulo from tabpendencia');
          sql.add('join tabboletobancario on tabpendencia.boletobancario=tabboletobancario.codigo');
          sql.add('where tabboletobancario.convenioboleto='+PConvenio);
          sql.add('and tabboletobancario.numeroboleto='+#39+PnumBoleto+#39);
          open;
          if (Recordcount>0)
          Then Begin
                    if (Self.titulo.localizacodigo(fieldbyname('titulo').asstring)=False)
                    Then BEgin
                              Messagedlg('T�tulo N� '+fieldbyname('titulo').asstring+' n�o encontrado',mterror,[mbok],0);
                              exit;
                    End
                    Else Begin
                             result:=True;
                             Self.Titulo.TabelaparaObjeto;
                    End;
          End
          Else exit;
     End;

end;

procedure TObjPendencia.Imprime_Titulos_por_ContaGerencial(PtipoConta: string);
var
saida:boolean;
DataInicial:string;
DataLimite:Tdate;
CredorDevedorTemp,CodigoCredorDevedorTemp:Integer;
GeradorTExto:String;
Qlocal:Tibquery;
SomaGeral,SomaContaGerencial,SomaSubContaGerencial:currency;
cont,linha:integer;
tmpSubContagerencial,TmpContagerencial,strtemp:string;
ContaGerencialTemp,SubContaGerencialTemp:TStringList;
ExcluirContaGerencial:TStringList;
temp : Integer;

Begin

        Try
                Qlocal:=Tibquery.create(nil);
                Qlocal.database:=Self.ObjDataset.database;
                ContaGerencialTemp:=TStringList.create;
                SubContaGerencialTemp:=TStringList.create;
                ExcluirContaGerencial:=TStringList.Create;
                ContaGerencialTemp.Clear;
                SubContaGerencialTemp.Clear;
        Except
              Messagedlg('Erro na tentativa de cria��o Query!',mterror,[mbok],0);
              exit;
        End;
Try

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo03.Enabled:=True;
          Grupo04.Enabled:=True;
          Grupo05.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Data Inicial';

          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.caption:='Limite';

          edtgrupo03.EditMask:='';
          edtgrupo03.OnKeyDown:=Self.Titulo.EdtcontagerencialKeyDown;;
          LbGrupo03.caption:='Excluir Conta Gerencial';
          edtgrupo03.color:=$005CADFE;


          edtgrupo04.EditMask:='';
          edtgrupo04.OnKeyDown:=Self.titulo.EdtcontagerencialKeyDown;
          LbGrupo04.caption:='Conta Gerencial';
          edtgrupo04.color:=$005CADFE;

          edtgrupo05.EditMask:='';
          edtgrupo05.OnKeyDown:=Self.titulo.edtsubcontagerencialKeyDown_PV;
          LbGrupo05.caption:='Sub-Conta Gerencial';
          edtgrupo05.color:=$005CADFE;



          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.titulo.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.titulo.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          edtgrupo07.onkeydown:=Self.titulo.edtcodigocredordevedor_SPV_KeyDown;
          edtgrupo07.color:=$005CADFE;

          showmodal;
          If tag=0
          Then exit;
          
          Try
                DataInicial:='';

                if (comebarra(trim(edtgrupo01.text))<>'')
                then Begin
                        Strtodate(edtgrupo01.text);
                        DataInicial:=edtgrupo01.text;
                End;
          Except
                Datainicial:='';
          End;



          Try
                DataLimite:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          Try
                CredorDevedorTemp:=strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);

          Except
                CredorDevedorTemp:=-1;
          End;

          Try
             CodigoCredorDevedorTemp:=Strtoint(EdtGrupo07.text);
          Except
              CodigoCredorDevedorTemp:=-1;
          End;


          if (ExplodeStr(edtgrupo04.text,ContaGerencialTemp,';','integer')=false)
          then exit;

          for cont:=0 to ContaGerencialTemp. count-1 do
          Begin
                 If (Self.titulo.CONTAGERENCIAL.LocalizaCodigo(ContaGerencialTemp[cont])=False)
                 Then begin
                           mensagemerro('Conta gerencial '+ContaGerencialTemp[cont]+' n�o encontrada');
                           exit;
                 End;

                 Self.titulo.CONTAGERENCIAL.TabelaparaObjeto;
                 If (Self.titulo.CONTAGERENCIAL.Get_Tipo<>PtipoConta)
                 Then Begin
                              Messagedlg(' A Conta Gerencial '+ContaGerencialTemp[cont]+' difere do tipo de t�tulos escolhidos!',mterror,[mbok],0);
                              exit;
                 End;
          End;

          if (ExplodeStr(edtgrupo05.text,subContaGerencialTemp,';','integer')=false)
          then exit;

          for cont:=0 to SubContaGerencialTemp. count-1 do
          Begin
                 If (Self.titulo.SubCONTAGERENCIAL.LocalizaCodigo(SubContaGerencialTemp[cont])=False)
                 Then begin
                           mensagemerro('Sub-Conta gerencial '+SubContaGerencialTemp[cont]+' n�o encontrada');
                           exit;
                 End;
          End;

          Try
             ExplodeStr(edtgrupo05.text,ExcluirContaGerencial,';','INTEGER');
          except
             MensagemErro('Erro ao tentar separar as contas gerenciais que ser�o exclu�das');
             exit;
          end;

     End;


     With Qlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('select tabpendencia.codigo');
          SQL.add('from tabpendencia join tabtitulo');
          SQL.add('on tabpendencia.titulo=tabtitulo.codigo');
          SQL.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SQL.add('left join tabsubcontagerencial on tabtitulo.subcontagerencial=tabsubcontagerencial.codigo');



          if (DataInicial='')
          Then SQL.add(' where Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39+'  and Saldo>0 ')
          Else SQL.add(' where (Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(DataInicial))+#39+'  and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39+') and Saldo>0 ');

          SQL.add(' and tabContager.tipo='+#39+PtipoConta+#39);

          if (ContaGerencialTemp.Count>0)
          Then Begin
                  SQL.add(' and (TabTitulo.ContaGerencial='+COntaGerencialTemp[0]);

                  for cont:=1 to ContaGerencialTemp.Count-1 do
                  Begin
                       SQL.add(' or TabTitulo.ContaGerencial='+COntaGerencialTemp[cont]);
                  End;
                  SQL.add(' )');
          End;

          if (SubContaGerencialTemp.Count>0)
          Then Begin
                  SQL.add(' and (TabTitulo.SubContaGerencial='+SubCOntaGerencialTemp[0]);

                  for cont:=1 to SubContaGerencialTemp.Count-1 do
                  Begin
                       SQL.add(' or TabTitulo.SubContaGerencial='+SubCOntaGerencialTemp[cont]);
                  End;
                  SQL.add(' )');
          End;


          If (CredorDevedorTemp<>-1)
          Then SQL.add(' and TabTitulo.CredorDevedor='+INttostr(CredorDevedorTemp));

          If(CodigoCredorDevedorTemp<>-1)
          Then SQL.add(' and Tabtitulo.CodigoCredorDevedor='+Inttostr(CodigoCredorDevedorTemp));

          if (ExcluirContaGerencial.Count>0)
          then Begin
                    SQL.add(' and (Tabtitulo.ContaGerencial<>'+ExcluirContaGerencial[0]);
                    for temp:=1 to ExcluirContaGerencial.count-1 do
                    Begin
                         SQL.add(' and Tabtitulo.ContaGerencial<>'+ExcluirContaGerencial[temp]);
                    End;
                    SQL.add(' )');
          end;

          sql.add('order by tabtitulo.contagerencial,tabtitulo.subcontagerencial,tabpendencia.vencimento,tabtitulo.credordevedor,tabtitulo.codigocredordevedor');

          open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Gerando relat�rio';
          first;

          Self.ZerarTabela;
          Self.Titulo.CONTAGERENCIAL.ZerarTabela;
          Self.Titulo.SubContaGerencial.ZerarTabela;
          Self.LocalizaCodigo(fieldbyname('codigo').asstring);
          Self.TabelaparaObjeto;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;


          linha:=3;
          FreltxtRDPRINT.rdprint.abrir;
          if ((FreltxtRDPRINT.RDprint.Setup)=False)
          Then Begin
                    FreltxtRDPRINT.rdprint.fechar;
                    exit;
          End;

          If (PtipoConta='D')
          Then FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'TITULOS A PAGAR   - VENCIMENTO '+DATETOSTR(DataLimite),[negrito])
          Else FreltxtRDPRINT.RDprint.ImpC(linha,40,Self.NumeroRelatorio+'TITULOS A RECEBER - VENCIMENTO '+DATETOSTR(DataLimite),[negrito]);
          inc(linha,2);

          If (ContaGerencialTemp.count>0)
          Then Begin
                    Self.titulo.CONTAGERENCIAL.LocalizaCodigo(ContaGerencialTemp[0]);
                    Self.titulo.CONTAGERENCIAL.TabelaparaObjeto;
                    FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta Gerencial='+Self.titulo.CONTAGERENCIAL.get_nome,[negrito]);
                    inc(linha,1);

                    for cont:=1 to ContaGerencialTemp.Count-1 do
                    begin
                         Self.titulo.CONTAGERENCIAL.LocalizaCodigo(ContaGerencialTemp[cont]);
                         Self.titulo.CONTAGERENCIAL.TabelaparaObjeto;
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'                '+Self.titulo.CONTAGERENCIAL.get_nome,[negrito]);
                         inc(linha,1);
                    End;

          End;

          If (subcontaGerencialTemp.count>0)
          Then Begin
                    Self.titulo.subcontaGERENCIAL.LocalizaCodigo(subcontaGerencialTemp[0]);
                    Self.titulo.subcontaGERENCIAL.TabelaparaObjeto;
                    FreltxtRDPRINT.RDprint.Impf(linha,1,'Sub-Conta Gerencial='+Self.titulo.subcontaGERENCIAL.get_nome,[negrito]);
                    inc(linha,1);

                    for cont:=1 to subcontaGerencialTemp.Count-1 do
                    begin
                         Self.titulo.subcontaGERENCIAL.LocalizaCodigo(subcontaGerencialTemp[cont]);
                         Self.titulo.subcontaGERENCIAL.TabelaparaObjeto;
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'                    '+Self.titulo.subcontaGERENCIAL.get_nome,[negrito]);
                         inc(linha,1);
                    End;

          End;


          
          if (DataInicial<>'')
          Then FreltxtRDPRINT.rdprint.impf(linha,1,'Intervalo de Datas de Vencimento:'+DataInicial+' a '+datetostr(DataLimite),[negrito])
          Else FreltxtRDPRINT.rdprint.impf(linha,1,'Vencimento Anterior ou Igual'+datetostr(DataLimite),[negrito]);
          inc(linha,1);

          If (CredorDevedorTemp<>-1)
          Then Begin
                    If (Self.titulo.CREDORDEVEDOR.LocalizaCodigo(InttoStr(credordevedortemp))=True)
                    Then Begin
                              Self.titulo.CREDORDEVEDOR.TabelaparaObjeto;
                              strtemp:='';
                              strtemp:=Self.titulo.CredorDevedor.Get_nome;
                              IF (CodigoCredorDevedorTemp<>-1)
                              Then strtemp:=strtemp+' = '+Self.titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(Inttostr(CredorDevedorTemp),Inttostr(CodigoCredorDevedorTemp));
                              FreltxtRDPRINT.RDprint.Impf(linha,1,strtemp,[negrito]);
                              inc(linha,1);
                    End;
          End;

          FreltxtRDPRINT.RDprint.Impf(linha,6,CompletaPalavra('EMISSAO',08,' ')+' '+CompletaPalavra('FORNECEDOR/CLIENTE',24,' ')+' '+CompletaPalavra('TELEFONE',12,' ')+' '+CompletaPalavra('CELULAR',13,' ')+' '+
                                              CompletaPalavra('HIST�RICO',25,' ')+' '+
                                              CompletaPalavra('PEND.',7,' ')+' '+CompletaPalavra('VENCIM.',08,' ')+' '+CompletaPalavra_a_Esquerda('SALDO',15,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,1,completapalavra('_',90,'_'),[negrito]);
          inc(linha,2);


          SomaGeral:=0;
          SomaContaGerencial:=0;
          SomaSubContaGerencial:=0;

          TmpContagerencial:=Self.titulo.CONTAGERENCIAL.Get_CODIGO;
          TmpSubContagerencial:=Self.titulo.Subcontagerencial.get_codigo;


          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta Gerencial='+Self.Titulo.CONTAGERENCIAL.get_codigo+'-'+Self.Titulo.CONTAGERENCIAL.Get_Nome,[negrito]);
          inc(linha,1);

          if (tmpSubContagerencial<>'')
          then begin

                    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                    FreltxtRDPRINT.RDprint.Impf(linha,4,'Sub-Conta='+Self.Titulo.SubContaGerencial.get_mascara+' '+Self.Titulo.SubContaGerencial.get_codigo+'-'+Self.Titulo.SubCONTAGERENCIAL.Get_Nome,[negrito]);
                    inc(linha,1);

          End;


          While not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               Application.ProcessMessages;

               Self.ZerarTabela;
               Self.Titulo.CONTAGERENCIAL.ZerarTabela;
               Self.Titulo.SubContaGerencial.ZerarTabela;
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;

               If (TmpContagerencial<>Self.titulo.CONTAGERENCIAL.Get_CODIGO)
               Then Begin
                         if (tmpSubContagerencial<>'')
                         Then begin
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,4,'Total da Sub-Conta='+formata_valor(SomaSubContaGerencial),[negrito]);
                                   inc(linha,2);
                         End;

                         tmpSubContagerencial:=Self.titulo.Subcontagerencial.get_codigo;
                         SomaSubContaGerencial:=0;

                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'Total da Conta='+formata_valor(SomaContaGerencial),[negrito]);
                         inc(linha,2);

                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,1,'Conta Gerencial='+Self.Titulo.CONTAGERENCIAL.get_codigo+'-'+Self.Titulo.CONTAGERENCIAL.Get_Nome,[negrito]);
                         inc(linha,1);

                         SomaContaGerencial:=0;
                         TmpContagerencial:=Self.titulo.CONTAGERENCIAL.Get_CODIGO;
                         if (tmpSubContagerencial<>'')
                         then begin

                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.Impf(linha,4,'Sub-Conta='+Self.Titulo.SubContaGerencial.get_mascara+' '+Self.Titulo.SubContaGerencial.get_codigo+'-'+Self.Titulo.SubCONTAGERENCIAL.Get_Nome,[negrito]);
                                   inc(linha,1);

                         End;
               End
               else Begin
                         if (tmpSubContagerencial<>Self.Titulo.SubContaGerencial.Get_CODIGO)
                         then begin
                                  //nao mudou a conta gerencial mas pode ter mudado a subconta
                                  FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                  FreltxtRDPRINT.RDprint.Impf(linha,4,'Total da Sub-Conta='+formata_valor(SomaSubContaGerencial),[negrito]);
                                  inc(linha,2);
                                  SomaSubContaGerencial:=0;
                                  
                                  TmpSubContagerencial:=Self.titulo.SubCONTAGERENCIAL.Get_CODIGO;
                                  if (tmpSubContagerencial<>'')
                                  then begin
                                          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                          FreltxtRDPRINT.RDprint.Impf(linha,4,'Sub-Conta='+Self.Titulo.SubContaGerencial.get_mascara+' '+Self.Titulo.SubContaGerencial.get_codigo+'-'+Self.Titulo.SubCONTAGERENCIAL.Get_Nome,[negrito]);
                                          inc(linha,1);
                                  End;
                         End;
               End;

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,6,CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(Self.titulo.get_EMISSAO)),08,' ')+' '+CompletaPalavra(Self.titulo.CREDORDEVEDOR.Get_RazaoSocialCredorDevedor(Self.titulo.credordevedor.get_codigo,self.titulo.get_codigocredordevedor),24,' ')+' '+
                                                  CompletaPalavra(Self.titulo.CREDORDEVEDOR.Get_TelefoneCredorDevedor(Self.titulo.credordevedor.get_codigo,self.titulo.get_codigocredordevedor),15,' ')+' '+
                                                  CompletaPalavra(Self.titulo.CREDORDEVEDOR.Get_CelularCredorDevedor(Self.titulo.credordevedor.get_codigo,self.titulo.get_codigocredordevedor),10,' ')+' '+
                                                  CompletaPalavra(Self.titulo.get_historico,25,' ')+' '+
                                                  CompletaPalavra(Self.get_codigo,7,' ')+' '+CompletaPalavra(formatdatetime('dd/mm/yy',strtodate(Self.vencimento)),08,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(Self.get_saldo),15,' '));

               inc(linha,1);


               SomaContaGerencial:=SomaContaGerencial+strtofloat(Self.saldo);
               SomaSubContaGerencial:=SomaSubContaGerencial+strtofloat(Self.saldo);
               SomaGeral:=SomaGeral+strtofloat(Self.Saldo);
               next;
          End;//while

          if (tmpSubContagerencial<>'')
          Then begin
                    FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                    FreltxtRDPRINT.RDprint.Impf(linha,4,'Total da Sub-Conta='+formata_valor(SomaSubContaGerencial),[negrito]);
                    inc(linha,2);
          End;

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Total da Conta='+formata_valor(SomaContaGerencial),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,1,completapalavra('_',90,'_'));
          inc(linha,2);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,1,'Total Geral='+formata_valor(SomaGeral),[negrito]);
          inc(linha,2);


          FreltxtRDPRINT.RDprint.Fechar;
     End;

Finally
       FMostraBarraProgresso.close;
       Freeandnil(Qlocal);
       freeandnil(ContaGerencialTemp);
       freeandnil(SubContaGerencialTemp);
End;

end;




function TObjPendencia.Get_Observacao: string;
begin
     Result:=Self.Observacao;
end;

procedure TObjPendencia.Submit_Observacao(Parametro: string);
begin
     Self.Observacao:=Parametro;
end;


procedure TObjPendencia.LancaBoletos(PnumeroTitulo: string);
var
  PcodigoConvenio:string;
  StrLPendencias:TStringList;
  cont:integer;
  novasInstr : string;
begin

     If(PnumeroTitulo='')
     Then Begin
          MensagemAviso('Escolha um titulo');
          Exit;
     End;

     if (Self.titulo.LocalizaCodigo(PNumeroTitulo)=False)
     Then Begin
               Messagedlg('T�tulo n�o localizado',mterror,[mbok],0);
               exit;
     end;
     Self.titulo.TabelaparaObjeto;

     Try
        StrLPendencias:=TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList de Pend�ncias',mterror,[mbok],0);
           exit
     End;

     Try

        if (Self.BoletoBancario.Convenioboleto.Pegaconvenio=False)
        Then exit;

        PcodigoConvenio:=Self.BoletoBancario.Convenioboleto.get_codigo;

        //Escolhendo as pendencias que ser�o impressas no boleto
        With Self.ObjQlocal do
        Begin

             Close;
             Sql.clear;

             Sql.add('Select * from Tabpendencia where titulo='+pnumerotitulo+' and Saldo>0 and BoletoBancario is NULL');

             open;
             If (recordcount=0)
             Then Begin
                       Messagedlg('Este t�tulo n�o possui pend�ncia com Saldo ou j� foi gerado boleto para todas as pend�ncias!',mtinformation,[mbok],0);
                       exit;
             End;
             last;
             //preenchendo uma lista de pendencias a serem escolhidas
             FescolhePendencias.grid.RowCount:=recordcount+1;
             first;

             cont:=1;
             FescolhePendencias.lbCliente.Caption:=Self.Titulo.RetornaNomesCredoresDevedores(Self.Titulo.CREDORDEVEDOR.Get_CODIGO, Self.Titulo.Get_CODIGOCREDORDEVEDOR);
             FescolhePendencias.Grid.ColCount:=5;
             FescolhePendencias.Grid.Cells[0,0]:='Marca';
             FescolhePendencias.Grid.Cells[1,0]:='C�digo';
             FescolhePendencias.Grid.Cells[2,0]:='Hist�rico';
             FescolhePendencias.Grid.Cells[3,0]:='Valor';
             FescolhePendencias.Grid.Cells[4,0]:='Saldo';

             while not(eof) do
             Begin
                  FescolhePendencias.Grid.Cells[1,cont]:=fieldbyname('codigo').asstring;
                  FescolhePendencias.Grid.Cells[2,cont]:=fieldbyname('historico').asstring;
                  FescolhePendencias.Grid.Cells[3,cont]:=formata_valor(fieldbyname('Valor').asstring);
                  FescolhePendencias.Grid.Cells[4,cont]:=formata_valor(fieldbyname('saldo').asstring);
                  inc(cont,1);
                  next;
             End;
        End;

        AjustaLArguraColunaGrid(FescolhePendencias.Grid);
        FescolhePendencias.showmodal;
        //voltou, tenho que ver quais ficaram marcadas e delas gerar o boleto

        StrLPendencias.clear;
        for cont:=1 to FescolhePendencias.Grid.rowcount-1 do
        Begin
                If (Fescolhependencias.Grid.cells[0,cont]='X')//marcada para gerar boleto
                Then StrLPendencias.add(Fescolhependencias.Grid.cells[1,cont]);
        End;

        If (StrLPendencias.count=0)
        Then Begin
                  Messagedlg('Nenhuma pend�ncia foi escolhida para gerar Boleto!',mtinformation,[mbok],0);
                  exit;
        End;

        if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
        Then begin
                  Messagedlg('Conv�nio n�o localizado!',mterror,[mbok],0);
                  exit;
        End;
        Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;


        // F�bio
        if (Self.BoletoBancario.Convenioboleto.Get_CobreBem = 'S')
        then Begin
                if (Lancaboleto_COM_registro_CobreBem(pnumerotitulo,PcodigoConvenio,StrLPendencias)=false)
                then Begin
                        MensagemErro('Erro ao tentar lan�ar os boletos COBREBEM');
                        FDataModulo.IBTransaction.RollbackRetaining;
                        exit;
                end;
                FDataModulo.IBTransaction.CommitRetaining;
                exit;
        end;

        if (Self.BoletoBancario.Convenioboleto.Get_PreImpresso='S') then
          Self.lancaboleto_PreImpresso(PnumeroTitulo,PcodigoConvenio,StrLPendencias)
        else
        begin
          //nao � pre-impresso entao � sem registro ou com registro ou seja imprime tudo
          //isso vai dependender do banco do convenio
          if (Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco='001')   or  (Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco='237')
            or (Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco='104') or  (Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco='409')
            or (Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco='748') or  (Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco='399') then
          begin
            Self.lancaboleto_Sem_registro(PnumeroTitulo,PcodigoConvenio,StrLPendencias);
          end
          else
          begin
            Messagedlg('O sistema n�o suporta impress�o de boletos para o banco do conv�nio escolhido. C�digo do Banco '+Self.BoletoBancario.Convenioboleto.Get_CodigoBAnco,mterror,[mbok],0);
            exit;
          end;
        end;

     finally
            FreeAndNil(StrLPendencias);
     end;

end;


PRocedure TobjPendencia.Re_imprimeboleto(ppendencia:string;ppreview:Boolean);
Begin
     Self.Re_imprimeboleto(ppendencia,ppreview,1);
End;

PRocedure TobjPendencia.Re_imprimeboleto(ppendencia:string;ppreview:Boolean;modelo:integer);
Begin
     //esse procedimento serve tanto para boletos impressos completos ou pr�-impressos
     //ele � chamado no form do titulo para reimpressao

     if (ppendencia='')
     then Begin
               Messagedlg('Escolha a pend�ncia que deseja imprimir',mtinformation,[mbok],0);
               exit;
     end;

     if (Self.LocalizaCodigo(ppendencia)=False)
     then begin
               Messagedlg('Pend�ncia n�o localizada',mterror,[mbok],0);
               exit;
     End;

     Self.TabelaparaObjeto;

     if (Self.BoletoBancario.get_codigo='')
     then begin
               Messagedlg('Esta pend�ncia n�o possui boleto',mtinformation,[mbok],0);
               exit;
     end;

     if (Self.BoletoBancario.Convenioboleto.Get_CobreBem = 'S')
     then Begin
               if (Self.ReimprimeBoletoCobreBem(Self.BoletoBancario.get_codigo)=false)
               then Begin
                       MensagemErro('Erro ao tentar Reimprimir o Boleto');
                       exit;
               end;
               Exit;
     end; 


     if (Self.BoletoBancario.Convenioboleto.Get_PreImpresso='N')
     Then  Self.Imprime_Boleto_SemRegistro(ppendencia,ppreview,modelo)
     Else Begin

               if (FConfiguraBoleto.AbrePosicoesIni(Self.boletobancario.Convenioboleto.Get_CodigoBanco)=False)
               then exit;

               FDataModulo.ComponenteRdPrint.abrir;
               if (FDataModulo.ComponenteRdPrint.setup=False)
               then begin
                         FDataModulo.ComponenteRdPrint.fechar;
                         exit;
               End;
               Self.ImprimeBoletoPreImpresso(False);
               FDataModulo.ComponenteRdPrint.fechar;
     End;


End;
procedure TObjPendencia.edtBoletoNaoUsadoKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FboletoBancario:TFBoletoBancario;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FboletoBancario:=TFBoletoBancario.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.BoletoBancario.get_pesquisanaousados(Self.BoletoBancario.Convenioboleto.Get_CODIGO),'BOLETOS',FboletoBancario)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FboletoBancario);
     End;
end;


function TObjPendencia.PegaTitulo: string;
Begin
     result:='';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.enabled:=True;
          LbGrupo01.caption:='T�tulo';
          edtgrupo01.OnKeyDown:=Self.edttituloKeyDown;
          Showmodal;

          if (tag=0)
          Then exit;

          Result:=edtgrupo01.Text;
     end;

end;

procedure TObjPendencia.edttituloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FTITULO.PESQUISATITULO';
            If (FpesquisaLocal.PreparaPesquisa(Self.Titulo.Get_Pesquisa,Self.Titulo.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjPendencia.edtpendenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,Self.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPendencia.PegaPendencia: string;
Begin
     result:='';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.enabled:=True;
          LbGrupo01.caption:='Pend�ncia';
          edtgrupo01.OnKeyDown:=Self.edtpendenciaKeyDown;
          Showmodal;

          if (tag=0)
          Then exit;

          Result:=edtgrupo01.Text;
     end;

end;


function TObjPendencia.LancaVariosBoletos(PStrPend,PAdicionalObservacao: TStringList;Pvalor_Saldo:string): boolean;
Begin
     //chama com pre3view = true
     result:=Self.LancaVariosBoletos(PStrPend,PAdicionalObservacao,Pvalor_Saldo,True);
End;

function TObjPendencia.LancaVariosBoletos(PStrPend,PAdicionalObservacao: TStringList;Pvalor_Saldo:string;Ppreview:Boolean): boolean;
var
   PcodigoConvenio:string;
begin
     {este procedimento foi criado para manter compatibilidade com o sistema bia
     la ele escolhe todas as pendencias e acrescenta algumas informacoes
     na observacao, e chama essa funcao}

     Result:=False;

     if (Self.BoletoBancario.Convenioboleto.Pegaconvenio=False)
     Then exit;

     PcodigoConvenio:=Self.BoletoBancario.Convenioboleto.get_codigo;

     if (Self.BoletoBancario.Convenioboleto.Get_PreImpresso='S')
     Then Begin
               result:=Self.lancaboleto_PreImpresso_VariosBoletos(PcodigoConvenio,PStrPend,PAdicionalObservacao);
     End
     Else Begin
               Self.lancaboleto_Sem_registro_VARIOSBOLETOS(PcodigoConvenio,PStrPend,PAdicionalObservacao,PValor_Saldo,ppreview);
               Result:=True;
     End;
end;

function TObjPendencia.lancaboleto_PreImpresso_VariosBoletos(
  PcodigoConvenio: string; StrPend,
  PStrObservacaoAdicional: TStringList): boolean;
var
cont:integer;
StrBoleto:TStringList;
Saltar:boolean;
ObjLancamentoTemp:TobjLancamento;
pvalormensal,ptaxamensal,pvaloraodia,PTaxaBanco:Currency;
temp,pdiasprotesto:integer;
PInstrucoes:String;
PcodigoLanctoJuros:string;
begin
     if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
     then Begin
               Messagedlg('Conv�nio n�o localizado',mterror,[mbok],0);
               exit;
     End;
     Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;

     With Self.ObjQlocal do
     Begin
          Try
            StrBoleto:=TStringList.create;
          Except
                Messagedlg('Erro na tentativa de Cria��o da StringList de Pend�ncias selecionadas!',mterror,[mbok],0);
                exit;
          End;

          Try
             ObjLancamentotemp:=TObjLancamento.Create;
          except
             Freeandnil(StrBoleto);
             Messagedlg('Erro na tentativa de Criar o ObjLancamentoTEMP',mterror,[mbok],0);
             exit;
          End;


          Try
             Self.BoletoBancario.ZerarTabela;
             Self.BoletoBancario.Resgatanumeroboletos(StrPend,StrBoleto,PcodigoConvenio);
             If (StrBoleto.count=0)
             Then Begin
                       Messagedlg('Sem escolher os Boletos n�o � poss�vel continuar!',mterror,[mbok],0);
                       FdataModulo.IBTransaction.RollbackRetaining;
                       exit;
             End;

             for cont:=0 to StrPend.count-1 do
             Begin
             //***********************************************************
                  Self.ZerarTabela;
                  if (Self.LocalizaCodigo(StrPend[cont])=False)
                  Then Begin
                          Messagedlg('Pend�ncia n�o localizada!',mtinformation,[mbok],0);
                          exit;
                  End;
                  Self.TabelaparaObjeto;

                  if (Self.BoletoBancario.get_codigo<>'')
                  Then Begin
                            Messagedlg('Essa pend�ncia j� possui o Boleto '+Self.boletobancario.get_codigo+' n�o � poss�vel imprimir outro boleto para ela!',mtinformation,[mbok],0);
                            exit;
                  End;
                  
                  if (Self.BoletoBancario.LocalizaCodigo(StrBoleto[cont])=False)
                  Then Begin
                            Messagedlg('Boleto n�o localizado!',mtinformation,[mbok],0);
                            exit;
                  End;

                  Self.BoletoBancario.TabelaparaObjeto;
                  if (Self.BoletoBancario.Get_Situacao<>'N')
                  Then Begin
                            messagedlg('O boleto '+StrBoleto[cont]+' n�o pode ser usado!',mtinformation,[mbok],0);
                            exit;
                  End;
                  //como a pendencia ja esta com o numero do boleto correto
                  //� s� editar e salvar
                  Self.Status:=dsedit;
                  If (Self.Salvar(false)=False)
                  Then Begin
                            messagedlg('Erro na tentativa de Atualizar a Pend�ncia '+Self.codigo+' com o boleto '+Self.boletobancario.get_codigo,mterror,[mbok],0);
                            exit;
                 End;

                 //*******GRAVANDO AS INFORMACOES NO BOLETO******

                 //Adicionando o valor da taxa do boleto no saldo da pendencia como juros
                 Try
                    PTaxaBanco:=strtofloat(Self.BoletoBancario.Convenioboleto.Get_taxabanco);
                 Except
                    PTaxaBanco:=0;
                 End;

                 //as instrucoes possuem variaveis pre-determinada
                 //que sao colocadas no texto, aqui apenas eu mando substituir
                 //caso o texto as tenha usado ele substitue
                 //senao nao tem erro nenhum
                 //Ser� Protestado ap�s dia &dataprotesto
                 Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;
                 PInstrucoes:=PInstrucoes+#13;
                 //Adicionando o complemento da observacao enviado em parametro
                 for temp:=1 to length(PStrObservacaoAdicional[cont]) do
                 Begin
                      if (PStrObservacaoAdicional[cont][temp]<>'|')
                      then PInstrucoes:=PInstrucoes+PStrObservacaoAdicional[cont][temp]
                      Else PInstrucoes:=PInstrucoes+#13;
                 End;


                 try
                     //juros
                     Ptaxamensal:=strtofloat(Self.BoletoBancario.ConvenioBoleto.get_taxamensal);
                 Except
                       Ptaxamensal:=0;
                 End;
    
                 try
                    PvalorMensal:=((strtofloat(Self.VALOR)*Ptaxamensal)/100);
                    pvaloraodia:=strtofloat(tira_ponto(formata_valor(Pvalormensal/30)));
                 Except
                       pvaloraodia:=0;
                 End;
    
                 //taxamensal,Quantdiasprotesto
                 Try
                    pdiasprotesto:=strtoint(Self.BoletoBancario.Convenioboleto.get_Quantdiasprotesto);
                 Except
                    pdiasprotesto:=0;
                 End;
    
                 Pinstrucoes:=strreplace(pinstrucoes,'&taxamensal',formata_valor(ptaxamensal));
                 Pinstrucoes:=strreplace(pinstrucoes,'&valoraodia',formata_valor(pvaloraodia));
                 Pinstrucoes:=strreplace(pinstrucoes,'&dataprotesto',datetostr(strtodate(Self.VENCIMENTO)+pdiasprotesto));
                 Pinstrucoes:=strreplace(pinstrucoes,'&datavencimento',Self.VENCIMENTO);
                 Pinstrucoes:=strreplace(pinstrucoes,'&taxabanco',Formata_valor(PtaxaBanco));
                 
                 //****************************************************************
                 Self.BoletoBancario.Submit_Instrucoes(Pinstrucoes);
                 Self.BoletoBancario.Status:=dsedit;
                 Self.BoletoBancario.Submit_Situacao('I');
                 Self.BoletoBancario.Submit_ValorDoc(floattostr(Strtofloat(Self.Get_VALOR)+PTaxaBanco));
                 Self.BoletoBancario.Submit_Vencimento(Self.VENCIMENTO);
                 Self.BoletoBancario.Submit_DataProc(datetostr(now));
                 Self.BoletoBancario.Submit_DataDoc(datetostr(now));
                 Self.BoletoBancario.Submit_NumeroDoc(Self.Titulo.Get_CODIGO);

                 If (Self.BoletoBancario.Salvar(False,'')=False)
                 Then Begin
                            messagedlg('Erro na tentativa de Atualizar o Boleto '+Self.boletobancario.get_codigo+' para impresso!',mterror,[mbok],0);
                            exit;
                 End;

                 if (PTaxaBanco>0)
                 Then Begin
                           PcodigoLanctoJuros:='';
                           If (ObjLancamentotemp.LancamentoAutomatico('J',StrPend[cont],floattostr(PTaxaBanco),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',PcodigoLanctoJuros,False)=False)
                           Then Begin
                                     Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                                     exit;
                           End;
                 End;
             //***********************************************************
             End;//for STRLPENDENCIAS
             FdataModulo.IBTransaction.CommitRetaining;


             try
                //imprimindo os boletos
                if (FConfiguraBoleto.AbrePosicoesIni(Self.boletobancario.Convenioboleto.Get_CodigoBanco)=False)
                Then exit;


                FDataModulo.ComponenteRdPrint.abrir;

                if (FDataModulo.ComponenteRdPrint.setup=False)
                then begin
                         FDataModulo.ComponenteRdPrint.fechar;
                         exit;
                End;

                Try
                    Saltar:=False;
                    for cont:=0 to StrPend.count-1 do
                    Begin
                         Self.ZerarTabela;
                         Self.LocalizaCodigo(StrPend[cont]);
                         Self.TabelaparaObjeto;
                         Self.ImprimeBoletoPreImpresso(saltar);
                         saltar:=True;
                    End;
                    
                Finally
                  FDataModulo.ComponenteRdPrint.fechar;
                End;
             Except
                   Messagedlg('Erro na tentativa de imprimir os Boletos!',mterror,[mbok],0);
                   exit;
             End;

             Messagedlg('Gera��o Conclu�da com Sucesso!',mtInformation,[mbok],0);
          Finally
                freeandnil(StrBoleto);
                ObjLancamentoTemp.Free;
                FDataModulo.IBTransaction.RollbackRetaining;
          End;
     End;
end;


procedure TObjPendencia.MostraClientesInadimplesPorContaGer(PCredorDevedor, PCodigoCredorDevedor, PdataInicial, PDataFinal: string;Var PStrGrid:TStringGrid;OpcaoExtra:string);
begin

     PStrGrid.RowCount:=2;
     PStrGrid.FixedRows:=1;
     PStrGrid.fixedcols:=1;
     PStrGrid.Rows[0].clear;
     PStrGrid.Rows[1].clear;


     With Self.ObjDataset do
     Begin
          if (OpcaoExtra='T')//agrupo por Titulo
          Then Begin
                   PStrGrid.RowCount:=2;
                   PStrGrid.colcount:=5;
                   PStrGrid.Rows[0].clear;
                   PStrGrid.Rows[1].clear;

                   // Localizo as pessoa que tem pendencias com Saldo nas datas informadas
                   Close;
                   SelectSQL.Clear;
                   SelectSQL.Add('Select Tabpendencia.titulo, tabcredordevedor.tabela, tabtitulo.codigocredordevedor,count(Tabpendencia.codigo) as CONTA,sum(tabpendencia.saldo) as SOMA');
                   SelectSQL.Add('from TabPendencia');
                   SelectSQL.Add('join TabTitulo on tabtitulo.codigo = tabpendencia.titulo');
                   SelectSQL.Add('join Tabcredordevedor on tabcredordevedor.codigo = tabtitulo.credordevedor');
                   SelectSQL.Add('join TabContaGer on TabContaGer.Codigo = Tabtitulo.ContaGerencial');
                   SelectSQL.Add('Where TabPendencia.Saldo > 0');
                   SelectSQL.Add('and   TabPendencia.Vencimento >= '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PdataInicial))+#39);
                   SelectSQL.Add('and   TabPendencia.Vencimento <= '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PdataFinal))+#39);
                   SelectSQL.Add('and   TabContaGer.Tipo = ''C'' ');
                   
                   if (PcredorDevedor<>'')
                   Then SelectSQL.Add('and  TabCredorDevedor.Codigo = '+PCredorDevedor);

                   if (PCodigoCredorDevedor <> '')
                   then  SelectSQL.Add('and  TabTitulo.CodigoCredorDevedor  = '+PCodigoCredorDevedor);

                   SelectSQL.Add('Group by Tabpendencia.titulo, tabcredordevedor.tabela, tabtitulo.codigocredordevedor');
                   SelectSQL.Add('order by tabcredordevedor.tabela, tabtitulo.codigocredordevedor');
                   Open;

                   if (RecordCount = 0)
                   then Begin
                             MensagemErro('Nenhum dado selecionado na pesquisa');
                             exit;
                   end;
                   
                   //Para cada titulo eu precido saber o nome do cliente
                   While not(eof)do
                   Begin

                         Self.ObjQlocal.Close;
                         Self.ObjQlocal.SQL.Clear;
                         Self.ObjQlocal.SQL.Add('Select Nome');
                         Self.ObjQlocal.SQL.Add('from '+fieldbyname('Tabela').AsString);
                         Self.ObjQlocal.SQL.Add('Where codigo = '+fieldbyname('CodigoCredorDevedor').AsString);
                         Self.ObjQlocal.Open;

                         PStrGrid.Cells[1,PStrGrid.RowCount-1]:=Self.ObjQlocal.fieldbyname('Nome').AsString;
                         PStrGrid.Cells[2,PStrGrid.RowCount-1]:=fieldbyname('Titulo').AsString;
                         PStrGrid.Cells[3,PStrGrid.RowCount-1]:=fieldbyname('conta').AsString;
                         PStrGrid.Cells[4,PStrGrid.RowCount-1]:=completapalavra_a_esquerda(formata_valor(fieldbyname('Soma').AsString),12,' ');

                         PStrGrid.RowCount:=PStrGrid.RowCount+1;
                         next;
                   end;
                   PStrGrid.Cells[0,0]:='>>T';
                   PStrGrid.Cells[1,0]:='CLIENTE/FORNECEDOR';
                   PStrGrid.Cells[2,0]:='T�TULO';
                   PStrGrid.Cells[3,0]:='QTDE PEND';
                   PStrGrid.Cells[4,0]:='SOMA SALDO';



          End;


          if (OpcaoExtra='P')//pendencia a pendencia
          Then Begin
                   PStrGrid.RowCount:=2;
                   PStrGrid.colcount:=6;
                   PStrGrid.Rows[0].clear;
                   PStrGrid.Rows[1].clear;

                   // Localizo as pessoa que tem pendencias com Saldo nas datas informadas
                   Close;
                   SelectSQL.Clear;
                   SelectSQL.Add('Select tabpendencia.titulo,tabcredordevedor.tabela, tabtitulo.codigocredordevedor,tabpendencia.vencimento,Tabpendencia.codigo as pendencia,tabpendencia.saldo');
                   SelectSQL.Add('from TabPendencia');
                   SelectSQL.Add('join TabTitulo on tabtitulo.codigo = tabpendencia.titulo');
                   SelectSQL.Add('join Tabcredordevedor on tabcredordevedor.codigo = tabtitulo.credordevedor');
                   SelectSQL.Add('join TabContaGer on TabContaGer.Codigo = Tabtitulo.ContaGerencial');
                   SelectSQL.Add('Where TabPendencia.Saldo > 0');
                   SelectSQL.Add('and   TabPendencia.Vencimento >= '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PdataInicial))+#39);
                   SelectSQL.Add('and   TabPendencia.Vencimento <= '+#39+FormatDateTime('mm/dd/yyyy', StrToDate(PdataFinal))+#39);
                   SelectSQL.Add('and   TabContaGer.Tipo = ''C'' ');
                   
                   if (PcredorDevedor<>'')
                   Then SelectSQL.Add('and  TabCredorDevedor.Codigo = '+PCredorDevedor);

                   if (PCodigoCredorDevedor <> '')
                   then  SelectSQL.Add('and  TabTitulo.CodigoCredorDevedor  = '+PCodigoCredorDevedor);

                   SelectSQL.Add('order by tabcredordevedor.tabela,tabtitulo.codigocredordevedor,tabpendencia.vencimento');
                   Open;

                   if (RecordCount = 0)
                   then Begin
                             MensagemErro('Nenhum dado selecionado na pesquisa');
                             exit;
                   end;
                   
                   //Para cada titulo eu precido saber o nome do cliente
                   While not(eof)do
                   Begin

                         Self.ObjQlocal.Close;
                         Self.ObjQlocal.SQL.Clear;
                         Self.ObjQlocal.SQL.Add('Select Nome');
                         Self.ObjQlocal.SQL.Add('from '+fieldbyname('Tabela').AsString);
                         Self.ObjQlocal.SQL.Add('Where codigo = '+fieldbyname('CodigoCredorDevedor').AsString);
                         Self.ObjQlocal.Open;

                         PStrGrid.Cells[1,PStrGrid.RowCount-1]:=Self.ObjQlocal.fieldbyname('Nome').AsString;
                         PStrGrid.Cells[2,PStrGrid.RowCount-1]:=fieldbyname('titulo').AsString;
                         PStrGrid.Cells[3,PStrGrid.RowCount-1]:=fieldbyname('pendencia').AsString;
                         PStrGrid.Cells[4,PStrGrid.RowCount-1]:=fieldbyname('vencimento').AsString;
                         PStrGrid.Cells[5,PStrGrid.RowCount-1]:=completapalavra_a_esquerda(formata_valor(fieldbyname('saldo').AsString),12,' ');

                         PStrGrid.RowCount:=PStrGrid.RowCount+1;
                         next;
                   end;
                   PStrGrid.Cells[0,0]:='>>P';
                   PStrGrid.Cells[1,0]:='CLIENTE/FORNECEDOR';
                   PStrGrid.Cells[2,0]:='TITULO';
                   PStrGrid.Cells[3,0]:='PEND';
                   PStrGrid.Cells[4,0]:='VENCIMENTO';
                   PStrGrid.Cells[5,0]:='SALDO';


          End;
     End;//with
          
     PStrGrid.RowCount:=PStrGrid.RowCount-1;
     AjustaLArguraColunaGrid(PStrGrid);

end;


procedure TObjPendencia.EmiteCartaCobranca(var PStrGrid: TStringGrid);
Var
VelhoWord, NovoWord: variant;
WinWord, Doc: Variant;
Pnomecliente,Pvencimentos,Texto,temp1,temp2:String;
cont,contaimprime,quantsel:integer;
PImprimeCartaporTitulo:boolean;
PsomaSaldo:currency;
PpendenciasEscolhidas,PTitulosEscolhidos,PretornaPendencia:TStringList;
QTitulo:Tibquery;
PcredorDevedor,PCodigoCredorDevedor:string;
totalDosTitulos:Currency;
begin

     Try
        PTitulosEscolhidos:=TStringList.Create;
        PpendenciasEscolhidas:=TStringList.Create;
        PretornaPendencia:=TStringList.Create;
        QTitulo:=Tibquery.create(nil);
        QTitulo.database:=FdataModulo.IBDatabase;
     Except
        MensagemErro('Erro na tentativa de criar a String List PtitulosEscolhidos');
        exit;
     End;

     Try
         //na primeira celula da coluna zero eu tenho >>T ou >>P
         //indicando se esta agrupado por titulo ou por pendencia


         PTitulosEscolhidos.clear;
         PpendenciasEscolhidas.clear;
    
         if (PStrGrid.Cells[0,0]='')
         Then Begin
                   MensagemAviso('Nenhuma Informa��o foi selecionada');
                   exit;
         End;

         quantsel:=0;
         for cont:=1 to PStrGrid.RowCount-1 do
         Begin
              if (PStrGrid.Cells[0,cont]='>>')
              Then Begin
                        inc(Quantsel,1);
                        if (PStrGrid.Cells[0,0]='>>T')
                        Then PTitulosEscolhidos.add(PStrGrid.Cells[2,cont])
                        Else Begin
                                  PTitulosEscolhidos.add(PStrGrid.Cells[2,cont]);
                                  PpendenciasEscolhidas.add(PStrGrid.Cells[3,cont]);
                                  
                        end;
              End;
         End;

         if (quantsel=0)
         Then Begin
                   MensagemAviso('Nenhuma Informa��o foi selecionada');
                   exit;
         End;

         //nesse caso as selecoes sao por titulo
         //ou seja, posso imprimir uma carta por titulo
         //ou por todos os titulos na mesma carta
         With FOpcaorel do
         Begin
              RgOpcoes.Items.clear;
              RgOpcoes.Items.add('Uma carta para todos os t�tulos selecionados');
              RgOpcoes.Items.add('Uma carta para cada t�tulo selecionado');
              showmodal;

              if (tag=0)
              Then exit;

              if (RgOpcoes.ItemIndex=0)
              Then PImprimeCartaporTitulo:=False
              Else PImprimeCartaporTitulo:=true;
         End;

         QTitulo.close;
         QTitulo.Sql.clear;
         QTitulo.Sql.add('Select * from tabtitulo');
         QTitulo.Sql.add('where (codigo='+PTitulosEscolhidos[0]);

         for cont:=1 to PTitulosEscolhidos.Count-1 do//varrendo o grid
         Begin
              QTitulo.Sql.add('or codigo='+PTitulosEscolhidos[cont]);
         End;
         QTitulo.Sql.add(')');
         QTitulo.sql.add('order by tabtitulo.credordevedor,tabtitulo.codigocredordevedor,tabtitulo.codigo');
         QTitulo.open;
         QTitulo.first;

         PcredorDevedor:=QTitulo.fieldbyname('credordevedor').asstring;
         PCodigoCredorDevedor:=QTitulo.fieldbyname('codigocredordevedor').AsString;
          // **********************Inicializando o Objeto**************************
         VelhoWord:=AdiquiraOuCrieObjeto('Word.Basic');
         NovoWord :=AdiquiraOuCrieObjeto('Word.Application');
         // Tornar o word visivel
         NovoWord.Visible := True;
         Doc:= NovoWord.Documents.Open(ExtractFilePath(Application.ExeName)+'\ModeloContrato\Carta_Cobranca.doc');

         totalDosTitulos:=0;
         While not(QTitulo.eof) do//titulo
         Begin
              //cada registro � um t�tulo
              PnomeCliente:=Self.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(QTitulo.fieldbyname('CREDORDEVEDOR').asstring,QTitulo.Fieldbyname('CODIGOCREDORDEVEDOR').asstring);

              if ((PImprimeCartaporTitulo=False) and ((PcredorDevedor<>QTitulo.fieldbyname('credordevedor').asstring) or (PcodigoCredorDevedor<>QTitulo.fieldbyname('codigocredordevedor').AsString)))//carta por credor/devedor
              or ((PImprimeCartaporTitulo=True) and (Qtitulo.bof=False))
              Then Begin
                        //Se for uma carta por titulo ele nao abre uma nova janela na primeira vez
                        //caso contrario todas as vezes ele passa por aqui, limpando as variaveis e criando uma nova janela

                        //Se for uma carta por credor/devedor
                        //Ele s� abre quando muda de cliente

                        //*************TITULOS********************************************
                        Doc.Content.Find.Execute(FindText := '[DATA]', ReplaceWith :='');
                        Doc.Content.Find.Execute(FindText := '[NOMECLIENTE]', ReplaceWith :='');
                        Doc.Content.Find.Execute(FindText := '[TITULO]', ReplaceWith :='');
                        Doc.Content.Find.Execute(FindText := '[EMISSAO]', ReplaceWith :='');
                        Doc.Content.Find.Execute(FindText := '[VALORTITULO]', ReplaceWith :='');
                        Doc.Content.Find.Execute(FindText := '[TOTAL_DA_DIVIDA]', ReplaceWith :='');
                        Doc.Content.Find.Execute(FindText := '[VENCIMENTOS]', ReplaceWith := '');
                        //*************PENDENCIAS********************
                        Doc.Content.Find.Execute(FindText := '[VENCIMENTO&SALDO]',ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[PENDENCIA]', ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[VENCIMENTO]',ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[SALDO]', ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[PENDENCIA&VENCIMENTO&SALDO]', ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[TITULO&PENDENCIA&VENCIMENTO&SALDO]', ReplaceWith := '');
                        //*******************************
                        Doc.Content.Find.Execute(FindText := '[VENCIMENTO&SALDO&ENTER]',ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[PENDENCIA&ENTER]', ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[VENCIMENTO&ENTER]',ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[SALDO&ENTER]', ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[PENDENCIA&VENCIMENTO&SALDO&ENTER]', ReplaceWith := '');
                        Doc.Content.Find.Execute(FindText := '[TITULO&PENDENCIA&VENCIMENTO&SALDO&ENTER]', ReplaceWith := '');
                        //*******************************


                        //trocou de cliente
                        //abro uma nova carta
                        PcredorDevedor:=QTitulo.fieldbyname('credordevedor').asstring;
                        PCodigoCredorDevedor:=QTitulo.fieldbyname('codigocredordevedor').AsString;
                        // **********************Inicializando o Objeto**************************
                        VelhoWord:=AdiquiraOuCrieObjeto('Word.Basic');
                        NovoWord :=AdiquiraOuCrieObjeto('Word.Application');
                        // Tornar o word visivel
                        NovoWord.Visible := True;
                        Doc:= NovoWord.Documents.Open(ExtractFilePath(Application.ExeName)+'\ModeloContrato\Carta_Cobranca.doc');
              End;

              Self.ObjQlocal.close;
              Self.ObjQlocal.sql.clear;
              if (PStrGrid. cells[0,0]='>>T')
              Then Self.ObjQlocal.sql.add('Select sum(saldo) as SOMASALDO from tabpendencia where titulo='+Qtitulo.fieldbyname('codigo').asstring)
              Else Begin
                        PretornaPendencia.clear;
                        //somente as pendencias escolhidas
                        if (Self.RetornaPendenciasTitulo(Qtitulo.fieldbyname('codigo').asstring,PTitulosEscolhidos,PpendenciasEscolhidas,PretornaPendencia)=False)
                        Then Begin
                                  MensagemErro('N�o foi encontrado pend�ncias para o t�tulo '+Qtitulo.fieldbyname('codigo').asstring);
                                  exit;
                        End;
                        //******************************************************
                        Self.ObjQlocal.sql.add('Select sum(saldo) as SOMASALDO from tabpendencia ');
                        Self.ObjQlocal.sql.add('Where (Tabpendencia.codigo='+PretornaPendencia[0]);
                        for cont:=1 to PretornaPendencia.count-1 do
                        begin
                             Self.ObjQlocal.sql.add('or tabpendencia.codigo='+Pretornapendencia[cont]);
                        End;
                        Self.ObjQlocal.sql.add(')');
              End;

              Self.ObjQlocal.open;
              PSomaSaldo:=Self.ObjQlocal.Fieldbyname('somasaldo').asfloat;

              //********Agora � s� substituir a palavra chave**************************
              for contaimprime:=1 to 10 do
              Begin
                  Doc.Content.Find.Execute(FindText := '[DATA]', ReplaceWith := DateToStr(Now));
                  Doc.Content.Find.Execute(FindText := '[NOMECLIENTE]', ReplaceWith := PNomeCliente);
              End;

              Doc.Content.Find.Execute(FindText := '[TITULO]', ReplaceWith := Qtitulo.Fieldbyname('CODIGO').asstring+',[TITULO]');
              Doc.Content.Find.Execute(FindText := '[EMISSAO]', ReplaceWith := Qtitulo.Fieldbyname('emissao').asstring+',[EMISSAO]');
              Doc.Content.Find.Execute(FindText := '[VALORTITULO]', ReplaceWith := Formata_valor(Qtitulo.Fieldbyname('valor').asstring)+',[VALORTITULO]');

              {jonas 12/04/2011 11:29}
              totalDosTitulos:=totalDosTitulos+PsomaSaldo;
              //Doc.Content.Find.Execute(FindText := '[TOTAL_DA_DIVIDA]', ReplaceWith := formata_valor(PsomaSaldo)+',[TOTAL_DA_DIVIDA]');
              {-}

              //gerando uma linha com os vencimentos
              Self.ObjQlocal.close;
              Self.ObjQlocal.sql.clear;
              Self.ObjQlocal.sql.add('Select tabpendencia.codigo as pendencia,tabpendencia.vencimento,tabpendencia.saldo from tabpendencia');

              if (PStrGrid. cells[0,0]='>>T')
              Then Self.ObjQlocal.sql.add('where tabpendencia.titulo='+Qtitulo.Fieldbyname('codigo').asstring)
              Else Begin
                        PretornaPendencia.clear;
                        //somente as pendencias escolhidas
                        if (Self.RetornaPendenciasTitulo(Qtitulo.fieldbyname('codigo').asstring,PTitulosEscolhidos,PpendenciasEscolhidas,PretornaPendencia)=False)
                        Then Begin
                                  MensagemErro('N�o foi encontrado pend�ncias para o t�tulo '+Qtitulo.fieldbyname('codigo').asstring);
                                  exit;
                        End;
                        //******************************************************
                        Self.ObjQlocal.sql.add('Where (Tabpendencia.codigo='+PretornaPendencia[0]);
                        for cont:=1 to PretornaPendencia.count-1 do
                        begin
                             Self.ObjQlocal.sql.add('or tabpendencia.codigo='+Pretornapendencia[cont]);
                        End;
                        Self.ObjQlocal.sql.add(')');
              End;
              Self.ObjQlocal.sql.add('and tabpendencia.saldo>0');
              Self.ObjQlocal.open;
              Self.ObjQlocal.first;
              Pvencimentos:='';
              While not (Self.ObjQlocal.eof) do
              Begin
                   Pvencimentos:=Pvencimentos+Self.ObjQlocal.fieldbyname('Vencimento').AsString+', ';
                   Self.ObjQlocal.Next;
              end;
              Self.ObjQlocal.first;

              temp1:=Pvencimentos;
              temp2:=Pvencimentos;
              While (temp2<>'') do
              begin
                  DividirValorCOMSEGUNDASEMTAMANHO(pvencimentos,200,temp1,temp2);
                  pvencimentos:=temp1;
                  //*************PENDENCIAS********
                  Doc.Content.Find.Execute(FindText := '[VENCIMENTOS]', ReplaceWith := Pvencimentos+',[VENCIMENTOS]');
                  pvencimentos:=temp2;
              end;



              While not(Self.ObjQlocal.eof) do
              Begin
                   //**************************
                   Doc.Content.Find.Execute(FindText := '[PENDENCIA]', ReplaceWith := Self.ObjQlocal.FieldByname('PENDENCIA').asstring+'  '+'[PENDENCIA]');
                   Doc.Content.Find.Execute(FindText := '[VENCIMENTO]', ReplaceWith := Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+'  '+'[VENCIMENTO]');
                   Doc.Content.Find.Execute(FindText := '[SALDO]', ReplaceWith := formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+'  '+'[SALDO]');
                   Doc.Content.Find.Execute(FindText := '[VENCIMENTO&SALDO]', ReplaceWith := Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+' '+Formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+'  '+'[VENCIMENTO&SALDO]');
                   Doc.Content.Find.Execute(FindText := '[PENDENCIA&VENCIMENTO&SALDO]', ReplaceWith := Self.ObjQlocal.FieldByname('PENDENCIA').asstring+' '+Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+' '+Formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+'  '+'[PENDENCIA&VENCIMENTO&SALDO]');
                   Doc.Content.Find.Execute(FindText := '[TITULO&PENDENCIA&VENCIMENTO&SALDO]', ReplaceWith := QTitulo.fieldbyname('codigo').asstring+' '+Self.ObjQlocal.FieldByname('PENDENCIA').asstring+' '+Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+' '+Formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+'  '+'[TITULO&PENDENCIA&VENCIMENTO&SALDO]');
                   //**************************
                   Doc.Content.Find.Execute(FindText := '[PENDENCIA&ENTER]', ReplaceWith := Self.ObjQlocal.FieldByname('PENDENCIA').asstring+#13+'[PENDENCIA&ENTER]');
                   Doc.Content.Find.Execute(FindText := '[VENCIMENTO&ENTER]', ReplaceWith := Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+#13+'[VENCIMENTO&ENTER]');
                   Doc.Content.Find.Execute(FindText := '[SALDO&ENTER]', ReplaceWith := formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+#13+'[SALDO&ENTER]');
                   Doc.Content.Find.Execute(FindText := '[VENCIMENTO&SALDO&ENTER]', ReplaceWith :=Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+' '+Formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+#13+'[VENCIMENTO&SALDO&ENTER]');
                   Doc.Content.Find.Execute(FindText := '[PENDENCIA&VENCIMENTO&SALDO&ENTER]', ReplaceWith := Self.ObjQlocal.FieldByname('PENDENCIA').asstring+' '+Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+' '+Formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+'  '+#13+'[PENDENCIA&VENCIMENTO&SALDO&ENTER]');
                   Doc.Content.Find.Execute(FindText := '[TITULO&PENDENCIA&VENCIMENTO&SALDO&ENTER]', ReplaceWith := QTitulo.fieldbyname('codigo').asstring+' '+Self.ObjQlocal.FieldByname('PENDENCIA').asstring+' '+Self.ObjQlocal.FieldByname('VENCIMENTO').asstring+' '+Formata_valor(Self.ObjQlocal.FieldByname('saldo').asstring)+'  '+#13+'[TITULO&PENDENCIA&VENCIMENTO&SALDO&ENTER]');
                   //**************************
                   Self.ObjQlocal.next;
              End;//while das pendencias de cada titulo

              QTitulo.next;//dos titulos
         End;//while dos titulos

         {jonas 12/04/2011 11:32}
         Doc.Content.Find.Execute(FindText := '[TOTAL_DA_DIVIDA]', ReplaceWith := formata_valor(formata_valor(totalDosTitulos))+',[TOTAL_DA_DIVIDA]');
         {-}
         
         //*************TITULOS********************************************
         Doc.Content.Find.Execute(FindText := '[DATA]', ReplaceWith :='');
         Doc.Content.Find.Execute(FindText := '[NOMECLIENTE]', ReplaceWith :='');
         Doc.Content.Find.Execute(FindText := '[TITULO]', ReplaceWith :='');
         Doc.Content.Find.Execute(FindText := '[EMISSAO]', ReplaceWith :='');
         Doc.Content.Find.Execute(FindText := '[VALORTITULO]', ReplaceWith :='');
         Doc.Content.Find.Execute(FindText := '[TOTAL_DA_DIVIDA]', ReplaceWith :='');
         Doc.Content.Find.Execute(FindText := '[VENCIMENTOS]', ReplaceWith := '');
         //*************PENDENCIAS********************
         Doc.Content.Find.Execute(FindText := '[VENCIMENTO&SALDO]',ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[PENDENCIA]', ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[VENCIMENTO]',ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[SALDO]', ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[PENDENCIA&VENCIMENTO&SALDO]', ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[TITULO&PENDENCIA&VENCIMENTO&SALDO]', ReplaceWith := '');
         //*******************************
         Doc.Content.Find.Execute(FindText := '[VENCIMENTO&SALDO&ENTER]',ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[PENDENCIA&ENTER]', ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[VENCIMENTO&ENTER]',ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[SALDO&ENTER]', ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[PENDENCIA&VENCIMENTO&SALDO&ENTER]', ReplaceWith := '');
         Doc.Content.Find.Execute(FindText := '[TITULO&PENDENCIA&VENCIMENTO&SALDO&ENTER]', ReplaceWith := '');
         //*******************************

     Finally
            Freeandnil(PTitulosEscolhidos);
            Freeandnil(ppendenciasescolhidas);
            Freeandnil(QTitulo);
            Freeandnil(PretornaPendencia);
     End;
End;

procedure TObjPendencia.Imprime_titulos_Aberto_ExcluirCG(
  PtipoConta: string);
var
STRcontaGerencial:TStringList;
tmpSomaSaldo:Currency;
DataInicial,DataFinal:string;
cont:integer;
begin
     Try
        STRcontaGerencial:=TStringList.create;
        STRcontaGerencial.clear;
     Except
           Messagedlg('Erro na tentativa de Criar a StringList "STRcontaGerencial"',mterror,[mbok],0);
           exit;
     End;

Try

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo05.Enabled:=True;

          LbGrupo01.caption:='Data 01';
          LbGrupo02.caption:='Data 02';
          edtgrupo01.EditMask:=mascaradata;
          edtgrupo02.EditMask:=mascaradata;

          edtgrupo05.EditMask:='';
          if(PtipoConta='C')
          Then edtgrupo05.OnKeyDown:=Self.titulo.EdtcontagerencialRECEBERKeyDown
          Else edtgrupo05.OnKeyDown:=Self.titulo.EdtcontagerencialPAGARKeyDown;

          edtgrupo05.color:=$005CADFE;
          
          LbGrupo05.caption:='Excluir Contas Gerenciais';

          showmodal;
          
          If tag=0
          Then exit;

          Try
             Datainicial:='';
             if (Trim(comebarra(edtgrupo01.text))<>'')
             Then Begin
                      Strtodate(edtgrupo01.text);
                      DataInicial:=edtgrupo01.text;
             End;
          Except
             Mensagemerro('Data inicial Inv�lida'+#13+'Deixe em branco ou digite uma data v�lida');
             exit;
          End;

          Try
             Datafinal:='';
             if (Trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                      Strtodate(edtgrupo02.text);
                      DataFinal:=edtgrupo02.text;
             End;
          Except
             Mensagemerro('Data Final Inv�lida'+#13+'Deixe em branco ou digite uma data v�lida');
             exit;
          End;

          if (explodestr(edtgrupo05.text,STRcontaGerencial,';','integer')=False)
          then exit;

          if (STRcontaGerencial.count>0)
          Then Begin
                    for cont:=0 to STRcontaGerencial.Count-1 do
                    Begin
                         if (Self.Titulo.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial[cont])=False)
                         then begin
                                   MensagemErro('Conta gerencial '+STRcontaGerencial[cont]+' n�o encontrada');
                                   exit;
                         End;
                    End;
          End;
     End;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSql.add('select tabtitulo.codigo as CODTITULO,tabtitulo.NumDcto,tabtitulo.Historico,contagerencial,');
          SelectSql.add('tabpendencia.codigo CODPENDENCIA,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo');
          SelectSql.add('from tabpendencia left join tabtitulo');
          SelectSql.add('on tabpendencia.titulo=tabtitulo.codigo');
          SelectSql.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SelectSql.add(' where Tabpendencia.Saldo>0 ');
          if (DataInicial<>'')
          Then SelectSql.add(' and Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(datainicial))+#39);
          if (Datafinal<>'')
          then SelectSql.add(' and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(dataFinal))+#39);
          SelectSql.add(' and tabcontager.tipo='+#39+PtipoConta+#39);
          if (STRcontaGerencial.count>0)
          then begin
                    for cont:=0 to STRcontaGerencial.count-1 do
                    Begin
                        SelectSQL.add(' and TabTitulo.ContaGerencial<>'+STRcontaGerencial[cont]);
                    End;
          End;
          SelectSQL.add('Order by tabpendencia.vencimento');
          open;
          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          With FreltxtRDPRINT do
          Begin

               ConfiguraImpressao;
               RDprint.TamanhoQteColunas:=130;
               RDprint.FonteTamanhoPadrao:=S17cpp;
               RDprint.Abrir;
               if(RDprint.setup=False)
               then begin
                         RDprint.fechar;
                         exit;
               End;


               Linhalocal:=3;

               If (PtipoConta='D')
               Then RDprint.Impc(linhalocal,65,Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A PAGAR',[negrito])
               Else RDprint.Impc(linhalocal,65,Self.NumeroRelatorio+'RELAT�RIO DE T�TULOS A RECEBER',[negrito]);
               incrementalinha(2);

               if (DataInicial<>'') or (DataFinal<>'')
               Then Begin
                         if (datainicial<>'') and (datafinal<>'')
                         then RDprint.ImpF(linhalocal,1,'Vencimento >= '+datainicial+' e <= '+DataFinal,[negrito])
                         Else Begin
                                   if (DataInicial<>'')
                                   Then RDprint.ImpF(linhalocal,1,'Vencimento >= '+datainicial,[negrito])
                                   Else RDprint.ImpF(linhalocal,1,'Vencimento <= '+datafinal,[negrito])
                         End;
                         IncrementaLinha(1);
               end;

               if (STRcontaGerencial.Count>0)
               then begin
                        Self.Titulo.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial[0]);
                        Self.Titulo.CONTAGERENCIAL.TabelaparaObjeto;

                        RDprint.ImpF(linhalocal,1,'Contas Gerenciais n�o Inclusas: ',[negrito]);
                        RDprint.Imp (linhalocal,33,CompletaPalavra(Self.Titulo.CONTAGERENCIAL.get_codigo+'-'+Self.Titulo.CONTAGERENCIAL.Get_Nome,97,' '));
                        IncrementaLinha(1);
                        for cont:=1 to STRcontaGerencial.count-1 do
                        begin
                             Self.Titulo.CONTAGERENCIAL.LocalizaCodigo(STRcontaGerencial[cont]);
                             Self.Titulo.CONTAGERENCIAL.TabelaparaObjeto;
                             RDprint.Imp (linhalocal,33,CompletaPalavra(Self.Titulo.CONTAGERENCIAL.get_codigo+'-'+Self.Titulo.CONTAGERENCIAL.Get_Nome,97,' '));
                             IncrementaLinha(1);
                        End;
                        IncrementaLinha(1);
               End;


               
               
               RDprint.ImpF(linhalocal,1,CompletaPalavra('T�TULO',6,' ')+' '+
                                         CompletaPalavra('HIST�RICO',80,' ')+' '+
                                         CompletaPalavra('N�DCTO',6,' ')+' '+
                                         CompletaPalavra('CNTGER',6,' ')+' '+
                                         CompletaPalavra('PEND.',6,' ')+' '+
                                         CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('SALDO',9,' '),[negrito]);
               incrementalinha(1);
               desenhalinha;
               incrementalinha(1);
               
               tmpSomaSaldo:=0;
               While not (Self.Objdataset.eof) do
               Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(FIELDBYNAME('CODTITULO').ASSTRING,6,' ')+' '+
                                             CompletaPalavra(FIELDBYNAME('HISTORICO').ASSTRING,80,' ')+' '+
                                             CompletaPalavra(FIELDBYNAME('NUMDCTO').ASSTRING,6,' ')+' '+
                                             CompletaPalavra(FIELDBYNAME('CONTAGERENCIAL').ASSTRING,6,' ')+' '+
                                             CompletaPalavra(FIELDBYNAME('CODPENDENCIA').ASSTRING,6,' ')+' '+
                                             CompletaPalavra(FIELDBYNAME('VENCIMENTO').ASSTRING,10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDO').ASSTRING),9,' '));
                    IncrementaLinha(1);
                    tmpSomaSaldo:=tmpSomaSaldo+FIELDBYNAME('SALDO').asfloat;
                    Self.Objdataset.NEXT;
               end;
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,1,'TOTAL R$ '+formata_valor(FloatToStr(tmpSomaSaldo)),[negrito]);
               rdprint.fechar;
          End;
     End;      

Finally
       Freeandnil(STRcontaGerencial);
End;

end;


procedure TObjPendencia.Imprime_Pendencia_por_vencimento_ContaGerencial;
var
Pdatainicial,PdataFinal:TDate;
PSaldo:string;
StrContaGer:TStringList;
Cont:integer;
PContagerencial:string;
PsomaComSaldoCG,PsomaSemSaldoCG:Currency;
begin
     try
        StrContaGer:=TStringList.Create;
     Except
           mensagemerro('Erro na tentativa de Criar a StringList StrContager');
           exit;
     End;
Try

     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          LbGrupo01.caption:='Vencimento Inicial';
          LbGrupo02.caption:='Vencimento Inicial';
          LbGrupo03.caption:='Conta gerencial';
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          edtgrupo03.OnKeyDown:=Self.titulo.EdtcontagerencialKeyDown;
          edtgrupo03.color:=$005CADFE;

          Showmodal;

          if (Validadata(1,pdatainicial,False)=False)
          Then exit;

          if (Validadata(2,pdatafinal,False)=False)
          Then exit;

          if (edtgrupo03.text<>'')
          then Begin
                    if (ExplodeStr(edtgrupo03.text,StrContaGer,';','integer')=False)
                    then exit;

                    for cont:=0 to StrContaGer.count-1 do
                    Begin
                         if (Self.Titulo.CONTAGERENCIAL.LocalizaCodigo(StrContaGer[cont])=False)
                         Then Begin
                                   mensagemerro('Conta gerencial '+StrContaGer[cont]+' n�o encontrada');
                                   exit;
                         End;
                    End;
          End;
     End;//with

     With Self.ObjDataset do
     begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select tabcontager.tipo,tabcontager.nome as NOMECONTAGERENCIAL,');
          SelectSQL.add('tabtitulo.contagerencial,tabPendencia.codigo as pendencia,tabtitulo.historico,Tabpendencia.vencimento,tabpendencia.valor,');
          SelectSQL.add('tabpendencia.saldo from TabPendencia');
          SelectSQL.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
          SelectSQL.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');

          //a linha abaixo foi adicionada especificamente para o bia - Isto retira do relatorio as vendas que tiveram cancelamento - 16/04/12 - rodolfo e gian
          SelectSQL.add('left join tablancamento  on (tablancamento.pendencia = tabpendencia.codigo)');

          SelectSQL.add('where tabpendencia.vencimento>='+#39+formatdatetime('mm/dd/yyyy',pdatainicial)+#39);
          SelectSQL.add('and tabpendencia.vencimento<='+#39+formatdatetime('mm/dd/yyyy',pdatafinal)+#39);

          //a linha abaixo foi adicionada especificamente para o bia - Isto retira do relatorio as vendas que tiveram cancelamento - 16/04/12 - rodolfo e gian
          SelectSQL.add('and (tablancamento.historico not like' + #39 + 'Cancelamento da venda do terreno%' + #39 + ' or tablancamento.historico is null)');

          if (StrContaGer.Count>0)
          then begin
                    SelectSQL.add('and (tabtitulo.contager='+StrContaGer[0]);
                    for cont:=1 to strcontager.count-1 do
                    begin
                         SelectSQL.add('or  tabtitulo.contager='+StrContaGer[cont]);
                    End;
                    SelectSQL.add(')');
          end;
          SelectSQL.Add('order by tabcontager.tipo,tabcontager.codigo,tabpendencia.vencimento,tabpendencia.saldo');

          //inputbox('','', SelectSQL.text);

          open;
          
          if (recordcount=0)
          then Begin
                    mensagemaviso('Nenhuma Informa��o foi selecionada');
                    exit;
          End;
          With FreltxtRDPRINT do
          begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.fechar;
                         exit;
               End;

               RDprint.Impc(linhalocal,45,Self.NumeroRelatorio+'PARCELAS POR VENCIMENTO E CONTA GERENCIAL',[negrito]);
               IncrementaLinha(2);

               RDprint.Impf(linhalocal,1,'Vencimento: '+DateToStr(Pdatainicial)+' a '+DateToStr(Pdatainicial),[negrito]);
               IncrementaLinha(2);

               if (StrContaGer.count>0)
               then Begin
                        Self.Titulo.CONTAGERENCIAL.LocalizaCodigo(StrContaGer[0]);
                        Self.Titulo.CONTAGERENCIAL.TabelaparaObjeto;

                        RDprint.Impf(linhalocal,1,CompletaPalavra('Conta Gerencial: '+Self.Titulo.CONTAGERENCIAL.get_codigo+'-'+Self.Titulo.CONTAGERENCIAL.Get_Nome,90,' '),[negrito]);
                        IncrementaLinha(2);

                        for cont:=1 To StrContaGer.count-1 do
                        Begin
                            Self.Titulo.CONTAGERENCIAL.LocalizaCodigo(StrContaGer[cont]);
                            Self.Titulo.CONTAGERENCIAL.TabelaparaObjeto;

                            RDprint.Impf(linhalocal,1,CompletaPalavra('                 '+Self.Titulo.CONTAGERENCIAL.get_codigo+'-'+Self.Titulo.CONTAGERENCIAL.Get_Nome,90,' '),[negrito]);
                            IncrementaLinha(2);
                        End;
               End;

               

               VerificaLinha;
               RDprint.Impf(linhalocal,1,CompletaPalavra('PEND',6,' ')+' '+
                                         CompletaPalavra('HIST�RICO',40,' ')+' '+
                                         CompletaPalavra('VENCTO',10,' ')+' '+
                                         CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                         CompletaPalavra_a_Esquerda('SALDO',12,' '),[negrito]);
               incrementalinha(1);

               DesenhaLinha;
               incrementalinha(1);

               PsomaComSaldoCG:=0;
               PsomaSemSaldoCG:=0;
               pcontagerencial:=Fieldbyname('contagerencial').asstring;
               VerificaLinha;
               if (Fieldbyname('tipo').asstring='D')
               then RDprint.ImpF(linhalocal,1,'PAGAR   - '+Fieldbyname('contagerencial').asstring+'-'+fieldbyname('nomecontagerencial').asstring,[negrito])
               Else RDprint.ImpF(linhalocal,1,'RECEBER - '+Fieldbyname('contagerencial').asstring+'-'+fieldbyname('nomecontagerencial').asstring,[negrito]);
               IncrementaLinha(2);

               While not(Self.ObjDataset.Eof) do
               begin
                    if (Pcontagerencial<>fieldbyname('contagerencial').asstring)
                    then Begin
                              IncrementaLinha(1);
                              
                              //Totalizando
                              VerificaLinha;
                              RDprint.ImpF(linhalocal,1,'Total com Saldo R$ '+formata_valor(PsomaComSaldoCG),[negrito]);
                              IncrementaLinha(1);

                              VerificaLinha;
                              RDprint.ImpF(linhalocal,1,'Total sem Saldo R$ '+formata_valor(PsomaSemSaldoCG),[negrito]);
                              IncrementaLinha(2);
                              
                              //***********************************************
                              PsomaComSaldoCG:=0;
                              PsomaSemSaldoCG:=0;
                              pcontagerencial:=Fieldbyname('contagerencial').asstring;
                              VerificaLinha;
                              if (Fieldbyname('tipo').asstring='D')
                              then RDprint.ImpF(linhalocal,1,'PAGAR   - '+Fieldbyname('contagerencial').asstring+'-'+fieldbyname('nomecontagerencial').asstring,[negrito])
                              Else RDprint.ImpF(linhalocal,1,'RECEBER - '+Fieldbyname('contagerencial').asstring+'-'+fieldbyname('nomecontagerencial').asstring,[negrito]);
                              IncrementaLinha(2);
                    End;

                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('pendencia').asstring,6,' ')+' '+
                                             CompletaPalavra(fieldbyname('historico').asstring,40,' ')+' '+
                                             CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),12,' ')+' '+
                                             CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('saldo').asstring),12,' '));
                    incrementalinha(1);

                    if (Fieldbyname('saldo').asfloat=0)
                    Then PsomaSemSaldoCG:=PsomaSemSaldoCG+Fieldbyname('valor').asfloat
                    else PsomaComSaldoCG:=PsomaComSaldoCG+Fieldbyname('saldo').asfloat;

                    Self.ObjDataset.next;
               end;//while
               desenhalinha;

               
               rdprint.Fechar;

          End;//with Frel
     End;//With Self.objdataset

Finally
       FreeAndNil(StrContaGer);
End;

end;

function TObjPendencia.RetornaPendenciasTitulo(Ptitulo: string;Ptitulos,Ppendencias,PretornaPendencia:TStringList): boolean;
var
   cont:integer;
begin
     result:=False;
     PretornaPendencia.clear;
     for cont:=0 to ptitulos.count-1 do
     Begin
          if (Ptitulos[cont]=ptitulo)
          Then Pretornapendencia.add(Ppendencias[cont]);
     End;
     
     if (ppendencias.count>0)
     then result:=true;
end;

procedure TObjPendencia.edtBoletoUsadoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.BoletoBancario.Get_PesquisaUsados,Self.BoletoBancario.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

function TObjPendencia.PegaDuplicata: string;
begin
     result:='';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.enabled:=True;
          LbGrupo01.caption:='Duplicata';
          edtgrupo01.OnKeyDown:=Self.edtduplicataKeydown;
          Showmodal;

          if (tag=0)
          Then exit;

          Result:=edtgrupo01.Text;
     end;

end;

procedure TObjPendencia.edtduplicataKeydown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa('Select * FROM VIEWDUPLICATA','Pesquisa de Duplicata de T�tulos',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('Duplicata').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;

function TObjPendencia.VerificaFuturosLancamentoNaPendencia_SistemaBIA(PLocadorLocatario, PPendencia, PValor, PValorComissao: string): Boolean;
Var PSaldo, PSOma:Currency;
begin
     // Antes de efetuar um lancto na pendencia eu preciso saber
     // se o que j� tem cadastrados para futuros lancamentos pooder�
     // deixar a pendencia Negativa

     // If Retornar FALSE � porque a verificou os lancamentos e porcebeiu que
     // naum d� mais pra lanncar nessa penencia


     // Quando eu voltar de ferias eu vejo isso aki, sen�o pode dar problema
     // enquanto eu estiver ausente

     RESULT:=TRUE;
     EXIT;


     Result:=false;
     if (PPendencia = '')
     then Begin
             Result:=true;
             exit;
     end;

     if (Self.LocalizaCodigo(PPendencia)=false)
     then Begin
            MensagemErro('Pendencia n�o encontrada');
            exit;
     end;
     Self.TabelaparaObjeto;
     PSaldo:=StrToCurr(Self.Get_Saldo);
     PSoma:=0;

     // Voui verificando a soma dos lancamentos at� agora naum sejam maior
     // que o saldo
     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.Lines.Add('O SALDO DA P�NDENCIA N�O PODE FICAR NEGATIVO. EFETUE O LAN�AMENTO DESEJADO EM UMA OUTRA PEND�NCIA');
     FmostraStringList.Memo.Lines.Add(' ');
     FmostraStringList.Memo.Lines.Add('LAN�AMENTOS ATUALMENTE EFETUADOS LIGADOS E ESSA PENDENCIA:');
     FmostraStringList.Memo.Lines.Add(' ');
     FmostraStringList.Memo.Lines.Add(CompletaPalavra('VALOR',60,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(Self.Get_VALOR), 12,' '));
     FmostraStringList.Memo.Lines.Add(CompletaPalavra('SALDO',60,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(Self.Get_SALDO), 12,' '));
     FmostraStringList.Memo.Lines.Add(' ');


     With Self.ObjDataset do
     Begin
          {// Lancamento na Pendencia
          Close;
          SelectSQL.Clear;
          SelectSQL.Add('Select TabLancamento.Historico, TabLancamento.Valor, TabTipoLancto.Classificacao');
          SelectSQL.Add('from TabLancamento');
          SelectSQL.Add('join TabTipoLancto on TabTipoLancto.Codigo = TabLancamento.TipoLancto');
          SelectSQL.Add('Where TabLancamento.Pendencia = '+PPendencia);
          Open;

          While not (eof) do
          Begin
              if (fieldbyname('Classificacao').AsString = 'J')
              then PSOma:=PSOma - fieldbyname('Valor').AsCurrency;

              if (fieldbyname('Classificacao').AsString = 'Q')
              then PSOma:=PSOma + fieldbyname('Valor').AsCurrency;

              if (fieldbyname('Classificacao').AsString = 'D')
              then PSOma:=PSOma + fieldbyname('Valor').AsCurrency;

              FmostraStringList.Memo.Lines.Add(CompletaPalavra(fieldbyname('Historico').AsString,60,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Valor').AsString), 12,' '));
              Next;
          end;}

          // IPTU
          Close;
          SelectSQL.Clear;
          SelectSQL.Add('Select ValorInquilino from TabIPTUCasaPendencia Where Pendencia = '+PPendencia);
          Open;
          if (fieldbyname('ValorInquilino').AsCurrency > 0)
          then Begin
                  PSOma:=PSOma + fieldbyname('ValorInquilino').AsCurrency;
                  FmostraStringList.Memo.Lines.Add(CompletaPalavra('IPTU',60,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VAlorInquilino').AsString),12, ' '));
                  FmostraStringList.Memo.Lines.Add(' ');
          end;

          // Lancamento Locador
          if (PLocadorLocatario = 'LOCADOR')
          then Begin
                  Close;
                  SelectSQL.Clear;
                  SelectSQL.Add('Select Valor, Historico  from TabLancamentoLocador');
                  SelectSQL.Add('Where Pendencia = '+PPendencia);
                  SelectSQL.Add('and   (Debito = ''S'' or DebitoAvulso = ''S'')  ');
                  Open;

                  While not (eof) do
                  Begin
                      if (fieldbyname('Valor').AsCurrency>0)
                      then Begin
                              PSOma:=PSOma + fieldbyname('Valor').AsCurrency;
                              FmostraStringList.Memo.Lines.Add(CompletaPalavra(fieldbyname('Historico').AsString,60,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VAlor').AsString), 12, ' '));
                      end;
                      Next;
                  end;
          end;

          // Lancamento Locatario
          if (PLocadorLocatario = 'LOCATARIO')
          then Begin
                  Close;
                  SelectSQL.Clear;
                  SelectSQL.Add('Select Valor, Historico  from TabLancamentoLocatario');
                  SelectSQL.Add('Where Pendencia = '+PPendencia);
                  SelectSQL.Add('and   Credito = ''S''  ');
                  Open;

                  While not (eof) do
                  Begin
                      if (fieldbyname('Valor').AsCurrency>0)
                      then Begin
                              PSOma:=PSOma + fieldbyname('Valor').AsCurrency;
                              FmostraStringList.Memo.Lines.Add(CompletaPalavra(fieldbyname('Historico').AsString,60,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VAlor').AsString), 12, ' '));
                      end;
                      Next;
                  end;
          end;

          //  Contas Mensais
          Close;
          SelectSQL.Clear;
          SelectSQL.Add('Select Agua, Luz, Telefone, Gas');
          SelectSQL.Add('from TabContasMensais  Where PEndencia = '+PPendencia);
          Open;

          While not (eof) do
          Begin

              if (fieldbyname('Agua').AsCurrency > 0)
              then FmostraStringList.Memo.Lines.Add(CompletaPalavra('�gua', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Agua').AsString),12, ' '));

              if (fieldbyname('Luz').AsCurrency > 0)
              then FmostraStringList.Memo.Lines.Add(CompletaPalavra('Luz', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Luz').AsString),12, ' '));

              if (fieldbyname('Telefone').AsCurrency > 0)
              then FmostraStringList.Memo.Lines.Add(CompletaPalavra('Telefone', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Telefone').AsString),12, ' '));

              if (fieldbyname('Gas').AsCurrency > 0)
              then FmostraStringList.Memo.Lines.Add(CompletaPalavra('Gas', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Gas').AsString),12, ' '));

              PSOma:=PSOma + fieldbyname('Agua').AsCurrency;
              PSOma:=PSOma + fieldbyname('Luz').AsCurrency;
              PSOma:=PSOma + fieldbyname('Telefone').AsCurrency;
              PSOma:=PSOma + fieldbyname('Gas').AsCurrency;

              Next;
          end;

          if (PLocadorLocatario = 'LOCADOR')
          then Begin
                  FmostraStringList.Memo.Lines.Add(' ');
                  FmostraStringList.Memo.Lines.Add(CompletaPalavra('Futuro Lan�amento', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PValor),12, ' '));
                  FmostraStringList.Memo.Lines.Add(CompletaPalavra('Comiss�o Futuro Lan�amento', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(PValorComissao),12, ' '));
                  FmostraStringList.Memo.Lines.Add(CompletaPalavra(' ', 60, ' ')+' '+CompletaPalavra_a_Esquerda('_____________________',12, ' '));
                  FmostraStringList.Memo.Lines.Add(CompletaPalavra('Total do Valor que N�O poder� ser lan�ado', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(PValor)+StrToCurr(PValorComissao)),12, ' '));
          end;

          if (PLocadorLocatario = 'LOCATARIO')
          then Begin
                  FmostraStringList.Memo.Lines.Add(' ');
                  FmostraStringList.Memo.Lines.Add(CompletaPalavra('Total do Valor que N�O poder� ser lan�ado', 60, ' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(StrToCurr(PValor)),12, ' '));
          end;

          if ((StrToCurr(PValor)+StrToCurr(PValorComissao)+ PSOma) > PSaldo)
          then Begin
                   Result:=false;
                   FmostraStringList.ShowModal;
                   exit;
          end;
     end;

     Result:=true;
end;


procedure TObjPendencia.Imprime_Titulos_por_ContaContabil(PtipoConta: string);
var
DataInicial:string;
DataLimite:Tdate;
CredorDevedorTemp:string;
pcontacontabil:string;
psomacliente,psomageral:currency;
totalizacliente:Boolean;
Begin

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo04.Enabled:=True;
          Grupo07.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Data Inicial';

          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.caption:='Limite';


          edtgrupo04.EditMask:='';
          edtgrupo04.OnKeyDown:=Self.Titulo.subcontagerencial.contagerencial.EdtCODIGOPLANODECONTASKeyDown;
          LbGrupo04.caption:='Conta Cont�bil';
          edtgrupo04.color:=$005CADFE;

          LbGrupo07.caption:='Credor/Devedor';
          ComboGrupo07.Items.clear;
          Combo2Grupo07.Items.clear;

          Self.titulo.Get_ListacredorDevedor(ComboGrupo07.items);
          Self.titulo.Get_ListacredorDevedorcodigo(Combo2Grupo07.items);
          //edtgrupo07.onkeydown:=Self.titulo.edtcodigocredordevedor_SPV_KeyDown;
          Try
              edtgrupo07.visible:=false;
              showmodal;
          Finally
              edtgrupo07.visible:=true;
          End;

          If tag=0
          Then exit;
          
          Try
                DataInicial:='';

                if (comebarra(trim(edtgrupo01.text))<>'')
                then Begin
                        Strtodate(edtgrupo01.text);
                        DataInicial:=edtgrupo01.text;
                End;
          Except
                Datainicial:='';
          End;



          Try
                DataLimite:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Data de Limite Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          Try
                strtoint(Combo2Grupo07.Items[ComboGrupo07.itemindex]);
                CredorDevedorTemp:=Combo2Grupo07.Items[ComboGrupo07.itemindex];
                if (Self.Titulo.CREDORDEVEDOR.LocalizaCodigo(CredorDevedorTemp)=False)
                Then Begin
                           MensagemErro('Credor/Devedor n�o localizado');
                           exit;
                End;
                Self.Titulo.CREDORDEVEDOR.TabelaparaObjeto;

          Except
                mensagemerro('� necess�rio escolher um credor/devedor');
                exit;
          End;


          Try
             strtoint(edtgrupo04.text);
             PcontaContabil:=edtgrupo04.text;

             if (self.Titulo.SubContaGerencial.ContaGerencial.PlanodeCOntas.LocalizaCodigo(edtgrupo04.text)=False)
             then begin
                       mensagemErro('Conta Cont�bil n�o localizada');
                       exit;
             End;
             self.Titulo.SubContaGerencial.ContaGerencial.PlanodeCOntas.TabelaparaObjeto;

          Except
             MensagemErro('C�digo cont�bil inv�lido');
             exit;
          End;

     End;


     With Self.ObjQlocal do
     Begin
          close;
          SQL.clear;
          SQL.add('select tabtitulo.historico,tabtitulo.codigocredordevedor,tabpendencia.codigo as pendencia,tabpendencia.saldo,tabpendencia.vencimento,'+Self.Titulo.CREDORDEVEDOR.Get_Tabela+'.'+Self.Titulo.CREDORDEVEDOR.Get_CampoNome+' as NOME');
          SQL.add('from tabpendencia join tabtitulo');
          SQL.add('on tabpendencia.titulo=tabtitulo.codigo');
          SQL.add('join tabcontager on tabtitulo.contagerencial=tabcontager.codigo');
          SQL.add('join '+Self.Titulo.CREDORDEVEDOR.Get_Tabela+' on '+Self.Titulo.CREDORDEVEDOR.Get_Tabela+'.codigo=tabtitulo.codigocredordevedor');



          if (DataInicial='')
          Then SQL.add(' where Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39+'  and Saldo>0 ')
          Else SQL.add(' where (Tabpendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(DataInicial))+#39+'  and Tabpendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',DataLimite)+#39+') and Saldo>0 ');

          SQL.add(' and tabContager.tipo='+#39+PtipoConta+#39);

          SQL.add(' and TabTitulo.CredorDevedor='+CredorDevedorTemp);

          SQL.add('and '+Self.Titulo.CREDORDEVEDOR.Get_Tabela+'.'+Self.Titulo.CREDORDEVEDOR.Get_CampoContabil+'='+pcontacontabil);

          sql.add('order by '+Self.Titulo.CREDORDEVEDOR.Get_Tabela+'.'+Self.Titulo.CREDORDEVEDOR.Get_CampoNome+',tabpendencia.vencimento');

          Try
              FmostraStringList.Memo.Lines.Text:=sql.TEXT;
              //FmostraStringList.showmodal;
              open;
          Except
                on e:exception do
                Begin
                     mensagemerro('Erro: '+E.message);
                     exit;
                End;
          End;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('Nenhum Dado foi Selecionado na Pesquisa!',mtinformation,[mbok],0);
                    exit;
          End;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Gerando relat�rio';
          first;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
          FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;
          FreltxtRDPRINT.linhalocal:=3;
          FreltxtRDPRINT.rdprint.abrir;
          if ((FreltxtRDPRINT.RDprint.Setup)=False)
          Then Begin
                    FreltxtRDPRINT.rdprint.fechar;
                    exit;
          End;

          if (MensagemPergunta('Deseja totalizar por '+Self.Titulo.CREDORDEVEDOR.Get_Nome+'?')=mrno)
          Then totalizacliente:=False
          Else totalizacliente:=True;

          If (PtipoConta='D')
          Then FreltxtRDPRINT.RDprint.ImpC(FreltxtRDPRINT.linhalocal,65,Self.NumeroRelatorio+'TITULOS A PAGAR   - VENCIMENTO '+DATETOSTR(DataLimite),[negrito])
          Else FreltxtRDPRINT.RDprint.ImpC(FreltxtRDPRINT.linhalocal,65,Self.NumeroRelatorio+'TITULOS A RECEBER - VENCIMENTO '+DATETOSTR(DataLimite),[negrito]);
          FreltxtRDPRINT.IncrementaLinha(2);


          if (DataInicial<>'')
          Then FreltxtRDPRINT.rdprint.impf(FreltxtRDPRINT.linhalocal,1,'Intervalo de Datas de Vencimento:'+DataInicial+' a '+datetostr(DataLimite),[negrito])
          Else FreltxtRDPRINT.rdprint.impf(FreltxtRDPRINT.linhalocal,1,'Vencimento Anterior ou Igual'+datetostr(DataLimite),[negrito]);
          FreltxtRDPRINT.IncrementaLinha(1);

          FreltxtRDPRINT.rdprint.impf(FreltxtRDPRINT.linhalocal,1,'Conta Cont�bil: '+pcontacontabil+'-'+self.Titulo.SubContaGerencial.ContaGerencial.PlanodeCOntas.Get_Nome,[negrito]);
          FreltxtRDPRINT.IncrementaLinha(1);

          FreltxtRDPRINT.rdprint.impf(FreltxtRDPRINT.linhalocal,1,'Cadastro      : '+Self.Titulo.CREDORDEVEDOR.Get_Nome,[negrito]);
          FreltxtRDPRINT.IncrementaLinha(1);



          FreltxtRDPRINT.RDprint.Impf(FreltxtRDPRINT.linhalocal,1, CompletaPalavra('FANTASIA',40,' ')+' '+
                                              CompletaPalavra('HISTORICO',50,' ')+' '+
                                              CompletaPalavra('PEND.',6,' ')+' '+
                                              CompletaPalavra('VENCTO',10,' ')+' '+
                                              CompletaPalavra_a_Esquerda('SALDO',15,' '),[negrito]);
          FreltxtRDPRINT.IncrementaLinha(1);
          FreltxtRDPRINT.DesenhaLinha;
          credordevedortemp:=fieldbyname('codigocredordevedor').asstring;
          psomacliente:=0;
          psomageral:=0;

          While not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.show;
               Application.ProcessMessages;

               if (fieldbyname('codigocredordevedor').asstring<>CredorDevedorTemp)
               Then BEgin
                         if (totalizacliente)
                         Then Begin
                                   FreltxtRDPRINT.VerificaLinha;
                                   FreltxtRDPRINT.RDprint.Impf(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('TOTAL',40,' ')+' '+
                                                  CompletaPalavra('',50,' ')+' '+
                                                  CompletaPalavra('',6,' ')+' '+
                                                  CompletaPalavra('',10,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(psomacliente),15,' '),[negrito]);
                                   FreltxtRDPRINT.IncrementaLinha(1);
                         End;
                         credordevedortemp:=fieldbyname('codigocredordevedor').asstring;
                         psomacliente:=0;
               End;

               FreltxtRDPRINT.VerificaLinha;
               FreltxtRDPRINT.RDprint.Imp(FreltxtRDPRINT.linhalocal,1,CompletaPalavra(fieldbyname('nome').asstring,40,' ')+' '+
                                                  CompletaPalavra(fieldbyname('historico').asstring,50,' ')+' '+
                                                  CompletaPalavra(fieldbyname('pendencia').asstring,6,' ')+' '+
                                                  CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                                  CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('saldo').asstring),15,' '));
               FreltxtRDPRINT.IncrementaLinha(1);

               psomacliente:=psomacliente+fieldbyname('saldo').ascurrency;
               psomageral:=psomageral+fieldbyname('saldo').ascurrency;

               next;
          End;//while

          if (totalizacliente)
          Then Begin
                    FreltxtRDPRINT.VerificaLinha;
                    FreltxtRDPRINT.RDprint.Impf(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('TOTAL',40,' ')+' '+
                                                            CompletaPalavra('',50,' ')+' '+
                                                            CompletaPalavra('',6,' ')+' '+
                                                            CompletaPalavra('',10,' ')+' '+
                                                            CompletaPalavra_a_Esquerda(formata_valor(psomacliente),15,' '),[negrito]);
                    FreltxtRDPRINT.IncrementaLinha(1);
          End;

          FreltxtRDPRINT.VerificaLinha;
          FreltxtRDPRINT.DesenhaLinha;

          FreltxtRDPRINT.VerificaLinha;
          FreltxtRDPRINT.RDprint.Impf(FreltxtRDPRINT.linhalocal,1,CompletaPalavra('TOTAL FINAL',40,' ')+' '+
                                                            CompletaPalavra('',50,' ')+' '+
                                                            CompletaPalavra('',6,' ')+' '+
                                                            CompletaPalavra('',10,' ')+' '+
                                                            CompletaPalavra_a_Esquerda(formata_valor(psomageral),15,' '),[negrito]);
          FreltxtRDPRINT.IncrementaLinha(1);

          FMostraBarraProgresso.close;
          FreltxtRDPRINT.RDprint.Fechar;
     End;

end;


procedure TObjPendencia.Re_imprimeduplicata(parametro: string);
var
      ModDuplicata : integer;
      CodigoPendencia:string;
begin

      If (ObjParametroGlobal.ValidaParametro('MODELO DE DUPLICATA')=FALSE)
      Then Begin
                   Messagedlg('O Par�metro "MODELO DE DUPLICATA" n�o foi encontrado!',mterror,[mbok],0);
                   exit;
      End;
      
      Try
            ModDuplicata:=strtoint(ObjParametroGlobal.Get_Valor);
      Except
            ModDuplicata:=1;//happy kids
            Messagedlg('O Valor do Par�metro "MODELO DE DUPLICATA" est� incorreto, ser� usado o Modelo N� 01!',mterror,[mbok],0);
      End;

      Case ModDuplicata of
        1:begin
                FRDPRINT.RDprint.TamanhoQteLinhas:=36;
                FRDPRINT.RDprint.TamanhoQteColunas:=80;
                FRDPRINT.RDprint.UsaGerenciadorImpr:=True;
                FRDPRINT.RDprint.TamanhoQteLPP:=Seis;
                FRDPRINT.RDprint.OpcoesPreview.Preview:=true;
                FRDPRINT.RDprint.abrir;
        End;
        2:begin
                FRDPRINT.RDprint.TamanhoQteLinhas:=33;
                FRDPRINT.RDprint.TamanhoQteColunas:=80;
                FRDPRINT.RDprint.UsaGerenciadorImpr:=True;
                FRDPRINT.RDprint.TamanhoQteLPP:=Seis;
                FRDPRINT.RDprint.OpcoesPreview.Preview:=true;
                FRDPRINT.RDprint.abrir;
        End;
        3:begin
                // alterado jonas 18/08/2010 *COMENTEI O CODIGO A BAIXO
                { original:}

                {with ObjQlocal do
                begin
                     Close;
                     SQL.add('select codigo from tabpendencia where Duplicata='+#39+parametro+#39);
                     Open;
                     CodigoPendencia:=fieldbyname('codigo').AsString;
                end;}
                //END ALTERA��O

                
        end;
        Else Begin
                  Messagedlg('O Modelo de Duplicata N� '+inttostr(modduplicata)+' n�o foi configurado no Sistema!',mterror,[mbok],0);
                  exit;
        End;
      End;

      Case modduplicata of
            1:begin
                    If (ImprimeDuplicataModelo01(parametro,False)=False)
                    Then Begin
                              Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                              FdataModulo.IBTransaction.RollbackRetaining;
                              FRDPRINT.RDprint.Abortar;
                              exit;
                    End;
            End;
            2:begin
                    If (Self.ImprimeDuplicataModelo02(parametro,False)=False)
                    Then Begin
                              Messagedlg('Erro na Tentativa de Imprimir a Duplicata!',mterror,[mbok],0);
                              FdataModulo.IBTransaction.RollbackRetaining;
                              FRDPRINT.RDprint.Abortar;
                              exit;
                    End;
            End;
            3:begin

                    Self.ImprimeDuplicataReportBuilder(parametro);
            end;
      End;//case

      //ALTERADO JONAS 18/08/2010
      if (ModDuplicata <> 3)
      then FRdPrint.Rdprint.fechar;
      //END ALTERA��O
end;

function TObjPendencia.VerificaLancamentos(Ptitulo: string): integer;
begin
     Result:=0;
     With Self.ObjQlocal do
     Begin
          close;
          sql.clear;
          sql.add('Select count(tablancamento.codigo) as CONTA  from tablancamento');
          sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
          sql.add('where tabpendencia.titulo='+pTITULO);
          open;
          result:=Fieldbyname('conta').asinteger;
     End;
end;

function TObjPendencia.RetornaPropriedadeDoCliente(PPendencia:string):String;
Var  QueryLocal:TIBQuery; 
Begin
try
     Result:='';
     try
         QueryLocal:=TIBQuery.Create(nil);
         QueryLocal.Database:=FDataModulo.IBDatabase;
     except
         MensagemErro('Erro ao tentar criar a Query Local');
         exit;
     end;

     With QueryLocal do
     Begin
            Close;
            SQL.Clear;
            SQL.Add('Select TabPropriedadeCliente.Nome, TabPropriedadeCliente.Endereco,');
            SQL.Add('TabPropriedadeCliente.Cidade,TabPropriedadeCliente.Estado  from TabPendencia');
            SQL.Add('join TabVendas on TabVendas.titulo = TabPendencia.Titulo');
            SQL.Add('join TabOrcamento on TabOrcamento.Codigo = TabVendas.CodigoOrcamento');
            SQL.Add('join TabPropriedadeCliente on TabPropriedadeCliente.Codigo = TabOrcamento.PropriedadeCliente');
            SQL.Add('Where TabPendencia.Codigo = '+PPendencia);
            Open;

            if (RecordCount = 0)
            then exit;

            Result:=fieldbyname('Nome').AsString+#13+
                    fieldbyname('Endereco').AsString+#13+
                    fieldbyname('Cidade').AsString+' - '+fieldbyname('Estado').AsString;
     end;

finally

end;

end;

Procedure TObjPendencia.ImprimeDuplicataReportBuilder(PPendencia:String);
Var ObjRelPersReportBuilder:TObjRELPERSREPORTBUILDER;
Begin
try
       try
           ObjRelPersreportBuilder:=TObjRELPERSREPORTBUILDER.Create;
       except
           MensagemErro('Erro ao tentar criar o Objeto ObjRelPersreportBuilder');
           exit;
       end;

       if (ObjRelPersReportBuilder.LocalizaNOme('DUPLICATA REPORTBUILDER') = false)
       then Begin
               MensagemErro('O Relatorio de ReporteBuilder "DUPLICATA REPORTBUILDER" n�o foi econtrado');
               exit;
       end;

       ObjRelPersReportBuilder.TabelaparaObjeto;
       ObjRelPersReportBuilder.SQLCabecalhoPreenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLCabecalho,':PCODIGO',PPendencia);
       ObjRelPersReportBuilder.SQLRepeticaoPreenchido:=StrReplace(ObjRelPersReportBuilder.get_SQLRepeticao,':PCODIGO',PPendencia);
       ObjRelPersReportBuilder.SQLRepeticao_2Preenchido:=StrReplace(ObjRelPersReportBuilder.Get_SQLRepeticao_2,':PCODIGO',PPendencia);


       ObjRelPersReportBuilder.ChamaRelatorio(false);

finally
       ObjRelPersreportBuilder.Free;
end;

end;

Function TObjPendencia.RetornaObservacaoPedido(PTitulo:String):String;
Begin

     //Isso n�o vai funcionar porque, no BIA n�o tem TabVendas vai dar problema nesse SQL

     if (PTitulo = '')
     then Begin
            Result:='';
            exit;
     end;

     With Self.ObjDataset do
     Begin
          Close;
          SelectSQL.Clear;
          SelectSQL.Add('Select NumDcto from TabTitulo Where Codigo = '+PTitulo);
          Open;

          Result:= fieldbyname('NumDcto').AsString;
     end;

     Result:='';

end;

Function TObjPendencia.Lancaboleto_COM_registro_CobreBem(pnumerotitulo,PcodigoConvenio: string; StrLPendencias: TStringList):Boolean;
var
cont:integer;
DiferencaDias, LimiteVencimento:Currency;
temp:string;
Pinstrucoes:String;
ptaxabanco,Ptaxamensal,pvaloraodia:Currency;
pdiasprotesto, InicioNossoNumeroCobreBem, FimNossoNumeroCobreBem, ProximoNossoNumeroCobreBem:integer;
ObjLancamentoTemp:TobjLancamento;
TmpQuantConvenio:integer;
ObjCobreBem:TObjCobreBem;
NumeroBoletoIgualNossoNumero:string;

begin
     Result:=false;
     ObjCobreBem:=nil;

     Try
        ObjLancamentoTemp:=TobjLancamento.create;
     Except
        Messagedlg('Erro na tentativa de Cria��o da StringList de Pend�ncias selecionadas!',mterror,[mbok],0);
        exit;
     End;

     Try
        if (ObjParametroGlobal.ValidaParametro('UTILIZA NUMERO DO BOLETO IGUAL AO NOSSONUMERO(COBREBEM) EM BOLETOS COM REGISTRO')=false) then
        begin
             MensagemErro('Parametro UTILIZA NUMERO DO BOLETO IGUAL AO NOSSONUMERO(COBREBEM) EM BOLETOS COM REGISTRO n�o encontrado');
             NumeroBoletoIgualNossoNumero:='SIM'
        end
        else
        begin
            NumeroBoletoIgualNossoNumero:=ObjParametroGlobal.Get_Valor;
        end;


        if (Self.titulo.LocalizaCodigo(PNumeroTitulo)=False)
        Then Begin
                  Messagedlg('T�tulo n�o localizado',mterror,[mbok],0);
                  exit;
        end;
        Self.titulo.TabelaparaObjeto;

        if (Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio)=False)
        Then begin
                  Messagedlg('Conv�nio n�o localizado!',mterror,[mbok],0);
                  exit;
        End;
        Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
        InicioNossoNumeroCobreBem:=StrToInt(Self.BoletoBancario.Convenioboleto.Get_CobreBemInicioNossoNumero);
        FimNossoNumeroCobreBem:=StrToInt(Self.BoletoBancario.Convenioboleto.Get_CobreBemFimNossoNumero);
        ProximoNossoNumeroCobreBem:=StrToInt(Self.BoletoBancario.Convenioboleto.Get_CobreBemProximoNossoNumero);
        LimiteVencimento:=StrToCurr(Self.BoletoBancario.Convenioboleto.Get_CobreBemLimiteVencimento);

        try
             ObjCobreBem:=TObjCobreBem.Create();
             if (ObjCobreBem.ConfiguraContaCorrente(PCodigoConvenio)=false)
             then exit;
        except
             MensagemErro('Erro ao tentar criar o Objeto CobreBem');
             exit;
        end;


        //********************************************************************

        Try
           ptaxabanco:=strtofloat(Self.BoletoBancario.Convenioboleto.Get_taxabanco);
        Except
           PtaxaBanco:=0;
        End;


        //Imprimindo boleto a boleto atraves dos registros das pendencias
        for cont:=0 to StrLPendencias.count-1 do
        Begin
             //criando um novo boleto e gravando o nosso numero nesse boleto
             Self.ZerarTabela;
             Self.LocalizaCodigo(StrLPendencias[cont]);
             Self.TabelaparaObjeto;

             DiferencaDias:=Int(StrToDate(Self.Get_VENCIMENTO)-Now);

             if (DiferencaDias <  LimiteVencimento)
             then Begin
                       MensagemErro('Esse convenio foi configurado para que a data de vencimento seja '+CurrToStr(LimiteVencimento)+' dias a mais da data de emiss�o. Altere o vencimento da pendencia ou do Conv�nio' );
                       exit;
             end;        

             Self.BoletoBancario.Status:=dsinsert;
             Self.BoletoBancario.Submit_CODIGO('0');

             Self.BoletoBancario.Submit_CobreBem('S');
             Self.BoletoBancario.ArquivoRemessaRetorno.Submit_Codigo('');

             Self.BoletoBancario.Submit_Situacao('I');

             // A grava�ao do nosso numero no cobrebem � controlada pelo convenio
             // N�o poder� ser maior do que o FIM Nosso Numero e nem Menor que o PrimeiroNossoNumero
             if (ProximoNossoNumeroCobreBem > FimNossoNumeroCobreBem)
             then Self.BoletoBancario.Submit_NossoNumeroCobreBem(IntToStr(InicioNossoNumeroCobreBem))
             else Self.BoletoBancario.Submit_NossoNumeroCobreBem(IntToStr(ProximoNossoNumeroCobreBem));

          //   Inc(ProximoNossoNumeroCobreBem,1);

             Self.BoletoBancario.Submit_NumeroBoleto('');//nosso numero

             Self.BoletoBancario.Submit_Vencimento(Self.VENCIMENTO);
             Self.BoletoBancario.Submit_DataDoc(datetostr(now));
             Self.BoletoBancario.Submit_NumeroDoc('');
             Self.BoletoBancario.Submit_DataProc(datetostr(now));
             //Era no valor que saia, o sadol pediu pra sair pelo saldo
             Self.BoletoBancario.Submit_ValorDoc(floattoStr(strtofloat(Self.Saldo)+ptaxabanco));
             Self.BoletoBancario.Submit_Desconto('');
             Self.BoletoBancario.Submit_OutrasDeducoes('');
             Self.BoletoBancario.Submit_Juros('');
             Self.BoletoBancario.Submit_outrosacrescimos('');
             Self.BoletoBancario.Submit_ValorCobrado('');
             //*******************************************
             Self.BoletoBancario.Convenioboleto.Submit_CODIGO(PcodigoConvenio);
             Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio);
             Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
             //*******************************************
             //as instrucoes possuem variaveis pre-determinada
             //que sao colocadas no texto, aqui apenas eu mando substituir
             //caso o texto as tenha usado ele substitue
             //senao nao tem erro nenhum
             //Ser� Protestado ap�s dia &dataprotesto
             Pinstrucoes:=Self.BoletoBancario.Convenioboleto.Get_Instrucoes;

             try
                 //juros
                 Ptaxamensal:=strtofloat(Self.BoletoBancario.ConvenioBoleto.get_taxamensal);
             Except
                   Ptaxamensal:=0;
             End;

             try
                ptaxamensal:=((strtofloat(Self.VALOR)*Ptaxamensal)/100);
                pvaloraodia:=strtofloat(tira_ponto(formata_valor(Ptaxamensal/30)));
             Except
                   pvaloraodia:=0;
             End;

             //taxamensal,Quantdiasprotesto

             Try
                pdiasprotesto:=strtoint(Self.BoletoBancario.Convenioboleto.get_Quantdiasprotesto);
             Except
                pdiasprotesto:=0;
             End;

             Pinstrucoes:=strreplace(pinstrucoes,'&taxamensal',formata_valor(ptaxamensal));
             Pinstrucoes:=strreplace(pinstrucoes,'&valoraodia',formata_valor(pvaloraodia));
             Pinstrucoes:=strreplace(pinstrucoes,'&dataprotesto',datetostr(strtodate(Self.VENCIMENTO)+pdiasprotesto));
             Pinstrucoes:=strreplace(pinstrucoes,'&datavencimento',Self.VENCIMENTO);
             Pinstrucoes:=strreplace(pinstrucoes,'&taxabanco',Formata_valor(PtaxaBanco));

             // Alterado por F�bio 30/06/2009 ordem Sadol
             if (ObjParametroGlobal.ValidaParametro('MOSTRA PROPRIEDADE DO CLIENTE EM BOLETO')=false)
             then
             else if (ObjParametroGlobal.Get_Valor = 'SIM')
                  then  Pinstrucoes:=strreplace(pinstrucoes,'&propriedade',Self.RetornaPropriedadeDoCliente(StrLPendencias[Cont]));


             //Rodolfo 29-08-12
             if(Self.Titulo.Get_NotaFiscal <> '')then
             begin
               PInstrucoes := 'Nota Fiscal: ' + Self.Titulo.Get_NotaFiscal + #13 + Pinstrucoes;
             end;
             

             Self.BoletoBancario.Submit_Instrucoes(Pinstrucoes);

             if (Self.BoletoBancario.Salvar(False,'')=False)
             then begin
                       FDataModulo.IBTransaction.RollbackRetaining;
                       Messagedlg('Erro na tentativa de Criar um novo boleto para pend�ncia '+StrLPendencias[cont],mterror,[mbok],0);
                       exit;
             end;

             //passando o codigo do Boleto para o numero do boleto e numero do documento
             Self.BoletoBancario.Status:=DsEdit;


             (*if (Self.Boletobancario.GeraNossoNumero=False)//ele gera de acordo com o Banco do Convenio
             Then Begin
                      FDataModulo.IBTransaction.RollbackRetaining;
                      Messagedlg('Erro na tentativa de Gerar O Nosso N�mero do Boleto',mterror,[mbok],0);
                      exit;
             end;*)

             Self.BoletoBancario.Submit_NumeroDoc(Self.BoletoBancario.Get_CODIGO);
            //ALTERA��O 05/07/2011
            //TEM QUE IR O NOSSO NUMERO COMO NUMERO DO BOLETO, SEN�O N�O ACHA PRA FAZER O ABATIMENTO DAS PENDENCIAS ]
            //COMO TEM CLIENTE (SINDICATO RURAL) QUE ESTA FUNCIONANDO CORRETAMENTE DESSA FORMA, ENT�O OPTEI POR CRIAR UM
            //PARAMETRO, QUE DA OP��O DE USAR O NOSSONUMERO COMO NUMERO DO BOLETO OU N�O....
            //JONATAN MEDINA
             Inc(ProximoNossoNumeroCobreBem,1);
             if(NumeroBoletoIgualNossoNumero='NAO')
             then Self.BoletoBancario.Submit_NumeroBoleto(Self.BoletoBancario.Get_CODIGO) //Numero boleto diferente nossonumero
             else Self.BoletoBancario.Submit_NumeroBoleto(CompletaPalavra_a_Esquerda(IntToStr(ProximoNossoNumeroCobreBem),11,'0'));

             if (Self.BoletoBancario.Salvar(False,'')=False)
             Then begin
                     Messagedlg('Erro na tentativa de Guardar o N� do Boleto e N� do Doc',mterror,[mbok],0);
                     exit;
             End;

             Self.Status:=dsedit;
             if (Self.Salvar(False)=False)
             Then Begin
                       Messagedlg('Erro na tentativa de Gravar o Numero do boleto na pend�ncia '+StrLPendencias[cont],mterror,[mbok],0);
                       exit;
             End;

             if (ptaxabanco>0)
             Then Begin
                       //lancando o juros para taxa do Boleto
                       temp:='';
                       If (ObjLancamentotemp.LancamentoAutomatico('J',StrLPendencias[cont],floattostr(PtaxaBanco),datetostr(now),'REFERENTE TAXA DE BOLETO BANC�RIO',temp,False)=False)
                       Then Begin
                                Messagedlg('N�o foi poss�vel lan�ar a taxa do boleto',mterror,[mbok],0);
                                exit;
                       End;
             End;

             Self.BoletoBancario.Convenioboleto.LocalizaCodigo(PcodigoConvenio);
             Self.BoletoBancario.Convenioboleto.TabelaparaObjeto;
             Self.BoletoBancario.Convenioboleto.Status:=dsEdit;

             Self.BoletoBancario.Convenioboleto.Submit_CobreBemProximoNossoNumero(IntToStr(ProximoNossoNumeroCobreBem));
             if (Self.BoletoBancario.Convenioboleto.Salvar(false)=false)
             then Begin
                      MensagemErro('Erro  ao tentar atualizar o campo Proximo Nosso Numero no Convenio');
                      exit;
             end;

             // Commito aki porque se der algum problema no cobre bem
             // pelo menos os boletos j� foram salvos
             FDataModulo.IBTransaction.CommitRetaining;


             // Gerando o Boleto no CobreBEM
             if (ObjCobreBem.GeraBoletoCobreBem(Self.BoletoBancario.Get_CODIGO,'','01 - Entrada de Titulo')=false)
             then Begin
                      MensagemErro('Erro ao tentar Gerar o Boleto no CobreBem');
                      exit;
             end;

        End;


        if (ObjCobreBem.ImprimeBoletos = false)
        then MensagemErro('Erro ao tentar gerar os Boletos no CobreBem');


     Result:=true;

     Finally
        if (ObjCobreBem <> nil)
        then ObjCobreBem.Free;

        ObjLancamentoTemp.free;

        FDataModulo.IBTransaction.RollbackRetaining;
     End;
End;

Function TObjPendencia.ReimprimeBoletoCobreBem(PBoletoBancario:String):Boolean;
Var ObjCobreBem : TObjCobreBem;
Begin
    Result:=false;
    ObjCobreBem:=nil;

    if (PBoletoBancario = '')
    then exit;

try
    if (Self.BoletoBancario.LocalizaCodigo(PBoletoBancario)=false)
    then Begin
            MensagemErro('Boleto Bancario n�o encontrado');
            exit;
    end;
    Self.BoletoBancario.TabelaparaObjeto;

    try
         ObjCobreBem:=TObjCobreBem.Create();
         if (ObjCobreBem.ConfiguraContaCorrente(Self.BoletoBancario.Convenioboleto.Get_CODIGO)=false)
         then exit;
    except
         MensagemErro('Erro ao tentar criar o Objeto CobreBEM');
         exit;
    end;

    // Gerando o Boleto no CobreBem
    if (ObjCobreBem.GeraBoletoCobreBem(PBoletoBancario,'','01 - Entrada de Titulo')=false)
    then Begin
             MensagemErro('Erro ao tentar Gerar o Boleto no CobreBem');
             exit;
    end;

    if (ObjCobreBem.ImprimeBoletos = false)
    then MensagemErro('Erro ao tentar gerar os Boletos');

    Result:=true;

finally
    if (ObjCobreBem <> nil)
    then ObjCobreBem.Free;
end;


end;

end.

