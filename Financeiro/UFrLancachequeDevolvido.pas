unit UFrLancachequeDevolvido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, ComCtrls, Grids, DBGrids, ExtCtrls,
  DB, IBCustomDataSet, IBQuery, DBCtrls,UobjCHEQUEDEVOLVIDO, UObjGeraTransferencia;

type
  TFrlancaChequeDevolvido = class(TFrame)
    QueryPesquisa: TIBQuery;
    DataSourcePesquisa: TDataSource;
    Panel1: TPanel;
    Shape3: TShape;
    Label4: TLabel;
    DBComp: TDBEdit;
    Dbbanco: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DbAgencia: TDBEdit;
    DbDV: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    DbC1: TDBEdit;
    DbConta: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    DbC2: TDBEdit;
    DbSerie: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DbNumCheque: TDBEdit;
    DbC3: TDBEdit;
    Label3: TLabel;
    Label17: TLabel;
    DbValor: TDBEdit;
    DbCpfCliente1: TDBEdit;
    DbCpfCliente2: TDBEdit;
    DbVencimento: TDBEdit;
    Label13: TLabel;
    dbcodigobarras: TDBEdit;
    Label16: TLabel;
    Label18: TLabel;
    Label14: TLabel;
    DbCliente1: TDBEdit;
    DbCliente2: TDBEdit;
    DbGridPesquisa: TDBGrid;
    Guia: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label19: TLabel;
    Bevel2: TBevel;
    edtportador_filtro: TEdit;
    Label22: TLabel;
    lbnomeportador: TLabel;
    edtnumcheque_filtro: TEdit;
    Label23: TLabel;
    edtvalor_filtro: TEdit;
    Label24: TLabel;
    Label25: TLabel;
    edtcpf_filtro: TEdit;
    BtConsultar: TButton;
    BtGeraContas: TButton;
    Bevel1: TBevel;
    Label15: TLabel;
    edtportador_CP: TEdit;
    lbnomeportador_CP: TLabel;
    EdtnumCheque_CP: TEdit;
    Label20: TLabel;
    Label21: TLabel;
    edtvalor_CP: TEdit;
    btConsulta_CP: TButton;
    BtGeraConta_CP: TButton;
    procedure edtportador_filtroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportador_filtroExit(Sender: TObject);
    procedure edtportador_filtroKeyPress(Sender: TObject; var Key: Char);
    procedure BtConsultarClick(Sender: TObject);
    procedure edtvalor_filtroKeyPress(Sender: TObject; var Key: Char);
    procedure BtGeraContasClick(Sender: TObject);
    procedure DbGridPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure edtportador_CPExit(Sender: TObject);
    procedure edtportador_CPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btConsulta_CPClick(Sender: TObject);
    procedure BtGeraConta_CPClick(Sender: TObject);
  private
    { Private declarations }
    ObjChequeDevolvido:TobjChequeDevolvido;
    ObjGeraTransferencia:TObjGeraTransferencia;
    Procedure LimpaLabels;
  public
    { Public declarations }
    Function Cria:boolean;
    Function Destroi:boolean;

  end;


implementation

uses UessencialGlobal,UobjPortador, UDataModulo, UObjTitulo,
  UObjGeraTitulo, UFiltraImp, 
  UmostraStringGrid, UObjCredorDevedor, UObjLancamento;

{$R *.dfm}

procedure TFrlancaChequeDevolvido.edtportador_filtroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Self.objChequeDevolvido.Cheque.Portador.edtportadorKeyDown(sender,key,shift);
end;

procedure TFrlancaChequeDevolvido.edtportador_filtroExit(Sender: TObject);
begin
     lbnomeportador.caption:='';

     if Tedit(Sender).Text=''
     Then exit;

     if (Self.ObjChequeDevolvido.Cheque.Portador.LocalizaCodigo(Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.ObjChequeDevolvido.Cheque.Portador.TabelaparaObjeto;
     lbnomeportador.Caption:=Self.ObjChequeDevolvido.Cheque.Portador.Get_Nome;
end;

procedure TFrlancaChequeDevolvido.LimpaLabels;
begin
     lbnomeportador.caption:='';
end;

procedure TFrlancaChequeDevolvido.edtportador_filtroKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFrlancaChequeDevolvido.BtConsultarClick(Sender: TObject);
begin
     if (ObjGeraTransferencia.LocalizaHistorico('TRANSFER�NCIA DE CHEQUE DEVOLVIDO')=False)
     then Begin
                MensagemErro('Gerador de Transfer�ncia: "TRANSFER�NCIA DE CHEQUE DEVOLVIDO" n�o encontrado');
                exit;
     End;
     ObjGeraTransferencia.TabelaparaObjeto;
     

     With QueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.add('Select * from TabChequesPortador where ChequedoPortador=''N'' ');

          if (edtportador_filtro.Text<>'')
          Then Sql.add('and TabChequesPortador.Portador='+edtportador_filtro.Text);

          if (edtnumcheque_FILTRO.Text<>'')
          Then Sql.add('and TabChequesPortador.NumCheque like '+#39+'%'+edtnumcheque_filtro.Text+'%'+#39);

          if (edtvalor_filtro.Text<>'')
          Then Sql.add('and TabchequesPortador.valor='+virgulaparaponto(edtvalor_filtro.text));

          if (edtcpf_filtro.Text<>'')
          Then Sql.add('and TabChequesPortador.CpfCliente1 like ''%'+edtcpf_filtro.text+'%'' ');

          Sql.add('and TabChequesPortador.Portador<>'+Self.ObjGeraTransferencia.PortadorDestino.Get_CODIGO);
          
          open;
          formatadbgrid(DbGridPesquisa);
          dbgridpesquisa.setfocus;

                   
     End;

end;

procedure TFrlancaChequeDevolvido.edtvalor_filtroKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8,','])
     Then if key='.'
          Then key:=','
          Else key:=#0; 
end;

procedure TFrlancaChequeDevolvido.BtGeraContasClick(Sender: TObject);
var
ObjQueryTemp:tibquery;
plancamento:string;
ptitulo:string;
PcodigoPc_portador,PCodigoPC_Fornecedor,PcredorDevedor,PCodigoCredorDevedor:string;
Ptituloapagar,Ptituloareceber,Ptransferencia:string;
objgeratitulo:TObjGeraTitulo;
pdatadevolucao:Tdate;
PcodigochequeDevolvido,PmotivoDevolucao:string;
cont:integer;
PlistaCheque:TStringList;
PlancaContabilidade:Boolean;
begin

     if (Messagedlg('Tem certeza que deseja lan�ar esse cheque como devolvido?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;



{***********************************Contabilidade************************************************************
(Transfer�ncia)Se o cheque estava no banco debito banco credito CHDEV
(T�tulo) Ao receber, credito Caixa Debito CHDEV, n�o pode exportar na cria��o do titulo, somente no recebimento, e nesse caso n�o uso o fornecedor e sim a conta gerencial (caso diferente).

Se o Cheque estava com um Fornecedor
(titulo) Credito CHEDEV e Debito Caixa (pagamento ao fornecedor)
N�o pode exportar o titulo e sim o pagamento, nesse caso, utilizo o banco e a conta de cheques (conta gerencial)

(titulo) Ao receber o Cheque debito CHEQDEV e Credito Caixa.
N�o pode exportar o titulo e sim o recebimento.

	Cheque do Portador

		Debita Banco, Credito o Fornecedor(lan�ado manual)
		Obs: N�o exporta na transfer�ncia, nem o titulo  a pagar
		No pagamento da conta Credita Banco e Debito o Fornecedor
    
**************************************************************************************************************}     

     //Primeiro descobrir qual a conta  a receber que o cadastrou
     //assim tenho Quem me Deu o Cheque
     //De posse dessa informacao gero uma Conta a receber da mesma pessoa
     //*************************************************************************

     //Verificar se o cheque foi utilizado por ultimo numa conta a pagar
     //se sim, gerar uma conta a pagar para o fornecedor
     //caso ele tenha sido usado em uma transferencia para algum banco
     //nao precisa gerar essa conta a pagar
     //*************************************************************************

     //Transferir ele do banco Atual para um Portador Cheques Devolvidos
     //*************************************************************************
     Ptituloapagar:='';
     Ptituloareceber:='';
     Ptransferencia:='';

     if (Self.objChequeDevolvido.localizaCheque(QueryPesquisa.FieldByName('codigo').asstring)=True)
     Then Begin
               MensagemErro('J� existe uma ocorr�ncia de devolu��o para este cheque');
               exit;
     End;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.EditMask:=MascaraData;
          LbGrupo01.Caption:='Data de Devolu��o';

          Grupo02.Enabled:=True;
          LbGrupo02.Caption:='Al�nea';
          edtgrupo02.OnKeyDown:=Self.ObjChequeDevolvido.Cheque.EdtMotivoDevolucaoKeyDown;

          Showmodal;

          if (tag=0)
          then exit;

          if (Validadata(1,pdatadevolucao,false)=False)
          Then exit;

          PmotivoDevolucao:='';

          if (edtgrupo02.Text<>'')
          then Begin
                    Try
                       strtoint(edtgrupo02.text);
                       PmotivoDevolucao:=edtgrupo02.text;

                       if (Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.LocalizaCodigo(pmotivodevolucao)=False)
                       Then begin
                                 MensagemErro('Al�nea n�o encontrada');
                                 exit;
                       End;
                       Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.TabelaparaObjeto;


                    except
                          MensagemErro('Al�nea inv�lida');
                          exit;
                    End;
          end;

     end;

     Try
        ObjQueryTemp:=tibquery.create(nil);
        ObjQueryTemp.Database:=FdataModulo.ibdatabase;

        objgeratitulo:=TObjGeraTitulo.create;

        PlistaCheque:=TStringList.create;
     Except
        MensagemErro('Erro na tentativa de Criar a Query ou os Objetos Geradores');
        exit;
     End;

     Try

        With ObjQueryTemp do
        begin
             close;
             sql.clear;
             sql.add('Select TabTitulo.codigo as TITULO,TabTitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor,tablancamento.codigo as LANCAMENTO');
             sql.add('from tabValores');
             sql.add('join tablancamento on tabValores.Lancamento=Tablancamento.codigo');
             sql.add('left join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
             sql.add('left join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
             sql.add('where tabvalores.cheque='+QueryPesquisa.fieldbyname('codigo').asstring);
             open;
             ptitulo:=Fieldbyname('titulo').asstring;
             Plancamento:=Fieldbyname('lancamento').asstring;
             PcredorDevedor:=Fieldbyname('CredorDevedor').asstring;
             PCodigoCredorDevedor:=Fieldbyname('CodigoCredorDevedor').asstring;


             if (plancamento='')
             Then Begin
                       MensagemErro('N�o foi localizado o Lan�amento que cadastrou esse cheque no sistema');
                       exit;
             End;

             if (ptitulo='')//foi em lote, por isso nao tem titulo
             Then Begin
                       close;
                       sql.clear;
                       sql.add('Select Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                       sql.add('from tabLancamento');
                       sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                       sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                       sql.add('where LancamentoPai='+plancamento);
                       sql.add('group by Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                       open;
                       last;
                       if (recordcount=1)
                       then Begin
                                 //todos os titulos do mesmo credor
                                 PcredorDevedor:=Fieldbyname('CredorDevedor').asstring;
                                 PCodigoCredorDevedor:=Fieldbyname('CodigoCredorDevedor').asstring;
                       End
                       Else Begin
                                 //escolhendo qual credor devedor
                                 FmostraStringGrid.Configuracoesiniciais;
                                 FmostraStringGrid.StringGrid.colcount:=4;
                                 FmostraStringGrid.StringGrid.RowCount:=recordcount+1;;
                                 FmostraStringGrid.StringGrid.cols[0].clear;
                                 FmostraStringGrid.StringGrid.cols[1].clear;
                                 FmostraStringGrid.StringGrid.cols[2].clear;
                                 FmostraStringGrid.StringGrid.cols[3].clear;
                                 FmostraStringGrid.StringGrid.Cells[0,0]:='XX';
                                 FmostraStringGrid.StringGrid.Cells[1,0]:='CADASTRO';
                                 FmostraStringGrid.StringGrid.Cells[2,0]:='CODIGO ';
                                 FmostraStringGrid.StringGrid.Cells[3,0]:='NOME';
                                 first;
                                 cont:=1;
                                 While not(eof) do
                                 Begin
                                      PcredorDevedor:=fieldbyname('credordevedor').asstring;
                                      PCodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;
                                      
                                      Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.LocalizaCodigo(fieldbyname('credordevedor').asstring);
                                      Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.TabelaparaObjeto;

                                      FmostraStringGrid.StringGrid.Cells[0,cont]:=fieldbyname('credordevedor').asstring;
                                      FmostraStringGrid.StringGrid.Cells[1,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Nome;
                                      FmostraStringGrid.StringGrid.Cells[2,cont]:=fieldbyname('codigocredordevedor').asstring;
                                      FmostraStringGrid.StringGrid.Cells[3,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PCodigoCredorDevedor);
                                      inc(cont,1);
                                      next;
                                 End;
                                 AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);
                                 FmostraStringGrid.caption:='Escolha uma op��o para Conta a Receber';
                                 FmostraStringGrid.ShowModal;

                                 if (FmostraStringGrid.tag=0)
                                 Then exit;

                                 PcredorDevedor:=FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.row];
                                 PCodigocredorDevedor:=FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.row];
                       End;
             End;


             if (objgeratitulo.LocalizaHistorico('T�TULO A RECEBER DE CHEQUES DEVOLVIDOS')=False)
             then Begin
                       mensagemerro('O gerador de T�tulos com Hist�rico: "T�TULO A RECEBER DE CHEQUES DEVOLVIDOS" n�o foi encontrado');
                       exit;
             End;
             objgeratitulo.TabelaparaObjeto;

             With Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo do
             Begin
                  ZerarTabela;
                  Ptituloareceber:=Get_NovoCodigo;
                  Status:=dsinsert;
                  Submit_CODIGO(Ptituloareceber);
                  Submit_HISTORICO('REF. CHEQUE DEVOLVIDO NUM '+QueryPesquisa.fieldbyname('numcheque').asstring);
                  Submit_GERADOR(Objgeratitulo.Get_Gerador);
                  Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
                  Submit_CREDORDEVEDOR(PcredorDevedor);
                  Submit_CODIGOCREDORDEVEDOR(PCodigoCredorDevedor);
                  Submit_EMISSAO(datetostr(pdatadevolucao));
                  Submit_PRAZO(Objgeratitulo.Get_Prazo);
                  Submit_PORTADOR(Objgeratitulo.get_portador);
                  Submit_VALOR(querypesquisa.fieldbyname('valor').asstring);
                  Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
                  Submit_NUMDCTO(querypesquisa.fieldbyname('numcheque').asstring);//vai o numero do cheque
                  Submit_GeradoPeloSistema('N');
                  Submit_ParcelasIguais(True);
                  //Nao exporta no titulo, mas na quitacao sim
                  //Porem nao uso o Cliente e sim a Conta gerencial
                  //debitando o caixa e Creditando a conta gerencial que vai estar ligada a cheques devolvidos
                  Submit_ExportaLanctoContaGer('S');
                  //salva sem commitar e sem exportar contabilidade
                  if (salvar(False,False)=False)
                  Then Begin
                            MensagemErro('N�o foi poss�vel salvar o T�tulo a receber do cheque devolvido');
                            exit;
                  End;
                  
             End;

             //localizando as transferencia ou pagamento
             PcredorDevedor:='';
             PCodigoCredorDevedor:='';

             close;
             SQL.clear;
             sql.add('Select TTP.* from TabValoresTransferenciaPortador TVTP');
             sql.add('join TabTransferenciaPortador TTP on TVTP.TRANSFERENCIAPORTADOR=TTP.codigo');
             sql.add('where TVTP.valores='+QueryPesquisa.fieldbyname('codigo').asstring);
             sql.add('order by TTP.data,TTP.codigo');
             open;
             last;//pegando a ultima transferencia feita com esse cheque

             if (Fieldbyname('codigolancamento').asstring<>'')//foi um pagamento
             Then Begin
                         plancamento:=Fieldbyname('codigolancamento').asstring;
                         //gerar o titulo a pagar
                         Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.ZerarTabela;
                         Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.LocalizaCodigo(fieldbyname('codigolancamento').asstring);
                         Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.TabelaparaObjeto;
                         if (Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Get_CODIGO='')//em lote
                         Then Begin
                                   close;
                                   sql.clear;
                                   sql.add('Select Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                                   sql.add('from tabLancamento');
                                   sql.add('join tabpendencia on tablancamento.pendencia=tabpendencia.codigo');
                                   sql.add('join tabtitulo on tabpendencia.titulo=tabtitulo.codigo');
                                   sql.add('where LancamentoPai='+plancamento);
                                   sql.add('group by Tabtitulo.CredorDevedor,TabTitulo.CodigoCredorDevedor');
                                   open;
                                   last;
                                   if (recordcount=1)
                                   then Begin
                                             //todos os titulos do mesmo credor
                                             PcredorDevedor:=Fieldbyname('CredorDevedor').asstring;
                                             PCodigoCredorDevedor:=Fieldbyname('CodigoCredorDevedor').asstring;
                                   End
                                   Else Begin
                                             //escolhendo qual credor devedor
                                             FmostraStringGrid.Configuracoesiniciais;
                                             FmostraStringGrid.StringGrid.colcount:=4;
                                             FmostraStringGrid.StringGrid.RowCount:=recordcount+1;;
                                             FmostraStringGrid.StringGrid.cols[0].clear;
                                             FmostraStringGrid.StringGrid.cols[1].clear;
                                             FmostraStringGrid.StringGrid.cols[2].clear;
                                             FmostraStringGrid.StringGrid.cols[3].clear;
                                             FmostraStringGrid.StringGrid.Cells[0,0]:='XX';
                                             FmostraStringGrid.StringGrid.Cells[1,0]:='CADASTRO';
                                             FmostraStringGrid.StringGrid.Cells[2,0]:='CODIGO ';
                                             FmostraStringGrid.StringGrid.Cells[3,0]:='NOME';
                                             first;
                                             cont:=1;
                                             While not(eof) do
                                             Begin
                                                  PcredorDevedor:=fieldbyname('credordevedor').asstring;
                                                  PCodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;
                                                  
                                                  Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.LocalizaCodigo(fieldbyname('credordevedor').asstring);
                                                  Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.TabelaparaObjeto;
                                   
                                                  FmostraStringGrid.StringGrid.Cells[0,cont]:=fieldbyname('credordevedor').asstring;
                                                  FmostraStringGrid.StringGrid.Cells[1,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Nome;
                                                  FmostraStringGrid.StringGrid.Cells[2,cont]:=fieldbyname('codigocredordevedor').asstring;
                                                  FmostraStringGrid.StringGrid.Cells[3,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PCodigoCredorDevedor);
                                                  inc(cont,1);
                                                  next;
                                             End;
                                             AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);
                                             FmostraStringGrid.caption:='Escolha uma op��o para Conta a Pagar';
                                             FmostraStringGrid.ShowModal;
                                   
                                             if (FmostraStringGrid.tag=0)
                                             Then exit;
                                   
                                             PcredorDevedor:=FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.row];
                                             PCodigocredorDevedor:=FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.row];
                                   End;
                         End//lote
                         Else Begin
                                   PcredorDevedor:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo;
                                   PcodigocredorDevedor:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
                         End;

                         
                         //titulo
                         if (objgeratitulo.LocalizaHistorico('T�TULO A PAGAR DE CHEQUES DEVOLVIDOS')=False)
                         then Begin
                                   mensagemerro('O gerador de T�tulos com Hist�rico: "T�TULO A PAGAR DE CHEQUES DEVOLVIDOS" n�o foi encontrado');
                                   exit;
                         End;
                         objgeratitulo.TabelaparaObjeto;
                         
                         With Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo do
                         Begin
                              ZerarTabela;
                              Ptituloapagar:=Get_NovoCodigo;
                              Status:=dsinsert;
                              Submit_CODIGO(Ptituloapagar);
                              Submit_HISTORICO('REF. CHEQUE DEVOLVIDO NUM '+QueryPesquisa.fieldbyname('numcheque').asstring);
                              Submit_GERADOR(Objgeratitulo.Get_Gerador);
                              Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
                              Submit_CREDORDEVEDOR(PcredorDevedor);
                              Submit_CODIGOCREDORDEVEDOR(PCodigoCredorDevedor);
                              Submit_EMISSAO(datetostr(pdatadevolucao));
                              Submit_PRAZO(Objgeratitulo.Get_Prazo);
                              Submit_PORTADOR(Objgeratitulo.get_portador);
                              Submit_VALOR(querypesquisa.fieldbyname('valor').asstring);
                              Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
                              Submit_NUMDCTO(querypesquisa.fieldbyname('numcheque').asstring);//vai o numero do cheque
                              Submit_GeradoPeloSistema('N');
                              Submit_ParcelasIguais(True);
                              //A Contabilidade � normal, ou seja, na quitacao C banco D fornecedor
                              //pois vai ser exportado aqui C fornecedor e D cheques devolvidos
                              //Assim quando eu pagar o Fornecedor ele mata a conta do fornecedor
                              //ficando apenas saldo na cheque devolvido que sera quitado quando o cliente pagar
                              Submit_ExportaLanctoContaGer('N');

                              //salva sem commitar e sem exportar contabilidade
                              if (salvar(False,False)=False)
                              Then Begin
                                        MensagemErro('N�o foi poss�vel salvar o T�tulo a pagar do cheque devolvido');
                                        exit;
                              End;
                              PCodigoPC_Fornecedor:=CREDORDEVEDOR.Get_CampoContabilCredorDevedor(PcredorDevedor,PCodigoCredorDevedor);
                         End;
             End;//foi um pagamento

             //e por fim transferindo o cheque para um portador de cheques devolvidos

             if (ObjGeraTransferencia.LocalizaHistorico('TRANSFER�NCIA DE CHEQUE DEVOLVIDO')=False)
             then Begin
                        MensagemErro('Gerador de Transfer�ncia: "TRANSFER�NCIA DE CHEQUE DEVOLVIDO" n�o encontrado');
                        exit;
             End;
             ObjGeraTransferencia.TabelaparaObjeto;

             With Self.objChequeDevolvido do
             Begin
                  Transferencia.ZerarTabela;
                  Ptransferencia:=Transferencia.Get_NovoCodigo;
                  Transferencia.Submit_codigo(PTransferencia);
                  Transferencia.PortadorOrigem.Submit_codigo(QueryPesquisa.fieldbyname('portador').asstring);
                  Transferencia.PortadorDestino.submit_codigo(ObjGeraTransferencia.PortadorDestino.get_codigo);
                  PcodigoPc_portador:=ObjGeraTransferencia.PortadorDestino.Get_CodigoPlanodeContas;
                  Transferencia.Grupo.Submit_CODIGO(ObjGeraTransferencia.Grupo.get_codigo);
                  Transferencia.Submit_Data(datetostr(pdatadevolucao));
                  Transferencia.Submit_Valor('0');//em dinheiro
                  Transferencia.Submit_Documento(Querypesquisa.fieldbyname('numcheque').asstring);
                  Transferencia.Submit_Complemento('CHEQ.DEVOLVIDO');
                  Transferencia.status:=dsinsert;

                  PlistaCheque.Clear;
                  PlistaCheque.add(QueryPesquisa.Fieldbyname('codigo').asstring);
                  Transferencia.Submit_ListaChequesTransferencia(PlistaCheque);
                  if (Ptituloapagar<>'')
                  Then Begin
                          //se foi usado para pagar fornecedor nao exporta essa transferencia
                          //Mas exporta depois no final
                          PlancaContabilidade:=True;
                          if (Transferencia.Salvar(False,True,True,False,False,False,'')=False)
                          Then Begin
                                    MensagemErro('N�o foi poss�vel salvar a transfer�ncia do cheque devolvido');
                                    exit;
                          End;
                  End
                  Else Begin
                          //Exporta a contabilidade da transferencia do Banco para Cheque devolvido
                          //pois esse cheque foi depositado e voltou
                          PlancaContabilidade:=False;
                          if (Transferencia.Salvar(False,True,True,False,False,True,'')=False)
                          Then Begin
                                    MensagemErro('N�o foi poss�vel salvar a transfer�ncia do cheque devolvido');
                                    exit;
                          End;
                  End;

             End;

             //salvando a ocorrencia de cheque devolvido

             With Self.objChequeDevolvido do
             Begin
                  ZerarTabela;
                  PcodigochequeDevolvido:=Get_NovoCodigo;
                  Submit_CODIGO(PcodigochequeDevolvido);
                  Cheque.Submit_CODIGO(QueryPesquisa.fieldbyname('codigo').asstring);
                  TituloaReceber.Submit_CODIGO(Ptituloareceber);
                  TituloaPagar.Submit_CODIGO(Ptituloapagar);
                  Transferencia.Submit_CODIGO(Ptransferencia);
                  Status:=dsInsert;
                  if (Salvar(False)=False)
                  Then Begin
                            MensagemErro('Erro na tentativa de Criar uma Ocorr�ncia de Cheque Devolvido');
                            exit;
                  End;


                  Self.ObjChequeDevolvido.Cheque.ZerarTabela;
                  Self.ObjChequeDevolvido.Cheque.LocalizaCodigo(QueryPesquisa.fieldbyname('codigo').asstring);
                  Self.ObjChequeDevolvido.Cheque.TabelaparaObjeto;
                  Self.ObjChequeDevolvido.Cheque.Status:=dsedit;
                  Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.Submit_CODIGO(PmotivoDevolucao);
                  if (Self.ObjChequeDevolvido.Cheque.Salvar(False,False,False,'')=False)
                  Then Begin
                            MensagemErro('N�o foi poss�vel salvar a al�nea do cheque devolvido');
                            exit;
                  end;

                  if (PlancaContabilidade=True)
                  Then Begin
                          //essa contabilidade � quando o cheque estava com um fornecedor e nao no banco
                          //Debita Cheques Devolvidos
                          //Credita Fornecedor

                          //Resumindo o Funcionamento da Contabilidade

                          //Debita Fornecedor
                          //Credita Cheque Devolvido

                          //gera-se um titulo a pagar para o fornecedor sem gerar contabilidade
                          //no pagamento


                          //***************************************************
                          //lancando a contabilidade
                          //Debita banco e Credita Fornecedor
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.ZerarTabela;
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_CODIGO(Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Get_NovoCodigo);
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Data(DateToStr(pdatadevolucao));
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ContaDebite(PcodigoPc_portador);
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ContaCredite(PCodigoPC_Fornecedor);
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Valor(querypesquisa.fieldbyname('valor').asstring);
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Historico('REF. CHEQUE DEVOLVIDO NUM '+QueryPesquisa.fieldbyname('numcheque').asstring);
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ObjGerador('OBJCHEQUEDEVOLVIDO');
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_CodGerador(PcodigochequeDevolvido);
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Exportado('N');
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Exporta('S');
                          Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Status:=dsinsert;

                          if (Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Salvar(False)=False)
                          Then Begin
                                    MensagemErro('Erro na tentativa de Gravar a Contabilidade');
                                    exit;
                          end;
                          //****************************************************
                  End;



                  FDataModulo.IBTransaction.CommitRetaining;
                  ImprimeOcorrenciaChequeDevolvido(PcodigochequeDevolvido);
             End;
             BtConsultarClick(sender);
        End;
     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
            freeandnil(ObjQueryTemp);
            objgeratitulo.free;
            Freeandnil(PlistaCheque);
     End;
end;

procedure TFrlancaChequeDevolvido.DbGridPesquisaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then BtGeraContasClick(sender);
end;

procedure TFrlancaChequeDevolvido.edtportador_CPExit(Sender: TObject);
begin
     lbnomeportador_Cp.caption:='';

     if Tedit(Sender).Text=''
     Then exit;

     if (Self.ObjChequeDevolvido.Cheque.Portador.LocalizaCodigo(Tedit(Sender).Text)=False)
     Then Begin
               Tedit(Sender).Text:='';
               exit;
     End;
     Self.ObjChequeDevolvido.Cheque.Portador.TabelaparaObjeto;
     lbnomeportador_Cp.Caption:=Self.ObjChequeDevolvido.Cheque.Portador.Get_Nome;

end;

procedure TFrlancaChequeDevolvido.edtportador_CPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.objChequeDevolvido.Cheque.Portador.edtportadorKeyDown(sender,key,shift);
end;

procedure TFrlancaChequeDevolvido.btConsulta_CPClick(Sender: TObject);
begin
     if (ObjGeraTransferencia.LocalizaHistorico('TRANSFER�NCIA DE CHEQUE DEVOLVIDO')=False)
     then Begin
                MensagemErro('Gerador de Transfer�ncia: "TRANSFER�NCIA DE CHEQUE DEVOLVIDO" n�o encontrado');
                exit;
     End;
     ObjGeraTransferencia.TabelaparaObjeto;

     With QueryPesquisa do
     Begin
          close;
          SQL.clear;
          SQL.add('Select TabChequesPortador.* from TabChequesPortador ');
          sql.add('join tabtalaodecheques on tabchequesportador.codigo=tabtalaodecheques.codigochequeportador');
          sql.add('where TabChequesPortador.ChequedoPortador=''S'' and TabTalaodeCheques.Usado=''S'' ');


          if (edtportador_CP.Text<>'')
          Then Sql.add('and TabTalaodeCheques.Portador='+edtportador_CP.Text);

          if (EdtnumCheque_CP.Text<>'')
          Then Sql.add('and TabTalaodeCheques.numero='+EdtnumCheque_CP.Text);

          if (edtvalor_CP.Text<>'')
          Then Sql.add('and TabTalaodeCheques.valor='+virgulaparaponto(edtvalor_CP.text));

          Sql.add('and TabChequesPortador.Portador<>'+Self.ObjGeraTransferencia.PortadorDestino.Get_CODIGO);

          open;
          formatadbgrid(DbGridPesquisa);
          dbgridpesquisa.setfocus;

     End;

end;

function TFrlancaChequeDevolvido.Cria: boolean;
begin
     limpaedit(Self);
     habilita_campos(Self);
  //try
     Try
        //Self.ObjChequeDevolvido.Cheque=TobjChequesPortador.create;

        //ObjValoresTransferenciaPortador:=TObjValoresTransferenciaPortador.Create;

        Self.ObjChequeDevolvido:=TobjChequeDevolvido.CReate;
        Self.QueryPesquisa.Database:=FdataModulo.Ibdatabase;
        Self.ObjGeraTransferencia:=TObjGeraTransferencia.create;
     Except
           desabilita_campos(Self);
           Messagedlg('N�o foi poss�vel criar o Objeto de Cheques Devolvidos ou o objeto Gerador de Transfer�ncia!',mterror,[mbok],0);
           exit;
     End;
     Self.guia.Tabindex:=0;

     Self.DBComp.DataField:='Comp';
     Self.DBComp.Enabled:=False;
     Self.Dbbanco.DataField:='Banco';
     Self.Dbbanco.Enabled:=False;
     Self.DbAgencia.DataField:='Agencia';
     Self.DbAgencia.Enabled:=False;
     Self.DbC1.DataField:='C1';
     Self.DbC1.Enabled:=False;
     Self.DbConta.DataField:='Conta';
     Self.DbConta.Enabled:=False;
     Self.DbC2.DataField:='C2';
     Self.DbC2.Enabled:=False;
     Self.DbSerie.DataField:='Serie';
     Self.DbSerie.Enabled:=False;
     Self.DbNumCheque.DataField:='NumCheque';
     Self.DbNumCheque.Enabled:=False;
     Self.DbC3.DataField:='C3';
     Self.DbC3.Enabled:=False;
     Self.DbCliente1.DataField:='Cliente1';
     Self.DbCliente1.Enabled:=False;
     Self.DbCpfCliente1.DataField:='CPFCliente1';
     Self.DbCpfCliente1.Enabled:=False;
     Self.DbCliente2.DataField:='Cliente2';
     Self.DbCliente2.Enabled:=False;
     Self.DbCpfCliente2.DataField:='CPFCliente2';
     Self.DbCpfCliente2.Enabled:=False;
     Self.dbcodigobarras.datafield:='CodigodeBarras';
     Self.dbcodigobarras.Enabled:=False;
     Self.DbValor.DataField:='Valor';
     Self.DbValor.Enabled:=False;
     Self.DbVencimento.DataField:='Vencimento';
     Self.DbVencimento.Enabled:=False;
  {
  finally
    ObjChequeDevolvido.Free;
    ObjGeraTransferencia.Free;
  end
  }
end;

function TFrlancaChequeDevolvido.Destroi: boolean;
begin
     if (Self.ObjChequeDevolvido<>nil)
     Then Self.ObjChequeDevolvido.Free;

     if (Self.ObjGeraTransferencia<>nil)
     Then Self.ObjGeraTransferencia.Free;
end;

procedure TFrlancaChequeDevolvido.BtGeraConta_CPClick(Sender: TObject);
var
  ObjQueryTemp:tibquery;
  plancamento:string;
  ptitulo:string;
  PcredorDevedor,PCodigoCredorDevedor:string;
  Ptituloapagar,Ptransferencia:string;
  objgeratitulo:TObjGeraTitulo;
  pdatadevolucao:Tdate;
  PCodigoPC_Fornecedor,PcodigoPc_portador,PcodigochequeDevolvido,PmotivoDevolucao:string;
  cont:integer;
  PlistaCheque:TStringList;
begin

  if (Messagedlg('Tem certeza que deseja lan�ar esse cheque como devolvido?',mtconfirmation,[mbyes,mbno],0)=mrno)
  then exit;

  Ptituloapagar:='';
  Ptransferencia:='';
  PCodigoPC_Fornecedor:='';
  PcodigoPc_portador:='';
  PcodigochequeDevolvido:='';
  PCodigoCredorDevedor:='';

  if (Self.objChequeDevolvido.localizaCheque(QueryPesquisa.FieldByName('codigo').asstring)=True) then
  begin
    MensagemErro('J� existe uma ocorr�ncia de devolu��o para este cheque');
    exit;
  end;

  with FfiltroImp do
  begin
    DesativaGrupos;
    Grupo01.Enabled:=True;
    edtgrupo01.EditMask:=MascaraData;
    LbGrupo01.Caption:='Data de Devolu��o';

    Grupo02.Enabled:=True;
    LbGrupo02.Caption:='Al�nea';
    edtgrupo02.OnKeyDown:=Self.ObjChequeDevolvido.Cheque.EdtMotivoDevolucaoKeyDown;

    Showmodal;

    if (tag=0)
    then exit;

    if (Validadata(1,pdatadevolucao,false)=False)
    Then exit;

    PmotivoDevolucao:='';

    if (edtgrupo02.Text<>'') then
    begin
      try
        strtoint(edtgrupo02.text);
        PmotivoDevolucao:=edtgrupo02.text;

        if (Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.LocalizaCodigo(pmotivodevolucao)=False) then
        begin
          MensagemErro('Al�nea n�o encontrada');
          exit;
        end;
        Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.TabelaparaObjeto;

      except
        MensagemErro('Al�nea inv�lida');
        exit;
      end;
    end;

  end;

  try
    ObjQueryTemp:=tibquery.create(nil);
    ObjQueryTemp.Database:=FdataModulo.ibdatabase;

    objgeratitulo:=TObjGeraTitulo.create;

    PlistaCheque:=TStringList.create;
  except
    MensagemErro('Erro na tentativa de Criar a Query ou os Objetos Geradores');
    exit;
  end;

  try

    with ObjQueryTemp do
    begin
      //localizando as transferencia ou pagamento
      PcredorDevedor:='';
      PCodigoCredorDevedor:='';

      close;
      SQL.clear;
      sql.add('Select TTP.* from TabValoresTransferenciaPortador TVTP');
      sql.add('join TabTransferenciaPortador TTP on TVTP.TransferenciaPortador = TTP.codigo');
      sql.add('where TVTP.valores=' + QueryPesquisa.fieldbyname('codigo').asstring);
      sql.add('order by TTP.data, TTP.codigo');

      //InputBox('','',SQL.text);

      open;
      last;//pegando a ultima transferencia feita com esse cheque

      if (Fieldbyname('codigolancamento').asstring<>'') then //foi um pagamento
      begin
        plancamento:=Fieldbyname('codigolancamento').asstring;
        //gerar o titulo a pagar
        Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.ZerarTabela;
        Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.LocalizaCodigo(fieldbyname('codigolancamento').asstring);
        Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.TabelaparaObjeto;

        if (Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Get_CODIGO='') then//em lote
        begin
          close;
          sql.clear;
          sql.add('Select Tabtitulo.CredorDevedor, TabTitulo.CodigoCredorDevedor');
          sql.add('from tabLancamento');
          sql.add('join tabpendencia on tablancamento.pendencia = tabpendencia.codigo');
          sql.add('join tabtitulo on tabpendencia.titulo = tabtitulo.codigo');
          sql.add('where LancamentoPai = ' +plancamento);
          sql.add('group by Tabtitulo.CredorDevedor, TabTitulo.CodigoCredorDevedor');
          open;
          last;

          if (recordcount=1) then
          begin
            //todos os titulos do mesmo credor
            PcredorDevedor:=Fieldbyname('CredorDevedor').asstring;
            PCodigoCredorDevedor:=Fieldbyname('CodigoCredorDevedor').asstring;
          end
          else
          begin
            //escolhendo qual credor devedor
            FmostraStringGrid.Configuracoesiniciais;
            FmostraStringGrid.StringGrid.colcount:=4;
            FmostraStringGrid.StringGrid.RowCount:=recordcount+1;;
            FmostraStringGrid.StringGrid.cols[0].clear;
            FmostraStringGrid.StringGrid.cols[1].clear;
            FmostraStringGrid.StringGrid.cols[2].clear;
            FmostraStringGrid.StringGrid.cols[3].clear;
            FmostraStringGrid.StringGrid.Cells[0,0]:='XX';
            FmostraStringGrid.StringGrid.Cells[1,0]:='CADASTRO';
            FmostraStringGrid.StringGrid.Cells[2,0]:='CODIGO ';
            FmostraStringGrid.StringGrid.Cells[3,0]:='NOME';
            first;
            cont:=1;

            while not(eof) do
            begin
              PcredorDevedor:=fieldbyname('credordevedor').asstring;
              PCodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;

              Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.LocalizaCodigo(fieldbyname('credordevedor').asstring);
              Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.TabelaparaObjeto;

              FmostraStringGrid.StringGrid.Cells[0,cont]:=fieldbyname('credordevedor').asstring;
              FmostraStringGrid.StringGrid.Cells[1,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Nome;
              FmostraStringGrid.StringGrid.Cells[2,cont]:=fieldbyname('codigocredordevedor').asstring;
              FmostraStringGrid.StringGrid.Cells[3,cont]:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(PcredorDevedor,PCodigoCredorDevedor);
              inc(cont,1);
              next;
            end;

            AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);
            FmostraStringGrid.caption:='Escolha uma op��o para Conta a Pagar';
            FmostraStringGrid.ShowModal;

            if (FmostraStringGrid.tag=0)
            then exit;

            PcredorDevedor:=FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.row];
            PCodigocredorDevedor:=FmostraStringGrid.StringGrid.Cells[2,FmostraStringGrid.StringGrid.row];
          end;
        end //lote
        else
        begin
          PcredorDevedor:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo;
          PcodigocredorDevedor:=Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR;
        end;
        //titulo

        if (objgeratitulo.LocalizaHistorico('T�TULO A PAGAR DE CHEQUES DEVOLVIDOS')=False) then
        begin
          mensagemerro('O gerador de T�tulos com Hist�rico: "T�TULO A PAGAR DE CHEQUES DEVOLVIDOS" n�o foi encontrado');
          exit;
        end;
        objgeratitulo.TabelaparaObjeto;

        with Self.ObjChequeDevolvido.Transferencia.CodigoLancamento.Pendencia.Titulo do
        begin
          ZerarTabela;
          Ptituloapagar:=Get_NovoCodigo;
          Status:=dsinsert;
          Submit_CODIGO(Ptituloapagar);
          Submit_HISTORICO('REF. CHEQUE DEVOLVIDO NUM '+QueryPesquisa.fieldbyname('numcheque').asstring);
          Submit_GERADOR(Objgeratitulo.Get_Gerador);
          Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
          Submit_CREDORDEVEDOR(PcredorDevedor);
          Submit_CODIGOCREDORDEVEDOR(PCodigoCredorDevedor);
          Submit_EMISSAO(datetostr(pdatadevolucao));
          Submit_PRAZO(Objgeratitulo.Get_Prazo);
          Submit_PORTADOR(Objgeratitulo.get_portador);
          Submit_VALOR(querypesquisa.fieldbyname('valor').asstring);
          Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
          Submit_NUMDCTO(querypesquisa.fieldbyname('numcheque').asstring);//vai o numero do cheque
          Submit_GeradoPeloSistema('N');
          Submit_ParcelasIguais(True);
          Submit_ExportaLanctoContaGer('N');

          //salva sem commitar e sem exportar contabilidade
          if (salvar(False,False)=False) then
          begin
            MensagemErro('N�o foi poss�vel salvar o T�tulo a pagar do cheque devolvido');
            exit;
          end;

          PCodigoPC_Fornecedor:=CREDORDEVEDOR.Get_CampoContabilCredorDevedor(PcredorDevedor,PCodigoCredorDevedor);
        end;
      end;//foi um pagamento

      //e por fim transferindo o cheque para um portador de cheques devolvidos

      if (ObjGeraTransferencia.LocalizaHistorico('TRANSFER�NCIA DE CHEQUE DEVOLVIDO')=False) then
      begin
        MensagemErro('Gerador de Transfer�ncia: "TRANSFER�NCIA DE CHEQUE DEVOLVIDO" n�o encontrado');
        exit;
      end;

      ObjGeraTransferencia.TabelaparaObjeto;

      with Self.objChequeDevolvido do
      begin
        Transferencia.ZerarTabela;
        Ptransferencia:=Transferencia.Get_NovoCodigo;
        Transferencia.Submit_codigo(PTransferencia);
        Transferencia.PortadorOrigem.Submit_codigo(QueryPesquisa.fieldbyname('portador').asstring);
        Transferencia.PortadorDestino.submit_codigo(ObjGeraTransferencia.PortadorDestino.get_codigo);
        Transferencia.Grupo.Submit_CODIGO(ObjGeraTransferencia.Grupo.get_codigo);
        Transferencia.Submit_Data(datetostr(pdatadevolucao));
        Transferencia.Submit_Valor('0');//em dinheiro
        Transferencia.Submit_Documento(Querypesquisa.fieldbyname('numcheque').asstring);
        Transferencia.Submit_Complemento('CHEQ.DEVOLVIDO');
        Transferencia.status:=dsinsert;

        PlistaCheque.Clear;
        PlistaCheque.add(QueryPesquisa.Fieldbyname('codigo').asstring);
        Transferencia.Submit_ListaChequesTransferencia(PlistaCheque);

        if (Transferencia.Salvar(False,True,True,False,False,False,'')=False) then
        begin
          MensagemErro('N�o foi poss�vel salvar a transfer�ncia do cheque devolvido');
          exit;
        end;

        Transferencia.PortadorOrigem.LocalizaCodigo(QueryPesquisa.fieldbyname('portador').asstring);
        Transferencia.PortadorOrigem.TabelaparaObjeto;
        PcodigoPc_portador:=Transferencia.PortadorOrigem.Get_CodigoPlanodeContas;

      end;

      //salvando a ocorrencia de cheque devolvido

      with Self.objChequeDevolvido do
      begin
        ZerarTabela;
        Submit_CODIGO('0');
        Cheque.Submit_CODIGO(QueryPesquisa.fieldbyname('codigo').asstring);
        TituloaReceber.Submit_CODIGO('');
        TituloaPagar.Submit_CODIGO(Ptituloapagar);
        Transferencia.Submit_CODIGO(Ptransferencia);
        Status:=dsInsert;

        if (Salvar(False)=False) then
        begin
          MensagemErro('Erro na tentativa de Criar uma Ocorr�ncia de Cheque Devolvido');
          exit;
        end;

        PcodigochequeDevolvido:=Get_CODIGO;

        //lancando a contabilidade
        //Debita banco e Credita Fornecedor
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.ZerarTabela;
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_CODIGO(Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Get_NovoCodigo);
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Data(DateToStr(pdatadevolucao));
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ContaDebite(PcodigoPc_portador);
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ContaCredite(PCodigoPC_Fornecedor);
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Valor(querypesquisa.fieldbyname('valor').asstring);
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Historico('REF. CHEQUE DEVOLVIDO NUM '+QueryPesquisa.fieldbyname('numcheque').asstring);
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_ObjGerador('OBJCHEQUEDEVOLVIDO');
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_CodGerador(PcodigochequeDevolvido);
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Exportado('N');
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Submit_Exporta('S');
        Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Status:=dsinsert;

        if (Self.ObjChequeDevolvido.TituloaReceber.ObjExportaContabilidade.Salvar(False)=False) then
        begin
          MensagemErro('Erro na tentativa de Gravar a Contabilidade');
          exit;
        end;

        //************************
        Self.ObjChequeDevolvido.Cheque.ZerarTabela;
        Self.ObjChequeDevolvido.Cheque.LocalizaCodigo(QueryPesquisa.fieldbyname('codigo').asstring);
        Self.ObjChequeDevolvido.Cheque.TabelaparaObjeto;
        Self.ObjChequeDevolvido.Cheque.Status:=dsedit;
        Self.ObjChequeDevolvido.Cheque.MotivoDevolucao.Submit_CODIGO(PmotivoDevolucao);

        if (Self.ObjChequeDevolvido.Cheque.Salvar(False,False,False,'')=False) then
        begin
          MensagemErro('N�o foi poss�vel salvar a al�nea do cheque devolvido');
          exit;
        end;

        FDataModulo.IBTransaction.CommitRetaining;
        ImprimeOcorrenciaChequeDevolvido(PcodigochequeDevolvido);
      end;

      btConsulta_CPClick(sender);

    end;
  finally
    FDataModulo.IBTransaction.RollbackRetaining;
    freeandnil(ObjQueryTemp);
    objgeratitulo.free;
    Freeandnil(PlistaCheque);
  end;
end;

end.
