object FCONVENIOSBOLETO: TFCONVENIOSBOLETO
  Left = 452
  Top = 210
  Width = 847
  Height = 636
  Caption = 'Cadastro de Conv'#234'nios em Boletos Banc'#225'rios - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 831
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 578
      Top = 0
      Width = 253
      Height = 51
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Conv'#234'nio Banc'#225'rio de Boletos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Btnovo: TBitBtn
      Left = 3
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 53
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 103
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 153
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 253
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 303
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object btexcluir: TBitBtn
      Left = 203
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 403
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object Btopcoes: TBitBtn
      Left = 353
      Top = -1
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = BtopcoesClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 548
    Width = 831
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 3
    DesignSize = (
      831
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 831
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 533
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 1002
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 1002
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 51
    Width = 831
    Height = 333
    Align = alTop
    BevelOuter = bvNone
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 0
      Top = 0
      Width = 831
      Height = 333
      Align = alClient
      Stretch = True
    end
    object LbCODIGO: TLabel
      Left = 20
      Top = 8
      Width = 44
      Height = 13
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 20
      Top = 29
      Width = 57
      Height = 13
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Lbnomeportador: TLabel
      Left = 236
      Top = 29
      Width = 57
      Height = 13
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNome: TLabel
      Left = 20
      Top = 50
      Width = 37
      Height = 13
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNomeBanco: TLabel
      Left = 20
      Top = 71
      Width = 100
      Height = 13
      Caption = 'Nome do Banco'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbCodigoBAnco: TLabel
      Left = 20
      Top = 92
      Width = 107
      Height = 13
      Caption = 'C'#243'digo do Banco'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbDgBanco: TLabel
      Left = 202
      Top = 92
      Width = 5
      Height = 13
      Caption = '-'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object LbLocalPagamento: TLabel
      Left = 236
      Top = 92
      Width = 131
      Height = 13
      Caption = 'Local de Pagamento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNomecedente: TLabel
      Left = 20
      Top = 134
      Width = 118
      Height = 13
      Caption = 'Nome  do Cedente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbCarteira: TLabel
      Left = 236
      Top = 155
      Width = 53
      Height = 13
      Caption = 'Carteira'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbCompCarteira: TLabel
      Left = 20
      Top = 155
      Width = 97
      Height = 13
      Caption = 'Comp. Carteira'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbAgencia: TLabel
      Left = 20
      Top = 179
      Width = 52
      Height = 13
      Caption = 'Ag'#234'ncia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbDGAgencia: TLabel
      Left = 417
      Top = 176
      Width = 6
      Height = 13
      Caption = '-'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbContaCorrente: TLabel
      Left = 236
      Top = 176
      Width = 98
      Height = 13
      Caption = 'Conta Corrente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbDgContaCorrente: TLabel
      Left = 202
      Top = 177
      Width = 6
      Height = 13
      Caption = '-'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Lbnumeroconvenio: TLabel
      Left = 20
      Top = 113
      Width = 135
      Height = 13
      Caption = 'N'#250'mero do Conv'#234'nio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbEspecieDoc: TLabel
      Left = 20
      Top = 218
      Width = 127
      Height = 13
      Caption = 'Esp'#233'cie Documento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbAceite: TLabel
      Left = 236
      Top = 218
      Width = 41
      Height = 13
      Caption = 'Aceite'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNumContaResponsavel: TLabel
      Left = 451
      Top = 218
      Width = 96
      Height = 13
      Caption = 'N'#186' Conta Resp.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbEspecieMoeda: TLabel
      Left = 20
      Top = 239
      Width = 96
      Height = 13
      Caption = 'Esp'#233'cie Moeda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbQuantidadeMoeda: TLabel
      Left = 236
      Top = 239
      Width = 68
      Height = 13
      Caption = 'Qtd Moeda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbValorMoeda: TLabel
      Left = 451
      Top = 239
      Width = 80
      Height = 13
      Caption = 'Valor Moeda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 20
      Top = 257
      Width = 83
      Height = 13
      Caption = 'Informa'#231#245'es'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 20
      Top = 197
      Width = 102
      Height = 13
      Caption = 'Juros % Mensal'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 236
      Top = 197
      Width = 121
      Height = 13
      Caption = 'Dias para protesto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 451
      Top = 155
      Width = 165
      Height = 13
      Caption = 'Taxa Cobrado pelo Banco'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label7: TLabel
      Left = 236
      Top = 113
      Width = 121
      Height = 13
      Caption = 'C'#243'digo do Cedente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 451
      Top = 113
      Width = 177
      Height = 13
      Caption = 'Qtde D'#237'gitos Nosso N'#250'mero'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 492
      Top = 5
      Width = 97
      Height = 13
      Caption = 'Pr'#233'-Impresso?'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 451
      Top = 257
      Width = 61
      Height = 13
      Caption = 'Vari'#225'veis'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label45: TLabel
      Left = 620
      Top = 6
      Width = 66
      Height = 13
      Caption = 'CobreBem'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCODIGO: TEdit
      Left = 160
      Top = 5
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object edtportador: TEdit
      Left = 160
      Top = 26
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 3
      OnExit = edtportadorExit
      OnKeyDown = edtportadorKeyDown
    end
    object EdtNome: TEdit
      Left = 160
      Top = 47
      Width = 550
      Height = 19
      MaxLength = 100
      TabOrder = 4
    end
    object EdtNomeBanco: TEdit
      Left = 160
      Top = 68
      Width = 550
      Height = 19
      MaxLength = 100
      TabOrder = 5
    end
    object EdtCodigoBAnco: TEdit
      Left = 160
      Top = 89
      Width = 41
      Height = 19
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 5
      ParentFont = False
      TabOrder = 6
    end
    object EdtDgBanco: TEdit
      Left = 208
      Top = 89
      Width = 21
      Height = 19
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      MaxLength = 1
      ParentFont = False
      TabOrder = 7
    end
    object EdtLocalPagamento: TEdit
      Left = 376
      Top = 89
      Width = 334
      Height = 19
      MaxLength = 150
      TabOrder = 8
    end
    object EdtNomecedente: TEdit
      Left = 160
      Top = 131
      Width = 550
      Height = 19
      MaxLength = 150
      TabOrder = 12
    end
    object EdtCarteira: TEdit
      Left = 376
      Top = 152
      Width = 70
      Height = 19
      MaxLength = 3
      TabOrder = 14
    end
    object EdtCompCarteira: TEdit
      Left = 160
      Top = 152
      Width = 70
      Height = 19
      MaxLength = 5
      TabOrder = 13
    end
    object EdtAgencia: TEdit
      Left = 160
      Top = 173
      Width = 41
      Height = 19
      MaxLength = 10
      TabOrder = 16
      Text = '59051'
    end
    object EdtDGAgencia: TEdit
      Left = 208
      Top = 173
      Width = 21
      Height = 19
      MaxLength = 2
      TabOrder = 17
    end
    object EdtContaCorrente: TEdit
      Left = 376
      Top = 173
      Width = 41
      Height = 19
      MaxLength = 10
      TabOrder = 18
    end
    object EdtDgContaCorrente: TEdit
      Left = 423
      Top = 173
      Width = 23
      Height = 19
      MaxLength = 2
      TabOrder = 19
    end
    object Edtnumeroconvenio: TEdit
      Left = 160
      Top = 110
      Width = 70
      Height = 19
      MaxLength = 10
      TabOrder = 9
    end
    object EdtEspecieDoc: TEdit
      Left = 160
      Top = 215
      Width = 70
      Height = 19
      MaxLength = 5
      TabOrder = 22
    end
    object EdtAceite: TEdit
      Left = 376
      Top = 215
      Width = 70
      Height = 19
      MaxLength = 1
      TabOrder = 23
    end
    object EdtNumContaResponsavel: TEdit
      Left = 641
      Top = 215
      Width = 70
      Height = 19
      MaxLength = 10
      TabOrder = 24
    end
    object EdtEspecieMoeda: TEdit
      Left = 160
      Top = 236
      Width = 70
      Height = 19
      MaxLength = 3
      TabOrder = 25
    end
    object EdtQuantidadeMoeda: TEdit
      Left = 376
      Top = 236
      Width = 70
      Height = 19
      MaxLength = 12
      TabOrder = 26
    end
    object EdtValorMoeda: TEdit
      Left = 641
      Top = 236
      Width = 70
      Height = 19
      MaxLength = 12
      TabOrder = 27
    end
    object Memoinstrucoes: TMemo
      Left = 160
      Top = 258
      Width = 286
      Height = 69
      Lines.Strings = (
        'Memo1')
      TabOrder = 29
    end
    object edttaxamensal: TEdit
      Left = 160
      Top = 194
      Width = 70
      Height = 19
      MaxLength = 5
      TabOrder = 20
    end
    object edtquantdiasprotesto: TEdit
      Left = 376
      Top = 194
      Width = 70
      Height = 19
      MaxLength = 5
      TabOrder = 21
    end
    object edttaxabanco: TEdit
      Left = 641
      Top = 152
      Width = 70
      Height = 19
      MaxLength = 12
      TabOrder = 15
    end
    object edtcodigocedente: TEdit
      Left = 376
      Top = 110
      Width = 70
      Height = 19
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
    end
    object edtQtdeDigitosNossoNumero: TEdit
      Left = 641
      Top = 110
      Width = 70
      Height = 19
      MaxLength = 10
      TabOrder = 11
    end
    object LboxVariaveis: TListBox
      Left = 584
      Top = 257
      Width = 128
      Height = 69
      TabStop = False
      ItemHeight = 13
      TabOrder = 28
      OnDblClick = LboxVariaveisDblClick
    end
    object checkpreimpresso: TCheckBox
      Left = 596
      Top = 5
      Width = 13
      Height = 13
      TabOrder = 1
    end
    object CheckCobreBem: TCheckBox
      Left = 692
      Top = 6
      Width = 13
      Height = 13
      Caption = 'CheckCobreBem'
      TabOrder = 2
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 384
    Width = 831
    Height = 164
    Align = alClient
    PageIndex = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 2
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Caixa Econ'#244'mica'
      object Label13: TLabel
        Left = 72
        Top = 42
        Width = 33
        Height = 14
        Caption = 'SICOB'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label15: TLabel
        Left = 72
        Top = 67
        Width = 44
        Height = 14
        Caption = 'Carteira'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtCodigoSICOB_caixa: TEdit
        Left = 128
        Top = 40
        Width = 121
        Height = 19
        TabOrder = 0
      end
      object edtCodigoCarteira_Caixa: TEdit
        Left = 128
        Top = 64
        Width = 121
        Height = 19
        TabOrder = 1
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Sicredi'
      object Label19: TLabel
        Left = 16
        Top = 21
        Width = 168
        Height = 14
        Caption = 'Tipo de Cobran'#231'a  (Posi'#231#227'o 20)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label20: TLabel
        Left = 16
        Top = 44
        Width = 220
        Height = 14
        Caption = 'Cooperativa de Cr'#233'dito (posi'#231#245'es 31-34)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label21: TLabel
        Left = 368
        Top = 43
        Width = 215
        Height = 14
        Caption = 'Unida de Atendimento (posi'#231#245'es 35-36)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 368
        Top = 21
        Width = 157
        Height = 14
        Caption = 'Tipo de Carteira (Posi'#231#227'o 21)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label22: TLabel
        Left = 16
        Top = 66
        Width = 216
        Height = 14
        Caption = 'C'#243'digo do Cedente (posi'#231#245'es de 37-41)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label23: TLabel
        Left = 368
        Top = 66
        Width = 138
        Height = 14
        Caption = 'Filler - zeros (posi'#231#227'o 42)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label24: TLabel
        Left = 16
        Top = 89
        Width = 138
        Height = 14
        Caption = 'Filler - zeros (posi'#231#227'o 43)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label14: TLabel
        Left = 368
        Top = 89
        Width = 109
        Height = 14
        Caption = 'Byte Nosso N'#250'mero'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtposicao20_sicred: TEdit
        Left = 240
        Top = 18
        Width = 121
        Height = 19
        TabOrder = 0
      end
      object edtposicao31_34_sicred: TEdit
        Left = 240
        Top = 41
        Width = 121
        Height = 19
        TabOrder = 3
        Text = '  '
      end
      object edtposicao35_36_sicred: TEdit
        Left = 587
        Top = 40
        Width = 121
        Height = 19
        TabOrder = 2
        Text = '  '
      end
      object edtposicao21_sicred: TEdit
        Left = 587
        Top = 18
        Width = 121
        Height = 19
        TabOrder = 1
      end
      object edtposicao37_41_sicred: TEdit
        Left = 240
        Top = 63
        Width = 121
        Height = 19
        TabOrder = 4
        Text = '  '
      end
      object edtposicao42_sicred: TEdit
        Left = 587
        Top = 63
        Width = 121
        Height = 19
        TabOrder = 5
        Text = '  '
      end
      object edtposicao43_sicred: TEdit
        Left = 240
        Top = 86
        Width = 121
        Height = 19
        TabOrder = 6
        Text = '  '
      end
      object edtBYTE_NOSSONUMERO_SICRED: TEdit
        Left = 587
        Top = 86
        Width = 121
        Height = 19
        TabOrder = 7
        Text = '  '
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&3 - Unibanco'
      object Label16: TLabel
        Left = 16
        Top = 34
        Width = 153
        Height = 14
        Caption = 'C'#243'digo para Transa'#231#227'o CVT:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 16
        Top = 57
        Width = 188
        Height = 14
        Caption = 'N'#186' do Cliente no C'#243'digo de Barras:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label28: TLabel
        Left = 16
        Top = 80
        Width = 225
        Height = 14
        Caption = 'Posi'#231#245'es de 28 a 29 no C'#243'digo de Barras:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtcodigotransacao_unibanco: TEdit
        Left = 248
        Top = 31
        Width = 121
        Height = 19
        TabOrder = 0
      end
      object edtnumerocliente_UNIBANCO: TEdit
        Left = 248
        Top = 54
        Width = 121
        Height = 19
        TabOrder = 1
        Text = 'edtCodigoCarteira_Caixa'
      end
      object edtposicoes_28_a_29_UNIBANCO: TEdit
        Left = 248
        Top = 77
        Width = 121
        Height = 19
        TabOrder = 2
        Text = 'edtCodigoCarteira_Caixa'
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&4 - HSBC'
      object Label25: TLabel
        Left = 40
        Top = 35
        Width = 96
        Height = 14
        Caption = 'Tipo Identificador'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label26: TLabel
        Left = 40
        Top = 58
        Width = 89
        Height = 14
        Caption = 'C'#243'digo Cedente'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label27: TLabel
        Left = 40
        Top = 81
        Width = 64
        Height = 14
        Caption = 'C'#243'digo CNR'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edtTipoIdentificador_HSBC: TEdit
        Left = 144
        Top = 32
        Width = 121
        Height = 19
        MaxLength = 1
        TabOrder = 0
        Text = 'edtTipoIdentificador_HSBC'
      end
      object edtCodigoCedente_HSBC: TEdit
        Left = 144
        Top = 55
        Width = 121
        Height = 19
        MaxLength = 7
        TabOrder = 1
        Text = 'edtCodigoCedente_HSBC'
      end
      object edtCodigoCNR_HSBC: TEdit
        Left = 145
        Top = 78
        Width = 121
        Height = 19
        MaxLength = 1
        TabOrder = 2
        Text = 'edtCodigoCNR_HSBC'
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&5 - Informa'#231#245'es Complementares'
      object Bevel1: TBevel
        Left = 0
        Top = 24
        Width = 812
        Height = 150
        Shape = bsFrame
      end
      object Label11: TLabel
        Left = 4
        Top = 32
        Width = 47
        Height = 13
        Caption = 'Sacado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 412
        Top = 32
        Width = 240
        Height = 13
        Caption = 'Vari'#225'veis para Impress'#227'o no Sacado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = -12
        Top = 0
        Width = 773
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'SOMENTE PARA BOLETOS PR'#201'-IMPRESSOS SEM REGISTRO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object MemoSacado: TMemo
        Left = 4
        Top = 48
        Width = 404
        Height = 76
        Lines.Strings = (
          'Memo1')
        TabOrder = 0
      end
      object lbvariaveissacado: TListBox
        Left = 428
        Top = 48
        Width = 274
        Height = 76
        TabStop = False
        ItemHeight = 13
        TabOrder = 1
        OnDblClick = lbvariaveissacadoDblClick
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&6 - CobreBem'
      object GuiaCobrebem: TTabbedNotebook
        Left = 1
        Top = -2
        Width = 818
        Height = 143
        TabFont.Charset = ANSI_CHARSET
        TabFont.Color = clBtnText
        TabFont.Height = -11
        TabFont.Name = 'Verdana'
        TabFont.Style = []
        TabOrder = 0
        object TTabPage
          Left = 4
          Top = 24
          Caption = 'Conta Corrente'
          object Label30: TLabel
            Left = 1
            Top = 2
            Width = 86
            Height = 14
            Caption = 'Codigo Ag'#234'ncia'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label29: TLabel
            Left = 1
            Top = 25
            Width = 132
            Height = 14
            Caption = 'N'#250'mero Conta Corrente'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label31: TLabel
            Left = 2
            Top = 48
            Width = 89
            Height = 14
            Caption = 'C'#243'digo Cedente'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label32: TLabel
            Left = 260
            Top = 4
            Width = 114
            Height = 14
            Caption = 'In'#237'cio Nosso N'#250'mero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label33: TLabel
            Left = 260
            Top = 27
            Width = 105
            Height = 14
            Caption = 'Fim Nosso Numero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label34: TLabel
            Left = 260
            Top = 49
            Width = 131
            Height = 14
            Caption = 'Proximo Nosso N'#250'mero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label35: TLabel
            Left = 523
            Top = 72
            Width = 137
            Height = 14
            Caption = 'OutroDadoConfiguracao1'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label46: TLabel
            Left = 523
            Top = 4
            Width = 120
            Height = 14
            Caption = 'Perc. Juros dia atraso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object label90: TLabel
            Left = 523
            Top = 27
            Width = 99
            Height = 14
            Caption = 'Perc. Multa atraso'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object CobreBemPercentualDesconto: TLabel
            Left = 523
            Top = 50
            Width = 83
            Height = 14
            Caption = 'Perc. Desconto'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label47: TLabel
            Left = 2
            Top = 71
            Width = 121
            Height = 14
            Caption = 'Banco Imprime Boleto'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label48: TLabel
            Left = 260
            Top = 72
            Width = 101
            Height = 14
            Caption = 'Dias para Protesto'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label49: TLabel
            Left = 2
            Top = 95
            Width = 119
            Height = 14
            Caption = 'Dias Limite de Vencto'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label50: TLabel
            Left = 260
            Top = 94
            Width = 99
            Height = 14
            Caption = 'Numero Remessa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label51: TLabel
            Left = 523
            Top = 92
            Width = 137
            Height = 14
            Caption = 'OutroDadoConfiguracao2'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdtCobreBemCodigoAgencia: TEdit
            Left = 135
            Top = 1
            Width = 121
            Height = 19
            TabOrder = 0
          end
          object EdtCobreBemNumeroContaCorrente: TEdit
            Left = 135
            Top = 24
            Width = 121
            Height = 19
            TabOrder = 1
          end
          object EdtCobreBemCodigoCedente: TEdit
            Left = 136
            Top = 47
            Width = 121
            Height = 19
            TabOrder = 2
          end
          object EdtCobreBemInicioNossoNumero: TEdit
            Left = 395
            Top = 1
            Width = 121
            Height = 19
            TabOrder = 4
          end
          object EdtCobreBemFimNossoNumero: TEdit
            Left = 396
            Top = 24
            Width = 121
            Height = 19
            TabOrder = 5
          end
          object EdtCobreBemProximoNossoNumero: TEdit
            Left = 396
            Top = 47
            Width = 121
            Height = 19
            TabOrder = 6
          end
          object EdtCobreBemOutroDadoConfiguracao1: TEdit
            Left = 666
            Top = 70
            Width = 121
            Height = 19
            TabOrder = 12
          end
          object EdtPercentualJurosDiasAtraso: TEdit
            Left = 664
            Top = 1
            Width = 121
            Height = 19
            TabOrder = 9
          end
          object EdtPercentualMultaAtraso: TEdit
            Left = 665
            Top = 24
            Width = 121
            Height = 19
            TabOrder = 10
          end
          object EdtPercentualDesconto: TEdit
            Left = 665
            Top = 47
            Width = 121
            Height = 19
            TabOrder = 11
          end
          object ComboCobreBemBancoImprimeBoleto: TComboBox
            Left = 137
            Top = 69
            Width = 59
            Height = 21
            ItemHeight = 13
            TabOrder = 3
            OnKeyDown = ComboCobreBemBancoImprimeBoletoKeyDown
            Items.Strings = (
              'SIM'
              'N'#195'O')
          end
          object EdtCobreBemDiasProtesto: TEdit
            Left = 396
            Top = 70
            Width = 121
            Height = 19
            TabOrder = 7
          end
          object EdtLimiteVencimento: TEdit
            Left = 137
            Top = 93
            Width = 121
            Height = 19
            TabOrder = 8
          end
          object EdtCobreBemNumeroRemessa: TEdit
            Left = 396
            Top = 92
            Width = 121
            Height = 19
            TabOrder = 13
          end
          object EdtCobreBemOutroDadoConfiguracao2: TEdit
            Left = 666
            Top = 92
            Width = 121
            Height = 19
            TabOrder = 14
          end
        end
        object TTabPage
          Left = 4
          Top = 24
          Caption = 'Caminhos'
          object Label37: TLabel
            Left = 1
            Top = 2
            Width = 105
            Height = 14
            Caption = 'Arquivo de Licen'#231'a'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label38: TLabel
            Left = 294
            Top = 4
            Width = 103
            Height = 14
            Caption = 'Diret'#243'rio Remessa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label39: TLabel
            Left = 565
            Top = 3
            Width = 95
            Height = 14
            Caption = 'Diretorio Retorno'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label40: TLabel
            Left = 1
            Top = 25
            Width = 95
            Height = 14
            Caption = 'Arquivo Logotipo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label41: TLabel
            Left = 2
            Top = 48
            Width = 129
            Height = 14
            Caption = 'Imagens C'#243'digo Barras'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label42: TLabel
            Left = 295
            Top = 27
            Width = 98
            Height = 14
            Caption = 'Arquivo Remessa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label43: TLabel
            Left = 295
            Top = 50
            Width = 92
            Height = 14
            Caption = 'Layout Remessa'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label36: TLabel
            Left = 565
            Top = 26
            Width = 90
            Height = 14
            Caption = 'Arquivo Retorno'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label44: TLabel
            Left = 565
            Top = 48
            Width = 84
            Height = 14
            Caption = 'Layout Retorno'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object btOpenDialogArquivoLicenca: TSpeedButton
            Left = 247
            Top = 1
            Width = 23
            Height = 18
            Caption = '...'
            OnClick = btOpenDialogArquivoLicencaClick
          end
          object btOpenDialogArquivoLogotipo: TSpeedButton
            Left = 247
            Top = 24
            Width = 23
            Height = 18
            Caption = '...'
            OnClick = btOpenDialogArquivoLogotipoClick
          end
          object btOpenDialogImagensCodigoBarras: TSpeedButton
            Left = 247
            Top = 47
            Width = 23
            Height = 18
            Caption = '...'
            OnClick = btOpenDialogImagensCodigoBarrasClick
          end
          object btOpenDialogDiretorioRemessa: TSpeedButton
            Left = 514
            Top = 1
            Width = 23
            Height = 18
            Caption = '...'
            OnClick = btOpenDialogDiretorioRemessaClick
          end
          object btOpenDialogDiretorioRetorno: TSpeedButton
            Left = 777
            Top = -1
            Width = 23
            Height = 18
            Caption = '...'
            OnClick = btOpenDialogDiretorioRetornoClick
          end
          object LayoutBoleto: TLabel
            Left = 2
            Top = 71
            Width = 75
            Height = 14
            Caption = 'Layout Boleto'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object EdtCobreBemArquivoLicenca: TEdit
            Left = 135
            Top = 1
            Width = 110
            Height = 19
            TabOrder = 0
          end
          object EdtCobreBemDiretorioRemessa: TEdit
            Left = 401
            Top = 1
            Width = 110
            Height = 19
            TabOrder = 4
          end
          object EdtCobreBemDiretorioRetorno: TEdit
            Left = 666
            Top = 1
            Width = 110
            Height = 19
            TabOrder = 7
          end
          object edtCobreBemArquivoLogotipo: TEdit
            Left = 135
            Top = 24
            Width = 110
            Height = 19
            TabOrder = 1
          end
          object EdtCobreBemImagensCodigoBarras: TEdit
            Left = 136
            Top = 47
            Width = 110
            Height = 19
            TabOrder = 2
          end
          object EdtCobreBemLayoutRemessa: TEdit
            Left = 402
            Top = 47
            Width = 110
            Height = 19
            TabOrder = 6
          end
          object EdtCobreBemArquivoRemessa: TEdit
            Left = 402
            Top = 24
            Width = 110
            Height = 19
            TabOrder = 5
          end
          object EdtCobreBemArquivoRetorno: TEdit
            Left = 666
            Top = 24
            Width = 110
            Height = 19
            TabOrder = 8
          end
          object EdtCobreBemLayoutRetorno: TEdit
            Left = 666
            Top = 46
            Width = 110
            Height = 19
            TabOrder = 9
          end
          object ComboCobreBemLayOutBoleto: TComboBox
            Left = 137
            Top = 70
            Width = 377
            Height = 21
            ItemHeight = 13
            TabOrder = 3
            Items.Strings = (
              'Padrao'
              'PadraoEnderecadoVerso'
              'PadraoHTML'
              'InvertidoEnderecadoVerso'
              'PadraoReciboPersonalizadoHTML'
              'PadraoEnderecadoVersoReciboPersonalizado'
              'Invertido'
              'InvertidoEnderecadoVersoReciboPersonalizado'
              'CarnetReciboLateralDireita'
              'PadraoVersoPersonalizado'
              'CarnetReciboLateralEsquerda'
              'InvertidoVersoPersonalizado'
              'PadraoReciboPersonalizado'
              'PadraoVersoPersonalizadoReciboPersonalizado'
              'InvertidoReciboPersonalizado'
              'InvertidoVersoPersonalizadoReciboPersonalizado'
              'CarnetReciboLateralDireitaPersonalizado'
              'CarnetReciboTopo'
              'CarnetReciboLateralEsquerdaPersonalizado'
              'CarnetReciboTopoPersonalizado'
              'ReciboLateralEsquerda'
              'ReciboLateralEsquerdaPersonalizado')
          end
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&7 - Banco do Brasil'
    end
  end
  object OpenDialog: TOpenDialog
    Left = 720
    Top = 76
  end
end
