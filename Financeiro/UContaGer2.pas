unit UContaGer2;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjContaGer;

type
  TFcontager2 = class(TForm)
    Guia: TTabbedNotebook;
    EdtCodigo: TEdit;
    Label1: TLabel;
    edtnome: TEdit;
    Label2: TLabel;
    Combotipo: TComboBox;
    Label3: TLabel;
    edtCodigoPlanodeContas: TMaskEdit;
    Label18: TLabel;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    BtCancelar: TBitBtn;
    btsair: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure edtCodigoPlanodeContasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCodigoPlanodeContasKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
         ObjContaGer:TObjContaGer;
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fcontager2: TFcontager2;


implementation

uses UessencialGlobal, Upesquisa, UPLanodeContas, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFcontager2.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjContaGer do
    Begin
         Submit_CODIGO                  (edtCODIGO.text);
         Submit_Nome                    (edtnome.text);
         Submit_Tipo                    (combotipo.text[1]);
         Submit_CodigoPlanodeContas     (edtCodigoPlanodeContas.text);

         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFcontager2.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjContaGer do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtnome.text            :=Get_nome            ;
        edtCodigoPlanodeContas.text :=Get_CodigoPlanodeContas;

        If (Get_tipo='D')
        Then Combotipo.itemindex:=0
        Else If (Get_tipo='C')
             then combotipo.itemindex:=1
             Else combotipo.itemindex:=2;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFcontager2.TabelaParaControles: Boolean;
begin
     Self.ObjContaGer.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFcontager2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Try
         If (Self.ObjContaGer=Nil)
         Then exit;

          If (Self.ObjContaGer.status<>dsinactive)
          Then Begin
                    Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
                    abort;
                    exit;
          End;
          Self.ObjContaGer.free;
     Finally
            if (SistemaMdiGlobal=True)
            Then Begin
                      Action := caFree;
                      Self := nil;
            End;
     End;
end;

procedure TFcontager2.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFcontager2.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFcontager2.BtNovoClick(Sender: TObject);
begin
     Limpaedit(Self);
     habilita_campos(Self);
     desab_botoes(Self);
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     Self.ObjContaGer.status:=dsInsert;
     guia.PageIndex:=0;
     //edtcodigo.text:='0';
     edtcodigo.text:=Self.ObjContaGer.get_novocodigo;
     //Edtcodigo.Enabled:=False;
     edtnome.SetFocus;

end;

procedure TFcontager2.BtCancelarClick(Sender: TObject);
begin
     Self.ObjContaGer.cancelar;

     Limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);


end;

procedure TFcontager2.BtgravarClick(Sender: TObject);
begin

     If Self.ObjContaGer.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjContaGer.salvar=False)
     Then exit;

     habilita_botoes(Self);
     Limpaedit(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFcontager2.PreparaAlteracao;
begin
     guia.PageIndex:=0;
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     Self.ObjContaGer.Status:=dsEdit;
     edtNome.setfocus;
end;

procedure TFcontager2.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjContaGer.Get_pesquisa,Self.ObjContaGer.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjContaGer.status<>dsinactive
                                  then exit;

                                  If (Self.ObjContaGer.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFcontager2.btalterarClick(Sender: TObject);
begin
    If (Self.ObjContaGer.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFcontager2.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjContaGer.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjContaGer.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjContaGer.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     Limpaedit(Self);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFcontager2.btrelatoriosClick(Sender: TObject);
begin
     Self.ObjContaGer.Imprime;
end;

procedure TFcontager2.edtCodigoPlanodeContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FplanodeCOntas:=TFplanodeCOntas.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjContaGer.Get_PesquisaPlanodeContas,Self.ObjContaGer.Get_TituloPesquisaPlanodeContas,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtCodigoPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;

procedure TFcontager2.edtCodigoPlanodeContasKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not (key in ['0'..'9',#8])
     Then key:=#0;

end;


procedure TFcontager2.FormCreate(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjContaGer:=TObjContaGer.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto Tipo de Professor!',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFcontage
