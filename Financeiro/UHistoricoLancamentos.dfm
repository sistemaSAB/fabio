object FhistoricoLancamento: TFhistoricoLancamento
  Left = 633
  Top = 344
  Width = 800
  Height = 429
  Caption = 'Hist'#243'rico de Lan'#231'amento em Pend'#234'ncias - EXCLAIM TECNOLOGIA'
  Color = clBtnFace
  Constraints.MinHeight = 429
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object spl1: TSplitter
    Left = 0
    Top = 201
    Width = 784
    Height = 9
    Cursor = crVSplit
    Align = alTop
  end
  object SGLancto: TStringGrid
    Left = 0
    Top = 41
    Width = 784
    Height = 160
    Align = alTop
    Ctl3D = False
    FixedCols = 0
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [goHorzLine, goRangeSelect, goRowSizing, goColSizing]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    OnKeyDown = SGLanctoKeyDown
    OnSelectCell = SGLanctoSelectCell
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    Color = 10643006
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 1
    DesignSize = (
      784
      41)
    object Label1: TLabel
      Left = 9
      Top = 13
      Width = 68
      Height = 19
      Caption = 'Pend'#234'ncia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbPendencia: TLabel
      Left = 139
      Top = 13
      Width = 79
      Height = 19
      Caption = 'lbpendencia'
      Color = 8539648
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object btexcluirlancamento: TButton
      Left = 646
      Top = 8
      Width = 131
      Height = 25
      Anchors = [akRight]
      Caption = '&Excluir Lan'#231'amento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btexcluirlancamentoClick
    end
    object btRecibo: TButton
      Left = 510
      Top = 8
      Width = 131
      Height = 25
      Anchors = [akRight]
      Caption = '&Recibo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btReciboClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 373
    Width = 784
    Height = 18
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'F8 - Imprime Recibo de Pagamento do lan'#231'amento selecionado'
    TabOrder = 2
  end
  object gridLancto: TDBGrid
    Left = 0
    Top = 210
    Width = 784
    Height = 163
    Align = alClient
    Ctl3D = False
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentCtl3D = False
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
end
