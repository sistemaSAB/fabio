�
 TFTITULO 0�4  TPF0TFtituloFtituloLeft}Top/Width�HeightKCaptionCadastro de TitulosColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
KeyPreview	OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnCreate
FormCreate
OnKeyPressFormKeyPressOnShowFormShowPixelsPerInch`
TextHeight TTabbedNotebookGuiaLeft Top Width�Height%AlignalClientFont.CharsetDEFAULT_CHARSET
Font.ColorclNavyFont.Height�	Font.NameMS Sans Serif
Font.Style TabFont.CharsetDEFAULT_CHARSETTabFont.Color	clBtnTextTabFont.Height�TabFont.NameMS Sans SerifTabFont.Style TabOrder OnChange
GuiaChange TTabPage LeftTopCaption&1 - Principal TLabelLabel1LeftTopWidth"HeightCaptionCodigoFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel2LeftTop?Width-HeightCaption
   HistóricoFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel3LeftTophWidth+HeightCaptionCadastroFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel4LeftTop� WidthrHeightCaption   Código Credor/DevedorFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel5LeftTop� Width(HeightCaption   EmissãoFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel6LeftTop� WidthHeightCaptionPrazoFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel9LeftTopWidth*HeightCaptionPortadorFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel10LeftTop5WidthHeightCaptionValorFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel11LeftTop_WidthMHeightCaptionConta GerencialFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel13Left�TopWidth6HeightCaption   Nº GeradorFont.CharsetANSI_CHARSET
Font.ColorclBlueFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFontVisible  TLabel
LbPortadorLeft^TopWidth1HeightCaptionPortadorFont.CharsetANSI_CHARSET
Font.ColorclTealFont.Height�	Font.NameVerdana
Font.Style 
ParentFont  TLabelLbContaGerencialLeftfTopsWidth1HeightCaptionPortadorFont.CharsetANSI_CHARSET
Font.ColorclTealFont.Height�	Font.NameVerdana
Font.Style 
ParentFont  TLabelLabel7LeftTop�Width'HeightCaption   Nº DctoFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLbNomeCredorDevedorLeft]Top� WidthHeightCaption...Font.CharsetANSI_CHARSET
Font.ColorclTealFont.Height�	Font.NameVerdana
Font.Style 
ParentFont  TLabelLbCRCPLeft Top Width�Height	AlignmenttaCenterAutoSizeCaptionLbCRCPFont.CharsetDEFAULT_CHARSET
Font.ColorclTealFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFont  TLabelLabel8LeftTop�Width7HeightCaptionNota FiscalFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabelLabel14LeftTop�WidthcHeightCaptionSub-Conta GerencialFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TLabellbnomesubcontagerencialLeftfTop�Width1HeightCaptionPortadorFont.CharsetANSI_CHARSET
Font.ColorclTealFont.Height�	Font.NameVerdana
Font.Style 
ParentFont  TLabelLabel15Left�TophWidth� HeightCaption,   Observação (Para pular linha CTRL + ENTER)Font.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameTimes New Roman
Font.Style 
ParentFont  TEdit	EdtCodigoLeftTop%WidthHHeightCtl3D	MaxLength	ParentCtl3DTabOrder  TEditedthistoricoLeftBTopNWidthoHeightCtl3DParentCtl3DTabOrder  	TComboBoxcombocredordevedorLeftTopwWidth� Height	BevelKindbkSoftCtl3D
ItemHeightParentCtl3DTabOrderOnChangecombocredordevedorChangeItems.StringsClientesFornecedoresAlunos   TEditedtcodigocredordevedorLeftTop� WidthGHeightColor	clSkyBlueCtl3DParentCtl3DTabOrderOnExitedtcodigocredordevedorExit	OnKeyDownedtcodigocredordevedorKeyDown
OnKeyPressedtcodigocredordevedorKeyPress  TPanelPanel1Left�Top� Width� HeightBorderStylebsSingleTabOrder  TBitBtnbtnovoLeftTopRWidthsHeight&Caption&NovoFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrder OnClickbtnovoClick  TBitBtn	btalterarLeftTopzWidthsHeight&Caption&AlterarFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrderOnClickbtalterarClick  TBitBtnbtgravarLeftTop� WidthsHeight&Caption&GravarFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrderOnClickbtgravarClick  TBitBtn
btcancelarLeftTop� WidthsHeight&Caption	&CancelarFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrderOnClickbtcancelarClick  TBitBtnbtpesquisarLeftwTopRWidthsHeight&Hint   Pesquisa Títulos com SaldoCaption&Pesquisar CSFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontParentShowHintShowHint	TabOrderOnClickbtpesquisarClick  TBitBtnbtrelatoriosLeftwTop� WidthsHeight&Caption   &RelatóriosFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrderOnClickbtrelatoriosClick  TBitBtn	btexcluirLeftwTop� WidthsHeight&Caption&ExcluirFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrderOnClickbtexcluirClick  TBitBtnbtsairLeftwTop� WidthsHeight&Caption&SairFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrderOnClickbtsairClick  TBitBtnbtpesquisasemsaldoLeftwTopzWidthsHeight&Hint   Pesquisa Títulos Sem SaldoCaptionPes&quisar SSFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontParentShowHintShowHint	TabOrderOnClickbtpesquisasemsaldoClick  TBitBtnbtopcoesLeftTop� WidthsHeight&Caption	   &OpçõesFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontTabOrder	OnClickbtopcoesClick  TBitBtnbtpesquisapendenciaLeftTop*WidthsHeight&Hint   Pesquisa PendênciasCaption   Pendênc&iaFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontParentShowHintShowHint	TabOrder
OnClickbtpesquisapendenciaClick  TBitBtnbtpesquisaboletoLeftwTop*WidthsHeight&HintPesquisa de BoletoCaption&BoletoFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontParentShowHintShowHint	TabOrderOnClickbtpesquisaboletoClick  TBitBtnbtpesquisatituloLeftTopWidthsHeight&Hint   Pesquisa PendênciasCaption   &TítuloFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontParentShowHintShowHint	TabOrderOnClickbtpesquisatituloClick  TBitBtnbtPesquisaDuplicataLeftwTopWidthsHeight&HintPesquisa de BoletoCaption
&DuplicataFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.StylefsBold 
ParentFontParentShowHintShowHint	TabOrderOnClickbtPesquisaDuplicataClick   	TMaskEdit
edtemissaoLeftTop� WidthFHeightCtl3DEditMask!99/99/9999;1;_	MaxLength
ParentCtl3DTabOrderText
  /  /      TEditedtportadorLeftTopWidthGHeightColor	clSkyBlueCtl3DParentCtl3DTabOrderOnExitedtportadorExit	OnKeyDownedtportadorKeyDown  TEditedtvalorLeftTopDWidth?HeightCtl3DParentCtl3DTabOrderOnExitedtvalorExit
OnKeyPressedtvalorKeyPress  TEditedtcontagerencialLeftTopoWidthOHeightColor	clSkyBlueCtl3DParentCtl3DTabOrderOnExitedtcontagerencialExit	OnKeyDownedtcontagerencialKeyDown  TEditedtcodigogeradorLeft0Top�Width9HeightTabOrderVisible  	TComboBoxcombogeradorLeftTop�Width� HeightCtl3D
ItemHeightParentCtl3DTabOrderVisibleOnChangecombogeradorChangeItems.StringsMatricula de AlunosNota Fiscal de EntradaNota Fiscal de SaidaEntradaSaida   	TComboBoxcombogeradorcodigoLeftTop�Width9HeightCtl3D
ItemHeightParentCtl3DTabOrderText VisibleOnChangecombogeradorcodigoChange  	TComboBoxcombocredordevedorcodigoLeft� TopwWidthFHeight	BevelKindbkSoftCtl3D
ItemHeightParentCtl3DTabOrderVisibleOnChangecombocredordevedorcodigoChange  	TComboBoxcomboprazonomeLeftTop� Width� Height	BevelKindbkSoftCtl3D
ItemHeightParentCtl3DTabOrderOnChangecomboprazonomeChange	OnKeyDowncomboprazonomeKeyDown  	TComboBoxcomboprazocodigoLeftUTop� Width9Height	BevelKindbkSoftCtl3D
ItemHeightParentCtl3DTabOrder	VisibleOnChangecomboprazocodigoChange  	TCheckBoxCheckParcelasIguaisLeft� Top� Width� HeightCaptionParcelas IguaisFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameVerdana
Font.Style 
ParentFontTabOrder
  TEdit
edtnumdctoLeftTop�Width� HeightCtl3D	MaxLength2ParentCtl3DTabOrder  TEditedtcodigohistoricosimplesLeftTopNWidth)HeightColor	clSkyBlueCtl3D	MaxLength	ParentCtl3DTabOrderOnExitedtcodigohistoricosimplesExit	OnKeyDown edtcodigohistoricosimplesKeyDown
OnKeyPress!edtcodigohistoricosimplesKeyPress  TEditedtnotafiscalLeftTop�Width� HeightCtl3D	MaxLength2ParentCtl3DTabOrder  TEditedtsubcontagerencialLeftTop�WidthOHeightColor	clSkyBlueCtl3DParentCtl3DTabOrderOnExitedtsubcontagerencialExit	OnKeyDownedtsubcontagerencialKeyDown  TMemomemoobservacaoLeft�TopxWidth� HeightqCtl3DLines.Stringsmemoobservacao ParentCtl3DTabOrder   TTabPage LeftTopCaption&2 - Parcelas TLabelLabel12Left Top\Width�HeightAlignalBottom	AlignmenttaCenterCaption[   Pressione ENTER ou clique duas vezes sobre a parcela para QUITAR, LANÇAR JUROS OU DESCONTOFont.CharsetANSI_CHARSET
Font.ColorclBlackFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TStringGridStrGridPendenciasLeft TopjWidth�Height�AlignalBottomColCount	FixedCols RowCountOptionsgoFixedVertLinegoFixedHorzLine
goVertLine
goHorzLinegoRangeSelectgoRowSizinggoColSizing TabOrder 
OnDblClickStrGridPendenciasDblClick
OnKeyPressStrGridPendenciasKeyPress
RowHeights   TBitBtnBtPesqMensaLeftTop Width� Height)Caption	PesquisarTabOrderVisibleOnClickBtPesqMensaClick  TBitBtnbtopcoesboletoLeftTop+Width� Height)Caption   Opções de BoletoTabOrderOnClickbtopcoesboletoClick  TBitBtnBthistoricoPendenciasLeftTopWidth� Height)Caption   &Histórico de LançamentosTabOrderOnClickBthistoricoPendenciasClick   TTabPage LeftTopCaption*   &3 - Lançamentos em Pendências && Outros �TFrLancamentoFrLancamento1Left Top Width�Height	AlignalClientTabOrder  �TPageControlGuiaWidth�Height	 �	TTabSheet	TabSheet1 �TBitBtnbtchequecaixaOnClickFrLancamento1btchequecaixaClick  �TBitBtn
btpesquisaOnClickFrLancamento1btpesquisaClick  �TBitBtnBtApagaLoteOnClickFrLancamento1BtApagaLoteClick  �TBitBtnbtimprimecomprovantepagamentoOnClick/FrLancamento1btimprimecomprovantepagamentoClick  �TBitBtnbtAlterarHistoricoLancamentoOnClick.FrLancamento1btAlterarHistoricoLancamentoClick   �	TTabSheet	TabSheet2 �TPanelPanel1Top�Width� �TLabelLabel13Width�   �TStringGridSTRGCPWidth�Height&	OnKeyDownFrLancamento1STRGCPKeyDown
OnKeyPressFrLancamento1STRGCPKeyPress  �TPanelPanel2Width� �TButtonBtSelecionaTodas_PGOnClick%FrLancamento1BtSelecionaTodas_PGClick    �	TTabSheet	TabSheet3 �TStringGridstrgcr	OnKeyDownFrLancamento1strgcrKeyDown
OnKeyPressFrLancamento1strgcrKeyPress  �TPanelPanel4 �TBevelBevel1LeftQ  �TLabelLbValorLancamentocrLeft?  �TLabellbrecebeLeft?  �TLabel	LbSaldocrLeft?  �TBitBtnbtlancaboletoOnClickFrLancamento1btlancaboletoClick  �TButtonBtSelecionaTodas_CROnClick%FrLancamento1BtSelecionaTodas_CRClick  �TBitBtnbtpesquisacrOnClickFrLancamento1btpesquisacrClick  �TBitBtnbtexecutacrOnClickFrLancamento1btexecutacrClick  �TButtonButton2OnClickFrLancamento1Button2Click         