unit UCHEQUEDEVOLVIDO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjCHEQUEDEVOLVIDO;

type
  TFCHEQUEDEVOLVIDO = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    LbCODIGO: TLabel;
    LbCheque: TLabel;
    LbNomeCheque: TLabel;
    LbTituloaReceber: TLabel;
    LbNomeTituloaReceber: TLabel;
    LbTituloaPagar: TLabel;
    LbNomeTituloaPagar: TLabel;
    LbTransferencia: TLabel;
    EdtCODIGO: TEdit;
    EdtCheque: TEdit;
    EdtTituloaReceber: TEdit;
    EdtTituloaPagar: TEdit;
    EdtTransferencia: TEdit;
    procedure edtChequeKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtChequeExit(Sender: TObject);
    procedure edtTituloaReceberKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtTituloaReceberExit(Sender: TObject);
    procedure edtTituloaPagarKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtTituloaPagarExit(Sender: TObject);
    procedure edtTransferenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtTransferenciaExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
  private
         ObjCHEQUEDEVOLVIDO:TObjCHEQUEDEVOLVIDO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCHEQUEDEVOLVIDO: TFCHEQUEDEVOLVIDO;


implementation

uses UessencialGlobal, Upesquisa, UObjTitulo, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCHEQUEDEVOLVIDO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCHEQUEDEVOLVIDO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Cheque.Submit_codigo(edtCheque.text);
        TituloaReceber.Submit_codigo(edtTituloaReceber.text);
        TituloaPagar.Submit_codigo(edtTituloaPagar.text);
        Transferencia.Submit_codigo(edtTransferencia.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCHEQUEDEVOLVIDO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCHEQUEDEVOLVIDO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtCheque.text:=Cheque.Get_codigo;
        LbNomeCheque.caption:='Cheque N�mero '+Cheque.Get_NumCheque+' | Valor R$ '+formata_valor(cheque.get_valor);
        EdtTituloaReceber.text:=TituloaReceber.Get_codigo;
        LbNomeTituloaReceber.caption:=TituloaReceber.Get_HISTORICO;
        EdtTituloaPagar.text:=TituloaPagar.Get_codigo;
        LbnomeTituloaPagar.caption:=TituloaPagar.Get_HISTORICO;
        EdtTransferencia.text:=Transferencia.Get_codigo;
        
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCHEQUEDEVOLVIDO.TabelaParaControles: Boolean;
begin
     If (Self.ObjCHEQUEDEVOLVIDO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCHEQUEDEVOLVIDO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCHEQUEDEVOLVIDO=Nil)
     Then exit;

    If (Self.ObjCHEQUEDEVOLVIDO.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjCHEQUEDEVOLVIDO.free;
end;

procedure TFCHEQUEDEVOLVIDO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCHEQUEDEVOLVIDO.BtnovoClick(Sender: TObject);
begin
     MensagemAviso('Entre em Lan�amento de Cheque Devolvido');
     {limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCHEQUEDEVOLVIDO.Get_novocodigo;
     edtcodigo.enabled:=False;


     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjCHEQUEDEVOLVIDO.status:=dsInsert;
     Guia.pageindex:=0;
     EdtCheque.setfocus;}

end;


procedure TFCHEQUEDEVOLVIDO.btalterarClick(Sender: TObject);
begin
    MensagemAviso('N�o � poss�vel alterar uma ocorr�ncia de Cheque Devolvido');
    {If (Self.ObjCHEQUEDEVOLVIDO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCHEQUEDEVOLVIDO.Status:=dsEdit;
                guia.pageindex:=0;
                edtcheque.setfocus;
                esconde_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;
    }


end;

procedure TFCHEQUEDEVOLVIDO.btgravarClick(Sender: TObject);
begin

     If Self.ObjCHEQUEDEVOLVIDO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCHEQUEDEVOLVIDO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCHEQUEDEVOLVIDO.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCHEQUEDEVOLVIDO.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCHEQUEDEVOLVIDO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCHEQUEDEVOLVIDO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCHEQUEDEVOLVIDO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
     End
     Else Begin

     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCHEQUEDEVOLVIDO.btcancelarClick(Sender: TObject);
begin
     Self.ObjCHEQUEDEVOLVIDO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCHEQUEDEVOLVIDO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCHEQUEDEVOLVIDO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCHEQUEDEVOLVIDO.Get_pesquisa,Self.ObjCHEQUEDEVOLVIDO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCHEQUEDEVOLVIDO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCHEQUEDEVOLVIDO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCHEQUEDEVOLVIDO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCHEQUEDEVOLVIDO.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeCheque.caption:='';
     LbNomeTituloaReceber.caption:='';
     LbNomeTituloaPagar.caption:='';
end;

procedure TFCHEQUEDEVOLVIDO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCHEQUEDEVOLVIDO:=TObjCHEQUEDEVOLVIDO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;
procedure TFCHEQUEDEVOLVIDO.edtChequeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCHEQUEDEVOLVIDO.edtChequekeydown(sender,key,shift);
end;
 
procedure TFCHEQUEDEVOLVIDO.edtChequeExit(Sender: TObject);
begin
    ObjCHEQUEDEVOLVIDO.edtChequeExit(sender,lbnomeCheque);
end;
procedure TFCHEQUEDEVOLVIDO.edtTituloaReceberKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCHEQUEDEVOLVIDO.edtTituloaReceberkeydown(sender,key,shift,lbnomeTituloaReceber);
end;
 
procedure TFCHEQUEDEVOLVIDO.edtTituloaReceberExit(Sender: TObject);
begin
    ObjCHEQUEDEVOLVIDO.edtTituloaReceberExit(sender,lbnomeTituloaReceber);
end;
procedure TFCHEQUEDEVOLVIDO.edtTituloaPagarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCHEQUEDEVOLVIDO.edtTituloaPagarkeydown(sender,key,shift,lbnomeTituloaPagar);
end;
 
procedure TFCHEQUEDEVOLVIDO.edtTituloaPagarExit(Sender: TObject);
begin
    ObjCHEQUEDEVOLVIDO.edtTituloaPagarExit(sender,lbnomeTituloaPagar);
end;
procedure TFCHEQUEDEVOLVIDO.edtTransferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCHEQUEDEVOLVIDO.edtTransferenciakeydown(sender,key,shift);
end;
 
procedure TFCHEQUEDEVOLVIDO.edtTransferenciaExit(Sender: TObject);
begin
    ObjCHEQUEDEVOLVIDO.edtTransferenciaExit(sender);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCHEQUEDEVOLVIDO.btrelatoriosClick(Sender: TObject);
begin
     Self.ObjCHEQUEDEVOLVIDO.ImprimeOcorrenciaChequeDevolvido(edtcodigo.Text);
end;

function TFCHEQUEDEVOLVIDO.atualizaQuantidade: string;
begin
      result:='Existem '+ContaRegistros('TABCHEQUEDEVOLVIDO','codigo')+' Ocorr�ncias Cadastradas';
end;

end.

