unit UimpDuplicata02;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, Qrctrls, ExtCtrls;

type
  TFImpDuplicata02 = class(TForm)
    QR: TQuickRep;
    ValorFatura: TQRLabel;
    NumeroFatura: TQRLabel;
    ValorDuplicata: TQRLabel;
    numeroduplicata: TQRLabel;
    vencimento: TQRLabel;
    emissao: TQRLabel;
    nome: TQRLabel;
    endereco: TQRLabel;
    estado: TQRLabel;
    cidade: TQRLabel;
    cnpj: TQRLabel;
    ie: TQRLabel;
    valorextenso1: TQRLabel;
    valorextenso2: TQRLabel;
    observacoes1: TQRLabel;
    observacoes2: TQRLabel;
    observacoes3: TQRLabel;
    observacoes4: TQRLabel;
    pedidos1: TQRLabel;
    pedidos2: TQRLabel;
    pedidos3: TQRLabel;
    pedidos4: TQRLabel;
    pedidos5: TQRLabel;
    pedidos6: TQRLabel;
    pedidos7: TQRLabel;
    pedidos8: TQRLabel;
    BAIRRO: TQRLabel;
    FONE: TQRLabel;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImpDuplicata02: TFImpDuplicata02;

implementation

uses UessencialGlobal;

{$R *.DFM}

procedure TFImpDuplicata02.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
end;

end.
