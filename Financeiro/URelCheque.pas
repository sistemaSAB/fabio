unit URelCheque;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, QuickRpt, Qrctrls, Db, IBCustomDataSet, IBQuery;

type
  TFrelCheque = class(TForm)
    QR: TQuickRep;
    QRBand1: TQRBand;
    LbValor: TQRLabel;
    LbExtenso1: TQRLabel;
    LbExtenso2: TQRLabel;
    LbNominal: TQRLabel;
    LbCidadeCheque: TQRLabel;
    LbPagoA: TQRLabel;
    LbDataAtual: TQRLabel;
    LbDiaCheque: TQRLabel;
    LbMesCheque: TQRLabel;
    LbAnoCheque: TQRLabel;
    LbValorCanhoto: TQRLabel;
    IBQuery1: TIBQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FrelCheque: TFrelCheque;

implementation

{$R *.DFM}

end.
