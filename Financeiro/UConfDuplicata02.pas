unit UConfDuplicata02;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UobjConfRelatorio,StdCtrls, Qrctrls, QuickRpt, Buttons, Mask, ExtCtrls,rdprint;

type
  TFConfDuplicata02 = class(TForm)
    ValorFatura: TLabel;
    NumeroFatura: TLabel;
    ValorDuplicata: TLabel;
    numeroduplicata: TLabel;
    vencimento: TLabel;
    nome: TLabel;
    endereco: TLabel;
    estado: TLabel;
    cidade: TLabel;
    cnpj: TLabel;
    ie: TLabel;
    valorextenso1: TLabel;
    valorextenso2: TLabel;
    emissao: TLabel;
    Panel1: TPanel;
    LBTOP: TLabel;
    LBLEFT: TLabel;
    LBNOMECOMPONENTE: TLabel;
    edttop: TMaskEdit;
    edtleft: TMaskEdit;
    botaogravar: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    FONE: TLabel;
    BAIRRO: TLabel;
    pedidos1: TLabel;
    pedidos2: TLabel;
    pedidos3: TLabel;
    pedidos4: TLabel;
    pedidos5: TLabel;
    pedidos6: TLabel;
    pedidos7: TLabel;
    pedidos8: TLabel;
    observacoes1: TLabel;
    observacoes2: TLabel;
    observacoes3: TLabel;
    observacoes4: TLabel;
    procedure ValorFaturaClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtleftExit(Sender: TObject);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure botaogravarClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConfDuplicata02: TFConfDuplicata02;
  ObjConfiguraRel:TObjConfRelatorio;
const
FATORLINHA=15;
FATORCOLUNA=10;
MAXIMOCOLUNA=80;
MAXIMOLINHA=36;

implementation

uses  UrelBoleto_PI, URDPRINT, UessencialGlobal, UDataModulo,
  UimpDuplicata02;

{$R *.DFM}

procedure TFConfDuplicata02.ValorFaturaClick(Sender: TObject);
begin

     edttop.text:=floattostr(int(Tlabel(sender).top/FATORLINHA));
     edtleft.text:=floattostr(int(Tlabel(sender).left/FATORCOLUNA));


     LbNomeComponente.caption:=Tlabel(sender).name;
     edttop.SetFocus;
end;

procedure TFConfDuplicata02.BitBtn3Click(Sender: TObject);
begin
     sELF.CLOSE;
end;

procedure TFConfDuplicata02.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
keyc:char;
begin
     keyc:=#13;
     if key=vk_left
     Then begin
                edtleft.text:=floattostr(strtoint(edtleft.text)-1);
                self.edtleftKeyPress(Sender,keyc);
     End
     Else
        if key=vk_right
        Then Begin
                edtleft.text:=floattostr(strtoint(edtleft.text)+1);
                self.edtleftKeyPress(Sender,keyc);
        End
        Else
            if key=vk_up
            Then Begin
                edttop.text:=inttostr(strtoint(edttop.text)-1);
                self.edtleftKeyPress(Sender,keyc);
            End
            Else
                if key=vk_down
                Then Begin
                        edttop.text:=inttostr(strtoint(edttop.text)+1);
                        self.edtleftKeyPress(Sender,keyc);
                End;
End;

procedure TFConfDuplicata02.edtleftExit(Sender: TObject);
var
numerico:integer;
begin
     Try
        numerico:=Strtoint(edtleft.text);
        if (numerico<0)
        Then edtleft.text:='0'
        Else Begin
                If (Numerico>MAXIMOCOLUNA)
                Then edtleft.text:=inttostr(MAXIMOCOLUNA);
        End;
        
        numerico:=Strtoint(edttop.text);
        if (numerico<0)
        Then edttop.text:='0'
        Else Begin
                  If (Numerico>MAXIMOLINHA)
                  Then edttop.text:=inttostr(MAXIMOLINHA);
        End;
     Except
           exit;
     End;


     TLabel(Self.FindComponent(LBNOMECOMPONENTE.caption)).left:=Strtoint(Edtleft.text)*FATORCOLUNA;
     TLabel(Self.FindComponent(LBNOMECOMPONENTE.caption)).Top:=Strtoint(EdtTop.text)*FATORLINHA;

end;

procedure TFConfDuplicata02.edtleftKeyPress(Sender: TObject; var Key: Char);
var
numerico:integer;
begin
     if (not (key in ['0'..'9',#8,#13]))
     Then key:=#0;

     if key=#13
     Then edtleftExit(sender);
end;

procedure TFConfDuplicata02.FormShow(Sender: TObject);
VAR
INT_HABILITA:INTEGER;
saida:boolean;
begin
     Uessencialglobal.PegaCorForm(Self);
     If (Tag=1)
     Then exit;
     edtleft.text:='0';
     edttop.text:='0';

     Try
        ObjConfiguraRel:=TObjConfRelatorio.create;
     Except
           Messagedlg('N�o � poss�vel Configurar a Impress�o Devido a um Problema na Cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0);
           exit;
     End;

     //resgatando as configura��es atuais do quickreport
    ObjconfiguraRel.RecuperaArquivoRelatorio(FImpDuplicata02,'IMPRIMEDUPLICATA02');

    for int_habilita:=0 to FImpDuplicata02.ComponentCount -1 do
    Begin
        if FImpDuplicata02.Components [int_habilita].ClassName = 'TQRLabel'
        then Begin
                Tlabel(Self.FindComponent(FImpDuplicata02.Components [int_habilita].Name)).Left:=TQrlabel(FImpDuplicata02.Components [int_habilita]).left;
                Tlabel(Self.FindComponent(FImpDuplicata02.Components [int_habilita].Name)).Top:=TQrlabel(FImpDuplicata02.Components [int_habilita]).Top;
        End
        Else Begin
                  if FImpDuplicata02.Components [int_habilita].ClassName = 'TQRMemo'
                  then Begin
                        Tmemo(Self.FindComponent(FImpDuplicata02.Components [int_habilita].Name)).Left:=TQrmemo(FImpDuplicata02.Components [int_habilita]).left;
                        Tmemo(Self.FindComponent(FImpDuplicata02.Components [int_habilita].Name)).Top:=TQrmemo(FImpDuplicata02.Components [int_habilita]).Top;
                  End
        End;
   End;
   tag:=1;
   ValorFaturaClick(ValorFatura);
end;

procedure TFConfDuplicata02.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (ObjConfiguraRel<>nil)
     Then ObjConfiguraRel.free;
     tag:=0;
end;

procedure TFConfDuplicata02.botaogravarClick(Sender: TObject);
var
int_habilita:Integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1 do
   Begin
        if ((Self.Components [int_habilita].ClassName = 'TLabel')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBLEFT')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBTOP')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBNOMECOMPONENTE'))
        Then Begin
                TQRlabel(FImpDuplicata02.FindComponent(Self.Components [int_habilita].Name)).Left:=Tlabel(Self.Components [int_habilita]).left;
                TQRlabel(FImpDuplicata02.FindComponent(Self.Components [int_habilita].Name)).Top:=TLabel(Self.Components [int_habilita]).Top;
             End;
        If (Self.Components[int_habilita].ClassName='TMemo')
        Then Begin
                TQRmemo(FImpDuplicata02.FindComponent(Self.Components [int_habilita].Name)).Left:=Tlabel(Self.Components [int_habilita]).left;
                TQRmemo(FImpDuplicata02.FindComponent(Self.Components [int_habilita].Name)).Top:=TLabel(Self.Components [int_habilita]).Top;
        End;
   End;

     //GRAVANDO OS DADOS ATUAIS NO QUICKREPORT
     IF (ObjconfiguraRel.NovoArquivoRelatorio(FImpDuplicata02,'IMPRIMEDUPLICATA02')=False)
     Then Begin
               Messagedlg('N�o foi poss�vel gravar o arquivo de configura��o!',mterror,[mbok],0);
               exit;
          End;
     Messagedlg('Arquivo Gravado com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFConfDuplicata02.BitBtn2Click(Sender: TObject);
var
extenso1,extenso2:string;
cont:integer;
begin
     //resgatando as configuracoes
     ObjConfiguraRelGlobal.RecuperaArquivoRelatorio(FImpDuplicata02,'IMPRIMEDUPLICATA02');

     FRDPRINT.RDprint.OpcoesPreview.PreviewZoom:=91;
     FRDPRINT.RDprint.TitulodoRelatorio:='';
     FRDPRINT.RDprint.CaptionSetup:='';
     FRDPRINT.RDprint.TamanhoQteColunas:=80;
     FRDPRINT.RDprint.TamanhoQteLinhas:=33;
     FRDPRINT.RDprint.PortaComunicacao:='LPT1';
     FRDPRINT.RDprint.UsaGerenciadorImpr:=True;
     FRDPRINT.RDprint.TamanhoQteLPP:=Seis;
     FRDPRINT.RDprint.OpcoesPreview.Preview:=true;

     With FRDPRINT.RDprint do
     Begin
           abrir;
           //dataemissao
           imp(strtoint(floattostr(int(FImpDuplicata02.emissao.Top/15))),strtoint(floattostr(int(FImpDuplicata02.emissao.left/10))),datetostr(now));
           //ValorFatura
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorFatura.left/10))),'1.000,00',[comp12]);
           //NumeroFatura,  uso o NUmerodaPendencia|NUmDcto do Titulo, que geralmente sera na venda Pedido/NF, e em outros casos depende do usuario que criou
           imp(strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.left/10))),'NUMERO');
           //ValorDuplicata=ValorFatura, usado o Saldo
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.left/10))),'1.000,00',[comp12]);
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.left/10))),'N� DUP');
           //vencimento
           imp(strtoint(floattostr(int(FImpDuplicata02.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata02.vencimento.left/10))),'01/01/1500');
           //PEDIDOS
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos1.left/10))),'123456789X1234567892123456789312345');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos2.left/10))),'123456789X1234567892123456789312345');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos3.left/10))),'123456789X1234567892123456789312345');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos4.left/10))),'123456789X1234567892123456789312345');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos5.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos5.left/10))),'123456789X1234567892123456789312345');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos6.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos6.left/10))),'123456789X1234567892123456789312345');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos7.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos7.left/10))),'123456789X1234567892123456789312345');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos8.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos8.left/10))),'123456789X1234567892123456789312345');
           //OBSERVACOES
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes1.left/10))),'Linha 1 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes2.left/10))),'Linha 2 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes3.left/10))),'Linha 3 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes4.left/10))),'Linha 4 Observa��es');


           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata02.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata02.nome.left/10))),'nome');
           //Endereco do Sacado
           imp(strtoint(floattostr(int(FImpDuplicata02.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata02.endereco.left/10))),'endereco');
           //Bairro
           imp(strtoint(floattostr(int(FImpDuplicata02.BAIRRO.Top/15))),strtoint(floattostr(int(FImpDuplicata02.BAIRRO.left/10))),'bairro');
           //Cidade
           imp(strtoint(floattostr(int(FImpDuplicata02.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cidade.left/10))),'cidade');
           //Estado
           imp(strtoint(floattostr(int(FImpDuplicata02.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata02.estado.left/10))),'estado');
           //fone
           imp(strtoint(floattostr(int(FImpDuplicata02.FONE.Top/15))),strtoint(floattostr(int(FImpDuplicata02.FONE.left/10))),'FONE');
           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata02.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cnpj.left/10))),'cnpj');
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata02.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ie.left/10))),'ie');
           //valorextenso
           ValorExtenso_DivididoDois(998999.99,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.valorextenso2.left/10))),extenso2);
           NOVAPAGINA;
                      //dataemissao
           imp(strtoint(floattostr(int(FImpDuplicata02.emissao.Top/15))),strtoint(floattostr(int(FImpDuplicata02.emissao.left/10))),datetostr(now));
           //ValorFatura
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorFatura.left/10))),'1.000,00',[comp12]);
           //NumeroFatura,  uso o NUmerodaPendencia|NUmDcto do Titulo, que geralmente sera na venda Pedido/NF, e em outros casos depende do usuario que criou
           imp(strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.left/10))),'NUMERO');
           //ValorDuplicata=ValorFatura, usado o Saldo
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.left/10))),'1.000,00',[comp12]);
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.left/10))),'N� DUP');
           //vencimento
           imp(strtoint(floattostr(int(FImpDuplicata02.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata02.vencimento.left/10))),'01/01/1500');
           //PEDIDOS
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos1.left/10))),'Linha 1 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos2.left/10))),'Linha 2 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos3.left/10))),'Linha 3 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos4.left/10))),'Linha 4 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos5.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos5.left/10))),'Linha 5 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos6.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos6.left/10))),'Linha 6 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos7.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos7.left/10))),'Linha 7 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos8.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos8.left/10))),'Linha 8 Pedidos');
           //OBSERVACOES
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes1.left/10))),'Linha 1 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes2.left/10))),'Linha 2 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes3.left/10))),'Linha 3 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes4.left/10))),'Linha 4 Observa��es');


           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata02.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata02.nome.left/10))),'nome');
           //Endereco do Sacado
           imp(strtoint(floattostr(int(FImpDuplicata02.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata02.endereco.left/10))),'endereco');
           //Bairro
           imp(strtoint(floattostr(int(FImpDuplicata02.BAIRRO.Top/15))),strtoint(floattostr(int(FImpDuplicata02.BAIRRO.left/10))),'bairro');
           //Cidade
           imp(strtoint(floattostr(int(FImpDuplicata02.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cidade.left/10))),'cidade');
           //Estado
           imp(strtoint(floattostr(int(FImpDuplicata02.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata02.estado.left/10))),'estado');
           //fone
           imp(strtoint(floattostr(int(FImpDuplicata02.FONE.Top/15))),strtoint(floattostr(int(FImpDuplicata02.FONE.left/10))),'FONE');
           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata02.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cnpj.left/10))),'cnpj');
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata02.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ie.left/10))),'ie');
           //valorextenso
           ValorExtenso_DivididoDois(998999.99,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.valorextenso2.left/10))),extenso2);
           NOVAPAGINA;
                      //dataemissao
           imp(strtoint(floattostr(int(FImpDuplicata02.emissao.Top/15))),strtoint(floattostr(int(FImpDuplicata02.emissao.left/10))),datetostr(now));
           //ValorFatura
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorFatura.left/10))),'1.000,00',[comp12]);
           //NumeroFatura,  uso o NUmerodaPendencia|NUmDcto do Titulo, que geralmente sera na venda Pedido/NF, e em outros casos depende do usuario que criou
           imp(strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.Top/15))),strtoint(floattostr(int(FImpDuplicata02.NumeroFatura.left/10))),'NUMERO');
           //ValorDuplicata=ValorFatura, usado o Saldo
           impf(strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ValorDuplicata.left/10))),'1.000,00',[comp12]);
           //NumeroDuplicata
           imp(strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.Top/15))),strtoint(floattostr(int(FImpDuplicata02.numeroduplicata.left/10))),'N� DUP');
           //vencimento
           imp(strtoint(floattostr(int(FImpDuplicata02.vencimento.Top/15))),strtoint(floattostr(int(FImpDuplicata02.vencimento.left/10))),'01/01/1500');
           //PEDIDOS
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos1.left/10))),'Linha 1 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos2.left/10))),'Linha 2 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos3.left/10))),'Linha 3 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos4.left/10))),'Linha 4 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos5.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos5.left/10))),'Linha 5 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos6.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos6.left/10))),'Linha 6 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos7.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos7.left/10))),'Linha 7 Pedidos');
           imp(strtoint(floattostr(int(FImpDuplicata02.pedidos8.Top/15))),strtoint(floattostr(int(FImpDuplicata02.pedidos8.left/10))),'Linha 8 Pedidos');
           //OBSERVACOES
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes1.left/10))),'Linha 1 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes2.left/10))),'Linha 2 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes3.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes3.left/10))),'Linha 3 Observa��es');
           imp(strtoint(floattostr(int(FImpDuplicata02.observacoes4.Top/15))),strtoint(floattostr(int(FImpDuplicata02.observacoes4.left/10))),'Linha 4 Observa��es');


           //NomedoSacado
           imp(strtoint(floattostr(int(FImpDuplicata02.nome.Top/15))),strtoint(floattostr(int(FImpDuplicata02.nome.left/10))),'nome');
           //Endereco do Sacado
           imp(strtoint(floattostr(int(FImpDuplicata02.endereco.Top/15))),strtoint(floattostr(int(FImpDuplicata02.endereco.left/10))),'endereco');
           //Bairro
           imp(strtoint(floattostr(int(FImpDuplicata02.BAIRRO.Top/15))),strtoint(floattostr(int(FImpDuplicata02.BAIRRO.left/10))),'bairro');
           //Cidade
           imp(strtoint(floattostr(int(FImpDuplicata02.cidade.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cidade.left/10))),'cidade');
           //Estado
           imp(strtoint(floattostr(int(FImpDuplicata02.estado.Top/15))),strtoint(floattostr(int(FImpDuplicata02.estado.left/10))),'estado');
           //fone
           imp(strtoint(floattostr(int(FImpDuplicata02.FONE.Top/15))),strtoint(floattostr(int(FImpDuplicata02.FONE.left/10))),'FONE');
           //cnpj
           imp(strtoint(floattostr(int(FImpDuplicata02.cnpj.Top/15))),strtoint(floattostr(int(FImpDuplicata02.cnpj.left/10))),'cnpj');
           //IE
           imp(strtoint(floattostr(int(FImpDuplicata02.ie.Top/15))),strtoint(floattostr(int(FImpDuplicata02.ie.left/10))),'ie');
           //valorextenso
           ValorExtenso_DivididoDois(998999.99,extenso1,extenso2);
           //ValorExtenso1
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso1.Top/15))),strtoint(floattostr(int(FImpDuplicata02.valorextenso1.left/10))),extenso1);
           //ValorExtenso2
           imp(strtoint(floattostr(int(FImpDuplicata02.valorextenso2.Top/15))),strtoint(floattostr(int(FImpDuplicata02.valorextenso2.left/10))),extenso2);
           fechar;
     End;

end;

procedure TFConfDuplicata02.FormDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
     Accept:=true;
end;

procedure TFConfDuplicata02.FormDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     Panel1.left:=X;
     Panel1.top:=y;

end;

end.
