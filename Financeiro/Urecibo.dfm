object Frecibo: TFrecibo
  Left = 166
  Top = 161
  Width = 532
  Height = 368
  Caption = 'Recibo - EXCLAIM TECNOLOGIA'
  Color = clCaptionText
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 4
    Top = 8
    Width = 515
    Height = 329
    Brush.Style = bsClear
  end
  object Label1: TLabel
    Left = 254
    Top = 26
    Width = 107
    Height = 32
    Caption = 'RECIBO'
    Color = clSilver
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel
    Left = 309
    Top = 72
    Width = 95
    Height = 22
    Caption = 'VALOR R$'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 12
    Top = 104
    Width = 100
    Height = 13
    Caption = 'Recebemos do Sr.(a)'
  end
  object Label4: TLabel
    Left = 12
    Top = 128
    Width = 62
    Height = 13
    Caption = 'a quantia de '
  end
  object Label5: TLabel
    Left = 12
    Top = 176
    Width = 87
    Height = 13
    Caption = 'Correspondente a '
  end
  object Label6: TLabel
    Left = 272
    Top = 240
    Width = 196
    Height = 13
    Caption = 'Para maior clareza firmo(amos) o presente'
  end
  object Label7: TLabel
    Left = 296
    Top = 264
    Width = 9
    Height = 13
    Caption = ' , '
  end
  object Image1: TImage
    Left = 12
    Top = 16
    Width = 209
    Height = 57
  end
  object labr8: TLabel
    Left = 330
    Top = 266
    Width = 12
    Height = 13
    Caption = 'de'
  end
  object Label9: TLabel
    Left = 458
    Top = 266
    Width = 12
    Height = 13
    Caption = 'de'
  end
  object btrelatorios: TBitBtn
    Left = 114
    Top = 296
    Width = 100
    Height = 38
    Caption = '&Imprimir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 11
    OnClick = btrelatoriosClick
  end
  object BtNovo: TBitBtn
    Left = 10
    Top = 296
    Width = 100
    Height = 38
    Caption = '&Novo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 12
    OnClick = BtNovoClick
  end
  object EDTVALOR: TEdit
    Left = 410
    Top = 74
    Width = 102
    Height = 19
    TabOrder = 0
    OnExit = EDTVALORExit
  end
  object edtrecebemos: TEdit
    Left = 119
    Top = 102
    Width = 393
    Height = 19
    TabOrder = 1
    Text = 'edtrecebemos'
  end
  object edtquantia1: TEdit
    Left = 81
    Top = 124
    Width = 431
    Height = 19
    TabOrder = 2
    Text = 'edtquantia1'
  end
  object edtquantia2: TEdit
    Left = 13
    Top = 147
    Width = 499
    Height = 19
    TabOrder = 3
    Text = 'edtquantia2'
  end
  object edtcorrespondente1: TEdit
    Left = 103
    Top = 173
    Width = 409
    Height = 19
    TabOrder = 4
    Text = 'edtcorrespondente1'
  end
  object edtcidade: TEdit
    Left = 163
    Top = 264
    Width = 121
    Height = 19
    TabOrder = 6
  end
  object edtcorrespondente2: TEdit
    Left = 12
    Top = 197
    Width = 500
    Height = 19
    TabOrder = 5
    Text = 'Edit5'
  end
  object edtdia: TEdit
    Left = 300
    Top = 264
    Width = 25
    Height = 19
    TabOrder = 7
  end
  object edtmes: TEdit
    Left = 350
    Top = 264
    Width = 105
    Height = 19
    TabOrder = 8
  end
  object edtano: TEdit
    Left = 472
    Top = 264
    Width = 40
    Height = 19
    TabOrder = 9
  end
  object edtcodigo: TEdit
    Left = 12
    Top = 78
    Width = 84
    Height = 19
    TabOrder = 10
  end
end
