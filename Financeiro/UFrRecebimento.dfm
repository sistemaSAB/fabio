object FrRecimento: TFrRecimento
  Left = 0
  Top = 0
  Width = 603
  Height = 314
  Color = clBtnFace
  Ctl3D = False
  ParentColor = False
  ParentCtl3D = False
  TabOrder = 0
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 603
    Height = 314
    Align = alClient
    Shape = bsFrame
    Style = bsRaised
  end
  object Bevel8: TBevel
    Left = 436
    Top = 16
    Width = 165
    Height = 46
    Shape = bsFrame
    Style = bsRaised
  end
  object Label19: TLabel
    Left = 2
    Top = 2
    Width = 28
    Height = 14
    Caption = '&Valor'
    FocusControl = edtvalor
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label23: TLabel
    Left = 175
    Top = 2
    Width = 43
    Height = 14
    Caption = '&Esp'#233'cie'
    FocusControl = combotipo
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label21: TLabel
    Left = 86
    Top = 2
    Width = 48
    Height = 14
    Caption = '&Portador'
    FocusControl = edtportador
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbportador: TLabel
    Left = 285
    Top = 19
    Width = 147
    Height = 15
    AutoSize = False
    Caption = 'zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbTotalCheque: TLabel
    Left = 529
    Top = 76
    Width = 73
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbtotalDinheiro: TLabel
    Left = 539
    Top = 64
    Width = 63
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbSaldo: TLabel
    Left = 439
    Top = 19
    Width = 157
    Height = 39
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Color = clInfoBk
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
  end
  object Label18: TLabel
    Left = 435
    Top = 2
    Width = 34
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Saldo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 435
    Top = 64
    Width = 96
    Height = 14
    Caption = 'Total em Dinheiro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 435
    Top = 76
    Width = 93
    Height = 14
    Caption = 'Total em Cheque'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbcredito: TLabel
    Left = 435
    Top = 89
    Width = 91
    Height = 14
    Caption = 'Total em Cr'#233'dito'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbtotalcredito: TLabel
    Left = 529
    Top = 89
    Width = 73
    Height = 14
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Valor'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBGrid: TDBGrid
    Left = 5
    Top = 106
    Width = 596
    Height = 205
    Color = clInfoBk
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object PanelCheque: TPanel
    Left = 8
    Top = 106
    Width = 591
    Height = 205
    Color = clInfoBk
    TabOrder = 5
    Visible = False
    object Label3: TLabel
      Left = 558
      Top = 16
      Width = 13
      Height = 13
      Caption = 'C3'
      Transparent = True
    end
    object Label12: TLabel
      Left = 453
      Top = 16
      Width = 55
      Height = 13
      Caption = 'Cheque N.'#186
      Transparent = True
    end
    object Label11: TLabel
      Left = 392
      Top = 16
      Width = 24
      Height = 13
      Caption = 'S'#233'rie'
      Transparent = True
    end
    object Label10: TLabel
      Left = 344
      Top = 16
      Width = 13
      Height = 13
      Caption = 'C2'
      Transparent = True
    end
    object Label9: TLabel
      Left = 219
      Top = 16
      Width = 28
      Height = 13
      Caption = 'Conta'
      Transparent = True
    end
    object Label8: TLabel
      Left = 181
      Top = 16
      Width = 13
      Height = 13
      Caption = 'C1'
      Transparent = True
    end
    object Label7: TLabel
      Left = 143
      Top = 16
      Width = 15
      Height = 13
      Caption = 'DV'
      Transparent = True
    end
    object Label6: TLabel
      Left = 95
      Top = 16
      Width = 39
      Height = 13
      Caption = 'Ag'#234'ncia'
      Transparent = True
    end
    object Label5: TLabel
      Left = 43
      Top = 16
      Width = 31
      Height = 13
      Caption = 'Banco'
      Transparent = True
    end
    object Label4: TLabel
      Left = 5
      Top = 16
      Width = 27
      Height = 13
      Caption = 'Comp'
      Transparent = True
    end
    object Label14: TLabel
      Left = 210
      Top = 103
      Width = 83
      Height = 13
      Caption = 'Portador e Cpf 01'
      Transparent = True
    end
    object Label15: TLabel
      Left = 211
      Top = 126
      Width = 83
      Height = 13
      Caption = 'Portador e Cpf 02'
      Transparent = True
    end
    object Label16: TLabel
      Left = 6
      Top = 167
      Width = 136
      Height = 13
      Caption = 'C'#243'digo de Barras do Cheque'
      Transparent = True
    end
    object Label13: TLabel
      Left = 469
      Top = 161
      Width = 22
      Height = 13
      Caption = 'Para'
      Transparent = True
    end
    object edtcodigo: TEdit
      Left = 458
      Top = 80
      Width = 121
      Height = 19
      TabOrder = 16
      Text = 'edtcodigo'
      Visible = False
    end
    object edtnumcheque: TEdit
      Left = 451
      Top = 32
      Width = 78
      Height = 19
      MaxLength = 25
      TabOrder = 8
    end
    object edtc3: TEdit
      Left = 560
      Top = 32
      Width = 19
      Height = 19
      MaxLength = 5
      TabOrder = 9
      OnExit = edtc3Exit
    end
    object edtserie: TEdit
      Left = 393
      Top = 32
      Width = 28
      Height = 19
      MaxLength = 5
      TabOrder = 7
    end
    object edtc2: TEdit
      Left = 346
      Top = 32
      Width = 21
      Height = 19
      MaxLength = 5
      TabOrder = 6
    end
    object edtconta: TEdit
      Left = 222
      Top = 32
      Width = 105
      Height = 19
      MaxLength = 25
      TabOrder = 5
    end
    object edtc1: TEdit
      Left = 177
      Top = 32
      Width = 25
      Height = 19
      MaxLength = 5
      TabOrder = 4
    end
    object edtdv: TEdit
      Left = 144
      Top = 32
      Width = 22
      Height = 19
      TabOrder = 3
    end
    object edtagencia: TEdit
      Left = 97
      Top = 32
      Width = 38
      Height = 19
      MaxLength = 10
      TabOrder = 2
    end
    object edtbanco: TEdit
      Left = 44
      Top = 32
      Width = 44
      Height = 19
      MaxLength = 5
      TabOrder = 1
    end
    object edtcomp: TEdit
      Left = 5
      Top = 32
      Width = 30
      Height = 19
      MaxLength = 5
      TabOrder = 0
    end
    object edtCliente1: TEdit
      Left = 302
      Top = 102
      Width = 155
      Height = 19
      MaxLength = 50
      TabOrder = 10
    end
    object edtcliente2: TEdit
      Left = 302
      Top = 124
      Width = 155
      Height = 19
      MaxLength = 50
      TabOrder = 12
    end
    object edtcpfcliente2: TEdit
      Left = 458
      Top = 124
      Width = 121
      Height = 19
      MaxLength = 20
      TabOrder = 13
    end
    object edtcpfcliente1: TEdit
      Left = 458
      Top = 102
      Width = 121
      Height = 19
      MaxLength = 20
      TabOrder = 11
    end
    object edtcodigodebarras: TEdit
      Left = 8
      Top = 182
      Width = 580
      Height = 19
      MaxLength = 50
      TabOrder = 15
      OnExit = edtcodigodebarrasExit
    end
    object edtvencimento: TMaskEdit
      Left = 497
      Top = 157
      Width = 80
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 14
      Text = '  /  /    '
    end
  end
  object edtvalor: TEdit
    Left = 4
    Top = 16
    Width = 73
    Height = 19
    MaxLength = 9
    TabOrder = 0
    OnKeyPress = edtvalorKeyPress
  end
  object btinserir: TBitBtn
    Left = 4
    Top = 38
    Width = 133
    Height = 23
    Caption = '&Gravar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = btinserirClick
  end
  object btexcluir: TBitBtn
    Left = 4
    Top = 63
    Width = 133
    Height = 22
    Caption = 'E&xcluir'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = btexcluirClick
  end
  object combotipo: TComboBox
    Left = 174
    Top = 16
    Width = 106
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Text = 'D - Dinheiro'
    OnChange = combotipoChange
    OnExit = combotipoExit
  end
  object edtportador: TEdit
    Left = 87
    Top = 16
    Width = 72
    Height = 19
    Color = 6073854
    MaxLength = 9
    TabOrder = 1
    OnDblClick = edtportadorDblClick
    OnExit = edtportadorExit
    OnKeyDown = edtportadorKeyDown
  end
  object BtAlternaCheque_Grid: TBitBtn
    Left = 4
    Top = 87
    Width = 133
    Height = 18
    Caption = '-->>'
    TabOrder = 6
    OnClick = BtAlternaCheque_GridClick
  end
  object PanelValorCredito: TPanel
    Left = 141
    Top = 86
    Width = 293
    Height = 18
    Alignment = taLeftJustify
    TabOrder = 8
  end
end
