unit UobjExportaContabilidade;
Interface
Uses ibquery,grids,ibdatabase,windows,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,Uobjplanodecontas;

Type
   TObjExportaContabilidade=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ObjPlanodeContas:TObjPlanodeContas;
                ObjQuery:tibquery;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean;PpartidaSimples:boolean):Boolean;overload;
                Function    Salvar(ComCommit:Boolean):Boolean;overload;


                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaGerador(Pobjgerador:string;Pcodgerador:string) :boolean;
                Function    excluiporgerador(Pobjgerador:string;Pcodgerador:string) :boolean;

                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_PesquisaNaoExportados       :TStringList;
                Function    Get_TituloPesquisaNaoExportados:string;
                Function    Get_PesquisaExportados          :TStringList;
                Function    Get_TituloPesquisaExportados    :string;
             
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO        :string;
                Function Get_Data          :string;
                Function Get_ContaDebite   :string;
                Function Get_NomeDebite    :string;
                Function Get_ContaCredite  :string;
                Function Get_NomeCredite   :string;
                Function Get_Valor         :string;
                Function Get_Historico     :string;
                Function Get_ObjGerador    :string;
                Function Get_CodGerador    :string;
                Function Get_Exportado     :string;
                Function Get_Exporta       :string;
                Function Get_NOmedoArquivo:string;
                Function Get_NumDocto:string;

                Procedure Submit_CODIGO        (parametro:string );
                Procedure Submit_Data          (parametro:string );
                Procedure Submit_ContaDebite   (parametro:string );
                Procedure Submit_ContaCredite  (parametro:string );
                Procedure Submit_Valor         (parametro:string );
                Procedure Submit_Historico     (parametro:string);
                Procedure Submit_ObjGerador    (parametro:string);
                Procedure Submit_CodGerador    (parametro:string );
                Procedure Submit_Exportado     (parametro:string );
                Procedure Submit_Exporta       (parametro:string );
                Procedure Submit_NomedoArquivo(Parametro:string);
                Procedure Submit_NumDocto(Parametro:string);

                Function  Get_NovoCodigo:string;
                Procedure Exportadados(PCaminho:string);
                Procedure Imprime;
                Procedure RetornaExportacao(PNomeArq:string);
                function Get_Pesquisa: TStringList;
                function LancaInverso(Pobjeto:string;Pcodigo:string;PdataAtual:string):boolean;

                Procedure Opcoesfechamento;
                Procedure ConfrontoGerador(PDatabaseBackup:Tibdatabase;PGrid:TStringGrid);
                procedure ConfrontoTABELA(PDatabaseBackup: Tibdatabase;PGrid: TStringGrid);
                function RetornacamposExportacao: string;

         Private
               ObjDataset:Tibdataset;

               CODIGO        :string;
               Data          :string;
               ContaDebite   :string;
               NomeDebite    :string;
               ContaCredite  :string;
               NomeCredite   :string;
               Valor         :string;
               Historico     :string;
               ObjGerador    :string;
               CodGerador    :string;
               Exportado     :string;
               Exporta       :string;
               nomedoarquivo :string;
               NumDocto      :string;
               

               ParametroPesquisa:TStringList;


                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos(PpartidaSimples:Boolean):boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ExportaContabilidade(Pcaminho:string;Todos:string;Tipo:string);
                Procedure ImprimeRegistrosporData(tipo:string;Somentebloqueados:Boolean);
                Procedure ImprimeExportados;
                Procedure ImportaRegistros;
                Procedure ImprimeBalanco2;
                Procedure ImprimeDados;
                Procedure ImprimeRazao;
                procedure edtcontaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                Procedure FechamentoContabilidade;
                procedure RetornaFechamentoContabilidade;
                Function  ContaPontosClassificacao(Pclassificacao:String):Integer;





   End;


implementation
uses rdprint,ureltxtrdprint,stdctrls,SysUtils,Dialogs,UDatamodulo,Controls,
Uopcaorel,ufiltraimp,UobjParametros, UMenuRelatorios,
  Upesquisa, Forms, UMostraBarraProgresso;


{ TTabTitulo }


Function  TObjExportaContabilidade.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring          ;
        Self.Data             :=FieldByName('Data').asstring            ;
        Self.ContaDebite      :=FieldByName('ContaDebite').asstring     ;
        Self.NomeDebite       :=FieldByName('NomeDebite').asstring      ;
        Self.ContaCredite     :=FieldByName('ContaCredite').asstring    ;
        Self.NomeCredite      :=FieldByName('NomeCredite').asstring     ;
        Self.Valor            :=FieldByName('Valor').asstring           ;
        Self.Historico        :=FieldByName('Historico').asstring       ;
        Self.ObjGerador       :=FieldByName('ObjGerador').asstring      ;
        Self.CodGerador       :=FieldByName('CodGerador').asstring      ;
        Self.Exportado        :=FieldByName('Exportado').asstring       ;
        Self.Exporta          :=FieldByName('Exporta').asstring         ;
        Self.nomedoarquivo    :=FieldByName('NomeDoArquivo').asstring   ;
        Self.numdocto         :=Fieldbyname('numdocto').asstring        ;
        result:=True;
     End;
end;


Procedure TObjExportaContabilidade.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring       :=Self.CODIGO      ;
      FieldByName('Data').asstring         :=Self.Data        ;
      FieldByName('ContaDebite').asstring  :=Self.ContaDebite ;
      FieldByName('NomeDebite').asstring   :=Self.NomeDebite  ;
      FieldByName('ContaCredite').asstring :=Self.ContaCredite;
      FieldByName('NomeCredite').asstring  :=Self.NomeCredite ;
      FieldByName('Valor').asstring        :=Self.Valor       ;
      FieldByName('Historico').asstring    :=Self.Historico   ;
      FieldByName('ObjGerador').asstring   :=Self.ObjGerador  ;
      FieldByName('CodGerador').asstring   :=Self.CodGerador  ;
      FieldByName('Exportado').asstring    :=Self.Exportado   ;
      FieldByName('Exporta').asstring      :=Self.Exporta     ;
      FieldByName('nomedoarquivo').asstring:=Self.nomedoarquivo;
      Fieldbyname('numdocto').asstring     :=Self.numdocto    ;
  End;
End;

//***********************************************************************
function TObjExportaContabilidade.Salvar(ComCommit:Boolean): Boolean;//Ok
Begin
     result:=Self.Salvar(ComCommit,False);
end;

function TObjExportaContabilidade.Salvar(ComCommit:Boolean;PpartidaSimples:Boolean): Boolean;//Ok
begin
    result:=False;
    
    if (Self.VerificaBrancos=True)
    Then Exit;
    
    if (Self.VerificaNumericos=False)
    Then Exit;
    
    if (Self.VerificaData=False)
    Then Exit;
    
    if (Self.VerificaFaixa=False)
    Then Exit;
    
    if (Self.VerificaRelacionamentos(PpartidaSimples)=False)
    Then Exit;

    if (self.codigo='0') and (Self.Status=dsinsert)
    then Begin
              Self.codigo:=Self.Get_NovoCodigo;
    End;


    
    If Self.LocalizaCodigo(Self.CODIGO)=False
    Then Begin
              if(Self.Status=dsedit)
              Then Begin
                        Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                        result:=False;
                        exit;
              End;
    End
    Else Begin
              if(Self.Status=dsinsert)
              Then Begin
                        Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                        result:=False;
                        exit;
              End;
    End;
    
    
    if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                 Self.ObjDataset.Insert;
                 MensagemAviso('Inser��o na Contabilidade a for�a devido a status inv�lido, comunique o suporte do sistema');
       End;

    Self.ObjetoParaTabela;
    Self.ObjDataset.Post;

    If ComCommit=True
    Then FDataModulo.IBTransaction.CommitRetaining;
    
    Self.status          :=dsInactive;
    result:=True;

end;

procedure TObjExportaContabilidade.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO           :='';
        Self.Data             :='';
        Self.ContaDebite      :='';
        Self.NomeDebite       :='';
        Self.ContaCredite     :='';
        Self.NomeCredite      :='';
        Self.Valor            :='';
        Self.Historico        :='';
        Self.ObjGerador       :='';
        Self.CodGerador       :='';
        Self.Exportado        :='';
        Self.Exporta          :='';
        Self.NomedoArquivo    :='';
        Self.numdocto         :='';
     End;
end;

Function TObjExportaContabilidade.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Self.Data='')
       Then Mensagem:=Mensagem+'/Data';

       If (Self.Valor='')
       Then Mensagem:=Mensagem+'/Valor';

       If (Self.ObjGerador='')
       Then Mensagem:=Mensagem+'/Objeto que Gerou';

       If (Self.CodGerador='')
       Then Mensagem:=Mensagem+'/C�digo do Objeto que Gerou';

       If(Self.Exportado='')
       Then Mensagem:=Mensagem+'/Exportado?';

       If (Self.Exporta='')
       Then Mensagem:=Mensagem+'/Exporta?';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjExportaContabilidade.VerificaRelacionamentos(PpartidaSimples:Boolean): Boolean;
var
mensagem:string;
TravaConta,TravaContaIguais:Boolean;
Begin
     result:=False;

     mensagem:='';

     TravaConta:=False;
     if (ObjParametroGlobal.ValidaParametro('TRAVA CONTA T�TULO E CONTA INEXISTENTE NA CONTABILIDADE?')=False)
     Then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then TravaConta:=true;

     TravaContaIguais:=False;
     if (ObjParametroGlobal.ValidaParametro('TRAVA LAN�AMENTO EM CONTAS IGUAIS NA CONTABILIDADE?')=False)
     Then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then TravaContaIguais:=true;



     if (Self.ContaDebite='0')
     Then Self.ContaDebite:='';

     if (Self.ContaCredite='0')
     Then Self.ContaCredite:='';


     if (Self.ContaDebite<>'')
     Then Begin
               if (Self.ObjPlanodeContas.LocalizaCodigo(Self.ContaDebite)=False)
               Then Begin
                        Self.ContaDebite:='';

                        if (TravaConta=True)
                        Then Mensagem:='Conta D�bito n�o existe no Plano de Contas'
               End
               Else Begin
                         Self.ObjPlanodeContas.TabelaparaObjeto;
                         Self.NomeDebite:=Self.ObjPlanodeContas.Get_Nome;

                         if (Self.ObjPlanodeContas.Get_Tipo='T')//conta titulos
                         Then Begin
                                    Self.ContaDebite:='';

                                    if (TravaConta=True)
                                    Then Mensagem:=Mensagem+'\A Conta D�bito � uma Conta T�tulo';
                         End;
               End;
      End;

     if (Self.ContaCredite<>'')
     Then Begin
               if (Self.ObjPlanodeContas.LocalizaCodigo(Self.ContaCredite)=False)
               Then Begin
                          Self.Contacredite:='';

                          if (travaConta=True)
                          Then Mensagem:=mensagem+'\Conta Cr�dito n�o existe no Plano de Contas';
               End
               Else Begin
                         Self.ObjPlanodeContas.TabelaparaObjeto;
                         Self.NomeCredite:=Self.ObjPlanodeContas.Get_Nome;

                         if (Self.ObjPlanodeContas.Get_Tipo='T')//conta titulos
                         Then Begin
                                    Self.ContaCredite:='';
                                    
                                    if (TravaConta=True)
                                    Then Mensagem:=Mensagem+'\A Conta Cr�dito � uma Conta T�tulo';
                         end;
               End;
     End;

     if ((Self.ContaCredite=Self.ContaDebite) and (TravaContaIguais=true))
     Then Mensagem:=Mensagem+mensagem+' \A Conta D�bito n�o por ser igual a conta cr�dito';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
     End
     Else Begin
               Self.Exporta:='S';
               
               if (PpartidaSimples=False)
               Then Begin//nao pode ter nenhuma em branco
                         if ((Self.ContaCredite='') or (self.ContaDebite=''))
                         Then Begin
                                  Self.Exporta:='N';
                                  if (TravaConta=True)
                                  Then Begin
                                            result:=False;
                                            Messagedlg('Existe uma Conta em Branco na Exporta��o da Contabilidade!',mterror,[mbok],0);
                                            exit;
                                  End;
                         End;
                         
               end
               Else Begin//nao pode ser as duas em branco
                         if ((Self.ContaCredite='') and (self.ContaDebite=''))
                         Then Begin
                                  Self.Exporta:='N';
                                  if (TravaConta=True)
                                  Then Begin
                                            result:=False;
                                            Messagedlg('Existe uma Conta em Branco na Exporta��o da Contabilidade!',mterror,[mbok],0);
                                            exit;
                                  End;
                         End;
               end;
     end;

     result:=true;
End;

function TObjExportaContabilidade.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Reais:=Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjExportaContabilidade.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     {Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjExportaContabilidade.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If (Self.Exportado<>'S') and (Self.Exportado<>'N')
     Then Mensagem:=Mensagem+'/O Campo Exportado tem que ser "S" ou "N"';

     If (Self.Exporta<>'S') and (Self.Exporta<>'N')
     Then Mensagem:=Mensagem+'/O Campo Exporta tem que ser "S" ou "N"';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;
end;

function TObjExportaContabilidade.excluiporgerador(Pobjgerador: string;
  Pcodgerador: string): boolean;
begin
     result:=False;
     With Self.ObjDataset do
     Begin
         close;
         SelectSql.Clear;
         SelectSql.add('Delete from tabexportacontabilidade where ObjGerador='+#39+PObjgerador+#39+' and codgerador='+#39+PcodGerador+#39);
         Try
            execsql;
            result:=true;
         except
               on e:exception  do
               Begin
                    Messagedlg('Erro na tentativa de Excluir os lan�amentos cont�beis'+#13+E.Message,mterror,[mbok],0);
                    exit;
               end;
         End;
     End;
end;


function TObjExportaContabilidade.LocalizaGerador(Pobjgerador: string;
  Pcodgerador: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Data,ContaDebite,NomeDebite,ContaCredite,NomeCredite,Valor,');
           SelectSql.add('Historico,numdocto,ObjGerador,CodGerador,Exportado,Exporta,nomedoarquivo ');
           SelectSQL.add('from tabexportacontabilidade where ObjGerador='+#39+PObjgerador+#39+' and codgerador='+#39+PcodGerador+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjExportaContabilidade.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Data,ContaDebite,NomeDebite,ContaCredite,NomeCredite,Valor,');
           SelectSql.add('Historico,numdocto,ObjGerador,CodGerador,Exportado,Exporta,nomedoarquivo ');
           SelectSQL.add('from tabexportacontabilidade where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjExportaContabilidade.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjExportaContabilidade.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
        End
        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjExportaContabilidade.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ObjQuery:=tibquery.create(nil);
        Self.ObjQuery.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;
        Self.ObjPlanodeContas:=TObjPlanodeContas.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Data,ContaDebite,NomeDebite,ContaCredite,NomeCredite,Valor,');
                SelectSql.add('Historico,ObjGerador,CodGerador,Exportado,Exporta,numdocto,nomedoarquivo ');
                SelectSQL.add('from tabexportacontabilidade where codigo=-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert into TabExportaContabilidade ');
                InsertSQL.add('(CODIGO,Data,ContaDebite,NomeDebite,ContaCredite,NomeCredite,Valor,');
                InsertSQL.add('Historico,ObjGerador,CodGerador,Exportado,Exporta,numdocto,nomedoarquivo) ');
                InsertSQL.add('values');
                InsertSQL.add('(:CODIGO,:Data,:ContaDebite,:NomeDebite,:ContaCredite,:NomeCredite,:Valor,');
                InsertSQL.add(':Historico,:ObjGerador,:CodGerador,:Exportado,:Exporta,:numdocto,:nomedoarquivo) ');

                ModifySQL.clear;
                ModifySQL.add('Update TabExportaContabilidade set  ');
                ModifySQL.add('CODIGO=:CODIGO,Data=:Data,ContaDebite=:ContaDebite,NomeDebite=:NomeDebite,');
                ModifySQL.add('ContaCredite=:ContaCredite,NomeCredite=:NomeCredite,Valor=:Valor,');
                ModifySQL.add('Historico=:Historico,ObjGerador=:ObjGerador,');
                ModifySQL.add('CodGerador=:CodGerador,Exportado=:Exportado,Exporta=:Exporta,nomedoarquivo=:nomedoarquivo,numdocto=:numdocto where codigo=:codigo ');

                DeleteSQL.clear;
                DeleteSQL.add(' Delete from TabExportaCOntabilidade where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Data,ContaDebite,NomeDebite,ContaCredite,NomeCredite,Valor,');
                RefreshSQL.add('Historico,numdocto,ObjGerador,CodGerador,Exportado,Exporta,nomedoarquivo ');
                RefreshSQL.add('from tabexportacontabilidade where codigo=-1');

                open;

                Self.ObjDataset.First ;
                
                Self.status:=dsInactive;
        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjExportaContabilidade.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjExportaContabilidade.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjExportaContabilidade.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;





{
function TObjExportaContabilidade.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjExportaContabilidade.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjExportaContabilidade.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_EXPORTACONTABILIDADE';
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o ExportaContabilidade',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjExportaContabilidade.Free;
begin
   Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);
   Self.ObjPlanodeContas.free;
   FreeAndNil(Self.objquery);

end;

function TObjExportaContabilidade.Get_CodGerador: string;
begin
     Result:=Self.CodGerador;
end;


function TObjExportaContabilidade.Get_ContaCredite: string;
begin
     Result:=Self.ContaCredite;
end;

function TObjExportaContabilidade.Get_ContaDebite: string;
begin
     Result:=Self.ContaDebite;
end;

function TObjExportaContabilidade.Get_Data: string;
begin
     Result:=Self.Data;
end;

function TObjExportaContabilidade.Get_Exporta: string;
begin
     Result:=Self.Exporta;
end;

function TObjExportaContabilidade.Get_Exportado: string;
begin
     Result:=Self.Exportado;
end;

function TObjExportaContabilidade.Get_Historico: string;
begin
     Result:=Self.Historico;
end;

function TObjExportaContabilidade.Get_NomeCredite: string;
begin
     Result:=Self.NomeCredite;
end;

function TObjExportaContabilidade.Get_NomeDebite: string;
begin
     Result:=Self.NomeDebite;
end;

function TObjExportaContabilidade.Get_ObjGerador: string;
begin
     Result:=Self.ObjGerador;
end;

function TObjExportaContabilidade.Get_Valor: string;
begin
     Result:=Self.Valor;
end;

procedure TObjExportaContabilidade.Submit_CodGerador(parametro: string);
begin
     Self.CodGerador:=Parametro;
end;


procedure TObjExportaContabilidade.Submit_ContaCredite(parametro: string);
begin
     Self.ContaCredite:=Parametro;
end;

procedure TObjExportaContabilidade.Submit_ContaDebite(parametro: string);
begin
     Self.ContaDebite:=Parametro;
end;

procedure TObjExportaContabilidade.Submit_Data(parametro: string);
begin
     Self.Data:=Parametro;
end;

procedure TObjExportaContabilidade.Submit_Exporta(parametro: string);
begin
     Self.Exporta:=Parametro;
end;

procedure TObjExportaContabilidade.Submit_Exportado(parametro: string);
begin
     Self.Exportado:=Parametro;
end;

procedure TObjExportaContabilidade.Submit_Historico(parametro: string);
begin
//     showmessage(parametro);
  //   showmessage(inttostr(length(parametro)));
     Self.Historico:=Parametro;
end;

procedure TObjExportaContabilidade.Submit_ObjGerador(parametro: string);
begin
     Self.ObjGerador:=Parametro;
end;

procedure TObjExportaContabilidade.Submit_Valor(parametro: string);
begin
     Self.Valor:=Parametro;
end;

procedure TObjExportaContabilidade.Exportadados(PCaminho: string);
begin
     With FopcaoRel do
     Begin

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Exportar dados que n�o foram exportados (por Data do t�tulo)');//0
                items.add('Exportar Todos os Dados (por data do t�tulo)');//1
                {items.add('Exportar dados que n�o foram exportados (por Data do movimento no sistema)');//2
                items.add('Exportar Todos os Dados (por data do movimento no sistema)');//3}
          End;

          showmodal;
          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:Begin
                If (Messagedlg('Tem certeza?',mtconfirmation,[mbyes,mbno],0)=mrno)
                Then exit;
                
                Self.ExportaContabilidade(Pcaminho,'N','T');
          End;
          1:Begin
                If (Messagedlg('Tem certeza?',mtconfirmation,[mbyes,mbno],0)=mrno)
                Then exit;
                Self.ExportaContabilidade(Pcaminho,'S','T');
          End;
          {2:Self.ExportaContabilidade(Pcaminho,'N','M');
          3:Self.ExportaContabilidade(Pcaminho,'S','M');}
          End;
     End;

end;

procedure TObjExportaContabilidade.ExportaContabilidade(Pcaminho: string;Todos: string;Tipo:string);
var
Data1,data2:Tdate;
Arq:TextFile;
linha,temp,saida1,saida2:string;
Qlocal:Tibquery;
Nome:String;
PcodEmpresaContabil,PNumLayout,PCodhistoricoPadrao:string;
cont:integer;
begin
     Nome:=UPPERCASE(ExtractFileName(PCaminho));

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Data Inicial';

          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.caption:='Data Final';

          showmodal;
          If tag=0
          Then exit;

          Try
                Data1:=Strtodate(edtgrupo01.text);
          Except
                Messagedlg('Data Inicial Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          Try
                Data2:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Data Final Inv�lida!',mterror,[mbok],0);
                exit;
          End;

     End;
     With Self.ObjDataset do
     Begin
          //eSCOLHENDO OS DADOS
          close;
          SelectSQL.clear;
          SelectSQL.add('Select TEC.*,TPD.Classificacao as ClassificacaoDebito,TPC.classificacao as Classificacaocredito');
          SelectSQL.add('from TabExportaContabilidade TEC ');
          SelectSQL.add('left Join TabPlanodeContas TPD on TEC.contadebite=TPD.codigo');
          SelectSQL.add('left Join TabPlanodeContas TPC on TEC.contacredite=TPC.codigo');
          SelectSQL.add('where Exporta=''S'' and');

          IF (Tipo='T')
          Then SelectSQL.add('(TEC.Data>='+#39+FormatDateTime('mm/dd/yyyy',data1)+#39+' and TEC.Data<='+#39+FormatDateTime('mm/dd/yyyy',data2)+#39+')')
          Else SelectSQL.add('(cast (TEC.DataC as date)>='+#39+FormatDateTime('mm/dd/yyyy',data1)+#39+' and cast (TEC.DataC as date)<='+#39+FormatDateTime('mm/dd/yyyy',data2)+#39+')');

          If (Todos='N')
          Then SelectSQL.add('and TEC.Exportado=''N'' ');
          SelectSQL.ADD('ORDER BY TEC.CODIGO');
          open;

          //aqui eu percorro os registros
          //e ou gravando em TXT
          //depois passo todos eles para exportado='S'
          Try
             Qlocal:=Tibquery.create(nil);
             Qlocal.database:=Self.Objdataset.database;
          Except
                Messagedlg('Erro na Cria��o da Query Tempor�ria!',mterror,[mbok],0);
                exit;
          End;

          If (ObjParametroGlobal.ValidaParametro('NUMERO DO LAYOUT DE EXPORTA��O DA CONTABILIDADE')=False)
          then Begin
                    Messagedlg('O Par�metro "NUMERO DO LAYOUT DE EXPORTA��O DA CONTABILIDADE" n�o foi encontrado, crie-o!',mterror,[mbok],0);
                    exit;
          End;
          
          PNumLayout:=ObjParametroGlobal.Get_Valor;

          If (ObjParametroGlobal.ValidaParametro('HISTORICO PADR�O EXPORTA��O CONT�BIL')=False)
          Then Begin
                   Messagedlg('"HISTORICO PADR�O EXPORTA��O CONT�BIL" n�o encontrado na tabela de parametros!',mterror,[mbok],0);
                   exit;
          End
          Else Begin
                   
                   PcodhistoricoPadrao:=ObjParametroGlobal.get_valor;
          End;

          If (ObjParametroGlobal.ValidaParametro('C�DIGO DA EMPRESA PARA EXPORTA��O DA CONTABILIDADE')=False)
          Then Begin
                   Messagedlg('"C�DIGO DA EMPRESA PARA EXPORTA��O DA CONTABILIDADE" n�o encontrado na tabela de parametros!',mterror,[mbok],0);
                   exit;
          End
          Else Begin
                   
                   PcodEmpresaContabil:=ObjParametroGlobal.get_valor;
          End;

          Try
             Assignfile(arq,Pcaminho);
             rewrite(arq);
          Except
                Messagedlg('Erro na tentativa de Cria��o do Arquivo de Exporta��o : '+#13+PCaminho,mterror,[mbok],0);
                freeandnil(qlocal);
                exit;
          End;
          Try
             While not(eof) do
             Begin
                  linha:='';
                  if (PNumLayout='1')
                  Then Begin
                          //aqui eu gravo no txt
                          {
                           dATA ,cONTA DEBITE,CNTACREDITE,valor,CODIGOHISTORICO,"COMPLEMENTO"
                           180803,3665,106,2.25,424,"TEste de historico padrao"
                           }
                          linha:=formatdatetime('ddmmyy',fieldbyname('data').asdatetime);
                          linha:=linha+','+fieldbyname('contadebite').AsString;
                          linha:=linha+','+fieldbyname('contacredite').AsString;
                          linha:=linha+','+virgulaparaponto(fieldbyname('valor').AsString);
                          linha:=linha+','+PcodhistoricoPadrao;
                          linha:=linha+','+'"'+fieldbyname('historico').AsString+'"';
                  End
                  Else Begin
                            if (PNumLayout='2')
                            Then Begin
                                      //modelo 2 � do Gouveia e Machado  (MHM SISTEMAS)
                                      {Campo	Descri��o do campo	   Tamanho	Observa��es
                                       01	    Data do lan�amento	   8	      Formato:DD/MM/AA(epoch 1930)  Exemplo: 25/02/03
                                       02	    V�rgula separadora	   1
                                       03	    Conta a ser debitada	 6	      Num�rico inteiro
                                       04	    V�rgula separadora	   1
                                       05	    Conta a ser creditada	 6	      Num�rico inteiro
                                       06	    V�rgula separadora	   1
                                       07	    Valor do lan�amento	   15,2	    Num�rico com duas decimais Exemplo: 1430.60
                                       08	    V�rgula separadora	   1
                                       09	    Hist�rico padr�o	     5	      Num�rico inteiro
                                       10	    V�rgula separadora	   1
                                       11	    Hist�rico complementar 250	    Caracteres entre aspas "Exemplo: "NF 4453 Empresa demonstracao ltda"
                                       12	    V�rgula separadora	   1
                                       13	    Letra de identifica��o 1	      Caractere entre aspas " que servir� para identificar o lan�amento. Reservados: " " - Contabilidade
                                                                                                                                                   "P" - Folha de Pagamento
                                                                                                                                                   "E" - Escritura��o Fiscal
                                                                                                                                                   "M" - Corre��o Monet�ria
                                                                                                                                                   "F" - FINANCEIRO USAR ESTE
                                      14	C�digo da empresa	        5	        Caracteres entre aspas "Exemplo: "01001"
                                      exemplo enviado pelo Helio
                                      07/02/05,14,489,26.50,15,"VENDA PARA DULCE DEMETRIA STEFASK- N� 8474-6499","F",01001
                                      }
                                      linha:=formatdatetime('dd/mm/yy',fieldbyname('data').asdatetime);
                                      linha:=linha+','+fieldbyname('contadebite').AsString;
                                      linha:=linha+','+fieldbyname('contacredite').AsString;
                                      linha:=linha+','+virgulaparaponto(FormatFloat('.00',fieldbyname('valor').asfloat));
                                      linha:=linha+','+PCodhistoricoPadrao;
                                      if length(fieldbyname('historico').AsString)>248
                                      Then linha:=linha+','+'"'+completapalavra(fieldbyname('historico').AsString,248,' ')+'"'
                                      Else linha:=linha+','+'"'+fieldbyname('historico').AsString+'"';
                                      linha:=linha+','+'"F"';
                                      linha:=linha+','+completapalavra(PcodEmpresaContabil,5,' ');
                            End
                            Else Begin
                                      if (PNumLayout='3')
                                      Then Begin
                                                //Layout 3
                                                //Exactus Sistemas - Imob. Portinari Navirai/MS
                                                {
                                                C�DIGO CONT�BIL COM 7 DIGITOS
                                                
                                                Coluna	       tamanho
                                                
                                                DEVEDORA		    X(07)				  1		  7
                                                CREDORA		      X(07)				  8		  7
                                                SIMBOLO		      X(02)				 15		  2
                                                DOCUMENTO	      X(06)				 17		  6
                                                ORDEM		        X(05)				 23		  5
                                                LINHA		        X(01)				 28		  1
                                                COMPLEMENTO	    X(29)				 29		 29
                                                VALOR		        X(12)				 58		 12
                                                DIA			        X(02)				 70		  2
                                                M�S			        X(02)				 72		  2
                                                ANO			        X(02)				 74		  2
                                                
                                                *****Explicacao****
                                                DEVEDORA		C�digo cont�bil devedor. Conte�do dever� ser num�rico
                                                CREDORA		  C�digo cont�bil credor. Conte�do dever� ser num�rico
                                                S�MBOLO		  S�mbolo alfab�tico ou hist�rico padr�o num�rico
                                                DOCUMENTO	  Documento de origem num�rica
                                                ORDEM		    N�mero de ordem  num�rica
                                                LINHA	      N�mero da linha de continua��o. Dever� conter zeros se o lan�amento tiver somente 1 linha.
                                                            Para lan�amentos com mais de uma linha, todos os campos dever�o ser repetidos em todas as
                                                            linhas com exce��o do campo VALOR que dever� aparecer somente na �ltima linha do lan�amento.
                                                            Nas linhas anteriores, zerar os campos VALOR
                                                COMPLEMENTO	Hist�rico  alfanum�rico
                                                VALOR 	    Valor sem centavos ( ou seja, multiplicado por 100).
                                                             Poder� ser "zerado"  se a linha for diferente de zeros.
                                                DIA			Num�rico
                                                M�S			Num�rico
                                                ANO			Num�rico
                                                }
                                                temp:=fieldbyname('historico').AsString;
                                                if (length(temp)>29)
                                                then Begin
                                                          //Nao posso ter mais de 261 porque o campo linha
                                                          //� um digito isso significa que
                                                          //posso ter no maximo 9 linhas de 29
                                                          
                                                          if (length(temp)>261)
                                                          Then Temp:=copy(temp,1,261);
          
                                                          //Quando o hist�rico ultrapassa 29 caractares tenho que enviar em v�rias linhas
                                                          //Explicacao do Marcos da Exactus:
                                                          {LINHA N�mero da linha de continua��o. Dever� conter zeros se o lan�amento
                                                          tiver somente 1 linha.
                                                          Para lan�amentos com mais de uma linha,
                                                          todos os campos dever�o ser repetidos em todas as linhas
                                                          com exce��o do campo VALOR que dever� aparecer somente na �ltima linha do lan�amento.
                                                          Nas linhas anteriores, zerar os campos VALOR
                                                          Exemplo:
                                                                  20000011000001             1TESTE 2 LINHAS TESTE 2 LINHAS000000000000010107
                                                                  20000011000001             2 TESTE 2 LINHAS              000001111111010107
                                                          }
                                                          cont:=0;
                                                          While (length(temp)>29) do
                                                          Begin
                                                               saida1:='';
                                                               saida2:='';
                                                               DividirValorCOMSEGUNDASEMTAMANHO(temp,29,saida1,saida2);
                                                               temp:=saida2;
          
                                                               inc(cont,1);
                                                               linha:='';
                                                               linha:=linha+completapalavra(fieldbyname('classificacaodebito').AsString,7,' ');//debito
                                                               linha:=linha+completapalavra(fieldbyname('classificacaocredito').AsString,7,' ');//credito
                                                               linha:=linha+completapalavra('',2,' ');//simbolo
                                                               linha:=linha+completapalavra_a_esquerda(fieldbyname('numdocto').asstring,6,' ');//documento
                                                               linha:=linha+completapalavra('',5,' ');//ordem
                                                               linha:=linha+completapalavra(inttostr(cont),1,'');//linha
                                                               linha:=linha+completapalavra(saida1,29,' ');//complemento
                                                               linha:=linha+CompletaPalavra_a_Esquerda('0',12,'0');
                                                               linha:=linha+formatdatetime('ddmmyy',fieldbyname('data').asdatetime);
                                                               Writeln(arq,linha);
                                                               Flush(arq);
                                                          End;
                                                          //o que restou do historico
                                                          //agora vai o valor junto
                                                          inc(cont,1);
                                                          linha:='';
                                                          linha:=linha+completapalavra(fieldbyname('classificacaodebito').AsString,7,' ');//debito
                                                          linha:=linha+completapalavra(fieldbyname('classificacaocredito').AsString,7,' ');//credito
                                                          linha:=linha+completapalavra('',2,' ');//simbolo
                                                          linha:=linha+completapalavra_a_esquerda(fieldbyname('numdocto').asstring,6,' ');//documento
                                                          linha:=linha+completapalavra('',5,' ');//ordem
                                                          linha:=linha+completapalavra(inttostr(cont),1,'');//linha
                                                          linha:=linha+completapalavra(temp,29,' ');//complemento
                                                          linha:=linha+CompletaPalavra_a_Esquerda(come(come(formata_valor(fieldbyname('valor').asstring),'.'),','),12,'0');
                                                          linha:=linha+formatdatetime('ddmmyy',fieldbyname('data').asdatetime);
          
          
                                                End
                                                Else Begin
                                                        linha:='';
                                                        linha:=linha+completapalavra(fieldbyname('classificacaodebito').AsString,7,' ');//debito
                                                        linha:=linha+completapalavra(fieldbyname('classificacaocredito').AsString,7,' ');//credito
                                                        linha:=linha+completapalavra('',2,' ');//simbolo
                                                        linha:=linha+completapalavra_a_esquerda(fieldbyname('numdocto').asstring,6,' ');//documento
                                                        linha:=linha+completapalavra('',5,' ');//ordem
                                                        linha:=linha+completapalavra('0',1,'');//linha
                                                        linha:=linha+completapalavra(temp,29,' ');//complemento
                                                        linha:=linha+CompletaPalavra_a_Esquerda(come(come(formata_valor(fieldbyname('valor').asstring),'.'),','),12,'0');
                                                        linha:=linha+formatdatetime('ddmmyy',fieldbyname('data').asdatetime);
                                                End;
                                      End;
                            End;
                  End;

                  Writeln(arq,linha);
                  Flush(arq);

                  Qlocal.close;
                  Qlocal.sql.clear;
                  Qlocal.sql.add('Update tabexportacontabilidade set exportado=''S'',nomedoarquivo='+#39+Nome+#39+' where codigo='+Fieldbyname('codigo').asstring);

                  Try
                    Qlocal.execsql;
                  Except
                        messagedlg('Erro na tentativa de atualizar os registros para exportado=S!'+#13+'Exporta��o Cancelada!',mterror,[mbok],0);
                        FDataModulo.IBTransaction.RollbackRetaining;
                        exit;
                  End;

                  next;
             End;
             Fdatamodulo.ibtransaction.CommitRetaining;



             Messagedlg('Arquivo Exportado Com Sucesso!',mtinformation,[mbok],0);
           
          Finally
                 closefile(arq);
                 freeandnil(qlocal);
          End;

     End;






end;

procedure TObjExportaContabilidade.Imprime;
begin
     With FMenuRelatorios  do
     Begin
          NomeObjeto:='UOBJEXPORTACONTABILIDADE';
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Dados a serem exportados por Data do T�tulo');//0
                items.add('Dados a serem exportados por Data do Movimento no Sistema');//1
                items.add('Dados que foram exportados');//2
                items.add('Dados Bloqueados');//3
                items.add('Todos os Lan�amentos');//4
                items.add('Raz�o');//5
                items.add('Balan�o');//6
          End;
          showmodal;
          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0:Self.ImprimeRegistrosporData('T',false);
                1:Self.ImprimeRegistrosporData('M',false);
                2:Self.ImprimeExportados;
                3:Self.ImprimeRegistrosporData('T',True);
                4:Self.ImprimeDados;
                5:Self.ImprimeRazao;
                6:Self.Imprimebalanco2;
          End;

     end;




end;

procedure TObjExportaContabilidade.ImprimeRegistrosporData(Tipo:string;Somentebloqueados:Boolean);
var
Data1,data2:Tdate;
linha:integer;
begin

     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Data Inicial';

          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.caption:='Data Final';

          showmodal;
          If tag=0
          Then exit;

          Try
                Data1:=Strtodate(edtgrupo01.text);
          Except
                Messagedlg('Data Inicial Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          Try
                Data2:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Data Final Inv�lida!',mterror,[mbok],0);
                exit;
          End;

     End;
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select * from TabExportaContabilidade where Exportado=''N'' and');
          IF (Tipo='T')//data do titulo da emissao...
          Then SelectSQL.add('(Data>='+#39+FormatDateTime('mm/dd/yyyy',data1)+#39+' and Data<='+#39+FormatDateTime('mm/dd/yyyy',data2)+#39+')')
          Else SelectSQL.add('(cast (DataC as date)>='+#39+FormatDateTime('mm/dd/yyyy',data1)+#39+' and cast (DataC as date)<='+#39+FormatDateTime('mm/dd/yyyy',data2)+#39+')');
          if (SomenteBLOQUEADOS)
          Then SelectSQL.add('and Exporta=''N'' ')
          Else SelectSQL.add('and Exporta=''S'' '); 
          open;
          If (Recordcount=0)
          Then Begin
                    Messagedlg('N�o foi selecionado nenhum registro nas datas estipuladas que n�o foram exportados ainda!',mtinformation,[mbok],0);
                    exit;
          End;

          //MODELO DE RELATORIO

          FreltxtRDPRINT.ConfiguraImpressao;//meu configura
          FreltxtRDPRINT.RDprint.Abrir;

          if (FreltxtRDPRINT.RDprint.setup=False)
          Then BEgin
                    FreltxtRDPRINT.RDprint.fechar;
                    exit;
          End;



          //comeca com 3 por causa de cabecalho
          linha:=3;
          //titulo
          if (Somentebloqueados=True)
          Then FreltxtRDPRINT.RDprint.ImpC(linha,45,'RELAT�RIO DE MOVIMENTA��O DE DADOS BLOQUEADOS -  DE '+datetostr(data1)+' A '+datetostr(Data2),[negrito])
          Else FreltxtRDPRINT.RDprint.ImpC(linha,45,'RELAT�RIO DE MOVIMENTA��O DE DADOS A SEREM EXPORTADOS -  DE '+datetostr(data1)+' A '+datetostr(Data2),[negrito]);
          inc(linha,2);
          //T�tulo das colunas
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('C�DIGO',6,' ')+' '+
                                              CompletaPalavra('DATA',8,' ')+' '+
                                              CompletaPalavra('CONTA D�BITO',15,' ')+' '+
                                              CompletaPalavra('CONTA CR�DITO',15,' ')+' '+
                                              CompletaPalavra('VALOR',10,' ')+' '+
                                              CompletaPalavra('HIST�RICO',36,' '),[negrito]);
          inc(linha,1);
          first;
          while not(eof) do
          Begin
               //verifica se precisa saltar de pagina
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);//meu

               FreltxtRDPRINT.RDprint.Imp(linha,1, CompletaPalavra(FIELDBYNAME('CODIGO').ASSTRING,6,' ')+' '+
                                              CompletaPalavra(formatdatetime('dd/mm/yy',FIELDBYNAME('DATA').asdatetime),8,' ')+' '+
                                              CompletaPalavra(FIELDBYNAME('CONTADEBITE').ASSTRING+'-'+FIELDBYNAME('NOMEDEBITE').ASSTRING,15,' ')+' '+
                                              CompletaPalavra(FIELDBYNAME('CONTACREDITE').ASSTRING+'-'+FIELDBYNAME('NOMECREDITE').ASSTRING,15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('VALOR').ASSTRING),10,' ')+' '+
                                              CompletaPalavra(FIELDBYNAME('HISTORICO').ASSTRING,36,' '));

               inc(linha,1);
               next;
          End;
          FreltxtRDPRINT.RDprint.Fechar;

     eND;

end;

function TObjExportaContabilidade.Get_NOmedoArquivo: string;
begin
     Result:=Self.nomedoarquivo;
end;

procedure TObjExportaContabilidade.Submit_NomedoArquivo(Parametro: string);
begin
     Self.nomedoarquivo:=parametro;
end;

procedure TObjExportaContabilidade.RetornaExportacao(PNomeArq: string);
var
conta:INteger;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select count(codigo) as CONTA from tabexportacontabilidade where nomedoarquivo='+#39+UPPERCASE(PNomearq)+#39);
          open;
          conta:=fieldbyname('CONTA').asinteger;
          If (conta=0)
          Then Begin
                    Messagedlg('N�o foi encontrado nenhum registro ligado a este arquivo!',mtinformation,[mbok],0);
                    exit;
          End;
          close;
          SelectSQL.clear;
          SelectSQL.add('Update TabExportaContabilidade set exportado=''N'' where nomedoarquivo='+#39+uppercase(PNomeArq)+#39);
          execsql;
          fdatamodulo.IBTransaction.CommitRetaining;
          Messagedlg(Inttostr(conta)+' Registros foram retornados!',mtinformation,[mbok],0);
          close;
          
     End;
end;

function TObjExportaContabilidade.Get_PesquisaExportados: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabExportaContabilidade where exportado=''S'' ');
     Result:=Self.ParametroPesquisa;

end;

function TObjExportaContabilidade.Get_PesquisaNaoExportados: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabExportaContabilidade where not(exportado=''S'') ');
     Result:=Self.ParametroPesquisa;
end;

function TObjExportaContabilidade.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabExportaContabilidade');
     Result:=Self.ParametroPesquisa;
end;

function TObjExportaContabilidade.Get_TituloPesquisaExportados: string;
begin
     Result:='Pesquisa de T�tulos Exportados';
end;

function TObjExportaContabilidade.Get_TituloPesquisaNaoExportados: string;
begin
     Result:='Pesquisa de T�tulos N�o Exportados';
end;

procedure TObjExportaContabilidade.ImprimeExportados;
VAR
NOME:string;
linha:integer;
begin
     nome:='';
     IF (InputQuery('RELAT�RIO','Nome do Arquivo:',nome)=false)
     Then exit;

     If (Nome='')
     Then Begin
               Messagedlg('O Nome do Relat�rio deve ser informado!',mtinformation,[mbok],0);
               exit;
     End;
     NOME:=UPPERCASE(NOME);
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select * from tabexportacontabilidade');
          SelectSQL.add('where exportado=''S'' and nomedoarquivo='+#39+uppercase(nome)+#39);
          SelectSQL.add('order by codigo');
          open;
          IF (recordcount=0)
          Then BEgin
                    Messagedlg('Nenhum registro foi encontrado ligado a este arquivo!',mtinformation,[mbok],0);
                    exit;

          End;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.fechar;
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,'DADOS EXPORTADOS ARQUIVO '+nome,[negrito]);
          inc(linha,2);
          
          //T�tulo das colunas
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('C�DIGO',6,' ')+' '+
                                                   CompletaPalavra('DATA',8,' ')+' '+
                                                   CompletaPalavra('CONTA D�BITO',15,' ')+' '+
                                                   CompletaPalavra('CONTA CR�DITO',15,' ')+' '+
                                                   CompletaPalavra('VALOR',10,' ')+' '+
                                                   CompletaPalavra('HIST�RICO',36,' '),[negrito]);
          inc(linha,1);
          first;
          while not(eof) do
          Begin
               //verifica se precisa saltar de pagina
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);//meu
               FreltxtRDPRINT.RDprint.Imp(linha,1, CompletaPalavra(FIELDBYNAME('CODIGO').ASSTRING,6,' ')+' '+
                                              CompletaPalavra(formatdatetime('dd/mm/yy',FIELDBYNAME('DATA').asdatetime),8,' ')+' '+
                                              CompletaPalavra(FIELDBYNAME('CONTADEBITE').ASSTRING+'-'+FIELDBYNAME('NOMEDEBITE').ASSTRING,15,' ')+' '+
                                              CompletaPalavra(FIELDBYNAME('CONTACREDITE').ASSTRING+'-'+FIELDBYNAME('NOMECREDITE').ASSTRING,15,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('VALOR').ASSTRING),10,' ')+' '+
                                              CompletaPalavra(FIELDBYNAME('HISTORICO').ASSTRING,36,' '));
          
               inc(linha,1);
               next;
          End;
          FreltxtRDPRINT.RDprint.Fechar;
          Close;
     End;
end;


procedure TObjExportaContabilidade.ImportaRegistros;
begin
     //serve para importar lancamentos efetuados em outro PC, ou ajuste
end;



procedure TObjExportaContabilidade.Imprimebalanco2;
var
   PdataBalanco,PinicialMes:Tdate;
   posicaocoluna,linha:integer;
   temp:string;
   
   PsomaTitulo:Currency;
   PSomaAtivo,PSomaPassivo,PsomaContaResultadoExercicio:Currency;
   PcontaAnterior,PtipoAnterior,PNomeAnterior,PClassificacaoAnterior:String;
   PValorAnterior:Currency;
   
   PmostraContaTituloSemSaldo:Boolean;
   PimprimeContaSS,PtipoAtual:string;

Begin
     if (ObjParametroGlobal.ValidaParametro('MOSTRA CONTA TITULO SEM SALDO NO BALANCO?')=False)
     then exit;

     if (ObjParametroGlobal.Get_Valor='SIM')
     Then PmostraContaTituloSemSaldo:=True
     Else PmostraContaTituloSemSaldo:=False;


     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.Caption:='Data do Balan�o';
          edtgrupo01.EditMask:=MascaraData;

          Showmodal;

          If (Tag=0)
          Then exit;

          Try
             PdataBalanco:=Strtodate(edtgrupo01.text);
          except
                Messagedlg('Data inv�lida!',mterror,[mbok],0);
                exit;
          End;
     End;

     PinicialMes:=strtodatetime('01/'+Formatdatetime('mm/yyyy',PdataBalanco));

     With Self.ObjDataset do
     Begin

          close;
          SelectSQL.clear;
          SelectSQL.add('Select Sum(PEC.valor) as SOMA from procexportacontabilidade PEC');
          SelectSQL.add('join tabplanodecontas TPC on PEC.conta=TPC.codigo');
          SelectSQL.add('where PEC.data>='+#39+formatdatetime('mm/dd/yyyy',PinicialMes)+#39);
          SelectSQL.add('and PEC.data<='+#39+formatdatetime('mm/dd/yyyy',PDataBalanco)+#39);
          SelectSQL.add('and (TPC.Classificacao like ''3%'' or TPC.Classificacao like ''4%'' or TPC.Classificacao like ''5%'' or TPC.Classificacao like ''6%'') ');
          open;
          PsomaContaResultadoExercicio:=Fieldbyname('soma').asfloat;


          close;
          SelectSQL.clear;
          SelectSQL.add('Select Sum(PEC.valor) as SOMA from procexportacontabilidade PEC');
          SelectSQL.add('join tabplanodecontas TPC on PEC.conta=TPC.codigo');
          SelectSQL.add('where PEC.data<='+#39+formatdatetime('mm/dd/yyyy',PDataBalanco)+#39);
          SelectSQL.add('and TPC.Classificacao like ''1%'' ');
          open;
          PsomaAtivo:=Fieldbyname('soma').asfloat;

          close;
          SelectSQL.clear;
          SelectSQL.add('Select Sum(PEC.valor) as SOMA from procexportacontabilidade PEC');
          SelectSQL.add('join tabplanodecontas TPC on PEC.conta=TPC.codigo');
          SelectSQL.add('where PEC.data<='+#39+formatdatetime('mm/dd/yyyy',PDataBalanco)+#39);
          SelectSQL.add('and TPC.Classificacao like ''2%'' ');
          open;
          PsomaPassivo:=Fieldbyname('soma').asfloat;
          
          close;
          SelectSQL.clear;
          SelectSQL.add('Select PBC.*');
          SelectSQL.add('from ProcBalancoContabil('+#39+formatdatetime('mm/dd/yyyy',PdataBalanco)+#39+','
          +#39+formatdatetime('mm/dd/yyyy',PinicialMes)+#39+','
          +#39+formatdatetime('mm/dd/yyyy',PdataBalanco)+#39+') PBC');
          SelectSQL.add('order by PBC.Pclassificacao');
          open;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Gerando Balan�o';



          if (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi encontrada!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          inc(linha,3);
          if (FreltxtRDPRINT.RDprint.Setup=false)
          then Begin
                    FreltxtRDPRINT.rdprint.Fechar;
                    exit;
          end;

          FreltxtRDPRINT.RDprint.impc(linha,45,'BALAN�O CONT�BIL '+datetostr(PdataBalanco),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.impf(linha,1,CompletaPalavra('NOME',45,' '),[negrito]);
          FreltxtRDPRINT.RDprint.impf(linha,60,CompletaPalavra_a_Esquerda('SALDO',12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);

          While not(eof) do
          Begin
               FMostraBarraProgresso.IncrementaBarra1(1);
               FMostraBarraProgresso.Show;
               Application.ProcessMessages;

               PsomaTitulo:=0;
               PtipoAtual:=fieldbyname('pclassificacao').asstring[1];
               
               case fieldbyname('pclassificacao').asstring[1] of
                '1':Begin
                        inc(linha,1);
                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                        FreltxtRDPRINT.RDprint.impf(linha,1,CompletaPalavra('ATIVO',45,' '),[negrito]);
                        inc(linha,1);
                 End;
                 '2':Begin
                        inc(linha,1);
                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                        FreltxtRDPRINT.RDprint.impf(linha,1,CompletaPalavra('PASSIVO',45,' '),[negrito]);
                        inc(linha,1);
                 End;
                 '3':Begin
                        inc(linha,1);
                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                        FreltxtRDPRINT.RDprint.impf(linha,1,CompletaPalavra('DEMONSTRA��O DO RESULTADO DO EXERC�CIO',45,' '),[negrito]);
                        inc(linha,1);
                 End;

               End;

          
               PcontaAnterior:=Fieldbyname('pcodigo').asstring;
               PtipoAnterior:=Fieldbyname('ptipo').asstring;
               PClassificacaoAnterior:=Fieldbyname('pclassificacao').asstring;
               PNomeAnterior:=Fieldbyname('pnome').asstring;
               PValorAnterior:=Fieldbyname('pvalor').asfloat;
               PimprimeContaSS:=Fieldbyname('pimprimebalancoss').asstring;
               PsomaTitulo:=PsomaTitulo+Fieldbyname('pvalor').asfloat;
               PosicaoColuna:=0;

               next;

               While ((Fieldbyname('pclassificacao').asstring[1]=PtipoAtual) and not(eof)) do
               Begin
                    posicaocoluna:=Self.ContaPontosClassificacao(PclassificacaoAnterior);

                    if (Fieldbyname('ptipo').asstring='T')
                    Then Begin
                              //a atual � uma conta titulo
                              if (PtipoAnterior='T')
                              Then Begin
                                        //a anterior tmbm era uma t�tulo
                                        //isso significa que a anterior esta zerada
                                        //e que s� imprimo se o parametro
                                        //estiver configurado para imprimir
                                        if (PmostraContaTituloSemSaldo=True)
                                        or (PValorAnterior>0)
                                        or (PimprimeContaSS='S')
                                        Then Begin
                                                  inc(linha,1);
                                                  FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                                  FreltxtRDPRINT.RDprint.ImpF(linha,posicaocoluna,CompletaPalavra(pnomeanterior,45,' '),[negrito]);
                                                  if (PValorAnterior>0)
                                                  Then Begin
                                                           FreltxtRDPRINT.RDprint.ImpF(linha,60,CompletaPalavra_a_Esquerda(formata_valor(pvaloranterior),12,' '),[negrito]);
                                                           FreltxtRDPRINT.RDprint.ImpF(linha,75,CompletaPalavra_a_Esquerda(formata_valor(PsomaTitulo),12,' '),[negrito]);
                                                  End;
                                                  inc(linha,1);
                                        End;
                              End
                              Else Begin
                                        //A Anterior era uma analitica, como a atual � titulo entao
                                        //tenho que imprimir a anterior seguida da totalizacao da titulo anterior
                                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                        FreltxtRDPRINT.RDprint.Imp(linha,posicaocoluna,CompletaPalavra(pnomeanterior,45,' '));
                                        FreltxtRDPRINT.RDprint.Imp(linha,60,CompletaPalavra_a_Esquerda(formata_valor(pvaloranterior),12,' '));
                                        FreltxtRDPRINT.RDprint.Imp(linha,75,CompletaPalavra_a_Esquerda(formata_valor(PsomaTitulo),12,' '));
                                        inc(linha,1);
                              End;
                              //como a atual � uma titulo tenho que zerar a titulo anterior
                              PsomaTitulo:=0;
                    End
                    Else Begin
                              //a atual � uma analitica
                              if (PtipoAnterior='T')
                              Then Begin
                                        //a anterior era uma t�tulo
                                        //tenho que imprimir
                                        inc(linha,1);
                                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                        FreltxtRDPRINT.RDprint.ImpF(linha,posicaocoluna,CompletaPalavra(pnomeanterior,45,' '),[negrito]);
                                        if (PValorAnterior>0)
                                        Then FreltxtRDPRINT.RDprint.ImpF(linha,60,CompletaPalavra_a_Esquerda(formata_valor(pvaloranterior),12,' '),[negrito]);
                                        inc(linha,1);
                              End
                              Else Begin
                                        //A Anterior era uma analitica
                                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                        FreltxtRDPRINT.RDprint.Imp(linha,posicaocoluna,CompletaPalavra(pnomeanterior,45,' '));
                                        FreltxtRDPRINT.RDprint.Imp(linha,60,CompletaPalavra_a_Esquerda(formata_valor(pvaloranterior),12,' '));
                                        inc(linha,1);
                              End;
                    End;

                    PcontaAnterior:=Fieldbyname('pcodigo').asstring;
                    PtipoAnterior:=Fieldbyname('ptipo').asstring;
                    PClassificacaoAnterior:=Fieldbyname('pclassificacao').asstring;
                    PNomeAnterior:=Fieldbyname('pnome').asstring;
                    PValorAnterior:=Fieldbyname('pvalor').asfloat;
                    PimprimeContaSS:=Fieldbyname('pimprimebalancoss').asstring;
                    PsomaTitulo:=PsomaTitulo+Fieldbyname('pvalor').asfloat;
                    next;
               End;//while


               
               //Saiu, ou acabou as contas ou mudou de classificacao
               if (PtipoAnterior='T')
               Then Begin
                         //a anterior tmbm era uma t�tulo
                         //isso significa que a anterior esta zerada
                         if (PmostraContaTituloSemSaldo=True)
                         or (PValorAnterior>0)
                         or (PimprimeContaSS='S')
                         Then Begin
                                   inc(linha,1);
                                   FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                                   FreltxtRDPRINT.RDprint.ImpF(linha,posicaocoluna,CompletaPalavra(pnomeanterior,45,' '),[negrito]);
                                   if (PValorAnterior>0)
                                   Then Begin
                                            FreltxtRDPRINT.RDprint.ImpF(linha,60,CompletaPalavra_a_Esquerda(formata_valor(pvaloranterior),12,' '),[negrito]);
                                            FreltxtRDPRINT.RDprint.ImpF(linha,75,CompletaPalavra_a_Esquerda(formata_valor(PsomaTitulo),12,' '),[negrito]);
                                   End;
                                   inc(linha,1);
                         End;
               End
               Else Begin
                         //A Anterior era uma analitica, como a atual � titulo entao
                         //tenho que imprimir a anterior seguida da totalizacao da titulo anterior
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Imp(linha,posicaocoluna,CompletaPalavra(pnomeanterior,45,' '));
                         FreltxtRDPRINT.RDprint.Imp(linha,60,CompletaPalavra_a_Esquerda(formata_valor(pvaloranterior),12,' '));
                         FreltxtRDPRINT.RDprint.Imp(linha,75,CompletaPalavra_a_Esquerda(formata_valor(PsomaTitulo),12,' '));
                         inc(linha,1);
               End;

               //*************totalizando***********************************
               inc(linha,1);
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               case PClassificacaoAnterior[1] of
                '1':Begin
                        FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('TOTAL DO ATIVO',45,' '),[negrito]);
                        FreltxtRDPRINT.RDprint.ImpF(linha,75,CompletaPalavra_a_Esquerda(formata_valor(PSomaAtivo),12,' '),[negrito]);
                        inc(linha,1);
                 End;
                 '2':Begin
                        FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('TOTAL DO PASSIVO',45,' '),[negrito]);
                        FreltxtRDPRINT.RDprint.ImpF(linha,75,CompletaPalavra_a_Esquerda(formata_valor(PSomaPassivo),12,' '),[negrito]);
                        inc(linha,1);
                 End;
                 '3':Begin
                        //FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('TOTAL DO DRE',45,' '),[negrito]);
                        FreltxtRDPRINT.RDprint.ImpF(linha,75,CompletaPalavra_a_Esquerda(formata_valor(PsomaContaResultadoExercicio),12,' '),[negrito]);
                        inc(linha,1);
                 End;
                End;

          End;//while geral

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',90,'_'));

          FMostraBarraProgresso.Close;
          FreltxtRDPRINT.RDprint.Fechar;

     end;

end;

procedure TObjExportaContabilidade.ImprimeDados;
var
Pdata1,PData2:Tdate;
linha:integer;
Pvalor:string;
historico1,historico2,temp,pobjgerador,pcodgerador:string;
PsomaDebite,PsomaCredite:Currency;
begin
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.Caption:='Data Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.Caption:='Data Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo03.Enabled:=true;
          lbgrupo03.caption:='OBJGERADOR (OPCIONAL)';

          Grupo04.Enabled:=true;
          lbgrupo04.caption:='CODGERADOR (OPCIONAL)';

          Showmodal;

          If (Tag=0)
          Then exit;

          pobjgerador:=edtgrupo03.text;
          pcodgerador:=edtgrupo04.text;

          Try
             Pdata1:=Strtodate(edtgrupo01.text);
             Pdata2:=Strtodate(edtgrupo02.text);
          except
                Messagedlg('Datas inv�lidas!',mterror,[mbok],0);
                exit;
          End;
     End;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select TEC.*,TCD.Nome as NOMECONTADEBITO,TCC.nome as NOMECONTACREDITO');
          SelectSQL.add('from tabexportacontabilidade TEC');
          SelectSQL.add('left join TabPlanodeContas TCD on TCD.Codigo=TEC.ContaDebite');
          SelectSQL.add('left join TabPlanodeContas TCC on TCC.Codigo=TEC.ContaCredite');
          SelectSQL.add('where TEC.Data>='+#39+formatdatetime('mm/dd/yyyy',pdata1)+#39);
          SelectSQL.add('and TEc.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata2)+#39);

          if (pobjgerador<>'')
          Then SelectSQL.add('and TEC.objgerador='+#39+pobjgerador+#39);

          if (pcodgerador<>'')           
          Then SelectSQL.add('and TEC.codgerador='+#39+pCODgerador+#39);

          SelectSQL.add('order by TEC.codigo');
          open;

          if (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi encontrada!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.rdprint.TamanhoQteColunas:=160;
          FreltxtRDPRINT.rdprint.FonteTamanhoPadrao:=S20cpp;
          //tem q ver quantas colunas cabem em 20cpp
          FreltxtRDPRINT.RDprint.Abrir;
          inc(linha,3);
          if (FreltxtRDPRINT.RDprint.Setup=false)
          then Begin
                    FreltxtRDPRINT.rdprint.Fechar;
                    exit;
          end;
          FreltxtRDPRINT.RDprint.impc(linha,80,'TODOS OS LAN�AMENTOS',[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.imp(linha,01,'Intervalo '+datetostr(Pdata1)+' a '+Datetostr(pdata2));
          inc(linha,2);

          if (pobjgerador<>'') or (pcodgerador<>'')
          Then Begin
                    FreltxtRDPRINT.RDprint.imp(linha,01,'OBJGERADOR: '+pobjgerador+' CODGERADOR: '+pcodgerador);
                    inc(linha,2);
          End;
          
          FreltxtRDPRINT.RDprint.impf(linha,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                              CompletaPalavra('DATA',8,' ')+' '+
                                              CompletaPalavra('HIST�RICO',69,' ')+' '+
                                              CompletaPalavra('CONTA DEBITO',30,' ')+' '+
                                              CompletaPalavra('CONTA CREDITO',30,' ')+' '+
                                              CompletaPalavra_a_Esquerda('VALOR',12,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',160,'_'));
          inc(linha,2);

          PsomaDebite:=0;
          PsomaCredite:=0;

          While not(eof) do
          begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.imp(linha,1,
                                              CompletaPalavra(fieldbyname('CODIGO').asstring,6,' ')+' '+
                                              CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('DATA').asdatetime),8,' ')+' '+
                                              CompletaPalavra(fieldbyname('HISTORICO').asstring,69,' ')+' '+
                                              CompletaPalavra(completapalavra(fieldbyname('contadebite').asstring,4,' ') +'-'+fieldbyname('NOMECONTADEBITO').asstring,30,' ')+' '+
                                              CompletaPalavra(completapalavra(fieldbyname('contacredite').asstring,4,' ')+'-'+fieldbyname('NOMECONTACREDITO').asstring,30,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),12,' '));
               inc(linha,1);

               if (fieldbyname('contadebite').asstring<>'')
               Then PsomaDebite:=PsomaDebite+fieldbyname('VALOR').ascurrency;

               if (fieldbyname('contacredite').asstring<>'')
               Then PsomaCredite:=Psomacredite+fieldbyname('VALOR').ascurrency;

               next;
          End;

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',160,'_'));
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'Total em Cr�ditos: R$ '+formata_valor(PsomaCredite)+' Total em D�bitos: R$ '+formata_valor(PsomaDebite),[negrito]);
          inc(linha,1);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',160,'_'));
          FreltxtRDPRINT.RDprint.Fechar;
     end;

end;

procedure TObjExportaContabilidade.ImprimeRazao;
var
Pdata1,PData2:Tdate;
linha:integer;
Pvalor:string;
ptemp:string;
Pconta:string;
Psaldo:Currency;
historico1,historico2:String;
begin
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.Caption:='Data Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.Caption:='Data Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo03.Enabled:=true;
          LbGrupo03.Caption:='Conta';
          edtgrupo03.OnKeyDown:=Self.edtcontaKeyDown;

          Showmodal;

          If (Tag=0)
          Then exit;

          Try
             Pdata1:=Strtodate(edtgrupo01.text);
             Pdata2:=Strtodate(edtgrupo02.text);
          except
                Messagedlg('Datas inv�lidas!',mterror,[mbok],0);
                exit;
          End;

          Try
              pconta:='';
              Strtoint(Edtgrupo03.text);
              Pconta:=Edtgrupo03.text;

              if (Self.ObjPlanodeContas.LocalizaCodigo(Pconta)=False)
              then Begin
                        MensagemErro('Conta n�o localizada');
                        exit;
              End;
              Self.ObjPlanodeContas.TabelaparaObjeto;

          except
              MensagemErro('Escolha uma conta');
              exit;
          End;
     End;

     With Self.ObjDataset do
     Begin
          //calculando o saldo anterior da conta
          close;
          SelectSQL.clear;
          SelectSQL.add('Select sum(valor) as SOMA');
          SelectSQL.add('from procexportacontabilidade');
          SelectSQL.add('Where Conta='+pconta);
          SelectSQL.add('and Data<'+#39+formatdatetime('mm/dd/yyyy',pdata1)+#39);
          open;
          Try
            Psaldo:=strtofloat(fieldbyname('soma').asstring);
          Except
            Psaldo:=0;
          End;
          //*******************************************************************
          close;
          SelectSQL.clear;
          SelectSQL.add('Select TEC.*');
          SelectSQL.add('from tabexportacontabilidade TEC');
          SelectSQL.add('where TEC.Data>='+#39+formatdatetime('mm/dd/yyyy',pdata1)+#39);
          SelectSQL.add('and TEc.Data<='+#39+formatdatetime('mm/dd/yyyy',pdata2)+#39);
          SelectSQL.add('and (Tec.ContaDebite='+pconta+' or Tec.ContaCredite='+pconta+')');
          SelectSQL.add('order by TEC.data,Tec.Codigo');

          open;

          if (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi encontrada!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.rdprint.TamanhoQteColunas:=130;
          FreltxtRDPRINT.rdprint.FonteTamanhoPadrao:=S17cpp;
          //tem q ver quantas colunas cabem em 20cpp
          FreltxtRDPRINT.RDprint.Abrir;
          inc(linha,3);
          if (FreltxtRDPRINT.RDprint.Setup=false)
          then Begin
                    FreltxtRDPRINT.rdprint.Fechar;
                    exit;
          end;
          FreltxtRDPRINT.RDprint.impc(linha,65,'LIVRO RAZ�O',[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.impf(linha,01,'Per�odo :'+datetostr(Pdata1)+' a '+Datetostr(pdata2),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.impf(linha,01,completapalavra('Conta   :'+Self.ObjPlanodeContas.Get_codigo+'-'+Self.ObjPlanodeContas.Get_Nome,130,' '),[negrito]);
          inc(linha,2);

          
          FreltxtRDPRINT.RDprint.impf(linha,1,CompletaPalavra('DATA',8,' ')+' '+
                                              CompletaPalavra('LANCTO',6,' ')+' '+
                                              CompletaPalavra('HIST�RICO',51,' ')+' '+
                                              CompletaPalavra_a_Esquerda('DEBITO',20,' ')+' '+
                                              CompletaPalavra_a_Esquerda('CREDITO',20,' ')+' '+
                                              CompletaPalavra_a_Esquerda('SALDO',20,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',130,'_'));
          inc(linha,1);

          FreltxtRDPRINT.RDprint.impf(linha,1,'SALDO INICIAL'+CompletaPalavra_a_Esquerda(formata_valor(psaldo),117,' '),[negrito]);
          inc(linha,2);

          While not(eof) do
          begin

               ptemp:='';

               if (Fieldbyname('contadebite').asstring=Pconta)
               and (Fieldbyname('contacredite').asstring<>Pconta)
               Then Begin
                        //debite
                        Psaldo:=Psaldo+fieldbyname('VALOR').asfloat;

                        PTemp:=CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),20,' ')+' '+CompletaPalavra('',20,' ');
               End;

               if (Fieldbyname('contadebite').asstring<>Pconta)
               and(Fieldbyname('contacredite').asstring=Pconta)

               Then Begin
                        //CREDITE
                        Psaldo:=Psaldo-fieldbyname('VALOR').asfloat;
                        PTemp:=CompletaPalavra('',20,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),20,' ');
               eND;

               if (Fieldbyname('contacredite').asstring=Pconta)
               and (Fieldbyname('contadebite').asstring=Pconta)
               Then PTemp:=CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),20,' ')+' '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),20,' ');


               historico1:=Fieldbyname('historico').asstring;
               historico2:='';
               if (length(historico1)>51)
               then DividirValorCOMSEGUNDASEMTAMANHO(Fieldbyname('historico').asstring,51,historico1,historico2);

               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.imp(linha,1,
                                              CompletaPalavra(formatdatetime('dd/mm/yy',fieldbyname('DATA').asdatetime),8,' ')+' '+
                                              CompletaPalavra(fieldbyname('CODIGO').asstring,6,' ')+' '+
                                              CompletaPalavra(historico1,51,' ')+' '+
                                              Ptemp+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(psaldo),20,' '));
               inc(linha,1);

               if (historico2<>'')
               Then begin
                         While (historico2<>'') do
                         Begin
                              Ptemp:=historico2;

                              DividirValorCOMSEGUNDASEMTAMANHO(ptemp,51,historico1,historico2);



                              if (length(historico1)>1)
                              Then Begin
                                        if (historico1[1]=' ')
                                        Then historico1:=copy(historico1,2,length(historico1)-1);

                                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                        FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('',8,' ')+' '+
                                                                           CompletaPalavra('',6,' ')+' '+
                                                                           CompletaPalavra(historico1,51,' '));
                                        Inc(linha,1);
                              End;
                         End;
               End;

               next;
          End;
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
          FreltxtRDPRINT.RDprint.impf(linha,1,'SALDO FINAL  '+CompletaPalavra_a_Esquerda(formata_valor(psaldo),117,' '),[negrito]);
          inc(linha,1);


          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.imp(linha,1,CompletaPalavra('_',130,'_'));
          FreltxtRDPRINT.RDprint.Fechar;
     end;

end;

procedure TObjExportaContabilidade.edtcontaKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjPlanodeContas.Get_Pesquisa,Self.ObjPlanodeContas.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


function TObjExportaContabilidade.LancaInverso(Pobjeto: string;
  Pcodigo: string;PdataAtual:string): boolean;
var
ObjQTemp:Tibquery;
Pinverso:string;
begin
     result:=False;
     Try
        ObjQTemp:=Tibquery.Create(nil);
        ObjQTemp.database:=Fdatamodulo.IBDatabase;
     Except
        Mensagemerro('Erro na tentativa de Criar a ObjQTemp');
        exit;
     End;

     Try
        With ObjQTemp do
        Begin
            close;
            sql.add('Select codigo from TabExportaContabilidade');
            sql.add('where Objgerador='+#39+uppercase(Pobjeto)+#39);
            sql.add('and Codgerador='+Pcodigo);
            open;
            while  not(eof) do
            begin
                 Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                 Self.TabelaparaObjeto;

                 Self.Codigo:=Self.Get_NovoCodigo;
                 Self.Status:=dsinsert;
                 Pinverso:=Self.ContaDebite;
                 Self.ContaDebite:=Self.ContaCredite;
                 Self.ContaCredite:=Pinverso;
                 Self.Data:=PdataAtual;
                 //gravo como se fosse partida simples, porque se a contabildiade
                 //� do tipo travada que nao pode lancar por exemplo sem uma das
                 //contas ou conta titulo, entao lanco como partida simples
                 //pra evitar que de problema ao retornar algo

                 if (self.Salvar(false,true)=False)
                 then begin
                           mensagemerro('Erro na tentativa de Gravar o Inverso da Contabilidade. C�digo da Exporta��o Origem: '+Fieldbyname('codigo').asstring);  
                           exit;
                 End;
                 

                 next;
            End;
        End;
        result:=true;
     Finally
            Freeandnil(ObjqTemp);
     End;

end;

procedure TObjExportaContabilidade.FechamentoContabilidade;
var
Pdata:Tdate;
begin

     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.EditMask:=MascaraData;
          LbGrupo01.caption:='Data do Fechamento';

          Showmodal;

          if (Tag=0)
          then exit;

          if (Validadata(1,pdata,false)=False)
          then exit;
     End;


     if (ObjParametroGlobal.PValidaParametro('DATA DO ULTIMO FECHAMENTO DA CONTABILIDADE')=False)
     Then exit;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Update tabexportacontabilidade set exportado=''S'',nomedoarquivo='+#39+datetostr(Pdata)+#39);
          SelectSQL.add('where Data<='+#39+Formatdatetime('mm/dd/yyyy',Pdata)+#39+' and Exportado=''N''  and exporta=''S'' ');
          try
            execsql;
            ObjParametroGlobal.Status:=dsedit;
            ObjParametroGlobal.submit_Pvalor(Datetostr(pdata));
            if (ObjParametroGlobal.Salvar(False)=False)
            then begin
                      fdatamodulo.IBTransaction.RollbackRetaining;
                      mensagemerro('N�o foi poss�vel atualizar o par�metro "'+'DATA DO ULTIMO FECHAMENTO DA CONTABILIDADE'+'"');
                      exit;
            end;

            FDataModulo.IBTransaction.CommitRetaining;
            mensagemaviso('Conclu�do');
            exit;
          except
            on e:exception do
            begin
                fdatamodulo.IBTransaction.RollbackRetaining;
                mensagemerro('Erro na tentativa de Alterar os registros da contabilidade'+#13+E.message);
            End;
          End;

     End;

end;


procedure TObjExportaContabilidade.RetornaFechamentoContabilidade;
var
Pdata:Tdate;
begin
     if (ObjParametroGlobal.PValidaParametro('DATA DO ULTIMO FECHAMENTO DA CONTABILIDADE')=False)
     Then exit;

     if (trim(ObjParametroGlobal.Get_PValor)='')
     Then Begin
               mensagemerro('N�o existe nem um fechamento em aberto');
               exit;
     End;

     Try
        pdata:=strtodate(ObjParametroGlobal.Get_PValor);
     Except
           mensagemerro('Data Inv�lida do fechamento no par�metro');
           exit;
     End;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Update tabexportacontabilidade set exportado=''N'',nomedoarquivo=NULL');
          SelectSQL.add('where nomedoarquivo='+#39+Formatdatetime('mm/dd/yyyy',Pdata)+#39+' and Exportado=''S'' ');
          try
            execsql;
            ObjParametroGlobal.Status:=dsedit;
            ObjParametroGlobal.submit_Pvalor('');
            if (ObjParametroGlobal.Salvar(False)=False)
            then begin
                      fdatamodulo.IBTransaction.RollbackRetaining;
                      mensagemerro('N�o foi poss�vel atualizar o par�metro "'+'DATA DO ULTIMO FECHAMENTO DA CONTABILIDADE'+'"');
                      exit;
            end;

            FDataModulo.IBTransaction.CommitRetaining;
            mensagemaviso('Conclu�do');
            exit;
          except
            on e:exception do
            begin
                fdatamodulo.IBTransaction.RollbackRetaining;
                mensagemerro('Erro na tentativa de Alterar os registros da contabilidade'+#13+E.message);
            End;
          End;

     End;

end;

procedure TObjExportaContabilidade.Opcoesfechamento;
begin
     with FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Fechamento da contabilidade');
          RgOpcoes.Items.add('Estorno de Fechamento da contabilidade');
          Showmodal;
          if (Tag=0)
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Self.FechamentoContabilidade;
            1:Self.RetornaFechamentoContabilidade;
          End;
     End;
end;

function TObjExportaContabilidade.ContaPontosClassificacao(
  Pclassificacao: String): Integer;
var
cont:integer;
quant:integer;
//067-3421-3061
begin
     //paulo
     //100 gb

     //6 meses
     quant:=0;
     if (Pos('.',pclassificacao)>0)
     Then Begin
               for cont:=1 to length(pclassificacao) do
               Begin
                    if (pclassificacao[cont]='.')
                    Then Quant:=Quant+1;
               End;
     End;

     if (quant>0)
     Then result:=(Quant*2)
     Else result:=1;
     //mesmo que noa tenha ponto eu retorno 1 porque sera usado como coluna de
     //impressao, e a coluna nao pode ser zero

end;


function TObjExportaContabilidade.Get_NumDocto: string;
begin
     Result:=Self.NumDocto;
end;

procedure TObjExportaContabilidade.Submit_NumDocto(Parametro: string);
begin
     Self.NumDocto:=parametro;
end;

procedure TObjExportaContabilidade.ConfrontoGerador(
  PDatabaseBackup: Tibdatabase; PGrid: TStringGrid);
var
PQuerybackup,PqueryLocal:TibQuery;
pobjetogerador,pdatainicial,pdatafinal:string;
cont:integer;
diferenca:currency;
begin
     PGrid.ColCount:=1;
     PGrid.RowCount:=2;
     PGrid.Cols[0].clear;
     PGrid.FixedRows:=1;

     if (PDatabaseBackup.TestConnected=False)
     Then Begin
               Mensagemerro('A base de Backup n�o est� conectada');
               exit;
     End;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          Grupo03.Enabled:=true;
          LbGrupo03.Caption:='OBJGERADOR';
          edtgrupo03.EditMask:='';

          showmodal;

          if (tag=0)
          Then exit;

          pdatainicial:='';
          pdatafinal:='';

          if (Validadata(1,pdatainicial,true)=False)
          Then exit;

          if (Validadata(2,pdatafinal,true)=False)
          Then exit;

          PObjetogerador:=edtgrupo03.text;
     End;


     Try
        PQuerybackup:=TIBQuery.create(nil);
        PQuerybackup.Database:=PDatabaseBackup;

        PqueryLocal:=TIBQuery.create(nil);
        PqueryLocal.Database:=FDataModulo.IBDatabase;

     except
           mensagemerro('Erro na tentativa de Criar a query de backup');
           exit;
     end;

     Try
         With PqueryLocal do
         begin
              close;
              SQL.Clear;
              sql.add('Select OBJGERADOR,CODGERADOR,sum(valor) as soma from tabexportacontabilidade');
              sql.add('where codigo<>-500');

              if (pdatainicial<>'')
              Then sql.add('and data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);

              if (pdatafinal<>'')
              Then sql.add('and data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatafinal))+#39);

              if (pobjetogerador<>'')
              then sql.add('and OBJGERADOR='+#39+uppercase(pobjetogerador)+#39);

              sql.add('group by OBJGERADOR,CODGERADOR');
              sql.add('order by OBJGERADOR,codgerador');
              open;
              last;

              if (recordcount=0)
              then Begin
                        mensagemaviso('Nenhuma Informa��o foi selecionada');
                        exit; 
              End;

              PGrid.RowCount:=RecordCount+1;
              PGrid.ColCount:=5;
              PGrid.cols[0].clear;PGrid.cols[1].clear;PGrid.cols[2].clear;PGrid.cols[3].clear;PGrid.cols[4].clear;
              PGrid.Cells[0,0]:='OBJETO';
              PGrid.Cells[1,0]:='CODIGO';
              PGrid.Cells[2,0]:='VALOR ATUAL';
              PGrid.Cells[3,0]:='VALOR BACKUP';
              PGrid.Cells[4,0]:='DIFERENCA';

              FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
              FMostraBarraProgresso.lbmensagem.caption:='Gerando dados...';
              first;

              cont:=1;
              While not(eof) do
              begin
                   FMostraBarraProgresso.IncrementaBarra1(1);
                   FMostraBarraProgresso.show;
                   Application.ProcessMessages;

                   PQuerybackup.Close;
                   PQuerybackup.sql.clear;
                   PQuerybackup.sql.add('select sum(valor) as soma from tabexportacontabilidade');
                   PQuerybackup.sql.add('where OBJGERADOR='+#39+uppercase(PqueryLocal.fieldbyname('objgerador').asstring)+#39);
                   PQuerybackup.sql.add('and codGERADOR='+#39+uppercase(PqueryLocal.fieldbyname('codgerador').asstring)+#39);
                   PQuerybackup.open;

                   Try
                      diferenca:=fieldbyname('soma').asfloat-PQuerybackup.fieldbyname('soma').asfloat;
                   Except
                         diferenca:=0;
                   End;

                   PGrid.Cells[0,cont]:=fieldbyname('objgerador').asstring;
                   PGrid.Cells[1,cont]:=fieldbyname('codgerador').asstring;
                   PGrid.Cells[2,cont]:=formata_valor(fieldbyname('soma').asstring);
                   PGrid.Cells[3,cont]:=formata_valor(PQuerybackup.fieldbyname('soma').asstring);
                   PGrid.Cells[4,cont]:=formata_valor(diferenca);

                   next;
                   inc(cont,1);
              End;
              AjustaLArguraColunaGrid(pgrid);
              close;
              PQuerybackup.Close;
              mensagemaviso('Concluido');
              exit;
         End;
     Finally
            FMostraBarraProgresso.Close;
            freeandnil(PqueryBackup);
            freeandnil(PqueryLocal);
     End;
end;



procedure TObjExportaContabilidade.ConfrontoTABELA(PDatabaseBackup: Tibdatabase; PGrid: TStringGrid);
var
PQuerybackup,PqueryLocal:TibQuery;
pcampodata,ptabela,pobjetogerador,pdatainicial,pdatafinal:string;
cont:integer;
somadiferenca,diferenca:currency;
begin
     PGrid.ColCount:=1;
     PGrid.RowCount:=2;
     PGrid.Cols[0].clear;
     PGrid.FixedRows:=1;

     if (PDatabaseBackup.TestConnected=False)
     Then Begin
               Mensagemerro('A base de Backup n�o est� conectada');
               exit;
     End;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          LbGrupo01.caption:='Data Inicial';
          LbGrupo02.caption:='Data Final';
          edtgrupo01.EditMask:=MascaraData;
          edtgrupo02.EditMask:=MascaraData;
          Grupo03.Enabled:=true;
          LbGrupo03.Caption:='OBJGERADOR';
          edtgrupo03.EditMask:='';

          Grupo04.Enabled:=true;
          LbGrupo04.Caption:='TABELA';
          edtgrupo04.EditMask:='';

          Grupo05.Enabled:=true;
          LbGrupo05.Caption:='CAMPO DATA';
          edtgrupo05.EditMask:='';

          showmodal;

          if (tag=0)
          Then exit;

          pdatainicial:='';
          pdatafinal:='';

          if (Validadata(1,pdatainicial,true)=False)
          Then exit;

          if (Validadata(2,pdatafinal,true)=False)
          Then exit;

          PObjetogerador:=edtgrupo03.text;
          Ptabela:=edtgrupo04.text;
          Pcampodata:=edtgrupo05.text;

     End;


     Try
        PQuerybackup:=TIBQuery.create(nil);
        PQuerybackup.Database:=PDatabaseBackup;

        PqueryLocal:=TIBQuery.create(nil);
        PqueryLocal.Database:=FDataModulo.IBDatabase;

     except
           mensagemerro('Erro na tentativa de Criar a query de backup');
           exit;
     end;

     Try
         With PqueryLocal do
         begin
              close;
              SQL.Clear;

              sql.add('Select OBJGERADOR,CODGERADOR,sum(valor) as soma from tabexportacontabilidade');
              sql.add('where codigo<>-500');

              if (pobjetogerador<>'')
              then sql.add('and OBJGERADOR='+#39+uppercase(pobjetogerador)+#39);

              sql.add('and CODGERADOR in');
              //selecionando da tabela escolhida
              
              sql.add('(Select codigo from '+ptabela+' where codigo<>-500');
              if (pdatainicial<>'')
              Then sql.add('and '+pcampodata+'>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatainicial))+#39);
              if (pdatafinal<>'')
              Then sql.add('and '+pcampodata+'<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdatafinal))+#39);
              sql.add(')');

              sql.add('group by OBJGERADOR,CODGERADOR');
              sql.add('order by OBJGERADOR,codgerador');
              open;
              last;

              if (recordcount=0)
              then Begin
                        mensagemaviso('Nenhuma Informa��o foi selecionada');
                        exit; 
              End;

              PGrid.RowCount:=RecordCount+2;//1 para titulo e 1 para linha da soma da diferenca
              PGrid.ColCount:=5;
              PGrid.cols[0].clear;PGrid.cols[1].clear;PGrid.cols[2].clear;PGrid.cols[3].clear;PGrid.cols[4].clear;
              PGrid.Cells[0,0]:='OBJETO';
              PGrid.Cells[1,0]:='CODIGO';
              PGrid.Cells[2,0]:='VALOR ATUAL';
              PGrid.Cells[3,0]:='VALOR BACKUP';
              PGrid.Cells[4,0]:='DIFERENCA';

              FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
              FMostraBarraProgresso.lbmensagem.caption:='Gerando dados...';
              first;

              cont:=1;
              somadiferenca:=0;
              While not(eof) do
              begin
                   FMostraBarraProgresso.IncrementaBarra1(1);
                   FMostraBarraProgresso.show;
                   Application.ProcessMessages;

                   PQuerybackup.Close;
                   PQuerybackup.sql.clear;
                   PQuerybackup.sql.add('select sum(valor) as soma from tabexportacontabilidade');
                   PQuerybackup.sql.add('where OBJGERADOR='+#39+uppercase(PqueryLocal.fieldbyname('objgerador').asstring)+#39);
                   PQuerybackup.sql.add('and codGERADOR='+#39+uppercase(PqueryLocal.fieldbyname('codgerador').asstring)+#39);
                   PQuerybackup.open;

                   Try
                      diferenca:=fieldbyname('soma').asfloat-PQuerybackup.fieldbyname('soma').asfloat;
                   Except
                         diferenca:=0;
                   End;

                   PGrid.Cells[0,cont]:=fieldbyname('objgerador').asstring;
                   PGrid.Cells[1,cont]:=fieldbyname('codgerador').asstring;
                   PGrid.Cells[2,cont]:=formata_valor(fieldbyname('soma').asstring);
                   PGrid.Cells[3,cont]:=formata_valor(PQuerybackup.fieldbyname('soma').asstring);
                   PGrid.Cells[4,cont]:=formata_valor(diferenca);

                   somadiferenca:=somadiferenca+diferenca;
                   next;
                   inc(cont,1);
              End;

              PGrid.Cells[0,cont]:='SOMA DA DIFERENCA';
              PGrid.Cells[1,cont]:='';
              PGrid.Cells[2,cont]:='';
              PGrid.Cells[3,cont]:='';
              PGrid.Cells[4,cont]:=formata_valor(somadiferenca);


              AjustaLArguraColunaGrid(pgrid);
              close;
              PQuerybackup.Close;
              mensagemaviso('Concluido');
              exit;
         End;
     Finally
            FMostraBarraProgresso.Close;
            freeandnil(PqueryBackup);
            freeandnil(PqueryLocal);
     End;
end;

Function TobjExportaContabilidade.RetornacamposExportacao:string;
var
cont:integer;
temp:string;
Begin
     result:='';
     temp:='';

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select * from ViewExportaContabilidade where codigo=-100');
          open;

          for cont:=0 to Fields.Count-1 do
          Begin
               if (Fields[cont].DisplayName<>'DATAC')and
                  (Fields[cont].DisplayName<>'USERC')and
                  (Fields[cont].DisplayName<>'DATAM')and
                  (Fields[cont].DisplayName<>'USERM')
               Then Begin
                         if (temp<>'')
                         Then temp:=temp+';'+Fields[cont].DisplayName
                         Else temp:=Fields[cont].DisplayName;
               End;
          End;
          close;
          result:=temp;
     End;
End;




end.
