unit UConfiguraComprovanteCheque;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, Buttons,UobjConfRelatorio,QRctrls;

type
  TFconfiguraComprovanteCheque = class(TForm)
    Panel: TPanel;
    edtleft: TMaskEdit;
    edttop: TMaskEdit;
    LBLEFT: TLabel;
    LBTOP: TLabel;
    Btsalvar: TBitBtn;
    LbDescricao: TLabel;
    LbComprovante: TLabel;
    LbProvidencias: TLabel;
    LbNomeRecebedor: TLabel;
    LbCPFCNPJRecebedor: TLabel;
    LbValor: TLabel;
    LBExtenso1: TLabel;
    LBExtenso2: TLabel;
    LbNominal: TLabel;
    LbCidadeCheque: TLabel;
    LbDiaCheque: TLabel;
    LbMesCheque: TLabel;
    LbAnoCheque: TLabel;
    LbNumeroCheque: TLabel;
    LbBAnco: TLabel;
    LBAgencia: TLabel;
    LbConta: TLabel;
    LbValorComprovante: TLabel;
    LbExtensoComprovante: TLabel;
    LbDescricao2: TLabel;
    LbCidadeComprovante: TLabel;
    lbDiaComprovante: TLabel;
    LbMesComprovante: TLabel;
    LbAnoComprovante: TLabel;
    btimprime: TBitBtn;
    procedure LBpagoAClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure BtsalvarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtleftExit(Sender: TObject);
    procedure btimprimeClick(Sender: TObject);
  private
    { Private declarations }
    Procedure PassaParaAzul;
  public
    { Public declarations }
  end;

var
  FconfiguraComprovanteCheque: TFconfiguraComprovanteCheque;
  ObjConfiguraRel:TObjConfRelatorio;
implementation

uses UessencialGlobal,URelComprovanteCheque, UobjParametros;



{$R *.DFM}

procedure TFconfiguraComprovanteCheque.LBpagoAClick(Sender: TObject);

begin
     edtleft.text:=inttostr(TLabel(Sender).left);
     edttop.text:=inttostr(TLabel(Sender).top);
     Panel.caption:=Tlabel(Sender).Name;
     Self.PassaParaAzul;
     TLabel(Sender).Font.Color:=clred;
     edtleft.SetFocus;
     
end;

procedure TFconfiguraComprovanteCheque.FormActivate(Sender: TObject);
VAR
INT_HABILITA:INTEGER;
begin
     Panel.caption:='';
     edtleft.text:='0';
     edttop.text:='0';
     Self.PassaParaAzul;
     PegaFiguraBotao(BtSalvar,'BOTAOGRAVAR.BMP');
     PegaFiguraBotao(btimprime,'BOTAORELATORIOS.BMP');

     Try
        ObjConfiguraRel:=TObjConfRelatorio.create;
     Except
           Messagedlg('N�o � poss�vel Configurar a Impress�o Devido a um Problema na Cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0);
           exit;
     End;
     //resgatando as configura��es atuais do quickreport

   ObjconfiguraRel.RecuperaArquivoRelatorio(FrelComprovanteCheque,'IMPRIMECOMPROVANTE');

   for int_habilita:=0 to Frelcomprovantecheque.ComponentCount -1 do
   Begin
        if Frelcomprovantecheque.Components [int_habilita].ClassName = 'TQRLabel'
        then Begin
                Tlabel(Self.FindComponent(Frelcomprovantecheque.Components [int_habilita].Name)).Left:=TQrlabel(Frelcomprovantecheque.Components [int_habilita]).left;
                Tlabel(Self.FindComponent(Frelcomprovantecheque.Components [int_habilita].Name)).Top:=TQrlabel(Frelcomprovantecheque.Components [int_habilita]).Top;
             End;
   End;

end;

procedure TFconfiguraComprovanteCheque.edtleftKeyPress(Sender: TObject;
  var Key: Char);
begin

     If not(key in ['0'..'9',#8,#13])
     Then key:=#0;

     If Key=#13
     Then Begin
               Tedit(Sender).setfocus;
               edtleft.OnExit(sender);
          End;
end;

procedure TFconfiguraComprovanteCheque.PassaParaAzul;
var
cont:Integer;
begin
     for cont:=0 to Self.ComponentCount -1 do
     Begin
        if (Uppercase(Self.Components [cont].ClassName)= 'TLABEL')
        then TLabel(Self.Components [cont]).font.color:=clblue;
     End;

end;

procedure TFconfiguraComprovanteCheque.BtsalvarClick(Sender: TObject);
var
int_habilita:Integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1 do
   Begin
        if ((Self.Components [int_habilita].ClassName = 'TLabel')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBLEFT')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBTOP'))
        Then Begin
                TQRlabel(Frelcomprovantecheque.FindComponent(Self.Components [int_habilita].Name)).Left:=Tlabel(Self.Components [int_habilita]).left;
                TQRlabel(Frelcomprovantecheque.FindComponent(Self.Components [int_habilita].Name)).Top:=TLabel(Self.Components [int_habilita]).Top;
             End;
   End;

     //GRAVANDO OS DADOS ATUAIS NO QUICKREPORT

     IF (ObjconfiguraRel.NovoArquivoRelatorio(FrelComprovanteCheque,'IMPRIMECOMPROVANTE')=False)
     Then Begin
               Messagedlg('N�o foi poss�vel gravar o arquivo de configura��o!',mterror,[mbok],0);
               exit;
          End;
     Messagedlg('Arquivo Gravado com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFconfiguraComprovanteCheque.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (ObjConfiguraRel<>nil)
     Then Freeandnil(ObjConfiguraRel);
end;

procedure TFconfiguraComprovanteCheque.edtleftExit(Sender: TObject);
var
numerico:INteger;

begin

               Try
                  numerico:=Strtoint(edtleft.text);
                  numerico:=Strtoint(edttop.text);
               Except
                     exit;
               End;
               
               If (panel.caption<>'')
               Then Begin
                        TLabel(Self.FindComponent(Panel.caption)).left:=Strtoint(Edtleft.text);
                        TLabel(Self.FindComponent(Panel.caption)).Top:=Strtoint(EdtTop.text);
                    End;


end;

procedure TFconfiguraComprovanteCheque.btimprimeClick(Sender: TObject);
var
Objparametros:TobjParametros;
ValorExtensoCheque,extenso1,extenso2:String;
quantcaractereschequelinha:Integer;
Tmpano,TmpMes,TmpDia:word;
begin

     Try
        Objparametros:=TObjParametros.create;
     Except
           Messagedlg('Erro na cria��o do Objeto de Parametros!',mterror,[mbok],0);
           exit;
     End;
     Try

       If (Objparametros.LocalizaNome('QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;

       Objparametros.TabelaparaObjeto;

       Try
          quantcaractereschequelinha:=StrToInt(Objparametros.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres no valor extenso do Cheque!',mterror,[mbok],0);
             exit;
       End;

       ObjConfiguraRel.RecuperaArquivoRelatorio(FrelcomprovanteCheque,'IMPRIMECOMPROVANTE');
       FrelComprovanteCheque.LbValor.caption:='R$ '+formatfloat('###,##0.00',777777.77);
       FrelComprovanteCheque.LbValorComprovante.caption:='R$ '+formatfloat('###,##0.00',777777.77);
       //**************************
       ValorExtensoCheque:='';
       extenso1:='';
       extenso2:='';
       ValorExtensoCheque:=valorextenso(777777.77);
       DividirValor(ValorExtensoCheque,quantcaractereschequelinha,200,extenso1,extenso2);
       //**************************
       FrelComprovanteCheque.LbExtenso1.caption:=extenso1;
       FrelComprovanteCheque.LbExtenso2.caption:=extenso2;
       FrelComprovanteCheque.LbExtensoComprovante.caption:=ValorExtensoCheque;
       FrelComprovanteCheque.LbNominal.caption:='NOMINAL';
       FrelComprovanteCheque.LbCidadeCheque.caption:='DOURADOS';
       FrelComprovanteCheque.LbCidadeComprovante.caption:='DOURADOS';
       //****************************************
       DecodeDate(NOW,Tmpano,TmpMes,TmpDia);
       FrelComprovanteCheque.LbDiaCheque.caption:=Inttostr(TmpDia);
       FrelComprovanteCheque.LbDiaComprovante.caption:=Inttostr(TmpDia);
       FrelComprovanteCheque.LbMesCheque.caption:=Inttostr(TmpMes);
       FrelComprovanteCheque.LbMesComprovante.caption:=Inttostr(TmpMes);
       FrelComprovanteCheque.LbAnoCheque.caption:=Inttostr(TmpAno);
       FrelComprovanteCheque.LbAnoComprovante.caption:=Inttostr(TmpAno);
       //*******************************************
       FrelComprovanteCheque.memoDescricaocomprovante.LINES.TEXT:='01234567890x01234567890x01234567890x01234567890x0123456789X123456789X123456789X123465798X123456789X123456789X123456789X123456789X123456789X123456789X123456789X123456789X';
       FrelComprovanteCheque.LbDescricaoRecibo.caption:='DESCRI��O';
       //Dados do Cheque
       FrelComprovanteCheque.LbAgencia.caption:='AGE';
       FrelComprovanteCheque.LbBanco.caption:='BANC';
       FrelComprovanteCheque.LbNumeroCheque.caption:='NUMCHEQ';
       FrelComprovanteCheque.LbConta.caption:='CONTA';
       //*******
       FrelComprovanteCheque.LbProvidencias.caption:='CONTABILIZAR E ARQUIVAR';
       FrelComprovanteCheque.LbNomeRecebedor.caption:='NOME RECEBEDOR';
       FrelComprovanteCheque.LbCPFCNPJRecebedor.caption:='CPF RECEBEDOR';
       FrelComprovanteCheque.qr.preview;
     Finally
            Objparametros.free;
     End;

end;

end.
