unit UPrazoPagamento;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjPrazoPagamento;

type
  TFprazoPagamento = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    edtparcelas: TEdit;
    edtformula: TEdit;
    ComboImprimeDuplicatas: TComboBox;
    combocarteira: TComboBox;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    BtCancelar: TBitBtn;
    btsair: TBitBtn;
    Label7: TLabel;
    EdtCreditoDebitto: TEdit;
    chkFimdoMes: TCheckBox;
    lb1: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtparcelasKeyPress(Sender: TObject; var Key: Char);
    procedure edtformulaKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure EdtCreditoDebittoKeyPress(Sender: TObject; var Key: Char);
  private
    Function ControlesParaObjeto:Boolean;
    Function ObjetoParaControles:Boolean;
    Function TabelaParaControles:Boolean;
    Procedure PreparaAlteracao;
    function atualizaQuantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FprazoPagamento: TFprazoPagamento;
  ObjPrazoPagamento:TObjPrazoPagamento;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, DateUtils;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFprazoPagamento.ControlesParaObjeto: Boolean;
var
  prazo,prazo2,dataatual,dataatual2:string;
  diferenca:integer;
Begin
  Try
    With ObjPrazoPagamento do
    Begin
         Submit_CODIGO  (edtCODIGO.text);
         Submit_Nome    (edtNome.text);
         Submit_Parcelas(edtParcelas.text);
         if(chkFimdoMes.Checked=true)then
         begin
           //prazo:= DateToStr(EndOfTheMonth(StrToDate('06/01/2011')));
           //prazo2:=prazo[1]+prazo[2];
           //dataatual:=formatdatetime ('dd/mm/yyyy',now);
           //dataatual2:=dataatual[1]+dataatual[2];
           //diferenca := StrToInt(prazo2) - StrToInt(dataatual2) ;
           //ShowMessage(prazo2);
           //ShowMessage(dataatual2);
           //ShowMessage(IntToStr(diferenca));
           edtFormula.text := '-5;';
         end;

         Submit_Formula (edtFormula.text);
         Submit_ImprimeDuplicatas(Copy(ComboImprimeDuplicatas.text, 0 , 1));
         Submit_Carteira(Copy(combocarteira.Text, 0 , 1));
         submit_Credito_Debito(EdtCreditoDebitto.Text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFprazoPagamento.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjPrazoPagamento do
     Begin
        edtCODIGO.text  :=Get_CODIGO         ;
        edtNome.text    :=Get_Nome           ;
        edtParcelas.text:=Get_Parcelas       ;
        edtFormula.text :=Get_Formula        ;
        EdtCreditoDebitto.Text:=get_CREDITO_DEBITO ;
        if (get_ImprimeDuplicatas='S')
        Then ComboImprimeDuplicatas.itemindex:=1
        Else ComboImprimeDuplicatas.itemindex:=0;

        if (Get_Carteira='S')
        Then combocarteira.itemindex:=1
        Else combocarteira.itemindex:=0;
        
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFprazoPagamento.TabelaParaControles: Boolean;
begin
     ObjPrazoPagamento.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFprazoPagamento.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjPrazoPagamento=Nil)
     Then exit;

     If (ObjPrazoPagamento.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

     ObjPrazoPagamento.free;

end;

procedure TFprazoPagamento.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFprazoPagamento.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
      end;
end;


procedure TFprazoPagamento.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:=ObjPrazoPagamento.Get_NovoCodigo;
     edtcodigo.enabled:=False;
     Btgravar.visible:=True;
     BtCancelar.visible:=True;

     ObjPrazoPagamento.status:=dsInsert;
     Edtnome.setfocus;
end;

procedure TFprazoPagamento.BtCancelarClick(Sender: TObject);
begin
     ObjPrazoPagamento.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFprazoPagamento.BtgravarClick(Sender: TObject);
begin

     If ObjPrazoPagamento.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjPrazoPagamento.salvar=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFprazoPagamento.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     EdtCodigo.enabled:=False;
     ObjPrazoPagamento.Status:=dsEdit;
     edtNome.setfocus;
end;

procedure TFprazoPagamento.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjPrazoPagamento.Get_pesquisa,ObjPrazoPagamento.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjPrazoPagamento.status<>dsinactive
                                  then exit;

                                  If (ObjPrazoPagamento.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFprazoPagamento.btalterarClick(Sender: TObject);
begin
    If (ObjPrazoPagamento.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFprazoPagamento.btexcluirClick(Sender: TObject);
begin
     If (ObjPrazoPagamento.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjPrazoPagamento.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjPrazoPagamento.exclui(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFprazoPagamento.edtparcelasKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in ['0'..'9',#8,'-'])
     Then key:=#0;
end;

procedure TFprazoPagamento.edtformulaKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not (key in ['0'..'9',#8,';'])
     Then key:=#0;
end;

procedure TFprazoPagamento.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjPrazoPagamento:=TObjPrazoPagamento.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;

     Uessencialglobal.PegaCorForm(Self);

     if (Self.Tag <> 0)
     then begin

        if (ObjPrazoPagamento.LocalizaCodigo( IntToStr(tag)))
        then begin
            ObjPrazoPagamento.TabelaparaObjeto;
            Self.ObjetoParaControles;
        end;

     end;
     Self.Tag:=0;
     
end;

function TFprazoPagamento.atualizaQuantidade: string;
begin
     result:='Existem '+ContaRegistros('tabprazopagamento','codigo')+' Prazos de pagamento Cadastrados';
end;

procedure TFprazoPagamento.EdtCreditoDebittoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in ['0'..'9',#8,'-',','])
     Then key:=#0;
end;

end.
