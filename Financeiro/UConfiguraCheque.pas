unit UConfiguraCheque;

interface

uses
  URelCheque, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, Buttons,UobjConfRelatorio,QRctrls, UobjPortador;

type
  TFConfiguraCheque = class(TForm)
    LbValor: TLabel;
    LBpagoA: TLabel;
    LBDataAtual: TLabel;
    LBExtenso1: TLabel;
    LBExtenso2: TLabel;
    LbNominal: TLabel;
    LbCidadeCheque: TLabel;
    Panel: TPanel;
    edtleft: TMaskEdit;
    edttop: TMaskEdit;
    LBLEFT: TLabel;
    LBTOP: TLabel;
    Btsalvar: TBitBtn;
    LbDiaCheque: TLabel;
    LbMesCheque: TLabel;
    LbAnoCheque: TLabel;
    btimprime: TBitBtn;
    LbValorCanhoto: TLabel;
    procedure LBpagoAClick(Sender: TObject);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure BtsalvarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btimprimeClick(Sender: TObject);
    procedure edtleftExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    Procedure PassaParaAzul;
  public
    { Public declarations }
  end;

var
  FConfiguraCheque: TFConfiguraCheque;
  ObjConfiguraRel:TObjConfRelatorio;
  ObjPortador:TObjPortador;
  FrelChequeX:TFrelCheque;
implementation

uses UessencialGlobal, UDataModulo;



{$R *.DFM}

procedure TFConfiguraCheque.LBpagoAClick(Sender: TObject);

begin
     edtleft.text:=inttostr(TLabel(Sender).left);
     edttop.text:=inttostr(TLabel(Sender).top);
     Panel.caption:=Tlabel(Sender).Name;
     Self.PassaParaAzul;
     TLabel(Sender).Font.Color:=clred;
     edtleft.SetFocus;
     
end;

procedure TFConfiguraCheque.edtleftKeyPress(Sender: TObject;
  var Key: Char);
begin

     If not(key in ['0'..'9',#8,#13])
     Then key:=#0;

     If Key=#13
     Then Begin
              edtleft.OnExit(sender);
          End;
end;

procedure TFConfiguraCheque.PassaParaAzul;
var
cont:Integer;
begin
     for cont:=0 to Self.ComponentCount -1 do
     Begin
        if (Uppercase(Self.Components [cont].ClassName)= 'TLABEL')
        then TLabel(Self.Components [cont]).font.color:=clblue;
     End;

end;

procedure TFConfiguraCheque.BtsalvarClick(Sender: TObject);
var
int_habilita:Integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1 do
   Begin
        if ((Self.Components [int_habilita].ClassName = 'TLabel')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBLEFT')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBTOP'))
        Then Begin
                TQRlabel(FrelchequeX.FindComponent(Self.Components [int_habilita].Name)).Left:=Tlabel(Self.Components [int_habilita]).left;
                TQRlabel(FrelchequeX.FindComponent(Self.Components [int_habilita].Name)).Top:=TLabel(Self.Components [int_habilita]).Top;
             End;
   End;

     //GRAVANDO OS DADOS ATUAIS NO QUICKREPORT

     IF (ObjconfiguraRel.NovoArquivoRelatorio(FrelchequeX,'IMPRIMECHEQUE'+objportador.Get_CODIGO)=False)
     Then Begin
               Messagedlg('N�o foi poss�vel gravar o arquivo de configura��o!',mterror,[mbok],0);
               exit;
          End;
     Messagedlg('Arquivo Gravado com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFConfiguraCheque.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (ObjConfiguraRel<>nil)
     Then ObjConfiguraRel.Free;

     If (Objportador<>nil)
     Then objportador.Free;

     freeandnil(FrelChequeX);
     tag:=0;

end;

procedure TFConfiguraCheque.btimprimeClick(Sender: TObject);
var
TempObjConfRelatorio:tobjconfrelatorio;
ValorExtensoCheque,extenso1,extenso2:String;
quantcaractereschequelinha:Integer;
Tmpano,TmpMes,TmpDia:word;

Begin
Try

       If (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;
       

       Try
          quantcaractereschequelinha:=StrToInt(ObjParametroGlobal.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres no valor extenso do Cheque!',mterror,[mbok],0);
             exit;
       End;


       //Imprimindo o Cheque e o Comprovante
       TempObjConfRelatorio.RecuperaArquivoRelatorio(FrelChequeX,'IMPRIMECHEQUE'+objportador.get_codigo);

       FrelChequeX.LbPagoA.caption:='PAGO A';
       FrelChequeX.LbDataAtual.caption:='DATACANH';
       
       FrelChequeX.LbValor.caption:='R$ '+formatfloat('###,##0.00',strtofloat('777777,88'));
       //****pegando o valor por extenso e dividindo nas duas labels***//
       ValorExtensoCheque:='';
       extenso1:='';
       extenso2:='';
       ValorExtensoCheque:=valorextenso(777777.88);
       DividirValor(ValorExtensoCheque,quantcaractereschequelinha,200,extenso1,extenso2);


       FrelChequeX.LbExtenso1.caption:=extenso1;
       FrelChequeX.LbExtenso2.caption:=extenso2;
       //*****************************************************************
       FrelChequeX.LbNominal.caption:='CAMPO NOMINAL';
       FrelChequeX.LbCidadeCheque.caption:='DOURADOS';
       DecodeDate(NOW,Tmpano,TmpMes,TmpDia);
       FrelChequeX.LbDiaCheque.caption:=Inttostr(TmpDia);
       FrelChequeX.LbMesCheque.caption:=Inttostr(TmpMes);
       FrelChequeX.LbAnoCheque.caption:=Inttostr(TmpAno);
       FrelChequeX.Qr.preview;
Finally

End;

end;

procedure TFConfiguraCheque.edtleftExit(Sender: TObject);
var
numerico:integer;
begin
     Try
        numerico:=Strtoint(edtleft.text);
        numerico:=Strtoint(edttop.text);
     Except
           exit;
     End;

     If (panel.caption<>'')
     Then Begin
              TLabel(Self.FindComponent(Panel.caption)).left:=Strtoint(Edtleft.text);
              TLabel(Self.FindComponent(Panel.caption)).Top:=Strtoint(EdtTop.text);
     End;
end;

procedure TFConfiguraCheque.FormShow(Sender: TObject);
VAR
INT_HABILITA:INTEGER;
POrtadorTemp:INteger;
PortadorTempStr:String;
saida:boolean;
begin
     Uessencialglobal.PegaCorForm(Self);

     If (Tag=1)
     Then exit;
     Panel.caption:='';
     edtleft.text:='0';
     edttop.text:='0';
     Self.PassaParaAzul;
     PegaFiguraBotao(BtSalvar,'BOTAOGRAVAR.BMP');
     PegaFiguraBotao(btImprime,'BOTAORELATORIOS.BMP');

     Try
        FrelChequeX:=TFrelcheque.create(nil);
        ObjPortador:=TObjPortador.create;
     Except
           Messagedlg('Erro na tentativa de cria��o do objeto Portador!',mterror,[mbok],0);
           exit;
     End;

     Repeat
        saida:=InputQuery('Escolha de Portador','Digite o Portador',PortadorTempStr);
        If (saida=False)
        Then BEgin
                  Btsalvar.Enabled:=False;
                  btimprime.Enabled:=False;
                  exit;
        End
        Else Begin
                  Try
                     POrtadorTemp:=StrToInt(PortadorTempStr);
                     If (ObjPortador.LocalizaCodigo(PortadorTempStr)=False)
                     Then portadortemp:=strtoint('a');
                     Btsalvar.Enabled:=True;
                     btimprime.Enabled:=True;
                     ObjPortador.TabelaparaObjeto;
                  Except
                        saida:=false;
                  End;


        End;
     Until(saida=True);



     Try
        ObjConfiguraRel:=TObjConfRelatorio.create;
     Except
           Messagedlg('N�o � poss�vel Configurar a Impress�o Devido a um Problema na Cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0);
           exit;
     End;

     //resgatando as configura��es atuais do quickreport

   ObjconfiguraRel.RecuperaArquivoRelatorio(FrelChequeX,'IMPRIMECHEQUE'+ObjPortador.get_codigo);

   for int_habilita:=0 to FrelChequeX.ComponentCount -1 do
   Begin
        if FrelChequeX.Components [int_habilita].ClassName = 'TQRLabel'
        then Begin
                Tlabel(Self.FindComponent(FrelChequeX.Components [int_habilita].Name)).Left:=TQrlabel(FrelChequeX.Components [int_habilita]).left;
                Tlabel(Self.FindComponent(FrelChequeX.Components [int_habilita].Name)).Top:=TQrlabel(FrelChequeX.Components [int_habilita]).Top;
             End;
   End;
   tag:=1;
end;

end.
