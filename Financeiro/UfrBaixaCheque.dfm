object FrBaixaCheque: TFrBaixaCheque
  Left = 0
  Top = 0
  Width = 497
  Height = 108
  Ctl3D = False
  ParentCtl3D = False
  TabOrder = 0
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 497
    Height = 105
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 11
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Lbportador: TLabel
      Left = 215
      Top = 10
      Width = 60
      Height = 15
      Caption = 'Lbportador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 46
      Width = 81
      Height = 14
      Caption = 'Data do Extrato'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 77
      Width = 119
      Height = 14
      Caption = 'Cheque a ser Baixado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtportador: TEdit
      Left = 134
      Top = 8
      Width = 73
      Height = 19
      Color = clSkyBlue
      TabOrder = 0
      OnExit = edtportadorExit
      OnKeyDown = edtportadorKeyDown
      OnKeyPress = edtportadorKeyPress
    end
    object edtdata: TMaskEdit
      Left = 134
      Top = 43
      Width = 73
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object edtcheque: TEdit
      Left = 134
      Top = 73
      Width = 73
      Height = 19
      Color = clSkyBlue
      TabOrder = 2
      OnKeyDown = edtchequeKeyDown
      OnKeyPress = edtchequeKeyPress
    end
    object Btbaixa: TBitBtn
      Left = 235
      Top = 39
      Width = 95
      Height = 50
      Caption = '&Baixar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BtbaixaClick
    end
    object btestornabaixa: TBitBtn
      Left = 371
      Top = 39
      Width = 95
      Height = 50
      Caption = '&Extornar Baixa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btestornabaixaClick
    end
  end
end
