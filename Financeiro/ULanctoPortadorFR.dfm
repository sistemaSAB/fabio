object FrLanctoPortador: TFrLanctoPortador
  Left = 0
  Top = 0
  Width = 647
  Height = 280
  Ctl3D = False
  ParentCtl3D = False
  TabOrder = 0
  object Guia: TPageControl
    Left = 0
    Top = 50
    Width = 647
    Height = 230
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      object Label1: TLabel
        Left = 0
        Top = 5
        Width = 39
        Height = 14
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 0
        Top = 29
        Width = 112
        Height = 14
        Caption = 'Tipo de Lan'#231'amento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 0
        Top = 53
        Width = 48
        Height = 14
        Caption = 'Portador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 0
        Top = 77
        Width = 49
        Height = 14
        Caption = 'Hist'#243'rico'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 0
        Top = 101
        Width = 28
        Height = 14
        Caption = 'Valor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 200
        Top = 98
        Width = 23
        Height = 14
        Caption = 'Data'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbTipoLanctoNome: TLabel
        Left = 200
        Top = 29
        Width = 88
        Height = 14
        Caption = 'lblancamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbportador: TLabel
        Left = 200
        Top = 51
        Width = 59
        Height = 15
        Caption = 'lbportador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 120
        Top = 3
        Width = 70
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object edttipolancto: TEdit
        Left = 120
        Top = 26
        Width = 70
        Height = 19
        Color = 6073854
        MaxLength = 9
        TabOrder = 1
        OnExit = edttipolanctoExit
        OnKeyDown = edttipolanctoKeyDown
        OnKeyPress = edttipolanctoKeyPress
      end
      object edtportador: TEdit
        Left = 120
        Top = 49
        Width = 70
        Height = 19
        Color = 6073854
        MaxLength = 9
        TabOrder = 2
        OnExit = edtportadorExit
        OnKeyDown = edtportadorKeyDown
        OnKeyPress = edttipolanctoKeyPress
      end
      object edthistorico: TEdit
        Left = 200
        Top = 72
        Width = 329
        Height = 19
        MaxLength = 150
        TabOrder = 4
      end
      object edtvalor: TEdit
        Left = 120
        Top = 95
        Width = 70
        Height = 19
        MaxLength = 9
        TabOrder = 5
        OnKeyPress = edtvalorKeyPress
      end
      object edtdata: TMaskEdit
        Left = 272
        Top = 95
        Width = 81
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 6
        Text = '  /  /    '
      end
      object edtcodigohistoricosimples: TEdit
        Left = 120
        Top = 72
        Width = 70
        Height = 19
        Color = 6073854
        MaxLength = 9
        TabOrder = 3
        OnExit = edtcodigohistoricosimplesExit
        OnKeyDown = edtcodigohistoricosimplesKeyDown
        OnKeyPress = edtcodigohistoricosimplesKeyPress
      end
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 647
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    object lbnomeformulario: TLabel
      Left = 409
      Top = 0
      Width = 238
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Lan'#231'amentos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -32
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BtNovo: TBitBtn
      Left = 1
      Top = -3
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object Btgravar: TBitBtn
      Left = 51
      Top = -3
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BtgravarClick
    end
    object btpesquisar: TBitBtn
      Left = 151
      Top = -3
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btpesquisarClick
    end
    object BtCancelar: TBitBtn
      Left = 101
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -3
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btrecibo: TBitBtn
      Left = 251
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btreciboClick
    end
  end
end
