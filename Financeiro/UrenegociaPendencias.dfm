object FrenegociaPendencias: TFrenegociaPendencias
  Left = 125
  Top = 154
  Width = 696
  Height = 521
  Caption = 'Renegocia'#231#227'o de Pend'#234'ncias por T'#237'tulo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StrgPendencias: TStringGrid
    Left = 0
    Top = 94
    Width = 663
    Height = 399
    Align = alBottom
    TabOrder = 1
    OnDblClick = StrgPendenciasDblClick
    OnKeyPress = StrgPendenciasKeyPress
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 663
    Height = 94
    Align = alTop
    BevelOuter = bvNone
    Color = 3355443
    TabOrder = 0
    object Label2: TLabel
      Left = 9
      Top = 7
      Width = 66
      Height = 14
      Caption = 'Vencimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 86
      Top = 25
      Width = 12
      Height = 14
      Caption = ' a '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 192
      Top = 7
      Width = 88
      Height = 14
      Caption = 'Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 346
      Top = 7
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbNomeCredorDevedor: TLabel
      Left = 386
      Top = 27
      Width = 99
      Height = 14
      Caption = 'LBCredorDevedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbtotalSelecionado: TLabel
      Left = 64
      Top = 56
      Width = 126
      Height = 14
      Caption = 'LbtotalSelecionado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 56
      Width = 56
      Height = 14
      Caption = 'Soma R$ '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
    end
    object edtvencimento1: TMaskEdit
      Left = 8
      Top = 23
      Width = 69
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 0
      Text = '  /  /    '
    end
    object edtvencimento2: TMaskEdit
      Left = 104
      Top = 23
      Width = 69
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 1
      Text = '  /  /    '
    end
    object combocredordevedor: TComboBox
      Left = 194
      Top = 23
      Width = 143
      Height = 21
      BevelKind = bkSoft
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 2
      Items.Strings = (
        'Clientes'
        'Fornecedores'
        'Alunos')
    end
    object combocredordevedorcodigo: TComboBox
      Left = 269
      Top = 23
      Width = 69
      Height = 21
      BevelKind = bkSoft
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      TabOrder = 3
      Visible = False
    end
    object edtcodigocredordevedor: TEdit
      Left = 342
      Top = 23
      Width = 39
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 4
      OnExit = edtcodigocredordevedorExit
      OnKeyDown = edtcodigocredordevedorKeyDown
    end
    object Btpesquisar: TBitBtn
      Left = 419
      Top = 52
      Width = 98
      Height = 37
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = BtpesquisarClick
    end
    object BTRenegocia: TBitBtn
      Left = 519
      Top = 52
      Width = 162
      Height = 37
      Caption = '&Renegociar Selecionadas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = BTRenegociaClick
    end
  end
end
