unit UConfiguraChequeRDPRINT;

interface

uses
  URelCheque, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons,UobjConfRelatorio,UobjPortador,
  RDprint,inifiles, Menus;

type
  TFConfiguraChequeRDPRINT = class(TForm)
    LbValor: TLabel;
    LBpagoA: TLabel;
    LBDataAtual: TLabel;
    LBExtenso1: TLabel;
    LBExtenso2: TLabel;
    LbNominal: TLabel;
    LbCidadeCheque: TLabel;
    LbDiaCheque: TLabel;
    LbMesCheque: TLabel;
    LbAnoCheque: TLabel;
    LbValorCanhoto: TLabel;
    ComponenteRdPrint: TRDprint;
    MainMenu1: TMainMenu;
    Opes1: TMenuItem;
    IMPRIMEFOLHADEGUIA1: TMenuItem;
    SalvaPosies1: TMenuItem;
    ImprimeTeste1: TMenuItem;
    PanelPOsicoes: TPanel;
    LbColuna: TLabel;
    lblinha: TLabel;
    LbQtdCaracteres: TLabel;
    edtleft: TEdit;
    edttop: TEdit;
    edtquantidade: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SalvaPosies1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure edttopKeyPress(Sender: TObject; var Key: Char);
    procedure edtquantidadeKeyPress(Sender: TObject; var Key: Char);
    procedure LBpagoAClick(Sender: TObject);
    procedure LBpagoAMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure IMPRIMEFOLHADEGUIA1Click(Sender: TObject);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure ImprimeTeste1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }

    procedure AbrePosicoesINI;
    procedure PassaLABELPRETO;
  public
    { Public declarations }
    TipoFonte:TTipoFonte;
    USaFonte:boolean;
    Function CarregaConfiguracoes:boolean;overload;
    Function CarregaConfiguracoes(RDPrint1:TRdPrint):boolean;overload;
  end;

var
  FConfiguraChequeRDPRINT: TFConfiguraChequeRDPRINT;
  ObjConfiguraRel:TObjConfRelatorio;
  ObjPortador:TObjPortador;
  POrtadorTemp:INteger;
implementation

uses UessencialGlobal, UObjConfiguraFolhaRdPrint;



{$R *.DFM}

procedure TFConfiguraChequeRDPRINT.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (Objportador<>nil)
     Then objportador.Free;
     tag:=0;

end;

Procedure TFConfiguraChequeRdPrint.AbrePosicoesINI;
var
int_habilita:integer;
NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin
     
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'CONFRELS\RELCHEQUE'+IntToStr(POrtadorTemp)+'.INI');
     Except
           Messagedlg('Erro na Abertura do Arquivo CONFRELS\RELCHEQUE'+IntToStr(POrtadorTemp)+'.INI',mterror,[mbok],0);
           exit;
     End;
Try
     Try

        for int_habilita:=0 to Self.ComponentCount -1
        do Begin
                if uppercase(Self.Components [int_habilita].ClassName) = 'TLABEL'
                then BEGIN
                          temp:=arquivo_ini.ReadString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_LINHA','');
                          try
                             strtoint(temp);
                             TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).top:=(5*strtoint(temp));
                          except

                          end;
                          temp:=arquivo_ini.ReadString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_COLUNA','');
                          try
                             strtoint(temp);
                             TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).left:=(5*strtoint(temp));
                          except

                          end;
                          temp:=arquivo_ini.ReadString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_QUANTIDADE','');
                          try
                             strtoint(temp);
                             TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).tag:=(strtoint(temp));
                          except

                          end;

                END;
        End;

     Except
           Messagedlg('Erro na escrita da CHAVE '+NOMECAMPO,mterror,[mbok],0);
           exit;
     End;
Finally
     FreeAndNil(arquivo_ini);
End;
end;


procedure TFConfiguraChequeRDPRINT.SalvaPosies1Click(Sender: TObject);
var
int_habilita:integer;
NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'CONFRELS\RELCHEQUE'+IntToStr(POrtadorTemp)+'.INI');
     Except
           Messagedlg('Erro na Abertura do Arquivo CONFRELS\CAMPOSNOTAFISCAL.ini',mterror,[mbok],0);
           exit;
     End;
Try
     Try

        for int_habilita:=0 to Self.ComponentCount -1
        do Begin
                if uppercase(Self.Components [int_habilita].ClassName) = 'TLABEL'
                then BEGIN
                          arquivo_ini.WriteString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_LINHA',FLOATTOSTR(INT((TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).TOP/5))));
                          arquivo_ini.WriteString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_COLUNA',FLOATTOSTR(INT((TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).left/5))));
                          arquivo_ini.WriteString('CONFIGURACOESLABELS',UPPERCASE(Self.Components [int_habilita].Name)+'_QUANTIDADE',FLOATTOSTR(INT((TLabel(Self.FindComponent(Self.Components [int_habilita].NAME)).TAG))));
                END;
        End;
        Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);
     Except
           Messagedlg('Erro na escrita da CHAVE '+NOMECAMPO,mterror,[mbok],0);
           exit;
     End;
Finally
     FreeAndNil(arquivo_ini);
End;
end;

Function TFConfiguraChequeRDPRINT.CarregaConfiguracoes(RDPrint1:TRdPrint):boolean;
var
ObjConfiguraRdPrint:TObjConfiguraFolhaRdPrint;
begin
     Try
        result:=False;
        ObjConfiguraRdPrint:=TObjConfiguraFolhaRdPrint.create;
        if (ObjConfiguraRdPrint.ResgataConfiguracoesFolha(rdprint1,'RELCHEQUE'+IntToStr(POrtadorTemp)+'.INI')=False)
        Then Begin
                  ObjConfiguraRdPrint.free;
                  strtoint('a');
        End;

        Self.USaFonte:=False;
        if (ObjConfiguraRdPrint.fonte<>'')
        Then Begin
                  Self.USaFonte:=True;
                  Self.tipofonte:=normal;

                  if (ObjConfiguraRdPrint.fonte='NEGRITO')
                  Then Self.tipofonte:=negrito;

                  if (ObjConfiguraRdPrint.fonte='ITALICO')
                  Then Self.tipofonte:=ITALICO;
                  
                  if (ObjConfiguraRdPrint.fonte='SUBLINHADO')
                  Then Self.tipofonte:=SUBLINHADO;
                  
                  if (ObjConfiguraRdPrint.fonte='EXPANDIDO')
                  Then Self.tipofonte:=EXPANDIDO;
                  
                  if (ObjConfiguraRdPrint.fonte='NORMAL')
                  Then Self.tipofonte:=NORMAL;
                  
                  if (ObjConfiguraRdPrint.fonte='COMP12')
                  Then Self.tipofonte:=COMP12;
                  
                  if (ObjConfiguraRdPrint.fonte='COMP17')
                  Then Self.tipofonte:=COMP17;
                  
                  if (ObjConfiguraRdPrint.fonte='COMP20')
                  Then Self.tipofonte:=COMP20;
        End;



        ObjConfiguraRdPrint.free;
        Self.AbrePosicoesINI;

           Opes1.Enabled:=True;
        result:=true;
     Except
           Messagedlg('Erro',mterror,[mbok],0);
           Opes1.Enabled:=False;
           exit;
     End;
end;



Function TFConfiguraChequeRDPRINT.CarregaConfiguracoes:boolean;
var
ObjConfiguraRdPrint:TObjConfiguraFolhaRdPrint;
begin
     Try
        result:=False;
        ObjConfiguraRdPrint:=TObjConfiguraFolhaRdPrint.create;
        if (ObjConfiguraRdPrint.ResgataConfiguracoesFolha(ComponenteRdPrint,'RELCHEQUE'+IntToStr(POrtadorTemp)+'.INI')=False)
        Then Begin
                  ObjConfiguraRdPrint.free;
                  strtoint('a');
        End;

        Self.USaFonte:=False;
        if (ObjConfiguraRdPrint.fonte<>'')
        Then Begin
                  Self.USaFonte:=True;
                  Self.tipofonte:=normal;

                  if (ObjConfiguraRdPrint.fonte='NEGRITO')
                  Then Self.tipofonte:=negrito;

                  if (ObjConfiguraRdPrint.fonte='ITALICO')
                  Then Self.tipofonte:=ITALICO;
                  
                  if (ObjConfiguraRdPrint.fonte='SUBLINHADO')
                  Then Self.tipofonte:=SUBLINHADO;
                  
                  if (ObjConfiguraRdPrint.fonte='EXPANDIDO')
                  Then Self.tipofonte:=EXPANDIDO;
                  
                  if (ObjConfiguraRdPrint.fonte='NORMAL')
                  Then Self.tipofonte:=NORMAL;
                  
                  if (ObjConfiguraRdPrint.fonte='COMP12')
                  Then Self.tipofonte:=COMP12;
                  
                  if (ObjConfiguraRdPrint.fonte='COMP17')
                  Then Self.tipofonte:=COMP17;
                  
                  if (ObjConfiguraRdPrint.fonte='COMP20')
                  Then Self.tipofonte:=COMP20;
        End;

        ObjConfiguraRdPrint.free;
        Self.AbrePosicoesINI;

        Opes1.Enabled:=True;
        result:=true;
     Except
           Messagedlg('Erro',mterror,[mbok],0);
           Opes1.Enabled:=False;
           exit;
     End;
end;

procedure TFConfiguraChequeRDPRINT.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
keyc:char;
begin
     keyc:=#13;
     if key=vk_left
     Then begin
                if (strtoint(edtleft.text)>=1)
                Then  edtleft.text:=inttostr(strtoint(edtleft.text)-1);
                self.edtleftKeyPress(Sender,keyc);
     End
     Else
        if key=vk_right
        Then Begin
                edtleft.text:=inttostr(strtoint(edtleft.text)+1);
                self.edtleftKeyPress(Sender,keyc);
        End
        Else
            if key=vk_up
            Then Begin
                if (strtoint(edttop.text)>=1)
                Then edttop.text:=inttostr(strtoint(edttop.text)-1);
                self.edtleftKeyPress(Sender,keyc);
            End
            Else
                if key=vk_down
                Then Begin
                        edttop.text:=inttostr(strtoint(edttop.text)+1);
                        self.edtleftKeyPress(Sender,keyc);
                End;
end;


procedure TFConfiguraChequeRDPRINT.edtleftKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=5*(strtoint(edttop.text));
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edttop.setfocus;
        End
        Else key:=#0;

end;

procedure TFConfiguraChequeRDPRINT.edttopKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=(5*strtoint(edttop.text))-VertScrollBar.position;
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edtquantidade.setfocus;
        End
        Else key:=#0;
end;

procedure TFConfiguraChequeRDPRINT.edtquantidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=5*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=5*(strtoint(edttop.text));
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).tag:=strtoint(edtquantidade.text);
               edtleft.setfocus;
        End
        Else key:=#0;
end;
procedure TFConfiguraChequeRDPRINT.PassaLABELPRETO;//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1
   do Begin
        if uppercase(Self.Components [int_habilita].ClassName) = 'TLABEL'
        then Tedit(Self.Components [int_habilita]).font.color:=clblack;
   End;

end;

procedure TFConfiguraChequeRDPRINT.LBpagoAClick(Sender: TObject);
begin
     PassaLABELPRETO;
     PanelPOsicoes.Caption:=TLabel(Sender).Name;
     TLabel(Sender).Font.Color:=clBlue;
     edtleft.text:=floattostr(int(TLabel(Sender).left/5));
     edttop.text:=floattostr(int((TLabel(Sender).Top+self.VertScrollBar.Position)/5));
     edtquantidade.text:=inttostr(TLabel(Sender).tag);
     edtleft.setfocus;
     VertScrollBar.Position:=0;
end;

procedure TFConfiguraChequeRDPRINT.LBpagoAMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     Tlabel(Sender).hint:=Tlabel(Sender).name;
end;

procedure TFConfiguraChequeRDPRINT.IMPRIMEFOLHADEGUIA1Click(
  Sender: TObject);
var
cont:integer;
cont2:integer;
begin
     ComponenteRdPrint.Abrir;
     //ComponenteRdPrint.FonteTamanhoPadrao:=S10cpp;
     //ComponenteRdPrint.TamanhoQteColunas:=80;

     for cont:=1 to ComponenteRdPrint.TamanhoQteColunas do
     Begin
         ComponenteRdPrint.Imp(1,cont,inttostr(cont)[length(inttostr(cont))]);
     End;

     for cont:=2 to ComponenteRdPrint.TamanhoQteLinhas do
     Begin
         ComponenteRdPrint.Imp(cont,1,inttostr(cont));
     End;
     ComponenteRdPrint.Novapagina;
     ComponenteRdPrint.Imp(1,1,'NOVA FOLHA');
     ComponenteRdPrint.fechar;
end;

procedure TFConfiguraChequeRDPRINT.FormDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     PanelPOsicoes.Left:=x;
     PanelPOsicoes.top:=y;

end;

procedure TFConfiguraChequeRDPRINT.FormDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
     accept:=true;
end;

procedure TFConfiguraChequeRDPRINT.ImprimeTeste1Click(Sender: TObject);
begin
     ComponenteRdPrint.abrir;
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBpagoA.top/5))),strtoint(floattostr(int(LBpagoA.left/5))),completapalavra('PAGO A ',LBpagoA.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBDATAATUAL.top/5))),strtoint(floattostr(int(LBDATAATUAL.left/5))),completapalavra('DATA ATUAL ',LBdataatual.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBvalorcanhoto.top/5))),strtoint(floattostr(int(LBvalorcanhoto.left/5))),completapalavra('valorcanhoto',LBvalorcanhoto.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBextenso1.top/5))),strtoint(floattostr(int(LBextenso1.left/5))),completapalavra('extenso1',LBextenso1.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBextenso2.top/5))),strtoint(floattostr(int(LBextenso2.left/5))),completapalavra('extenso2',LBextenso2.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBnominal.top/5))),strtoint(floattostr(int(LBnominal.left/5))),completapalavra('nominal',LBnominal.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBvalor.top/5))),strtoint(floattostr(int(LBvalor.left/5))),completapalavra('valor',LBvalor.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBcidadecheque.top/5))),strtoint(floattostr(int(LBcidadecheque.left/5))),completapalavra('cidadecheque',LBcidadecheque.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBdiacheque.top/5))),strtoint(floattostr(int(LBdiacheque.left/5))),completapalavra('diacheque',LBdiacheque.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBmescheque.top/5))),strtoint(floattostr(int(LBmescheque.left/5))),completapalavra('mescheque',LBmescheque.tag,' '));
     ComponenteRdPrint.Imp(strtoint(floattostr(int(LBanocheque.top/5))),strtoint(floattostr(int(LBanocheque.left/5))),completapalavra('anocheque',LBanocheque.tag,' '));
     ComponenteRdPrint.fechar;
     
end;

procedure TFConfiguraChequeRDPRINT.FormShow(Sender: TObject);
VAR
INT_HABILITA:INTEGER;
PortadorTempStr:String;
saida:boolean;
begin
     PegaCorForm(Self);

     
     If (Tag=1)
     Then exit;

     Try
        //FrelCheque:=TFrelcheque.create(nil);
        ObjPortador:=TObjPortador.create;
     Except
           Messagedlg('Erro na tentativa de cria��o do objeto Portador!',mterror,[mbok],0);
           exit;
     End;
     Repeat
        saida:=InputQuery('Escolha de Portador','Digite o Portador',PortadorTempStr);
        If (saida=False)
        Then BEgin
                  Opes1.Enabled:=False;
                  exit;
        End
        Else Begin
                  Try
                     POrtadorTemp:=StrToInt(PortadorTempStr);
                     If (ObjPortador.LocalizaCodigo(PortadorTempStr)=False)
                     Then portadortemp:=strtoint('a');
                     Opes1.Enabled:=True;
                     ObjPortador.TabelaparaObjeto;
                  Except
                        saida:=false;
                  End;
        End;
     Until(saida=True);

     Self.CarregaConfiguracoes;
     LBpagoA.onclick(LBpagoA);
     tag:=1;
end;

end.
