unit UEmiteCartaCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, Mask, UobjPendencia,UessencialGlobal,
  ExtCtrls, ComCtrls, TabNotBk;

type
  TFEmiteCartaCobranca = class(TForm)
    Guia: TTabbedNotebook;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    LbNomeCredorDevedor: TLabel;
    Label4: TLabel;
    StrGrid: TStringGrid;
    ComboNomeCredorDevedor: TComboBox;
    btPesquisar: TBitBtn;
    edtVencimentoInicial: TMaskEdit;
    edtVencimentoFinal: TMaskEdit;
    ComboCodigoTabelaCredorDevedor: TComboBox;
    Panel1: TPanel;
    btsair: TBitBtn;
    btOpcoes: TBitBtn;
    ChecMarcarDesmarcarTodos: TCheckBox;
    edtcodigocredordevedor: TEdit;
    RgOpcoes: TRadioGroup;
    TabbedNotebook1: TTabbedNotebook;
    Memo1: TMemo;
    Label6: TLabel;
    Label7: TLabel;
    Memo2: TMemo;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ComboNomeCredorDevedorClick(Sender: TObject);
    procedure ComboNomeCredorDevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ComboNomeCredorDevedorKeyPress(Sender: TObject; var Key: Char);
    procedure ComboNomeCredorDevedorKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ChecMarcarDesmarcarTodosClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure StrGridDblClick(Sender: TObject);
    procedure StrGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btPesquisarClick(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorExit(Sender: TObject);
    procedure ComboNomeCredorDevedorExit(Sender: TObject);
    procedure edtcodigocredordevedorKeyPress(Sender: TObject;
      var Key: Char);
    procedure RgOpcoesClick(Sender: TObject);
  private
    ObjPendencia : TOBJPendencia;
    procedure LimpaStringGrid(PStrGrid: TStringGrid);
  public
    { Public declarations }
  end;

var
  FEmiteCartaCobranca: TFEmiteCartaCobranca;

implementation

uses DB, Upesquisa;

{$R *.dfm}

procedure TFEmiteCartaCobranca.FormShow(Sender: TObject);
begin
      try
            ObjPendencia:=TOBJPendencia.Create;
      except
            Messagedlg('Erro ao tentar criar o Objeto Pendencia!',mterror,[mbok],0);
            exit;
      end;
      UessencialGlobal.PegaCorForm(Self);
      
      StrGrid.ColCount:=2;
      StrGrid.cols[1].clear;
      StrGrid.cols[2].clear;
      StrGrid.Cells[0,0]:='';
      StrGrid.Cells[1,0]:='NOME';
      StrGrid.Cells[2,0]:='TITULO';
      StrGrid.Cells[3,0]:='PARC.ATRAS';
      StrGrid.Cells[4,0]:='TOTAL';


      ObjPendencia.Titulo.CREDORDEVEDOR.Get_listaNomeCredorDevedor(ComboNomeCredorDevedor.Items);
      ObjPendencia.Titulo.CREDORDEVEDOR.Get_listaCodigoCredorDevedor(ComboCodigoTabelaCredorDevedor.Items);
      Guia.pageindex:=0;
      edtVencimentoInicial.setfocus;

end;

procedure TFEmiteCartaCobranca.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      if (ObjPendencia <> nil)
      then ObjPendencia.Free;
end;


procedure TFEmiteCartaCobranca.ComboNomeCredorDevedorClick(Sender: TObject);
begin
ComboCodigoTabelaCredorDevedor.ItemIndex:=ComboNomeCredorDevedor.ItemIndex;
end;

procedure TFEmiteCartaCobranca.ComboNomeCredorDevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
ComboCodigoTabelaCredorDevedor.ItemIndex:=ComboNomeCredorDevedor.ItemIndex;
end;

procedure TFEmiteCartaCobranca.ComboNomeCredorDevedorKeyPress(Sender: TObject;
  var Key: Char);
begin
ComboCodigoTabelaCredorDevedor.ItemIndex:=ComboNomeCredorDevedor.ItemIndex;
end;

procedure TFEmiteCartaCobranca.ComboNomeCredorDevedorKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
ComboCodigoTabelaCredorDevedor.ItemIndex:=ComboNomeCredorDevedor.ItemIndex;

end;
procedure TFEmiteCartaCobranca.ChecMarcarDesmarcarTodosClick(Sender: TObject);
Var  Cont:Integer;
begin
     for cont:=1 to StrGrid.RowCount-1 do
     Begin
          if (ChecMarcarDesmarcarTodos.Checked = true)
          then StrGrid.Cells[0,cont]:='>>'
          else StrGrid.Cells[0,cont]:=''
     end;
end;

procedure TFEmiteCartaCobranca.btSairClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFEmiteCartaCobranca.StrGridDblClick(Sender: TObject);
begin
      if (StrGrid.Cells[0,StrGrid.Row]='')
      then StrGrid.Cells[0,StrGrid.Row]:='>>'
      else StrGrid.Cells[0,StrGrid.Row]:='';
end;

procedure TFEmiteCartaCobranca.StrGridKeyPress(Sender: TObject;
  var Key: Char);
begin

   if (Key=#32) or (Key=#13)
   Then Begin
             if (StrGrid.Cells[0,StrGrid.Row]='')
             then StrGrid.Cells[0,StrGrid.Row]:='>>'
             else StrGrid.Cells[0,StrGrid.Row]:='';

             StrGrid.setfocus;
   End;

end;

procedure TFEmiteCartaCobranca.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then begin
           Perform(Wm_NextDlgCtl,0,0);
      end;
end;

procedure TFEmiteCartaCobranca.btPesquisarClick(Sender: TObject);
Var
Extra:string;
begin

      case RgOpcoes.ItemIndex of
       0:Begin
            extra:='T';
       End;
       1:Begin
              extra:='P'
       End;
     End;

      Self.LimpaStringGrid(StrGrid);
      Self.ObjPendencia.MostraClientesInadimplesPorContaGer(ComboCodigoTabelaCredorDevedor.Text,edtcodigocredordevedor.Text,edtVencimentoInicial.Text,edtVencimentoFinal.Text, StrGrid,extra);

end;

Procedure TFEmiteCartaCobranca.LimpaStringGrid(PStrGrid : TStringGrid);
Var Linha, Coluna : Integer;
Begin
    For linha:=1 to PStrGrid.RowCount-1 do
    Begin
         For  Coluna:=0 to PStrGrid.ColCount-1 do
         Begin
             PStrGrid.Cells[Coluna, Linha]:='';
         end;
    end;
end;


procedure TFEmiteCartaCobranca.btOpcoesClick(Sender: TObject);
Var Cont : Integer;
begin
     Self.ObjPendencia.EmiteCartaCobranca(StrGrid);
end;

procedure TFEmiteCartaCobranca.edtcodigocredordevedorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     If (ComboCodigoTabelaCredorDevedor.itemindex=-1)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(ObjPendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(ComboCodigoTabelaCredorDevedor.text),'Pesquisa de '+ComboNomeCredorDevedor.text,ObjPendencia.Titulo.Get_FormularioCredorDevedor(ComboCodigoTabelaCredorDevedor.text))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedor.caption:=FpesquisaLocal.QueryPesq.fieldbyname(ObjPendencia.Titulo.Get_CampoNomeCredorDevedor(ComboCodigoTabelaCredorDevedor.text)).asstring;
                             End;

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFEmiteCartaCobranca.edtcodigocredordevedorExit(Sender: TObject);
begin
     LbNomeCredorDevedor.caption:='';
     If (edtcodigocredordevedor.text='') or (ComboCodigoTabelaCredorDevedor.ItemIndex=-1)
     Then exit;
     LbNomeCredorDevedor.caption:=ObjPendencia.Titulo.Get_NomeCredorDevedor(ComboCodigoTabelaCredorDevedor.text,edtcodigocredordevedor.text);

end;

procedure TFEmiteCartaCobranca.ComboNomeCredorDevedorExit(Sender: TObject);
begin
      edtcodigocredordevedor.Clear;
      LbNomeCredorDevedor.Caption:='';
end;

procedure TFEmiteCartaCobranca.edtcodigocredordevedorKeyPress(
  Sender: TObject; var Key: Char);
begin
    if not (key in ['0'..'9',#8])
    Then key:=#0;
end;

procedure TFEmiteCartaCobranca.RgOpcoesClick(Sender: TObject);
begin
     btPesquisarClick(sender);
end;

end.
