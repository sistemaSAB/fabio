unit UBoletoBancario;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjRegistroRETORNOCOBRANCA,
  Grids, DBGrids;

type
  TFBoletoBancario = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btpesquisar: TBitBtn;
    btopcoes: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LbNomeConvenio: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    combosituacao: TComboBox;
    edtnumeroboleto: TEdit;
    edtnumeroboleto2: TEdit;
    edtconvenioboleto: TEdit;
    edtvencimento: TMaskEdit;
    edtdatadoc: TMaskEdit;
    edtdataproc: TMaskEdit;
    edtnumerodoc: TEdit;
    edtvalordoc: TEdit;
    edtdesconto: TEdit;
    edtoutrasdeducoes: TEdit;
    edtjuros: TEdit;
    edtoutrosacrescimos: TEdit;
    edtvalorcobrado: TEdit;
    edtdatapagamento: TMaskEdit;
    memoinstrucoes: TMemo;
    DBGrid1: TDBGrid;
    memoObservacoesSuperior: TMemo;
    Label19: TLabel;
    Label18: TLabel;
    Label20: TLabel;
    EdtRemessaRetorno: TEdit;
    Label21: TLabel;
    EdtNossoNumeroCobreBem: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure edtconvenioboletoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtconvenioboletoExit(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure memoObservacoesSuperiorKeyPress(Sender: TObject;
      var Key: Char);
  private
         ObjRegistroRetornoCobranca:TObjRegistroRETORNOCOBRANCA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         function atualizaQuantidade:string;
    procedure controlChange(Sender: Tobject);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBoletoBancario: TFBoletoBancario;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UObjBoletoBancario;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFBoletoBancario.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjRegistroRetornoCobranca.Boleto do
    Begin
         Submit_CODIGO               (edtCODIGO.text);
         Submit_Situacao             (combosituacao.text[1]);
         Submit_Historico            (edthistorico.text);
         Submit_NumeroBoleto         (edtnumeroboleto.Text);
         Convenioboleto.Submit_CODIGO(edtconvenioboleto.Text);
         //***********************************************************
         Submit_Vencimento      (edtVencimento      .text);
         Submit_DataDoc         (edtDataDoc         .text);
         Submit_DataProc        (edtDataProc        .text);
         Submit_NumeroDoc       (edtNumeroDoc       .text);
         Submit_ValorDoc        (edtValorDoc        .text);
         Submit_Desconto        (edtDesconto        .text);
         Submit_OutrasDeducoes  (edtOutrasDeducoes  .text);
         Submit_Juros           (edtJuros           .text);
         Submit_outrosacrescimos(edtoutrosacrescimos.text);
         Submit_ValorCobrado    (edtValorCobrado    .text);
         Submit_DataPagamento   (edtDataPagamento   .text);
         Submit_Instrucoes(memoinstrucoes.Text);
         Submit_ObservacoesSuperior(memoObservacoesSuperior.text);
         ArquivoRemessaRetorno.Submit_Codigo(EdtRemessaRetorno.Text);
         Submit_NossoNumeroCobreBem(EdtNossoNumeroCobreBem.Text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFBoletoBancario.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjRegistroRetornoCobranca.Boleto do
     Begin
        edtCODIGO.text          :=Get_CODIGO;
        edthistorico.text       :=Get_Historico;
        edtnumeroboleto.Text    :=Get_NumeroBoleto;
        edtconvenioboleto.Text  :=Convenioboleto.Get_CODIGO;
        LbNomeConvenio.caption:=Convenioboleto.get_nome;
        //************************************************
        edtVencimento      .text:=Get_Vencimento      ;
        edtDataDoc         .text:=Get_DataDoc         ;
        edtDataProc        .text:=Get_DataProc        ;
        edtNumeroDoc       .text:=Get_NumeroDoc       ;
        edtValorDoc        .text:=Get_ValorDoc        ;
        edtDesconto        .text:=Get_Desconto        ;
        edtOutrasDeducoes  .text:=Get_OutrasDeducoes  ;
        edtJuros           .text:=Get_Juros           ;
        edtoutrosacrescimos.text:=Get_outrosacrescimos;
        edtValorCobrado    .text:=Get_ValorCobrado    ;
        edtDataPagamento   .text:=Get_DataPagamento   ;
        memoinstrucoes.Text:=Get_Instrucoes;
        memoObservacoesSuperior.text:=Get_ObservacoesSuperior;

        If (Get_Situacao='N')
        Then combosituacao.itemindex:=0
        Else If (Get_Situacao='I')
             Then combosituacao.ItemIndex:=1
             Else If (Get_Situacao='R')
                  Then combosituacao.itemindex:=2
                  Else combosituacao.itemindex:=-1;

        EdtRemessaRetorno.Text:=ArquivoRemessaRetorno.Get_Codigo;
        EdtNossoNumeroCobreBem.Text:=Get_NossoNumeroCobreBem;          

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFBoletoBancario.TabelaParaControles: Boolean;
begin
     If (Self.ObjRegistroRetornoCobranca.Boleto.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFBoletoBancario.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjRegistroRetornoCobranca.Boleto=Nil)
     Then exit;

     If (Self.ObjRegistroRetornoCobranca.Boleto.status<>dsinactive)
     Then Begin
              Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
              abort;
              exit;
     End;

    Self.ObjRegistroRetornoCobranca.free;
    Screen.OnActiveControlChange:=nil;
end;

procedure TFBoletoBancario.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
      end;
end;



procedure TFBoletoBancario.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.LimpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjRegistroRetornoCobranca.Boleto.status:=dsInsert;
     edtnumeroboleto.setfocus;
     combosituacao.itemindex:=0;

     EdtCodigo.enabled:=False;
     combosituacao.Enabled:=False;
     edthistorico.Enabled:=False;
end;


procedure TFBoletoBancario.btalterarClick(Sender: TObject);
begin
    If (Self.ObjRegistroRetornoCobranca.Boleto.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                Self.ObjRegistroRetornoCobranca.Boleto.Status:=dsEdit;
                EdtCodigo.Enabled:=False;
                edtnumeroboleto.Enabled:=False;
                edtnumeroboleto2.Enabled:=False;
                edtconvenioboleto.Enabled:=False;

                combosituacao.setfocus;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
    End;


end;

procedure TFBoletoBancario.btgravarClick(Sender: TObject);
begin

     If Self.ObjRegistroRetornoCobranca.Boleto.Status=dsInactive
     Then exit;

     If Self.ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjRegistroRetornoCobranca.Boleto.salvar(true,edtnumeroboleto2.text)=False)
     Then exit;

     mostra_botoes(Self);
     desabilita_campos(Self);
     Self.ObjRegistroRetornoCobranca.Boleto.LocalizaCodigo(EdtCodigo.Text);
     Self.ObjRegistroRetornoCobranca.Boleto.TabelaparaObjeto;
     Self.ObjetoParaControles;
     lbquantidade.caption:=atualizaQuantidade;
     MensagemAviso('Boletos Cadastrados com Sucesso!');
end;

procedure TFBoletoBancario.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjRegistroRetornoCobranca.Boleto.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjRegistroRetornoCobranca.Boleto.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     

     If (Self.ObjRegistroRetornoCobranca.Boleto.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.LimpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFBoletoBancario.btcancelarClick(Sender: TObject);
begin
     Self.ObjRegistroRetornoCobranca.Boleto.cancelar;

     limpaedit(Self);
     Self.LimpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFBoletoBancario.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFBoletoBancario.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjRegistroRetornoCobranca.Boleto.Get_pesquisa,Self.ObjRegistroRetornoCobranca.Boleto.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjRegistroRetornoCobranca.Boleto.status<>dsinactive
                                  then exit;

                                  If (Self.ObjRegistroRetornoCobranca.Boleto.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjRegistroRetornoCobranca.Boleto.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.LimpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFBoletoBancario.edtconvenioboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjRegistroRetornoCobranca.Boleto.edtconvenioboletokeydown(sender,key,shift,LbNomeConvenio);
end;

procedure TFBoletoBancario.edtconvenioboletoExit(Sender: TObject);
begin
   Self.ObjRegistroRetornoCobranca.Boleto.edtconvenioboletoexit(sender,LbNomeConvenio);
end;

procedure TFBoletoBancario.LimpaLabels;
begin
     LbNomeConvenio.caption:='';
end;

procedure TFBoletoBancario.btopcoesClick(Sender: TObject);
begin
     Self.ObjRegistroRetornoCobranca.Boleto.opcoes(edtcodigo.Text);
end;

procedure TFBoletoBancario.btrelatoriosClick(Sender: TObject);
begin
     Self.ObjRegistroRetornoCobranca.Imprime_Cadastro_boleto(EdtCodigo.text);
end;

procedure TFBoletoBancario.FormShow(Sender: TObject);
begin
      limpaedit(Self);
      Self.LimpaLabels;
      desabilita_campos(Self);

      Try
            Self.ObjRegistroRetornoCobranca:=TObjRegistroRetornoCobranca.create;
      Except
            Messagedlg('Erro na Inicializa��o do Objeto!',mterror,[mbok],0);
            Self.close;
      End;
      FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
      FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
      FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
      Self.Color:=clwhite;
      lbquantidade.caption:=atualizaQuantidade;
      Uessencialglobal.PegaCorForm(Self);
      Screen.OnActiveControlChange:=Self.controlChange;
end;

procedure TFBoletoBancario.memoObservacoesSuperiorKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then memoObservacoesSuperior.setfocus;
end;

function TFBoletoBancario.atualizaQuantidade: string;
begin
       result:='Existem '+ContaRegistros('TABBOLETOBANCARIO','CODIGO')+' Boletos Cadastrados';
end;

procedure TFBoletoBancario.controlChange(Sender: Tobject);
begin
     controlChange_focus(Sender, self);
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjRegistroRetornoCobranca.Boleto.OBJETO.Get_Pesquisa,Self.ObjRegistroRetornoCobranca.Boleto.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjRegistroRetornoCobranca.Boleto.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjRegistroRetornoCobranca.Boleto.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjRegistroRetornoCobranca.Boleto.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
