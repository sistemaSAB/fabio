unit UObjPrazoPagamento;

interface

uses controls,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,Classes;

type
  TObjPrazoPagamento=class

  public
    //ObjDatasource                               :TDataSource;
    Status                                      :TDataSetState;
    SqlInicial                                  :String[200];

    constructor Create;
    destructor Free;
    function    Salvar                          :Boolean;
    function    LocalizaCodigo(Parametro:string) :boolean;
    function    exclui(Pcodigo:string)            :Boolean;
    function    Get_Pesquisa                    :string;
    function    Get_TituloPesquisa              :string;
    procedure   TabelaparaObjeto;
    procedure   ZerarTabela;
    procedure   Cancelar;
    procedure   Commit;

    function Get_CODIGO           :string;
    function Get_Nome             :string;
    function Get_Parcelas         :string;
    function Get_Formula          :string;
    function get_ImprimeDuplicatas:string;
    function Get_Carteira         :string;
    function get_CREDITO_DEBITO   :string;

    procedure Submit_CODIGO           (parametro:string);
    procedure Submit_Nome             (parametro:string);
    procedure Submit_Parcelas         (parametro:string);
    procedure Submit_Formula          (parametro:string);
    procedure Submit_Carteira         (parametro:string);
    procedure submit_Credito_Debito   (parametro:String);
    procedure Get_listaCodigo         (parametro:TStrings);
    procedure Get_listanome           (parametro:TStrings);
    function Get_ParcelaPorNum        (parametro:Integer):Integer;
    procedure Submit_ImprimeDuplicatas(parametro:string);
    function RetornaCampoNome:string;
    function RetornaCampoCodigo:string;
    function Verifica_PRAZO:boolean;
    function Get_NovoCodigo: string;
    procedure RetornaPrevisaoParcelas(Pvalor: currency; Pdata:tdate;Pvencimentos,Pvalores: TStringList);


  private
    ObjDataset:Tibdataset;

    CODIGO    :String[09];
    Nome      :String[50];
    Parcelas  :String[09];
    Formula   :String[254];
    ImprimeDuplicatas:string;
    Carteira   :string;
    CREDITO_DEBITO:string;

    function  VerificaBrancos:Boolean;
    function  VerificaRelacionamentos:Boolean;
    function  VerificaNumericos:Boolean;
    function  VerificaData:Boolean;
    function  VerificaFaixa:boolean;
    procedure ObjetoparaTabela;

  end;

implementation

uses SysUtils,Dialogs,UDatamodulo;


{ TTabTitulo }

procedure TObjPrazoPagamento.TabelaparaObjeto;//ok
begin
  with ObjDataset do
  begin
    Self.CODIGO       :=FieldByName('CODIGO').asstring      ;
    Self.Nome         :=FieldByName('Nome').asstring        ;
    Self.Parcelas     :=FieldByName('Parcelas').asstring    ;
    Self.Formula      :=FieldByName('Formula').asstring     ;
    Self.carteira     :=FieldByName('carteira').asstring    ;
    Self.ImprimeDuplicatas:=FieldByName('ImprimeDuplicatas').asstring     ;
    Self.CREDITO_DEBITO     :=FieldByName('CREDITO_DEBITO').asstring    ;
  end;
end;


Procedure TObjPrazoPagamento.ObjetoparaTabela;//ok
begin
  with ObjDataset do
  begin
    FieldByName('CODIGO').asstring            :=Self.CODIGO   ;
    FieldByName('Nome').asstring              :=Self.Nome     ;
    FieldByName('Parcelas').asstring          :=Self.Parcelas ;
    FieldByName('Formula').asstring           :=Self.Formula  ;
    FieldByName('ImprimeDuplicatas').asstring :=Self.ImprimeDuplicatas;
    FieldByName('carteira').asstring          :=Self.carteira;
    FieldByName('CREDITO_DEBITO').AsString    :=Self.CREDITO_DEBITO;
  end;
end;

//***********************************************************************

function TObjPrazoPagamento.Salvar: Boolean;//Ok
begin

  if (Self.VerificaBrancos=True) then
  begin
    result:=false;
    Exit;
  end;

  if (Self.VerificaNumericos=False) then
  begin
    result:=false;
    Exit;
  end;

  if (Self.VerificaData=False) then
  begin
    result:=false;
    Exit;
  end;

  if (Self.VerificaFaixa=False) then
  begin
    result:=false;
    Exit;
  end;

  if (Self.VerificaRelacionamentos=False) then
  begin
    result:=false;
    Exit;
  end;

  if Self.LocalizaCodigo(Self.CODIGO)=False then
  begin
    if(Self.Status=dsedit) then
    begin
      Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
      result:=False;
      exit;
    end;
  end
  else
  begin
    if(Self.Status=dsinsert) then
    begin
      Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
      result:=False;
      exit;
    end;
  end;

  if Self.status=dsinsert then
    Self.ObjDataset.Insert//libera para insercao
  else
    if (Self.Status=dsedit) then
      Self.ObjDataset.edit//se for edicao libera para tal
    else
    begin
      mensagemerro('Status Inv�lido na Grava��o');
      exit;
    end;

  Self.ObjetoParaTabela;
  Self.ObjDataset.Post;
  FDataModulo.IBTransaction.CommitRetaining;
  Self.status          :=dsInactive;
  result:=True;
end;

procedure TObjPrazoPagamento.ZerarTabela;//Ok
begin
  with Self do
  begin
    Self.CODIGO           :='';
    Self.Nome             :='';
    Self.Parcelas         :='';
    Self.Formula          :='';
    Self.ImprimeDuplicatas:='';
    Self.Carteira         :='';
    Self.CREDITO_DEBITO   :='';
  end;
end;

function TObjPrazoPagamento.VerificaBrancos:boolean;
var
  Mensagem:string;
begin
  mensagem:='';

  with Self do
  begin
    if (codigo='')
    then Mensagem:=mensagem+'/C�digo';

    if (nome='')
    then Mensagem:=mensagem+'/Nome';

    if (Parcelas='')
    then Mensagem:=mensagem+'/Parcelas';

    if (Formula='')
    then Mensagem:=mensagem+'/F�rmula';

    if (ImprimeDuplicatas='')
    then Mensagem:=Mensagem+'/Imprime Duplicatas';

    if (Carteira='')
    Then Mensagem:=Mensagem+'/Carteira';

    if (CREDITO_DEBITO='')
    then Mensagem:=Mensagem+'/ Cr�dito/D�bito %';
  end;

  if mensagem <> '' then
  begin//mostra mensagem de erro caso existam cpos requeridos em branco
    messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
    result:=true;
    exit;
  end;

  result:=false;
end;


function TObjPrazoPagamento.VerificaRelacionamentos: Boolean;
var
  mensagem:string;
  Inteiros:Integer;
  cont:Byte;
  QuantFormula:byte;
  NumatualStr:String;
  NumAtualInt:Integer;
begin
  mensagem:='';

  try
    inteiros:=Strtoint(Self.Parcelas);
  except
    result:=False;
    exit;
  end;

  if (Inteiros=-1) then
  begin
    Result:=True;
    exit;
  end;
  quantformula:=0;

  for cont:=1 to length(Self.formula) do
  begin
    If (Self.formula[cont]=';') then
    begin
      try
        NumAtualint:=Strtoint(NumAtualStr);
      except
        Messagedlg('F�rmula Inv�lida!',mterror,[mbok],0);
        result:=False;
        exit;
      end;

      Inc(quantformula,1);
      NumAtualStr:='';
    end
    else NumAtualStr:=NumAtualStr+Self.Formula[cont];
  end;

  if (quantformula<>Inteiros) then
  begin
    Messagedlg('Quantidade de Dias na f�rmula Incompat�vel com N� de Parcelas'+#13+'F�rmula->'+inttostr(QuantFormula)+#13+'Parcelas->'+Self.Parcelas,mterror,[mbok],0);
    result:=False;
    exit;
  end;

  {     If (mensagem<>'')
  Then Begin
  Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
  result:=False;
  exit;
  end;}
  result:=true;
end;

function TObjPrazoPagamento.VerificaNumericos: Boolean;
var
  Inteiros:Integer;
  Reais:Currency;
  Mensagem:string;
begin
  Mensagem:='';

  try
    Inteiros:=Strtoint(Self.codigo);
  except
    Mensagem:=mensagem+'/C�digo';
  end;

  try
    Inteiros:=StrToint(Self.Parcelas);
  except
    Mensagem:=Mensagem+'/Parcelas';
  end;

  try
    Reais:=StrToCurr(Self.CREDITO_DEBITO);
  except
    mensagem:=mensagem+'/Cr�dito/D�bito %';
  end;

  if Mensagem<>'' then
  begin
    Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
    result:=false;
    exit;
  end;
  result:=true;

end;

function TObjPrazoPagamento.VerificaData: Boolean;
var
  Datas:Tdate;
  Horas:Ttime;
  Mensagem:string;
begin
  mensagem:='';

  if Mensagem<>'' then
  begin
    Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
    result:=false;
    exit;
  end;
  result:=true;
end;

function TObjPrazoPagamento.VerificaFaixa: boolean;
var
  Mensagem:string;
begin

  try

    Mensagem:='';

    if ((Self.ImprimeDuplicatas<>'S') and (Self.ImprimeDuplicatas<>'N'))
    then mensagem:=Mensagem+'/ O Campo "Imprime Duplicatas" cont�m um valor inv�lido!';

    if ((Self.Carteira<>'S') and (Self.Carteira<>'N'))
    then mensagem:=Mensagem+'/ O Campo "Carteira" cont�m um valor inv�lido!';

    if mensagem <> '' then
    begin
      Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
      result:=false;
      exit;
    end;

    result:=true;
  finally
  end;

end;

function TObjPrazoPagamento.LocalizaCodigo(parametro: string): boolean;//ok
begin
  if(Parametro='') then
  begin
    Result:=False;
    Exit;
  end;

  with Self.ObjDataset do
  begin
    close;
    SelectSql.Clear;
    SelectSql.add('Select Codigo,Nome,Parcelas,Formula,IMPRIMEDUPLICATAS,carteira,CREDITO_DEBITO from TabPrazoPagamento where codigo='+parametro);
    Open;
    If (recordcount > 0) then
      Result:=True
    Else Result:=False;
  end;
end;


procedure TObjPrazoPagamento.Cancelar;
begin
  FDataModulo.IBTransaction.rollbackretaining;
  Self.status:=dsInactive;
end;

function TObjPrazoPagamento.Exclui(Pcodigo: string): Boolean;
begin
  try
    result:=true;
    if (Self.LocalizaCodigo(Pcodigo)=True) then
    begin
      Self.ObjDataset.delete;
      FDataModulo.IBTransaction.CommitRetaining;
    end
    else result:=false;
  except
    result:=false;
  end;
end;

constructor TObjPrazoPagamento.create;
begin

  Self.ObjDataset:=Tibdataset.create(nil);
  Self.ObjDataset.Database:=FDataModulo.IbDatabase;

  ZerarTabela;

  with Self.ObjDataset do
  begin
    SelectSQL.clear;
    SelectSql.add('Select Codigo,Nome,Parcelas,Formula,IMPRIMEDUPLICATAS,carteira,CREDITO_DEBITO from TabPrazoPagamento where codigo=0');

    Self.SqlInicial:=SelectSQL.text;

    InsertSQL.clear;
    InsertSQL.add('Insert into TabPrazoPagamento (Codigo,Nome,Parcelas,Formula,IMPRIMEDUPLICATAS,carteira,CREDITO_DEBITO) values (:Codigo,:Nome,:Parcelas,:Formula,:IMPRIMEDUPLICATAS,:carteira,:CREDITO_DEBITO) ');

    ModifySQL.clear;
    ModifySQL.add(' Update Tabprazopagamento set Codigo=:Codigo,Nome=:Nome,Parcelas=:Parcelas,Formula=:Formula,IMPRIMEDUPLICATAS=:IMPRIMEDUPLICATAS,carteira=:carteira,CREDITO_DEBITO=:CREDITO_DEBITO where codigo=:codigo ');


    DeleteSQL.clear;
    DeleteSQL.add(' Delete from TabPrazoPagamento where codigo=:codigo');

    RefreshSQL.clear;
    RefreshSQL.add('Select Codigo,Nome,Parcelas,Formula,IMPRIMEDUPLICATAS,carteira,CREDITO_DEBITO from TabPrazoPagamento where codigo=0');

    open;

    Self.ObjDataset.First ;
    Self.status := dsInactive;

  end;
end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjPrazoPagamento.Get_CODIGO: string;
begin
  Result:=Self.Codigo;
end;

procedure TObjPrazoPagamento.Submit_CODIGO(parametro: string);
begin
  Self.Codigo:=parametro;
end;

procedure TObjPrazoPagamento.Commit;
begin
  FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPrazoPagamento.Get_Pesquisa: string;
begin
  Result:=' Select * from TabPrazoPagamento ';
end;

function TObjPrazoPagamento.Get_TituloPesquisa: string;
begin
  Result:=' Pesquisa de Prazo de Pagamento ';
end;

function TObjPrazoPagamento.Get_Formula: string;
begin
  Result:=Self.Formula;
end;

function TObjPrazoPagamento.Get_Nome: string;
begin
  Result:=Self.nome;
end;

function TObjPrazoPagamento.Get_Parcelas: string;
begin
  Result:=Self.Parcelas;
end;

procedure TObjPrazoPagamento.Submit_Formula(parametro: string);
begin
  Self.Formula:=Parametro;
end;

procedure TObjPrazoPagamento.Submit_Nome(parametro: string);
begin
  Self.nome:=Parametro;
end;

procedure TObjPrazoPagamento.Submit_Parcelas(parametro: string);
begin
  Self.parcelas:=Parametro;
end;

procedure TObjPrazoPagamento.Get_listaCodigo(parametro: TStrings);
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add('Select codigo from TabPrazoPagamento order by codigo');
    open;
    first;
    parametro.clear;
    While not(eof) do
    Begin
      parametro.add(Fieldbyname('codigo').asstring);
      next;
    end;
  end;
end;

procedure TObjPrazoPagamento.Get_listanome(parametro: TStrings);
begin
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add('Select nome from TabPrazoPagamento order by codigo');
    open;
    first;
    parametro.clear;
    While not(eof) do
    Begin
      parametro.add(Fieldbyname('nome').asstring);
      next;
    end;
  end;
end;

function TObjPrazoPagamento.Get_ParcelaPorNum(parametro: Integer): Integer;
var
  quantformula,cont,NumAtualint:Integer;
  NumAtualStr:String;
begin
  quantformula:=0;
  NumAtualStr:='';
  for cont:=1 to length(Self.formula) do
  Begin
    If (Self.formula[cont]=';')
    Then Begin
      NumAtualint:=Strtoint(NumAtualStr);
      Inc(quantformula,1);
      If (QuantFormula=Parametro)
      Then Begin
        Result:=NumAtualInt;
        exit;
      end;
      NumAtualStr:='';
    end
    Else NumAtualStr:=NumAtualStr+Self.Formula[cont];
  end;

end;

destructor TObjPrazoPagamento.Free;
begin
  Freeandnil(Self.ObjDataset);
end;

function TObjPrazoPagamento.RetornaCampoCodigo: string;
begin
  result:='codigo';
end;

function TObjPrazoPagamento.RetornaCampoNome: string;
begin
  result:='nome';
end;

function TObjPrazoPagamento.get_ImprimeDuplicatas: string;
begin
  Result:=Self.ImprimeDuplicatas;
end;

procedure TObjPrazoPagamento.Submit_ImprimeDuplicatas(parametro: string);
begin
  Self.ImprimeDuplicatas:=Parametro;
end;

function TObjPrazoPagamento.Verifica_PRAZO: boolean;
begin
  result:=True;

  //se � mais de uma parcela � a prazo entaum
  If (strtoint(Self.Parcelas)>1)
  Then exit;

  if (Self.Get_ParcelaPorNum(1)=0)
  Then Result:=False;

end;

function TObjPrazoPagamento.Get_Carteira: string;
begin
  Result:=Self.Carteira;
end;

procedure TObjPrazoPagamento.Submit_Carteira(parametro: string);
begin
  Self.Carteira:=parametro;
end;

function TObjPrazoPagamento.Get_NovoCodigo: string;
var
  StrTemp:TIBStoredProc;
begin
  Try
    Try
      StrTemp:=TIBStoredProc.create(nil);
      StrTemp.database:=ObjDataset.Database;
      StrTemp.StoredProcName:='PROC_GERA_PRAZOPAGAMENTO';

      StrTemp.ExecProc;
      Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
    Except
      Messagedlg('Erro durante a Cria��o de Um novo C�digo para o T�tulo',mterror,[mbok],0);
      result:='0';
      exit;
    end;
  Finally
    FreeandNil(StrTemp);
  end;
end;

Procedure TObjPrazoPagamento.RetornaPrevisaoParcelas(Pvalor:currency;Pdata:tdate;Pvencimentos,Pvalores: TStringList);
var
  cont:Integer;
  NumParcelas:Integer;
  ValorParcela:Currency;
  ValorParcelaStr:String;
  DefineData:boolean;
  CREDITO_DEBITO:Currency;
begin
  Pvencimentos.clear;
  Pvalores.clear;
  NumParcelas:=Strtoint(Self.get_parcelas);
  CREDITO_DEBITO:=StrToCurr(Self.get_CREDITO_DEBITO);

  ValorParcela:=PValor/NumParcelas;
  //ValorParcela:=(Pvalor+(pvalor*CREDITO_DEBITO)/100)/NumParcelas;
  ValorParcelaStr:='';
  ValorParcelaStr:=ArredondaValor(ValorParcela);

  For cont:=1 to NumParcelas do
  Begin
    Pvencimentos.add(Datetostr(Pdata+Self.Get_ParcelaPorNum(cont)));
    Pvalores.add(ValorParcelaStr);
  end;
end;

function TObjPrazoPagamento.get_CREDITO_DEBITO: string;
begin
  Result:=Self.CREDITO_DEBITO;
end;

procedure TObjPrazoPagamento.submit_Credito_Debito(parametro: String);
begin
  Self.CREDITO_DEBITO:=parametro;
end;

end.
