unit UescolheConvenio;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, ExtCtrls;

type
  TFescolheConvenio = class(TForm)
    pnl1: TPanel;
    pnl2: TPanel;
    STRGConvenio: TStringGrid;
    pnl3: TPanel;
    btCancelar: TSpeedButton;
    lb1: TLabel;
    pnlRodape: TPanel;
    Img1: TImage;
    btOK: TSpeedButton;
    procedure STRGConvenioKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure BtOkClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure STRGConvenioDblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FescolheConvenio: TFescolheConvenio;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFescolheConvenio.STRGConvenioKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then bTOKCLICK(SENDER);
end;

procedure TFescolheConvenio.FormShow(Sender: TObject);
begin
     Self.Tag:=-1;
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
end;

procedure TFescolheConvenio.BtOkClick(Sender: TObject);
begin
     Self.Tag:=STRGConvenio.Row;
     Self.Close;
end;

procedure TFescolheConvenio.BtCancelarClick(Sender: TObject);
begin
     Self.Tag:=-1;
     Self.Close;
end;

procedure TFescolheConvenio.STRGConvenioDblClick(Sender: TObject);
begin
     Self.Tag:=STRGConvenio.Row;
     Self.Close;
end;

end.
