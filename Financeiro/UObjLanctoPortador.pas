unit UObjLanctoPortador;
Interface
Uses ibquery,Windows,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjTipoLanctoPortador,UobjPortador,uobjlancamento,uobjvalores,uobjtransferenciaportador,uobjvalorestransferenciaportador;

Type
   TObjLanctoPortador=class

          Public
                TipoLancto                    :TobjTipoLAnctoPortador;
                Portador                      :TObjPortador;
                Lancamento                    :TobjLancamento;
                Valores                       :TobjValores;
                TransferenciaPortador         :TObjTransferenciaPortador;
                ValoresTransferenciaPortador  :TobjValoresTransferenciaPortador;
                
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                function    Salvar(ComCommit,ParamAtualizaPortador,LanctoManual:boolean): Boolean;overload;
                Function    Salvar(ComCommit:Boolean;ParamAtualizaPortador:boolean):Boolean;overload;

                Function    LocalizaCodigo(Parametro:string)             :boolean;
                Function    LocalizaValores(Parametro:string)             :boolean;
                Function    LocalizaTransferenciaPortador(Parametro:string)             :boolean;
                Function    LocalizaValoresTransferenciaPortador(Parametro:string)             :boolean;
                Function    LocalizaDados(PTabela:string;PvalordoCampo,PLancamento:string):boolean;

                Function    exclui(Pcodigo:string;ComCommit:boolean)      :Boolean;
                function    excluiTodosValoresTransferenciaPortadpr(PCodigoValorTrans: string; ComCommit: boolean): Boolean;

                Function    Get_Pesquisa                                :string;
                Function    Get_TituloPesquisa                          :string;
                Function    Get_PesquisatipoLanctoPortador              :string;
                Function    Get_TituloPesquisaTipoLanctoPortador        :string;
                Function    Get_PesquisaPortador                        :string;
                Function    Get_TituloPesquisaPortador                  :string;

                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_Portador         :string;
                Function Get_TipoLancto       :string;
                Function Get_Historico        :string;
                Function Get_Data             :string;
                Function Get_Valor            :string;
                Function Get_TabelaGeradora   :string;
                Function Get_ObjetoGerador    :string;
                Function Get_CampoPrimario    :string;
                Function Get_ValordoCampo     :string;
                Function Get_ImprimeRel       :string;

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_Portador         (parametro:string);
                Procedure Submit_TipoLancto       (parametro:string);
                Procedure Submit_Historico        (parametro:string);
                Procedure Submit_Data             (parametro:string);
                Procedure Submit_Valor            (parametro:string);
                Procedure Submit_TabelaGeradora   (parametro:string);
                Procedure Submit_ObjetoGerador    (parametro:string);
                Procedure Submit_CampoPrimario    (parametro:string);
                Procedure Submit_ValordoCampo     (parametro:string);
                Procedure Submit_ImprimeRel       (parametro:Boolean);

                Procedure Submit_UsuarioAutorizou (parametro:String);
                Procedure Submit_Autorizacao (parametro:String);

                Function  Get_AUTORIZACAO          :string;
                Function  Get_UsuarioAutorizou          :string;


                Function  Get_NovoCodigo:string;
                Function PegaNometipolancto(parametro:string):string;

                Function AumentaSaldo(ParametroPortador:string;ParametroValor:string;ComCommit:boolean):Boolean;
                Function DiminuiSaldo(ParametroPortador:string;ParametroValor:string;ComCommit:boolean):Boolean;
                Procedure Ajeitahistoricos;
                function ExcluiporObjeto(TmpObjgerador, Tmpvalordocampo: string;ComCommit: Boolean): Boolean;
                Function Zeracampolancamento(Plancamento:string):boolean;
                procedure edtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                procedure edttipolanctoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
                Procedure ImprimeRecibo(Pcodigo:string);
                function ValidaData: boolean;

         Private
               ObjDataset:Tibdataset;
               ObjQueryLocal:Tibquery;
               CODIGO           :String[09];

               Historico        :string;
               Data             :String[10];
               Valor            :String[10];
               ImprimeRel       :String[1];

               TabelaGeradora   :String[50];
               ObjetoGerador    :String[50];
               CampoPrimario    :String[50];
               ValordoCampo     :String[50];

               UsuarioAutorizou :String;
               AUTORIZACAO      :String;



               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               //Function  AtualizaPortador:boolean;
               Function  LancaContabilidade:boolean;
               procedure EDTCODIGOPLANODECONTASKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
               function  ExportaContabilidade(PTipo:string): boolean;
               function  PegaCodigoPlanodeContas: string;
               function  Exclui_e_RetornaSaldo(PCodigo: string;ComCommit: Boolean): Boolean;
               Function  ExcluiContabilidade:Boolean;



   End;

implementation
uses stdctrls,SysUtils,Dialogs,UDatamodulo,Controls,
Ufiltraimp,Upesquisa,uplanodecontas,Uobjexportacontabilidade,
  Uportador, UTipoLanctoPortador, URelPedidoRdPrint,printers,rdprint,
  DateUtils;



{ TTabTitulo }


Procedure  TObjLanctoPortador.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring          ;
        Self.Historico        :=FieldByName('Historico').asstring       ;
        Self.Data             :=FieldByName('Data').asstring            ;
        Self.Valor            :=FieldByName('Valor').asstring           ;
        //TipoLancto       :TobjTipoLAnctoPortador;

        Self.TabelaGeradora   :=FieldByName('TabelaGeradora').asstring;
        Self.ObjetoGerador    :=FieldByName('ObjetoGerador').asstring;
        Self.CampoPrimario    :=FieldByName('CampoPrimario').asstring;
        Self.ValordoCampo     :=FieldByName('ValordoCampo').asstring;  
        Self.ImprimeRel       :=FieldByName('ImprimeRel').asstring;

        Self.AUTORIZACAO      :=Fieldbyname('autorizacao').asstring;
        Self.UsuarioAutorizou :=Fieldbyname('usuarioautorizou').asstring;


        
        If (Self.TipoLancto.LocalizaCodigo(FieldByName('TipoLancto').asstring)=False)
        Then Begin
                  Messagedlg('Tipo de Lancto de Portador N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.TipoLancto.TabelaparaObjeto;

        If (Self.Portador.LocalizaCodigo(FieldByName('portador').asstring)=False)
        Then Begin
                  Messagedlg('Portador N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.Portador.TabelaparaObjeto;

        If (FieldByName('lancamento').asstring<>'')
        Then Begin
                If (Self.Lancamento.LocalizaCodigo(FieldByName('Lancamento').asstring)=False)
                Then Begin
                        Messagedlg('Lan�amento N�o encontrado!',mterror,[mbok],0);
                        Self.ZerarTabela;
                        exit;
                End
                Else Self.Lancamento.TabelaparaObjeto;
        End;


        If (FieldByName('valores').asstring<>'')
        Then Begin
                If (Self.valores.LocalizaCodigo(FieldByName('valores').asstring)=False)
                Then Begin
                        Messagedlg('Valores N�o encontrado!',mterror,[mbok],0);
                        Self.ZerarTabela;
                        exit;
                End
                Else Self.valores.TabelaparaObjeto;
        End;

        If (FieldByName('TransferenciaPortador').asstring<>'')
        Then Begin
                If (Self.TransferenciaPortador.LocalizaCodigo(FieldByName('TransferenciaPortador').asstring)=False)
                Then Begin
                        Messagedlg('TransferenciaPortador N�o encontrado!',mterror,[mbok],0);
                        Self.ZerarTabela;
                        exit;
                End
                Else Self.TransferenciaPortador.TabelaparaObjeto;
        End;

        If (FieldByName('ValoresTransferenciaPortador').asstring<>'')
        Then Begin
                If (Self.ValoresTransferenciaPortador.LocalizaCodigo(FieldByName('ValoresTransferenciaPortador').asstring)=False)
                Then Begin
                        Messagedlg('ValoresTransferenciaPortador N�o encontrado!',mterror,[mbok],0);
                        Self.ZerarTabela;
                        exit;
                End
                Else Self.ValoresTransferenciaPortador.TabelaparaObjeto;
        End;


     End;
end;


Procedure TObjLanctoPortador.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring           :=Self.CODIGO                    ;
      FieldByName('Historico').asstring        :=Self.Historico                 ;
      FieldByName('Data').asstring             :=Self.Data                      ;
      FieldByName('TipoLAncto').asstring       :=Self.TipoLancto.Get_CODIGO     ;
      FieldByName('Portador').asstring         :=Self.Portador.Get_CODIGO       ;
      FieldByName('Valor').asstring            :=Self.valor                     ;
      FieldByName('TabelaGeradora').asstring   :=Self.TabelaGeradora            ;
      FieldByName('ObjetoGerador').asstring    :=Self.ObjetoGerador             ;
      FieldByName('CampoPrimario').asstring    :=Self.CampoPrimario             ;
      FieldByName('ValordoCampo').asstring     :=Self.ValordoCampo              ;
      FieldByName('ImprimeRel').asstring       :=Self.ImprimeRel                ;
      FieldByName('Lancamento').asstring       :=Self.Lancamento.Get_codigo     ;
      FieldByName('valores').asstring       :=Self.Valores.Get_codigo     ;
      FieldByName('transferenciaportador').asstring       :=Self.transferenciaportador.Get_codigo     ;
      FieldByName('valorestransferenciaportador').asstring       :=Self.valorestransferenciaportador.Get_codigo ;
      
      Fieldbyname('autorizacao').asstring     :=Self.AUTORIZACAO;
      Fieldbyname('usuarioautorizou').asstring:=Self.UsuarioAutorizou;
  End;
End;

//***********************************************************************

function TObjLanctoPortador.Salvar(ComCommit:Boolean;ParamAtualizaPortador:boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If (ParamAtualizaPortador=True)
 Then Begin
         (*If (Self.AtualizaPortador=False)
         Then Begin
                   Result:=False;
                   If ComCommit=True
                   Then FDataModulo.IBTransaction.RollbackRetaining;
                   exit;
              End;*)
      End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

function TObjLanctoPortador.Salvar(ComCommit,ParamAtualizaPortador,LanctoManual:boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;



if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If (ParamAtualizaPortador=True)
 Then Begin
         (*If (Self.AtualizaPortador=False)
         Then Begin
                   Result:=False;
                   If ComCommit=True
                   Then FDataModulo.IBTransaction.RollbackRetaining;
                   exit;
              End;*)
         If(LanctoManual=True)
         Then Begin
                   If (Self.LancaContabilidade=False)
                   Then Begin
                                Result:=False;
                                If ComCommit=True
                                Then FDataModulo.IBTransaction.RollbackRetaining;
                                exit;
                   End;


              End;
      End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 if (Self.Status=dsinsert)
 Then begin
           If (Self.TipoLancto.Get_ImprimeRecibo='S')
           Then Self.ImprimeRecibo(Self.Codigo);
 end;


 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjLanctoPortador.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO             :='';
        Self.Historico          :='';
        Self.Data               :='';
        Self.Valor              :='';
        Self.TipoLancto.ZerarTabela;        
        Self.Portador.ZerarTabela;
        Self.TabelaGeradora     :='';
        Self.ObjetoGerador      :='';
        Self.CampoPrimario      :='';
        Self.ValordoCampo       :='';
        Self.ImprimeRel         :='';
        Self.Lancamento.ZerarTabela;
        Self.Valores.ZerarTabela;
        Self.TransferenciaPortador.ZerarTabela;
        Self.ValoresTransferenciaPortador.ZerarTabela;
        Self.AUTORIZACAO:='';
        Self.UsuarioAutorizou:='';

     End;
end;

Function TObjLanctoPortador.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Valor='')
       Then Mensagem:=mensagem+'/Valor';

       If (TipoLancto.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Tipo de Lancto em Portadores';

       If (Portador.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Portador';

       If (ImprimeRel='')
       Then Mensagem:=mensagem+'/Indica��o de Impress�o de Relat�rio';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjLanctoPortador.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.TipoLancto.LocalizaCodigo(Self.TipoLancto.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Tipo de Lancto em Portadores n�o Encontrado!'
     Else Self.TipoLancto.TabelaparaObjeto;

     If (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';

     if (Self.Lancamento.Get_CODIGO<>'')
     Then Begin
               If (Self.Lancamento.LocalizaCodigo(Self.Lancamento.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Lan�amento n�o Encontrado!';
     end;

     if (Self.valores.Get_CODIGO<>'')
     Then Begin
               If (Self.valores.LocalizaCodigo(Self.valores.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Valores n�o Encontrado!';
     end;


     if (Self.transferenciaPortador.Get_CODIGO<>'')
     Then Begin
               If (Self.transferenciaPortador.LocalizaCodigo(Self.transferenciaPortador.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ TransferenciaPortador n�o Encontrado!';
     end;

     if (Self.valorestransferenciaPortador.Get_CODIGO<>'')
     Then Begin
                 If (Self.valorestransferenciaPortador.LocalizaCodigo(Self.valorestransferenciaPortador.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ ValorestransferenciaPortador n�o Encontrado!';
     end;


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjLanctoPortador.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Reais:=StrtoFloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;


     try
        Inteiros:=Strtoint(Self.TipoLancto.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Tipo de Lancto em Portadores';
     End;

     try
        if (Self.Lancamento.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.Lancamento.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Lan�amento';
     End;

     try
        if (Self.valores.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.valores.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Valores';
     End;


     try
        if (Self.transferenciaPortador.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.transferenciaPortador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/transferenciaPortador';
     End;


     try
        if (Self.valorestransferenciaPortador.Get_CODIGO<>'')
        Then Inteiros:=Strtoint(Self.valorestransferenciaPortador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/valorestransferenciaPortador';
     End;

     try
        Inteiros:=Strtoint(Self.Portador.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Portador';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjLanctoPortador.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

{     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjLanctoPortador.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';
     If ((Self.ImprimeRel<>'S') and (Self.ImprimeRel<>'N'))
     then mensagem:=mensagem+'Vari�vel Impress�o de Relat�rio Inv�lida';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;





function TObjLanctoPortador.LocalizaTransferenciaPortador(
  Parametro: string): boolean;
begin


       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador');
           SelectSql.add(',AUTORIZACAO,UsuarioAutorizou  from tablanctoportador where  Valores='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjLanctoPortador.LocalizaValores(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador');
           SelectSql.add(',AUTORIZACAO,UsuarioAutorizou  from tablanctoportador where  Valores='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjLanctoPortador.excluiTodosValoresTransferenciaPortadpr(PCodigoValorTrans:string;ComCommit: boolean): Boolean;
begin

     result:=False;
     With Self.ObjQueryLocal do
     Begin
          close;
          sql.clear;
          Sql.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador');
          Sql.add(',AUTORIZACAO,UsuarioAutorizou  from tablanctoportador where  ValoresTransferenciaPortador='+PCodigoValorTrans);
          open;
          first;
          while not(eof) do
          Begin
               if (Self.exclui(fieldbyname('codigo').asstring,ComCommit)=False)
               Then exit;
               next;
          End;
          result:=true;
     End;
end;


function TObjLanctoPortador.LocalizaValoresTransferenciaPortador(
  Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador');
           SelectSql.add(',AUTORIZACAO,UsuarioAutorizou  from tablanctoportador where  ValoresTransferenciaPortador='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjLanctoPortador.LocalizaDados(PTabela: string; PvalordoCampo,
  PLancamento: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,valores,transferenciaportador,valorestransferenciaportador,AUTORIZACAO,UsuarioAutorizou from tablanctoportador ');
           SelectSql.add('where TabelaGeradora='+#39+PTabela+#39+' and valordocampo='+#39+PvalordoCampo+#39+' and Lancamento='+plancamento);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;


end;

function TObjLanctoPortador.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador');
           SelectSql.add(',AUTORIZACAO,UsuarioAutorizou  from tablanctoportador where  codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjLanctoPortador.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjLanctoPortador.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     result:=False;

     Try
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                  Self.TabelaparaObjeto;

                  if (self.ExcluiContabilidade=False)
                  Then exit;

                  (*if ((Self.TipoLancto.Get_Debito_Credito='C') and (Self.ImprimeRel='S'))
                  Then  Self.Portador.DebitaSaldo(Self.Portador.Get_CODIGO,Self.Valor,ComCommit)
                  Else  Self.Portador.CreditaSaldo(Self.Portador.Get_CODIGO,Self.Valor,ComCommit);
                  *)

                  
                  Self.ObjDataset.delete;
                  If (ComCommit=True)
                  Then FDataModulo.IBTransaction.CommitRetaining;
                  result:=True;
                  exit;
        End;
     Except
           If (ComCommit=True)
           Then FDataModulo.IBTransaction.RollbackRetaining;
     End;
end;

function TObjLanctoPortador.ExcluiporObjeto(TmpObjgerador,Tmpvalordocampo:string;ComCommit:Boolean): Boolean;
begin
     result:=False;
     //localiza os lancamento do objeto com o valor do campo
     //e exclui retornando o saldo do portador
     With ObjQueryLocal do
     Begin
          close;
          sql.clear;
          SQL.add('select CODIGO from tablanctoportador WHERE');
          SQL.add('OBJETOGERADOR='+#39+TMPOBJGERADOR+#39' AND');
          SQL.add('CAMPOPRIMARIO=''CODIGO'' AND VALORDOCAMPO='+#39+Tmpvalordocampo+#39);
          open;
          if recordcount=0
          Then Begin//naum tem nada
                    result:=True;
                    exit;
          End;
          first;
          While not(eof) do
          Begin
               If (Self.Exclui_e_RetornaSaldo(fieldbyname('codigo').asstring,comcommit)=False)
               Then exit;
               next;
          End;
          result:=True;

     End;



end;

function TObjLanctoPortador.Exclui_e_RetornaSaldo(PCodigo:string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.TabelaparaObjeto;
                 (*If (Self.TipoLancto.Get_Debito_Credito='C')
                 Then Begin
                        If (self.Portador.debitaSaldo(Self.Portador.Get_codigo,Self.valor,False)=False)
                        Then Begin
                                Messagedlg('Portador n�o Atualizado!',mterror,[mbok],0);
                                result:=False;
                                exit;
                        End;
                 End
                 Else Begin
                        If (Self.TipoLancto.Get_Debito_Credito='D')
                        Then Begin
                                If (self.Portador.CreditaSaldo(Self.Portador.Get_codigo,Self.valor,False)=False)
                                Then Begin
                                        Messagedlg('Portador n�o Atualizado!',mterror,[mbok],0);
                                        result:=False;
                                        exit;
                                End;
                        End;
                End;
                *)
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjLanctoPortador.create;
begin
        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.TipoLancto:=TObjTipoLanctoPortador.create;
        Self.Portador:=TObjPortador.create;
        Self.Lancamento:=TobjLancamento.create;
        Self.Valores                       :=TobjValores.create;
        Self.TransferenciaPortador         :=TObjTransferenciaPortador.create;
        Self.ValoresTransferenciaPortador  :=TobjValoresTransferenciaPortador.create;


        Self.ObjQueryLocal:=Tibquery.create(nil);
        Self.ObjQueryLocal.database:=Fdatamodulo.ibdatabase;

        ZerarTabela;
        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador');
                SelectSql.add(',AUTORIZACAO,UsuarioAutorizou  from tablanctoportador where codigo=-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('insert into tablanctoportador (codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador,AUTORIZACAO,UsuarioAutorizou) values ');
                InsertSQL.add('(:codigo,:historico,:tipolancto,:portador,:data,:valor,:TabelaGeradora,:ObjetoGerador,:CampoPrimario,:ValordoCampo,:ImprimeRel,:lancamento,:Valores,:TransferenciaPortador,:ValoresTransferenciaPortador,:AUTORIZACAO,:UsuarioAutorizou) ');

                ModifySQL.clear;
                ModifySQL.add(' Update Tablanctoportador set codigo=:codigo,historico=:historico,tipolancto=:tipolancto,portador=:portador,data=:data,valor=:valor,');
                ModifySQL.add(' TabelaGeradora=:TabelaGeradora,ObjetoGerador=:ObjetoGerador,');
                ModifySQL.add(' CampoPrimario=:CampoPrimario,ValordoCampo=:ValordoCampo,ImprimeRel=:ImprimeRel,lancamento=:lancamento,Valores=:Valores,TransferenciaPortador=:TransferenciaPortador,ValoresTransferenciaPortador=:ValoresTransferenciaPortador');
                ModifySQL.add(' ,AUTORIZACAO=:AUTORIZACAO,UsuarioAutorizou=:UsuarioAutorizou');
                ModifySQL.add(' where codigo=:codigo');


                DeleteSQL.clear;
                DeleteSQL.add(' Delete from TabLanctoPortador where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add('Select codigo,historico,tipolancto,portador,data,valor,TabelaGeradora,ObjetoGerador,CampoPrimario,ValordoCampo,ImprimeRel,lancamento,Valores,TransferenciaPortador,ValoresTransferenciaPortador');
                RefreshSQL.add(',AUTORIZACAO,UsuarioAutorizou  from tablanctoportador where codigo=-1');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjLanctoPortador.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjLanctoPortador.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjLanctoPortador.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjLanctoPortador.Get_Pesquisa: string;
begin
     Result:=' Select * from TabLanctoPortador ';
end;

function TObjLanctoPortador.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Lan�amento em Portadores ';
end;

function TObjLanctoPortador.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_LANCTOPORTADOR' ;
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o TabLanctoPortador',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


function TObjLanctoPortador.Get_Data: string;
begin
     Result:=self.data;
end;

function TObjLanctoPortador.Get_Historico: string;
begin
     Result:=Self.Historico;
end;

function TObjLanctoPortador.Get_PesquisatipoLanctoPortador: string;
begin
     Result:=Self.TipoLancto.Get_Pesquisa;
end;

function TObjLanctoPortador.Get_TipoLancto: string;
begin
     Result:=Self.TipoLancto.get_codigo;
end;

function TObjLanctoPortador.Get_TituloPesquisaTipoLanctoPortador: string;
begin
     Result:=Self.TipoLancto.Get_TituloPesquisa;
end;

procedure TObjLanctoPortador.Submit_Data(parametro: string);
begin
     Self.data:=Parametro;
end;

procedure TObjLanctoPortador.Submit_Historico(parametro: string);
begin
     Self.historico:=parametro;
end;

procedure TObjLanctoPortador.Submit_TipoLancto(parametro: string);
begin
     Self.TipoLancto.Submit_CODIGO(parametro);
end;

function TObjLanctoPortador.PegaNometipolancto(parametro:string): string;
begin
     If Self.TipoLancto.LocalizaCodigo(parametro)=true
     Then Begin
                Self.TipoLancto.TabelaparaObjeto;
                result:=Self.TipoLancto.Get_Historico;
          End
     Else result:='';
end;



(*function TObjLanctoPortador.AtualizaPortador: boolean;
begin

     If (Self.TipoLancto.LocalizaCodigo(Self.TipoLancto.get_codigo)=False)
     Then Begin
               Messagedlg('Tipo de Lancto n�o Encontrado em sua Tabela!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
     Self.TipoLancto.TabelaparaObjeto;

     If (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Begin
               Messagedlg('Portador N�o Localizado em sua Tabela!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
     Self.Portador.TabelaparaObjeto;

    (* If (Self.TipoLancto.Get_Debito_Credito='C')
     Then Begin
               If (self.Portador.CreditaSaldo(Self.Portador.Get_codigo,Self.valor,False)=False)
               Then Begin
                         Messagedlg('Portador n�o Atualizado!',mterror,[mbok],0);
                         result:=False;
                         exit;
                    End;
          End
     Else Begin
             If (Self.TipoLancto.Get_Debito_Credito='D')
             Then Begin
                       If (self.Portador.DebitaSaldo(Self.Portador.Get_codigo,Self.valor,False)=False)
                       Then Begin
                                 Messagedlg('Portador n�o Atualizado!',mterror,[mbok],0);
                                 result:=False;
                                 exit;
                            End;
                  End;
          End;*
    Result:=True;
end;*)

function TObjLanctoPortador.Get_PesquisaPortador: string;
begin
     Result:=Self.portador.Get_Pesquisa;
end;

function TObjLanctoPortador.Get_Portador: string;
begin
     Result:=Self.portador.get_codigo;
end;

function TObjLanctoPortador.Get_TituloPesquisaPortador: string;
begin
     Result:=Self.portador.Get_TituloPesquisa;
end;

procedure TObjLanctoPortador.Submit_Portador(parametro: string);
begin
     Self.Portador.Submit_CODIGO(parametro);
end;

function TObjLanctoPortador.Get_Valor: string;
begin
     Result:=Self.valor;
end;

procedure TObjLanctoPortador.Submit_Valor(parametro: string);
begin
     Self.valor:=parametro;
end;

function TObjLanctoPortador.Get_CampoPrimario: string;
begin
     Result:=self.CampoPrimario;
end;

function TObjLanctoPortador.Get_ObjetoGerador: string;
begin
     Result:=self.ObjetoGerador;
end;

function TObjLanctoPortador.Get_TabelaGeradora: string;
begin
     Result:=self.TabelaGeradora
end;

function TObjLanctoPortador.Get_ValordoCampo: string;
begin
     Result:=self.ValordoCampo
end;

procedure TObjLanctoPortador.Submit_CampoPrimario(parametro: string);
begin
     Self.CampoPrimario:=parametro;
end;

procedure TObjLanctoPortador.Submit_ObjetoGerador(parametro: string);
begin
        self.ObjetoGerador:=Parametro;
end;

procedure TObjLanctoPortador.Submit_TabelaGeradora(parametro: string);
begin
        self.TabelaGeradora:=Parametro;
end;

procedure TObjLanctoPortador.Submit_ValordoCampo(parametro: string);
begin
     self.ValordoCampo:=Parametro;
end;


function TObjLanctoPortador.ValidaData:boolean;
var
   Pdata:Tdate;
   pautorizacao,pautorizou:String;
   CarenciaRetroativa, CarenciaFutura:Integer;
begin
     result:=False;
     
     Pdata:=RetornaDataServidor;
     Self.Submit_UsuarioAutorizou('');
     Self.submit_autorizacao('');
     pautorizou:='';
     pautorizacao:='';


     if (strtodate(Self.Data)=Pdata)
     Then Begin
              result:=True;
              exit;
     End;

     if  (VerificaDataMenorLancamentoFinanceiro_global=False)
     and (verificaDataMaiorLancamentoFinanceiro_global=false)
     then Begin
               result:=true;
               exit;
     End;

     CarenciaRetroativa := 0;
     CarenciaFutura:=0;

     if (ObjParametroGlobal.ValidaParametro('CARENCIA LANCAMENTO FUTURO NO FINANCEIRO')=true)
     then Begin
            try
                CarenciaFutura:=StrToInt(ObjParametroGlobal.Get_Valor);
            except
                MensagemErro('Valor Incorreto para carencia futura lancamento financeiro');
                exit;
            end;
      end;

     if (ObjParametroGlobal.ValidaParametro('CARENCIA LANCAMENTO RETROATIVO NO FINANCEIRO')=true)
     then Begin
            try
                CarenciaRetroativa:=StrToInt(ObjParametroGlobal.Get_Valor);
            except
                MensagemErro('Valor Incorreto para carencia retroativa lancamento financeiro');
                exit;
            end;
     end;

     if ((IncDay(strtodate(Self.Data),CarenciaRetroativa)<Pdata) and (VerificaDataMenorLancamentoFinanceiro_global=true))
     Then Begin
               if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('LAN�AMENTO RETROATIVO NO FINANCEIRO')=False)
               Then Begin
                         //senha
                         if (ObjPermissoesUsoGlobal.PedePermissao(ObjPermissoesUsoGlobal.get_codigo,pautorizou,'Senha lan�amento retroativo')=false)
                         Then exit;

                         Pautorizacao:='LANCTO DATA RETROATIVA';
               End;
     End;

     if ((strtodate(Self.Data)> IncDay(Pdata,CarenciaFutura)) and (verificaDataMaiorLancamentoFinanceiro_global=true))
     Then Begin
               if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('LAN�AMENTO FUTURO NO FINANCEIRO')=False)
               Then Begin
                         //senha
                         if (ObjPermissoesUsoGlobal.PedePermissao(ObjPermissoesUsoGlobal.get_codigo,pautorizou,'Senha lan�amento futuro')=false)
                         Then exit;

                         Pautorizacao:='LANCTO DATA FUTURA';
               End;
     End;

     Self.Submit_UsuarioAutorizou(Pautorizou);
     Self.Submit_Autorizacao(pautorizacao);

     result:=True;
End;

function TObjLanctoPortador.AumentaSaldo(ParametroPortador,
  ParametroValor: string; ComCommit: boolean): Boolean;
BEgin

     result:=False;

     if (self.ValidaData=False) // Essa func�o � para naum deixar o usuario fazer lancamento retroativo ou futuro sem ter permissao
     Then exit;

     If Self.TipoLancto.LocalizaHistorico('CR�DITO GERADO PELO SISTEMA')=false
     Then Begin
               Messagedlg('TIPO DE LANCTO EM PORTADOR-> "CR�DITO GERADO PELO SISTEMA". N�O ENCONTRADO',mterror,[mbok],0);
               exit;

     End;
     Self.TipoLancto.TabelaparaObjeto;

     Self.Status:=dsinsert;
     Self.codigo:=Self.Get_NovoCodigo;
     Self.Portador.submit_codigo(ParametroPortador);
     Self.Valor:=ParametroValor;



     If (Self.Salvar(Comcommit,true)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Lancto em Portadores!Atualiza��o de Saldo Cancelada!',mterror,[mbok],0);
               Self.Status:=dsInactive;
               exit;
     End;

     RESULT:=true;
End;

function TObjLanctoPortador.DiminuiSaldo(ParametroPortador,
  ParametroValor: string; ComCommit: boolean): Boolean;
begin
     result:=False;

     if (self.ValidaData=False)// Essa func�o � para naum deixar o usuario fazer lancamento retroativo ou futuro sem ter permissao
     Then exit;

     If Self.TipoLancto.LocalizaHistorico('D�BITO GERADO PELO SISTEMA')=false
     Then Begin
               Messagedlg('TIPO DE LANCTO EM PORTADOR -> "D�BITO GERADO PELO SISTEMA". N�O ENCONTRADO',mterror,[mbok],0);
               exit;

     End;
     Self.TipoLancto.TabelaparaObjeto;

     Self.Status:=dsinsert;
     Self.codigo:=Self.Get_NovoCodigo;
     Self.Portador.submit_codigo(ParametroPortador);
     Self.Valor:=ParametroValor;



     If (Self.Salvar(Comcommit,true)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Lancto em Portadores!Atualiza��o de Saldo Cancelada!',mterror,[mbok],0);
               Self.Status:=dsInactive;
               exit;
     End;

     result:=True;

end;


function TObjLanctoPortador.Get_ImprimeRel: string;
begin
     Result:=Self.imprimerel;
end;

procedure TObjLanctoPortador.Submit_ImprimeRel(parametro: Boolean);
begin

     If parametro=False
     Then Self.ImprimeRel:='N'
     Else Self.ImprimeRel:='S';
     
end;

destructor TObjLanctoPortador.Free;
begin
    Freeandnil(Self.ObjDataset);
    Self.TipoLancto.free;
    Self.Portador.free;
    Self.Lancamento.free;
    Self.Valores.free;
    Self.TransferenciaPortador.free;
    Self.ValoresTransferenciaPortador.free;

    freeandnil(Self.objquerylocal);
end;

function TObjLanctoPortador.LancaContabilidade: boolean;
begin
     Result:=True;
     If (ObjParametroGlobal.ValidaParametro('PLANO DE CONTAS PARA LANCTO EM PORTADORES?')=False)
     Then Begin
               Messagedlg('O Parametro "PLANO DE CONTAS PARA LANCTO EM PORTADORES?" com valor="SIM/N�O" n�o foi encontrado!',mterror,[mbok],0);
               Result:=False;
               exit;
     End;

     If (Self.TipoLancto.LocalizaCodigo(Self.TipoLancto.get_codigo)=False)
     Then Begin
               Messagedlg('O Tipo de Lancto n�o foi localizado!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
     Self.TipoLancto.TabelaparaObjeto;

     Result:=Self.ExportaContabilidade(Self.TipoLancto.Get_Debito_Credito);
End;

function TObjLanctoPortador.ExportaContabilidade(Ptipo:string): boolean;
var
PCPortador,PCLancto:string;
NomePCPortador,NomePclancto:string;
ObjExportaContabilidade:TObjExportaContabilidade;
begin
     result:=False;

     If (Self.Portador.LocalizaCodigo(self.portador.get_codigo)=False)
     Then Begin
               Messagedlg('Portador n�o localizado!',mterror,[mbok],0);
               exit;
     End;
     //pegando os dados do plano de contas do portador
     Self.Portador.TabelaparaObjeto;
     PCPortador:=Self.Portador.Get_CodigoPlanodeContas;
     NomePCPortador:='';
     if (PCPortador<>'')
     Then Begin
                If (Self.Portador.PlanodeContas.LocalizaCodigo(PCPortador)=True)
                Then Begin
                          Self.Portador.PlanodeContas.TabelaparaObjeto;
                          NomePCPortador:=Self.Portador.PlanodeContas.Get_Nome;
                     End
                Else PCPortador:='0';
     End
     Else PCPortador:='0';



     Try
        ObjExportaContabilidade:=TObjExportaContabilidade.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Exporta��o de Contabilidade!',mterror,[mbok],0);
           exit;
     End;


     Try
        //Pegando os dados do plano de contas do lancto
        PCLancto:='0';
        NomePclancto:='';
        PCLancto:=Self.TipoLancto.Get_CodigoPlanodecontas;

        If (PcLancto='')
        Then PcLancto:=PegaCodigoPlanodeContas;                                   ;

        If (Pclancto='')
        Then Pclancto:='0';

        If (PCLancto<>'0')
        Then Begin
                  If (Self.Portador.PlanodeContas.LocalizaCodigo(pclancto)=True)
                  Then Begin
                            Self.portador.PlanodeContas.TabelaparaObjeto;
                            NomePclancto:=Self.portador.PlanodeContas.Get_Nome;
                  End;
        End;

        ObjExportaContabilidade.ZerarTabela;

        ObjExportaContabilidade.Submit_Exportado('N');
        ObjExportaContabilidade.Submit_ObjGerador('OBJLANCTOPORTADOR');
        ObjExportaContabilidade.Submit_CodGerador(Self.codigo);

        If (PCPortador='0') or (PCLancto='0')
        Then ObjExportaContabilidade.Submit_Exporta('N')
        Else ObjExportaContabilidade.Submit_Exporta('S');
        
        ObjExportaContabilidade.Submit_CODIGO(ObjExportaContabilidade.Get_NovoCodigo);
        ObjExportaContabilidade.Submit_Data(Self.Data);
        ObjExportaContabilidade.Submit_Valor(Self.VALOR);
        ObjExportaContabilidade.Submit_Historico(Self.HISTORICO);

        If (Ptipo='C')
        Then Begin
                ObjExportaContabilidade.Submit_ContaCredite(PCLancto);
                ObjExportaContabilidade.Submit_ContaDebite(PCPortador);
        End
        Else Begin
                ObjExportaContabilidade.Submit_ContaCredite(PCPortador);
                ObjExportaContabilidade.Submit_ContaDebite(PcLancto);
        End;
        ObjExportaContabilidade.Status:=dsinsert;
        Result:=ObjExportaContabilidade.Salvar(False);
        ObjExportaContabilidade.Status:=dsInactive;
     Finally
        ObjExportaContabilidade.free;
     End;
End;

procedure TObjLanctoPortador.EDTCODIGOPLANODECONTASKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(NIL);
            FplanodeCOntas:=TFplanodeCOntas.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Portador.Get_PesquisaPlanodeContas,Self.Portador.Get_TituloPesquisaPlanodeContas,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;

function TObjLanctoPortador.PegaCodigoPlanodeContas: string;
begin
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;

          edtgrupo01.EditMask:='';
          edtgrupo01.OnKeyDown:=Self.EDTCODIGOPLANODECONTASKeyDown;
          LbGrupo01.caption:='COD PCTS';

          showmodal;

          If (tag=0)
          Then result:='0'
          Else result:=edtgrupo01.text;
     End;
end;


procedure TObjLanctoPortador.Ajeitahistoricos;
var
QTemp:Tibquery;
historicoTmp:String;
posicao:integer;
begin
     If (Messagedlg('Certeza que deseja reprocessar os hist�ricos?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     Try
        QTemp:=TIBQuery.create(nil);
        QTemp.database:=Self.objdataset.database;
     Except
           Messagedlg('Erro na cria��o da Query!',mterror,[mbok],0);
           exit;
     End;

 Try

     With QTemp do
     Begin//dinheiro
          close;
          SQL.clear;
          SQL.add('Select codigo,historico from Tablanctoportador where imprimerel=''S'' ');
          SQL.add('and historico like ''TRSF.DIN(%'' and historico like ''%-Lct=%'' ');
          open;
          If(recordcount>0)
          Then  first;
          While not(eof) do
          Begin
               posicao:=Pos('-Lct=',fieldbyname('historico').asstring);
               If (posicao=0)
               Then next
               Else Begin
                         historicoTmp:='';
                         historicoTmp:=copy(fieldbyname('historico').asstring,posicao+4,length(fieldbyname('historico').asstring));
                         posicao:=Pos('-',historicoTmp);
                         If (posicao<>0)
                         Then historicotmp:=copy(historicotmp,posicao+1,length(historicotmp));
                         //showmessage(fieldbyname('historico').asstring+#13+'FINAL= '+historicotmp);
                         If (Self.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
                         Then Begin
                                   Messagedlg('Lancto Portador n�o encontrado!',mterror,[mbok],0);
                                   exit;
                         End;
                         Self.TabelaparaObjeto;
                         Self.Status:=Dsedit;
                         Self.historico:=HistoricoTmp;
                         If (Self.Salvar(True,False,False)=False)
                         Then Begin
                                   Messagedlg('Erro na tentativa de salvar o registro '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                                   exit;
                        End;
                        next;
               End;
          End;
     End;


     With QTemp do
     Begin//cheques
          close;
          SQL.clear;
          SQL.add('Select codigo,historico from Tablanctoportador where imprimerel=''S'' ');
          SQL.add('and historico like ''TRSF.CH(%'' and historico like ''%)N�%'' ');
          open;
          If (recordcount=0)
          Then exit;
          first;
          While not(eof) do
          Begin
               posicao:=Pos('-',fieldbyname('historico').asstring);
               If (posicao=0)
               Then next
               Else Begin
                         historicoTmp:='';
                         historicoTmp:=copy(fieldbyname('historico').asstring,posicao+1,length(fieldbyname('historico').asstring));
                         //showmessage(fieldbyname('historico').asstring+#13+'FINAL= '+historicotmp);
                         If (Self.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
                         Then Begin
                                   Messagedlg('Lancto Portador n�o encontrado!',mterror,[mbok],0);
                                   exit;
                         End;
                         Self.TabelaparaObjeto;
                         Self.Status:=Dsedit;
                         Self.historico:=HistoricoTmp;
                         If (Self.Salvar(True,False,False)=False)
                         Then Begin
                                   Messagedlg('Erro na tentativa de salvar o registro '+fieldbyname('codigo').asstring,mterror,[mbok],0);
                                   exit;
                         End;
                         next;
               End;
          End;
     End;

 Finally
       FreeAndNil(QTemp);
 End;


End;



function TObjLanctoPortador.Zeracampolancamento(
  Plancamento: string): boolean;
begin
     result:=False;
     With Self.ObjDataset do
     Begin
          Close;
          SelectSQL.clear;
          SelectSQL.add('Update TabLanctoPortador set lancamento=null where lancamento='+Plancamento);
          Try
            ExecSQL;
          Except
                exit;
          End;
     End;
     result:=True;
end;


procedure TObjlanctoPortador.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fportador:=TFportador.create(nil);
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.portador.Get_Pesquisa,Self.portador.Get_TituloPesquisa,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(Fportador);
     End;


end;

procedure TObjlanctoPortador.edttipolanctoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FtipoLanctoPortador:TFtipolanctoportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            FtipoLanctoPortador:=TFtipolanctoportador.create(nil);
            Fpesquisalocal:=Tfpesquisa.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.TipoLancto.Get_Pesquisa,Self.TipoLancto.Get_TituloPesquisa,Ftipolanctoportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FtipoLanctoPortador);
     End;


end;


procedure TObjLanctoPortador.ImprimeRecibo(Pcodigo: string);
var
linha:integer;
begin
     if (Pcodigo='')
     Then Begin
               Messagedlg('Escolha o Lan�amento que deseja emitir o Recibo',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.LocalizaCodigo(pcodigo)=False)
     Then begin
               Messagedlg('Lan�amento N� '+pcodigo+' n�o foi encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     With FRelPedidoRdPrint do
     Begin
          //preparando a impress�o
          ConfiguraImpressao;
          imprimenumeropagina:=False;
          RDprint1.Orientacao:=poPortrait;
          rdprint1.Abrir;
          if (RDprint1.Setup=False)
          Then Begin
                    RDprint1.Fechar;
                    exit;
          End;
          linha            := 3;                                  
          rdprint1.ImpC(linha,45,'RECIBO DE LANCTO EM PORTADORES',[negrito]);
          inc(linha,3);

          rdprint1.Impf(linha,1,'CODIGO DO LANCTO:',[negrito]);
          rdprint1.Imp (linha,20,Self.codigo);
          inc(linha,2);
          rdprint1.Impf(linha,1,'PORTADOR        :',[negrito]);
          rdprint1.Imp (linha,20,Self.Portador.Get_Nome);
          inc(linha,2);
          rdprint1.Impf(linha,1,'LANCTO          :',[negrito]);
          rdprint1.Imp (linha,20,Self.TipoLancto.Get_CODIGO+'-'+Self.TipoLancto.Get_Historico);
          inc(linha,2);
          rdprint1.Impf(linha,1,'D�BITO/CR�DITO  :',[negrito]);
          IF (Self.TipoLancto.Get_Debito_Credito='C')
          Then rdprint1.Imp (linha,20,'Cr�dito')
          Else rdprint1.Imp (linha,20,'D�bito');
          inc(linha,2);
          rdprint1.Impf(linha,1,'DATA            :',[negrito]);
          rdprint1.Imp (linha,20,Self.Data);
          inc(linha,2);
          rdprint1.Impf(linha,1,'HIST�RICO       :',[negrito]);
          rdprint1.Imp (linha,20,Self.Historico);
          inc(linha,2);
          rdprint1.Impf(linha,1,'VALOR           :',[negrito]);
          rdprint1.Imp (linha,20,FORMATA_VALOR(Self.Valor));
          inc(linha,2);
          rdprint1.Impf(linha,1,'USU�RIO         :',[negrito]);
          rdprint1.Imp (linha,20,ObjUsuarioGlobal.Get_nome);
          inc(linha,5);


          rdprint1.ImpF(linha,1,'____________________________________              ____________________________________',[negrito]);
          inc(linha,1);
          rdprint1.ImpF(linha,1,'         ASSINATURA USUARIO                                  ASSINATURA GERENCIA      ',[negrito]);
          inc(linha,1);
          rdprint1.fechar;
     End;
end;

function TObjLanctoPortador.ExcluiContabilidade: Boolean;
var
temp1,temp2:String;
begin
     result:=false;

     if   (Self.TransferenciaPortador.Get_CODIGO<>'')
       or (Self.Valores.Get_CODIGO<>'')
       or (Self.ValoresTransferenciaPortador.Get_CODIGO<>'')
       or (Self.Lancamento.Get_CODIGO<>'')
     Then Begin
               //como foi resultado de outro lancamento, nao tento excluir
               //a contabilidade, pois ela foi gerada por outro objeto
               result:=true;
               exit;
     end;
          
     With Self.Lancamento do
     Begin
          if (objExportaContabilidade.LocalizaGerador('OBJLANCTOPORTADOR',Self.Codigo)=True)
          Then Begin
                    objExportaContabilidade.TabelaparaObjeto;
                    if (objExportaContabilidade.Get_Exportado='S')
                    Then Begin//ja foi exportado
                             objExportaContabilidade.Submit_CODIGO(objExportaContabilidade.Get_NovoCodigo);
                             objExportaContabilidade.Status:=dsinsert;
                             temp1:=objExportaContabilidade.Get_ContaDebite;
                             temp2:=objExportaContabilidade.Get_ContaCredite;
                             objExportaContabilidade.Submit_ContaCredite(temp1);
                             objExportaContabilidade.Submit_ContaDebite(temp2);
                             temp1:=objExportaContabilidade.Get_Historico;
                             objExportaContabilidade.Submit_Historico('EXCLUS�O '+temp1);
                             if (objExportaContabilidade.Salvar(False,False)=False)
                             Then Begin
                                       Messagedlg('Erro na tentativa de Lan�ar a contabilidade de exclus�o',mterror,[mbok],0);
                                       exit;
                             End;
                    End
                    Else Begin
                              If (objExportaContabilidade.excluiporgerador('OBJLANCTOPORTADOR',Self.CODIGO)=False)
                              Then exit;
                    end;
          End;
     End;

     Result:=true;
end;

function TObjLanctoPortador.Get_AUTORIZACAO: string;
begin
     Result:=Self.AUTORIZACAO;
end;

procedure TObjLanctoPortador.Submit_UsuarioAutorizou(parametro: String);
begin
     Self.UsuarioAutorizou:=parametro;
end;

function TObjLanctoPortador.Get_UsuarioAutorizou: string;
begin
     Result:=self.UsuarioAutorizou;
end;

procedure TObjLanctoPortador.Submit_Autorizacao(parametro: String);
begin
     Self.AUTORIZACAO:=parametro;
end;

end.
