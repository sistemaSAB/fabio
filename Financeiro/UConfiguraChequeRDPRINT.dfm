object FConfiguraChequeRDPRINT: TFConfiguraChequeRDPRINT
  Left = 54
  Top = 122
  Width = 708
  Height = 344
  Caption = 'Configura'#231#227'o das Posi'#231#245'es de Impress'#227'o do  Cheque'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnDragDrop = FormDragDrop
  OnDragOver = FormDragOver
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LbValor: TLabel
    Left = 400
    Top = 20
    Width = 36
    Height = 13
    Caption = 'LbValor'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LBpagoA: TLabel
    Left = 15
    Top = 15
    Width = 44
    Height = 13
    Caption = 'LBpagoA'
    DragCursor = crSizeAll
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
    OnMouseMove = LBpagoAMouseMove
  end
  object LBDataAtual: TLabel
    Left = 15
    Top = 40
    Width = 60
    Height = 13
    Caption = 'LBDataAtual'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LBExtenso1: TLabel
    Left = 100
    Top = 70
    Width = 57
    Height = 13
    Caption = 'LBExtenso1'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LBExtenso2: TLabel
    Left = 100
    Top = 90
    Width = 57
    Height = 13
    Caption = 'LBExtenso2'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LbNominal: TLabel
    Left = 100
    Top = 120
    Width = 50
    Height = 13
    Caption = 'LbNominal'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LbCidadeCheque: TLabel
    Left = 300
    Top = 170
    Width = 33
    Height = 13
    Caption = 'Cidade'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LbDiaCheque: TLabel
    Left = 390
    Top = 170
    Width = 16
    Height = 13
    Caption = 'Dia'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LbMesCheque: TLabel
    Left = 470
    Top = 170
    Width = 20
    Height = 13
    Caption = 'Mes'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LbAnoCheque: TLabel
    Left = 570
    Top = 170
    Width = 19
    Height = 13
    Caption = 'Ano'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object LbValorCanhoto: TLabel
    Left = 15
    Top = 80
    Width = 77
    Height = 13
    Caption = 'LBValorCanhoto'
    ParentShowHint = False
    ShowHint = True
    OnClick = LBpagoAClick
  end
  object PanelPOsicoes: TPanel
    Left = 50
    Top = 190
    Width = 116
    Height = 75
    DockSite = True
    DragMode = dmAutomatic
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnFace
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object LbColuna: TLabel
      Left = 4
      Top = 7
      Width = 40
      Height = 13
      Caption = 'Coluna'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblinha: TLabel
      Left = 4
      Top = 31
      Width = 32
      Height = 13
      Caption = 'Linha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbQtdCaracteres: TLabel
      Left = 4
      Top = 55
      Width = 55
      Height = 13
      Caption = 'Qt.Carac.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtleft: TEdit
      Left = 57
      Top = 3
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 0
      Text = 'EDTLEFT'
      OnKeyPress = edtleftKeyPress
    end
    object edttop: TEdit
      Left = 57
      Top = 27
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 1
      Text = 'EDTTOP'
      OnKeyPress = edttopKeyPress
    end
    object edtquantidade: TEdit
      Left = 57
      Top = 51
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 2
      Text = 'EDTTOP'
      OnKeyPress = edtquantidadeKeyPress
    end
  end
  object ComponenteRdPrint: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Rdprint Preview'
    OpcoesPreview.PreviewZoom = 100
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = False
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Ativo
    OpcoesPreview.BotaoLer = Ativo
    OpcoesPreview.BotaoProcurar = Ativo
    Margens.Left = 10
    Margens.Right = 10
    Margens.Top = 10
    Margens.Bottom = 10
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'Exclaim Tecnologia'
    RegistroUsuario.SerieProduto = 'SINGLE-0706/01006'
    RegistroUsuario.AutorizacaoKey = 'BTXV-5778-EZCC-2224-OESY'
    About = 'RDprint 4.0c - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = True
    CorForm = clBtnFace
    CorFonte = clBlack
    Impressora = Epson
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    PortaComunicacao = 'LPT1'
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S10cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    Left = 648
    Top = 10
  end
  object MainMenu1: TMainMenu
    Left = 8
    Top = 250
    object Opes1: TMenuItem
      Caption = 'Op'#231#245'es'
      object IMPRIMEFOLHADEGUIA1: TMenuItem
        Caption = 'Imprime Folha Guia'
        OnClick = IMPRIMEFOLHADEGUIA1Click
      end
      object SalvaPosies1: TMenuItem
        Caption = 'Salva Posi'#231#245'es'
        OnClick = SalvaPosies1Click
      end
      object ImprimeTeste1: TMenuItem
        Caption = 'Imprime Teste'
        OnClick = ImprimeTeste1Click
      end
    end
  end
end
