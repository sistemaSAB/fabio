object FtalaodeCheques: TFtalaodeCheques
  Left = 247
  Top = 258
  Width = 625
  Height = 334
  Caption = 'Cadastro de Cheques de Tal'#227'o de Cheques - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 609
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 503
      Top = 0
      Width = 106
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Tal'#227'o de Cheques'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object BtNovo: TBitBtn
      Left = -1
      Top = -2
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btalterar: TBitBtn
      Left = 49
      Top = -2
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object Btgravar: TBitBtn
      Left = 99
      Top = -2
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object BtCancelar: TBitBtn
      Left = 149
      Top = -2
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 249
      Top = -2
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 299
      Top = -2
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
    end
    object btexcluir: TBitBtn
      Left = 199
      Top = -2
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btopcoes: TBitBtn
      Left = 349
      Top = -2
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
    end
    object btsair: TBitBtn
      Left = 399
      Top = -2
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 246
    Width = 609
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    DesignSize = (
      609
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 609
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 311
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 780
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 780
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 50
    Width = 609
    Height = 196
    Align = alClient
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 607
      Height = 194
      Align = alClient
      Stretch = True
      Transparent = True
    end
    object Label1: TLabel
      Left = 18
      Top = 19
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 18
      Top = 41
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 18
      Top = 64
      Width = 74
      Height = 14
      Caption = 'N'#186' do Cheque'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 18
      Top = 87
      Width = 28
      Height = 14
      Caption = 'Valor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 18
      Top = 110
      Width = 66
      Height = 14
      Caption = 'Vencimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 192
      Top = 64
      Width = 6
      Height = 14
      Caption = 'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label7: TLabel
      Left = 192
      Top = 110
      Width = 34
      Height = 14
      Caption = 'Usado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 18
      Top = 133
      Width = 94
      Height = 14
      Caption = 'Cheque/Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 18
      Top = 156
      Width = 49
      Height = 14
      Caption = 'Hist'#243'rico'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label10: TLabel
      Left = 192
      Top = 19
      Width = 28
      Height = 14
      Caption = 'Tal'#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label11: TLabel
      Left = 346
      Top = 110
      Width = 65
      Height = 14
      Caption = 'Descontado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label12: TLabel
      Left = 192
      Top = 133
      Width = 68
      Height = 14
      Caption = 'Lan'#231'amento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbnomeportador: TLabel
      Left = 192
      Top = 41
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label13: TLabel
      Left = 346
      Top = 133
      Width = 76
      Height = 14
      Caption = 'Transfer'#234'ncia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCodigo: TEdit
      Left = 115
      Top = 16
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 0
      OnKeyPress = EdtCodigoKeyPress
    end
    object edtportador: TEdit
      Left = 115
      Top = 39
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 2
      OnExit = edtportadorExit
      OnKeyDown = edtportadorKeyDown
      OnKeyPress = EdtCodigoKeyPress
    end
    object edtnumero: TEdit
      Left = 115
      Top = 62
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 3
      OnKeyPress = EdtCodigoKeyPress
    end
    object edtvalor: TEdit
      Left = 115
      Top = 85
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 5
      OnKeyPress = edtvalorKeyPress
    end
    object edtvencimento: TMaskEdit
      Left = 115
      Top = 108
      Width = 70
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 6
      Text = '  /  /    '
    end
    object edtnumerofinal: TEdit
      Left = 268
      Top = 62
      Width = 70
      Height = 19
      TabOrder = 4
      OnEnter = edtnumerofinalEnter
    end
    object combousado: TComboBox
      Left = 268
      Top = 108
      Width = 70
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 7
      Items.Strings = (
        'Sim'
        'N'#227'o')
    end
    object edtCodigoChequePortador: TEdit
      Left = 115
      Top = 131
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 9
      OnKeyPress = edtvalorKeyPress
    end
    object edthistoricopagamento: TEdit
      Left = 115
      Top = 154
      Width = 381
      Height = 19
      MaxLength = 100
      TabOrder = 12
    end
    object edtnumtalaocheque: TEdit
      Left = 268
      Top = 16
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 1
      OnKeyPress = EdtCodigoKeyPress
    end
    object combodescontado: TComboBox
      Left = 425
      Top = 108
      Width = 70
      Height = 21
      BevelKind = bkSoft
      ItemHeight = 13
      TabOrder = 8
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
    object edtlancamento: TEdit
      Left = 268
      Top = 131
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 10
      OnKeyPress = edtvalorKeyPress
    end
    object edttransferencia: TEdit
      Left = 425
      Top = 131
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 11
      OnKeyPress = edtvalorKeyPress
    end
  end
end
