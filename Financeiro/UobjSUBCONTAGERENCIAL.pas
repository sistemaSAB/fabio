unit UobjSUBCONTAGERENCIAL;
Interface
Uses rdprint,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJCONTAGER,UobjPlanodeContas;

//USES_INTERFACE



Type
   TObjSUBCONTAGERENCIAL=class

          Public
                ObjDatasourcepesquisa                       :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ContaGerencial:TOBJCONTAGER;
                PlanodeCOntas:TobjPlanodeContas;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;overload;
                Function    Get_Pesquisa(PcontaGer:string):TStringList;overload;

                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                Procedure Submit_Mascara(parametro: string);
                Function Get_Mascara: string;
                Procedure Submit_CODIGOPLANODECONTAS(parametro: string);
                Function Get_CODIGOPLANODECONTAS: string;
                procedure EdtContaGerencialExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtContaGerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtCODIGOPLANODECONTASExit(Sender: TObject; Lbnome: TLabel);
                procedure EdtCODIGOPLANODECONTASKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LbNome: TLabel);
                Procedure RetornaSubContas(Pcontagerencial:string);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Nome:string;
               Mascara:string;
               CODIGOPLANODECONTAS:string;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                procedure ImprimeContas_subContas;
                Procedure ImprimeContaPlanodeContas;
                procedure ImprimeContas_SubContascomMascara;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, Ucontager, UPLanodeContas, UReltxtRDPRINT;





Function  TObjSUBCONTAGERENCIAL.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('ContaGerencial').asstring<>'')
        Then Begin
                 If (Self.ContaGerencial.LocalizaCodigo(FieldByName('ContaGerencial').asstring)=False)
                 Then Begin
                          Messagedlg('Conta Gerencial N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ContaGerencial.TabelaparaObjeto;
        End;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.Mascara:=fieldbyname('Mascara').asstring;
        Self.CODIGOPLANODECONTAS:=fieldbyname('CODIGOPLANODECONTAS').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjSUBCONTAGERENCIAL.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('ContaGerencial').asstring:=Self.ContaGerencial.GET_CODIGO;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('Mascara').asstring:=Self.Mascara;
        ParamByName('CODIGOPLANODECONTAS').asstring:=Self.CODIGOPLANODECONTAS;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjSUBCONTAGERENCIAL.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;



  if Self.status=dsinsert
  Then Begin
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
  End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjSUBCONTAGERENCIAL.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        ContaGerencial.ZerarTabela;
        Nome:='';
        Mascara:='';
        CODIGOPLANODECONTAS:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjSUBCONTAGERENCIAL.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (ContaGerencial.get_Codigo='')
      Then Mensagem:=mensagem+'/Conta Gerencial';

      If (Nome='')
      Then Mensagem:=mensagem+'/Nome';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjSUBCONTAGERENCIAL.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.ContaGerencial.LocalizaCodigo(Self.ContaGerencial.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Conta Gerencial n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjSUBCONTAGERENCIAL.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.ContaGerencial.Get_Codigo<>'')
        Then Strtoint(Self.ContaGerencial.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Conta Gerencial';
     End;

     try
        if (Self.CODIGOPLANODECONTAS<>'')
        then Strtoint(Self.CODIGOPLANODECONTAS);
     Except
           Mensagem:=mensagem+'/C�digo do Plano de Contas';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjSUBCONTAGERENCIAL.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjSUBCONTAGERENCIAL.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem,Tmascara2:string;
   cont:integer;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';

        //Vou apagar os espa�os em branco da String da MAscara
        //e verificar aonde termina os numeros para cortar os pontos
        Tmascara2:='';
        for cont:=1 to length(self.Mascara) do
        Begin
             If (Self.Mascara[cont]<>' ')
             Then TMascara2:=Tmascara2+Self.Mascara[cont];
        End;

        Self.Mascara:='';

        for cont:=1 to length(Tmascara2) do
        Begin
             If (Tmascara2[cont]='.')
             Then Begin
                       If  (cont<>length(Tmascara2))//n�o � a ultima
                       Then Begin
                                 If (not (TMascara2[cont+1] in ['0'..'9']))
                                 Then break
                                 Else Self.Mascara:=Self.Mascara+Tmascara2[cont];
                            End;
                  End
             Else Self.Mascara:=Self.Mascara+Tmascara2[cont];
        End;


//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjSUBCONTAGERENCIAL.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro SUBCONTAGERENCIAL vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ContaGerencial,Nome,Mascara,CODIGOPLANODECONTAS');
           SQL.ADD(' from  TabSubContaGerencial');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjSUBCONTAGERENCIAL.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjSUBCONTAGERENCIAL.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjSUBCONTAGERENCIAL.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=Tibquery.create(nil);
        Self.ObjqueryPesquisa.database:=Fdatamodulo.IBDatabase;
        Self.ObjDatasourcepesquisa:=TDataSource.Create(nil);
        Self.ObjDatasourcepesquisa.DataSet:=Self.ObjqueryPesquisa;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.ContaGerencial:=TOBJCONTAGER.create;
        Self.PlanodeCOntas:=TobjPlanodeContas.create;
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabSubContaGerencial(CODIGO,ContaGerencial');
                InsertSQL.add(' ,Nome,Mascara,CODIGOPLANODECONTAS)');
                InsertSQL.add('values (:CODIGO,:ContaGerencial,:Nome,:Mascara,:CODIGOPLANODECONTAS');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabSubContaGerencial set CODIGO=:CODIGO,ContaGerencial=:ContaGerencial');
                ModifySQL.add(',Nome=:Nome,Mascara=:Mascara,CODIGOPLANODECONTAS=:CODIGOPLANODECONTAS');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabSubContaGerencial where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjSUBCONTAGERENCIAL.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjSUBCONTAGERENCIAL.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabSUBCONTAGERENCIAL');
     Result:=Self.ParametroPesquisa;
end;

function TObjSUBCONTAGERENCIAL.Get_Pesquisa(PcontaGer: string): TStringList;
begin
     Self.ParametroPesquisa.clear;

     Self.ParametroPesquisa.add('Select *');
     Self.ParametroPesquisa.add('from');
     Self.ParametroPesquisa.add('TabSUBCONTAGERENCIAL ');
     Self.ParametroPesquisa.add('where contagerencial='+pcontager);
     
     Result:=Self.ParametroPesquisa;
end;


function TObjSUBCONTAGERENCIAL.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Sub-Contas Gerenciais ';
end;


function TObjSUBCONTAGERENCIAL.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENSUBCONTAGERENCIAL,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENSUBCONTAGERENCIAL,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjSUBCONTAGERENCIAL.Free;
begin
    //Freeandnil(Self.Objquery);
    //Freeandnil(Self.ParametroPesquisa);
    //Freeandnil(InsertSql);
    //Freeandnil(DeleteSql);
    //Freeandnil(ModifySQl);
    //freeandnil(Self.objdatasourcepesquisa);
    //freeandnil(Self.ObjqueryPesquisa);
    //Self.ContaGerencial.FREE;
    freeandnil(Objquery);
    freeandnil(ParametroPesquisa);
    freeandnil(InsertSql);
    freeandnil(DeleteSql);
    freeandnil(modifysql);
    freeandnil(ObjDatasourcepesquisa);
    freeandnil(ObjqueryPesquisa);
    ContaGerencial.free;
    Self.PlanodeCOntas.FREE;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjSUBCONTAGERENCIAL.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjSUBCONTAGERENCIAL.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjSubContaGerencial.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjSubContaGerencial.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjSubContaGerencial.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjSubContaGerencial.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
procedure TObjSubContaGerencial.Submit_Mascara(parametro: string);
begin
        Self.Mascara:=Parametro;
end;
function TObjSubContaGerencial.Get_Mascara: string;
begin
        Result:=Self.Mascara;
end;
procedure TObjSubContaGerencial.Submit_CODIGOPLANODECONTAS(parametro: string);
begin
        Self.CODIGOPLANODECONTAS:=Parametro;
end;
function TObjSubContaGerencial.Get_CODIGOPLANODECONTAS: string;
begin
        Result:=Self.CODIGOPLANODECONTAS;
end;
//CODIFICA GETSESUBMITS


procedure TObjSUBCONTAGERENCIAL.EdtContaGerencialExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ContaGerencial.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ContaGerencial.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.ContaGerencial.GET_NOME;
End;
procedure TObjSUBCONTAGERENCIAL.EdtContaGerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCONTAGER:TFCONTAGER;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCONTAGER:=TFCONTAGER.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ContaGerencial.Get_Pesquisa,Self.ContaGerencial.Get_TituloPesquisa,FContaGer)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ContaGerencial.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.ContaGerencial.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ContaGerencial.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCONTAGER);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjSUBCONTAGERENCIAL.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJSUBCONTAGERENCIAL';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Lista de Contas Gerenciais');//0
                items.add('Lista de Contas Gerenciais D�bito (Contas a Pagar)');//1
                items.add('Lista de Contas Gerenciais Cr�dito (Contas a Receber)');//2
                items.add('Lista de Contas e Sub-Contas');
                items.add('Contas com Plano de Contas');
                items.add('Lista de Contas e Sub-Contas com M�scara');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
              0: Self.contagerencial.ImprimeListagem('');
              1: Self.contagerencial.ImprimeListagem('D');
              2: Self.contagerencial.ImprimeListagem('C');
              3: Self.ImprimeContas_Subcontas;
              4: Self.ImprimeContaPlanodeContas;
              5: Self.ImprimeContas_SubContascomMascara;
          End;
     end;

end;

procedure TObjSubContaGerencial.EdtCODIGOPLANODECONTASKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState; LbNome: TLabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPlanodeContasX:TFplanodeContas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FPlanodeContasX:=TFplanodeContas.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PlanodeCOntas.Get_Pesquisa,Self.PlanodeCOntas.Get_TituloPesquisa,FPlanodeContasX)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        LbNome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeandNil(FPlanodeContasX);

     End;
end;

procedure TObjSubContaGerencial.EdtCODIGOPLANODECONTASExit(Sender: TObject;
  Lbnome: TLabel);
begin
     Lbnome.Caption:='';
     if (TEdit(Sender).text='')
     Then exit;

     if (Self.PlanodeCOntas.LocalizaCodigo(TEdit(Sender).text)=False)
     Then Begin
              TEdit(Sender).text:='';
              exit;
     End;
     Self.PlanodeCOntas.TabelaparaObjeto;
     Lbnome.caption:=Self.PlanodeCOntas.Get_Nome;
end;


procedure TObjSUBCONTAGERENCIAL.RetornaSubContas(Pcontagerencial: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select mascara,nome,codigo,codigoplanodecontas from tabsubcontagerencial where contagerencial='+Pcontagerencial);
          sql.add('order by mascara');
          open;
     End;
end;



procedure TObjSubContaGerencial.ImprimeContas_subContas;
var
pconta:string;
begin
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          SQL.add('Select TabContaGer.Codigo as contagerencial,TabContaGer.Nome as NomeContaGerencial,');
          SQL.add('TabContaGer.Tipo, TabSubContaGerencial.Codigo as SubContaGerencial,');
          SQL.add('TabSubContaGerencial.Nome as NomeSubContaGerencial');
          SQL.add('from TabContaGer');
          SQL.add('left Join TabSubContaGerencial on TabSubContaGerencial.ContaGerencial = TabContaGer.Codigo');
          SQL.add('Order By TabContaGer.Codigo, TabSubContaGerencial.Codigo');
          Open;

          if (recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               RDprint.abrir;
               if (RDprint.Setup=False)
               then Begin
                         RDprint.Fechar;
                         exit;
               end;
               RDprint.ImpC(linhalocal,45,'CONTAS E SUB-CONTAS',[negrito]);
               IncrementaLinha(2);

               pconta:=fieldbyname('contagerencial').asstring;
               While not(self.Objquery.Eof) do
               Begin

                    if (pconta<>fieldbyname('contagerencial').asstring)
                    then Begin
                              IncrementaLinha(1);
                              pconta:=fieldbyname('contagerencial').asstring;
                              VerificaLinha;
                              RDprint.Impf(linhalocal,1,CompletaPalavra(fieldbyname('contagerencial').asstring,10,' ')+' '+
                                                        CompletaPalavra(fieldbyname('tipo').asstring+' - '+fieldbyname('nomecontagerencial').asstring,80,' '),[negrito]);
                              IncrementaLinha(1);
                    End;

                    VerificaLinha;
                    RDprint.Imp(linhalocal,5, CompletaPalavra(fieldbyname('SubContaGerencial').asstring,10,' ')+' '+
                                              CompletaPalavra(fieldbyname('NomeSubContaGerencial').asstring,80,' '));
                    IncrementaLinha(1);

                    self.objquery.next;
               End;
               VerificaLinha;
               DesenhaLinha;
               RDprint.Fechar;
          End;
     End;

end;

procedure TObjSUBCONTAGERENCIAL.ImprimeContaPlanodeContas;
var
pconta:string;
begin
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          SQL.add('Select tabcontager.codigo,tabplanodecontas.classificacao,tabcontager.nome,tabcontager.tipo,tabplanodecontas.tipo as TIPOPLANODECONTA');
          SQL.add('from tabcontager');
          SQL.add('left join tabplanodecontas on tabcontager.codigoplanodecontas=tabplanodecontas.codigo');
          SQL.add('order by tabplanodecontas.classificacao');
          Open;

          if (recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               RDprint.abrir;
               if (RDprint.Setup=False)
               then Begin
                         RDprint.Fechar;
                         exit;
               end;
               RDprint.ImpC(linhalocal,45,'CONTAS GERENCIAIS E PLANO DE CONTAS',[negrito]);
               IncrementaLinha(2);

               RDprint.Impf(linhalocal,1, CompletaPalavra('CODIGO',6,' ')+' '+
                                          CompletaPalavra('CLASSIFICACAO',30,' ')+' '+
                                          CompletaPalavra('NOME',50,' ')+' '+
                                          CompletaPalavra('D/C',3,' '),[negrito]);
               incrementalinha(1);


               pconta:='';
               While not(self.Objquery.Eof) do
               Begin
                    VerificaLinha;

                    if (fieldbyname('tipoplanodeconta').asstring='T')
                    then BEgin
                              RDprint.Impf(linhalocal,1, CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                                        CompletaPalavra(fieldbyname('classificacao').asstring,30,' ')+' '+
                                                        CompletaPalavra(fieldbyname('nome').asstring,50,' ')+' '+
                                                        CompletaPalavra(fieldbyname('tipo').asstring,1,' '),[negrito]);
                    End
                    Else Begin
                              RDprint.Imp(linhalocal,1, CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                                        CompletaPalavra(fieldbyname('classificacao').asstring,30,' ')+' '+
                                                        CompletaPalavra(fieldbyname('nome').asstring,50,' ')+' '+
                                                        CompletaPalavra(fieldbyname('tipo').asstring,1,' '));

                    End;
                    IncrementaLinha(1);

                    self.objquery.next;
               End;
               VerificaLinha;
               DesenhaLinha;

               RDprint.Fechar;
          End;
     End;

end;

Procedure TObjSUbContaGerencial.ImprimeContas_SubContascomMascara;
var
PContaGer:string;
Linha:Integer;
begin
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          SQL.add('Select TabContaGer.Codigo as ContaGer, TabContaGer.Mascara as MascaraContaGer,');
          SQL.add('TabContaGer.NOme as NomeContaGer,TabSubContaGerencial.Codigo as SubConta,');
          SQL.add('TabSubContaGerencial.Mascara as MascaraSubConta,');
          SQL.add('TabSubContaGerencial.NOme as NomeSubConta');
          SQL.add('From TabContaGer');
          SQL.add('left Join TabSubContaGerencial on TabSubContaGerencial.ContaGerencial=TabContaGer.Codigo');
          SQL.add('Order by TabContaGer.Codigo, TabsubContagerencial.Codigo');
          Open;

          if (recordcount=0)
          then Begin
                    MensagemAviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               Linha:=3;
               RDprint.abrir;
               if (RDprint.Setup=False)
               then Begin
                         RDprint.Fechar;
                         exit;
               end;
               RDprint.ImpC(linha,45,'CONTAS GERENCIAIS E SUBCONTAS',[negrito]);
               inc(linha,2);

               PContaGer:=fieldbyname('ContaGer').AsString;

               VerificaLinha(rdprint, linha);
               RDprint.Impf(linha,1, CompletaPalavra(fieldbyname('ContaGer').asstring,6,' ')+' '+
                                          CompletaPalavra(fieldbyname('MascaraContaGer').asstring,30,' ')+' '+
                                          CompletaPalavra(fieldbyname('nomecontager').asstring,50,' '),[Negrito]);
               inc(linha,1);
               While not(self.Objquery.Eof) do
               Begin
                    if (PcontaGer <> fieldbyname('ContaGer').asstring)
                    then BEgin
                              VerificaLinha(rdprint, linha);
                              RDprint.Impf(linha,1, CompletaPalavra(fieldbyname('ContaGer').asstring,6,' ')+' '+
                                                         CompletaPalavra(fieldbyname('MascaraContaGer').asstring,30,' ')+' '+
                                                         CompletaPalavra(fieldbyname('nomecontager').asstring,50,' '),[Negrito]);
                              inc(linha,1);
                              PContaGer := fieldbyname('ContaGer').asstring;
                    End
                    Else Begin
                              VerificaLinha(rdprint, linha);
                              RDprint.Imp(linha,1, CompletaPalavra(fieldbyname('SubConta').asstring,6,' ')+' '+
                                                        CompletaPalavra(fieldbyname('MascaraSubConta').asstring,30,' ')+' '+
                                                        CompletaPalavra(fieldbyname('nomesubConta').asstring,50,' '));
                             inc(linha,1);

                    End;
                    self.objquery.next;
               End;

               RDprint.Fechar;
          End;
     End;

end;


end.



