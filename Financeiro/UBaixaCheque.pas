unit UBaixaCheque;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,UObjTalaodeCheques,
  Buttons, StdCtrls, Mask, ExtCtrls;

type
  TFbaixaCheque = class(TForm)
    edtcheque: TEdit;
    Label1: TLabel;
    Btbaixa: TBitBtn;
    edtdata: TMaskEdit;
    Label2: TLabel;
    edtportador: TEdit;
    Label3: TLabel;
    Lbportador: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtchequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtchequeKeyPress(Sender: TObject; var Key: Char);
    procedure BtbaixaClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorExit(Sender: TObject);
    procedure edtportadorKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FbaixaCheque: TFbaixaCheque;
  ObjTalaodeCheques:Tobjtalaodecheques;

implementation

uses Upesquisa, Uportador, UessencialGlobal, UescolheImagemBotao;



{$R *.DFM}

procedure TFbaixaCheque.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

     If ObjTalaodeCheques<>nil
     Then objtalaodecheques.free;

end;

procedure TFbaixaCheque.edtchequeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   complemento:string;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            complemento:='';
            if (edtportador.text<>'')
            Then complemento:=' and TabTalaodeCheques.portador='+edtportador.text;
            If (FpesquisaLocal.PreparaPesquisa(ObjtalaodeCheques.Get_PesquisaUsadosNoPortadorOriginal+complemento,ObjTalaodeCheques.Get_TituloPesquisaUSados,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtcheque.text:=FpesquisaLocal.QueryPesq.fieldbyname('numero').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;

procedure TFbaixaCheque.edtchequeKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFbaixaCheque.BtbaixaClick(Sender: TObject);
begin
     ObjTalaodeCheques.baixaCheque(Edtcheque.text,edtdata.text,edtportador.text);
     edtdata.text:='';
     edtcheque.text:='';
     edtdata.setfocus;
end;

procedure TFbaixaCheque.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador:=TFportador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjtalaodeCheques.Get_PesquisaPortador,Objtalaodecheques.get_titulopesquisaportador,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 Lbportador.caption:=FpesquisaLocal.QueryPesq.fieldbyname('Nome').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;

end;

procedure TFbaixaCheque.edtportadorExit(Sender: TObject);
begin
     If (Edtportador.text='')
     Then lbportador.caption:='';
end;

procedure TFbaixaCheque.edtportadorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFbaixaCheque.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

      Try
        ObjTalaodeCheques:=Tobjtalaodecheques.create;
        edtcheque.text:='';
        edtdata.text:='';
        edtportador.text:='';
        Lbportador.caption:='';
        edtportador.SetFocus;
     Except
           Messagedlg('Erro na Tentativa de Criar o Objeto de Tal�o de Cheques!',mterror,[mbok],0);
           Close;
     End;
     FescolheImagemBotao.PegaFiguraBotoes(nil,nil,nil,Btbaixa,nil,nil,nil,nil);
end;

end.
