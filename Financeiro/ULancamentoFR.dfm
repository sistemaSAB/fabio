object FrLancamento: TFrLancamento
  Left = 0
  Top = 0
  Width = 677
  Height = 320
  TabOrder = 0
  object Guia: TPageControl
    Left = 0
    Top = 0
    Width = 677
    Height = 320
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    OnChange = GuiaChange
    object TabSheet1: TTabSheet
      object btchequecaixa: TBitBtn
        Left = 0
        Top = 185
        Width = 212
        Height = 57
        Caption = 'CHEQUE VALE CAIXA'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = btchequecaixaClick
      end
      object btpesquisa: TBitBtn
        Left = 0
        Top = 12
        Width = 212
        Height = 57
        Caption = 'Pesquisar Lan'#231'amentos'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object BtApagaLote: TBitBtn
        Left = 0
        Top = 70
        Width = 212
        Height = 57
        Caption = 'Apaga Lan'#231'amento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = BtApagaLoteClick
      end
      object btimprimecomprovantepagamento: TBitBtn
        Left = 0
        Top = 128
        Width = 212
        Height = 57
        Caption = 'Imprime Recibo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = btimprimecomprovantepagamentoClick
      end
      object btAlterarHistoricoLancamento: TBitBtn
        Left = 1
        Top = 243
        Width = 212
        Height = 57
        Caption = 'Alterar Hist'#243'rico de Lan'#231'amentos'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnClick = btAlterarHistoricoLancamentoClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = '&Pagamentos em Lote'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clTeal
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object Panel1: TPanel
        Left = 0
        Top = 252
        Width = 669
        Height = 40
        Align = alBottom
        TabOrder = 0
        object Label13: TLabel
          Left = 1
          Top = 26
          Width = 667
          Height = 13
          Align = alBottom
          Alignment = taCenter
          Caption = 'Ctrl +P Imprime Grade Atual'
        end
        object combocredordevedorcodigoCP: TComboBox
          Left = 549
          Top = 19
          Width = 70
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = ' '
          Visible = False
        end
        object edtpesquisa_STRG_GRID_CP: TMaskEdit
          Left = 6
          Top = 6
          Width = 121
          Height = 21
          TabOrder = 1
          Text = 'edtpesquisa_STRG_GRID_CP'
          Visible = False
          OnKeyPress = edtpesquisa_STRG_GRID_CPKeyPress
        end
        object LbtipoCampoPG: TListBox
          Left = 629
          Top = 6
          Width = 33
          Height = 33
          ItemHeight = 13
          TabOrder = 2
          Visible = False
        end
      end
      object STRGCP: TStringGrid
        Tag = 8
        Left = 0
        Top = 159
        Width = 669
        Height = 93
        Align = alClient
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
        OnDblClick = STRGCPDblClick
        OnDrawCell = strgcrDrawCell
        OnEnter = STRGCPEnter
        OnKeyDown = STRGCPKeyDown
        OnKeyPress = STRGCPKeyPress
        ColWidths = (
          64
          64
          64
          64
          64)
        RowHeights = (
          24
          24
          24
          24
          24)
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 669
        Height = 159
        Align = alTop
        TabOrder = 2
        object Bevel2: TBevel
          Left = 338
          Top = 101
          Width = 326
          Height = 56
          Shape = bsFrame
        end
        object Label4: TLabel
          Left = 198
          Top = 62
          Width = 140
          Height = 13
          Caption = 'Hist'#243'rico do Lan'#231'amento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 6
          Top = 62
          Width = 184
          Height = 13
          Caption = 'Valor do Lan'#231'amento (Opcional)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbNomeCredorDevedorCP: TLabel
          Left = 6
          Top = 44
          Width = 538
          Height = 13
          AutoSize = False
          Caption = 'ascdasuidyauisdy uiay sduioay sdoiuay soiuda'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object Label11: TLabel
          Left = 6
          Top = 1
          Width = 94
          Height = 13
          Caption = 'Credor/Devedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label12: TLabel
          Left = 151
          Top = 1
          Width = 40
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 381
          Top = 1
          Width = 67
          Height = 13
          Caption = 'Vencimento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 461
          Top = 1
          Width = 86
          Height = 13
          Caption = 'Data do Lancto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object lbvalorlanctoCP: TLabel
          Left = 518
          Top = 105
          Width = 144
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'sdfsdfsdfsdfsdfsdf'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object lbvalorpagamento: TLabel
          Left = 518
          Top = 121
          Width = 144
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'sdfsdfsdfsdfsdfsdf'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object lbsaldocp: TLabel
          Left = 494
          Top = 137
          Width = 168
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'sdfsdfsdfsdfsdfsdfsdf'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object Label16: TLabel
          Left = 218
          Top = 1
          Width = 63
          Height = 13
          Caption = 'Conta Ger.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label17: TLabel
          Left = 287
          Top = 1
          Width = 60
          Height = 13
          Caption = 'Sub Conta'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object BtSelecionaTodas_PG: TButton
          Left = 2
          Top = 136
          Width = 127
          Height = 20
          Caption = 'Seleciona Todas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 11
          OnClick = BtSelecionaTodas_PGClick
        end
        object edthistoricoCP: TEdit
          Left = 240
          Top = 78
          Width = 420
          Height = 21
          TabOrder = 8
        end
        object edtcodigohistoricoCP: TEdit
          Left = 200
          Top = 78
          Width = 31
          Height = 21
          TabOrder = 7
          OnExit = edtcodigohistoricoCPExit
          OnKeyDown = edtcodigohistoricoCPKeyDown
          OnKeyPress = edtcodigohistoricoCRKeyPress
        end
        object edtvalorlancamentoCP: TMaskEdit
          Left = 6
          Top = 78
          Width = 85
          Height = 21
          TabOrder = 6
          OnExit = edtvalorlancamentoCPExit
          OnKeyPress = edtvalorlancamentoCPKeyPress
        end
        object combocredordevedorCP: TComboBox
          Left = 6
          Top = 17
          Width = 143
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          OnChange = combocredordevedorCPChange
          Items.Strings = (
            'Clientes'
            'Fornecedores'
            'Alunos')
        end
        object edtcodigocredordevedorCP: TEdit
          Left = 151
          Top = 17
          Width = 65
          Height = 21
          Color = 6073854
          TabOrder = 1
          OnDblClick = edtcodigocredordevedorCPDblClick
          OnExit = edtcodigocredordevedorCPExit
          OnKeyDown = edtcodigocredordevedorCPKeyDown
        end
        object edtvencimentocp: TMaskEdit
          Left = 381
          Top = 17
          Width = 72
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 4
          Text = '  /  /    '
        end
        object edtdatalancamentoCP: TMaskEdit
          Left = 461
          Top = 17
          Width = 73
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 5
          Text = '  /  /    '
        end
        object BtpesquisaCP: TBitBtn
          Left = 551
          Top = 1
          Width = 112
          Height = 38
          Caption = 'Pesqu&isar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
          OnClick = BtpesquisaCPClick
        end
        object BtexecutaCP: TBitBtn
          Left = 551
          Top = 39
          Width = 112
          Height = 38
          Caption = '&Executar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
          OnClick = BtexecutaCPClick
        end
        object edtcontagerencial_CP: TEdit
          Left = 220
          Top = 17
          Width = 65
          Height = 21
          Color = 6073854
          TabOrder = 2
          OnDblClick = edtcontagerencial_CPDblClick
          OnKeyDown = edtcontagerencial_CPKeyDown
        end
        object edtsubcontagerencial_CP: TEdit
          Left = 289
          Top = 17
          Width = 65
          Height = 21
          Color = 6073854
          TabOrder = 3
          OnDblClick = edtsubcontagerencial_CPDblClick
          OnKeyDown = edtsubcontagerencial_CPKeyDown
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = '&Recebimentos em Lote e Lan'#231'amento de Boletos'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clTeal
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ImageIndex = 2
      ParentFont = False
      object strgcr: TStringGrid
        Tag = 8
        Left = 0
        Top = 161
        Width = 669
        Height = 89
        Align = alClient
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 0
        OnDblClick = strgcrDblClick
        OnDrawCell = strgcrDrawCell
        OnEnter = strgcrEnter
        OnKeyDown = strgcrKeyDown
        OnKeyPress = strgcrKeyPress
        ColWidths = (
          64
          64
          64
          64
          64)
        RowHeights = (
          24
          24
          24
          24
          24)
      end
      object Panel3: TPanel
        Left = 0
        Top = 250
        Width = 669
        Height = 42
        Align = alBottom
        TabOrder = 1
        object Label6: TLabel
          Left = 1
          Top = 28
          Width = 129
          Height = 13
          Align = alBottom
          Alignment = taCenter
          Caption = 'Ctrl +P Imprime Grade Atual'
        end
        object edtpesquisa_STRG_GRID_CR: TMaskEdit
          Left = 3
          Top = 10
          Width = 121
          Height = 21
          TabOrder = 0
          Text = 'edtpesquisa_STRG_GRID_CP'
          OnKeyPress = edtpesquisa_STRG_GRID_CRKeyPress
        end
        object LbtipoCampoCR: TListBox
          Left = 632
          Top = 7
          Width = 33
          Height = 33
          ItemHeight = 13
          TabOrder = 1
          Visible = False
        end
        object combocredordevedorcodigocr: TComboBox
          Left = 565
          Top = 19
          Width = 68
          Height = 21
          ItemHeight = 0
          TabOrder = 2
          Text = ' '
          Visible = False
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 669
        Height = 161
        Align = alTop
        TabOrder = 2
        object Bevel1: TBevel
          Left = 334
          Top = 101
          Width = 326
          Height = 56
          Shape = bsFrame
        end
        object LbValorLancamentocr: TLabel
          Left = 569
          Top = 107
          Width = 88
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'aaaaaaaaaaa'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object lbrecebe: TLabel
          Left = 569
          Top = 123
          Width = 88
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'aaaaaaaaaaa'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object LbSaldocr: TLabel
          Left = 569
          Top = 139
          Width = 88
          Height = 16
          Alignment = taRightJustify
          BiDiMode = bdLeftToRight
          Caption = 'aaaaaaaaaaa'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentBiDiMode = False
          ParentFont = False
        end
        object Label2: TLabel
          Left = 198
          Top = 62
          Width = 201
          Height = 13
          Caption = 'Hist'#243'rico do Lan'#231'amento (opcional)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 6
          Top = 62
          Width = 184
          Height = 13
          Caption = 'Valor do Lan'#231'amento (Opcional)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object LbNomeCredorDevedorcr: TLabel
          Left = 6
          Top = 44
          Width = 537
          Height = 13
          AutoSize = False
          Caption = 'ascdasuidyauisdy uiay sduioay sdoiuay soiuda'
          Color = clBtnFace
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object Label10: TLabel
          Left = 155
          Top = 1
          Width = 40
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 247
          Top = 1
          Width = 67
          Height = 13
          Caption = 'Vencimento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 327
          Top = 1
          Width = 86
          Height = 13
          Caption = 'Data do Lancto'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 417
          Top = 1
          Width = 132
          Height = 14
          Caption = 'C'#243'digo do Boleto (opcional)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 6
          Top = 1
          Width = 94
          Height = 13
          Caption = 'Credor/Devedor'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object btlancaboleto: TBitBtn
          Left = 9
          Top = 135
          Width = 98
          Height = 24
          Caption = '&Lan'#231'a Boleto'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
          OnClick = btlancaboletoClick
        end
        object BtSelecionaTodas_CR: TButton
          Left = 108
          Top = 135
          Width = 98
          Height = 24
          Caption = 'Seleciona Todas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 11
          OnClick = BtSelecionaTodas_CRClick
        end
        object edthistoricoCR: TEdit
          Left = 240
          Top = 78
          Width = 420
          Height = 21
          TabOrder = 7
        end
        object edtcodigohistoricoCR: TEdit
          Left = 200
          Top = 78
          Width = 31
          Height = 21
          TabOrder = 6
          OnExit = edtcodigohistoricoCRExit
          OnKeyDown = edtcodigohistoricoCRKeyDown
          OnKeyPress = edtcodigohistoricoCRKeyPress
        end
        object edtvalorlancamentoCR: TMaskEdit
          Left = 6
          Top = 78
          Width = 85
          Height = 21
          TabOrder = 5
          OnExit = edtvalorlancamentoCRExit
          OnKeyPress = edtvalorlancamentoCPKeyPress
        end
        object edtcodigocredordevedorcr: TEdit
          Left = 155
          Top = 17
          Width = 85
          Height = 21
          Color = 6073854
          TabOrder = 1
          OnDblClick = edtcodigocredordevedorcrDblClick
          OnExit = edtcodigocredordevedorcrExit
          OnKeyDown = edtcodigocredordevedorcrKeyDown
        end
        object edtvencimentocr: TMaskEdit
          Left = 247
          Top = 17
          Width = 72
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 2
          Text = '  /  /    '
        end
        object edtdatalancamentocr: TMaskEdit
          Left = 327
          Top = 17
          Width = 73
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 3
          Text = '  /  /    '
        end
        object edtcodigoboleto: TEdit
          Left = 417
          Top = 17
          Width = 89
          Height = 21
          Color = 6073854
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnDblClick = edtcodigoboletoDblClick
          OnKeyDown = edtcodigoboletoKeyDown
        end
        object btpesquisacr: TBitBtn
          Left = 551
          Top = 1
          Width = 112
          Height = 38
          Caption = 'Pesqu&isar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          OnClick = btpesquisacrClick
        end
        object btexecutacr: TBitBtn
          Left = 551
          Top = 39
          Width = 112
          Height = 38
          Caption = '&Executar'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
          OnClick = btexecutacrClick
        end
        object combocredordevedorcr: TComboBox
          Left = 6
          Top = 17
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          OnChange = combocredordevedorcrChange
          Items.Strings = (
            'Clientes'
            'Fornecedores'
            'Alunos')
        end
        object Button1: TButton
          Left = 9
          Top = 110
          Width = 98
          Height = 24
          Caption = '&Desconto R$'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 12
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 108
          Top = 110
          Width = 98
          Height = 24
          Caption = '&Desconto (%)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 13
          OnClick = Button2Click
        end
      end
    end
  end
end
