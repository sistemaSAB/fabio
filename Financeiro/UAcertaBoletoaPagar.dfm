object FAcertaBoletoaPagar: TFAcertaBoletoaPagar
  Left = 278
  Top = 103
  Width = 531
  Height = 503
  Caption = 'Acerto de Boletos a Pagar por T'#237'tulo - Exclaim Tecnologia'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 58
    Height = 16
    Caption = 'T'#237'tulo N'#186':'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 0
    Top = 16
    Width = 98
    Height = 16
    Caption = 'Valor T'#237'tulo R$ '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbcredordevedor: TLabel
    Left = 0
    Top = 36
    Width = 98
    Height = 16
    Caption = 'Valor T'#237'tulo R$ '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbnumtitulo: TLabel
    Left = 112
    Top = 0
    Width = 58
    Height = 16
    Caption = 'T'#237'tulo N'#186':'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbvalortitulo: TLabel
    Left = 112
    Top = 16
    Width = 98
    Height = 16
    Caption = 'Valor T'#237'tulo R$ '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Grid: TStringGrid
    Left = 0
    Top = 55
    Width = 521
    Height = 418
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
    TabOrder = 0
    OnKeyPress = GridKeyPress
    ColWidths = (
      64
      64
      64
      64
      64)
  end
  object Btsair: TBitBtn
    Left = 404
    Top = 1
    Width = 118
    Height = 37
    Caption = '&Sair'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BtsairClick
  end
  object Btcancelar: TBitBtn
    Left = 284
    Top = 1
    Width = 118
    Height = 37
    Caption = '&Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = BtcancelarClick
  end
end
