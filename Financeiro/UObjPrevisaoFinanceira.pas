unit UObjPrevisaoFinanceira;
Interface
Uses windows,stdctrls,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,Uobjportador
,UOBJCONTAGER
,UOBJSUBCONTAGERENCIAL,uobjcredordevedor
;

Type
   TObjPrevisaoFinanceira=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Portador         :TobjPortador;
                ContaGerencial:TOBJCONTAGER;
                SubContaGerencial:TOBJSUBCONTAGERENCIAL;
                CredorDevedor:TobjCredorDevedor;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :string;

                Function    TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Procedure Submit_CODIGO           (parametro:string);
                function Get_Data: string;
                function Get_Debito_Credito: string;
                function Get_Historico: string;
                function Get_Valor: string;
                procedure Submit_Data(parametro: string);
                procedure Submit_Debito_Credito(parametro: string);
                procedure Submit_Historico(parametro: string);
                procedure Submit_Valor(parametro: string);
                Function  Get_NovoCodigo:string;
                function Get_ConsiderarMesesSubsequentes: string;
                procedure Submit_ConsiderarMesesSubsequentes(parametro: string);
                function Get_DataLimite: string;
                procedure Submit_DataLimite(parametro: string);

                Function Get_CodigoCredorDevedor:string;
                Procedure Submit_CodigoCredorDevedor(parametro:string);

                Function Get_somatoriaprevisao:string;
                Procedure Submit_somatoriaprevisao(parametro:string);




                procedure EdtContaGerencialExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtContaGerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;PdebitoCredito:string);
                procedure EdtSubContaGerencialExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtSubContaGerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure edtsubcontagerencialKeyDown(Sender: TObject;  var Key: Word; Shift: TShiftState;lbnome:Tlabel;PContager:string);overload;

                Function  LancaTitulo(Pcodigo:string):boolean;


         Private
               ObjDataset:Tibdataset;

               CODIGO           :string;
               Historico        :string;
               Data             :string;
               Valor            :string;
               Debito_Credito   :string;
               ConsiderarMesesSubsequentes:string;
               DataLimite:string;
               CodigoCredorDevedor:string;
               somatoriaprevisao:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls,uobjtitulo,utitulo,
  Upesquisa, Ucontager, USUBCONTAGERENCIAL;


{ TTabTitulo }


Function  TObjPrevisaoFinanceira.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Historico:=fieldbyname('Historico').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.Datalimite:=fieldbyname('Datalimite').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.Debito_Credito:=fieldbyname('Debito_Credito').asstring;

        If (Self.Portador.LocalizaCodigo(fieldbyname('portador').asstring)=False)
        Then Begin
                  Messagedlg('Portador n�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.Portador.TabelaparaObjeto;

         If(FieldByName('ContaGerencial').asstring<>'')
        Then Begin
                 If (Self.ContaGerencial.LocalizaCodigo(FieldByName('ContaGerencial').asstring)=False)
                 Then Begin
                          Messagedlg('ContaGerencial N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ContaGerencial.TabelaparaObjeto;
        End;

        If(FieldByName('SubContaGerencial').asstring<>'')
        Then Begin
                 If (Self.SubContaGerencial.LocalizaCodigo(FieldByName('SubContaGerencial').asstring)=False)
                 Then Begin
                          Messagedlg('SubContaGerencial N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SubContaGerencial.TabelaparaObjeto;
        End;

        If(FieldByName('credordevedor').asstring<>'')
        Then Begin
                 If (Self.credordevedor.LocalizaCodigo(FieldByName('credordevedor').asstring)=False)
                 Then Begin
                          Messagedlg('Credor Devedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.credordevedor.TabelaparaObjeto;
        End;
        Self.CodigoCredorDevedor:=fieldbyname('codigocredordevedor').asstring;

        Self.ConsiderarMesesSubsequentes:=fieldbyname('ConsiderarMesesSubsequentes').asstring;
        Self.somatoriaprevisao:=fieldByname('somatoriaprevisao').asstring;


        result:=True;
     End;
end;


Procedure TObjPrevisaoFinanceira.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('Historico').asstring:=Self.Historico;
        fieldbyname('Data').asstring:=Self.Data;
        fieldbyname('Datalimite').asstring:=Self.Datalimite;
        fieldbyname('Valor').asstring:=Self.Valor;
        fieldbyname('Debito_Credito').asstring:=Self.Debito_Credito;
        fieldbyname('Portador').asstring:=Self.Portador.Get_codigo;
        fieldbyname('ContaGerencial').asstring:=Self.ContaGerencial.GET_CODIGO;
        fieldbyname('SubContaGerencial').asstring:=Self.SubContaGerencial.GET_CODIGO;
        fieldbyname('ConsiderarMesesSubsequentes').asstring:=Self.ConsiderarMesesSubsequentes;
        fieldbyname('CredorDevedor').asstring:=Self.CredorDevedor.Get_codigo;
        fieldbyname('codigoCredorDevedor').asstring:=Self.codigoCredorDevedor;
        fieldbyname('somatoriaprevisao').asstring:=Self.somatoriaprevisao;
  End;
End;

//***********************************************************************

function TObjPrevisaoFinanceira.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjPrevisaoFinanceira.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        CODIGO:='';
        Historico:='';
        Data:='';
        Datalimite:='';
        Valor:='';
        Debito_Credito:='';
        Portador.ZerarTabela;
        ContaGerencial.ZerarTabela;
        SubContaGerencial.ZerarTabela;
        ConsiderarMesesSubsequentes:='';
        CredorDevedor.zerartabela;
        CodigoCredorDevedor:='';
        somatoriaprevisao:='';
        
     End;
end;

Function TObjPrevisaoFinanceira.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (historico='')
       Then Mensagem:=mensagem+'/Hist�rico';
       If (data='')
       Then Mensagem:=mensagem+'/Data';

       If (comebarra(trim(self.datalimite))='')
       Then self.datalimite:='';

       If (valor='')
       Then Mensagem:=mensagem+'/valor';
       If (Debito_Credito='')
       Then Mensagem:=mensagem+'/D�bito ou Cr�dito';

       If (Portador.Get_codigo='')
       Then Mensagem:=mensagem+'/Portador';

       if (somatoriaprevisao='')
       Then somatoriaprevisao:='N';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjPrevisaoFinanceira.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.Portador.LocalizaCodigo(Self.Portador.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';

     if (Self.ContaGerencial.get_codigo<>'')
     then begin
             If (Self.ContaGerencial.LocalizaCodigo(Self.ContaGerencial.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ ContaGerencial n�o Encontrado!'
             Else Begin
                      Self.ContaGerencial.TabelaparaObjeto;
                      if (Self.ContaGerencial.Get_Tipo<>Self.Get_Debito_Credito)
                      Then Begin
                                mensagemErro('O tipo da conta gerencial deve ser igual ao tipo do t�tulo a ser lan�ado');
                                result:=false;
                                exit;
                      End;
             End;
     End;

     if (Self.SubContaGerencial.get_codigo<>'')
     then begin
               If (Self.SubContaGerencial.LocalizaCodigo(Self.SubContaGerencial.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ SubContaGerencial n�o Encontrado!';
     End;


     if (Self.CredorDevedor.get_codigo<>'')
     then begin
               If (Self.CredorDevedor.LocalizaCodigo(Self.CredorDevedor.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Credor Devedor n�o Encontrado!'
               Else
                  if (Self.CredorDevedor.LocalizaCredorDevedor(Self.CodigoCredorDevedor)=False)
                  then Mensagem:=mensagem+'/ C�digo do Credor Devedor n�o Encontrado!'
     End
     Else Self.CodigoCredorDevedor:='';


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
     End;


     result:=true;
End;

function TObjPrevisaoFinanceira.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.Portador.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Portador';
     End;


     try
        reais:=Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;

     try
        if (Self.ContaGerencial.get_codigo<>'')
        Then StrtoInt(Self.ContaGerencial.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Conta Gerencial';
     End;

     try
        if (Self.SubContaGerencial.get_codigo<>'')
        Then StrtoInt(Self.subContaGerencial.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Conta Gerencial';
     End;



     try
        if (Self.CredorDevedor.get_codigo<>'')
        Then StrtoInt(Self.CredorDevedor.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Credor Devedor';
     End;

     try
        if (Self.CodigoCredorDevedor<>'')
        Then StrtoInt(Self.CodigoCredorDevedor);
     Except
           Mensagem:=mensagem+'/C�digo Credor Devedor';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjPrevisaoFinanceira.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        if (self.DataLimite<>'')
        then strtodate(Self.datalimite);
     Except
           Mensagem:=mensagem+'/Data Limite';
     End;

{
     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjPrevisaoFinanceira.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';
     If ((Self.Debito_Credito<>'C') and (Self.Debito_Credito<>'D'))
     Then mensagem:=mensagem+'Valor Inv�lido para D�bito ou Cr�dito';

     If not((Self.CONSIDERARMESESSUBSEQUENTES='S') OR (Self.CONSIDERARMESESSUBSEQUENTES='N'))
     Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Considerar Meses Subsequentes'
     Else Begin
               if (self.ConsiderarMesesSubsequentes='S') and (Self.DataLimite='')
               Then Begin
                         result:=false;
                         MensagemErro('Especifique uma data de limite');
                         exit;
               End;
     End;

     if ((Self.somatoriaprevisao<>'S')and (Self.somatoriaprevisao<>'N'))
     Then Mensagem:=Mensagem+'/Valor Inv�lido para o Campo Somat�ria na Previs�o';


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjPrevisaoFinanceira.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Historico,Data,Valor,Debito_Credito,portador,ContaGerencial,SubContaGerencial,ConsiderarMesesSubsequentes,DataLimite,CredorDevedor,CodigoCredorDevedor,somatoriaprevisao');
           SelectSQL.ADD(' from  TABPREVISAOFINANCEIRA');
           SelectSQL.ADD(' WHERE CODIGO='+Parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjPrevisaoFinanceira.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPrevisaoFinanceira.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPrevisaoFinanceira.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        //Self.ObjDatasource:=TDataSource.Create(nil);
        //Self.CodCurso:=TObjCursos.create;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Portador:=TObjPortador.create;
        Self.ContaGerencial:=TOBJCONTAGER.create;
        Self.SubContaGerencial:=TOBJSUBCONTAGERENCIAL.create;
        Self.CredorDevedor:=TobjCredorDevedor.create;

        Self.ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSql.clear;
                SelectSQL.ADD('Select CODIGO,Historico,Data,Valor,Debito_Credito,portador,ContaGerencial,SubContaGerencial,ConsiderarMesesSubsequentes,DataLimite,CredorDevedor,CodigoCredorDevedor,somatoriaprevisao');
                SelectSQL.ADD(' from  TABPREVISAOFINANCEIRA');
                SelectSQL.ADD(' WHERE CODIGO=0');

                InsertSql.clear;
                InsertSQL.add('Insert Into TABPREVISAOFINANCEIRA(CODIGO,Historico,Data');
                InsertSQL.add(' ,Valor,Debito_Credito,portador,ContaGerencial,SubContaGerencial,ConsiderarMesesSubsequentes,DataLimite,CredorDevedor,CodigoCredorDevedor,somatoriaprevisao)');
                InsertSQL.add('values (:CODIGO,:Historico,:Data,:Valor,:Debito_Credito,:portador,:ContaGerencial,:SubContaGerencial,:ConsiderarMesesSubsequentes,:DataLimite,:CredorDevedor,:CodigoCredorDevedor,:somatoriaprevisao');
                InsertSQL.add(' )');

                ModifySQL.clear;
                ModifySQL.add('Update TABPREVISAOFINANCEIRA set CODIGO=:CODIGO,Historico=:Historico');
                ModifySQL.add(',Data=:Data,Valor=:Valor,Debito_Credito=:Debito_Credito,portador=:portador');
                ModifySQL.add(',ContaGerencial=:ContaGerencial,SubContaGerencial=:SubContaGerencial,ConsiderarMesesSubsequentes=:ConsiderarMesesSubsequentes,DataLimite=:DataLimite,CredorDevedor=:CredorDevedor,CodigoCredorDevedor=:CodigoCredorDevedor');
                ModifySQL.add(',somatoriaprevisao=:somatoriaprevisao where CODIGO=:CODIGO');

                DeleteSql.clear;
                DeleteSql.add('Delete from TABPREVISAOFINANCEIRA where CODIGO=:CODIGO ');

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,Historico,Data,Valor,Debito_Credito,portador,ContaGerencial,SubContaGerencial,ConsiderarMesesSubsequentes,DataLimite,credordevedor,codigocredordevedor');
                RefreshSQL.ADD(',somatoriaprevisao from  TABPREVISAOFINANCEIRA');
                RefreshSQL.ADD('WHERE CODIGO=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjPrevisaoFinanceira.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjPrevisaoFinanceira.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjPrevisaoFinanceira.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPrevisaoFinanceira.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPrevisaoFinanceira');
     Result:=Self.ParametroPesquisa;
end;

function TObjPrevisaoFinanceira.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Previs�o Financeira ';
end;

function TObjPrevisaoFinanceira.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_PREVISAOFINANCEIRA';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o PrevisaoFinanceira',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjPrevisaoFinanceira.Free;
begin
   Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);
   Portador.free;
   Self.ContaGerencial.Free;
   Self.SubContaGerencial.Free;
   Self.CredorDevedor.free;
end;

procedure TOBJPREVISAOFINANCEIRA.Submit_Historico(parametro: string);
begin
        Self.Historico:=Parametro;
end;
function TOBJPREVISAOFINANCEIRA.Get_Historico: string;
begin
        Result:=Self.Historico;
end;
procedure TOBJPREVISAOFINANCEIRA.Submit_Data(parametro: string);
begin
        Self.Data:=Parametro;
end;
function TOBJPREVISAOFINANCEIRA.Get_Data: string;
begin
        Result:=Self.Data;
end;
procedure TOBJPREVISAOFINANCEIRA.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TOBJPREVISAOFINANCEIRA.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
procedure TOBJPREVISAOFINANCEIRA.Submit_Debito_Credito(parametro: string);
begin
        Self.Debito_Credito:=Parametro;
end;
function TOBJPREVISAOFINANCEIRA.Get_Debito_Credito: string;
begin
        Result:=Self.Debito_Credito;
end;

procedure TObjPREVISAOFINANCEIRA.EdtContaGerencialExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ContaGerencial.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ContaGerencial.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.ContaGerencial.GET_NOME;
End;
procedure TObjPREVISAOFINANCEIRA.EdtContaGerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel;PdebitoCredito:string);
var
   FpesquisaLocal:Tfpesquisa;
   FCONTAGER:TFCONTAGER;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCONTAGER:=TFCONTAGER.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ContaGerencial.Get_Pesquisa2(PdebitoCredito),Self.ContaGerencial.Get_TituloPesquisa,FContaGer)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ContaGerencial.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.ContaGerencial.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ContaGerencial.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCONTAGER);
     End;
end;
procedure TObjPREVISAOFINANCEIRA.EdtSubContaGerencialExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SubContaGerencial.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SubContaGerencial.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.SubContaGerencial.GET_NOME;
End;
procedure TObjPREVISAOFINANCEIRA.EdtSubContaGerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FSUBCONTAGERENCIAL:TFSUBCONTAGERENCIAL;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FSUBCONTAGERENCIAL:=TFSUBCONTAGERENCIAL.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.SubContaGerencial.Get_Pesquisa,Self.SubContaGerencial.Get_TituloPesquisa,FSubContaGerencial)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SubContaGerencial.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.SubContaGerencial.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SubContaGerencial.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FSUBCONTAGERENCIAL);
     End;
end;


procedure TObjprevisaofinanceira.edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState;lbnome:Tlabel;PContager:string);

var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            if (lbnome<>nil)
            then lbnome.caption:='';

            If (FpesquisaLocal.PreparaPesquisa(Self.SubContaGerencial.Get_Pesquisa(Pcontager),self.SubContaGerencial.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then begin
                                  TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

                                  if (lbnome<>nil)
                                  then lbnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TObjprevisaofinanceira.Submit_ConsiderarMesesSubsequentes(parametro: string);
begin
        Self.ConsiderarMesesSubsequentes:=Parametro;
end;
function TObjprevisaofinanceira.Get_ConsiderarMesesSubsequentes: string;
begin
        Result:=Self.ConsiderarMesesSubsequentes;
end;




function TObjPrevisaoFinanceira.LancaTitulo(Pcodigo: string): boolean;
var
Ftitulo:TFtitulo;
begin
Try
   Ftitulo:=TFtitulo.create(nil);


     result:=False;
     //este procedimento apaga a previsao que esta no parametro
     //e abre o cadastro de titulos com campos pr�-preenchidos
     If (Self.LocalizaCodigo(Pcodigo)=False)
     Then Begin
               Messagedlg('A previs�o n�o foi encontrada!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;
     If (Self.Exclui(Pcodigo,True)=False)
     Then Begin
               Messagedlg('N�o foi poss�vel excluir a previs�o atual!',mterror,[mbok],0);
               exit;
     End;
     Uobjtitulo.LimpaRegLancTituloExterno;
     Uobjtitulo.RegLancTituloExterno.LancamentoExterno:=True;
     Uobjtitulo.RegLancTituloExterno.LE_HISTORICO:=Self.Historico;
     Uobjtitulo.RegLancTituloExterno.LE_EMISSAO:=Self.Data;
     Uobjtitulo.RegLancTituloExterno.LE_PORTADOR:=Self.Portador.get_codigo;
     Uobjtitulo.RegLancTituloExterno.LE_VALOR:=Self.valor;
     Uobjtitulo.RegLancTituloExterno.LE_ParcelasIguais:=true;
     Uobjtitulo.RegLancTituloExterno.LE_CONTAGERENCIAL:=Self.ContaGerencial.Get_CODIGO;
     Uobjtitulo.RegLancTituloExterno.LE_SUBCONTAGERENCIAL:=Self.SubContaGerencial.Get_CODIGO;
     Uobjtitulo.RegLancTituloExterno.LE_CREDORDEVEDOR:=Self.CredorDevedor.Get_CODIGO;
     Uobjtitulo.RegLancTituloExterno.LE_CODIGOCREDORDEVEDOR:=Self.CodigoCredorDevedor;




     Ftitulo.showmodal;
     Messagedlg('T�tulo Exclu�do com Sucesso!',mtinformation,[mbok],0);
     result:=True;

Finally
       Freeandnil(Ftitulo);
End;

end;

function TObjPrevisaoFinanceira.Get_DataLimite: string;
begin
     Result:=Self.DataLimite;
end;

procedure TObjPrevisaoFinanceira.Submit_DataLimite(parametro: string);
begin
     Self.DataLimite:=parametro;
end;

function TObjPrevisaoFinanceira.Get_CodigoCredorDevedor: string;
begin
     Result:=Self.CodigoCredorDevedor;
end;

procedure TObjPrevisaoFinanceira.Submit_CodigoCredorDevedor(
  parametro: string);
begin
     Self.CodigoCredorDevedor:=parametro;
end;

function TObjPrevisaoFinanceira.Get_somatoriaprevisao: string;
begin
     Result:=Self.somatoriaprevisao;
end;

procedure TObjPrevisaoFinanceira.Submit_somatoriaprevisao(
  parametro: string);
begin
     Self.somatoriaprevisao:=Parametro;
end;

end.
