unit ULanctoPortadorFR;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls, ComCtrls,UObjLanctoPortador,db,Upesquisa;

type
  TFrLanctoPortador = class(TFrame)
    Guia: TPageControl;
    TabSheet1: TTabSheet;
    Label1: TLabel;
    EdtCodigo: TEdit;
    edttipolancto: TEdit;
    Label2: TLabel;
    Label5: TLabel;
    edtportador: TEdit;
    edthistorico: TEdit;
    Label3: TLabel;
    Label6: TLabel;
    edtvalor: TEdit;
    edtdata: TMaskEdit;
    Label4: TLabel;
    LbTipoLanctoNome: TLabel;
    edtcodigohistoricosimples: TEdit;
    lbportador: TLabel;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    BtNovo: TBitBtn;
    Btgravar: TBitBtn;
    btpesquisar: TBitBtn;
    BtCancelar: TBitBtn;
    btexcluir: TBitBtn;
    btrecibo: TBitBtn;
    procedure BtNovoClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edttipolanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edttipolanctoExit(Sender: TObject);
    procedure edttipolanctoKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure edtcodigohistoricosimplesExit(Sender: TObject);
    procedure edtcodigohistoricosimplesKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
    procedure edtcodigohistoricosimplesKeyPress(Sender: TObject;var Key: Char);
    procedure edtportadorExit(Sender: TObject);
    procedure Label7Click(Sender: TObject);
    procedure btreciboClick(Sender: TObject);


  private
    { Private declarations }
          Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;

  public
    { Public declarations }
    Function Criar:boolean;
    Procedure Destruir;

  end;



implementation

uses UessencialGlobal, UTipoLanctoPortador, Uportador,
  UHistoricoSimples, UTitulo, UDataModulo, UescolheImagemBotao;
{$R *.DFM}


//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFrLanctoPortador.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjlanctoportadorGlobal do
    Begin
         Submit_CODIGO          (Self.edtCODIGO.text);
         Submit_Portador        (Self.edtportador.text);
         Submit_TipoLancto      (Self.edttipolancto.text);
         Submit_Historico       (Self.edthistorico.text);
         Submit_Data            (Self.edtdata.Text);
         Submit_Valor           (Self.edtvalor.text);
         Submit_ImprimeRel      (True);

         result:=true;
    End;
  Except
        result:=False;
  End;

end;

function TFrLanctoPortador.ObjetoParaControles: Boolean;
begin
  Try
     With ObjlanctoportadorGlobal do
     Begin
        Self.edtCODIGO.text          :=Get_CODIGO            ;
        Self.edtPortador.text        :=Get_Portador          ;
        Self.edtTipoLancto.text      :=Get_TipoLancto        ;
        Self.edtHistorico.text       :=Get_Historico         ;
        Self.edtData.text            :=Get_Data              ;
        Self.edtValor.text           :=Get_Valor             ;
        If (Self.edtTipoLancto.text<>'')
        Then Self.LbTipoLanctoNome.caption:=ObjlanctoportadorGlobal.TipoLancto.Get_Historico;
        If (Self.edtportador.text<>'')
        Then Self.lbportador.caption:=ObjlanctoportadorGlobal.Portador.Get_Nome;
        
        result:=True;
     End;
  Except
        Result:=False;
  End;

end;

function TFrLanctoPortador.TabelaParaControles: Boolean;
begin
     ObjlanctoportadorGlobal.TabelaparaObjeto;
     Self.ObjetoParaControles;

end;

//****************************************
//****************************************

Function TFrLanctoPortador.Criar:boolean;
begin
     limpaedit(Self);
     lbportador.caption:='';
     desabilita_campos(Self);
     Guia.ActivePageIndex:=0;

     FescolheImagemBotao.PegaFiguraBotoesPequeno(btnovo,nil,btcancelar,btgravar,btpesquisar,btrecibo,btexcluir,nil);
     result:=True;

end;

procedure TFrLanctoPortador.destruir;
begin
     If (ObjlanctoportadorGlobal.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;
end;


procedure TFrLanctoPortador.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     lbportador.caption:='';
     LbTipoLanctoNome.caption:='';

     habilita_campos(Self);
     esconde_botoes(Self);
     Self.Btgravar.Visible:=True;
     Self.BtCancelar.Visible:=True;
     


     ObjlanctoportadorGlobal.ZerarTabela;
     Self.edtcodigo.text:=ObjlanctoportadorGlobal.Get_NovoCodigo;
     Self.edtcodigo.enabled:=False;

     ObjlanctoportadorGlobal.status:=dsInsert;
     Self.Guia.ActivePageindex:=0;
     edtdata.text:=Datetostr(Now);
     edttipolancto.setfocus;

end;

procedure TFrLanctoPortador.BtCancelarClick(Sender: TObject);
begin
     ObjlanctoportadorGlobal.cancelar;

     limpaedit(Self);
     lbportador.caption:='';
     desabilita_campos(Self);
     mostra_botoes(Self);

     LbTipoLanctoNome.caption:='';

end;


procedure TFrLanctoPortador.BtgravarClick(Sender: TObject);
var
Ftitulo:TFtitulo;
begin
     Try
        Ftitulo:=TFtitulo.create(nil);


     If ObjlanctoportadorGlobal.Status=dsInactive
     Then exit;

     If Self.ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     if (ObjLanctoPortadorGlobal.ValidaData=False)
     then Begin
              MensagemErro('Lan�amento n�o autorizado');
              exit;
     End;

     If (ObjlanctoportadorGlobal.salvar(true,true,true)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     lbportador.caption:='';
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
     LbTipoLanctoNome.caption:='';

     ObjlanctoportadorGlobal.tipolancto.localizacodigo(ObjlanctoportadorGlobal.TipoLancto.Get_CODIGO);
     ObjlanctoportadorGlobal.TipoLancto.TabelaparaObjeto;

     If (ObjlanctoportadorGlobal.TipoLancto.Get_LancaTitulo='S')
     Then FTitulo.Showmodal;

Finally
            Freeandnil(Ftitulo);
End;

end;

procedure TFrLanctoPortador.PreparaAlteracao;
begin
     habilita_campos(Self);
     Self.EdtCodigo.enabled:=False;
     ObjlanctoportadorGlobal.Status:=dsEdit;
     Self.Guia.ActivePageIndex:=0;
     //edtNome.setfocus;
end;

procedure TFrLanctoPortador.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjlanctoportadorGlobal.Get_pesquisa,ObjlanctoportadorGlobal.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjlanctoportadorGlobal.status<>dsinactive
                                  then exit;

                                  If (ObjlanctoportadorGlobal.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  Self.TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;

end;

procedure TFrLanctoPortador.btexcluirClick(Sender: TObject);
Var pautorizou:String;
begin

     If (ObjlanctoportadorGlobal.status<>dsinactive) or (Self.Edtcodigo.text='')
     Then exit;


     // Alterado por F�bio ********************
     if (ObjParametroGlobal.ValidaParametro('PEDIR SENHA AO APAGAR LANCAMENTO FINANCEIRO')=true)
     then Begin
              if (ObjParametroGlobal.Get_Valor = 'SIM')
              then Begin
                     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('APAGAR LAN�AMENTO FINANCEIRO PORTADDOR')=False)
                     Then Begin
                          //senha
                          if (ObjPermissoesUsoGlobal.PedePermissao(ObjPermissoesUsoGlobal.get_codigo,pautorizou,'Senha lan�amento retroativo')=false)
                          Then exit;

                          pautorizou:='AUTORIZADO';
                     End;
              end else pautorizou:='AUTORIZADO';
     end;

     if (pautorizou = '')
     then exit;
     //*************************************


     ObjlanctoportadorGlobal.ZerarTabela;
     If (ObjlanctoportadorGlobal.LocalizaCodigo(Self.edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;
     ObjlanctoportadorGlobal.TabelaparaObjeto;

     if   (ObjLanctoPortadorGlobal.TransferenciaPortador.Get_CODIGO<>'')
       or (ObjLanctoPortadorGlobal.Valores.Get_CODIGO<>'')
       or (ObjLanctoPortadorGlobal.ValoresTransferenciaPortador.Get_CODIGO<>'')
       or (ObjLanctoPortadorGlobal.Lancamento.Get_CODIGO<>'')
     Then Begin
               Messagedlg('Este Lan�amento n�o pode ser exclu�do porque est� ligado a algum lan�amento ou transfer�ncia. Voc� deve excluir quem o gerou!',mtInformation,[mbok],0);
               exit;
     end;


     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;
     
     If (ObjlanctoportadorGlobal.exclui(Self.edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbportador.caption:='';
     LbTipoLanctoNome.caption:='';
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFrLanctoPortador.edttipolanctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Ftipolanctoportador:TFtipolanctoportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Ftipolanctoportador:=TFtipolanctoportador.create(nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FrLanctoPortador.Edtipolancto';
            If (FpesquisaLocal.PreparaPesquisa(ObjlanctoportadorGlobal.Get_PesquisatipoLanctoPortador,ObjlanctoportadorGlobal.Get_TituloPesquisaTipoLanctoPortador,Ftipolanctoportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If Self.edtTipoLancto.text<>''
                                 Then Self.LbTipoLanctoNome.caption:=ObjlanctoportadorGlobal.PegaNometipolancto(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Ftipolanctoportador);
     End;

end;

procedure TFrLanctoPortador.edtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador:=TFportador.create(nil);
             FpesquisaLocal.NomeCadastroPersonalizacao:='FrLanctoPortador.edtportador';
            If (FpesquisaLocal.PreparaPesquisa(ObjlanctoportadorGlobal.Get_PesquisaPortador,ObjlanctoportadorGlobal.Get_TituloPesquisaPortador,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 lbportador.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                             End;

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;

end;

procedure TFrLanctoPortador.edttipolanctoExit(Sender: TObject);
begin
      If Self.edtTipoLancto.text<>''
      Then Self.LbTipoLanctoNome.caption:=ObjlanctoportadorGlobal.PegaNometipolancto(edttipolancto.text);

end;

procedure TFrLanctoPortador.edttipolanctoKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in ['0'..'9',#8])
     Then key:=#0;

end;

procedure TFrLanctoPortador.edtvalorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(Key in['0'..'9',',',#8])
     Then Begin
               If key='.'
               Then key:=','
               Else key:=#0;
          End;

end;

procedure TFrLanctoPortador.edtcodigohistoricosimplesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:TFpesquisa;
   FHistoricoSimples:TFHistoricoSimples;
begin
        If (key<>vk_f9)
        Then exit;

        Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FHistoricoSimples:=TFHistoricoSimples.create(nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FrLanctoPortador.edtcodigohistoricosimples';
            If (FpesquisaLocal.PreparaPesquisa(ObjPortador.historicocontabil.Get_pesquisa,ObjPortador.historicocontabil.Get_TituloPesquisa,FHistoricoSimples)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
                                  If (ObjPortador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
                                  Then Begin
                                            TEdit(Sender).text:='';
                                            exit;
                                  End;
                                  ObjPortador.HIstoricoContabil.TabelaparaObjeto;
                                  edthistorico.text:=ObjPortador.HIstoricoContabil.Get_Nome;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FHistoricoSimples);
        End;
end;

procedure TFrLanctoPortador.edtcodigohistoricosimplesExit(Sender: TObject);
begin
     IF (TEdit(Sender).text='')
     Then exit;

     If (ObjPortador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     ObjPortador.HIstoricoContabil.TabelaparaObjeto;
     edthistorico.text:=ObjPortador.HIstoricoContabil.Get_Nome;
end;

procedure TFrLanctoPortador.edtcodigohistoricosimplesKeyPress(Sender: TObject;
  var Key: Char);
begin
     If (not (key in ['0'..'9',#8]))
     Then key:=#0;
end;

procedure TFrLanctoPortador.edtportadorExit(Sender: TObject);
begin
     iF (edtportador.text<>'')
     Then BEgin
               if (ObjlanctoportadorGlobal.Portador.LocalizaCodigo(edtportador.text)=False)
               Then Begin
                        lbportador.caption:='';
                        edtportador.text:='';
                        exit;
               End;
               ObjlanctoportadorGlobal.Portador.TabelaparaObjeto;
               lbportador.caption:=ObjlanctoportadorGlobal.Portador.Get_Nome;
               exit;
     End;
     LBPORTADOR.CAPTION:='';
end;

procedure TFrLanctoPortador.Label7Click(Sender: TObject);
begin
     If (ObjlanctoportadorGlobal.status<>dsinactive)
     Then exit;
     ObjlanctoportadorGlobal.Ajeitahistoricos;
end;

procedure TFrLanctoPortador.btreciboClick(Sender: TObject);
begin
    if (edtcodigo.text='')
    then exit;
    
    ObjlanctoportadorGlobal.ImprimeRecibo(EdtCodigo.text);
end;

end.

