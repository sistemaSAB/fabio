unit UTitulo_novo;

interface

uses
  UessencialGlobal, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPendencia,
  Grids, ULancamentoFR_novo, DBGrids, ibquery, IBCustomDataSet;

type
  TFtitulo_novo = class(TForm)
    panelbotoes: TPanel;
    lbnomeformulario: TLabel;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;           
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    btpesquisar: TBitBtn;
    PanelCabecalho: TPanel;
    Label2: TLabel;
    edtcodigohistoricosimples: TEdit;
    edthistorico: TEdit;
    Label3: TLabel;
    combocredordevedor: TComboBox;
    combocredordevedorcodigo: TComboBox;
    Label4: TLabel;
    edtcodigocredordevedor: TEdit;
    LbNomeCredorDevedor: TLabel;
    Label5: TLabel;
    edtemissao: TMaskEdit;
    Label6: TLabel;
    comboprazocodigo: TComboBox;
    comboprazonome: TComboBox;
    CheckParcelasIguais: TCheckBox;
    edtportador: TEdit;
    Label9: TLabel;
    LbPortador: TLabel;
    Label10: TLabel;
    edtvalor: TEdit;
    Label11: TLabel;
    edtcontagerencial: TEdit;
    LbContaGerencial: TLabel;
    Label14: TLabel;
    edtsubcontagerencial: TEdit;
    lbnomesubcontagerencial: TLabel;
    Label7: TLabel;
    edtnumdcto: TEdit;
    Label8: TLabel;
    edtnotafiscal: TEdit;
    Shape1: TShape;
    memoobservacao: TMemo;
    Label17: TLabel;
    edtcodigogerador: TEdit;
    combogerador: TComboBox;
    combogeradorcodigo: TComboBox;
    ImagemFundo: TImage;
    btpesquisasemsaldo: TBitBtn;
    btpesquisatitulo: TBitBtn;
    btpesquisaboleto: TBitBtn;
    Label20: TLabel;
    Label16: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label25: TLabel;
    LbMesCorrente: TLabel;
    Label35: TLabel;
    btAtualizarVencimentos: TBitBtn;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label31: TLabel;
    lbReceberHoje: TLabel;
    lbPagarHoje: TLabel;
    lbPagarMes: TLabel;
    lbReceberMes: TLabel;
    lbReceberGeral: TLabel;
    LbPagarGeral: TLabel;
    lbcodigo: TLabel;
    btpesquisapendencia: TBitBtn;
    btPesquisaDuplicata: TBitBtn;
    Label23: TLabel;
    Label24: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Image2: TImage;
    Guia: TTabbedNotebook;
    PanelParcelas: TPanel;
    Panel5: TPanel;
    LbCRCP: TLabel;
    BtPesqMensa: TBitBtn;
    StrGridPendencias: TStringGrid;
    Panel2: TPanel;
    Label1: TLabel;
    lbboletos: TLabel;
    BthistoricoPendencias: TBitBtn;
    btopcoesboleto: TBitBtn;
    PanelLancamentoLote: TPanel;
    FrLancamento1: TFrLancamento_novo;
    PanelTitulosReceber: TPanel;
    Label15: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    lbTotalConta: TLabel;
    lbRecebido: TLabel;
    lbSaldo: TLabel;
    rbContasComSaldo: TRadioButton;
    rbContasSemSaldo: TRadioButton;
    Label12: TLabel;
    GuiaTitulos: TTabbedNotebook;
    DBGridReceber: TDBGrid;
    DBGridPagar: TDBGrid;
    QueryPesq: TIBQuery;
    DataSource1: TDataSource;
    Panel1: TPanel;
    lbQtdeRegistrosEncontrados: TLabel;
    EdtBusca: TMaskEdit;
    rbInciciaCom: TRadioButton;
    rbTenhaemQualquerParte: TRadioButton;
    rbExata: TRadioButton;
    btAjuda: TSpeedButton;
    pnlPesquisa: TPanel;
    lbPesquisando: TLabel;
    rb100: TRadioButton;
    rb1000: TRadioButton;
    rbTodos: TRadioButton;
    rbHoje: TRadioButton;
    chkAtualizaraoAbrir: TCheckBox;
    btpesquisacr: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure combogeradorChange(Sender: TObject);
    procedure combocredordevedorChange(Sender: TObject);
    procedure combocredordevedorcodigoChange(Sender: TObject);
    procedure combogeradorcodigoChange(Sender: TObject);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigogeradorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure comboprazonomeChange(Sender: TObject);
    procedure comboprazocodigoChange(Sender: TObject);
    procedure GuiaTitulosChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure StrGridPendenciasDblClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure StrGridPendenciasKeyPress(Sender: TObject; var Key: Char);
    procedure comboprazonomeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtPesqMensaClick(Sender: TObject);
    procedure edtportadorExit(Sender: TObject);
    procedure edtcontagerencialExit(Sender: TObject);
    procedure edtvalorExit(Sender: TObject);
    procedure btpesquisasemsaldoClick(Sender: TObject);
    procedure edtcodigocredordevedorExit(Sender: TObject);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure FrLancamento1BtpesquisaLoteClick(Sender: TObject);
    procedure FrLancamento1BtexecutaLoteClick(Sender: TObject);
    procedure edtcodigohistoricosimplesKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure edtcodigohistoricosimplesExit(Sender: TObject);
    procedure edtcodigohistoricosimplesKeyPress(Sender: TObject;
      var Key: Char);
    procedure btopcoesClick(Sender: TObject);
    procedure FrLancamento1btexecutacrClick(Sender: TObject);
    procedure btopcoesboletoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PesquisaParcela(Sender: TObject);
    procedure BthistoricoPendenciasClick(Sender: TObject);
    procedure PesquisaBoleto(Sender: TObject);
    procedure PesquisaTitulo(Sender: TObject);
    procedure FrLancamento1btpesquisaClick(Sender: TObject);
    procedure FrLancamento1BtApagaLoteClick(Sender: TObject);
    procedure FrLancamento1btchequecaixaClick(Sender: TObject);
    procedure FrLancamento1btlancaboletoClick(Sender: TObject);
    procedure FrLancamento1btimprimecomprovantepagamentoClick(
      Sender: TObject);
    procedure FrLancamento1btpesquisacrClick(Sender: TObject);
    procedure edtcodigocredordevedorKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtsubcontagerencialExit(Sender: TObject);
    procedure edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure PesquisaDuplicata(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FrLancamento1btAlterarHistoricoLancamentoClick(
      Sender: TObject);
    procedure FrLancamento1strgcrKeyPress(Sender: TObject; var Key: Char);
    procedure FrLancamento1strgcrKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FrLancamento1STRGCPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FrLancamento1STRGCPKeyPress(Sender: TObject; var Key: Char);
    procedure rbContasComSaldoClick(Sender: TObject);
    procedure rbContasSemSaldoClick(Sender: TObject);
    procedure DBGridReceberKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridReceberDblClick(Sender: TObject);
    procedure DBGridPagarDblClick(Sender: TObject);
    procedure DBGridPagarKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridReceberKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridPagarKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridReceberCellClick(Column: TColumn);
    procedure DBGridPagarCellClick(Column: TColumn);
    procedure btAtualizarVencimentosClick(Sender: TObject);
    procedure Sublinhado(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SoNegrito(Sender: TObject);
    procedure FrLancamento1BtexecutaCPClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure FrLancamento1GuiaChange(Sender: TObject);
    procedure FrLancamento1chkSelecionaTodasCPClick(Sender: TObject);
    procedure FrLancamento1chkSelecionaTodasCRClick(Sender: TObject);
    procedure FrLancamento1BtSelecionaTodas_PGClick(Sender: TObject);
    procedure FrLancamento1Label24Click(Sender: TObject);
    procedure FrLancamento1Label25Click(Sender: TObject);
    procedure DBGridPagarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridReceberKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rbExataClick(Sender: TObject);
    procedure rbInciciaComClick(Sender: TObject);
    procedure rbTenhaemQualquerParteClick(Sender: TObject);
    procedure EdtBuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtBuscaKeyPress(Sender: TObject; var Key: Char);
    procedure edtportadorKeyPress(Sender: TObject; var Key: Char);
    procedure edtcontagerencialKeyPress(Sender: TObject; var Key: Char);
    procedure edtsubcontagerencialKeyPress(Sender: TObject; var Key: Char);
    procedure FrLancamento1edtcodigocredordevedorCPKeyPress(
      Sender: TObject; var Key: Char);
    procedure FrLancamento1edtcontagerencial_CPKeyPress(Sender: TObject;
      var Key: Char);
    procedure FrLancamento1edtsubcontagerencial_CPKeyPress(Sender: TObject;
      var Key: Char);
    procedure FrLancamento1edtcodigohistoricoCPKeyPress(Sender: TObject;
      var Key: Char);
    procedure FrLancamento1edtcodigocredordevedorcrKeyPress(
      Sender: TObject; var Key: Char);
    procedure FrLancamento1edtcodigoboletoKeyPress(Sender: TObject;
      var Key: Char);
    procedure FrLancamento1edtcodigohistoricoCRKeyPress(Sender: TObject;
      var Key: Char);
    procedure DBGridReceberDrawColumnCell(Sender: TObject;
      const Rect: TRect; DataCol: Integer; Column: TColumn;
      State: TGridDrawState);
    procedure DBGridPagarDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FrLancamento1BtSelecionaTodas_CRClick(Sender: TObject);
    procedure FrLancamento1CheckSelicionaPagamentoClick(Sender: TObject);
    procedure FrLancamento1CheckSelecionaRecebimentoClick(Sender: TObject);
    procedure memoobservacaoExit(Sender: TObject);
    procedure FrLancamento1BtpesquisaCPClick(Sender: TObject);
    procedure FrLancamento1edtcodigocredordevedorCPKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
    procedure StrGridPendenciasMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAjudaClick(Sender: TObject);
    procedure rb100Click(Sender: TObject);
    procedure rb1000Click(Sender: TObject);
    procedure rbTodosClick(Sender: TObject);
    procedure FrLancamento1lbDefaultMouseLeave(Sender: TObject);
    procedure FrLancamento1lbDefaultMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure rbHojeClick(Sender: TObject);
    procedure chkAtualizaraoAbrirClick(Sender: TObject);

  { Private declarations }
  private
    str_pegacampo, str_valorpesquisado:string;
    comandosql:string;
    Invisiveis:TStringList;
    str_campopesquisainicial,str_valorcampopesquisainicial:String;
    opPesquisa : Integer; {Rodolfo}
    AtualizaPanelRodape : Boolean;
    function  ControlesParaObjeto:Boolean;
    function  ObjetoParaControles:Boolean;
    function  TabelaParaControles:Boolean;
    procedure PreparaAlteracao;
    procedure Preenche_Dados_Externo;
    procedure limpalabels;
    procedure PersonalizaGrid;
    procedure PersonalizaColunas;
    procedure ListaTitulos(Ptipo, PComSemSaldo: string);
    procedure LimpaGridParcelas;
    procedure ListaTitulos2(pTipo, pComSemSaldo : string); //Rodolfo

  { Public declarations }
  public

    QuitaPrimeiraParcela:Boolean;
    NomeCadastroPersonalizacao:String;
    PesquisaEmEstoque:Boolean;

    Procedure LocalizaCodigoInicial(Pcodigo:string);
    procedure AtualizaLabelsValores(Ptipo:string);

    function VerificaOpcao: String;  {Rodolfo}

    procedure AtualizaParametroInformacoes(pvalor:boolean);
    procedure LimpaLabelsPanelRodape;
  end;

var
  Ftitulo_novo: TFtitulo_novo;
  CodigoaserLocalizado:string='';
  Receber:Boolean;
  Pagar:Boolean;


implementation

uses Upesquisa, Uportador, UContaGer, UHistoricoLancamentos,
  UPrazoPagamento, UopcaoRel, UHistoricoSimples,
  UObjTitulo, UDataModulo, UescolheImagemBotao, DateUtils, Uprincipal,
  UAjuda;




{$R *.DFM}

//***********************************************
//********LANCAMENTO EXTERNO*********************
//***********************************************

procedure TFtitulo_novo.Preenche_Dados_Externo;
var
  auxilio:string;
  cont,contg:integer;
begin
  if RegLancTituloExterno.LancamentoExterno=True then
  begin
    with RegLancTituloExterno do
    begin
      lbcodigo.caption           :=LE_CODIGO             ;
      edthistorico.text        :=LE_HISTORICO          ;
      auxilio                  :=LE_CREDORDEVEDOR      ;

      if (auxilio<>'') then
      begin
        for cont:=0 to combocredordevedorcodigo.items.Count-1 do
        Begin
          if combocredordevedorcodigo.Items[cont]=auxilio then
          begin
            combocredordevedor.itemindex:=cont;
            combocredordevedorcodigo.itemindex:=cont;
            combocredordevedor.enabled:=False;
            break;
          End;
        End;
      End;

      If (LE_CODIGOCREDORDEVEDOR<>'') then
      begin
        edtcodigocredordevedor.text      :=LE_CODIGOCREDORDEVEDOR;
        edtcodigocredordevedor.enabled:=False;
      End;

      auxilio                          := LE_gerador;

      If auxilio<>'' then
      begin
        for cont:=0 to combogeradorcodigo.items.Count-1 do
        Begin
          if combogeradorcodigo.Items[cont]=auxilio then
          begin
            combogerador.itemindex:=cont;
            combogeradorcodigo.itemindex:=cont;
            combogeradorcodigo.enabled:=False;
            break;
          End;
        End;
        combogerador.enabled:=False;
        edtcodigogerador.text            :=LE_codigogerador;
        edtcodigogerador.enabled:=False;
      End
      Else Begin//sem gerador uso o 'SEM GERADOR'
        for contg:=0 to combogerador.Items.Count-1 do
        Begin
          If (combogerador.Items[contg]='SEM GERADOR') then
          begin
            combogerador.ItemIndex:=contg;
            combogeradorcodigo.ItemIndex:=contg;
            break;
          End;
        End;
        edtcodigogerador.text:='0';
      End;

      edtEMISSAO.text                 :=LE_EMISSAO           ;

      auxilio                         :=LE_prazo             ;
      for cont:=0 to comboprazocodigo.items.Count-1 do
      Begin
        if (comboprazocodigo.Items[cont]=auxilio) then
        begin
          comboprazocodigo.itemindex:=cont;
          comboprazonome.itemindex:=cont;
          break;
        End;
      End;

      edtPORTADOR.text                :=LE_PORTADOR          ;
      edtVALOR.text                   :=LE_VALOR             ;
      edtCONTAGERENCIAL.text          :=LE_CONTAGERENCIAL    ;
      edtSUBCONTAGERENCIAL.text          :=LE_SUBCONTAGERENCIAL    ;

      LbContaGerencial.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomeContaGerencial(edtcontagerencial.TEXT);
      LbPortador.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomePortador(EDTPORTADOR.TEXT);
    End;
  End
  Else
  Begin
    combogerador.text:='';
    combogerador.ItemIndex:=-1;
    for contg:=0 to combogerador.Items.Count-1 do
    Begin
      If (combogerador.Items[contg]='SEM GERADOR') then
      begin
        combogerador.ItemIndex:=contg;
        combogeradorcodigo.text:=combogeradorcodigo.items[contg];
        break;
      End;
    End;
    edtcodigogerador.text:='0';
    edtemissao.text:=datetostr(now);
  End;
end;



//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************

function TFtitulo_novo.ControlesParaObjeto: Boolean;
Begin
  Try
    With FrLancamento1.ObjLancamento.Pendencia.Titulo do
    Begin

      Submit_CODIGO         (lbcodigo.caption);
      Submit_HISTORICO      (edtHISTORICO.text);

      Submit_GERADOR        (combogeradorcodigo.text);
      Submit_codigoGERADOR  (edtcodigogerador.text);
      Submit_CREDORDEVEDOR  (comboCREDORDEVEDORcodigo.text);
      Submit_codigocredordevedor(edtcodigocredordevedor.text);


      Submit_EMISSAO        (edtEMISSAO.text);
      Submit_PRAZO          (comboprazocodigo.text);

      Submit_PORTADOR       (edtPORTADOR.text);
      Submit_VALOR          (tira_ponto(edtVALOR.text));
      Submit_CONTAGERENCIAL (edtCONTAGERENCIAL.text);
      SubContaGerencial.Submit_CODIGO(edtsubcontagerencial.text);


      Submit_ParcelasIguais (CheckParcelasIguais.Checked);
      Submit_NumDcto(edtnumdcto.text);
      Submit_GeradoPeloSistema('N');
      Submit_NotaFiscal(edtnotafiscal.text);
      Submit_ExportaLanctoContaGer('N');//este campo indica se no momento do lancamento de quitacao em vez e usar
      Submit_Observacao(memoobservacao.text);
      result:=true;//o credor devedor use a conta gerencial          
    End;
  Except
    result:=False;
  End;
End;

function TFtitulo_novo.ObjetoParaControles: Boolean;
var
  auxilio:string;
  cont:integer;
Begin
  Try
    With FrLancamento1.ObjLancamento.Pendencia.Titulo do
    Begin
      lbcodigo.caption          :=Get_CODIGO            ;
      edtHISTORICO.text       :=Get_HISTORICO         ;

      auxilio:= Get_CREDORDEVEDOR;

      for cont:=0 to combocredordevedor.items.Count-1 do
      Begin
        if combocredordevedor.Items[cont]=auxilio then
        begin
          combocredordevedor.itemindex:=cont;
          combocredordevedorcodigo.itemindex:=cont;
          break;
        End;
      End;
      edtcodigocredordevedor.text:=Get_CODIGOCREDORDEVEDOR;


      auxilio:= Get_gerador;

      for cont:=0 to combogerador.items.Count-1 do
      Begin
        if combogerador.Items[cont]=auxilio then
        begin
          combogerador.itemindex:=cont;
          combogeradorcodigo.itemindex:=cont;
          break;
        End;
      End;
      edtcodigogerador.text:=Get_codigogerador;

      edtEMISSAO.text         :=Get_EMISSAO           ;
      auxilio:= Get_prazo;
      for cont:=0 to comboprazocodigo.items.Count-1 do
      Begin
        if comboprazocodigo.Items[cont]=auxilio then
        begin
          comboprazocodigo.itemindex:=cont;
          comboprazonome.itemindex:=cont;
          break;
        End;
      End;


      edtPORTADOR.text        :=Get_PORTADOR          ;
      edtVALOR.text           :=formata_valor(Get_VALOR)             ;
      edtCONTAGERENCIAL.text  :=Get_CONTAGERENCIAL    ;
      LbContaGerencial.caption:=CONTAGERENCIAL.get_mascara+' '+CONTAGERENCIAL.Get_Nome;
        
      edtsubcontagerencial.text:=SubContaGerencial.Get_CODIGO;
      lbnomesubcontagerencial.caption:=SubContaGerencial.get_mascara+' '+SubContaGerencial.get_nome;



      If FrLancamento1.ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='D'
      Then LbCRCP.caption:='CONTA A PAGAR'
      Else LbCRCP.caption:='CONTA A RECEBER';

      LbPortador.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomePortador(EDTPORTADOR.TEXT);
      edtnumdcto.text:=Get_NumDcto;
      LbNomeCredorDevedor.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomeCredorDevedor(combocredordevedorcodigo.text,edtcodigocredordevedor.text);
      edtnotafiscal.text:=Get_NotaFiscal;
      memoobservacao.text:=Get_Observacao;
      Guia.PageIndex:=0; //sempre que carregar um titulo mostro a aba de parcelas (Mauricio 17/09/2009)
      Self.BtPesqMensaClick(nil);
      result:=True;
    End;
  Except
    Result:=False;
  End;
End;

function TFtitulo_novo.TabelaParaControles: Boolean;
begin
  FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
  ObjetoParaControles;
end;

//****************************************
//****************************************

procedure TFtitulo_novo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CodigoaserLocalizado:='';

  If (FrLancamento1.ObjLancamento.Pendencia=Nil)
  Then exit;

  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
    or (FrLancamento1.ObjLancamento.Status<>dsinactive)  then
  begin
    Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
    abort;
    exit;
  End;

  FrLancamento1.Destruir;

  //FrLancamento1.ObjLancamento.Pendencia.free;
  //NO caso de lancamento externo provavelmente o form que o chamou
  //possui um Frame Lancamento, se eu destrui-lo aqui, dara um
  //erro de excecao se eu precisar utiliza-lo
  //por isso verifico se vem de lancamento externo ou nao
  sELF.TAG:=0;
  Self.QuitaPrimeiraParcela:=False;
  Self.LimpaGridParcelas;
  Self.AtualizaParametroInformacoes(AtualizaPanelRodape);
  ObjParametroGlobal.RecarregaParametro_STL;

  if self.Owner = nil then
    FDataModulo.IBTransaction.Commit;

end;

procedure TFtitulo_novo.btsairClick(Sender: TObject);
begin
  Close;
end;

procedure TFtitulo_novo.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13
  Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFtitulo_novo.BtNovoClick(Sender: TObject);
begin

  if (SistemaemModoDemoGlobal=True) then
  begin
    if (FrLancamento1.ObjLancamento.Pendencia.Titulo.ContaTitulos>250) then
    begin
      MostraMensagemSistemaDemo('S� � permitido 250 T�tulos');
      exit;
    End;
  End;


  limpaedit(Self);
  habilita_campos(Self);
  esconde_botoes(panelbotoes);

  Self.limpalabels;
  Preenche_Dados_Externo;


  Guia.PageIndex:=0;

  lbcodigo.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NovoCodigo;

  Btgravar.Visible:=True;
  BtCancelar.Visible:=True;
  btpesquisar.Visible:=True;

  FrLancamento1.ObjLancamento.Pendencia.Titulo.status:=dsInsert;
  edtcodigohistoricosimples.setfocus;
  CheckParcelasIguais.Checked:=True;
end;

procedure TFtitulo_novo.BtCancelarClick(Sender: TObject);
begin
  FrLancamento1.ObjLancamento.Pendencia.Titulo.cancelar;
  limpaedit(Self);
  Self.limpalabels;
  desabilita_campos(PanelCabecalho);
  mostra_botoes(panelbotoes);
  Guia.PageIndex:=0;
  Self.LimpaGridParcelas;
end;

procedure TFtitulo_novo.BtgravarClick(Sender: TObject);
var
  codigotitulo:string;
begin

  If FrLancamento1.ObjLancamento.Pendencia.Titulo.Status=dsInactive
  Then exit;

  If ControlesParaObjeto=False then
  begin
    Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
    exit;
  End;

  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.salvar(true)=False)
  Then exit;

  mostra_botoes(panelbotoes);
  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(lbcodigo.caption)=False) then
  begin
    Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
    exit;
  End;

  limpaedit(Self);
  Self.limpalabels;

  desabilita_campos(PanelCabecalho);
  TabelaParaControles;

end;

procedure TFtitulo_novo.PreparaAlteracao;
begin
  if (ObjPermissoesUsoGlobal.ValidaPermissao('ALTERAR T�TULO')=False)
  then exit;

  edthistorico.enabled:=true;
  memoobservacao.enabled:=true;
  edtportador.enabled:=true;
  edtcontagerencial.Enabled:=true;
  edtsubcontagerencial.Enabled:=true;
  edtnumdcto.Enabled:=true;
  edtnotafiscal.Enabled:=true;

  esconde_botoes(panelbotoes);
  Btgravar.Visible:=True;
  BtCancelar.Visible:=True;
  btpesquisar.Visible:=True;

  FrLancamento1.ObjLancamento.Pendencia.Titulo.Status:=dsEdit;
  edtportador.setfocus;
end;

procedure TFtitulo_novo.btpesquisarClick(Sender: TObject);
var
  FpesquisaLocal:TFpesquisa;
  Pparametro:TStringList;
begin

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    Pparametro:=TStringList.create;
    FpesquisaLocal.NomeCadastroPersonalizacao:='FTITULO.PESQUISACS';
    pparametro.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_pesquisa(true).text;

    if (Pparametro.Text='')
    Then exit;


    If (FpesquisaLocal.PreparaPesquisa(pparametro,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisa,Nil)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok) then
        begin
          If FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive
          then exit;

          If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False) then
          begin
            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
            exit;
          End;

          TabelaParaControles;

        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    FreeandNil(Pparametro);
  End;
end;

procedure TFtitulo_novo.btalterarClick(Sender: TObject);
begin
  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status=dsinactive) and (lbcodigo.caption<>'')
  Then Self.PreparaAlteracao;
end;

procedure TFtitulo_novo.btexcluirClick(Sender: TObject);
begin
  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive) or (lbcodigo.caption='')
  Then exit;

  if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR TITULO')=False)
  then Exit;

  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(lbcodigo.caption)=False) then
  begin
    Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
    exit;
  End;

  If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
  Then exit;

  If (FrLancamento1.ObjLancamento.ExcluiTitulo(lbcodigo.caption,False)=False) then
  begin
    Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
    FrLancamento1.ObjLancamento.Pendencia.Titulo.rollback;
    exit;
  End
  Else FrLancamento1.ObjLancamento.Pendencia.Titulo.Commit;

  limpaedit(Self);
  Self.limpalabels;
  Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFtitulo_novo.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
  Fportador:TFportador;
begin

  If (key <>vk_f9)
  Then exit;

  Try
    Fportador:=TFportador.create(nil);
    Fpesquisalocal:=Tfpesquisa.create(Self);
    If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaPortador,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisaPortador,Fportador)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok)
        Then self.edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(Fportador);
  End;
end;

procedure TFtitulo_novo.edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
  Fcontager:TFcontager;
begin

  If (key <>vk_f9)
  Then exit;

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);

    Fcontager:=TFcontager.create(nil);
    if (SistemaMdiGlobal=False) then
    begin
      Fcontager.FormStyle:=fsNormal;
      Fcontager.Visible:=False;
    End;

    If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaContaGerencial,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisaContaGerencial,Fcontager)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok) then
        begin
          edtcontagerencial.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(Fcontager);
  End;
end;

procedure TFtitulo_novo.combogeradorChange(Sender: TObject);
begin
  combogeradorcodigo.itemindex:=combogerador.itemindex;
end;

procedure TFtitulo_novo.combocredordevedorChange(Sender: TObject);
begin
  combocredordevedorcodigo.itemindex:=combocredordevedor.itemindex;
end;

procedure TFtitulo_novo.combocredordevedorcodigoChange(Sender: TObject);
begin
  combocredordevedor.itemindex:=Combocredordevedorcodigo.itemindex;
end;

procedure TFtitulo_novo.combogeradorcodigoChange(Sender: TObject);
begin
  ComboGerador.itemindex:=ComboGeradorCodigo.itemindex;
end;

procedure TFtitulo_novo.edtcodigocredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin

  If (key <>vk_f9)
  Then exit;


  If (combocredordevedor.itemindex=-1)
  Then exit;


  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    If (FpesquisaLocal.PreparaPesquisaN(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigo.text),'Pesquisa de '+Combocredordevedor.text,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigo.text))=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok) then
        begin
          edtcodigocredordevedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
          LbNomeCredorDevedor.caption:=FpesquisaLocal.QueryPesq.fieldbyname(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_CampoNomeCredorDevedor(combocredordevedorcodigo.text)).asstring;
        End;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
  End;
end;

procedure TFtitulo_novo.edtcodigogeradorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
begin

  If (key <>vk_f9)
  Then exit;

  If (combogerador.itemindex=-1)
  Then exit;

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaGeradorinstrucaoSql(combogeradorcodigo.text),'Pesquisa de '+Combogerador.text,nil)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok)
        Then edtcodigogerador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
  End;
end;

procedure TFtitulo_novo.comboprazonomeChange(Sender: TObject);
begin
  Comboprazocodigo.itemindex:=ComboPrazonome.itemindex;
end;

procedure TFtitulo_novo.comboprazocodigoChange(Sender: TObject);
begin
  ComboPrazonome.itemindex:=Comboprazocodigo.itemindex;
end;

procedure TFtitulo_novo.GuiaTitulosChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  try
    screen.Cursor:=crHourGlass;
    if NewTab=0 then//contas a receber
    begin

      if(rbContasComSaldo.Checked)
      then Self.ListaTitulos2('C','C')
      else Self.ListaTitulos2('C','S')

    end
    else
      if NewTab=1 then
      begin

        if(rbContasComSaldo.Checked)
        then Self.ListaTitulos2('D','C')
        else Self.ListaTitulos2('D','S')
      end;
  finally
    screen.Cursor:=crDefault;
  end;

end;

procedure TFtitulo_novo.StrGridPendenciasDblClick(Sender: TObject);
var
Ptemp:string;
begin
  Ptemp:='';

  if (lbcodigo.caption='') or
    (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status<>dsInactive)
  Then exit;

  If (StrGridPendencias.Cells[0,StrGridPendencias.row]<>'') then
  begin
    If StrGridPendencias.cells[0,0]<>'C�DIGO'
    Then Messagedlg('A Configura��o do Grid com C�digo na Coluna 0 Foi Alterada, verifique o C�digo!',mterror,[mbok],0)
    Else FrLancamento1.ObjLancamento.NovoLancamento(StrGridPendencias.cells[0,StrGridPendencias.row],'','','',edthistorico.text,True,True,False);
    BtPesqMensaClick(sender);
  End;
end;

procedure TFtitulo_novo.btrelatoriosClick(Sender: TObject);
begin
  FrLancamento1.ObjLancamento.Imprime(lbcodigo.caption);
end;

procedure TFtitulo_novo.StrGridPendenciasKeyPress(Sender: TObject;
  var Key: Char);
begin
  If key=#13
  Then StrGridPendencias.OnDblClick(sender);
end;

procedure TFtitulo_novo.comboprazonomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  FpesquisaLocal:Tfpesquisa;
  FprazoPagamento:TFprazoPagamento;
begin

  If (key <>vk_f9)
  Then exit;

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FprazoPagamento:=TFprazoPagamento.create(nil);
    If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_PesquisaPrazo,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisaPrazo,FprazoPagamento)=True) then
    begin
      Try
        FpesquisaLocal.showmodal;
        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoCodigo(comboprazocodigo.items);
        FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoNome(comboprazonome.items);
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
   FreeandNil(FPesquisaLocal);
   Freeandnil(FprazoPagamento);
  End;

end;


procedure TFtitulo_novo.BtPesqMensaClick(Sender: TObject);
begin
  btopcoesboleto.Visible:=False;
  lbboletos.Visible:=False;
  StrGridPendencias.Colcount:=2;
  StrGridPendencias.cols[0].clear;
  StrGridPendencias.cols[1].clear;

  StrGridPendencias.RowCount:=FrLancamento1.ObjLancamento.Pendencia.Get_QuantRegs(lbcodigo.caption)+1;
  If (StrGridPendencias.RowCount>1) then
  begin
    StrGridPendencias.FixedRows:=1;
    //*****Localizando o Titulo***************
    if (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCODIGO(lbcodigo.caption)=False) then
    begin
      Messagedlg('T�tulo n�o localizado',mterror,[mbok],0);
      exit;
    End;
    FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;

    if (FrLancamento1.ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C') then
    begin
      //titulo a receber
      btopcoesboleto.Visible:=True;
      lbboletos.Visible:=True;
      Frlancamento1.Objlancamento.Pendencia.RetornaGrid_Receber(StrGridPendencias,lbcodigo.caption);
    End
    Else Begin
      StrGridPendencias.ColCount:=7;
      FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListaPendenciaCampos(StrGridPendencias.Cols[0],StrGridPendencias.Cols[1],StrGridPendencias.Cols[2],StrGridPendencias.Cols[3],StrGridPendencias.Cols[4],nil,nil,nil,StrGridPendencias.Cols[5],strgridpendencias.cols[6],lbcodigo.caption);
    End;

    BthistoricoPendencias.Enabled:=true;
  End
  Else StrGridPendencias.RowCount:=0;

  AjustaLArguraColunaGrid(StrGridPendencias);
end;

procedure TFtitulo_novo.edtportadorExit(Sender: TObject);
begin
  LbPortador.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomePortador(edtportador.text);
end;

procedure TFtitulo_novo.edtcontagerencialExit(Sender: TObject);
begin
  FrLancamento1.ObjLancamento.Pendencia.Titulo.EdtcontagerencialExit(sender,LbContaGerencial);
End;

procedure TFtitulo_novo.edtvalorExit(Sender: TObject);
begin
  edtvalor.text:=formata_valor(edtvalor.text);
end;

procedure TFtitulo_novo.btpesquisasemsaldoClick(Sender: TObject);
var
  FpesquisaLocal:TFpesquisa;
  Pparametro:TStringList;
begin

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    Pparametro:=TStringList.create;

    Pparametro.Text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_pesquisa(false).Text;

    if (Pparametro.text='')
    Then exit;

    If (FpesquisaLocal.PreparaPesquisa(pparametro,FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_TituloPesquisa,Nil)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok) then
        begin
          If FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive
          then exit;

          If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False) then
          begin
            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
            exit;
          End;

          TabelaParaControles;

        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(Pparametro);
  End;
end;

procedure TFtitulo_novo.edtcodigocredordevedorExit(Sender: TObject);
begin
  //preciso sair com o nome do credordevedor escolhido
  //atraves do FrLancamento1.ObjLancamento.Pendencia.Titulo
  LbNomeCredorDevedor.caption:='';

  If (edtcodigocredordevedor.text='') or (combocredordevedor.ItemIndex=-1)
  Then exit;

  LbNomeCredorDevedor.caption:=FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_NomeCredorDevedor(combocredordevedorcodigo.text,edtcodigocredordevedor.text);
  //pegando a contagerencial deste cadastro escolhido
  edtcontagerencial.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_ContaGerencial(combocredordevedorcodigo.text,edtcodigocredordevedor.text);
  edtcontagerencialExit(edtcontagerencial);//colocando o nome na label
end;

procedure TFtitulo_novo.edtvalorKeyPress(Sender: TObject; var Key: Char);
begin
  If not(key in ['0'..'9',#8,',']) then
  begin
    If (key='.')
    Then Key:=','
    Else Key:=#0;
  End;
end;

procedure TFtitulo_novo.FrLancamento1BtpesquisaLoteClick(Sender: TObject);
begin
  FrLancamento1.BtpesquisacpClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1BtexecutaLoteClick(Sender: TObject);
begin
  FrLancamento1.BtexecutacpClick(Sender);
end;

procedure TFtitulo_novo.edtcodigohistoricosimplesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  FpesquisaLocal:TFpesquisa;
  FHistoricoSimples:TFHistoricoSimples;
begin
  If (key<>vk_f9)
  Then exit;

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    FHistoricoSimples:=TFHistoricoSimples.create(nil);
    FpesquisaLocal.NomeCadastroPersonalizacao:='FTITULO_NOVO.EDTCODIGOHISTORICOSIMPLES';  {Rodolfo}

    If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_pesquisa,FrLancamento1.ObjLancamento.Pendencia.Titulo.PORTADOR.historicocontabil.Get_TituloPesquisa,FHistoricoSimples)=True) then
    begin
      Try
        If (FpesquisaLocal.showmodal=mrok) then
        begin
          TEdit(Sender).text:=FpesquisaLocal.querypesq.fieldbyname('codigo').asstring;
          If (FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False) then
          begin
            TEdit(Sender).text:='';
            exit;
          End;
          FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
          edthistorico.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    Freeandnil(FHistoricoSimples);
  End;
end;

procedure TFtitulo_novo.edtcodigohistoricosimplesExit(Sender: TObject);
begin
  IF (TEdit(Sender).text='')
  Then exit;

  If (FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.LocalizaCodigo(TEdit(Sender).text)=False) then
  begin
    TEdit(Sender).text:='';
    exit;
  End;
  FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.TabelaparaObjeto;
  edthistorico.text:=FrLancamento1.ObjLancamento.Pendencia.Titulo.portador.HIstoricoContabil.Get_Nome;
end;

procedure TFtitulo_novo.edtcodigohistoricosimplesKeyPress(Sender: TObject;
  var Key: Char);
begin
  If (not (key in ['0'..'9',#8]))
  Then key:=#0;
end;


procedure TFtitulo_novo.btopcoesClick(Sender: TObject);
begin
  FrLancamento1.ObjLancamento.Pendencia.Opcoes(lbcodigo.caption);

  if (lbcodigo.caption<>'') then
  begin
    if (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(lbcodigo.caption)=False) then
    begin
      limpaedit(Self);
      exit;
    End;
    FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
    Self.TabelaParaControles;
  End;
end;

procedure TFtitulo_novo.FrLancamento1btexecutacrClick(Sender: TObject);
begin
  try
    screen.Cursor:=crHourGlass;
    FrLancamento1.btexecutacrClick(Sender);
  finally
    screen.cursor:=crDefault;
  end;
end;

procedure TFtitulo_novo.btopcoesboletoClick(Sender: TObject);
begin
  //Numero da pendencia
  If StrGridPendencias.cells[0,0]<>'C�DIGO'
  Then Messagedlg('A Configura��o do Grid com C�digo na Coluna 0 Foi Alterada, verifique o C�digo!',mterror,[mbok],0);

  With FOpcaorel do
  begin
    RgOpcoes.Items.clear;
    RgOpcoes.Items.add('Cancelar Boleto');
    RgOpcoes.Items.add('Reimpress�o de Boleto');
    RgOpcoes.Items.add('Excluir Duplicata');
    RgOpcoes.Items.Add('Reimpress�o de Duplicata');
    showmodal;
    if (tag=0)
    then exit;

    case RgOpcoes.ItemIndex of

      0: Begin
        FrLancamento1.ObjLancamento.Pendencia.CancelaBoleto(StrGridPendencias.cells[0,StrGridPendencias.row]);
        BtPesqMensaClick(sender);
      End;
      1: Begin
        FrLancamento1.ObjLancamento.Pendencia.Re_imprimeboleto(StrGridPendencias.cells[0,StrGridPendencias.row],true);
        BtPesqMensaClick(sender);
      End;
      2: Begin
        FrLancamento1.ObjLancamento.Pendencia.ExcluiDuplicata(StrGridPendencias.cells[0,StrGridPendencias.row]);
        BtPesqMensaClick(sender);
      End;
      3: Begin
        if (StrGridPendencias.cells[7,StrGridPendencias.row]='')
        then exit;
        FrLancamento1.ObjLancamento.Pendencia.Re_imprimeduplicata(StrGridPendencias.cells[7,StrGridPendencias.row]);
        BtPesqMensaClick(sender);
      End;
    End;
  End;
end;

procedure TFtitulo_novo.FormShow(Sender: TObject);
var
Pallow:boolean;
begin
  try
    screen.cursor:=crHourGlass;
    Uessencialglobal.PegaCorForm(Self);

    if (Tag = 1) then
      Exit;

    limpaedit(Self);
    desabilita_campos(PanelCabecalho);
    Self.limpalabels;

    Try

      If FrLancamento1.Criar=False then
      begin
        desabilita_campos(self);
      End;

      FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedor.items);
      FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigo.items);

      FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListaGerador(combogerador.items);
      FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_ListaGeradorCodigo(combogeradorcodigo.items);

      FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoCodigo(comboprazocodigo.items);
      FrLancamento1.ObjLancamento.Pendencia.Titulo.Get_listaPrazoPagamentoNome(comboprazonome.items);

    Except
      Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
      self.close;
    End;

    FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
    FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
    //FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
    FescolheImagemBotao.PegaFiguraBotaopequeno(btPesquisaDuplicata,'BOTAODUPLICATAS.BMP');
    FescolheImagemBotao.PegaFiguraBotaopequeno(btAtualizarVencimentos,'BOTAOATUALIZARFUNDO.BMP');
    retira_fundo_labels(self);

    Self.Color:=clwhite;

    Self.DBGridReceber.DataSource:=Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjDatasourceMOstraTitulo;
    Self.DBGridPagar.DataSource:=Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjDatasourceMOstraTitulo;

    tag:=1;

    If (FrLancamento1.ObjLancamento.Pendencia.Titulo.VerificaPermissao=False)
    Then esconde_botoes(panelbotoes)
    Else mostra_botoes(panelbotoes);

    //esta variavel � usada em casos que eu quero chamr o form e ele ja
    //procurar um codigo e trazer os dados pra tela
    if(CodigoaserLocalizado<>'') then
    begin
      if (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(CodigoaserLocalizado)=true) then
      begin
        FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
        Self.ObjetoParaControles;

        if (self.QuitaPrimeiraParcela) then
        begin
          Self.GuiaChange(sender,1,pallow);
          StrGridPendencias.Row:=1;
          StrGridPendenciasDblClick(sender);
        End;
      End;
    end;

    //esconde todos os botoes
    //**************************
    btpesquisatitulo.Width:=0;
    btpesquisatitulo.Height:=0;
    btpesquisapendencia.Width:=0;
    btpesquisapendencia.Height:=0;
    btpesquisaboleto.Width:=0;
    btpesquisaboleto.Height:=0;
    btPesquisaDuplicata.Width:=0;
    btPesquisaDuplicata.Height:=0;
    //***************************

    chkAtualizaraoAbrir.Checked := False;
    if(ObjParametroGlobal.ValidaParametro('ATUALIZA VALORES NA TELA DE CONTAS AO CARREGAR',true)) then
    begin
      if(ObjParametroGlobal.Get_Valor = 'SIM') then
      begin
        chkAtualizaraoAbrir.Checked := True;
        btAtualizarVencimentosClick(nil);
      end
      else
        LimpaLabelsPanelRodape;
    end
    else
      LimpaLabelsPanelRodape;

    PanelParcelas.Align:=alClient;
    PanelLancamentoLote.Align:=alClient;

    if (CodigoaserLocalizado = '')
    then Guia.PageIndex:=1;

    rbHoje.Caption := FormatDateTime('dd/mm/yyyy',Now);
    rbHoje.Checked := True; //Abre os 100 primeiros registros    -     Rodolfo

  finally
    Screen.Cursor:=crDefault;
  end;
  combocredordevedor.Text := '';

  GuiaTitulos.PageIndex := 0;
  
end;

procedure TFtitulo_novo.PesquisaParcela(Sender: TObject);
VAR
  NumPend:string;
begin
  if (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
  Then exit;
  numpend:='';
  Numpend:=FrLancamento1.ObjLancamento.Pendencia.PegaPendencia;
  try
    strtoint(numpend);
    If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaPendencia(NUmpend)=False)
    Then exit;
    FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
    self.ObjetoParaControles;
  except
    exit;
  end;
end;

procedure TFtitulo_novo.BthistoricoPendenciasClick(Sender: TObject);
var
  FhistoricoLancamento:TFhistoricoLancamento;
begin
  Try
    FhistoricoLancamento:=TFhistoricoLancamento.create(nil);

    If StrGridPendencias.cells[0,StrGridPendencias.row]=''
    Then exit;

    If StrGridPendencias.cells[0,0]<>'C�DIGO'
    Then Messagedlg('A Configura��o do Grid com C�digo na Coluna 0 Foi Alterada, verifique o C�digo!',mterror,[mbok],0)
    Else Begin
      FhistoricoLancamento.LbPendencia.caption:=StrGridPendencias.cells[0,StrGridPendencias.row];
      FhistoricoLancamento.Objlancamento:=FrLancamento1.ObjLancamento;
      FhistoricoLancamento.btRecibo.Visible:=FhistoricoLancamento.ObjLancamento.LancamentoPagamento(StrGridPendencias.cells[0,StrGridPendencias.row]);

      //CHAMA FORMUL�RIO - ALEX
      //if   (FhistoricoLancamento=nil)
      //then Application.CreateForm(TFhistoricoLancamento, FhistoricoLancamento);
      FhistoricoLancamento.ShowModal;
      //FreeAndNil(FhistoricoLancamento);
      if lbcodigo.Caption<>''
      then btpesqmensa.onclick(sender);
      
    End;
  Finally
    Freeandnil(FhistoricoLancamento);
  End;
end;

procedure TFtitulo_novo.PesquisaBoleto(Sender: TObject);
VAR
  Numboleto:string;
begin
  if (FrLancamento1.ObjLancamento.Pendencia.LocalizaBoleto_Convenio=true)
  Then self.ObjetoParaControles
  Else limpaedit(self);
end;

procedure TFtitulo_novo.PesquisaTitulo(Sender: TObject);
VAR
  NumPend:string;
begin
  if (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
  Then exit;

  numpend:='';
  NUmPend:=FrLancamento1.ObjLancamento.Pendencia.pegatitulo;
  if (numpend = '')
  then exit;
  try
    strtoint(numpend);
    If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(NUmpend)=False) then
    begin
      Messagedlg('T�tulo n�o Localizado!',mtinformation,[mbok],0);
      exit;
    End;
    FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
    self.ObjetoParaControles;
  except
    exit;
  end;
end;

procedure TFtitulo_novo.LocalizaCodigoInicial(Pcodigo: string);
begin
     CodigoaserLocalizado:=pcodigo;
end;

procedure TFtitulo_novo.FrLancamento1btpesquisaClick(Sender: TObject);
var
  FpesquisaLocal:TFpesquisa;
  numpend:String;
begin

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);
    If (FpesquisaLocal.PreparaPesquisa(FrLancamento1.ObjLancamento.Get_Pesquisa,FrLancamento1.ObjLancamento.Get_TituloPesquisa,Nil)=True) then
    begin
      Try
        if (FpesquisaLocal.showmodal=mrOk) then
        begin
          //escolheu um Lancamento
          if (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status<>dsinactive)
            or (FpesquisaLocal.querypesq.fieldbyname('pendencia').asstring='')
          then exit;

          try
            numpend:=FpesquisaLocal.querypesq.fieldbyname('pendencia').asstring;
            If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaPendencia(NUmpend)=False)
            Then exit;
            FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
            self.ObjetoParaControles;
            Guia.PageIndex:=0;
          except
            exit;
          end;
        End;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
  End;
end;

procedure TFtitulo_novo.FrLancamento1BtApagaLoteClick(Sender: TObject);
begin
  FrLancamento1.BtApagaLoteClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1btchequecaixaClick(Sender: TObject);
begin
  FrLancamento1.btchequecaixaClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1btlancaboletoClick(Sender: TObject);
begin
  FrLancamento1.btlancaboletoClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1btimprimecomprovantepagamentoClick(
  Sender: TObject);
begin
  FrLancamento1.btimprimecomprovantepagamentoClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1btpesquisacrClick(Sender: TObject);
begin
  try
    screen.Cursor:=crHourGlass;
    FrLancamento1.btpesquisacrClick(Sender);
  finally
    screen.cursor:=crDefault;
  end;
end;

procedure TFtitulo_novo.edtcodigocredordevedorKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in['0'..'9',#8])
  Then Key:=#0;
end;

procedure TFtitulo_novo.edtsubcontagerencialExit(Sender: TObject);
begin
  FrLancamento1.ObjLancamento.Pendencia.Titulo.EdtSubcontagerencialExit(sender,lbnomesubcontagerencial);
end;

procedure TFtitulo_novo.edtsubcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if edtcontagerencial.text='' then
  Begin
    MensagemAviso('Escolha antes a Conta Gerencial!');
    edtcontagerencial.setfocus;
    exit
  end;
  FrLancamento1.ObjLancamento.Pendencia.Titulo.edtsubcontagerencialKeyDown(sender,key,shift,lbnomesubcontagerencial,edtcontagerencial.Text);
end;

procedure TFtitulo_novo.limpalabels;
begin
  LbNomeCredorDevedor.caption:='';
  lbnomesubcontagerencial.caption:='';
  LbContaGerencial.caption:='';
  LbPortador.caption:='';
  LbCRCP.caption:='';
  lbTotalConta.Caption:='0,00';
  lbSaldo.Caption:='0,00';
  lbRecebido.Caption:='0,00';
  lbcodigo.Caption:='';
end;

procedure TFtitulo_novo.PesquisaDuplicata(Sender: TObject);
VAR
  NumDupl:string;
begin
  if (FrLancamento1.ObjLancamento.Pendencia.Titulo.status<>dsinactive)
  Then exit;
  NumDupl:='';
  NumDupl:=FrLancamento1.ObjLancamento.Pendencia.PegaDuplicata;
  try
    strtoint(NumDupl);
    If (FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaDuplicata(NumDupl)=False)
    Then exit;
    FrLancamento1.ObjLancamento.Pendencia.Titulo.TabelaparaObjeto;
    self.ObjetoParaControles;
  except
    exit;
  end;
end;

procedure TFtitulo_novo.FormCreate(Sender: TObject);
var
  addDuploClique : TDuploClique; //Rodolfo
begin
  Self.QuitaPrimeiraParcela := false;
  self.QueryPesq.Database := FDataModulo.IBDatabase;
  Receber := false;
  Pagar := false;
  Self.PesquisaEmEstoque := false;

  addDuploClique.AdicionaDuploClique(sender); //Chamada da adi�ao do evento duplo clique nos edits - Rodolfo
end;

procedure TFtitulo_novo.FrLancamento1btAlterarHistoricoLancamentoClick(
  Sender: TObject);
begin
  FrLancamento1.btAlterarHistoricoLancamentoClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1strgcrKeyPress(Sender: TObject;
  var Key: Char);
begin
  FrLancamento1.strgcrKeyPress(Sender, Key);
end;

procedure TFtitulo_novo.FrLancamento1strgcrKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FrLancamento1.strgcrKeyDown(Sender, Key, Shift);
end;


procedure TFtitulo_novo.FrLancamento1STRGCPKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  FrLancamento1.STRGCPKeyDown(Sender, Key, Shift);
end;

procedure TFtitulo_novo.FrLancamento1STRGCPKeyPress(Sender: TObject;
  var Key: Char);
begin
  FrLancamento1.STRGCPKeyPress(Sender, Key);
end;

//Rodolfo
function TFtitulo_novo.VerificaOpcao: String;
begin
  result:= '10000';
  case opPesquisa of
    0: Result := FormatDateTime('dd/mm/yyyy',Now);

    1: result := '10000';

    2: Result := '100';

    3: Result := '1000';
  end;
end;


procedure TFtitulo_novo.ListaTitulos(Ptipo,PComSemSaldo:string);//Ptipo: C-Credito, D-Debito, PComSemSaldo, C ou S
begin
  Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ListaTitulos(Ptipo,PComSemSaldo, '','');
  AtualizaLabelsValores(Ptipo);
end;

//Rodolfo
procedure TFtitulo_novo.ListaTitulos2(Ptipo,PComSemSaldo:string);//Ptipo: C-Credito, D-Debito, PComSemSaldo, C ou S
begin
  Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ListaTitulos2(Ptipo,PComSemSaldo, VerificaOpcao);
  AtualizaLabelsValores(Ptipo);
end;

procedure TFtitulo_novo.rbContasComSaldoClick(Sender: TObject);
begin
  if GuiaTitulos.PageIndex=0 then//Receber, Tipo C-Credito
  begin
    Self.ListaTitulos2('C','C');  //Rodolfo
    formatadbgrid(DBGridReceber);
    exit;
  end
  else Begin
    if GuiaTitulos.PageIndex=1 then//Pagar, Tipo D-Debito
    begin
      Self.ListaTitulos2('D','C'); //Rodolfo
      formatadbgrid(DBGridPagar);
      Exit;
    end;
  end;
end;

procedure TFtitulo_novo.rbContasSemSaldoClick(Sender: TObject);
begin
  if GuiaTitulos.PageIndex=0 then //Receber, Tipo C-Credito
  begin
    Self.ListaTitulos2('C','S'); //Rodolfo
    formatadbgrid(DBGridReceber);
    exit;
  end
  else Begin
    if GuiaTitulos.PageIndex=1 then//Pagar, Tipo D-Debito
    begin
      Self.ListaTitulos2('D','S');  //Rodolfo
      formatadbgrid(DBGridPagar);
      Exit;
    end;
  end;
end;

procedure TFtitulo_novo.DBGridReceberKeyPress(Sender: TObject; var Key: Char);
begin
  rbInciciaCom.Visible:=false;
  rbExata.Visible:=false;
  rbTenhaemQualquerParte.Visible:=false;

  Case Key of
    #32:
    Begin
      EdtBusca.text:='';
      str_pegacampo:=DBGridReceber.SelectedField.FieldName;
      EdtBusca.Visible:=true;
      EdtBusca.SetFocus;
      rbInciciaCom.Visible:=true;
      rbExata.Visible:=true;
      rbTenhaemQualquerParte.Visible:=true;

      Case DBGridReceber.SelectedField.datatype of

        ftdatetime :  Begin
               edtBusca.EditMask:='00/00/0000 00:00:00';
               EdtBusca.width:=300;
            End;
        ftdate    :  Begin
               edtBusca.EditMask:='00/00/0000;1;_';
               EdtBusca.width:=130;
            End;
        ftTime    :  Begin
               edtBusca.EditMask:='00:00';
               EdtBusca.width:=130;
            End;
        ftInteger  :Begin
              edtBusca.EditMask:='';
              EdtBusca.width:=300;
              EdtBusca.maxlength:=15
           End;

        ftLargeint  :Begin
              edtBusca.EditMask:='';
              EdtBusca.width:=300;
              EdtBusca.maxlength:=15
           End;

        ftbcd      :Begin
              edtBusca.EditMask:='';
              EdtBusca.width:=300;
              EdtBusca.maxlength:=15
           End;
        ftFMTBcd   :begin
              edtBusca.EditMask:='';
              EdtBusca.width:=300;
              EdtBusca.maxlength:=15
           end;

        ftfloat    :Begin
              edtBusca.EditMask:='';
              EdtBusca.width:=300;
              EdtBusca.maxlength:=15
           End;
        ftString   :Begin
              edtBusca.EditMask:='';
              EdtBusca.width:=700;
              EdtBusca.maxlength:=255;
              rbTenhaemQualquerParte.Visible:=true;
              rbInciciaCom.Visible:=true;
              rbExata.Visible:=true;
              //rbTenhaemQualquerParte.Checked:=true;
           End;

      Else
        Begin
          Messagedlg('Este tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
          DBGridReceber.setfocus;
          exit;
        End;

      End;
      //EdtBusca.Text :='';

      //EdtBusca.Text:=Key;
      //EdtBusca.SelStart := 1; // Deixa o cursor apos o 1� digito

    End;

    #13:
    Begin
      if DBGridReceber.DataSource.DataSet.fieldbyname('CODIGO').asstring=''
      then exit;

      if FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(DBGridReceber.DataSource.DataSet.fieldbyname('CODIGO').AsString)=False
      then Exit;
      Self.TabelaParaControles;
    End;

    #27:
    Begin
      Self.modalresult:=mrcancel;
    End;
  end;

end;

procedure TFtitulo_novo.DBGridPagarKeyPress(Sender: TObject; var Key: Char);
begin
  rbInciciaCom.Visible:=false;
  rbExata.Visible:=false;
  rbTenhaemQualquerParte.Visible:=false;

  Case Key of
    #32:Begin
      EdtBusca.text:='';
      str_pegacampo:=DBGridPagar.SelectedField.FieldName;
      EdtBusca.Visible:=true;
      EdtBusca.SetFocus;
      rbInciciaCom.Visible:=TRUE;
      rbExata.Visible:=true;
      rbTenhaemQualquerParte.Visible:=true;

      Case DBGridPagar.SelectedField.datatype of

        ftdatetime :  Begin
           edtBusca.EditMask:='00/00/0000 00:00:00';
           EdtBusca.width:=300;
        End;

        ftdate    :  Begin
           edtBusca.EditMask:='00/00/0000;1;_';
           EdtBusca.width:=130;
        End;

        ftTime    :  Begin
           edtBusca.EditMask:='00:00';
           EdtBusca.width:=130;
        End;

        ftInteger  :Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;

        ftLargeint  :Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;

        ftbcd      :Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;
        ftFMTBcd   :begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        end;

        ftfloat    :Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;
        
        ftString   :Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=700;
          EdtBusca.maxlength:=255;
          rbTenhaemQualquerParte.Visible:=true;
          rbInciciaCom.Visible:=true;
          rbExata.Visible:=true;
        End;
      Else
        Begin
          Messagedlg('Este tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
          DBGridPagar.setfocus;
          exit;
        End;
      End;
    End;

    #13:
    Begin
      if DBGridReceber.DataSource.DataSet.fieldbyname('CODIGO').asstring=''
      then exit;

      if FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(DBGridPagar.DataSource.DataSet.fieldbyname('CODIGO').AsString)=False
      then Exit;
      Self.TabelaParaControles;
    End;
    
    #27:
    Begin
      Self.modalresult:=mrcancel;
    End;
  end;
end;

procedure TFtitulo_novo.DBGridReceberDblClick(Sender: TObject);
begin
  if DBGridReceber.DataSource.DataSet.fieldbyname('CODIGO').asstring=''
  then exit;

  if FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(DBGridReceber.DataSource.DataSet.fieldbyname('CODIGO').AsString)=False
  then Exit;
  Self.TabelaParaControles;
end;

procedure TFtitulo_novo.DBGridPagarDblClick(Sender: TObject);
begin
  if DBGridPagar.DataSource.DataSet.fieldbyname('CODIGO').asstring=''
  then exit;

  if FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(DBGridPagar.DataSource.DataSet.fieldbyname('CODIGO').AsString)=False
  then Exit;
  Self.TabelaParaControles;
end;

procedure TFtitulo_novo.DBGridReceberKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Self.AtualizaLabelsValores('C');
end;

procedure TFtitulo_novo.DBGridPagarKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  Self.AtualizaLabelsValores('D');
end;

procedure TFtitulo_novo.AtualizaLabelsValores(Ptipo: string);
begin
  { Cursor:=crHourGlass;
  if Ptipo='D'
  then begin
    lbTotalConta.Caption:=formata_valor(DBGridPagar.DataSource.DataSet.fieldbyname('valor').AsString);
    lbRecebido.Caption:=formata_valor(DBGridPagar.DataSource.DataSet.fieldbyname('Total_Quitado').AsString);
    lbSaldo.caption:=formata_valor(DBGridPagar.DataSource.DataSet.fieldbyname('SALDOPENDENCIA').AsString);
  end
  else begin
    lbTotalConta.Caption:=formata_valor(DBGridReceber.DataSource.DataSet.fieldbyname('valor').AsString);
    lbRecebido.Caption:=formata_valor(DBGridReceber.DataSource.DataSet.fieldbyname('Total_Quitado').AsString);
    lbSaldo.caption:=formata_valor(DBGridReceber.DataSource.DataSet.fieldbyname('SALDOPENDENCIA').AsString);
  end;
  Cursor:=crDefault;   }
end;

procedure TFtitulo_novo.DBGridReceberCellClick(Column: TColumn);
begin
  Self.AtualizaLabelsValores('C');
end;

procedure TFtitulo_novo.DBGridPagarCellClick(Column: TColumn);
begin
  Self.AtualizaLabelsValores('D');
end;

procedure TFtitulo_novo.btAtualizarVencimentosClick(Sender: TObject);
var
  dia, mes, ano:string;
begin
  ano:=FormatDateTime('yyyy',Now);
  mes:=FormatDateTime('mm',Now);
  dia:=IntToStr(DiasdoMes(ano, mes));

  lbReceberHoje.caption:=formata_valor(FrLancamento1.ObjLancamento.Pendencia.Titulo.RetornaValoresFinanceiros('C',DateToStr(Now)));
  lbPagarHoje.caption:=formata_valor(FrLancamento1.ObjLancamento.Pendencia.Titulo.RetornaValoresFinanceiros('D',DateToStr(Now)));
  lbReceberMes.caption:=formata_valor(FrLancamento1.ObjLancamento.Pendencia.Titulo.RetornaValoresFinanceiros('C',dia+'/'+mes+'/'+ano));
  lbPagarMes.caption:=formata_valor(FrLancamento1.ObjLancamento.Pendencia.Titulo.RetornaValoresFinanceiros('D',dia+'/'+mes+'/'+ano));
  lbReceberGeral.caption:=formata_valor(FrLancamento1.ObjLancamento.Pendencia.Titulo.RetornaValoresFinanceiros('C',''));
  LbPagarGeral.caption:=formata_valor(FrLancamento1.ObjLancamento.Pendencia.Titulo.RetornaValoresFinanceiros('D',''));
  LbMesCorrente.Caption:=MesExtenso(StrToInt(mes));
end;

procedure TFtitulo_novo.Sublinhado(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  TLabel(Sender).Font.Style:=[fsBold, fsUnderline];
end;

procedure TFtitulo_novo.SoNegrito(Sender: TObject);
begin
  TLabel(Sender).Font.Style:=[fsBold];
end;

procedure TFtitulo_novo.FrLancamento1BtexecutaCPClick(Sender: TObject);
begin
  try
    screen.Cursor:=crHourGlass;
    FrLancamento1.BtexecutaCPClick(Sender);
  finally
    screen.Cursor:=crDefault;
  end;
end;

procedure TFtitulo_novo.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  if NewTab=0 then
  begin
    StrGridPendencias.Cols[0].clear;
    StrGridPendencias.Cols[1].clear;
    StrGridPendencias.Cols[2].clear;
    StrGridPendencias.Cols[3].clear;
    StrGridPendencias.Cols[4].clear;
    StrGridPendencias.Cols[5].clear;
    StrGridPendencias.Cols[6].clear;
    StrGridPendencias.Cols[7].clear;
    StrGridPendencias.Cols[8].clear;

    if lbcodigo.Caption<>''
    then btpesqmensa.onclick(sender);
  end
  else
    if NewTab=1 then
    begin
      try
        Screen.Cursor:=crHourGlass;
        if GuiaTitulos.PageIndex=0
        then rbContasComSaldoClick(nil)
        else GuiaTitulos.PageIndex:=0;
      finally
        Screen.cursor:=crDefault;
      end;
    end
    else
      if NewTab=2 then
      begin
        If (FrLancamento1.ObjLancamento.Pendencia.Titulo.Status<>dsinactive)
        Then exit;

        if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO LAN�AMENTO EM LOTE')=False)
        then Exit;

        FrLancamento1.Guia.ActivePageIndex:=1;
      end;
end;

procedure TFtitulo_novo.FrLancamento1GuiaChange(Sender: TObject);
begin
  FrLancamento1.GuiaChange(Sender);
end;

procedure TFtitulo_novo.LimpaGridParcelas;
begin
  StrGridPendencias.Cols[0].clear;
  StrGridPendencias.Cols[1].clear;
  StrGridPendencias.Cols[2].clear;
  StrGridPendencias.Cols[3].clear;
  StrGridPendencias.Cols[4].clear;
  StrGridPendencias.Cols[5].clear;
  StrGridPendencias.Cols[6].clear;
  StrGridPendencias.Cols[7].clear;
  StrGridPendencias.Cols[8].clear;
end;

procedure TFtitulo_novo.FrLancamento1chkSelecionaTodasCPClick(
  Sender: TObject);
begin
  FrLancamento1.chkSelecionaTodasCPClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1chkSelecionaTodasCRClick(
  Sender: TObject);
begin
  FrLancamento1.chkSelecionaTodasCRClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1BtSelecionaTodas_PGClick(
  Sender: TObject);
begin
  FrLancamento1.BtSelecionaTodas_PGClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1Label24Click(Sender: TObject);
begin
  FrLancamento1.BtSelecionaTodas_PGClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1Label25Click(Sender: TObject);
begin
  FrLancamento1.BtSelecionaTodas_CRClick(Sender);
end;

procedure TFtitulo_novo.DBGridPagarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  indice_grid:integer;
  cont,marca_registro:integer ;
  comandoordena:String;
  ColunaAtual:integer;
begin
  if (key=VK_Delete) then
  begin
    DBGridPagar.Columns.Items[DBGridPagar.SelectedIndex].Visible :=false;
    DBGridPagar.SelectedIndex :=DBGridPagar.SelectedIndex +1;
  end;

  If (key=VK_F10) then
  begin
    Self.QueryPesq.close;
    Self.QueryPesq.sql.clear;
    Self.QueryPesq.sql.add(comandosql);
    Self.QueryPesq.open;
    DBGridPagar.SelectedIndex:=ColunaAtual;
    Formatadbgrid(DBGridPagar,querypesq);
    Self.Personalizagrid;
  end;

  if (key=VK_f12) then
  begin

    try
      ColunaAtual:=DBGridPagar.SelectedIndex;
      str_pegacampo:=DBGridPagar.SelectedField.FieldName;
      if (Length(str_pegacampo)>4) then
      begin
        if (uppercase(copy(str_pegacampo,1,2))='XX')
          and (uppercase(copy(str_pegacampo,length(str_pegacampo)-1,2))='XX')
        Then exit;
      End;

      indice_grid:=self.DBGridPagar.SelectedIndex;
      marca_registro:= self.DBGridPagar.DataSource.DataSet.fieldbyname('codigo').AsInteger;
      ComandoOrdena:=comandosql+' order by ';

      If (ssCtrl in Shift) then
      begin  //Control pressionado, sinal de ordem por mais de uma coluna
        //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
        If(Pos('order ',Self.QueryPesq.sql.text)<>0)
        Then ComandoOrdena:=Self.QueryPesq.sql.text+','
        Else ComandoOrdena:=comandosql+' order by ';
      End;

      Self.QueryPesq.close;
      Self.QueryPesq.sql.clear;
      Self.QueryPesq.sql.add(Comandoordena+str_pegacampo);
      Self.QueryPesq.open;
      DBGridPagar.SelectedIndex:=ColunaAtual;
      Formatadbgrid(DBGridPagar,querypesq);
      Self.Personalizagrid;
    except

    end;
    DBGridPagar.setfocus;
  End
  Else Begin
    if key=VK_F11 then
    begin
      for cont:=0 to DBGridPagar.Columns.Count-1 do
        DBGridPagar.Columns.Items[cont].Visible :=true;
    End
    Else Begin
      if ((key=VK_f3) and (Self.NomeCadastroPersonalizacao<>'')) then
      begin
        Self.personalizacolunas;
      End;
    End;
  End;
end;

procedure TFtitulo_novo.DBGridReceberKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  indice_grid:integer;
  cont,marca_registro:integer ;
  comandoordena:String;
  ColunaAtual:integer;
begin
  if (key=VK_Delete) then
  begin
    DBGridReceber.Columns.Items[DBGridReceber.SelectedIndex].Visible :=false;
    DBGridReceber.SelectedIndex :=DBGridReceber.SelectedIndex +1;
  end;

  If (key=VK_F10) then
  begin
    Self.QueryPesq.close;
    Self.QueryPesq.sql.clear;
    Self.QueryPesq.sql.add(comandosql);
    Self.QueryPesq.open;
    DBGridReceber.SelectedIndex:=ColunaAtual;
    Formatadbgrid(DBGridReceber,querypesq);
    Self.Personalizagrid;
  end;

  if (key=VK_f12) then
  begin


    try
      ColunaAtual:=DBGridReceber.SelectedIndex;
      str_pegacampo:=DBGridReceber.SelectedField.FieldName;
      if (Length(str_pegacampo)>4) then
      begin
        if (uppercase(copy(str_pegacampo,1,2))='XX')
          and (uppercase(copy(str_pegacampo,length(str_pegacampo)-1,2))='XX')
        Then exit;
      End;

      indice_grid:=self.DBGridReceber.SelectedIndex;
      marca_registro:= self.DBGridReceber.DataSource.DataSet.fieldbyname('codigo').AsInteger;
      ComandoOrdena:=comandosql+' order by ';

      If (ssCtrl in Shift) then
      begin  //Control pressionado, sinal de ordem por mais de uma coluna
        //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
        If(Pos('order ',Self.QueryPesq.sql.text)<>0)
        Then ComandoOrdena:=Self.QueryPesq.sql.text+','
        Else ComandoOrdena:=comandosql+' order by ';
      End;

      Self.QueryPesq.close;
      Self.QueryPesq.sql.clear;
      Self.QueryPesq.sql.add(Comandoordena+str_pegacampo);
      Self.QueryPesq.open;
      DBGridReceber.SelectedIndex:=ColunaAtual;
      Formatadbgrid(DBGridReceber,querypesq);
      Self.Personalizagrid;

    except

    end;
    DBGridReceber.setfocus;
  End
  Else Begin
    if key=VK_F11 then
    begin
      for cont:=0 to DBGridReceber.Columns.Count-1 do
        DBGridReceber.Columns.Items[cont].Visible :=true;
    End
    Else Begin
      if ((key=VK_f3) and (Self.NomeCadastroPersonalizacao<>'')) then
      begin
        Self.personalizacolunas;
      End;
    End;
  End;
end;

procedure TFtitulo_novo.PersonalizaGrid;
begin
  ObjCamposPesquisa_Global.Personalizagrid(Self.DBGridReceber,self.NomeCadastroPersonalizacao);
end;

procedure TFtitulo_novo.PersonalizaColunas;
begin
  ObjCamposPesquisa_Global.ConfiguraColunas(Self.DBGridReceber,Self.NomeCadastroPersonalizacao);
  Self.PersonalizaGrid;
end;

procedure TFtitulo_novo.rbExataClick(Sender: TObject);
begin
  if (edtbusca.Enabled=true)
  then edtbusca.SetFocus;
end;

procedure TFtitulo_novo.rbInciciaComClick(Sender: TObject);
begin
  if (edtbusca.Enabled=true)
  then edtbusca.SetFocus;
end;

procedure TFtitulo_novo.rbTenhaemQualquerParteClick(Sender: TObject);
begin
  if (edtbusca.Enabled=true)
  then edtbusca.SetFocus;
end;

procedure TFtitulo_novo.EdtBuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if GuiaTitulos.PageIndex=0 then
  begin
    If (key=VK_F10) then
    begin
      Self.QueryPesq.close;
      Self.QueryPesq.sql.clear;
      Self.QueryPesq.sql.add(comandosql);
      Self.QueryPesq.open;
      Formatadbgrid(DBGridPagar,querypesq);
      Self.Personalizagrid;
    end;
  end
  else Begin
    if (GuiaTitulos.PageIndex=1) then
    begin
      If (key=VK_F10) then
      begin
        Self.QueryPesq.close;
        Self.QueryPesq.sql.clear;
        Self.QueryPesq.sql.add(comandosql);
        Self.QueryPesq.open;
        Formatadbgrid(DBGridPagar,querypesq);
        Self.Personalizagrid;
      end
    end;
  end;
end;

procedure TFtitulo_novo.EdtBuscaKeyPress(Sender: TObject; var Key: Char);
var
 str_busca,OrderBy:string;
 indice_grid:integer;
begin

  if key =#27 then//ESC
  begin
    if (GuiaTitulos.PageIndex=0) then
    begin
      Self.str_valorpesquisado:='';
      EdtBusca.Visible := false;
      lbQtdeRegistrosEncontrados.Caption:='';
      DBGridReceber.SetFocus;
      exit;
    end
    else Begin
      if (GuiaTitulos.PageIndex=1) then
      begin
        Self.str_valorpesquisado:='';
        EdtBusca.Visible := false;
        lbQtdeRegistrosEncontrados.Caption:='';
        DBGridPagar.SetFocus;
        exit;
      end;
    end;
  End;

  If Key=#13 then//procura  //Enter
  begin
    //Ftitulo_novo.Cursor:=crHourGlass;
    if (edtbusca.Text='')
    then exit;

    Self.str_valorpesquisado:='';
    str_busca:=edtbusca.Text;
    OrderBy:='';
    rbTenhaemQualquerParte.Visible:=true;
    rbInciciaCom.Visible:=true;
    rbExata.visible:=true;

    if (GuiaTitulos.PageIndex=0)then
    begin

      Case DBGridReceber.SelectedField.DataType of

        ftstring   :Begin
                      if (rbTenhaemQualquerParte.Checked=true)
                      then str_busca:='%'+str_busca+'%';

                      if (rbInciciaCom.Checked=true)
                      then str_busca:=str_busca+'%';

                      str_busca:=' UPPER('+str_pegacampo+') like '+#39+str_busca+#39;
                      OrderBy:= ' order by '+str_pegacampo;
                    End;

        ftinteger  :Begin
                      str_busca := str_pegacampo+' = '+trim(str_busca);
                    End;

        ftLargeint :Begin
                      str_busca := str_pegacampo+' = '+trim(str_busca)
                    end;

        ftfloat    :Begin
                      str_busca := tira_ponto(str_busca);
                      str_busca := virgulaparaponto(str_busca);
                      str_busca := str_pegacampo+' = '+trim(str_busca)
                    End;

        ftBcd      :Begin
                      str_busca := tira_ponto(str_busca);
                      str_busca := virgulaparaponto(str_busca);
                      str_busca := str_pegacampo+' = '+trim(str_busca)
                    End;
        ftFMTBcd    :Begin
                      str_busca := tira_ponto(str_busca);
                      str_busca := virgulaparaponto(str_busca);
                      str_busca := str_pegacampo+' = '+trim(str_busca)
                    End;

        ftDate     :Begin
                      str_busca := #39+FormatDateTime('mm/dd/yyyy', StrToDate(str_busca))+#39;
                      str_busca := str_pegacampo+' = '+trim(str_busca)
                    End;

        fttime     :Begin
                      str_busca := str_pegacampo+' = '+trim(str_busca)
                    End;

        ftDateTime :Begin
                      str_busca := str_pegacampo+' = '+trim(str_busca)
                    End;
      Else
        Begin
          Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
        End;
      end;

      indice_grid:=self.DBGridReceber.SelectedIndex;
      Self.QueryPesq.close;
      Self.QueryPesq.sql.clear;

      if(rbContasComSaldo.Checked) then//Com Saldo
      begin
        //titulos a receber
        Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ListaTitulos('C','C',str_pegacampo,str_busca)
      end
      else begin //sem saldo
        //titulos a receber
        Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ListaTitulos('C','S',str_pegacampo,str_busca)
      end;

      Self.str_valorpesquisado:=edtbusca.text;
      edtbusca.Text:='';
      Formatadbgrid(DBGridReceber);
      Formatadbgrid(DBGridReceber,FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo);
      Self.Personalizagrid;

      edtbusca.Visible:=False;
      rbInciciaCom.Visible:=false;
      rbExata.Visible:=false;
      rbTenhaemQualquerParte.Visible:=false;
      DBGridReceber.SetFocus;
      exit;

    End

    else Begin
      if (GuiaTitulos.PageIndex=1) then
      Begin
        cursor:=crHourGlass;
        Case DBGridPagar.SelectedField.DataType of

           ftstring   :Begin
                         if (rbTenhaemQualquerParte.Checked=true)
                         then str_busca:='%'+str_busca+'%';

                         if (rbInciciaCom.Checked=true)
                         then str_busca:=str_busca+'%';

                         str_busca:=' UPPER('+str_pegacampo+') like '+#39+str_busca+#39;
                         OrderBy:= ' order by '+str_pegacampo;
                       End;

           ftinteger  :Begin
                         str_busca := str_pegacampo+' = '+trim(str_busca);
                       End;

           ftLargeint :Begin
                         str_busca := str_pegacampo+' = '+trim(str_busca)
                       end;

           ftfloat    :Begin
                         str_busca := tira_ponto(str_busca);
                         str_busca := virgulaparaponto(str_busca);
                         str_busca := str_pegacampo+' = '+trim(str_busca)
                       End;

           ftBcd      :Begin
                         str_busca := tira_ponto(str_busca);
                         str_busca := virgulaparaponto(str_busca);
                         str_busca := str_pegacampo+' = '+trim(str_busca)
                       End;

           ftDate     :Begin
                         str_busca := #39+FormatDateTime('mm/dd/yyyy', StrToDate(str_busca))+#39;
                         str_busca := str_pegacampo+' = '+trim(str_busca)
                       End;

           fttime     :Begin
                         str_busca := str_pegacampo+' = '+trim(str_busca)
                       End;

           ftDateTime :Begin
                         str_busca := str_pegacampo+' = '+trim(str_busca)
                       End;
        Else
          Begin
            Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
          End;
        end;

        indice_grid:=self.DBGridPagar.SelectedIndex;
        Self.QueryPesq.close;
        Self.QueryPesq.sql.clear;

        if(rbContasComSaldo.Checked)//Com Saldo
        then Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ListaTitulos('D','C',str_pegacampo,str_busca)
        else Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ListaTitulos('D','S',str_pegacampo,str_busca);
      end;

      Self.str_valorpesquisado:=edtbusca.text;
      edtbusca.Text:='';
      Formatadbgrid(DBGridPagar);
      Formatadbgrid(DBGridPagar,FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo);
      Self.Personalizagrid;

      edtbusca.Visible:=False;
      rbInciciaCom.Visible:=false;
      rbExata.Visible:=false;
      rbTenhaemQualquerParte.Visible:=false;
      DBGridPagar.SetFocus;
      exit;
      cursor:=crDefault;
    End;

  end;

  //Aqui defino as regras que podem ser digitadas
  //como estou utilizando maskEdit Para Datas e Horas
  //N�o preciso pois defini uma mascara, mas para
  //float e Integer preciso

  if (GuiaTitulos.PageIndex=0) then
  begin

    Case DBGridReceber.SelectedField.DataType OF

      ftInteger:  Begin
                       if not (Key in ['0'..'9',#8])
                       Then key:=#0;
                  End;
      ftFloat  :  Begin
                       If Not(Key in ['0'..'9',#8,'.',','])
                       Then key:=#0
                       Else Begin
                                 If Key='.'
                                 Then Key:=',';
                            End;
                  End;
      ftCurrency  :  Begin
                       If Not(Key in ['0'..'9',#8,'.',','])
                       Then key:=#0
                       Else Begin
                                 If Key='.'
                                 Then Key:=',';
                            End;
                  End;
      ftBCD  :  Begin
                       If Not(Key in ['0'..'9',#8,'.',','])
                       Then key:=#0
                       Else Begin
                                 If Key='.'
                                 Then Key:=',';
                            End;
                  End;


    End;
  end
  else Begin
    if (GuiaTitulos.PageIndex=1) then
    begin

      Case DBGridPagar.SelectedField.DataType OF

        ftInteger:  Begin
                         if not (Key in ['0'..'9',#8])
                         Then key:=#0;
                    End;
        ftFloat  :  Begin
                         If Not(Key in ['0'..'9',#8,'.',',']) then
                           key:=#0
                         Else Begin
                           If Key='.'
                           Then Key:=',';
                         End;
                    End;
        ftCurrency  :  Begin
                       If Not(Key in ['0'..'9',#8,'.',',']) then
                         key:=#0
                       Else Begin
                         If Key='.'
                         Then Key:=',';
                       End;
                    End;
        ftBCD  :  Begin
                    If Not(Key in ['0'..'9',#8,'.',',']) then
                      key:=#0
                    Else Begin
                      If Key='.'
                      Then Key:=',';
                    End;
                  End;

      End;
    end;
  end
end;


procedure TFtitulo_novo.edtportadorKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;
end;

procedure TFtitulo_novo.edtcontagerencialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;
end;

procedure TFtitulo_novo.edtsubcontagerencialKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;
end;

procedure TFtitulo_novo.FrLancamento1edtcodigocredordevedorCPKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;
end;

procedure TFtitulo_novo.FrLancamento1edtcontagerencial_CPKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;
end;

procedure TFtitulo_novo.FrLancamento1edtsubcontagerencial_CPKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;
end;

procedure TFtitulo_novo.FrLancamento1edtcodigohistoricoCPKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;

  FrLancamento1.edtcodigohistoricoCRKeyPress(Sender, Key);
end;

procedure TFtitulo_novo.FrLancamento1edtcodigocredordevedorcrKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key := #0;
end;

procedure TFtitulo_novo.FrLancamento1edtcodigoboletoKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;
end;

procedure TFtitulo_novo.FrLancamento1edtcodigohistoricoCRKeyPress(
  Sender: TObject; var Key: Char);
begin
  if not (key in ['0'..'9',#8])
  Then key:=#0;

  FrLancamento1.edtcodigohistoricoCRKeyPress(Sender, Key);
end;

procedure TFtitulo_novo.DBGridReceberDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin

  if (Self.PesquisaEmEstoque = false) then // Pesquisa comum
  begin
    (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

    If not Odd(Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo.RecNo) then
    If not (gdselected in state)
    Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

    (Sender as TdbGrid).canvas.FillRect(rect);
    (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);
  end else
  Begin   // Pesquisa com cores diferente comformeo estoque
    (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

    If not Odd(Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo.RecNo) then
    If not (gdselected in state)
    Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

    (Sender as TdbGrid).canvas.FillRect(rect);
    (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);


    if (DBGridReceber.DataSource.DataSet.FieldList.IndexOf('codigo')<>-1) then
    begin
      if (DBGridReceber.DataSource.DataSet.FieldByName('codigo').asfloat<0) then
      begin
        DBGridReceber.canvas.Font.color:=rgb(153,0,0);
        DBGridReceber.DefaultDrawDataCell(rect,DBGridReceber.Columns[datacol].Field,state);
      End
      Else Begin
        if (DBGridReceber.DataSource.DataSet.FieldList.IndexOf('codigo')=-1)
        Then exit;

        if (DBGridReceber.DataSource.DataSet.FieldByName('codigo').asfloat<=DBGridReceber.DataSource.DataSet.FieldList.IndexOf('codigo')) then
        begin
          DBGridReceber.canvas.Font.color:=RGB(0,121,0);
          DBGridReceber.DefaultDrawDataCell(rect,DBGridReceber.Columns[datacol].Field,state);
        End;
      End;
    End;
  end;

  {
  if Odd(Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo.RecNo) and (Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo.State <> dsInsert) then   //Odd retorna verdadeiro se o argumento for impar, State Retorna o estado do segmento. USES DB
  begin
    DBGridReceber.Canvas.Brush.Color := CORGRIDZEBRADOGLOBAL1; // muda a cor
    DBGridReceber.Canvas.FillRect(Rect); // Preenche o fundo com a cor especificada
    DBGridReceber.DefaultDrawDataCell(Rect,Column.Field,State);// desenha as c�lulas da grade
  end;
        }
end;

procedure TFtitulo_novo.DBGridPagarDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (Self.PesquisaEmEstoque = false) then // Pesquisa comum
  begin
    (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;
    If not Odd(Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo.RecNo) then
    If not (gdselected in state)
    Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

    (Sender as TdbGrid).canvas.FillRect(rect);
    (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);
  end else
  Begin   // Pesquisa com cores diferente comformeo estoque
    (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

    If not Odd(Self.FrLancamento1.ObjLancamento.Pendencia.Titulo.ObjQueryMostraTitulo.RecNo) then
    If not (gdselected in state)
    Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

    (Sender as TdbGrid).canvas.FillRect(rect);
    (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);

    if (DBGridPagar.DataSource.DataSet.FieldList.IndexOf('CODIGO')<>-1) then
    begin
      if (DBGridPagar.DataSource.DataSet.FieldByName('CODIGO').asfloat<0) then
      begin
        DBGridPagar.canvas.Font.color:=rgb(153,0,0);
        DBGridPagar.DefaultDrawDataCell(rect,DBGridPagar.Columns[datacol].Field,state);
      End
      Else Begin
        if (DBGridPagar.DataSource.DataSet.FieldList.IndexOf('CODIGO')=-1)
        Then exit;

        if (DBGridPagar.DataSource.DataSet.FieldByName('estoque').asfloat<=DBGridPagar.DataSource.DataSet.FieldList.IndexOf('estoqueminimo')) then
        begin
          DBGridPagar.canvas.Font.color:=RGB(0,121,0);
          DBGridPagar.DefaultDrawDataCell(rect,DBGridPagar.Columns[datacol].Field,state);
        End;
      End;
    End;
  end;

end;

procedure TFtitulo_novo.FrLancamento1BtSelecionaTodas_CRClick(
  Sender: TObject);
begin
  FrLancamento1.BtSelecionaTodas_CRClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1CheckSelicionaPagamentoClick(
  Sender: TObject);
begin
  FrLancamento1.CheckSelecionaPagamentoClick(sender);
end;

procedure TFtitulo_novo.FrLancamento1CheckSelecionaRecebimentoClick(
  Sender: TObject);
begin
  FrLancamento1.CheckSelecionaRecebimentoClick(Sender);
end;

procedure TFtitulo_novo.memoobservacaoExit(Sender: TObject);
begin
  btgravar.setfocus;
end;

procedure TFtitulo_novo.FrLancamento1BtpesquisaCPClick(Sender: TObject);
begin
  try
    screen.Cursor:=crHourGlass;
    FrLancamento1.BtpesquisaCPClick(Sender);
  finally
    screen.cursor:=crDefault;
  end;
end;

procedure TFtitulo_novo.FrLancamento1edtcodigocredordevedorCPKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  FrLancamento1.edtcodigocredordevedorCPKeyDown(Sender, Key, Shift);
end;

procedure TFtitulo_novo.StrGridPendenciasMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Observacao:string;
  QueryAddObs:TIBQuery;
begin
  if(Button=mbright)then
  begin
    InputQuery('Observa��o na pendencia','Digite uma observa��o para esta pendencia',Observacao);
    try
      try
        QueryAddObs:=TIBQuery.Create(nil);
        QueryAddObs.Database:=FDataModulo.IBDatabase;
      except
      end;

      With QueryAddObs do
      begin
        Close;
        sql.Clear;
        sql.Add('update tabpendencia set observacao='+#39+Observacao+#39);
        sql.Add('where codigo='+StrGridPendencias.Cells[0,StrGridPendencias.Row]);
        ExecSQL;
        FDataModulo.IBTransaction.CommitRetaining;
        if FrLancamento1.ObjLancamento.Pendencia.Titulo.LocalizaCodigo(lbcodigo.Caption)=False
        then Exit;
        Self.TabelaParaControles;
      end;

    finally
    end;
  end;
end;

procedure TFtitulo_novo.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
  if (Key = VK_F1) then
    Fprincipal.chamaPesquisaMenu(FrLancamento1.ObjLancamento.Pendencia.Titulo.Status);

  if (Key = VK_F2) then
  begin
    FAjuda.PassaAjuda('CONTAS');
    FAjuda.ShowModal;
  end;
end ;

procedure TFtitulo_novo.btAjudaClick(Sender: TObject);
begin
  FAjuda.PassaAjuda('CONTAS');
  FAjuda.ShowModal;
end;

{Rodolfo}
procedure TFtitulo_novo.rb100Click(Sender: TObject);
begin
  opPesquisa := 2;

  if(rbContasComSaldo.checked)then
    rbContasComSaldoClick(Sender)
  else rbContasSemSaldoClick(Sender);
end;

{Rodolfo}
procedure TFtitulo_novo.rb1000Click(Sender: TObject);
begin
  opPesquisa := 3;
  if(rbContasComSaldo.checked)then
    rbContasComSaldoClick(Sender)
  else rbContasSemSaldoClick(Sender);
end;

{Rodolfo}
procedure TFtitulo_novo.rbTodosClick(Sender: TObject);
begin
  opPesquisa := 1;
  if(rbContasComSaldo.checked)then
    rbContasComSaldoClick(Sender)
  else rbContasSemSaldoClick(Sender);
end;

procedure TFtitulo_novo.FrLancamento1lbDefaultMouseLeave(
  Sender: TObject);
begin
  TLabel(Sender).Font.Style:=[fsbold];
  TLabel(Sender).Font.Color:= clBlack;
  Screen.Cursor := crDefault;
end;

procedure TFtitulo_novo.FrLancamento1lbDefaultMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  TLabel(Sender).Font.Style:=[fsBold,fsUnderline];
  TLabel(Sender).Font.Color:= clGreen;
  Screen.Cursor := crHandPoint;
end;

procedure TFtitulo_novo.rbHojeClick(Sender: TObject);
begin
  opPesquisa := 0;

  if(rbContasComSaldo.checked)then
    rbContasComSaldoClick(Sender)
  else rbContasSemSaldoClick(Sender);
end;

procedure TFtitulo_novo.AtualizaParametroInformacoes(pvalor:boolean);
var
  qtemp : TIBQuery;
  temp : string;
begin
  if(pvalor) then
    temp := 'SIM'
  else
    temp := 'NAO';

  qtemp := TIBQuery.Create(nil);

  try
    qtemp.Database := FDataModulo.IBDatabase;
    qtemp.Close;
    qtemp.SQL.Clear;
    qtemp.SQL.Add('UPDATE TABPARAMETROS SET VALOR=' + QuotedStr(temp));
    qtemp.SQL.Add('WHERE NOME = '+QuotedStr('ATUALIZA VALORES NA TELA DE CONTAS AO CARREGAR'));
    //InputBox('','',qtemp.SQL.Text);
    qtemp.ExecSQL;
    FDataModulo.IBTransaction.CommitRetaining;
  finally
    FreeAndNil(qtemp);
  end;
end;

procedure TFtitulo_novo.chkAtualizaraoAbrirClick(Sender: TObject);
begin
  if(chkAtualizaraoAbrir.Checked) then
    AtualizaPanelRodape := True
  else
    AtualizaPanelRodape := False;
end;

procedure TFtitulo_novo.LimpaLabelsPanelRodape;
var
  dia, mes, ano:string;
begin
  ano:=FormatDateTime('yyyy',Now);
  mes:=FormatDateTime('mm',Now);
  dia:=IntToStr(DiasdoMes(ano, mes));

  lbReceberHoje.caption:=formata_valor('0');
  lbPagarHoje.caption:=formata_valor('0');
  lbReceberMes.caption:=formata_valor('0');
  lbPagarMes.caption:=formata_valor('0');
  lbReceberGeral.caption:=formata_valor('0');
  LbPagarGeral.caption:=formata_valor('0');
  LbMesCorrente.Caption:=MesExtenso(StrToInt(mes));

end;

end.


