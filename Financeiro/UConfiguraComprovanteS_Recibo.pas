unit UConfiguraComprovanteS_Recibo;

interface

uses
  URelCheque, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, Buttons,UobjConfRelatorio,QRctrls, UobjPortador,UrelComprovanteChequeSRecibo;

type
  TFConfiguraComprovanteS_recibo = class(TForm)
    LbValor: TLabel;
    LBExtenso1: TLabel;
    LBExtenso2: TLabel;
    LbNominal: TLabel;
    LbCidadeCheque: TLabel;
    Panel: TPanel;
    edtleft: TMaskEdit;
    edttop: TMaskEdit;
    LBLEFT: TLabel;
    LBTOP: TLabel;
    Btsalvar: TBitBtn;
    LbDiaCheque: TLabel;
    LbMesCheque: TLabel;
    LbAnoCheque: TLabel;
    btimprime: TBitBtn;
    lbdobanco: TLabel;
    LbNumeroCheque: TLabel;
    LbLinha1Desc: TLabel;
    LbLinha2Desc: TLabel;
    LbLinha3Desc: TLabel;
    LbConta: TLabel;
    procedure LBpagoAClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure BtsalvarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtleftExit(Sender: TObject);
    procedure btimprimeClick(Sender: TObject);
  private
    { Private declarations }
    Procedure PassaParaAzul;
  public
    { Public declarations }
  end;

var
  FConfiguraComprovanteS_recibo: TFConfiguraComprovanteS_recibo;
  FrelComprovanteChequeSRecibo:TFrelComprovanteChequeSRecibo;
  ObjConfiguraRel:TObjConfRelatorio;
  ObjPortador:TObjPortador;
implementation

uses UessencialGlobal, UobjParametros;



{$R *.DFM}

procedure TFConfiguraComprovanteS_recibo.LBpagoAClick(Sender: TObject);

begin
     edtleft.text:=inttostr(TLabel(Sender).left);
     edttop.text:=inttostr(TLabel(Sender).top);
     Panel.caption:=Tlabel(Sender).Name;
     Self.PassaParaAzul;
     TLabel(Sender).Font.Color:=clred;
     edtleft.SetFocus;
     
end;

procedure TFConfiguraComprovanteS_recibo.FormActivate(Sender: TObject);
VAR
INT_HABILITA:INTEGER;
POrtadorTemp:INteger;
PortadorTempStr:String;
saida:boolean;
begin
     If (Tag=1)
     Then exit;
     Panel.caption:='';
     edtleft.text:='0';
     edttop.text:='0';
     Self.PassaParaAzul;
     PegaFiguraBotao(BtSalvar,'BOTAOGRAVAR.BMP');
     PegaFiguraBotao(btImprime,'BOTAORELATORIOS.BMP');

     Try
        FrelComprovanteChequeSRecibo:=TFrelComprovanteChequeSRecibo.create(nil);
        ObjPortador:=TObjPortador.create;
     Except
           Messagedlg('Erro na tentativa de cria��o do objeto Portador!',mterror,[mbok],0);
           exit;
     End;

     Try
        ObjConfiguraRel:=TObjConfRelatorio.create;
     Except
           Messagedlg('N�o � poss�vel Configurar a Impress�o Devido a um Problema na Cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0);
           exit;
     End;

     //resgatando as configura��es atuais do quickreport

   ObjconfiguraRel.RecuperaArquivoRelatorio(FrelComprovanteChequeSRecibo,'IMPRIMECOMPROVANTES_RECIBO');

   for int_habilita:=0 to FrelComprovanteChequeSRecibo.ComponentCount -1 do
   Begin
        if FrelComprovanteChequeSRecibo.Components [int_habilita].ClassName = 'TQRLabel'
        then Begin
                Try
                        Tlabel(Self.FindComponent(FrelComprovanteChequeSRecibo.Components [int_habilita].Name)).Left:=TQrlabel(FrelComprovanteChequeSRecibo.Components [int_habilita]).left;
                        Tlabel(Self.FindComponent(FrelComprovanteChequeSRecibo.Components [int_habilita].Name)).Top:=TQrlabel(FrelComprovanteChequeSRecibo.Components [int_habilita]).Top;
                Except
                      Messagedlg('Erro no Componente '+FrelComprovanteChequeSRecibo.Components [int_habilita].Name+' apresentou um erro!',mterror,[mbok],0);
                      Self.Close;
                      exit;
                End;
             End;
   End;
   tag:=1;
end;

procedure TFConfiguraComprovanteS_recibo.edtleftKeyPress(Sender: TObject;
  var Key: Char);
begin

     If not(key in ['0'..'9',#8,#13])
     Then key:=#0;

     If Key=#13
     Then Begin
              edtleft.OnExit(sender);
          End;
end;

procedure TFConfiguraComprovanteS_recibo.PassaParaAzul;
var
cont:Integer;
begin
     for cont:=0 to Self.ComponentCount -1 do
     Begin
        if (Uppercase(Self.Components [cont].ClassName)= 'TLABEL')
        then TLabel(Self.Components [cont]).font.color:=clblue;
     End;

end;

procedure TFConfiguraComprovanteS_recibo.BtsalvarClick(Sender: TObject);
var
int_habilita:Integer;
begin
   for int_habilita:=0 to Self.ComponentCount -1 do
   Begin
        if ((Self.Components [int_habilita].ClassName = 'TLabel')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBLEFT')
        and (Uppercase(Self.Components [int_habilita].Name) <>'LBTOP'))
        Then Begin
                TQRlabel(FrelComprovanteChequeSRecibo.FindComponent(Self.Components [int_habilita].Name)).Left:=Tlabel(Self.Components [int_habilita]).left;
                TQRlabel(FrelComprovanteChequeSRecibo.FindComponent(Self.Components [int_habilita].Name)).Top:=TLabel(Self.Components [int_habilita]).Top;
             End;
   End;

     //GRAVANDO OS DADOS ATUAIS NO QUICKREPORT

     IF (ObjconfiguraRel.NovoArquivoRelatorio(FrelComprovanteChequeSRecibo,'IMPRIMECOMPROVANTES_RECIBO')=False)
     Then Begin
               Messagedlg('N�o foi poss�vel gravar o arquivo de configura��o!',mterror,[mbok],0);
               exit;
          End;
     Messagedlg('Arquivo Gravado com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFConfiguraComprovanteS_recibo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     If (ObjConfiguraRel<>nil)
     Then Freeandnil(ObjConfiguraRel);
     If (Objportador<>nil)
     Then Freeandnil(objportador);
     tag:=0;
     Freeandnil(FrelComprovanteChequeSRecibo);
end;

procedure TFConfiguraComprovanteS_recibo.edtleftExit(Sender: TObject);
var
numerico:integer;
begin
     Try
        numerico:=Strtoint(edtleft.text);
        numerico:=Strtoint(edttop.text);
     Except
           exit;
     End;

     If (panel.caption<>'')
     Then Begin
              TLabel(Self.FindComponent(Panel.caption)).left:=Strtoint(Edtleft.text);
              TLabel(Self.FindComponent(Panel.caption)).Top:=Strtoint(EdtTop.text);
     End;
end;

procedure TFConfiguraComprovanteS_recibo.btimprimeClick(Sender: TObject);
//usado pelo Sintraesul
var
   TempObjConfRelatorio:tobjconfrelatorio;
   Objparametros:TobjParametros;
   ValorExtensoCheque,extenso1,extenso2:String;
   linhaatual,cont,quantcaractereschequelinha:Integer;
   quantcaractereslinhadescricao,quantidadelinhasdescricao:integer;
   Tmpano,TmpMes,TmpDia:word;
   FrelComprovanteChequeSRecibo:TFrelComprovanteChequeSRecibo;
   TMPdescricaocomprovante:String;
Begin
     If (Messagedlg('Deseja Salvar as Altera��es antes de Imprimir?',mtconfirmation,[mbyes,mbno],0)=MrYEs)
     Then Self.Btsalvar.OnClick(sender);

Try

     FrelComprovanteChequeSRecibo:=TFrelComprovanteChequeSRecibo.create(nil);

     Try
        quantcaractereschequelinha:=1;
        TempObjConfRelatorio:=TObjConfRelatorio.create;
        quantcaractereschequelinha:=2;
        Objparametros:=TObjParametros.create;
     Except
           If (quantcaractereschequelinha=1)
           Then Messagedlg('Erro na cria��o do Objeto de Configura��o de Relat�rio!',mterror,[mbok],0)
           Else Begin
                     freeandnil(TempObjConfRelatorio);
                     If (quantcaractereschequelinha=2)
                     Then Messagedlg('Erro na cria��o do Objeto de Parametros!',mterror,[mbok],0);
                End;
           exit;
     End;
     Try

       If (Objparametros.LocalizaNome('QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 01 NO CHEQUE?" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;

       Objparametros.TabelaparaObjeto;

       Try
          quantcaractereschequelinha:=StrToInt(Objparametros.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres no valor extenso do Cheque!',mterror,[mbok],0);
             exit;
       End;
       //***********************************************
       //Referente ao campo descricao do comprovante de pagamento

       If (Objparametros.LocalizaNome('QUANTIDADE DE CARACTERES LINHA 1 DESC. COMPROVANTE')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE CARACTERES LINHA 1 DESC. COMPROVANTE" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;

       Objparametros.TabelaparaObjeto;

       Try
          quantcaractereslinhadescricao:=StrToInt(Objparametros.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de caracteres na linha da descri��o do comprovante!',mterror,[mbok],0);
             exit;
       End;
       If (Objparametros.LocalizaNome('QUANTIDADE DE LINHAS NA DESCRICAO DO COMPROVANTE')=fALSE)
       Then Begin
                 Messagedlg('O Parametro "QUANTIDADE DE LINHAS NA DESCRICAO DO COMPROVANTE" "VALOR=NUM�RICO" n�o foi encontrado na tabela de parametros!',mterror,[mbok],0);
                 exit;
            End;

       Objparametros.TabelaparaObjeto;

       Try
          quantidadelinhasdescricao:=StrToInt(Objparametros.Get_Valor);
       Except
             Messagedlg('Valor Inv�lido para a quantidade de linhas na descri��o do comprovante!',mterror,[mbok],0);
             exit;
       End;
       //***********************************************
       TempObjConfRelatorio.RecuperaArquivoRelatorio(FrelComprovanteChequeSRecibo,'IMPRIMECOMPROVANTES_RECIBO');
       FrelComprovanteChequeSRecibo.LbValor.caption:='R$ '+formatfloat('###,##0.00',Strtofloat('88888,88'));
       //**************************IMPRIMINDO A C�PIA DO CHEQUE*****************
       ValorExtensoCheque:='';
       extenso1:='';
       extenso2:='';
       ValorExtensoCheque:=valorextenso(Strtofloat('88888,88'));
       DividirValor(ValorExtensoCheque,quantcaractereschequelinha,200,extenso1,extenso2);
       FrelComprovanteChequeSRecibo.LbExtenso1.caption:=extenso1;
       FrelComprovanteChequeSRecibo.LbExtenso2.caption:=extenso2;
       FrelComprovanteChequeSRecibo.LbNominal.caption:='NOMINAL A TESTE DE IMPRESS�O';
       FrelComprovanteChequeSRecibo.LbCidadeCheque.caption:='DOURADOS/MS';
       DecodeDate(NOW,Tmpano,TmpMes,TmpDia);
       FrelComprovanteChequeSRecibo.LbDiaCheque.caption:=Inttostr(TmpDia);
       FrelComprovanteChequeSRecibo.LbMesCheque.caption:=MesExtenso(TmpMes);
       FrelComprovanteChequeSRecibo.LbAnoCheque.caption:=Inttostr(TmpAno);
       //*********IMPRIMINDO OS DADOS ABAIXO DA C�PIA DO CHEQUE*****************
       FrelComprovanteChequeSRecibo.LbNumeroCheque.caption:='999.999';
       FrelComprovanteChequeSRecibo.LbConta.caption:='123456-X';

       FrelComprovanteChequeSRecibo.LbdoBanco.caption:='BANCO DO MATO GROSSO DO SUL';

       //*******************************************
       //Tenho apenas 3 linhas neste tipo de comprovante, se for um unico pagamento
       //utilizo as 3, se for mais de um tenho que imprimir em anexo a listagem dos t�tulos pagos

       FrelComprovanteChequeSRecibo.LbLinha1Desc.caption:='';
       FrelComprovanteChequeSRecibo.LbLinha2Desc.caption:='';
       FrelComprovanteChequeSRecibo.LbLinha3Desc.caption:='';
       
       linhaatual:=1;
       //dividindo em varias linhas
       TMPdescricaocomprovante:='';
       TMPdescricaocomprovante:='TESTE CONTENDO 3 LINHAS NA IMPRESS�O DA C�PIA DE CHEQUE';
       TMPdescricaocomprovante:=TMPdescricaocomprovante+' DO SISTEMA FINANCEIRO ANA, TODOS OS DIREITOS  DE C�PIA';
       TMPdescricaocomprovante:=TMPdescricaocomprovante+'PERTENCEM UNICA E EXCLUSIVAMENTE A EXCLAIM TECNOLOGIA';
       for cont:=1 to length(TMPdescricaocomprovante) do
       Begin

            if (cont mod quantcaractereslinhadescricao=0)
            Then inc(linhaatual,1);

            Case linhaatual of
            1:FrelComprovanteChequeSRecibo.LbLinha1Desc.caption:=FrelComprovanteChequeSRecibo.LbLinha1Desc.caption+TMPdescricaocomprovante[cont];
            2:FrelComprovanteChequeSRecibo.LbLinha2Desc.caption:=FrelComprovanteChequeSRecibo.LbLinha2Desc.caption+TMPdescricaocomprovante[cont];
            3:FrelComprovanteChequeSRecibo.LbLinha3Desc.caption:=FrelComprovanteChequeSRecibo.LbLinha3Desc.caption+TMPdescricaocomprovante[cont];
            Else FrelComprovanteChequeSRecibo.LbLinha3Desc.caption:=FrelComprovanteChequeSRecibo.LbLinha3Desc.caption+'...';
            End;
       End;

       If (messagedlg('Pressione <OK> para imprimir ou <CANCELAR> para visualizar a impress�o do Comprovante!',mtinformation,[mbok,mbcancel],0)=MrCAncel)
       Then FrelComprovanteChequeSRecibo.Qr.preview
       Else FrelComprovanteChequeSRecibo.Qr.print;

     Finally
            freeandnil(TempObjconfrelatorio);
            Objparametros.free;
     End;
Finally
	Freeandnil(FrelComprovanteChequeSRecibo);
End;

End;

end.
