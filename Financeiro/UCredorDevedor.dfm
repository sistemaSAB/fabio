object Fcredordevedor: TFcredordevedor
  Left = 216
  Top = 218
  Width = 781
  Height = 540
  Caption = 'Cadastro de Credores/Devedores - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelrodape: TPanel
    Left = 0
    Top = 452
    Width = 765
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    DesignSize = (
      765
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 765
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 454
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 851
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 851
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 765
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 499
      Top = 0
      Width = 266
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Credor / Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -27
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btOpcoes: TBitBtn
      Left = 351
      Top = -1
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
    end
    object BtCancelar: TBitBtn
      Left = 151
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btsair: TBitBtn
      Left = 401
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object Btgravar: TBitBtn
      Left = 101
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object BtNovo: TBitBtn
      Left = 1
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 50
    Width = 765
    Height = 402
    Align = alClient
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 763
      Height = 400
      Align = alClient
      Stretch = True
      Transparent = True
    end
    object Label1: TLabel
      Left = 17
      Top = 12
      Width = 39
      Height = 14
      Caption = 'Codigo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 17
      Top = 36
      Width = 32
      Height = 14
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 17
      Top = 60
      Width = 35
      Height = 14
      Caption = 'Tabela'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 17
      Top = 84
      Width = 36
      Height = 14
      Caption = 'Objeto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 17
      Top = 108
      Width = 77
      Height = 14
      Caption = 'Instru'#231#227'o SQL'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel
      Left = 17
      Top = 132
      Width = 74
      Height = 14
      Caption = 'Campo Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label7: TLabel
      Left = 17
      Top = 156
      Width = 95
      Height = 14
      Caption = 'Nome Formulario'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label8: TLabel
      Left = 17
      Top = 180
      Width = 87
      Height = 14
      Caption = 'Campo Cont'#225'bil'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label9: TLabel
      Left = 17
      Top = 204
      Width = 109
      Height = 14
      Caption = 'Campo Raz'#227'o Social'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label10: TLabel
      Left = 17
      Top = 228
      Width = 94
      Height = 14
      Caption = 'Campo CPF/CNPJ'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label11: TLabel
      Left = 17
      Top = 252
      Width = 69
      Height = 14
      Caption = 'Campo RG/IE'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label12: TLabel
      Left = 17
      Top = 276
      Width = 129
      Height = 14
      Caption = 'Campo Conta Gerencial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label13: TLabel
      Left = 362
      Top = 12
      Width = 90
      Height = 14
      Caption = 'Campo Telefone'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label14: TLabel
      Left = 362
      Top = 36
      Width = 94
      Height = 14
      Caption = 'Campo Endereco'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label15: TLabel
      Left = 362
      Top = 84
      Width = 75
      Height = 14
      Caption = 'Campo Bairro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label16: TLabel
      Left = 362
      Top = 108
      Width = 64
      Height = 14
      Caption = 'Campo Cep'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label17: TLabel
      Left = 362
      Top = 132
      Width = 79
      Height = 14
      Caption = 'Campo Estado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label18: TLabel
      Left = 362
      Top = 156
      Width = 80
      Height = 14
      Caption = 'Campo Cidade'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label19: TLabel
      Left = 362
      Top = 180
      Width = 81
      Height = 14
      Caption = 'Campo Celular'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label20: TLabel
      Left = 362
      Top = 60
      Width = 132
      Height = 14
      Caption = 'Campo N'#250'mero da Casa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label21: TLabel
      Left = 17
      Top = 300
      Width = 83
      Height = 14
      Caption = 'Campo Cr'#233'dito'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label22: TLabel
      Left = 362
      Top = 204
      Width = 145
      Height = 14
      Caption = 'Campo Telefone Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label23: TLabel
      Left = 362
      Top = 228
      Width = 149
      Height = 14
      Caption = 'Campo Endereco Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label24: TLabel
      Left = 362
      Top = 252
      Width = 187
      Height = 14
      Caption = 'Campo N'#250'mero da Casa Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object label25: TLabel
      Left = 362
      Top = 276
      Width = 130
      Height = 14
      Caption = 'Campo Bairro Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object label26: TLabel
      Left = 362
      Top = 300
      Width = 119
      Height = 14
      Caption = 'Campo Cep Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object label27: TLabel
      Left = 362
      Top = 324
      Width = 134
      Height = 14
      Caption = 'Campo Estado Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object label28: TLabel
      Left = 362
      Top = 348
      Width = 135
      Height = 14
      Caption = 'Campo Cidade Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object label29: TLabel
      Left = 362
      Top = 372
      Width = 136
      Height = 14
      Caption = 'Campo Celular Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCodigo: TEdit
      Left = 147
      Top = 9
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object edtnome: TEdit
      Left = 147
      Top = 33
      Width = 199
      Height = 19
      MaxLength = 50
      TabOrder = 1
    end
    object edttabela: TEdit
      Left = 147
      Top = 57
      Width = 199
      Height = 19
      MaxLength = 50
      TabOrder = 2
    end
    object edtobjeto: TEdit
      Left = 147
      Top = 81
      Width = 199
      Height = 19
      MaxLength = 50
      TabOrder = 3
    end
    object edtinstrucaosql: TEdit
      Left = 147
      Top = 105
      Width = 199
      Height = 19
      MaxLength = 100
      TabOrder = 4
    end
    object edtcamponome: TEdit
      Left = 147
      Top = 129
      Width = 199
      Height = 19
      MaxLength = 30
      TabOrder = 5
    end
    object edtnomeformulario: TEdit
      Left = 147
      Top = 153
      Width = 199
      Height = 19
      MaxLength = 50
      TabOrder = 6
    end
    object edtcampocontabil: TEdit
      Left = 147
      Top = 177
      Width = 199
      Height = 19
      MaxLength = 50
      TabOrder = 7
    end
    object edtcamporazaosocial: TEdit
      Left = 147
      Top = 201
      Width = 199
      Height = 19
      MaxLength = 50
      TabOrder = 8
    end
    object edtcampocpfcnpj: TEdit
      Left = 147
      Top = 225
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 9
    end
    object edtcamporgie: TEdit
      Left = 148
      Top = 249
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 10
    end
    object edtcampocontagerencial: TEdit
      Left = 148
      Top = 273
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 11
    end
    object edttelefone: TEdit
      Left = 551
      Top = 10
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 15
    end
    object edtendereco: TEdit
      Left = 551
      Top = 34
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 16
    end
    object edtbairro: TEdit
      Left = 551
      Top = 82
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 17
    end
    object edtcampocep: TEdit
      Left = 551
      Top = 106
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 18
    end
    object edtcampoestado: TEdit
      Left = 551
      Top = 130
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 19
    end
    object edtcampocidade: TEdit
      Left = 551
      Top = 154
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 20
    end
    object edtcelular: TEdit
      Left = 551
      Top = 178
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 21
    end
    object EdtCamponumeroCasa: TEdit
      Left = 550
      Top = 58
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 13
    end
    object edtcampocredito: TEdit
      Left = 148
      Top = 298
      Width = 199
      Height = 19
      MaxLength = 50
      TabOrder = 12
    end
    object edtTelefoneCobranca: TEdit
      Left = 551
      Top = 202
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 22
    end
    object edtEnderecocobranca: TEdit
      Left = 551
      Top = 226
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 23
    end
    object edtNumeroCasacobranca: TEdit
      Left = 550
      Top = 250
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 14
    end
    object edtBairrocobranca: TEdit
      Left = 551
      Top = 274
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 24
    end
    object edtCEPCobranca: TEdit
      Left = 551
      Top = 298
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 25
    end
    object edtEstadoCobranca: TEdit
      Left = 551
      Top = 322
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 26
    end
    object edtCidadeCobranca: TEdit
      Left = 551
      Top = 346
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 27
    end
    object edtCelularCobranca: TEdit
      Left = 551
      Top = 370
      Width = 199
      Height = 19
      MaxLength = 20
      TabOrder = 28
    end
  end
end
