unit UValores;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db,UObjValores,Uobjchequesportador,
  UessencialGlobal;

type
  TFvalores = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel2: TPanel;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    BtCancelar: TBitBtn;
    btopcoes: TBitBtn;
    Label1: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label23: TLabel;
    lbportador: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    edtvalor: TEdit;
    edtlancamento: TEdit;
    edtportador: TEdit;
    combotipo: TComboBox;
    ImagemFundo: TImage;
    Panel1: TPanel;
    Label17: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label22: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    edtcodigodebarras: TEdit;
    edtcpfcliente2: TEdit;
    edtcliente2: TEdit;
    edtCliente1: TEdit;
    edtcpfcliente1: TEdit;
    edtcomp: TEdit;
    edtbanco: TEdit;
    edtagencia: TEdit;
    edtdv: TEdit;
    edtc1: TEdit;
    edtconta: TEdit;
    edtc2: TEdit;
    edtserie: TEdit;
    edtnumcheque: TEdit;
    edtc3: TEdit;
    Label13: TLabel;
    edtvencimento: TMaskEdit;
    Shape1: TShape;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure edtlancamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure combotipoChange(Sender: TObject);
    procedure edtportadorExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         Procedure ResgataValores;
         Function SalvaDadosCheque:boolean;
         Procedure ExcluiCheque(Cod:string);
         Procedure PegaDadosCheque(PCodCheque:string);

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fvalores: TFvalores;
  ObjValores:TObjValores;
  codigocheque:string[09];


implementation

uses Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFvalores.ControlesParaObjeto: Boolean;
Begin
     If (combotipo.ItemIndex=1)
     Then Begin
               If SalvaDadosCheque=False
               Then Begin
                        Messagedlg('Erro na tentativa de Salvar o Cheque Recebido!',mterror,[mbok],0);
                        result:=False;
                        exit;
               End;
     End
     Else CodigoCheque:='';

  Try

    With ObjValores do
    Begin
         Submit_CODIGO           (edtCODIGO.text);
         Submit_Historico        (edtHistorico.text);
         Submit_Tipo             (comboTipo.text[1]);
         Submit_Valor            (edtValor.text);
         Submit_Lancamento       (edtLancamento.text);
         Submit_Portador         (edtPortador.text);
         Submit_Cheque           (codigocheque);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFvalores.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjValores do
     Begin
        edtCODIGO.text          :=Get_CODIGO            ;
        edtHistorico.text       :=Get_Historico         ;

        If (Get_Tipo='C')
        Then Begin
                comboTipo.itemindex:=1;
                PegaDadosCheque(Get_cheque);
             End
        Else If (Get_Tipo='D')
             Then ComboTipo.ItemIndex:=0
             Else ComboTipo.ItemIndex:=2;

        edtValor.text           :=Get_Valor             ;
        edtLancamento.text      :=Get_Lancamento        ;
        edtPortador.text        :=Get_Portador          ;
        lbportador.caption:=ObjValores.Portador.Get_Nome;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFvalores.TabelaParaControles: Boolean;
begin
     ObjValores.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFvalores.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjValores=Nil)
     Then exit;

    If (ObjValores.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjValores.free;
end;

procedure TFvalores.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFvalores.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
      end;
end;


procedure TFvalores.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     lbportador.caption:='';
     habilita_campos(Self);
     esconde_botoes(Self);
     edtvencimento.text:=datetostr(now);
     //edtcodigo.text:='0';
     edtcodigo.text:=ObjValores.get_NovoCodigo;
     edtcodigo.enabled:=False;
     ResgataValores;

     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;

     ObjValores.status:=dsInsert;
     Edthistorico.setfocus;
end;

procedure TFvalores.BtCancelarClick(Sender: TObject);
begin
     ObjValores.cancelar;
     
     limpaedit(Self);
     lbportador.caption:='';
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFvalores.BtgravarClick(Sender: TObject);
begin

     If ObjValores.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     If (RegistroValores.LancamentoExterno=True)
     Then Begin
                If (ObjValores.salvar(False,true)=False)
                Then Begin
                          //se deu erro na ora de gravar na tabvalores
                          //exclui o cheque lan�ado
                         If (combotipo.itemindex=0)//cheque
                         Then ExcluiCheque(codigocheque);
                         exit;

                     End;
          End
     Else Begin
                If (ObjValores.salvar(True,true)=False)
                Then Begin
                          //se deu erro na ora de gravar na tabvalores
                          //exclui o cheque lan�ado
                         If (combotipo.itemindex=0)//cheque
                         Then ExcluiCheque(codigocheque);
                        exit;
                     End;
          End;
     mostra_botoes(Self);
     limpaedit(Self);
     lbportador.caption:='';
     desabilita_campos(Self);
     //Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFvalores.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     ObjValores.Status:=dsEdit;
     edthistorico.setfocus;
end;

procedure TFvalores.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjValores.Get_pesquisa,ObjValores.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjValores.status<>dsinactive
                                  then exit;

                                  If (ObjValores.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFvalores.btalterarClick(Sender: TObject);
begin
{    If (ObjValores.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;}
end;

procedure TFvalores.btexcluirClick(Sender: TObject);
begin
     If (ObjValores.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjValores.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     IF (RegistroValores.LancamentoExterno=True)
     Then Begin//naum posso comitar ta sendo lancado por outro que precisa finalizar antes de dar commit
             If (ObjValores.exclui(edtcodigo.text,false)=False)
             Then Begin
                       Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
                       exit;
                  End;
     End
     Else Begin
             If (ObjValores.exclui(edtcodigo.text,True)=False)
             Then Begin
                       Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
                       exit;
                  End;
     End;
     limpaedit(Self);
     lbportador.caption:='';
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFvalores.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
Var
   TempPortador:Integer;
Begin
     If NewTab=1
     Then Begin
               If combotipo.itemindex<>1
               Then AllowChange:=False;
          End;
end;

procedure TFvalores.ResgataValores;
var
atual:currency;
begin
     If (UobjValores.RegistroValores.LancamentoExterno=True)
     Then Begin
               edthistorico.text                :=UobjValores.RegistroValores.Historico;
               atual:=ObjValores.Get_SomaValores(UobjValores.RegistroValores.Lancamento);
               edtvalor.text                    :=floattostr(strtofloat(UobjValores.RegistroValores.Valor)-Atual);
               edtlancamento.text               :=UobjValores.RegistroValores.Lancamento;
               edtlancamento.Enabled:=false;
          end;


end;

procedure TFvalores.edtlancamentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjValores.Get_PesquisaLancamento,ObjValores.get_titulopesquisalancamento,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtlancamento.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFvalores.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjValores.Get_PesquisaPortador,ObjValores.get_titulopesquisaPortador,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        edtportador.OnExit(sender);

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFvalores.combotipoChange(Sender: TObject);
begin
     If Combotipo.itemindex=1
     Then Begin
               If (Messagedlg('Deseja lan�ar os dados do Cheque?',mtconfirmation,[mbyes,mbno],0)=Mryes)
               Then Begin
                         Try
                            strtoint(edtlancamento.text);
                            If (ObjValores.Lancamento.LocalizaCodigo(edtlancamento.text)=True)
                            Then Begin
                                      ObjValores.Lancamento.TabelaparaObjeto;
                                      edtcpfcliente1.text:=ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CPFCNPJCredorDevedor(ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,ObjValores.Lancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);
                                      edtcliente1.text:=ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor(ObjValores.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.get_codigo,ObjValores.Lancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR);

                            End;

                         Except

                         End;

                         //Transfiro para outra guia
                    End;

          End;
end;

function TFvalores.SalvaDadosCheque: boolean;
var
  ObjChequesPortador:tobjchequesportador;
begin
     Try
        ObjChequesPortador:=Tobjchequesportador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Controle de Cheques por Portadores',mterror,[mbok],0);
           result:=False;
           exit;
     End;

     Try
        ObjchequesPortador.status:=dsinsert;
        codigocheque:=ObjchequesPortador.Get_NovoCodigo;
        ObjchequesPortador.Submit_CODIGO(codigocheque);
        ObjchequesPortador.Submit_Portador(edtportador.text);
        ObjchequesPortador.Submit_Valor           ( edtvalor.text);
        ObjchequesPortador.Submit_Vencimento      (edtVencimento.text);
        ObjchequesPortador.Submit_Comp            (edtComp.text);
        ObjchequesPortador.Submit_Banco           (edtBanco.text);
        ObjchequesPortador.Submit_Agencia         (edtAgencia.text);
        ObjchequesPortador.Submit_C1              (edtC1.text);
        ObjchequesPortador.Submit_Conta           (edtConta.text);
        ObjchequesPortador.Submit_C2              (edtC2.text);
        ObjchequesPortador.Submit_Serie           (edtSerie.text);
        ObjchequesPortador.Submit_NumCheque       (edtNumCheque.text);
        ObjchequesPortador.Submit_C3              (edtC3.text);
        ObjchequesPortador.Submit_Cliente1        (edtCliente1.text);
        ObjchequesPortador.Submit_CPFCliente1     (edtCPFCliente1.text);
        ObjchequesPortador.Submit_Cliente2        (edtCliente2.text);
        ObjchequesPortador.Submit_CPFCliente2     (edtCPFCliente2.text);
        ObjchequesPortador.Submit_CodigodeBarras  (edtCodigodeBarras.text);

        If (ObjChequesPortador.SalvarChequeRecebido(False)=False)
        Then Begin
                  result:=False;
                  CodigoCheque:='';
                  exit;
             End
        Else result:=True;
     Finally
            ObjChequesPortador.free;
     End;


end;

procedure TFvalores.ExcluiCheque(Cod: string);
var
  ObjChequesPortador:tobjchequesportador;
begin
     Try
        ObjChequesPortador:=Tobjchequesportador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Controle de Cheques por Portadores',mterror,[mbok],0);
           exit;
     End;

     Try
     If (ObjChequesPortador.status<>dsinactive) or (cod='')
     Then exit;

     If (ObjChequesPortador.LocalizaCodigo(cod)=False)
     Then Begin
               Messagedlg('Cheque n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (ObjChequesPortador.Exclui(cod,false)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o do Cheque!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;

        
     Finally
            ObjChequesPortador.free;
     End;




end;

procedure TFvalores.PegaDadosCheque(PCodCheque: string);
var
  ObjChequesPortador:tobjchequesportador;
begin
     Try
        ObjChequesPortador:=Tobjchequesportador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto de Controle de Cheques por Portadores',mterror,[mbok],0);
           exit;
     End;

     Try
        If (ObjChequesPortador.LocalizaCodigo(PcodCheque)=False)
        Then Begin
                  Messagedlg('O Cheque N�o Foi Encontrado na Tabela de Cheques!',mterror,[mbok],0);
                  exit;
             End;
        ObjChequesPortador.TabelaparaObjeto;
        
        edtVencimento.text    :=ObjchequesPortador.Get_Vencimento       ;
        edtComp.text          :=ObjchequesPortador.Get_Comp             ;
        edtBanco.text         :=ObjchequesPortador.Get_Banco            ;
        edtAgencia.text       :=ObjchequesPortador.Get_Agencia          ;
        edtC1.text            :=ObjchequesPortador.Get_C1               ;
        edtConta.text         :=ObjchequesPortador.Get_Conta            ;
        edtC2.text            :=ObjchequesPortador.Get_C2               ;
        edtSerie.text         :=ObjchequesPortador.Get_Serie            ;
        edtNumCheque.text     :=ObjchequesPortador.Get_NumCheque        ;
        edtC3.text            :=ObjchequesPortador.Get_C3               ;
        edtCliente1.text      :=ObjchequesPortador.Get_Cliente1         ;
        edtCPFCliente1.text   :=ObjchequesPortador.Get_CPFCliente1      ;
        edtCliente2.text      :=ObjchequesPortador.Get_Cliente2         ;
        edtCPFCliente2.text   :=ObjchequesPortador.Get_CPFCliente2      ;
        edtCodigodeBarras.text:=ObjchequesPortador.Get_CodigodeBarras   ;
     Finally
            ObjChequesPortador.free;
     End;


end;

procedure TFvalores.edtportadorExit(Sender: TObject);
begin
     lbportador.caption:='';
     if (edtportador.text='')
     Then exit;

     If (ObjValores.Portador.LocalizaCodigo(edtportador.text)=False)
     Then Begin
               edtportador.text:='';
               exit;
     End;
     ObjValores.Portador.TabelaparaObjeto;
     lbportador.caption:=ObjValores.Portador.Get_Nome;

end;

procedure TFvalores.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

      limpaedit(Self);

     desabilita_campos(Self);

     Try
        ObjValores:=TObjValores.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
end;

end.
