unit UExportaContabilidade;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjexportacontabilidade,
  Grids, IBCustomDataSet, IBQuery, IBDatabase,uobjarquivoini;

type
  TFExportaContabilidade = class(TForm)
    Guia: TTabbedNotebook;
    EdtCodigo: TEdit;
    Label1: TLabel;
    edtcontadebite: TMaskEdit;
    Label18: TLabel;
    edtdata: TMaskEdit;
    edtcontacredite: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    lbnomedebite: TLabel;
    lbnomecredite: TLabel;
    edtvalor: TMaskEdit;
    Label4: TLabel;
    edthistorico: TMaskEdit;
    comboexporta: TComboBox;
    Label6: TLabel;
    SaveDialog: TSaveDialog;
    edtobjgerador: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    edtcodgerador: TEdit;
    Label9: TLabel;
    edtexportado: TEdit;
    Label10: TLabel;
    edtnomedoarquivo: TEdit;
    Panel1: TPanel;
    btabrir: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btrelatorios: TBitBtn;
    BtImportaRegistros: TBitBtn;
    BtRetornaExportacao: TBitBtn;
    Btexportados: TBitBtn;
    btalterar: TBitBtn;
    Btnovo: TBitBtn;
    BtExportacao: TBitBtn;
    btnaoexportados: TBitBtn;
    btpesquisar: TBitBtn;
    bFechamento: TBitBtn;
    STGridConferencia: TStringGrid;
    Panelfiltro: TPanel;
    Label11: TLabel;
    Btpesquisa_conferencia: TButton;
    QueryConferencia: TIBQuery;
    edtdatafinal: TMaskEdit;
    edtdatainicial: TMaskEdit;
    Label12: TLabel;
    edtnumdocto: TMaskEdit;
    IBDatabaseBackup: TIBDatabase;
    IBTransactionBackup: TIBTransaction;
    OpenDialog: TOpenDialog;
    PanelConfronto: TPanel;
    Label13: TLabel;
    edtcaminhobancobackup: TEdit;
    btabrirbancobackup: TButton;
    btcaminhobancobackup: TButton;
    Button1: TButton;
    STRGConfronto: TStringGrid;
    BtConfrontotabela: TButton;
    PanelAuditoria: TPanel;
    btlocalizaauditoria: TButton;
    StrgAuditoria: TStringGrid;
    edtdatainicial_auditoria: TMaskEdit;
    edtdatafinal_auditoria: TMaskEdit;
    Label5: TLabel;
    btlancar_auditoria: TButton;
    btselecionar_todos_auditoria: TButton;
    StrgConfiguracao: TStringGrid;
    panelconfiguracao: TPanel;
    Label14: TLabel;
    Button2: TButton;
    combocampos: TComboBox;
    Label15: TLabel;
    edtcaracterseparador: TEdit;
    Label16: TLabel;
    btexportar: TButton;
    MemoLog: TMemo;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btnaoexportadosClick(Sender: TObject);
    procedure edtcontadebiteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtExportacaoClick(Sender: TObject);
    procedure edtcontacrediteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcontadebiteExit(Sender: TObject);
    procedure edtcontacrediteExit(Sender: TObject);
    procedure edtcontadebiteKeyPress(Sender: TObject; var Key: Char);
    procedure btrelatoriosClick(Sender: TObject);
    procedure BtexportadosClick(Sender: TObject);
    procedure BtRetornaExportacaoClick(Sender: TObject);
    procedure btabrirClick(Sender: TObject);
    procedure BtImportaRegistrosClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure bFechamentoClick(Sender: TObject);
    procedure Btpesquisa_conferenciaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure Button1Click(Sender: TObject);
    procedure btabrirbancobackupClick(Sender: TObject);
    procedure btcaminhobancobackupClick(Sender: TObject);
    procedure BtConfrontotabelaClick(Sender: TObject);
    procedure STRGConfrontoDblClick(Sender: TObject);
    procedure btlocalizaauditoriaClick(Sender: TObject);
    procedure btlancar_auditoriaClick(Sender: TObject);
    procedure btselecionar_todos_auditoriaClick(Sender: TObject);
    procedure StrgAuditoriaDblClick(Sender: TObject);
    procedure StrgAuditoriaKeyPress(Sender: TObject; var Key: Char);
    procedure StrgConfiguracaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Button2Click(Sender: TObject);
    procedure combocamposKeyPress(Sender: TObject; var Key: Char);
    procedure btexportarClick(Sender: TObject);
  private
         Objexportacontabilidade:TObjexportacontabilidade;
         ObjArquivoIni:tobjarquivoini;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Function  Busca:boolean;
         Function CarregaIni:boolean;
    { Private declarations }
  public
    { Public declarations }
        { Public declarations }
        Function Criar:boolean;
        Procedure Destruir;

  end;

var
  FExportaContabilidade: TFExportaContabilidade;


implementation

uses UessencialGlobal, Upesquisa, UPLanodeContas, UDataModulo,
  UescolheImagemBotao, UconfrontoContabilidade, UObjLancamento,
  UescolheTipoCamporelatorio, UopcoesListCheckBox, UopcaoRel, UFiltraImp;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFExportaContabilidade.ControlesParaObjeto: Boolean;
Begin
  Try
    With Objexportacontabilidade do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Data            (edtData.text        );
         Submit_ContaDebite     (edtContaDebite.text );
         Submit_ContaCredite    (edtContaCredite.text);
         Submit_Valor           (edtValor.text       );
         Submit_Historico       (edtHistorico.text   );
         Submit_Exporta         (comboExporta.text[1]);
         Submit_NumDocto(edtnumdocto.Text);



         Submit_ObjGerador    (edtobjgerador.text);
         Submit_CodGerador    (edtcodgerador.text);
         Submit_Exportado     (edtexportado.text);
         Submit_NomedoArquivo(edtnomedoarquivo.Text);


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFExportaContabilidade.ObjetoParaControles: Boolean;
Begin
  Try
     With Objexportacontabilidade do
     Begin
        edtCODIGO.text       :=Get_CODIGO       ;
        edtData.text         :=Get_Data         ;
        edtContaDebite.text  :=Get_ContaDebite  ;
        LbNomeDebite.caption:=Get_NomeDebite   ;
        edtContaCredite.text :=Get_ContaCredite ;
        lbNomeCredite.caption:=Get_NomeCredite  ;
        edtValor.text        :=Get_Valor        ;
        edtHistorico.text    :=Get_Historico    ;
        IF (get_Exporta='S')
        Then comboexporta.itemindex:=0
        Else comboexporta.itemindex:=1;
        edtnumdocto.Text:=Get_NumDocto;

        edtobjgerador.text:=Get_ObjGerador;
        edtcodgerador.text:=Get_CodGerador;
        edtexportado.text:=Get_Exportado;
        edtnomedoarquivo.Text:=Get_NomedoArquivo;
        
        Result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFExportaContabilidade.TabelaParaControles: Boolean;
begin
     If (Objexportacontabilidade.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
//****************************************



procedure TFExportaContabilidade.FormClose(Sender: TObject; var Action: TCloseAction);
begin

    If (Objexportacontabilidade.status<>dsinactive)
    Then Begin
            Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
            abort;
            exit;
    End;

    If (Objexportacontabilidade<>Nil)
    Then Objexportacontabilidade.free;

    if (ObjArquivoIni <> nil)
    then ObjArquivoIni.free;

    IBDatabaseBackup.Close;


end;

procedure TFExportaContabilidade.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFExportaContabilidade.BtnovoClick(Sender: TObject);
begin
     if (ObjPermissoesUsoGlobal.ValidaPermissao('CRIAR NOVO REGISTRO DE EXPORTA��O PARA CONTABILIDADE')=False)
     Then EXIT;

     //limpaedit(Self);
     lbnomedebite.caption:='';
     lbnomecredite.caption:='';
     habilita_campos(Self);
     desab_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=Objexportacontabilidade.Get_novocodigo;
     edtobjgerador.Text:='MANUAL';
     edtcodgerador.Text:='0';
     edtexportado.text:='N';
     comboexporta.ItemIndex:=1;
     edtcodigo.enabled:=False;


     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     Objexportacontabilidade.status:=dsInsert;
     Guia.pageindex:=0;
     edtdata.setfocus;
end;


procedure TFExportaContabilidade.btalterarClick(Sender: TObject);
var
TMPCODIGO:string;
begin
    if (ObjPermissoesUsoGlobal.ValidaPermissao('ALTERAR REGISTRO DE EXPORTA��O PARA CONTABILIDADE')=False)
    Then EXIT;

    If (Objexportacontabilidade.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Objexportacontabilidade.Status:=dsEdit;
                guia.pageindex:=0;
                edtdata.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btnaoexportados.enabled:=True;
    End
    Else Begin
              if (Self.Busca)
              Then Self.btalterarClick(sender);
    End;


end;

procedure TFExportaContabilidade.btgravarClick(Sender: TObject);
begin

     If Objexportacontabilidade.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     //passo como se fosse partida simples
     If (Objexportacontabilidade.salvar(true,true)=False)
     Then exit;
     //************************************************************************
     habilita_botoes(Self);
     //************************************************************************
     lbnomedebite.caption:=edtdatainicial.text;
     lbnomecredite.caption:=edtdatafinal.text;
     //************************************************************************
     limpaedit(Self);
     //************************************************************************
     edtdatainicial.text:=lbnomedebite.caption;
     edtdatafinal.text:=lbnomecredite.caption;
     //************************************************************************
     lbnomedebite.caption:='';
     lbnomecredite.caption:='';
     //************************************************************************
     desabilita_campos(Self);
end;

procedure TFExportaContabilidade.btexcluirClick(Sender: TObject);
begin
     if (ObjPermissoesUsoGlobal.ValidaPermissao('EXCLUIR REGISTRO DE EXPORTA��O PARA CONTABILIDADE')=False)
     Then EXIT;

     If (Objexportacontabilidade.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Objexportacontabilidade.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (Objexportacontabilidade.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     lbnomedebite.caption:='';
     lbnomecredite.caption:='';
end;

procedure TFExportaContabilidade.btcancelarClick(Sender: TObject);
begin
     Objexportacontabilidade.cancelar;
     //************************************************************************
     habilita_botoes(Self);
     //************************************************************************
     lbnomedebite.caption:=edtdatainicial.text;
     lbnomecredite.caption:=edtdatafinal.text;
     //************************************************************************
     limpaedit(Self);
     //************************************************************************
     edtdatainicial.text:=lbnomedebite.caption;
     edtdatafinal.text:=lbnomecredite.caption;
     //************************************************************************
     lbnomedebite.caption:='';
     lbnomecredite.caption:='';
     //************************************************************************
     desabilita_campos(Self);
end;

procedure TFExportaContabilidade.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFExportaContabilidade.btnaoexportadosClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
            Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Objexportacontabilidade.Get_PesquisaNaoExportados,Objexportacontabilidade.Get_TituloPesquisaNaoExportados,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Objexportacontabilidade.status<>dsinactive
                                  then exit;

                                  If (Objexportacontabilidade.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            lbnomedebite.caption:='';
                                            lbnomecredite.caption:='';
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

function TFExportaContabilidade.Criar: boolean;
begin


end;

procedure TFExportaContabilidade.Destruir;
begin

end;

procedure TFExportaContabilidade.edtcontadebiteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FplanodeCOntas:=TFplanodeCOntas.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Objexportacontabilidade.ObjPlanodeContas.Get_Pesquisa,Objexportacontabilidade.ObjPlanodeContas.Get_TituloPesquisa,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                lbnomedebite.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;


procedure TFExportaContabilidade.BtExportacaoClick(Sender: TObject);
var
path:string;
begin
     If Objexportacontabilidade.Status<>dsinactive
     then exit;
     Path:='';
     path:=ExtractFilePath(Application.exename);
     If path[length(path)]<>'\'
     then path:=path+'\';
     path:=path+'Exporta\';
     SaveDialog.InitialDir:=path;
     
     IF (SaveDialog.execute)
     Then ObjExportaContabilidade.exportadados(SaveDialog.filename);

end;

procedure TFExportaContabilidade.edtcontacrediteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);

var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FplanodeCOntas:=TFplanodeCOntas.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Objexportacontabilidade.ObjPlanodeContas.Get_Pesquisa,Objexportacontabilidade.ObjPlanodeContas.Get_TituloPesquisa,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                lbnomecredite.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;

procedure TFExportaContabilidade.edtcontadebiteExit(Sender: TObject);
begin
     if edtcontadebite.text<>''
     Then Begin
               If Objexportacontabilidade.ObjPlanodeContas.LocalizaCodigo(edtcontadebite.text)=true
               then Begin
                         Objexportacontabilidade.ObjPlanodeContas.TabelaparaObjeto;
                         lbnomedebite.caption:=Objexportacontabilidade.ObjPlanodeContas.Get_Nome;
                    End
               Else BEgin
                        edtcontadebite.text:='';
                        lbnomedebite.caption:='';
                    End;
          End
     Else lbnomedebite.caption:='';
end;

procedure TFExportaContabilidade.edtcontacrediteExit(Sender: TObject);
begin
     if edtcontacredite.text<>''
     Then Begin
               If Objexportacontabilidade.ObjPlanodeContas.LocalizaCodigo(edtcontacredite.text)=true
               then Begin
                         Objexportacontabilidade.ObjPlanodeContas.TabelaparaObjeto;
                         lbnomecredite.caption:=Objexportacontabilidade.ObjPlanodeContas.Get_Nome;
                    End
               Else BEgin
                        edtcontacredite.text:='';
                        lbnomecredite.caption:='';
                    End;
          End
     Else lbnomecredite.caption:='';
end;

procedure TFExportaContabilidade.edtcontadebiteKeyPress(Sender: TObject;
  var Key: Char);
begin
if not (key in['0'..'9',#8])
then key:=#0;
end;

procedure TFExportaContabilidade.btrelatoriosClick(Sender: TObject);
begin
     Objexportacontabilidade.imprime;
end;

procedure TFExportaContabilidade.BtexportadosClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Objexportacontabilidade.Get_PesquisaExportados,Objexportacontabilidade.Get_TituloPesquisaExportados,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Objexportacontabilidade.status<>dsinactive
                                  then exit;

                                  If (Objexportacontabilidade.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            lbnomedebite.caption:='';
                                            lbnomecredite.caption:='';
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

procedure TFExportaContabilidade.BtRetornaExportacaoClick(Sender: TObject);
var
nomearq:String;
begin
     If (InputQuery('RETORNA EXPORTA��O','Nome do Arquivo',nomearq)=false)
     Then exit;
     If (nomearq='')
     Then Messagedlg('O Nome do Arquivo deve ser informado!',mtinformation,[mbok],0)
     Else Objexportacontabilidade.RetornaExportacao(NomeArq);
end;

function TFExportaContabilidade.Busca: boolean;
var
TmpCodigo:String;
begin
     result:=False;
      if(Objexportacontabilidade.Status=dsinactive)
      Then Begin
                tmpcodigo:='';
                if (InputQuery('PROCURA C�DIGO','DIGITE O C�DIGO',tmpcodigo)=False)
                Then Exit;

                if (tmpcodigo='')
                Then exit;

                Try
                   strtoint(tmpcodigo);
                except
                      exit;
                end;

                if (Objexportacontabilidade.LocalizaCodigo(tmpcodigo)=False)
                Then exit;

                Objexportacontabilidade.TabelaparaObjeto;
                Self.ObjetoParaControles;
                result:=True;
      End;
end;

procedure TFExportaContabilidade.btabrirClick(Sender: TObject);
begin
     Self.Busca;
end;

procedure TFExportaContabilidade.BtImportaRegistrosClick(Sender: TObject);
begin
     //Objexportacontabilidade.ImportaRegistros;
end;


procedure TFExportaContabilidade.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
            Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Objexportacontabilidade.Get_Pesquisa,'LAN�AMENTOS CONT�BEIS',Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Objexportacontabilidade.status<>dsinactive
                                  then exit;

                                  If (Objexportacontabilidade.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            lbnomedebite.caption:='';
                                            lbnomecredite.caption:='';
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFExportaContabilidade.bFechamentoClick(Sender: TObject);
begin
     Objexportacontabilidade.Opcoesfechamento;
end;

procedure TFExportaContabilidade.Btpesquisa_conferenciaClick(
  Sender: TObject);
var
cont,cont2:integer;
psaldo:Currency;
begin
     try
         StrToDate(edtdatainicial.Text);
         strtodate(edtdatafinal.text);
     Except
           Mensagemerro('Datas Inv�lidas');
           exit;
     End;

     STGridConferencia.ColCount:=12;
     STGridConferencia.Cols[0].clear;
     STGridConferencia.Cells[0,0]:='DIF';
     STGridConferencia.Cols[1].clear;
     STGridConferencia.Cells[1,0]:='CODIGO';
     STGridConferencia.Cols[2].clear;
     STGridConferencia.Cells[2,0]:='DATA';
     STGridConferencia.Cols[3].clear;
     STGridConferencia.Cells[3,0]:='CONTA D';
     STGridConferencia.Cols[4].clear;
     STGridConferencia.Cells[4,0]:='NOME D';
     STGridConferencia.Cols[5].clear;
     STGridConferencia.Cells[5,0]:='CONTA C';
     STGridConferencia.Cols[6].clear;
     STGridConferencia.Cells[6,0]:='NOME C';
     STGridConferencia.Cols[7].clear;
     STGridConferencia.Cells[7,0]:='VALOR';
     STGridConferencia.Cols[8].clear;
     STGridConferencia.Cells[8,0]:='HISTORICO';
     STGridConferencia.Cols[9].clear;
     STGridConferencia.Cells[9,0]:='OBJGERADOR';
     STGridConferencia.Cols[10].clear;
     STGridConferencia.Cells[10,0]:='CODGERADOR';
     STGridConferencia.Cols[11].clear;
     STGridConferencia.Cells[11,0]:='SALDO';
     cont:=0;
     With QueryConferencia do
     begin

          close;
          sql.clear;
          sql.add('Select codigo,');
          sql.add('data,');
          sql.add('contadebite,nomedebite,contacredite,nomecredite,');
          sql.add('valor,historico,objgerador,codgerador');
          sql.add('from tabexportacontabilidade  TC');
          sql.add('where Tc.data>='+#39+Formatdatetime('mm/dd/yyyy', strtodate(edtdatainicial.text))+#39);
          sql.add('and Tc.Data<='#39+Formatdatetime('mm/dd/yyyy', strtodate(edtdatafinal.text))+#39);
          sql.add('order by codigo');
          open;
          last;
          STGridConferencia.RowCount:=recordcount+1;
          first;
          psaldo:=0;
          While not(eof) do
          begin
               if (fieldbyname('contacredite').asstring<>'')
               then Psaldo:=PSaldo+fieldbyname('valor').asfloat;

               if (fieldbyname('contadebite').asstring<>'')
               then Psaldo:=PSaldo-fieldbyname('valor').asfloat;

               inc(cont,1);
               STGridConferencia.Cells[0,cont]:='';
               STGridConferencia.Cells[1,cont]:=fieldbyname('CODIGO').asstring;
               STGridConferencia.Cells[2,cont]:=fieldbyname('DATA').asstring;
               STGridConferencia.Cells[3,cont]:=fieldbyname('CONTADebite').asstring;
               STGridConferencia.Cells[4,cont]:=fieldbyname('NOMEDebite').asstring;
               STGridConferencia.Cells[5,cont]:=fieldbyname('CONTACredite').asstring;
               STGridConferencia.Cells[6,cont]:=fieldbyname('NOMECredite').asstring;
               STGridConferencia.Cells[7,cont]:=formata_valor(fieldbyname('VALOR').asstring);
               STGridConferencia.Cells[8,cont]:=fieldbyname('HISTORICO').asstring;
               STGridConferencia.Cells[9,cont]:=fieldbyname('OBJGERADOR').asstring;
               STGridConferencia.Cells[10,cont]:=fieldbyname('CODGERADOR').asstring;
               STGridConferencia.Cells[11,cont]:=Formata_valor(Psaldo);
               next;
          End;
          for cont2:=cont downto 1 do
          Begin
               if (strtofloat(tira_ponto(STGridConferencia.Cells[11,cont2]))=0)
               then break;
          End;


          for cont:=cont2+1 to stgridconferencia.rowcount-1 do
          begin
               STGridConferencia.Cells[0,cont]:='X';
          End;

          
          AjustaLArguraColunaGrid(STGridConferencia);
          mensagemaviso('Conclu�do');
          STGridConferencia.Setfocus;

          if (cont2<STGridConferencia.Rowcount-1)
          Then STGridConferencia.Row:=Cont2+1;
     End;
end;

procedure TFExportaContabilidade.FormShow(Sender: TObject);
begin


     limpaedit(Self);
     desabilita_campos(Self);
     lbnomedebite.caption:='';
     lbnomecredite.caption:='';
     Guia.PageIndex:=0;
     Uessencialglobal.PegaCorForm(Self);

     Try
        Objexportacontabilidade:=TObjexportacontabilidade.create;
        QueryConferencia.Database:=FDataModulo.IBDatabase;
        ObjArquivoIni:=tobjarquivoini.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btnaoexportados,btrelatorios,btexcluir,btsair);
     PegaFiguraBotao(Btexportados,'botaopesquisar.bmp');
     PegaFiguraBotao(btpesquisar,'botaopesquisar.bmp');
     PegaFiguraBotao(btabrir,'botaoalterar.bmp');

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE DADOS A SEREM EXPORTADOS PARA A CONTABILIDADE')=False)
     Then desab_botoes(Self);


end;

procedure TFExportaContabilidade.GuiaChange(Sender: TObject;
  NewTab: Integer; var AllowChange: Boolean);
begin
     case newtab of
     1:habilita_campos(Panelfiltro);
     2:begin
            habilita_campos(PanelConfronto);
            habilita_botoes(PanelConfronto);
     end;
     3:Begin
            habilita_campos(PanelAuditoria);
            habilita_botoes(PanelAuditoria);
     End;
     4:Begin//configura Exportacao
            Self.CarregaIni;
            habilita_campos(panelconfiguracao);
            habilita_botoes(panelconfiguracao);
     end;

     
     end;
end;

procedure TFExportaContabilidade.Button1Click(Sender: TObject);
begin
     Self.Objexportacontabilidade.ConfrontoGerador(IBDatabaseBackup,STRGConfronto);
     
end;

procedure TFExportaContabilidade.btabrirbancobackupClick(Sender: TObject);
begin
     IBDatabaseBackup.close;
     IBDatabaseBackup.DatabaseName:=edtcaminhobancobackup.Text;
     IBDatabaseBackup.Open;
end;

procedure TFExportaContabilidade.btcaminhobancobackupClick(
  Sender: TObject);
begin
     if (opendialog.execute)
     then edtcaminhobancobackup.Text:=OpenDialog.FileName;
     
end;

procedure TFExportaContabilidade.BtConfrontotabelaClick(Sender: TObject);
begin
     Self.Objexportacontabilidade.ConfrontoTABELA(IBDatabaseBackup,STRGConfronto);
end;

procedure TFExportaContabilidade.STRGConfrontoDblClick(Sender: TObject);
var
QueryLocal,QueryBackup:Tibquery;
begin
     IF (TRIM(STRGConfronto.Cells[0,STRGConfronto.row])='')
      or (TRIM(STRGConfronto.Cells[1,STRGConfronto.row])='')
     then Begin
               mensagemaviso('N�o existe informa��o nessa linha para abrir os dados');
               exit;
     end;





     try
        QueryLocal:=Tibquery.Create(nil);
        QueryLocal.Database:=FDataModulo.IBDatabase;
        QueryBackup:=Tibquery.Create(nil);
        QueryBackup.Database:=IBDatabaseBackup;
     Except
        mensagemerro('Erro na tentativa de criar as querys');
        exit;
     End;

     Try



        try
            QueryLocal.Close;
            QueryLocal.sql.Clear;
            QueryLocal.sql.add('Select * from tabexportacontabilidade where OBJGERADOR='+#39+STRGConfronto.Cells[0,STRGConfronto.row]+#39);
            QueryLocal.sql.add('and codgerador='+#39+STRGConfronto.Cells[1,STRGConfronto.row]+#39);
            QueryLocal.sql.add('order by historico');
            QueryLocal.open;

            QueryBackup.Close;
            QueryBackup.sql.Clear;
            QueryBackup.sql.add('Select * from tabexportacontabilidade where OBJGERADOR='+#39+STRGConfronto.Cells[0,STRGConfronto.row]+#39);
            QueryBackup.sql.add('and codgerador='+#39+STRGConfronto.Cells[1,STRGConfronto.row]+#39);
            Querybackup.sql.add('order by historico');
            QueryBackup.open;
        Except
              on e:exception do
              begin
                    mensagemerro('Erro na tentativa de resgatar os dados '+#13+'Erro: '+e.Message);
                    exit;
              End;
        End;


        FconfrontoContabilidade.caption:='Diferen�a '+STRGConfronto.Cells[4,STRGConfronto.row];
        FconfrontoContabilidade.DSAtual.DataSet:=QueryLocal;
        FconfrontoContabilidade.DSBackup.DataSet:=QueryBackup;
        formatadbgrid(FconfrontoContabilidade.DbGridAtual);
        formatadbgrid(FconfrontoContabilidade.DbGridbackup);
        FConfrontoContabilidade.Showmodal;
     Finally
        Freeandnil(QueryLocal);
        FreeAndNil(QueryBackup);
     End;
     
end;

procedure TFExportaContabilidade.btlocalizaauditoriaClick(Sender: TObject);
begin
     ObjLanctoPortadorGlobal.Lancamento.RetornaLancamentoSemContabilidade(edtdatainicial_auditoria.text,edtdatafinal_auditoria.text,strgauditoria);
end;

procedure TFExportaContabilidade.btlancar_auditoriaClick(Sender: TObject);
var
cont:integer;
objlancamentox:tobjlancamento;
PreceberPagar:string;
begin
     Try
        objlancamentox:=tobjlancamento.create;
     Except
        mensagemerro('Erro na tentativa de gerar o objeto de lan�amento');
        exit; 
     end;

     Try
         for cont:=1 to StrgAuditoria.rowcount-1 do
         Begin
              StrgAuditoria.row:=cont;
              if (StrgAuditoria.cells[0,cont]='X')
              Then Begin
                        if (StrgAuditoria.cells[2,cont]='TABLANCAMENTO')
                        Then Begin
                                  objlancamentox.ZerarTabela;
                                  objlancamentox.LocalizaCodigo(StrgAuditoria.cells[1,cont]);
                                  objlancamentox.TabelaparaObjeto;

                                  PreceberPagar:='';

                                  if (objlancamentox.Pendencia.get_codigo='')
                                  Then Begin
                                            //lote
                                            if (objlancamentox.LocalizaPai(objlancamentox.get_codigo)=False)
                                            then Begin
                                                      if (mensagempergunta('Nenhum lan�amento ligado ao lote '+objlancamentox.get_codigo+' foi encontrado. Deseja continuar?')=mrno)
                                                      then exit;
                                            End
                                            Else Begin
                                                     objlancamentox.TabelaparaObjeto;

                                                     if (objlancamentox.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C')
                                                     Then Begin
                                                               if (objlancamentox.ExportaContabilidade_Recebimento_Lote(StrgAuditoria.cells[1,cont])=False)
                                                               Then Begin
                                                                         if (mensagempergunta('N�o foi poss�vel lan�ar a contabilidade do lan�amento '+objlancamentox.get_codigo+#13+'Deseja continuar?')=mrno)
                                                                         Then exit;
                                                               End
                                                               Else objlancamentox.Commit;
                                                     End
                                                     Else Begin
                                                               if (objlancamentox.ExportaContabilidade_Pagamento_Lote(StrgAuditoria.cells[1,cont])=False)
                                                               Then Begin
                                                                         if (mensagempergunta('N�o foi poss�vel lan�ar a contabilidade do lan�amento '+objlancamentox.get_codigo+#13+'Deseja continuar?')=mrno)
                                                                         Then exit;
                                                               End
                                                               Else objlancamentox.Commit;
                                                     End;
                                            End;
                                  End
                                  Else Begin
                                            //nao � em lote
                                            if (objlancamentox.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo='C')
                                            Then Begin
                                                      if (objlancamentox.ExportaContabilidade_Recebimento=False)
                                                      Then Begin
                                                                if (mensagempergunta('N�o foi poss�vel lan�ar a contabilidade do lan�amento '+objlancamentox.get_codigo+#13+'Deseja continuar?')=mrno)
                                                                Then exit;
                                                      End
                                                      Else objlancamentox.Commit;
                                            End
                                            Else Begin
                                                      if (objlancamentox.ExportaContabilidade_Pagamento=False)
                                                      Then Begin
                                                                if (mensagempergunta('N�o foi poss�vel lan�ar a contabilidade do lan�amento '+objlancamentox.get_codigo+#13+'Deseja continuar?')=mrno)
                                                                Then exit;
                                                      End
                                                      Else objlancamentox.Commit;
                                            End;
                                  End;

                        End;
              End;

         End;

         mensagemAviso('Conclu�do');

     Finally
            objlancamentox.free;
     End;
end;

procedure TFExportaContabilidade.btselecionar_todos_auditoriaClick(
  Sender: TObject);
var
cont:integer;
begin
     for cont:=1 to StrgAuditoria.rowcount-1 do
     Begin
          StrgAuditoria.cells[0,cont]:='X';

     End; 
end;

procedure TFExportaContabilidade.StrgAuditoriaDblClick(Sender: TObject);
begin
     if (StrgAuditoria.cells[0,StrgAuditoria.row]='X')
     Then StrgAuditoria.cells[0,StrgAuditoria.row]:=''
     Else StrgAuditoria.cells[0,StrgAuditoria.row]:='X';
end;

procedure TFExportaContabilidade.StrgAuditoriaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then StrgAuditoriaDblClick(sender);
end;

Function TFExportaContabilidade.CarregaIni:boolean;
var
pnomecoluna,Temp,nome,valor,campos:string;
cont,posicao:integer;
Strcampos,Stropcoes:TStringList;

begin
    result:=False;
    StrgConfiguracao.FixedRows:=0;
    StrgConfiguracao.FixedCols:=0;

    StrgConfiguracao.ColCount:=1;
    StrgConfiguracao.rowcount:=1;

    StrgConfiguracao.cells[0,0]:='';

    campos:=Self.Objexportacontabilidade.RetornacamposExportacao;

    if (Campos='')
    Then exit;

    Try
       Strcampos:=TStringList.create;
       Stropcoes:=TStringList.create;
    Except
          MensagemErro('Erro na tentativa de criar a StringList "Strcampos"');
          exit; 
    End;

    Try

       ExplodeStr(campos,Strcampos,';','STRING');

       combocampos.Items.clear;
       combocampos.Items.text:=strcampos.text;
       

       if (self.ObjArquivoIni.Localizanome('CONF_EXPORTA_CONTABILIDADE')=False)
       then Begin
                 self.ObjArquivoIni.Submit_CODIGO('0');
                 self.ObjArquivoIni.Status:=dsinsert;
                 Self.ObjArquivoIni.Submit_nome('CONF_EXPORTA_CONTABILIDADE');

                 Self.ObjArquivoIni.Arquivo.Clear;

                 if (Self.ObjArquivoIni.Salvar(true)=false)
                 Then Begin
                           mensagemerro('Erro na tentativa de salvar as configura��es de campos na Tabela de INI');
                           exit;
                 End;

       End
       Else Self.ObjArquivoIni.TabelaparaObjeto;


       StrgConfiguracao.ColCount:=4;
       StrgConfiguracao.cells[0,0]:='CAMPO';
       StrgConfiguracao.cells[1,0]:='TIPO';
       StrgConfiguracao.cells[2,0]:='TAMANHO';
       StrgConfiguracao.cells[3,0]:='OPCOES';



       for cont:=0 to Self.ObjArquivoIni.Arquivo.Count-1 do
       Begin
            Stropcoes.clear;
            ExplodeStr_COMVAZIOS(Self.ObjArquivoIni.Arquivo[cont],Stropcoes,';','STRING');

            if (Stropcoes.Count>0)
            Then Begin

                      posicao:=pos('_TIPO',Stropcoes[0]);

                      if (posicao>0)
                      Then Begin
                                //pegando o nome da coluna
                                pnomecoluna:=uppercase(copy(stropcoes[0],1,posicao-1));

                                posicao:=StrgConfiguracao.Cols[0].IndexOf(pnomecoluna);

                                if (posicao<0)
                                Then Begin
                                          StrgConfiguracao.rowcount:=StrgConfiguracao.rowcount+1;
                                          posicao:=StrgConfiguracao.rowcount-1;
                                          StrgConfiguracao.Cells[0,posicao]:=pnomecoluna;
                                End;

                                StrgConfiguracao.Cells[1,posicao]:=Stropcoes[1];
                      End;

                      posicao:=pos('_TAMANHO',Stropcoes[0]);

                      if (posicao>0)
                      Then Begin
                                //pegando o nome da coluna
                                pnomecoluna:=uppercase(copy(stropcoes[0],1,posicao-1));

                                posicao:=StrgConfiguracao.Cols[0].IndexOf(pnomecoluna);

                                if (posicao<0)
                                Then Begin
                                          StrgConfiguracao.rowcount:=StrgConfiguracao.rowcount+1;
                                          posicao:=StrgConfiguracao.rowcount-1;
                                          StrgConfiguracao.Cells[0,posicao]:=pnomecoluna;
                                End;

                                StrgConfiguracao.Cells[2,posicao]:=Stropcoes[1];
                      End;
                                                         
                      posicao:=pos('_OPCOES',Stropcoes[0]);

                      if (posicao>0)
                      Then Begin
                                //pegando o nome da coluna
                                pnomecoluna:=uppercase(copy(stropcoes[0],1,posicao-1));

                                posicao:=StrgConfiguracao.Cols[0].IndexOf(pnomecoluna);

                                if (posicao<0)
                                Then Begin
                                          StrgConfiguracao.rowcount:=StrgConfiguracao.rowcount+1;
                                          posicao:=StrgConfiguracao.rowcount-1;
                                          StrgConfiguracao.Cells[0,posicao]:=pnomecoluna;
                                End;
                                StrgConfiguracao.Cells[3,posicao]:=Stropcoes[1];
                      End;
                      //*******************************************************
                 
            End;
       End;

       IF (StrgConfiguracao.ROWcount>1)
       Then StrgConfiguracao.fixedrows:=1;

       AjustaLArguraColunaGrid(StrgConfiguracao);
  Finally
         freeandnil(Strcampos);
         freeandnil(Stropcoes);
  End;

end;

procedure TFExportaContabilidade.StrgConfiguracaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
temp:string;
begin
     if (key=vk_f5)
     Then Begin
               if (StrgConfiguracao.Col=0)
               then exit;

               case StrgConfiguracao.Col of

                1:Begin
                      //tipo
                      FescolheTipoCamporelatorio.RGTIPO.Items.clear;
                      FescolheTipoCamporelatorio.RGTIPO.Items.add('Caracter');
                      FescolheTipoCamporelatorio.RGTIPO.Items.add('Data');
                      FescolheTipoCamporelatorio.RGTIPO.Items.add('Hora');
                      FescolheTipoCamporelatorio.RGTIPO.Items.add('Inteiro');
                      FescolheTipoCamporelatorio.RGTIPO.Items.add('Decimal');

                      if (StrgConfiguracao.cells[1,StrgConfiguracao.Row]='C')
                      Then Fescolhetipocamporelatorio.RGTIPO.ItemIndex:=0;

                      if (StrgConfiguracao.cells[1,StrgConfiguracao.Row]='D')
                      Then Fescolhetipocamporelatorio.RGTIPO.ItemIndex:=1;

                      if (StrgConfiguracao.cells[1,StrgConfiguracao.Row]='H')
                      Then Fescolhetipocamporelatorio.RGTIPO.ItemIndex:=2;

                      if (StrgConfiguracao.cells[1,StrgConfiguracao.Row]='I')
                      Then Fescolhetipocamporelatorio.RGTIPO.ItemIndex:=3;

                      if (StrgConfiguracao.cells[1,StrgConfiguracao.Row]='M')
                      Then Fescolhetipocamporelatorio.RGTIPO.ItemIndex:=4;

                      Fescolhetipocamporelatorio.showmodal;

                      if (Fescolhetipocamporelatorio.RGTIPO.ItemIndex=0)
                      Then StrgConfiguracao.cells[1,StrgConfiguracao.Row]:='C';

                      if (Fescolhetipocamporelatorio.RGTIPO.ItemIndex=1)
                      Then StrgConfiguracao.cells[1,StrgConfiguracao.Row]:='D';

                      if (Fescolhetipocamporelatorio.RGTIPO.ItemIndex=2)
                      Then StrgConfiguracao.cells[1,StrgConfiguracao.Row]:='H';

                      if (Fescolhetipocamporelatorio.RGTIPO.ItemIndex=3)
                      Then StrgConfiguracao.cells[1,StrgConfiguracao.Row]:='N';

                      if (Fescolhetipocamporelatorio.RGTIPO.ItemIndex=4)
                      Then StrgConfiguracao.cells[1,StrgConfiguracao.Row]:='M';

                End;

                2:Begin
                      //tamanho

                      temp:=StrgConfiguracao.cells[2,StrgConfiguracao.Row];

                      if (InputQuery('Op��es','Digite o Tamanho do campo',temp)=false)
                      then exit;

                      try
                         strtoint(temp);
                      Except
                            MensagemErro('Inteiro Inv�lido');
                            temp:='1';
                      End;

                      StrgConfiguracao.cells[2,StrgConfiguracao.Row]:=temp;
                End;

                3:Begin
                      //Opcoes
                        FopcoesListCheckBox.CHLBOpcoes.Items.clear;
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Completa com Espa�o');//0
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Completa com Zero a Esquerda');//1
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Retira virgula do Decimal');//2
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Ano com 2 digitos');//3
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Truncar no Tamanho Maximo');//4
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Retira Barra de Data');//5
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Alinhar a Direita (completa espa�o a esquerda)');//6
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Utilizar ponto como separador de decimal');//7
                        FopcoesListCheckBox.CHLBOpcoes.Items.add('Adicionar aspas "" no inicio e fim da informa��o');//8




                        temp:=StrgConfiguracao.cells[3,StrgConfiguracao.Row];

                        if (pos('COMPLETA_ESPACO',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[0]:=true;

                        if (pos('COMPLETA_ZERO_A_ESQUERDA',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[1]:=true;

                        if (pos('RETIRA_VIRGULA_DECIMAL',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[2]:=true;

                        if (pos('ANO_2_DIGITOS',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[3]:=true;

                        if (pos('TRUNCAR_TAMANHO_MAXIMO',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[4]:=true;

                        if (pos('RETIRA_BARRA_DE_DATA',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[5]:=true;

                        if (pos('ALINHA_A_DIREITO_COM_ESPACO',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[6]:=true;

                        if (pos('PONTO_SEPARA_DECIMAL',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[7]:=true;

                        if (pos('ADICIONA_ASPAS',temp)<>0)
                        then FopcoesListCheckBox.CHLBOpcoes.Checked[8]:=true;


                        FopcoesListCheckBox.showmodal;

                        temp:='|';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[0]=true)
                        then temp:=temp+'|COMPLETA_ESPACO';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[1]=true)
                        then temp:=temp+'|COMPLETA_ZERO_A_ESQUERDA';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[2]=true)
                        then temp:=temp+'|RETIRA_VIRGULA_DECIMAL';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[3]=true)
                        then temp:=temp+'|ANO_2_DIGITOS';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[4]=true)
                        then temp:=temp+'|TRUNCAR_TAMANHO_MAXIMO';


                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[5]=true)
                        then temp:=temp+'|RETIRA_BARRA_DE_DATA';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[6]=true)
                        then temp:=temp+'|ALINHA_A_DIREITO_COM_ESPACO';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[7]=true)
                        then temp:=temp+'|PONTO_SEPARA_DECIMAL';

                        if (FopcoesListCheckBox.CHLBOpcoes.Checked[8]=true)
                        then temp:=temp+'|ADICIONA_ASPAS';

                        StrgConfiguracao.cells[3,StrgConfiguracao.Row]:=TEMP;

                        AjustaLArguraColunaGrid(self.StrgConfiguracao);
                End;

               End;
     End;

     if (key=vk_delete)
     Then Begin
               if (self.StrgConfiguracao.row=0)
               Then exit;

               if (mensagempergunta('Certeza que deseja excluir essa linha')=mrno)
               Then exit;

               apagalinhagrid(self.StrgConfiguracao,self.StrgConfiguracao.row);
     End;
end;

procedure TFExportaContabilidade.Button2Click(Sender: TObject);
var
cont:integer;
begin
     if (self.ObjArquivoIni.Localizanome('CONF_EXPORTA_CONTABILIDADE')=False)
     then Begin
                 self.ObjArquivoIni.Submit_CODIGO('0');
                 self.ObjArquivoIni.Status:=dsinsert;
                 Self.ObjArquivoIni.Submit_nome('CONF_EXPORTA_CONTABILIDADE');

                 if (Self.ObjArquivoIni.Salvar(true)=false)
                 Then Begin
                           mensagemerro('Erro na tentativa de salvar as configura��es de campos na Tabela de INI');
                           exit;
                 End;

       End;

       if (self.ObjArquivoIni.Localizanome('CONF_EXPORTA_CONTABILIDADE')=False)
       then Begin
                 MensagemErro('Configura��o n�o encontrada');
                 exit;
       End;
     
       Self.ObjArquivoIni.TabelaparaObjeto;
       Self.ObjArquivoIni.Status:=dsedit;
       Self.ObjArquivoIni.Arquivo.Clear;


       for cont:=1 to StrgConfiguracao.RowCount-1 do
       Begin
            Self.ObjArquivoIni.Arquivo.add(StrgConfiguracao.cells[0,cont]+'_TIPO;'+StrgConfiguracao.cells[1,cont]);
            Self.ObjArquivoIni.Arquivo.add(StrgConfiguracao.cells[0,cont]+'_TAMANHO;'+StrgConfiguracao.cells[2,cont]);
            Self.ObjArquivoIni.Arquivo.add(StrgConfiguracao.cells[0,cont]+'_OPCOES;'+StrgConfiguracao.cells[3,cont]);
       End;

    


       if (Self.ObjArquivoIni.salvar(True)=false)
       Then Begin
                 MensagemErro('Erro na tentativa de salvar as configura��es no INI');
                 exit;
       End;

       MensagemAviso('Salvo com Sucesso');

end;

procedure TFExportaContabilidade.combocamposKeyPress(Sender: TObject;
  var Key: Char);
var
posicao:integer;
begin
     if (key=#13)
     Then Begin
               if (combocampos.itemindex<0)
               Then exit;

               posicao:=StrgConfiguracao.Cols[0].IndexOf(uppercase(combocampos.text));

               if (posicao<0)
               Then Begin
                          StrgConfiguracao.RowCount:=StrgConfiguracao.RowCount+1;
                          posicao:=StrgConfiguracao.RowCount-1;
                          StrgConfiguracao.Cells[0,posicao]:=uppercase(combocampos.text);
                          StrgConfiguracao.Cells[1,posicao]:='C';
                          StrgConfiguracao.Cells[2,posicao]:='1';
                          StrgConfiguracao.Cells[3,posicao]:=' ';
               End;

               if (StrgConfiguracao.rowcount>1)
               Then StrgConfiguracao.FixedRows:=1;

               AjustaLArguraColunaGrid(StrgConfiguracao);
               combocampos.SetFocus;
     End;
end;

procedure TFExportaContabilidade.btexportarClick(Sender: TObject);
var
Data1,data2:Tdate;
Arq:TextFile;
pquantidade,pvalor,linha,temp,saida1,saida2:string;
Qlocal:Tibquery;
Nome:String;
cont:integer;
Todos:string;
path:string;
begin
     MemoLog.Lines.Clear;
     If Objexportacontabilidade.Status<>dsinactive
     then exit;
     Path:='';
     path:=ExtractFilePath(Application.exename);
     If path[length(path)]<>'\'
     then path:=path+'\';
     path:=path+'Exporta\';
     SaveDialog.InitialDir:=path;

     IF (SaveDialog.execute)
     Then

     path:=SaveDialog.filename;
     Nome:=UPPERCASE(ExtractFileName(path));

     With FopcaoRel do
     Begin

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Exportar dados que n�o foram exportados');//0
                items.add('Exportar Todos os Dados');//1
          End;

          showmodal;
          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Begin
                  If (Messagedlg('Tem certeza?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  Then exit;

                  Todos:='N';
            End;
            1:Begin
                  If (Messagedlg('Tem certeza?',mtconfirmation,[mbyes,mbno],0)=mrno)
                  Then exit;
                  Todos:='S';
            End;
          End;
     End;
     
     Limpaedit(Ffiltroimp);
     With FfiltroImp do
     Begin
          DesativaGrupos;

          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;

          edtgrupo01.EditMask:='!99/99/9999;1;_';
          LbGrupo01.caption:='Data Inicial';

          edtgrupo02.EditMask:='!99/99/9999;1;_';
          LbGrupo02.caption:='Data Final';

          showmodal;
          If tag=0
          Then exit;

          Try
                Data1:=Strtodate(edtgrupo01.text);
          Except
                Messagedlg('Data Inicial Inv�lida!',mterror,[mbok],0);
                exit;
          End;

          Try
                Data2:=Strtodate(edtgrupo02.text);
          Except
                Messagedlg('Data Final Inv�lida!',mterror,[mbok],0);
                exit;
          End;

     End;
     
     With Self.Objexportacontabilidade.ObjQuery do
     Begin
          //eSCOLHENDO OS DADOS
          close;
          sql.clear;
          sql.add('Select * from ViewExportaContabilidade');
          sql.add('where (Data>='+#39+FormatDateTime('mm/dd/yyyy',data1)+#39+' and Data<='+#39+FormatDateTime('mm/dd/yyyy',data2)+#39+')');

          If (Todos='N')
          Then sql.add('and Exportado=''N'' ');

          sql.ADD('ORDER BY CODIGO');
          open;

          //aqui eu percorro os registros
          //e vou gravando em TXT
          //depois passo todos eles para exportado='S'
          Try
             Qlocal:=Tibquery.create(nil);
             Qlocal.database:=Fdatamodulo.ibdatabase;
          Except
                Messagedlg('Erro na Cria��o da Query Tempor�ria!',mterror,[mbok],0);
                exit;
          End;

          Try
             Assignfile(arq,path);
             rewrite(arq);
          Except
                Messagedlg('Erro na tentativa de Cria��o do Arquivo de Exporta��o : '+#13+path,mterror,[mbok],0);
                freeandnil(qlocal);
                exit;
          End;

          Try
             While not(eof) do
             Begin
                  linha:='';

                  for cont:=1 to self.StrgConfiguracao.RowCount-1 do
                  Begin
                       temp:=StrgConfiguracao.cells[3,cont];

                       //pegando o valor da view
                       pvalor:=fieldbyname(StrgConfiguracao.cells[0,cont]).asstring;
                       pquantidade:=StrgConfiguracao.cells[2,cont];

                       if (StrgConfiguracao.cells[1,cont]='M') (*decimal*)
                       Then Begin
                                 Try
                                    pvalor:=formata_valor(pvalor);
                                    pvalor:=come(pvalor,'.');
                                 Except
                                    on e:exception do
                                    Begin
                                          MemoLog.lines.add(e.message);
                                    End;
                                 End;


                                 if (pos('PONTO_SEPARA_DECIMAL',temp)<>0)
                                 then Begin
                                           if (pos(',',pvalor)>0)
                                           Then Begin
                                                     pvalor[pos(',',pvalor)]:='.';
                                           End;
                                 End;



                                 if (pos('RETIRA_VIRGULA_DECIMAL',temp)<>0)
                                 then pvalor:=come(pvalor,',');


                       End;



                       if (pos('ANO_2_DIGITOS',temp)<>0)
                       then Begin
                                 Try
                                    pvalor:=FormatDateTime('dd/mm/yy',strtodate(pvalor));
                                 Except
                                       on e:exception do
                                       Begin
                                            MemoLog.lines.add(e.message);
                                       End;
                                 End;

                       End;

                       if (pos('COMPLETA_ESPACO',temp)<>0)
                       then pvalor:=CompletaPalavra(pvalor,strtoint(pquantidade),' ');


                       if (pos('ALINHA_A_DIREITO_COM_ESPACO',temp)<>0)
                       then pvalor:=CompletaPalavra_a_Esquerda(pvalor,strtoint(pquantidade),' ');

                       if (pos('RETIRA_BARRA_DE_DATA',temp)<>0)
                       then pvalor:=come(pvalor,'/');

                       if (pos('COMPLETA_ZERO_A_ESQUERDA',temp)<>0)
                       then pvalor:=CompletaPalavra_a_Esquerda(pvalor,strtoint(pquantidade),'0');

                       if (pos('TRUNCAR_TAMANHO_MAXIMO',temp)<>0)
                       Then Begin
                                 if (length(pvalor)>strtoint(pquantidade))
                                 Then pvalor:=copy(pvalor,1,strtoint(pquantidade));
                       End;

                       if (pos('ADICIONA_ASPAS',temp)<>0)
                       then pvalor:='"'+PVALOR+'"';

                       if (linha<>'')
                       Then linha:=linha+edtcaracterseparador.text+pvalor
                       Else linha:=pvalor;
                  End;

                  Writeln(arq,linha);
                  Flush(arq);

                  Qlocal.close;
                  Qlocal.sql.clear;
                  Qlocal.sql.add('Update tabexportacontabilidade set exportado=''S'',nomedoarquivo='+#39+Nome+#39+' where codigo='+Fieldbyname('codigo').asstring);

                  Try
                    Qlocal.execsql;
                  Except
                        messagedlg('Erro na tentativa de atualizar os registros para exportado=S!'+#13+'Exporta��o Cancelada!',mterror,[mbok],0);
                        exit;
                  End;

                  next;
             End;
             Fdatamodulo.ibtransaction.CommitRetaining;
             Messagedlg('Arquivo Exportado Com Sucesso!',mtinformation,[mbok],0);
          Finally
                 FDataModulo.IBTransaction.RollbackRetaining;
                 closefile(arq);
                 freeandnil(qlocal);
          End;
     End;
end;

end.

