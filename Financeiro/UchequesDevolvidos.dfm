object FchequesDevolvidos: TFchequesDevolvidos
  Left = 728
  Top = 241
  Width = 640
  Height = 474
  Caption = 'Cheques Devolvidos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inline FrLancaChequeDevolvido1: TFrlancaChequeDevolvido
    Left = 2
    Top = 4
    Width = 622
    Height = 431
    Color = clMenu
    Ctl3D = False
    ParentColor = False
    ParentCtl3D = False
    TabOrder = 0
    inherited Panel1: TPanel
      Top = 262
    end
    inherited DbGridPesquisa: TDBGrid
      Top = 99
    end
    inherited Guia: TPageControl
      inherited TabSheet1: TTabSheet
        inherited edtportador_filtro: TEdit
          Color = 6073854
        end
        inherited BtConsultar: TButton
          OnClick = FrLancaChequeDevolvido1BtConsultarClick
        end
        inherited BtGeraContas: TButton
          OnClick = FrLancaChequeDevolvido1BtGeraContasClick
        end
      end
      inherited TabSheet2: TTabSheet
        inherited edtportador_CP: TEdit
          Color = 6073854
        end
        inherited BtGeraConta_CP: TButton
          OnClick = FrLancaChequeDevolvido1BtGeraConta_CPClick
        end
      end
    end
    inherited QueryPesquisa: TIBQuery
      Left = 350
      Top = 4
    end
    inherited DataSourcePesquisa: TDataSource
      Left = 318
      Top = 6
    end
  end
end
