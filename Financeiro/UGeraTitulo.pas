unit UGeraTitulo;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjGeraTitulo;

type
  TFGeraTitulo = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    BtCancelar: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    Btgravar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    edtgerador: TEdit;
    edtcodigogerador: TEdit;
    edtcredordevedor: TEdit;
    edtcodigocredordevedor: TEdit;
    edtprazo: TEdit;
    edtportador: TEdit;
    edtcontagerencial: TEdit;
    edtsubcontagerencial: TEdit;
    combocontafacil: TComboBox;
    edthistoricocontafacil: TEdit;
    ChMostraSubConta_ContaFacil: TCheckBox;
    ImagemFundo: TImage;
    Label13: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtgeradorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtprazoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigogeradorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtsubcontagerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtsubcontagerencialExit(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGeraTitulo: TFGeraTitulo;
  ObjGeraTitulo:TObjGeraTitulo;

implementation

uses UessencialGlobal, Upesquisa, 
  UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFGeraTitulo.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjGeraTitulo do
    Begin
         Submit_CODIGO             (edtCODIGO             .text);
         Submit_Historico          (edtHistorico          .text);

         Submit_Gerador            (edtGerador            .text);
         Submit_CodigoGerador      (edtCodigoGerador      .text);
         Submit_CredorDevedor      (edtCredorDevedor      .text);
         Submit_CodigoCredorDevedor(edtCodigoCredorDevedor.text);
         Submit_Prazo              (edtPrazo              .text);
         Submit_Portador           (edtPortador           .text);
         Submit_ContaGerencial     (edtContaGerencial     .text);
         SubContaGerencial.Submit_CODIGO(edtsubcontagerencial.Text);

         Submit_contafacil(Submit_ComboBox(combocontafacil));
         Submit_historicocontafacil(edthistoricocontafacil.text);

         if (chMostraSubConta_ContaFacil.checked=true)
         Then Submit_MostraSubConta_ContaFacil('S')
         Else Submit_MostraSubConta_ContaFacil('N');


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFGeraTitulo.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjGeraTitulo do
     Begin
        edtCODIGO             .text:=Get_CODIGO                      ;
        edtHistorico          .text:=Get_Historico                   ;
        edtGerador            .text:=Get_Gerador                     ;
        edtCodigoGerador      .text:=Get_CodigoGerador               ;
        edtCredorDevedor      .text:=Get_CredorDevedor               ;
        edtCodigoCredorDevedor.text:=Get_CodigoCredorDevedor         ;
        edtPrazo              .text:=Get_Prazo                       ;
        edtPortador           .text:=Get_Portador                    ;
        edtContaGerencial     .text:=Get_ContaGerencial              ;
        edtsubcontagerencial.Text:=SubContaGerencial.Get_CODIGO;
        edthistoricocontafacil.Text:=Get_HistoricoContaFacil;

        if (Get_MostraSubConta_ContaFacil='S')
        Then ChMostraSubConta_ContaFacil.Checked:=true
        Else ChMostraSubConta_ContaFacil.Checked:=false;

        if (get_contafacil='S')
        Then combocontafacil.ItemIndex:=1
        Else combocontafacil.ItemIndex:=0;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFGeraTitulo.TabelaParaControles: Boolean;
begin
     ObjGeraTitulo.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFGeraTitulo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjGeraTitulo=Nil)
     Then exit;

    If (ObjGeraTitulo.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjGeraTitulo.free;
end;

procedure TFGeraTitulo.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFGeraTitulo.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFGeraTitulo.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjGeraTitulo.Get_novocodigo;
     edtcodigo.enabled:=False;
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.Visible:=True;

     ObjGeraTitulo.status:=dsInsert;
     combocontafacil.setfocus;
end;

procedure TFGeraTitulo.BtCancelarClick(Sender: TObject);
begin
     ObjGeraTitulo.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFGeraTitulo.BtgravarClick(Sender: TObject);
begin

     If ObjGeraTitulo.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjGeraTitulo.salvar(true)=False)
     Then exit;

     mostra_botoes(Self);
     //limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     //Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFGeraTitulo.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     esconde_botoes(Self);
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.Visible:=True;

     ObjGeraTitulo.Status:=dsEdit;
     combocontafacil.setfocus;
end;

procedure TFGeraTitulo.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjGeraTitulo.Get_pesquisa,ObjGeraTitulo.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjGeraTitulo.status<>dsinactive
                                  then exit;

                                  If (ObjGeraTitulo.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGeraTitulo.btalterarClick(Sender: TObject);
begin
    If (ObjGeraTitulo.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFGeraTitulo.btexcluirClick(Sender: TObject);
begin
     If (ObjGeraTitulo.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjGeraTitulo.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjGeraTitulo.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFGeraTitulo.edtgeradorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjGeraTitulo.edtgeradorKeyDown(sender,key,shift);
end;

procedure TFGeraTitulo.edtcredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjGeraTitulo.edtcredordevedorKeyDown(sender,key,shift);
end;

procedure TFGeraTitulo.edtprazoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    ObjGeraTitulo.edtprazoKeyDown(sender,key,shift);
end;

procedure TFGeraTitulo.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ObjGeraTitulo.edtportadorKeyDown(sender,key,shift);
end;

procedure TFGeraTitulo.edtcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     objgeratitulo.edtcontagerencialKeyDown(sender,key,shift);
end;

procedure TFGeraTitulo.edtcodigogeradorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Objgeratitulo.edtcodigogeradorKeyDown(edtgerador.text,sender,key,shift);
end;
procedure TFGeraTitulo.edtcodigocredordevedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjGeraTitulo.edtcodigocredordevedorKeyDown(edtcredordevedor.Text,sender,key,shift);
end;

procedure TFGeraTitulo.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjGeraTitulo:=TObjGeraTitulo.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
     
end;

procedure TFGeraTitulo.edtsubcontagerencialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjGeraTitulo.edtsubcontagerencialKeyDown(sender,key,shift,nil,edtcontagerencial.Text);
end;

procedure TFGeraTitulo.edtsubcontagerencialExit(Sender: TObject);
begin
     ObjGeraTitulo.EdtSubcontagerencialExit(sender,nil);
end;

function TFGeraTitulo.atualizaQuantidade: string;
begin
    result:='Existem '+ContaRegistros('TABGERATITULO', 'CODIGO')+' Geradores de T�tulo Cadastrados';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjGeraTitulo.Get_PesquisaGeraTitulo,ObjGeraTitulo.Get_TituloPesquisaGeraTitulo,Self)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If Self.edtTipoLancto.text<>''
                                 Then Self.LbTipoLanctoNome.caption:=OBjLancamento.PegaNometipolancto(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;
}
