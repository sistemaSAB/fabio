object FBaixaEmCheques: TFBaixaEmCheques
  Left = 463
  Top = 162
  Width = 920
  Height = 643
  BorderIcons = [biSystemMenu]
  Caption = 'Dar Baixa em Cheques - Exclaim Tecnologia'
  Color = 10643006
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -16
  Font.Name = 'Courier'
  Font.Style = [fsBold]
  OldCreateOrder = False
  DesignSize = (
    904
    605)
  PixelsPerInch = 96
  TextHeight = 16
  object LbCodigo: TLabel
    Left = 8
    Top = 64
    Width = 208
    Height = 13
    Caption = 'N'#250'mero                   :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Courier'
    Font.Style = []
    ParentFont = False
  end
  object LbNome: TLabel
    Left = 8
    Top = 104
    Width = 208
    Height = 13
    Caption = 'Cliente                  :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Courier'
    Font.Style = []
    ParentFont = False
  end
  object LbPortador: TLabel
    Left = 8
    Top = 128
    Width = 208
    Height = 13
    Caption = 'Portador                 :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Courier'
    Font.Style = []
    ParentFont = False
  end
  object LbValor: TLabel
    Left = 8
    Top = 176
    Width = 208
    Height = 13
    Caption = 'Valor                    :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Courier'
    Font.Style = []
    ParentFont = False
  end
  object LbCliente: TLabel
    Left = 273
    Top = 104
    Width = 54
    Height = 16
    Caption = 'LbCliente'
    Color = 10643006
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object LbPortador1: TLabel
    Left = 273
    Top = 128
    Width = 70
    Height = 16
    Caption = 'LbPortador1'
    Color = 10643006
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object LbValor1: TLabel
    Left = 273
    Top = 176
    Width = 47
    Height = 16
    Caption = 'Lbvalor1'
    Color = 10643006
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object lblVenc: TLabel
    Left = 8
    Top = 86
    Width = 208
    Height = 13
    Caption = 'Vencimento               :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Courier'
    Font.Style = []
    ParentFont = False
  end
  object LbNumCheq2: TLabel
    Left = 273
    Top = 64
    Width = 43
    Height = 16
    Caption = 'numero'
    Color = 10643006
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object LbTipoCheque: TLabel
    Left = 8
    Top = 152
    Width = 208
    Height = 13
    Caption = 'Tipo de cheque           :'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Courier'
    Font.Style = []
    ParentFont = False
  end
  object LbTipocHEQ: TLabel
    Left = 273
    Top = 152
    Width = 73
    Height = 16
    Caption = 'LbTipocHEQ'
    Color = 10643006
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object lbdatvenc: TLabel
    Left = 273
    Top = 86
    Width = 46
    Height = 16
    Caption = 'venc     '
    Color = 10643006
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object lblnomPort: TLabel
    Left = 416
    Top = 128
    Width = 70
    Height = 16
    Caption = 'LbPortador1'
    Color = 10643006
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object bvl1: TBevel
    Left = 0
    Top = 209
    Width = 905
    Height = 33
    Anchors = [akLeft, akTop, akRight]
  end
  object lbl1: TLabel
    Left = -2
    Top = 3
    Width = 899
    Height = 36
    Caption = 
      'Obs: Dar baixa somente nos cheques que j'#225' foram compensandos, te' +
      'ndo em vista que o valor dos cheques baixados come'#231'ar'#227'o '#13#10'a cont' +
      'ar como j'#225' recebido, portanto, cheques baixados incorretamente p' +
      'oder'#227'o dar diferen'#231'a no total recebido e pago no m'#234's. '
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object lbl2: TLabel
    Left = 8
    Top = 216
    Width = 49
    Height = 16
    Caption = 'Portador'
    Font.Charset = ANSI_CHARSET
    Font.Color = clLime
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object lblobs: TLabel
    Left = 8
    Top = 497
    Width = 532
    Height = 16
    Caption = 
      'Obs: Cheques compensados ser'#227'o convertidos em dinheiro no portad' +
      'or em que se encontra'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object bvl2: TBevel
    Left = 0
    Top = 481
    Width = 909
    Height = 41
    Anchors = [akLeft, akTop, akRight]
  end
  object pnl2: TPanel
    Left = 0
    Top = 249
    Width = 902
    Height = 233
    Anchors = [akLeft, akTop, akRight]
    Color = 14024703
    TabOrder = 3
    object lblBaixarCheque: TLabel
      Left = 726
      Top = 194
      Width = 163
      Height = 24
      Cursor = crHandPoint
      Caption = 'Baixar Cheque(s)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -20
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = lblBaixarChequeClick
    end
    object pnl3: TPanel
      Left = 8
      Top = 184
      Width = 545
      Height = 33
      BevelOuter = bvNone
      Color = 14024703
      TabOrder = 1
      object edtpesquisa: TMaskEdit
        Left = 8
        Top = 0
        Width = 201
        Height = 24
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Courier'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnKeyDown = edtpesquisaKeyDown
        OnKeyPress = edtpesquisaKeyPress
      end
    end
    object StrGridBaixa1: TStringGrid
      Left = 8
      Top = 8
      Width = 889
      Height = 169
      Ctl3D = False
      DefaultColWidth = 0
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnClick = StrGridBaixa1Click
      OnDblClick = StrGridBaixa1dblClick
      OnDrawCell = StrGridBaixa1DrawCell
      OnKeyDown = StrGridBaixa1KeyDown
    end
  end
  object PanPanel2: TPanel
    Left = 0
    Top = 0
    Width = 904
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    Caption = 'Indique os cheques  compensados'
    Color = clMenu
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object btnSair: TBitBtn
      Left = 832
      Top = 8
      Width = 41
      Height = 41
      TabOrder = 0
      OnClick = btnSairClick
    end
  end
  object rbPagos: TRadioButton
    Left = 465
    Top = 217
    Width = 177
    Height = 17
    Caption = 'Cheques Emitidos'
    TabOrder = 1
    OnClick = rbPagosClick
  end
  object rbRecebidos: TRadioButton
    Left = 265
    Top = 217
    Width = 193
    Height = 17
    BiDiMode = bdLeftToRight
    Caption = 'Cheques Recebidos'
    Color = 10643006
    ParentBiDiMode = False
    ParentColor = False
    TabOrder = 2
    OnClick = rbRecebidosClick
  end
  object pnl1: TPanel
    Left = 0
    Top = 524
    Width = 904
    Height = 81
    Align = alBottom
    TabOrder = 4
    object lblQuantsel: TLabel
      Left = 464
      Top = 10
      Width = 264
      Height = 24
      Caption = 'Quantidade Selecionada  :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblquant: TLabel
      Left = 792
      Top = 4
      Width = 63
      Height = 37
      Caption = '       '
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -32
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblval: TLabel
      Left = 464
      Top = 42
      Width = 245
      Height = 24
      Caption = 'Valor Total selecionado :'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -21
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblvalsel: TLabel
      Left = 792
      Top = 40
      Width = 45
      Height = 37
      Caption = '     '
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -32
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object edtpesquisaport: TEdit
    Left = 96
    Top = 216
    Width = 89
    Height = 22
    Color = 6073854
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 5
    OnDblClick = edtpesquisaportDblClick
    OnKeyDown = edtpesquisaportKeyDown
    OnKeyPress = edtpesquisaportKeyPress
  end
  object dscheques: TDataSource
    Left = 832
    Top = 289
  end
end
