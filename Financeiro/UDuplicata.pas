unit UDuplicata;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjDuplicata,
  jpeg;

type
  TFduplicata = class(TForm)
    Guia: TTabbedNotebook;
    EdtCodigo: TEdit;
    Label1: TLabel;
    edthistorico: TEdit;
    Label2: TLabel;
    Panel1: TPanel;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    Btnovo: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btpesquisar: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fduplicata: TFduplicata;
  ObjDuplicata:TObjDuplicata;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFduplicata.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjDuplicata do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);

         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFduplicata.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjDuplicata do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFduplicata.TabelaParaControles: Boolean;
begin
     If (ObjDuplicata.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFduplicata.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjDuplicata=Nil)
     Then exit;

    If (ObjDuplicata.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjDuplicata.free;
end;

procedure TFduplicata.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFduplicata.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjDuplicata.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjDuplicata.status:=dsInsert;
     Guia.pageindex:=0;
     EdtHistorico.setfocus;

end;


procedure TFduplicata.btalterarClick(Sender: TObject);
begin
    If (ObjDuplicata.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjDuplicata.Status:=dsEdit;
                guia.pageindex:=0;
                edtHistorico.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;


end;

procedure TFduplicata.btgravarClick(Sender: TObject);
begin

     If ObjDuplicata.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjDuplicata.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFduplicata.btexcluirClick(Sender: TObject);
begin
     If (ObjDuplicata.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjDuplicata.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjDuplicata.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFduplicata.btcancelarClick(Sender: TObject);
begin
     ObjDuplicata.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFduplicata.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFduplicata.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjDuplicata.Get_pesquisa,ObjDuplicata.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjDuplicata.status<>dsinactive
                                  then exit;

                                  If (ObjDuplicata.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjDuplicata.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFduplicata.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        ObjDuplicata:=TObjDuplicata.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjDuplicata.OBJETO.Get_Pesquisa,ObjDuplicata.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(OBJDuplicata.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If OBJDuplicata.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(OBJDuplicata.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
