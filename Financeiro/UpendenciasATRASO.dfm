object FpendenciasAtraso: TFpendenciasAtraso
  Left = 192
  Top = 114
  Width = 544
  Height = 375
  Caption = 'Pend'#234'ncias em Atraso'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 309
    Top = 24
    Width = 111
    Height = 13
    Caption = 'Total em Aberto R$'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbtotalpendencias: TLabel
    Left = 422
    Top = 24
    Width = 105
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = 'Label1'
  end
  object lbcliente: TLabel
    Left = 117
    Top = 9
    Width = 111
    Height = 13
    Caption = 'Total em Aberto R$'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Grid: TStringGrid
    Left = 7
    Top = 51
    Width = 520
    Height = 281
    FixedCols = 0
    TabOrder = 0
  end
  object BtImprime: TBitBtn
    Left = 8
    Top = 8
    Width = 103
    Height = 37
    Caption = 'Imprimir'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BtImprimeClick
  end
end
