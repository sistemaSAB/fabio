unit UChequesPortador;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjChequesPortador;

type
  TFchequesPortador = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    BtNovo: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label22: TLabel;
    Label13: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    edtportador: TEdit;
    edtvalor: TEdit;
    edtvencimento: TMaskEdit;
    edtChequedoPortador: TEdit;
    EdtCodigo: TEdit;
    EdtNumeroChequeTemp: TEdit;
    ComboTemp: TComboBox;
    edtmotivodevolucao: TEdit;
    Panel2: TPanel;
    Label17: TLabel;
    Label16: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    edtcodigodebarras: TEdit;
    edtcpfcliente2: TEdit;
    edtcliente2: TEdit;
    edtCliente1: TEdit;
    edtcpfcliente1: TEdit;
    edtcomp: TEdit;
    edtbanco: TEdit;
    edtagencia: TEdit;
    edtdv: TEdit;
    edtc1: TEdit;
    edtconta: TEdit;
    edtc2: TEdit;
    edtserie: TEdit;
    edtnumcheque: TEdit;
    edtc3: TEdit;
    ImagemFundo: TImage;
    Shape1: TShape;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure ComboTempExit(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         function  AtualizaQuantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FchequesPortador: TFchequesPortador;
  ObjChequesPortador:TObjChequesPortador;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFchequesPortador.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjChequesPortador do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Portador        ( edtportador.text);
         Submit_Valor           ( edtvalor.text);
         Submit_Vencimento      (edtVencimento.text);
         Submit_Comp            (edtComp.text);
         Submit_Banco           (edtBanco.text);
         Submit_Agencia         (edtAgencia.text);
         Submit_C1              (edtC1.text);
         Submit_Conta           (edtConta.text);
         Submit_C2              (edtC2.text);
         Submit_Serie           (edtSerie.text);
         Submit_NumCheque       (edtNumCheque.text);
         Submit_C3              (edtC3.text);
         Submit_Cliente1        (edtCliente1.text);
         Submit_CPFCliente1     (edtCPFCliente1.text);
         Submit_Cliente2        (edtCliente2.text);
         Submit_CPFCliente2     (edtCPFCliente2.text);
         Submit_CodigodeBarras  (edtCodigodeBarras.text);
         Submit_Chequedoportador(edtChequedoPortador.text);
         Submit_Temp            (ComboTemp.Text[1]);
         Submit_MotivoDevolucao (edtmotivodevolucao.text);


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFchequesPortador.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjChequesPortador do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtPortador.text        :=Get_portador        ;
        edtvalor.text           :=Get_Valor           ;
        edtVencimento.text      :=Get_Vencimento        ;
        edtComp.text            :=Get_Comp              ;
        edtBanco.text           :=Get_Banco             ;
        edtAgencia.text         :=Get_Agencia           ;
        edtC1.text              :=Get_C1                ;
        edtConta.text           :=Get_Conta             ;
        edtC2.text              :=Get_C2                ;
        edtSerie.text           :=Get_Serie             ;
        edtNumCheque.text       :=Get_NumCheque         ;
        edtC3.text              :=Get_C3                ;
        edtCliente1.text        :=Get_Cliente1          ;
        edtCPFCliente1.text     :=Get_CPFCliente1       ;
        edtCliente2.text        :=Get_Cliente2          ;
        edtCPFCliente2.text     :=Get_CPFCliente2       ;
        edtCodigodeBarras.text  :=Get_CodigodeBarras    ;
        edtChequedoPortador.text:=Get_chequedoportador  ;
        edtmotivodevolucao.text :=Get_motivodevolucao   ;

        if (get_Temp = 'S')
        then ComboTemp.Text:='SIM'
        else ComboTemp.Text:='N�O';
        
        EdtNumeroChequeTemp.Text:=Get_NumCheque;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFchequesPortador.TabelaParaControles: Boolean;
begin
     ObjChequesPortador.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFchequesPortador.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjChequesPortador=Nil)
     Then exit;

    If (ObjChequesPortador.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjChequesPortador.free;
end;

procedure TFchequesPortador.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFchequesPortador.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFchequesPortador.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='';
     edtcodigo.text:=Objchequesportador.get_novocodigo;
     edtcodigo.enabled:=False;
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.Visible:=True;
     edtportador.setfocus;

     ObjChequesPortador.status:=dsInsert;
end;

procedure TFchequesPortador.BtCancelarClick(Sender: TObject);
begin
     ObjChequesPortador.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFchequesPortador.BtgravarClick(Sender: TObject);
begin

     If ObjChequesPortador.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjChequesPortador.salvar(True,true,true,'')=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFchequesPortador.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.Visible:=True;

     EdtCodigo.enabled:=False;
     edtvalor.enabled:=False;
     edtmotivodevolucao.enabled:=False;

     ObjChequesPortador.Status:=dsEdit;
     edtportador.setfocus;

end;

procedure TFchequesPortador.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjChequesPortador.Get_pesquisa,ObjChequesPortador.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjChequesPortador.status<>dsinactive
                                  then exit;

                                  If (ObjChequesPortador.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFchequesPortador.btalterarClick(Sender: TObject);
begin
    If (ObjChequesPortador.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFchequesPortador.btexcluirClick(Sender: TObject);
begin
     If (ObjChequesPortador.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjChequesPortador.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     //O True significa que pode ser dado um Commit ap�s excluir
     If (ObjChequesPortador.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFchequesPortador.edtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(ObjChequesPortador.Get_PesquisaPortador,ObjChequesPortador.get_titulopesquisaPortador,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFchequesPortador.edtvalorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not (key in['0'..'9',#8,','])
     Then Begin
               If key='.'
               Then key:=','
               Else key:=#0;
          End;
end;

procedure TFchequesPortador.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(self);

     Try
        ObjChequesPortador:=TObjChequesPortador.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;

     lbquantidade.caption:=atualizaQuantidade;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE CHEQUES DO PORTADOR')=False)
     Then Begin
                esconde_botoes(Self);
                exit;
     End;
end;

procedure TFchequesPortador.ComboTempExit(Sender: TObject);
begin
     edtnumcheque.setfocus;
end;

function TFchequesPortador.AtualizaQuantidade: string;
begin
   result:='Existem '+ContaRegistros('tabchequesportador','codigo')+' Cheques Portador Cadastrados';
end;

end.
