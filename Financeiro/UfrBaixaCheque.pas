unit UfrBaixaCheque;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Mask, ExtCtrls,UObjTalaodeCheques;

type
  TFrBaixaCheque = class(TFrame)
    Panel1: TPanel;
    Label3: TLabel;
    Lbportador: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    edtportador: TEdit;
    edtdata: TMaskEdit;
    edtcheque: TEdit;
    Btbaixa: TBitBtn;
    btestornabaixa: TBitBtn;
    procedure edtchequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtchequeKeyPress(Sender: TObject; var Key: Char);
    procedure BtbaixaClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorExit(Sender: TObject);
    procedure edtportadorKeyPress(Sender: TObject; var Key: Char);
    procedure btestornabaixaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    Function Cria:Boolean;
    Function Destroi:boolean;
  end;
var
ObjTalaodeCheques:Tobjtalaodecheques;
implementation

uses Upesquisa, Uportador, UescolheImagemBotao;



{$R *.DFM}

{ TFrBaixaCheque }

function TFrBaixaCheque.Cria: Boolean;
begin
     Try
        ObjTalaodeCheques:=Tobjtalaodecheques.create;
        edtcheque.text:='';
        edtdata.text:='';
        edtportador.text:='';
        Lbportador.caption:='';
        //edtportador.SetFocus;
     Except
           Messagedlg('Erro na Tentativa de Criar o Objeto de Tal�o de Cheques!',mterror,[mbok],0);
           Result:=False;
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoes(nil,nil,nil,Btbaixa,nil,nil,nil,nil);


end;

function TFrBaixaCheque.Destroi: boolean;
begin
     If ObjTalaodeCheques<>nil
     Then objtalaodecheques.free;
end;

procedure TFrBaixaCheque.edtchequeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   complemento:string;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FrBaixaCheque.edtcheque';
            complemento:='';
            if (edtportador.text<>'')
            Then complemento:=' and TabTalaodeCheques.portador='+edtportador.text;
            
            If (FpesquisaLocal.PreparaPesquisa(ObjtalaodeCheques.Get_PesquisaUsadosNoPortadorOriginal+complemento,ObjTalaodeCheques.Get_TituloPesquisaUSados,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtcheque.text:=FpesquisaLocal.QueryPesq.fieldbyname('numero').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;



end;

procedure TFrBaixaCheque.edtchequeKeyPress(Sender: TObject; var Key: Char);
begin
     If not(key in ['0'..'9',#8])
     Then key:=#0;

end;

procedure TFrBaixaCheque.BtbaixaClick(Sender: TObject);
begin
     ObjTalaodeCheques.baixaCheque(Edtcheque.text,edtdata.text,edtportador.text);
     edtcheque.text:='';
     edtcheque.setfocus;

end;

procedure TFrBaixaCheque.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador:=TFportador.create(nil);
            FpesquisaLocal.NomeCadastroPersonalizacao:='FrBaixaCheque.edtportador';
            If (FpesquisaLocal.PreparaPesquisa(ObjtalaodeCheques.Get_PesquisaPortador,Objtalaodecheques.get_titulopesquisaportador,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 edtportador.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 Lbportador.caption:=FpesquisaLocal.QueryPesq.fieldbyname('Nome').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;

end;

procedure TFrBaixaCheque.edtportadorExit(Sender: TObject);
begin
     If (Edtportador.text='')
     Then lbportador.caption:='';

end;

procedure TFrBaixaCheque.edtportadorKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not(key in['0'..'9',#8])
     Then key:=#0;

end;

procedure TFrBaixaCheque.btestornabaixaClick(Sender: TObject);
begin
     repeat
            if (ObjTalaodeCheques.ExtornaBaixa=False)
            then exit;
     Until(False);
end;

end.
