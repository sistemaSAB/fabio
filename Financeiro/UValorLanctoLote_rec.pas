unit UValorLanctoLote_rec;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  comctrls,Dialogs, UfrValorLanctoLote, StdCtrls, Buttons, Mask;

type
  TFvalorLanctoLote_REC = class(TForm)
    BTOK: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    FrValorLanctoLote: TFrValorLanctoLote;
    edtvalor: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure FrValorLanctoLoteedtdatalancamentoExit(Sender: TObject);
    procedure FrValorLanctoLoteedtcarenciaExit(Sender: TObject);
    procedure FrValorLanctoLoteUpDownChangingEx(Sender: TObject;
      var AllowChange: Boolean; NewValue: Smallint;
      Direction: TUpDownDirection);
    procedure FrValorLanctoLoteUpDownChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure BTOKClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure edtvalorKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FrValorLanctoLoteedtcarenciaKeyPress(Sender: TObject;
      var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FvalorLanctoLote_REC: TFvalorLanctoLote_REC;

implementation

{$R *.dfm}

procedure TFvalorLanctoLote_REC.FormShow(Sender: TObject);
begin

     if (FrValorLanctoLote.Visible=True)
     Then Begin
                EDTVALOR.text:='';
                FrValorLanctoLote.AcertapelaCarencia(edtvalor);
     End;
     
     edtvalor.setfocus;
     Self.Tag:=0;
end;

procedure TFvalorLanctoLote_REC.FrValorLanctoLoteedtdatalancamentoExit(
  Sender: TObject);
begin
     FrValorLanctoLote.AcertapelaCarencia(edtvalor);
end;

procedure TFvalorLanctoLote_REC.FrValorLanctoLoteedtcarenciaExit(
  Sender: TObject);
begin
  FrValorLanctoLote.edtcarenciaExit(Sender);
  FrValorLanctoLote.AcertapelaCarencia(edtvalor);

end;

procedure TFvalorLanctoLote_REC.FrValorLanctoLoteUpDownChangingEx(
  Sender: TObject; var AllowChange: Boolean; NewValue: Smallint;
  Direction: TUpDownDirection);
begin
  FrValorLanctoLote.UpDownChangingEx(Sender, AllowChange, NewValue,
  Direction);
  FrValorLanctoLote.AcertapelaCarencia(edtvalor);

end;

procedure TFvalorLanctoLote_REC.FrValorLanctoLoteUpDownChanging(
  Sender: TObject; var AllowChange: Boolean);
begin
     FrValorLanctoLote.AcertapelaCarencia(edtvalor);
end;

procedure TFvalorLanctoLote_REC.BTOKClick(Sender: TObject);
begin
     Self.tag:=1;
     Self.Close;
end;

procedure TFvalorLanctoLote_REC.BitBtn2Click(Sender: TObject);
begin
     Self.Tag:=0;
     Self.Close;
end;

procedure TFvalorLanctoLote_REC.edtvalorKeyPress(Sender: TObject;
  var Key: Char);
begin

     if key=#13
     then BTOKClick(sender);

     if not (Key in ['0'..'9',',',#8])
     then
        if key='.'
        Then key:=','
        Else Key:=#0;
end;

procedure TFvalorLanctoLote_REC.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFvalorLanctoLote_REC.FrValorLanctoLoteedtcarenciaKeyPress(
  Sender: TObject; var Key: Char);
begin
  FrValorLanctoLote.edtcarenciaKeyPress(Sender, Key);

end;

end.
