unit UObjRecibo;
Interface
uses UessencialGlobal;
Type
   TObjrecibo=class

          Public
               Function  Get_NovoCodigo:string;

         Private

   End;


implementation
uses UDatamodulo,Ibstoredproc,Dialogs,Sysutils;

function TObjrecibo.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=FDataModulo.IbDatabase;
           StrTemp.StoredProcName:='PROC_GERA_RECIBO' ;

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o recibo',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


end.
