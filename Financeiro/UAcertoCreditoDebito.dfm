object FAcertoCreditoDebito: TFAcertoCreditoDebito
  Left = 186
  Top = 64
  Width = 817
  Height = 575
  Caption = 
    'ACERTO DE CONTAS A RECEBER E A PAGAR POR CREDOR/DEVEDOR - Exclai' +
    'm Tecnologia'
  Color = 13160661
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 397
    Top = 76
    Width = 401
    Height = 405
    Shape = bsFrame
    Style = bsRaised
  end
  object Bevel8: TBevel
    Left = 0
    Top = 76
    Width = 395
    Height = 405
    Shape = bsFrame
    Style = bsRaised
  end
  object Label7: TLabel
    Left = 6
    Top = 68
    Width = 168
    Height = 18
    Alignment = taCenter
    AutoSize = False
    Caption = 'T'#205'TULOS A PAGAR'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 404
    Top = 68
    Width = 188
    Height = 18
    Alignment = taCenter
    AutoSize = False
    Caption = 'T'#205'TULOS A RECEBER'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object StrGridPAGAR: TStringGrid
    Left = 3
    Top = 113
    Width = 388
    Height = 331
    Color = clInfoBk
    ColCount = 7
    TabOrder = 0
    OnDblClick = StrGridPAGARDblClick
    OnKeyPress = StrGridPAGARKeyPress
  end
  object StrGRIDRECEBER: TStringGrid
    Left = 401
    Top = 113
    Width = 393
    Height = 331
    Color = clInfoBk
    ColCount = 7
    TabOrder = 1
    OnDblClick = StrGRIDRECEBERDblClick
    OnKeyPress = StrGRIDRECEBERKeyPress
    RowHeights = (
      24
      24
      24
      24
      24)
  end
  object btprocessar: TBitBtn
    Left = 696
    Top = 496
    Width = 98
    Height = 40
    Caption = 'Processar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = btprocessarClick
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 48
    Align = alTop
    Color = 3355443
    TabOrder = 3
    object Label2: TLabel
      Left = 9
      Top = 7
      Width = 66
      Height = 14
      Caption = 'Vencimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 90
      Top = 25
      Width = 12
      Height = 14
      Caption = ' a '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 192
      Top = 7
      Width = 88
      Height = 14
      Caption = 'Credor/Devedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 346
      Top = 7
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbNomeCredorDevedor: TLabel
      Left = 410
      Top = 25
      Width = 99
      Height = 14
      Caption = 'LBCredorDevedor'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtvencimento1: TMaskEdit
      Left = 8
      Top = 23
      Width = 69
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object edtvencimento2: TMaskEdit
      Left = 104
      Top = 23
      Width = 69
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object combocredordevedor: TComboBox
      Left = 194
      Top = 22
      Width = 143
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 2
      OnChange = combocredordevedorChange
      Items.Strings = (
        'Clientes'
        'Fornecedores'
        'Alunos')
    end
    object combocredordevedorcodigo: TComboBox
      Left = 269
      Top = 22
      Width = 69
      Height = 21
      BevelKind = bkFlat
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 3
      Visible = False
    end
    object edtcodigocredordevedor: TEdit
      Left = 342
      Top = 23
      Width = 39
      Height = 19
      TabOrder = 4
      OnExit = edtcodigocredordevedorExit
      OnKeyDown = edtcodigocredordevedorKeyDown
    end
    object Btpesquisar: TBitBtn
      Left = 700
      Top = 8
      Width = 98
      Height = 37
      Caption = 'Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = BtpesquisarClick
    end
  end
  object CheckSelecionaPagar: TCheckBox
    Left = 6
    Top = 96
    Width = 139
    Height = 17
    Caption = 'Selecionar Todos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = CheckSelecionaPagarClick
  end
  object Panel2: TPanel
    Left = 2
    Top = 444
    Width = 392
    Height = 35
    Color = 3355443
    TabOrder = 5
    object Label5: TLabel
      Left = 159
      Top = 10
      Width = 124
      Height = 19
      Caption = 'Total a Pagar R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbtotalapagar: TLabel
      Left = 348
      Top = 10
      Width = 27
      Height = 19
      Alignment = taRightJustify
      Caption = '000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object Panel3: TPanel
    Left = 399
    Top = 444
    Width = 397
    Height = 35
    Caption = 'Panel1'
    Color = 3355443
    TabOrder = 6
    object Label6: TLabel
      Left = 148
      Top = 11
      Width = 143
      Height = 19
      Caption = 'Total a Receber R$'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbtotalareceber: TLabel
      Left = 364
      Top = 11
      Width = 27
      Height = 19
      Alignment = taRightJustify
      Caption = '000'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object CheckSelecionaReceber: TCheckBox
    Left = 402
    Top = 96
    Width = 139
    Height = 17
    Caption = 'Selecionar Todos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
    OnClick = CheckSelecionaReceberClick
  end
  object IBQuery: TIBQuery
    Left = 760
    Top = 56
  end
end
