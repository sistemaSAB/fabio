unit USUBCONTAGERENCIAL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjSUBCONTAGERENCIAL;

type
  TFSUBCONTAGERENCIAL = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    Panel1: TPanel;
    LbCODIGO: TLabel;
    LbContaGerencial: TLabel;
    LbNomeContaGerencial: TLabel;
    LbNome: TLabel;
    LbMascara: TLabel;
    LbCODIGOPLANODECONTAS: TLabel;
    LbNomePlanodeContas: TLabel;
    EdtCODIGO: TEdit;
    EdtContaGerencial: TEdit;
    EdtNome: TEdit;
    EdtCODIGOPLANODECONTAS: TEdit;
    edtmascara: TMaskEdit;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
//DECLARA COMPONENTES
    procedure edtContaGerencialKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtContaGerencialExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtCODIGOPLANODECONTASExit(Sender: TObject);
    procedure EdtCODIGOPLANODECONTASKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         ObjSUBCONTAGERENCIAL:TObjSUBCONTAGERENCIAL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSUBCONTAGERENCIAL: TFSUBCONTAGERENCIAL;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFSUBCONTAGERENCIAL.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjSUBCONTAGERENCIAL do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        ContaGerencial.Submit_codigo(edtContaGerencial.text);
        Submit_Nome(edtNome.text);
        Submit_Mascara(edtMascara.text);
        Submit_CODIGOPLANODECONTAS(edtCODIGOPLANODECONTAS.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFSUBCONTAGERENCIAL.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjSUBCONTAGERENCIAL do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtContaGerencial.text:=ContaGerencial.Get_codigo;
        LbNomeContaGerencial.caption:=ContaGerencial.get_nome;
        EdtNome.text:=Get_Nome;
        EdtMascara.text:=Get_Mascara;
        EdtCODIGOPLANODECONTAS.text:=Get_CODIGOPLANODECONTAS;
        Self.EdtCODIGOPLANODECONTASExit(Self.EdtCODIGOPLANODECONTAS);
        
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFSUBCONTAGERENCIAL.TabelaParaControles: Boolean;
begin
     If (Self.ObjSUBCONTAGERENCIAL.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFSUBCONTAGERENCIAL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjSUBCONTAGERENCIAL=Nil)
     Then exit;

    If (Self.ObjSUBCONTAGERENCIAL.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
            exit;
     End;

    Self.ObjSUBCONTAGERENCIAL.free;
end;

procedure TFSUBCONTAGERENCIAL.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFSUBCONTAGERENCIAL.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjSUBCONTAGERENCIAL.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjSUBCONTAGERENCIAL.status:=dsInsert;
     edtContaGerencial.setfocus;

end;


procedure TFSUBCONTAGERENCIAL.btalterarClick(Sender: TObject);
begin
    If (Self.ObjSUBCONTAGERENCIAL.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                EdtContaGerencial.Enabled:=False;
                Self.ObjSUBCONTAGERENCIAL.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtnome.setfocus;
                
    End;
end;

procedure TFSUBCONTAGERENCIAL.btgravarClick(Sender: TObject);
begin

     If Self.ObjSUBCONTAGERENCIAL.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjSUBCONTAGERENCIAL.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjSUBCONTAGERENCIAL.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFSUBCONTAGERENCIAL.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjSUBCONTAGERENCIAL.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjSUBCONTAGERENCIAL.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjSUBCONTAGERENCIAL.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFSUBCONTAGERENCIAL.btcancelarClick(Sender: TObject);
begin
     Self.ObjSUBCONTAGERENCIAL.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFSUBCONTAGERENCIAL.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFSUBCONTAGERENCIAL.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjSUBCONTAGERENCIAL.Get_pesquisa,Self.ObjSUBCONTAGERENCIAL.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjSUBCONTAGERENCIAL.status<>dsinactive
                                  then exit;

                                  If (Self.ObjSUBCONTAGERENCIAL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjSUBCONTAGERENCIAL.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFSUBCONTAGERENCIAL.LimpaLabels;
begin
//LIMPA LABELS
    LbNomeContaGerencial.caption:='';
    LbNomePlanodeContas.caption:='';
end;

procedure TFSUBCONTAGERENCIAL.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     if (ObjParametroGlobal.ValidaParametro('MASCARA DA CLASSIFICACAO NO CADASTRO DE PLANO DE CONTAS')=False)
     Then edtmascara.EditMask:='999.999.999.999.999.999.999;1;_'
     Else edtmascara.EditMask:=ObjParametroGlobal.Get_Valor;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjSUBCONTAGERENCIAL:=TObjSUBCONTAGERENCIAL.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;
procedure TFSUBCONTAGERENCIAL.edtContaGerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjSUBCONTAGERENCIAL.edtContaGerencialkeydown(sender,key,shift,lbnomeContaGerencial);
end;

procedure TFSUBCONTAGERENCIAL.edtContaGerencialExit(Sender: TObject);
begin
    ObjSUBCONTAGERENCIAL.edtContaGerencialExit(sender,lbnomeContaGerencial);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFSUBCONTAGERENCIAL.EdtCODIGOPLANODECONTASExit(Sender: TObject);
begin
     Self.ObjSUBCONTAGERENCIAL.EdtCODIGOPLANODECONTASExit(Sender,LbNomePlanodeContas);
end;

procedure TFSUBCONTAGERENCIAL.EdtCODIGOPLANODECONTASKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    Self.ObjsUBCONTAGERENCIAL.EdtCODIGOPLANODECONTASKeyDown(sender,key,shift,LbNomePlanodeContas);
end;

function TFSUBCONTAGERENCIAL.atualizaQuantidade: string;
begin
     result:='Existem '+ContaRegistros('tabsubcontagerencial','codigo')+' Sub-Contas Gerenciais Cadastradas';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjSUBCONTAGERENCIAL.OBJETO.Get_Pesquisa,Self.ObjSUBCONTAGERENCIAL.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjSUBCONTAGERENCIAL.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjSUBCONTAGERENCIAL.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjSUBCONTAGERENCIAL.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
