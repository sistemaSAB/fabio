unit UObjTransferenciaPortador;
Interface
Uses StdCtrls,rdprint,Grids,classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc,UobjPortador,UobjGrupo,UobjLancamento,ibquery,uobjchequesportador;
Type

   TRegTransferenciaPortador=Record
                                Origem:string;//Nome do Objeto quem chamou a transferencia
                                CodigoLancamento:string;//codigo de quem chamou
                                PortadorDestino:string;
                                Valor:string;
                                Data:string;
                                Historico:string;//USado para gerar o historico da TabLAnctoPortadores
                             End;

TObjTransferenciaPortador=class
          Public
                //ObjDatasource                               :TDataSource;

                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                PortadorDestino      :TobjPortador;
                PortadorOrigem       :TobjPortador;
                CodigoLancamento     :TObjLancamento;
                Grupo                :TobjGrupo;

                Constructor Create;
                Destructor Free;
                Function    Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro:Boolean):Boolean;overload;
                Function    Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro,PExportaContabilidade:Boolean;Plancamento:string):Boolean;overload;
                Function    Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro,PExportaContabilidade:Boolean;Plancamento:string;PhistoricoLanctoPortador:String):Boolean;overload;


                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaLancamento(Plancamento:string) :boolean;

                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :string;
                Function    Get_TituloPesquisa              :string;
                Procedure   TabelaparaObjeto                        ;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Function Get_PortadorOrigem   :string;
                Function Get_Data             :string;
                Function Get_Grupo            :string;
                Function Get_Valor            :string;
                Function Get_Documento        :string;
                Function Get_Complemento      :string;
                Function Get_PortadorDestino  :string;

                Procedure Submit_CODIGO           (parametro:string);
                Procedure Submit_PortadorOrigem   (parametro:string);
                Procedure Submit_Data             (parametro:string);
                Procedure Submit_Grupo            (parametro:string);
                Procedure Submit_Valor            (parametro:string);
                Procedure Submit_Documento        (parametro:string);
                Procedure Submit_Complemento      (parametro:string);
                Procedure Submit_PortadorDestino  (parametro:string);
                Procedure Submit_ListaChequesTransferencia(ParametroCodigo:TStrings);
                Procedure Submit_HistoricoLancamentoPortador(parametro:string);

                Function    Get_PesquisaPortadorOrigem        :string;
                Function    Get_TituloPesquisaPortadorOrigem  :string;
                Function    Get_PesquisaGrupo                 :string;
                Function    Get_TituloPesquisaGrupo           :string;
                Function    Get_PesquisaPortadorDestino       :string;
                Function    Get_TituloPesquisaPortadorDestino :string;
                Function    Get_HistoricoLancamentoPortador   :string;


                Function  Get_NovoCodigo:string;
                Procedure Get_ListaChequesPortador(ParametroPortador:string;ParametroListaGeral:TstringGrid;PVencimento:string);overload;
                Procedure Get_ListaChequesPortador(ParametroPortador:string;ParametroListaGeral:TstringGrid;ComChequedoPortador:boolean;PVencimento:string);overload;

                Procedure Get_ListaChequesTransferidos(ParametroCodigo:string;Listagem:TstringGrid);
                Function  Get_SomaPorLancamento(ParametroLancamento:string):Currency;
                Function  LocalizaLancamentoporPortador(Plancamento: string;STLPORTADOR_DIN_CHTERC,STL_DIN_CH_TERC,STRPORTADOR_CH_PORT,STLCHEQUEPORTADOR,STLCODIGOCHEQUEPORTADOR:TStringList): boolean;
                Function  Get_Conta_Portadores_Diferentes(ParametroLancamento:string):Integer;
                Function  Get_NomePortadorOrigem:string;
                Function  Get_NomePortadorDestino:string;
                Procedure PegaSomaValorDinheiro_lancamento(Plancamento: string;out ValorDinheiro: string);
                Procedure Imprime(Pcodigo:string);
                Procedure ImprimeComprovante(pcodigo:string);
                Function  ContaCheques(PcodigoTransferencia:string):integer;
                Function  VerificaPermissao:boolean;

                procedure edtportadorExit(Sender: TObject; LbNome: Tlabel);

          Private

               ObjQueryLocal:Tibquery;
               ObjChequePortador:TObjChequesPortador;
               ObjDataset:Tibdataset;


               CODIGO           :string;
               Data             :string;
               Valor            :string;
               Documento        :string;
               Complemento      :string;
               HistoricoLancamentoPortador:string;


               ListaChequesTransferencia:Tstrings;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Function  AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino:boolean):boolean;overload;
                Function  AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino:Boolean;Plancamento:string): boolean;overload;
                Function  AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino:Boolean;Plancamento:string;PhistoricoLanctoPortador:String): boolean;overload;

                Function  AtualizaDinheiro(ImprimeOrigem,ImprimeDestino:boolean;phistoricolanctoportador:String):Boolean;
                Function  VerificaCheques:boolean;
                Function  ExportaContabilidade:boolean;
                Function  Excluilanctoportador(PcodigoTransferencia:string):boolean;

    
   End;

var
RegTransferenciaPortador:TRegTransferenciaPortador;

implementation
uses Uobjlanctoportador,SysUtils,Dialogs,UDatamodulo,Controls,UobjValoresTransferenciaPortador,
UOBJEXPORTACONTABILIDADE, UReltxtRDPRINT,
  UObjConfiguraFolhaRdPrint, UMenuRelatorios;


{ TTabTitulo }


Procedure  TObjTransferenciaPortador.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.HistoricoLancamentoPortador:=Fieldbyname('HistoricoLancamentoPortador').AsString;

        If (Self.PortadorOrigem.LocalizaCodigo(FieldByName('PortadorOrigem').asstring)=False)
        Then Begin
                  Messagedlg('Portador de Origem N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.PortadorOrigem.TabelaparaObjeto;

        Self.Data           :=FieldByName('Data').asstring;

        if (fieldbyname('Grupo').asstring<>'')
        Then Begin
                If (Self.Grupo.LocalizaCodigo(FieldByName('Grupo').asstring)=False)
                Then Begin
                        Messagedlg('Grupo N�o encontrado!',mterror,[mbok],0);
                        Self.ZerarTabela;
                        exit;
                End
                Else Self.Grupo.TabelaparaObjeto;
        End;


        Self.Valor          :=FieldByName('Valor').asstring;
        Self.Documento      :=FieldByName('Documento').asstring;
        Self.Complemento    :=FieldByName('Complemento').asstring;

        If (Self.PortadorDestino.LocalizaCodigo(FieldByName('PortadorDestino').asstring)=False)
        Then Begin
                  Messagedlg('Portador de Destino N�o encontrado!',mterror,[mbok],0);
                  Self.ZerarTabela;
                  exit;
             End
        Else Self.PortadorDestino.TabelaparaObjeto;

        If (FieldByName('CodigoLancamento').asstring<>'')//nao tem lancamento como gerador
        Then Begin
                If (Self.CodigoLancamento.LocalizaCodigo(FieldByName('CodigoLancamento').asstring)=False)
                Then Begin
                          Messagedlg('Codigo de Lan�amento N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          exit;
                     End
                Else Self.CodigoLancamento.TabelaparaObjeto;
             End;

     End;
end;


Procedure TObjTransferenciaPortador.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin

      FieldByName('CODIGO').asstring           := Self.CODIGO                           ;
      FieldByName('PortadorOrigem').asstring   := Self.PortadorOrigem.Get_CODIGO        ;
      FieldByName('Data').asstring             := Self.Data                             ;
      FieldByName('Grupo').asstring            := Self.grupo.Get_CODIGO                 ;
      FieldByName('Valor').asstring            := Self.valor                            ;
      FieldByName('Documento').asstring        := Self.documento                        ;
      FieldByName('Complemento').asstring      := Self.complemento                      ;
      FieldByName('PortadorDestino').asstring  := Self.PortadorDestino.get_codigo       ;
      FieldByName('CodigoLancamento').asstring := Self.CodigoLancamento.Get_CODIGO      ;
      FieldByName('HistoricoLancamentoPortador').AsString:=Self.HistoricoLancamentoPortador;

  End;
End;


function TObjTransferenciaPortador.Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro:Boolean):Boolean;
Begin
     //faz a chamada do salvar sempre exportando a contabilidade
     result:=Self.Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro,True,'','');
End;

function TObjTransferenciaPortador.Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro,PExportaContabilidade:Boolean;Plancamento:string):Boolean;
Begin
     Result:=Self.Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro,PExportaContabilidade,Plancamento,'');
End;
//***********************************************************************
//as variaves ,ImprimeOrigem,ImprimeDestino sao para indicar que no momento
//que for gerar a LanctoPortador vai prencher com 0 ou 1 o campo
//que indica se vai sair ou n�o nos relat�rios
//Plancamento s� � preenchida em casos de transferencia para baixa de cheque
//porque nesse caso nao existe lancamento, mas enviamos o lancamento
//da quitacao que usou esse cheque, para que no extrato por portador
//tenha como identificar qual o lancamento que gerou
//O Historico do LanctoPortador � usado quando se deseja personalizar
//o hist�rico que aparecera no LanctoPortador
//Exemplo TROCO - Rec. Do Seu Ze
function TObjTransferenciaPortador.Salvar(comcommit,ImprimeOrigemCheque,ImprimeDestinoCheque,ImprimeOrigemDinheiro,ImprimeDestinoDinheiro,PExportaContabilidade:Boolean;Plancamento:string;PhistoricoLanctoPortador:String):Boolean;
begin
  Result:=False;

  if (Self.VerificaBrancos=True)
  Then Exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;

   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

   if Self.status=dsinsert
   then Self.ObjDataset.Insert//libera para insercao
   Else
      if (Self.Status=dsedit)
      Then Self.ObjDataset.edit//se for edicao libera para tal
      else Begin
                 mensagemerro('Status Inv�lido na Grava��o');
                 exit;
      End;


 try
         Self.ObjetoParaTabela;
         Self.ObjDataset.Post;
 Except
       Messagedlg('Erro na tentativa de Salvar!',mterror,[mbok],0);
       exit;
 End;

 Self.PortadorOrigem.LocalizaCodigo(Self.PortadorOrigem.get_codigo);
 Self.PortadorOrigem.TabelaparaObjeto;
 Self.PortadorDestino.LocalizaCodigo(Self.PortadorDestino.get_codigo);
 Self.PortadorDestino.TabelaparaObjeto;
 //Atualizando os Valores em Dinheiro nos portadores Origem e Destino
 If (Self.AtualizaDinheiro(ImprimeOrigemDinheiro,ImprimeDestinoDinheiro,phistoricolanctoportador)=False)
 Then Begin
           Messagedlg('Erro Ao Tentar Atualizar o Valor em Dinheiro da Transfer�ncia',mterror,[mbok],0);
           if (Comcommit=True)
           Then FDataModulo.IBTransaction.RollbackRetaining;
           exit;
 End;
 //Atualizando os Valores em Cheques nos portadores Origem e Destino
 If (Self.AtualizaListadeCheques(ImprimeOrigemCheque,ImprimeDestinoCheque,Plancamento,PhistoricoLanctoPortador)=False)
 Then Begin
           Messagedlg('Erro Ao Tentar Atualizar a Lista de Cheques que ser�o Transferidos',mterror,[mbok],0);
           if (ComCommit=true)
           Then FDataModulo.IBTransaction.RollbackRetaining;
           exit;
 End;
//*******************************************************************
 if (PExportaContabilidade=True)
 Then Begin
        If (Self.ExportaContabilidade=False)
        Then Begin
                  Messagedlg('N�o foi poss�vel gerar a exporta��o para a contabilidade!'+#13+'A Transfer�ncia n�o foi efetuada!',mterror,[mbok],0);
                  if (Comcommit=True)
                  Then FDataModulo.IBTransaction.RollbackRetaining;
                  exit;
        End;
 End;

 If (ComCommit=True)
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjTransferenciaPortador.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO                             :='';
        Self.PortadorOrigem.ZerarTabela;
        Self.Data                               :='';
        Self.grupo.ZerarTabela;
        Self.valor                              :='';
        Self.documento                          :='';
        Self.complemento                        :='';
        Self.HistoricoLancamentoPortador        :='';
        
        Self.CodigoLancamento.ZerarTabela;
        Self.PortadorDestino.ZerarTabela;
     End;
end;

Function TObjTransferenciaPortador.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Self.PortadorOrigem.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Portador de Origem';

       If (Self.Data='')
       Then Mensagem:=Mensagem+'/Data';

       If (Self.Valor='')
       Then Mensagem:=mensagem+'/Valor';

       If (Self.PortadorDestino.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Portador Destino';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjTransferenciaPortador.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.PortadorOrigem.LocalizaCodigo(Self.PortadorOrigem.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador de Origem n�o Encontrado!'
     Else Begin
                Self.PortadorOrigem.TabelaparaObjeto;
     End;

     if (Self.Grupo.Get_CODIGO<>'')
     Then Begin
             If (Self.Grupo.LocalizaCodigo(Self.Grupo.Get_CODIGO)=False)
             Then Mensagem:=mensagem+'/ Grupo n�o Encontrado!';
     End;

     If (Self.PortadorDestino.LocalizaCodigo(Self.PortadorDestino.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Portador Destino n�o Encontrado!';

     if (Self.CodigoLancamento.get_codigo<>'')
     Then Begin
               if (Self.CodigoLancamento.LocalizaCodigo(Self.CodigoLancamento.Get_CODIGO)=False)
               Then Mensagem:=Mensagem+'/ Lan�amento n�o encontrado!'
               Else Self.CodigoLancamento.TabelaparaObjeto;
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End
     Else Begin
               Self.PortadorOrigem.TabelaparaObjeto;
               Self.PortadorDestino.TabelaparaObjeto;

     End;

     result:=true;
End;

function TObjTransferenciaPortador.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.PortadorOrigem.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Portador de Origem';
     End;

     try
        if (Self.Grupo.Get_codigo<>'')
        Then Inteiros:=Strtoint(Self.Grupo.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Grupo';
     End;

     try
        if (Self.CodigoLancamento.Get_codigo<>'')
        Then Inteiros:=Strtoint(Self.CodigoLancamento.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Lan�amento';
     End;

     try
        Reais:=Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;


     try
        Inteiros:=Strtoint(Self.PortadorDestino.get_codigo);
     Except
           Mensagem:=mensagem+'/Portador de Destino';
     End;
                                                      
     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjTransferenciaPortador.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

     Try
        Datas:=strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjTransferenciaPortador.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try


     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
     End;

     result:=true;

Finally

end;

end;

function TObjTransferenciaPortador.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,PortadorOrigem,Data,Grupo,Valor,Documento,Complemento,PortadorDestino,CodigoLancamento,HistoricoLancamentoPortador');
           SelectSql.add('from TabTransferenciaPortador where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjTransferenciaPortador.LocalizaLancamento(Plancamento: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,PortadorOrigem,Data,Grupo,Valor,Documento,Complemento,PortadorDestino,CodigoLancamento,HistoricoLancamentoPortador');
           SelectSql.add('from TabTransferenciaPortador where CodigoLancamento='+Plancamento);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjTransferenciaPortador.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjTransferenciaPortador.Exclui(Pcodigo: string;ComCommit:boolean): Boolean;
begin
     //para excluir uma transferencia � necessario primeiro
     //excluir os cheques que estaum ligadas a ela
     //senaum tera erro
     //depois disto retornar o saldo dos portadores
     //em relacao ao valor em dinheiro
     //somente depois disto apagar a transferencia
     result:=False;

     Try
        if (Self.ContaCheques(Pcodigo)>0)
        Then Begin
                  Messagedlg('N�o � poss�vel excluir uma transfer�ncia que possui cheques transferidos!',mterror,[mbok],0);
                  exit;
        End;

        //excluindo o Lancto em Portador desta transferencia

        if (Self.Excluilanctoportador(Pcodigo)=False)
        Then Begin
                  Messagedlg('Erro na Tentativa de Excluir os Lan�amentos no portador referentes a transferencia n� '+pcodigo,mterror,[mbok],0);
                  exit;
        End;


        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;

                 if (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;

                 result:=true;
        End
        Else exit;
     Except

     End;
end;


constructor TObjTransferenciaPortador.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.Objquerylocal:=Tibquery.create(nil);
        Self.Objquerylocal.Database:=FDataModulo.IbDatabase;

        Self.PortadorOrigem:=TObjPortador.Create;
        Self.PortadorDestino:=TObjPortador.Create;
        Self.Grupo:=TObjGrupo.Create;
        Self.CodigoLancamento:=TObjLancamento.create;
        Self.ObjChequePortador:=TObjChequesPortador.create;

        Self.ZerarTabela;

        With Self.ObjDataset do
        Begin
             SelectSQL.clear;
             SelectSql.add('Select CODIGO,PortadorOrigem,Data,Grupo,Valor,Documento,Complemento,PortadorDestino,CodigoLancamento,HistoricoLancamentoPortador');
             SelectSql.add('from TabTransferenciaPortador where codigo=0');

             Self.SqlInicial:=SelectSQL.text;

             InsertSQL.clear;
             InsertSQL.add(' Insert into TabTransferenciaPortador (CODIGO,PortadorOrigem,Data,Grupo,Valor,Documento,Complemento,PortadorDestino,CodigoLancamento,HistoricoLancamentoPortador)');
             InsertSQL.add(' values (:CODIGO,:PortadorOrigem,:Data,:Grupo,:Valor,:Documento,:Complemento,:PortadorDestino,:CodigoLancamento,:HistoricoLancamentoPortador)');

             ModifySQL.clear;
             ModifySQL.add(' Update TabTransferenciaPortador set CODIGO=:CODIGO,PortadorOrigem=:PortadorOrigem,Data=:Data,Grupo=:Grupo,');
             ModifySQL.add(' Valor=:Valor,Documento=:Documento,Complemento=:Complemento,PortadorDestino=:PortadorDestino,CodigoLancamento=:CodigoLancamento,HistoricoLancamentoPortador=:HistoricoLancamentoPortador where codigo=:codigo');


             DeleteSQL.clear;
             DeleteSQL.add('Delete from TabTransferenciaPortador where codigo=:codigo');

             RefreshSQL.clear;
             RefreshSQL.add('Select CODIGO,PortadorOrigem,Data,Grupo,Valor,Documento,Complemento,PortadorDestino,CodigoLancamento,HistoricoLancamentoPortador');
             RefreshSQL.add('from TabTransferenciaPortador where codigo=0');

             open;

             Self.ObjDataset.First ;
             Self.status          :=dsInactive;
        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjTransferenciaPortador.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjTransferenciaPortador.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjTransferenciaPortador.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjTransferenciaPortador.Get_Pesquisa: string;
begin
     Result:=' Select * from TabTransferenciaPortador ';
end;

function TObjTransferenciaPortador.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Transfer�ncia entre Portadores ';
end;

function TObjTransferenciaPortador.Get_Complemento: string;
begin
     Result:=Self.Complemento;
end;

function TObjTransferenciaPortador.Get_Data: string;
begin
     Result:=Self.Data;
end;

function TObjTransferenciaPortador.Get_Documento: string;
begin
     Result:=Self.documento;
end;

function TObjTransferenciaPortador.Get_Grupo: string;
begin
     Result:=Self.grupo.get_codigo;
end;

function TObjTransferenciaPortador.Get_PortadorDestino: string;
begin
     Result:=Self.portadordestino.get_codigo;
end;

function TObjTransferenciaPortador.Get_PortadorOrigem: string;
begin
     Result:=Self.portadororigem.get_codigo;
end;

function TObjTransferenciaPortador.Get_Valor: string;
begin
     Result:=Self.valor;
end;

procedure TObjTransferenciaPortador.Submit_Complemento(parametro: string);
begin
     Self.Complemento:=Parametro;
end;

procedure TObjTransferenciaPortador.Submit_Data(parametro: string);
begin
     Self.data:=Parametro;
end;

procedure TObjTransferenciaPortador.Submit_Documento(parametro: string);
begin
     Self.documento:=Parametro;
end;

procedure TObjTransferenciaPortador.Submit_Grupo(parametro: string);
begin
     Self.grupo.submit_codigo(Parametro);
end;

procedure TObjTransferenciaPortador.Submit_PortadorDestino(
  parametro: string);
begin
     Self.portadordestino.submit_codigo(parametro);
end;

procedure TObjTransferenciaPortador.Submit_PortadorOrigem(
  parametro: string);
begin
     Self.portadorOrigem.submit_codigo(Parametro);
end;

procedure TObjTransferenciaPortador.Submit_Valor(parametro: string);
begin
     Self.valor:=Parametro;
end;

function TObjTransferenciaPortador.Get_PesquisaGrupo: string;
begin
     Result:=Self.Grupo.Get_Pesquisa;
end;

function TObjTransferenciaPortador.Get_PesquisaPortadorDestino: string;
begin
     Result:=Self.portadordestino.get_pesquisa;
end;

function TObjTransferenciaPortador.Get_PesquisaPortadorOrigem: string;
begin
     Result:=Self.PortadorOrigem.Get_Pesquisa;
end;

function TObjTransferenciaPortador.Get_TituloPesquisaGrupo: string;
begin
     Result:=Self.Grupo.Get_TituloPesquisa;
end;

function TObjTransferenciaPortador.Get_TituloPesquisaPortadorDestino: string;
begin
     Result:=Self.PortadorDestino.Get_TituloPesquisa;
end;

function TObjTransferenciaPortador.Get_TituloPesquisaPortadorOrigem: string;
begin
     Result:=Self.PortadorOrigem.Get_TituloPesquisa;
end;

function TObjTransferenciaPortador.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=Self.ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_TRANSFERENCIAPORTADOR';
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o Transfer�ncia de Portadores',mterror,[mbok],0);
           result:='0';
           exit;
           
        End;
     Finally
            FreeandNil(StrTemp);
     End;
end;



procedure TObjTransferenciaPortador.Get_ListaChequesPortador(
  ParametroPortador: string; ParametroListaGeral: TstringGrid;PVencimento:string);
begin
   Self.PortadorOrigem.Get_ListaChequesPortador(ParametroPortador,ParametroListaGeral,false,Pvencimento);
end;

procedure TObjTransferenciaPortador.Get_ListaChequesPortador(
  ParametroPortador: string; ParametroListaGeral: TstringGrid;
  ComChequedoPortador: boolean;PVencimento:string);
begin
     Self.PortadorOrigem.Get_ListaChequesPortador(ParametroPortador,ParametroListaGeral,ComChequedoPortador,Pvencimento);
end;


procedure TObjTransferenciaPortador.Submit_ListaChequesTransferencia(
  ParametroCodigo: TStrings);
begin
     Self.ListaChequesTransferencia:=ParametroCodigo;
     
end;

function TObjTransferenciaPortador.AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino:Boolean): boolean;
begin

     Result:=Self.AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino,'','');
end;

function TObjTransferenciaPortador.AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino:Boolean;Plancamento:string): boolean;
begin
     Result:=Self.AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino,Plancamento,'');
End;

function TObjTransferenciaPortador.AtualizaListadeCheques(ImprimeOrigem,ImprimeDestino:Boolean;Plancamento:string;PhistoricoLanctoPortador:String): boolean;
var
Cont:Integer;
TempValorTRansferencia:TObjValoresTransferenciaPortador;
Historicoenviado:string;
TmpCodigoValTransfportador,TmpLancamento:string;
begin
     
     
     result:=False;
     //tenho que verificar se o campo chequedoportador=S so posso tranferir de
     //um portador qualquer para  o portador original dele

     Try
         TempValorTransferencia:=TObjValoresTransferenciaPortador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto dos Valores Transfer�ncia do Portador',mterror,[mbok],0);
           result:=False;
           exit;
     End;

     Try
     //verifico se tenho cheque na Lista para ser atualizado
     //se n�o tenho retorno que ta tudo beleza com TRUE
     If (Self.VerificaCheques=False)
     Then Begin
               If (strtofloat(self.valor)=0)
               Then Begin
                        Messagedlg('N�o h� cheques Nem Dinheiro para serem atualizados serem Transferidos!',mterror,[mbok],0);
                        result:=False;
                    End
               Else result:=True;
               exit;
     End;

     //preciso transferir os cheques do portador origem para o destino
     //e atualizando o saldo os portadores
     //chamo a funcao do portador de atualizar o cheque
     //que por sua vez cria um objeto temporario da chequeportador
     //e chama sua funcao de atualiza��o que por sua vez atualiza o saldo do
     //portador origem e destino
     //Usado para o hist�rico da TabLanctoPortadores
     if (Self.Grupo.Get_codigo<>'')
     Then Begin
             Self.grupo.LocalizaCodigo(Self.Grupo.Get_CODIGO);
             Self.Grupo.TabelaparaObjeto;
     End
     Else Self.Grupo.ZerarTabela;
     //*********************************************
     historicoenviado:='';

     if (PhistoricoLanctoPortador='')
     Then Begin
             If self.CodigoLancamento.Get_CODIGO<>''
             Then Historicoenviado:=Self.CodigoLancamento.Get_historico+' (Cheque 3�)'
             Else Historicoenviado:='Transf. CH '+Self.PortadorOrigem.get_codigo+' p/ '+Self.PortadorDestino.Get_CODIGO+' - '+Self.Grupo.Get_Descricao+' - '+SELF.Complemento;
     End
     Else Historicoenviado:=PhistoricoLanctoPortador+' (Cheque 3�)';

     {A Variavel HistoricoLancamentoPortador foi criado na tabela de Transferencia para facilitar a utilizacao de historicos
     para evitar de ficar tratando se foi baixa de cheque, transferencia, se usa historico do lancamento....
     dessa forma, o que for digitado � utilizado no LanctoPortador}


     if (Self.HistoricoLancamentoPortador<>'')
     Then historicoenviado:=Self.HistoricoLancamentoPortador;
     

     //esse lancamento soh existe se for por exemplo uma quitacao
     //de contas a pagar com cheque
     //precisa ser tmbm no caso de cheque descontado, porque assim eu consigo
     //no extrato saber de qual lancamento se refere o cheque que esta sendo descontado
     //entao no caso de descontar cheque � enviado o PLANCAMENTO preenchido
     if (Plancamento='')
     Then TmpLancamento:=Self.CodigoLancamento.get_codigo
     Else TmpLancamento:=Plancamento;

     For cont:=0 to Self.ListaChequesTransferencia.Count-1 do
     Begin

             Self.ObjChequePortador.ZerarTabela;
             If (Self.ObjChequePortador.LocalizaCodigo(Self.ListaChequesTransferencia[cont])=False)
             Then Begin
                     Messagedlg('Cheque n�o encontrado->'+Self.ListaChequesTransferencia[cont]+' Na Tabela de Cheques por Portador',mterror,[mbok],0);
                     result:=False;
                     exit;
             End;
             Self.ObjChequePortador.TabelaparaObjeto;
             If(Self.ObjChequePortador.MotivoDevolucao.get_codigo<>'')
             Then Begin
                     If (Self.ObjChequePortador.MotivoDevolucao.Get_Reapresenta='N')
                     Then Begin
                               Messagedlg('O Cheque n�o pode ser transferido, pois foi devolvido por motivo que n�o aceita reapresenta��o!',mterror,[mbok],0);
                               Result:=False;
                               exit;
                     End;
             End;
             Self.ObjChequePortador.Portador.Submit_CODIGO(Self.PortadorDestino.Get_CODIGO);
             Self.ObjChequePortador.Status:=dsedit;
             If (Self.ObjChequePortador.Salvar(False,False,False,'')=False)
             Then Begin
                     Messagedlg('Erro Ao Tentar Trocar o Portador Origem para Destino na Tabela de Cheque por Portadores',mterror,[mbok],0);
                     result:=False;
                     exit;
             End;
             TempValorTRansferencia.ZerarTabela;
             TmpCodigoValTransfportador:=TempValorTRansferencia.Get_NovoCodigo;

             TempValorTRansferencia.Submit_CODIGO(TmpCodigoValTransfportador);
             TempValorTRansferencia.Submit_TransferenciaPortador(Self.codigo);
             TempValorTRansferencia.Submit_Valores(Self.ListaChequesTransferencia[cont]);
             TempValorTRansferencia.Status:=DsInsert;
             If (TempValorTRansferencia.salvar(False)=False)
             Then Begin
                       Messagedlg('N�o foi poss�vel Lan�ar o T�tulo de Valores da Transfer�ncia Entre Portadores',mterror,[mbok],0);
                       result:=False;
                       exit;
             End;

             //Lan�ando  na tabLanctoPortador
             ObjlanctoportadorGlobal.ZerarTabela;
             ObjlanctoportadorGlobal.Submit_Historico(Historicoenviado);
             ObjlanctoportadorGlobal.Submit_Data(Self.Data);
             ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABVALORESTRANSFERENCIAPORTADOR');
             ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJVALORESTRANSFERENCIAPORTADOR');
             ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
             ObjlanctoportadorGlobal.Submit_ValordoCampo(TmpCodigoValTransfportador);
             ObjlanctoportadorGlobal.Submit_ImprimeRel(ImprimeOrigem);
             ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(TmpLancamento);
             ObjlanctoportadorGlobal.ValoresTransferenciaPortador.submit_codigo(TmpCodigoValTransfportador);
             If (ObjlanctoportadorGlobal.AumentaSaldo(Self.PortadorDestino.Get_codigo,Self.ObjChequePortador.Get_Valor,False)=False)
             Then Begin
                     Messagedlg('Erro ao tentar Creditar do Saldo Destino',mterror,[mbok],0);
                     result:=False;
                     exit;
             End;

             ObjlanctoportadorGlobal.ZerarTabela;
             ObjlanctoportadorGlobal.Submit_Historico(Historicoenviado);
             ObjlanctoportadorGlobal.Submit_Data(Self.Data);
             ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABVALORESTRANSFERENCIAPORTADOR');
             ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJVALORESTRANSFERENCIAPORTADOR');
             ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
             ObjlanctoportadorGlobal.Submit_ValordoCampo(TmpCodigoValTransfportador);
             ObjlanctoportadorGlobal.Submit_ImprimeRel(imprimedestino);
             ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(TmpLancamento);
             ObjlanctoportadorGlobal.ValoresTransferenciaPortador.submit_codigo(TmpCodigoValTransfportador);
             If (ObjlanctoportadorGlobal.DiminuiSaldo(Self.PortadorOrigem.Get_codigo,Self.ObjChequePortador.Get_Valor,False)=False)
             Then Begin
                     Messagedlg('Erro ao tentar Diminuir o Saldo do Origem',mterror,[mbok],0);
                     result:=False;
                     exit;
             End;
     End;
     
Finally
       TempValorTRansferencia.Free;
end;
     result:=True;
end;

function TObjTransferenciaPortador.VerificaCheques: boolean;
Var
Codigotemp:integer;
begin
     If Self.ListaChequesTransferencia.Count>0
     Then result:=true
     Else result:=False;

end;


procedure TObjTransferenciaPortador.Get_ListaChequesTransferidos(
  ParametroCodigo: string; Listagem: TstringGrid);
var
    TempValorTRansferencia:TObjValoresTransferenciaPortador;
begin
     Try
         TempValorTransferencia:=TObjValoresTransferenciaPortador.create;
     Except
           Messagedlg('Erro na Cria��o do Objeto dos Valores Transfer�ncia do Portador',mterror,[mbok],0);
           exit;
     End;

     Try
        TempValorTransferencia.Get_ListaChequesTransferidos(ParametroCodigo,Listagem);
     Finally
            TempValorTRansferencia.Free;
     End;

End;

Procedure TObjTransferenciaPortador.PegaSomaValorDinheiro_lancamento(Plancamento:string;out ValorDinheiro:string);
var
Cheques,Dinheiro:Currency;
begin
     With Self.ObjDataset do
     Begin
          //soma-se os totais da TabtransferenciaPortador que contem os valores transferidos em dinheiro
          close;
          SelectSql.Clear;
          SelectSql.add('Select sum(valor) as Soma from Tabtransferenciaportador where CodigoLancamento='+PLancamento);
          open;
          ValorDinheiro:='';//parametro
          Dinheiro:=0;
          Dinheiro:=Fieldbyname('SOMA').asfloat;
          If (Dinheiro>0)
          Then Valordinheiro:=floattostr(dinheiro);
     End;
end;

function TObjTransferenciaPortador.Get_SomaPorLancamento(
  ParametroLancamento: string): Currency;
var
Cheques,Dinheiro:Currency;
begin
     With Self.ObjDataset do
     Begin
          //soma-se os totais da TabtransferenciaPortador que contem os valores transferidos em dinheiro
          close;
          SelectSql.Clear;
          SelectSql.add('Select sum(valor) as Soma from Tabtransferenciaportador where CodigoLancamento='+ParametroLancamento);
          open;
          Dinheiro:=0;
          Dinheiro:=Fieldbyname('SOMA').asfloat;
          close;
          SelectSql.Clear;
          //Soma-se os valores dos cheques Na TabChequesPortador
          //Para saber quais foram os cheques Transferidos eu
          //Verifico a lista de TabValoresTransferenciaPortador
          //que contem o codigo dos cheques transferidos pela
          //TabtransferenciaPortador
          SelectSql.add('select Sum(tabchequesportador.valor) as Soma from tabchequesportador where codigo in');
          SelectSql.add('(select tabvalorestransferenciaportador.valores from tabvalorestransferenciaportador left join tabtransferenciaportador');
          SelectSql.add('on tabvalorestransferenciaportador.transferenciaportador=tabtransferenciaportador.codigo');
          SelectSql.add('where tabtransferenciaportador.codigolancamento='+ParametroLancamento+')');

          open;
          Cheques:=0;
          Cheques:=Fieldbyname('SOMA').asfloat;
          close;
          Result:=Cheques+Dinheiro;
     End;
end;
function TObjTransferenciaPortador.Get_Conta_Portadores_Diferentes(ParametroLancamento: string): Integer;
begin
     With Self.ObjDataset do
     Begin
          //soma-se os totais da TabtransferenciaPortador que contem os valores transferidos em dinheiro
          close;
          SelectSql.Clear;
          SelectSql.add('Select distinct(portadororigem) from Tabtransferenciaportador where CodigoLancamento='+ParametroLancamento);
          open;
          last;
          Result:=Recordcount;
     End;

end;

function TObjTransferenciaPortador.AtualizaDinheiro(ImprimeOrigem,ImprimeDestino:boolean;phistoricolanctoportador:String): Boolean;
VAR
HISTORICOENVIADO:STRING;
begin
     If (strtofloat(Self.valor)=0)
     Then Begin
               result:=True;
               Exit;
          End;

    //Atualiza o Portador Origem e Destino atraves do ObjLanctoPortador
    //assim eu guardo o historico da Transferencia

    if (Self.Grupo.Get_codigo<>'')
    Then Begin
            Self.grupo.LocalizaCodigo(Self.Grupo.Get_CODIGO);
            Self.Grupo.TabelaparaObjeto;
    End
    Else Self.grupo.zerartabela;


    HISTORICOENVIADO:='';

    if (phistoricolanctoportador='')
    Then Begin
              If self.CodigoLancamento.Get_CODIGO<>''
              Then HistoricoEnviado:=Self.CodigoLancamento.get_historico
              Else HistoricoEnviado:='Transf. '+Self.PortadorOrigem.get_codigo+' p/ '+Self.PortadorDestino.Get_CODIGO+' - '+Self.Grupo.Get_Descricao+'-'+Self.Complemento;
    End
    Else HistoricoEnviado:=phistoricolanctoportador;

    {A Variavel HistoricoLancamentoPortador foi criado na tabela de Transferencia para facilitar a utilizacao de historicos
     para evitar de ficar tratando se foi baixa de cheque, transferencia, se usa historico do lancamento....
     dessa forma, o que for digitado � utilizado no LanctoPortador}

    if (Self.HistoricoLancamentoPortador<>'')
    Then historicoenviado:=Self.HistoricoLancamentoPortador;

    ObjlanctoportadorGlobal.ZerarTabela;
    ObjlanctoportadorGlobal.Submit_Historico(HISTORICOENVIADO);
    ObjlanctoportadorGlobal.Submit_Data(Self.Data);
    ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABTRANSFERENCIAPORTADOR');
    ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJTRANSFERENCIAPORTADOR');
    ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
    ObjlanctoportadorGlobal.Submit_ValordoCampo(Self.Codigo);
    ObjlanctoportadorGlobal.Submit_ImprimeRel(ImprimeOrigem);
    ObjlanctoportadorGlobal.TransferenciaPortador.Submit_CODIGO(Self.Codigo);

    if (self.CodigoLancamento.get_codigo='')
    Then ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO('')
    Else ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(self.CodigoLancamento.get_codigo);

     //Atualizando o Valor em Dinheiro nos Portadores Origem e Destino
     If (ObjlanctoportadorGlobal.DiminuiSaldo(Self.PortadorOrigem.get_codigo,Self.valor,False)=False)
     Then Begin
                Messagedlg('Erro Ao Tentar Debitar no Saldo na Conta de Origem',mterror,[mbok],0);
                result:=False;
                exit;
          End;
    ObjlanctoportadorGlobal.ZerarTabela;
    ObjlanctoportadorGlobal.Submit_Historico(HISTORICOENVIADO);
    ObjlanctoportadorGlobal.Submit_Data(Self.Data);
    ObjlanctoportadorGlobal.Submit_TabelaGeradora('TABTRANSFERENCIAPORTADOR');
    ObjlanctoportadorGlobal.Submit_ObjetoGerador('OBJTRANSFERENCIAPORTADOR');
    ObjlanctoportadorGlobal.Submit_CampoPrimario('CODIGO');
    ObjlanctoportadorGlobal.Submit_ValordoCampo(Self.Codigo);
    ObjlanctoportadorGlobal.Submit_ImprimeRel(ImprimeDestino);
    ObjlanctoportadorGlobal.TransferenciaPortador.Submit_CODIGO(Self.Codigo);

    if (self.CodigoLancamento.get_codigo='')
    Then ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO('')
    Else ObjlanctoportadorGlobal.Lancamento.Submit_CODIGO(self.CodigoLancamento.get_codigo);

    If (ObjlanctoportadorGlobal.AumentaSaldo(Self.PortadorDestino.get_codigo,Self.valor,False)=False)
    Then Begin
                Messagedlg('Erro Ao Tentar Creditar no Saldo na Conta de Destino',mterror,[mbok],0);
                result:=False;
                exit;
    End;
    Result:=True;

end;


destructor TObjTransferenciaPortador.Free;
begin
     Freeandnil(Self.ObjDataset);
     Freeandnil(Self.Objquerylocal);
     Self.PortadorOrigem.free;
     Self.PortadorDestino.free;
     Self.Grupo.free;
     Self.CodigoLancamento.free;
     Self.ObjChequePortador.free;
end;

function TObjTransferenciaPortador.Get_NomePortadorDestino: string;
begin
     result:='';
     If (Self.PortadorDestino.get_codigo='')
     Then exit;
     If (Self.PortadorDestino.LocalizaCodigo(Self.PortadorDestino.Get_CODIGO)=False)
     Then exit;
     Self.PortadorDestino.TabelaparaObjeto;
     Result:=Self.PortadorDestino.Get_Nome;
end;

function TObjTransferenciaPortador.Get_NomePortadorOrigem: string;
begin
     result:='';
     If (Self.PortadorOrigem.get_codigo='')
     Then exit;
     If (Self.PortadorOrigem.LocalizaCodigo(Self.PortadorOrigem.Get_CODIGO)=False)
     Then exit;
     Self.PortadorOrigem.TabelaparaObjeto;
     Result:=Self.PortadorOrigem.Get_Nome;
end;


//Separo as transferencias de um determinado lancamento
//por portador, o que foi cheque do portador
//e o que foi em cheque de 3� e dinheiro
function TObjTransferenciaPortador.LocalizaLancamentoporPortador(Plancamento: string;STLPORTADOR_DIN_CHTERC,STL_DIN_CH_TERC,STRPORTADOR_CH_PORT,STLCHEQUEPORTADOR,STLCODIGOCHEQUEPORTADOR:TStringList): boolean;
var
cont:Integer;
TmpValor:Currency;
begin
     With Self.ObjDataset do
     Begin
          //soma-se os totais da TabtransferenciaPortador que contem os valores transferidos em dinheiro
          close;
          SelectSql.Clear;
          SelectSql.add('Select portadororigem,sum(valor) as Soma from Tabtransferenciaportador where CodigoLancamento='+PLancamento);
          SelectSql.add('group by portadororigem');
          open;
         //STLPortador,STLVALORDIN,STRVALORCHEQUETERC,STLCHEQUEPORTADOR:
         //pegando os portadores que foram usados
          first;
          STLPORTADOR_DIN_CHTERC.clear;
          STL_DIN_CH_TERC.clear;
          //ADICIONANDO O DINHEIRO
          While not(eof) do
          Begin
               STLPORTADOR_DIN_CHTERC.add(fieldbyname('portadororigem').asstring);
               STL_DIN_CH_TERC.add(FLOATTOSTR(fieldbyname('SOMA').ASFLOAT));
               next;
          End;
          //ADICIONANDO O VALOR DOS CHEQUES DE TERCEIRO
          for cont:=0 to STLPORTADOR_DIN_CHTERC.Count-1 do
          Begin
               //vou somando o valor de 3� e cheque do portador
               //de cada portador
               //contra as minha ideologias vou ter
               //que pesquisar na tabvalores do objeto de transferencia
               close;
               SelectSQL.clear;
               SelectSQL.add('select sum(valor)as Soma from tabchequesportador where');
               SelectSQL.add('chequedoportador=''N'' ');
               SelectSQL.add('and codigo in (');
               SelectSQL.add('select tabvalorestransferenciaportador.valores');
               SelectSQL.add('from tabvalorestransferenciaportador left join TabTransferenciaPortador');
               SelectSQL.add('on tabvalorestransferenciaportador.TransferenciaPortador=TabTransferenciaPortador.codigo');
               SelectSQL.add(' where TabTransferenciaPortador.codigolancamento='+Plancamento+' and TabTransferenciaPortador.portadororigem='+STLPORTADOR_DIN_CHTERC.Strings[cont]+')');
               open;
               TmpValor:=0;
               tmpvalor:=strtofloat(STL_DIN_CH_TERC[cont]);
               tmpvalor:=tmpvalor+fieldbyname('SOMA').ASFLOAT;
               STL_DIN_CH_TERC.strings[cont]:=FLOATTOSTR(tmpvalor);
          End;
          //resgatando agora todos os cheques do portador usados
          //para pagar conta
          STRPORTADOR_CH_PORT.clear;
          STLCHEQUEPORTADOR.clear;
          STLCODIGOCHEQUEPORTADOR.clear;

          for cont:=0 to STLPORTADOR_DIN_CHTERC.Count-1 do
          Begin
                close;
                SelectSQL.clear;
                SelectSQL.add('select codigo,valor from tabchequesportador where');
                SelectSQL.add('chequedoportador=''S'' ');
                SelectSQL.add('and codigo in (');
                SelectSQL.add('select tabvalorestransferenciaportador.valores');
                SelectSQL.add('from tabvalorestransferenciaportador left join TabTransferenciaPortador');
                SelectSQL.add('on tabvalorestransferenciaportador.TransferenciaPortador=TabTransferenciaPortador.codigo');
                SelectSQL.add(' where TabTransferenciaPortador.codigolancamento='+Plancamento+' and TabTransferenciaPortador.portadororigem='+STLPORTADOR_DIN_CHTERC.Strings[cont]+')');
                open;
                first;
                While not (eof) do
                Begin
                     STRPORTADOR_CH_PORT.add(STLPORTADOR_DIN_CHTERC.strings[cont]);
                     STLCHEQUEPORTADOR.add(floattostr(fieldbyname('valor').asfloat));
                     STLCODIGOCHEQUEPORTADOR.add(Fieldbyname('codigo').asstring);
                     next;
                End;
          End;
     End;

end;


function TObjTransferenciaPortador.ExportaContabilidade: boolean;
var

PCCREDITE,PCDEBITE:string;
NomeCredite,NomeDebite:string;
EXPORTACONTABILIDADE:TOBJEXPORTACONTABILIDADE;
ChequesPortadorTemp:TObjChequesPortador;
cont:integer;
valor,valorcheque:currency;
begin
     Result:=False;
     PCCREDITE:='';
     PCDEBITE:='';
     NomeCredite:='';
     NomeDebite:='';
     Self.LocalizaCodigo(self.codigo);
     self.TabelaparaObjeto;

     Try
        ChequesPortadorTemp:=TObjChequesPortador.create;
        valorcheque:=0;
        
        for cont:=0 to ListaChequesTransferencia.Count-1 do
        Begin
             ChequesPortadorTemp.LocalizaCodigo(ListaChequesTransferencia.Strings[cont]);
             ChequesPortadorTemp.TabelaparaObjeto;
             try
                valor:=StrToFloat(ChequesPortadorTemp.Get_Valor);
             except
                   valor:=0;
             end;
             valorcheque:=valorcheque+valor;
        End;
        ChequesPortadorTemp.free;
     Except
           Messagedlg('Erro Durante a Cria��o do Objeto de Cheques por Portador!',mterror,[mbok],0);
           exit;
     End;

     Try
        EXPORTACONTABILIDADE:=TObjExportaContabilidade.create;
     Except
            Messagedlg('Erro na tentativa de criar o objeto de exporta��o de contabilidade',mterror,[mbok],0);
            exit;
     End;
Try

        //Encontrando os dados do plano de contas do credor/devedsdor
     If (Self.PortadorOrigem.Get_CodigoPlanodeContas<>'')
     Then PCCREDITE:=Self.PortadorOrigem.Get_CodigoPlanodeContas;

     If (PCCREDITE<>'')
     Then Begin
             If (Self.PortadorOrigem.PlanodeContas.LocalizaCodigo(PCCREDITE)=False)
             Then PCCREDITE:=''
             Else Begin
                     Self.PortadorOrigem.PlanodeContas.TabelaparaObjeto;
                     NomeCredite:=Self.PortadorOrigem.PlanodeContas.Get_Nome;
             End;
     End;

     If (Self.PortadorDestino.Get_CodigoPlanodeContas<>'')
     Then PCDEBITE:=Self.PortadorDestino.Get_CodigoPlanodeContas;

     If (PCDEBITE='')
     Then PCDEBITE:=''
     Else Begin
             If (Self.PortadorDestino.PlanodeContas.LocalizaCodigo(PCDEBITE)=False)
             Then PCDEBITE:=''
             Else Begin
                     Self.PortadorDestino.PlanodeContas.TabelaparaObjeto;
                     NomeDebite:=Self.PortadorDestino.PlanodeContas.Get_Nome;
             End;
     End;
     //*******************************************************
     ExportaContabilidade.ZerarTabela;
     ExportaContabilidade.Submit_Exportado('N');
     ExportaContabilidade.Submit_ObjGerador('OBJTRANSFERENCIAPORTADOR');
     ExportaContabilidade.Submit_CodGerador(Self.codigo);

     If (PCCREDITE='') or (PCDEBITE='')
     Then ExportaContabilidade.Submit_Exporta('N')
     Else ExportaContabilidade.Submit_Exporta('S');

     ExportaContabilidade.Submit_CODIGO(ExportaContabilidade.Get_NovoCodigo);
     ExportaContabilidade.Submit_Data(Self.Data);
     ExportaContabilidade.Submit_Historico(Self.GRUPO.GET_DESCRICAO+'-'+SELF.COMPLEMENTO);
     ExportaContabilidade.Submit_ContaCredite(PCCREDITE);
     ExportaContabilidade.Submit_ContaDebite(PCDEBITE);
     ExportaContabilidade.Submit_Valor(Floattostr(strtofloat(Self.Valor)+valorcheque));
     ExportaContabilidade.Status:=dsinsert;
     RESULT:=ExportaContabilidade.Salvar(False);
     ExportaContabilidade.Status:=dsInactive;
     //*******************************************************

Finally
       EXPORTACONTABILIDADE.free;
End;
End;


procedure TObjTransferenciaPortador.Imprime(Pcodigo: string);
begin
     With FMenuRelatorios do
     Begin
          NomeObjeto:='UOBJTRANSFERENCIAPORTADOR';
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Comprovante de Transfer�ncia');
          
          showmodal;

          if (tag=0)
          Then exit;

          Case RgOpcoes.ItemIndex of

            0:Self.ImprimeComprovante(pcodigo);

          End;
     End;

end;

procedure TObjTransferenciaPortador.ImprimeComprovante(pcodigo: string);
var
linha:integer;
valortotalcheque:Currency;
OBJCONFIGURAFOLHA:TObjConfiguraFolhaRdPrint;
begin


     if (Pcodigo='')
     Then Begin
               Messagedlg('Escolha a transfer�ncia a ser impressa!',mterror,[mbok],0);
               exit;
     End;
     if (Self.LocalizaCodigo(Pcodigo)=False)
     Then Begin
               Messagedlg('Transfer�ncia N�o Encontrada!',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;

     FreltxtRDPRINT.ConfiguraImpressao;

     try
        OBJCONFIGURAFOLHA:=TObjConfiguraFolhaRdPrint.Create;
     Except
           Messagedlg('Erro na Tentativa de Criar o Objeto de Configura��o de Folha do Relat�rio',mtError,[mbok],0);
           exit;
     End;

     Try
        If (OBJCONFIGURAFOLHA.ResgataConfiguracoesFolha(FreltxtRDPRINT.RDprint,'COMPROVANTETRANSFERENCIA.INI')=fALSE)
        Then Begin
                  Messagedlg('Erro na tentativa de resgatar as configura��es do relat�rio no arquivo "COMPROVANTETRANSFERENCIA.INI"',mterror,[mbok],0);
                  exit;
        End;
     Finally
            OBJCONFIGURAFOLHA.free;
     End;

     FreltxtRDPRINT.RDprint.Abrir;
     If (FreltxtRDPRINT.RDprint.Setup=False)
     Then Begin
               FreltxtRDPRINT.RDprint.Fechar;
               exit;
     End;
     linha:=3;
     FreltxtRDPRINT.RDprint.ImpC(LINHA,45,'COMPROVANTE DA TRANSFER�NCIA N� '+Self.Codigo,[negrito]);
     inc(linha,2);
     FreltxtRDPRINT.RDprint.ImpF(LINHA,1,'Data: ',[negrito]);
     FreltxtRDPRINT.RDprint.Imp(LINHA,7,Self.Data);
     inc(linha,1);
     FreltxtRDPRINT.RDprint.ImpF(LINHA,1,'Grupo: ',[negrito]);
     FreltxtRDPRINT.RDprint.Imp(LINHA,7,Self.Grupo.Get_CODIGO+' - '+Self.Grupo.Get_Descricao);
     inc(linha,1);
     FreltxtRDPRINT.RDprint.ImpF(LINHA,1,'Complemento: ',[negrito]);
     FreltxtRDPRINT.RDprint.Imp(LINHA,13,Self.Complemento);
     inc(linha,1);
     FreltxtRDPRINT.RDprint.ImpF(LINHA,1,'Documento: ',[negrito]);
     FreltxtRDPRINT.RDprint.Imp(LINHA,12,Self.Documento);
     inc(linha,1);
     FreltxtRDPRINT.RDprint.ImpF(LINHA,1,'Portador Origem: ',[negrito]);
     FreltxtRDPRINT.RDprint.Imp(LINHA,17,Self.PortadorOrigem.Get_CODIGO+' - '+Self.PortadorOrigem.Get_Nome);
     inc(linha,1);
     FreltxtRDPRINT.RDprint.ImpF(LINHA,1,'Portador Destino: ',[negrito]);
     FreltxtRDPRINT.RDprint.Imp(LINHA,17,Self.PortadorDestino.Get_CODIGO+' - '+Self.PortadorDestino.Get_Nome);
     inc(linha,1);
     FreltxtRDPRINT.RDprint.ImpF(LINHA,1,'Valor em Dinheiro: ',[negrito]);
     FreltxtRDPRINT.RDprint.Imp(LINHA,21,formata_valor(Self.Valor));
     inc(linha,1);
     FreltxtRDPRINT.RDprint.Imp(LINHA,1,CompletaPalavra('_',90,'_'));
     inc(linha,1);
     
     with Self.ObjDataset do
     Begin
          close;
          SelectSQL.Clear;
          SelectSQL.add('Select TabValoresTransferenciaPortador.Valores as NumChequeSIS,');
          SelectSQL.add('TabChequesportador.Banco,TabChequesportador.Conta,TabChequesportador.NumCheque,');
          SelectSQL.add('TabChequesportador.Valor,TabValoresTransferenciaPortador.Codigo as CODVALTRANSFCHEQUE,');
          SelectSQL.add('TabValoresTransferenciaPortador.TransferenciaPortador as NUMTRANSF from TabValoresTransferenciaPortador');
          SelectSQL.add('join TabChequesportador on TabValoresTransferenciaPortador.Valores=TabchequesPortador.codigo');
          SelectSQL.add(' where TabValoresTransferenciaPortador.transferenciaportador='+Self.Codigo);
          open;
          last;
          if (recordcount=0)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          first;
          valortotalcheque:=0;
          FreltxtRDPRINT.RDprint.ImpF(linha,1,CompletaPalavra('N�CHSIS',9,' ')+' '+
                                              CompletaPalavra('BANCO',9,' ')+' '+
                                              CompletaPalavra('CONTA',9,' ')+' '+
                                              CompletaPalavra('NUMCHEQUE',9,' ')+' '+
                                              CompletaPalavra_a_Esquerda('VALOR',12,' '),[NEGRITO]);
          inc(linha,1);
          While not(eof) do
          Begin                                                                                       
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('NumChequeSIS').asstring,9,' ')+' '+
                                              CompletaPalavra(fieldbyname('BANCO').asstring,9,' ')+' '+
                                              CompletaPalavra(fieldbyname('CONTA').asstring,9,' ')+' '+
                                              CompletaPalavra(fieldbyname('NUMCHEQUE').asstring,9,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),12,' '));
               valortotalcheque:=valortotalcheque+fieldbyname('VALOR').asfloat;
               inc(linha,1);
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(LINHA,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.ImpF(linha,1,'Total em Cheques: R$ ' +formata_valor(floattostr(valortotalcheque)),[negrito]);
          FreltxtRDPRINT.RDprint.Fechar;
     End;

end;

function TObjTransferenciaPortador.ContaCheques(
  PcodigoTransferencia: string): integer;
begin
     Result:=0;
     With Self.ObjQueryLocal do
     begin
          close;
          sql.clear;
          SQL.add('select count(codigo) as CONTA from tabvalorestransferenciaportador');
          SQL.add('where transferenciaportador='+PcodigoTransferencia);
          open;
          Result:=fieldbyname('conta').asinteger;
     End;
end;

function TObjTransferenciaPortador.Excluilanctoportador(
  PcodigoTransferencia: string): boolean;
begin
     result:=False;
     With Self.ObjQueryLocal do
     Begin
          close;
          SQL.clear;
          SQL.add('Select codigo from TabLanctoPortador where TransferenciaPortador='+PcodigoTransferencia);
          open;
          first;
          While not(eof) do
          Begin
              if (ObjLanctoPortadorGlobal.exclui(fieldbyname('codigo').asstring,False)=False)
              Then Begin
                        Messagedlg('Erro na tentativa de Excluir o Lan�amento n�'+fieldbyname('codigo').asstring+' no portador',mterror,[mbok],0);
                        exit;
              End;
              next;
          End;
          result:=true;
     End;
end;

function TObjTransferenciaPortador.VerificaPermissao: boolean;
begin
     Result:=False;
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE TRANSFER�NCIA ENTRE PORTADORES')=False)
     Then exit
     Else Result:=True;
end;

procedure TObjTransferenciaPortador.edtportadorExit(Sender: TObject;LbNome:Tlabel);
var
Pcodigo:string;
begin
     lbnome.caption:='';
     Pcodigo:=Tedit(sender).text;
     Tedit(sender).text:='';

     if (Pcodigo='')
     then exit;

     if (Self.PortadorDestino.LocalizaCodigo(pcodigo)=False)
     then exit;

     Self.portadorDestino.TabelaparaObjeto;
     LbNome.caption:=Self.portadorDestino.get_nome;
     TEdit(sender).text:=pcodigo;
end;




function TObjTransferenciaPortador.Get_HistoricoLancamentoPortador: string;
begin
     Result:=Self.HistoricoLancamentoPortador;
end;

procedure TObjTransferenciaPortador.Submit_HistoricoLancamentoPortador(
  parametro: string);
begin
     Self.HistoricoLancamentoPortador:=parametro;
end;

end.
