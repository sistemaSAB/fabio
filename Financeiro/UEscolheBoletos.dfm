object FEscolheBoletos: TFEscolheBoletos
  Left = 270
  Top = 119
  Width = 395
  Height = 343
  Cursor = crDrag
  Caption = 'Escolha de Boletos Banc'#225'rios'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TStringGrid
    Left = 0
    Top = 51
    Width = 387
    Height = 224
    ColCount = 3
    TabOrder = 0
    ColWidths = (
      64
      64
      64)
  end
  object Btsair: TBitBtn
    Left = 268
    Top = 277
    Width = 118
    Height = 37
    Caption = '&Sair'
    TabOrder = 1
    OnClick = BtsairClick
  end
  object Panel1: TPanel
    Left = -1
    Top = 0
    Width = 388
    Height = 51
    Caption = 'Panel1'
    Color = 3355443
    TabOrder = 2
    object Label1: TLabel
      Left = 6
      Top = 18
      Width = 66
      Height = 14
      Caption = 'Boleto Atual'
      Color = clInfoBk
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object edtboletoatual: TEdit
      Left = 81
      Top = 16
      Width = 83
      Height = 19
      Color = clSkyBlue
      TabOrder = 0
      OnKeyDown = edtboletoatualKeyDown
    end
    object btok: TBitBtn
      Left = 170
      Top = 7
      Width = 88
      Height = 39
      Caption = '&OK'
      Default = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btokClick
    end
    object btajusta: TBitBtn
      Left = 262
      Top = 7
      Width = 118
      Height = 39
      Caption = '&Ajusta Pr'#243'ximos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btajustaClick
    end
  end
  object edtcodigoconvenio: TEdit
    Left = 6
    Top = 295
    Width = 121
    Height = 19
    TabOrder = 3
    Text = 'edtcodigoconvenio'
    Visible = False
  end
end
