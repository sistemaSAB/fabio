object FExportaContabilidade: TFExportaContabilidade
  Left = 247
  Top = 142
  Width = 681
  Height = 569
  Caption = 
    'Lan'#231'amentos Cont'#225'beis (EXPORTAR PARA SISTEMA CONT'#193'BIL) - EXCLAIM' +
    ' TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 665
    Height = 408
    Align = alClient
    PageIndex = 4
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object Label1: TLabel
        Left = 4
        Top = 9
        Width = 39
        Height = 14
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label18: TLabel
        Left = 4
        Top = 97
        Width = 70
        Height = 14
        Caption = 'Conta debite'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 141
        Width = 76
        Height = 14
        Caption = 'Conta Credite'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 4
        Top = 53
        Width = 23
        Height = 14
        Caption = 'Data'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbnomedebite: TLabel
        Left = 81
        Top = 114
        Width = 82
        Height = 13
        Caption = 'Conta debite'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbnomecredite: TLabel
        Left = 81
        Top = 158
        Width = 82
        Height = 13
        Caption = 'Conta debite'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 4
        Top = 185
        Width = 28
        Height = 14
        Caption = 'Valor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 4
        Top = 229
        Width = 76
        Height = 14
        Caption = 'Exporta (S/N)?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 4
        Top = 273
        Width = 63
        Height = 14
        Caption = 'ObjGerador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 139
        Top = 273
        Width = 67
        Height = 14
        Caption = 'CodGerador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 267
        Top = 273
        Width = 55
        Height = 14
        Caption = 'Exportado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 403
        Top = 273
        Width = 95
        Height = 14
        Caption = 'Nome do Arquivo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 112
        Top = 229
        Width = 45
        Height = 14
        Caption = 'N'#186' Docto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 4
        Top = 23
        Width = 73
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object edtcontadebite: TMaskEdit
        Left = 4
        Top = 111
        Width = 73
        Height = 19
        Hint = 
          'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
          #225'bil)'
        BiDiMode = bdRightToLeft
        CharCase = ecUpperCase
        MaxLength = 9
        ParentBiDiMode = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnExit = edtcontadebiteExit
        OnKeyDown = edtcontadebiteKeyDown
        OnKeyPress = edtcontadebiteKeyPress
      end
      object edtdata: TMaskEdit
        Left = 4
        Top = 67
        Width = 73
        Height = 19
        EditMask = '!99/99/99;1;_'
        MaxLength = 8
        TabOrder = 1
        Text = '  /  /  '
      end
      object edtcontacredite: TMaskEdit
        Left = 4
        Top = 155
        Width = 73
        Height = 19
        Hint = 
          'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
          #225'bil)'
        BiDiMode = bdRightToLeft
        CharCase = ecUpperCase
        MaxLength = 9
        ParentBiDiMode = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnExit = edtcontacrediteExit
        OnKeyDown = edtcontacrediteKeyDown
        OnKeyPress = edtcontadebiteKeyPress
      end
      object edtvalor: TMaskEdit
        Left = 4
        Top = 199
        Width = 58
        Height = 19
        Hint = 
          'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
          #225'bil)'
        BiDiMode = bdRightToLeft
        CharCase = ecUpperCase
        MaxLength = 9
        ParentBiDiMode = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
      end
      object edthistorico: TMaskEdit
        Left = 72
        Top = 199
        Width = 505
        Height = 19
        Hint = 
          'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
          #225'bil)'
        BiDiMode = bdRightToLeft
        CharCase = ecUpperCase
        MaxLength = 255
        ParentBiDiMode = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
      end
      object comboexporta: TComboBox
        Left = 4
        Top = 243
        Width = 99
        Height = 21
        ItemHeight = 13
        TabOrder = 6
        Text = ' '
        Items.Strings = (
          'Sim'
          'N'#227'o')
      end
      object edtobjgerador: TEdit
        Left = 4
        Top = 288
        Width = 121
        Height = 19
        TabOrder = 8
        Text = 'edtobjgerador'
      end
      object edtcodgerador: TEdit
        Left = 139
        Top = 288
        Width = 121
        Height = 19
        TabOrder = 9
        Text = 'Edit1'
      end
      object edtexportado: TEdit
        Left = 267
        Top = 288
        Width = 121
        Height = 19
        TabOrder = 10
        Text = 'Edit1'
      end
      object edtnomedoarquivo: TEdit
        Left = 403
        Top = 288
        Width = 174
        Height = 19
        TabOrder = 11
        Text = 'Edit1'
      end
      object edtnumdocto: TMaskEdit
        Left = 112
        Top = 243
        Width = 148
        Height = 19
        Hint = 
          'C'#243'digo do Plano de Contas Cont'#225'bil (Somente para Exporta'#231#227'o Cont' +
          #225'bil)'
        BiDiMode = bdRightToLeft
        CharCase = ecUpperCase
        MaxLength = 9
        ParentBiDiMode = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Confer'#234'ncia'
      object STGridConferencia: TStringGrid
        Left = 0
        Top = 87
        Width = 657
        Height = 293
        Align = alClient
        FixedCols = 0
        TabOrder = 1
      end
      object Panelfiltro: TPanel
        Left = 0
        Top = 0
        Width = 657
        Height = 87
        Align = alTop
        TabOrder = 0
        object Label11: TLabel
          Left = 96
          Top = 27
          Width = 7
          Height = 13
          Caption = 'a'
        end
        object Btpesquisa_conferencia: TButton
          Left = 484
          Top = 55
          Width = 109
          Height = 30
          Caption = 'Pesq&uisa'
          TabOrder = 2
          OnClick = Btpesquisa_conferenciaClick
        end
        object edtdatafinal: TMaskEdit
          Left = 112
          Top = 25
          Width = 74
          Height = 19
          EditMask = '99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 1
          Text = '  /  /    '
        end
        object edtdatainicial: TMaskEdit
          Left = 8
          Top = 24
          Width = 76
          Height = 19
          EditMask = '99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 0
          Text = '  /  /    '
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&3 - Confronto com Backup'
      object PanelConfronto: TPanel
        Left = 0
        Top = 0
        Width = 657
        Height = 380
        Align = alClient
        TabOrder = 0
        object Label13: TLabel
          Left = 4
          Top = 0
          Width = 115
          Height = 13
          Caption = 'Caminho do Backup'
        end
        object edtcaminhobancobackup: TEdit
          Left = 4
          Top = 16
          Width = 497
          Height = 19
          TabOrder = 0
          Text = 'edtcaminhobancobackup'
        end
        object btabrirbancobackup: TButton
          Left = 532
          Top = 11
          Width = 60
          Height = 25
          Caption = '&Abrir'
          TabOrder = 1
          OnClick = btabrirbancobackupClick
        end
        object btcaminhobancobackup: TButton
          Left = 504
          Top = 11
          Width = 25
          Height = 25
          Caption = '...'
          TabOrder = 2
          OnClick = btcaminhobancobackupClick
        end
        object Button1: TButton
          Left = 4
          Top = 44
          Width = 593
          Height = 25
          Caption = 'Confronto por ObjGerador'
          TabOrder = 3
          OnClick = Button1Click
        end
        object STRGConfronto: TStringGrid
          Left = 1
          Top = 162
          Width = 655
          Height = 217
          Align = alBottom
          TabOrder = 4
          OnDblClick = STRGConfrontoDblClick
        end
        object BtConfrontotabela: TButton
          Left = 4
          Top = 68
          Width = 593
          Height = 25
          Caption = 'Confronto por Tabela'
          TabOrder = 5
          OnClick = BtConfrontotabelaClick
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&4 - Auditoria'
      object PanelAuditoria: TPanel
        Left = 0
        Top = 0
        Width = 657
        Height = 89
        Align = alTop
        TabOrder = 0
        object Label5: TLabel
          Left = 8
          Top = 8
          Width = 43
          Height = 13
          Caption = 'Per'#237'odo'
        end
        object btlocalizaauditoria: TButton
          Left = 226
          Top = 22
          Width = 75
          Height = 25
          Caption = 'Localizar'
          TabOrder = 0
          OnClick = btlocalizaauditoriaClick
        end
        object edtdatainicial_auditoria: TMaskEdit
          Left = 8
          Top = 24
          Width = 98
          Height = 19
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 1
          Text = '  /  /    '
        end
        object edtdatafinal_auditoria: TMaskEdit
          Left = 120
          Top = 24
          Width = 98
          Height = 19
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 2
          Text = '  /  /    '
        end
        object btlancar_auditoria: TButton
          Left = 306
          Top = 22
          Width = 135
          Height = 25
          Caption = 'Lan'#231'ar Selecionados'
          TabOrder = 3
          OnClick = btlancar_auditoriaClick
        end
        object btselecionar_todos_auditoria: TButton
          Left = 4
          Top = 62
          Width = 119
          Height = 25
          Caption = 'Selecionar Todos'
          TabOrder = 4
          OnClick = btselecionar_todos_auditoriaClick
        end
      end
      object StrgAuditoria: TStringGrid
        Left = 0
        Top = 89
        Width = 657
        Height = 291
        Align = alClient
        TabOrder = 1
        OnDblClick = StrgAuditoriaDblClick
        OnKeyPress = StrgAuditoriaKeyPress
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&5 - Configura'#231#227'o Exporta'#231#227'o Personalizada'
      object StrgConfiguracao: TStringGrid
        Left = 0
        Top = 79
        Width = 657
        Height = 207
        Align = alClient
        ColCount = 3
        FixedCols = 0
        TabOrder = 0
        OnKeyDown = StrgConfiguracaoKeyDown
      end
      object panelconfiguracao: TPanel
        Left = 0
        Top = 0
        Width = 657
        Height = 79
        Align = alTop
        TabOrder = 1
        object Label14: TLabel
          Left = 1
          Top = 65
          Width = 655
          Height = 13
          Align = alBottom
          Alignment = taCenter
          Caption = 'Pressione F5 para alterar a coluna | DEL para Apagar a Linha'
        end
        object Label15: TLabel
          Left = 8
          Top = 8
          Width = 219
          Height = 13
          Caption = 'Campos Dispon'#237'veis (ENTER Adiciona)'
        end
        object Label16: TLabel
          Left = 296
          Top = 8
          Width = 112
          Height = 13
          Caption = 'Caracter separador'
        end
        object Button2: TButton
          Left = 209
          Top = 23
          Width = 75
          Height = 25
          Caption = 'Salvar'
          TabOrder = 0
          OnClick = Button2Click
        end
        object combocampos: TComboBox
          Left = 8
          Top = 24
          Width = 193
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Text = '   '
          OnKeyPress = combocamposKeyPress
        end
        object edtcaracterseparador: TEdit
          Left = 296
          Top = 24
          Width = 33
          Height = 19
          TabOrder = 2
        end
        object btexportar: TButton
          Left = 417
          Top = 23
          Width = 75
          Height = 25
          Caption = 'Exportar'
          TabOrder = 3
          OnClick = btexportarClick
        end
      end
      object MemoLog: TMemo
        Left = 0
        Top = 286
        Width = 657
        Height = 94
        Align = alBottom
        Lines.Strings = (
          'Memo1')
        TabOrder = 2
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 408
    Width = 665
    Height = 123
    Align = alBottom
    TabOrder = 1
    object btabrir: TBitBtn
      Left = 462
      Top = 2
      Width = 115
      Height = 38
      Caption = 'A&brir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btabrirClick
    end
    object btsair: TBitBtn
      Left = 462
      Top = 81
      Width = 115
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btsairClick
    end
    object btexcluir: TBitBtn
      Left = 462
      Top = 42
      Width = 115
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btexcluirClick
    end
    object btcancelar: TBitBtn
      Left = 347
      Top = 2
      Width = 115
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btgravar: TBitBtn
      Left = 232
      Top = 2
      Width = 115
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btgravarClick
    end
    object btrelatorios: TBitBtn
      Left = 347
      Top = 42
      Width = 115
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btrelatoriosClick
    end
    object BtImportaRegistros: TBitBtn
      Left = 232
      Top = 81
      Width = 115
      Height = 38
      Caption = 'Importa Registros'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = BtImportaRegistrosClick
    end
    object BtRetornaExportacao: TBitBtn
      Left = 117
      Top = 81
      Width = 115
      Height = 38
      Caption = 'Retorna Exporta'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = BtRetornaExportacaoClick
    end
    object Btexportados: TBitBtn
      Left = 117
      Top = 42
      Width = 115
      Height = 38
      Caption = 'E&xportados'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = BtexportadosClick
    end
    object btalterar: TBitBtn
      Left = 117
      Top = 2
      Width = 115
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 9
      OnClick = btalterarClick
    end
    object Btnovo: TBitBtn
      Left = 2
      Top = 2
      Width = 115
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 10
      OnClick = BtnovoClick
    end
    object BtExportacao: TBitBtn
      Left = 2
      Top = 81
      Width = 115
      Height = 38
      Caption = 'Exporta&'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 11
      OnClick = BtExportacaoClick
    end
    object btnaoexportados: TBitBtn
      Left = 2
      Top = 42
      Width = 115
      Height = 38
      Caption = 'Penden&tes'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 12
      OnClick = btnaoexportadosClick
    end
    object btpesquisar: TBitBtn
      Left = 232
      Top = 42
      Width = 115
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 13
      OnClick = btpesquisarClick
    end
    object bFechamento: TBitBtn
      Left = 347
      Top = 81
      Width = 115
      Height = 38
      Caption = 'Fechamento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 14
      OnClick = bFechamentoClick
    end
  end
  object SaveDialog: TSaveDialog
    Left = 553
    Top = 90
  end
  object QueryConferencia: TIBQuery
    Left = 492
    Top = 304
  end
  object IBDatabaseBackup: TIBDatabase
    DefaultTransaction = IBTransactionBackup
    Left = 532
    Top = 304
  end
  object IBTransactionBackup: TIBTransaction
    Left = 492
    Top = 272
  end
  object OpenDialog: TOpenDialog
    Left = 524
    Top = 90
  end
end
