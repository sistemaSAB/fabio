unit UMotivoDevolucao;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjMotivoDevolucao;

type
  TFMotivoDevolucao = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    btcancelar: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    comboreapresenta: TComboBox;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         function  AtualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMotivoDevolucao: TFMotivoDevolucao;
  ObjMotivoDevolucao:TObjMotivoDevolucao;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFMotivoDevolucao.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjMotivoDevolucao do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Nome            ( edtnome.text);
         Submit_Reapresenta     ( comboreapresenta.text[1]);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFMotivoDevolucao.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjMotivoDevolucao do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtNome.text            :=Get_Nome          ;
        IF (Get_Reapresenta='S')
        Then comboreapresenta.itemindex:=0
        Else comboreapresenta.itemindex:=1;


        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFMotivoDevolucao.TabelaParaControles: Boolean;
begin
     If (ObjMotivoDevolucao.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
//****************************************



procedure TFMotivoDevolucao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjMotivoDevolucao=Nil)
     Then exit;

     If (ObjMotivoDevolucao.status<>dsinactive)
      Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

     ObjMotivoDevolucao.free;
end;

procedure TFMotivoDevolucao.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
          Perform(Wm_NextDlgCtl,0,0);
          //key:=#0;
      end;
end;



procedure TFMotivoDevolucao.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjMotivoDevolucao.Get_novocodigo;
     edtcodigo.enabled:=True;

     
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     ObjMotivoDevolucao.status:=dsInsert;
     Edtcodigo.setfocus;

end;


procedure TFMotivoDevolucao.btalterarClick(Sender: TObject);
begin
    If (ObjMotivoDevolucao.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjMotivoDevolucao.Status:=dsEdit;
                edtnome.setfocus;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
          End;


end;

procedure TFMotivoDevolucao.btgravarClick(Sender: TObject);
begin

     If ObjMotivoDevolucao.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjMotivoDevolucao.salvar(true)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFMotivoDevolucao.btexcluirClick(Sender: TObject);
begin
     If (ObjMotivoDevolucao.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjMotivoDevolucao.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjMotivoDevolucao.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFMotivoDevolucao.btcancelarClick(Sender: TObject);
begin
     ObjMotivoDevolucao.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFMotivoDevolucao.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFMotivoDevolucao.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjMotivoDevolucao.Get_pesquisa,ObjMotivoDevolucao.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjMotivoDevolucao.status<>dsinactive
                                  then exit;

                                  If (ObjMotivoDevolucao.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

procedure TFMotivoDevolucao.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

       limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjMotivoDevolucao:=TObjMotivoDevolucao.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

function TFMotivoDevolucao.AtualizaQuantidade: string;
begin
     Result:='Existem '+ContaRegistros('tabmotivodevolucao','codigo') +' Motivos de devolu��o Cadastrados';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Obj.Get_PesquisaMotivoDevolucao,Obj.Get_TituloPesquisaMotivoDevolucao,Self)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If Self.edtTipoLancto.text<>''
                                 Then Self.LbTipoLanctoNome.caption:=OBjLancamento.PegaNometipolancto(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;
}
