unit UgeraTransferencia;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjGeraTransferencia;

type
  TFgeratransferencia = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    Panel2: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    EdtCodigo: TEdit;
    edthistorico: TEdit;
    edtportadororigem: TEdit;
    edtgrupo: TEdit;
    edtportadordestino: TEdit;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    BtCancelar: TBitBtn;
    lbportadorOrigem: TLabel;
    lbGrupo: TLabel;
    lbportadorDestino: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure edtportadororigemKeyPress(Sender: TObject; var Key: Char);
    procedure edtportadororigemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadordestinoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtgrupoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtportadororigemExit(Sender: TObject);
    procedure edtportadordestinoExit(Sender: TObject);
    procedure edtgrupoExit(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         procedure Limpalabels;
         function  atualizaQuantidade:string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGeraTransferencia: TFGeraTransferencia;
  ObjGeraTransferencia:TObjGeraTransferencia;

implementation

uses UessencialGlobal, Upesquisa, Uportador, UGrupo,
  UescolheImagemBotao, UObjGrupo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFGeraTransferencia.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjGeraTransferencia do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Historico       ( edthistorico.text);
         Submit_portadordestino ( edtportadordestino.text);
         Submit_Grupo           ( edtgrupo.text);
         Submit_portadororigem  ( edtportadororigem.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFGeraTransferencia.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjGeraTransferencia do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edthistorico.text       :=Get_Historico       ;
        edtportadordestino.text :=Get_Portadordestino ;
        edtgrupo.text           :=Get_Grupo;
        edtportadororigem.text  :=Get_portadororigem;
        lbportadorOrigem.Caption:=PortadorOrigem.Get_Nome;
        lbportadorDestino.Caption:=PortadorDestino.Get_Nome;
        lbGrupo.Caption:=Grupo.Get_Descricao;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFGeraTransferencia.TabelaParaControles: Boolean;
begin
     ObjGeraTransferencia.TabelaparaObjeto;
     ObjetoParaControles;
end;

//****************************************
//****************************************

procedure TFGeraTransferencia.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjGeraTransferencia=Nil)
     Then exit;

    If (ObjGeraTransferencia.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    ObjGeraTransferencia.free;
end;

procedure TFGeraTransferencia.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFGeraTransferencia.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFGeraTransferencia.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.Limpalabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjGeraTransferencia.Get_novocodigo;
     edtcodigo.enabled:=False;
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjGeraTransferencia.status:=dsInsert;
     Edthistorico.setfocus;
end;

procedure TFGeraTransferencia.BtCancelarClick(Sender: TObject);
begin
     ObjGeraTransferencia.cancelar;
     
     limpaedit(Self);
     Self.Limpalabels;
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFGeraTransferencia.BtgravarClick(Sender: TObject);
begin

     If ObjGeraTransferencia.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjGeraTransferencia.salvar(true)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     Self.Limpalabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFGeraTransferencia.PreparaAlteracao;
begin
     habilita_campos(Self);
     EdtCodigo.enabled:=False;
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     ObjGeraTransferencia.Status:=dsEdit;
     edthistorico.setfocus;
end;

procedure TFGeraTransferencia.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjGeraTransferencia.Get_pesquisa,ObjGeraTransferencia.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjGeraTransferencia.status<>dsinactive
                                  then exit;

                                  If (ObjGeraTransferencia.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados !',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFGeraTransferencia.btalterarClick(Sender: TObject);
begin
    If (ObjGeraTransferencia.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFGeraTransferencia.btexcluirClick(Sender: TObject);
begin
     If (ObjGeraTransferencia.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjGeraTransferencia.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjGeraTransferencia.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.Limpalabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFGeraTransferencia.edtportadororigemKeyPress(Sender: TObject;
  var Key: Char);
begin
     If not (Key In ['0'..'9',#8])
     Then key:=#0; 
end;

procedure TFGeraTransferencia.edtportadororigemKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
FpesquisaLocal:TFpesquisa;
Fportador:TFportador;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador:=TFportador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(OBjgeraTransferencia.Get_PesquisaPortadorDestino,OBjgeraTransferencia.Get_TituloPesquisaPortadorDestino,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtportadororigem.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;
End;



procedure TFgeratransferencia.edtportadordestinoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
FpesquisaLocal:TFpesquisa;
Fportador:TFportador;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fportador:=TFportador.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(OBjgeraTransferencia.Get_PesquisaPortadorDestino,OBjgeraTransferencia.Get_TituloPesquisaPortadorDestino,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtportadordestino.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;
End;

procedure TFgeratransferencia.edtgrupoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
FpesquisaLocal:TFpesquisa;
Fgrupo:TFgrupo;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            Fgrupo:=TFgrupo.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(OBjgeraTransferencia.Get_PesquisaGrupo,OBjgeraTransferencia.Get_TituloPesquisaGrupo,Fgrupo)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then edtgrupo.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fgrupo);
     End;
End;

procedure TFgeratransferencia.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);

      limpaedit(Self);
     Self.Limpalabels;
     desabilita_campos(Self);

     Try
        ObjGeraTransferencia:=TObjGeraTransferencia.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

procedure TFgeratransferencia.edtportadororigemExit(Sender: TObject);
begin
      lbportadorOrigem.Caption:='';
      if edtportadororigem.Text<>''
      then begin
            if ObjGeraTransferencia.PortadorOrigem.LocalizaCodigo(edtportadororigem.Text)=True
            then ObjGeraTransferencia.PortadorOrigem.TabelaparaObjeto;
            lbportadorOrigem.Caption:=ObjGeraTransferencia.PortadorOrigem.Get_Nome;
      end;
end;

procedure TFgeratransferencia.edtportadordestinoExit(Sender: TObject);
begin
      lbportadorDestino.Caption:='';
      if edtportadorDestino.Text<>''
      then begin
            if ObjGeraTransferencia.PortadorDestino.LocalizaCodigo(edtportadorDestino.Text)=True
            then ObjGeraTransferencia.PortadorDestino.TabelaparaObjeto;
            lbportadorDestino.Caption:=ObjGeraTransferencia.PortadorDestino.Get_Nome;
      end;
end;

procedure TFgeratransferencia.edtgrupoExit(Sender: TObject);
begin
      lbGrupo.Caption:='';
      if edtgrupo.Text<>''
      then begin
            if ObjGeraTransferencia.Grupo.LocalizaCodigo(edtgrupo.Text)=True
            then ObjGeraTransferencia.Grupo.TabelaparaObjeto;
            lbGrupo.Caption:=ObjGeraTransferencia.Grupo.Get_Descricao;
      end;
end;

procedure TFgeratransferencia.Limpalabels;
begin
     lbportadorOrigem.Caption:='';
     lbportadorDestino.Caption:='';
     lbGrupo.Caption:='';
end;

function TFgeratransferencia.atualizaQuantidade: string;
begin
     result:='Existem '+ContaRegistros('tabgeratransferencia','codigo')+' Geradores de Transfer�ncia Cadastrados';
end;

end.
