unit UAcertoCreditoDebito;

interface

uses
  UessencialGlobal, Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Mask, StdCtrls, Buttons, Grids,UObjLancamento, DB,
  IBCustomDataSet, UObjGeraLancamento,IBQuery, UObjGeraTitulo, UObjACERTOPENDENCIAS,
  ExtCtrls;

type
  TFAcertoCreditoDebito = class(TForm)
    StrGridPAGAR: TStringGrid;
    StrGRIDRECEBER: TStringGrid;
    btprocessar: TBitBtn;
    IBQuery: TIBQuery;
    Label7: TLabel;
    Label8: TLabel;
    Panel1: TPanel;
    Label2: TLabel;
    edtvencimento1: TMaskEdit;
    Label1: TLabel;
    edtvencimento2: TMaskEdit;
    Label3: TLabel;
    combocredordevedor: TComboBox;
    combocredordevedorcodigo: TComboBox;
    edtcodigocredordevedor: TEdit;
    Label4: TLabel;
    LbNomeCredorDevedor: TLabel;
    Btpesquisar: TBitBtn;
    CheckSelecionaPagar: TCheckBox;
    Bevel8: TBevel;
    Bevel1: TBevel;
    Panel2: TPanel;
    Label5: TLabel;
    lbtotalapagar: TLabel;
    Panel3: TPanel;
    Label6: TLabel;
    lbtotalareceber: TLabel;
    CheckSelecionaReceber: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtcodigocredordevedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcodigocredordevedorExit(Sender: TObject);
    procedure BtpesquisarClick(Sender: TObject);
    procedure combocredordevedorChange(Sender: TObject);
    procedure StrGridPAGARDblClick(Sender: TObject);
    procedure StrGRIDRECEBERDblClick(Sender: TObject);
    procedure StrGridPAGARKeyPress(Sender: TObject; var Key: Char);
    procedure StrGRIDRECEBERKeyPress(Sender: TObject; var Key: Char);
    procedure btprocessarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CheckSelecionaPagarClick(Sender: TObject);
    procedure CheckSelecionaReceberClick(Sender: TObject);
  private
    { Private declarations }
    Procedure Somatudo;
    Function PegaContaGerencial(out PcontaGer:string;Debito_Credito:string):Boolean;
  public
    { Public declarations }
  end;

var
  FAcertoCreditoDebito: TFAcertoCreditoDebito;
  ObjLancamento:Tobjlancamento;
  ObjGeraLancamento:TObjGeraLancamento;
  Objgeratitulo:TObjGeraTitulo;
  ObjAcertoPendencias:TObjACERTOPENDENCIAS;

implementation

uses  Upesquisa, UDataModulo, UObjTitulo, UFiltraImp;

{$R *.dfm}

procedure TFAcertoCreditoDebito.FormShow(Sender: TObject);
begin
     Uessencialglobal.PegaCorForm(Self);
     limpaedit(Self);
     LbNomeCredorDevedor.caption:='';

     Try
        ObjLancamento:=TObjLancamento.Create;
        ObjGeraLancamento:=TObjGeraLancamento.create;
        Objgeratitulo:=TObjGeraTitulo.create;
        ObjAcertoPendencias:=TObjACERTOPENDENCIAS.create;
     Except
           desabilita_campos(Self);
           Messagedlg('Erro na Tentativa de Cria��o do Objeto de Lan�amento!',mterror,[mbok],0);
           exit;
     End;
     IBQuery.Database:=FdataModulo.Ibdatabase;
     ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedor(combocredordevedor.items);
     ObjLancamento.Pendencia.Titulo.Get_ListacredorDevedorCodigo(combocredordevedorcodigo.items);
     edtvencimento1.setfocus;

     StrGridPAGAR.Cols[0].clear;
     StrGridPAGAR.Cols[1].clear;
     StrGridPAGAR.Cols[2].clear;
     StrGridPAGAR.Cols[3].clear;
     StrGridPAGAR.Cols[4].clear;
     StrGridPAGAR.Cols[5].clear;
     StrGRIDRECEBER.Cols[0].clear;
     StrGRIDRECEBER.Cols[1].clear;
     StrGRIDRECEBER.Cols[2].clear;
     StrGRIDRECEBER.Cols[3].clear;
     StrGRIDRECEBER.Cols[4].clear;
     StrGRIDRECEBER.Cols[5].clear;

     StrGridPAGAR.Cells[0,0]:='X';
     StrGridPAGAR.Cells[1,0]:='T�TULO';
     StrGridPAGAR.Cells[2,0]:='PEND.';
     StrGridPAGAR.Cells[3,0]:='VENCIMENTO';
     StrGridPAGAR.Cells[4,0]:='VALOR';
     StrGridPAGAR.Cells[5,0]:='SALDO';
     StrGridPAGAR.Cells[6,0]:='HIST�RICO';

     StrGRIDRECEBER.Cells[0,0]:='X';
     StrGRIDRECEBER.Cells[1,0]:='T�TULO';
     StrGRIDRECEBER.Cells[2,0]:='PEND.';
     StrGRIDRECEBER.Cells[3,0]:='VENCIMENTO';
     StrGRIDRECEBER.Cells[4,0]:='VALOR';
     StrGRIDRECEBER.Cells[5,0]:='SALDO';
     StrGRIDRECEBER.Cells[6,0]:='HIST�RICO';
     StrGridPAGAR.RowCount:=2;
     StrGRIDRECEBER.RowCount:=2;

     AjustaLArguraColunaGrid(StrGridPAGAR);
     AjustaLArguraColunaGrid(StrGRIDRECEBER);
     Self.Somatudo;
     PegaFiguraBotao(Btpesquisar,'botaopesquisar.bmp');
     PegaFiguraBotao(btprocessar,'botaoconcluir.bmp');

end;

procedure TFAcertoCreditoDebito.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (ObjLancamento<>nil)
     Then ObjLancamento.Free;

     if (ObjGeraLancamento<>nil)
     Then ObjGeraLancamento.Free;

     if (Objgeratitulo<>nil)
     Then Objgeratitulo.free;

     if (ObjAcertoPendencias<>nil)
     Then ObjAcertoPendencias.free;
end;

procedure TFAcertoCreditoDebito.edtcodigocredordevedorKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;


     If (combocredordevedor.itemindex=-1)
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisaN(ObjLancamento.Pendencia.Titulo.Get_PesquisaCredorDevedorinstrucaoSql(combocredordevedorcodigo.items[combocredordevedor.itemindex]),'Pesquisa de '+Combocredordevedor.text,ObjLancamento.Pendencia.Titulo.Get_FormularioCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex]))=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                edtcodigocredordevedor.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNomeCredorDevedor.caption:=FpesquisaLocal.QueryPesq.fieldbyname(ObjLancamento.Pendencia.Titulo.Get_CampoNomeCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex])).asstring;
                             End;

                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TFAcertoCreditoDebito.edtcodigocredordevedorExit(
  Sender: TObject);
begin
     //preciso sair com o nome do credordevedor escolhido
     //atraves do FrLancamento1.ObjLancamento.Pendencia.Titulo
     LbNomeCredorDevedor.caption:='';

     If (edtcodigocredordevedor.text='') or (combocredordevedor.ItemIndex=-1)
     Then exit;

     LbNomeCredorDevedor.caption:=ObjLancamento.Pendencia.Titulo.Get_NomeCredorDevedor(combocredordevedorcodigo.items[combocredordevedor.itemindex],edtcodigocredordevedor.text);
end;

procedure TFAcertoCreditoDebito.BtpesquisarClick(Sender: TObject);
var
cont:integer;
begin

     if (edtcodigocredordevedor.Text='')
     Then Begin
               Messagedlg('Escolha um Credor/Devedor!',mtInformation,[mbok],0);
               exit;
     End;
     StrGRIDRECEBER.Cols[0].clear;
     StrGRIDRECEBER.Cols[1].clear;
     StrGRIDRECEBER.Cols[2].clear;
     StrGRIDRECEBER.Cols[3].clear;
     StrGRIDRECEBER.Cols[4].clear;
     StrGRIDRECEBER.Cols[5].clear;


     StrGRIDRECEBER.Cells[0,0]:='X';
     StrGRIDRECEBER.Cells[1,0]:='T�TULO';
     StrGRIDRECEBER.Cells[2,0]:='PEND.';
     StrGRIDRECEBER.Cells[3,0]:='VENCIMENTO';
     StrGRIDRECEBER.Cells[4,0]:='VALOR';
     StrGRIDRECEBER.Cells[5,0]:='SALDO';

     With IBQuery do
     Begin
          close;
          SQL.clear;
          sql.add('Select tabpendencia.titulo,tabpendencia.codigo as pendencia,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo,tabtitulo.historico from tabpendencia');
          SQL.add('join TabTitulo on TabPendencia.Titulo=TabTitulo.codigo');
          sql.add('join tabcontager on TabTitulo.ContaGerencial=TabContaGer.codigo');
          sql.add('where tabcontager.tipo=''D'' and tabpendencia.saldo>0');
          sql.add('and TabTitulo.credordevedor='+combocredordevedorcodigo.Items[combocredordevedor.itemindex]);
          sql.add('and TabTitulo.CodigocredorDevedor='+edtcodigocredordevedor.Text);
          Try
             StrToDate(edtvencimento1.text);
             StrToDate(edtvencimento2.text);
             sql.add('and TabPendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento1.text))+#39);
             sql.add('and TabPendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento2.text))+#39);
          Except

          End;
          open;
          last;
          if (RecordCount>0)
          Then StrGridPAGAR.RowCount:=Recordcount+1
          Else StrGridPAGAR.RowCount:=2;
          
          first;
          StrGridPAGAR.Cols[0].clear;
          StrGridPAGAR.Cols[1].clear;
          StrGridPAGAR.Cols[2].clear;
          StrGridPAGAR.Cols[3].clear;
          StrGridPAGAR.Cols[4].clear;
          StrGridPAGAR.Cols[5].clear;
          StrGridPAGAR.Cols[6].clear;
          
          StrGridPAGAR.Cells[0,0]:='X';
          StrGridPAGAR.Cells[1,0]:='T�TULO';
          StrGridPAGAR.Cells[2,0]:='PEND.';
          StrGridPAGAR.Cells[3,0]:='VENCIMENTO';
          StrGridPAGAR.Cells[4,0]:='VALOR';
          StrGridPAGAR.Cells[5,0]:='SALDO';
          StrGridPAGAR.Cells[6,0]:='HIST�RICO';

          cont:=1;
          While not(eof) do
          Begin
               StrGridPAGAR.Cells[0,cont]:='';
               StrGridPAGAR.Cells[1,cont]:=fieldbyname('titulo').asstring;
               StrGridPAGAR.Cells[2,cont]:=fieldbyname('pendencia').asstring;
               StrGridPAGAR.Cells[3,cont]:=fieldbyname('vencimento').asstring;
               StrGridPAGAR.Cells[4,cont]:=formata_valor(fieldbyname('valor').asstring);
               StrGridPAGAR.Cells[5,cont]:=formata_valor(fieldbyname('saldo').asstring);
               StrGridPAGAR.Cells[6,cont]:=fieldbyname('historico').asstring;
               next;
               inc(cont,1);
          End;

          //**************A RECEBER*************************
          close;
          SQL.clear;
          sql.add('Select tabpendencia.titulo,tabpendencia.codigo as pendencia,tabpendencia.vencimento,tabpendencia.valor,tabpendencia.saldo,tabtitulo.historico from tabpendencia');
          SQL.add('join TabTitulo on TabPendencia.Titulo=TabTitulo.codigo');
          sql.add('join tabcontager on TabTitulo.ContaGerencial=TabContaGer.codigo');
          sql.add('where tabcontager.tipo=''C'' and tabpendencia.saldo>0');
          sql.add('and TabTitulo.credordevedor='+combocredordevedorcodigo.Items[combocredordevedor.itemindex]);
          sql.add('and TabTitulo.CodigocredorDevedor='+edtcodigocredordevedor.Text);
          Try
             StrToDate(edtvencimento1.text);
             StrToDate(edtvencimento2.text);
             sql.add('and TabPendencia.vencimento>='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento1.text))+#39);
             sql.add('and TabPendencia.vencimento<='+#39+FormatDateTime('mm/dd/yyyy',strtodate(edtvencimento2.text))+#39);
          Except

          End;
          open;
          last;
          if (RecordCount>0)
          Then StrGRIDRECEBER.RowCount:=Recordcount+1
          Else StrGRIDRECEBER.RowCount:=2;

          first;
          StrGridRECEBER.Cols[0].clear;
          StrGridRECEBER.Cols[1].clear;
          StrGridRECEBER.Cols[2].clear;
          StrGridRECEBER.Cols[3].clear;
          StrGridRECEBER.Cols[4].clear;
          StrGridRECEBER.Cols[5].clear;
          StrGridRECEBER.Cols[6].clear;
          
          StrGridRECEBER.Cells[0,0]:='X';
          StrGridRECEBER.Cells[1,0]:='T�TULO';
          StrGridRECEBER.Cells[2,0]:='PEND.';
          StrGridRECEBER.Cells[3,0]:='VENCIMENTO';
          StrGridRECEBER.Cells[4,0]:='VALOR';
          StrGridRECEBER.Cells[5,0]:='SALDO';
          StrGridRECEBER.Cells[6,0]:='HIST�RICO';

          cont:=1;
          While not(eof) do
          Begin
               StrGridRECEBER.Cells[0,cont]:='';
               StrGridRECEBER.Cells[1,cont]:=fieldbyname('titulo').asstring;
               StrGridRECEBER.Cells[2,cont]:=fieldbyname('pendencia').asstring;
               StrGridRECEBER.Cells[3,cont]:=fieldbyname('vencimento').asstring;
               StrGridRECEBER.Cells[4,cont]:=formata_valor(fieldbyname('valor').asstring);
               StrGridRECEBER.Cells[5,cont]:=formata_valor(fieldbyname('saldo').asstring);
               StrGRIDRECEBER.Cells[6,cont]:=fieldbyname('historico').asstring;
               next;
               inc(cont,1);
          End;
          AjustaLArguraColunaGrid(StrGridPAGAR);
          AjustaLArguraColunaGrid(StrGRIDRECEBER);
     End;
End;

procedure TFAcertoCreditoDebito.combocredordevedorChange(Sender: TObject);
begin
     edtcodigocredordevedor.Text:='';
end;

procedure TFAcertoCreditoDebito.StrGridPAGARDblClick(Sender: TObject);
begin
     if (StrGridPAGAR.Cells[0,StrGridPAGAR.row]='X')
     Then StrGridPAGAR.Cells[0,StrGridPAGAR.row]:=''
     Else StrGridPAGAR.Cells[0,StrGridPAGAR.row]:='X';

     Self.Somatudo;
end;

procedure TFAcertoCreditoDebito.StrGRIDRECEBERDblClick(Sender: TObject);
begin
     if (StrGRIDRECEBER.Cells[0,StrGRIDRECEBER.row]='X')
     Then StrGRIDRECEBER.Cells[0,StrGRIDRECEBER.row]:=''
     Else StrGRIDRECEBER.Cells[0,StrGRIDRECEBER.row]:='X';

     Self.Somatudo;

end;

procedure TFAcertoCreditoDebito.StrGridPAGARKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then  StrGridPAGARDblClick(Sender);
end;

procedure TFAcertoCreditoDebito.StrGRIDRECEBERKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then  StrGRIDRECEBERDblClick(Sender);
end;

procedure TFAcertoCreditoDebito.Somatudo;
var
   cont:integer;
   tmpsoma:currency;
begin
     tmpsoma:=0;
     for cont:=1 to StrGridPAGAR.RowCount-1 do
     Begin
          if (StrGridPAGAR.Cells[0,cont]='X')
          Then TmpSoma:=TmpSoma+strtofloat(tira_ponto(StrGridPAGAR.cells[5,cont]));
     End;
     lbtotalapagar.caption:=formata_valor(FloatToStr(tmpsoma));

     tmpsoma:=0;
     for cont:=1 to StrGridreceber.RowCount-1 do
     Begin
          if (StrGridreceber.Cells[0,cont]='X')
          Then TmpSoma:=TmpSoma+strtofloat(tira_ponto(StrGridreceber.cells[5,cont]));
     End;
     lbtotalareceber.caption:=formata_valor(FloatToStr(tmpsoma));

end;

procedure TFAcertoCreditoDebito.btprocessarClick(Sender: TObject);
var
Resultado:Currency;
PcontaGer,PNovoTitulo:string;
cont:integer;
EscolheContaGerencial:Boolean;
begin
     //guardar as pendencias a pagar e a receber e o numero
     //do titulo resultante e uma tabela qq ou em um txt mesmo

     //procedimentos a serem adotados
     //PAGAR-RECEBER
     Resultado:=StrToFloat(tira_ponto(lbtotalapagar.caption))-StrToFloat(tira_ponto(lbtotalareceber.caption));

     if (Resultado=0)
     Then PNovoTitulo:=''
     Else PNovoTitulo:=ObjLancamento.Pendencia.Titulo.Get_NovoCodigo;

     //Dar desconto em todas as pendencias selecionadas
     //gerar um titulo com o Saldo (RESULTADO)
     //Se o Resultado>0 o Titulo devera ser a pagar
     //Se o Resultado<0 o Titulo devere ser a receber
     If (ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO')=False)
     Then Begin
                  Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="DESCONTO", n�o foi encontrado!',mterror,[mbok],0);
                  exit;
     End;
     ObjGeraLancamento.TabelaparaObjeto;


     if (ObjParametroGlobal.ValidaParametro('ESCOLHE CONTA GERENCIAL NO ACERTO DE D�BITO - CR�DITO?')=FALSE)
     Then Begin
               Messagedlg('O par�metro "ESCOLHE CONTA GERENCIAL NO ACERTO DE D�BITO - CR�DITO?" n�o foi encontrado', mterror,[mbok],0);
               exit;
     End;
     
     
     if (ObjParametroGlobal.Get_Valor='SIM')
     Then EscolheContaGerencial:=True
     Else EscolheContaGerencial:=False;



     if (Resultado<0)
     Then Begin
               if (Objgeratitulo.LocalizaHistorico('T�TULO A RECEBER DO ACERTO')=False)
               Then Begin
                       Messagedlg('O gerador de T�tulo "T�TULO A RECEBER DO ACERTO" n�o foi encontrado!',mterror,[mbok],0);
                       exit;
               End;
     End
     Else Begin
              if (Objgeratitulo.LocalizaHistorico('T�TULO A PAGAR DO ACERTO')=False)
              Then Begin
                       Messagedlg('O gerador de T�tulo "T�TULO A PAGAR DO ACERTO" n�o foi encontrado!',mterror,[mbok],0);
                       exit;
              End;
     End;
     Objgeratitulo.TabelaparaObjeto;
     
     if (EscolheContaGerencial=TRUE)
     Then Begin
               if (resultado<0)
               Then Begin
                       if (Self.PegaContaGerencial(PcontaGer,'C')=False)
                       Then exit;
               End
               Else Begin
                        if (Self.PegaContaGerencial(PcontaGer,'D')=False)
                        Then exit;
               End;
     End;
     
     for cont:=1 to StrGridPAGAR.RowCount-1 do
     Begin
          if (StrGridPAGAR.Cells[0,cont]='X')
          Then Begin
                   ObjLancamento.ZerarTabela;
                   ObjLancamento.status:=dsinsert;
                   ObjLancamento.Submit_CODIGO(ObjLancamento.Get_NovoCodigo);
                   ObjLancamento.Submit_Pendencia(StrGridPAGAR.Cells[2,cont]);
                   ObjLancamento.Submit_TipoLancto(ObjGeraLancamento.TipoLAncto.Get_CODIGO);
                   ObjLancamento.Submit_Valor(tira_ponto(StrGridPAGAR.Cells[5,cont]));
                   If (Resultado<>0)
                   Then ObjLancamento.Submit_Historico('ACERTO DE T�TULOS A RECEBER E A PAGAR T�TULO RESULTANTE '+PNovoTitulo)
                   Else ObjLancamento.Submit_Historico('ACERTO DE T�TULOS A RECEBER E A PAGAR');
                   ObjLancamento.Submit_Data(datetostr(now));
                   ObjLancamento.Submit_LancamentoPai('');
                   if (ObjLancamento.Salvar(False,False,'',False,False)=False)
                   Then Begin
                             FDataModulo.IBTransaction.RollbackRetaining;
                             MessageDlg('Erro na tentativa de lan�ar desconto para a pend�ncia N� '+StrGridPAGAR.Cells[2,cont],mterror,[mbok],0);
                             exit;
                   End;
          End;
     End;

     for cont:=1 to StrGridreceber.RowCount-1 do
     Begin
          if (StrGridreceber.Cells[0,cont]='X')
          Then Begin
                   ObjLancamento.ZerarTabela;
                   ObjLancamento.status:=dsinsert;
                   ObjLancamento.Submit_CODIGO(ObjLancamento.Get_NovoCodigo);
                   ObjLancamento.Submit_Pendencia(StrGridreceber.Cells[2,cont]);
                   ObjLancamento.Submit_TipoLancto(ObjGeraLancamento.TipoLAncto.Get_CODIGO);
                   ObjLancamento.Submit_Valor(tira_ponto(StrGridreceber.Cells[5,cont]));
                   If (Resultado<>0)
                   Then ObjLancamento.Submit_Historico('ACERTO DE T�TULOS A RECEBER E A PAGAR T�TULO RESULTANTE '+PNovoTitulo)
                   Else ObjLancamento.Submit_Historico('ACERTO DE T�TULOS A RECEBER E A PAGAR');
                   ObjLancamento.Submit_Data(datetostr(now));
                   ObjLancamento.Submit_LancamentoPai('');
                   if (ObjLancamento.Salvar(False,False,'',False,False)=False)
                   Then Begin
                             FDataModulo.IBTransaction.RollbackRetaining;
                             MessageDlg('Erro na tentativa de lan�ar desconto para a pend�ncia N� '+StrGridreceber.Cells[2,cont],mterror,[mbok],0);
                             exit;
                   End;
          End;
     End;

     //**************Lan�ando o t�tulo Financeiro Resultante********************************
     if (Resultado<>0)
     Then Begin
               With ObjLancamento.Pendencia.Titulo do
               Begin

                     ZerarTabela;
                     Status:=Dsinsert;
                     Submit_CODIGO(PNovoTitulo);
                     Submit_CREDORDEVEDOR(combocredordevedorcodigo.Items[combocredordevedor.itemindex]);
                     Submit_CODIGOCREDORDEVEDOR(edtcodigocredordevedor.text);
                     Submit_HISTORICO('Saldo Ref. Acerto dia '+datetostr(now)+' CP/CR - '+LbNomeCredorDevedor.caption);
                     Submit_GERADOR(Objgeratitulo.Get_Gerador);
                     Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
                     Submit_EMISSAO(datetostr(now));
                     Submit_PRAZO(Objgeratitulo.Get_Prazo);
                     Submit_PORTADOR(Objgeratitulo.Get_Portador);
                     if (Resultado>0)
                     Then Submit_VALOR(Floattostr(Resultado))
                     Else Submit_VALOR(Floattostr(-1*(Resultado)));//para naum dar negativo

                     if (EscolheContaGerencial=False)
                     Then Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial)
                     Else Submit_CONTAGERENCIAL(PcontaGer); 

                     Submit_NUMDCTO('');
                     Submit_GeradoPeloSistema('S');
                     If (Salvar(False)=False)
                     Then Begin
                               FDataModulo.IBTransaction.RollbackRetaining;
                               messagedlg('Erro no lan�amento do T�tulo do Saldo! Lan�amentos Cancelados',mterror,[mbok],0);
                               exit;
                     End;
               End;
     End;
     //******************************************************************************************
     //Guardando este acerto na Tabela de Acerto de Pendencias

     for cont:=1 to StrGridPAGAR.RowCount-1 do
     Begin
          if (StrGridPAGAR.Cells[0,cont]='X')
          Then Begin
                   ObjAcertoPendencias.ZerarTabela;
                   ObjAcertoPendencias.Submit_CODIGO('0');
                   ObjAcertoPendencias.Submit_Data(datetostr(now));
                   ObjAcertoPendencias.Pendencia.Submit_CODIGO(StrGridPAGAR.Cells[2,cont]);
                   ObjAcertoPendencias.TituloResultante.Submit_CODIGO(PNovoTitulo);
                   ObjAcertoPendencias.Status:=dsInsert;
                   if (ObjAcertoPendencias.Salvar(False)=False)
                   Then Begin
                             FDataModulo.IBTransaction.RollbackRetaining;
                             Messagedlg('N�o foi poss�vel gravar a pend�ncia n� '+StrGridPAGAR.Cells[2,cont]+' na tabela de acerto!',mterror,[mbok],0);
                             exit;
                   End;
          End;
     End;

     for cont:=1 to StrGridRECEBER.RowCount-1 do
     Begin
          if (StrGridRECEBER.Cells[0,cont]='X')
          Then Begin
                   ObjAcertoPendencias.ZerarTabela;
                   ObjAcertoPendencias.Submit_CODIGO('0');
                   ObjAcertoPendencias.Submit_Data(datetostr(now));
                   ObjAcertoPendencias.Pendencia.Submit_CODIGO(StrGridRECEBER.Cells[2,cont]);
                   ObjAcertoPendencias.TituloResultante.Submit_CODIGO(PNovoTitulo);
                   ObjAcertoPendencias.Status:=dsInsert;
                   if (ObjAcertoPendencias.Salvar(False)=False)
                   Then Begin
                             FDataModulo.IBTransaction.RollbackRetaining;
                             Messagedlg('N�o foi poss�vel gravar a pend�ncia n� '+StrGridRECEBER.Cells[2,cont]+' na tabela de acerto!',mterror,[mbok],0);
                             exit;
                   End;
          End;
     End;

     FDataModulo.IBTransaction.CommitRetaining;
     if Resultado<>0
     Then Messagedlg('Finalizado '+#13+'T�tulo Resultante '+PNovoTitulo,mtInformation,[mbok],0)
     Else Messagedlg('Processo Finalizado',mtInformation,[mbok],0);

     BtpesquisarClick(SENDER);
     
end;

procedure TFAcertoCreditoDebito.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
      end;
end;

procedure TFAcertoCreditoDebito.CheckSelecionaPagarClick(Sender: TObject);
var
cont:integer;
begin

     if (CheckSelecionaPagar.Checked) Then
     Begin
        for cont:=1 to StrGridPAGAR.RowCount-1 do
        Begin
            if (StrGridPAGAR.Cells[1,cont]<>'')
            Then StrGridPAGAR.Cells[0,cont]:='X';
        End;
        Self.Somatudo;
     End else
      Begin

        for cont:=1 to StrGridPAGAR.RowCount-1 do
        Begin
          if (StrGridPAGAR.Cells[1,cont]<>'')
          Then StrGridPAGAR.Cells[0,cont]:='';
        End;
        Self.Somatudo;
        
      End;

end;

procedure TFAcertoCreditoDebito.CheckSelecionaReceberClick(
  Sender: TObject);
var
cont:integer;
begin

     if (CheckSelecionareceber.Checked) Then
     Begin
        for cont:=1 to StrGridreceber.RowCount-1 do
        Begin
            if (StrGridreceber.Cells[1,cont]<>'')
            Then StrGridreceber.Cells[0,cont]:='X';
        End;
        Self.Somatudo;
     End else
      Begin

        for cont:=1 to StrGridreceber.RowCount-1 do
        Begin
          if (StrGridreceber.Cells[1,cont]<>'')
          Then StrGridreceber.Cells[0,cont]:='';
        End;
        Self.Somatudo;

      End;


end;

function TFAcertoCreditoDebito.PegaContaGerencial(out PcontaGer: string;Debito_Credito:string): Boolean;
begin
     PcontaGer:='';
     result:=False;
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          edtgrupo01.EditMask:='';
          LbGrupo01.Caption:='CONTA GERENCIAL';

          IF (UpperCase(Debito_Credito)='D')
          Then edtgrupo01.OnKeyDown:=ObjLancamento.Pendencia.Titulo.EdtcontagerencialPAGARKeyDown_spv
          Else edtgrupo01.OnKeyDown:=ObjLancamento.Pendencia.Titulo.EdtcontagerencialRECEBERKeyDown_spv;


          repeat
            showmodal;

            if tag=0
            then exit;

            tag:=0;
            
            Try
               strtoint(edtgrupo01.text);
               if (ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.LocalizaCodigo(edtgrupo01.Text)=True)
               Then  Begin
                          ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.TabelaparaObjeto;
                          if (UpperCase(ObjLancamento.Pendencia.Titulo.CONTAGERENCIAL.Get_Tipo)<>UpperCase(Debito_Credito))
                          Then Begin
                                    if (UpperCase(Debito_Credito)='D')
                                    Then Messagedlg('A Conta Gerencial dever� ser de d�bito para gerar uma conta a pagar!',mterror,[mbok],0)
                                    Else Messagedlg('A Conta Gerencial dever� ser de cr�dito para gerar uma conta a receber!',mterror,[mbok],0);
                          End
                          Else Begin
                                    result:=True;
                                    PcontaGer:=edtgrupo01.text;
                                    exit;
                          End;
               End
               Else Messagedlg('Conta gerencial n�o encontrada!',mtError,[mbok],0);

            Except
                  Messagedlg('Conta Inv�lida!',mterror,[mbok],0);
            End;

          Until (tag<>0);

     End;


end;

end.
