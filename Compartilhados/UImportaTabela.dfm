object FImportaTabela: TFImportaTabela
  Left = 311
  Top = 164
  Width = 1120
  Height = 624
  Caption = 'Importa'#231#227'o de Tabelas - EXCLAIM TECNOLOGIA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1104
    Height = 586
    ActivePage = TabSheet2
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Firebird'
      inline FrImportaFirebird1: TFrImportaFirebird
        Left = 0
        Top = 0
        Width = 1096
        Height = 558
        Align = alClient
        TabOrder = 0
        inherited GuiaFirebird: TPageControl
          Width = 1096
          Height = 558
          inherited TabSheet1: TTabSheet
            inherited MemoSQL: TMemo
              Width = 1088
            end
            inherited PanelCaminhotabela: TPanel
              Width = 1088
              inherited BitBtn2: TBitBtn
                OnClick = FrImportaFirebird1BitBtn2Click
              end
            end
            inherited DbGrid: TDBGrid
              Width = 1088
              Height = 305
            end
            inherited BarradeProgresso: TProgressBar
              Top = 514
              Width = 1088
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TXT'
      ImageIndex = 1
      inline FrImportaTXT1: TFrImportaTXT
        Left = 0
        Top = 0
        Width = 1096
        Height = 558
        Align = alClient
        TabOrder = 0
        inherited GuiaTXT: TPageControl
          Width = 1096
          Height = 558
          inherited TabSheet1: TTabSheet
            inherited BarradeProgresso: TProgressBar
              Top = 489
              Width = 1088
              TabOrder = 3
            end
            inherited STRGDados: TStringGrid
              Width = 1088
              Height = 185
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
            end
            inherited MemoArquivo: TRichEdit
              Width = 1088
            end
            inherited PanelCaminhotabela: TPanel
              Width = 1088
              inherited Label1: TLabel
                Left = 854
              end
              inherited Label7: TLabel
                Left = 976
              end
              inherited LblinhaAtual: TLabel
                Left = 1098
              end
              inherited ComboTabela: TComboBox
                Left = 804
              end
              inherited BitBtn2: TBitBtn
                Left = 953
                Caption = '&Iniciar Importa'#231#227'o'
                OnClick = FrImportaTXT1BitBtn2Click
              end
              inherited btcaminhobanco: TBitBtn
                OnClick = FrImportaTXT1btcaminhobancoClick
              end
            end
            inherited Panel9: TPanel
              Top = 505
              Width = 1088
              TabOrder = 4
              inherited Button3: TButton
                Left = 1098
              end
              inherited Button2: TButton
                Left = 978
                OnClick = FrImportaTXT1Button2Click
              end
              inherited btPegaLinha: TButton
                Left = 866
                Anchors = [akTop, akRight]
                BiDiMode = bdRightToLeft
                ParentBiDiMode = False
                OnClick = FrImportaTXT1btPegaLinhaClick
              end
            end
          end
          inherited TabSheet2: TTabSheet
            inherited PanelPrincipalConfiguracao: TPanel
              inherited PanelNovoValor: TPanel
                inherited Button1: TButton
                  OnClick = FrImportaTXT1Button1Click
                end
              end
              inherited Panel6: TPanel
                inherited BitBtn1: TBitBtn
                  OnClick = FrImportaTXT1BitBtn1Click
                end
              end
            end
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'TabSheet3'
      ImageIndex = 2
    end
  end
end
