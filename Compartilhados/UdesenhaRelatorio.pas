unit UdesenhaRelatorio;

interface

uses
  Ulabelrel,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Menus;

type
  TFdesenhaRelatorio = class(TForm)
    ImageRel: TImage;
    MainMenu1: TMainMenu;
    Objetos1: TMenuItem;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    procedure DesenhaLegenda;
    function CompletaPalavra(palavra: string; quantidade: Integer;ValorASerUSado: Char): String;
   { Private declarations }


  public
    { Public declarations }
    procedure ClicaLabel(Sender: TObject);
  end;


const

   //Indica o N�mero M�ximo de Labels que poderemos ter no formul�rio
   MaximoCampos=300;
var
  FdesenhaRelatorio: TFdesenhaRelatorio;
  //Indica a Label que est� Selecionada
  labelselecionada:Integer;
  //Guarda quantas labels foram alocadas na vetor
  Conta_label_usada_gb:integer;
  //vetor de TLabelRel
  VetorLabel:array[0..MaximoCampos] of TLabelRel;




implementation

uses Uregua;

{$R *.dfm}


Function TFdesenhaRelatorio.CompletaPalavra(palavra:string;quantidade:Integer;ValorASerUSado:Char):String;
var
apoio:String;
Begin
     //essa funcao � utilizada para completar uma palavra
     //ou cortar de acordo com um tamanho definido
     apoio:='';
     If (length(palavra)>=quantidade)
     Then Begin
                apoio:=copy(palavra,1,quantidade);
                result:=apoio;
                exit;
     End;

     apoio:=Palavra;
     While (length(apoio)<quantidade) do
     apoio:=apoio+Valoraserusado;

     result:=apoio;
End;

procedure TFdesenhaRelatorio.FormShow(Sender: TObject);
begin
    //Esta Fun��o � chamada no momento que o formulario � mostrado na tela
    //neste momento ele chama a fun��o que seleciona uma Label,
    //neste caso ele chama a primeira label criada 
    Self.ClicaLabel(vetorlabel[0]);

    Self.DesenhaLegenda;

    //Ativa a captura de teclas no formul�rio
    Self.KeyPreview:=True;
end;

procedure TFdesenhaRelatorio.ClicaLabel(Sender: TObject);
begin
     //ao clicar sobre uma das labels
     //sera preenchido as informa��es dessa label em uma legenda
     //no canto direito
     //Colocando o fundo transparente na que estava selecionada antes
     //e amarelo na selecionada atual

     //Colocando o fundo transparente na selecionada anteriormente
     VetorLabel[labelselecionada].Transparent:=true;
     //guardando o indice do vetor da nova selecao 
     labelselecionada:=TLabelRel(Sender).PosicaoVetor;
     //Colocando o fundo amarelo na label selecionada 
     TLabelRel(Sender).Transparent:=False;
     TLabelRel(Sender).Color:=clYellow;
     //escrevendo espaco em branco onde sera escrito o tamanho da linha, coluna, quant.caracteres
     //essas informa��es ser�o escritas depois da �ltima coluna da r�gua
     //foi utilizado o n� 93, pois a r�gua vai somente at� 90
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(1),'Linha           =     ');
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(3),'Coluna          =     ');
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(5),'Qtde  Caracteres=     ');
     //escrevendo as informacoes da label selecionada
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(1),'Linha           ='+inttostr(TLabelRel(Sender).Get_linha));
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(3),'Coluna          ='+inttostr(TLabelRel(Sender).Get_coluna));
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(5),'Qtde  Caracteres='+inttostr(TLabelRel(Sender).QuantidadeCaracteres));
     if (TLabelRel(Sender).tipo='R')
     Then ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(7),'Tipo='+'R�tulo')
     Else ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(7),'Tipo='+'Dados')
end;



procedure TFdesenhaRelatorio.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
temp:string;
begin
     //Esta Fun��o � chamada no momento que alguma tecla � pressionada

     if (labelselecionada=-1)
     Then exit;

     case key of //verificando qual tecla foi pressionada

        VK_RIGHT://seta pra direita
              Begin
                   //se a nova posi��o da label + quant. caracteres
                   //n�o ultrassar o limite de colunas,
                   //reposiciona a label com uma coluna a mais
                   if ((VetorLabel[labelselecionada].Get_coluna+VetorLabel[labelselecionada].QuantidadeCaracteres)<Uregua.quantcolunas_gb)
                   Then VetorLabel[labelselecionada].Reposiciona(VetorLabel[labelselecionada].Get_coluna+1,VetorLabel[labelselecionada].Get_Linha);
                   Self.ClicaLabel(VetorLabel[labelselecionada]);
              End;

        VK_left://seta pra esquerda
              Begin
                   //reposiciona a label com uma coluna a menos
                   //caso essa ja nao esteja no inicio da regua
                   if (VetorLabel[labelselecionada].Get_coluna>1)
                   Then VetorLabel[labelselecionada].Reposiciona(VetorLabel[labelselecionada].Get_coluna-1,VetorLabel[labelselecionada].Get_linha);
                   Self.ClicaLabel(VetorLabel[labelselecionada]);
              End;
        VK_UP://seta pra cima
            Begin
                 //reposiciona a label com uma linha a menos
                 //caso essa ja nao esteja no inicio da regua
                 if (VetorLabel[labelselecionada].Get_linha>1)
                 Then VetorLabel[labelselecionada].Reposiciona(VetorLabel[labelselecionada].Get_Coluna,VetorLabel[labelselecionada].Get_linha-1);
                 Self.ClicaLabel(VetorLabel[labelselecionada]);
            End;
        VK_DOWN://seta pra cima
            Begin
                 //se a nova posi��o da label
                 //n�o ultrassar o limite de linha,
                 //reposiciona a label com uma linha a mais
                 if (VetorLabel[labelselecionada].Get_linha<Uregua.quantlinhas_gb)
                 Then VetorLabel[labelselecionada].Reposiciona(VetorLabel[labelselecionada].Get_Coluna,VetorLabel[labelselecionada].Get_linha+1);
                 Self.ClicaLabel(VetorLabel[labelselecionada]);
            End;

        VK_F2://F2 � usada para renomear a caption do r�tulo
            Begin
                 //s� renomeia se for um r�tulo, pois se for uma
                 //label de dados o r�tulo dela ser� o dado
                 //que ser� preenchido no momento da impress�o
                 //do relat�rio
                 if (VetorLabel[labelselecionada].tipo='R')//r�tulo
                 Then Begin
                           temp:=VetorLabel[labelselecionada].rotulo;
                           if (InputQuery('Novo R�tulo','Digite um novo R�tulo',temp)=false)
                           then exit;
                           VetorLabel[labelselecionada].rotulo:=temp;
                           VetorLabel[labelselecionada].QuantidadeCaracteres:=length(temp);
                           VetorLabel[labelselecionada].caption:=Self.CompletaPalavra(VetorLabel[labelselecionada].rotulo,VetorLabel[labelselecionada].QuantidadeCaracteres,'�');
                           Self.ClicaLabel(VetorLabel[labelselecionada]);
                 End;
            End;

        VK_F3://F3 define a Quantidade Caracteres que ser� impresso
            Begin

                 temp:=inttostr(VetorLabel[labelselecionada].QuantidadeCaracteres);
                 if (InputQuery('Novo Quantidade','Digite uma nova Quantidade',temp)=false)
                 then exit;
                 Try
                    strtoint(temp);
                    if ((strtoint(temp)+VetorLabel[labelselecionada].Get_coluna)>91)
                    Then Begin
                              Messagedlg('A Quantidade de caracteres + posi��o da coluna n�o pode ultrapassar o limite horizontal de 90',mterror,[mbok],0);
                              exit;
                    End;

                 Except
                       Messagedlg('Quantidade inv�lida!',mterror,[mbok],0);
                       exit;
                 End;
                 VetorLabel[labelselecionada].QuantidadeCaracteres:=strtoint(temp);
                 //preenchendo com '�' o que falta pra dar a frase ou retirando
                 //aquilo que esta sobrando
                 VetorLabel[labelselecionada].caption:=Self.CompletaPalavra(VetorLabel[labelselecionada].rotulo,VetorLabel[labelselecionada].QuantidadeCaracteres,'�');
                 Self.ClicaLabel(VetorLabel[labelselecionada]);
            End;
        VK_F4:Begin//negrito
                   if (VetorLabel[labelselecionada].Font.Style=[fsbold])
                   Then VetorLabel[labelselecionada].Font.Style:=[]
                   Else VetorLabel[labelselecionada].Font.Style:=[fsbold];
              End;
        VK_F5:Begin//Desativa
                   VetorLabel[labelselecionada].Visible:=False;
              End;
     End;

end;

procedure TFdesenhaRelatorio.DesenhaLegenda;
begin
     //Esta Fun��o desenha uma Legenda indicando quais s�o os
     //atalhos que podem ser usados
     
     //desenha a borda da legenda
     ImageRel.Canvas.Rectangle(RetornaColunaPixel(92),RetornaLinhaPixel(30),RetornaColunaPixel(108),RetornaLinhaPixel(40));
     //texto interno da legenda
     ImageRel.Canvas.TextOut(RetornaColunaPixel(95),RetornaLinhaPixel(30),'ATALHOS');
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(32),'F2=Novo R�tulo');
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(34),'F3=Nova Qtde ');
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(36),'F4=Negrito');
     ImageRel.Canvas.TextOut(RetornaColunaPixel(93),RetornaLinhaPixel(38),'F5=Desativa');
end;


end.
