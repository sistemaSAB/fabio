unit UAcertaDataServidor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask;

type
  TFacertaDataServidor = class(TForm)
    edtdata: TMaskEdit;
    edthora: TMaskEdit;
    edtverificador: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  FacertaDataServidor: TFacertaDataServidor;

implementation

uses UessencialGlobal;


{$R *.dfm}

procedure TFacertaDataServidor.BitBtn1Click(Sender: TObject);
var
Str:String;
d:integer;
begin
     Try
        strtodate(edtdata.text);
        strtotime(edthora.Text);
     Except
           Messagedlg('Data ou Hora Inv�lida!',mterror,[mbok],0);
           exit;
     End;
     Str:='';
     Str:=formatdatetime('ddmmyyyy',strtodate(edtdata.text))+
          formatdatetime('hhmm',strtotime(edthora.text));

     d:=RetornaDigitoData(str);

     if (d=-1)
     Then exit;
     
     if (strtoint(edtverificador.Text)<>d)
     Then Begin
               Messagedlg('D�gito Verificador inv�lido!',mterror,[mbok],0);
               exit;
     End;

     Tag:=1;
     Self.Close;

end;

procedure TFacertaDataServidor.FormShow(Sender: TObject);
begin
     edtdata.text:='';
     edthora.text:='';
     edtverificador.Text:='';
     tag:=0;
end;


end.
