unit UobjUsuarioIB;
Interface
Uses useg,forms,Classes,Db,Ibcustomdataset,IBStoredProc,IBServices,ibdatabase;

Type
   TObjUsuarioIB=class

          Public
                Function InsereUsuario(nome,senha1,senha2:String):Boolean;overload;
                Function InsereUsuario(nome,senha1,senha2:String;PDatabase:Tibdatabase): Boolean;overload;

                function AlteraUsuario(nome, senha1, senha2: String): Boolean;overload;
                function AlteraUsuario(nome, senha1, senha2: String;PibDatabase:Tibdatabase): Boolean;overload;


                function ApagaUsuario(Nome: String): Boolean;overload;
                function ApagaUsuario(Nome: String;Pdatabase:Tibdatabase): Boolean;overload;


                Function ConcedePermissoes(nome:string):Boolean;overload;
                Function ConcedePermissoes(nome:string;Pdatabase:Tibdatabase): Boolean;overload;

                Function RetiraPermissoes(nome:String): Boolean;overload;
                Function RetiraPermissoes(nome:String;Pdatabase:Tibdatabase): Boolean;overload;

                Procedure GeraTXTDelecao;
                Constructor Create;
                Destructor  Free;
                procedure   AcertarGenerator;overload;
                procedure   AcertarGenerator(Pdatabase:TIBDatabase);overload;


                Procedure RetiraPermissoesAntigosUsuarios(Pdatabase:TIBDatabase);overload;
                Procedure RetiraPermissoesAntigosUsuarios;overload;

                Procedure RetiraTodos;overload;
                Procedure RetiraTodos(Pdatabase:TIBDatabase);overload;


         Private
                ListagemUsuarios:TStringList;

                Function ListaUsuarios:boolean;overload;
                Function ListaUsuarios(Pdatabase:TIBDatabase):boolean;overload;


                function PreparaUSer(Pdatabase:TIBDatabase): boolean;
                //Function EncriptaDecripta(Senha:String):String;

                Function PegaNomeServidor:String;


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Ibquery,Controls,Uessencialglobal,ibtable,
  UFiltraImp, UMostraBarraProgresso;


{ TTabTitulo }

{ TObjUsuarioIB }

constructor TObjUsuarioIB.Create;
begin
     ListagemUsuarios:=TStringList.create;
end;


destructor TObjUsuarioIB.Free;
begin
     freeandnil(Self.Listagemusuarios);
end;



function TObjUsuarioIB.ApagaUsuario(Nome: String): Boolean;
Begin
      Self.ApagaUsuario(Nome,FDataModulo.IBDatabase);
end;

function TObjUsuarioIB.ApagaUsuario(Nome: String;Pdatabase:Tibdatabase): Boolean;
Begin
   nome:=uppercase(nome);
   result:=False;
   with FDataModulo.IbUser do
   Begin
        ServerName:=Self.PegaNomeServidor;

        If (VerificaBaseRemota(PegaPathBanco)=True)
        Then Protocol:=TCP
        Else Protocol:=Local;

        if (preparauser(Pdatabase)=false)
        then exit;

        try
           UserName :=Nome;
           DeleteUser;
           result:=Self.RetiraPermissoes(nome,Pdatabase);
        Except
              Active := False;
              exit;
        End;
   End;
End;

function TObjUsuarioIB.AlteraUsuario(nome, senha1, senha2: String): Boolean;
Begin
     Self.AlteraUsuario(nome, senha1, senha2,Fdatamodulo.ibdatabase);
End;

function TObjUsuarioIB.AlteraUsuario(nome, senha1, senha2: String;PibDatabase:Tibdatabase): Boolean;
var
TmpSenha:String;
Begin
     nome:=uppercase(nome);
     result:=False;
     with FDataModulo.IbUser do
     Begin
       ServerName:=Self.PegaNomeServidor;

       If (VerificaBaseRemota(PegaPathBanco)=True)
       Then Protocol:=TCP
       Else Protocol:=Local;

       if (Senha1<>Senha2)
       Then Begin
                 messagedlg('As senhas n�o conferem!',mterror,[mbok],0);
                 exit;
       End;

       if (uppercase(nome)<>'SYSDBA')
       Then Tmpsenha:=Useg.EncriptaSenha(senha1)
       Else Tmpsenha:=senha1;


       if (Self.preparauser(PibDatabase)=false)
       then exit;

       Try
            Result:=True;
            UserName  :=nome;
            FirstName :=nome;
            MiddleName:=nome;
            LastName  :=nome;
            UserID    :=0;
            GroupID   :=0;
            Password  :=TmpSenha;
            ModifyUser;
            result:=True;
        Except
            on e:exception do
            Begin
                  mensagemerro(e.message);
                  Active := False;
                  exit;
            End;
        end;
     End;
end;

function TObjUsuarioIB.InsereUsuario(nome, senha1, senha2: String): Boolean;
Begin
     Self.InsereUsuario(nome,senha1,senha2,FDataModulo.IBDatabase);
end;

function TObjUsuarioIB.InsereUsuario(nome, senha1, senha2: String;PDatabase:Tibdatabase): Boolean;
var
TmpSenha:String;
begin
     nome:=uppercase(nome);
     Result:=False;

     With FDataModulo.IbUser do
     Begin
           Active:=False;
           ServerName:=Self.PegaNomeServidor;

           If (VerificaBaseRemota(PegaPathBanco)=True)
           Then Protocol:=TCP
           Else Protocol:=Local;


           if (Senha1<>Senha2)
           Then Begin
                     messagedlg('As senhas n�o conferem!',mterror,[mbok],0);
                     exit;
           End;

           tmpsenha:=Useg.EncriptaSenha(senha1);


           If (Self.preparauser(PDatabase)=false)
           Then exit;

           try
               UserName := nome;
               FirstName := nome;
               MiddleName :=nome;
               LastName := nome;
               UserID := 0;
               GroupID := 0;
               Password := TmpSenha;
               AddUser;

               If (Self.ConcedePermissoes(nome,Pdatabase)=True)
               Then Result:=true;
           Except
             on e:exception do
             Begin
                 Messagedlg('Erro na cria��o de um novo usu�rio!'+#13+'Erro: '+e.message,mterror,[mbok],0);
                 exit;
             End;
           end;
     End;
End;

function TObjUsuarioIB.PegaNomeServidor: String;
var
posic:integer;
pathstr:string;
begin
     pathstr:=PegaPathBanco;
     If (VerificaBaseRemota(pathstr)=False)
     Then result:=''
     Else Begin
               //acha o 2 pontos, encotrou copia
               //do primeiro ate ele -1
               posic:=pos(':',pathstr);
               If (posic=0)
               Then result:=''
               Else result:=copy(pathstr,1,posic-1);
     End;

end;

function TObjUsuarioIB.PreparaUSer(Pdatabase:TIBDatabase): boolean;
Begin

     Try
        FDataModulo.IbUser.Active := false;

        FDataModulo.IbUser.LoginPrompt:=False;
        FDataModulo.IbUser.serverName:=Self.PegaNomeServidor;

        If (VerificaBaseRemota(PegaPathBanco)=True)
        Then FDataModulo.IbUser.Protocol:=TCP
        Else FDataModulo.IbUser.Protocol:=Local;

        FDataModulo.IbUser.Params.clear;
        FDataModulo.IbUser.Params.add('user_name='+PDatabase.Params.Values['user_name']);
        FDataModulo.IbUser.Params.add('password='+Pdatabase.Params.Values['password']);
        FDataModulo.IbUser.Active := True;
        result:=true;
     Except
           on e:exception do
           Begin
               result:=false;
               Messagedlg('Erro  '+#13+E.message,mterror,[mbok],0);
               exit;
           End;

     End;

End;


Function TObjUsuarioIB.ListaUsuarios:boolean;
Begin
     Self.ListaUsuarios(FDataModulo.IBDatabase);
end;

Function TObjUsuarioIB.ListaUsuarios(Pdatabase:TIBDatabase):boolean;
var
cont:integer;
Begin
     result:=False;
     try
          if (Self.preparauser(Pdatabase)=false)
          then exit;

          ListagemUsuarios.clear;
          Try
                  FDataModulo.IbUser.DisplayUsers;

                  for cont:=1 to FDataModulo.IbUser.UserInfoCount-1 do
                  Begin
                     ListagemUsuarios.add(FDataModulo.IbUser.UserInfo[cont].UserName);
                  End;
                  result:=True;
                  exit;
          Except
                exit;
          End;

    Finally
          FDataModulo.IbUser.Active:=False;
    End;


End;



function TObjUsuarioIB.ConcedePermissoes(nome:string): Boolean;
begin
     Self.ConcedePermissoes(nome,FDataModulo.IBDatabase);
End;

function TObjUsuarioIB.ConcedePermissoes(nome:string;Pdatabase:Tibdatabase): Boolean;
VAR
STrTabelas,StrProcedimentos:TStringList;
Tabela:Tibtable;
Stproc:TIBStoredProc;
Qlocal:Tibquery;
cont:Integer;
begin
     result:=False;

     Try
        Tabela:=TIBTable.create(nil);
        Tabela.database:=Pdatabase;
        Tabela.TableTypes:=[ttView];
        Stproc:=TIBStoredProc.create(nil);
        Stproc.database:=Pdatabase;
        Qlocal:=TIBQuery.create(nil);
        Qlocal.database:=Pdatabase;
     Except

     End;

     Try
        STrTabelas:=TStringList.create;
        StrProcedimentos:=TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Criar a LIstagem de Tabelas e Procedimentos!',mterror,[mbok],0);
           exit;
     End;



     Try
        Try
                STrTabelas.clear;
                StrProcedimentos.clear;
                With Tabela do
                Begin
                     for cont:=0 to (tabela.TableNames.Count-1) do
                     Begin
                          STrTabelas.add(Tabela.TableNames[cont]);
                     end;
                End;
                for cont:=0 to (Stproc.StoredProcedureNames.Count -1) do
                Begin
                     StrProcedimentos.add(Stproc.StoredProcedureNames[cont]);
                end;
        Except
              Messagedlg('Erro na Listagem das Tabelas e Stored Procedures',mterror,[mbok],0);
              exit;
        End;

        Try
           for cont:=0 to STrTabelas.count-1 do
           Begin
                Qlocal.close;
                Qlocal.sql.clear;
                Qlocal.sql.add('Grant All on '+STrTabelas[cont]+' to '+nome);
                Qlocal.ExecSQL;           
           End;
           
          for cont:=0 to StrProcedimentos.count-1 do
          Begin
                Qlocal.close;
                Qlocal.sql.clear;
                Qlocal.sql.add('Grant EXECUTE on PROCEDURE '+StrProcedimentos[cont]+' to '+nome);
                Qlocal.ExecSQL;
          End;
           result:=True;
           Pdatabase.DefaultTransaction.CommitRetaining;
           exit;
        Except
              Messagedlg('Erro na tentativa de conceder permiss�o para o usu�rio!',mterror,[mbok],0);
               exit;
        End;

     Finally
            freeandnil(strtabelas);
            Freeandnil(strprocedimentos);
            Freeandnil(Tabela);
            Freeandnil(Stproc);
            Freeandnil(Qlocal);
     End;


end;


function TObjUsuarioIB.RetiraPermissoes(nome: String): boolean;
Begin
    Self.RetiraPermissoes(nome,FDataModulo.IBDatabase);
end;

function TObjUsuarioIB.RetiraPermissoes(nome:String;Pdatabase:Tibdatabase): Boolean;
VAR
STrTabelas,StrProcedimentos:TStringList;
Tabela:Tibtable;
Stproc:TIBStoredProc;
Qlocal:Tibquery;
cont:Integer;
begin
     result:=False;

     Try
        Tabela:=TIBTable.create(nil);
        Tabela.database:=Pdatabase;
        Tabela.TableTypes:=[ttView];
        Stproc:=TIBStoredProc.create(nil);
        Stproc.database:=Pdatabase;
        Qlocal:=TIBQuery.create(nil);
        Qlocal.database:=Pdatabase;
     Except

     End;
     Try
        STrTabelas:=TStringList.create;
        StrProcedimentos:=TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Criar a LIstagem de Tabelas e Procedimentos!',mterror,[mbok],0);
           exit;
     End;

     Try
        Try
                STrTabelas.clear;
                StrProcedimentos.clear;
                With Tabela do
                Begin
                     for cont:=0 to (tabela.TableNames.Count-1) do
                     Begin
                          STrTabelas.add(Tabela.TableNames[cont]);
                     end;
                End;
                for cont:=0 to (Stproc.StoredProcedureNames.Count -1) do
                Begin
                     StrProcedimentos.add(Stproc.StoredProcedureNames[cont]);
                end;
        Except
              Messagedlg('Erro na Listagem das Tabelas e Stored Procedures',mterror,[mbok],0);
              exit;
        End;

        Try
           for cont:=0 to STrTabelas.count-1 do
           Begin
                Qlocal.close;
                Qlocal.sql.clear;
                Qlocal.sql.add('Revoke All on '+STrTabelas[cont]+' from '+nome);
                Qlocal.ExecSQL;
           End;



          for cont:=0 to StrProcedimentos.count-1 do
          Begin
                Qlocal.close;
                Qlocal.sql.clear;
                Qlocal.sql.add('revoke EXECUTE on PROCEDURE '+StrProcedimentos[cont]+' from '+nome);
                Qlocal.ExecSQL;
           End;
           Pdatabase.DefaultTransaction.CommitRetaining;
           result:=True;
           exit;
        Except
              Messagedlg('Erro na tentativa de conceder permiss�o para o usu�rio!',mterror,[mbok],0);
              exit;
        End;

     Finally
            freeandnil(strtabelas);
            Freeandnil(strprocedimentos);
            Freeandnil(Tabela);
            Freeandnil(Stproc);
            Freeandnil(Qlocal);
     End;

End;

procedure TObjUsuarioIB.GeraTXTDelecao;
VAR
STrTabelas:TStringList;
Tabela:Tibtable;
Qlocal:Tibquery;
cont:Integer;
begin
     Try
        Tabela:=TIBTable.create(nil);
        Tabela.database:=FDataModulo.ibdatabase;
        Qlocal:=TIBQuery.create(nil);
        Qlocal.database:=FDataModulo.ibdatabase;
     Except

     End;

     Try
        STrTabelas:=TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Criar a LIstagem de Tabelas e Procedimentos!',mterror,[mbok],0);
           exit;
     End;

     Try
        Try
                STrTabelas.clear;
                With Tabela do
                Begin
                     for cont:=0 to (tabela.TableNames.Count-1) do
                     Begin
                          STrTabelas.add(Tabela.TableNames[cont]);
                     end;
                End;
        Except
              Messagedlg('Erro na Listagem das Tabelas e Stored Procedures',mterror,[mbok],0);
              exit;
        End;
        Qlocal.close;
        Qlocal.sql.clear;
        STrTabelas.Sort;
        for cont:=0 to STrTabelas.count-1 do
        Begin
             Qlocal.sql.add('delete from '+STrTabelas[cont]+';');
        End;

     Finally
            freeandnil(strtabelas);
            Freeandnil(Tabela);
            Freeandnil(Qlocal);
     End;



end;

procedure TObjUsuarioIB.AcertarGenerator;
Begin
     Self.AcertarGenerator(FDataModulo.IBDatabase);
End;

procedure TObjUsuarioIB.AcertarGenerator(Pdatabase:TIBDatabase);
VAR
STrTabelas,str2:TStringList;
Tabela:Tibtable;
Qlocal:Tibquery;
maximo,cont:Integer;

begin
     Try
        Tabela:=TIBTable.create(nil);
        Tabela.database:=Pdatabase;
        Qlocal:=TIBQuery.create(nil);
        Qlocal.database:=Pdatabase;
     Except

     End;

     Try
        STrTabelas:=TStringList.create;
        str2:=TStringList.create;
     Except
           Messagedlg('Erro na tentativa de Criar a LIstagem de Tabelas e Procedimentos!',mterror,[mbok],0);
           exit;
     End;

     Try
        Try
                STrTabelas.clear;
                With Tabela do
                Begin
                     for cont:=0 to (tabela.TableNames.Count-1) do
                     Begin
                          STrTabelas.add(Tabela.TableNames[cont]);
                     end;
                End;
        Except
              Messagedlg('Erro na Listagem das Tabelas e Stored Procedures',mterror,[mbok],0);
              exit;
        End;

        STrTabelas.Sort;
        str2.clear;
        for cont:=0 to STrTabelas.count-1 do
        Begin
             try
                Qlocal.close;
                Qlocal.sql.clear;
                Qlocal.sql.add('Select max(codigo) as SOMA from '+STrTabelas[cont]);
                Qlocal.open;
                maximo:=0;
                maximo:=Qlocal.fieldbyname('soma').asinteger;
                Qlocal.close;
                Qlocal.sql.clear;
                Qlocal.sql.add('set generator gen'+copy(STrTabelas[cont],4,length(STrTabelas[cont])-3)+' to '+inttostr(maximo));


                Qlocal.ExecSQL;
             Except
                str2.add(STrTabelas[cont]);
                //showmessage(STrTabelas[cont]);
             End;
            
        End;
        showmessage('VERIFIQUE NO "C:\SETAGENERATORS.SQL" SE EXISTE ALGUM ERRO');
        
     Finally
            freeandnil(strtabelas);
            freeandnil(str2);
            Freeandnil(Tabela);
            Freeandnil(Qlocal);
     End;



end;

procedure TObjUsuarioIB.RetiraPermissoesAntigosUsuarios;
Begin
     Self.RetiraPermissoesAntigosUsuarios(FdataModulo.ibdatabase);
end;

procedure TObjUsuarioIB.RetiraPermissoesAntigosUsuarios(Pdatabase:TIBDatabase);
var
Cont:integer;
Pnomes:TStringList;
begin
     try
        Pnomes:=TStringList.create;
     except
           Messagedlg('Erro na tentativa de Criar a StringLists PNOME',mterror,[mbok],0);
           exit; 
     end;
Try     

     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          LbGrupo01.Caption:='Usuarios (sv)';

          Showmodal;

          if (tag=0)
          then exit;

          if (ExplodeStr(edtgrupo01.text,Pnomes,',','STRING')=False)
          Then Begin
                    Messagedlg('Valores inv�lidos',mterror,[mbok],0);
                    exit;
          end;

          for cont:=0 to Pnomes.count-1 do
          begin
               Self.RetiraPermissoes(uppercase(pnomes[cont]),Pdatabase);
          end;
          Pdatabase.DefaultTransaction.CommitRetaining;
          Messagedlg('Conclu�do',mtinformation,[mbok],0);
     end;

Finally
       freeandnil(pnomes);
End;

end;

procedure TObjUsuarioIB.RetiraTodos;
Begin
     Self.retiratodos(fdatamodulo.ibdatabase);
end;

procedure TObjUsuarioIB.RetiraTodos(Pdatabase:TIBDatabase);
VAR
Qlocal:Tibquery;
cont:Integer;
begin
     if (Messagedlg('Tem certeza que deseja retirar a permiss�o de todos os usu�rios',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     Try
        Qlocal:=TIBQuery.create(nil);
        Qlocal.database:=pdatabase;
     Except

     End;

     Try
        Qlocal.close;
        Qlocal.sql.clear;
        Qlocal.sql.add('Select distinct(RDB$USER) as NOME from');
        Qlocal.sql.add('RDB$USER_PRIVILEGES');
        Qlocal.sql.add('where RDB$USER not like ''%SYSDBA%''');
        Qlocal.sql.add('and  RDB$USER not like ''%PUBLIC%''');
        Qlocal.open;
        Qlocal.last;
        FMostraBarraProgresso.ConfiguracoesIniciais(Qlocal.RecordCount,0);
        Qlocal.first;

        While not(qlocal.eof) do
        Begin
             FMostraBarraProgresso.IncrementaBarra1(1);
             FMostraBarraProgresso.Lbmensagem.caption:='Retirando Permiss�o '+Qlocal.fieldbyname('NOME').asstring;
             FMostraBarraProgresso.show;
             application.processmessages;

             if (Self.RetiraPermissoes(uppercase(trim(Qlocal.fieldbyname('NOME').asstring)),
             PDATABASE)=False)
             Then Begin
                       Mensagemerro('Erro na tentativa de retirar a permiss�o de '+Qlocal.fieldbyname('NOME').asstring);
                       exit;
             End;
             
             Qlocal.Next;
        End;
        MensagemAviso('Conclu�do');

     Finally
            Freeandnil(Qlocal);
            FMostraBarraProgresso.close;
     End;

End;





end.








