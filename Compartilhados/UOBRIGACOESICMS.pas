unit UOBRIGACOESICMS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjOBRIGACOESICMS,
  jpeg;

type
  TFOBRIGACOESICMS = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    ImagemFundo: TImage;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbDescricao: TLabel;
    EdtDescricao: TEdit;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjOBRIGACOESICMS:TObjOBRIGACOESICMS;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOBRIGACOESICMS: TFOBRIGACOESICMS;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFOBRIGACOESICMS.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjOBRIGACOESICMS do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Descricao(edtDescricao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFOBRIGACOESICMS.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjOBRIGACOESICMS do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtDescricao.text:=Get_Descricao;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFOBRIGACOESICMS.TabelaParaControles: Boolean;
begin
     If (Self.ObjOBRIGACOESICMS.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFOBRIGACOESICMS.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjOBRIGACOESICMS:=TObjOBRIGACOESICMS.create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
     retira_fundo_labels(self);
end;

procedure TFOBRIGACOESICMS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjOBRIGACOESICMS=Nil)
     Then exit;

     If (Self.ObjOBRIGACOESICMS.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjOBRIGACOESICMS.free;
     if(Self.Owner = nil) then
       FDataModulo.IBTransaction.Commit;
end;

procedure TFOBRIGACOESICMS.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFOBRIGACOESICMS.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjOBRIGACOESICMS.status:=dsInsert;
     edtDescricao.setfocus;

end;


procedure TFOBRIGACOESICMS.btalterarClick(Sender: TObject);
begin
    If (Self.ObjOBRIGACOESICMS.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjOBRIGACOESICMS.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtDescricao.setfocus;
                
    End;


end;

procedure TFOBRIGACOESICMS.btgravarClick(Sender: TObject);
begin

     If Self.ObjOBRIGACOESICMS.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjOBRIGACOESICMS.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjOBRIGACOESICMS.Get_codigo;
     Self.ObjOBRIGACOESICMS.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFOBRIGACOESICMS.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjOBRIGACOESICMS.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjOBRIGACOESICMS.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjOBRIGACOESICMS.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFOBRIGACOESICMS.btcancelarClick(Sender: TObject);
begin
     Self.ObjOBRIGACOESICMS.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFOBRIGACOESICMS.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFOBRIGACOESICMS.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjOBRIGACOESICMS.Get_pesquisa,Self.ObjOBRIGACOESICMS.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjOBRIGACOESICMS.status<>dsinactive
                                  then exit;

                                  If (Self.ObjOBRIGACOESICMS.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjOBRIGACOESICMS.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFOBRIGACOESICMS.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFOBRIGACOESICMS.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

