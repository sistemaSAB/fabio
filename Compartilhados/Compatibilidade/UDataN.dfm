object FdataN: TFdataN
  Left = 225
  Top = 125
  BorderStyle = bsNone
  Caption = 'EXCLAIM TECNOLOGIA'
  ClientHeight = 94
  ClientWidth = 190
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbfrase: TLabel
    Left = 4
    Top = 8
    Width = 186
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Entre com a Data'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Shape1: TShape
    Left = 0
    Top = 0
    Width = 190
    Height = 94
    Brush.Style = bsClear
  end
  object btok: TBitBtn
    Left = 32
    Top = 58
    Width = 61
    Height = 26
    Caption = '&OK'
    Default = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btokClick
  end
  object btcancel: TBitBtn
    Left = 104
    Top = 58
    Width = 61
    Height = 26
    Caption = '&CANCEL'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = btcancelClick
  end
  object edtdata: TMaskEdit
    Left = 61
    Top = 31
    Width = 73
    Height = 19
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
    OnKeyPress = edtdataKeyPress
  end
end
