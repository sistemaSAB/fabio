unit UDataN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls, Buttons;

type
  TFdataN = class(TForm)
    edtdata: TMaskEdit;
    lbfrase: TLabel;
    btok: TBitBtn;
    btcancel: TBitBtn;
    Shape1: TShape;
    procedure btcancelClick(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure edtdataKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FdataN: TFdataN;

implementation

uses UessencialgLOBAL;


{$R *.DFM}

procedure TFdataN.btcancelClick(Sender: TObject);
begin
     ModalResult:=mrCancel;
end;

procedure TFdataN.btokClick(Sender: TObject);
var
DataLocal:Tdate;
begin
     try
        DataLocal:=Strtodate(edtdata.text);
        ModalResult:=Mrok;
     Except

     End;
end;

procedure TFdataN.edtdataKeyPress(Sender: TObject; var Key: Char);
begin
     If Key=#13
     Then btok.OnClick(sender)
     Else Begin
               If key=#27
               Then btcancel.OnClick(sender);
          End;

end;

procedure TFdataN.FormActivate(Sender: TObject);
begin
     edtdata.SetFocus;
end;

procedure TFdataN.FormShow(Sender: TObject);
begin
     UessencialGlobal.PegaCorForm(Self);
end;

end.
