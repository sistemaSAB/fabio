unit UopcaoRel;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Buttons;

type
  TFOpcaorel = class(TForm)
    RgOpcoes: TRadioGroup;
    Btrelatorios: TBitBtn;
    BtCancelar: TBitBtn;
    procedure BtrelatoriosClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOpcaorel: TFOpcaorel;

implementation

uses UessencialGlobal;

{$R *.DFM}

procedure TFOpcaorel.BtrelatoriosClick(Sender: TObject);
begin
     tag:=1;
     close;
end;

procedure TFOpcaorel.BtCancelarClick(Sender: TObject);
begin
     tag:=0;
     close;
End;

procedure TFOpcaorel.FormActivate(Sender: TObject);
var
cont:Integer;
apoio:STring;
begin
     tag:=0;
     RgOpcoes.ItemIndex:=0;
     PegaFiguraBotoes(nil,nil,btcancelar,nil,nil,btrelatorios,nil,nil);
     If(RgOpcoes.Items.Count=0)
     Then exit;

     //criando o atalho para as opcoes
     for cont:=0 to RgOpcoes.items.count -1 do
     Begin
          if Pos('&',RgOpcoes.items[cont])=0
          Then Begin
                    apoio:='';
                    apoio:='&'+Inttostr(cont+1)+' - '+RgOpcoes.items[cont];
                    RgOpcoes.items[cont]:='';
                    RgOpcoes.items[cont]:=apoio;
          End;
     End;

end;

procedure TFOpcaorel.FormShow(Sender: TObject);
begin
     UessencialGlobal.PegaCorForm(Self);
end;

end.
