unit UobjPERMISSOESEXTRAS;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal;//USES_INTERFACE
Type
   TObjPERMISSOESEXTRAS=class

          Public
                ObjDatasource                               :TDataSource;
                ObjDatasourceusuarios                        :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                //Usuario:TOBJUSUARIOS;
               //Permissao:TOBJPERMISSOESUSO;
//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaPermissaoUsuario(PPermissao:string;PUsuario:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;

                Procedure Submit_usuario(parametro: string);
                Function Get_usuario: string;

                Procedure Submit_permissao(parametro: string);
                Function Get_permissao: string;

                procedure RetornaUsuariosDisponiveis(ppermissao: string);
                procedure RetornaUsuariosUsados(ppermissao: string);


//CODIFICA DECLARA GETSESUBMITS



         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:tibquery;
               ObjqueryPesquisaUsuarios:tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Usuario:string;
               Permissao:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjPERMISSOESEXTRAS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        {If(FieldByName('Usuario').asstring<>'')
        Then Begin
                 If (Self.Usuario.LocalizaCodigo(FieldByName('Usuario').asstring)=False)
                 Then Begin
                          Messagedlg('Usu�rio N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Usuario.TabelaparaObjeto;
        End;}
        Self.usuario:=Fieldbyname('usuario').asstring;
        Self.permissao:=Fieldbyname('permissao').asstring;
        {If(FieldByName('Permissao').asstring<>'')
        Then Begin
                 If (Self.Permissao.LocalizaCodigo(FieldByName('Permissao').asstring)=False)
                 Then Begin
                          Messagedlg('Permiss�o N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Permissao.TabelaparaObjeto;
        End;}
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjPERMISSOESEXTRAS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Usuario').asstring:=Self.Usuario;//.GET_CODIGO;
        ParamByName('Permissao').asstring:=Self.Permissao;//.GET_CODIGO;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjPERMISSOESEXTRAS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPERMISSOESEXTRAS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Usuario:='';//.ZerarTabela;
        Permissao:='';//.ZerarTabela;
//CODIFICA ZERARTABELA



     End;
end;

Function TObjPERMISSOESEXTRAS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
       If (CODIGO='')
       Then Mensagem:=mensagem+'/C�digo';

       If (Usuario='')
       Then Mensagem:=mensagem+'/Usu�rio';

       If (Permissao='')
       Then Mensagem:=mensagem+'/Permiss�o';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjPERMISSOESEXTRAS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select Count(codigo) as conta from tabusuarios where codigo='+Self.usuario);
          open;
          if (fieldbyname('conta').asinteger=0)
          Then Mensagem:=mensagem+'/ Usu�rio n�o Encontrado!';

          close;
          sql.clear;
          sql.add('Select Count(codigo) as conta from tabpermissoesuso where codigo='+Self.Permissao);
          open;
          if (fieldbyname('conta').asinteger=0)
          Then Mensagem:=mensagem+'/ Permiss�o n�o Encontrado!';
     End;
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPERMISSOESEXTRAS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtoint(Self.Usuario);
     Except
           Mensagem:=mensagem+'/Usu�rio';
     End;
     try
        Strtoint(Self.Permissao);
     Except
           Mensagem:=mensagem+'/Permiss�o';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPERMISSOESEXTRAS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPERMISSOESEXTRAS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPERMISSOESEXTRAS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PERMISSOESEXTRAS vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Usuario,Permissao');
           SQL.ADD(' from  TabPermissoesExtras');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjPERMISSOESEXTRAS.LocalizaPermissaoUsuario(PPermissao:string;PUsuario:string) :boolean;
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Usuario,Permissao');
           SQL.ADD(' from  TabPermissoesExtras');
           SQL.ADD(' WHERE permissao='+ppermissao);
           SQL.ADD(' and usuario='+pusuario);
           Try
              Open;
           Except
                 on e:exception do
                 Begin
                       mensagemerro(e.message);
                       result:=False;
                       exit;
                 End;
           End;

           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPERMISSOESEXTRAS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPERMISSOESEXTRAS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPERMISSOESEXTRAS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;


        Self.ObjDatasource:=TDataSource.create(nil);
        Self.ObjDatasource.dataset:=Self.ObjqueryPesquisa;

        Self.ObjqueryPesquisaUsuarios:=TIBQuery.create(nil);
        Self.ObjqueryPesquisaUsuarios.Database:=FDataModulo.IbDatabase;


        Self.ObjDatasourceUsuarios:=TDataSource.create(nil);
        Self.ObjDatasourceUsuarios.dataset:=Self.ObjqueryPesquisausuarios;


        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        //Self.Usuario:=TOBJUSUARIOS.create;
        //Self.Permissao:=TOBJPERMISSOESUSO.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPermissoesExtras(CODIGO,Usuario,Permissao');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:Usuario,:Permissao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPermissoesExtras set CODIGO=:CODIGO,Usuario=:Usuario');
                ModifySQL.add(',Permissao=:Permissao');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPermissoesExtras where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPERMISSOESEXTRAS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPERMISSOESEXTRAS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPERMISSOESEXTRAS');
     Result:=Self.ParametroPesquisa;
end;

function TObjPERMISSOESEXTRAS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PERMISSOESEXTRAS ';
end;


function TObjPERMISSOESEXTRAS.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPERMISSOESEXTRAS,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPERMISSOESEXTRAS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPERMISSOESEXTRAS.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(Self.ObjqueryPesquisa);

    Freeandnil(Self.ObjDataSourceUsuarios);
    Freeandnil(Self.ObjqueryPesquisausuarios);

    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPERMISSOESEXTRAS.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPERMISSOESEXTRAS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPermissoesExtras.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjPermissoesExtras.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
//CODIFICA GETSESUBMITS


procedure TObjPERMISSOESEXTRAS.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPERMISSOESEXTRAS';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;





function TObjPERMISSOESEXTRAS.Get_permissao: string;
begin
     Result:=Self.permissao;
end;

function TObjPERMISSOESEXTRAS.Get_usuario: string;
begin
     Result:=Self.usuario;
end;

procedure TObjPERMISSOESEXTRAS.Submit_permissao(parametro: string);
begin
     Self.permissao:=parametro;
end;

procedure TObjPERMISSOESEXTRAS.Submit_usuario(parametro: string);
begin
     Self.usuario:=parametro;
end;

procedure TObjPERMISSOESEXTRAS.RetornaUsuariosUsados(ppermissao: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          if ppermissao<>''
          then begin
              sql.add('Select Tabpermissoesextras.usuario,Tabusuarios.nome,Tabusuarios.nivel,Tabpermissoesextras.codigo');
              sql.add('from tabpermissoesextras join tabusuarios on tabpermissoesextras.usuario=tabusuarios.codigo');
              sql.add('where Tabpermissoesextras.permissao='+Ppermissao+' order by tabusuarios.nome');
          end;
          Try
             open;
          Except

          End;
     End;
end;

procedure TObjPERMISSOESEXTRAS.RetornaUsuariosDisponiveis(ppermissao: string);
begin
     With Self.ObjqueryPesquisaUsuarios do
     Begin
          close;
          sql.clear;
          if ppermissao<>''
          then begin
                sql.add('select Tabusuarios.codigo as usuario,Tabusuarios.nome,Tabusuarios.nivel');
                sql.add('from tabusuarios');
                sql.add('where tabusuarios.codigo not in(');
                sql.add('Select Tabpermissoesextras.usuario');
                sql.add('from tabpermissoesextras');
                sql.add('where Tabpermissoesextras.permissao='+Ppermissao);
                sql.add(')');
                sql.add('order by tabusuarios.nome');
          end;
          Try
             open;
          Except

          End;
     End;
end;


end.



