unit UobjParametrosAvulso;
Interface
Uses ibquery,ibdatabase,Classes,Db,Ibcustomdataset,IBStoredProc, Grids,uutils;

Type
   TObjParametrosAvulso=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                IBDatabase: TIBDatabase;
                IBTransaction: TIBTransaction;

                procedure   Cria;
                Constructor Create(caminhobanco:string);overload;
                Constructor Create(caminhobanco:string;pusuario,psenha:string);overload;

                Constructor Create(Pdatabase:TibDatabase;PTransaction:TibTransaction);overload;

                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:Str09) :boolean;
                Function    LocalizaNome(parametro: Str150): boolean;
                Function    ValidaParametro(Parametro:String):Boolean;overload;
                Function    ValidaParametro(Parametro:String;Silencioso:boolean):Boolean;overload;
                Function    exclui(Pcodigo:str09;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :Str100;
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :Str100;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :Str09;
                Function Get_Nome             :Str150;
                Function Get_Valor            :sTRING;
                Function get_Funcao           :String;

                Procedure Submit_CODIGO           (parametro:Str09);
                Procedure Submit_NOME             (parametro:Str150);
                Procedure Submit_VALOR            (parametro:sTRING);
                Procedure Submit_Funcao           (parametro:String);
                function LocalizaParametroLocal(Parametro:String): Boolean;
                Procedure ResgataTodosOsParametros(Var PStrGrid:TStringGrid);
                Procedure AtualizaTodosOsParametros(Var PStrGrid:TStringGrid);
                procedure PesquisaParametros;
                
         Private
               ObjDataset:Tibdataset;
               ObjQueryPesquisa:Tibquery;

               CODIGO           :Str09;
               NOME             :STR150;
               VALOR            :STRING;
               Funcao           :String;

               ParametroPesquisa:TStringList;
               TransacaoDatabaseExterna:Boolean;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;






   End;


implementation
uses inifiles,forms,stdctrls,SysUtils,Dialogs,Controls;


{ TTabTitulo }


Function  TobjParametrosAvulso.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.Nome             :=FieldByName('Nome').asstring;
        Self.Valor            :=FieldByName('Valor').asstring;
        Self.Funcao           :=FieldByName('Funcao').asstring;
        result:=True;
     End;
end;


Procedure TobjParametrosAvulso.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring         :=Self.CODIGO;
      FieldByName('Nome').asstring           :=Self.Nome;
      FieldByName('Valor').asstring          :=Self.Valor;
      FieldByName('Funcao').asstring         :=Self.Funcao;
  End;
End;

//***********************************************************************

function TobjParametrosAvulso.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
  End;


   If Self.LocalizaNome(Self.CODIGO)=True
   Then Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End
             Else Begin
                       if (Self.Status=dsedit)
                       Then Begin
                                 if (Self.Objdataset.fieldbyname('codigo').asstring<>Self.codigo)
                                 Then Begin
                                           Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                                           result:=False;
                                           exit;
                                 End;
                       End;
             End;
        End;



   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




    if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;



 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True Then
  //Self.IBTransaction.CommitRetaining;
  Self.IBTransaction.Commit;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TobjParametrosAvulso.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        CODIGO           :='';
        nome:='';
        funcao:='';
     End;
end;

Function TobjParametrosAvulso.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (Nome='')
       Then Mensagem:=mensagem+'/Nome';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TobjParametrosAvulso.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Curso n�o Encontrado!';}

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TobjParametrosAvulso.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TobjParametrosAvulso.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TobjParametrosAvulso.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TobjParametrosAvulso.LocalizaCodigo(parametro: Str09): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,valor,funcao from tabparametros where codigo='+parametro);
           Open;

           If (recordcount>0)
           Then Result:=True
           Else
           begin
            Result:=False;
            ObjDataset.Transaction.Commit;
           end;


       End;
end;

function TobjParametrosAvulso.LocalizaNome(parametro: Str150): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,valor,funcao from tabparametros where Nome=');
           SelectSql.add(#39+parametro+#39);
           Open;


           If (recordcount>0)
           Then Result:=True
           Else
           begin
            ObjDataset.Transaction.Commit;
            Result:=False;
           end

           
       End;
end;


procedure TobjParametrosAvulso.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TobjParametrosAvulso.Exclui(Pcodigo: str09;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True) Then
        Begin

          Self.ObjDataset.delete;

          If (ComCommit=True) Then
          Self.IBTransaction.Commit;

        End
        Else
          result:=false;

     Except
           result:=false;
     End;
end;


Constructor TobjParametrosAvulso.Create(Pdatabase:TibDatabase;PTransaction:TibTransaction);
Begin
     Self.IBDatabase:=Pdatabase;
     Self.IBTransaction:=PTransaction;
     Self.TransacaoDatabaseExterna:=true;
     Self.Cria;
End;

constructor TobjParametrosAvulso.create(caminhobanco:string);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin
     Self.create(caminhobanco,'','');
end;


constructor TobjParametrosAvulso.create(caminhobanco:string;pusuario,psenha:string);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.IBTransaction:=TIBTransaction.create(nil);
        Self.IBTransaction.DefaultAction:=TARollback;
        Self.IBTransaction.Params.add('read_committed');
        Self.IBTransaction.Params.add('rec_version');
        Self.IBTransaction.Params.add('nowait');
        Self.IBDatabase:=TIBDatabase.create(nil);
        Self.IBDatabase.SQLDialect:=3;
        Self.IBDatabase.LoginPrompt:=False;
        Self.IBDatabase.DefaultTransaction:=Self.IBTransaction;
        Self.IBDatabase.Databasename:=CaminhoBanco;
        //USUARIO PROTECAO
        //SENHA   PROTECAO
        Self.IbDatabase.Params.clear;
        if (pusuario='')
        Then Begin
                  Self.IbDatabase.Params.Add('User_name='+DesincriptaSenha('��������'));
                  Self.IbDatabase.Params.Add('password='+DesincriptaSenha('��������'));
        End
        Else Begin
                  Self.IbDatabase.Params.Add('User_name='+pusuario);
                  Self.IbDatabase.Params.Add('password='+psenha);
        End;
        
        Self.IbDatabase.Open;

        //************************************************************************
        Self.TransacaoDatabaseExterna:=False;
        Self.Cria;
end;

procedure TobjParametrosAvulso.Cria;
begin

        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=Self.IbDatabase;

        Self.ObjQueryPesquisa:=Tibquery.create(nil);
        Self.ObjQueryPesquisa.Database:=Self.IBDatabase;
        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;
        Self.ParametroPesquisa:=TStringList.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,nome,valor,funcao from tabparametros where codigo=-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' insert into tabparametros (codigo,nome,valor,funcao) values (:codigo,:nome,:valor,:funcao)');

                ModifySQL.clear;
                ModifySQL.add(' Update tabparametros set codigo=:codigo,nome=:nome,valor=:valor,funcao=:funcao where codigo=:codigo ');


                DeleteSQL.clear;
                DeleteSQL.add('Delete from tabparametros where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add(' Select codigo,nome,valor,funcao from tabparametros where codigo=-1');

                //open;

                //Self.ObjDataset.First;
                Self.status:=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TobjParametrosAvulso.Get_CODIGO: Str09;
begin
        Result:=Self.Codigo;
end;

procedure TobjParametrosAvulso.Submit_CODIGO(parametro: Str09);
begin
        Self.Codigo:=parametro;
end;


procedure TobjParametrosAvulso.Commit;
begin
     //Self.IBTransaction.CommitRetaining;
     Self.IBTransaction.Commit;
end;

function TobjParametrosAvulso.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabParametros');
     Result:=Self.ParametroPesquisa;
end;

function TobjParametrosAvulso.Get_TituloPesquisa: Str100;
begin
     Result:=' Pesquisa de Parametros ';
end;



{
function TobjParametrosAvulso.Get_PesquisaTurma: Str100;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TobjParametrosAvulso.Get_TituloPesquisaTurma: Str100;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}



destructor TobjParametrosAvulso.Free;
begin

   Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);
   freeandnil(Self.objdatasource);
   freeandnil(Self.objquerypesquisa);

   //11/10/2010 - celio Adicionado pois falata desalocar
   FreeAndNil(Self.IBTransaction);
   FreeAndNil(Self.IBDatabase);

end;

function TobjParametrosAvulso.Get_Nome: Str150;
begin
     Result:=Self.Nome;
end;

function TobjParametrosAvulso.Get_Valor: String;
begin
     Result:=Self.Valor;
end;

procedure TobjParametrosAvulso.Submit_NOME(parametro: Str150);
begin
     Self.Nome:=Parametro;
end;

procedure TobjParametrosAvulso.Submit_VALOR(parametro: String);
begin
     Self.Valor:=Parametro;
end;




function TobjParametrosAvulso.LocalizaParametroLocal(Parametro:String): Boolean;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'ParametrosLocais.Ini');
     Except
           Messagedlg('Erro na Abertura do Arquivo ParametrosLocais.ini',mterror,[mbok],0);
           Result:=False;
           exit;
     End;

     Try
        Try
            Temp:='';
            Temp:=arquivo_ini.ReadString('SISTEMA',UPPERCASE(parametro),'');
        Except
              Messagedlg('Par�metro n�o encontrado "'+UpperCase(Parametro)+'"',mterror,[mbok],0);
              result:=False;
              exit;
        End;
        Result:=True;
        Self.VALOR:=UpperCase(temp);
        
     finally
         freeandnil(arquivo_ini);
     End;
    
end;

function TobjParametrosAvulso.get_Funcao: String;
begin
     Result:=Self.Funcao;
end;

procedure TobjParametrosAvulso.Submit_Funcao(parametro: String);
begin
     Self.Funcao:=Parametro;
end;

function TobjParametrosAvulso.ValidaParametro(Parametro: String): Boolean;
Begin
     Result:=Self.ValidaParametro(parametro,false);
End;

function TobjParametrosAvulso.ValidaParametro(Parametro:String;Silencioso:boolean):Boolean;
begin
     result:=False;
     
     if (Self.LocalizaNome(parametro)=False)
     Then Begin
               if (silencioso=false)
               then Messagedlg('O par�metro "'+parametro+'" n�o foi encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;
     self.ObjDataset.Transaction.Commit;
     result:=True;
end;

procedure TobjParametrosAvulso.ResgataTodosOsParametros(var PStrGrid: TStringGrid);
VAr  Linha, Coluna:Integer;
begin
     // Limpa o Grid
     For Linha:=0 To PStrGrid.RowCount-1 do
         for Coluna:=0 To PStrGrid.ColCount-1 do
               PStrGrid. Cells[Coluna,Linha]:='';

     PStrGrid.RowCount:=2;
     PStrGrid.Cells[0,0]:='Codigo';
     PStrGrid.Cells[1,0]:='Nome';
     PStrGrid.Cells[2,0]:='Valor';

     With Self.ObjDataset do
     Begin
          Close;
          SelectSQL.Clear;
          SelectSQL.Add('Select Codigo, nome, valor from TabParametros  Order By Codigo');
          Open;

          linha:=1;
          While Not (eof) do
          Begin
              if (Linha>1)
              then PStrGrid.RowCount:=PStrGrid.RowCount+1;

              PStrGrid.Cells[0,Linha]:=fieldbyname('Codigo').AsString;
              PStrGrid.Cells[1,Linha]:=fieldbyname('Nome').AsString;
              PStrGrid.Cells[2,Linha]:=fieldbyname('Valor').AsString;

              Next;
              Inc(Linha,1);
          end;

          Self.ObjDataset.Transaction.Commit;
     end;

end;

procedure TobjParametrosAvulso.AtualizaTodosOsParametros(var PStrGrid: TStringGrid);
Var Linha:Integer;
begin
     For Linha:=1 to PStrGrid.RowCount-1 do
     Begin
          Self.LocalizaCodigo(PStrGrid.Cells[0,linha]);
          Self.TabelaparaObjeto;
          self.Commit;
          Self.Status:=dsEdit;
          Self.Submit_NOME(PStrGrid.Cells[1,linha]);
          Self.Submit_VALOR(PStrGrid.Cells[2,linha]);
          Self.Salvar(true);
     end;

     MensagemAviso('Parametros Atualizados com Sucesso!');
     Self.ResgataTodosOsParametros(PStrGrid);
end;

procedure TobjParametrosAvulso.PesquisaParametros;
begin
     With Self.ObjqueryPesquisa do
     begin
          close;
          sql.clear;
          sql.add('Select codigo,nome,valor from Tabparametros order by codigo');
          open; 
     End;
end;



end.
