unit UobjPrinterMatrix;
interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

const
	// Mensagem padrao para o caso da Porta nao estar respondendo...
	MessageDefault = 'O estado atual da impressora �: %s'+#13+
  		         'A Impressora/Porta %s n�o est� respondendo.'+#13+#13+
  		         'Verifique se a impressora est� OnLine e se ainda tem papel.'+#13+
                         'Se a impressora estiver conectada na rede do Windows, entre em Configura��es de Impressoras e "Capture" a Porta %s.';

	// s�o os tamanhos m�ximos para bobinas com 75mm "respectivamente" com "TFontType"
	FontSizeBobbin75: array[0..3] of byte = (47,28,24,14);
	// s�o os tamanhos m�ximos para bobinas com 89mm "respectivamente" com "TFontType"
	FontSizeBobbin89: array[0..3] of byte = (57,33,28,17);
	// s�o os tamanhos m�ximos para Folhas com aprox. 210mm "respectivamente" com "TFontType"
        FontSizeLetter: array[0..3] of byte = (137,80,68,40);
        FontSizeCarta:array[0..3] of byte = (137,90,68,40);
  { comandos ESCAPES padr�o Epson }
	a_Con = #15;            {ativa condensado}
 	d_Con = #18;		{desativa condensado}
 	a_Exp = #27+'W'+#1;	{ativa expandido}
 	d_Exp = #27+'W'+#0;	{desativa expandido}
 	a_Neg = #27+'E';	{ativa negrito}
 	d_Neg = #27+'F';	{desativa negrito}
 	p_Default = #27+'@';	{reset}
        p_Eject = #12;		{FF - Avan�a Folha}
        p_Backspace = #8;       {Backspace - usado para a acentua��o}
        p_Return = #13;		{Retorno do Carro}
        p_Line = #10;           {Avan�a uma linha}
type
	TPorts     = (LPT1, LPT2, LPT3, LPT4, LPT5);
        TFontType  = (ftCondensed,ftSingle,ftMiddle,ftExpand);
	TAlignment = (taLeft,taCenter,taRight);
        TPaper     = (paBobbin75,paBobbin89,paLetter,paCarta);
        TPrinterStatus = (psOn,psOff,psOffLine,psPaperOut,psPrinting,psIndefined);
        
  TObjPrinterMatrix = class
  private
     FPort: TPorts;
     FHandle: TextFile;  {Handle da porta de comunicacao da impressora}
     FPortStr: String;   {� o apelido da porta LPT1...}
     FFontType: TFontType;
     FAlignment: TAlignment;
     FAdvanceLines: Byte;
     FPaper: TPaper;
     FMessageError: String;
     FBold: Boolean;
     procedure Error(Mess: String);
     procedure Convert(var Text: String);
     function Replicate(Car: Char; Quant: Integer): String;
  public
     procedure SetPort(Value: TPorts);
     procedure SetPaper(Value: TPaper);
     procedure SetFontType( Value: TFontType );
     constructor Create ;
     destructor  Free;
     function  Start: Boolean;
     procedure Terminate;
     procedure Print(Text: String; Column: Byte=0; NextLine: Boolean=True);
     procedure Eject;
     procedure Line;
     procedure Trace;
     function  MaxColumns: Byte;
     property Port:        TPorts     read FPort         write SetPort;
     property FontType:    TFontType  read FFontType     write SetFontType;
     property Alignment:   TAlignment read FAlignment    write FAlignment;
     property AdvanceLines:Byte       read FAdvanceLines write FAdvanceLines Default 0;
     property Paper:       TPaper     read FPaper        write SetPaper;
     property Bold: 	Boolean    read FBold	      write FBold;
End;

implementation






{-------------------------------------------------------------------------}
constructor TobjPrinterMatrix.Create;
begin
  inherited;
  FPort 	:= LPT1;
  FPortStr := 'Lpt1';
  FFontType := ftCondensed;
  FAlignment := taLeft;
  FPaper := paBobbin75;
  Self.Start;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.SetPort( Value: TPorts );
begin
	if FPort = Value then
  	exit;

  FPort 	:= Value;
  FPortStr := format('Lpt%d',[ord(FPort)+1]);
end;
{-------------------------------------------------------------------------}
function TobjPrinterMatrix.MaxColumns: Byte;
begin
	{ o MaxColumns depende da
  		Largura do Papel = FPaper  e do
			Tamanho da Letra = FFontType
  }
  case FPaper of
  	paBobbin75:
			MaxColumns := FontSizeBobbin75[ ord(FFontType) ];
  	paBobbin89:
     	MaxColumns := FontSizeBobbin89[ ord(FFontType) ];

        paCarta:
        MaxColumns := FontSizeCarta[ ord(FFontType) ];
  else
	MaxColumns := FontSizeLetter[ ord(FFontType) ];
  end;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.SetPaper(Value: TPaper);
begin

  {se n�o mudou}
	if FPaper = Value then
  	exit;

  FPaper := Value;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.SetFontType( Value: TFontType );
begin
  {se n�o mudou}
	if FFontType = Value then       
  	exit;

	FFontType := Value;
end;
{-------------------------------------------------------------------------}
function TobjPrinterMatrix.Start: Boolean;
begin
	result := false;

  try
        AssignFile(FHandle, FPortStr );
	ReWrite( FHandle );
        write(FHandle, p_Default);
        result := true;
  except
 	on E: Exception do begin
    	if UpperCase(E.Message) = 'FILE NOT FOUND'
        then Error('Porta n�o dispon�vel: '+FPortStr)
	else Error(E.Message);
  end;
  end;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.Terminate;
var
	i: Integer;
begin
     for i := 1 to FAdvanceLines do
  		write( FHandle, p_Line );

 		write( FHandle, p_Default );    //reseta tudo
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.Print(Text: String; Column: Byte=0; NextLine: Boolean=True);
var
	iMax: byte;  // � o m�ximo de caracteres que podem ser impresso neste "tipo de Papel" atualmente selecionado.
  i: Integer;
  EscI,EscF: String;
begin

     iMax := ord( FFontType );
     case FPaper of
	  paBobbin75: iMax := FontSizeBobbin75[iMax];
	  paBobbin89:	iMax := FontSizeBobbin89[iMax];
     else
	 iMax := FontSizeLetter[iMax];
     end;

  // para garantir, se o Texto for maior que o M�ximo, ent�o vamos 'cortar' o excesso
  Text := copy(Text,1,iMax);

  case FAlignment of
		taCenter:
     	begin
				i := (iMax - length(Text)) div 2;
				Text := replicate(' ',i)+Text;
     	end;
     taRight:
     	begin
     		i := (iMax - length(Text));
				Text := replicate(' ',i)+Text;
     	end;
  	else
			Text := replicate(' ',Column)+Text;
  end;

  // a acentua��o tem que ser depois do 'alinhamento' se n�o
  // a contagem para o alinhamento dar� errada, pois ir� contar
  // tamb�m o backspace e um caracter � mais...
  Convert(Text);  {acentua��o}


  case FFontType of
  	ftCondensed: 	begin EscI := a_Con       ; EscF := d_Con;       end;
     ftSingle:		begin EscI := d_Con+d_Exp ; EscF := '';          end;
     ftMiddle:		begin EscI := a_Con+a_Exp ; EscF := d_Con+d_Exp; end;
  else
     					begin EscI := a_Exp       ; EscF := d_Exp;       end;
  end;

  try
  	// volta o carro
     Write( FHandle, p_Return);

     // imprime a linha
  	Write( FHandle, EscI+Text+EscF );

  	// aqui simplesmente imprime duas vezes se for Bold
  	if FBold then begin
        Write( FHandle, p_Return);
  		Write( FHandle, EscI+Text+EscF );
     end;

     if NextLine then
     	Line;
  except
  	on E: Exception do begin
     	Error(E.Message);
     end;
  end;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.Eject;
begin

  try
  	Write( FHandle, p_Eject );
  except
  	on E: Exception do begin
     	Error(E.Message);
  end;
  end;
end;
{-------------------------------------------------------------------------}
function TobjPrinterMatrix.Replicate(Car: Char;  Quant: Integer): String;
var
	X: Integer;
begin
	result := '';
	for X := 1 to Quant do
  	result := result + Car;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.Error(Mess: String);
var
	m: String;
begin
	if Mess = 'Default' then begin
  	FMessageError := format(MessageDefault,[m,FPortStr,FPortStr])
  end else
	FMessageError := Mess;
  	raise exception.create('(PrinterMatrix) - '+FMessageError);
  abort;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.Line;
begin

  try
  	Write( FHandle, p_Line);
  except
  	on E: Exception do begin
     	Error(E.Message);
     end;
  end;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.Trace;
begin

  try
  	Print(replicate('-', MaxColumns),0,true);
  except
  	on E: Exception do begin
     	Error(E.Message);
     end;
  end;
end;
{-------------------------------------------------------------------------}
procedure TobjPrinterMatrix.Convert(var Text: String);
const
	Max=22;
	Chars: array[1..Max] of string[1] = ('�','�','�','�','�','�','�','�','�','�','�',
  												 '�','�','�','�','�','�','�','�','�','�','�');
	Esc  : array[1..Max] of string[3] = (
  			''''+p_Backspace+'a', ''''+p_Backspace+'e',
           ''''+p_Backspace+'i', ''''+p_Backspace+'o',
           ''''+p_Backspace+'u', '~'+p_Backspace+'a',
           '~'+p_Backspace+'o',  ','+p_Backspace+'c',
           '`'+p_Backspace+'a',  '`'+p_Backspace+'o',
				'^'+p_Backspace+'e',  ''''+p_Backspace+'A',
           ''''+p_Backspace+'E', ''''+p_Backspace+'I',
           ''''+p_Backspace+'O', ''''+p_Backspace+'U',
           '~'+p_Backspace+'A',  '~'+p_Backspace+'O',
           ','+p_Backspace+'C',  '`'+p_Backspace+'A',
           '`'+p_Backspace+'O',  '^'+p_Backspace+'E'

  );
var
	ii,p: Integer;
  S,C: String;
begin
	{ aqui ir� converter estes caracteres acentuados }

	for ii := 1 to Max do begin
		while true do begin
     	s := Chars[ii];
  		p := Pos( s , Text );
     	if p <= 0 then
     		break;  // sai do while

			// achou, entao substituir
        c := Esc[ii];
        S := copy(Text,1,p-1)  +c+   copy(Text,p+1, length(Text)-p );
     	Text := S;
		end;
  end;
end;
destructor TObjPrinterMatrix.Free;
begin
   CloseFile( FHandle );
end;

end.
