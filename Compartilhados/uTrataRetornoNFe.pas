unit uTrataRetornoNFe;

interface

uses uClasseBase;

type
  TTrataRetornoNFe = class( TClasseBase )
  private

  public
    function trataRetornoUsuario( ptexto:string ):string;
  end;

implementation

uses SysUtils;


{ TTrataRetornoNFe }

function TTrataRetornoNFe.trataRetornoUsuario(ptexto: string): string;
begin
  if( Pos( 'requisi��o n�o enviada', LowerCase( ptexto ) ) > 0 ) then
    if( Pos( 'the server name or address could not be resolved', LowerCase( ptexto ) ) > 0 ) then
      Result := 'N�o foi poss�vel estabelecer conex�o com o servidor. Cheque sua conex�o com a internet';
end;

end.
 