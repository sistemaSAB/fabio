unit UobjParametros;
Interface
Uses rdprint,Classes,Db,UessencialGlobal,Ibcustomdataset,Grids,IBDatabase;

Type

   TparametrosSTRL=record
     CODIGO:TSTRINGLIST;
     Nome:TSTRINGLIST;
     VALOR:TSTRINGLIST;

     GetValor:String;
     GetCodigo:String;
     GetNome:String;
   End;

   TObjParametros=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                DATABASELOCAL:TIBDatabase;
                TRANSACTIONLOCAL:TIBTransaction;

                Constructor Create;overload;
                Constructor Create(pDatabase:TIBDatabase;pTransaction:TIBTransaction);overload;

                Destructor  Free;
                Function    LocalizaCodigo(Parametro:string) :boolean;


                //*****************FORAM TRANSPOSTAS PARA STRING LIST*******************
                //Function    LocalizaNome(parametro: string): boolean;
                function    ValidaParametro(Parametro: String; Silenciosa:Boolean=False): Boolean;
                Function    Get_Valor            :sTRING;
                //**********************************************************************



                Function    Salvar(ComCommit:Boolean)       :Boolean;

                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Procedure Opcoes;
                function  VerificaPermissao: Boolean;
                function  LocalizaParametroLocal(Parametro:String): Boolean;
                Procedure Imprime;
                Procedure ResgataTodosOsParametros(Var PStrGrid:TStringGrid);
                Procedure AtualizaTodosOsParametros(Var PStrGrid:TStringGrid);



                Procedure Submit_PCODIGO           (parametro:string);
                Procedure Submit_PNOME             (parametro:string);
                Procedure Submit_PVALOR            (parametro:sTRING);
                Procedure Submit_PFuncao           (parametro:String);
                Procedure Submit_Popcoes (parametro:String);

                Function Get_PCODIGO           :string;
                Function Get_PNome             :String;
                Function Get_PValor             :String;
                Function get_PFuncao           :String;
                Function get_Popcoes            :String;
                function LocalizaPNome(parametro: string): boolean;
                Function PValidaParametro(Parametro:String):Boolean;

                Function    PTabelaparaObjeto:Boolean;
                Procedure   PZerarTabela;
                Function    Pexclui(Pcodigo:string;ComCommit:boolean)            :Boolean;

                Procedure   RecarregaParametro_STL;

         Private
               ObjDataset:Tibdataset;
               STLPARAMETROS:TparametrosSTRL;
               CODIGO           :string;
               NOME             :string;
               VALOR            :STRING;
               Funcao           :String;
               Parametro_opcoes            :String;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure Exporta;

   End;


implementation
uses inifiles,forms,SysUtils,Dialogs,UDatamodulo,Controls,uopcaorel,
  UReltxtRDPRINT;


{ TTabTitulo }


Function  TObjParametros.PTabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO           :=FieldByName('CODIGO').asstring;
        Self.Nome             :=FieldByName('Nome').asstring;
        Self.Valor            :=FieldByName('Valor').asstring;
        Self.Funcao           :=FieldByName('Funcao').asstring;
        Self.parametro_Opcoes           :=FieldbyName('opcoes').AsString;
        result:=True;
     End;
end;


Procedure TObjParametros.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
      FieldByName('CODIGO').asstring         :=Self.CODIGO;
      FieldByName('Nome').asstring           :=Self.Nome;
      FieldByName('Valor').asstring          :=Self.Valor;
      FieldByName('Funcao').asstring         :=Self.Funcao;
      FieldByName('opcoes').asstring         :=Self.parametro_Opcoes;
  End;
End;

//***********************************************************************

function TObjParametros.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
  End;


   If Self.LocalizaCodigo(Self.CODIGO)=True
   Then Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
             End
             Else Begin
                       if (Self.Status=dsedit)
                       Then Begin
                                 if (Self.Objdataset.fieldbyname('codigo').asstring<>Self.codigo)
                                 Then Begin
                                           Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                                           result:=False;
                                           exit;
                                 End;
                       End;
             End;
        End;



   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




    if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;



 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then Begin
            TRANSACTIONLOCAL.CommitRetaining;
            Self.RecarregaParametro_STL;
 End;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjParametros.PZerarTabela;//Ok
Begin
     With Self do
     Begin
        CODIGO :='';
        nome   :='';
        funcao :='';
        parametro_Opcoes  :='';
     End;
end;

Function TObjParametros.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (Nome='')
       Then Mensagem:=mensagem+'/Nome';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjParametros.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjParametros.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjParametros.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjParametros.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjParametros.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,valor,funcao, opcoes from tabparametros where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;



function TObjParametros.LocalizaPNome(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select codigo,nome,valor,funcao, opcoes from tabparametros where Nome=');
           SelectSql.add(#39+parametro+#39);
           Open;
           last;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjParametros.PExclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then TRANSACTIONLOCAL.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjParametros.create;
begin
      self.Create(FDataModulo.IBDatabase,FDataModulo.IBTransaction);
end;

constructor TObjParametros.Create(pDatabase: TIBDatabase;
  pTransaction: TIBTransaction);
begin

        DATABASELOCAL := pDatabase;
        TRANSACTIONLOCAL := pTransaction;

        Self.STLPARAMETROS.codigo:=TStringList.create;
        Self.STLPARAMETROS.nome:=TStringList.create;
        Self.STLPARAMETROS.valor:=TStringList.create;

        Self.STLPARAMETROS.codigo.clear;
        Self.STLPARAMETROS.nome.clear;
        Self.STLPARAMETROS.valor.clear;

        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=DATABASELOCAL;
        //Self.CodCurso:=TObjCursos.create;
        Self.ParametroPesquisa:=TStringList.create;

        Self.PZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select codigo,nome,valor,funcao, opcoes from tabparametros where codigo=-1');

                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add(' insert into tabparametros (codigo,nome,valor,funcao,opcoes) values (:codigo,:nome,:valor,:funcao, :opcoes)');

                ModifySQL.clear;
                ModifySQL.add(' Update tabparametros set codigo=:codigo,nome=:nome,valor=:valor,funcao=:funcao,opcoes=:opcoes where codigo=:codigo ');


                DeleteSQL.clear;
                DeleteSQL.add('Delete from tabparametros where codigo=:codigo');

                RefreshSQL.clear;
                RefreshSQL.add(' Select codigo,nome,valor,funcao,opcoes from tabparametros where codigo=-1');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

        Self.RecarregaParametro_STL;
end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjParametros.Get_PCODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjParametros.Submit_PCODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;



function TObjParametros.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabParametros');
     Result:=Self.ParametroPesquisa;
end;

function TObjParametros.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Parametros ';
end;

destructor TObjParametros.Free;
begin
    Freeandnil(Self.ObjDataset);
    Freeandnil(Self.ParametroPesquisa);

    Freeandnil(Self.STLPARAMETROS.codigo);
    Freeandnil(Self.STLPARAMETROS.nome);
    Freeandnil(Self.STLPARAMETROS.valor);

end;

function TObjParametros.Get_PNome: string;
begin
     Result:=Self.Nome;
end;

function TObjParametros.Get_PValor: String;
begin
     Result:=Self.Valor;
end;

procedure TObjParametros.Submit_PNOME(parametro: string);
begin
     Self.Nome:=Parametro;
end;

procedure TObjParametros.Submit_PVALOR(parametro: String);
begin
     Self.Valor:=Parametro;
end;

procedure TObjParametros.opcoes;
begin
     With FopcaoRel do
     Begin
          With Rgopcoes do
          Begin
                items.clear;
                items.add('Exporta Todos par�metros');//1
          End;
          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case Rgopcoes.ItemIndex of
                0:Self.Exporta;
          End;

     end;


end;

procedure TObjParametros.Exporta;
var
arq:textfile;
path:string;
begin
     path:='';
     path:=ExtractFilePath(application.exename);
     if (path[length(path)]<>'\')
     Then path:=path+'\';
     path:=path+'Parametros.txt';
     AssignFile(arq,path);
     Try
        Rewrite(arq);
     Except
           Messagedlg('N�o foi poss�vel criar o arquivo '+path,mterror,[mbok],0);
           exit;
     End;
     Try
        With Self.objdataset do
        Begin
             close;
             SelectSQL.clear;
             SelectSQL.add('Select * from Tabparametros order by codigo');
             open;
             first;
             While not(eof) do
             Begin
                Writeln(arq,'Insert into tabParametros (codigo,nome,valor,opcoes) values (0,'''+fieldbyname('nome').asstring+''','''+fieldbyname('valor').asstring+''','''+fieldbyname('opcoes').asstring+''');');
                next;
             End;
             Messagedlg('Exporta��o Conclu�da!',mtinformation,[mbok],0);
             exit;
        End;
     Finally
            CloseFile(arq);
     End;

end;


function TObjParametros.VerificaPermissao: Boolean;
begin
     result:=False;
     
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE PARAMETROS')=true)
     then result:=True;
     
end;


function TObjParametros.LocalizaParametroLocal(Parametro:String): Boolean;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'ParametrosLocais.Ini');
     Except
           Messagedlg('Erro na Abertura do Arquivo ParametrosLocais.ini',mterror,[mbok],0);
           Result:=False;
           exit;
     End;

     Try
        Try
            Temp:='';
            Temp:=arquivo_ini.ReadString('SISTEMA',UPPERCASE(parametro),'');
        Except
              Messagedlg('Par�metro n�o encontrado "'+UpperCase(Parametro)+'"',mterror,[mbok],0);
              result:=False;
              exit;
        End;
        Result:=True;
        Self.VALOR:=UpperCase(temp);
        
     finally
         freeandnil(arquivo_ini);
     End;
    
end;

function TObjParametros.get_PFuncao: String;
begin
     Result:=Self.Funcao;
end;

procedure TObjParametros.Submit_PFuncao(parametro: String);
begin
     Self.Funcao:=Parametro;
end;

function TObjParametros.get_Popcoes: String;
begin
     Result:=Self.parametro_Opcoes;
end;

procedure TObjParametros.Submit_Popcoes(parametro: String);
begin
     Self.parametro_Opcoes:=Parametro;
end;

procedure TObjParametros.Imprime;
var
cont,linha:Integer;
Linha1:String;
begin
     With Self.Objdataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select * from Tabparametros order by Nome');
          open;
          if (recordcount=0)
          Then exit;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.RDprint.ImpC(linha,45,'PARAMETROS DO SISTEMA',[negrito]);
          inc(linha,2);
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('C�DIGO',6,' ')+' '+
                                             CompletaPalavra('NOME',83,' '),[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,2);
          first;
          While not(eof) do
          Begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                                   CompletaPalavra(fieldbyname('nome').asstring,83,' '));
               inc(linha,1);
               //descricao
               linha1:='';
               if (fieldbyname('funcao').asstring<>'')
               Then Begin
                        //sempre imprimo de 90 em 90
                        for cont:=1 to length(fieldbyname('funcao').asstring) do
                        Begin
                             if ((cont mod 89)=0)
                             Then Begin
                                     linha1:=linha1+fieldbyname('funcao').asstring[cont];
                                     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                     FreltxtRDPRINT.rdprint.imp(linha,1,linha1);
                                     inc(linha,1);
                                     linha1:='';
                             End
                             Else linha1:=linha1+fieldbyname('funcao').asstring[cont];
                        End;
                        if (linha1<>'')
                        then Begin
                                  FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.rdprint,linha);
                                  FreltxtRDPRINT.rdprint.imp(linha,1,linha1);
                                  inc(linha,1);
                                  linha1:='';
                        End;
               End;
               next;
          End;
          Freltxtrdprint.RDprint.Fechar;
          
     End;     
          
          
end;      
          
function TObjParametros.PValidaParametro(Parametro: String): Boolean;
begin
     result:=False;
     Self.PZerarTabela;
     if (Self.LocalizaPNome(parametro)=False)
     Then Begin
               Messagedlg('O par�metro "'+parametro+'" n�o foi encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.PTabelaparaObjeto;
     result:=True;
end;

procedure TObjParametros.ResgataTodosOsParametros(var PStrGrid: TStringGrid);
VAr  Linha, Coluna:Integer;
begin
     // Limpa o Grid
     For Linha:=0 To PStrGrid.RowCount-1 do
         for Coluna:=0 To PStrGrid.ColCount-1 do
               PStrGrid. Cells[Coluna,Linha]:='';

     PStrGrid.RowCount:=2;
     PStrGrid.Cells[0,0]:='Codigo';
     PStrGrid.Cells[1,0]:='Nome';
     PStrGrid.Cells[2,0]:='Valor';

     With Self.ObjDataset do
     Begin
          Close;
          SelectSQL.Clear;
          SelectSQL.Add('Select Codigo, nome, valor from TabParametros  Order By Codigo');
          Open;

          linha:=1;
          While Not (eof) do
          Begin
              if (Linha>1)
              then PStrGrid.RowCount:=PStrGrid.RowCount+1;

              PStrGrid.Cells[0,Linha]:=fieldbyname('Codigo').AsString;
              PStrGrid.Cells[1,Linha]:=fieldbyname('Nome').AsString;
              PStrGrid.Cells[2,Linha]:=fieldbyname('Valor').AsString;

              Next;
              Inc(Linha,1);
          end;
     end;
end;

procedure TObjParametros.AtualizaTodosOsParametros(var PStrGrid: TStringGrid);
Var Linha:Integer;
begin
     InicializaBarradeProgressoRelatorio(PStrGrid.RowCount,'Atualizando Par�metros do Sistema....');
     For Linha:=1 to PStrGrid.RowCount-1 do
     Begin
          IncrementaBarraProgressoRelatorio;
          Self.LocalizaCodigo(PStrGrid.Cells[0,linha]);
          Self.PTabelaparaObjeto;
          Self.Status:=dsEdit;
          Self.Submit_PNOME(PStrGrid.Cells[1,linha]);
          Self.Submit_PVALOR(PStrGrid.Cells[2,linha]);
          Self.Salvar(true);
     end;
     FechaBarraProgressoRelatorio;

     MensagemAviso('Par�metros Atualizados com Sucesso!');
     Self.ResgataTodosOsParametros(PStrGrid);
end;

function TObjParametros.Get_Valor: STRING;
begin
     //String List
     //Este Valor foi preenchido no procedimento ValidaParametro
     Result:=Self.STLPARAMETROS.GetValor;
end;

function TObjParametros.ValidaParametro(Parametro: String; Silenciosa:Boolean=False): Boolean;
var
posicao:integer;
begin

  //STRINGLIT
  posicao:=Self.STLPARAMETROS.Nome.IndexOf(uppercase(parametro));

  Self.STLPARAMETROS.GetValor:='';
  Self.STLPARAMETROS.Getnome:='';
  Self.STLPARAMETROS.GetCodigo:='';


  if (posicao<0) then
  begin
    if(silenciosa = False) then
      Messagedlg('O par�metro "'+parametro+'" n�o foi encontrado',mterror,[mbok],0);
    result:=false;
    exit;
  end;

  result:=True;
  //preenchendo as 3 variaveis do registro com os valores
  Self.STLPARAMETROS.GetValor:=Self.STLPARAMETROS.VALOR[posicao];
  Self.STLPARAMETROS.Getnome:=Self.STLPARAMETROS.NOME[posicao];
  Self.STLPARAMETROS.GetCodigo:=Self.STLPARAMETROS.CODIGO[posicao];
end;



procedure TObjParametros.RecarregaParametro_STL;
begin
     Self.STLPARAMETROS.codigo.clear;
     Self.STLPARAMETROS.nome.clear;
     Self.STLPARAMETROS.valor.clear;

     With Self.ObjDataset do
     Begin
          close;
          selectsql.clear;
          selectsql.add('Select codigo,nome,valor from tabparametros order by nome');
          open;

          while not(eof) do
          Begin
               Self.STLPARAMETROS.CODIGO.add(Self.ObjDataset.fieldbyname('codigo').asstring);
               Self.STLPARAMETROS.NOME.add(uppercase(Self.ObjDataset.fieldbyname('nome').asstring));
               Self.STLPARAMETROS.VALOR.add(Self.ObjDataset.fieldbyname('valor').asstring);
              next;
          End;
     End;
end;


end.
