{*------------------------------------------------------------------------------
  unit para envio de e-mail que encapsula o componente ACBRMail
  @author  Celio
  @version 2016/09/09 1.0.0.0 Initial revision.
  @todo
  @a vers�o deste arquivo deve seguir a versao do projeto Mail
-------------------------------------------------------------------------------}         
unit uEnviaEmail;

interface

uses Classes, Variants, mimemess, synachar, ACBrMail, uClasseBase, uGlobal, Dialogs;

type
  TEnviaEmail = class( TClasseErro )
  private
    ACBRMail: TACBrMail;
    listaDestinatario, listaCopia, listaAnexos: TStringList;
    mensagem: TStrings;
    
    FcorpoHTML: boolean;
    Fassunto: string;

    host, usuario, senha: string;
    porta: Integer;
    tls: boolean;
    ssl: Boolean;

    procedure limpaComponente;
    procedure configuraPadrao;
    procedure validaPreenchimento;
    procedure validaRemetente;
    procedure validaDadosEmail;
    procedure preencheComponente;
    procedure preencheManual;
  public
    property assunto: string read Fassunto write Fassunto;
    property corpoHTML: boolean read FcorpoHTML write FcorpoHTML;

    constructor create;
    destructor destroy;override;
    procedure configuraRemetente( host, usuario, senha: string; porta: Integer; tls: boolean = False; ssl: Boolean = true );
    procedure adicionaDestinatario( email: string );
    procedure adicionaComCopia( email: string );
    procedure adicionaAnexo( arquivo: string );
    procedure adicionaMensagem( mensagem: TStrings );

    procedure envia;
  end;
  

implementation

uses SysUtils;

{ TEnviaEmail }

procedure TEnviaEmail.limpaComponente;
begin
  ACBRMail.Clear;
  ACBRMail.ClearAttachments;
  ACBRMail.Host := '';
  ACBRMail.Username := '';
  ACBRMail.Password := '';
  ACBRMail.Port := '';
  ACBRMail.SetTLS := false;
  ACBRMail.SetSSL := false;

end;


procedure TEnviaEmail.adicionaAnexo(arquivo: string);
begin
  if( not testaVazio( arquivo ) ) then
    listaAnexos.Add( arquivo );
end;

procedure TEnviaEmail.adicionaComCopia(email: string);
begin
  if( not testaVazio(email) ) then
    listaCopia.Add( email );
end;

procedure TEnviaEmail.adicionaDestinatario(email: string);
begin
  if( not testaVazio(email) ) then
    listaDestinatario.Add( email );
end;

procedure TEnviaEmail.adicionaMensagem(mensagem: TStrings);
begin
  self.mensagem := mensagem;
end;

procedure TEnviaEmail.configuraRemetente(host, usuario, senha: string;
  porta: Integer; tls, ssl: Boolean);
begin
  self.Host := host;
  self.usuario := usuario;
  self.senha := senha;
  self.porta := porta; // troque pela porta do seu servidor smtp
  self.tls := tls;  // Verifique se o seu servidor necessita SSL
  self.ssl := ssl;
end;

constructor TEnviaEmail.create;
begin
  inherited;
  ACBRMail          := TACBrMail.Create( nil );
  listaDestinatario := TStringList.Create;
  listaCopia        := TStringList.Create;
  listaAnexos       := TStringList.Create;
  mensagem          := TStringList.Create;

  limpaComponente;
  configuraPadrao;
end;

destructor TEnviaEmail.destroy;
begin
  FreeAndNil( ACBRMail );
  FreeAndNil( listaDestinatario );
  FreeAndNil( listaCopia );
  FreeAndNil( listaAnexos );
  //FreeAndNil( mensagem );
  inherited;
end;

procedure TEnviaEmail.envia;
begin

  validaPreenchimento;
  if teveErro then
    Exit;

  preencheComponente;
  if teveErro then
    Exit;

  //preencheManual;
  try
    try
      ACBRMail.Send( false );
    except
      on e:Exception do
      begin
        addErro( 'Erro inexperado: ' + e.Message );
        Exit;
      end;
    end;
  finally

  end;
end;

procedure TEnviaEmail.validaDadosEmail;
begin
  if( testaVazio( Self.assunto ) ) then
    addErro( 'Assunto do e-mail vazio.'+#13+'-Adicione um assunto.' );

  if( listaDestinatario.Count = 0 ) then
    addErro( 'E-mail do destinat�rio vazio.'+#13+'-Adicione um destinat�rio.' );

  if( mensagem.Count  = 0 ) then
    addErro( 'Sem mensagem no e-mail.'+#13+'-Adicione a mensagem.' );

end;

procedure TEnviaEmail.validaPreenchimento;
begin
  validaRemetente;
  validaDadosEmail;
end;

procedure TEnviaEmail.validaRemetente;
begin
  if( testaVazio( self.Host ) ) then
    addErro( 'HOST SMTP Remetente n�o configurado'+#13+'-Configure atrav�s do m�todo "configuraRemetente"' );
  if( testaVazio( self.usuario ) ) then
    addErro( 'Usu�rio Remetente n�o configurado'+#13+'-Configure atrav�s do m�todo "configuraRemetente"'  );
  //if( testaVazio( ACBRMail.Password ) ) then
  //  addErro( 'HOST n�o configurado' );
  if( testaVazio( IntToStr( self.porta ) ) ) then
    addErro( 'Porta do HOST SMTP n�o configurado'+#13+'-Configure atrav�s do m�todo "configuraRemetente"'  );
end;

procedure TEnviaEmail.configuraPadrao;
begin
  self.corpoHTML := False;
  ACBRMail.Priority := MP_normal;
  ACBRMail.Attempts := 3;
  ACBRMail.DefaultCharset := UTF_8;
end;

procedure TEnviaEmail.preencheComponente;
var
  i: Integer;
begin

  ACBRMail.Clear;
  ACBRMail.ClearAttachments;

  ACBRMail.Subject := self.assunto;

  for i := 0 to listaDestinatario.Count -1 do
    ACBRMail.AddAddress( listaDestinatario[i], '' );

  for i := 0 to listaCopia.Count -1 do
    ACBRMail.AddCC( listaCopia[i] );

  for i := 0 to listaAnexos.Count -1 do
    if( FileExists( listaAnexos[i] ) ) then
      ACBRMail.AddAttachment( listaAnexos[i] )
    else
      addErro( 'N�o foi poss�vel incluir o anexo: ' + listaAnexos[i] );

  if( self.corpoHTML ) then
    ACBRMail.Body.Assign( Self.mensagem )
  else
    ACBRMail.AltBody.Assign( self.mensagem );

  ACBrMail.Host := host;
  ACBrMail.Username := usuario;
  ACBrMail.Password := senha;
  ACBrMail.Port := IntToStr( porta ); // troque pela porta do seu servidor smtp
  ACBrMail.SetTLS := tls;  // Verifique se o seu servidor necessita SSL
  ACBrMail.SetSSL := ssl;

end;

procedure TEnviaEmail.preencheManual;
var
  mail:TStringList;
begin
  mail := TStringList.Create;
  mail.Add('teste linha 1 manual ' + DateTimeToStr( now ));
  mail.Add('teste linha 1 manual ' + DateTimeToStr( now ));
  mail.Add('teste linha 1 manual ' + DateTimeToStr( now ));
  mail.Add('teste linha 1 manual ' + DateTimeToStr( now ));
  mail.Add('teste linha 1 manual ' + DateTimeToStr( now ));

  ACBrMail.Clear;
  ACBrMail.Subject := 'teste e-mail acbr manual ' + DateTimeToStr( now );
  ACBrMail.AddAddress('murakamicelio@gmail.com', 'Celio Murakami');
  //ACBrMail.AddCC('satoshimurakami@hotmail.com'); // opcional
  ACBrMail.AltBody.Assign( mail );
  //para anexar html ACBrMail1.Body.Assign( mBody.Lines )
  //ACBrMail.AddAttachment( 'C:\Users\Celio\Downloads\1455300258_import-export.png' );
  //ACBrMail.AddAttachment( 'C:\Users\Celio\Downloads\2217_EXCLAIM TECNOLOGIA EM INFORMATICA LTDA.pdf' );
  //ACBrMail.AddAttachment( 'C:\Users\Celio\Downloads\50160905537538000153550010000000101000000102-nfe.xml' );

  //configurando
  ACBrMail.Host := 'smtp.gmail.com';
  ACBrMail.Username := 'murakamicelio@gmail.com';
  ACBrMail.Password := 'pgrzfjjvqbtzsawh';
  ACBrMail.Port := '465'; // troque pela porta do seu servidor smtp
  ACBrMail.SetTLS := False;  // Verifique se o seu servidor necessita SSL
  ACBrMail.SetSSL := True;
end;

end.
