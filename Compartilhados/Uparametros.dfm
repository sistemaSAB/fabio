object Fparametros: TFparametros
  Left = 599
  Top = 239
  Width = 703
  Height = 521
  Caption = 'Cadastro de Par'#226'metros - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 206
    Width = 687
    Height = 227
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 2
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Todos'
      object StrGrid: TStringGrid
        Left = 0
        Top = 35
        Width = 679
        Height = 164
        Align = alClient
        ColCount = 3
        RowCount = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        ParentFont = False
        ScrollBars = ssNone
        TabOrder = 1
        OnDblClick = StrGridDblClick
        ColWidths = (
          65
          296
          351)
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 679
        Height = 35
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        DesignSize = (
          679
          35)
        object btCarregar: TBitBtn
          Left = 9
          Top = 7
          Width = 141
          Height = 23
          Anchors = [akTop]
          Caption = 'Carregar Parametros'
          TabOrder = 0
          OnClick = btCarregarClick
        end
        object btAtualizar: TBitBtn
          Left = 167
          Top = 7
          Width = 141
          Height = 23
          Anchors = [akTop]
          Caption = 'Atualizar Parametros'
          TabOrder = 1
          OnClick = btAtualizarClick
        end
      end
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 687
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 511
      Top = 0
      Width = 176
      Height = 51
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Par'#226'metros'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -27
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Btnovo: TBitBtn
      Left = 1
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 101
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 401
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object btopcoes: TBitBtn
      Left = 351
      Top = -1
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = btopcoesClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 433
    Width = 687
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 3
    DesignSize = (
      687
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 687
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 389
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 858
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 858
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 51
    Width = 687
    Height = 155
    Align = alTop
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 685
      Height = 153
      Align = alClient
      Stretch = True
    end
    object Label1: TLabel
      Left = 11
      Top = 10
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 11
      Top = 35
      Width = 110
      Height = 14
      Caption = 'Nome do Par'#226'metro'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 11
      Top = 60
      Width = 91
      Height = 14
      Caption = 'Valor / Descri'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 11
      Top = 85
      Width = 39
      Height = 14
      Caption = 'Fun'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbopcoes: TLabel
      Left = 521
      Top = 60
      Width = 42
      Height = 14
      Caption = 'Op'#231#245'es'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCodigo: TEdit
      Left = 124
      Top = 8
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object edtnome: TEdit
      Left = 124
      Top = 31
      Width = 391
      Height = 19
      CharCase = ecUpperCase
      MaxLength = 150
      TabOrder = 1
    end
    object memofuncao: TMemo
      Left = 124
      Top = 77
      Width = 391
      Height = 68
      Lines.Strings = (
        'memofuncao')
      TabOrder = 3
      OnKeyPress = memofuncaoKeyPress
    end
    object ComboValor: TComboBox
      Left = 124
      Top = 53
      Width = 391
      Height = 21
      BevelKind = bkSoft
      Ctl3D = False
      ItemHeight = 13
      ParentCtl3D = False
      PopupMenu = PopupMenuOpcoes
      TabOrder = 2
    end
    object Memoopcoes: TMemo
      Left = 520
      Top = 77
      Width = 161
      Height = 68
      Hint = 'Use ";" para separar as Op'#231#245'es'
      Lines.Strings = (
        'memofuncao')
      TabOrder = 4
      OnKeyPress = memofuncaoKeyPress
    end
  end
  object PopupMenuOpcoes: TPopupMenu
    TrackButton = tbLeftButton
    Left = 536
    Top = 59
    object Editarvalorcomformatao1: TMenuItem
      Caption = 'Editar valor com formata'#231#227'o'
      OnClick = Editarvalorcomformatao1Click
    end
  end
end
