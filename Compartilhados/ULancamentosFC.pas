unit ULancamentosFC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls,UobjLancamentosFC, StdCtrls, Mask, ExtCtrls, Grids,
  DBGrids, DB, Buttons;

type
  TFLancamentosFrentedeCaixa = class(TForm)
    PanelPrincipal: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edtvenda: TEdit;
    edtvencimento1: TMaskEdit;
    edtvencimento2: TMaskEdit;
    btiniciar: TButton;
    MemoLOG: TMemo;
    ProgressBar1: TProgressBar;
    Label5: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btiniciarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtvendaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtvencimento1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtvencimento2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    LancamentosFC : TobjLancamentosFC;

    { Private declarations }
  public
    { Public declarations }
    procedure PassaObjeto(pobjLancamentosFC:TobjLancamentosFC);
  end;

var
  FLancamentosFrentedeCaixa: TFLancamentosFrentedeCaixa;

implementation

uses UessencialGlobal;

{$R *.dfm}

{ TFLancamentosFrentedeCaixa }

procedure TFLancamentosFrentedeCaixa.PassaObjeto(
  pobjLancamentosFC: TobjLancamentosFC);
begin
      self.LancamentosFC := pobjLancamentosFC;
      LancamentosFC.CarregaLOG(memolog);
end;

procedure TFLancamentosFrentedeCaixa.FormCreate(Sender: TObject);
begin
      self.LancamentosFC := nil;
end;


procedure TFLancamentosFrentedeCaixa.btiniciarClick(Sender: TObject);
begin
      self.LancamentosFC.ProcessaRecebimentos(edtvenda.Text,edtvencimento1.Text,edtvencimento2.Text);
end;

procedure TFLancamentosFrentedeCaixa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
      gravalog(pegapathsistema + 'LogLancamentosFC' + FormatDateTime('yyyymmdd',Now) + '.txt' ,MemoLOG.Text);
end;

procedure TFLancamentosFrentedeCaixa.edtvendaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      Self.LancamentosFC.EdtVENDAKeyDown(Sender,Key,Shift,nil);
end;

procedure TFLancamentosFrentedeCaixa.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFLancamentosFrentedeCaixa.edtvencimento1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      BarraDataKeyUP(Sender,Key,shift);
end;

procedure TFLancamentosFrentedeCaixa.edtvencimento2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      BarraDataKeyUP(Sender,Key,shift);
end;

end.


