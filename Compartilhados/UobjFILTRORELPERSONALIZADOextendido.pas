unit Uobjfiltrorelpersonalizadoextendido;
Interface
Uses sysutils,Ibquery,windows,Classes,Db,UessencialGlobal,UOBJfiltroRELPERSONALIZADO;
Type
   Tobjfiltrorelpersonalizadoextendido=class(tobjfiltrorelpersonalizado)

         Public
               ObjDatasource                               :TDataSource;
               a:integer;
               constructor create;override; 
               destructor free;override;

               Procedure PesquisaFiltros(PRelatorio:string);

         Private
                ObjqueryPesquisa:Tibquery;
   End;


implementation

uses UDataModulo;
{ Tobjfiltrorelpersonalizadoextendido }

constructor Tobjfiltrorelpersonalizadoextendido.create;
begin
        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.dataset:=Self.ObjqueryPesquisa;
        inherited create;
end;

destructor Tobjfiltrorelpersonalizadoextendido.free;
begin
     freeandnil(Self.ObjDatasource);
     freeandnil(Self.ObjqueryPesquisa);
     inherited free;//chamando o Free do Pai
end;

procedure Tobjfiltrorelpersonalizadoextendido.PesquisaFiltros(
  PRelatorio: string);
begin
     With Self.ObjqueryPesquisa do
     begin
          close;
          sql.clear;
          sql.add('select TABFILTRORELPERSONALIZADO.CODIGO');
          sql.add('     , TABFILTRORELPERSONALIZADO.NOME');
          sql.add('     , TABFILTRORELPERSONALIZADO.RELATORIO');
          sql.add('     , TABFILTRORELPERSONALIZADO.TIPO');
          sql.add('     , TABFILTRORELPERSONALIZADO.CAPTIONLABEL');
          sql.add('     , TABFILTRORELPERSONALIZADO.MASCARA');
          sql.add('     , TABFILTRORELPERSONALIZADO.REQUERIDO');
          sql.add('     , TABFILTRORELPERSONALIZADO.USAMESMOEMBRANCO');
          sql.add('     , TABFILTRORELPERSONALIZADO.COMANDOSQLCHAVEESTRANGEIRA');
          sql.add('     , TABFILTRORELPERSONALIZADO.CAMPOCHAVESTRANGEIRA');
          sql.add('     , TABFILTRORELPERSONALIZADO.COMANDOANDPRINCIPAL');
          sql.add('     , TABFILTRORELPERSONALIZADO.COMANDOANDREPETICAO');
          sql.add('     , TABFILTRORELPERSONALIZADO.MULTIPLAESCOLHA');
          sql.add('     , TABFILTRORELPERSONALIZADO.CAMPOMULTIPLAESCOLHA');
          sql.add('     , TABFILTRORELPERSONALIZADO.DATAC');
          sql.add('     , TABFILTRORELPERSONALIZADO.USERC');
          sql.add('     , TABFILTRORELPERSONALIZADO.DATAM');
          sql.add('     , TABFILTRORELPERSONALIZADO.USERM from tabfiltrorelpersonalizado');
          sql.add('where tabfiltrorelpersonalizado.relatorio='+PRelatorio);
          open;
     End;
end;

end.



