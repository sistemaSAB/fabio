object FLancamentosFrentedeCaixa: TFLancamentosFrentedeCaixa
  Left = 426
  Top = 238
  Width = 800
  Height = 600
  Caption = 'Lancamentos Frente de Caixa'
  Color = clBtnFace
  Constraints.MinHeight = 600
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object PanelPrincipal: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 89
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 14
      Top = 13
      Width = 35
      Height = 15
      Caption = 'Venda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 123
      Top = 13
      Width = 62
      Height = 15
      Alignment = taCenter
      Caption = 'Data inicial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 212
      Top = 34
      Width = 7
      Height = 15
      Caption = 'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 227
      Top = 13
      Width = 53
      Height = 15
      Alignment = taCenter
      Caption = 'Data final'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 319
      Top = 13
      Width = 188
      Height = 15
      Caption = 'Datas referentes ao recebimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtvenda: TEdit
      Left = 14
      Top = 32
      Width = 87
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      OnKeyDown = edtvendaKeyDown
    end
    object edtvencimento1: TMaskEdit
      Left = 123
      Top = 32
      Width = 72
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 1
      Text = '  /  /    '
      OnKeyUp = edtvencimento1KeyUp
    end
    object edtvencimento2: TMaskEdit
      Left = 227
      Top = 32
      Width = 73
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 2
      Text = '  /  /    '
      OnKeyUp = edtvencimento2KeyUp
    end
    object btiniciar: TButton
      Left = 318
      Top = 29
      Width = 88
      Height = 25
      Caption = 'Iniciar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btiniciarClick
    end
    object ProgressBar1: TProgressBar
      Left = 1
      Top = 64
      Width = 782
      Height = 24
      Align = alBottom
      TabOrder = 4
    end
  end
  object MemoLOG: TMemo
    Left = 0
    Top = 89
    Width = 784
    Height = 473
    Hint = 'Log de Erros'
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    Lines.Strings = (
      'LOG DE ERROS')
    ParentFont = False
    ScrollBars = ssBoth
    TabOrder = 1
  end
end
