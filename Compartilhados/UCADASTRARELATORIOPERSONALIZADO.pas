unit UCADASTRARELATORIOPERSONALIZADO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjRELATORIOPERSONALIZADOextendido,
  Grids, DBGrids,uobjfiltrorelpersonalizadoextendido;

type
  TFCADASTRARELATORIOPERSONALIZADO = class(TForm)
    Guia: TTabbedNotebook;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbSubMenu: TLabel;
    EdtSubMenu: TEdit;
    LbNomeSubMenu: TLabel;
    memoSqlPrincipal: TMemo;
    memoSqlRepeticao: TMemo;

    LbNome: TLabel;
    EdtNome: TEdit;
    LbSqlPrincipal: TLabel;
    LbSqlRepeticao: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtpermissao: TEdit;
    lbnomepermissao: TLabel;
    panelfiltro: TPanel;
    lbcodigofiltro: TLabel;
    Lbtipo: TLabel;
    Label5: TLabel;
    edtnomefiltro: TEdit;
    Lblabel: TLabel;
    Edtcaptionlabelfiltro: TEdit;
    Lbmascara: TLabel;
    Edtmascarafiltro: TEdit;
    Lbrequerido: TLabel;
    Lbtabelachaveestrangeira: TLabel;
    Lbcampochavestrangeira: TLabel;
    Edtcampochavestrangeirafiltro: TEdit;
    Lbcomandoandprincipal: TLabel;
    Lbcomandoandsecundario: TLabel;
    combotipofiltro: TComboBox;
    comborequeridofiltro: TComboBox;
    memocomandosqlchaveestrangeirafiltro: TMemo;
    memocomandoandprincipalfiltro: TMemo;
    memocomandoandrepeticaofiltro: TMemo;
    Dbgridfiltro: TDBGrid;
    combousamesmoembrancofiltro: TComboBox;
    Label4: TLabel;
    panelorderby: TPanel;
    Lbordemdefault: TLabel;
    Edtordemdefaultorderby: TEdit;
    Edtordemorderby: TEdit;
    Lbordem: TLabel;
    Lbcomando: TLabel;
    edtcomandoorderby: TEdit;
    edtnomeorderby: TEdit;
    Label8: TLabel;
    LbCodigoOrderBy: TLabel;
    Dbgridorderby: TDBGrid;
    comboorderbypersonalizado: TComboBox;
    Label6: TLabel;
    Panel: TPanel;
    ListBoxOrderBy: TListBox;
    BtGravarOrdem: TButton;
    botaosetacima: TSpeedButton;
    botaosetabaixo: TSpeedButton;
    SaveDialog1: TSaveDialog;
    Label7: TLabel;
    memosqlgroupby: TMemo;




    Label9: TLabel;
    combomultiplaescolhafiltro: TComboBox;
    edtcampomultiplaescolhafiltro: TEdit;
    Label10: TLabel;
    btextrai13memorepeticao: TButton;
    btextrai13memoprincipal: TButton;
    memotabelaprincipal: TMemo;
    memotabelarepeticao: TMemo;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigoPedido: TLabel;
    lb1: TLabel;
    btPesquisar: TBitBtn;
    btExcluir: TBitBtn;
    btCancelar: TBitBtn;
    btsalvar: TBitBtn;
    btAlterar: TBitBtn;
    btnovo: TBitBtn;
    btSair: TBitBtn;
    lbVisualizar: TLabel;
    lbImportarExportar: TLabel;
    lbSqlUltimaExecucao: TLabel;
    lbConfigura: TLabel;
    pnl1: TPanel;
    imgrodape: TImage;
    lb9: TLabel;
    pnl2: TPanel;
    btBtgravarOrderBy: TBitBtn;
    btcancelarnovoorderby: TBitBtn;
    btexcluirorderby: TBitBtn;
    lbGravar: TLabel;
    lbCancelarNovo: TLabel;
    lb4: TLabel;
    lb5: TLabel;
//DECLARA COMPONENTES

    procedure edtSubMenuKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtSubMenuExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btBtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btconfiguraClick(Sender: TObject);
    procedure edtpermissaoExit(Sender: TObject);
    procedure edtpermissaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btrelatoriosClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure btgravarfiltrosClick(Sender: TObject);
    procedure btcancelarfiltrosClick(Sender: TObject);
    procedure btexcluirfiltrosClick(Sender: TObject);
    procedure DbgridfiltroDblClick(Sender: TObject);
    procedure DbgridfiltroKeyPress(Sender: TObject; var Key: Char);
    procedure bt1Click(Sender: TObject);
    procedure btBtgravarOrderByClick(Sender: TObject);
    procedure btcancelarnovoorderbyClick(Sender: TObject);
    procedure btexcluirorderbyClick(Sender: TObject);
    procedure DbgridorderbyDblClick(Sender: TObject);
    procedure DbgridorderbyKeyPress(Sender: TObject; var Key: Char);
    procedure BtGravarOrdemClick(Sender: TObject);
    procedure ListBoxOrderByDragDrop(Sender, Source: TObject; X,
      Y: Integer);
    procedure ListBoxOrderByDragOver(Sender, Source: TObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure ListBoxOrderByMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure botaosetacimaClick(Sender: TObject);
    procedure botaosetabaixoClick(Sender: TObject);
    procedure btBTImportarExportarClick(Sender: TObject);
    procedure btextrai13memorepeticaoClick(Sender: TObject);
    procedure btextrai13memoprincipalClick(Sender: TObject);
    procedure btreplicarfiltroClick(Sender: TObject);
    procedure memoSqlPrincipalKeyPress(Sender: TObject; var Key: Char);
    procedure memoSqlRepeticaoKeyPress(Sender: TObject; var Key: Char);
    procedure memosqlgroupbyKeyPress(Sender: TObject; var Key: Char);
    procedure memotabelarepeticaoKeyPress(Sender: TObject; var Key: Char);
    procedure lbSqlUltimaExecucaoMouseLeave(Sender: TObject);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbSqlUltimaExecucaoMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure lbSqlUltimaExecucaoMouseUp(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure lbSqlUltimaExecucaoMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
         ObjRELATORIOPERSONALIZADOextendido:TObjRELATORIOPERSONALIZADOextendido;

         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         procedure ControlesParaObjetoFiltro;
         procedure tabelaparaobjetos_filtro;
         Procedure PesquisaFiltros;
         Procedure PesquisaOrderby;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCADASTRARELATORIOPERSONALIZADO: TFCADASTRARELATORIOPERSONALIZADO;
  StartingPoint : TPoint;

implementation

uses UessencialGlobal, Upesquisa, UrelatorioPersonalizado,
  UobjFILTRORELPERSONALIZADO, UmostraStringList,
  UobjRELATORIOPERSONALIZADO, UobjORDERBYRELPERSONALIZADOextendido,
  UobjORDERBYRELPERSONALIZADO, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCADASTRARELATORIOPERSONALIZADO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjRELATORIOPERSONALIZADOextendido do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        SubMenu.Submit_codigo(edtSubMenu.text);
        Submit_Nome(edtNome.text);
        Permissao.Submit_codigo(edtPermissao.text);
        Submit_SqlPrincipal(memoSqlPrincipal.text);
        Submit_SqlRepeticao(memoSqlRepeticao.text);
        Submit_TabelaPrincipal(memotabelaprincipal.text);
        Submit_orderbypersonalizado(submit_combobox(comboorderbypersonalizado));
        Submit_TabelaRepeticao(memotabelarepeticao.text);
        Submit_SqlGroupBy(memosqlgroupby.text);


        Submit_UltimoSqlPrincipal('');
        Submit_UltimoSqlrepeticao('');


        //CODIFICA SUBMITS
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCADASTRARELATORIOPERSONALIZADO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjRELATORIOPERSONALIZADOextendido do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtSubMenu.text:=SubMenu.Get_codigo;
        EdtNome.text:=Get_Nome;
        EdtPermissao.text:=Permissao.Get_codigo;
        lbnomepermissao.Caption:=Permissao.Get_nome;
        memoSqlPrincipal.text:=Get_SqlPrincipal;
        memoSqlRepeticao.text:=Get_SqlRepeticao;
        memotabelaprincipal.Text:=Get_TabelaPrincipal;
        memotabelarepeticao.Text:=Get_TabelaRepeticao;

        if (Get_orderbypersonalizado='S')
        Then comboorderbypersonalizado.ItemIndex:=1
        Else comboorderbypersonalizado.ItemIndex:=0;

        memosqlgroupby.text:=Get_SqlGroupBy;
        
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCADASTRARELATORIOPERSONALIZADO.TabelaParaControles: Boolean;
begin
     If (Self.ObjRELATORIOPERSONALIZADOextendido.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCADASTRARELATORIOPERSONALIZADO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjRELATORIOPERSONALIZADOextendido=Nil)
     Then exit;

      If (Self.ObjRELATORIOPERSONALIZADOextendido.status<>dsinactive)
      Then Begin
                Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
                abort;
                exit;
     End;

      Self.ObjRELATORIOPERSONALIZADOextendido.free;

      //if (ObjFiltro<>nil)
      //Then Self.objfiltro.Free;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
           Perform(Wm_NextDlgCtl,0,0);
      end;
end;



procedure TFCADASTRARELATORIOPERSONALIZADO.btBtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     
     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjRELATORIOPERSONALIZADOextendido.Get_novocodigo;
     edtcodigo.enabled:=False;


     {Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;
                                  }
     Self.ObjRELATORIOPERSONALIZADOextendido.status:=dsInsert;
     Guia.pageindex:=0;
     edtSubMenu.setfocus;
     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;

end;


procedure TFCADASTRARELATORIOPERSONALIZADO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjRELATORIOPERSONALIZADOextendido.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjRELATORIOPERSONALIZADOextendido.Status:=dsEdit;
                guia.pageindex:=0;
               { Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;    }
                edtSubMenu.setfocus;
                 btnovo.Visible :=false;
                 btalterar.Visible:=false;
                 btpesquisar.Visible:=false;
                 btexcluir.Visible:=false;
                 btsair.Visible:=false;
                
          End;


end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btgravarClick(Sender: TObject);
begin

     If Self.ObjRELATORIOPERSONALIZADOextendido.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjRELATORIOPERSONALIZADOextendido.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjRELATORIOPERSONALIZADOextendido.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
      btnovo.Visible :=true;
      btalterar.Visible:=true;
      btpesquisar.Visible:=true;
      btexcluir.Visible:=true;
      btsair.Visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjRELATORIOPERSONALIZADOextendido.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjRELATORIOPERSONALIZADOextendido.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjRELATORIOPERSONALIZADOextendido.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btcancelarClick(Sender: TObject);
begin
     Self.ObjRELATORIOPERSONALIZADOextendido.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;

end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjRELATORIOPERSONALIZADOextendido.Get_pesquisa,Self.ObjRELATORIOPERSONALIZADOextendido.Get_TituloPesquisa,Nil)
=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjRELATORIOPERSONALIZADOextendido.status<>dsinactive
                                  then exit;

                                  If (Self.ObjRELATORIOPERSONALIZADOextendido.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjRELATORIOPERSONALIZADOextendido.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCADASTRARELATORIOPERSONALIZADO.LimpaLabels;
begin
//LIMPA LABELS
     Self.LbNomeSubMenu.caption:='';
     Self.lbnomepermissao.caption:='';
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     Guia.PageIndex:=0;

     Try
        Self.ObjRELATORIOPERSONALIZADOextendido:=TObjRELATORIOPERSONALIZADOextendido.create;
        Self.Dbgridfiltro.DataSource:=Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.objdatasource;
        Self.Dbgridorderby.DataSource:=Self.ObjRELATORIOPERSONALIZADOextendido.objorderby.ObjDataSource;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
      FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,nil,btexcluir,btsair,nil);
      FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btBtgravarOrderBy,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btcancelarnovoorderby,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btexcluirorderby,'BOTAORETIRAR.BMP');
     //FescolheImagemBotao.PegaFiguraBotaopequeno(btAlterarPedidoProjeto,'BOTAOALTERARPRODUTO.BMP');

end;
procedure TFCADASTRARELATORIOPERSONALIZADO.edtSubMenuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjRELATORIOPERSONALIZADOextendido.edtSubMenukeydown(sender,key,shift,lbnomeSubMenu);
end;
 
procedure TFCADASTRARELATORIOPERSONALIZADO.edtSubMenuExit(Sender: TObject);
begin
    ObjRELATORIOPERSONALIZADOextendido.edtSubMenuExit(sender,lbnomeSubMenu);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCADASTRARELATORIOPERSONALIZADO.btconfiguraClick(
  Sender: TObject);
begin
     if (edtcodigo.text='')
     Then exit;

     if (Self.ObjRELATORIOPERSONALIZADOextendido.LocalizaCodigo(EdtCODIGO.Text)=false)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.TabelaparaObjeto;

     FrelatorioPersonalizado.caption:=Self.ObjRELATORIOPERSONALIZADOextendido.get_nome;
     FrelatorioPersonalizado.NomeRelatorio:='REL_PERSONALIZADO_'+Self.ObjRELATORIOPERSONALIZADOextendido.Get_codigo;
     FrelatorioPersonalizado.TabelaCampos:=Self.ObjRELATORIOPERSONALIZADOextendido.Get_TabelaPrincipal;
     FrelatorioPersonalizado.TabelaCamposRepeticao:=Self.ObjRELATORIOPERSONALIZADOextendido.Get_TabelaRepeticao;
     FrelatorioPersonalizado.showmodal;
     
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.edtpermissaoExit(
  Sender: TObject);
begin
    ObjRELATORIOPERSONALIZADOextendido.edtpermissaoExit(sender,lbnomepermissao);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.edtpermissaoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjRELATORIOPERSONALIZADOextendido.edtpermissaokeydown(sender,key,shift,lbnomepermissao);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btrelatoriosClick(
  Sender: TObject);
begin
     if (EdtCODIGO.Text='') or (Self.ObjRELATORIOPERSONALIZADOextendido.Status=dsinsert)
     then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.ImprimeRelatorio(strtoint(EdtCODIGO.Text));

end;

procedure TFCADASTRARELATORIOPERSONALIZADO.GuiaChange(Sender: TObject;
  NewTab: Integer; var AllowChange: Boolean);
begin
     if (newtab=1)
     Then Begin
               if (Self.ObjRELATORIOPERSONALIZADOextendido.Status=dsinsert) or (EdtCODIGO.Text='')
               Then Begin
                        AllowChange:=False;
                        exit;
               End;

               habilita_campos(panelorderby);
               habilita_botoes(panelorderby);

               limpaedit(panelorderby);
               lbcodigoorderby.caption:='';
               Self.PesquisaOrderby;
     End;

     if (newtab=2)
     Then Begin
               if (Self.ObjRELATORIOPERSONALIZADOextendido.Status=dsinsert) or (EdtCODIGO.Text='')
               Then Begin
                        AllowChange:=False;
                        exit;
               End;

               habilita_campos(panelfiltro);
               habilita_botoes(panelfiltro);

               limpaedit(panelfiltro);
               lbcodigofiltro.caption:='';
               Self.PesquisaFiltros;
     End;
end;


procedure TFCADASTRARELATORIOPERSONALIZADO.tabelaparaobjetos_filtro;
Begin
     With Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro do
     Begin

        lbcodigofiltro.caption:=Get_CODIGO;

        if (get_tipo='INTEIRO')
        Then combotipofiltro.itemindex:=0;

        if (get_tipo='DECIMAL')
        Then combotipofiltro.itemindex:=1;

        if (get_tipo='DATA')
        Then combotipofiltro.itemindex:=2;

        if (get_tipo='HORA')
        Then combotipofiltro.itemindex:=3;

        if (get_tipo='DATA E HORA')
        Then combotipofiltro.itemindex:=4;

        if (get_tipo='STRING')
        Then combotipofiltro.itemindex:=5;


        Edtnomefiltro.text:=Get_nome;
        Edtcaptionlabelfiltro.text:=Get_captionlabel;
        Edtmascarafiltro.text:=Get_mascara;

        if (Get_requerido='S')
        Then comborequeridofiltro.itemindex:=1
        Else comborequeridofiltro.itemindex:=0;

        if (Get_multiplaescolha='S')
        Then combomultiplaescolhafiltro.itemindex:=1
        Else combomultiplaescolhafiltro.itemindex:=0;

        edtcampomultiplaescolhafiltro.text:=Get_CampoMultiplaEscolha;









        if (Get_usamesmoembranco='S')
        Then combousamesmoembrancofiltro.itemindex:=1
        Else combousamesmoembrancofiltro.itemindex:=0;

        memocomandosqlchaveestrangeirafiltro.text:=Get_comandosqlchaveestrangeira;
        Edtcampochavestrangeirafiltro.text:=Get_campochavestrangeira;
        memocomandoandprincipalfiltro.text:=Get_comandoandprincipal;
        memocomandoandrepeticaofiltro.text:=Get_comandoandrepeticao;
        
     End;
End;



procedure TFCADASTRARELATORIOPERSONALIZADO.ControlesParaObjetoFiltro;
Begin
     With Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro do
     Begin
        if (lbcodigofiltro.caption='')
        Then Submit_CODIGO('0')
        Else Submit_CODIGO(lbcodigofiltro.caption);
        relatorio.Submit_codigo(EdtCODIGO.text);
        Submit_tipo(combotipofiltro.text);
        Submit_nome(edtnomefiltro.text);
        Submit_captionlabel(edtcaptionlabelfiltro.text);
        Submit_mascara(edtmascarafiltro.text);


        Submit_multiplaescolha(Submit_ComboBox(combomultiplaescolhafiltro));
        Submit_campomultiplaescolha(edtcampomultiplaescolhafiltro.Text);
        Submit_requerido(Submit_ComboBox(comborequeridofiltro));
        Submit_usamesmoembranco(Submit_ComboBox(combousamesmoembrancofiltro));
        Submit_comandosqlchaveestrangeira(memocomandosqlchaveestrangeirafiltro.text);
        Submit_campochavestrangeira(edtcampochavestrangeirafiltro.text);
        Submit_comandoandprincipal(memocomandoandprincipalfiltro.text);
        Submit_comandoandrepeticao(memocomandoandrepeticaofiltro.text);
     End;
End;


procedure TFCADASTRARELATORIOPERSONALIZADO.PesquisaFiltros;
begin
     Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.PesquisaFiltros(EdtCODIGO.Text);
     formatadbgrid(Self.Dbgridfiltro);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btgravarfiltrosClick(
  Sender: TObject);
begin
     With Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro do
     Begin
           Self.ControlesParaObjetoFiltro;
           if ((Self.lbcodigofiltro.caption='') or (Self.lbcodigofiltro.caption='0'))
           Then Status:=dsInsert
           Else Status:=dsEdit;

           if (Salvar(True)=False)
           Then Begin
                     edtnomefiltro.SetFocus;
                     exit;
           End;

           Self.PesquisaFiltros;
           btcancelarfiltrosClick(sender);
     End;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btcancelarfiltrosClick(
  Sender: TObject);
begin
    limpaedit(panelfiltro);
    lbcodigofiltro.caption:='';
    edtnomefiltro.SetFocus;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btexcluirfiltrosClick(
  Sender: TObject);
begin
     if (Dbgridfiltro.DataSource.DataSet.RecordCount=0)
     then exit;

     if (MensagemPergunta('Certeza que deseja Excluir o Filtro: '+Dbgridfiltro.DataSource.DataSet.fieldbyname('nome').asstring+' ?')=mrno)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.Exclui(Dbgridfiltro.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.PesquisaFiltros;
     btcancelarfiltrosClick(sender);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.DbgridfiltroDblClick(
  Sender: TObject);
begin
     if (Dbgridfiltro.DataSource.DataSet.RecordCount=0)
     then exit;

     if (Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.LocalizaCodigo(Dbgridfiltro.DataSource.dataset.fieldbyname('codigo').asstring)=False)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.TabelaparaObjeto;
     Self.tabelaparaobjetos_filtro;
     edtnomefiltro.SetFocus;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.DbgridfiltroKeyPress(
  Sender: TObject; var Key: Char);
begin
     if (key=#13)
     Then Self.DbgridfiltroDblClick(sender);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.bt1Click(Sender: TObject);
begin
     if (Self.ObjRELATORIOPERSONALIZADOextendido.LocalizaCodigo(EdtCODIGO.text)=False)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.TabelaparaObjeto;
     
     FmostraStringList.Memo.Lines.Clear;
     FmostraStringList.Memo.Lines.add('Principal: '+Self.ObjRELATORIOPERSONALIZADOextendido.Get_UltimoSqlPrincipal);
     FmostraStringList.Memo.Lines.add('');
     FmostraStringList.Memo.Lines.add('Repeti��o: '+Self.ObjRELATORIOPERSONALIZADOextendido.Get_UltimoSqlrepeticao);
     FmostraStringList.ShowModal;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btBtgravarOrderByClick(
  Sender: TObject);
begin
     With Self.ObjRELATORIOPERSONALIZADOextendido.ObjOrderBy do
     Begin
          if (LbCodigoOrderBy.Caption='')
          Then Begin
                    Submit_CODIGO('0');
                    Status:=dsInsert;
          End
          Else BEgin
                    Submit_CODIGO(LbCodigoOrderBy.Caption);
                    Status:=dsEdit;
          End;
          relatorio.Submit_codigo(EdtCODIGO.Text);
          Submit_Nome(edtnomeorderby.text);
          Submit_comando(edtcomandoorderby.text);
          Submit_ordem(edtordemorderby.text);
          Submit_ordemdefault(edtordemdefaultorderby.text);
          if (Salvar(True)=False)
          Then exit;

          Self.PesquisaOrderby;
          btcancelarnovoorderbyClick(sender);
          
     End;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btcancelarnovoorderbyClick(
  Sender: TObject);
begin
     limpaedit(panelorderby);
     LbCodigoOrderBy.caption:='';
     edtnomeorderby.SetFocus;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btexcluirorderbyClick(
  Sender: TObject);
begin
     if (Dbgridorderby.DataSource.DataSet.RecordCount=0)
     then exit;

     if (MensagemPergunta('Certeza que deseja Excluir o comando order by: '+Dbgridorderby.DataSource.DataSet.fieldbyname('nome').asstring+' ?')=mrno)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.ObjOrderBy.Exclui(Dbgridorderby.DataSource.DataSet.fieldbyname('codigo').asstring,true);
     Self.PesquisaOrderBy;
     btcancelarnovoorderbyClick(sender);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.PesquisaOrderby;
begin
     ListBoxOrderBy.Items.clear;
     Self.ObjRELATORIOPERSONALIZADOextendido.ObjOrderBy.PesquisaComandos(EdtCODIGO.Text);

     Self.Dbgridorderby.DataSource.DataSet.First;
     while not(Self.Dbgridorderby.DataSource.DataSet.eof) do
     Begin
          ListBoxOrderBy.Items.add(Self.Dbgridorderby.DataSource.DataSet.fieldbyname('nome').asstring);
          Self.Dbgridorderby.DataSource.DataSet.Next;
     End;
     formatadbgrid(Self.Dbgridorderby);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.DbgridorderbyDblClick(
  Sender: TObject);
begin
    if (Dbgridorderby.DataSource.DataSet.RecordCount=0)
     then exit;

     if (Self.ObjRELATORIOPERSONALIZADOextendido.Objorderby.LocalizaCodigo(Dbgridorderby.DataSource.dataset.fieldbyname('codigo').asstring)=False)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.Objorderby.TabelaparaObjeto;

     With Self.ObjRELATORIOPERSONALIZADOextendido.ObjOrderBy do
     Begin
        LbCodigoOrderBy.caption:=Get_CODIGO;
        edtnomeorderby.text:=Get_Nome;
        edtcomandoorderby.text:=Get_comando;
        Edtordemorderby.text:=Get_ordem;
        Edtordemdefaultorderby.text:=Get_ordemdefault;
     End;
     edtnomeorderby.SetFocus;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.DbgridorderbyKeyPress(
  Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.DbgridorderbyDblClick(sender);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.BtGravarOrdemClick(
  Sender: TObject);
var
temp:string;
begin
     Self.ObjRELATORIOPERSONALIZADOextendido.RetornaOrderByegravaordem(edtcodigo.text,ListBoxOrderBy,temp,true);
     Self.PesquisaOrderby;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.ListBoxOrderByDragDrop(Sender,
  Source: TObject; X, Y: Integer);
var
  DropPosition, StartPosition: Integer;
  DropPoint: TPoint;
begin
    DropPoint.X := X;
    DropPoint.Y := Y;
    with Source as TListBox do
    begin
      StartPosition := ItemAtPos(StartingPoint,True);
      DropPosition := ItemAtPos(DropPoint,True);
      Items.Move(StartPosition, DropPosition);
    end;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.ListBoxOrderByDragOver(Sender,
  Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
       Accept := Source = TListbox(Sender);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.ListBoxOrderByMouseDown(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
    StartingPoint.X := X;
    StartingPoint.Y := Y;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.botaosetacimaClick(
  Sender: TObject);
var
temp:string;
begin
     if (Self.ListBoxOrderBy.ItemIndex>0)//primeiro nao
     and (Self.ListBoxOrderBy.ItemIndex<=(Self.ListBoxOrderBy.Items.count-1))//ultimo pode
     Then Begin
               temp:=Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex];
               Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex]:=Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex-1];
               Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex-1]:=temp;
               Self.ListBoxOrderBy.ItemIndex:=Self.ListBoxOrderBy.itemindex-1;
               Self.ListBoxOrderBy.SetFocus;

     End;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.botaosetabaixoClick(
  Sender: TObject);
var
temp:string;
begin
     if (Self.ListBoxOrderBy.ItemIndex>=0)//do primeiro pra cima
     and (Self.ListBoxOrderBy.ItemIndex<(Self.ListBoxOrderBy.Items.count-1))//ultimo nao da
     Then Begin
               temp:=Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex];
               Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex]:=Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex+1];
               Self.ListBoxOrderBy.items[Self.ListBoxOrderBy.itemindex+1]:=temp;
               Self.ListBoxOrderBy.ItemIndex:=Self.ListBoxOrderBy.itemindex+1;
               Self.ListBoxOrderBy.SetFocus;
     End;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btBTImportarExportarClick(
  Sender: TObject);
begin
     Self.ObjRELATORIOPERSONALIZADOextendido.ImportaExporta(edtcodigo.text);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btextrai13memorepeticaoClick(Sender: TObject);
var
Pstr:String;
begin
     pstr:=memoSqlRepeticao.text;

     Pstr:=StringReplace(pstr,#13,' ',[rfReplaceAll]);
     Pstr:=StringReplace(pstr,#10,' ',[rfReplaceAll]);

      InputQuery('','',pstr);
     
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btextrai13memoprincipalClick(Sender: TObject);
var
Pstr:String;
begin
     pstr:=memoSqlPrincipal.text;

     Pstr:=StringReplace(pstr,#13,' ',[rfReplaceAll]);
     Pstr:=StringReplace(pstr,#10,' ',[rfReplaceAll]);

      InputQuery('','',pstr);
     
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.btreplicarfiltroClick(
  Sender: TObject);
begin
     if (Dbgridfiltro.DataSource.DataSet.RecordCount=0)
     then exit;

     if (Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.LocalizaCodigo(Dbgridfiltro.DataSource.dataset.fieldbyname('codigo').asstring)=False)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.TabelaparaObjeto;

     if (MensagemPergunta('Certeza que deseja replicar o filtro: '+Self.ObjRELATORIOPERSONALIZADOextendido.Objfiltro.Get_nome)=mrno)
     Then exit;

     Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.Status:=dsInsert;
     Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.Submit_CODIGO('0');
     Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.Submit_nome(Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.get_nome+'replicado');

     if (Self.ObjRELATORIOPERSONALIZADOextendido.ObjFiltro.Salvar(True)=False)
     Then Begin
               Dbgridfiltro.SetFocus;
               exit;
      End;

      Self.PesquisaFiltros;
      btcancelarfiltrosClick(sender);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.memoSqlPrincipalKeyPress(
  Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.memotabelaprincipal.SetFocus;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.memoSqlRepeticaoKeyPress(
  Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.memoSqlRepeticao.SetFocus;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.memosqlgroupbyKeyPress(
  Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.memosqlgroupby.SetFocus;

end;

procedure TFCADASTRARELATORIOPERSONALIZADO.memotabelarepeticaoKeyPress(
  Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Self.memotabelarepeticao.SetFocus;
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.lbSqlUltimaExecucaoMouseLeave(
  Sender: TObject);
begin
       TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.FormMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     ScreenCursorProc(crDefault);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.lbSqlUltimaExecucaoMouseMove(
  Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
        TEdit(sender).Font.Style:=[fsBold,fsUnderline];
      // ScreenCursorProc(crHandPoint);

end;

procedure TFCADASTRARELATORIOPERSONALIZADO.lbSqlUltimaExecucaoMouseUp(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
      ScreenCursorProc(crDefault);
end;

procedure TFCADASTRARELATORIOPERSONALIZADO.lbSqlUltimaExecucaoMouseDown(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
     ScreenCursorProc(crDefault);
end;

end.

