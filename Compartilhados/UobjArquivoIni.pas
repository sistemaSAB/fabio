unit UobjArquivoIni;
Interface
Uses inifiles,forms,Ibquery,windows,Classes,Db,UessencialGlobal,extctrls;
//USES_INTERFACE


Type
   TObjarquivoini=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Arquivo:TStringList;
                Arquivo_Ini:Tinifile;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Localizanome(Parametro:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_nome(parametro: string);
                Function Get_nome: string;
                Procedure Submit_arquivo(parametro: String);
                Function Get_arquivo: String;

                function Criar_ini: boolean;
                function fecha_ini: boolean;
                Function PegaCampo(Psecao,Pchave:String;var Pvalor:string):boolean;
                Function GravaCampo(Psecao,Pchave,Pvalor:string):boolean;
                function transfere_ini_para_bd: boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               nome:string;


               arquivo_Stream:TmemoryStream;
//CODIFICA VARIAVEIS PRIVADAS
               //**************************************************************
               Left:integer;
               Top:integer;
               visible:boolean;
               //**************************************************************
               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjarquivoini.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
var
  Temp:TStream;
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.nome:=fieldbyname('nome').asstring;

        try
           try
             Temp:=CreateBlobStream(FieldByName('arquivo'),bmRead);
             Self.arquivo.LoadFromStream(temp);
           Except
                 Messagedlg('Erro na Tentativa de Recuperar o valor do Campo Configura��es!',mterror,[mbok],0);
                 exit;
           End;
        Finally
               freeandnil(temp);
        End;

//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjarquivoini.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('nome').asstring:=Self.nome;
        Self.arquivo_Stream.Clear;
        Self.arquivo.SaveToStream(Self.arquivo_Stream);
        ParamByName('arquivo').LoadFromStream(Self.arquivo_Stream,ftBlob);
  End;
End;

//***********************************************************************

function TObjarquivoini.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjarquivoini.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        nome:='';
        arquivo.Text:='';
//CODIFICA ZERARTABELA



     End;
end;

Function TObjarquivoini.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (nome='')
      Then Mensagem:=mensagem+'/Nome do Formul�rio';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjarquivoini.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjarquivoini.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjarquivoini.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjarquivoini.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjarquivoini.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro arquivoini vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,nome,arquivo');
           SQL.ADD(' from  Tabarquivoini');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjarquivoini.Localizanome(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro Nome do Formul�rio vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,nome,arquivo');
           SQL.ADD(' from  Tabarquivoini');
           SQL.ADD(' WHERE nome='+#39+uppercase(parametro)+#39);
//CODIFICA LOCALIZACODIGO
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;
procedure TObjarquivoini.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjarquivoini.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjarquivoini.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.InsertSql:=TStringList.create;
        Self.DeleteSql:=TStringList.create;
        Self.ModifySQl:=TStringList.create;

        Self.arquivo       :=TStringList.Create;
        Self.arquivo_Stream:=TmemoryStream.Create;
        Self.Arquivo_Ini:=nil;
        
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into Tabarquivoini(CODIGO,nome');
                InsertSQL.add(' ,arquivo)');
                InsertSQL.add('values (:CODIGO,:nome,:arquivo)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update Tabarquivoini set CODIGO=:CODIGO,nome=:nome');
                ModifySQL.add(',arquivo=:arquivo');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from Tabarquivoini where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjarquivoini.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjarquivoini.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from Tabarquivoini');
     Result:=Self.ParametroPesquisa;
end;

function TObjarquivoini.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de arquivoini ';
end;


function TObjarquivoini.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENarquivoini,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENarquivoini,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjarquivoini.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.InsertSql);
    Freeandnil(Self.DeleteSql);
    Freeandnil(Self.ModifySQl);

    Self.arquivo.Free;
    Self.arquivo_Stream.Free;

    if (self.Arquivo_Ini<>nil)
    Then self.arquivo_ini.Free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjarquivoini.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjarquivoini.RetornaCampoNome: string;
begin
      result:='nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjarquivoini.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjarquivoini.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjarquivoini.Submit_nome(parametro: string);
begin
        Self.nome:=Parametro;
end;
function TObjarquivoini.Get_nome: string;
begin
        Result:=Self.nome;
end;
procedure TObjarquivoini.Submit_arquivo(parametro: String);
begin
        Self.arquivo.text:=Parametro;
end;
function TObjarquivoini.Get_arquivo: String;
begin
        Result:=Self.arquivo.text;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjarquivoini.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJarquivoini';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;


end;


Function TObjarquivoini.transfere_ini_para_bd:boolean;
var
temp:string;
begin
    //dou um freeandnil no ini para garantir que os dados foram descarregados no arquivo fisico
    Self.fecha_ini;
    
    result:=False;
    temp:=ExtractFilePath(Application.ExeName);
    if (temp[length(temp)]<>'\')
    Then temp:=temp+'\';

    temp:=Temp+'CONFRELS\Temp.ini';
    Self.arquivo.clear;
    try
        Self.arquivo.LoadFromFile(temp);
        result:=true;
    Except
    End;
End;


function TObjarquivoini.Criar_ini: boolean;
var
temp:string;
begin
{Cria um Ini de acordo com os dados do registro atual da tabarquivoini}

    result:=False;
    Try
        temp:=ExtractFilePath(Application.ExeName);
        if (temp[length(temp)]<>'\')
        Then temp:=temp+'\';

        temp:=Temp+'CONFRELS\Temp.ini';

        Self.Arquivo.SaveToFile(temp);

        if (FileExists(temp)=False)
        then Begin
                  MensagemErro('N�o foi poss�vel criar o arquivo '+temp);
                  exit;
        End;

        if (self.Arquivo_Ini<>nil)
        then freeandnil(self.arquivo_ini);
        
        Self.Arquivo_ini:=Tinifile.Create(temp);

        result:=true;

     Except
           Messagedlg('Erro na Abertura do Arquivo '+temp,mterror,[mbok],0);
           exit;
     End;
end;

function TObjarquivoini.PegaCampo(Psecao, Pchave: String;
  var Pvalor: string): boolean;
var
temp:string;
begin
        result:=False;
        Temp:='';
        Try
            if (self.Arquivo_Ini=nil)
            Then Begin
                      mensagemerro('O arquivo ini est� nil');
                      exit;
            End;

            Temp:=arquivo_ini.ReadString(uppercase(Psecao),pchave,'');
            Pvalor:=temp;
            result:=true;

        Except
              mensagemerro('Erro na tentativa de ler a chave '+pchave+' na se��o '+psecao);
              exit;
        End;
end;

function TObjarquivoini.GravaCampo(Psecao, Pchave,
  Pvalor: string): boolean;
begin
     result:=False;
     try
        if (self.Arquivo_Ini=nil)
        Then Begin
                  mensagemerro('O arquivo ini est� nil');
                  exit;
        End;

        Self.arquivo_ini.WriteString(uppercase(psecao),pchave,pvalor);
        result:=true;
     Except
           mensagemerro('Erro na tentativa de gravar a chave '+pchave+' na se��o '+psecao);
           exit;
     End;
end;

function TObjarquivoini.fecha_ini: boolean;
begin
     freeandnil(Self.Arquivo_Ini);
end;

end.
