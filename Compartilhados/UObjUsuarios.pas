unit UObjUsuarios;
Interface
Uses windows,forms,Classes,Db,Ibcustomdataset,UObjNiveis,Uobjusuarioib,UessencialGlobal,Useg,IBDatabase,stdctrls,shellapi;

Type


   TObjUsuarios=class

          Public
                //ObjDatasource                             :TDataSource;
                ObjDataset:Tibdataset;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

               pDATABASELOCAL:TIBDatabase;
               pTRANSACTIONLOCAL:TIBTransaction;

                ObjUsuariosIb:TobjUsuarioIb;

                Constructor Create(Owner:TComponent);overload;
                Constructor Create(PLogaSysDba: Boolean;Owner:TComponent);overload;
                Constructor create(PLogaSysDba:Boolean;pDatabase:TIBDatabase;pTransaction:TIBTransaction;Owner:TComponent);overload;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;overload;
                Function    Salvar(ComCommit:Boolean;PsomenteSistema:boolean):Boolean;overload;

                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaNome(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;somentenosistema:boolean)            :Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;overload;

                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                Function    Get_NIvelPesquisa                    :TStringList;
                Function    Get_NivelTituloPesquisa              :string;
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :string;

                Function    TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Procedure Submit_CODIGO           (parametro:string);
                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                function Get_nome: string;
                function Get_senha: string;
                procedure Submit_nome(parametro: string);
                procedure Submit_senha(parametro: string);
                function VerificaPermissao: boolean;
                Function Get_Nivel:string;
                PRocedure Submit_Nivel(parametro:string);
                Procedure Opcoes(pcodigo:string);
                function PedePermissao(TNivelPermissao: Integer): boolean;overload;
                function PedePermissao(TNivelPermissao: Integer;Pfrase:String):boolean;overload;
                function PedePermissao(TNivelPermissao: Integer;out PUsuarioAutorizou: String;Pfrase:String): boolean;overload;
                Procedure RecriaUsuarios;

                procedure trocasenhausuarioatual;
                procedure edtportadorKeyDown_PV(Sender: TObject; var Key: Word;  Shift: TShiftState);
                procedure EdtUsuarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
                procedure EdtUsuarioExit(Sender: TObject; LABELNOME:Tlabel);

         Private

                //por causa de erros de excecao joguei paraprivate
                Nivel	          :TObjNiveis;
                CODIGO           :string;
                nome             :string;
                senha            :string;
                DatabaseLocal:Tibdatabase;
                TransactionLocal:Tibtransaction;
                Owner:TComponent;
                
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ConcedePermissaoTodos;
                Procedure UsuarioGsec(pacao:string;pcodigo:string);



   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Ibquery,Controls,uopcaorel,
  UFiltraImp, UMostraBarraProgresso, UmostraStringList, Upesquisa,
  UUsuarios;


{ TTabTitulo }


Function  TObjUsuarios.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;

        If(FieldByName('nivel').asstring<>'')
        Then Begin
                If (Self.nivel.LocalizaCodigo(FieldByName('nivel').asstring)=False)
                Then Begin
                          Messagedlg('nivel N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                     End
                Else Self.nivel.TabelaparaObjeto;
        End;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.nome:=fieldbyname('nome').asstring;
        //showmessage(fieldbyname('senha').asstring);
        if (uppercase(Self.nome)<>'SYSDBA')
        Then Self.senha:=Useg.DesincriptaSenha(fieldbyname('senha').asstring)
        Else Self.senha:=fieldbyname('senha').asstring;
        result:=True;

     End;
end;


Procedure TObjUsuarios.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin

        fieldbyname('nivel').asstring:=Self.nivel.Get_codigo;
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('nome').asstring:=Self.nome;

        if (uppercase(Self.nome)<>'SYSDBA')
        then fieldbyname('senha').asstring:=Useg.encriptasenha(Self.Senha)
        Else fieldbyname('senha').asstring:=Self.Senha;

  End;
End;

//***********************************************************************

function TObjUsuarios.Salvar(ComCommit:Boolean): Boolean;//Ok
begin
     Self.Salvar(ComCommit,False);
End;



function TObjUsuarios.Salvar(ComCommit:Boolean;PsomenteSistema:boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;


if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If (self.Status=dsinsert)
 Then Begin
        if (PsomenteSistema=False)
        Then Begin
                  If (Self.ObjUsuariosIb.InsereUsuario(Self.nome,Self.senha,Self.Senha,Self.ObjDataset.Database)=False)
                  Then Begin
                            Messagedlg('Erro na tentativa de cria��o de usu�rio no Interbase!',mterror,[mbok],0);
                            result:=False;
                            If (ComCommit=True)
                            Then Self.ObjDataset.Transaction.RollbackRetaining;
                            exit;
                  End;
     End
     Else Begin
              If (Self.ObjUsuariosIb.ConcedePermissoes(nome,Self.ObjDataset.Database)=False)
              Then begin
                        MensagemErro('Erro na tentativa de conceder permiss�es');
                        result:=False;
                        If (ComCommit=True)
                        Then Self.ObjDataset.Transaction.RollbackRetaining;
                        exit;
              End;
     End;
 End
 Else Begin
           if (PsomenteSistema=False)
           Then Begin
                     If (Self.ObjUsuariosIb.alteraUsuario(Self.nome,Self.senha,Self.Senha,Self.ObjDataset.Database)=False)
                     Then Begin
                               Messagedlg('Erro na tentativa de Altera��o de usu�rio no Interbase!',mterror,[mbok],0);
                               result:=False;

                               If (ComCommit=True)
                               Then Self.ObjDataset.Transaction.RollbackRetaining;
                               exit;
                     End;
           End;
 End;




 If ComCommit=True
 Then Self.ObjDataset.Transaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjUsuarios.ZerarTabela;//Ok
Begin
     With Self do
     Begin

        Self.nivel.ZerarTabela;
        Self.CODIGO:='';
        Self.nome:='';
        Self.senha:='';

     End;
end;

Function TObjUsuarios.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjUsuarios.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.nivel.LocalizaCodigo(Self.nivel.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ N�vel n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjUsuarios.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjUsuarios.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjUsuarios.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';


     if (pos(#32,Self.nome)>0)
     Then mensagem:='Retire os espa�os do nome';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;
function TObjUsuarios.LocalizaNome(Parametro: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.clear;
           SelectSQL.ADD('Select nivel,CODIGO,nome,senha');
           SelectSQL.ADD(' from  Tabusuarios');
           SelectSQL.ADD(' WHERE nome='+#39+parametro+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjUsuarios.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
                SelectSql.clear;
                SelectSQL.ADD('Select nivel,CODIGO,nome,senha');
                SelectSQL.ADD(' from  Tabusuarios');
                SelectSQL.ADD(' WHERE codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjUsuarios.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjUsuarios.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     result:=Self.Exclui(pcodigo,comcommit,false);//envia somente no sistema=false
End;

function TObjUsuarios.Exclui(Pcodigo: string;ComCommit:Boolean;somentenosistema:boolean): Boolean;
begin

Try
     Try
        result:=False;

        If (Self.LocalizaCodigo(Pcodigo)=False)
        Then exit;


        Self.TabelaparaObjeto;
        Self.ObjDataset.delete;

        if (somentenosistema=false)
        Then Begin
                  If (self.ObjUsuariosIb.ApagaUsuario(self.nome,Self.ObjDataset.Database)=False)
                  Then  exit;

        End
        Else BEgin
                  if (self.ObjUsuariosIb.RetiraPermissoes(self.nome,Self.ObjDataset.Database)=false)
                  then exit;
        End;
        result:=true;
        
     Except
           on e:exception do
           Begin
                mensagemerro(e.message);
           End;
     End;

Finally
       if ((result=False) and (ComCommit=True))
       Then Self.ObjDataset.Transaction.RollbackRetaining;

       if ((result=true) and (ComCommit=True))
       Then Self.ObjDataset.Transaction.CommitRetaining;
End;

end;


constructor TObjUsuarios.create(Owner:TComponent);
Begin
     Self.create(false,fdatamodulo.ibdatabase,fdatamodulo.ibtransaction,Owner);
end;

constructor TObjUsuarios.create(PLogaSysDba:Boolean;Owner:TComponent);
Begin
     Self.create(PLogaSysDba,fdatamodulo.ibdatabase,fdatamodulo.ibtransaction,Owner);
end;

constructor TObjUsuarios.create(PLogaSysDba:Boolean;pDatabase:TIBDatabase;pTransaction:TIBTransaction;Owner:TComponent);
var
  PusuarioAtual:string;
begin
        self.owner := owner;
        self.pDATABASELOCAL := pDatabase;
        self.pTRANSACTIONLOCAL := pTransaction;

        Self.DatabaseLocal:=nil;
        Self.Transactionlocal:=nil;

        Self.ObjDataset:=Tibdataset.create(nil);

        //Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        self.objdataset.database:=self.pDATABASELOCAL;
        //Self.ObjDataset.Transaction:=FDataModulo.IBTransaction;
        Self.ObjDataset.Transaction:= self.pTRANSACTIONLOCAL;

        if (PLogaSysDba=True)
        Then Begin
                  PusuarioAtual:=ObjUsuarioGlobal.Get_CODIGO;
                  if (ObjUsuarioGlobal.LocalizaNome('EXCLAIM')=False)
                  Then Mensagemerro('Usu�rio default para esta opera��o n�o foi encontrado')
                  Else BEgin
                          ObjUsuarioGlobal.TabelaparaObjeto;
                          Self.DatabaseLocal:=TIBDatabase.Create(nil);
                          Self.Transactionlocal:=TIBTransaction.Create(nil);

                          Self.DatabaseLocal.SQLDialect:=3;
                          //Self.DatabaseLocal.DatabaseName:=Fdatamodulo.IBDatabase.DatabaseName;
                          Self.DatabaseLocal.DatabaseName:=self.pDATABASELOCAL.DatabaseName;
                          Self.DatabaseLocal.Params.Clear;
                          Self.DatabaseLocal.Params.Add('user_name='+useg.DesincriptaSenha('������'));
                          Self.DatabaseLocal.Params.Add('password='+ObjUsuarioGlobal.Get_senha);
                          Self.DatabaseLocal.LoginPrompt:=False;

                          Self.TransactionLocal.DefaultAction:=TARollback;
                          Self.TransactionLocal.Params.clear;
                          self.TransactionLocal.Params.Add('read_committed');
                          self.TransactionLocal.Params.Add('rec_version');
                          self.TransactionLocal.Params.Add('nowait');
                          self.TransactionLocal.DefaultDatabase:=Self.DatabaseLocal;

                          Self.DatabaseLocal.DefaultTransaction:=Self.TransactionLocal;

                          Try
                              Self.DatabaseLocal.Open;
                              Self.ObjDataset.Database:=Self.DatabaseLocal;
                              Self.ObjDataset.Transaction:=Self.TransactionLocal;

                          Except
                                on e:exception do
                                Begin
                                    MensagemErro(E.message);
                                end;
                          End;
                  end;

                  ObjUsuarioGlobal.LocalizaCodigo(PusuarioAtual);
                  ObjUsuarioGlobal.TabelaparaObjeto;
        end;


        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjUsuariosIb:=TobjUsuarioIb.create;
        Self.nivel:=TObjNiveis.create(self.pDatabaseLocal,self.pTransactionLocal);

        Self.ZerarTabela;

        With Self.ObjDataset do
        Begin
              SelectSql.clear;
                SelectSQL.ADD('Select nivel,CODIGO,nome,senha');
                SelectSQL.ADD(' from  Tabusuarios');
                SelectSQL.ADD(' WHERE codigo=0');
                InsertSql.clear;
                InsertSQL.add('Insert Into Tabusuarios(nivel,CODIGO,nome,senha)');
                InsertSQL.add('values (:nivel,:CODIGO,:nome,:senha)');
                ModifySQL.clear;
                ModifySQL.add('Update Tabusuarios set nivel=:nivel,CODIGO=:CODIGO,nome=:nome');
                ModifySQL.add(',senha=:senha');
                ModifySQL.add('where codigo=:codigo');
                DeleteSql.clear;
                DeleteSql.add('Delete from Tabusuarios where codigo=:codigo ');
                RefreshSQL.clear;
                RefreshSQL.ADD('Select nivel,CODIGO,nome,senha');
                RefreshSQL.ADD(' from  Tabusuarios');
                RefreshSQL.ADD(' WHERE codigo=0');
                open;
                Self.ObjDataset.First ;
                Self.status:=dsInactive;
        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjUsuarios.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjUsuarios.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjUsuarios.Commit;
begin
     Self.ObjDataset.Transaction.CommitRetaining;
end;

function TObjUsuarios.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select CODIGO,nome,NIVEL from TabUsuarios');
     Result:=Self.ParametroPesquisa;
end;

function TObjUsuarios.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Usuarios ';
end;



{
function TObjUsuarios.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjUsuarios.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjUsuarios.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           //IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.database:=Self.pDATABASELOCAL;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENUSUARIOS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjUsuarios.Free;
begin

   Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);

   
   Self.ObjUsuariosIb.free;


   if (Self.DatabaseLocal<>nil)
   Then Begin
            Self.DatabaseLocal.Close;
            freeandnil(Self.databaselocal);
   end;

   if (Self.transactionLocal<>nil)
   Then freeandnil(Self.transactionlocal);

   //adicionado celio 301109
   //alterado jonas 03/08/2010 11:17
   //original: if (Self.nivel<>nil)
   //          Then freeandnil(Self.nivel);

   //alterado:
   if (Self.Nivel <> nil)
   then Self.Nivel.Free;



end;

function TObjUsuarios.RetornaCampoCodigo: string;
begin
     result:='codigo';
end;

function TObjUsuarios.RetornaCampoNome: string;
begin
     result:='nome';
end;

procedure TObjUsuarios.Submit_nome(parametro: string);
begin
        Self.nome:=Parametro;
end;
function TObjUsuarios.Get_nome: string;
begin
        Result:=Self.nome;
end;
procedure TObjUsuarios.Submit_senha(parametro: string);
begin
        Self.senha:=Parametro;
end;
function TObjUsuarios.Get_senha: string;
begin
        Result:=Self.senha;
end;

function TObjUsuarios.VerificaPermissao: boolean;
Begin
     result:=False;
     iF (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE USUARIOS')=fALSE)
     Then exit;

     result:=True;
end;


function TObjUsuarios.Get_Nivel: string;
begin
    Result:=Self.nivel.get_codigo;
end;

procedure TObjUsuarios.Submit_Nivel(parametro: string);
begin
     Self.Nivel.submit_codigo(parametro);
end;

function TObjUsuarios.Get_NIvelPesquisa: TStringList;
begin
     result:=Self.nivel.get_pesquisa;
end;

function TObjUsuarios.Get_NivelTituloPesquisa: string;
begin
     result:=Self.nivel.get_titulopesquisa;
end;

procedure TObjUsuarios.Opcoes(pcodigo:string);
begin

     With FopcaoRel do
     Begin
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Permiss�o ap�s Atualiza��o de Base');
                ITEMS.ADD('Acerta generators');
                items.add('Retira permiss�o de usu�rios individual no Banco');
                items.add('Retira permiss�o de todos os usu�rios no Banco');
                items.add('Recria todos usu�rios');
                items.add('Criar   usu�rio atual pelo GSEC (utilit�rio do firebird)');
                items.add('Alterar usu�rio atual pelo GSEC (utilit�rio do firebird)');
                items.add('Excluir usu�rio atual pelo GSEC (utilit�rio do firebird)');
                items.add('Exibir a lista de usu�rios pelo GSEC (utilit�rio do firebird)');


          End;
          showmodal;

          If (FopcaoRel.Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
                0:Self.ConcedePermissaoTodos;
                1:Self.ObjUsuariosIb.Acertargenerator(self.ObjDataset.Database);
                2:self.ObjUsuariosIb.RetiraPermissoesAntigosUsuarios(self.ObjDataset.Database);
                3:self.ObjUsuariosIb.RetiraTodos(self.ObjDataset.Database);
                4:Self.RecriaUsuarios;
                5:Self.UsuarioGsec('I',pcodigo);
                6:Self.UsuarioGsec('A',pcodigo);
                7:Self.UsuarioGsec('E',pcodigo);
                8:Self.UsuarioGsec('D',pcodigo);
          End;

     end;
 End;


procedure TObjUsuarios.ConcedePermissaoTodos;
begin
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select nome from tabusuarios where nome<>''SYSDBA'' ');
          open;
          last;
          if (recordcount=0)
          Then Begin
                    Messagedlg('N�o foi localizado nenhum usu�rio para receber permiss�o!',mtinformation,[mbok],0);
                    exit;
          End;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.Lbmensagem.caption:='Concedendo permiss�es';


          first;

          While not(eof) do
          Begin
                FMostraBarraProgresso.Lbmensagem.caption:='Concedendo permiss�es '+uppercase(fieldbyname('nome').asstring);
                FMostraBarraProgresso.IncrementaBarra1(1);
                FMostraBarraProgresso.show;
                Application.ProcessMessages;
                if (Self.ObjUsuariosIb.ConcedePermissoes (uppercase(fieldbyname('nome').asstring),self.ObjDataset.Database)=False)
                Then Begin
                       Messagedlg('Erro na tentativa de Conceder Permiss�o a '+fieldbyname('nome').asstring,mterror,[mbok],0);
                       exit;
                End;
               next;
          End;
          FMostraBarraProgresso.close;
          Messagedlg('Atualiza��o das Permiss�es com Sucesso!',mtInformation,[mbok],0);
          Application.ProcessMessages;
     End;

end;

function TObjUsuarios.PedePermissao(TNivelPermissao: Integer): boolean;
var
Ptemp:String;
begin
     Result:=Self.PedePermissao(TNivelPermissao,Ptemp,'');
end;

function TObjUsuarios.PedePermissao(TNivelPermissao: Integer;Pfrase:String): boolean;
var
Ptemp:String;
begin
     Result:=Self.PedePermissao(TNivelPermissao,Ptemp,Pfrase);
end;


function TObjUsuarios.PedePermissao(TNivelPermissao: Integer;out PUsuarioAutorizou: String;Pfrase:String): boolean;
var
PCaptionAnterior,UsuarioAtual:String;
PsenhaTemp:String;
begin
     result:=False;
     //este procedimento � usado em casos que o usuario que esta logado
     //naum tem permissao para fazer determinada opera��o
     //desta forma ele chama este procedimento que pede o usuario
     //e a senha de alguem que tenha esta permissao para isso
     //se o usuario que tentou tem permissao retorna TRUE, else FALSE
     UsuarioAtual:=Self.codigo;
     Try
        limpaedit(FfiltroImp);
        With FfiltroImp do
        Begin
             DesativaGrupos;
             Grupo01.enabled:=True;
             Grupo02.enabled:=True;

             edtgrupo01.EditMask:='';
             edtgrupo02.EditMask:='';
             edtgrupo02.PasswordChar:='#';
             edtgrupo01.OnKeyDown:=nil;
             edtgrupo01.OnkeyPress:=Nil;
             edtgrupo02.OnKeyDown:=nil;
             edtgrupo02.OnkeyPress:=Nil;

             edtgrupo01.CharCase:=ecUpperCase;
             LbGrupo01.caption:='Usu�rio';
             LbGrupo02.caption:='Senha';

             PcaptionAnterior:=Caption;
             caption:=pfrase;
             showmodal;
             caption:=PCaptionAnterior;

             if (tag=0)
             then exit;

             if (edtgrupo01.text='')
             Then Begin
                       Messagedlg('Usu�rio n�o encontrado!',mterror,[mbok],0);
                       exit;
             End;

             if (Self.LocalizaNome(edtgrupo01.text)=False)
             Then Begin
                       MEssagedlg('Usu�rio n�o encontrado!',mterror,[mbok],0);
                       exit;
              End;

              Self.TabelaparaObjeto;
              //a senha ja foi desincriptada no tabela para objeto
              //so falta trocar as letras
              PsenhaTemp:=Self.Senha;

              if (PsenhaTemp<>edtgrupo02.text)
              Then Begin
                        Messagedlg('Senha Inv�lida!',mterror,[mbok],0);
                        exit;
              End;

              if (strtoint(Self.Nivel.Get_CODIGO)>TNivelPermissao)
              Then Begin
                       Messagedlg('Usu�rio com N�vel Inv�lido para esta permiss�o!',mterror,[mbok],0);
                       exit;
              End
              Else result:=True;

        End;
     Finally
            if (Result=True)
            Then PUsuarioAutorizou:=Self.Get_nome
            Else PUsuarioAutorizou:='';

            Ffiltroimp.edtgrupo02.PasswordChar:=#0;
            self.LocalizaCodigo(UsuarioAtual);
            self.TabelaparaObjeto;
     End;
end;




procedure TObjUsuarios.RecriaUsuarios;
begin
     With Self.ObjDataset do
     begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select * from Tabusuarios where nome<>''SYSDBA'' ');
          open;

          if (recordcount=0)
          then begin
                    MensagemAviso('Nenhum usu�rio selecionado');
                    exit;
          End;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Recriando Usu�rios';
          first;

          Try
            While not(eof) do
            begin
                FMostraBarraProgresso.lbmensagem.caption:='Recriando Usu�rios - '+uppercase(fieldbyname('nome').asstring);
                FMostraBarraProgresso.IncrementaBarra1(1);
                FMostraBarraProgresso.show;
                application.processmessages;
                Self.ObjUsuariosIb.ApagaUsuario(uppercase(fieldbyname('nome').asstring),Self.ObjDataset.Database);
                Self.ObjUsuariosIb.InsereUsuario(uppercase(fieldbyname('nome').asstring),Useg.DesincriptaSenha(fieldbyname('senha').asstring),Useg.DesincriptaSenha(fieldbyname('senha').asstring),Self.ObjDataset.Database);
                next;
            End;
          Finally
                FMostraBarraProgresso.close;
          End;
     End;
end;

procedure TObjUsuarios.trocasenhausuarioatual;
var
psai:boolean;
begin
     if (Self.Get_nome='SYSDBA')
     Then Begin
               MensagemErro('N�o � poss�vel alterar a senha do SYSDBA');
               exit;
     End;


     Repeat
         With FfiltroImp do
         Begin
              psai:=False;
              DesativaGrupos;
              Grupo01.Enabled:=True;
              Grupo02.Enabled:=True;
              Grupo03.Enabled:=True;

              LbGrupo01.caption:='Senha Atual';
              LbGrupo02.caption:='Nova Senha';
              LbGrupo03.caption:='Digite Novamente';

              showmodal;

              if (Tag=0)
              Then exit;

              if (self.Get_senha=edtgrupo01.text)
              Then Begin
                        if (edtgrupo02.Text=edtgrupo03.text)
                        Then Begin
                                  psai:=True;
                        End
                        Else MensagemErro('As novas senhas n�o conferem');
              End
              else MensagemErro('A senha atual n�o confere com a digitada');
         End;
     Until(psai=true);

     Self.Status:=dsedit;
     Self.Submit_senha(FfiltroImp.edtgrupo02.text);
     if (Self.Salvar(true)=False)
     Then Begin
               MensagemErro('Erro na tentativa de alterar a senha');
               exit;
     End;
     
end;


procedure TObjUsuarios.UsuarioGsec(pacao:string;pcodigo:string);
var
path:string;
psenha,plocalsecurity2:string;
begin

    if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
    Then Begin
              mensagemErro('Somente o usu�rio SYSDBA tem permiss�o para manuten��o de usu�rios no firebird');
              exit;
    End;

    if (pcodigo='') and (pacao<>'D')//display
    Then Begin
              MensagemAviso('Escolha primeiramente o usu�rio');
              exit;
    End;

     if (Pacao<>'D')
     Then Begin
               if (self.LocalizaCodigo(pcodigo)=False)
               then Begin
                         mensagemaviso('O usu�rio '+pcodigo+' n�o foi localizado');
                         exit;
               End;

               Self.TabelaparaObjeto;

               if (Self.nome<>'SYSDBA')
               Then Psenha:=Useg.encriptasenha(Self.Senha)
               Else psenha:=self.senha;
     End;


     if (ObjParametroGlobal.ValidaParametro('LOCAL DO SECURITY2.FDB PARA MANUTEN��O DE USU�RIOS COM GSEC')=False)
     Then exit;


     if (ObjParametroGlobal.Get_Valor='')
     Then Begin
               //100.99.98.12:/opt/firebird/security2.fdb
               MensagemErro('Par�metro inv�lido "LOCAL DO SECURITY2.FDB PARA MANUTEN��O DE USU�RIOS COM GSEC"');
               exit;
      End;

     plocalsecurity2:=ObjParametroGlobal.Get_Valor;
     path:=uessencialglobal.PegaPathSistema;
     path:=path+'GSEC.exe';

     if (FileExists(path)=False)
     then Begin
               mensagemErro('O Arquivo '+path+' n�o foi encontrado');
               exit;
     End;

     if (plocalsecurity2<>'LOCAL')
     then path:=path+' -user sysdba -password '+ObjUsuarioGlobal.Get_senha+' -database '+PlocalSecurity2
     Else path:=path+' -user sysdba -password '+ObjUsuarioGlobal.Get_senha;

     path:='CMD.EXE /C "'+path;
     
     if (pacao='I')
     then Begin
                path:=path+' -add '+Self.nome+' -pw '+psenha+'" >'+PegaPathSistema+'result.txt';
                //inputquery('','',path);

                //Para migrar o SAB para o Delphi XE2 o winexec vai ter q ser sub por shellexecute0
               // Winexec(pchar(path),SW_MAXIMIZE);

               ShellExecute(Application.Handle,'',pchar(path),'','',SW_MAXIMIZE);


                //como o usuario ja possa existir o erro nao podera ser visto
                //entao logo em seguida dou um modify para alterar a senha
                //caso o mesma ja exista na base
                path:=StringReplace(path,'-add','-modify',[rfReplaceAll]);
                //Winexec(pchar(path),SW_MAXIMIZE);
                ShellExecute(Application.Handle,'',pchar(path),'','',SW_MAXIMIZE);

                sleep(1000);
                FmostraStringList.Memo.Lines.clear;
                FmostraStringList.Memo.Lines.LoadFromFile(PegaPathSistema+'result.txt');
                FmostraStringList.Memo.lines.text:='RESULTADO DO COMANDO: '+#13+FmostraStringList.Memo.lines.text;
                FmostraStringList.showmodal;

                exit;
     End;

     if (pacao='A')
     then path:=path+' -modify '+Self.nome+' -pw '+psenha;

     if (pacao='E')
     then path:=path+' -delete '+Self.nome;

     if (pacao='D')
     then path:=path+' -display ';


     path:=path+'" >'+PegaPathSistema+'result.txt';


//     WinExec(pchar(path),SW_MAXIMIZE);
     ShellExecute(Application.Handle,'',pchar(path),'','',SW_MAXIMIZE);

     sleep(1000);
     FmostraStringList.Memo.Lines.clear;
     FmostraStringList.Memo.Lines.LoadFromFile(PegaPathSistema+'result.txt');
     FmostraStringList.Memo.lines.text:='RESULTADO DO COMANDO: '+#13+FmostraStringList.Memo.lines.text;
     FmostraStringList.showmodal;

End;

procedure TObjUsuarios.edtportadorKeyDown_PV(Sender: TObject; var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(self.Owner);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_Pesquisa,self.get_titulopesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

procedure TObjusuarios.EdtUsuarioKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FUsuarioXXX :TFusuarios ;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self.Owner);
            FUsuarioXXX :=TFusuarios.create(self.Owner);
            FUsuarioXXX.SetaDatabaseeTransaction(self.pDATABASELOCAL,Self.pTRANSACTIONLOCAL);

            If (FpesquisaLocal.PreparaPesquisa(Get_Pesquisa,Self.Get_TituloPesquisa,FUsuarioXXX,Self.pDATABASELOCAL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RetornaCampoCodigo).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.RetornaCampoNome<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RetornaCampoNome).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FUsuarioXXX);
     End;
end;

procedure TObjusuarios.EdtUsuarioExit(Sender: TObject; LABELNOME:Tlabel);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.GET_NOME;

end;

end.
