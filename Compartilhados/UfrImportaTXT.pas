unit UfrImportaTXT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBDatabase, StdCtrls, Buttons,
  ComCtrls, Grids, ExtCtrls, CheckLst,uobjimporta;

type
  TFrImportaTXT = class(TFrame)
    OpenDialog: TOpenDialog;
    GuiaTXT: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    BarradeProgresso: TProgressBar;
    STRGDados: TStringGrid;
    MemoArquivo: TRichEdit;
    PanelCaminhotabela: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    BtAbrirArquivo: TBitBtn;
    ComboTabela: TComboBox;
    BitBtn2: TBitBtn;
    edtcaminhobanco: TEdit;
    btcaminhobanco: TBitBtn;
    edtcaracterdelimitador: TEdit;
    checkcabecalho: TCheckBox;
    BtLerArquivo: TBitBtn;
    PanelPrincipalConfiguracao: TPanel;
    PanelNovoValor: TPanel;
    Button1: TButton;
    Rg_Opcoes_valor: TRadioGroup;
    PanelConfExtras: TPanel;
    Label4: TLabel;
    CLB_ConfExtras: TCheckListBox;
    BtAdicionaConfExtra: TButton;
    Panel4: TPanel;
    Label3: TLabel;
    Lbnometabela: TLabel;
    LbCamposTabela: TListBox;
    Panel6: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Panel5: TPanel;
    Panel2: TPanel;
    Label6: TLabel;
    LbCamposGRID: TListBox;
    LbValorGrid: TListBox;
    Panel3: TPanel;
    Label5: TLabel;
    LbCamposExtras: TListBox;
    LbValorExtras: TListBox;
    LbSelecionado: TLabel;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BtSalvaConf_GRID: TButton;
    BtAbreConf_GRID: TButton;
    Panel8: TPanel;
    Button5: TButton;
    Button6: TButton;
    SaveDialog: TSaveDialog;
    Label7: TLabel;
    LblinhaAtual: TLabel;
    Panel9: TPanel;
    Button3: TButton;
    Button2: TButton;
    btPegaLinha: TButton;
    procedure btcaminhobancoClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure LbCamposGRIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LbCamposGRIDClick(Sender: TObject);
    procedure LbValorGridClick(Sender: TObject);
    procedure LbValorGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn4Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtAbrirArquivoClick(Sender: TObject);
    procedure LbCamposTabelaClick(Sender: TObject);
    procedure BtLerArquivoClick(Sender: TObject);
    procedure LbCamposExtrasClick(Sender: TObject);
    procedure LbCamposExtrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GuiaTXTChanging(Sender: TObject; var AllowChange: Boolean);
    procedure LbValorExtrasClick(Sender: TObject);
    procedure LbValorExtrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtSalvaConf_GRIDClick(Sender: TObject);
    procedure BtAbreConf_GRIDClick(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure BtAdicionaConfExtraClick(Sender: TObject);
    procedure ComboTabelaKeyPress(Sender: TObject; var Key: Char);
    procedure LblinhaAtualDblClick(Sender: TObject);
    procedure btPegaLinhaClick(Sender: TObject);
    procedure LbCamposExtrasDblClick(Sender: TObject);
  private
      ObjImportaTXT:Tobjimporta;
      Strextras:TstringList;

      function  LocalizaPalavraRichEdit(Palavra: String): Boolean;
      Procedure MarcaOpcoesExtras;
      Procedure Setaconfiguracoesextras;
      Procedure SetaConfiguracoesgrid;
      procedure Importa;
      Function  ConfiguraExtras(PValorAtual:String):String;
    { Private declarations }
  public
    { Public declarations }
    Procedure InicializaFrame;
    Procedure DestroiFrame;
  end;

implementation

uses UessencialGlobal, UDataModulo;

{$R *.dfm}

function TFrImportaTXT.LocalizaPalavraRichEdit(Palavra: String): Boolean;
var
  FoundAt: LongInt;
  StartPos, ToEnd: Integer;
  cont:Integer;
begin
  With memoarquivo do
  begin
      StartPos := 0;

      ToEnd := Length(Text) - StartPos;

      FoundAt := FindText(Palavra,StartPos, ToEnd, [stMatchCase]);

      if (FoundAt <> -1)
      then begin
                setfocus;
                SelStart := FoundAt;
                SelLength := Length(Palavra);
                SelAttributes.Color:=clred;
      end;
  End;
end;

procedure TFrImportaTXT.btcaminhobancoClick(Sender: TObject);
begin
     if (OpenDialog.Execute=False)
     then exit;
     edtcaminhobanco.Text:=OpenDialog.FileName;
end;

procedure TFrImportaTXT.BitBtn3Click(Sender: TObject);
var
cont:integer;
begin
     LbCamposGRID.Items.clear;
     LbValorGrid.items.clear;
     for cont:=0 to (STRGDados.ColCount-1) do
     Begin
          LbCamposGRID.Items.add(uppercase(STRGDados.Cells[cont,0]));
          LbValorGrid.items.add('[VALORTXT]');
     End;
end;

procedure TFrImportaTXT.LbCamposGRIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    Pselecionado:integer;
begin
     LbValorGrid.ItemIndex:=LbCamposGRID.itemindex;
     Self.SetaConfiguracoesgrid;

     if (key=VK_DELETE)
     Then begin
               Pselecionado:=LbCamposGRID.ItemIndex;
               LbCamposGRID.Items[pselecionado]:='';
     End;
end;

procedure TFrImportaTXT.LbCamposGRIDClick(Sender: TObject);
begin
    LbValorGrid.ItemIndex:=LbCamposGRID.itemindex;
    Self.SetaConfiguracoesgrid;

end;

procedure TFrImportaTXT.LbValorGridClick(Sender: TObject);
begin
    LbCamposGRID.ItemIndex:=LbValorGrid.itemindex;
    Self.SetaConfiguracoesgrid;
end;

procedure TFrImportaTXT.LbValorGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     LbCamposGRID.ItemIndex:=LbValorGrid.itemindex;
     Self.SetaConfiguracoesgrid;
end;

procedure TFrImportaTXT.BitBtn4Click(Sender: TObject);
begin
     if (LbSelecionado.caption='')
     Then exit;

     if (LbSelecionado.caption='GRID')
     Then Begin
               LbCamposGRID.Items.clear;
               LbValorGrid.items.clear;
     End
     Else Begin
               LbCamposExtras.Items.clear;
               LbValorExtras.items.clear;
     End;
end;

procedure TFrImportaTXT.Button1Click(Sender: TObject);
var
PvalorPadrao,Pvalorextras:String;
cont:integer;
begin

     if (LbSelecionado.caption='')
     Then exit;

     if (LbSelecionado.Caption='GRID')
     Then Begin
               if (LbValorGrid.Items.count=0)
               then exit;

               //guardando as extras caso tenha
               PvalorExtras:='';
               cont:=pos('{#',LbValorGrid.Items[LbCamposGrid.ItemIndex]);
               if (cont>0)
               Then pvalorextras:=copy(LbValorGrid.Items[LbCamposGrid.ItemIndex],cont,length(LbValorGrid.Items[LbCamposGrid.ItemIndex])-cont+1);


               case Rg_Opcoes_valor.ItemIndex of
                  0:LbValorGrid.Items[LbCamposGrid.ItemIndex]:='[VALORTXT]'+Pvalorextras;
                  1:LbValorGrid.Items[LbCamposGrid.ItemIndex]:='[VAZIO]'+Pvalorextras;
                  2:begin
                         PvalorPadrao:='';
                         if (inputquery('Valor Padr�o','Digite o valor padr�o',pvalorpadrao)=False)
                         Then exit;

                         LbValorGrid.Items[LbCamposGrid.ItemIndex]:='[VAZIO]'+Pvalorpadrao+Pvalorextras;
                  end;
                  3:begin
                         PvalorPadrao:='';
                         if (inputquery('Valor Concatenado','Digite o valor a ser concatenado',pvalorpadrao)=False)
                         Then exit;
                  
                         LbValorGrid.Items[LbCamposGrid.ItemIndex]:='[VALORTXT]'+Pvalorpadrao+Pvalorextras;
                  end
                  else begin
                         PvalorPadrao:='';
                         if (inputquery('Valor Padr�o','Digite o valor padr�o',pvalorpadrao)=False)
                         Then exit;

                         LbValorGrid.Items[LbCamposGrid.ItemIndex]:='[VAZIO]'+Pvalorpadrao+Pvalorextras;

                  end;
               End;
     End
     Else Begin
               if (LbValorExtras.Items.count=0)
               then exit; 

               case Rg_Opcoes_valor.ItemIndex of
                  0:LbValorextras.Items[LbCamposextras.ItemIndex]:='';//vazio
                  1:begin//valor padrao
                         PvalorPadrao:='';
                         if (inputquery('Valor Padr�o','Digite o valor padr�o',pvalorpadrao)=False)
                         Then exit;

                         LbValorextras.Items[LbCamposextras.ItemIndex]:=Pvalorpadrao;
                  end;
                  2:begin//valor de um campo
                         PvalorPadrao:='';
                         if (inputquery('Valor de um CAMPO','Digite o nome do campo',pvalorpadrao)=False)
                         Then exit;

                         LbValorextras.Items[LbCamposextras.ItemIndex]:='[CAMPO]'+uppercase(Pvalorpadrao);
                  end
                  else begin
                      //valor padrao
                         PvalorPadrao:='';
                         if (inputquery('Valor Padr�o','Digite o valor padr�o',pvalorpadrao)=False)
                         Then exit;

                         LbValorextras.Items[LbCamposextras.ItemIndex]:=Pvalorpadrao;
                  end;

               End;

     End;
end;

procedure TFrImportaTXT.DestroiFrame;
begin
     Self.ObjImportaTXT.Free;
     freeandnil(StrExtras);
end;

procedure TFrImportaTXT.InicializaFrame;
begin
     GuiaTXT.ActivePageIndex:=0;
     MemoArquivo.Lines.clear;
     edtcaracterdelimitador.text:=';';
     checkcabecalho.checked:=true;
     LblinhaAtual.caption:='0';

     Try
        Self.ObjImportaTXT:=TObjimporta.create(self);
        Self.ObjImportaTXT.RetornaTabelasDisponiveis(ComboTabela.Items);
        Self.Strextras:=TstringList.create;
        ComboTabela.ItemIndex:=-1;
        ComboTabela.Text:='';

        
     Except
        mensagemerro('Erro na tentativa de Criar o Objeto ImportaTXT');
        exit;
     End;

end;

procedure TFrImportaTXT.BtAbrirArquivoClick(Sender: TObject);
begin
     if (edtcaminhobanco.text='')
     Then Begin
               mensagemaviso('Escolha o arquivo que deseja importar');
               exit;
     End;
     
     MemoArquivo.Lines.Clear;
     MemoArquivo.Lines.LoadFromFile(edtcaminhobanco.text);
end;

procedure TFrImportaTXT.LbCamposTabelaClick(Sender: TObject);
begin
     if (LbSelecionado.Caption='')
     then exit;

     if (LbSelecionado.Caption='GRID')
     Then Begin
              if (LbCamposGrid.Items.count=0)
              Then exit;

              if (Messagedlg('Deseja alterar o nome do campo selecionado?',mtconfirmation,[mbyes,mbno],0)=mrno)
              Then exit;

              LbCamposGrid.Items[lbcamposGrid.ItemIndex]:=LbCamposTabela.Items[LbCamposTabela.itemindex];
     End
     Else Begin//extras
              if (LbCamposextras.Items.count=0)
              Then exit;

              if (Messagedlg('Deseja alterar o nome do campo selecionado?',mtconfirmation,[mbyes,mbno],0)=mrno)
              Then exit;

              LbCamposextras.Items[lbcamposextras.ItemIndex]:=LbCamposTabela.Items[LbCamposTabela.itemindex];
     End;
end;


procedure TFrImportaTXT.BtLerArquivoClick(Sender: TObject);
var
cont,cont2,inicio,pquantcolunas:integer;
PTStrings:TStringList;
Ptemp:String;
begin

    if (MemoArquivo.Lines.Count=0)
    Then exit;

    Try
       PTStrings:=TStringList.Create;
       PTStrings.clear;
    Except
       Mensagemerro('Erro na tentativa de criar a StringList PtStrings');
       exit
    End;

    
    Try
          Self.BarradeProgresso.Position:=0;

          //lendo o cabecalho
          if (ExplodeStr_COMVAZIOS(MemoArquivo.lines[0],PTStrings,edtcaracterdelimitador.text,'')=False)
          Then begin
                    mensagemerro('Erro na tentativa de ler o cabe�alho do arquivo');
                    exit;
          end;
          pquantcolunas:=PTStrings.Count;


          STRGDados.ColCount:=pquantcolunas;
          STRGDados.RowCount:=MemoArquivo.Lines.Count;
          AjustaLArguraColunaGrid(strgdados);

          for cont:=0 to pquantcolunas -1 do
          Begin
               STRGDados.Cols[cont].Clear;
          End;

          if (checkcabecalho.Checked=True)
          Then Begin
                    inicio:=1;
                    for cont:=0 to pquantcolunas -1 do
                    Begin
                         STRGDados.Cells[cont,0]:=PTStrings[cont];
                    End;
          End
          Else Begin
                    inicio:=0;
                    for cont:=0 to pquantcolunas -1 do
                    Begin
                         STRGDados.Cells[cont,0]:='COLUNA'+inttostr(cont+1);
                    End;
          End;

          Self.BarradeProgresso.min:=0;
          Self.BarradeProgresso.max:=MemoArquivo.Lines.count;
          Application.ProcessMessages;

          for cont:=inicio to MemoArquivo.Lines.count-1 do
          Begin
               Self.BarradeProgresso.Position:=Self.BarradeProgresso.Position+1;
               Self.STRGDados.row:=cont;
               Application.ProcessMessages;

               Repeat
                     PTStrings.clear;
                     if (ExplodeStr_COMVAZIOS(MemoArquivo.lines[cont],PTStrings,edtcaracterdelimitador.text,'')=False)
                     Then begin
                              mensagemerro('Erro na tentativa de ler a linha atual'+#13+MemoArquivo.lines[cont]);
                              exit;
                     end;
                     
                     if (PTStrings.Count<>pquantcolunas)
                     Then begin
                               Ptemp:=MemoArquivo.lines[cont];
                               if (inputquery('Acerte a Linha','A linha atual n�o cont�m a quantidade de colunas igual a primeira linha',ptemp)=False)
                               Then exit;
                               MemoArquivo.lines[cont]:=Ptemp;
                     End;
               Until (PTStrings.Count=pquantcolunas);

               for cont2:=0 to PTStrings.count-1 do
               Begin
                    STRGDados.Cells[cont2,cont]:=PTStrings[cont2];
               End;
          End;
          Self.BarradeProgresso.Position:=0;
          MensagemAviso('Conclu�do');
          
          AjustaLArguraColunaGrid(STRGDados);
          
    Finally
          Freeandnil(PTStrings);
    End;
end;

procedure TFrImportaTXT.LbCamposExtrasClick(Sender: TObject);
begin
    LbValorExtras.ItemIndex:=LbCamposExtras.ItemIndex;
    Self.SetaConfiguracoesExtras;
end;

procedure TFrImportaTXT.LbCamposExtrasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
Pselecionado:integer;
pnovocampo:string;
begin
    LbValorExtras.ItemIndex:=LbCamposExtras.ItemIndex;
    Self.Setaconfiguracoesextras;


    pnovocampo:='';

    if (key=VK_DELETE)
    Then begin
              Pselecionado:=LbCamposExtras.ItemIndex;
              LbCamposExtras.DeleteSelected;
              LbValorExtras.itemindex:=Pselecionado;
              LbValorExtras.DeleteSelected;
    End;

    if (key=VK_INSERT)
    Then Begin
              if (InputQuery('Novo Campo','Digite o nome do campo',pnovocampo)=False)
              Then exit;
              if (Pnovocampo<>'')
              then Begin
                        LbCamposExtras.Items.add(uppercase(pnovocampo));
                        LbValorExtras.items.add('');
              End;
    end;
end;

procedure TFrImportaTXT.GuiaTXTChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
     LbSelecionado.caption:='';
end;

procedure TFrImportaTXT.LbValorExtrasClick(Sender: TObject);
begin
    LbCamposExtras.ItemIndex:=LbValorExtras.ItemIndex;
    Self.Setaconfiguracoesextras;
end;

procedure TFrImportaTXT.LbValorExtrasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
Pselecionado:integer;
pnovocampo:string;
begin
    LbCamposExtras.ItemIndex:=LbValorExtras.ItemIndex;
    Self.Setaconfiguracoesextras;

    pnovocampo:='';

    if (key=VK_DELETE)
    Then begin
              Pselecionado:=LbCamposExtras.ItemIndex;
              LbCamposExtras.DeleteSelected;
              LbValorExtras.itemindex:=Pselecionado;
              LbValorExtras.DeleteSelected;
    End;

    if (key=VK_INSERT)
    Then Begin
              if (InputQuery('Novo Campo','Digite o nome do campo',pnovocampo)=False)
              Then exit;
              
              if (Pnovocampo<>'')
              then Begin
                        LbCamposExtras.Items.add(uppercase(pnovocampo));
                        LbValorExtras.items.add('');
              End;
    end;
end;

procedure TFrImportaTXT.BitBtn2Click(Sender: TObject);
begin
     if (STRGDados.Cells[0,0]='')
     Then exit;

     case ComboTabela.ItemIndex of

         -1:Begin
                 MensagemAviso('Escolha uma Tabela');
                 exit;
         End;
         Else Self.Importa;
     end;
end;

procedure TFrImportaTXT.Importa;
var
  quant,posicaoinicial,cont,cont2:integer;
  pnometabela,Pvalor,pvalorbanco:string;
begin

    if (LbCamposGRID.items.count=0)
    Then Begin
              Mensagemaviso('Configure os campos que ser�o importados');
              exit;
    End;

    Pnometabela:=ComboTabela.text;
    
    if (Self.ObjImportaTXT.InstanciaObjeto(pnometabela)=False)
    then Begin
              Mensagemerro('Erro na tentativa de instanciar o objeto da '+pnometabela);
              exit;
    End;

     Try

        Try
           posicaoinicial:=strtoint(LblinhaAtual.caption);
           if (posicaoInicial=0)
           then Begin
                    PosicaoInicial:=1;
                    lblinhaatual.caption:='1';
           End;
        Except
              posicaoinicial:=1;
              lblinhaatual.caption:='1';
        End;

        if (posicaoinicial<>1)
        Then Begin
                  if (messagedlg('Deseja retornar a primeira linha para iniciar a importa��o?',mtconfirmation,[mbyes,mbno],0)=mryes)
                  Then PosicaoInicial:=1;
        End;

        BarradeProgresso.Position:=0;
        BarradeProgresso.Max:=STRGDados.RowCount-posicaoinicial;


        for cont:=posicaoinicial to STRGDados.RowCount-1 do
        Begin
             LblinhaAtual.caption:=inttostr(cont);
             Application.ProcessMessages;

             BarradeProgresso.Position:=BarradeProgresso.Position+1;
             STRGDados.row:=cont;
             
             Self.ObjImportaTXT.zerartabela;

             //campos do grid
             for cont2:=0 to STRGDados.ColCount-1 do
             Begin
                  Pvalorbanco:=STRGDados.Cells[cont2,cont];
                  Pvalor:=LbValorGrid.items[cont2];
                  Pvalor:=StrReplace(Pvalor,'[VALORTXT]',pvalorbanco);
                  Pvalor:=StrReplace(Pvalor,'[VAZIO]','');

                  //Caso tenha alguma configuracao extra, como por virgula, barra....
                  //esse � o lugar, antes de mandar para o objeto
                  pvalor:=Self.ConfiguraExtras(pvalor);
                  Self.Objimportatxt.fieldbyname(uppercase(LbCamposGRID.Items[cont2]),Pvalor);
             End;
             
             //campos extras
             for cont2:=0 to LbCamposExtras.Items.Count-1 do
             Begin
                  Pvalor:=LbValorExtras.items[cont2];
                  quant:=pos('[CAMPO]',pvalor);
                  if (quant<>0)
                  Then Begin
                            //separo qual � o campo e substituo
                            pvalor:=copy(pvalor,quant+7,length(Pvalor)-quant+7);
                            pvalor:=Self.objimportatxt.fieldbyname(pvalor);
                  End;
                  Self.objimportatxt.fieldbyname(uppercase(LbCamposExtras.Items[cont2]),Pvalor);
             End;


             if (Self.ObjImportatxt.Salvar(False)=False)
             Then Begin
                       MensagemErro('Erro na linha '+#13+STRGDados.Rows[cont].Text);

                       if (Messagedlg('Deseja Comitar a Informa��o adicionada at� o momento?',mtconfirmation,[mbyes,mbno],0)=mryes)
                       Then FDataModulo.IBTransaction.CommitRetaining;

                       exit;
             End;
        End;
        FDataModulo.IBTransaction.CommitRetaining;
        MensagemAviso('Conclu�do');
     Finally
          FDataModulo.IBTransaction.RollbackRetaining;
          Self.ObjImportaTXT.destroiobjeto;
     end;
end;

procedure TFrImportaTXT.BitBtn1Click(Sender: TObject);
var
cont:integer;
begin
     LbCamposExtras.Items.clear;
     LbValorExtras.Items.clear;

     for cont:=0 to  LbCamposTabela.Items.count-1 do
     Begin
          //procuro no grid de cima se nao encontrar insiro no de baixo
          if (LbCamposGRID.Items.IndexOf(LbCamposTabela.Items[cont])<0)
          Then Begin
                  LbCamposExtras.items.add(LbCamposTabela.Items[cont]);
                  LbValorExtras.Items.add('');
          End;
     end;
end;

procedure TFrImportaTXT.BtSalvaConf_GRIDClick(Sender: TObject);
begin
     If(SaveDialog.Execute=False)
     Then exit;


     LbCamposGRID.Items.SaveToFile(SaveDialog.FileName);
     LbValorGrid.Items.SaveToFile(SaveDialog.FileName+'V');

end;

procedure TFrImportaTXT.BtAbreConf_GRIDClick(Sender: TObject);
begin
     If(OpenDialog.Execute=False)
     Then exit;

     LbCamposGRID.Items.clear;
     LbValorGrid.Items.clear;

     LbCamposGRID.Items.LoadFromFile(OpenDialog.FileName);
     LbValorGrid.Items.LoadFromFile(OpenDialog.FileName+'V');


end;

procedure TFrImportaTXT.Button5Click(Sender: TObject);
begin
     If(SaveDialog.Execute=False)
     Then exit;


     LbCamposextras.Items.SaveToFile(SaveDialog.FileName);
     LbValorextras.Items.SaveToFile(SaveDialog.FileName+'V');

end;

procedure TFrImportaTXT.Button6Click(Sender: TObject);
begin
     If(OpenDialog.Execute=False)
     Then exit;

     LbCamposextras.Items.clear;
     LbValorextras.Items.clear;

     LbCamposextras.Items.LoadFromFile(OpenDialog.FileName);
     LbValorextras.Items.LoadFromFile(OpenDialog.FileName+'V');

end;

procedure TFrImportaTXT.Button3Click(Sender: TObject);
var
plinhastr:String;
begin
     if (inputquery('Processar Linha','Digite o N� da linha a Processar',plinhastr)=False)
     Then exit;

     Try
        strtoint(Plinhastr);
     Except
        mensagemerro('Linha Inv�lida');
        exit;
     End;
     LblinhaAtual.Caption:=plinhastr;
end;

procedure TFrImportaTXT.BtAdicionaConfExtraClick(Sender: TObject);
var
ptemp,pvalor:String;
cont:integer;
begin
     Ptemp:='';

     if (LbSelecionado.caption='')
     then exit;

     if (CLB_ConfExtras.Checked[0]=true)
     Then ptemp:=Ptemp+';'+'"';

     if (CLB_ConfExtras.Checked[1]=true)
     Then ptemp:=Ptemp+';'+#39;

     if (CLB_ConfExtras.Checked[2]=true)
     Then ptemp:=Ptemp+';'+'.';

     if (CLB_ConfExtras.Checked[3]=true)
     Then ptemp:=Ptemp+';'+',2';

     if (CLB_ConfExtras.Checked[4]=true)
     Then ptemp:=Ptemp+';'+',3';

     if (CLB_ConfExtras.Checked[5]=true)
     Then ptemp:=Ptemp+';'+'/';

     if (Ptemp<>'')
     Then ptemp:='{#'+ptemp+'#}';

     if (LbSelecionado.caption='GRID')
     Then Begin
               Pvalor:=LbValorGrid.Items[LbValorGrid.itemindex];
               cont:=pos('{#',pvalor);

               if (Cont>0)
               Then Pvalor:=copy(pvalor,1,cont-1);//+copy(pvalor,cont+3,length(pvalor)-3);

               Pvalor:=pvalor+ptemp;
               LbValorGrid.Items[LbValorGrid.itemindex]:=pvalor;
     End;
end;

procedure TFrImportaTXT.MarcaOpcoesExtras;
var
pvalor:string;
cont:integer;
begin
     //quando clicar em uma das listbox ele marca
     // as opcoes extras de acordo com o valor

     if (LbSelecionado.Caption='')
     then exit;

     if (LbSelecionado.caption='GRID')
     Then Begin
              if (LbValorGrid.itemindex<0)
              Then exit;
              
              Pvalor:=LbValorGrid.Items[LbValorGrid.itemindex];
     end
     Else exit;

     for cont:=0 to CLB_ConfExtras.Items.count-1 do
     Begin
          CLB_ConfExtras.Checked[cont]:=False;
     End;

     cont:=pos('{#',Pvalor);
     if (cont>0)
     Then Begin
               Pvalor:=copy(pvalor,cont+2,length(pvalor)-cont);//-2 porque tem o #}
               Pvalor:=copy(pvalor,1,length(pvalor)-2);//-2 porque tem o #}
               ExplodeStr(pvalor,StrExtras,';','');

               if (StrExtras.IndexOf('"')>=0)
               Then CLB_ConfExtras.Checked[0]:=True;

               if (StrExtras.IndexOf(#39)>=0)
               Then CLB_ConfExtras.Checked[1]:=True;

               if (StrExtras.IndexOf('.')>=0)
               Then CLB_ConfExtras.Checked[2]:=True;

               if (StrExtras.IndexOf(',2')>=0)
               Then CLB_ConfExtras.Checked[3]:=True;

               if (StrExtras.IndexOf(',3')>=0)
               Then CLB_ConfExtras.Checked[4]:=True;

               if (StrExtras.IndexOf('/')>=0)
               Then CLB_ConfExtras.Checked[5]:=True;
     end;
end;

procedure TFrImportaTXT.Setaconfiguracoesextras;
begin
    LbSelecionado.caption:='EXTRA';
    Rg_Opcoes_valor.Items.clear;
    Rg_Opcoes_valor.Items.add('Vazio');
    Rg_Opcoes_valor.Items.add('Valor Padr�o');
    Rg_Opcoes_valor.Items.add('Valor de um campo');

    PanelConfExtras.Visible:=False;
    LbCamposGRID.ItemIndex:=-1;
    LbValorGrid.ItemIndex:=-1;
end;

procedure TFrImportaTXT.SetaConfiguracoesgrid;
begin
    LbValorExtras.ItemIndex:=-1;
    LbCamposExtras.ItemIndex:=-1;
    Self.MarcaOpcoesExtras;
    LbSelecionado.caption:='GRID';
    PanelConfExtras.Visible:=True;
    Rg_Opcoes_valor.items.clear;
    Rg_Opcoes_valor.Items.add('Valor do TXT');
    Rg_Opcoes_valor.Items.add('Vazio');
    Rg_Opcoes_valor.Items.add('Valor Padr�o');
    Rg_Opcoes_valor.Items.add('Concatenado');
end;

procedure TFrImportaTXT.ComboTabelaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if (key=#13)
     Then begin
               LbCamposTabela.Items.clear;
               LbNometabela.caption:=ComboTabela.Text;
               Self.ObjImportaTXT.RetornaCamposTabela(ComboTabela.Text,LbCamposTabela.Items);
     End;
end;


function TFrImportaTXT.ConfiguraExtras(PValorAtual: String): String;
var
pvalor:string;
cont:integer;
begin
       result:=PvalorAtual;

       Pvalor:=PvalorAtual;

       cont:=pos('{#',Pvalor);
       if (cont>0)
       Then Begin
                 Pvalor:=copy(pvalor,cont+2,length(pvalor)-cont);//-2 porque tem o #}
                 Pvalor:=copy(pvalor,1,length(pvalor)-2);//-2 porque tem o #}
                 ExplodeStr(pvalor,StrExtras,';','');

                 pvalor:=copy(pvaloratual,1,cont-1);

                 if (StrExtras.IndexOf('"')>=0)
                 Then Pvalor:=come(pvalor,'"');

                 if (StrExtras.IndexOf(#39)>=0)
                 Then Pvalor:=come(pvalor,#39);

                 if (StrExtras.IndexOf('.')>=0)
                 Then Pvalor:=come(pvalor,'.');

                 if (StrExtras.IndexOf(',2')>=0)
                 Then pvalor:=AdicionaVirgulaDecimal(pvalor,2);

                 if (StrExtras.IndexOf(',3')>=0)
                 Then pvalor:=AdicionaVirgulaDecimal(pvalor,3);

                 if (StrExtras.IndexOf('/')>=0)
                 Then pvalor:=inserebarra(Pvalor);

                 result:=Pvalor;
       end;
end;

procedure TFrImportaTXT.LblinhaAtualDblClick(Sender: TObject);
var
temp:string;
begin
     temp:='';

     if (InputQuery('LINHA','DIGITE A LINHA QUE DESEJA INICIAR',temp)=false)
     then exit;

     try
        strtoint(temp);
     Except
           exit;
     End;

     LblinhaAtual.caption:=temp;
     STRGDados.row:=strtoint(temp);

     

     
end;

procedure TFrImportaTXT.btPegaLinhaClick(Sender: TObject);
begin
      LblinhaAtual.caption:=IntToStr(STRGDados.Row);
end;

procedure TFrImportaTXT.LbCamposExtrasDblClick(Sender: TObject);
begin
     SELF.Button1Click(nil);
end;


end.
