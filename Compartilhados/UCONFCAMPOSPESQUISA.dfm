object FCONFCAMPOSPESQUISA: TFCONFCAMPOSPESQUISA
  Left = 251
  Top = 88
  Width = 544
  Height = 360
  Caption = 
    'Cadastro de Configura'#231#227'o de Campos de Pesquisa - EXCLAIM TECNOLO' +
    'GIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 232
    Width = 528
    Height = 90
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object Btnovo: TBitBtn
      Left = 3
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 115
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 227
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 339
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 115
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 227
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 339
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btsairClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 528
    Height = 232
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = 'Principal'
      object LbCODIGO: TLabel
        Left = 10
        Top = 20
        Width = 44
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Lbusuario: TLabel
        Left = 10
        Top = 68
        Width = 50
        Height = 13
        Caption = 'Usu'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbIDENTIFICADOR: TLabel
        Left = 10
        Top = 117
        Width = 85
        Height = 13
        Caption = 'Identificador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Lbcampo: TLabel
        Left = 10
        Top = 166
        Width = 44
        Height = 13
        Caption = 'Campo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 10
        Top = 36
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object Edtusuario: TEdit
        Left = 10
        Top = 84
        Width = 400
        Height = 19
        MaxLength = 100
        TabOrder = 1
      end
      object EdtIDENTIFICADOR: TEdit
        Left = 10
        Top = 133
        Width = 400
        Height = 19
        MaxLength = 150
        TabOrder = 2
      end
      object Edtcampo: TEdit
        Left = 10
        Top = 182
        Width = 400
        Height = 19
        MaxLength = 100
        TabOrder = 3
      end
    end
  end
end
