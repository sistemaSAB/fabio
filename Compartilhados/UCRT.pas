unit UCRT;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCRT,
  jpeg;

type
  TFCRT = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    ImagemFundo: TImage;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbDescricao: TLabel;
    EdtDescricao: TEdit;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCRT:TObjCRT;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCRT: TFCRT;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCRT.ControlesParaObjeto: Boolean;
Begin

  Try
    With Self.ObjCRT do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Descricao(edtDescricao.text);

        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCRT.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCRT do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtDescricao.text:=Get_Descricao;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCRT.TabelaParaControles: Boolean;
begin
     If (Self.ObjCRT.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFCRT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCRT=Nil)
     Then exit;

     If (Self.ObjCRT.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjCRT.free;
end;

procedure TFCRT.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFCRT.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjCRT.status:=dsInsert;
     edtDescricao.setfocus;

end;


procedure TFCRT.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCRT.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                Self.ObjCRT.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtDescricao.setfocus;
                
    End;


end;

procedure TFCRT.btgravarClick(Sender: TObject);
begin

     If Self.ObjCRT.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCRT.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCRT.Get_codigo;
     Self.ObjCRT.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCRT.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjCRT.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCRT.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCRT.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCRT.btcancelarClick(Sender: TObject);
begin
     Self.ObjCRT.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCRT.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFCRT.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCRT.Get_pesquisa,Self.ObjCRT.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCRT.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCRT.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjCRT.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFCRT.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCRT.FormShow(Sender: TObject);
begin

      limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCRT:=TObjCRT.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
     retira_fundo_labels(self);

     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

