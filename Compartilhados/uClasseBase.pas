{******************************************************************************}
{2016/08/26 - cria��o                                                          }
{Classe base de todas outras classes que ser�o criadas, para ter um ponto de   }
{ partida unico a todos                                                        }
{******************************************************************************}

unit uClasseBase;

interface

uses Classes, Variants;

type
  TClasseBase = class
  end;

  TClasseErro = class( TClasseBase )
  private
    listaErros: TStringList;
  public
    constructor create;
    destructor destroy; override;

    procedure addErro( erro: string );
    function teveErro: Boolean;
    function getErro: string;
  end;

implementation

uses SysUtils;

{ TClasseErro }

procedure TClasseErro.addErro(erro: string);
begin
  if( not VarIsEmpty( erro ) ) then
    listaErros.Add( erro );
end;

constructor TClasseErro.create;
begin
  listaErros := TStringList.create;
end;

destructor TClasseErro.destroy;
begin
  FreeAndNil( listaErros );
  inherited;
end;

function TClasseErro.getErro: string;
begin
  Result := listaErros.Text;
end;

function TClasseErro.teveErro: Boolean;
begin
  Result := listaErros.Count > 0;
end;

end.
 