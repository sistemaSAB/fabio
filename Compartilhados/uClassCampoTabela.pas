unit uClassCampoTabela;

interface

uses Classes;

type
  Tcampo = class(TCollectionItem)
  private
    FObrigatorio: boolean;
    FEscrita: Boolean;
    FNome: string;
    FTamanho: string;
    FTipo: string;
    FSubTipo: string;
    procedure setFEscrita(const Value: Boolean);
    procedure setFNome(const Value: string);
    procedure setFObrigatorio(const Value: boolean);
    procedure setFTamanho(const Value: string);
    procedure setFTipo(const Value: string);
    procedure setSubFTipo(const Value: string);
  public
    property Nome: string read FNome write setFNome;
    property Tamanho: string read FTamanho write setFTamanho;
    property Tipo: string read FTipo write setFTipo;
    property SubTipo: string read FSubTipo write setSubFTipo;
    property Obrigatorio: boolean read FObrigatorio write setFObrigatorio;
    property Escrita: Boolean read FEscrita write setFEscrita;
  end;

  TListaCampos = class(TCollection)
  private
    function getCampo(Index: integer): Tcampo;

  public
    property Item[Index: integer]: Tcampo read getCampo;

    function Add: Tcampo;
    function AddCampo(const Nome: string; const Tamanho: string; const Tipo: string;
                      const Obrigatorio: boolean; const Escrita: boolean): Tcampo;
    procedure DelCampo(index: integer);
    function getIndiceCampo(nome: string): integer;
    function RetornaTipoCampo(pTipoBD,SubTipoBD: string):string;
  end;

implementation

{ Tcampo }

procedure Tcampo.setFEscrita(const Value: Boolean);
begin
  FEscrita := Value;
end;

procedure Tcampo.setFNome(const Value: string);
begin
  FNome := Value;
end;

procedure Tcampo.setFObrigatorio(const Value: boolean);
begin
  FObrigatorio := Value;
end;

procedure Tcampo.setFTamanho(const Value: string);
begin
  FTamanho := Value;
end;

procedure Tcampo.setFTipo(const Value: string);
begin
  FTipo := Value;
end;

procedure Tcampo.setSubFTipo(const Value: string);
begin
  FSubTipo := Value;
end;

{ TListaCampos }

function TListaCampos.Add: Tcampo;
begin
  Result := inherited Add as Tcampo;
end;

function TListaCampos.AddCampo(const Nome, Tamanho, Tipo: string;
  const Obrigatorio, Escrita: boolean): Tcampo;
begin
  Result := inherited Add as Tcampo;
  Result.setFNome(nome);
  Result.setFTamanho(tamanho);
  Result.setFTipo(Tipo);
  Result.setFObrigatorio(Obrigatorio);
  Result.setFEscrita(Escrita);
end;

procedure TListaCampos.DelCampo(index: integer);
begin
  inherited Delete(index);
end;

function TListaCampos.getCampo(index: integer): Tcampo;
begin
  Result := inherited Items[index] as Tcampo;
end;

function TListaCampos.getIndiceCampo(nome: string): integer;
var
  cont: Integer;
begin
  Result := -1;
  for cont := 0 to Count - 1 do
  begin
    if(nome = getCampo(cont).Nome) then
      Result := cont;
  end;
end;

function TListaCampos.RetornaTipoCampo(pTipoBD,SubTipoBD: string): string;
begin
  Result := '';
  if(pTipoBD = 'SHORT') then
    Result := 'INTEGER'
  else
    if(pTipoBD = 'LONG') then
      Result := 'INTEGER'
    else
      if(pTipoBD = 'INT64') then
        Result := 'INTEGER'
      else
        if(pTipoBD = 'FLOAT') then
          Result := 'FLOAT'
        else
          if(pTipoBD = 'DOUBLE') then
            Result := 'FLOAT'
          else
            if(pTipoBD = 'TEXT') then
              Result := 'CHAR'
            else
              if(pTipoBD = 'VARYING') then
                Result := 'STRING'
              else
                if(pTipoBD = 'BLOB') then
                  Result := 'STRING'
                else
                  if(pTipoBD = 'DATE') then
                    Result := 'DATE'
                  else
                    if(pTipoBD = 'TIME') then
                      Result := 'TIME'
                    else
                      if(pTipoBD = 'TIMESTAMP') then
                        Result := 'STRING';

end;

end.
 