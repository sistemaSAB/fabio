unit UobjFORMASPAGAMENTO_FC;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,uobjportador;

Type
   TObjFORMASPAGAMENTO_FC=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               portador:TOBJPORTADOR;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaNome(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;


                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_NOME(parametro: string);
                Function Get_NOME: string;
                Procedure Submit_DINHEIRO_CHEQUE(parametro: string);
                Function Get_DINHEIRO_CHEQUE: string;
                Procedure Submit_APRAZO(parametro: string);
                Function Get_APRAZO: string;
                procedure EdtportadorExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                //CODIFICA DECLARA GETSESUBMITS



                
         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               NOME:string;
               DINHEIRO_CHEQUE:string;
               APRAZO:string;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  Uportador;


{ TTabTitulo }


Function  TObjFORMASPAGAMENTO_FC.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.NOME:=fieldbyname('NOME').asstring;
        Self.DINHEIRO_CHEQUE:=fieldbyname('DINHEIRO_CHEQUE').asstring;
        Self.APRAZO:=fieldbyname('APRAZO').asstring;
        If(FieldByName('portador').asstring<>'')
        Then Begin
                 If (Self.portador.LocalizaCodigo(FieldByName('portador').asstring)=False)
                 Then Begin
                          Messagedlg('Portador N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.portador.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjFORMASPAGAMENTO_FC.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NOME').asstring:=Self.NOME;
        ParamByName('DINHEIRO_CHEQUE').asstring:=Self.DINHEIRO_CHEQUE;
        ParamByName('APRAZO').asstring:=Self.APRAZO;
        ParamByName('portador').asstring:=Self.portador.GET_CODIGO;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjFORMASPAGAMENTO_FC.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFORMASPAGAMENTO_FC.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NOME:='';
        DINHEIRO_CHEQUE:='';
        APRAZO:='';
        portador.ZerarTabela;
//CODIFICA ZERARTABELA





     End;
end;

Function TObjFORMASPAGAMENTO_FC.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (NOME='')
      Then Mensagem:=mensagem+'/Nome';
      If (DINHEIRO_CHEQUE='')
      Then Mensagem:=mensagem+'/Dinheiro ou Cheque?';
      If (APRAZO='')
      Then Mensagem:=mensagem+'/A Prazo';
      If (portador.get_codigo='')
      Then Mensagem:=mensagem+'/Portador';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFORMASPAGAMENTO_FC.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.portador.LocalizaCodigo(Self.portador.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFORMASPAGAMENTO_FC.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.portador.Get_Codigo<>'')
        Then Strtoint(Self.portador.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Portador';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFORMASPAGAMENTO_FC.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFORMASPAGAMENTO_FC.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
      If not(((DINHEIRO_CHEQUE='D') OR (DINHEIRO_CHEQUE='C')))
      Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Dinheiro ou Cheque?';
      If not((APRAZO='S') OR (APRAZO='N'))
      Then Mensagem:=mensagem+'/Valor Inv�lido para o campo A Prazo';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;
function TObjFORMASPAGAMENTO_FC.LocalizaNome(Parametro: string): boolean;
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NOME,DINHEIRO_CHEQUE,APRAZO,portador');
           SQL.ADD(' from  TABFORMASPAGAMENTO_FC');
           SQL.ADD(' WHERE NOME='+#39+uppercase(parametro)+#39);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjFORMASPAGAMENTO_FC.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NOME,DINHEIRO_CHEQUE,APRAZO,portador');
           SQL.ADD(' from  TABFORMASPAGAMENTO_FC');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFORMASPAGAMENTO_FC.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFORMASPAGAMENTO_FC.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFORMASPAGAMENTO_FC.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.portador:=TOBJPORTADOR.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABFORMASPAGAMENTO_FC(CODIGO,NOME,DINHEIRO_CHEQUE');
                InsertSQL.add(' ,APRAZO,portador)');
                InsertSQL.add('values (:CODIGO,:NOME,:DINHEIRO_CHEQUE,:APRAZO,:portador');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABFORMASPAGAMENTO_FC set CODIGO=:CODIGO,NOME=:NOME');
                ModifySQL.add(',DINHEIRO_CHEQUE=:DINHEIRO_CHEQUE,APRAZO=:APRAZO,portador=:portador');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABFORMASPAGAMENTO_FC where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFORMASPAGAMENTO_FC.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFORMASPAGAMENTO_FC.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFORMASPAGAMENTO_FC');
     Result:=Self.ParametroPesquisa;
end;

function TObjFORMASPAGAMENTO_FC.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FORMASPAGAMENTO_FC ';
end;


destructor TObjFORMASPAGAMENTO_FC.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.portador.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFORMASPAGAMENTO_FC.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFORMASPAGAMENTO_FC.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFORMASPAGAMENTO_FC.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjFORMASPAGAMENTO_FC.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjFORMASPAGAMENTO_FC.Submit_NOME(parametro: string);
begin
        Self.NOME:=Parametro;
end;
function TObjFORMASPAGAMENTO_FC.Get_NOME: string;
begin
        Result:=Self.NOME;
end;
procedure TObjFORMASPAGAMENTO_FC.Submit_DINHEIRO_CHEQUE(parametro: string);
begin
        Self.DINHEIRO_CHEQUE:=Parametro;
end;
function TObjFORMASPAGAMENTO_FC.Get_DINHEIRO_CHEQUE: string;
begin
        Result:=Self.DINHEIRO_CHEQUE;
end;
procedure TObjFORMASPAGAMENTO_FC.Submit_APRAZO(parametro: string);
begin
        Self.APRAZO:=Parametro;
end;
function TObjFORMASPAGAMENTO_FC.Get_APRAZO: string;
begin
        Result:=Self.APRAZO;
end;
//CODIFICA GETSESUBMITS


procedure TObjFORMASPAGAMENTO_FC.EdtportadorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.portador.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.portador.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.portador.GET_NOME;
End;
procedure TObjFORMASPAGAMENTO_FC.EdtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPORTADOR:TFPORTADOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPORTADOR:=TFPORTADOR.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.portador.Get_Pesquisa,Self.portador.Get_TituloPesquisa,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPORTADOR);
     End;
end;
//CODIFICA EXITONKEYDOWN


end.



