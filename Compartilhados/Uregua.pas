unit Uregua;

interface
uses ExtCtrls,SysUtils,graphics;

    //***************Funcoes*******************************
    function  RetornaColunaPixel(coluna: integer): integer;
    function  RetornaLinhaPixel(linha: integer): integer;
    procedure Desenha_regua(Image:Timage);
    procedure DesenhaReguaHorizontal(Image:Timage);
    procedure DesenhaReguaVertical(Image:Timage);
var
  //Determinam quantas linhas e quantas colunas ter�o a r�gua
  quantlinhas_gb,quantcolunas_gb:integer;
  //determinam o espaco entre uma coluna e outra, ou linha e outra
  espacamento_horizontal,espacamento_vertical:integer;
  //determina o espaco entre a borda esquerda e a r�gua
  //e borda superior e r�gua
  recuo_esquerdo,recuo_superior:integer;

implementation


function RetornaColunaPixel(coluna: integer): integer;
begin
    {Esta Fun��o retorna em pixels o posicionamento
    para uma determinada coluna, exemplo:
     Para posicionar uma Label na Coluna 5 chama-se essa
     fun��o passando o N� 5 no par�metro coluna}

    //Obs: O N�mero 2 foi adicionado apenas como ajuste
    //visto que as labels n�o ficam exatamente emcima da coluna
    //desejada
    result:=recuo_esquerdo+(espacamento_horizontal*coluna)-2;
end;

function RetornaLinhaPixel(linha: integer): integer;
begin
     {Esta Fun��o retorna em pixels o posicionamento
     para uma determinada linha, exemplo:
     Para posicionar uma Label na Linha 5 chama-se essa
     fun��o passando o N� 5 no par�metro Linha}
     result:=recuo_superior+(linha-1)*espacamento_vertical;
end;

procedure Desenha_regua(Image:Timage);
begin
     //Esta fun��o preenche as variaveis globais
     //que determinam a r�gua, e desenha a r�gua.

     //Quantidade de Colunas na Horizontal
     quantcolunas_gb:=90;
     //Quantidade de Linhas na Vertical
     quantlinhas_gb:=66;
     //Recuo Esquerdo (margem)
     recuo_esquerdo:=30;
     //Recuo Direito (margem)
     recuo_superior:=30;
     //Espaco em Pixels entre as colunas
     espacamento_horizontal:=7;
     //Espaco em Pixels entre as Linhas
     espacamento_vertical:=10;
     //******Desenhando as Reguas******************
     DesenhaReguaHorizontal(Image);
     DesenhaReguaVertical(Image);
end;

procedure DesenhaReguaHorizontal(Image:Timage);
var
cont:integer;
begin
     {
      Exemplo:
              Recuo esquerdo
              |______|
                      espacamento horizontal
                     |___|____|____|____|...
                     0   1    2    3    4...
                     Exemplo, recuo esquerdo 10 pixels
                     espacamento horizontal 5 pixels:
            Pixel        10 15  20  25  30  35  40  45  50  55 ...
            Colunas      1  2   3   4   5   6   7   8   9   10 ...
     }

     //desenhando a regua horizontal
     for cont:=1 to quantcolunas_gb do
     Begin
          if ((cont mod 10)=0)//marcando as divisoes de 10 em 10 na regua
          Then Begin
                    //numeros desenhados, desenho 5 pixels antes pra ficar um digito do lado de cada traco exemplo 1|0
                    image.Canvas.TextOut(recuo_esquerdo+(cont*espacamento_horizontal)-5,5,inttostr(cont));
                    //desenho a linha azul que dividira de 10 em 10 colunas
                    image.Canvas.Pen.Color:=clBlue;
                    image.Canvas.MoveTo(recuo_esquerdo+cont*espacamento_horizontal,20);
                    //10 pixels de tamanho vertical, inicia no 20 termina no 30
                    image.Canvas.LineTo(recuo_esquerdo+cont*espacamento_horizontal,30);
          End
          Else Begin
                    //marcando de vermelho as colunas  de 5 em 5
                    //ou preto em colunas normais
                    if ((cont mod 5 )=0)
                    Then image.Canvas.Pen.Color:=clRed
                    Else image.Canvas.Pen.Color:=clblack;
                    //*****desenhando uma linha de 10 pixels****************
                    image.Canvas.MoveTo(recuo_esquerdo+cont*espacamento_horizontal,20);
                    image.Canvas.LineTo(recuo_esquerdo+cont*espacamento_horizontal,30);
          End;
     End;
end;
procedure DesenhaReguaVertical(Image:Timage);
var
cont2:integer;
begin

     {
      Exemplo:
              Recuo Superior em 10 pixels
              e espacamento vertical em 10 pixels

              0_ _ _
              |
              |Recuo Superior
              - - -
        Pixels    Linha
              10  1

              20  2

              30  3
     }

     //desenhando a regua vertical
      
     for cont2:=1 to quantlinhas_gb do
     Begin
          if ((cont2 mod 10)=0)
          Then Begin//marcando as divisoes de 10 em 10 na regua

                    //Desenha os Numeros
                    image.Canvas.TextOut(recuo_esquerdo-25,recuo_superior+cont2*espacamento_vertical-5,inttostr(cont2));
                    image.Canvas.Pen.Color:=clblue;
                    //desenha uma linha ---   iniciando 5 pixels antes do recuo resquerdo
                    //ate 5 pixels depois
                    image.Canvas.MoveTo(recuo_esquerdo-10,recuo_superior+cont2*espacamento_vertical);
                    image.Canvas.LineTo(recuo_esquerdo,recuo_superior+cont2*espacamento_vertical);
          End
          Else Begin
                    if ((cont2 mod 5 )=0)//marcando as divisoes de 5 em 5 na regua
                    Then image.Canvas.Pen.Color:=clRed
                    Else image.Canvas.Pen.Color:=clblack;
                    image.Canvas.MoveTo(recuo_esquerdo-10,recuo_superior+cont2*espacamento_vertical);
                    image.Canvas.LineTo(recuo_esquerdo,recuo_superior+cont2*espacamento_vertical);
          End;
     End;

End;



end.
