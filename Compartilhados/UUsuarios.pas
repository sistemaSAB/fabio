unit UUsuarios;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjUsuarios,IBDatabase;

type
  TFusuarios = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btsair: TBitBtn;
    BtOpcoes: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    LbNivel: TLabel;
    Senha: TLabel;
    Label4: TLabel;
    EdtCodigo: TEdit;
    EdtNome: TEdit;
    EdtSenha: TEdit;
    Edtnivel: TEdit;
    EdtSenha2: TEdit;
    CheckSomenteSistema: TCheckBox;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
    Label5: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure EdtnivelKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtOpcoesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtNomeKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
         ObjUsuarios:TObjUsuarios;

         DATABASELOCAL:TIBDatabase;
         TRANSACTIONLOCAL:TIBTransaction;

         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         function  atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
        procedure SetaDatabaseeTransaction(pDatabase: TIBDatabase;
          pTransaction: TIBTransaction);
  end;

var
  Fusuarios: TFusuarios;


implementation

uses Uessencialglobal, Upesquisa, UNiveis, UescolheImagemBotao,
  UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFusuarios.ControlesParaObjeto: Boolean;
Begin

        if edtsenha.text<>edtsenha2.text then
        begin
                messagedlg('As Senhas devem ser iguais!',mterror,[mbok],0);
                result:=FALSE;
                exit;
        end;

        Try
        With ObjUsuarios do
        Begin
             Submit_nivel(edtnivel.text);
             Submit_CODIGO(edtCODIGO.text);
             Submit_nome(edtnome.text);
             Submit_senha(edtsenha.text);
             result:=true;
        End;
        Except
          result:=False;
        End;

End;

function TFusuarios.ObjetoParaControles: Boolean;
Begin
        Try
        With ObjUsuarios do
        Begin
             edtnivel.text:=Get_nivel;
             edtCODIGO.text:=Get_CODIGO;
             edtnome.text:=Get_nome;
             edtsenha.text:=Get_senha;
             EdtSenha2.text:=Get_senha;
             result:=true;
        End;
        Except
              result:=False;
        End;

End;

function TFusuarios.TabelaParaControles: Boolean;
begin
     If (ObjUsuarios.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFusuarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  If (ObjUsuarios=Nil) Then
    exit;

  If (ObjUsuarios.status<>dsinactive) Then
  Begin
    Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
    abort;
    exit;
  End;

  ObjUsuarios.free;
  if(Self.Owner = nil) then
    FDataModulo.IBTransaction.Commit;

end;

procedure TFusuarios.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFusuarios.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     LbNivel.Caption:='';
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjUsuarios.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjUsuarios.status:=dsInsert;
     Edtnome.setfocus;

end;


procedure TFusuarios.btalterarClick(Sender: TObject);
begin
    If (ObjUsuarios.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjUsuarios.Status:=dsEdit;
                edtnome.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
          End;


end;

procedure TFusuarios.btgravarClick(Sender: TObject);
begin

     If ObjUsuarios.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjUsuarios.salvar(true,CheckSomenteSistema.Checked)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     LbNivel.Caption:='';
     desabilita_campos(Self);

     if (ObjUsuarios.LocalizaCodigo(ObjUsuarios.Get_CODIGO))
     then Begin
               ObjUsuarios.TabelaparaObjeto;
               Self.ObjetoParaControles;
     End;
     lbquantidade.caption:=atualizaQuantidade;
end;

procedure TFusuarios.btexcluirClick(Sender: TObject);
begin
     If (ObjUsuarios.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjUsuarios.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjUsuarios.exclui(edtcodigo.text,true,checksomentesistema.checked)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     LbNivel.Caption:='';
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFusuarios.btcancelarClick(Sender: TObject);
begin
     ObjUsuarios.cancelar;

     limpaedit(Self);
     LbNivel.Caption:='';
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFusuarios.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFusuarios.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjUsuarios.Get_pesquisa,ObjUsuarios.Get_TituloPesquisa,Nil,self.DATABASELOCAL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjUsuarios.status<>dsinactive
                                  then exit;

                                  If (ObjUsuarios.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjUsuarios.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            LbNivel.Caption:='';
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFusuarios.EdtnivelKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FNiveisX:TFNiveis;
begin
     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FNiveisX:=TFNiveis.create(self);
            FNiveisX.SetaDatabaseeTransaction(self.DATABASELOCAL,Self.TRANSACTIONLOCAL);

            If (FpesquisaLocal.PreparaPesquisa(ObjUsuarios.Get_nivelPesquisa,ObjUsuarios.Get_nivelTituloPesquisa,FNiveisX,self.DATABASELOCAL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        Self.LbNivel.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           freeandnil(FNiveisX);
     End;
end;

procedure TFusuarios.BtOpcoesClick(Sender: TObject);
begin
     ObjUsuarios.opcoes(edtcodigo.text);
end;

procedure TFusuarios.FormShow(Sender: TObject);
begin
     UessencialGlobal.PegaCorForm(Self);
     limpaedit(Self);
     LbNivel.Caption:='';
     desabilita_campos(Self);
     Self.ObjUsuarios:=nil;


     If (ObjUsuarios.VerificaPermissao=False)
     Then esconde_botoes(self)
     Else Begin
              if(Self.Tag<>99)
              then mostra_botoes(Self);

              Try

                 ObjUsuarios:=TObjUsuarios.create(True,self.DATABASELOCAL,self.TRANSACTIONLOCAL,self.Owner);
              Except
                    Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
                    Self.close;
              End;

              FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
              FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
              FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
              Self.Color:=clwhite;
              lbquantidade.caption:=atualizaQuantidade;
     end;

end;

procedure TFusuarios.EdtNomeKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#32
     Then key:=#0;
end;

function TFusuarios.atualizaQuantidade: string;
begin
    result:='Existem '+ContaRegistros('TABUSUARIOS','CODIGO')+' Usu�rios Cadastrados';
end;

procedure TFusuarios.SetaDatabaseeTransaction(pDatabase: TIBDatabase;
  pTransaction: TIBTransaction);
begin
      self.DATABASELOCAL:=pDatabase;
      Self.TRANSACTIONLOCAL:=pTransaction;
end;

procedure TFusuarios.FormCreate(Sender: TObject);
begin
      try
      self.DATABASELOCAL := FDataModulo.IBDatabase;
      self.TRANSACTIONLOCAL := FDataModulo.IBTransaction;
      except
            on e:exception do
            MensagemErro(e.Message);
      end;
end;

end.

