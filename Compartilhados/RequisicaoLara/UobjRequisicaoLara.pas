unit UobjRequisicaoLara;

interface

  uses UobjREQUISICAOSENHA, UObjREQUISICAOSENHALOCAL;

type

      TObjRequisicaoLara=class

      Public
            constructor create;
            function ValidaAcesso(pversao:integer):boolean; 
      Private
             PathLaraGlobal:String;
             PcodigoBaseSite:string;

             ObjRequisicaoSenha:TObjREQUISICAOSENHA;
             ObjRequisicaoSenhaLocal:tobjrequisicaosenhalocal;
      End;
      
implementation

uses UDataModulo, UessencialGlobal, Useg,sysutils;

{ ObjRequisicaoLara }

constructor TObjRequisicaoLara.create;
begin

end;

function TObjRequisicaoLara.ValidaAcesso(pversao: integer): boolean;
begin
      result:=False;

      Self.ObjRequisicaoSenha:=nil;
      Self.ObjRequisicaoSenhaLocal:=nil;

      if (ObjParametroGlobal.ValidaParametro('CODIGO BASE DO CLIENTE NO SITE')=false)
      Then exit;

      PcodigoBaseSite:=useg.DesincriptaSenha(ObjParametroGlobal.Get_Valor);

      Try
         strtoint(pcodigobasesite);
      Except
            mensagemerro('Valor Inv�lido para o par�metro "CODIGO BASE DO CLIENTE NO SITE"');
            exit;
      End;

      Try
        PathLaraGlobal:=PegaPathBanco('PATH_SENHA');

        if (pathLaraGlobal='')
        then Begin
                  Mensagemerro('Configure o PATH_SENHA no arquivo PATH.INI');
                  exit;
        End;
      Except
            on e:exception do
            begin
                  Mensagemerro('Erro na tentativo de acesso a chave PATH_SENHA no arquivo PATH.INI'+#13+e.message);
                  exit;
            End;
      End;

      Try//try finally que destroi os objetos

        Try
             Self.ObjRequisicaoSenha:=TObjREQUISICAOSENHA.create(PathLaraGlobal);
        Except
              on e:exception do
              Begin
                   MensagemErro(e.message);
                   exit;
              End;
        End;

        Try
            //tabela local que recebera a resposta
            Self.ObjRequisicaoSenhaLocal:=tobjrequisicaosenhalocal.create;
        Except
              on e:exception do
              Begin
                   mensagemerro(e.message);
                   exit;
              End;
        End;

        SistemaemModoDemoGlobal:=False;

        if (Self.ObjRequisicaoSenha.VersaoLara(pversao)=false)
        Then exit;

        //requisitante 'SISTEMA_SAFIRA_EXCLAIM_TECNOLOGIA'
        //mensagemAviso('ERROR IN PROTECTION');
        //SistemaemModoDemoGlobal:=True;
        if(Objrequisicaosenha.ValidaEntrada('00000000','0','�����������������������������',SistemaemModoDemoGlobal,PcodigoBaseSite,ObjRequisicaoSenhaLocal.Objquery)=False)
        then Begin
                     Mensagemerro(objrequisicaosenha.PmensagemExterna);
                     exit;
        End
        Else result:=True;

      Finally
             Try
                if (Self.ObjRequisicaoSenha<>nil)
                Then Self.ObjRequisicaoSenha.free;
             Except
                   on e:exception do
                   Begin
                        Mensagemerro('Erro na tentativa de destruir o objeto OBJREQUISICAOSENHA'+#13+e.message);
                   End;
             End;

             Try
                if (Self.ObjRequisicaoSenhaLocal<>nil)
                Then Self.ObjRequisicaoSenhaLocal.free;
             Except
                  on e:exception do
                   Begin
                        Mensagemerro('Erro na tentativa de destruir o objeto OBJREQUISICAOSENHALOCAL'+#13+e.message);
                   End;
             End;
      End;
end;

end.
