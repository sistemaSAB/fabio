unit UobjRECIBOAVULSO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjRECIBOAVULSO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Data(parametro: string);
                Function Get_Data: string;
                Procedure Submit_Hora(parametro: string);
                Function Get_Hora: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;
                Procedure Submit_Credor(parametro: string);
                Function Get_Credor: string;
                Procedure Submit_Recebedor(parametro: string);
                Function Get_Recebedor: string;
                Procedure Submit_Referente(parametro: string);
                Function Get_Referente: string;
                PRocedure ImprimeRecibo(pcodigo:string);
                
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Data:string;
               Hora:string;
               Valor:string;
               Credor:string;
               Recebedor:string;
               Referente:string;
//CODIFICA VARIAVEIS PRIVADAS









               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UrelatorioPersonalizado;





Function  TObjRECIBOAVULSO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.Hora:=fieldbyname('Hora').asstring;
        Self.Valor:=fieldbyname('Valor').asstring;
        Self.Credor:=fieldbyname('Credor').asstring;
        Self.Recebedor:=fieldbyname('Recebedor').asstring;
        Self.Referente:=fieldbyname('Referente').asstring;
//CODIFICA TABELAPARAOBJETO







        result:=True;
     End;
end;


Procedure TObjRECIBOAVULSO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('Hora').asstring:=Self.Hora;
        ParamByName('Valor').asstring:=virgulaparaponto(Self.Valor);
        ParamByName('Credor').asstring:=Self.Credor;
        ParamByName('Recebedor').asstring:=Self.Recebedor;
        ParamByName('Referente').asstring:=Self.Referente;
//CODIFICA OBJETOPARATABELA







  End;
End;

//***********************************************************************

function TObjRECIBOAVULSO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjRECIBOAVULSO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Data:='';
        Hora:='';
        Valor:='';
        Credor:='';
        Recebedor:='';
        Referente:='';
//CODIFICA ZERARTABELA







     End;
end;

Function TObjRECIBOAVULSO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Data='')
      Then Mensagem:=mensagem+'/Data';
      If (Hora='')
      Then Mensagem:=mensagem+'/Hora';
      If (Valor='')
      Then Mensagem:=mensagem+'/Valor';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjRECIBOAVULSO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjRECIBOAVULSO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjRECIBOAVULSO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
     try
        Strtotime(Self.Hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjRECIBOAVULSO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjRECIBOAVULSO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro RECIBOAVULSO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Data,Hora,Valor,Credor,Recebedor,Referente');
           SQL.ADD(' from  TabReciboAvulso');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjRECIBOAVULSO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjRECIBOAVULSO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjRECIBOAVULSO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabReciboAvulso(CODIGO,Data,Hora,Valor,Credor');
                InsertSQL.add(' ,Recebedor,Referente)');
                InsertSQL.add('values (:CODIGO,:Data,:Hora,:Valor,:Credor,:Recebedor');
                InsertSQL.add(' ,:Referente)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabReciboAvulso set CODIGO=:CODIGO,Data=:Data');
                ModifySQL.add(',Hora=:Hora,Valor=:Valor,Credor=:Credor,Recebedor=:Recebedor');
                ModifySQL.add(',Referente=:Referente');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabReciboAvulso where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjRECIBOAVULSO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjRECIBOAVULSO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabRECIBOAVULSO');
     Result:=Self.ParametroPesquisa;
end;

function TObjRECIBOAVULSO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de RECIBOAVULSO ';
end;


function TObjRECIBOAVULSO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENRECIBOAVULSO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENRECIBOAVULSO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjRECIBOAVULSO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjRECIBOAVULSO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjRECIBOAVULSO.RetornaCampoNome: string;
begin
      result:='Referente';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjReciboAvulso.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjReciboAvulso.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjReciboAvulso.Submit_Data(parametro: string);
begin
        Self.Data:=Parametro;
end;
function TObjReciboAvulso.Get_Data: string;
begin
        Result:=Self.Data;
end;
procedure TObjReciboAvulso.Submit_Hora(parametro: string);
begin
        Self.Hora:=Parametro;
end;
function TObjReciboAvulso.Get_Hora: string;
begin
        Result:=Self.Hora;
end;
procedure TObjReciboAvulso.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjReciboAvulso.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
procedure TObjReciboAvulso.Submit_Credor(parametro: string);
begin
        Self.Credor:=Parametro;
end;
function TObjReciboAvulso.Get_Credor: string;
begin
        Result:=Self.Credor;
end;
procedure TObjReciboAvulso.Submit_Recebedor(parametro: string);
begin
        Self.Recebedor:=Parametro;
end;
function TObjReciboAvulso.Get_Recebedor: string;
begin
        Result:=Self.Recebedor;
end;
procedure TObjReciboAvulso.Submit_Referente(parametro: string);
begin
        Self.Referente:=Parametro;
end;
function TObjReciboAvulso.Get_Referente: string;
begin
        Result:=Self.Referente;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjRECIBOAVULSO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJRECIBOAVULSO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TObjRECIBOAVULSO.ImprimeRecibo(pcodigo: string);
var
folhaatual,pquantidadefolhas,pquantidadeprodutos,plinhaproduto,contaprodutos,
linhatemp:integer;
Objquerylocal:tibquery;
PcamposExtras,PvalorCamposExtras:TStringList;
pparcelas,pfuncionarios:string;
ps:string;
psomatemp:currency;
begin
     If(Pcodigo='')
     Then Begin
          MensagemErro('� necess�rio escolher um Recibo');
          Exit;
     End;

     
     Try
        PcamposExtras:=TStringList.create;
        PvalorCamposExtras:=TStringList.create;
        PcamposExtras.Clear;
        PvalorCamposExtras.clear;
     Except
           MensagemErro('Erro na tentativa de criar duas stringlists para campos extras');
           exit;
     End;

     Try
        Objquerylocal:=tibquery.create(nil);
        Objquerylocal.database:=FdataModulo.ibdatabase;

     Except
           Freeandnil(PCamposExtras);
           Freeandnil(PvalorCamposExtras);
           mensagemerro('Erro na tentativa de criar a Querylocal');
           exit;
     End;
Try
     With ObjqueryLocal do
     Begin
         close;
         SQL.clear;
         SQL.add('Select * from ViewReciboAvulso where codigo='+pcodigo);
         open;
         if (recordcount=0)
         then begin
                   Mensagemerro('Recibo n�o encontrado');
                   exit;
         End;


         pcamposextras.add('VALORPOREXTENSO');
         PvalorCamposExtras.add('('+valorextenso(fieldbyname('valor').ascurrency)+')');

         Frelatoriopersonalizado.Nomerelatorio:='REL_RECIBO_AVULSO_PERSONALIZADO';
         if (Frelatoriopersonalizado.Inicializa=False)
         then Begin
                   mensagemerro('N�o foi poss�vel inicializar o Recibo Avulso');
                   exit;
         End;

         if (Frelatoriopersonalizado.STRGCampos.RowCount=2)//linha de cabecalho e de quantidade
         then Begin
                   mensagemerro('Configure o Recibo Avulso no m�dulo de configura��o');
                   exit;
         End;

         Frelatoriopersonalizado.Rdprint.Abrir;
         if (Frelatoriopersonalizado.Rdprint.Setup=False)
         then begin
                   Frelatoriopersonalizado.Rdprint.Fechar;
                   exit;
         End;

         Plinhaproduto:=Frelatoriopersonalizado.RetornaPrimeiroProdutorepeticao;
         pquantidadefolhas:=1;
         linhatemp := 0;
         Frelatoriopersonalizado.ImprimeCamposPreImpresso(Objquerylocal,false,linhatemp,PcamposExtras,PvalorCamposExtras);
         Frelatoriopersonalizado.Rdprint.Fechar;
         Frelatoriopersonalizado.Encerra;
     End;

Finally
       freeandnil(objquerylocal);
       Freeandnil(PCamposExtras);
       Freeandnil(PvalorCamposExtras);
End;
     
end;

end.



