unit UDigitaSenhaPermissao;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, UObjUsuarios;

type
  TFDigitaSenhaPermissao = class(TForm)
    edtnome: TEdit;
    edtsenha: TEdit;
    BtnOk: TBitBtn;
    BtnCancel: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure BtnOkClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnCancelClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDigitaSenhaPermissao: TFDigitaSenhaPermissao;
  Objusuarios:Tobjusuarios;

implementation

uses Useg, Uessencialglobal, UDataModulo, Uprincipal;

{$R *.DFM}

procedure TFDigitaSenhaPermissao.FormShow(Sender: TObject);
begin
     UessencialGlobal.PegaCorForm(Self);
     Self.Tag:=-1;
     edtnome.text:='';
     edtsenha.text:='';
     edtsenha.enabled:=False;
     edtnome.enabled:=False;
     Try
        ObjUsuarios:=TObjUsuarios.create;
        edtsenha.enabled:=True;
        edtnome.enabled:=True;
        edtnome.setfocus;                       
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           exit;
     End;
End;

procedure TFDigitaSenhaPermissao.BtnOkClick(Sender: TObject);
var
tmpsenha:string;
begin

      tmpsenha:='';

      if (edtnome.text='')
      Then Begin
                messagedlg('Digite o Nome do Usu�rio!',mterror,[mbok],0);
                exit;
      End;
      TmpSenha:=Edtsenha.text;

      {If (edtnome.text<>'SYSDBA')
      Then tmpSenha:=Useg.EncriptaDecripta(Useg.trocaletra(Tmpsenha));}
      
      If (objusuarios.LocalizaNome(edtnome.text)=False)
      Then Begin
                   Messagedlg('Usu�rio n�o encontrado no sistema!',mterror,[mbok],0);
                   exit;
      End;
      Objusuarios.TabelaparaObjeto;//no sistema inteiro terei o nivel dele

      if (Objusuarios.Get_senha<>Tmpsenha)
      then Begin
                Messagedlg('Senha Inv�lida!',mterror,[mbok],0);
                exit;
      End;

      edtnome.text:='';
      edtsenha.text:='';
      //envio no Tag  Nivel Cara
      Self.tag:=strtoint(Objusuarios.Get_Nivel);
      Self.Close;


end;

procedure TFDigitaSenhaPermissao.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (objusuarios<>nil)
     then Objusuarios.free;
end;

procedure TFDigitaSenhaPermissao.BtnCancelClick(Sender: TObject);
begin
     Self.Tag:=-1;
     Self.close;
end;

end.
