unit UObjniveis;
Interface
Uses Classes,Db,Ibcustomdataset,IBStoredProc,uessencialglobal,ibdatabase;

Type
   TObjniveis=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                DATABASELOCAL:TIBDatabase;
                TRANSACTIONLOCAL:TIBTransaction;

                Constructor Create;overload;
                Constructor Create(pDatabase:TIBDatabase;pTransaction:TIBTransaction);overload;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Procedure Submit_CODIGO           (parametro:string);
                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                function  Get_nome: string;
                procedure Submit_nome(parametro: string);
                function VerificaPermissao: boolean;


         Private
               ObjDataset:Tibdataset;

               CODIGO           :string;
               nome             :string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls;


{ TTabTitulo }


Function  TObjniveis.TabelaparaObjeto:Boolean;//ok
begin
        With ObjDataset do
        Begin
                Self.CODIGO:=fieldbyname('CODIGO').asstring;
                Self.nome:=fieldbyname('nome').asstring;
        end;
end;


Procedure TObjniveis.ObjetoparaTabela;//ok
begin
        With ObjDataset do
        Begin
                fieldbyname('CODIGO').asstring:=Self.CODIGO;
                fieldbyname('nome').asstring:=Self.nome;
        end;
End;

//***********************************************************************

function TObjniveis.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 //Then FDataModulo.IBTransaction.CommitRetaining;
 then self.TRANSACTIONLOCAL.commitretaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjniveis.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO:='';
        Self.nome:='';
     End;
end;

Function TObjniveis.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjniveis.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     {If (Self.CODCURSO.LocalizaCodigo(Self.codcurso.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Curso n�o Encontrado!';}

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjniveis.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjniveis.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjniveis.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjniveis.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
                SelectSql.clear;
                SelectSQL.ADD('Select CODIGO,nome');
                SelectSQL.ADD(' from  TabNiveis');
                SelectSQL.ADD(' WHERE codigo='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjniveis.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjniveis.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 //Then FDataModulo.IBTransaction.CommitRetaining;
                 Then Self.Transactionlocal.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjniveis.Create;
begin
      self.Create(FDataModulo.IBDatabase,FDataModulo.IBTransaction);
end;

constructor TObjniveis.create(pDatabase:TIBDatabase;pTransaction:TIBTransaction);
begin

        self.DATABASELOCAL := pDatabase;
        self.TRANSACTIONLOCAL := pTransaction;

        Self.ObjDataset:=Tibdataset.create(nil);
        //Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ObjDataset.Database:=self.DATABASELOCAL;
        //Self.ObjDatasource:=TDataSource.Create(nil);
        //Self.CodCurso:=TObjCursos.create;
        Self.ParametroPesquisa:=TStringList.create;

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSql.clear;
                SelectSQL.ADD('Select CODIGO,nome');
                SelectSQL.ADD(' from  TabNiveis');
                SelectSQL.ADD(' WHERE codigo=0');
                InsertSql.clear;
                InsertSQL.add('Insert Into TabNiveis(CODIGO,nome)');
                InsertSQL.add('values (:CODIGO,:nome)');
                ModifySQL.clear;
                ModifySQL.add('Update TabNiveis set CODIGO=:CODIGO,nome=:nome');
                ModifySQL.add('where codigo=:codigo');
                DeleteSql.clear;
                DeleteSql.add('Delete from TabNiveis where codigo=:codigo ');
                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,nome');
                RefreshSQL.ADD(' from  TabNiveis');
                RefreshSQL.ADD(' WHERE codigo=0');
                open;
                Self.ObjDataset.First ;
                Self.status:=dsInactive;
        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjniveis.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjniveis.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjniveis.Commit;
begin
     //FDataModulo.IBTransaction.CommitRetaining;
     Self.Transactionlocal.CommitRetaining;
end;

function TObjniveis.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from Tabniveis');
     Result:=Self.ParametroPesquisa;
end;

function TObjniveis.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de niveis ';
end;



{
function TObjniveis.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjniveis.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjniveis.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_NIVEIS';
           
           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o niveis',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjniveis.Free;
begin
Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);

end;

function TObjniveis.RetornaCampoCodigo: string;
begin
     result:='codigo';
end;

function TObjniveis.RetornaCampoNome: string;
begin
     result:='nome';
end;

procedure TObjniveis.Submit_nome(parametro: string);
begin
        Self.nome:=Parametro;
end;
function TObjniveis.Get_nome: string;
begin
        Result:=Self.nome;
end;

function TObjNiveis.VerificaPermissao: boolean;
begin
     result:=False;
     iF (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE NIVEIS')=fALSE)
     Then exit;
     result:=True;
end;



end.
