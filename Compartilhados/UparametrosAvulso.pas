unit UparametrosAvulso;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjParametrosavulso,
  jpeg, Grids, DBGrids;

type
  TFparametrosAvulso = class(TForm)
    Guia: TTabbedNotebook;
    EdtCodigo: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtnome: TEdit;
    Label3: TLabel;
    edtvalor: TEdit;
    memofuncao: TMemo;
    Label4: TLabel;
    Panel1: TPanel;
    StrGrid: TStringGrid;
    btCarregar: TBitBtn;
    btAtualizar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    Btnovo: TBitBtn;
    DBGrid: TDBGrid;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure memofuncaoKeyPress(Sender: TObject; var Key: Char);
    procedure btCarregarClick(Sender: TObject);
    procedure btAtualizarClick(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
  private
         ObjParametros:TObjParametrosavulso;

         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         procedure PesquisaParametros;
    function Criar: boolean;
    { Private declarations }
  public
    { Public declarations }
        { Public declarations }
        Procedure PassaObjeto(Pobjeto:tobjparametrosavulso);
        Procedure Destruir;

  end;

var
  FparametrosAvulso: TFparametrosAvulso;



implementation

uses UUtils, UdataMOdulo;


{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFparametrosAvulso.ControlesParaObjeto: Boolean;
Begin
  Try
    With self.objparametros do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_nome(edtnome.text);
         submit_valor(edtvalor.text);
         Submit_Funcao(memofuncao.lines.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFparametrosAvulso.ObjetoParaControles: Boolean;
Begin
  Try
     With self.objparametros do
     Begin
        edtCODIGO.text          :=Get_CODIGO;
        edtnome.text            :=Get_nome;
        edtvalor.text           :=Get_valor;
        memofuncao.lines.Text   :=get_Funcao;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFparametrosAvulso.TabelaParaControles: Boolean;
begin
     If (self.objparametros.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
//****************************************



procedure TFparametrosAvulso.FormClose(Sender: TObject; var Action: TCloseAction);
begin

  if ObjParametros.IBTransaction.InTransaction then
    self.ObjParametros.Commit;

{If (self.objparametros=Nil)
     Then exit;

If (self.objparametros.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

self.objparametros.free;
}

end;

procedure TFparametrosAvulso.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFparametrosAvulso.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=self.objparametros.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;

     self.objparametros.status:=dsInsert;
     Guia.pageindex:=0;
     Edtnome.setfocus;

end;


procedure TFparametrosAvulso.btalterarClick(Sender: TObject);
begin
    If (self.objparametros.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                self.objparametros.Status:=dsEdit;
                guia.pageindex:=0;
                edtnome.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
    End;


end;

procedure TFparametrosAvulso.btgravarClick(Sender: TObject);
begin

     If self.objparametros.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (self.objparametros.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);

     if (Self.ObjParametros.LocalizaCodigo(Self.ObjParametros.Get_CODIGO)=true)
     Then Begin
               Self.ObjParametros.TabelaparaObjeto;
               self.ObjParametros.Commit;
               Self.ObjetoParaControles;
     End;

     Self.pesquisaparametros;



end;

procedure TFparametrosAvulso.btexcluirClick(Sender: TObject);
begin
     If (self.objparametros.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (self.objparametros.LocalizaCodigo(edtcodigo.text)=False) Then
     Begin
      Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
      exit;
     End;
     self.ObjParametros.Commit;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (self.objparametros.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.ObjParametros.PesquisaParametros;
end;

procedure TFparametrosAvulso.btcancelarClick(Sender: TObject);
begin
     self.objparametros.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFparametrosAvulso.btsairClick(Sender: TObject);
begin
    Close;
end;

function TFparametrosAvulso.Criar: boolean;
begin


end;

procedure TFparametrosAvulso.Destruir;
begin

end;

procedure TFparametrosAvulso.FormShow(Sender: TObject);
begin

     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(self);
     Guia.PageIndex:=0;

     Try
        DBGrid.DataSource:=Self.ObjParametros.ObjDatasource;
        Self.pesquisaparametros;
     Except
           desab_botoes(self);
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           exit;
     End;
end;

procedure TFparametrosAvulso.memofuncaoKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     then memofuncao.SetFocus;
end;

procedure TFparametrosAvulso.btCarregarClick(Sender: TObject);
begin
     self.objparametros.ResgataTodosOsParametros(StrGrid);
end;

procedure TFparametrosAvulso.btAtualizarClick(Sender: TObject);
begin
        if MessageDlg('Tem certeza que deseja Atualizar todos os parametros de uma s� vez?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
        then exit;

        self.objparametros.AtualizaTodosOsParametros(StrGrid);
end;

procedure TFparametrosAvulso.PesquisaParametros;
begin
     Self.ObjParametros.PesquisaParametros;
end;

procedure TFparametrosAvulso.DBGridDblClick(Sender: TObject);
begin
     if (self.objparametros.status<>dsinactive)
     then exit;

     if (Self.DBGrid.DataSource.DataSet.RecordCount=0)
     then exit;

     if (self.objparametros.LocalizaCodigo(Self.DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring)=False)
     Then exit;

     self.objparametros.TabelaparaObjeto;
     self.ObjParametros.Commit;
     Self.ObjetoParaControles;
end;

procedure TFparametrosAvulso.DBGridKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     then Begin
               Self.DBGridDblClick(sender);
               Self.DBGrid.SetFocus;
     End;

end;

procedure TFparametrosAvulso.PassaObjeto(Pobjeto: tobjparametrosavulso);
begin
     Self.ObjParametros:=Pobjeto;
end;

end.

