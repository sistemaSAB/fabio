unit UAcertaVenctoServidor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, ExtCtrls;

type
  TFacertaVenctoServidor = class(TForm)
    edtdata: TMaskEdit;
    edtverificador: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtvencimento: TMaskEdit;
    Image1: TImage;
    btok: TSpeedButton;
    lbcnpj: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btokClick(Sender: TObject);
    procedure edtverificadorKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    Function ValidaSenhaModo1(Pdigito:integer):Boolean;
  public
    { Public declarations }

  end;

var
  FacertaVenctoServidor: TFacertaVenctoServidor;

implementation

uses Uessencialglobal, UDataModulo, Useg;


{$R *.dfm}

procedure TFacertaVenctoServidor.FormShow(Sender: TObject);
begin
     edtdata.text:='';
     edtvencimento.text:='';
     edtverificador.Text:='';
     self.tag:=0;


      if (ObjParametroGlobal.ValidaParametro('C�DIGO GERADOR DE T�TULO')=False)
     Then Begin
               Messagedlg('C�digo do Gerador de T�tulo n�o encontrado para verificar Vencto!',mterror,[mbok],0);
               exit;
     End;
     lbcnpj.caption:='CNPJ: '+useg.DesincriptaSenha(ObjParametroGlobal.get_valor);

     if (ModoCalculoSenhaGlobal=0)
     Then lbcnpj.caption:=lbcnpj.caption+' - Senha somente n�meros';

     if (ModoCalculoSenhaGlobal=1)
     Then lbcnpj.caption:=lbcnpj.caption+' - Senha com Letras';



end;


function TFacertaVenctoServidor.ValidaSenhaModo1(Pdigito: integer): Boolean;
var
Ptemp,ptemp2:String;
cont,venctemp:integer;
pano,pmes,pdia:word;
begin
     result:=False;
     //o D � o digito simples ja usado anteriormente
     Ptemp:=inttostr(Pdigito);
     ptemp2:='';

     for cont:=1 to length(ptemp) do
     Begin
          case strtoint(ptemp[cont]) of

          0:Ptemp2:=ptemp2+'L';
          1:Ptemp2:=ptemp2+'K';
          2:Ptemp2:=ptemp2+'J';
          3:Ptemp2:=ptemp2+'H';
          4:Ptemp2:=ptemp2+'G';
          5:Ptemp2:=ptemp2+'F';
          6:Ptemp2:=ptemp2+'S';
          7:Ptemp2:=ptemp2+'A';
          8:Ptemp2:=ptemp2+'P';
          9:Ptemp2:=ptemp2+'N';
          End;
     End;

     venctemp:=0;
     //usando o vencimento
     for cont:=1 to length(edtvencimento.text) do
     Begin
          if (edtvencimento.text[cont] in ['0'..'9'])
          Then venctemp:=venctemp+strtoint(edtvencimento.text[cont]);
     End;

     Try
        DecodeDate(strtodate(edtdata.Text),pano,pmes,pdia);
     Except
           MensagemErro('Data Atual Inv�lida');
           exit;
     End;

     venctemp:=(venctemp*pmes);//to mes muda o multiplicador de acordo com o da  data atual
     ptemp2:=ptemp2+Inttostr(venctemp);

     Try
        DecodeDate(strtodate(edtvencimento.Text),pano,pmes,pdia);
     Except
           MensagemErro('Data de Vencimento Inv�lida');
           exit;
     End;

     ptemp:=inttostr((pano+pmes+pdia));
     
     for cont:=1 to length(ptemp) do
     Begin
          case strtoint(ptemp[cont]) of

          0:Ptemp2:=ptemp2+'L';
          1:Ptemp2:=ptemp2+'K';
          2:Ptemp2:=ptemp2+'J';
          3:Ptemp2:=ptemp2+'H';
          4:Ptemp2:=ptemp2+'G';
          5:Ptemp2:=ptemp2+'F';
          6:Ptemp2:=ptemp2+'S';
          7:Ptemp2:=ptemp2+'A';
          8:Ptemp2:=ptemp2+'P';
          9:Ptemp2:=ptemp2+'M';
          End;
     End;

    // showmessage(ptemp2);
     if (ptemp2<>UPPERCASE(edtverificador.text))
     Then exit;

     result:=True;
end;

procedure TFacertaVenctoServidor.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFacertaVenctoServidor.btokClick(Sender: TObject);
var
Str:String;
d:integer;
begin
     Try
        strtodate(edtdata.text);
        strtodate(edtvencimento.text);
     Except
           Messagedlg('Datas Inv�lidas!',mterror,[mbok],0);
           exit;
     End;
     Str:='';
     Str:=formatdatetime('ddmmyyyy',strtodate(edtdata.text))+
          formatdatetime('ddmmyyyy',strtodate(edtvencimento.text));
     //a senha do vencimento � dada em cima de um valor unico pra cada cliente
     //que esta em parametro, este valor pode ser um CNPJ ou CPF

     if (ObjParametroGlobal.ValidaParametro('C�DIGO GERADOR DE T�TULO')=False)
     Then Begin
               Messagedlg('C�digo do Gerador de T�tulo n�o encontrado para verificar Vencto!',mterror,[mbok],0);
               exit;
     End;

     Try
        strtoint64(useg.DesincriptaSenha(ObjParametroGlobal.Get_Valor));
     Except
           Messagedlg('C�digo do Cliente inv�lido!',mterror,[mbok],0);
           exit;
     End;

     str:=str+useg.DesincriptaSenha(ObjParametroGlobal.Get_Valor);

     d:=RetornaDigitoData(str);

     if (d=-1)
     Then exit;


     if (ModoCalculoSenhaGlobal=0)
     Then Begin
               if (strtoint(edtverificador.Text)<>d)
               Then Begin
                         Messagedlg('D�gito Verificador inv�lido!',mterror,[mbok],0);
                         exit;
               End;
     End
     Else Begin
               if (ModoCalculoSenhaGlobal=1)
               then Begin
                         //modo usando letras e numeros (desenvolvido para qualifica)
                         if (Self.ValidaSenhaModo1(d)=False)
                         then Begin
                                   Messagedlg('D�gito Verificador inv�lido!',mterror,[mbok],0);
                                   Exit;
                         End;
               End
               Else Begin
                         MensagemErro('Modo Inv�lido de C�lculo da Senha');
                         exit;
               End;
     End;

     
     Self.Tag:=1;
     Self.Close;
END;

procedure TFacertaVenctoServidor.edtverificadorKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then btokClick(sender);
end;

end.
