object FNOVIDADE: TFNOVIDADE
  Left = 908
  Top = 250
  Width = 581
  Height = 420
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Novidades da Vers'#227'o - EXCLAIM TECNOLOGIA LTDA'
  Color = 15458499
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 565
    Height = 37
    Align = alTop
    TabOrder = 0
    object btVarsaoAnterior: TSpeedButton
      Left = 305
      Top = 8
      Width = 23
      Height = 21
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333FF3333333333333003333333333333F77F33333333333009033
        333333333F7737F333333333009990333333333F773337FFFFFF330099999000
        00003F773333377777770099999999999990773FF33333FFFFF7330099999000
        000033773FF33777777733330099903333333333773FF7F33333333333009033
        33333333337737F3333333333333003333333333333377333333333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = btVarsaoAnteriorClick
    end
    object lbVersao: TLabel
      Left = 328
      Top = 11
      Width = 209
      Height = 16
      Alignment = taCenter
      AutoSize = False
      Caption = '5.1.0.59 - 12/03/2009'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btVersaoProxima: TSpeedButton
      Left = 537
      Top = 7
      Width = 23
      Height = 22
      Glyph.Data = {
        76010000424D7601000000000000760000002800000020000000100000000100
        04000000000000010000120B0000120B00001000000000000000000000000000
        800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        3333333333333333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333FF3333333333333003333
        3333333333773FF3333333333309003333333333337F773FF333333333099900
        33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
        99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
        33333333337F3F77333333333309003333333333337F77333333333333003333
        3333333333773333333333333333333333333333333333333333333333333333
        3333333333333333333333333333333333333333333333333333}
      NumGlyphs = 2
      OnClick = btVersaoProximaClick
    end
    object EdtCodigo: TEdit
      Left = 69
      Top = 7
      Width = 53
      Height = 19
      MaxLength = 9
      TabOrder = 0
      Visible = False
    end
    object StaticText1: TStaticText
      Left = 9
      Top = 4
      Width = 211
      Height = 32
      BevelInner = bvLowered
      Caption = 'Novidades da Vers'#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -24
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold, fsItalic]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 357
    Width = 565
    Height = 25
    Align = alBottom
    TabOrder = 1
    object CheckVisualizado: TCheckBox
      Left = 8
      Top = 5
      Width = 169
      Height = 17
      Caption = 'N'#227'o mostrar novamente'
      TabOrder = 0
      OnClick = CheckVisualizadoClick
    end
  end
  object RE_texto: TRichEdit
    Left = 0
    Top = 37
    Width = 565
    Height = 320
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Verdana'
    Font.Style = []
    Lines.Strings = (
      'RE_texto')
    ParentFont = False
    ReadOnly = True
    TabOrder = 2
  end
end
