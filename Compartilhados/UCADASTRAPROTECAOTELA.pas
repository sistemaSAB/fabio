unit UCADASTRAPROTECAOTELA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjPROTECAOTELA,
  jpeg;

type
  TFCadastraPROTECAOTELA = class(TForm)
    Guia: TTabbedNotebook;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbMensagem: TLabel;
    Lbdatahorainicial: TLabel;
    Edtdatahorainicial: TMaskEdit;     
    Lbdatahorafinal: TLabel;
    Edtdatahorafinal: TMaskEdit;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    panelconfiguracao: TPanel;
    Label1: TLabel;
    edttempo: TEdit;
    Label2: TLabel;
    edtvelocidade: TEdit;
    btgravarINI: TButton;
    btvisualiza: TButton;
    btfonte: TButton;
    FontDialog: TFontDialog;
    Label3: TLabel;
    edttop: TEdit;
    ColorDialog: TColorDialog;
    btcorfundo: TBitBtn;
    PanelCor: TPanel;
    edtmensagem: TMemo;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure btgravarINIClick(Sender: TObject);
    procedure btvisualizaClick(Sender: TObject);
    procedure edttempoKeyPress(Sender: TObject; var Key: Char);
    procedure btfonteClick(Sender: TObject);
    procedure btcorfundoClick(Sender: TObject);
  private
         ObjPROTECAOTELA:TObjPROTECAOTELA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         procedure CarregaDadosConfiguracao;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCadastraPROTECAOTELA: TFCadastraPROTECAOTELA;


implementation

uses UessencialGlobal, Upesquisa, UPROTECAOTELA;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCadastraPROTECAOTELA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjPROTECAOTELA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Mensagem(edtMensagem.text);
        Submit_datahorainicial(edtdatahorainicial.text);
        Submit_datahorafinal(edtdatahorafinal.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCadastraPROTECAOTELA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjPROTECAOTELA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtMensagem.text:=Get_Mensagem;
        Edtdatahorainicial.text:=Get_datahorainicial;
        Edtdatahorafinal.text:=Get_datahorafinal;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCadastraPROTECAOTELA.TabelaParaControles: Boolean;
begin
     If (Self.ObjPROTECAOTELA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCadastraPROTECAOTELA.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjPROTECAOTELA:=TObjPROTECAOTELA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFCadastraPROTECAOTELA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjPROTECAOTELA=Nil)
     Then exit;

If (Self.ObjPROTECAOTELA.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjPROTECAOTELA.free;
end;

procedure TFCadastraPROTECAOTELA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCadastraPROTECAOTELA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjPROTECAOTELA.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjPROTECAOTELA.status:=dsInsert;
     Guia.pageindex:=0;
     edtMensagem.setfocus;

end;


procedure TFCadastraPROTECAOTELA.btalterarClick(Sender: TObject);
begin
    If (Self.ObjPROTECAOTELA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjPROTECAOTELA.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtMensagem.setfocus;
                
          End;


end;

procedure TFCadastraPROTECAOTELA.btgravarClick(Sender: TObject);
begin

     If Self.ObjPROTECAOTELA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjPROTECAOTELA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjPROTECAOTELA.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCadastraPROTECAOTELA.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjPROTECAOTELA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjPROTECAOTELA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjPROTECAOTELA.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCadastraPROTECAOTELA.btcancelarClick(Sender: TObject);
begin
     Self.ObjPROTECAOTELA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCadastraPROTECAOTELA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCadastraPROTECAOTELA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjPROTECAOTELA.Get_pesquisa,Self.ObjPROTECAOTELA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjPROTECAOTELA.status<>dsinactive
                                  then exit;

                                  If (Self.ObjPROTECAOTELA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjPROTECAOTELA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCadastraPROTECAOTELA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCadastraPROTECAOTELA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCadastraPROTECAOTELA.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if newtab=1
     Then Begin
               Self.CarregaDadosConfiguracao;
               habilita_campos(panelconfiguracao);
     End;
end;

procedure TFCadastraPROTECAOTELA.btgravarINIClick(Sender: TObject);
begin
      With Self.ObjPROTECAOTELA do
      Begin
            TempoAtivacao     :=edttempo.Text;
            VelocidadeAnimacao:=edtvelocidade.Text;
            fonte:=FontDialog.Font;
            TopMensagem:=edttop.text;
            CorFundo:=inttostr(PanelCor.color);
            GravaINI;
            Self.CarregaDadosConfiguracao;
      End;
end;

procedure TFCadastraPROTECAOTELA.CarregaDadosConfiguracao;
begin
    Self.ObjPROTECAOTELA.CarregaINI;
    edttempo.Text:=Self.ObjPROTECAOTELA.TempoAtivacao;
    edtvelocidade.Text:=Self.ObjPROTECAOTELA.VelocidadeAnimacao;
    edttop.Text:=Self.ObjPROTECAOTELA.TopMensagem;
    FontDialog.Font:=Self.ObjPROTECAOTELA.Fonte;
    PanelCor.Color:=strtoint(Self.ObjPROTECAOTELA.CorFundo);
end;

procedure TFCadastraPROTECAOTELA.btvisualizaClick(Sender: TObject);
var
pmensagem:String;
begin
     if (edtvelocidade.text='')
     then Begin
               edtvelocidade.Text:='500';
     End;

     if (edttop.text='')
     then Begin
               edttop.Text:='200';
     End;

     FprotecaoTela.ObjprotecaoTela:=Self.ObjPROTECAOTELA;

     pmensagem:=Self.ObjprotecaoTela.RetornaMensagem(RetornaDataHoraServidor);
     
     if (pmensagem='')
     Then FprotecaoTela.lbmensagem.caption:='Mensagem de Teste  - Sistema de Prote��o de Tela  - Exclaim Tecnologia Ltda - www.exclaim.com.br'
     Else FprotecaoTela.lbmensagem.caption:=pmensagem;

     FprotecaoTela.TimerAnimacao.Interval:=strtoint(edtvelocidade.text);
     FprotecaoTela.lbmensagem.Font:=FontDialog.Font;
     FprotecaoTela.lbmensagem.Top:=strtoint(edttop.text);
     FprotecaoTela.Color:=PanelCor.Color;
     FprotecaoTela.showmodal;
end;

procedure TFCadastraPROTECAOTELA.edttempoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFCadastraPROTECAOTELA.btfonteClick(Sender: TObject);
begin
     FontDialog.Execute;
end;

procedure TFCadastraPROTECAOTELA.btcorfundoClick(Sender: TObject);
begin
     ColorDialog.Color:=PanelCor.Color;
     ColorDialog.Execute;
     PanelCor.Color:=ColorDialog.Color;
end;

end.

