unit Urelatorio;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, ExtCtrls, Qrctrls, Db;

type
  TFRelatorio = class(TForm)
    QR: TQuickRep;
    QRBand1: TQRBand;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    titulo: TQRLabel;
    ColTitulo01: TQRLabel;
    ColTitulo02: TQRLabel;
    ColTitulo03: TQRLabel;
    ColTitulo04: TQRLabel;
    ColTitulo05: TQRLabel;
    ColTitulo06: TQRLabel;
    ColTitulo07: TQRLabel;
    ColTitulo08: TQRLabel;
    Campo01: TQRDBText;
    Campo02: TQRDBText;
    Campo03: TQRDBText;
    Campo04: TQRDBText;
    Campo05: TQRDBText;
    Campo06: TQRDBText;
    Campo07: TQRDBText;
    Campo08: TQRDBText;
    QRShape1: TQRShape;
    BandaSumario: TQRBand;
    CampoExpr01: TQRExpr;
    QRShape2: TQRShape;
    CampoExpr03: TQRExpr;
    CampoExpr02: TQRExpr;
    CampoExpr04: TQRExpr;
    QRShape3: TQRShape;
    LabelSubtitulo1: TQRLabel;
    LabelSubtitulo2: TQRLabel;
    ColTitulo0101: TQRLabel;
    ColTitulo0102: TQRLabel;
    ColTitulo0103: TQRLabel;
    Campo0101: TQRDBText;
    campo0102: TQRDBText;
    campo0103: TQRDBText;
    ColTitulo0201: TQRLabel;
    campo0201: TQRDBText;
    LbCampoExpr01: TQRLabel;
    LbCampoExpr02: TQRLabel;
    LbCampoExpr03: TQRLabel;
    LbCampoExpr04: TQRLabel;
    Campo0202: TQRDBText;
    ColTitulo0202: TQRLabel;
    Campo0203: TQRDBText;
    ColTitulo0203: TQRLabel;
    campo0204: TQRDBText;
    coltitulo0204: TQRLabel;
    Campo0301: TQRDBText;
    coltitulo0301: TQRLabel;
    ColTitulo0302: TQRLabel;
    Campo0302: TQRDBText;
    campo0401: TQRDBText;
    coltitulo0401: TQRLabel;
    campo0501: TQRDBText;
    coltitulo0501: TQRLabel;
    bandatitulo: TQRBand;
    LbNomeSuperior: TQRLabel;
    QRSysData2: TQRSysData;
    procedure QRBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
        Procedure DesativaCampos;
    { Public declarations }
  end;

var
  FRelatorio: TFRelatorio;

implementation

uses UDataModulo;

{$R *.DFM}

{ TFrelPortador }

procedure TFRelatorio.DesativaCampos;
begin
     Campo01.Enabled:=False;
     campo01.mask:='';
     //estao entre o 1 e 2
     Campo0101.Enabled:=False;
     campo0101.mask:='';

     Campo0102.Enabled:=False;
     campo0102.mask:='';

     Campo0103.Enabled:=False;
     campo0103.mask:='';

     Campo02.Enabled:=False;
     campo02.mask:='';

     Campo0201.Enabled:=False;
     campo0201.mask:='';

     Campo0202.Enabled:=False;
     campo0202.mask:='';

     Campo0203.Enabled:=False;
     Campo0203.mask:='';

     Campo0204.Enabled:=False;
     Campo0204.mask:='';

     Campo03.Enabled:=False;
     Campo03.mask:='';

     Campo0301.Enabled:=False;
     Campo0301.mask:='';

     Campo0302.Enabled:=False;
     Campo0302.mask:='';


     Campo04.Enabled:=False;
     Campo04.mask:='';

     Campo0401.Enabled:=False;
     Campo0401.mask:='';

     Campo05.Enabled:=False;
     Campo05.mask:='';

     Campo0501.Enabled:=False;
     Campo0501.mask:='';

     Campo06.Enabled:=False;
     Campo06.mask:='';

     Campo07.Enabled:=False;
     Campo07.mask:='';

     Campo08.Enabled:=False;
     Campo08.mask:='';

     ColTitulo01.Enabled:=False;
     ColTitulo0101.Enabled:=False;
     ColTitulo0102.Enabled:=False;
     ColTitulo0103.Enabled:=False;
     //**********************
     ColTitulo02.Enabled:=False;
     ColTitulo0201.Enabled:=False;
     ColTitulo0202.Enabled:=False;
     ColTitulo0203.Enabled:=False;
     ColTitulo0204.Enabled:=False;
     //**************************
     ColTitulo03.Enabled:=False;
     ColTitulo0301.Enabled:=False;
     ColTitulo0302.Enabled:=False;
     //**************************

     ColTitulo04.Enabled:=False;
     ColTitulo0401.Enabled:=False;

     ColTitulo05.Enabled:=False;
     ColTitulo0501.Enabled:=False;

     ColTitulo06.Enabled:=False;
     ColTitulo07.Enabled:=False;
     ColTitulo08.Enabled:=False;

     CampoExpr01.Enabled:=False;
     CampoExpr01.mask:='';

     CampoExpr02.Enabled:=False;
     CampoExpr02.mask:='';

     CampoExpr03.Enabled:=False;
     CampoExpr03.mask:='';

     CampoExpr04.Enabled:=False;
     CampoExpr04.mask:='';

     LbCampoExpr01.Enabled:=False;
     LbCampoExpr02.Enabled:=False;
     LbCampoExpr03.Enabled:=False;
     LbCampoExpr04.Enabled:=False;

     LabelSubtitulo1.Enabled:=False;
     LabelSubtitulo2.Enabled:=False;

end;

procedure TFRelatorio.QRBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
     lbnomesuperior.width:=BANDATITULO.width-30;
     if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then LbNomeSuperior.caption:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin
               
               LbNomeSuperior.caption:=ObjParametroGlobal.get_valor;
     End;
     lbnomesuperior.caption:=lbnomesuperior.caption+'  - '+datetostr(now)+'-'+timetostr(now);
end;

end.
