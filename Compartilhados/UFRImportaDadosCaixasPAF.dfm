object FRImportaDadosCaixasPAF: TFRImportaDadosCaixasPAF
  Left = 0
  Top = 0
  Width = 844
  Height = 516
  TabOrder = 0
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 844
    Height = 41
    Align = alTop
    Color = clWhite
    TabOrder = 0
  end
  object pnl2: TPanel
    Left = 0
    Top = 475
    Width = 844
    Height = 41
    Align = alBottom
    Color = clWhite
    TabOrder = 1
  end
  object STRG1: TStringGrid
    Left = 0
    Top = 41
    Width = 844
    Height = 159
    Align = alTop
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 2
    OnDblClick = STRG1DblClick
  end
  object pnl3: TPanel
    Left = 0
    Top = 200
    Width = 844
    Height = 275
    Align = alClient
    BevelOuter = bvNone
    Color = 10643006
    TabOrder = 3
    object gBarradeProgresso: TGauge
      Left = 0
      Top = 232
      Width = 844
      Height = 43
      Align = alBottom
      BackColor = clSkyBlue
      Color = clWhite
      ForeColor = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Progress = 0
    end
    object lb1: TLabel
      Left = 358
      Top = 11
      Width = 51
      Height = 14
      Caption = 'Data Inicial'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lb2: TLabel
      Left = 536
      Top = 11
      Width = 47
      Height = 14
      Caption = 'Data Final'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btImportacao: TButton
      Left = 715
      Top = 5
      Width = 123
      Height = 24
      Caption = 'Iniciar Importa'#231#227'o'
      TabOrder = 0
      OnClick = btImportacaoClick
    end
    object edtDataInicial: TMaskEdit
      Left = 416
      Top = 8
      Width = 79
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 1
      Text = '  /  /    '
      OnKeyDown = edtDataInicialKeyDown
    end
    object edtDataFinal: TMaskEdit
      Left = 592
      Top = 8
      Width = 79
      Height = 19
      Ctl3D = False
      EditMask = '!99/99/0000;1;_'
      MaxLength = 10
      ParentCtl3D = False
      TabOrder = 2
      Text = '  /  /    '
      OnKeyDown = edtDataFinalKeyDown
    end
    object btAtualizarCaixas: TButton
      Left = 715
      Top = 37
      Width = 123
      Height = 24
      Caption = 'Atualizar Caixas'
      TabOrder = 3
      OnClick = btAtualizarCaixasClick
    end
    object edtpath: TEdit
      Left = 256
      Top = 39
      Width = 417
      Height = 19
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 4
    end
    object bt1: TButton
      Left = 674
      Top = 38
      Width = 27
      Height = 23
      Caption = '...'
      TabOrder = 5
      OnClick = bt1Click
    end
    object panelpafamanda: TPanel
      Left = 200
      Top = 72
      Width = 641
      Height = 89
      TabOrder = 6
      object btExportarVendas: TButton
        Left = 16
        Top = 16
        Width = 123
        Height = 24
        Caption = 'Exportar Vendas'
        TabOrder = 0
      end
      object Button2: TButton
        Left = 152
        Top = 16
        Width = 123
        Height = 24
        Caption = 'Atualiza Estoque'
        TabOrder = 1
        OnClick = Button2Click
      end
    end
  end
  object dlgOpen1: TOpenDialog
    Left = 432
    Top = 48
  end
end
