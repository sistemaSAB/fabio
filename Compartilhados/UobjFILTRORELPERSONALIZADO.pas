unit UobjFILTRORELPERSONALIZADO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJRELATORIOPERSONALIZADO
;
//USES_INTERFACE



Type
   TObjFILTRORELPERSONALIZADO=class

          Public

                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                relatorio:TOBJRELATORIOPERSONALIZADO;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;virtual;
                Destructor  Free;virtual;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                function    LocalizaFiltro(Pnome: String; Prelatorio: string): boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_tipo(parametro: string);
                Function Get_tipo: string;
                Procedure Submit_nome(parametro: string);
                Function Get_nome: string;
                Procedure Submit_captionlabel(parametro: string);
                Function Get_captionlabel: string;
                Procedure Submit_mascara(parametro: string);
                Function Get_mascara: string;
                Procedure Submit_requerido(parametro: string);
                Function Get_requerido: string;

                Procedure Submit_multiplaescolha(parametro: string);
                Function Get_multiplaescolha: string;

                Procedure Submit_campomultiplaescolha(parametro: string);
                Function Get_campomultiplaescolha: string;

                Procedure Submit_usamesmoembranco(parametro: string);
                Function Get_usamesmoembranco: string;

                Procedure Submit_comandosqlchaveestrangeira(parametro: string);
                Function Get_comandosqlchaveestrangeira: string;
                Procedure Submit_campochavestrangeira(parametro: string);
                Function Get_campochavestrangeira: string;
                Procedure Submit_comandoandprincipal(parametro: string);
                Function Get_comandoandprincipal: string;
                Procedure Submit_comandoandrepeticao(parametro: string);
                Function Get_comandoandrepeticao: string;
                procedure EdtrelatorioExit(Sender: TObject;labelnome:Tlabel);
                procedure EdtrelatorioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;labelnome:Tlabel);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;

               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               tipo:string;
               nome:string;
               captionlabel:string;
               mascara:string;
               requerido:string;
               multiplaescolha:string;
               campomultiplaescolha:string;
               usamesmoembranco:string;
               comandosqlchaveestrangeira:string;
               campochavestrangeira:string;
               comandoandprincipal:string;
               comandoandrepeticao:string;
//CODIFICA VARIAVEIS PRIVADAS


               ParametroPesquisa:TStringList;
               function VerificaUnico: boolean;
               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;


   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UCADASTRARELATORIOPERSONALIZADO;





Function  TObjFILTRORELPERSONALIZADO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('relatorio').asstring<>'')
        Then Begin
                 If (Self.relatorio.LocalizaCodigo(FieldByName('relatorio').asstring)=False)
                 Then Begin
                          Messagedlg('relatorio N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.relatorio.TabelaparaObjeto;
        End;
        Self.tipo:=fieldbyname('tipo').asstring;
        Self.nome:=fieldbyname('nome').asstring;
        Self.captionlabel:=fieldbyname('captionlabel').asstring;
        Self.mascara:=fieldbyname('mascara').asstring;
        Self.requerido:=fieldbyname('requerido').asstring;
        Self.multiplaescolha:=fieldbyname('multiplaescolha').asstring;
        Self.campomultiplaescolha:=fieldbyname('campomultiplaescolha').asstring;
        Self.usamesmoembranco:=fieldbyname('usamesmoembranco').asstring;
        Self.comandosqlchaveestrangeira:=fieldbyname('comandosqlchaveestrangeira').asstring;
        Self.campochavestrangeira:=fieldbyname('campochavestrangeira').asstring;
        Self.comandoandprincipal:=fieldbyname('comandoandprincipal').asstring;
        Self.comandoandrepeticao:=fieldbyname('comandoandrepeticao').asstring;
//CODIFICA TABELAPARAOBJETO











        result:=True;
     End;
end;


Procedure TObjFILTRORELPERSONALIZADO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('relatorio').asstring:=Self.relatorio.GET_CODIGO;
        ParamByName('tipo').asstring:=Self.tipo;
        ParamByName('nome').asstring:=Self.nome;
        ParamByName('captionlabel').asstring:=Self.captionlabel;
        ParamByName('mascara').asstring:=Self.mascara;
        ParamByName('requerido').asstring:=Self.requerido;
        ParamByName('multiplaescolha').asstring:=Self.multiplaescolha;
        ParamByName('campomultiplaescolha').asstring:=Self.campomultiplaescolha;
        ParamByName('usamesmoembranco').asstring:=Self.usamesmoembranco;        
        ParamByName('comandosqlchaveestrangeira').asstring:=Self.comandosqlchaveestrangeira;
        ParamByName('campochavestrangeira').asstring:=Self.campochavestrangeira;
        ParamByName('comandoandprincipal').asstring:=Self.comandoandprincipal;
        ParamByName('comandoandrepeticao').asstring:=Self.comandoandrepeticao;
//CODIFICA OBJETOPARATABELA











  End;
End;

//***********************************************************************

function TObjFILTRORELPERSONALIZADO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;

  if (Self.VerificaUnico=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFILTRORELPERSONALIZADO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        relatorio.ZerarTabela;
        tipo:='';
        nome:='';
        captionlabel:='';
        mascara:='';
        requerido:='';
        multiplaescolha:='';
        campomultiplaescolha:='';
        usamesmoembranco:='';
        comandosqlchaveestrangeira:='';
        campochavestrangeira:='';
        comandoandprincipal:='';
        comandoandrepeticao:='';
//CODIFICA ZERARTABELA











     End;
end;



Function TobjFiltroRelpersonalizado.VerificaUnico:boolean;
begin
      result:=False;

      Self.localizafiltro(Self.nome,self.relatorio.Get_CODIGO);

      With Self.Objquery do
      Begin
          if (recordcount>0)
          Then Begin
                    if (Self.CODIGO<>fieldbyname('codigo').asstring)
                    Then Begin
                              mensagemerro('J� existe um Filtro com o nome "'+Self.nome+'" no relat�rio atual, escolha outro nome');
                              exit;
                    End;
          End;
          result:=True;
     End;
End;




Function TObjFILTRORELPERSONALIZADO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';

      if (multiplaescolha='')
      Then multiplaescolha:='N';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjFILTRORELPERSONALIZADO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin

     Result:=False;
     mensagem:='';

     If (Self.relatorio.LocalizaCodigo(Self.relatorio.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ relatorio n�o Encontrado!'
     Else Begin


     End;



//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFILTRORELPERSONALIZADO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        If (Self.relatorio.Get_Codigo<>'')
        Then Strtoint(Self.relatorio.Get_Codigo);
     Except
           Mensagem:=mensagem+'/relatorio';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFILTRORELPERSONALIZADO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFILTRORELPERSONALIZADO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';


        If not( ((REQUERIDO='S') OR (REQUERIDO='N')))
        Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Requerido';

        If not( ((multiplaescolha='S') OR (multiplaescolha='N')))
        Then Mensagem:=mensagem+'/Valor Inv�lido para o campo M�ltipla Escolha'
        Else Begin
                  if (multiplaescolha='S') and (campomultiplaescolha='')
                  Then Mensagem:=Mensagem+'\O campo multipla escolha n�o pode estar vazio';
        End;


        If not( ((usamesmoembranco='S') OR (usamesmoembranco='N')))
        Then Mensagem:=mensagem+'/Valor Inv�lido para o campo "Usa mesmo em branco"';

        if ((Self.tipo<>'INTEIRO') and (Self.tipo<>'DECIMAL') and (Self.tipo<>'DATA') and (Self.tipo<>'HORA') and (Self.tipo<>'DATA E HORA') and (Self.tipo<>'STRING'))
        Then Mensagem:=Mensagem+'/Valor Inv�lido para o Campo Tipo';

//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
                  Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                  exit;
        End;
        result:=true;
  End;
  
Finally

end;

end;

function TObjFILTRORELPERSONALIZADO.LocalizaCodigo(parametro: string): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FILTRORELPERSONALIZADO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,relatorio,tipo,nome,captionlabel,mascara,requerido,multiplaescolha,campomultiplaescolha,usamesmoembranco,comandosqlchaveestrangeira');
           SQL.ADD(' ,campochavestrangeira,comandoandprincipal,comandoandrepeticao');
           SQL.ADD(' ');
           SQL.ADD(' from  TabFiltroRelpersonalizado');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


function TObjFILTRORELPERSONALIZADO.LocalizaFiltro(Pnome:String;Prelatorio:string): boolean;//ok
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,relatorio,tipo,nome,captionlabel,mascara,requerido,multiplaescolha,campomultiplaescolha,usamesmoembranco,comandosqlchaveestrangeira');
           SQL.ADD(' ,campochavestrangeira,comandoandprincipal,comandoandrepeticao');
           SQL.ADD(' ');
           SQL.ADD(' from  TabFiltroRelpersonalizado');
           SQL.ADD(' WHERE relatorio='+prelatorio+' and upper(nome)='+#39+uppercase(pnome)+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFILTRORELPERSONALIZADO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFILTRORELPERSONALIZADO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFILTRORELPERSONALIZADO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;



        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.relatorio:=TOBJRELATORIOPERSONALIZADO.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabFiltroRelpersonalizado(CODIGO,relatorio');
                InsertSQL.add(' ,tipo,nome,captionlabel,mascara,requerido,multiplaescolha,campomultiplaescolha,usamesmoembranco,comandosqlchaveestrangeira');
                InsertSQL.add(' ,campochavestrangeira,comandoandprincipal,comandoandrepeticao');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:relatorio,:tipo,:nome,:captionlabel,:mascara');
                InsertSQL.add(' ,:requerido,:multiplaescolha,:campomultiplaescolha,:usamesmoembranco,:comandosqlchaveestrangeira,:campochavestrangeira');
                InsertSQL.add(' ,:comandoandprincipal,:comandoandrepeticao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabFiltroRelpersonalizado set CODIGO=:CODIGO');
                ModifySQL.add(',relatorio=:relatorio,tipo=:tipo,nome=:nome,captionlabel=:captionlabel');
                ModifySQL.add(',mascara=:mascara,requerido=:requerido,multiplaescolha=:multiplaescolha,campomultiplaescolha=:campomultiplaescolha,usamesmoembranco=:usamesmoembranco,comandosqlchaveestrangeira=:comandosqlchaveestrangeira');
                ModifySQL.add(',campochavestrangeira=:campochavestrangeira,comandoandprincipal=:comandoandprincipal');
                ModifySQL.add(',comandoandrepeticao=:comandoandrepeticao');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabFiltroRelpersonalizado where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFILTRORELPERSONALIZADO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFILTRORELPERSONALIZADO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFILTRORELPERSONALIZADO');
     Result:=Self.ParametroPesquisa;
end;

function TObjFILTRORELPERSONALIZADO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FILTRORELPERSONALIZADO ';
end;


function TObjFILTRORELPERSONALIZADO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFILTRORELPERSONALIZADO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFILTRORELPERSONALIZADO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFILTRORELPERSONALIZADO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.relatorio.FREE;


//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFILTRORELPERSONALIZADO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para captionlabels.
function TObjFILTRORELPERSONALIZADO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFiltroRelpersonalizado.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjFiltroRelpersonalizado.Submit_tipo(parametro: string);
begin
        Self.tipo:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_tipo: string;
begin
        Result:=Self.tipo;
end;
procedure TObjFiltroRelpersonalizado.Submit_nome(parametro: string);
begin
        Self.nome:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_nome: string;
begin
        Result:=Self.nome;
end;
procedure TObjFiltroRelpersonalizado.Submit_captionlabel(parametro: string);
begin
        Self.captionlabel:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_captionlabel: string;
begin
        Result:=Self.captionlabel;
end;
procedure TObjFiltroRelpersonalizado.Submit_mascara(parametro: string);
begin
        Self.mascara:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_mascara: string;
begin
        Result:=Self.mascara;
end;
procedure TObjFiltroRelpersonalizado.Submit_requerido(parametro: string);
begin
        Self.requerido:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_requerido: string;
begin
        Result:=Self.requerido;
end;

procedure TObjFiltroRelpersonalizado.Submit_multiplaescolha(parametro: string);
begin
        Self.multiplaescolha:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_multiplaescolha: string;
begin
        Result:=Self.multiplaescolha;
        
end;



procedure TObjFiltroRelpersonalizado.Submit_campomultiplaescolha(parametro: string);
begin
        Self.campomultiplaescolha:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_campomultiplaescolha: string;
begin
        Result:=Self.campomultiplaescolha;

end;


procedure TObjFiltroRelpersonalizado.Submit_usamesmoembranco(parametro: string);
begin
        Self.usamesmoembranco:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_usamesmoembranco: string;
begin
        Result:=Self.usamesmoembranco;
end;


procedure TObjFiltroRelpersonalizado.Submit_comandosqlchaveestrangeira(parametro: STRing);
begin
        Self.comandosqlchaveestrangeira:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_comandosqlchaveestrangeira: STRing;
begin
        Result:=Self.comandosqlchaveestrangeira;
end;
procedure TObjFiltroRelpersonalizado.Submit_campochavestrangeira(parametro: string);
begin
        Self.campochavestrangeira:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_campochavestrangeira: string;
begin
        Result:=Self.campochavestrangeira;
end;
procedure TObjFiltroRelpersonalizado.Submit_comandoandprincipal(parametro: string);
begin
        Self.comandoandprincipal:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_comandoandprincipal: string;
begin
        Result:=Self.comandoandprincipal;
end;
procedure TObjFiltroRelpersonalizado.Submit_comandoandrepeticao(parametro: string);
begin
        Self.comandoandrepeticao:=Parametro;
end;
function TObjFiltroRelpersonalizado.Get_comandoandrepeticao: string;
begin
        Result:=Self.comandoandrepeticao;
end;
//CODIFICA GETSESUBMITS


procedure TObjFILTRORELPERSONALIZADO.EdtrelatorioExit(Sender: TObject;labelnome:Tlabel);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.relatorio.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.relatorio.tabelaparaobjeto;
     labelnome.CAPTION:=Self.relatorio.GET_NOME;
End;
procedure TObjFILTRORELPERSONALIZADO.EdtrelatorioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;labelnome:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FRELATORIOPERSONALIZADO:TFCadastraRELatorioPERSONALIZADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FRELATORIOPERSONALIZADO:=TFCadastraRELATORIOPERSONALIZADO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.relatorio.Get_Pesquisa,Self.relatorio.Get_TituloPesquisa,FRELATORIOPERSONALIZADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.relatorio.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (labelnome<>NIl)) 
                                 Then Begin
                                        If Self.relatorio.RETORNACAMPONOME<>'' 
                                        Then labelnome.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.relatorio.RETORNACAMPONOME).asstring
                                        Else labelnome.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FRELATORIOPERSONALIZADO);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjFILTRORELPERSONALIZADO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFILTRORELPERSONALIZADO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



end.



