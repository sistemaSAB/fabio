unit UinputQuery;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFinputQuery = class(TForm)
    Label1: TLabel;
    lbfrase: TLabel;
    edttexto: TEdit;
    btok: TButton;
    btcancelar: TButton;
    procedure edttextoKeyPress(Sender: TObject; var Key: Char);
    procedure btokClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
        function MeuInputQuery(CaptionSuperior, Frase: String; var ptexto:string;QuantidadeCaracteres:integer): Boolean;
  end;

var
  FinputQuery: TFinputQuery;

implementation

{$R *.dfm}

function TFInputQuery.MeuInputQuery(CaptionSuperior, Frase: String;var ptexto:string;QuantidadeCaracteres:integer): Boolean;
begin
     Self.caption:=CaptionSuperior;
     lbfrase.caption:=frase;
     edttexto.text:=ptexto;
     edttexto.maxlength:=quantidadecaracteres;
     Self.ShowModal;
     ptexto:=Self.edttexto.text;
     if (Self.Tag=1)
     Then result:=True
     Else result:=False;
     
end;


procedure TFinputQuery.edttextoKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then btokclick(sender);

     if key=#27
     Then Self.close;


end;

procedure TFinputQuery.btokClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.close;
end;

procedure TFinputQuery.btcancelarClick(Sender: TObject);
begin
     Self.Tag:=0;
     Self.close;
end;

procedure TFinputQuery.FormShow(Sender: TObject);
begin
     Self.Tag:=0;
end;

end.
