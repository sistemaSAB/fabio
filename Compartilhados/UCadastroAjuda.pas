unit UCadastroAjuda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,udatamodulo,
  Dialogs, ExtCtrls, StdCtrls, Buttons, UessencialGlobal,UobjAJUDA, Upesquisa,DB,IBQuery,
  ComCtrls, Grids, Menus;

type
  TFCadastroAjuda = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    ImgRodape: TImage;
    pnl3: TPanel;
    lb1: TLabel;
    edtNomeMenu: TEdit;
    lb2: TLabel;
    edtligadomenu: TEdit;
    lbmenuligado: TLabel;
    dlgFontFonte: TFontDialog;
    ColorDialog1: TColorDialog;
    pgc1: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    pnl2: TPanel;
    btfonte: TSpeedButton;
    btcor: TSpeedButton;
    btAlinhaEsquerda: TBitBtn;
    btCentralizar: TBitBtn;
    btAlinhaDireita: TBitBtn;
    btitalico: TBitBtn;
    btsublinhado: TBitBtn;
    btnegrito: TBitBtn;
    mmoAjuda: TRichEdit;
    STRGLinkVideo: TStringGrid;
    PopUpOp: TPopupMenu;
    AtulizarLinks1: TMenuItem;
    estarLink1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btsalvarClick(Sender: TObject);
    procedure edtligadomenuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btexcluirClick(Sender: TObject);
    procedure btNegritoClick(Sender: TObject);
    procedure btItalicoClick(Sender: TObject);
    procedure btSublinhadoClick(Sender: TObject);
    procedure btAlinhaEsquerdaClick(Sender: TObject);
    procedure btCentralizarClick(Sender: TObject);
    procedure btAlinhaDireitaClick(Sender: TObject);
    procedure btfonteClick(Sender: TObject);
    procedure btcorClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btopcoesClick(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure AtulizarLinks1Click(Sender: TObject);
    procedure STRGLinkVideoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
      ObjAjuda:TObjAJUDA;
      function TabelaParaControles  : Boolean;
      Function  ObjetoParaControles : Boolean;
      Function  ControlesParaObjeto:Boolean;
      procedure LimpaLabels;
      function TextoSelecionado: TTextAttributes;
      procedure MontaStringGrid;
      procedure Carregalinks;
  //    procedure AtualizaBotoes;

  public

  end;

var
  FCadastroAjuda: TFCadastroAjuda;

implementation

uses UescolheImagemBotao, UpesquisaMenu, UAjuda, Uprincipal;

{$R *.dfm}

procedure TFCadastroAjuda.FormShow(Sender: TObject);
begin
         ObjAjuda:=TObjAJUDA.Create;

         FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
         FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
         desabilita_campos(Self);
         LimpaLabels;
         MontaStringGrid;
         pgc1.TabIndex:=0;
end;


procedure TFCadastroAjuda.MontaStringGrid;
begin
   with STRGLinkVideo do
   begin
       RowCount:=2;
       ColCount:=3;
       ColWidths[0]:=400;
       ColWidths[1]:=500;
       ColWidths[2]:=300;
       Cells[0,0]:='Link P�gina';
       Cells[1,0]:='Link V�deo';
       Cells[2,0]:='Nome do V�deo';
       Cells[0,1]:='';
       Cells[1,1]:='';
       Cells[2,1]:='';
   end;

end;

procedure TFCadastroAjuda.btsairClick(Sender: TObject);
begin
     self.Close;
end;

procedure TFCadastroAjuda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     ObjAjuda.Free;

end;

procedure TFCadastroAjuda.btnovoClick(Sender: TObject);
begin
        limpaedit(Self);
        btnovo.Visible:=False;
        btalterar.Visible:=False;
        btrelatorio.Visible:=False;
        btsair.Visible:=False;
        btopcoes.Visible:=False;
        btexcluir.Visible:=False;
        btpesquisar.Visible:=False;
        habilita_campos(Self);
        ObjAjuda.Status:=dsInsert;
        lbCodigo.Caption:='0';

end;

procedure TFCadastroAjuda.btalterarClick(Sender: TObject);
begin
        if(lbCodigo.Caption='')
        then Exit;
       // mmoAjuda.Lines.LoadFromFile('D:\nome.txt');

        btnovo.Visible:=False;
        btalterar.Visible:=False;
        btrelatorio.Visible:=False;
        btsair.Visible:=False;
        btopcoes.Visible:=False;
        btexcluir.Visible:=False;
        btpesquisar.Visible:=False;
        habilita_campos(Self);
        ObjAjuda.Status:=dsEdit;
end;

procedure TFCadastroAjuda.btcancelarClick(Sender: TObject);
begin
        limpaedit(Self);
        limpalabels;

        btnovo.Visible:=True;
        btalterar.Visible:=True;
        btrelatorio.Visible:=True;
        btsair.Visible:=True;
        btopcoes.Visible:=True;
        btexcluir.Visible:=True;
        btpesquisar.Visible:=True;
        desabilita_campos(Self);
        ObjAjuda.Status:=dsInactive;
end;

procedure TFCadastroAjuda.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin
        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjAjuda.Get_pesquisa,ObjAjuda.Get_TituloPesquisa,Nil)=True) Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok) Then
                        Begin
                                  If ObjAjuda.status<>dsinactive
                                  then exit;

                                  If (ObjAjuda.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False) Then
                                  Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;
                                  ObjAjuda.ZERARTABELA;
                                  If (TabelaParaControles=False) Then
                                  Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                  End;
                                  desabilita_campos(self);

                        End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
            End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

function TFCadastroAjuda.TabelaParaControles: Boolean;
begin
     If (ObjAjuda.TabelaparaObjeto=False) Then
     Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False) Then
     Begin
                result:=False;
                exit;
     End;
     Result:=True;

end;

function TFCadastroAjuda.ObjetoParaControles:Boolean;
var
  Query: TIBQuery;
begin
      Query:=TIBQuery.Create(nil);
      Query.Database:= FDataModulo.IBDatabase;
      try


          try
               with ObjAjuda do
               begin
                    edtNomeMenu.Text:=Get_NOMEMENU;
                    edtligadomenu.Text:=Get_LIGADOMENU;
                    //mmoajuda.Text:=
                    Get_AJUDA(mmoajuda);
                    lbCodigo.Caption:=Get_CODIGO;

                    if(Get_LIGADOMENU<>'') then
                    begin
                        Query.close;
                        Query.SQL.Clear;
                        Query.SQL.Add('select nomemenu from tabajuda where codigo='+Get_LIGADOMENU);
                        Query.Open;

                        lbmenuligado.caption:=Query.fieldbyname('nomemenu').asstring;
                    end;


               end;
               result:=True;
          except
              result:=False;
          end;
      finally
            FreeAndNil(Query);
      end;


end ;


procedure TFCadastroAjuda.btsalvarClick(Sender: TObject);
begin
       If ObjAjuda.Status=dsInactive
       Then exit;

       If ControlesParaObjeto=False
       Then Begin
                 Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
                 exit;
            End;

       If (ObjAjuda.salvar(true)=False)
       Then exit;
       lbCodigo.Caption:=ObjAjuda.Get_CODIGO;

       ObjAjuda.LocalizaCodigo(lbCodigo.Caption) ;
       ObjAjuda.TabelaparaObjeto;
       Self.TabelaParaControles;

       btnovo.Visible :=true;
       btalterar.Visible:=true;
       btpesquisar.Visible:=true;
       btrelatorio.Visible:=true;
       btexcluir.Visible:=true;
       btsair.Visible:=true;
       btopcoes.Visible :=true;
       desabilita_campos(self);



end;

function TFCadastroAjuda.ControlesParaObjeto:Boolean;
begin
    try
        with ObjAjuda do
        begin
                Submit_CODIGO(lbCodigo.Caption);
                Submit_NOMEMENU(edtNomeMenu.Text);
                Submit_LIGADOMENU(edtligadomenu.Text);

                Submit_AJUDA(mmoajuda);

        end;

    except
         Result:=False;
    end;

end;

procedure TFCadastroAjuda.LimpaLabels;
begin
    lbCodigo.Caption:='';
    lbmenuligado.Caption:='';
end;

procedure TFCadastroAjuda.edtligadomenuKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            If (FpesquisaLocal.PreparaPesquisa('select * from tabajuda where ligadomenu is null','Pesquisa de menus',nil)=True)Then
            Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)Then
                        Begin
                            TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                            lbmenuligado.Caption:=FpesquisaLocal.querypesq.fieldbyname('nomemenu').AsString;
                        End;
                      Finally
                            FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TFCadastroAjuda.btexcluirClick(Sender: TObject);
begin
     If (ObjAjuda.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (ObjAjuda.LocalizaCodigo(lbCodigo.Caption)=False)Then
     Begin
           Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
           exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjAjuda.exclui(lbCodigo.Caption,True)=False)Then
     Begin
           Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
           exit;
     End;
     limpaedit(Self);
     limpalabels;
end;

function TFCadastroAjuda.TextoSelecionado:TTextAttributes;
begin

  if mmoAjuda.SelLength > 0 then Result := mmoAjuda.SelAttributes

  else Result := mmoAjuda.DefAttributes;

end;

procedure TFCadastroAjuda.btNegritoClick(Sender: TObject);
begin
      if (TextoSelecionado.Style = TextoSelecionado.Style + [fsBold])
      then TextoSelecionado.Style := TextoSelecionado.Style - [fsBold]
      else TextoSelecionado.Style := TextoSelecionado.Style + [fsBold];
end;

procedure TFCadastroAjuda.btItalicoClick(Sender: TObject);
begin
       if (TextoSelecionado.Style = TextoSelecionado.Style - [fsItalic])
       then TextoSelecionado.Style := TextoSelecionado.Style + [fsItalic]
       else TextoSelecionado.Style := TextoSelecionado.Style - [fsItalic];
end;

procedure TFCadastroAjuda.btSublinhadoClick(Sender: TObject);
begin
     if (TextoSelecionado.Style = TextoSelecionado.Style - [fsUnderline])
     then TextoSelecionado.Style := TextoSelecionado.Style + [fsUnderline]
     else TextoSelecionado.Style := TextoSelecionado.Style - [fsUnderline];
end;

procedure TFCadastroAjuda.btAlinhaEsquerdaClick(Sender: TObject);
begin
     mmoAjuda.Paragraph.Alignment := TAlignment(0);
end;

procedure TFCadastroAjuda.btCentralizarClick(Sender: TObject);
begin
    mmoAjuda.Paragraph.Alignment := TAlignment(2);
end;

procedure TFCadastroAjuda.btAlinhaDireitaClick(Sender: TObject);
begin
    mmoAjuda.Paragraph.Alignment := TAlignment(1);
end;

procedure TFCadastroAjuda.btfonteClick(Sender: TObject);
begin
    if dlgFontFonte.Execute then
     begin
        mmoAjuda.Font:=dlgFontFonte.Font;
     
     end;
end;

procedure TFCadastroAjuda.btcorClick(Sender: TObject);
begin
     if(ColorDialog1.Execute) then
     begin
         TextoSelecionado.Color:=ColorDialog1.Color;
     end;
end;

procedure TFCadastroAjuda.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
        if (Key = VK_F1) then
        begin

          try
            formPesquisaMenu:=TfPesquisaMenu.Create(nil);
            formPesquisaMenu.submit_formulario(Fprincipal);
            formPesquisaMenu.ShowModal;

          finally
            FreeAndNil(formPesquisaMenu);
          end;

        end;

        if (Key = VK_F2) then
        begin

             FAjuda.PassaAjuda('');
             FAjuda.ShowModal;
        end;
end;

procedure TFCadastroAjuda.btopcoesClick(Sender: TObject);
begin
     ObjAjuda.BotaoOpcoes(lbCodigo.Caption);
end;

procedure TFCadastroAjuda.pgc1Change(Sender: TObject);
begin
   if(pgc1.TabIndex=1)then
   begin
       if(edtligadomenu.Text<>'')then
       begin
          pgc1.TabIndex:=0;
          Exit;
       end;
       MontaStringGrid;
       Carregalinks;
       STRGLinkVideo.SetFocus;
   end;
end;

procedure TFCadastroAjuda.AtulizarLinks1Click(Sender: TObject);
var
  Query:TIBQuery;
  i:integer;
begin
   Query:=TIBQuery.Create(nil);
   Query.Database:=FDataModulo.IBDatabase;

   try
     with Query do
     begin
       Close;
       SQL.Clear;
       SQL.Add('delete from tablinksvideos');
       sql.Add('where ajuda='+lbCodigo.Caption);
       ExecSQL;
       FDataModulo.IBTransaction.CommitRetaining;      

       for i:=1 to STRGLinkVideo.RowCount-1 do
       begin
         Close;
         SQL.Clear;
         SQL.Add('INSERT INTO TABLINKSVIDEOS');
         SQL.Add('(codigo,LINKPAGINA,LINKVIDEO,AJUDA,nomevideo)VALUES(0'+','+#39+STRGLinkVideo.Cells[0,i]+#39+','+#39+STRGLinkVideo.Cells[1,i]+#39+','+lbcodigo.caption+','+#39+STRGLinkVideo.Cells[2,i]+#39+')');
         ExecSQL;

       end;

       FDataModulo.IBTransaction.CommitRetaining;
       
     end;
   finally
     FreeAndNil(Query);
   end;


end;

procedure TFCadastroAjuda.Carregalinks;
var
  query:TIBQuery;
begin
  query:=TIBQuery.Create(nil);
  query.Database:=FDataModulo.IBDatabase;  
  try
    Query.Close;
    Query.SQL.Clear;
    query.SQL.Add('select * from tablinksvideos where ajuda='+lbCodigo.Caption);
    query.Open;

    while not query.Eof do
    begin
        STRGLinkVideo.Cells[0,STRGLinkVideo.RowCount-1]:=query.fieldbyname('linkpagina').AsString;
        STRGLinkVideo.Cells[1,STRGLinkVideo.RowCount-1]:=query.fieldbyname('linkvideo').AsString;
        STRGLinkVideo.Cells[2,STRGLinkVideo.RowCount-1]:=query.fieldbyname('nomevideo').AsString;
        STRGLinkVideo.RowCount:=STRGLinkVideo.RowCount+1;
        query.Next;
    end;
    if(STRGLinkVideo.RowCount>2)
    then STRGLinkVideo.RowCount:=STRGLinkVideo.RowCount-1;
  finally
    FreeAndNil(query);

  end;

end;

procedure TFCadastroAjuda.STRGLinkVideoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   if(key=40) then
   begin
       //Caso coluna seja = 11, ou seja esteja na ultima coluna
      if(STRGLinkVideo.Row = STRGLinkVideo.RowCount-1 )then
      begin
          //Se a primeira coluna da linha estiver vazia, logo, n�o
          if(STRGLinkVideo.Cells[0,STRGLinkVideo.RowCount-1]='') then
          begin
                 STRGLinkVideo.col:=0;
                 Exit;
          end;

          STRGLinkVideo.RowCount:=STRGLinkVideo.RowCount+1;
          STRGLinkVideo.Row:=STRGLinkVideo.RowCount-1;
          STRGLinkVideo.col:=0;
      end
   end;

   if(Key=38) then
   begin
      if(STRGLinkVideo.Cells[0,STRGLinkVideo.RowCount-1]='') and (STRGLinkVideo.Row>1) then
      begin
          STRGLinkVideo.RowCount:=STRGLinkVideo.RowCount-1;
          STRGLinkVideo.col:=0;
      end;
   end;
end;

end.
