object FtestaConfiguracaoVisual: TFtestaConfiguracaoVisual
  Left = 520
  Top = 243
  Width = 545
  Height = 447
  Caption = 'Teste de Configura'#231#227'o Visual de Tela'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 48
    Top = 8
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Shape1: TShape
    Left = 328
    Top = 192
    Width = 105
    Height = 97
  end
  object Edit1: TEdit
    Left = 48
    Top = 32
    Width = 201
    Height = 21
    TabOrder = 0
    Text = 'Edit1'
    OnKeyDown = Edit1KeyDown
  end
  object Memo1: TMemo
    Left = 48
    Top = 192
    Width = 201
    Height = 89
    Lines.Strings = (
      'Memo1'
      'Teste de On Exit')
    TabOrder = 1
    OnExit = Memo1Exit
  end
  object CheckBox1: TCheckBox
    Left = 336
    Top = 112
    Width = 97
    Height = 17
    Caption = 'CheckBox1'
    TabOrder = 2
    OnClick = CheckBox1Click
  end
  object RadioButton1: TRadioButton
    Left = 336
    Top = 136
    Width = 113
    Height = 17
    Caption = 'RadioButton1'
    TabOrder = 3
  end
  object ComboBox1: TComboBox
    Left = 328
    Top = 160
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 4
    Text = 'Teste de On Click'
    Items.Strings = (
      'linha 1'
      'linha 2'
      'linha 3')
  end
  object MaskEdit1: TMaskEdit
    Left = 48
    Top = 80
    Width = 120
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object ListBox1: TListBox
    Left = 48
    Top = 288
    Width = 201
    Height = 113
    ItemHeight = 13
    Items.Strings = (
      'linha 1'
      'linha 2'
      'Teste de On Enter')
    TabOrder = 6
    OnEnter = ListBox1Enter
  end
  object RadioGroup1: TRadioGroup
    Left = 48
    Top = 112
    Width = 201
    Height = 73
    Caption = 'RadioGroup1'
    DragMode = dmAutomatic
    Items.Strings = (
      'linha 1'
      'linha 2')
    TabOrder = 7
  end
  object RichEdit1: TRichEdit
    Left = 328
    Top = 16
    Width = 185
    Height = 89
    Lines.Strings = (
      'RichEdit1'
      'Teste de Mouse Down')
    TabOrder = 8
    OnMouseDown = RichEdit1MouseDown
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 401
    Width = 513
    Height = 19
    Panels = <>
  end
end
