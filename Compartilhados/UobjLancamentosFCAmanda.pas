unit UobjLancamentosFCAmanda;

interface

Uses
    IBquery,UDataModulo,Classes,StdCtrls,DB,UObjValores,Dialogs,Windows,Controls,UobjLancamentosFC;

type
    TobjLancamentosFCAmanda = class(TobjLancamentosFC)
      public

            function VerificaDados(pVenda:string):Boolean;override;
            function VerificaRelacionamentoFormasPagamento(pVenda:string):Boolean;override;
            function VerificaRelacionamentoFormasPagamento2(idVenda, idOrdemServico, idOrdemServidoGeral :Variant):Boolean;
            function VerificaLancamentoPendencias(pVenda,pPendencia:String; SomenteAVista:boolean):integer;override;
            function VerificaCorrespondenciaValores(pVenda,pPendencia:string; SomenteAVista:boolean):Integer;override;

            procedure ProcessaRecebimentos(pVenda,pData1,pData2:string);override;
            function EfetuaLancamentos(pVenda,pPendencia:string; SomenteAVista:Boolean):Boolean;override;
            function EfetuaLancamentos2(idVenda, idOrdemServico, idOrdemServicoGeral :Variant; pPendencia :String; SomenteAVista:Boolean):Boolean;


            procedure EdtVENDAKeyDown(Sender: TObject; var Key: Word;
              Shift: TShiftState; LABELNOME: Tlabel);override;

      private

    end;


implementation

uses SysUtils, UessencialGlobal,ULancamentosFC, Upesquisa, Variants;

var
  Lancamentos:TFLancamentosFrentedeCaixa;

{ TobjLancamentosFC }

function TobjLancamentosFCAmanda.VerificaDados(pVenda: string): Boolean;
begin
      Result := True;
      with ObjQuery do
      begin
            Close;
            SQL.Clear;
            SQL.Add('SELECT CODIGO,NOME,DINHEIRO_CHEQUE,APRAZO,PORTADOR');
            SQL.Add('FROM TABFORMASPAGAMENTO_FC');
            SQL.Add('WHERE CODIGO=-100');
            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('Erro na verifica��o dos campos da tabela: TABFORMASPAGAMENTO_FC'+#13+'Erro: '+e.Message+#13+' SQL = '+SQL.Text);
                        Result := false;
                  end;
            end;
      end;

      with ObjQuery do
      begin
            Close;
            SQL.Clear;
            SQL.Add('SELECT CODIGO,VENDA,FORMAPAGAMENTO,DESCRICAO,VALOR,VENCIMENTO,BANCO');
            SQL.Add(',AGENCIA,CONTA,SERIE,NUMCHEQUE,CLIENTE,CPFCLIENTE,PROCESSADO');
            SQL.Add(',CANCELADO,RECEBIMENTOAVULSO,DESCRICAORECAVULSO,IDTRANSACAO');
            SQL.Add(',VALORTRANSACAO,NOMEREDE,NSU,AUTORIZACAO,DATA,HORA,TIPOTRANSACAO');
            SQL.Add(',TIPOPARCELAMENTO,QUANTIDADEPARCELAS,DATAVENCIMENTOPARCELAS ');
            SQL.Add(',VALORPARCELAS,CANCELAMENTO_PROCESSADO');
            SQL.Add('FROM TABRECVENDA_IMPORTADO');
            if(Trim(pvenda)='')
            then SQL.Add('WHERE CODIGO=-100')
            else SQL.Add('WHERE VENDA='+pVenda);
            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('Erro na verifica��o dos campos da tabela: TABRECVENDA_IMPORTADO'+#13+'Erro: '+e.Message+#13+' SQL = '+SQL.Text);
                        Result := false;
                  end;
            end;
      end;


end;

function TobjLancamentosFCAmanda.VerificaRelacionamentoFormasPagamento(
  pVenda: string): Boolean;
begin
      result := false;
      with objQueryAuxiliar do
      begin
            //Resgatando os pagamentos realizados na venda
            Close;
            SQL.Clear;
            SQL.Add('SELECT CODIGO,FORMAPAGAMENTO,DESCRICAO,REFERENCIA');
            SQL.Add('FROM TABRECVENDA_IMPORTADO');
            SQL.Add('WHERE VENDA='+pVenda);
            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('Erro ao resgatar valores da tabela: TABRECVENDA_IMPORTADO'+#13+'Erro: '+e.Message);
                        Exit;
                  end;
            end;

            Last;
            if(recordcount > 0)
            then begin
                  First;
                  while not (Eof) do
                  begin
                        //Verificando os relacionamentos entre os prazos e os recebimentos
                        ObjQueryTEMP.Close;
                        ObjQueryTEMP.SQL.Clear;
                        ObjQueryTEMP.SQL.Add('SELECT *');
                        ObjQueryTEMP.SQL.Add('FROM TABFORMASPAGAMENTO_FC');
                        ObjQueryTEMP.SQL.Add('WHERE CODIGO='+fieldbyname('FORMAPAGAMENTO').AsString);
                        try
                              ObjQueryTEMP.Open;
                        except
                              on e:Exception do
                              begin
                                    LogAdd('Erro ao Validar o relacionamento entre as formas de pagamento do Frente de Caixa e os Prazos.'+#13+'Erro: '+e.Message);
                                    Exit;
                              end;
                        end;
                        ObjQueryTEMP.Last;
                        if(ObjQueryTEMP.RecordCount = 0)
                        then begin
                              LogAdd('Sem relacionamento para a forma de pagamento: '+fieldbyname('FORMAPAGAMENTO').AsString+' - '+fieldbyname('DESCRICAO').AsString+'. Venda: '+pVenda+#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString);
                              Exit;
                        end;
                        Next;
                  end;

            end
            else begin
                  LogAdd('N�o foram encontrados recebimentos a serem processados para a Venda: '+pVenda+#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString );
                  Exit;
            end;
      end;
      result := True;
end;

function TobjLancamentosFCAmanda.VerificaLancamentoPendencias(
  pVenda,pPendencia: String; SomenteAVista:boolean): integer;
begin
      result := 0;
      with objQueryAuxiliar do
      begin
            //Verificando se houve alguma altera��o nas pendencias
            Close;
            SQL.Clear;
            SQL.Add('SELECT SUM(TP.VALOR) AS VALOR,SUM(TP.SALDO) AS SALDO FROM TABPENDENCIA TP');
            SQL.Add('JOIN TABTITULO TT ON TT.CODIGO=TP.TITULO');
            SQL.Add('JOIN TABVENDAS TV ON TV.TITULO=TT.CODIGO');
            SQL.Add('WHERE TV.CODIGO='+pVenda);

            if(Trim(pPendencia)<>'')
            then SQL.Add('AND TP.CODIGO='+pPendencia);

            if(SomenteAVista)
            then SQL.Add('AND TV.DATAEMISSAO=TP.VENCIMENTO');


            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('Erro ao Verificar lan�amentos na Venda: '+pvenda+#13+'Erro: '+e.Message);
                        Result := -1;
                        Exit;
                  end;
            end;

            Last;
            if(recordcount > 0)
            then begin
                  First;
                  if(FieldByName('VALOR').AsCurrency <> FieldByName('SALDO').AsCurrency)
                  then result := 1;
            end;
      end;
end;

procedure TobjLancamentosFCAmanda.ProcessaRecebimentos(pVenda,pData1,pData2:string);
begin
  //Verificando as tabelas e os campos para ver se existem
  if not(VerificaDados('')) then
  begin
    MensagemErro('Ocorreram erros, o processamento n�o poder� continuar.'+#13+'Entre em contato com nosso suporte');
    Exit;
  end;

  with ObjQuery do
  begin
    Close;
    SQL.Clear;
    SQL.Text := ' SELECT distinct VENDA, ORDEMSERVICO, ORDEMSERVICOGERAL '+
                ' FROM TABRECVENDA_IMPORTADO '+
                ' WHERE PROCESSADO = ' + QuotedStr('N');

    try
      Open;
    except
      on e:Exception do
      begin
        MensagemErro('Erro ao selecionar vendas' + e.Message);
        Exit;
      end;
    end;
    if IsEmpty then
    begin
      MensagemAviso('N�o existem registros a serem processados');
      Exit;
    end;
    First;

    while not(Eof) do
    begin
      if VerificaRelacionamentoFormasPagamento2(Fields[0].AsVariant, Fields[1].AsVariant, Fields[2].AsVariant) then
      begin
        Self.EfetuaLancamentos2(Fields[0].AsVariant, Fields[1].AsVariant, Fields[2].AsVariant,'',True);
      end;
      Next;
    end;
  end;
end;

function TobjLancamentosFCAmanda.EfetuaLancamentos(pVenda,pPendencia:string; SomenteAVista:Boolean): Boolean;
var
      ValorAReceber,ValorRecebido,SomaTroco:Currency;
      Plancamentoavista,Titulo,Cupom,Referencia,Data,fraseerro,Pcodigocheque:string;
begin
      result := False;
      ValorAReceber := 0;
      ValorRecebido := 0;
      Titulo := '';
      Cupom := '';
      Data := '';
      Referencia := '';
      with objQueryAuxiliar do
      begin
            //Verificando se houve alguma altera��o nas pendencias
            Close;
            SQL.Clear;
            SQL.Add('SELECT TT.CODIGO AS TITULO,TV.NUMEROCUPOM,SUM(TP.VALOR) AS VALOR,SUM(TP.SALDO) AS SALDO FROM TABPENDENCIA TP');
            SQL.Add('JOIN TABTITULO TT ON TT.CODIGO=TP.TITULO');
            SQL.Add('JOIN TABVENDAS TV ON TV.TITULO=TT.CODIGO');
            SQL.Add('WHERE TV.CODIGO='+pVenda);

            if(Trim(pPendencia)<>'')
            then SQL.Add('AND TP.CODIGO='+pPendencia);

            if(SomenteAVista)
            then SQL.Add('AND TV.DATAEMISSAO=TP.VENCIMENTO');

            SQL.Add('GROUP BY TT.CODIGO,TV.NUMEROCUPOM');


            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('ERRO - Erro ao Verificar lan�amentos na Venda: '+pvenda+#13+'Erro: '+e.Message);
                        Exit;
                  end;
            end;

            Last;
            if(recordcount > 0)
            then begin
                  First;
                  ValorAReceber := FieldByName('VALOR').AsCurrency;
                  Titulo := Fieldbyname('TITULO').AsString;
                  Cupom := Fieldbyname('NUMEROCUPOM').AsString;
                  if(FieldByName('VALOR').AsCurrency <> FieldByName('SALDO').AsCurrency)
                  then begin
                        LogAdd('ERRO - J� ocorreram lan�amentos na venda N� '+pVenda);
                        Exit;
                  end;
            end
            else begin
                  LogAdd('ERRO - N�o foram encontrados lan�amentos para a venda N� '+pVenda);
                  Exit;
            end;


            {Verificando se o valor a receber a vista � igual ao valor recebido
            Aqui irei adotar a seguinte l�gica:

            PodeM ocorrer in�meras situa��es no recebimento, como valor recebido maior ou menor
            ent�o precisaria saber se houve desconto ou acr�scimo ou simplesmente adiantou o valor de
            uma pr�xima parcela, em casos de venda a prazo.

            Devido a isso, resolvi trabalhar somente com o caso ideal, onde o valor recebido � igual
            ao valor devido a vista

            *obs: ver casos onde o recebimento das parcelas tiver sido feito em cheque e o valor
            a vista em dinheiro/cartao...etc
            }

            Close;
            SQL.Clear;
            SQL.Add('SELECT SUM(TRV.VALOR) AS VALOR,TRV.REFERENCIA,TRV.DATARECEBIMENTO FROM TABRECVENDA_IMPORTADO TRV');
            SQL.Add('JOIN TABFORMASPAGAMENTO_FC TFP ON TFP.CODIGO=TRV.FORMAPAGAMENTO');
            SQL.Add('JOIN TABVENDAS TV ON TV.CODIGO=TRV.VENDA');
            SQL.Add('WHERE TRV.VENDA='+pVenda);
            SQL.Add('AND TFP.APRAZO=''N''');// AND TRV.VALOR>0');
            SQL.Add('GROUP BY TRV.REFERENCIA,TRV.DATARECEBIMENTO');

            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('ERRO - Erro ao Somar lan�amentos na Recebimentos Venda Importado. Venda: '+pvenda+#13+'Erro: '+e.Message);
                        Exit;
                  end;
            end;

            Last;
            if(recordcount > 0)
            then begin
                  First;
                  Referencia := FieldByName('REFERENCIA').AsString;
                  Data := FieldByName('DATARECEBIMENTO').AsString;
                  ValorRecebido := FieldByName('VALOR').AsCurrency;
            end;

            if(ValorAReceber <> ValorRecebido)
            then begin
                  LogAdd('ERRO - Venda: '+pvenda + '. T�tulo: '+Titulo + '. Valor a receber � Vista: '+CurrToStr(ValorAReceber) + ' e valor Recebido: '+CurrToStr(ValorRecebido)+'. Refer�ncia PAF: '+Referencia);
                  Exit;
            end;

            //agora tenho as valida��es necess�rias executadas, posso dar prosseguimento
            //nas quita��es.

            Close;
            SQL.Clear;
            SQL.Add('SELECT SUM(TRV.VALOR) AS SOMA FROM TABRECVENDA_IMPORTADO TRV');
            SQL.Add('WHERE TRV.VALOR<0 AND TRV.VENDA='+pVenda);
            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('ERRO - Erro ao Somar lan�amentos de TROCO no Recebimentos Venda Importado. Venda: '+pvenda+#13+'Erro: '+e.Message);
                        Exit;
                  end;
            end;
            SomaTroco := fieldbyname('SOMA').AsCurrency * -1; //o troco � salvo negativo no banco de dados
            fraseerro := 'VENDA N� '+Referencia+' E CUPOM N� '+Cupom;

            IF (ValorAReceber > 0)
            Then Begin
                  try

                        Close;
                        SQL.clear;
                        SQL.add('Select min(codigo) as codigo from tabpendencia where titulo='+Titulo);
                        Open;

                        //a primeira pendencia � sempre a avista
                        //por isso faco um lancamento no valor a vista
                        objValores.Lancamento.ZerarTabela;
                        Plancamentoavista:=ObjValores.Lancamento.Get_NovoCodigo;
                        ObjValores.Lancamento.Submit_CODIGO(plancamentoavista);
                        ObjValores.Lancamento.Pendencia.Submit_CODIGO(fieldbyname('codigo').asstring);
                        ObjValores.Lancamento.TipoLancto.Submit_CODIGO(PCodigoquitacao);
                        ObjValores.Lancamento.Submit_Valor(CurrToStr(ValorAReceber));
                        ObjValores.Lancamento.Submit_Historico('REC. NO FRENTE DE CAIXA '+fraseerro);
                        ObjValores.Lancamento.Submit_Data(Data);
                        ObjValores.Lancamento.Submit_LancamentoPai('');
                        ObjValores.Lancamento.Status:=dsInsert;
                        if (ObjValores.Lancamento.Salvar(false,true,'',false,false)=False)
                        Then Begin
                              LogAdd('ERRO - Erro na tentativa de salvar o lan�amento de quita��o da '+fraseerro);
                              FDataModulo.IBTransaction.RollbackRetaining;
                              exit;
                        End;
                        //Gravando na tabvalores os valores recebidos;
                        //selecionando as formas de recebimento do frente de caixa
                        Close;
                        SQL.Clear;
                        SQL.Add('SELECT * FROM TABRECVENDA_IMPORTADO TRV');
                        SQL.Add('JOIN TABFORMASPAGAMENTO_FC TFP ON TFP.CODIGO = TRV.FORMAPAGAMENTO');
                        SQL.Add('WHERE TFP.APRAZO='+#39+'N'+#39);
                        SQL.Add('AND TRV.VENDA='+pVenda);
                        SQL.Add('AND TRV.VALOR>0');
                        Open;
                        First;
                        While not(eof) do
                        Begin
                              Pcodigocheque:='';
                              if (fieldbyname('dinheiro_cheque').asstring='C')
                              Then Begin
                                    objValores.ZerarTabela;
                                    ObjValores.Cheque.Status:=dsinsert;
                                    Pcodigocheque:=ObjValores.Cheque.Get_NovoCodigo;
                                    ObjValores.Cheque.Submit_CODIGO          (Pcodigocheque);
                                    ObjValores.Cheque.Submit_Portador        (fieldbyname('portador').asstring);
                                    ObjValores.Cheque.Submit_Valor           (fieldbyname('valor').asstring);
                                    if(fieldbyname('vencimento').asstring<>'')
                                    Then ObjValores.Cheque.Submit_Vencimento      (fieldbyname('vencimento').asstring)
                                    Else ObjValores.Cheque.Submit_Vencimento      (Data);
                                    ObjValores.Cheque.Submit_Comp            (fieldbyname('comp').asstring);
                                    ObjValores.Cheque.Submit_Banco           (fieldbyname('banco').asstring);
                                    ObjValores.Cheque.Submit_Agencia         (fieldbyname('agencia').asstring);
                                    ObjValores.Cheque.Submit_C1              (fieldbyname('c1').asstring);
                                    ObjValores.Cheque.Submit_Conta           (fieldbyname('conta').asstring);
                                    ObjValores.Cheque.Submit_C2              (fieldbyname('c2').asstring);
                                    ObjValores.Cheque.Submit_Serie           (fieldbyname('serie').asstring);
                                    ObjValores.Cheque.Submit_NumCheque       (fieldbyname('numcheque').asstring);
                                    ObjValores.Cheque.Submit_C3              (fieldbyname('c3').asstring);
                                    ObjValores.Cheque.Submit_Cliente1        (fieldbyname('cliente1').asstring);
                                    ObjValores.Cheque.Submit_CPFCliente1     (fieldbyname('cpfcliente1').asstring);
                                    ObjValores.Cheque.Submit_Cliente2        (fieldbyname('cliente2').asstring);
                                    ObjValores.Cheque.Submit_CPFCliente2     (fieldbyname('cpfcliente2').asstring);
                                    ObjValores.Cheque.Submit_CodigodeBarras  (fieldbyname('codigodebarras').asstring);

                                    If not(ObjValores.Cheque.SalvarChequeRecebido(False))
                                    Then Begin
                                             LogAdd('ERRO - Erro na tentativa de salvar o cheque da '+fraseerro);
                                             FDataModulo.IBTransaction.RollbackRetaining;
                                             exit;
                                    End;
                              End;//cheque

                              ObjValores.ZerarTabela;
                              ObjValores.Submit_CODIGO(ObjValores.Get_NovoCodigo);
                              ObjValores.Status:=dsinsert;
                              ObjValores.Submit_Historico('REC. VENDA NO FRENTE DE CAIXA');
                              ObjValores.Submit_Tipo(fieldbyname('dinheiro_Cheque').asstring);
                              ObjValores.Submit_Valor(fieldbyname('valor').asstring);
                              ObjValores.Submit_Lancamento(Plancamentoavista);
                              ObjValores.Submit_Portador(fieldbyname('portador').asstring);
                              ObjValores.Submit_Cheque(Pcodigocheque);

                              if not(ObjValores.Salvar(false,true))
                              Then Begin
                                    LogAdd('ERRO - Erro na tentativa de salvar o lan�amento de recebimento da '+fraseerro);
                                    FDataModulo.IBTransaction.RollbackRetaining;
                                    exit;
                              End;
                              next;//proximos recebimentos

                        End;
                  Except

                  End;
            End;

            if (somatroco > 0)
            Then Begin
                  Try
                        Close;
                        SQL.Clear;
                        SQL.Add('SELECT PORTADOR FROM TABFORMASPAGAMENTO_FC TFP');
                        SQL.Add('WHERE NOME = ' + #39 +'DINHEIRO' + #39);
                        try
                              Open;
                        except
                              on e:Exception do
                              begin
                                    LogAdd('ERRO - Erro ao resgatar informa��es da forma de pagamento de Troco. Venda: '+pvenda+#13+'Erro: '+e.Message);
                                    FDataModulo.IBTransaction.RollbackRetaining;
                                    Exit;
                              end;
                        end;

                        ObjLanctoPortadorGlobal.ZerarTabela;

                        If ObjLanctoPortadorGlobal.TipoLancto.LocalizaHistorico('D�BITO GERADO PELO SISTEMA')=false
                        Then Begin
                              LogAdd('ERRO - TIPO DE LANCTO EM PORTADOR "D�BITO GERADO PELO SISTEMA" N�O ENCONTRADO');
                              FDataModulo.IBTransaction.RollbackRetaining;
                              exit;
                        End;
                        ObjLanctoPortadorGlobal.TipoLancto.TabelaparaObjeto;

                        ObjLanctoPortadorGlobal.Submit_CODIGO(ObjLanctoPortadorGlobal.Get_NovoCodigo);
                        ObjLanctoPortadorGlobal.Submit_Portador(Fieldbyname('PORTADOR').AsString);
                        ObjLanctoPortadorGlobal.Submit_Historico('TROCO REF. '+FRASEERRO);
                        ObjLanctoPortadorGlobal.Submit_Data(Data);
                        ObjLanctoPortadorGlobal.Submit_Valor(CurrToStr((SomaTroco)*-1));
                        ObjLanctoPortadorGlobal.Submit_TabelaGeradora('TABTITULO');
                        ObjLanctoPortadorGlobal.Submit_ObjetoGerador('OBJTITULO');
                        ObjLanctoPortadorGlobal.Submit_CampoPrimario('CODIGO');
                        ObjLanctoPortadorGlobal.Submit_ValordoCampo(Titulo);
                        ObjLanctoPortadorGlobal.Submit_ImprimeRel(true);
                        ObjLanctoPortadorGlobal.Status:=dsinsert;

                        if not(ObjLanctoPortadorGlobal.Salvar(false,true,false))
                        Then begin
                              LogAdd('ERRO - Erro na tentativa de gravar o troco ref. '+fraseerro);
                              FDataModulo.IBTransaction.RollbackRetaining;
                              exit;
                        end;

                  Except

                  End;
            end;

            //Atualizando os registros da tabrecvenda_importado para processado=sim

            Close;
            SQL.Clear;
            SQL.Add('UPDATE TABRECVENDA_IMPORTADO SET PROCESSADO='+#39 + 'S' + #39);
            SQL.Add('WHERE VENDA='+pVenda);
            try
                  ExecSQL;
            except
                  on e:Exception do
                  begin
                        LogAdd('ERRO - Erro ao alterar registros para processado na Venda: '+pvenda+#13+'Erro: '+e.Message);
                        FDataModulo.IBTransaction.RollbackRetaining;
                        Exit;
                  end;
            end;


            //Ao final de cada venda da um commit, para evitar problemas
            LogAdd('QUITADO - Venda: '+pVenda+' T�tulo: '+Titulo+' Quita��o de recebimentos � vista conclu�do com sucesso!'+#13+'Refer�ncia PAF: '+Referencia);
            FdataModulo.IBTransaction.CommitRetaining;

      end;
end;




function TobjLancamentosFCAmanda.VerificaCorrespondenciaValores(
  pVenda,pPendencia: string; SomenteAVista:boolean): Integer;
begin
      result := 0;
      with objQueryAuxiliar do
      begin
            {Aqui irei adotar a seguinte l�gica:

            PodeM ocorrer in�meras situa��es no recebimento, como valor recebido maior ou menor
            ent�o precisaria saber se houve desconto ou acr�scimo ou simplesmente adiantou o valor de
            uma pr�xima parcela, em casos de venda a prazo.

            Devido a isso, resolvi trabalhar somente com o caso ideal, onde o valor recebido � igual
            ao valor devido a vista

            *obs: ver casos onde o recebimento das parcelas tiver sido feito em cheque e o valor
            a vista em dinheiro/cartao...etc
            }
            
            Close;
            SQL.Clear;
            SQL.Add('SELECT SUM(TP.VALOR) AS VALOR');
            SQL.Add('JOIN TABTITULO TT ON TT.CODIGO=TP.TITULO');
            SQL.Add('JOIN TABVENDAS TV ON TV.TITULO=TT.CODIGO');
            SQL.Add('WHERE TV.CODIGO='+pVenda);

            if(Trim(pPendencia)<>'')
            then SQL.Add('AND TP.CODIGO='+pPendencia);

            if(SomenteAVista)
            then SQL.Add('AND TV.DATAEMISSAO=TP.VENCIMENTO');

            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd('Erro ao Verificar lan�amentos na Venda: '+pvenda+#13+'Erro: '+e.Message);
                        Result := -1;
                        Exit;
                  end;
            end;

            Last;
            if(recordcount > 0)
            then begin
                  First;
                  if(FieldByName('VALOR').AsCurrency <> FieldByName('SALDO').AsCurrency)
                  then result := 1;
            end;
      end;
     
end;



procedure TobjLancamentosFCAmanda.EdtVENDAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   //FVENDA:TFvendas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            //FVENDA:=TFvendas.create(nil);

            If (FpesquisaLocal.PreparaPesquisa('SELECT * FROM VIEWVENDAIMPORTACAO','VENDAS',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('CODIGO').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           //Freeandnil(FVENDA);
     End;
end;


function TobjLancamentosFCAmanda.VerificaRelacionamentoFormasPagamento2(
  idVenda, idOrdemServico, idOrdemServidoGeral :Variant): Boolean;
begin
  result := false;
  with objQueryAuxiliar do
  begin
            //Resgatando os pagamentos realizados na venda
    Close;
    SQL.Clear;
    SQL.Text := ' SELECT CODIGO,FORMAPAGAMENTO,DESCRICAO,REFERENCIA '+
                ' FROM TABRECVENDA_IMPORTADO ';

    if not VarIsNull(idVenda) then
    begin
      SQL.Add(' WHERE VENDA = :id ');
      Params[0].Value := idVenda;
    end;

    if not VarIsNull(idOrdemServico) then
    begin
      SQL.Add(' WHERE ordemservico = :id ');
      Params[0].Value := idOrdemServico;
    end;

    if not VarIsNull(idOrdemServidoGeral) then
    begin
      SQL.Add(' WHERE ordemservicogeral = :id ');
      Params[0].Value := idOrdemServidoGeral;
    end;
    try
      Open;
    except
      on e:Exception do
      begin
        LogAdd('Erro ao resgatar valores da tabela: TABRECVENDA_IMPORTADO'+#13+'Erro: '+e.Message);
        Exit;
      end;
    end;
    Last;
    if(recordcount > 0) then
    begin
      First;
      while not (Eof) do
      begin
        //Verificando os relacionamentos entre os prazos e os recebimentos
        ObjQueryTEMP.Close;
        ObjQueryTEMP.SQL.Clear;
        ObjQueryTEMP.SQL.Text :=  ' SELECT * '+
                                  ' FROM TABFORMASPAGAMENTO_FC '+
                                  ' WHERE CODIGO = '+fieldbyname('FORMAPAGAMENTO').AsString;
        try
          ObjQueryTEMP.Open;
        except
          on e:Exception do
          begin
            LogAdd('Erro ao Validar o relacionamento entre as formas de pagamento do Frente de Caixa e os Prazos.'+#13+'Erro: '+e.Message);
            Exit;
          end;
        end;
        ObjQueryTEMP.Last;
        if(ObjQueryTEMP.RecordCount = 0) then
        begin
          if not VarIsNull(idVenda) then
            LogAdd('Sem relacionamento para a forma de pagamento: '+fieldbyname('FORMAPAGAMENTO').AsString+' - '+fieldbyname('DESCRICAO').AsString+'. Venda: '+VarToStr(idVenda)+#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString);
          if not VarIsNull(idOrdemServico) then
            LogAdd('Sem relacionamento para a forma de pagamento: '+fieldbyname('FORMAPAGAMENTO').AsString+' - '+fieldbyname('DESCRICAO').AsString+'. Ordem Servi�o: '+VarToStr(idOrdemServico)+#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString);
          if not VarIsNull(idOrdemServidoGeral) then
            LogAdd('Sem relacionamento para a forma de pagamento: '+fieldbyname('FORMAPAGAMENTO').AsString+' - '+fieldbyname('DESCRICAO').AsString+'. Ordem Servi�o Geral: '+VarToStr(idOrdemServidoGeral)+#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString);
          Exit;
        end;
        Next;
      end;
    end
    else begin
      if not VarIsNull(idVenda) then
        LogAdd('N�o foram encontrados recebimentos a serem processados para a Venda: '+ VarToStr(idVenda) +#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString );
      if not VarIsNull(idOrdemServico) then
        LogAdd('N�o foram encontrados recebimentos a serem processados para a Ordem Servi�o: '+ VarToStr(idOrdemServico) +#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString );
      if not VarIsNull(idOrdemServidoGeral) then
        LogAdd('N�o foram encontrados recebimentos a serem processados para a Venda: '+ VarToStr(idOrdemServidoGeral) +#13+'Refer�ncia PAF: '+fieldbyname('REFERENCIA').AsString );
      Exit;
    end;
  end;
  result := True;
end;

function TobjLancamentosFCAmanda.EfetuaLancamentos2(idVenda,
  idOrdemServico, idOrdemServicoGeral: Variant; pPendencia: String;
  SomenteAVista: Boolean): Boolean;
var
      ValorAReceber,ValorRecebido,SomaTroco:Currency;
      Plancamentoavista,Titulo,Cupom,Referencia,Data,fraseerro,Pcodigocheque:string;
begin
  Result := False;
  ValorAReceber := 0;
  ValorRecebido := 0;
  Titulo := '';
  Cupom := '';
  Data := '';
  Referencia := '';
  with objQueryAuxiliar do
  begin
    //Verificando se houve alguma altera��o nas pendencias
    Close;
    SQL.Clear;
    if not VarIsNull(idVenda) then
    begin
      SQL.Text := ' select t.codigo as titulo, v.numerocupom, sum (p.valor) as valor, sum (p.saldo) as saldo '+
                  ' from tabpendencia p '+
                  ' join tabtitulo t on (t.codigo = p.titulo) '+
                  ' join tabvendas v on (v.titulo = t.codigo) '+
                  ' where v.codigo = :id ';
      if (Trim(pPendencia) <> '') then
        SQL.Add(' AND P.CODIGO = '+pPendencia);

      if (SomenteAVista) then
        SQL.Add(' AND V.DATAEMISSAO = P.VENCIMENTO ');

      SQL.Add(' group by 1,2');
      Params[0].Value := idVenda;
    end;

    if not VarIsNull(idOrdemServico) then
    begin
      SQL.Text := ' select t.codigo as titulo, o.numerocupom, sum(p.valor) as valor, sum (p.saldo) as saldo '+
                  ' from tabpendencia p '+
                  ' join tabtitulo t on (t.codigo = p.titulo) '+
                  ' join tabordemservico o on (o.titulo = t.codigo) '+
                  ' where o.codigo = :id ';

      if (Trim(pPendencia) <> '') then
        SQL.Add(' AND P.CODIGO = '+pPendencia);

      if (SomenteAVista) then
        SQL.Add(' AND o.dataconcluido = P.VENCIMENTO ');

      SQL.Add(' group by 1,2');
      Params[0].Value := idOrdemServico
    end;

    if not VarIsNull(idOrdemServicoGeral) then
    begin
      SQL.Text := ' select t.codigo as titulo, o.numerocupom, sum(p.valor) as valor, sum (p.saldo) as saldo '+
                  ' from tabpendencia p '+
                  ' join tabtitulo t on (t.codigo = p.titulo) '+
                  ' join tabordemservicogeral o on (o.tituto = t.codigo) '+
                  ' where o.codigo = :id ';

      if (Trim(pPendencia) <> '') then
        SQL.Add(' AND P.CODIGO = '+pPendencia);

      if (SomenteAVista) then
        SQL.Add(' AND o.dataconclusao = P.VENCIMENTO ');

      SQL.Add(' group by 1,2');
      Params[0].Value := idOrdemServicoGeral
    end;
    try
      Open;
    except
      on e:Exception do
      begin
        if not VarIsNull(idVenda) then
          LogAdd('ERRO - Erro ao Verificar lan�amentos na Venda: '+ VarToStr(idVenda) +#13+'Erro: '+e.Message);
        if not VarIsNull(idOrdemServico) then
          LogAdd('ERRO - Erro ao Verificar lan�amentos na Ordem de Servi�o: '+ VarToStr(idOrdemServico) +#13+'Erro: '+e.Message);
        if not VarIsNull(idOrdemServicoGeral) then
          LogAdd('ERRO - Erro ao Verificar lan�amentos na Ordem de Servi�o Geral: '+ VarToStr(idOrdemServicoGeral) +#13+'Erro: '+e.Message);
        Exit;
      end;
    end;
    if not IsEmpty then
    begin
      First;
      ValorAReceber := FieldByName('VALOR').AsCurrency;
      Titulo := Fieldbyname('TITULO').AsString;
      Cupom := Fieldbyname('NUMEROCUPOM').AsString;
      if(FieldByName('VALOR').AsCurrency <> FieldByName('SALDO').AsCurrency) then
      begin
        LogAdd('ERRO - J� ocorreram lan�amentos na venda N� '+VarToStr(idVenda));
        Exit;
      end;
    end
    else begin
      LogAdd('ERRO - N�o foram encontrados lan�amentos para a venda N� '+VarToStr(idVenda));
      Exit;
    end;

            {Verificando se o valor a receber a vista � igual ao valor recebido
            Aqui irei adotar a seguinte l�gica:

            PodeM ocorrer in�meras situa��es no recebimento, como valor recebido maior ou menor
            ent�o precisaria saber se houve desconto ou acr�scimo ou simplesmente adiantou o valor de
            uma pr�xima parcela, em casos de venda a prazo.

            Devido a isso, resolvi trabalhar somente com o caso ideal, onde o valor recebido � igual
            ao valor devido a vista

            *obs: ver casos onde o recebimento das parcelas tiver sido feito em cheque e o valor
            a vista em dinheiro/cartao...etc
            }

    Close;
    SQL.Clear;
    if not VarIsNull(idVenda) then
    begin
      SQL.Text := ' select sum (trv.valor) as valor, trv.referencia, trv.datarecebimento '+
                  ' from tabrecvenda_importado trv '+
                  ' inner join tabformaspagamento_fc tfp on tfp.codigo = trv.formapagamento '+
                  ' where trv.venda = :venda and tfp.aprazo = ' + QuotedStr('N')+' '+
                  ' group by trv.referencia, trv.datarecebimento ';
      Params[0].Value := idVenda;
      try
        Open;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao Somar lan�amentos na Recebimentos Venda Importado. Venda: '+ VarToStr(idVenda) +#13+'Erro: '+e.Message);
          Exit;
        end;
      end;
    end;

    if not VarIsNull(idOrdemServico) then
    begin
      SQL.Text := ' select sum (trv.valor) as valor, trv.referencia, trv.datarecebimento '+
                  ' from tabrecvenda_importado trv '+
                  ' inner join tabformaspagamento_fc tfp on tfp.codigo = trv.formapagamento '+
                  ' where trv.ordemservico = :ordemservico and tfp.aprazo = ' + QuotedStr('N')+' '+
                  ' group by trv.referencia, trv.datarecebimento ';
      Params[0].Value := idOrdemServico;
      try
        Open;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao Somar lan�amentos na Recebimentos Venda Importado. Ordem Servico: '+ VarToStr(idOrdemServico) +#13+'Erro: '+e.Message);
          Exit;
        end;
      end;
    end;

    if not VarIsNull(idOrdemServicoGeral) then
    begin
      SQL.Text := ' select sum (trv.valor) as valor, trv.referencia, trv.datarecebimento '+
                  ' from tabrecvenda_importado trv '+
                  ' inner join tabformaspagamento_fc tfp on tfp.codigo = trv.formapagamento '+
                  ' where trv.ordemservicogeral = :ordemservicogeral and tfp.aprazo = ' + QuotedStr('N')+' '+
                  ' group by trv.referencia, trv.datarecebimento ';
      Params[0].Value := idOrdemServicoGeral;
      try
        Open;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao Somar lan�amentos na Recebimentos Venda Importado. Ordem Servico: '+ VarToStr(idOrdemServicoGeral) +#13+'Erro: '+e.Message);
          Exit;
        end;
      end;
    end;

    if not IsEmpty then
    begin
      First;
      Referencia := FieldByName('REFERENCIA').AsString;
      Data := FieldByName('DATARECEBIMENTO').AsString;
      ValorRecebido := FieldByName('VALOR').AsCurrency;
    end;

    if (ValorAReceber <> ValorRecebido) then
    begin
      if not VarIsNull(idVenda) then
        LogAdd('ERRO - Venda: '+ VarToStr(idVenda)  + '. T�tulo: '+Titulo + '. Valor a receber � Vista: '+CurrToStr(ValorAReceber) + ' e valor Recebido: '+CurrToStr(ValorRecebido)+'. Refer�ncia PAF: '+Referencia);

      if not VarIsNull(idOrdemServico) then
        LogAdd('ERRO - Ordem Servi�o: '+ VarToStr(idOrdemServico)  + '. T�tulo: '+Titulo + '. Valor a receber � Vista: '+CurrToStr(ValorAReceber) + ' e valor Recebido: '+CurrToStr(ValorRecebido)+'. Refer�ncia PAF: '+Referencia);

      if not VarIsNull(idOrdemServicoGeral) then
        LogAdd('ERRO - Ordem Servi�o Geral: '+ VarToStr(idOrdemServicoGeral)  + '. T�tulo: '+Titulo + '. Valor a receber � Vista: '+CurrToStr(ValorAReceber) + ' e valor Recebido: '+CurrToStr(ValorRecebido)+'. Refer�ncia PAF: '+Referencia);

      Exit;
    end;

            //agora tenho as valida��es necess�rias executadas, posso dar prosseguimento
            //nas quita��es.

    Close;
    SQL.Clear;
    //venda
    if not VarIsNull(idVenda) then
    begin
      SQL.Text := ' SELECT SUM(TRV.VALOR) AS SOMA '+
                  ' FROM TABRECVENDA_IMPORTADO TRV '+
                  ' WHERE TRV.VALOR < 0 AND '+
                  ' TRV.VENDA = :id';
      Params[0].Value := idVenda;
      try
        Open;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao Somar lan�amentos de TROCO no Recebimentos Venda Importado. Venda: '+ VarToStr(idVenda) +#13+'Erro: '+e.Message);
          Exit;
        end;
      end;
      SomaTroco := fieldbyname('SOMA').AsCurrency * -1; //o troco � salvo negativo no banco de dados
      fraseerro := 'VENDA N� '+Referencia+' E CUPOM N� '+Cupom;
    end;

    //ordemservico
    if not VarIsNull(idOrdemServico) then
    begin
      SQL.Text := ' SELECT SUM(TRV.VALOR) AS SOMA '+
                  ' FROM TABRECVENDA_IMPORTADO TRV '+
                  ' WHERE TRV.VALOR < 0 AND '+
                  ' TRV.ordemservico = :id';
      Params[0].Value := idOrdemServico;
      try
        Open;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao Somar lan�amentos de TROCO no Recebimentos Venda Importado. Ordem Servi�o: '+ VarToStr(idOrdemServico) +#13+'Erro: '+e.Message);
          Exit;
        end;
      end;
      SomaTroco := fieldbyname('SOMA').AsCurrency * -1; //o troco � salvo negativo no banco de dados
      fraseerro := 'Ordem Servi�o N� '+Referencia+' E CUPOM N� '+Cupom;
    end;

    //ordemservicogeral
    if not VarIsNull(idOrdemServicoGeral) then
    begin
      SQL.Text := ' SELECT SUM(TRV.VALOR) AS SOMA '+
                  ' FROM TABRECVENDA_IMPORTADO TRV '+
                  ' WHERE TRV.VALOR < 0 AND '+
                  ' TRV.ordemservicogeral = :id';
      Params[0].Value := idOrdemServicoGeral;
      try
        Open;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao Somar lan�amentos de TROCO no Recebimentos Venda Importado. Ordem Servi�o Geral: '+ VarToStr(idOrdemServicoGeral) +#13+'Erro: '+e.Message);
          Exit;
        end;
      end;
      SomaTroco := fieldbyname('SOMA').AsCurrency * -1; //o troco � salvo negativo no banco de dados
      fraseerro := 'Ordem Servi�o Geral N� '+Referencia+' E CUPOM N� '+Cupom;
    end;

    if (ValorAReceber > 0) then
    begin
      try
        Close;
        SQL.clear;
        SQL.add('Select min(codigo) as codigo from tabpendencia where titulo='+Titulo);
        Open;
                        //a primeira pendencia � sempre a avista
                        //por isso faco um lancamento no valor a vista
        objValores.Lancamento.ZerarTabela;
        Plancamentoavista:=ObjValores.Lancamento.Get_NovoCodigo;
        ObjValores.Lancamento.Submit_CODIGO(plancamentoavista);
        ObjValores.Lancamento.Pendencia.Submit_CODIGO(fieldbyname('codigo').asstring);
        ObjValores.Lancamento.TipoLancto.Submit_CODIGO(PCodigoquitacao);
        ObjValores.Lancamento.Submit_Valor(CurrToStr(ValorAReceber));
        ObjValores.Lancamento.Submit_Historico('REC. NO FRENTE DE CAIXA '+fraseerro);
        ObjValores.Lancamento.Submit_Data(Data);
        ObjValores.Lancamento.Submit_LancamentoPai('');
        ObjValores.Lancamento.Status:=dsInsert;

        if (ObjValores.Lancamento.Salvar(false,true,'',false,false)=False) then
        begin
          LogAdd('ERRO - Erro na tentativa de salvar o lan�amento de quita��o da '+fraseerro);
          FDataModulo.IBTransaction.RollbackRetaining;
          exit;
        end;
                        //Gravando na tabvalores os valores recebidos;
                        //selecionando as formas de recebimento do frente de caixa
        Close;
        SQL.Clear;
        if not VarIsNull(idVenda) then
        begin
          SQL.Text := ' select * '+
                      ' from tabrecvenda_importado trv '+
                      ' inner join tabformaspagamento_fc tfp on tfp.codigo = trv.formapagamento '+
                      ' where trv.venda = :id and tfp.aprazo = ' + QuotedStr('N') + ' and trv.valor > 0 ';
          Params[0].Value := idVenda;
        end;

        if not VarIsNull(idOrdemServico) then
        begin
          SQL.Text := ' select * '+
                      ' from tabrecvenda_importado trv '+
                      ' inner join tabformaspagamento_fc tfp on tfp.codigo = trv.formapagamento '+
                      ' where trv.ordemservico = :id and tfp.aprazo = ' + QuotedStr('N') + ' and trv.valor > 0 ';
          Params[0].Value := idOrdemServico;
        end;

        if not VarIsNull(idOrdemServicoGeral) then
        begin
          SQL.Text := ' select * '+
                      ' from tabrecvenda_importado trv '+
                      ' inner join tabformaspagamento_fc tfp on tfp.codigo = trv.formapagamento '+
                      ' where trv.ordemservicogeral = :id and tfp.aprazo = ' + QuotedStr('N') + ' and trv.valor > 0 ';
          Params[0].Value := idOrdemServicoGeral;
        end;
        Open;
        First;
        while not(eof) do
        begin
          Pcodigocheque:='';
          if (fieldbyname('dinheiro_cheque').asstring='C') then
          begin
            objValores.ZerarTabela;
            ObjValores.Cheque.Status:=dsinsert;
            Pcodigocheque:=ObjValores.Cheque.Get_NovoCodigo;
            ObjValores.Cheque.Submit_CODIGO          (Pcodigocheque);
            ObjValores.Cheque.Submit_Portador        (fieldbyname('portador').asstring);
            ObjValores.Cheque.Submit_Valor           (fieldbyname('valor').asstring);
            if(fieldbyname('vencimento').asstring<>'') then
              ObjValores.Cheque.Submit_Vencimento      (fieldbyname('vencimento').asstring)
            else
              ObjValores.Cheque.Submit_Vencimento      (Data);
            ObjValores.Cheque.Submit_Comp            (fieldbyname('comp').asstring);
            ObjValores.Cheque.Submit_Banco           (fieldbyname('banco').asstring);
            ObjValores.Cheque.Submit_Agencia         (fieldbyname('agencia').asstring);
            ObjValores.Cheque.Submit_C1              (fieldbyname('c1').asstring);
            ObjValores.Cheque.Submit_Conta           (fieldbyname('conta').asstring);
            ObjValores.Cheque.Submit_C2              (fieldbyname('c2').asstring);
            ObjValores.Cheque.Submit_Serie           (fieldbyname('serie').asstring);
            ObjValores.Cheque.Submit_NumCheque       (fieldbyname('numcheque').asstring);
            ObjValores.Cheque.Submit_C3              (fieldbyname('c3').asstring);
            ObjValores.Cheque.Submit_Cliente1        (fieldbyname('cliente1').asstring);
            ObjValores.Cheque.Submit_CPFCliente1     (fieldbyname('cpfcliente1').asstring);
            ObjValores.Cheque.Submit_Cliente2        (fieldbyname('cliente2').asstring);
            ObjValores.Cheque.Submit_CPFCliente2     (fieldbyname('cpfcliente2').asstring);
            ObjValores.Cheque.Submit_CodigodeBarras  (fieldbyname('codigodebarras').asstring);

            if not(ObjValores.Cheque.SalvarChequeRecebido(False)) then
            begin
              LogAdd('ERRO - Erro na tentativa de salvar o cheque da '+fraseerro);
              FDataModulo.IBTransaction.RollbackRetaining;
              exit;
            end;
          end;//cheque

          ObjValores.ZerarTabela;
          ObjValores.Submit_CODIGO(ObjValores.Get_NovoCodigo);
          ObjValores.Status:=dsinsert;

          if not VarIsNull(idVenda) then
            ObjValores.Submit_Historico('REC. VENDA NO FRENTE DE CAIXA');
          if not VarIsNull(idOrdemServico) then
            ObjValores.Submit_Historico('REC. ORDEM DE SERVI�O NO FRENTE DE CAIXA');
          if not VarIsNull(idOrdemServicoGeral) then
            ObjValores.Submit_Historico('REC. ORDEM DE SERVI�O GERAL NO FRENTE DE CAIXA');

          ObjValores.Submit_Tipo(fieldbyname('dinheiro_Cheque').asstring);
          ObjValores.Submit_Valor(fieldbyname('valor').asstring);
          ObjValores.Submit_Lancamento(Plancamentoavista);
          ObjValores.Submit_Portador(fieldbyname('portador').asstring);
          ObjValores.Submit_Cheque(Pcodigocheque);

          if not(ObjValores.Salvar(false,true)) then
          begin
            LogAdd('ERRO - Erro na tentativa de salvar o lan�amento de recebimento da '+fraseerro);
            FDataModulo.IBTransaction.RollbackRetaining;
            exit;
          end;
          next;//proximos recebimentos

        end;
      except
      end;
    end;
    if (somatroco > 0) then
    begin
      try
        Close;
        SQL.Clear;
        SQL.Add('SELECT PORTADOR FROM TABFORMASPAGAMENTO_FC TFP');
        SQL.Add('WHERE NOME = ' + #39 +'DINHEIRO' + #39);
        try
          Open;
        except
          on e:Exception do
          begin
            if not VarIsNull(idVenda) then
              LogAdd('ERRO - Erro ao resgatar informa��es da forma de pagamento de Troco. Venda: ' + VarToStr(idVenda) +#13+'Erro: '+e.Message);
            if not VarIsNull(idOrdemServico) then
              LogAdd('ERRO - Erro ao resgatar informa��es da forma de pagamento de Troco. Ordem Servi�o: ' + VarToStr(idOrdemServico) +#13+'Erro: '+e.Message);
            if not VarIsNull(idOrdemServicoGeral) then
              LogAdd('ERRO - Erro ao resgatar informa��es da forma de pagamento de Troco. Ordem Servi�o Geral: ' + VarToStr(idOrdemServicoGeral)+#13+'Erro: '+e.Message);
            FDataModulo.IBTransaction.RollbackRetaining;
            Exit;
          end;
        end;
        ObjLanctoPortadorGlobal.ZerarTabela;
        if ObjLanctoPortadorGlobal.TipoLancto.LocalizaHistorico('D�BITO GERADO PELO SISTEMA')=false then
        begin
          LogAdd('ERRO - TIPO DE LANCTO EM PORTADOR "D�BITO GERADO PELO SISTEMA" N�O ENCONTRADO');
          FDataModulo.IBTransaction.RollbackRetaining;
          exit;
        end;
        ObjLanctoPortadorGlobal.TipoLancto.TabelaparaObjeto;
        ObjLanctoPortadorGlobal.Submit_CODIGO(ObjLanctoPortadorGlobal.Get_NovoCodigo);
        ObjLanctoPortadorGlobal.Submit_Portador(Fieldbyname('PORTADOR').AsString);
        ObjLanctoPortadorGlobal.Submit_Historico('TROCO REF. '+FRASEERRO);
        ObjLanctoPortadorGlobal.Submit_Data(Data);
        ObjLanctoPortadorGlobal.Submit_Valor(CurrToStr((SomaTroco)*-1));
        ObjLanctoPortadorGlobal.Submit_TabelaGeradora('TABTITULO');
        ObjLanctoPortadorGlobal.Submit_ObjetoGerador('OBJTITULO');
        ObjLanctoPortadorGlobal.Submit_CampoPrimario('CODIGO');
        ObjLanctoPortadorGlobal.Submit_ValordoCampo(Titulo);
        ObjLanctoPortadorGlobal.Submit_ImprimeRel(true);
        ObjLanctoPortadorGlobal.Status:=dsinsert;

        if not(ObjLanctoPortadorGlobal.Salvar(false,true,false)) then
        begin
          LogAdd('ERRO - Erro na tentativa de gravar o troco ref. '+fraseerro);
          FDataModulo.IBTransaction.RollbackRetaining;
          exit;
        end;
      except
      end;
    end;
            //Atualizando os registros da tabrecvenda_importado para processado=sim
    Close;
    SQL.Clear;

    if not VarIsNull(idVenda) then
    begin
      SQL.Text := ' UPDATE TABRECVENDA_IMPORTADO SET PROCESSADO = '+QuotedStr('S')+ ' '+
                  ' WHERE VENDA = :id ';
      Params[0].Value := idVenda;
      try
        ExecSQL;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao alterar registros para processado na Venda: ' + VarToStr(idVenda) + #13+'Erro: '+e.Message);
          FDataModulo.IBTransaction.RollbackRetaining;
          Exit;
        end;
      end;
      //Ao final de cada venda da um commit, para evitar problemas
      LogAdd('QUITADO - Venda: ' + VarToStr(idVenda) + ' T�tulo: '+Titulo+' Quita��o de recebimentos � vista conclu�do com sucesso!'+#13+'Refer�ncia PAF: '+Referencia);
    end;

    if not VarIsNull(idOrdemServico) then
    begin
      SQL.Text := ' UPDATE TABRECVENDA_IMPORTADO SET PROCESSADO = '+QuotedStr('S')+ ' '+
                  ' WHERE ordemservico = :id ';
      Params[0].Value := idOrdemServico;
      try
        ExecSQL;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao alterar registros para processado na Ordem Servi�o: ' + VarToStr(idOrdemServico) + #13+'Erro: '+e.Message);
          FDataModulo.IBTransaction.RollbackRetaining;
          Exit;
        end;
      end;
      //Ao final de cada venda da um commit, para evitar problemas
      LogAdd('QUITADO - Ordem Servi�o: ' + VarToStr(idOrdemServico) + ' T�tulo: '+Titulo+' Quita��o de recebimentos � vista conclu�do com sucesso!'+#13+'Refer�ncia PAF: '+Referencia);
    end;

    if not VarIsNull(idOrdemServicoGeral) then
    begin
      SQL.Text := ' UPDATE TABRECVENDA_IMPORTADO SET PROCESSADO = '+QuotedStr('S')+ ' '+
                  ' WHERE ordemservico = :id ';
      Params[0].Value := idOrdemServicoGeral;
      try
        ExecSQL;
      except
        on e:Exception do
        begin
          LogAdd('ERRO - Erro ao alterar registros para processado na Ordem Servi�o Geral: ' + VarToStr(idOrdemServicoGeral) + #13+'Erro: '+e.Message);
          FDataModulo.IBTransaction.RollbackRetaining;
          Exit;
        end;
      end;
      //Ao final de cada venda da um commit, para evitar problemas
      LogAdd('QUITADO - Ordem Servi�o Geral: ' + VarToStr(idOrdemServicoGeral) + ' T�tulo: '+Titulo+' Quita��o de recebimentos � vista conclu�do com sucesso!'+#13+'Refer�ncia PAF: '+Referencia);
    end;


    FdataModulo.IBTransaction.CommitRetaining;
  end;
end;

end.
