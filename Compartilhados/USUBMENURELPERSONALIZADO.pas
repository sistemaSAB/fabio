unit USUBMENURELPERSONALIZADO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjSUBMENURELPERSONALIZADO;

type
  TFSUBMENURELPERSONALIZADO = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbMenu: TLabel;
    EdtMenu: TEdit;
    LbNomeMenu: TLabel;

    LbNome: TLabel;
    EdtNome: TEdit;
//DECLARA COMPONENTES
    procedure edtMenuKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtMenuExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjSUBMENURELPERSONALIZADO:TObjSUBMENURELPERSONALIZADO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FSUBMENURELPERSONALIZADO: TFSUBMENURELPERSONALIZADO;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFSUBMENURELPERSONALIZADO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjSUBMENURELPERSONALIZADO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Menu.Submit_codigo(edtMenu.text);
        Submit_Nome(edtNome.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFSUBMENURELPERSONALIZADO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjSUBMENURELPERSONALIZADO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtMenu.text:=Menu.Get_codigo;
        EdtNome.text:=Get_Nome;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFSUBMENURELPERSONALIZADO.TabelaParaControles: Boolean;
begin
     If (Self.ObjSUBMENURELPERSONALIZADO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFSUBMENURELPERSONALIZADO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjSUBMENURELPERSONALIZADO=Nil)
     Then exit;

    If (Self.ObjSUBMENURELPERSONALIZADO.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjSUBMENURELPERSONALIZADO.free;
end;

procedure TFSUBMENURELPERSONALIZADO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFSUBMENURELPERSONALIZADO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjSUBMENURELPERSONALIZADO.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjSUBMENURELPERSONALIZADO.status:=dsInsert;
     Guia.pageindex:=0;
     edtMenu.setfocus;

end;


procedure TFSUBMENURELPERSONALIZADO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjSUBMENURELPERSONALIZADO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjSUBMENURELPERSONALIZADO.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtMenu.setfocus;
                
          End;


end;

procedure TFSUBMENURELPERSONALIZADO.btgravarClick(Sender: TObject);
begin

     If Self.ObjSUBMENURELPERSONALIZADO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjSUBMENURELPERSONALIZADO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjSUBMENURELPERSONALIZADO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFSUBMENURELPERSONALIZADO.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjSUBMENURELPERSONALIZADO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjSUBMENURELPERSONALIZADO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjSUBMENURELPERSONALIZADO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFSUBMENURELPERSONALIZADO.btcancelarClick(Sender: TObject);
begin
     Self.ObjSUBMENURELPERSONALIZADO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFSUBMENURELPERSONALIZADO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFSUBMENURELPERSONALIZADO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjSUBMENURELPERSONALIZADO.Get_pesquisa,Self.ObjSUBMENURELPERSONALIZADO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjSUBMENURELPERSONALIZADO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjSUBMENURELPERSONALIZADO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjSUBMENURELPERSONALIZADO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFSUBMENURELPERSONALIZADO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFSUBMENURELPERSONALIZADO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjSUBMENURELPERSONALIZADO:=TObjSUBMENURELPERSONALIZADO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;
procedure TFSUBMENURELPERSONALIZADO.edtMenuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjSUBMENURELPERSONALIZADO.edtMenukeydown(sender,key,shift,lbnomeMenu);
end;
 
procedure TFSUBMENURELPERSONALIZADO.edtMenuExit(Sender: TObject);
begin
    ObjSUBMENURELPERSONALIZADO.edtMenuExit(sender,lbnomeMenu);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjSUBMENURELPERSONALIZADO.OBJETO.Get_Pesquisa,Self.ObjSUBMENURELPERSONALIZADO.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NI
L)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjSUBMENURELPERSONALIZADO.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjSUBMENURELPERSONALIZADO.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjSUBMENURELPERSONALIZADO.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
