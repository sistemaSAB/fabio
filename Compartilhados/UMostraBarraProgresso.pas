unit UMostraBarraProgresso;

interface

uses
Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Gauges, StdCtrls;

type
  TFMostraBarraProgresso = class(TForm)
    BarradeProgresso: TGauge;
    Lbmensagem: TLabel;
    BarradeProgresso2: TGauge;
    Lbmensagem2: TLabel;
    btcancelar: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    Cancelar:Boolean;
    Procedure ConfiguracoesIniciais(PValorMaximo,PvalorMaximo2:Integer);
    Procedure IncrementaBarra1(Pquant:Integer);
    Procedure IncrementaBarra2(Pquant:Integer);
    Procedure IncrementaBarra3(Pquant:Integer);
  end;

var
  FMostraBarraProgresso: TFMostraBarraProgresso;

implementation



{$R *.dfm}

procedure TFMostraBarraProgresso.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Lbmensagem2.Visible:=False;
     BarradeProgresso2.Visible:=False;
end;

procedure TFMostraBarraProgresso.ConfiguracoesIniciais(
  PValorMaximo,PvalorMaximo2: Integer);
begin
     Self.barradeprogresso.minvalue:=0;
     Self.barradeprogresso.maxvalue:=PvalorMaximo;
     Self.barradeprogresso.progress:=0;

     Self.barradeprogresso2.minvalue:=0;
     Self.barradeprogresso2.maxvalue:=0;
     Self.barradeprogresso2.progress:=0;
     Self.barradeprogresso2.Visible:=False;
     Self.lbmensagem2.caption:='';
     Self.lbmensagem2.Visible:=False;

     btcancelar.visible:=false;
     cancelar:=false;
end;

procedure TFMostraBarraProgresso.IncrementaBarra1(Pquant: Integer);
begin
     Self.BarradeProgresso.Progress:=Self.BarradeProgresso.Progress+Pquant;
end;

procedure TFMostraBarraProgresso.IncrementaBarra2(Pquant: Integer);
begin
     Self.BarradeProgresso2.Progress:=Self.BarradeProgresso2.Progress+Pquant;
end;

procedure TFMostraBarraProgresso.IncrementaBarra3(Pquant: Integer);
begin
     Self.BarradeProgresso2.Progress:=Self.BarradeProgresso2.Progress+Pquant;
end;

procedure TFMostraBarraProgresso.FormCreate(Sender: TObject);
begin
     btcancelar.visible:=false;
end;

procedure TFMostraBarraProgresso.btcancelarClick(Sender: TObject);
begin
     Self.Cancelar:=True;
end;

end.
