object FrImportaFirebird: TFrImportaFirebird
  Left = 0
  Top = 0
  Width = 790
  Height = 543
  TabOrder = 0
  object GuiaFirebird: TPageControl
    Left = 0
    Top = 0
    Width = 790
    Height = 543
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Principal'
      object MemoSQL: TMemo
        Left = 0
        Top = 0
        Width = 782
        Height = 129
        Align = alTop
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          'MemoSQL')
        ParentFont = False
        TabOrder = 0
        OnKeyDown = MemoSQLKeyDown
      end
      object PanelCaminhotabela: TPanel
        Left = 0
        Top = 129
        Width = 782
        Height = 80
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Left = 3
          Top = 32
          Width = 48
          Height = 16
          Caption = 'Tabela'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object BitBtn1: TBitBtn
          Left = 3
          Top = 2
          Width = 161
          Height = 25
          Caption = 'Abrir Banco (fdb,gdb)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BitBtn1Click
        end
        object ComboTabela: TComboBox
          Left = 3
          Top = 48
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Text = '  '
          OnKeyPress = ComboTabelaKeyPress
        end
        object BitBtn2: TBitBtn
          Left = 152
          Top = 48
          Width = 137
          Height = 25
          Caption = 'Iniciar Importa'#231#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = BitBtn2Click
        end
        object edtcaminhobanco: TEdit
          Left = 168
          Top = 4
          Width = 521
          Height = 21
          TabOrder = 3
          Text = 'edtcaminhobanco'
        end
        object btcaminhobanco: TBitBtn
          Left = 694
          Top = 2
          Width = 29
          Height = 25
          Caption = '...'
          TabOrder = 4
          OnClick = btcaminhobancoClick
        end
      end
      object DbGrid: TDBGrid
        Left = 0
        Top = 209
        Width = 782
        Height = 290
        Align = alClient
        DataSource = DataSource
        TabOrder = 2
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object BarradeProgresso: TProgressBar
        Left = 0
        Top = 499
        Width = 782
        Height = 16
        Align = alBottom
        TabOrder = 3
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Conf. Campos'
      ImageIndex = 1
      object LbCampos: TListBox
        Left = 0
        Top = 41
        Width = 265
        Height = 474
        Align = alLeft
        ItemHeight = 13
        TabOrder = 0
        OnClick = LbCamposClick
        OnKeyDown = LbCamposKeyDown
      end
      object LbValor: TListBox
        Left = 265
        Top = 41
        Width = 288
        Height = 474
        Align = alLeft
        ItemHeight = 13
        TabOrder = 1
        OnClick = LbValorClick
        OnKeyDown = LbValorKeyDown
        OnKeyPress = LbValorKeyPress
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 782
        Height = 41
        Align = alTop
        TabOrder = 2
      end
      object BitBtn3: TBitBtn
        Left = 554
        Top = 42
        Width = 265
        Height = 25
        Caption = 'Carregar Campos do SQL'
        TabOrder = 3
        OnClick = BitBtn3Click
      end
      object BitBtn4: TBitBtn
        Left = 555
        Top = 68
        Width = 265
        Height = 25
        Caption = 'Limpa Campos'
        TabOrder = 4
        OnClick = BitBtn4Click
      end
      object PanelNovoValor: TPanel
        Left = 560
        Top = 232
        Width = 257
        Height = 169
        TabOrder = 5
        object Button1: TButton
          Left = 180
          Top = 136
          Width = 75
          Height = 25
          Caption = 'Altera Campo'
          TabOrder = 0
          OnClick = Button1Click
        end
        object Rg_Opcoes_valor: TRadioGroup
          Left = 1
          Top = 1
          Width = 255
          Height = 136
          Align = alTop
          Caption = 'Op'#231#245'es de Valor'
          Items.Strings = (
            'Valor do Banco'
            'Vazio'
            'Valor Padr'#227'o'
            'Concatenado')
          TabOrder = 1
        end
      end
    end
  end
  object IBTransaction1: TIBTransaction
    DefaultDatabase = IBDatabase1
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 692
    Top = 40
  end
  object IBQuery1: TIBQuery
    Database = IBDatabase1
    Transaction = IBTransaction1
    Left = 732
    Top = 32
  end
  object OpenDialog: TOpenDialog
    Filter = 'Banco de Dados Firebird|*.gdb;*.fdb'
    Left = 600
    Top = 32
  end
  object DataSource: TDataSource
    DataSet = IBQuery1
    Left = 784
    Top = 40
  end
  object IBTable1: TIBTable
    Database = IBDatabase1
    Transaction = IBTransaction1
    Left = 760
    Top = 88
  end
  object IBDatabase1: TIBDatabase
    LoginPrompt = False
    DefaultTransaction = IBTransaction1
    Left = 652
    Top = 40
  end
end
