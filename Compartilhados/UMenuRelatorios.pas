unit UMenuRelatorios;

interface

uses
  db,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  UessencialGlobal,Dialogs, Menus, StdCtrls, Buttons, ExtCtrls, ComCtrls,UObjMENURELATORIOS, UObjABASMENURELATORIOS;

type
  TFMenuRelatorios = class(TForm)
    Paginas: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Rgopcoes1: TRadioGroup;
    RgOpcoes2: TRadioGroup;
    RgOpcoes4: TRadioGroup;
    RgOpcoes3: TRadioGroup;
    TabSheet5: TTabSheet;
    RgOpcoes5: TRadioGroup;
    PopupMenu1: TPopupMenu;
    renomear1: TMenuItem;
    PopupMenu2: TPopupMenu;
    Enviarpara1: TMenuItem;
    MenuGrupo01: TMenuItem;
    MenuGrupo02: TMenuItem;
    MenuGrupo03: TMenuItem;
    MenuGrupo04: TMenuItem;
    MenuGrupo05: TMenuItem;
    pnl3: TPanel;
    btCancel: TSpeedButton;
    lb1: TLabel;
    pnlRodape: TPanel;
    Img1: TImage;
    pnl1: TPanel;
    lbgrupo1: TLabel;
    lbgrupo2: TLabel;
    lbgrupo3: TLabel;
    lbgrupo4: TLabel;
    lbgrupo5: TLabel;
    lb2: TLabel;
    lb3: TLabel;
    lb4: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    rgOpcoes: TRadioGroup;
    btOK: TSpeedButton;
    lbdata: TLabel;
    procedure renomear1Click(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btBtCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btBtokClick(Sender: TObject);
    procedure PaginasChange(Sender: TObject);
    procedure MenuGrupo01Click(Sender: TObject);
    procedure MenuGrupo02Click(Sender: TObject);
    procedure MenuGrupo03Click(Sender: TObject);
    procedure MenuGrupo04Click(Sender: TObject);
    procedure MenuGrupo05Click(Sender: TObject);
  private
    { Private declarations }
    Procedure RecuperaNomeAbas;
    Function  CopiaNumero(StrLinha:String):integer;
    Procedure DistribuiMenu;
    procedure GravaNovaPosicao(PAba: Integer);
  public
    { Public declarations }
    NomeObjeto:string;
  end;

var
  FMenuRelatorios: TFMenuRelatorios;
  objmenurelatorio:TObjMENURELATORIOS;
  Objabamenurelatorio:TObjABASMENURELATORIOS;
  PermiteAlterar:boolean;
  itemescolhido:integer;

implementation

uses UDataModulo, UescolheImagemBotao;





{$R *.dfm}

procedure TFMenuRelatorios.renomear1Click(Sender: TObject);
var
Tmp:String;
begin
     if (NomeObjeto='')
     Then exit;

     if (PermiteAlterar=False)
     Then exit;    

     tmp:=Paginas.Pages[Paginas.tabindex].Caption;
     if (InputQuery('Renomear','Digite um nome para o Grupo',tmp)=False)
     Then exit;
     Paginas.Pages[Paginas.tabindex].Caption:=Tmp;

     Objabamenurelatorio.ZerarTabela;
     if (Objabamenurelatorio.LocalizaObjetoAba(NomeObjeto,inttostr(Paginas.tabindex))=False)
     Then Begin
              Objabamenurelatorio.Status:=dsinsert;
              Objabamenurelatorio.Submit_CODIGO('0');
     End
     Else Begin
              Objabamenurelatorio.Status:=dsedit;
              Objabamenurelatorio.tabelaparaobjeto;
     End;
     Objabamenurelatorio.Submit_Objeto(NomeObjeto);
     Objabamenurelatorio.Submit_Aba(inttostr(paginas.TabIndex));
     Objabamenurelatorio.Submit_Nome(tmp);
     if (Objabamenurelatorio.Salvar(True)=False)
     Then Begin
               Messagedlg('N�o foi poss�vel Salvar',mterror,[mbok],0);
     End;

     LBGrupo1.Caption:=Paginas.Pages[0].Caption;
     LBGrupo2.Caption:=Paginas.Pages[1].Caption;
     LBGrupo3.Caption:=Paginas.Pages[2].Caption;
     LBGrupo4.Caption:=Paginas.Pages[3].Caption;
     LBGrupo5.Caption:=Paginas.Pages[4].Caption;

     

end;

procedure TFMenuRelatorios.PopupMenu2Popup(Sender: TObject);
begin
     MenuGrupo01.Caption:=Paginas.Pages[0].Caption;
     MenuGrupo02.Caption:=Paginas.Pages[1].Caption;
     MenuGrupo03.Caption:=Paginas.Pages[2].Caption;
     MenuGrupo04.Caption:=Paginas.Pages[3].Caption;
     MenuGrupo05.Caption:=Paginas.Pages[4].Caption;



end;

procedure TFMenuRelatorios.FormShow(Sender: TObject);
var
cont:integer;
apoio:string;
begin
     Paginas.Pages[0].Caption:='';
     Paginas.Pages[1].Caption:='';
     Paginas.Pages[2].Caption:='';
     Paginas.Pages[3].Caption:='';
     Paginas.Pages[4].Caption:='';
     Self.Tag:=0;

     lbdata.Caption:= DiaSemana(Now);
     lbdata.Caption:=lbdata.Caption+', '+FormatDateTime('dd "de" mmmm "de" yyyy', Now);


     Try
        objmenurelatorio:=TObjMENURELATORIOS.create;
        Objabamenurelatorio:=TObjABASMENURELATORIOS.create;
     Except
        Messagedlg('Erro na Cria��o dos Objetos MenuRelatorio e AbaMenuRelatorio!',mterror,[mbok],0);
        exit;
     End;

     Self.RecuperaNomeAbas;

     //O Fabio tinha tirado essas linhas da permissao em 09/12, adicionei hoje (26/02/2010)

     if (ObjPermissoesUsoGlobal.ValidaPermissao('PERMITE ALTERAR MENU DE RELAT�RIOS')=False)
     Then PermiteAlterar:=False
     Else PermiteAlterar:=true;


     Paginas.TabIndex:=0;
     if (Rgopcoes1.Items.count>0)
     Then Rgopcoes1.ItemIndex:=0;

     for cont:=0 to RgOpcoes.items.count -1 do
     Begin
          if Pos('&',RgOpcoes.items[cont])=0
          Then Begin
                    apoio:='';
                    apoio:='&'+Inttostr(cont+1)+' - '+RgOpcoes.items[cont];
                    RgOpcoes.items[cont]:='';
                    RgOpcoes.items[cont]:=apoio;
          End;
     End;

     Self.DistribuiMenu;
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');

end;

procedure TFMenuRelatorios.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if key=Vk_F5
     Then Paginas.TabIndex:=0
     Else
     if key=Vk_F6
     Then Paginas.TabIndex:=1
     Else
     if key=Vk_F7
     Then Paginas.TabIndex:=2
     Else
     if key=Vk_F8
     Then Paginas.TabIndex:=3
     Else
     if key=Vk_F9
     Then Paginas.TabIndex:=4;

end;

procedure TFMenuRelatorios.btBtCancelClick(Sender: TObject);
begin
     Self.Tag:=0;
     Self.Close;
end;

procedure TFMenuRelatorios.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

     if (objmenurelatorio<>nil)
     Then objmenurelatorio.free;
     if (Objabamenurelatorio<>nil)
     Then Objabamenurelatorio.Free;
     NomeObjeto:='';

     if (Self.Tag=1)
     Then Rgopcoes.ItemIndex:=itemescolhido;
     
end;

procedure TFMenuRelatorios.RecuperaNomeAbas;
begin

     if (Objabamenurelatorio.LocalizaObjetoAba(NomeObjeto,'0')=True) Then
     Begin
              Objabamenurelatorio.TabelaparaObjeto;
              Paginas.Pages[0].Caption:=Objabamenurelatorio.get_nome;
     End;

     if (Objabamenurelatorio.LocalizaObjetoAba(NomeObjeto,'1')=True) Then
     Begin
              Objabamenurelatorio.TabelaparaObjeto;
              Paginas.Pages[1].Caption:=Objabamenurelatorio.get_nome;
     End;

     if (Objabamenurelatorio.LocalizaObjetoAba(NomeObjeto,'2')=True) Then
     Begin
              Objabamenurelatorio.TabelaparaObjeto;
              Paginas.Pages[2].Caption:=Objabamenurelatorio.get_nome;
     End;

     if (Objabamenurelatorio.LocalizaObjetoAba(NomeObjeto,'3')=True) Then
     Begin
              Objabamenurelatorio.TabelaparaObjeto;
              Paginas.Pages[3].Caption:=Objabamenurelatorio.get_nome;
     End;

     if (Objabamenurelatorio.LocalizaObjetoAba(NomeObjeto,'4')=True) Then
     Begin
              Objabamenurelatorio.TabelaparaObjeto;
              Paginas.Pages[4].Caption:=Objabamenurelatorio.get_nome;
     End;

     LBGrupo1.Caption:=Paginas.Pages[0].Caption;
     LBGrupo2.Caption:=Paginas.Pages[1].Caption;
     LBGrupo3.Caption:=Paginas.Pages[2].Caption;
     LBGrupo4.Caption:=Paginas.Pages[3].Caption;
     LBGrupo5.Caption:=Paginas.Pages[4].Caption;


end;

procedure TFMenuRelatorios.btBtokClick(Sender: TObject);
begin
     case Paginas.TabIndex of
         0:Begin
                if (Rgopcoes1.ItemIndex<0)
                Then exit;
                itemescolhido:=Self.CopiaNumero(Rgopcoes1.Items[Rgopcoes1.ItemIndex]);
                //comeca em 1 porem os items comecam em zero
                itemescolhido:=itemescolhido-1;
         End;
         1:Begin
                if (Rgopcoes2.ItemIndex<0)
                Then exit;
                itemescolhido:=Self.CopiaNumero(Rgopcoes2.Items[Rgopcoes2.ItemIndex]);
                //comeca em 1 porem os items comecam em zero
                itemescolhido:=itemescolhido-1;
         End;
         2:Begin
                if (Rgopcoes3.ItemIndex<0)
                Then exit;
                itemescolhido:=Self.CopiaNumero(Rgopcoes3.Items[Rgopcoes3.ItemIndex]);
                //comeca em 1 porem os items comecam em zero
                itemescolhido:=itemescolhido-1;
         End;
         3:Begin
                if (Rgopcoes4.ItemIndex<0)
                Then exit;
                itemescolhido:=Self.CopiaNumero(Rgopcoes4.Items[Rgopcoes4.ItemIndex]);
                //comeca em 1 porem os items comecam em zero
                itemescolhido:=itemescolhido-1;
         End;
         4:Begin
                if (Rgopcoes5.ItemIndex<0)
                Then exit;
                itemescolhido:=Self.CopiaNumero(Rgopcoes5.Items[Rgopcoes5.ItemIndex]);
                //comeca em 1 porem os items comecam em zero
                itemescolhido:=itemescolhido-1;
         End;

     End;
     Self.Tag:=1;//indica que escolheu algo
     Self.Close;
end;

procedure TFMenuRelatorios.PaginasChange(Sender: TObject);
begin

     case Paginas.TabIndex of
           0:Begin
                  if (Rgopcoes1.Items.count>0)
                  Then Rgopcoes1.ItemIndex:=0;
           End;
           1:Begin
                  if (Rgopcoes2.Items.count>0)
                  Then Rgopcoes2.ItemIndex:=0;
           End;
           2:Begin
                  if (Rgopcoes3.Items.count>0)
                  Then Rgopcoes3.ItemIndex:=0;
           End;

           3:Begin
                  if (Rgopcoes4.Items.count>0)
                  Then Rgopcoes4.ItemIndex:=0;
           End;
           4:Begin
                  if (Rgopcoes5.Items.count>0)
                  Then Rgopcoes5.ItemIndex:=0;
           End;
     End;
end;

function TFMenuRelatorios.CopiaNumero(StrLinha: String): integer;
var
cont:integer;
tmpstr:string;
begin
     tmpstr:='';
     for cont:=2 to length(StrLinha) do
     Begin
          if (StrLinha[cont]='-')
          Then break
          Else tmpstr:=tmpstr+StrLinha[cont];
     End;
     tmpstr:=Trim(tmpstr);

     Try
        strtoint(tmpstr);
        result:=strtoint(tmpstr);
     Except
           result:=-1;
     End;
end;

procedure TFMenuRelatorios.DistribuiMenu;
var
temp,cont:integer;
begin
  Rgopcoes1.Items.clear;
  Rgopcoes2.Items.clear;
  Rgopcoes3.Items.clear;
  Rgopcoes4.Items.clear;
  Rgopcoes5.Items.clear;

  for cont:=0 to Rgopcoes.Items.Count-1 do
  Begin
       //se n�o encontrar nada referente aquele item na tabela
       //fica na Aba1

       if (NomeObjeto<>'')
       Then Begin
                temp:=Self.CopiaNumero(rgopcoes.items[cont]);
                if (objmenurelatorio.LocalizaObjetoItem(NomeObjeto,inttostr(temp))=False)
                Then Rgopcoes1.items.add(Rgopcoes.items[cont])
                Else Begin
                          objmenurelatorio.TabelaparaObjeto;
                          case strtoint(objmenurelatorio.Get_Aba) of
                            0:Rgopcoes1.items.add(Rgopcoes.items[cont]);
                            1:Rgopcoes2.items.add(Rgopcoes.items[cont]);
                            2:Rgopcoes3.items.add(Rgopcoes.items[cont]);
                            3:Rgopcoes4.items.add(Rgopcoes.items[cont]);
                            4:Rgopcoes5.items.add(Rgopcoes.items[cont]);
                          End;
                End;
       End
       Else Rgopcoes1.items.add(Rgopcoes.items[cont]);
  End;

end;

Procedure TFmenuRelatorios.GravaNovaPosicao(Paba:Integer);
var
itemescolhido:integer;
begin

     if (NomeObjeto='')
     Then exit;

     if(PermiteAlterar=False)
     Then exit;

     //descobrindo em qual Rgopcaoestou
     case Paginas.TabIndex of
        0:Begin
               if (Rgopcoes1.ItemIndex<0)
               Then exit;
               ItemEscolhido:=Self.CopiaNumero(Rgopcoes1.Items[Rgopcoes1.itemindex]);

        End;
        1:Begin
               if (Rgopcoes2.ItemIndex<0)
               Then exit;
               ItemEscolhido:=Self.CopiaNumero(Rgopcoes2.Items[Rgopcoes2.itemindex]);
        End;
        2:Begin
               if (Rgopcoes3.ItemIndex<0)
               Then exit;
               ItemEscolhido:=Self.CopiaNumero(Rgopcoes3.Items[Rgopcoes3.itemindex]);
        End;
        3:Begin
               if (Rgopcoes4.ItemIndex<0)
               Then exit;
               ItemEscolhido:=Self.CopiaNumero(Rgopcoes4.Items[Rgopcoes4.itemindex]);
        End;
        4:Begin
               if (Rgopcoes5.ItemIndex<0)
               Then exit;
               ItemEscolhido:=Self.CopiaNumero(Rgopcoes5.Items[Rgopcoes5.itemindex]);
        End;
     End;

     //de posse do item escolhido basta gravar no banco e reorganizar

     objmenurelatorio.ZerarTabela;

     if (objmenurelatorio.LocalizaObjetoItem(NomeObjeto,inttostr(itemescolhido))=False)
     Then Begin
               objmenurelatorio.Status:=dsInsert;
               objmenurelatorio.Submit_CODIGO('0');
     End
     Else Begin
               objmenurelatorio.TabelaparaObjeto;
               objmenurelatorio.Status:=dsEdit;
     End;
     objmenurelatorio.Submit_Objeto(NomeObjeto);
     objmenurelatorio.Submit_Item(inttostr(itemescolhido));
     objmenurelatorio.Submit_Aba(IntToStr(Paba));
     if (objmenurelatorio.Salvar(true)=False)
     Then Begin
               Messagedlg('N�o foi poss�vel salvar!',mterror,[mbok],0);
               exit;
     End;
     Self.DistribuiMenu;

     
     

     
     

end;

procedure TFMenuRelatorios.MenuGrupo01Click(Sender: TObject);
begin
    Self.GravaNovaPosicao(0);
end;

procedure TFMenuRelatorios.MenuGrupo02Click(Sender: TObject);
begin
Self.GravaNovaPosicao(1);
end;

procedure TFMenuRelatorios.MenuGrupo03Click(Sender: TObject);
begin
Self.GravaNovaPosicao(2);
end;

procedure TFMenuRelatorios.MenuGrupo04Click(Sender: TObject);
begin
Self.GravaNovaPosicao(3);
end;

procedure TFMenuRelatorios.MenuGrupo05Click(Sender: TObject);
begin
Self.GravaNovaPosicao(4);
end;

end.
