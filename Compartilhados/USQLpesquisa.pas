unit USQLpesquisa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DB, DBGrids, ComCtrls, StdCtrls,IBCustomDataSet, IBTable, IBQuery, IBStoredProc, IBDatabase,
  IBEvents, Mask, Buttons, UessencialGlobal;

type
  TFSQLpesquisa = class(TForm)
    DBGrid: TDBGrid;
    StatusBar1: TStatusBar;
    edtbusca: TMaskEdit;
    DataSource1: TDataSource;
    querypesq: TIBQuery;
    BTSAIR: TBitBtn;
    Memosql: TMemo;
    btpesquisa: TBitBtn;
    btcomando: TBitBtn;
    SpeedButton1: TSpeedButton;
    OpenDialog: TOpenDialog;
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure edtbuscaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtbuscaExit(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure BTSAIRClick(Sender: TObject);
    procedure btpesquisaClick(Sender: TObject);
    procedure btcomandoClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);

  private
      FormCadastro:Tform;
      str_pegacampo:string;
      comandosql:string;

    { Private declarations }
  public
        { Public declarations }
  end;

var
     FSQLpesquisa: TFSQLpesquisa;

implementation

uses UDataModulo;


{$R *.DFM}

procedure TFSQLpesquisa.DBGridKeyPress(Sender: TObject; var Key: Char);
begin

  Case Key of

  #32: Begin
               str_pegacampo:=dbgrid.SelectedField.FieldName;
               Case dbgrid.SelectedField.datatype of

                 ftdatetime :  Begin
                                 edtBusca.EditMask:='00/00/0000 00:00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftdate    :  Begin
                                 edtBusca.EditMask:='00/00/0000;1;_';
                                 EdtBusca.width:=70;
                              End;
                 ftTime    :  Begin
                                 edtBusca.EditMask:='00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftInteger  :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;

                 ftbcd      :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftfloat    :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftString   :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=400;
                                EdtBusca.maxlength:=255;
                             End;
                 Else Begin
                           Messagedlg('Est� tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                           DBGrid.setfocus;
                           exit;
                      End;

               End;
               EdtBusca.Text :='';
               EdtBusca.Visible :=true;
               EdtBusca.SetFocus;
       End;
  #13: Begin
            if  Self.QueryPesq.recordcount<>0
            Then Self.modalresult:=mrok
            Else Self.modalresult:=mrCancel;
       End;
  #27:Begin
           Self.modalresult:=mrcancel;
      End;
  End;

end;

procedure TFSQLpesquisa.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if edtbusca.Visible = true
    then  edtbusca.Visible:= false;

    if (Self.QueryPesq.RecordCount=0)
    Then Self.modalresult:=mrCancel;
end;

procedure TFSQLpesquisa.FormActivate(Sender: TObject);
begin
     PegaFiguraBotoes(nil,nil,nil,nil,nil,nil,nil,btsair);
     PegaFiguraBotao(btpesquisa,'botaopesquisar.bmp');
     PegaFiguraBotao(btcomando,'botaopesquisar.bmp');


end;

procedure TFSQLpesquisa.edtbuscaKeyPress(Sender: TObject; var Key: Char);
var
 int_busca:integer;
 str_busca:string;
 flt_busca:Currency;
 data_busca:Tdate;
 hora_busca:Ttime;
 datahora_busca:tdatetime;
 indice_grid:integer;
begin

   if key =#27//ESC
   then Begin
           EdtBusca.Visible := false;
           dbgrid.SetFocus;
           exit;
        End;
   If Key=#13//procura
   Then Begin
          //ordenando pela coluna antes de procurar
          If ( Self.QueryPesq.recordcount>0)
          Then Begin
            try
             indice_grid:=self.DBGrid.SelectedIndex;
             Self.QueryPesq.close;
             Self.QueryPesq.sql.clear;
             Self.QueryPesq.sql.add(comandosql+' order by '+str_pegacampo);
             Self.QueryPesq.open;
             formatadbgrid(dbgrid,querypesq);
             self.DBGrid.SelectedIndex:=indice_grid;
           except
           end;
          end;



             Case dbgrid.SelectedField.DataType of


             ftstring    :Begin
                               //utilizar a pesquisa com like "testar"S
                               try
                                        indice_grid:=self.DBGrid.SelectedIndex;
                                        Self.QueryPesq.close;
                                        Self.QueryPesq.sql.clear;
                                        If(Pos('WHERE',UpperCase(comandosql))<>0)
                                        Then Self.QueryPesq.sql.add(comandosql+' and UPPER('+str_pegacampo+') like '+#39+edtbusca.text+#39)
                                        Else Self.QueryPesq.sql.add(comandosql+' where UPPER('+str_pegacampo+') like '+#39+edtbusca.text+#39);

                                        Self.QueryPesq.sql.add(' order by '+str_pegacampo);
                                        Self.QueryPesq.open;
                                        //self.querypesq.SQL.SaveToFile('c:\ronnei.txt');
                                        formatadbgrid(dbgrid,querypesq);
                                        self.DBGrid.SelectedIndex:=indice_grid;
                                except
                                end;
                                //Self.QueryPesq.Locate(str_pegacampo,edtbusca.text,[Lopartialkey]);
                          End;
             ftinteger   :Begin
                              try int_busca:=Strtoint(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,int_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftfloat     :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftBcd      :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftDate     :Begin
                              try data_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,data_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             fttime     :Begin
                              try hora_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,hora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;

             ftDateTime :Begin
                              try datahora_busca:=Strtodatetime(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,datahora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             Else Begin
                       Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                  End;
             End;


             Dbgrid.SetFocus;
             exit;
        End;

//Aqui defino as regras que podem ser digitadas
//como estou utilizando maskEdit Para Datas e Horas
//N�o preciso pois defini uma mascara, mas para
//float e Integer preciso

        Case dbgrid.SelectedField.DataType OF

                ftInteger:  Begin
                                 if not (Key in ['0'..'9',#8])
                                 Then key:=#0;
                            End;
                ftFloat  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftCurrency  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftBCD  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;


        End;
End;


procedure TFSQLpesquisa.DBGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
indice_grid:integer;
cont,marca_registro:integer ;
comandoordena:String;
begin
     if (key=VK_Delete)
     then begin
          dbgrid.Columns.Items[dbgrid.SelectedIndex].Visible :=false;
          dbgrid.SelectedIndex :=dbgrid.SelectedIndex +1;
     end;



     if (key=VK_f12)
     Then Begin
                //If ( Self.QueryPesq.recordcount>0)
                //Then Begin
                     try
                         str_pegacampo:=DbGrid.SelectedField.FieldName;
                         indice_grid:=self.DBGrid.SelectedIndex;
                         marca_registro:= self.DBGrid.DataSource.DataSet.fieldbyname('codigo').AsInteger;
                         ComandoOrdena:=comandosql+' order by ';

                         If (ssCtrl in Shift)
                         Then Begin  //Control pressionado, sinal de ordem por mais de uma coluna
                                   //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
                                   If(Pos('order ',Self.QueryPesq.sql.text)<>0)
                                   Then ComandoOrdena:=Self.QueryPesq.sql.text+','
                                   Else ComandoOrdena:=comandosql+' order by ';
                              End;

                         Self.QueryPesq.close;
                         Self.QueryPesq.sql.clear;
                         Self.QueryPesq.sql.add(Comandoordena+str_pegacampo);
                         Self.QueryPesq.open;
                        // formatadbgrid(dbgrid,querypesq);
                         //self.DBGrid.DataSource.DataSet.Locate( 'codigo',marca_registro,[loCaseInsensitive]);
                         //self.DBGrid.SelectedIndex:=indice_grid;
                         //self.DBGrid.DataSource.DataSet.GotoBookmark(marca_registro);

                     except

                     end;
                //End
                //Else Begin
                 //       If (Self.QueryPesq.recordcount=0)
                 //       Then messagedlg('N�o Existem Registros para Serem Ordenados!',mterror,[mbok],0)
                  //      Else messagedlg('Selecione um Campo para Ordena��o e Pressione Ordenar!',mtwarning,[mbok],0);
                   //  End;
                DbGrid.setfocus;
          End
     Else Begin
               if key=VK_F11
               Then Begin
                         for cont:=0 to dbgrid.Columns.Count-1 do
                         dbgrid.Columns.Items[cont].Visible :=true;
               End;
     End;


end;


procedure TFSQLpesquisa.edtbuscaExit(Sender: TObject);
begin
     edtbusca.Visible:=False;
end;





procedure TFSQLpesquisa.DBGridDblClick(Sender: TObject);
begin
    if  Self.QueryPesq.recordcount<>0
    Then Self.modalresult:=mrok
    Else Self.modalresult:=mrCancel;
    
end;


procedure TFSQLpesquisa.BTSAIRClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TFSQLpesquisa.btpesquisaClick(Sender: TObject);
begin

     querypesq.close;
     querypesq.database:=Fdatamodulo.ibdatabase;
     querypesq.SQL.clear;
     querypesq.SQL.text:=Memosql.text;
     querypesq.Open;
     comandosql:=querypesq.SQL.text;

end;

procedure TFSQLpesquisa.btcomandoClick(Sender: TObject);
begin
     querypesq.close;
     querypesq.database:=Fdatamodulo.ibdatabase;
     querypesq.SQL.clear;
     querypesq.SQL.text:=Memosql.text;
     querypesq.ExecSQL;

end;

procedure TFSQLpesquisa.SpeedButton1Click(Sender: TObject);
var
QLocal:tibquery;
PathAtualiza:String;
begin
     PathAtualiza:='';
     If (OpenDialog.Execute)
     Then PathAtualiza:=OPenDialog.FileName
     Else exit;

     Try
        Memosql.Lines.LoadFromFile(PathAtualiza);
     Except
        Messagedlg('Erro na tentativa de abertura do arquivo!',mterror,[mbok],0);
        exit;
     End;



end;

end.


