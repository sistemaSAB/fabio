unit UpesquisaPersonalizada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGrids, Mask,DB, IBCustomDataSet,
  IBQuery;

type
  TfPesquisaPersonalizada = class(TForm)
    PanelCabecalho: TPanel;
    Label1: TLabel;
    PanelEscolheResultado: TPanel;
    Label3: TLabel;
    lbNomeCampo: TLabel;
    DBGrid: TDBGrid;
    Panel1: TPanel;
    edtbusca: TMaskEdit;
    rbExata: TRadioButton;
    rbInciciaCom: TRadioButton;
    rbTenhaemQualquerParte: TRadioButton;
    PanelInformacoes: TPanel;
    ImageRodape: TImage;
    Label4: TLabel;
    lbNomeCampoInformacao: TLabel;
    Label5: TLabel;
    lbValorDigitado: TLabel;
    lbQtdeRegistrosEncontrados: TLabel;
    lbCadastrar: TLabel;
    lbSair: TLabel;
    labelrodape: TLabel;
    QueryColunas: TIBQuery;
    querypesq: TIBQuery;
    DataSource1: TDataSource;
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure edtbuscaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridCellClick(Column: TColumn);
    procedure edtbuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbCadastrarClick(Sender: TObject);
    procedure lbSairClick(Sender: TObject);
    procedure lbCadastrarMouseLeave(Sender: TObject);
    procedure lbCadastrarMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormDestroy(Sender: TObject);
  private
     strWhere:TStringList;
     VetorLabel : Array of TLabel;
     FormCadastro:Tform;
     comandosql:string;
     ComplementoPorcentagem:string;//usado para por % na pesquisa de nome automaticamente de acordo com um parametro
    procedure LimpaLabel;
    procedure PersonalizaColunas;
    procedure PersonalizaGrid;
    procedure criaLabel(labelName: string);
    procedure doubleClickLabel(Sender: TObject);

    { Private declarations }
  public
    PesquisaEmEstoque:Boolean;
    Invisiveis:TStringList;
    str_pegacampo,str_valorpesquisado,NomeCadastroPersonalizacao:string;
    str_campopesquisainicial,str_valorcampopesquisainicial:String;
    function PreparaPesquisa(comando, TitulodoForm: String;FormCad: Tform): Boolean;overload;
    function PreparaPesquisa(comando: TStringList;TitulodoForm: String; FormCad: Tform): Boolean; overload;
  end;

var
  fPesquisaPersonalizada: TfPesquisaPersonalizada;

implementation

uses UescolheImagemBotao, UDataModulo, UessencialGlobal;

{$R *.dfm}

procedure TfPesquisaPersonalizada.DBGridKeyPress(Sender: TObject;var Key: Char);
begin

  rbInciciaCom.Visible:=false;
  rbExata.Visible:=false;
  rbTenhaemQualquerParte.Visible:=false;

  Case (Key) of

    #32:
    Begin

      edtbusca.text:='';
      str_pegacampo:=dbgrid.SelectedField.FieldName;
      EdtBusca.Visible :=true;
      EdtBusca.SetFocus;

      //cria label

      if lbNomeCampo.Caption = '' then
        lbNomeCampo.Caption := dbgrid.SelectedField.FieldName
      else
        lbNomeCampo.Caption := lbNomeCampo.Caption+' + '+dbgrid.SelectedField.FieldName;

      Case dbgrid.SelectedField.datatype of

        ftdatetime:
        Begin
          edtBusca.EditMask:='00/00/0000 00:00:00';
          EdtBusca.width:=300;
        End;

        ftdate:
        Begin
          edtBusca.EditMask:='00/00/0000;1;_';
          EdtBusca.width:=130;
        End;

        ftTime:
        Begin
          edtBusca.EditMask:='00:00';
          EdtBusca.width:=130;
        End;

        ftInteger:
        Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;

        ftLargeint:
        Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;

        ftbcd:
        Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;

        ftfloat:
        Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=300;
          EdtBusca.maxlength:=15
        End;

        ftString:
        Begin
          edtBusca.EditMask:='';
          EdtBusca.width:=700;
          EdtBusca.maxlength:=255;
          rbTenhaemQualquerParte.Visible:=true;
          rbInciciaCom.Visible:=true;
          rbExata.Visible:=true;
        End;

      Else

        Begin
          Messagedlg('Este tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
          DBGrid.setfocus;
          exit;
        End;

      End;

    End;

    #13:
    Begin

      if  Self.QueryPesq.recordcount <> 0 Then
        Self.modalresult:=mrok
      Else
        Self.modalresult:=mrCancel;

    End;

    #27:
    Begin
      Self.modalresult:=mrcancel;
    End;

  end;

end;

procedure TfPesquisaPersonalizada.FormCreate(Sender: TObject);
begin
  Self.NomeCadastroPersonalizacao:='';
  Self.str_valorcampopesquisainicial:='';
  Self.str_campopesquisainicial:='';
  Self.str_valorpesquisado:='';
  Self.PesquisaEmEstoque:=false;
  self.strWhere:=TStringList.Create;
end;

procedure TfPesquisaPersonalizada.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraImagem(ImageRodape,'RODAPE');
     //lbNomeCampo.Caption:=dbgrid.SelectedField.FieldName;
     Self.LimpaLabel;
     strWhere.Clear;
end;

procedure TfPesquisaPersonalizada.LimpaLabel;
begin
      lbNomeCampo.Caption:='';
      lbNomeCampoInformacao.Caption:='';
      lbValorDigitado.Caption:='';
      lbQtdeRegistrosEncontrados.Caption:='';
end;

function TfPesquisaPersonalizada.PreparaPesquisa(comando: string;TitulodoForm:String;FormCad:Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando); 
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TfPesquisaPersonalizada.PreparaPesquisa(comando: TStringList;TitulodoForm: String; FormCad: Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;


end;

procedure TfPesquisaPersonalizada.FormActivate(Sender: TObject);
var
  cont:integer;
  enter:char;
begin
     UessencialGlobal.PegaCorForm(Self);
     Self.Tag:=0;
     
     Case Self.QueryPesq.state of

        dsInactive: Begin
                        Messagedlg('A tabela n�o foi preparada antes de chamar o form!', mterror,[mbok],0);
                        abort;
                    End;
        Else Begin
                ComplementoPorcentagem:='';
                comandosql:='';
                comandosql:= Self.QueryPesq.SQL.Text;
                if (Self.Tag=3)
                Then Formatadbgrid_3_casas(dbgrid)
                Else Formatadbgrid(dbgrid);

                dbgrid.setfocus;
                If (ObjParametroGlobal.ValidaParametro('% PADRAO NA PESQUISA EM CAMPOS NOME')=True)
                Then Begin
                          
                          If (ObjParametroGlobal.Get_Valor='SIM')
                          Then ComplementoPorcentagem:='%'
                          Else ComplementoPorcentagem:='';
                End;

                if (Invisiveis<>nil)
                Then Begin
                          for cont:=0 to Invisiveis.Count-1 do
                          Begin
                               try
                                  strtoint(Invisiveis[cont]);
                                  if (DBGrid.Columns.Count>Cont)
                                  Then dbgrid.Columns.Items[strtoint(Invisiveis[cont])].Visible :=False;
                               except
                               end;

                          End;
                End;

             End;
     End;

     if (Self.str_campopesquisainicial<>'') and (Self.str_valorcampopesquisainicial<>'')
     Then Begin
               if (DBGrid.DataSource.DataSet.Fields.FindField(str_campopesquisainicial)<>nil)
               Then Begin
                         dbgrid.SelectedField:=DBGrid.DataSource.DataSet.Fields.FindField(str_campopesquisainicial);
                         self.str_pegacampo:=Self.str_campopesquisainicial;
                         edtbusca.text:=Self.str_valorcampopesquisainicial;
                         Enter:=#13;
                         edtbuscaKeyPress(edtbusca,enter);
               End;
     End;

     if (Self.NomeCadastroPersonalizacao<>'')
     Then Self.labelrodape.Caption:=Self.labelrodape.Caption+'/F3 - Personaliza Colunas';

     Self.PersonalizaGrid;

end;

procedure TfPesquisaPersonalizada.edtbuscaKeyPress(Sender: TObject;var Key: Char);
var
 str_busca,OrderBy:string;
 indice_grid:integer;
begin

   if key =#27//ESC
   then Begin
           lbNomeCampo.Caption := '';
           strWhere.Clear;
           Self.str_valorpesquisado:='';
           EdtBusca.Visible := false;
           lbQtdeRegistrosEncontrados.Caption:='';
           dbgrid.SetFocus;
           exit;
   End;

   //enter
   If Key=#13 Then
   Begin

    if (edtbusca.Text='') then
      exit;

    Self.str_valorpesquisado:='';
    str_busca:=edtbusca.Text;
    OrderBy:='';

    Case dbgrid.SelectedField.DataType of

      ftstring:
      Begin

        if (rbTenhaemQualquerParte.Checked=true) then
          str_busca:='%'+str_busca+'%';

        if (rbInciciaCom.Checked=true) then
          str_busca:=str_busca+'%';

        str_busca:=' UPPER('+str_pegacampo+') like '+#39+str_busca+#39;
        OrderBy:= ' order by '+str_pegacampo;

      End;

      ftinteger:
      Begin
        str_busca := str_pegacampo+' = '+trim(str_busca);
      End;

      ftLargeint:
      Begin
        str_busca := str_pegacampo+' = '+trim(str_busca)
      end;

      ftfloat:
      Begin
        str_busca := tira_ponto(str_busca);
        str_busca := virgulaparaponto(str_busca);
        str_busca := str_pegacampo+' = '+trim(str_busca)
      End;

      ftBcd:
      Begin
        str_busca := tira_ponto(str_busca);
        str_busca := virgulaparaponto(str_busca);
        str_busca := str_pegacampo+' = '+trim(str_busca)
      End;

      ftDate:
      Begin
        str_busca := #39+FormatDateTime('mm/dd/yyyy', StrToDate(str_busca))+#39;
        str_busca := str_pegacampo+' = '+trim(str_busca)
      End;

      fttime:
      Begin
        str_busca := str_pegacampo+' = '+trim(str_busca)
      End;

      ftDateTime:
      Begin
        str_busca := str_pegacampo+' = '+trim(str_busca)
      End;

    Else

      Begin
        Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
      End;

    end;

    if (strWhere.Count = 0) then
      strWhere.Add(str_busca)
    else
      strWhere.Add('and '+str_busca);


    indice_grid:=self.DBGrid.SelectedIndex;
    Self.QueryPesq.close;
    Self.QueryPesq.sql.clear;

    {If(Pos('WHERE',UpperCase(comandosql))<>0) Then
      Self.QueryPesq.sql.add(comandosql+' and '+str_busca)
    Else
      Self.QueryPesq.sql.add(comandosql+' where '+str_busca);}

    if (Pos('where',comandosql) = 0) then
      self.querypesq.SQL.Add(comandosql+' where '+ strWhere.Text)
    else
      self.querypesq.SQL.Add(comandosql+' and '+ strWhere.Text);

    Self.QueryPesq.sql.add(OrderBy);

    //InputBox('','',self.querypesq.SQL.Text);

    try
      Self.QueryPesq.open;
    except
      MensagemErro('Erro na pesquisa !');
      exit;
    end;


    lbQtdeRegistrosEncontrados.Caption:='';
    if (Self.querypesq.RecordCount=0) then
      lbQtdeRegistrosEncontrados.Caption:='Nenhum registro ecncontrado, tecle F10 para Pesquisa Completa';

    lbNomeCampoInformacao.Caption:=str_pegacampo;
    lbValorDigitado.Caption:=edtbusca.Text;

    Self.str_valorpesquisado:=edtbusca.text;
    edtbusca.Text:='';


    Formatadbgrid(dbgrid);
    Formatadbgrid(dbgrid,querypesq);
    Self.Personalizagrid;

    //self.DBGrid.SelectedIndex:=indice_grid;
    edtbusca.Visible:=False;
    rbInciciaCom.Visible:=false;
    rbExata.Visible:=false;
    rbTenhaemQualquerParte.Visible:=false;
    Dbgrid.SetFocus;
    exit;

   End;


  //Aqui defino as regras que podem ser digitadas
  //como estou utilizando maskEdit Para Datas e Horas
  //N�o preciso pois defini uma mascara, mas para
  //float e Integer preciso

        Case dbgrid.SelectedField.DataType OF

                ftInteger:  Begin
                                 if not (Key in ['0'..'9',#8])
                                 Then key:=#0;
                            End;
                ftFloat  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftCurrency  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftBCD  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;


        End;

end;

procedure TfPesquisaPersonalizada.PersonalizaColunas;
begin
     ObjCamposPesquisa_Global.ConfiguraColunas(Self.DBGrid,Self.NomeCadastroPersonalizacao);
     Self.PersonalizaGrid;
end;

procedure TfPesquisaPersonalizada.PersonalizaGrid;
begin
    ObjCamposPesquisa_Global.Personalizagrid(Self.DBGrid,self.NomeCadastroPersonalizacao);
end;

procedure TfPesquisaPersonalizada.DBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin

    if (Self.PesquisaEmEstoque = false)  // Pesquisa comum
    then Begin
            (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

            If not Odd(Self.querypesq.RecNo) then
            If not (gdselected in state)
            Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

            (Sender as TdbGrid).canvas.FillRect(rect);
            (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);
    end else
    Begin   // Pesquisa com cores diferente comformeo estoque
            (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

            If not Odd(Self.querypesq.RecNo) then
            If not (gdselected in state)
            Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

            (Sender as TdbGrid).canvas.FillRect(rect);
            (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);


             if (DBGrid.DataSource.DataSet.FieldList.IndexOf('estoque')<>-1)
             Then Begin
                      if (DBGrid.DataSource.DataSet.FieldByName('estoque').asfloat<0)
                      Then Begin
                                dbgrid.canvas.Font.color:=rgb(153,0,0);
                                dbgrid.DefaultDrawDataCell(rect,dbgrid.Columns[datacol].Field,state);
                      End
                      Else Begin
                               if (DBGrid.DataSource.DataSet.FieldList.IndexOf('estoqueminimo')=-1)
                               Then exit;

                               if (DBGrid.DataSource.DataSet.FieldByName('estoque').asfloat<=DBGrid.DataSource.DataSet.FieldList.IndexOf('estoqueminimo'))
                               Then Begin
                                         dbgrid.canvas.Font.color:=RGB(0,121,0);
                                         dbgrid.DefaultDrawDataCell(rect,dbgrid.Columns[datacol].Field,state);
                               End;
                      End;
             End;
        end;

end;

procedure TfPesquisaPersonalizada.DBGridKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
indice_grid:integer;
cont,marca_registro:integer ;
comandoordena:String;
ColunaAtual:integer;
begin
     if (key=VK_Delete)
     then begin
               dbgrid.Columns.Items[dbgrid.SelectedIndex].Visible :=false;
               dbgrid.SelectedIndex :=dbgrid.SelectedIndex +1;
     end;

     If (key=VK_F10)
     then Begin
               Self.QueryPesq.close;
               Self.QueryPesq.sql.clear;
               Self.QueryPesq.sql.add(comandosql);
               //InputBox('','',comandosql);
               Self.QueryPesq.open;
               DbGrid.SelectedIndex:=ColunaAtual;
               Formatadbgrid(dbgrid,querypesq);
               Self.Personalizagrid;
     end;

     if (key=VK_f12)
     Then Begin


                     try
                         ColunaAtual:=DbGrid.SelectedIndex;
                         str_pegacampo:=DbGrid.SelectedField.FieldName;
                         if (Length(str_pegacampo)>4)
                         Then Begin
                                   if (uppercase(copy(str_pegacampo,1,2))='XX')
                                   and (uppercase(copy(str_pegacampo,length(str_pegacampo)-1,2))='XX')
                                   Then exit;
                         End;

                         indice_grid:=self.DBGrid.SelectedIndex;
                         marca_registro:= self.DBGrid.DataSource.DataSet.fieldbyname('codigo').AsInteger;
                         ComandoOrdena:=comandosql+' order by ';

                         (*If (ssCtrl in Shift) Then
                         Begin

                          //Control pressionado, sinal de ordem por mais de uma coluna

                          //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
                          If(Pos('order ',Self.QueryPesq.sql.text)<>0) Then
                            ComandoOrdena:=Self.QueryPesq.sql.text+','
                          Else
                            ComandoOrdena:=comandosql+' order by ';

                         End; *)

                         Self.QueryPesq.close;
                         Self.QueryPesq.sql.clear;

                         //self.querypesq.SQL.Add(comandosql+' where '+ strWhere.Text+' order by '+str_pegacampo);

                         if strWhere.Count = 0 then
                          self.querypesq.SQL.Add(comandosql)
                         else
                         begin

                          if (Pos('where',comandosql) = 0) then
                            self.querypesq.SQL.Add(comandosql+' where '+ strWhere.Text)
                          else
                           self.querypesq.SQL.Add(comandosql+' and '+ strWhere.Text);
                           
                         end;

                         querypesq.SQL.Add('order by '+str_pegacampo);

                         Self.QueryPesq.open;
                         DbGrid.SelectedIndex:=ColunaAtual;
                         Formatadbgrid(dbgrid,querypesq);
                         Self.Personalizagrid;

                        //self.DBGrid.DataSource.DataSet.Locate( 'codigo',marca_registro,[loCaseInsensitive]);
                        //self.DBGrid.SelectedIndex:=indice_grid;
                        //self.DBGrid.DataSource.DataSet.GotoBookmark(marca_registro);

                     except

                     end;
                //End
                //Else Begin
                 //       If (Self.QueryPesq.recordcount=0)
                 //       Then messagedlg('N�o Existem Registros para Serem Ordenados!',mterror,[mbok],0)
                  //      Else messagedlg('Selecione um Campo para Ordena��o e Pressione Ordenar!',mtwarning,[mbok],0);
                   //  End;
                DbGrid.setfocus;
          End
     Else Begin
               if key=VK_F11
               Then Begin
                         for cont:=0 to dbgrid.Columns.Count-1 do
                         dbgrid.Columns.Items[cont].Visible :=true;
               End
               Else Begin
                         if ((key=VK_f3) and (Self.NomeCadastroPersonalizacao<>''))
                         Then Begin
                                   Self.personalizacolunas;
                         End;
               End;
     End;

end;

procedure TfPesquisaPersonalizada.DBGridKeyUp(Sender: TObject;var Key: Word; Shift: TShiftState);
begin
   //lbNomeCampo.Caption:=dbgrid.SelectedField.FieldName;
end;

procedure TfPesquisaPersonalizada.DBGridDblClick(Sender: TObject);
begin
    if  Self.QueryPesq.recordcount<>0
    Then Self.modalresult:=mrok
    Else Self.modalresult:=mrCancel;
end;

procedure TfPesquisaPersonalizada.DBGridCellClick(Column: TColumn);
begin
  //lbNomeCampo.Caption:=dbgrid.SelectedField.FieldName;
end;

procedure TfPesquisaPersonalizada.edtbuscaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     If (key=VK_F10)
     then Begin
               Self.QueryPesq.close;
               Self.QueryPesq.sql.clear;
               Self.QueryPesq.sql.add(comandosql);
               Self.QueryPesq.open;
               Formatadbgrid(dbgrid,querypesq);
               Self.Personalizagrid;
     end;

end;

procedure TfPesquisaPersonalizada.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     If(ssalt in Shift) and (key=67)
     Then lbCadastrarClick(sender);

     If(ssalt in Shift) and (key=83)
     Then lbSairClick(sender);
end;

procedure TfPesquisaPersonalizada.lbCadastrarClick(Sender: TObject);
begin
     If (Self.FormCadastro<>nil)
     Then Begin

               IF Self.FormCadastro.Visible=False
               Then Self.FormCadastro.showmodal
               Else Self.FormCadastro.show;

               Self.QueryPesq.close;
               Self.QueryPesq.open;
               Self.DBGrid.setfocus;
          End;
end;

procedure TfPesquisaPersonalizada.lbSairClick(Sender: TObject);
begin
  Self.Close;
end;

procedure TfPesquisaPersonalizada.lbCadastrarMouseLeave(Sender: TObject);
begin
 TLabel(Sender).Font.Style:=[fsbold];
end;

procedure TfPesquisaPersonalizada.lbCadastrarMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  TLabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TfPesquisaPersonalizada.criaLabel(labelName: string);
var
  i,NewLenght:Integer;
begin

  (*
  //NewLenght := self.painelFiltros.ComponentCount+1;

  SetLength(VetorLabel,NewLenght);

  VetorLabel[NewLenght-1]         := TLabel.Create(painelFiltros);
  VetorLabel[NewLenght-1].Parent  := painelFiltros;
  VetorLabel[NewLenght-1].Align   := alLeft;
  VetorLabel[NewLenght-1].Layout  := tlCenter;
  VetorLabel[NewLenght-1].Caption := labelName+'  ';

  for  i := 0 to High(VetorLabel) do
  begin
    ShowMessage(VetorLabel[i].Caption);
  end;  *)


end;


procedure TfPesquisaPersonalizada.doubleClickLabel(Sender: TObject);
begin
  //Tlabel
end;

procedure TfPesquisaPersonalizada.FormDestroy(Sender: TObject);
begin
  FreeAndNil(strWhere);
end;

end.
