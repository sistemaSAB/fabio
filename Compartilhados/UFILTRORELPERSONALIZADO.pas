unit UFILTRORELPERSONALIZADO;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ExtCtrls, ComCtrls,Uobjfiltrorelpersonalizadoextendido;

type
  TFfiltrorelpersonalizado = class(TForm)
    SpeedButton1: TSpeedButton;
    PanelBotao: TPanel;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    BotaoModelo: TSpeedButton;
    botaosetacima: TSpeedButton;
    botaosetabaixo: TSpeedButton;
    Label1: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure botaosetabaixoClick(Sender: TObject);
    procedure botaosetacimaClick(Sender: TObject);
  private



    procedure LimpaOnExit(Sender: TObject);


    { Private declarations }
  Public
    { Public declarations }
         ObjFiltro:tobjfiltrorelpersonalizadoextendido;
         ContaEdits:integer;
         ContaTotal:integer;

         Grupo   :array [1..30] of TPanel;
         LbGrupo :array [1..30] of TLabel;
         edtgrupo:array [1..30] of TMaskEdit;
         btgrupo :array [1..30] of TSpeedButton;
         ListBoxGrupo :array [1..30] of TListBox;

         function  MascaraData: String;
         function  MascaraHora: String;
         Function  Validadata(PGrupo:Integer;var Pdata:String;Pdesconsideraembranco:boolean):boolean;overload;
         Function  Validadata(PGrupo:Integer;var Pdata:Tdate;Pdesconsideraembranco:boolean):boolean;overload;
         procedure EdtSomenteNumerosKeyPress(Sender: TObject; var Key: Char);
         procedure AbreCalendario(Sender: TObject);
         procedure destroi;
         procedure EditKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
         procedure ListBoxDragDrop(Sender, Source: TObject; X, Y: Integer);
         procedure ListBoxDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
         procedure ListBoxMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

  End;

var
  Ffiltrorelpersonalizado: TFfiltrorelpersonalizado;
  StartingPoint : TPoint;




implementation

uses UessencialGlobal, Ucalendario, UescolheImagemBotao, Upesquisa;

{$R *.DFM}

procedure TFfiltrorelpersonalizado.FormActivate(Sender: TObject);
var
Cont:integer;
begin
     FescolheImagemBotao.PegaFiguraBotoes(nil,nil,btcancelar,btgravar,nil,nil,nil,nil);
     for cont:=1 to ContaEdits do
     Begin
          if ((edtgrupo[cont].EditMask='!99/99/9999;1;_') or (edtgrupo[cont].EditMask='!99/99/9999;') or (edtgrupo[cont].EditMask='99/99/9999'))
          Then btgrupo[cont].Visible:=True
          Else btgrupo[cont].Visible:=False;
     End;

    
     Self.edtgrupo[1].setfocus;
end;

procedure TFfiltrorelpersonalizado.BtgravarClick(Sender: TObject);
begin
     Tag:=1;
     close;
End;

procedure TFfiltrorelpersonalizado.BtCancelarClick(Sender: TObject);
begin
     Tag:=0;
     close;
end;

procedure TFfiltrorelpersonalizado.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else
         if (key=#27)
         Then Begin
                  Self.Tag:=0;
                  Self.close;
         End;
end;


procedure TFfiltrorelpersonalizado.AbreCalendario(Sender: TObject);
var
editenviou:Tobject;
begin
     editenviou:=Self.edtgrupo[TspeedButton(sender).tag];
     Fcalendario.EditEnviouGlobal:=editenviou;
     Fcalendario.Left:=Self.Left+Tedit(editenviou).left;
     Fcalendario.top:=Self.Top+Tedit(editenviou).top;
     Fcalendario.Calendario.Date:=now;
     Fcalendario.showmodal;
end;



procedure TFfiltrorelpersonalizado.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;

Function TFfiltrorelpersonalizado.MascaraData:String;
Begin
   //usado em editmask do Self
   result:='!99/99/9999;1;_';
End;
Function TFfiltrorelpersonalizado.MascaraHora:String;
Begin
   result:='!99:99;1;_';
End;

function TFfiltrorelpersonalizado.Validadata(PGrupo: Integer; var Pdata:String;Pdesconsideraembranco:boolean): boolean;
var
pdatatemp:Tdate;
begin
     pdata:='';
     result:=Validadata(PGrupo,Pdatatemp,Pdesconsideraembranco);
     if (result=true)
     then pdata:=datetostr(Pdatatemp);
End;

function TFfiltrorelpersonalizado.Validadata(PGrupo: Integer; var Pdata:Tdate;Pdesconsideraembranco:boolean): boolean;
var
Tempedit,templabel:String;
begin
     result:=False;
     with Self do
     Begin
       //o pgrupo indica o Grupo que devo validar e o pdata � o retorno

       Case PGrupo of
         1:Begin
                Tempedit:=edtgrupo[pgrupo].Text;
                templabel:=LbGrupo[pgrupo].caption;
         End;
       end;

       try
          if (Pdesconsideraembranco=True)
          Then Begin
                    if (comebarra(tempedit)<>'')
                    Then Pdata:=strtodate(Tempedit);
          End
          Else Pdata:=strtodate(Tempedit);

          result:=true;
       except
             Messagedlg(templabel+' Inv�lida',mterror,[mbok],0);
             exit;
       End;


     End;
end;
procedure TFfiltrorelpersonalizado.EdtSomenteNumerosKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not(key in['0'..'9',#8])
     Then key:=#0;
end;
procedure TFfiltrorelpersonalizado.LimpaOnExit(Sender: TObject);
begin
     // Esse procedimento fica vazio mesmo
end;
procedure TFfiltrorelpersonalizado.destroi;
var
cont:integer;
begin
     for cont:=Self.ContaTotal downto 1 do
     Begin
          freeandnil(Self.grupo[cont]);
     End;
end;

procedure TFfiltrorelpersonalizado.EditKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
pcodigo:string;
FpesquisaLocal:Tfpesquisa;
begin

     if (key<>vk_f9)
     Then exit;

     Pcodigo:=inttostr(TMaskEdit(sender).Tag);

     if (Self.ObjFiltro.LocalizaCodigo(pcodigo)=False)
     Then exit;

     Self.ObjFiltro.TabelaparaObjeto;

     if ((Self.ObjFiltro.Get_comandosqlchaveestrangeira='') or (Self.ObjFiltro.Get_campochavestrangeira=''))
     Then exit;


     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjFiltro.Get_comandosqlchaveestrangeira,'',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 if ((Self.ObjFiltro.Get_multiplaescolha='S') and (TEdit(Sender).text<>''))
                                 Then TEdit(Sender).text:=TEdit(Sender).text+';'+FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjFiltro.Get_campochavestrangeira).asstring
                                 Else TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjFiltro.Get_campochavestrangeira).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


procedure TFfiltrorelpersonalizado.ListBoxDragDrop(Sender, Source: TObject;
  X, Y: Integer);
var
  DropPosition, StartPosition: Integer;
  DropPoint: TPoint;
begin
    DropPoint.X := X;
    DropPoint.Y := Y;
    with Source as TListBox do
    begin
      StartPosition := ItemAtPos(StartingPoint,True);
      DropPosition := ItemAtPos(DropPoint,True);
      Items.Move(StartPosition, DropPosition);
    end;
end; 

procedure TFfiltrorelpersonalizado.ListBoxDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
       Accept := Source = TListbox(Sender);
end;

procedure TFfiltrorelpersonalizado.ListBoxMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
    StartingPoint.X := X;
    StartingPoint.Y := Y;
end;

procedure TFfiltrorelpersonalizado.botaosetabaixoClick(Sender: TObject);
var
temp:string;
begin
     if (Self.ListBoxGrupo[TSpeedButton(sender).Tag].ItemIndex>=0)//do primeiro pra cima
     and (Self.ListBoxGrupo[TSpeedButton(sender).Tag].ItemIndex<(Self.ListBoxGrupo[TSpeedButton(sender).Tag].Items.count-1))//ultimo nao da
     Then Begin
               temp:=Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex];
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex]:=Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex+1];
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex+1]:=temp;
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].ItemIndex:=Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex+1;
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].SetFocus;
     End;
end;

procedure TFfiltrorelpersonalizado.botaosetacimaClick(Sender: TObject);
var
temp:string;
begin
     if (Self.ListBoxGrupo[TSpeedButton(sender).Tag].ItemIndex>0)//primeiro nao
     and (Self.ListBoxGrupo[TSpeedButton(sender).Tag].ItemIndex<=(Self.ListBoxGrupo[TSpeedButton(sender).Tag].Items.count-1))//ultimo pode
     Then Begin
               temp:=Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex];
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex]:=Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex-1];
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].items[Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex-1]:=temp;
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].ItemIndex:=Self.ListBoxGrupo[TSpeedButton(sender).Tag].itemindex-1;
               Self.ListBoxGrupo[TSpeedButton(sender).Tag].SetFocus;

     End;
end;

End.
