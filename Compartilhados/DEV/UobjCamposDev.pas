unit UobjCamposDev;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UOBJCONFDEV;

(*******************************************************************************
*** * * * * * * * * * *  * * * DICIONARIO DE DADOS * * * ** * *  * * *  * * * **

NOME:string;                  CAMPO COM O NOME DO CAMPO NO BANCO DE DADOS
COMPONENTE:string;            TIPO DE COMPONENTE (TEDIT, TLABEL, TRADIOGROUP, TCOMBOBOX
caption:string;               CAPTION (SOMENTE EM CASOS LABEL E RADIOGROUP)
TIPO:string;                  TIPO NO BANCO DE DADOS (INTEGER, DECIMAL, VARCHAR....)

CAMPOSBD:string;              INDICA SE � UM CAMPO QUE SERA GRAVADO NO BD (LABEL POR EXEMPLO NAO GRAVA)

  VERIFICABRANCO:string;            INDICA SE FARA A VERIFICACAO DE BRANCO
  VERIFICANUMERICO:string;          INDICA SE FARA A VERIFICACAO DE NUMERICO
  VERIFICADATA:string;              INDICA SE FARA A VERIFICACAO DE DATA
  VERIFICAHORA:string;              INDICA SE FARA A VERIFICACAO DE HORA
  VERIFICARELACIONAMENTO:string;    INDICA SE FARA A VERIFICACAO DE RELACIONAMENTO
  verificafaixa:string;             INDICA SE FARA A VERIFICACAO DE FAIXA
  FAIXAVERIFICACAO:string;          Separar os valores por | indicando o que tem q ser na faixa (Exemplo M|F) s� pode ser M ou F
  ITEMS:string;                     ITEMS NO CASO DE COMBOBOX

MASCARA:string;               MASCARA EM CASO DE TMASKEDIT
QUANTIDADECARACTERES:string;  QUANTIDADE DE CARACTERES NO EDI
PosicaoEsquerda:String;       POSICAO LEFT NO FORMULARIO
PosicaoSuperior:String;       POSICAO TOP NO FORMULARIO
TabOrder:String;              POSICAO TABORDER NO FORMULARIO

*******************************************************************************)


Type
   TObjCAMPOSDEV=class

          Public
                Status                                      :TDataSetState;
                SqlInicial                                  :String;
                ListaCodigos:TStringList;
                ValorAtual:String;

                Constructor Create;
                Destructor  Free;

                procedure LimpaCampos;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;

                Procedure Submit_NOME(parametro: string);
                Function Get_NOME: string;

                Procedure Submit_caption(parametro: string);
                Function Get_caption: string;

                Procedure Submit_COMPONENTE(parametro: string);
                Function Get_COMPONENTE: string;
                Procedure Submit_TIPO(parametro: string);
                Function Get_TIPO: string;
                Procedure Submit_VERIFICABRANCO(parametro: string);
                Function Get_VERIFICABRANCO: string;

                Procedure Submit_verificafaixa(parametro: string);
                Function Get_verificafaixa: string;
                Procedure Submit_VERIFICANUMERICO(parametro: string);
                Function Get_VERIFICANUMERICO: string;
                Procedure Submit_VERIFICADATA(parametro: string);
                Function Get_VERIFICADATA: string;
                Procedure Submit_VERIFICAHORA(parametro: string);
                Function Get_VERIFICAHORA: string;
                Procedure Submit_VERIFICARELACIONAMENTO(parametro: string);
                Function Get_VERIFICARELACIONAMENTO: string;
                Procedure Submit_FAIXAVERIFICACAO(parametro: string);
                Function Get_FAIXAVERIFICACAO: string;
                Procedure Submit_ITEMS(parametro: string);
                Function Get_ITEMS: string;
                Procedure Submit_MASCARA(parametro: string);
                Function Get_MASCARA: string;
                Procedure Submit_QUANTIDADECARACTERES(parametro: string);
                Function Get_QUANTIDADECARACTERES: string;
                Procedure Submit_CAMPOSBD(parametro: string);
                Function Get_CAMPOSBD: string;

               Function Get_PosicaoEsquerda:String;
               Function Get_PosicaoSuperior:String;
               Function Get_TabOrder:String;

               Procedure Submit_PosicaoEsquerda(parametro:String);
               Procedure Submit_PosicaoSuperior(parametro:String);
               Procedure Submit_TabOrder(parametro:String);

         Protected
               CODIGO:string;

               NOME:string;
               COMPONENTE:string;
               caption:string;
               TIPO:string;
               VERIFICABRANCO:string;
               verificafaixa:string;
               VERIFICANUMERICO:string;
               VERIFICADATA:string;
               VERIFICAHORA:string;
               VERIFICARELACIONAMENTO:string;
               FAIXAVERIFICACAO:string;
               ITEMS:string;
               MASCARA:string;
               QUANTIDADECARACTERES:string;
               CAMPOSBD:string;
               PosicaoEsquerda:String;
               PosicaoSuperior:String;
               TabOrder:String;

               ParametroPesquisa:TStringList;

               
   End;


implementation

uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UmenuRelatorios,
  UCONFDEV;


procedure TObjCAMPOSDEV.LimpaCampos;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NOME:='';
        COMPONENTE:='';
        caption:='';
        TIPO:='';
        VERIFICABRANCO:='';
        verificafaixa:='';
        VERIFICANUMERICO:='';
        VERIFICADATA:='';
        VERIFICAHORA:='';
        VERIFICARELACIONAMENTO:='';
        FAIXAVERIFICACAO:='';
        ITEMS:='';
        MASCARA:='';
        QUANTIDADECARACTERES:='';
        CAMPOSBD:='';

        PosicaoEsquerda:='';
        PosicaoSuperior:='';
        TabOrder:='';
     End;
end;


constructor TObjCAMPOSDEV.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin



end;
destructor TObjCAMPOSDEV.Free;
begin

end;


procedure TObjCAMPOSDEV.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjCAMPOSDEV.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjCAMPOSDEV.Submit_NOME(parametro: string);
begin
        Self.NOME:=Parametro;
end;
function TObjCAMPOSDEV.Get_NOME: string;
begin
        Result:=Self.NOME;
end;
procedure TObjCAMPOSDEV.Submit_COMPONENTE(parametro: string);
begin
        Self.COMPONENTE:=Parametro;
end;
function TObjCAMPOSDEV.Get_COMPONENTE: string;
begin
        Result:=Self.COMPONENTE;
end;


procedure TObjCAMPOSDEV.Submit_caption(parametro: string);
begin
        Self.caption:=Parametro;
end;
function TObjCAMPOSDEV.Get_caption: string;
begin
        Result:=Self.caption;
end;


procedure TObjCAMPOSDEV.Submit_TIPO(parametro: string);
begin
        Self.TIPO:=Parametro;
end;
function TObjCAMPOSDEV.Get_TIPO: string;
begin
        Result:=Self.TIPO;
end;
procedure TObjCAMPOSDEV.Submit_VERIFICABRANCO(parametro: string);
begin
        Self.VERIFICABRANCO:=Parametro;
end;
function TObjCAMPOSDEV.Get_VERIFICABRANCO: string;
begin
        Result:=Self.VERIFICABRANCO;
end;



procedure TObjCAMPOSDEV.Submit_verificafaixa(parametro: string);
begin
        Self.verificafaixa:=Parametro;
end;
function TObjCAMPOSDEV.Get_verificafaixa: string;
begin
        Result:=Self.verificafaixa;
end;


procedure TObjCAMPOSDEV.Submit_VERIFICANUMERICO(parametro: string);
begin
        Self.VERIFICANUMERICO:=Parametro;
end;
function TObjCAMPOSDEV.Get_VERIFICANUMERICO: string;
begin
        Result:=Self.VERIFICANUMERICO;
end;
procedure TObjCAMPOSDEV.Submit_VERIFICADATA(parametro: string);
begin
        Self.VERIFICADATA:=Parametro;
end;
function TObjCAMPOSDEV.Get_VERIFICADATA: string;
begin
        Result:=Self.VERIFICADATA;
end;
procedure TObjCAMPOSDEV.Submit_VERIFICAHORA(parametro: string);
begin
        Self.VERIFICAHORA:=Parametro;
end;
function TObjCAMPOSDEV.Get_VERIFICAHORA: string;
begin
        Result:=Self.VERIFICAHORA;
end;
procedure TObjCAMPOSDEV.Submit_VERIFICARELACIONAMENTO(parametro: string);
begin
        Self.VERIFICARELACIONAMENTO:=Parametro;
end;
function TObjCAMPOSDEV.Get_VERIFICARELACIONAMENTO: string;
begin
        Result:=Self.VERIFICARELACIONAMENTO;
end;
procedure TObjCAMPOSDEV.Submit_FAIXAVERIFICACAO(parametro: string);
begin
        Self.FAIXAVERIFICACAO:=Parametro;
end;
function TObjCAMPOSDEV.Get_FAIXAVERIFICACAO: string;
begin
        Result:=Self.FAIXAVERIFICACAO;
end;
procedure TObjCAMPOSDEV.Submit_ITEMS(parametro: string);
begin
        Self.ITEMS:=Parametro;
end;
function TObjCAMPOSDEV.Get_ITEMS: string;
begin
        Result:=Self.ITEMS;
end;
procedure TObjCAMPOSDEV.Submit_MASCARA(parametro: string);
begin
        Self.MASCARA:=Parametro;
end;
function TObjCAMPOSDEV.Get_MASCARA: string;
begin
        Result:=Self.MASCARA;
end;
procedure TObjCAMPOSDEV.Submit_QUANTIDADECARACTERES(parametro: string);
begin
        Self.QUANTIDADECARACTERES:=Parametro;
end;
function TObjCAMPOSDEV.Get_QUANTIDADECARACTERES: string;
begin
        Result:=Self.QUANTIDADECARACTERES;
end;
procedure TObjCAMPOSDEV.Submit_CAMPOSBD(parametro: string);
begin
        Self.CAMPOSBD:=Parametro;
end;
function TObjCAMPOSDEV.Get_CAMPOSBD: string;
begin
        Result:=Self.CAMPOSBD;
end;
//CODIFICA GETSESUBMITS

function TObjCAMPOSDEV.Get_PosicaoEsquerda: String;
begin
     Result:=Self.PosicaoEsquerda;
end;

function TObjCAMPOSDEV.Get_PosicaoSuperior: String;
begin
     Result:=Self.PosicaoSuperior;
end;

function TObjCAMPOSDEV.Get_TabOrder: String;
begin
     Result:=Self.TabOrder;
end;

procedure TObjCAMPOSDEV.Submit_PosicaoEsquerda(parametro: String);
begin
     Self.PosicaoEsquerda:=parametro;
end;

procedure TObjCAMPOSDEV.Submit_PosicaoSuperior(parametro: String);
begin
     Self.PosicaoSuperior:=parametro;
end;

procedure TObjCAMPOSDEV.Submit_TabOrder(parametro: String);
begin
     Self.TabOrder:=parametro;
end;

end.



