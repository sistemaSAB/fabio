unit UDEV;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjDEV,UobjCONFCAMPOSDEV,
  jpeg,uobjcamposdev;

type
  TFDEV = class(TForm)
    Guia: TTabbedNotebook;
    panelbotes: TPanel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    Panelprincipal: TPanel;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    PanelMensagens: TPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btrelatoriosClick(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
  private
         ObjDev:TObjDEV;
         PcodigoConf:String;
         ModoDev:boolean;//indica se � para criar novos campos, modo de desenvolvimento
         ComponenteSelecionadoTipo,ComponenteSelecionadoNome:String;

         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         procedure InsereComponente;
         Function  MensagemDev:boolean;

         procedure EditClickDEV(Sender: TObject);
         procedure LabelClickDev(Sender:TObject);
         procedure ComboBoxClickDev(Sender: TObject);
         procedure RadioGroupClickDev(Sender: TObject);


    { Private declarations }
  public
    { Public declarations }
         Function Inicializa(Pcodigo:String;PmodoDev:Boolean):Boolean;
         Function CriaComponente(Parametro:TobjCamposDev):boolean;
  end;

var
  FDEV: TFDEV;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UobjectInspectorDEV;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFDEV.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjDev do
    Begin
        Submit_CODIGO(edtCODIGO.text);


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFDEV.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjDEV do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFDEV.TabelaParaControles: Boolean;
begin
     If (Self.ObjDEV.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFDEV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjDEV=Nil)
     Then exit;

     If (Self.ObjDEV.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjDEV.free;
end;

procedure TFDEV.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFDEV.BtnovoClick(Sender: TObject);
begin
     if (Self.MensagemDev)
     Then exit;


     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjDEV.status:=dsInsert;
     Guia.pageindex:=0;
     //edtCAPTION.setfocus;

end;


procedure TFDEV.btalterarClick(Sender: TObject);
begin
     if (Self.MensagemDev)
     Then exit;

    If (Self.ObjDEV.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjDEV.Status:=dsEdit;
                guia.pageindex:=0;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                //edtCAPTION.setfocus;
                
    End;


end;

procedure TFDEV.btgravarClick(Sender: TObject);
begin
     if (Self.MensagemDev)
     Then exit;
     
     If Self.ObjDEV.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjDEV.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjDEV.Get_codigo;
     Self.ObjDEV.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFDEV.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     if (Self.MensagemDev)
     Then exit;
     
     If (Self.ObjDEV.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjDEV.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjDEV.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFDEV.btcancelarClick(Sender: TObject);
begin
     if (Self.MensagemDev)
     Then exit;
     
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFDEV.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFDEV.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin
     if (Self.MensagemDev)
     Then exit;

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjDEV.Get_pesquisa,Self.ObjDEV.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjDEV.status<>dsinactive
                                  then exit;

                                  If (Self.ObjDEV.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjDEV.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFDEV.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFDEV.FormShow(Sender: TObject);
begin
     //PegaCorForm(Self);
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;


     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     //FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=$00824E00;
     retira_fundo_labels(self);

     if (Self.ModoDev)
     Then Self.EdtCODIGO.Enabled:=True;//� usado como focus em situacoes de radiogroup que invalida o onkeydown

end;
//CODIFICA ONKEYDOWN E ONEXIT


Function TFDEV.Inicializa(Pcodigo: String;PmodoDev:Boolean):boolean;
var
cont:integer;
begin
     result:=False;
     Self.PCodigoConf:=pcodigo;
     Self.ModoDev:=PmodoDev;
     Self.ComponenteSelecionadoTipo:='';
     Self.ComponenteSelecionadoNome:='';

     Try
        Self.ObjDEV:=TObjDEV.create;

     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;


     Try

        if (Self.ObjDEV.ConfCamposDEV.CONFDEV.LocalizaCodigo(pcodigo)=False)
        Then Begin
                  raise Exception.create('Formul�rio '+pcodigo+' n�o encontrado');//para for�ar o except
                  exit;
        End;
        
        With Self.ObjDEV.ConfCamposDEV.CONFDEV do
        Begin
            TabelaparaObjeto;
            Self.Height:=strtoint(Get_TAMANHOVERTICAL);
            Self.Width:=strtoint(Get_TAMANHOHORIZONTAL);
            Self.Caption:=Get_CAPTION;
        End;

        Self.ObjDev.PreencheVetor(Pcodigo);

        for cont:=0 to Self.ObjDev.StrCodigo.count-1 do
        Begin
             Self.CriaComponente(Self.ObjDev.vCamposDev[cont]);
        End;

        if (Self.ModoDev)
        Then FobjectInspectorDev.PassaObjeto(Self.ObjDev);


        result:=true;
        
     Except
           on e:exception do
           Begin
                 MensagemErro(e.message);
                 Self.ObjDEV.Free;
                 exit;
           End;
     End;
     
end;

function TFDEV.CriaComponente(Parametro:TobjCamposDev): boolean;
begin
     With Parametro do
     Begin
         if  (uppercase(Get_COMPONENTE)='TEDIT')
         Then Begin
                

                with TEdit.Create(Self.Panelprincipal)  do
                begin
                     Parent:=Self.Panelprincipal;
                     Height:=19;

                     if (strtoint(Get_QUANTIDADECARACTERES)>0)
                     then Width:=(strtoint(Get_QUANTIDADECARACTERES)*8)
                     Else Width:=80;

                     tag:=strtoint(get_codigo);

                     Left:=strtoint(Get_PosicaoEsquerda);
                     Top:=strtoint(Get_PosicaoSuperior);
                     
                     Name:='CAMPO'+Get_codigo;
                     text:='';
                     Font:=EdtCODIGO.Font;

                     if (Self.ModoDev)
                     then Begin
                                onClick:=Self.EditClickDEV;
                                FobjectInspectorDEV.ComboComponente.Items.add(name);
                     End;

                     

                End;
         End
         else
         if  (uppercase(Get_COMPONENTE)='TLABEL')
         Then Begin


                with TLabel.Create(Self.Panelprincipal)  do
                begin
                     AutoSize:=true;
                     Parent:=Self.Panelprincipal;
                     Height:=Self.LbCODIGO.Height;
                     tag:=strtoint(get_codigo);
                     Left:=strtoint(Get_PosicaoEsquerda);
                     Top:=strtoint(Get_PosicaoSuperior);
                     Name:='CAMPO'+Get_codigo;
                     Font:=Self.lbcodigo.Font;
                     caption:=Get_caption;
                     
                     if (Self.ModoDev)
                     then Begin
                              onclick:=Self.LabelClickDev;
                              FobjectInspectorDEV.ComboComponente.Items.add(name);
                     End;
                End;
         End
         else
         if  (uppercase(Get_COMPONENTE)='TCOMBOBOX')
         Then Begin
                with TComboBox.Create(Self.Panelprincipal)  do
                begin
                     AutoSize:=true;
                     Parent:=Self.Panelprincipal;
                     Height:=Self.LbCODIGO.Height;
                     tag:=strtoint(get_codigo);
                     Left:=strtoint(Get_PosicaoEsquerda);
                     Top:=strtoint(Get_PosicaoSuperior);
                     Name:='CAMPO'+Get_codigo;
                     Font:=Self.lbcodigo.Font;
                     caption:=Get_caption;
                     Items.Text:=StringReplace(Get_ITEMS,'|',#13,[rfReplaceAll]);
                     text:=' ';
                     if (Self.ModoDev)
                     then Begin
                              onclick:=Self.ComboBoxClickDev;
                              FobjectInspectorDEV.ComboComponente.Items.add(name);
                     End;
                End;
         End
         else
         if  (uppercase(Get_COMPONENTE)='TRADIOGROUP')
         Then Begin
                with TRadioGroup.Create(Self.Panelprincipal)  do
                begin
                     AutoSize:=true;
                     Parent:=Self.Panelprincipal;
                     Height:=105;
                     width:=185;
                     tag:=strtoint(get_codigo);
                     Left:=strtoint(Get_PosicaoEsquerda);
                     Top:=strtoint(Get_PosicaoSuperior);
                     Name:='CAMPO'+Get_codigo;
                     Font:=Self.lbcodigo.Font;
                     caption:=Get_caption;
                     Items.Text:=StringReplace(Get_ITEMS,'|',#13,[rfReplaceAll]);

                     if (Self.ModoDev)
                     then Begin
                              onclick:=Self.RadioGroupClickDev;
                              FobjectInspectorDEV.ComboComponente.Items.add(name);
                     End;
                End;
         End;

     End;



end;

procedure TFDEV.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
Labeltemp:Tlabel;
ComboTemp:Tcombobox;
RadioTemp:TradioGroup;
EditTemp:TEdit;
begin
     if (Self.ModoDev=False)
     Then exit;

     //Daqui para baixo tudo � relativo ao Modo Dev

     if (Key=vk_insert)
     then Self.InsereComponente;

     if (key=vk_f11)
     Then Begin
               FobjectInspectorDEV.Show;
     End;


     If ((ssCtrl in Shift) and ((key=vk_left) or (key=vk_right)))or

        ((ssCtrl in Shift) and ((key=vk_up) or (key=vk_down)))

     Then Begin
               if (Self.ComponenteSelecionadoTipo='TLABEL')
               Then Begin
                         LabelTemp:=nil;
                         LabelTemp:=(Self.panelprincipal.FindComponent(Self.ComponenteSelecionadoNome) as TLabel);

                         if (LabelTemp<>nil)
                         then Begin
                                   if (Key=vk_left)
                                   Then LabelTemp.left:=LabelTemp.left-1;

                                   if (Key=vk_right)
                                   Then LabelTemp.left:=LabelTemp.left+1;

                                   if (Key=vk_up)
                                   Then LabelTemp.top:=LabelTemp.top-1;

                                   if (Key=vk_down)
                                   Then LabelTemp.top:=LabelTemp.top+1;
                         End;
               End
               else
               if (Self.ComponenteSelecionadoTipo='TEDIT')
               Then Begin
                         EDITTemp:=nil;
                         EDITTemp:=(Self.panelprincipal.FindComponent(Self.ComponenteSelecionadoNome) as TEDIT);

                         if (EDITTemp<>nil)
                         then Begin
                                   if (Key=vk_left)
                                   Then EDITTemp.left:=EDITTemp.left-1;

                                   if (Key=vk_right)
                                   Then EDITTemp.left:=EDITTemp.left+1;

                                   if (Key=vk_up)
                                   Then EDITTemp.top:=EDITTemp.top-1;

                                   if (Key=vk_down)
                                   Then EDITTemp.top:=EDITTemp.top+1;
                         End;
               End
               else
               if (Self.ComponenteSelecionadoTipo='TCOMBOBOX')
               Then Begin
                         ComboTemp:=nil;
                         ComboTemp:=(Self.panelprincipal.FindComponent(Self.ComponenteSelecionadoNome) as TComboBox);

                         if (combotemp<>nil)
                         then Begin
                                   if (Key=vk_left)
                                   Then combotemp.left:=combotemp.left-1;

                                   if (Key=vk_right)
                                   Then combotemp.left:=combotemp.left+1;

                                   if (Key=vk_up)
                                   Then combotemp.top:=combotemp.top-1;

                                   if (Key=vk_down)
                                   Then combotemp.top:=combotemp.top+1;
                         End;
               End
               else
               if (Self.ComponenteSelecionadoTipo='TRADIOGROUP')
               Then Begin
                         RadioTemp:=nil;
                         RadioTemp:=(Self.panelprincipal.FindComponent(Self.ComponenteSelecionadoNome) as TRadioGroup);

                         if (RadioTemp<>nil)
                         then Begin
                                   if (Key=vk_left)
                                   Then RadioTemp.left:=RadioTemp.left-1;

                                   if (Key=vk_right)
                                   Then RadioTemp.left:=RadioTemp.left+1;

                                   if (Key=vk_up)
                                   Then RadioTemp.top:=RadioTemp.top-1;

                                   if (Key=vk_down)
                                   Then RadioTemp.top:=RadioTemp.top+1;
                         End;
               End;

     End;
end;

procedure TFDEV.InsereComponente;
var
pcodigo:string;
begin
      if (Self.objDev.InsereNovoComponente(PcodigoConf,pcodigo)=False)
      then exit;


      if (Self.ObjDev.InsereVetor(pcodigo)=False)
      Then exit;
      
      Self.CriaComponente(Self.ObjDev.VcamposDev[Self.ObjDev.strcodigo.count-1]);
      
end;

procedure TFDEV.btrelatoriosClick(Sender: TObject);
begin
     if (Self.MensagemDev)
     Then exit;
     
end;

procedure TFDEV.btopcoesClick(Sender: TObject);
begin
     if (Self.MensagemDev)
     Then exit;
     
end;

function TFDEV.MensagemDev: boolean;
begin
     result:=Self.ModoDev;

     if (Self.ModoDev)
     Then MensagemAviso('Formul�rio em Modo de Desenvolvimento');

end;

procedure TFDEV.EditClickDEV(Sender: TObject);
begin
     if (Self.ModoDev=False)
     Then exit;

     Self.ComponenteSelecionadoTipo:='TEDIT';
     Self.ComponenteSelecionadoNome:=TEdit(Sender).name;
     Self.PanelMensagens.Caption:=Self.ComponenteSelecionadoNome;

     FobjectInspectorDEV.CarregaPropriedades(Self.ComponenteSelecionadoNome);

     
end;

procedure TFDEV.LabelClickDev(Sender: TObject);
begin
     if (Self.ModoDev=False)
     Then exit;


     Self.ComponenteSelecionadoTipo:='TLABEL';
     Self.ComponenteSelecionadoNome:=Tlabel(Sender).name;
     Self.PanelMensagens.Caption:=Self.ComponenteSelecionadoNome;

     FobjectInspectorDEV.CarregaPropriedades(Self.ComponenteSelecionadoNome);

     Self.edtcodigo.SetFocus;
end;

procedure TFDEV.ComboBoxClickDev(Sender: TObject);
begin
     if (Self.ModoDev=False)
     Then exit;


     Self.ComponenteSelecionadoTipo:='TCOMBOBOX';
     Self.ComponenteSelecionadoNome:=Tlabel(Sender).name;
     Self.PanelMensagens.Caption:=Self.ComponenteSelecionadoNome;

     FobjectInspectorDEV.CarregaPropriedades(Self.ComponenteSelecionadoNome);
end;

procedure TFDEV.RadioGroupClickDev(Sender: TObject);
begin
     if (Self.ModoDev=False)
     Then exit;


     Self.ComponenteSelecionadoTipo:='TRADIOGROUP';
     Self.ComponenteSelecionadoNome:=Tlabel(Sender).name;
     Self.PanelMensagens.Caption:=Self.ComponenteSelecionadoNome;

     FobjectInspectorDEV.CarregaPropriedades(Self.ComponenteSelecionadoNome);

     Self.edtcodigo.SetFocus;


end;


end.

