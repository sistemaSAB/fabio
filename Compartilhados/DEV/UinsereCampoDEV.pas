unit UinsereCampoDEV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, TabNotBk, CheckLst;

type
  TFinsereCampoDEV = class(TForm)
    RgComponente: TRadioGroup;
    Panel1: TPanel;
    edtcaption: TLabeledEdit;
    PanelBD: TPanel;
    edtnome: TLabeledEdit;
    ComboTipo: TComboBox;
    Label1: TLabel;
    EdtPrecisao: TLabeledEdit;
    edtmascara: TLabeledEdit;
    edtquantidade: TLabeledEdit;
    edtposicaoesquerda: TLabeledEdit;
    edtposicaosuperior: TLabeledEdit;
    edttaborder: TLabeledEdit;
    ChCampoBD: TCheckBox;
    CHKVerifica: TCheckListBox;
    LbFaixaVerificacao: TListBox;
    Label2: TLabel;
    BTok: TButton;
    btcancelar: TButton;
    Label3: TLabel;
    LBItens: TListBox;
    procedure LbFaixaVerificacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure BTokClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure ChCampoBDClick(Sender: TObject);
    procedure LBItensKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FinsereCampoDEV: TFinsereCampoDEV;

implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TFinsereCampoDEV.LbFaixaVerificacaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
temp:String;
begin
     if key=vk_insert
     then Begin
               temp:='';
               if (inputquery('Faixa de Verifica��o','Digite o valor',temp)=False)
               then exit;

               Self.LbFaixaVerificacao.Items.add(temp);
     End;

     if key=vk_delete
     then Begin
               if (MensagemPergunta('Certeza que deseja excluir o item?')=mrno)
               then exit;

               Self.LbFaixaVerificacao.DeleteSelected;
     End;


end;

procedure TFinsereCampoDEV.FormShow(Sender: TObject);
begin
     Self.tag:=0;
     
end;

procedure TFinsereCampoDEV.BTokClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.close;
end;

procedure TFinsereCampoDEV.btcancelarClick(Sender: TObject);
begin
     Self.tag:=0;
     Self.close;
end;

procedure TFinsereCampoDEV.ChCampoBDClick(Sender: TObject);
begin
     panelbd.visible:=ChCampoBD.Checked;
end;

procedure TFinsereCampoDEV.LBItensKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
temp:String;
begin
     if key=vk_insert
     then Begin
               temp:='';
               if (inputquery('Adi��o de Itens','Digite o valor',temp)=False)
               then exit;

               Self.LbItens.Items.add(temp);
     End;

     if key=vk_delete
     then Begin
               if (MensagemPergunta('Certeza que deseja excluir o item?')=mrno)
               then exit;

               Self.LbItens.DeleteSelected;
     End;



end;

end.
