object FinsereCampoDEV: TFinsereCampoDEV
  Left = 72
  Top = 199
  Width = 978
  Height = 338
  Align = alLeft
  Caption = 'M'#243'dulo de Desenvolvimento'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RgComponente: TRadioGroup
    Left = 0
    Top = 0
    Width = 217
    Height = 311
    Align = alLeft
    Caption = 'Tipo de Componente'
    ItemIndex = 0
    Items.Strings = (
      'TEdit'
      'TLabel'
      'TComboBox'
      'TRadioGroup')
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 217
    Top = 0
    Width = 753
    Height = 311
    Align = alClient
    TabOrder = 1
    object Label3: TLabel
      Left = 400
      Top = 144
      Width = 152
      Height = 13
      Caption = 'Itens (ComboBox e RadioGroup)'
    end
    object edtcaption: TLabeledEdit
      Left = 8
      Top = 160
      Width = 249
      Height = 21
      EditLabel.Width = 36
      EditLabel.Height = 13
      EditLabel.Caption = 'Caption'
      TabOrder = 0
    end
    object PanelBD: TPanel
      Left = 1
      Top = 1
      Width = 751
      Height = 136
      Align = alTop
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 72
        Width = 45
        Height = 13
        Caption = 'Tipo (BD)'
      end
      object Label2: TLabel
        Left = 440
        Top = 16
        Width = 96
        Height = 13
        Caption = 'Faixa de Verifica'#231#227'o'
      end
      object edtnome: TLabeledEdit
        Left = 8
        Top = 46
        Width = 121
        Height = 21
        EditLabel.Width = 52
        EditLabel.Height = 13
        EditLabel.Caption = 'Nome (BD)'
        TabOrder = 0
      end
      object ComboTipo: TComboBox
        Left = 8
        Top = 88
        Width = 121
        Height = 21
        ItemHeight = 13
        TabOrder = 1
        Text = '  '
        Items.Strings = (
          'Integer'
          'Decimal'
          'Varchar'
          'Time'
          'Date'
          'TimeStamp')
      end
      object EdtPrecisao: TLabeledEdit
        Left = 129
        Top = 88
        Width = 63
        Height = 21
        EditLabel.Width = 41
        EditLabel.Height = 13
        EditLabel.Caption = 'Precis'#227'o'
        TabOrder = 2
      end
      object CHKVerifica: TCheckListBox
        Left = 208
        Top = 16
        Width = 225
        Height = 97
        ItemHeight = 13
        Items.Strings = (
          'Verifica Branco'
          'Verifica Num'#233'rico'
          'Verifica Data'
          'Verifica Hora'
          'Verifica Relacionamento'
          'Verifica Faixa')
        TabOrder = 3
      end
      object LbFaixaVerificacao: TListBox
        Left = 440
        Top = 32
        Width = 121
        Height = 78
        ItemHeight = 13
        Items.Strings = (
          '0'
          '0'
          '0'
          '0')
        TabOrder = 4
        OnKeyDown = LbFaixaVerificacaoKeyDown
      end
    end
    object edtmascara: TLabeledEdit
      Left = 8
      Top = 200
      Width = 121
      Height = 21
      EditLabel.Width = 96
      EditLabel.Height = 13
      EditLabel.Caption = 'M'#225'scara de Entrada'
      TabOrder = 2
    end
    object edtquantidade: TLabeledEdit
      Left = 8
      Top = 240
      Width = 121
      Height = 21
      EditLabel.Width = 76
      EditLabel.Height = 13
      EditLabel.Caption = 'Qtde caracteres'
      TabOrder = 3
      Text = '0'
    end
    object edtposicaoesquerda: TLabeledEdit
      Left = 144
      Top = 200
      Width = 121
      Height = 21
      EditLabel.Width = 86
      EditLabel.Height = 13
      EditLabel.Caption = 'Posi'#231#227'o Esquerda'
      TabOrder = 4
      Text = '100'
    end
    object edtposicaosuperior: TLabeledEdit
      Left = 144
      Top = 240
      Width = 121
      Height = 21
      EditLabel.Width = 80
      EditLabel.Height = 13
      EditLabel.Caption = 'Posi'#231#227'o Superior'
      TabOrder = 5
      Text = '100'
    end
    object edttaborder: TLabeledEdit
      Left = 272
      Top = 200
      Width = 121
      Height = 21
      EditLabel.Width = 45
      EditLabel.Height = 13
      EditLabel.Caption = 'TabOrder'
      TabOrder = 6
      Text = '0'
    end
    object ChCampoBD: TCheckBox
      Left = 8
      Top = 3
      Width = 97
      Height = 25
      Caption = 'Campo do BD'
      TabOrder = 7
      OnClick = ChCampoBDClick
    end
    object BTok: TButton
      Left = 8
      Top = 278
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 8
      OnClick = BTokClick
    end
    object btcancelar: TButton
      Left = 88
      Top = 278
      Width = 75
      Height = 25
      Caption = '&CANCELAR'
      TabOrder = 9
      OnClick = btcancelarClick
    end
    object LBItens: TListBox
      Left = 400
      Top = 160
      Width = 121
      Height = 137
      ItemHeight = 13
      TabOrder = 10
      OnKeyDown = LBItensKeyDown
    end
  end
end
