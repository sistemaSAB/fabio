object FDEV: TFDEV
  Left = 292
  Top = 228
  Width = 790
  Height = 377
  Caption = 'Cadastro de DEV - EXCLAIM TECNOLOGIA'
  Color = 8539648
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 774
    Height = 318
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object panelbotes: TPanel
        Left = 0
        Top = 0
        Width = 766
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 0
        object Btnovo: TBitBtn
          Left = 2
          Top = -3
          Width = 50
          Height = 52
          Caption = '&n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BtnovoClick
        end
        object btpesquisar: TBitBtn
          Left = 253
          Top = -3
          Width = 50
          Height = 52
          Caption = '&p'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnClick = btpesquisarClick
        end
        object btrelatorios: TBitBtn
          Left = 303
          Top = -3
          Width = 50
          Height = 52
          Caption = '&r'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          OnClick = btrelatoriosClick
        end
        object btalterar: TBitBtn
          Left = 52
          Top = -3
          Width = 52
          Height = 52
          Caption = '&a'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btalterarClick
        end
        object btexcluir: TBitBtn
          Left = 203
          Top = -3
          Width = 50
          Height = 52
          Caption = '&e'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = btexcluirClick
        end
        object btgravar: TBitBtn
          Left = 103
          Top = -3
          Width = 50
          Height = 52
          Caption = '&g'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btgravarClick
        end
        object btcancelar: TBitBtn
          Left = 153
          Top = -3
          Width = 50
          Height = 52
          Caption = '&c'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = btcancelarClick
        end
        object btsair: TBitBtn
          Left = 403
          Top = -3
          Width = 50
          Height = 52
          Caption = '&s'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          OnClick = btsairClick
        end
        object btopcoes: TBitBtn
          Left = 353
          Top = -3
          Width = 50
          Height = 52
          Caption = '&o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          OnClick = btopcoesClick
        end
      end
      object panelrodape: TPanel
        Left = 0
        Top = 240
        Width = 766
        Height = 50
        Align = alBottom
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 1
        DesignSize = (
          766
          50)
        object ImagemRodape: TImage
          Left = 0
          Top = 0
          Width = 766
          Height = 50
          Align = alClient
          Stretch = True
        end
        object lbquantidadeformulario: TLabel
          Left = 514
          Top = 16
          Width = 240
          Height = 18
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Existem X DEV cadastrados'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
      end
      object Panelprincipal: TPanel
        Left = 0
        Top = 50
        Width = 766
        Height = 190
        Align = alClient
        Color = 8539648
        TabOrder = 2
        object LbCODIGO: TLabel
          Left = 5
          Top = 12
          Width = 44
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdtCODIGO: TEdit
          Left = 8
          Top = 28
          Width = 80
          Height = 19
          MaxLength = 9
          TabOrder = 0
          Text = 'aaaaaaaaaa'
        end
      end
    end
  end
  object PanelMensagens: TPanel
    Left = 0
    Top = 318
    Width = 774
    Height = 21
    Align = alBottom
    Alignment = taLeftJustify
    Color = 8539648
    TabOrder = 1
  end
end
