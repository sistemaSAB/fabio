unit UobjectInspectorDEV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls,UobjDEV,uobjcamposdev;

type
  TFobjectInspectorDEV = class(TForm)
    Panel1: TPanel;
    ComboComponente: TComboBox;
    STRGPropriedades: TStringGrid;
    ComboOpcao: TComboBox;
    procedure ComboComponenteChange(Sender: TObject);
    procedure STRGPropriedadesKeyPress(Sender: TObject; var Key: Char);
    procedure STRGPropriedadesSetEditText(Sender: TObject; ACol,
      ARow: Integer; const Value: String);
  private
    { Private declarations }
         PObjDev:TobjDev;
         Procedure LimpaGrid;
  public
    { Public declarations }
      Procedure PassaObjeto(Parametro:TobjDev);
      Procedure CarregaPropriedades(PnomeComponente:String);overload;
      Procedure CarregaPropriedades(PCamposDev:TobjCamposDev);overload;
  end;

var
  FobjectInspectorDEV: TFobjectInspectorDEV;

implementation

{$R *.dfm}

{ TFobjectInspectorDEV }

procedure TFobjectInspectorDEV.CarregaPropriedades(
  PnomeComponente: String);
var
cont:integer;
temp:String;
begin
     temp:=copy(pnomecomponente,6,length(pnomecomponente)-5);

     Self.ComboComponente.ItemIndex:=Self.ComboComponente.Items.IndexOf(pnomecomponente);
     
     for cont:=0 to Self.PObjDev.StrCodigo.count-1 do
     Begin
          if (Self.PObjDev.StrCodigo[cont]=temp)
          Then begin
                    Self.CarregaPropriedades(Self.PObjDev.VCamposDev[cont]);
                    break;
          End;
     End;
end;

procedure TFobjectInspectorDEV.CarregaPropriedades(
  PCamposDev: TobjCamposDev);
var
Linha:integer;
begin
     Self.LimpaGrid;
     STRGPropriedades.rowcount:=50;
     STRGPropriedades.FixedRows:=1;

     WITH PCamposDev do
     begin
          Linha:=0;

          STRGPropriedades.Cells[0,linha]:='NOME';
          STRGPropriedades.Cells[1,linha]:='VALOR';

          inc(linha,1);
          
          STRGPropriedades.Cells[0,linha]:='CODIGO';
          STRGPropriedades.Cells[1,linha]:=Get_CODIGO;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='COMPONENTE';
          STRGPropriedades.Cells[1,linha]:=Get_COMPONENTE;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='NOME';
          STRGPropriedades.Cells[1,linha]:=Get_NOME;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='CAPTION';
          STRGPropriedades.Cells[1,linha]:=Get_caption;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='TIPO';
          STRGPropriedades.Cells[1,linha]:=Get_TIPO;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='CAMPOSBD';
          STRGPropriedades.Cells[1,linha]:=Get_CAMPOSBD;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='VERIFICABRANCO';
          STRGPropriedades.Cells[1,linha]:=Get_VERIFICABRANCO;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='VERIFICAFAIXA';
          STRGPropriedades.Cells[1,linha]:=Get_verificafaixa;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='VERIFICANUMERICO';
          STRGPropriedades.Cells[1,linha]:=Get_VERIFICANUMERICO;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='VERIFICADATA';
          STRGPropriedades.Cells[1,linha]:=Get_VERIFICADATA;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='VERIFICAHORA';
          STRGPropriedades.Cells[1,linha]:=Get_VERIFICAHORA;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='VERIFICARELACIONAMENTO';
          STRGPropriedades.Cells[1,linha]:=Get_VERIFICARELACIONAMENTO;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='FAIXAVERIFICACAO';
          STRGPropriedades.Cells[1,linha]:=Get_FAIXAVERIFICACAO;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='ITEMS';
          STRGPropriedades.Cells[1,linha]:=Get_ITEMS;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='MASCARA';
          STRGPropriedades.Cells[1,linha]:=Get_MASCARA;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='QUANTIDADECARACTERES';
          STRGPropriedades.Cells[1,linha]:=Get_QUANTIDADECARACTERES;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='POSICAOESQUERDA';
          STRGPropriedades.Cells[1,linha]:=Get_PosicaoEsquerda;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='POSICAOSUPERIOR';
          STRGPropriedades.Cells[1,linha]:=Get_PosicaoSuperior;

          inc(linha,1);

          STRGPropriedades.Cells[0,linha]:='TABORDER';
          STRGPropriedades.Cells[1,linha]:=Get_TabOrder;

          inc(linha,1);

          STRGPropriedades.rowcount:=linha;
     End;

end;

procedure TFobjectInspectorDEV.LimpaGrid;
begin
     STRGPropriedades.ColCount:=2;
     STRGPropriedades.rowcount:=1;
     STRGPropriedades.cols[0].clear;
     STRGPropriedades.cols[1].clear;
end;

procedure TFobjectInspectorDEV.PassaObjeto(Parametro: TobjDev);
begin
     Self.PObjDev:=parametro;
end;

procedure TFobjectInspectorDEV.ComboComponenteChange(Sender: TObject);
begin
     Self.CarregaPropriedades(Self.ComboComponente.Text);
end;

procedure TFobjectInspectorDEV.STRGPropriedadesKeyPress(Sender: TObject;
  var Key: Char);
begin
      if (Self.STRGPropriedades.Col=0)
     Then Key:=#0;
end;

procedure TFobjectInspectorDEV.STRGPropriedadesSetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: String);
begin
     if (Acol=1)
     then Begin
               if (Self.STRGPropriedades.cells[0,Arow]='VERIFICABRANCO')
               then begin
                        ComboOpcao.Visible:=True;
                        //ComboOpcao.Left:=

                        ComboOpcao.SetFocus;
               End;
     End;
end;

end.



