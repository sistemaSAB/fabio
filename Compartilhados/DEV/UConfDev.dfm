object FconfDev: TFconfDev
  Left = 208
  Top = 213
  Width = 707
  Height = 449
  Caption = 
    'Configura'#231#227'o de Formul'#225'rio de Desenvolvimento - EXCLAIM TECNOLOG' +
    'IA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 699
    Height = 422
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object LbCODIGO: TLabel
        Left = 5
        Top = 11
        Width = 44
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbCAPTION: TLabel
        Left = 5
        Top = 103
        Width = 49
        Height = 13
        Caption = 'Caption'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbTAMANHOHORIZONTAL: TLabel
        Left = 5
        Top = 149
        Width = 131
        Height = 13
        Caption = 'Tamanho Horizontal'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbTAMANHOVERTICAL: TLabel
        Left = 165
        Top = 149
        Width = 114
        Height = 13
        Caption = 'Tamanho Vertical'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 109
        Top = 11
        Width = 68
        Height = 13
        Caption = 'Permiss'#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbnomepermissao: TLabel
        Left = 173
        Top = 29
        Width = 24
        Height = 13
        Caption = 'XXX'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 5
        Top = 56
        Width = 37
        Height = 13
        Caption = 'Nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 8
        Top = 27
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtCAPTION: TEdit
        Left = 8
        Top = 119
        Width = 654
        Height = 19
        MaxLength = 100
        TabOrder = 3
      end
      object EdtTAMANHOHORIZONTAL: TEdit
        Left = 8
        Top = 165
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 4
      end
      object EdtTAMANHOVERTICAL: TEdit
        Left = 168
        Top = 165
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 5
      end
      object edtpermissao: TEdit
        Left = 112
        Top = 27
        Width = 57
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        OnExit = edtpermissaoExit
        OnKeyDown = edtpermissaoKeyDown
      end
      object Panel1: TPanel
        Left = 0
        Top = 304
        Width = 691
        Height = 90
        Align = alBottom
        Color = 3355443
        TabOrder = 6
        object Btnovo: TBitBtn
          Left = 3
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Novo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BtnovoClick
        end
        object btalterar: TBitBtn
          Left = 115
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Alterar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btalterarClick
        end
        object btgravar: TBitBtn
          Left = 227
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Gravar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btgravarClick
        end
        object btcancelar: TBitBtn
          Left = 339
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Cancelar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = btcancelarClick
        end
        object btpesquisar: TBitBtn
          Left = 3
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Pesquisar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = btpesquisarClick
        end
        object btrelatorios: TBitBtn
          Left = 115
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Relat'#243'rios'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object btexcluir: TBitBtn
          Left = 227
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Excluir'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          OnClick = btexcluirClick
        end
        object btsair: TBitBtn
          Left = 339
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Sair'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          OnClick = btsairClick
        end
        object BitBtn1: TBitBtn
          Left = 451
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Op'#231#245'es'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          OnClick = BitBtn1Click
        end
      end
      object edtnome: TEdit
        Left = 8
        Top = 72
        Width = 654
        Height = 19
        MaxLength = 100
        TabOrder = 2
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Script'
      object MemoScript: TMemo
        Left = 0
        Top = 0
        Width = 691
        Height = 394
        Align = alClient
        Lines.Strings = (
          'MemoScript')
        TabOrder = 0
        OnKeyPress = MemoScriptKeyPress
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&3 - Campos'
      object DBGridCampos: TDBGrid
        Left = 0
        Top = 190
        Width = 691
        Height = 204
        Align = alClient
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'Verdana'
        TitleFont.Style = []
      end
      object PanelCampos: TPanel
        Left = 0
        Top = 0
        Width = 691
        Height = 190
        Align = alTop
        TabOrder = 1
        object BTExcluirCampos: TButton
          Left = 592
          Top = 144
          Width = 75
          Height = 25
          Caption = '&Excluir'
          TabOrder = 0
        end
        object BtSalvarCampos: TButton
          Left = 512
          Top = 144
          Width = 75
          Height = 25
          Caption = '&Salvar'
          TabOrder = 1
        end
      end
    end
  end
end
