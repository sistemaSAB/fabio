unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, uPSComponent, uPSComponent_Default,
  uPSComponent_Forms, uPSComponent_StdCtrls, uPSComponent_Controls,uPSCompiler, Menus, uPSRuntime;

type
  TForm1 = class(TForm)
    Button1: TButton;
    ps: TPSScript;
    PSDllPlugin1: TPSDllPlugin;
    procedure Button1Click(Sender: TObject);
    procedure psCompile(Sender: TPSScript);
    procedure psExecute(Sender: TPSScript);
    procedure psCompImport(Sender: TObject; x: TPSPascalCompiler);
    procedure psExecImport(Sender: TObject; se: TPSExec;
      x: TPSRuntimeClassImporter);
  private
    { Private declarations }

    Teste:TStringList;
    function ObterErro(Parametro:TPSScript): String;

  public
    { Public declarations }
  end;

Pvetor=array[0..100] of string;

var
  Form1: TForm1;
  AAA:Pvetor;

implementation
uses
  uPSR_std,
  uPSC_std,
  uPSR_stdctrls,
  uPSC_stdctrls,
  uPSR_forms,
  uPSC_forms,
  uPSC_graphics,
  uPSC_controls,
  uPSC_classes,
  uPSR_graphics,
  uPSR_controls,
  uPSR_classes,
  uPsr_DateUtils;


{$R *.dfm}

type
  TFuncaoMensagem = function(PTeste:TStringList):String of object;

procedure MensagemAviso(prstMensagem: String);
begin
  ShowMessage(prstMensagem);
end;



procedure TForm1.Button1Click(Sender: TObject);
var
  Funcao: TFuncaoMensagem;
  Msg:string;

begin
     Teste:=TStringList.Create;
     Teste.Add('Teste1 ');
     Teste.Add('Teste2 ');

     //Abrindo o Script
     ps.Script.LoadFromFile('Script1.pas');

     //Compilando o Script
     if not ps.Compile then
     begin
           ShowMessage('Erro: ' +ObterErro(Ps));
           Exit;
     end;
     //Pegando o Endereco da Funcao Dentro do Script
     Funcao := TFuncaoMensagem(ps.GetProcMethod('FuncaoMensagem'));
     
     //Verificando se a funcao existe dentro do Script
     if @Funcao = nil
     then begin
             ShowMessage('Funcao FuncaoMensagem n�o definida no Script');
             Exit;
     end;
     
     //Chamando a Funcao e pagando o resultado em uma String
     Msg:= Funcao(Teste);
     //Mostrando a String
     Showmessage(msg);
end;


function TForm1.ObterErro(Parametro:TPSScript): String;
var
  i: integer;
begin
  Result := '';

  for i := 0 to Parametro.CompilerMessageCount - 1 do
    Result := Result + Parametro.CompilerMessages[i].MessageToString + #13#10;


end;

procedure TForm1.psCompile(Sender: TPSScript);
begin
    //registrando a funcao que pode ser acessada do script
    Sender.AddMethod(nil, @MensagemAviso, 'procedure MensagemAviso(prst: String);');

  //Registrando Variaveis que podem ser acessadas via Script
  Sender.AddRegisteredVariable('Application', 'TApplication');
  Sender.AddRegisteredVariable('Self', 'TForm');
  Sender.AddRegisteredVariable('Button1', 'TButton');
end;

procedure TForm1.psExecute(Sender: TPSScript);
begin
    PS.SetVarToInstance('APPLICATION', Application);
    PS.SetVarToInstance('SELF', Self);
    PS.SetVarToInstance('BUTTON1', button1);
end;

procedure TForm1.psCompImport(Sender: TObject; x: TPSPascalCompiler);
begin
  //importando os plugins
  SIRegister_Std(x);
  SIRegister_Classes(x, true);
  SIRegister_Graphics(x, true);
  SIRegister_Controls(x);
  SIRegister_stdctrls(x);
  SIRegister_Forms(x);


end;

procedure TForm1.psExecImport(Sender: TObject; se: TPSExec;
  x: TPSRuntimeClassImporter);
begin
     //plugins de importacao
  RIRegister_Std(x);
  RIRegister_Classes(x, True);
  RIRegister_Graphics(x, True);
  RIRegister_Controls(x);
  RIRegister_stdctrls(x);
  RIRegister_Forms(x);

end;

end.
