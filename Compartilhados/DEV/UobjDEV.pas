unit UobjDEV;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,
IBStoredProc,UobjConfCamposDev, UobjCamposDev;
//USES_INTERFACE


const LIMITEVETOR=1000;

Type



   TObjDEV=class

          Public
                CodigoDev:String;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ConfCamposDEV:tobjconfcamposdev;
                StrCodigo:TStringList;
                VCamposDEV:array[0..LIMITEVETOR] of TObjCAMPOSDEV;
                

                Constructor Create;
                Destructor  Free;

                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;


                Function  Get_NovoCodigo:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;

                Function PreencheVetor(PCodigoconf:String):boolean;
                function DestroiVetor: boolean;

                Function InsereNovoComponente(pcodigodev:string;var pcodigo:String):Boolean;

                function InsereVetor(Pcodigo: String): boolean;
                
         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;





               CODIGO:string;

               


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UinsereCampoDEV;





Function  TObjDEV.TabelaparaObjeto:Boolean;//ok
var
cont:integer;
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;

        //percorro a string pegando os nomes dos campos e usando o fieldbyname preencho o valor
        for cont:=0 to Self.StrCodigo.count-1 do
        begin
             if (Self.VCamposDEV[cont].Get_CAMPOSBD='S')
             Then Self.VCamposDEV[cont].ValorAtual:=fieldbyname(Self.VCamposDEV[cont].get_nome).asstring;
        End;

        result:=True;
     End;
end;


Procedure TObjDEV.ObjetoparaTabela;//ok
var
cont:integer;
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;

        //percorro a string pegando os nomes dos campos e usando o fieldbyname preencho o valor
        for cont:=0 to Self.StrCodigo.count-1 do
        begin
             //se for um Campo do BD, envio para o valor atual para o parametro
             if (Self.VCamposDEV[cont].Get_CAMPOSBD='S')
             Then Begin
                       if (pos('DECIMAL',Self.VCamposDEV[cont].Get_TIPO)>0)
                       Then ParamByName(Self.VCamposDEV[cont].get_nome).asstring:=StringReplace(Self.VCamposDEV[cont].ValorAtual,',','.',[rfReplaceAll])
                       Else ParamByName(Self.VCamposDEV[cont].get_nome).asstring:=Self.VCamposDEV[cont].ValorAtual;
             End;
        End;

  End;
End;

//***********************************************************************

function TObjDEV.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjDEV.ZerarTabela;//Ok
var
cont:integer;
Begin
     With Self do
     Begin
        CODIGO:='';

        for cont:=0 to Self.StrCodigo.count-1 do
        begin
             Self.VCamposDEV[cont].ValorAtual:='';
        End;
     End;
end;

Function TObjDEV.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
   cont:integer;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      for cont:=0 to Self.StrCodigo.count-1 do
      begin
             if ((Self.VCamposDEV[cont].Get_VERIFICABRANCO='S')
             and (Self.VCamposDEV[cont].Get_CAMPOSBD='S'))
             then Begin
                       if (Self.VCamposDEV[cont].valoratual='')
                       Then mensagem:=Mensagem+'/'+Self.VCamposDEV[cont].get_caption;
             End;
      End;

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjDEV.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS




     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjDEV.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
   cont:integer;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;


      for cont:=0 to Self.StrCodigo.count-1 do
      begin
             if ((Self.VCamposDEV[cont].Get_VERIFICANUMERICO='S')
             and (Self.VCamposDEV[cont].Get_CAMPOSBD='S'))
             then Begin
                       
                       if (pos('INTEGER',Self.VCamposDEV[cont].Get_TIPO)>0)
                       Then Begin
                                 try
                                    Strtoint(Self.VCamposDEV[cont].valoratual);
                                 Except
                                       Mensagem:=mensagem+'/'+Self.VCamposDEV[cont].get_caption;
                                 End;
                       End;

                       if (pos('DECIMAL',Self.VCamposDEV[cont].Get_TIPO)>0)
                       Then Begin
                                 try
                                    StrToCurr(Self.VCamposDEV[cont].valoratual);
                                 Except
                                       Mensagem:=mensagem+'/'+Self.VCamposDEV[cont].get_caption;
                                 End;
                       End;
   
             End;
      End;





     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjDEV.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
cont:integer;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA

      for cont:=0 to Self.StrCodigo.count-1 do
      begin
             if ((Self.VCamposDEV[cont].Get_VERIFICADATA='S')
             and (Self.VCamposDEV[cont].Get_CAMPOSBD='S'))
             then Begin
                       
                       if (pos('DATE',Self.VCamposDEV[cont].Get_TIPO)>0)
                       Then Begin
                                 try
                                    Strtodate(Self.VCamposDEV[cont].valoratual);
                                 Except
                                       Mensagem:=mensagem+'/'+Self.VCamposDEV[cont].get_caption;
                                 End;
                       End
                       else
                       if (pos('TIMESTAMP',Self.VCamposDEV[cont].Get_TIPO)>0)
                       Then Begin
                                 try
                                    strtodatetime(Self.VCamposDEV[cont].valoratual);
                                 Except
                                       Mensagem:=mensagem+'/'+Self.VCamposDEV[cont].get_caption;
                                 End;
                       End
                       else
                       if (pos('TIME',Self.VCamposDEV[cont].Get_TIPO)>0)
                       Then Begin
                                 try
                                    strtotime(Self.VCamposDEV[cont].valoratual);
                                 Except
                                       Mensagem:=mensagem+'/'+Self.VCamposDEV[cont].get_caption;
                                 End;
                       End; 
   
             End;
      End;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjDEV.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
   cont:integer;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        for cont:=0 to Self.StrCodigo.count-1 do
        begin

             if (Self.VCamposDEV[cont].get_verificafaixa='S')
             then Begin
                       

                       
             End;

        End;

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjDEV.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro DEV vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,CAPTION,TAMANHOHORIZONTAL,TAMANHOVERTICAL');
           SQL.ADD(' from  TABDEV');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;



function TObjDEV.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjDEV.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjDEV.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
var
cont:integer;
begin

        Try
           Self.ConfCamposDEV:=tobjconfcamposdev.create;
        Except
              on e:exception do
              Begin
                  raise Exception.Create('Erro na tentativa de criar o Objeto CONFCAMPOSDEV '+#13+e.message);
                  exit;
              End;
        End;

        //Limpando o Vetor
        
        for cont:=0 to LIMITEVETOR do
        Begin
             self.VCamposDEV[cont]:=nil;
        End;


        StrCodigo:=TStringList.Create;

        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABDEV(CODIGO,CAPTION,TAMANHOHORIZONTAL');
                InsertSQL.add(' ,TAMANHOVERTICAL)');
                InsertSQL.add('values (:CODIGO,:CAPTION,:TAMANHOHORIZONTAL,:TAMANHOVERTICAL');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABDEV set CODIGO=:CODIGO,CAPTION=:CAPTION,TAMANHOHORIZONTAL=:TAMANHOHORIZONTAL');
                ModifySQL.add(',TAMANHOVERTICAL=:TAMANHOVERTICAL');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABDEV where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;


function TObjDEV.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabDev'+Self.CodigoDev);
     Result:=Self.ParametroPesquisa;
end;

function TObjDEV.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de DEV ';
end;


function TObjDEV.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENDEV,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENDEV,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjDEV.Free;
begin
    Self.DestroiVetor;
    
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);


    Self.ConfCamposDEV.free;
    freeandnil(StrCodigo);
end;


procedure TObjDEV.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjDEV.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;


function TObjDEV.PreencheVetor(PCodigoconf: String): boolean;
var
cont:integer;
begin
     result:=False;
     
     Self.StrCodigo.clear;

     Self.ConfCamposDEV.CarregaCodigos(PCodigoconf);


     for cont:=0 to Self.ConfCamposDEV.ListaCodigos.Count-1 do
     Begin
          Self.Inserevetor(Self.confCamposDev.ListaCodigos[cont]);
     End;

     result:=true;
end;

function TobjDev.InsereVetor(Pcodigo:String):boolean;
begin
     result:=False;
     
     //adicionando o codigo para controle
     Self.StrCodigo.add(pcodigo);

     Try
        Self.VCamposDEV[Self.strcodigo.count-1]:=TObjCAMPOSDEV.Create;
     Except
           on e:exception do
           begin
                mensagemerro('Erro ao tentar criar o vetor de conf. de campos '+#13+E.message);
                exit;
           End;
      End;

      Self.ConfCamposDEV.LocalizaCodigo(Pcodigo);
      //o Tabela para Objeto � chamado passando a posicao do vetor
      //atual apenas com os campos
      Self.ConfCamposDEV.TabelaparaObjeto(Self.VCamposDEV[Self.strcodigo.count-1]);
      result:=True;
End;



function TObjDEV.DestroiVetor: boolean;
var
cont:integer;
begin
     result:=False;

     for cont:=0 to Self.StrCodigo.Count-1 do
     Begin
           Try
              if (Self.VCamposDEV[cont]<>nil)
              then Begin
                      Self.VCamposDEV[cont].free;
                      Self.VCamposDEV[cont]:=nil;
              End;
           Except
                 on e:exception do
                 begin
                      mensagemerro('Erro ao tentar destruir o vetor de conf. de campos. Posi��o '+inttostr(cont)+#13+E.message);
                 End;
           End;
     End;

     result:=true;
end;

function TObjDEV.InsereNovoComponente(pcodigodev:string;var pcodigo:String): Boolean;
var
temp:string;
cont:integer;
begin
     With FinsereCampoDEV do
     Begin
          RgComponente.ItemIndex:=0;
          LbFaixaVerificacao.Items.clear;
          LBItens.Items.clear;
          CHKVerifica.Checked[0]:=False;//branco
          CHKVerifica.Checked[1]:=False;//numerico
          CHKVerifica.Checked[2]:=False;//data
          CHKVerifica.Checked[3]:=False;//hora
          CHKVerifica.Checked[4]:=False;//relacionamento
          CHKVerifica.Checked[5]:=False;//faixa
          edtcaption.Text:='';
          edtnome.text:='';
          EdtPrecisao.text:='';
          edtmascara.text:='';
          edtquantidade.text:='0';
          edtposicaoesquerda.Text:='100';
          edtposicaosuperior.Text:='100';
          edttaborder.text:='0';
          ComboTipo.itemindex:=0;
          ChCampoBD.Checked:=False;
          PanelBD.Visible:=False;


          showmodal;

          if (tag=0)
          then exit;




          Self.ConfCamposDEV.ZerarTabela;
          Self.ConfCamposDEV.Status:=dsinsert;
          Pcodigo:=Self.ConfCamposDEV.Get_NovoCodigo;

          Self.ConfCamposDEV.CONFDEV.submit_codigo(pcodigodev);
          Self.ConfCamposDEV.Submit_CODIGO(pcodigo);
          Self.ConfCamposDEV.Submit_NOME(edtnome.Text);
          Self.ConfCamposDEV.Submit_caption(edtcaption.Text);
          Self.ConfCamposDEV.Submit_COMPONENTE(uppercase(RgComponente.Items[RgComponente.itemindex]));
          Self.ConfCamposDEV.Submit_TIPO(uppercase(ComboTipo.Text));

          if (CHKVerifica.Checked[0])
          then Self.ConfCamposDEV.Submit_VERIFICABRANCO('S')
          Else Self.ConfCamposDEV.Submit_VERIFICABRANCO('N');


          if (CHKVerifica.Checked[1])
          then Self.ConfCamposDEV.Submit_VERIFICANUMERICO('S')
          Else Self.ConfCamposDEV.Submit_VERIFICANUMERICO('N');


          if (CHKVerifica.Checked[2])
          then Self.ConfCamposDEV.Submit_VERIFICADATA('S')
          Else Self.ConfCamposDEV.Submit_VERIFICADATA('N');


          if (CHKVerifica.Checked[3])
          then Self.ConfCamposDEV.Submit_VERIFICAHORA('S')
          Else Self.ConfCamposDEV.Submit_VERIFICAHORA('N');


          if (CHKVerifica.Checked[4])
          then Self.ConfCamposDEV.Submit_VERIFICARELACIONAMENTO('S')
          Else Self.ConfCamposDEV.Submit_VERIFICARELACIONAMENTO('N');


          if (CHKVerifica.Checked[5])
          then Self.ConfCamposDEV.Submit_verificafaixa('S')
          Else Self.ConfCamposDEV.Submit_verificafaixa('N');


          temp:='';
          if (LbFaixaVerificacao.Items.count>0)
          Then Begin
                    temp:=LbFaixaVerificacao.Items[0];
                    for cont:=1 to LbFaixaVerificacao.Items.count-1 do
                    Begin
                        temp:=temp+'|'+LbFaixaVerificacao.Items[cont];
                    End;
          End;

          Self.ConfCamposDEV.Submit_FAIXAVERIFICACAO(temp);


          temp:='';
          if (LBItens.Items.count>0)
          Then Begin
                    temp:=LBItens.Items[0];
                    for cont:=1 to lbitens.Items.count-1 do
                    Begin
                        temp:=temp+'|'+lbitens.Items[cont];
                    End;
          End;
          Self.ConfCamposDEV.Submit_ITEMS(temp);

          Self.ConfCamposDEV.Submit_MASCARA(edtmascara.Text);
          Self.ConfCamposDEV.Submit_QUANTIDADECARACTERES(edtquantidade.Text);

          if (ChCampoBD.Checked)
          then Self.ConfCamposDEV.Submit_CAMPOSBD('S')
          else Self.ConfCamposDEV.Submit_CAMPOSBD('N');

          Self.ConfCamposDEV.Submit_PosicaoEsquerda(edtposicaoesquerda.text);
          Self.ConfCamposDEV.Submit_PosicaoSuperior(edtposicaosuperior.text);
          Self.ConfCamposDEV.Submit_TabOrder(edttaborder.Text);

          if (self.ConfCamposDEV.Salvar(true)=False)
          then Begin
                    MensagemErro('Erro na tentativa de salvar um novo campo');
                    exit;
          End;

          result:=True;
     End;
End;

end.



