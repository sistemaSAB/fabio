unit UobjConfDEV;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc, UObjPermissoesUso;
//USES_INTERFACE


Type
   TObjCONFDEV=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Permissao:TObjPermissoesUso;
                SCRIPT:TStringList;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;

                Function    LocalizaCodigo(Parametro:String) :boolean;
                Function    LocalizaCAMPO(Pcampo,Pvalor:String) :boolean;


                Function    Exclui(Pcodigo:String;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :String;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:String;
                Function  RetornaCampoCodigo:String;
                Function  RetornaCampoNome:String;
                Procedure Imprime(Pcodigo:String);

                Procedure Submit_CODIGO(parametro: String);
                Function Get_CODIGO: String;
                Procedure Submit_CAPTION(parametro: String);
                Function Get_CAPTION: String;

                Procedure Submit_nome(parametro: String);
                Function Get_nome: String;

                Procedure Submit_TAMANHOHORIZONTAL(parametro: String);
                Function Get_TAMANHOHORIZONTAL: String;
                Procedure Submit_TAMANHOVERTICAL(parametro: String);
                Function Get_TAMANHOVERTICAL: String;
                procedure EdtPERMISSAOExit(Sender: TObject; LABELNOME: TLABEL);
                procedure EdtPERMISSAOKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);

                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:String;
               CAPTION:String;
               nome:String;
               TAMANHOHORIZONTAL:String;
               TAMANHOVERTICAL:String;


               Script_Stream:TmemoryStream;

//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UPermissoesUso;





Function  TObjCONFDEV.TabelaparaObjeto:Boolean;//ok
var
temp:TStream;
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.CAPTION:=fieldbyname('CAPTION').asstring;
        Self.nome:=fieldbyname('nome').asstring;

        If(FieldByName('PERMISSAO').asstring<>'')
        Then Begin
                 If (Self.PERMISSAO.LocalizaCodigo(FieldByName('PERMISSAO').asstring)=False)
                 Then Begin
                          Messagedlg('C�digo de Permiss�o  N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.PERMISSAO.TabelaparaObjeto;
        End;

        Self.TAMANHOHORIZONTAL:=fieldbyname('TAMANHOHORIZONTAL').asstring;
        Self.TAMANHOVERTICAL:=fieldbyname('TAMANHOVERTICAL').asstring;

        try
           try
             Temp:=CreateBlobStream(FieldByName('script'),bmRead);
             Self.Script.LoadFromStream(temp);
           Except
                 Messagedlg('Erro na Tentativa de Recuperar o valor do Campo Script!',mterror,[mbok],0);
                 exit;
           End;
        Finally
               freeandnil(temp);
        End;



        result:=True;
     End;
end;


Procedure TObjCONFDEV.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('CAPTION').asstring:=Self.CAPTION;
        ParamByName('nome').asstring:=Self.nome;
        ParamByName('TAMANHOHORIZONTAL').asstring:=Self.TAMANHOHORIZONTAL;
        ParamByName('TAMANHOVERTICAL').asstring:=Self.TAMANHOVERTICAL;
        ParamByName('Permissao').asstring:=Self.Permissao.get_codigo;

        Self.script_Stream.Clear;
        Self.script.SaveToStream(Self.script_Stream);
        ParamByName('script').LoadFromStream(Self.script_Stream,ftBlob);

  End;
End;

//***********************************************************************

function TObjCONFDEV.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

   If (Self.LocalizaCampo('NOME',Self.nome)=True)
   Then Begin
             if(Self.Status=dsedit) and (Self.CODIGO<>Self.Objquery.FieldByName('codigo').asstring)
             Then Begin
                       Messagedlg('O registro n� '+Self.Objquery.FieldByName('codigo').asstring+' j� possui este nome no formul�rio DEV',mterror,[mbok],0);
                       exit;
             End
             Else Begin
                       if(Self.Status=dsinsert)
                       Then Begin
                                 Messagedlg('J� existe um registro com este nome!',mterror,[mbok],0);
                                 exit;
                       End;
             End;

   End;


   if Self.status=dsinsert
   Then Begin
             Self.Objquery.SQL.Clear;
             Self.Objquery.SQL.text:=Self.InsertSql.Text;
             if (Self.Codigo='0')
             Then Self.codigo:=Self.Get_NovoCodigo;
   End
   Else Begin
             if (Self.Status=dsedit)
             Then Begin
                       Self.Objquery.SQL.Clear;
                       Self.Objquery.SQL.text:=Self.ModifySQl.Text;
             End
             Else Begin
                       Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End;


   Self.ObjetoParaTabela;
   
   Try
      Self.Objquery.ExecSQL;
   Except
         if (Self.Status=dsInsert)
         Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
         Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0);
         exit;
   End;

   If ComCommit=True
   Then FDataModulo.IBTransaction.CommitRetaining;

   Self.status          :=dsInactive;
   result:=True;
end;

procedure TObjCONFDEV.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        CAPTION:='';
        nome:='';
        TAMANHOHORIZONTAL:='';
        TAMANHOVERTICAL:='';
        Permissao.ZerarTabela;
        SCRIPT.clear;
//CODIFICA ZERARTABELA




     End;
end;

Function TObjCONFDEV.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      
      If (Nome='')
      Then Mensagem:=mensagem+'/Nome';

      If (CAPTION='')
      Then Mensagem:=mensagem+'/Caption';

      If (nome='')
      Then Mensagem:=mensagem+'/Nome';
      
      If (TAMANHOHORIZONTAL='')
      Then Mensagem:=mensagem+'/Tamanho Horizontal';
      
      If (TAMANHOVERTICAL='')
      Then Mensagem:=mensagem+'/Tamanho Vertical';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjCONFDEV.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     if (Self.Permissao.get_codigo<>'')
     then Begin
               if (Self.Permissao.LocalizaCodigo(Self.Permissao.get_codigo)=False)
               Then mensagem:=mensagem+'/ Permiss�o n�o localizada';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCONFDEV.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtoint(Self.TAMANHOHORIZONTAL);
     Except
           Mensagem:=mensagem+'/Tamanho Horizontal';
     End;
     try
        Strtoint(Self.TAMANHOVERTICAL);
     Except
           Mensagem:=mensagem+'/Tamanho Vertical';
     End;
//CODIFICA VERIFICANUMERICOS

     if (Self.Permissao.get_codigo<>'')
     then Begin
               Try
                  strtoint(Self.Permissao.get_codigo);
               Except
                     mensagem:=mensagem+'/ Permiss�o inv�lida';
               End;
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCONFDEV.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum CONFDEVeria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCONFDEV.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCONFDEV.LocalizaCAMPO(Pcampo, Pvalor: String): boolean;
begin
       if (Pcampo='') or (PValor='')
       Then Begin
                 Messagedlg('Par�metros Campo ou Valor vazios no m�todo LocalizaCampo',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,nome,CAPTION,TAMANHOHORIZONTAL,TAMANHOVERTICAL,permissao,script');
           SQL.ADD(' from  TABCONFDEV');
           SQL.ADD(' WHERE '+pcampo+'='+#39+pvalor+#39);
           Open;
           
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


function TObjCONFDEV.LocalizaCodigo(parametro: String): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CONFDEV vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,nome,CAPTION,TAMANHOHORIZONTAL,TAMANHOVERTICAL,permissao,script');
           SQL.ADD(' from  TABCONFDEV');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCONFDEV.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCONFDEV.Exclui(Pcodigo: String;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCONFDEV.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Permissao:=TObjPermissoesUso.create;

        Self.InsertSql:=TStringList.create;
        Self.DeleteSql:=TStringList.create;
        Self.ModifySQl:=TStringList.create;
        Self.SCRIPT   :=TStringList.create;
        Self.Script_Stream:=TmemoryStream.Create;

//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCONFDEV(CODIGO,nome,CAPTION,TAMANHOHORIZONTAL');
                InsertSQL.add(' ,TAMANHOVERTICAL,permissao,script)');
                InsertSQL.add('values (:CODIGO,:nome,:CAPTION,:TAMANHOHORIZONTAL,:TAMANHOVERTICAL,:permissao,:script');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCONFDEV set CODIGO=:CODIGO,CAPTION=:CAPTION,nome=:nome,TAMANHOHORIZONTAL=:TAMANHOHORIZONTAL');
                ModifySQL.add(',TAMANHOVERTICAL=:TAMANHOVERTICAL,permissao=:permissao,script=:script');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCONFDEV where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCONFDEV.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCONFDEV.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCONFDEV');
     Result:=Self.ParametroPesquisa;
end;

function TObjCONFDEV.Get_TituloPesquisa: String;
begin
     Result:=' Pesquisa de Configura��o de Formul�rios DEV ';
end;


function TObjCONFDEV.Get_NovoCodigo: String;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCONFDEV,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCONFDEV,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCONFDEV.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Permissao.Free;
    freeandnil(SCRIPT);
    freeandnil(Self.script_stream);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCONFDEV.RetornaCampoCodigo: String;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCONFDEV.RetornaCampoNome: String;
begin
      result:='NOME';
end;

procedure TObjCONFDEV.Submit_CODIGO(parametro: String);
begin
        Self.CODIGO:=Parametro;
end;
function TObjCONFDEV.Get_CODIGO: String;
begin
        Result:=Self.CODIGO;
end;
procedure TObjCONFDEV.Submit_CAPTION(parametro: String);
begin
        Self.CAPTION:=Parametro;
end;
function TObjCONFDEV.Get_CAPTION: String;
begin
        Result:=Self.CAPTION;
end;


procedure TObjCONFDEV.Submit_nome(parametro: String);
begin
        Self.nome:=Parametro;
end;
function TObjCONFDEV.Get_nome: String;
begin
        Result:=Self.nome;
end;

procedure TObjCONFDEV.Submit_TAMANHOHORIZONTAL(parametro: String);
begin
        Self.TAMANHOHORIZONTAL:=Parametro;
end;
function TObjCONFDEV.Get_TAMANHOHORIZONTAL: String;
begin
        Result:=Self.TAMANHOHORIZONTAL;
end;
procedure TObjCONFDEV.Submit_TAMANHOVERTICAL(parametro: String);
begin
        Self.TAMANHOVERTICAL:=Parametro;
end;
function TObjCONFDEV.Get_TAMANHOVERTICAL: String;
begin
        Result:=Self.TAMANHOVERTICAL;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjCONFDEV.Imprime(Pcodigo: String);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCONFDEV';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TobjConfDev.edtpermissaoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.PERMISSAO.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.PERMISSAO.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.PERMISSAO.Get_nome;
End;
procedure TobjConfDev.edtpermissaokeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FPERMISSAO:TFPermissoesUso;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FPERMISSAO:=TFPermissoesUso.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.PERMISSAO.Get_Pesquisa,Self.PERMISSAO.Get_TituloPesquisa,FPERMISSAO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERMISSAO.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.PERMISSAO.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.PERMISSAO.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FPERMISSAO);
     End;
end;



end.



