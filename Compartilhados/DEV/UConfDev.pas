unit UCONFDEV;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UobjCONFCAMPOSDEV,
  jpeg, Grids, DBGrids;

type
  TFCONFDEV = class(TForm)
    Guia: TTabbedNotebook;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbCAPTION: TLabel;
    EdtCAPTION: TEdit;
    LbTAMANHOHORIZONTAL: TLabel;
    EdtTAMANHOHORIZONTAL: TEdit;
    LbTAMANHOVERTICAL: TLabel;
    EdtTAMANHOVERTICAL: TEdit;
    Label1: TLabel;
    edtpermissao: TEdit;
    lbnomepermissao: TLabel;
    MemoScript: TMemo;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    BitBtn1: TBitBtn;
    DBGridCampos: TDBGrid;
    PanelCampos: TPanel;
    BTExcluirCampos: TButton;
    BtSalvarCampos: TButton;
    edtnome: TEdit;
    Label2: TLabel;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure edtpermissaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpermissaoExit(Sender: TObject);
    procedure MemoScriptKeyPress(Sender: TObject; var Key: Char);
  private
         ObjCONFCamposDEV:TObjCONFCamposDEV;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCONFDEV: TFCONFDEV;


implementation

uses UessencialGlobal, Upesquisa, UDEV;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCONFDEV.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCONFCamposDEV.CONFDEV do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_CAPTION(edtCAPTION.text);
        Submit_nome(edtnome.text);
        Submit_TAMANHOHORIZONTAL(edtTAMANHOHORIZONTAL.text);
        Submit_TAMANHOVERTICAL(edtTAMANHOVERTICAL.text);
        permissao.Submit_CODIGO(edtpermissao.text);
        SCRIPT.Text:=MemoScript.Lines.Text;
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCONFDEV.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCONFCamposDEV.CONFDEV do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtCAPTION.text:=Get_CAPTION;
        Edtnome.text:=Get_nome;
        EdtTAMANHOHORIZONTAL.text:=Get_TAMANHOHORIZONTAL;
        EdtTAMANHOVERTICAL.text:=Get_TAMANHOVERTICAL;
        edtpermissao.text:=Permissao.get_codigo;
        lbnomepermissao.caption:=permissao.Get_nome;
        MemoScript.Lines.text:=Script.Text;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCONFDEV.TabelaParaControles: Boolean;
begin
     If (Self.ObjCONFCamposDEV.CONFDEV.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCONFDEV.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjCONFCamposDEV:=TObjCONFCamposDEV.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFCONFDEV.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCONFCamposDEV=Nil)
     Then exit;

If (Self.ObjCONFCamposDEV.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjCONFCamposDEV.free;
end;

procedure TFCONFDEV.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCONFDEV.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCONFCamposDEV.CONFDEV.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjCONFCamposDEV.CONFDEV.status:=dsInsert;
     Guia.pageindex:=0;
     edtpermissao.setfocus;

end;


procedure TFCONFDEV.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCONFCamposDEV.CONFDEV.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCONFCamposDEV.CONFDEV.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtpermissao.setfocus;
                
          End;


end;

procedure TFCONFDEV.btgravarClick(Sender: TObject);
begin

     If Self.ObjCONFCamposDEV.CONFDEV.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCONFCamposDEV.CONFDEV.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCONFCamposDEV.CONFDEV.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCONFDEV.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCONFCamposDEV.CONFDEV.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCONFCamposDEV.CONFDEV.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCONFCamposDEV.CONFDEV.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCONFDEV.btcancelarClick(Sender: TObject);
begin
     Self.ObjCONFCamposDEV.CONFDEV.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCONFDEV.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCONFDEV.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCONFCamposDEV.CONFDEV.Get_pesquisa,Self.ObjCONFCamposDEV.CONFDEV.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCONFCamposDEV.CONFDEV.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCONFCamposDEV.CONFDEV.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCONFCamposDEV.CONFDEV.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCONFDEV.LimpaLabels;
begin
//LIMPA LABELS
     Self.lbnomepermissao.caption:='';
end;

procedure TFCONFDEV.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCONFDEV.BitBtn1Click(Sender: TObject);
var
Fdevx:TFDEV;
begin
     Try
        Fdevx:=TFDEV.Create(nil);
     Except
           on e:exception do
           Begin
                MensagemErro('Erro na tentativa de criar o formul�rio personalizado'+#13+e.Message);
                exit;
           End;
     End;

     Try
         if (FdevX.Inicializa(EdtCODIGO.text,True)=False)
         Then exit;

         FdevX.showmodal;

     Finally
            freeandnil(Fdevx);
     End;
end;

procedure TFCONFDEV.edtpermissaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Self.ObjCONFCamposDEV.CONFDEV.EdtPERMISSAOKeyDown(sender,key,shift,lbnomepermissao);
end;

procedure TFCONFDEV.edtpermissaoExit(Sender: TObject);
begin
Self.ObjCONFCamposDEV.CONFDEV.EdtPERMISSAOExit(sender,lbnomepermissao);
end;

procedure TFCONFDEV.MemoScriptKeyPress(Sender: TObject; var Key: Char);
begin
     if (key=#13)
     Then Self.MemoScript.SetFocus;
end;

end.

