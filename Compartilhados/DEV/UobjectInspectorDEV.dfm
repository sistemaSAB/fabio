object FobjectInspectorDEV: TFobjectInspectorDEV
  Left = 278
  Top = 109
  Width = 315
  Height = 518
  Caption = 'Object Inspector'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 307
    Height = 26
    Align = alTop
    TabOrder = 0
    object ComboComponente: TComboBox
      Left = 3
      Top = 2
      Width = 262
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = 'ComboComponente'
      OnChange = ComboComponenteChange
    end
  end
  object STRGPropriedades: TStringGrid
    Left = 0
    Top = 26
    Width = 307
    Height = 465
    Align = alClient
    Color = 13160660
    DefaultColWidth = 130
    FixedColor = clGray
    FixedCols = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
    ParentFont = False
    TabOrder = 1
    OnKeyPress = STRGPropriedadesKeyPress
    OnSetEditText = STRGPropriedadesSetEditText
  end
  object ComboOpcao: TComboBox
    Left = 144
    Top = 136
    Width = 113
    Height = 21
    ItemHeight = 13
    TabOrder = 2
    Text = 'ComboOpcao'
    Visible = False
  end
end
