unit UobjCONFCAMPOSDEV;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UOBJCONFDEV,uobjcamposdev;

Type
   TObjCONFCAMPOSDEV=class(TobjCamposDev)

          Public
                Status                                      :TDataSetState;
                SqlInicial                                  :String;
                CONFDEV:TOBJCONFDEV;
                ListaCodigos:TStringList;
                ValorAtual:String;


//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;overload;
                Function   TabelaparaObjeto(Parametro:TobjCamposDev):Boolean;overload;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                procedure EdtCONFDEVExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCONFDEVKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                //CODIFICA DECLARA GETSESUBMITS

                Function CarregaCodigos(pcodigo:String):TStringList;


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  FVerificaData:boolean;
               Function  FVerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
   End;


implementation

uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UmenuRelatorios,
  UCONFDEV;


Function  TObjCONFCAMPOSDEV.TabelaparaObjeto(Parametro:TobjCamposDev):Boolean;//ok
Begin
     With Objquery do
     Begin
        Parametro.LimpaCampos;
        Parametro.Submit_CODIGO(Fieldbyname('CODIGO').asstring);
        Parametro.Submit_NOME(fieldbyname('NOME').asstring);
        Parametro.Submit_COMPONENTE(fieldbyname('COMPONENTE').asstring);
        Parametro.Submit_caption(fieldbyname('caption').asstring);
        Parametro.Submit_TIPO(fieldbyname('TIPO').asstring);
        Parametro.Submit_VERIFICABRANCO(fieldbyname('VERIFICABRANCO').asstring);
        Parametro.Submit_verificafaixa(fieldbyname('verificafaixa').asstring);
        Parametro.Submit_VERIFICANUMERICO(fieldbyname('VERIFICANUMERICO').asstring);
        Parametro.Submit_VERIFICADATA(fieldbyname('VERIFICADATA').asstring);
        Parametro.Submit_VERIFICAHORA(fieldbyname('VERIFICAHORA').asstring);
        Parametro.Submit_VERIFICARELACIONAMENTO(fieldbyname('VERIFICARELACIONAMENTO').asstring);
        Parametro.Submit_FAIXAVERIFICACAO(fieldbyname('FAIXAVERIFICACAO').asstring);
        Parametro.Submit_ITEMS(fieldbyname('ITEMS').asstring);
        Parametro.Submit_MASCARA(fieldbyname('MASCARA').asstring);
        Parametro.Submit_QUANTIDADECARACTERES(fieldbyname('QUANTIDADECARACTERES').asstring);
        Parametro.Submit_CAMPOSBD(fieldbyname('CAMPOSBD').asstring);
        Parametro.Submit_PosicaoEsquerda(fieldbyname('PosicaoEsquerda').asstring);
        Parametro.Submit_PosicaoSuperior(fieldbyname('PosicaoSuperior').asstring);
        Parametro.Submit_TabOrder(fieldbyname('TabOrder').asstring);
        result:=True;
     End;
End;


Function  TObjCONFCAMPOSDEV.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;

        Self.CODIGO:=Fieldbyname('CODIGO').asstring;

        If(FieldByName('CONFDEV').asstring<>'')
        Then Begin
                 If (Self.CONFDEV.LocalizaCodigo(FieldByName('CONFDEV').asstring)=False)
                 Then Begin
                          Messagedlg('C�digo de Configura��o  N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CONFDEV.TabelaparaObjeto;
        End;

        
        Self.NOME:=fieldbyname('NOME').asstring;
        Self.COMPONENTE:=fieldbyname('COMPONENTE').asstring;
        Self.caption:=fieldbyname('caption').asstring;
        Self.TIPO:=fieldbyname('TIPO').asstring;
        Self.VERIFICABRANCO:=fieldbyname('VERIFICABRANCO').asstring;
        Self.verificafaixa:=fieldbyname('verificafaixa').asstring;
        Self.VERIFICANUMERICO:=fieldbyname('VERIFICANUMERICO').asstring;
        Self.VERIFICADATA:=fieldbyname('VERIFICADATA').asstring;
        Self.VERIFICAHORA:=fieldbyname('VERIFICAHORA').asstring;
        Self.VERIFICARELACIONAMENTO:=fieldbyname('VERIFICARELACIONAMENTO').asstring;
        Self.FAIXAVERIFICACAO:=fieldbyname('FAIXAVERIFICACAO').asstring;
        Self.ITEMS:=fieldbyname('ITEMS').asstring;
        Self.MASCARA:=fieldbyname('MASCARA').asstring;
        Self.QUANTIDADECARACTERES:=fieldbyname('QUANTIDADECARACTERES').asstring;
        Self.CAMPOSBD:=fieldbyname('CAMPOSBD').asstring;
        Self.PosicaoEsquerda:=fieldbyname('PosicaoEsquerda').asstring;
        Self.PosicaoSuperior:=fieldbyname('PosicaoSuperior').asstring;
        Self.TabOrder:=fieldbyname('TabOrder').asstring;
        
        result:=True;
     End;
end;


Procedure TObjCONFCAMPOSDEV.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('CONFDEV').asstring:=Self.CONFDEV.GET_CODIGO;
        ParamByName('NOME').asstring:=Self.NOME;
        ParamByName('COMPONENTE').asstring:=Self.COMPONENTE;
        ParamByName('caption').asstring:=Self.caption;
        ParamByName('TIPO').asstring:=Self.TIPO;
        ParamByName('VERIFICABRANCO').asstring:=Self.VERIFICABRANCO;
        ParamByName('verificafaixa').asstring:=Self.verificafaixa;
        ParamByName('VERIFICANUMERICO').asstring:=Self.VERIFICANUMERICO;
        ParamByName('VERIFICADATA').asstring:=Self.VERIFICADATA;
        ParamByName('VERIFICAHORA').asstring:=Self.VERIFICAHORA;
        ParamByName('VERIFICARELACIONAMENTO').asstring:=Self.VERIFICARELACIONAMENTO;
        ParamByName('FAIXAVERIFICACAO').asstring:=Self.FAIXAVERIFICACAO;
        ParamByName('ITEMS').asstring:=Self.ITEMS;
        ParamByName('MASCARA').asstring:=Self.MASCARA;
        ParamByName('QUANTIDADECARACTERES').asstring:=Self.QUANTIDADECARACTERES;
        ParamByName('CAMPOSBD').asstring:=Self.CAMPOSBD;

        ParamByName('PosicaoEsquerda').asstring:=Self.PosicaoEsquerda;
        ParamByName('PosicaoSuperior').asstring:=Self.PosicaoSuperior;
        ParamByName('TabOrder').asstring:=Self.TabOrder;
//CODIFICA OBJETOPARATABELA

  End;
End;

//***********************************************************************

function TObjCONFCAMPOSDEV.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.FVerificaData=False)
  Then Exit;

  if (Self.FVerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCONFCAMPOSDEV.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CONFDEV.ZerarTabela;
        LimpaCampos;
     End;
end;

Function TObjCONFCAMPOSDEV.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (CONFDEV.get_Codigo='')
      Then Mensagem:=mensagem+'/C�digo de Configura��o ';

      
      If (CAMPOSBD='')
      Then Mensagem:=mensagem+'/Campos BD';


      if (CAMPOSBD='S')
      then Begin
                If (NOME='')
                Then Mensagem:=mensagem+'/Nome';

                If (TIPO='')
                Then Mensagem:=mensagem+'/Tipo';
      End;

      If (COMPONENTE='')
      Then Mensagem:=mensagem+'/Componente';

      //If (caption='')
      //Then Mensagem:=mensagem+'/caption';

      If (VERIFICABRANCO='')
      Then Mensagem:=mensagem+'/Verifica Branco';

      If (verificafaixa='')
      Then Mensagem:=mensagem+'/Verifica Faixa';
      
      If (VERIFICANUMERICO='')
      Then Mensagem:=mensagem+'/Verifica Num�rico';

      If (VERIFICADATA='')
      Then Mensagem:=mensagem+'/Verifica Data';

      If (VERIFICAHORA='')
      Then Mensagem:=mensagem+'/Verifica Hora';

      If (VERIFICARELACIONAMENTO='')
      Then Mensagem:=mensagem+'/Verifica Relacionamento';

      If (PosicaoEsquerda='')
      Then Mensagem:=mensagem+'/Posi��o Esquerda';

      If (PosicaoSuperior='')
      Then Mensagem:=mensagem+'/Posi��o Superior';

      If (tabOrder='')
      Then Mensagem:=mensagem+'/Ordem dos Tabs';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCONFCAMPOSDEV.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.CONFDEV.LocalizaCodigo(Self.CONFDEV.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ C�digo de Configura��o  n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCONFCAMPOSDEV.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        If (Self.CONFDEV.Get_Codigo<>'')
        Then Strtoint(Self.CONFDEV.Get_Codigo);
     Except
           Mensagem:=mensagem+'/C�digo de Configura��o ';
     End;


     try
        Strtoint(Self.PosicaoEsquerda);
     Except
           Mensagem:=mensagem+'/Posi��o Esquerda';
     End;

     try
        Strtoint(Self.PosicaoSuperior);
     Except
           Mensagem:=mensagem+'/Posi��o Superior';
     End;


     try
        Strtoint(Self.taborder);
     Except
           Mensagem:=mensagem+'/Ordem dos Tabs';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCONFCAMPOSDEV.FVerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;
end;

function TObjCONFCAMPOSDEV.FVerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCONFCAMPOSDEV.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CONFCAMPOSDEV vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,CONFDEV,NOME,COMPONENTE,caption,TIPO,VERIFICABRANCO,verificafaixa,VERIFICANUMERICO');
           SQL.ADD(' ,VERIFICADATA,VERIFICAHORA,VERIFICARELACIONAMENTO,FAIXAVERIFICACAO');
           SQL.ADD(' ,ITEMS,MASCARA,QUANTIDADECARACTERES,CAMPOSBD,PosicaoEsquerda,PosicaoSuperior,TabOrder');
           SQL.ADD(' from  TABCONFCAMPOSDEV');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCONFCAMPOSDEV.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCONFCAMPOSDEV.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCONFCAMPOSDEV.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCONFCAMPOSDEV.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.ListaCodigos:=TStringList.create;


        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.CONFDEV:=TOBJCONFDEV.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCONFCAMPOSDEV(CODIGO,CONFDEV,NOME,COMPONENTE,caption');
                InsertSQL.add(' ,TIPO,VERIFICABRANCO,verificafaixa,VERIFICANUMERICO,VERIFICADATA');
                InsertSQL.add(' ,VERIFICAHORA,VERIFICARELACIONAMENTO,FAIXAVERIFICACAO');
                InsertSQL.add(' ,ITEMS,MASCARA,QUANTIDADECARACTERES,CAMPOSBD,PosicaoEsquerda,PosicaoSuperior,TabOrder)');
                InsertSQL.add('values (:CODIGO,:CONFDEV,:NOME,:COMPONENTE,:caption,:TIPO,:VERIFICABRANCO,:verificafaixa');
                InsertSQL.add(' ,:VERIFICANUMERICO,:VERIFICADATA,:VERIFICAHORA,:VERIFICARELACIONAMENTO');
                InsertSQL.add(' ,:FAIXAVERIFICACAO,:ITEMS,:MASCARA,:QUANTIDADECARACTERES');
                InsertSQL.add(' ,:CAMPOSBD,:PosicaoEsquerda,:PosicaoSuperior,:TabOrder)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCONFCAMPOSDEV set CODIGO=:CODIGO,CONFDEV=:CONFDEV');
                ModifySQL.add(',NOME=:NOME,COMPONENTE=:COMPONENTE,caption=:caption,TIPO=:TIPO,VERIFICABRANCO=:VERIFICABRANCO,verificafaixa=:verificafaixa');
                ModifySQL.add('VERIFICANUMERICO=:VERIFICANUMERICO,VERIFICADATA=:VERIFICADATA');
                ModifySQL.add(',VERIFICAHORA=:VERIFICAHORA,VERIFICARELACIONAMENTO=:VERIFICARELACIONAMENTO');
                ModifySQL.add(',FAIXAVERIFICACAO=:FAIXAVERIFICACAO,ITEMS=:ITEMS,MASCARA=:MASCARA');
                ModifySQL.add(',QUANTIDADECARACTERES=:QUANTIDADECARACTERES,CAMPOSBD=:CAMPOSBD');
                ModifySQL.add(',PosicaoEsquerda=:PosicaoEsquerda,PosicaoSuperior=:PosicaoSuperior,TabOrder=:TabOrder');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCONFCAMPOSDEV where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCONFCAMPOSDEV.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCONFCAMPOSDEV.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCONFCAMPOSDEV');
     Result:=Self.ParametroPesquisa;
end;

function TObjCONFCAMPOSDEV.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Configura��o de Campos de DEV ';
end;


function TObjCONFCAMPOSDEV.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENCONFCAMPOSDEV,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCONFCAMPOSDEV.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(Self.ListaCodigos);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.CONFDEV.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCONFCAMPOSDEV.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCONFCAMPOSDEV.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCONFCAMPOSDEV.EdtCONFDEVExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CONFDEV.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CONFDEV.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.CONFDEV.GET_NOME;
End;
procedure TObjCONFCAMPOSDEV.EdtCONFDEVKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCONFDEV:TFCONFDEV;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCONFDEV:=TFCONFDEV.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.CONFDEV.Get_Pesquisa,Self.CONFDEV.Get_TituloPesquisa,FCONFDEV)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CONFDEV.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CONFDEV.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CONFDEV.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCONFDEV);
     End;
end;

function TObjCONFCAMPOSDEV.CarregaCodigos(pcodigo: String): TStringList;
begin
     with Self.Objquery do
     begin
          close;
          sql.clear;
          sql.add('Select codigo from tabconfcamposdev where confdev='+pcodigo);
          open;

          Self.ListaCodigos.clear;
          while not(eof) do
          Begin
               Self.ListaCodigos.add(fieldbyname('codigo').asstring);
               next;
          End;
     End;
End;
end.



