unit UobjCOBRFATURA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJNOTAFISCAL
;
//USES_INTERFACE



Type
   TObjCOBRFATURA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               notafiscal:TOBJNOTAFISCAL;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_observacao(parametro: string);
                Function Get_observacao: string;
                Procedure Submit_nFat(parametro: string);
                Function Get_nFat: string;
                Procedure Submit_vOrig(parametro: string);
                Function Get_vOrig: string;
                Procedure Submit_vDesc(parametro: string);
                Function Get_vDesc: string;
                Procedure Submit_vLiq(parametro: string);
                Function Get_vLiq: string;
                procedure EdtnotafiscalExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtnotafiscalKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               queryTemp:TIBQuery;
               querytemp2:TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               observacao:string;
               nFat:string;
               vOrig:string;
               vDesc:string;
               vLiq:string;
//CODIFICA VARIAVEIS PRIVADAS








               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UNotaFiscal;





Function  TObjCOBRFATURA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.observacao:=fieldbyname('observacao').asstring;
        Self.nFat:=fieldbyname('nFat').asstring;
        Self.vOrig:=fieldbyname('vOrig').asstring;
        Self.vDesc:=fieldbyname('vDesc').asstring;
        Self.vLiq:=fieldbyname('vLiq').asstring;
        If(FieldByName('notafiscal').asstring<>'')
        Then Begin
                 If (Self.notafiscal.LocalizaCodigo(FieldByName('notafiscal').asstring)=False)
                 Then Begin
                          Messagedlg('notafiscal N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.notafiscal.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO







        result:=True;
     End;
end;


Procedure TObjCOBRFATURA.ObjetoparaTabela;
begin

  With Objquery do
  Begin
    ParamByName('Codigo').asstring:=Self.Codigo;
    ParamByName('observacao').asstring:=Self.observacao;
    ParamByName('nFat').asstring:=Self.nFat;
    ParamByName('vOrig').asstring:=virgulaparaponto(Self.vOrig);
    ParamByName('vDesc').asstring:=virgulaparaponto(Self.vDesc);
    ParamByName('vLiq').asstring:=virgulaparaponto(Self.vLiq);
    ParamByName('notafiscal').asstring:=Self.notafiscal.GET_CODIGO;
  End;

End;

//***********************************************************************

function TObjCOBRFATURA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;

  Self.ObjetoParaTabela;

  queryTemp.Active := false;
  queryTemp.SQL.Clear;
  queryTemp.SQL.Text := 'delete from tabcobrfatura where notafiscal = '+notafiscal.Get_CODIGO; {a chave � delete cascade}
  try
    queryTemp.ExecSQL;
  except
    on e:exception do
    begin
      MensagemErro('Erro ao deletar Cobran�a fatura. '+e.Message);
      Exit;
    end
  end;

 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOBRFATURA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        observacao:='';
        nFat:='';
        vOrig:='';
        vDesc:='';
        vLiq:='';
        notafiscal.ZerarTabela;
//CODIFICA ZERARTABELA







     End;
end;

Function TObjCOBRFATURA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  if Self.nFat = '' then
    Mensagem:=Mensagem+'N�mero fatura';

  if self.vOrig = '' then
    Mensagem:=Mensagem+'Valor original duplicata';

  if self.vLiq = '' then
    Mensagem:=Mensagem+'Valor liquido duplicata';

  If (Codigo='') Then
    Mensagem:=mensagem+'/Codigo';

  if mensagem<>'' Then
  Begin
    messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
    exit;
  End;
   result:=false;
end;


function TObjCOBRFATURA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.notafiscal.LocalizaCodigo(Self.notafiscal.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ notafiscal n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCOBRFATURA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        Strtofloat(Self.vOrig);
     Except
           Mensagem:=mensagem+'/vOrig';
     End;
     try
        Strtofloat(Self.vDesc);
     Except
           Mensagem:=mensagem+'/vDesc';
     End;
     try
        Strtofloat(Self.vLiq);
     Except
           Mensagem:=mensagem+'/vLiq';
     End;
     try
        If (Self.notafiscal.Get_Codigo<>'')
        Then Strtoint(Self.notafiscal.Get_Codigo);
     Except
           Mensagem:=mensagem+'/notafiscal';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCOBRFATURA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCOBRFATURA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCOBRFATURA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COBRFATURA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,observacao,nFat,vOrig,vDesc,vLiq,notafiscal');
           SQL.ADD(' from  TABCOBRFATURA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOBRFATURA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCOBRFATURA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCOBRFATURA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCOBRFATURA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.queryTemp:=TIBQuery.create(nil);
        Self.queryTemp2:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.queryTemp.Database:=FDataModulo.IbDatabase;
        Self.queryTemp2.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.notafiscal:=TOBJNOTAFISCAL.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCOBRFATURA(Codigo,observacao,nFat,vOrig');
                InsertSQL.add(' ,vDesc,vLiq,notafiscal)');
                InsertSQL.add('values (:Codigo,:observacao,:nFat,:vOrig,:vDesc,:vLiq');
                InsertSQL.add(' ,:notafiscal)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCOBRFATURA set Codigo=:Codigo,observacao=:observacao');
                ModifySQL.add(',nFat=:nFat,vOrig=:vOrig,vDesc=:vDesc,vLiq=:vLiq,notafiscal=:notafiscal');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCOBRFATURA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOBRFATURA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOBRFATURA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOBRFATURA');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOBRFATURA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de COBRFATURA ';
end;


function TObjCOBRFATURA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCOBRFATURA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCOBRFATURA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOBRFATURA.Free;
begin
    Freeandnil(Self.Objquery);
    FreeAndNil(Self.queryTemp);
    FreeAndNil(Self.queryTemp2);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.notafiscal.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOBRFATURA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOBRFATURA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCobrFATURA.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjCobrFATURA.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjCobrFATURA.Submit_observacao(parametro: string);
begin
        Self.observacao:=Parametro;
end;
function TObjCobrFATURA.Get_observacao: string;
begin
        Result:=Self.observacao;
end;
procedure TObjCobrFATURA.Submit_nFat(parametro: string);
begin
        Self.nFat:=Parametro;
end;
function TObjCobrFATURA.Get_nFat: string;
begin
        Result:=Self.nFat;
end;
procedure TObjCobrFATURA.Submit_vOrig(parametro: string);
begin
        Self.vOrig:=Parametro;
end;
function TObjCobrFATURA.Get_vOrig: string;
begin
        Result:=Self.vOrig;
end;
procedure TObjCobrFATURA.Submit_vDesc(parametro: string);
begin
        Self.vDesc:=Parametro;
end;
function TObjCobrFATURA.Get_vDesc: string;
begin
        Result:=Self.vDesc;
end;
procedure TObjCobrFATURA.Submit_vLiq(parametro: string);
begin
        Self.vLiq:=Parametro;
end;
function TObjCobrFATURA.Get_vLiq: string;
begin
        Result:=Self.vLiq;
end;
//CODIFICA GETSESUBMITS


procedure TObjCOBRFATURA.EdtnotafiscalExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.notafiscal.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.notafiscal.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.notafiscal.Get_Numero;
End;
procedure TObjCOBRFATURA.EdtnotafiscalKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FNOTAFISCAL:TFNOTAFISCAL;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FNOTAFISCAL:=TFNOTAFISCAL.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.notafiscal.Get_Pesquisa,Self.notafiscal.Get_TituloPesquisa,Fnotafiscal)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.notafiscal.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.notafiscal.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.notafiscal.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FNOTAFISCAL);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjCOBRFATURA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOBRFATURA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;


end.



