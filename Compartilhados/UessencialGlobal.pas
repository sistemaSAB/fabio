unit UessencialGlobal;

interface

uses
  ibquery, stdctrls, messages, comObj, windows, strutils, DB, dbgrids, Forms, ComCtrls, Buttons,
  Mask, Controls, extctrls , classes, qrctrls, grids, inifiles, ppctrls, graphics, IBStoredProc,
  RDPrint, TLHelp32, PsAPI, shellapi, Variants,pcnLeitor,pcnConversao;


const IndiceBtOpcoes=0;

type

  //Str255=String[255];
  
  Vetor1a8de35=array[1..8] of string;

  TRegFocus=record
    Edit:TEdit;
    Mask:TMaskEdit;
    memo:TMemo;
    ListBox:TListBox;
    ComboBox:TComboBox;
    CorAnteriorGlobal:TColor;
  end;

// ==
type
  TDuploClique = class  
    procedure AdicionaDuploClique(Sender : TObject);
    procedure edtDblClick(Sender: TObject);
  end;

type
  TobjSqlTxt = class(TComponent)
  private
    arq: TextFile;
    FpathArq: string;
    str: TStringList;
    procedure inicializa;
    procedure SetpathArq(const Value: string);
    function contemSection(const value: string):Boolean;
  protected
    property pathArq: string read FpathArq write SetpathArq;
  public
    constructor Create(AOwner: TComponent;pathArq:string = '');
    destructor destroy;override;
    function readString(const sectionName:string; tiraPontoVirgula:Boolean = False):string;
    function readStringSilencioso(const sectionName:string; tiraPontoVirgula:Boolean = False):string;
    procedure writeString(const sectionName:string; novoValor:string);
    procedure fechaArquivo();
end;

function InverteDataSQL(Pdata:String):string;

function Cronometro(Pinicial:string;PFinal:String;out presultado:String):boolean;
procedure apagalinhagrid(Grid:TstringGrid;posicao: integer);
function RetornaDataServidor: Tdate;
function RetornaDataHoraServidor: TdateTime;
procedure MostraMensagemSistemaDemo;overload;
procedure MostraMensagemSistemaDemo(parametro:string);overload;
function ProcuraFrase(Arquivo:string;frase:string):boolean;

function NumeroExtenso_Bilhao(const Valor: double; Moeda: Boolean = False): string;
function numeroExtenso(Valor: String):String;
function valorextenso (valor: Currency): string;

function ValidaCNPJ(const cCNPJ: string): boolean;
function  ValidaCPF(const cCPF: String): boolean;
function  ExplodeStr_COMVAZIOS(Pcodigos:String;var StrRetorno:TStringList;PSeparador:string;PtipoValor:String):boolean;
function  CarregaAtualizacao:boolean;
function  PegaPathSistema:String;
function PegaPathWindows:String;
function ValidaPathDiretorio(pDiretorio:String):boolean;
function ValidaPathArquivo(pArquivo:String):boolean;
procedure AbreCalendario(editenviou: TObject;Form:TForm);
function AdiquiraOuCrieObjeto(const ClasseNome: String): IDispatch;

function  Retorna_codigo(Pcodigos:String;var StrRetorno:TStringList):boolean;

function  ExplodeStr(Pcodigos:String;var StrRetorno:TStringList;PSeparador:string;PtipoValor:String):boolean;overload;
function  ExplodeStr(Pcodigos:String;var StrRetorno:TStrings;PSeparador:string;PtipoValor:String):boolean;overload;

function  Implode(StringList:TStringList;var retorno:string):boolean;

function  tirazeroesquerda(parametro:string):string;
function  StrReplace(Ptexto,Pantiga,Pnova:String):String;
function  tira_espacos_final(palavra:string):String;
function  VerificaSoNumeros(Pvalor:String):boolean;
function  AdicionaVirgulaDecimal(Pvalor:String;QtdeCasas:Integer):String;
function  CalculaDigitoVerificador(PCodigo: string): string;
function  AdicionaZero(PValor:String):String;
procedure PegaCorForm(PForm:Tform);overload;
procedure PegaCorForm(PForm:Tframe);overload;

procedure formatadbgrid(grid:Tdbgrid;QUERY:TIBquery);overload;

procedure Formatadbgrid_MySql(grid:Tdbgrid);

procedure formatadbgrid(grid:Tdbgrid);overload;
procedure Formatadbgrid_3_casas(grid:Tdbgrid);
procedure Formatadbgrid_5_casas(grid:Tdbgrid);
procedure Formatadbgrid(grid:Tdbgrid;PnumeroCasas:integer);overload;

function preenche(palavra:string;quantidade:Integer;ValorASerUSado:string):String;

function CompletaPalavra(palavra:string;quantidade:Integer;ValorASerUSado:string):String;

function LimitaPalavra(palavra:string;quantidade:Integer):String;

function CompletaPalavra_a_Esquerda(palavra:string;quantidade:Integer;ValorASerUSado:string):String;

procedure limpaedit(FORMULARIO:Tform);overload;
procedure limpaedit(FORMULARIO:Tpanel);overload;
procedure limpaedit(FORMULARIO:TGroupBox);overload;
procedure limpaedit(FORMULARIO:Tframe);overload;

procedure desabilita_campos(FORMULARIO:Tform);overload;
procedure desabilita_campos(FORMULARIO:Tpanel);overload;
procedure desabilita_campos(FORMULARIO:TGroupBox);overload;
procedure desabilita_campos(FORMULARIO:Tframe);overload;//PROC PARA DESABILITAR OS EDITS

procedure habilita_campos(FORMULARIO:Tform);overload;
procedure habilita_campos(FORMULARIO:TPanel);overload;
procedure habilita_campos(FORMULARIO:TGroupBox);overload;
procedure habilita_campos(FORMULARIO:Tframe);overload;

procedure limpalabel(FORMULARIO:Tform);
procedure retira_fundo_labels(formulario:Tform);

procedure esconde_botoes(formulario:Tform);overload;
procedure esconde_botoes(formulario:Tpanel);overload;
procedure mostra_botoes(formulario:Tform);overload;
procedure mostra_botoes(formulario:Tpanel);overload;
procedure mostra_botoes(formulario:Tframe);overload;

procedure esconde_botoes(formulario:Tframe);overload;

procedure habilita_botoes(formulario:Tform);overload;
procedure habilita_botoes(formulario:Tpanel);overload;
procedure habilita_botoes(formulario:TGroupBox);overload;
procedure habilita_botoes(formulario:Tframe);overload;

procedure desab_botoes(formulario:Tform);overload;
procedure desab_botoes(formulario:TPanel);overload;
procedure desab_botoes(formulario:TGroupBox);overload;
procedure desab_botoes(formulario:Tframe);overload;

procedure Esconde_campos(FORMULARIO:Tform);overload;
procedure Esconde_campos(FORMULARIO:Tpanel);overload;
procedure Esconde_campos(FORMULARIO:TGroupBox);overload;
procedure Esconde_campos(FORMULARIO:Tframe);overload;

function tira_ponto(valor:string):string;
function tira_caracter(valor:string; PCaracter : string):string;
function tira_decimal(valor:string):string;
function tira_virgula(valor:string):string;
function formata_valor(valor:string):string;overload;
function formata_valor(valor:Currency):string;overload;
function formata_valor(valor:string;pCasasDecimais:Integer;pArredonda:boolean):string;overload;

function formata_valor_1_casa(valor:currency):string;

function formata_valor_4_casas(valor:string):string;
function formata_valor_3_casas(valor:string):string;
function formata_valor_tempo_real_2_casas(pTexto: String;sender:Tobject):String;
function formata_valor_tempo_real_N_casas(pTexto: String;sender:Tobject;pCasasDecimais:integer):String;
function formata_hora(valor:string):string;
function formata_horaHHMMSS(valor:string):string;
function formata_dataDDMMAAAA(valor:String):String;

function formata_valor_com_virgula(valor:string):string;overload;

function pontoparavirgula(valores:string):string;
function virgulaparaponto(valores:string):string;
function comebarra(texto:string):string;
function inserebarra(texto:string):string;
function insere_dois_pontos(texto:string):string;
function come(texto:string;simbolo:Char):string;
function troca(texto:string;procura:Char;troca:char):string;overload;

procedure ValorExtenso_DivididoDois(Valor:Currency;out Extenso1,Extenso2:String);
function ArredondaValor(Valor:Real):String;
function ArredondaValor_1_Casa(Valor:Real):Integer;
function MesExtenso(PMes:Word):string;
function RetornaDigitoData(Str:String):integer;
function  RetornadiaExtenso(dia:integer):string;
function AcertaMaiusculaMinusculo(Frase:String):String;
function PegaPrimeiroNome(Nome:String):String;

function PegaDataExtenso(Data:Tdate):STRING;
function GeraZero(Quant:Byte):string;
function Get_PathFotos:String;
function RetornaPastaSistema:String;
function RetornaNomeEXE:string;
procedure DividirValor(apoio: string;Quant1,Quant2:INteger;out Saida1,Saida2:String);overload;
procedure DividirValor(apoio: string;Quant:INteger;out Saida1,Saida2:String);overload;
procedure DividirValorSemQuebra(apoio: string;Quant:INteger;out Saida1,Saida2:String);
procedure DividirValorCOMSEGUNDASEMTAMANHO(apoio: string;Quant1:INteger;out Saida1,Saida2:String);
procedure DividirValorEmStringlist(apoio:String; Quant:integer; var Pstringlist:TStringList);
function TotalLinhas(Pfrase:string;ColunasporLinha:integer ):integer;
function TotalLinhasStringlist(PSTL:TStringList;ColunasporLinha:integer ):integer;
function DivideEmLinhas(pFrase:String;ColunasporLinha:integer):string;

procedure AjustaLArguraColunaGrid(PGrid: TStringGrid);overload;
procedure AjustaLArguraColunaGrid(PGrid: TStringGrid;PFonte:TFontName;Ptamanhofonte:integer);overload;
function VerificaBaseRemota(PathBase:String):Boolean;
function PegaPathBanco: String;overload;
function PegaPathBanco(nomechave:String): String;overload;
function PegaPathBancoDAV(nomechave:String;grupo:String): String;overload;
function Submit_ComboBox(PCombo:TComboBox):string;

procedure PegaFiguraBotoesPEquenos(Botaonovo,BotaoAlterar,BotaoCancelar,BotaoGravar,BotaoPesquisar,BotaoRelatorios,BotaoExcluir,BotaoSair:TBitBtn);
procedure PegaFiguraBotoes(Botaonovo,BotaoAlterar,BotaoCancelar,BotaoGravar,BotaoPesquisar,BotaoRelatorios,BotaoExcluir,BotaoSair:TSpeedButton);overload;
procedure PegaFiguraBotoes(Botaonovo,BotaoAlterar,BotaoCancelar,BotaoGravar,BotaoPesquisar,BotaoRelatorios,BotaoExcluir,BotaoSair:TBitBtn);overload;
procedure PegaFiguraBotao(Parametro:Tspeedbutton;Nome:String);overload;
procedure PegaFiguraBotao(Parametro:TBitBtn;Nome:String);overload;

procedure PegaFigura(Imagem:Timage;NomeArq:String);overload;
procedure PegaFigura(Imagem:TQrimage;NomeArq:String);overload;
procedure PegaFigura(Imagem:TPPimage;NomeArq:String);overload;

procedure Filtradatas(out Plista:TStringList);
procedure edtf9KeyPress(Sender: TObject; var Key: Char);
procedure FormKeyPress13(Sender: TObject; var Key: Char);
procedure BarraDataKeyUP(Sender: TObject; var Key: Word; Shift: TShiftState);
//procedure edtKeyPress_SoNumeros(Sender: TObject; var Key: Char);
function Centraliza_String(Ptexto:String;Pquantidade:integer):string;overload;
function Centraliza_String(Ptexto:String;Pquantidade:integer;PCompleta:string):string;overload;
function Centraliza_String(Ptexto:String;Pquantidade:integer;PCompleta:string;TruncaString:boolean):string;overload;

function Arredonda(PValor:String):String;
function Troca_13_10_por_pipeline(palavra:string):string;
function Criptografacaixa(PNumeroCaixa:String):String;
function DesCriptografacaixa(PNumeroCaixa:String):String;
function PegaPrimeiraPalavra(Palavra:String):String;
function RetornaSoNumeros(Palavra:String):String;
function RetornaNumeroseLetras(Palavra:String):String;
function RetornaSoLetras(Palavra:string):string;
function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;

procedure MensagemErro(Mensagem:String);
procedure MensagemAviso(Mensagem:String);
function  MensagemPergunta(Mensagem:String):integer;
procedure MensagemSucesso(Mensagem:String);

function ContaRegistros(ptabela,pcampo: string): string;

function RETORNA_HORA_5_DIGITOS(Phora:String):string;

function LeChaveIni(nomearquivo,secao,chave:String;var valor:String): Boolean;overload;
function GravaChaveIni(nomearquivo,secao,chave,valor:String): Boolean; overload;

function LeChaveIni(nomearquivo,secao,chave:String;var valor:String;silencioso:boolean): Boolean;overload;
function GravaChaveIni(nomearquivo,secao,chave,valor:String;silencioso:boolean): Boolean;overload;

procedure ValidaNumeros(var texto:tedit;var key: char;tipo: string);overload;

function cdate2julian(pvencimento:String):string;

//******* Feito por F�bio **************
function AbrirImpressao: Boolean;
function AbrirImpressaoPaisagem: Boolean;
procedure ImprimirSimples(Var PLinha: Integer; PColuna:Integer;PTexto : String);
procedure ImprimirNegrito(Var PLinha: Integer; PColuna:Integer;PTexto : String);
function FecharImpressao: Boolean;
function Deleta_Linha_StringGrid(out StrGrig : TStringGrid; linha : Integer):Boolean;

procedure InicializaBarradeProgressoRelatorio(PRecordCount:Integer;PMensagem:String);
procedure IncrementaBarraProgressoRelatorio;
procedure FechaBarraProgressoRelatorio;

procedure IniciaProgresso(pMensagem: string; pMax: Integer);
procedure IncrementaProgresso(pQtde: integer);

function LinhaVertical:string;
procedure controlChange_focus(sender: TObject;pform:tform);
function DataDeModificacaoExe: String;
function DataDeCriacaoExe: String;
procedure MakeRounded(Control: TWinControl);

//Mauricio
function Submit_CheckBox(PcheckBox:TCheckBox):string;
function RetiraAspas(pparametro: string): string;

procedure RetiraAspasInicio_e_Fim(parametro: TStringList);overload;
function RetiraAspasInicio_e_Fim(parametro:string): string;overload;
function InsereInicio_e_Fim(pCaracterInserir,pTexto:string):string;

function DiferencaDias(DataVenc:TDateTime; DataAtual:TDateTime): Currency;
function ArredondaFloat(x : Real): Real;

//celio
function Bissexto( pAno : String ): Boolean;
function DiasdoMes( pAno,pMes : String ) : integer;

procedure LimpaStringGrid(Var StrGrid : TStringGrid);

function BlockInput(fBlockIt: Boolean): Boolean; stdcall; external 'User32.dll';

function CarregaCombo(var pCombo:TComboBox; pLista:TStringList):boolean;
function AjustaCentralizaObjetonaTela(Referencia:TControl; ObjetoaAjustar:TControl):boolean;

procedure Sublinha_Label(Sender: TObject; Shift: TShiftState;X, Y: Integer);
procedure Negrita_Label(Sender: TObject);

{jonas}
function alinhaTexto(str1,str2:string;espacamento:integer):string;
function get_campoTabela (campoSelect,campoWhere,nomeTabela,parametro:string;alias:string = ''):string;
function update_campoTabela(nomeTabela,campoUpdate,valor,campoWhere,parametro:string):Boolean;
function exec_sql (pSql:string):Boolean;
function trunca_string(palavra:string;pquantidade:Integer):string;
function formata_cnpj (CNPJ:string):string;
function formata_cpf  (cpj:string):string;
function fDate (parametro:string):string;
function fTime(parametro:string):string;
procedure chamaFormulario  (sender:TForm; tipo:TFormClass; Owner:TComponent=nil);overload;
procedure chamaFormulario  (tipo:TFormClass;Owner:TComponent;tag:string = '');overload;
function embaralha_desembaralha(parametro:string):string;
function verificaCapsLock():Boolean;
function get_valorF(pValor:string;pSimbolo:string = 'R$'):string;
procedure primeiroUltimoDia(mes,ano:String; var dataIni,dataFin:string);
function trunca(str: currency): Currency; {trunca duas casas}
function get_cfop (pCodigoProduto,pCliente,estadoEmpresa:string):string;
function AllTrim (str:string):string;
function validaData (data:string):Boolean;
function format_db (str:string):string;
function format_si (str:string):string;
function getEstoqueSazional(pProduto:string;pData:string = ''):string;
function validaInscricaoEstadual( inscricao, UF : string ) : Boolean;
function getUF(pCodigoUF : integer) : string;
function getCodUF(pUF : string) : Integer;
function copyStrFront(chrBack,str : string) :string;
function tira_pontoVirgula(info:string):string;
procedure trocaWhile(var info:string; indIni:Integer; chrBreak:string; pTroca:string);overload;
function criarDiretorio(nomeDir:string;var msg:string):Integer;
procedure limpaSoEdit(form:TForm);
function default(valor:string;default:string='0'):string;
function RetornaValorCampos(Parquivo,PCampo: String): string;
procedure get_impostoCOFINS(valorF: string; var bcCOFINS, vCOFINS, pCOFINS: Real;cstCOFINS:string);
procedure get_impostoPIS(valorF: string; var bcPIS, vPIS, pPIS: real;cstPIS:string);

//Jonatan
function RetornaListadeDatasEmUmIntervalo(DataInicio,DataFim:string;var ListaDatas:TStringList;intervalo:string):Boolean ;
function RetornaQuantidadeDeDiasIntervaloDatas(dataini,datafim:TDateTime):integer;
function Criptografa(parametro: string):string;
function TerminarProcessoWord(sFile: String): Boolean;
function RetornaDiasUteisEntreDuasDatas(dataini,datafin:string):integer;
function DiaSemana(Data:TDateTime): String;
function VerificaDuplicidadeCPFCGCCadastro(NOMETABELA,NOMECAMPOTABELA,CFPCNPJVERIFICAR,Codigo:string):Boolean;

//Fim das minhas baga�as

function ForceForegroundWindow(hwnd: THandle): Boolean;

procedure BloqueiaTecladoeMouse;
procedure DesbloqueiaTecladoeMouse;

procedure ForcaGravacaoSemBuffer(Arquivo:String);
function GravaLOG(pNomeArquivo,pTexto:string):Boolean;
function IsNumeric(parametro: String) : boolean;
function PesquisaStringList(pTexto:string;plista:TStringList):Integer;
function retornaPalavrasAntesSimbolo(str: string;Simbolo:string): string;
function retornaPalavrasDepoisSimbolo(str: string;Simbolo:string): string;
function retornaPalavrasDepoisSimbolo2(str: string;Simbolo:string): string;
function tbKeyIsDown(const Key: integer): boolean;

{Rodolfo}
function StrReplaceRef(pString: string): String;
procedure Explode(str, separador: string; var StrList: TStringList);
procedure RetiraAspasInicio(parametro: TStringList); overload;
function RetiraAspasInicio(str: string): string; overload;
function GetVencimentoCertificadoDigital : string;
procedure VerificaVencimentoCertificadoDigital; 
//==

procedure FixDBGridColumnsWidth(const Grid: TDBGrid);
procedure AdjustColumnWidths(DBGrid: TDBGrid);

Function ordinal(numero: string): string;
function DecToBin(Decimal:integer): string;

procedure Espera( pMensagem: string; Fecha: Boolean = False );
function TamanhoArquivo( pArquivo:string ): Integer;

function ValidaIE(pIE, pUF: string): string;
function ValidarUF(UF: AnsiString):string ;

function VerificaBarraFinalDiretorio( pvalor:string ):string;
procedure abreTXTnotepad(parquivo:String);

procedure TrimAppMemorySize;
Function ReplaceNonAscii(const s: String) : String;


var
  regFocus:TRegFocus;

implementation

uses
  sysutils,dialogs, FileCtrl, Useg, Ucalendario, DateUtils,
  UReltxtRDPRINT, UMostraBarraProgresso, UDataModulo, UComponentesNfe,
  uEspera;

function MesExtenso(PMes:Word):string;
const
  Meses: array[1..12] of string[12] =
  ('Janeiro','Fevereiro','Mar�o','Abril','Maio','Junho','Julho',
  'Agosto','Setembro','Outubro','Novembro','Dezembro');
begin
    result:=meses[PMes];
end;

function pontoparavirgula(valores:string):string;
var
  cont:byte;
begin
  for cont:=1 to length(valores) do
  begin
    if valores[cont]= '.' then
      valores[cont]:= ','
  end;
  Result:=valores;
end;

function virgulaparaponto(valores:string):string;
var
  cont:byte;
begin
  for cont:=1 to length(valores) do
  begin
    if valores[cont]=',' then
      valores [cont]:='.'
  end;
  Result:=valores;
end;

function comebarra(texto:string):string;
var //funcao que retira as barras de data de um tipo data passando para string
  i :integer;
  textosai:string;
begin
  textosai:='';
  for i :=1 to length(texto) do
  begin

    if ((texto[i] ='/') or (texto[i]=' ') or (texto[i]=':'))
    then
    else
       textosai:=textosai+texto[i];
  end;
  result:=textosai;
end;


function inserebarra(texto:string):string;
var //funcao que insere as barras de datas
  i :integer;
  textosai:string;
begin
  result:=texto;

  if (Length(texto)>=4) then
  begin
    textosai:='';
    try
      textosai:=texto[1]+texto[2]+'/'+texto[3]+texto[4]+'/'+copy(texto,5,length(texto)-4);
      result:=textosai;
    except
      result:=Texto;
    end;
  end;
end;

function insere_dois_pontos(texto:string):string;
var //funcao que insere as barras de datas
  i :integer;
  textosai:string;
begin
  result:=texto;

  if (Length(texto)=4) then
  begin
    textosai:='';
    try
      textosai:=texto[1]+texto[2]+':'+texto[3]+texto[4];
      result:=textosai;
    except
         result:=Texto;
    end;
  end;
end;

function troca(texto:string;procura:Char;troca:char):string;
var //funcao que retira as barras de data de um tipo data passando para string
  i :integer;
begin
  for i :=1 to length(texto) do
  begin
    if (texto[i] =procura)
    then texto[i]:=troca;
  end;
  result:=texto;
end;


function come(texto:string;simbolo:Char):string;
var //funcao que retira as barras de data de um tipo data passando para string
  i : integer;
  textosai : string;
begin
  textosai:='';
  
  for i :=1 to length(texto) do
  begin
    if (texto[i] =simbolo)
    then
    else
      textosai:=textosai+texto[i];
  end;
  
  result:=textosai;
end;

//PROCEDIMENTOS DE LIMPEZAS E DESABILITACOES E HABILITACOES

procedure retira_fundo_labels(formulario:Tform);
var
  int_habilita:integer;
begin
  for int_habilita:=0 to formulario.ComponentCount -1  do
  begin
    if (uppercase(formulario.Components [int_habilita].ClassName) = 'TLABEL') then
      Tlabel(formulario.Components [int_habilita]).Transparent:=true;
  end;
end;


procedure mostra_botoes(formulario:Tform);
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Components [int_habilita]).visible:=true;
    end;
end;



procedure mostra_botoes(formulario:Tframe);
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Components [int_habilita]).visible:=true;
    end;
end;

procedure mostra_botoes(formulario:Tpanel);
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.controlCount -1
   do begin
        if (formulario.controls [int_habilita].ClassName = 'TBitBtn') or
        (formulario.controls [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.controls [int_habilita]).visible:=true;
    end;
end;


procedure esconde_botoes(formulario:Tframe);
  var
  int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
           (formulario.Components [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Components [int_habilita]).visible:=false;
    end;

end;

procedure esconde_botoes(formulario:Tform);
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Components [int_habilita]).visible:=false;
    end;
end;


procedure esconde_botoes(formulario:Tpanel);
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.controlCount -1
   do begin
        if (formulario.controls [int_habilita].ClassName = 'TBitBtn') or
        (formulario.controls [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.controls [int_habilita]).visible:=false;
    end;
end;


procedure habilita_botoes(formulario:Tform);//PROC PARA HABILITAR OS BOTOES
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton') or
        (formulario.Components [int_habilita].ClassName = 'TSpeedButton')
        then
          TBitBtn(formulario.Components [int_habilita]).enabled:=true;
    end;
end;

procedure habilita_botoes(formulario:TPanel);//PROC PARA HABILITAR OS BOTOES
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ControlCount -1
   do begin
        if (formulario.Controls [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Controls [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Controls [int_habilita]).enabled:=true;
    end;
end;

procedure habilita_botoes(formulario:TGroupBox);//PROC PARA HABILITAR OS BOTOES
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ControlCount -1
   do begin
        if (formulario.Controls [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Controls [int_habilita]).enabled:=true;
    end;
end;
procedure habilita_botoes(formulario:Tframe);//PROC PARA HABILITAR OS BOTOES
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Components [int_habilita]).enabled:=true;
    end;
end;


procedure desab_botoes(formulario:Tform);//PROC P/ DESABILITAR OS BOTOES
  var
  int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
           (formulario.Components [int_habilita].ClassName = 'TButton') or
           (formulario.Components [int_habilita].ClassName = 'TSpeedButton')
        then
          TBitBtn(formulario.Components [int_habilita]).enabled:=false;
    end;

end;

procedure desab_botoes(formulario:TPanel);//PROC P/ DESABILITAR OS BOTOES
  var
  int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ControlCount -1
   do begin
        if (formulario.Controls[int_habilita].ClassName = 'TBitBtn') or
           (formulario.Controls [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Controls[int_habilita]).enabled:=false;
    end;

end;

procedure desab_botoes(formulario:TGroupBox);//PROC P/ DESABILITAR OS BOTOES
  var
  int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ControlCount -1
   do begin
        if (formulario.Controls[int_habilita].ClassName = 'TBitBtn') or
           (formulario.Controls [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Controls[int_habilita]).enabled:=false;
    end;

end;
procedure desab_botoes(formulario:Tframe);//PROC P/ DESABILITAR OS BOTOES
  var
  int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
           (formulario.Components [int_habilita].ClassName = 'TButton')
        then
          TBitBtn(formulario.Components [int_habilita]).enabled:=false;
    end;

end;

procedure limpalabel(FORMULARIO:Tform);
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ComponentCount -1
   do begin
        if FORMULARIO.Components [limpa].ClassName = 'TQRLabel'
        then TQRLabel(FORMULARIO.Components [limpa]).caption:=''
        Else
                if FORMULARIO.Components [limpa].ClassName = 'TLabel'
                then TLabel(FORMULARIO.Components [limpa]).caption:='';

   end;

end;


procedure limpaedit(FORMULARIO:Tform);//PROC PARA LIMPAR OS
                                      //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ComponentCount -1
   do begin
        if FORMULARIO.Components [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.Components [limpa]).text:='';
                Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.Components [limpa]).text:='';
                    TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.Components [limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
                        //TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
                End
                Else
                    if FORMULARIO.Components [limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.Components [limpa]).lines.clear;
                            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
                    End
                    else
                        if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit'
                        then Begin
                                  TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
                                  TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                        End
                        Else
                            if FORMULARIO.Components [limpa].ClassName = 'TRichEdit'
                            then Begin
                                    TRichEdit(FORMULARIO.Components [limpa]).text:='';
                                    TRichEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                            End
                            ELSE
                                if FORMULARIO.Components [limpa].ClassName = 'TCheckBox'
                                then  TCheckBox(FORMULARIO.Components [limpa]).Checked:=False;



   end;

end;
procedure limpaedit(FORMULARIO:Tframe);//PROC PARA LIMPAR OS
                                    //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ComponentCount -1
   do begin
        if FORMULARIO.Components [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.Components [limpa]).text:='';
                Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.Components [limpa]).text:='';
                    TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.Components [limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
                        TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
                End
                else
                    if FORMULARIO.Components [limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.Components [limpa]).lines.clear;
                            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
                    End
                    else
                          if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit'
                          then Begin
                                    TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
                                    TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                          End
                          Else
                                    if FORMULARIO.Components [limpa].ClassName = 'TRichEdit'
                                    then Begin
                                            TRichEdit(FORMULARIO.Components [limpa]).text:='';
                                            TRichEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                                    End
                                    ELSE
                                          if FORMULARIO.Components [limpa].ClassName = 'TCheckBox'
                                          then TCheckBox(FORMULARIO.Components [limpa]).Checked:=False;
   End;
end;


procedure limpaedit(Formulario:TPanel);//PROC PARA LIMPAR OS
                                     //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ControlCount -1
   do begin
        if FORMULARIO.Controls [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.controls[limpa]).text:='';
                Tedit(FORMULARIO.controls[limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.controls[limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.controls[limpa]).text:='';
                    TMaskEdit(FORMULARIO.controls[limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.controls[limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.controls[limpa]).itemindex:=-1;
                        TCombobox(FORMULARIO.controls[limpa]).Ctl3D:=False;
                End
                Else
                    if FORMULARIO.controls[limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.controls[limpa]).lines.clear;
                            TMemo(FORMULARIO.controls[limpa]).Ctl3D:=False;
                    End
                    else
                      if FORMULARIO.Controls [limpa].ClassName = 'TLabeledEdit'
                      then Begin
                                      TLabeledEdit(FORMULARIO.Controls [limpa]).text:='';
                                      TLabeledEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
                      End
                      Else
                        if FORMULARIO.Controls [limpa].ClassName = 'TRichEdit'
                        then Begin
                                                TRichEdit(FORMULARIO.Controls [limpa]).text:='';
                                                TRichEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
                        End
                        ELSE
                          if FORMULARIO.Controls [limpa].ClassName = 'TCheckBox'
                          then TCheckBox(FORMULARIO.Controls [limpa]).Checked:=False;
   End;

end;

procedure limpaedit(Formulario:TGroupBox);//PROC PARA LIMPAR OS
                                     //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ComponentCount -1
   do begin
        if FORMULARIO.Components [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.Components [limpa]).text:='';
                Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.Components [limpa]).text:='';
                    TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.Components [limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
                        TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
                End
                else
                    if FORMULARIO.Components [limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.Components [limpa]).lines.clear;
                            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
                    End
                    else
                    if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit'
                    then Begin
                                    TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
                                    TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                    End
                    Else
                          if FORMULARIO.Components [limpa].ClassName = 'TRichEdit'
                          then Begin
                                            TRichEdit(FORMULARIO.Components [limpa]).text:='';
                                            TRichEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                          End
                          ELSE
                          if FORMULARIO.Components [limpa].ClassName = 'TCheckBox'
                          then TCheckBox(FORMULARIO.Components [limpa]).Checked:=False;
   
   end;

end;

procedure desabilita_campos(FORMULARIO:Tform);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1
   do Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).enabled:=false
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).enabled:=false
            else
               if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
               then TComboBox(FORMULARIO.Components [int_habilita]).enabled:=false
               else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                  then TMemo(FORMULARIO.Components [int_habilita]).enabled:=false
                  else
                      if FORMULARIO.Components [int_habilita].ClassName = 'TRadioButton'
                      then TRadioButton(FORMULARIO.Components [int_habilita]).enabled:=false
                      else
                          if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled:=false
                          Else
                              if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Components [int_habilita]).enabled:=false
                              else
                                  if FORMULARIO.Components [int_habilita].ClassName = 'TCheckBox'
                                  then TCheckBox (FORMULARIO.Components [int_habilita]).Enabled:=False
                                  else
                                    if FORMULARIO.Components [int_habilita].ClassName = 'TRadioGroup'
                                    then TRadioGroup (FORMULARIO.Components [int_habilita]).Enabled:=False;



   End;

end;

procedure desabilita_campos(FORMULARIO:Tframe);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1
   do Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).enabled:=false
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).enabled:=false
            else
               if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
               then TComboBox(FORMULARIO.Components [int_habilita]).enabled:=false
               else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                  then TMemo(FORMULARIO.Components [int_habilita]).enabled:=false
                  else
                          if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled:=false
                          Else
                              if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Components [int_habilita]).enabled:=false
                              else
                                  if FORMULARIO.Components [int_habilita].ClassName = 'TCheckBox'
                                  then TCheckBox (FORMULARIO.Components [int_habilita]).Enabled:=False;
      End;

end;



procedure desabilita_campos(FORMULARIO:Tpanel);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ControlCount -1
   do Begin
        if FORMULARIO.Controls [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Controls [int_habilita]).enabled:=false
        else
           if FORMULARIO.Controls [int_habilita].ClassName = 'TMaskEdit'
           Then TMaskEdit(FORMULARIO.Controls [int_habilita]).enabled:=false
           else
              if FORMULARIO.Controls [int_habilita].ClassName = 'TComboBox'
              then TComboBox(FORMULARIO.Controls [int_habilita]).enabled:=false
              else
                 if FORMULARIO.Controls [int_habilita].ClassName = 'TMemo'
                 then TMemo(FORMULARIO.Controls [int_habilita]).enabled:=false
                 else
                     if FORMULARIO.Controls [int_habilita].ClassName = 'TLabeledEdit'
                     then TLabeledEdit(FORMULARIO.Controls[int_habilita]).enabled:=false
                     Else
                          if FORMULARIO.Controls [int_habilita].ClassName = 'TRichEdit'
                          then TRichEdit(FORMULARIO.Controls [int_habilita]).enabled:=false
                          else
                              if FORMULARIO.Controls [int_habilita].ClassName = 'TCheckBox'
                              then TCheckBox (FORMULARIO.Controls [int_habilita]).Enabled:=False;
      End;
end;

procedure desabilita_campos(FORMULARIO:TGroupBox);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ControlCount -1
   do Begin
        if FORMULARIO.Controls [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Controls [int_habilita]).enabled:=false
        else
           if FORMULARIO.Controls [int_habilita].ClassName = 'TMaskEdit'
           Then TMaskEdit(FORMULARIO.Controls [int_habilita]).enabled:=false
           else
              if FORMULARIO.Controls [int_habilita].ClassName = 'TComboBox'
              then TComboBox(FORMULARIO.Controls [int_habilita]).enabled:=false
              else
                     if FORMULARIO.Controls [int_habilita].ClassName = 'TMemo'
                     then TComboBox(FORMULARIO.Controls [int_habilita]).enabled:=false
                     else
                        if FORMULARIO.Controls [int_habilita].ClassName = 'TLabeledEdit'
                        then TLabeledEdit(FORMULARIO.Controls [int_habilita]).enabled:=false
                        Else
                            if FORMULARIO.Controls[int_habilita].ClassName = 'TRichEdit'
                            then TRichEdit(FORMULARIO.Controls [int_habilita]).enabled:=false
                            else
                              if FORMULARIO.Controls [int_habilita].ClassName = 'TCheckBox'
                              then TCheckBox (FORMULARIO.Controls [int_habilita]).Enabled:=False;
      End;
end;


procedure habilita_campos(FORMULARIO:Tform);//PROC PARA HABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1   do
   Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).enabled:=true
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).enabled:=true
            else
                if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
                then TComboBox(FORMULARIO.Components [int_habilita]).enabled:=True
                else
                    if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                    then TMemo(FORMULARIO.Components [int_habilita]).enabled:=true
                    Else
                        if FORMULARIO.Components [int_habilita].ClassName = 'TRadioButton'
                        then TRadioButton(FORMULARIO.Components [int_habilita]).enabled:=True
                        else
                          if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled:=True
                          Else
                              if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Components [int_habilita]).enabled:=True
                              else
                                  if FORMULARIO.Components [int_habilita].ClassName = 'TCheckBox'
                                  then TCheckBox (FORMULARIO.Components [int_habilita]).Enabled:=True
                                  else
                                    if FORMULARIO.Components [int_habilita].ClassName = 'TRadioGroup'
                                    then TRadioGroup (FORMULARIO.Components [int_habilita]).Enabled:=true;

   End;

end;

procedure habilita_campos(FORMULARIO:TPanel);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ControlCount -1   do
   Begin
        if FORMULARIO.Controls [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Controls [int_habilita]).enabled:=true
        else
            if FORMULARIO.Controls [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Controls[int_habilita]).enabled:=true
            else
                if FORMULARIO.Controls [int_habilita].ClassName = 'TComboBox'
                then TComboBox(FORMULARIO.Controls [int_habilita]).enabled:=True
                else
                    if FORMULARIO.Controls [int_habilita].ClassName = 'TMemo'
                    then TMemo(FORMULARIO.Controls [int_habilita]).enabled:=true
                    else
                          if FORMULARIO.Controls[int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Controls [int_habilita]).enabled:=True
                          Else
                              if FORMULARIO.Controls [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Controls [int_habilita]).enabled:=True
                              else
                                  if FORMULARIO.Controls [int_habilita].ClassName = 'TCheckBox'
                                  then TCheckBox (FORMULARIO.Controls [int_habilita]).Enabled:=True;
   End;
end;

procedure habilita_campos(FORMULARIO:TGroupBox);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ControlCount -1   do
   Begin
        if FORMULARIO.Controls [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Controls [int_habilita]).enabled:=true
        else
            if FORMULARIO.Controls [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Controls[int_habilita]).enabled:=true
            else
                if FORMULARIO.Controls [int_habilita].ClassName = 'TComboBox'
                then TComboBox(FORMULARIO.Controls [int_habilita]).enabled:=True
                else
                    if FORMULARIO.Controls[int_habilita].ClassName = 'TMemo'
                    then TMemo(FORMULARIO.Controls [int_habilita]).enabled:=true
                    else
                          if FORMULARIO.Controls [int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Controls [int_habilita]).enabled:=True
                          Else
                              if FORMULARIO.Controls [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Controls [int_habilita]).enabled:=True
                              else
                                  if FORMULARIO.Controls [int_habilita].ClassName = 'TCheckBox'
                                  then TCheckBox (FORMULARIO.Controls [int_habilita]).Enabled:=True;
   End;
end;

procedure habilita_campos(FORMULARIO:Tframe);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1   do
   Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).enabled:=true
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).enabled:=true
            else
                if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
                then TComboBox(FORMULARIO.Components [int_habilita]).enabled:=True
                else
                    if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                    then TMemo(FORMULARIO.Components [int_habilita]).enabled:=true
                    else
                          if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled:=True
                          Else
                              if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Components [int_habilita]).enabled:=True
                              else
                                  if FORMULARIO.Components [int_habilita].ClassName = 'TCheckBox'
                                  then TCheckBox (FORMULARIO.Components [int_habilita]).Enabled:=True;

   End;

end;


procedure Esconde_campos(FORMULARIO:Tform);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1
   do Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).visible:=false
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).visible:=false
            else
               if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
               then TComboBox(FORMULARIO.Components [int_habilita]).visible:=false
               else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                  then Tmemo(FORMULARIO.Components [int_habilita]).visible:=false
                  else
                      if FORMULARIO.Components [int_habilita].ClassName = 'TLabel'
                      then Tlabel(FORMULARIO.Components [int_habilita]).visible:=false;
      End;


end;
procedure Esconde_campos(FORMULARIO:Tframe);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1
   do Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).visible:=false
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).visible:=false
            else
               if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
               then TComboBox(FORMULARIO.Components [int_habilita]).visible:=false
               else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                  then Tmemo(FORMULARIO.Components [int_habilita]).visible:=false
                  else
                      if FORMULARIO.Components [int_habilita].ClassName = 'TLabel'
                      then Tlabel(FORMULARIO.Components [int_habilita]).visible:=false;
      End;


end;



procedure Esconde_campos(FORMULARIO:TPanel);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.controlCount -1
   do Begin
        if FORMULARIO.controls [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.controls [int_habilita]).visible:=false
        else
            if FORMULARIO.controls [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.controls [int_habilita]).visible:=false
            else
               if FORMULARIO.controls [int_habilita].ClassName = 'TComboBox'
               then TComboBox(FORMULARIO.controls [int_habilita]).visible:=false
               else
                  if FORMULARIO.controls [int_habilita].ClassName = 'TMemo'
                  then Tmemo(FORMULARIO.controls [int_habilita]).visible:=false
                  else
                      if FORMULARIO.controls [int_habilita].ClassName = 'TLabel'
                      then Tlabel(FORMULARIO.controls [int_habilita]).visible:=false;
      End;


end;

procedure Esconde_campos(FORMULARIO:TGroupBox);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1
   do Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).visible:=false
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).visible:=false
            else
               if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
               then TComboBox(FORMULARIO.Components [int_habilita]).visible:=false
               else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                  then Tmemo(FORMULARIO.Components [int_habilita]).visible:=false
                  else
                      if FORMULARIO.Components [int_habilita].ClassName = 'TLabel'
                      then Tlabel(FORMULARIO.Components [int_habilita]).visible:=false;
      End;


end;




//******************************************************************


Function ArredondaValor(Valor:Real):String;
var
ValorStr:String;
cont,cont2,contaposvirg:integer;
ValorFinal:String;
Begin
     ValorStr:=floattostr(Valor);
     ValorFinal:='';

     for cont:=1 to length(valorstr) do
     Begin
          If ValorStr[cont]=','
          Then Begin
                    ValorFinal:=Valorfinal+ValorStr[cont];
                    CONTAPOSVIRG:=0;
                    for cont2:=cont+1 to length(valorstr) do
                    Begin
                         ValorFinal:=Valorfinal+ValorStr[cont2];
                         Inc(contaposvirg,1);
                         If (contaposvirg=2)
                         Then break;
                    End;
                    break;
               End
          Else ValorFinal:=Valorfinal+ValorStr[cont];
    End;

    result:=ValorFinal;
End;


Function ArredondaValor_1_Casa(Valor:Real):Integer;
var
ValorStr:String;
cont,cont2,contaposvirg:integer;
ValorFinal:String;
Begin
     //esse procedimento pega um float e devolve um inteiro
     //arredondando pela primeira casa decimal
     //ou seja <5 nao arredonda
     //>=5 sobe 1
     //Exemplo
     //120,3 = 120
     //120,6 = 121

     result:=strtoint(floattostr(int(valor)));
     
     ValorStr:=Floattostr(Valor);
     
     ValorFinal:='';

     if (pos(',',ValorStr)=0)
     Then exit;

     for cont:=1 to length(valorstr) do
     Begin
          If ValorStr[cont]=','
          Then Begin
                    ValorFinal:=Valorfinal+ValorStr[cont];
                    if (cont<length(valorstr))
                    then Begin
                              if (strtoint(ValorStr[cont+1])<5)
                              Then ValorFinal:=ValorFinal+'0'
                              Else Begin
                                        ValorFinal:=ValorFinal+'0';
                                        ValorFinal:=Floattostr(Strtofloat(ValorFinal)+1);
                              End;
                    End
                    else ValorFinal:=ValorFinal+'0';

                    break;
          End
          Else ValorFinal:=Valorfinal+ValorStr[cont];
    End;

    result:=strtoint(Floattostr(int(Strtofloat(ValorFinal))));
End;



Function tira_virgula(valor:string):string;
var
str_valor:string;
cont:integer;
begin
     result:='';

     If (valor='')
     then exit;
     str_valor:='';
     for cont :=1 to length(valor)
     do begin
        if valor[cont]=','
        then
        else str_valor:=str_valor + valor[cont];
     end;
     result:=str_valor;
end;

Function tira_decimal(valor:string):string;
var
decimal:Currency;
begin
    result:='';

    If (Valor='')
    Then exit;
    
    valor:=tira_ponto(valor);
    try
       decimal:=strtofloat(valor);
       result:=formatfloat('###,##',decimal);
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
       exit;
    end;

end;

Function formata_valor_1_casa(valor:currency):string;
var
decimal:Currency;
begin
    result:='';
    try
       result:=formatfloat('###,##0.0',valor);
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
          exit;
    end;
end;


Function formata_valor(valor:currency):string;
var
decimal:Currency;
begin
    result:='';
    try
       result:=formatfloat('###,##0.00',valor);
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
          exit;
    end;
end;

function formata_valor(valor:string;pCasasDecimais:Integer;pArredonda:boolean):string;
{Descri��o
valor = valor a ser formatado
pCasasDecimais = quantidade de casas decimais a ser utilizada
pArredonda = se true ent�o o valor ser� arredondado, caso contrario ser� truncado em pcasasdecimais
}
var
  cont,posvirgula:Integer;
  Maskara,temp:string;
begin
{   testes
0 = 1235
00 = 1235
00000 = 01235
#.## = 1234,57
#.### = 1234,567
###,## = 1.235
###,##0.00 = 1.234,57
###,##0.000 = 1.234,567
###,## = 1.235
###.## = 1234,57
000.00 = 1234,57
000.0000 = 1234,5670
###.#### = 1234,567
#.### = 1234,567
#.## = 1234,57
 }
  result:='';
  if(Length(valor)=0) then
    Exit;
  //removendo o caracter '.' ponto
  temp := valor;
  temp := StringReplace(temp,'.',',',[rfReplaceAll]);
  try
    StrToFloat(temp);
  except
    result := 'Valor inv�lido';
    Exit;
  end;
  if not(pArredonda) then
  begin
    posvirgula := Pos(',',temp);
    if(posvirgula>0) then
    begin
      if(pCasasDecimais=0) then
        temp := FloatToStr(Trunc(StrToFloat(temp))) //s� ir� retornar inteiros
      else
      begin
        if((Length(temp) - posvirgula)>=pCasasDecimais) then
          temp := Copy(temp,1,posvirgula+pCasasDecimais)
        else
          for cont := Length(temp) - posvirgula +1 to pCasasDecimais do
            temp := temp + '0';
      end;
    end
    else
    begin
      temp := temp +',';
      for cont := 1 to pCasasDecimais do
        temp := temp + '0';
    end;
  end
  else
  begin
    Maskara:='#0.';
    for cont:=1 to pCasasDecimais do
      Maskara:=Maskara+'0';
    temp := FormatFloat(Maskara,StrToFloat(temp));
  end;
  result := temp;
end;

Function tira_ponto(valor:string):string;
var
str_valor:string;
cont:integer;
begin
     result:='';

     If (valor='')
     then exit;
     str_valor:='';
     for cont :=1 to length(valor)
     do begin
        if (valor[cont]='.') or (valor[cont]=' ')  
        then
        else str_valor:=str_valor + valor[cont]
     end;
     result:=trim(str_valor);

end;

Function formata_valor(valor:string):string;
var
decimal:Currency;
begin
    result:='';

    If (Valor='')
    Then exit;

    valor:=tira_ponto(valor);
    try
       decimal:=strtofloat(valor);
       result:=formatfloat('###,##0.00',decimal);
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
       exit;
    end;

end;


Function formata_valor_4_casas(valor:string):string;
var
decimal:Currency;
begin
    result:='';

    If (Valor='')
    Then exit;
    
    valor:=tira_ponto(valor);
    try
       decimal:=strtofloat(valor);
       result:=formatfloat('###,##0.0000',decimal);
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
       exit;
    end;

end;



Function formata_hora(valor:string):string;
Begin
     //formata a hora sem os segundos
     result:='';

     if (valor<>'')
     Then Begin
               Try
                  Strtotime(valor);
                  result:=FormatDateTime('HH:MM',Strtotime(valor));
               Except
                     exit;
               End;
     End;
End;

Function formata_horaHHMMSS(valor:string):string;
var
  temphora: String;
Begin
     //formata a hora original HHMMSS em HH:MM:SS 
     result:='';

     if (valor<>'')
     Then Begin
               Try
                  temphora := (copy(valor,1,2)+':'+copy(valor,3,2)+':'+copy(valor,5,2));
                  Strtotime(temphora);
                  result:=FormatDateTime('HH:MM:SS',Strtotime(temphora));
               Except
                     exit;
               End;
     End;
End;

Function formata_dataDDMMAAAA(valor:String):String;
var
      tempdata:string;
Begin
     //formata a data original DDMMAA para DD/MM/AAAA
     result:='';

     if (valor<>'')
     Then Begin
               Try
                  tempdata := (copy(valor,1,2)+'/'+copy(valor,3,2));
                  If(Length(valor)=6)
                  Then tempdata := tempdata +'/'+copy(valor,5,2)
                  Else tempdata := tempdata +'/'+copy(valor,5,4);
                  StrToDate(tempdata);
                  result:=FormatDateTime('DD/MM/YYYY',StrToDate(tempdata));
               Except
                     exit;
               End;
     End;
End;

Function formata_valor_com_virgula(valor:string):string;
var
decimal:Currency;
begin
{
      Este procedimento efetua a adi��o da v�rgula decimal ao valor passado
    como par�metro. Por exemplo:

    Ex1: valor veio com "1700", ent�o a sa�da seria "17,00"
    Ex2: valor veio com "1", ent�o a sa�da seria "0,01"
    Ex3: valor veio com "10" , ent�o a sa�da seria "0,10"
    Ex3: valor veio com ",7", ent�o a sa�da seria "0,70"

}
    result:='';

    If (Valor='')
    Then exit;

    try
       If(pos(',',valor)=0)
       Then begin //n�o tem v�rgula
            case Length(valor) of
                  1:result := '0,0'+valor;
                  2:Result := '0,'+valor;
            else
                  if(Length(valor)>2)
                  then Result :=  copy(valor,1,length(valor)-2)+','+copy(valor,length(valor)-2+1,2);
            end;
       end
       Else Result :=  formata_valor(valor);

{
       If(pos(',',valor)=0)
       Then decimal:=StrToCurr(copy(valor,1,length(valor)-2)+','+copy(valor,length(valor)-1,2))
       Else decimal := StrToCurr(valor);
       result:=CurrToStr(decimal);
}
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
       exit;
    end;

end;



Function formata_valor_3_casas(valor:string):string;
var
decimal:Currency;
begin
    result:='';

    If (Valor='')
    Then exit;

    valor:=tira_ponto(valor);
    try
       decimal:=strtofloat(valor);
       result:=formatfloat('###,##0.000',decimal);
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
       exit;
    end;

end;

Function formata_valor_tempo_real_2_casas(pTexto: String;sender:Tobject):String;
var temp,auxiliar : String;
begin
      If(TEdit(sender).Text='')
      Then Begin
            Result :='';
            Exit;
      End;
      auxiliar := tira_virgula(pTexto);
      auxiliar := tira_ponto(auxiliar);
      If(length(auxiliar)>18)
      Then Begin
            Result := copy(auxiliar, 1, length(auxiliar)-3)+','+copy(auxiliar, length(auxiliar)-2,2);
            Tedit(sender).SelStart := length(Tedit(sender).text);
            exit;
      End;
      auxiliar := IntToStr(StrToInt64(auxiliar));
      If(Length(auxiliar)>2)
      Then Begin
            temp := copy(auxiliar, 1, length(auxiliar)-2)+','+copy(auxiliar, length(auxiliar)-1,2);
            auxiliar := temp;
      End
      Else Begin
            If(Length(auxiliar)=1)
            Then auxiliar := '0,0' + auxiliar
            Else auxiliar := '0,' + auxiliar;
      End;
      Tedit(sender).SelStart := length(Tedit(sender).text);
      result := auxiliar;
end;

Function formata_valor_tempo_real_N_casas(pTexto: String;sender:Tobject;pCasasDecimais:integer):String;
var
  temp,auxiliar : String;
  i:Integer;
begin
      If(TEdit(sender).Text='')
      Then Begin
            Result :='';
            Exit;
      End;
      auxiliar := tira_virgula(pTexto);
      auxiliar := tira_ponto(auxiliar);
      If(length(auxiliar)>18)
      Then Begin
            Result := copy(auxiliar, 1, length(auxiliar)-pCasasDecimais+1)+','+copy(auxiliar, length(auxiliar)-pCasasDecimais,pCasasDecimais);
            Tedit(sender).SelStart := length(Tedit(sender).text);
            exit;
      End;
      auxiliar := IntToStr(StrToInt64(auxiliar));
      Temp:='';
      If(Length(auxiliar)>pCasasDecimais)
      Then Begin
            temp := copy(auxiliar, 1, length(auxiliar)-pCasasDecimais)+','+copy(auxiliar, length(auxiliar)-pCasasDecimais+1,pCasasDecimais);
            auxiliar := temp;
      End
      Else Begin
            auxiliar := CompletaPalavra_a_Esquerda(auxiliar,pCasasDecimais,'0');
            auxiliar := '0,'+auxiliar;
      End;
      Tedit(sender).SelStart := length(Tedit(sender).text);
      result := auxiliar;
end;

Function PegaDataExtenso(Data:Tdate):String;
var
dia,mes,ano:word;
volta:string;
Begin
     DecodeDate(data,ano,mes,dia);
     volta:='';
     volta:=inttostr(dia)+ ' de '+MesExtenso(mes)+' de '+inttostr(ano);
     result:=volta;
End;

Function GeraZero(Quant:Byte):string;
var
Cont:integer;
resulta:string;
Begin
     resulta:='';
     for cont:=1 to quant do
     resulta:=resulta+'0';

     result:=resulta;
End;


Function Get_PathFotos:String;
var
   pathtemp:string;

Begin

     pathtemp:='';
     pathtemp:=ExtractFilePath(Application.exename);
     If (Pathtemp[length(pathtemp)]<>'\')
     Then pathtemp:=pathtemp+'\';
     PathTemp:=PathTemp+'Fotos\';

     if (DirectoryExists(PathTemp)=False)
     Then Begin
               If (createdir(PathTemp)=False)
               Then Begin
                         Messagedlg('Erro na Tentativa de Cria��o de Diret�rio!',mterror,[mbok],0);
                         PathTemp:='';
                    End;
          End;
     result:=PathTemp;

end;


Function RetornaPastaSistema:String;
var
   pathtemp:string;
Begin

     pathtemp:='';
     pathtemp:=ExtractFilePath(Application.exename);

     If (Pathtemp[length(pathtemp)]<>'\')
     Then pathtemp:=pathtemp+'\';

      result:=PathTemp;

end;

Function RetornaNomeEXE:string;
begin
  result := ExtractFileName(Application.ExeName);
end;


procedure DividirValor(apoio: string;Quant:INteger;out Saida1,Saida2:String);
var
cont1,cont2:integer;
primeira,segunda:string;
begin
     primeira:='';
     segunda:='';
     If (Length(apoio)<=quant)
     Then Begin
               saida1:=apoio;
               saida2:='';
               exit;
     End;
     primeira:=copy(apoio,1,quant);


     for cont1:=length(primeira) downto 1 do
     Begin
          if (primeira[cont1]=' ')
          Then Begin
                    primeira:=copy(apoio,1,cont1-1);
                    segunda:=copy(apoio,cont1,length(apoio));
                    break;
               End;
     End;

     //esta parte nao serve para outras coisas
     If (length(segunda)>60)
     Then saida2:=copy(segunda,1,60)
     else saida2:=segunda;

     saida1:=primeira;

end;

procedure DividirValor(apoio: string;Quant1,Quant2:INteger;out Saida1,Saida2:String);
var
cont1,cont2:integer;
primeira,segunda:string;
begin
     primeira:='';
     segunda:='';
     If (Length(apoio)<=quant1)
     Then Begin
               saida1:=apoio;
               saida2:='';
               exit;
          End;
     primeira:=copy(apoio,1,quant1);


     for cont1:=length(primeira) downto 1 do
     Begin
          if (primeira[cont1]=' ')
          Then Begin
                    primeira:=copy(apoio,1,cont1-1);
                    segunda:=copy(apoio,cont1,length(apoio));
                    break;
               End;
     End;

     //esta parte nao serve para outras coisas
     If (length(segunda)>quant2)
     Then saida2:=copy(segunda,1,quant2)
     else saida2:=segunda;

     saida1:=primeira;

end;


procedure DividirValorCOMSEGUNDASEMTAMANHO(apoio: string;Quant1:INteger;out Saida1,Saida2:String);
var
cont1,cont2:integer;
primeira,segunda:string;
begin
     primeira:='';
     segunda:='';
     If (Length(apoio)<quant1)
     Then Begin
               saida1:=apoio;
               saida2:='';
               exit;
     End;
     primeira:=copy(apoio,1,quant1);
     segunda:=copy(apoio,quant1,length(apoio)-quant1+1);

     for cont1:=length(primeira) downto 1 do
     Begin
          if (primeira[cont1]=' ')
         Then Begin
                   primeira:=copy(apoio,1,cont1-1);
                   segunda:=copy(apoio,cont1,length(apoio));
                   break;
         End;
     End;

     if (Primeira='') and (Segunda<>'')
     Then Begin
               Primeira:=Segunda;
               Segunda:='';
     End;

     saida2:=segunda;
     saida1:=primeira;
end;

Procedure DividirValorEmStringlist(apoio:String; Quant:integer; var Pstringlist:TStringList);
var cont,linha,inicio,fim:integer;
    frase:string;
    ultimo,proximo:Char;
begin
      linha:=(Length(apoio)div Quant);
      If(Length(apoio)mod Quant <> 0)
      Then inc(linha,1);
      inicio:=1;
      For cont:=0 to linha -1 do
      Begin
            //ultimo :=Copy(apoio,inicio+Quant,1);
            ultimo := apoio[inicio+Quant];
            if(ultimo in ['0'..'9', 'a'..'z', 'A'..'Z'] ) then
            begin
              //proximo := Copy(apoio,inicio+Quant+1,1);
              proximo := apoio[inicio+Quant+1];
              while(proximo in ['0'..'9', 'a'..'z', 'A'..'Z'] ) do
              begin
                inc(Quant,-1);
                //proximo := Copy(apoio,inicio+Quant,1);
                proximo := apoio[inicio+Quant];
              end;
              Inc(Quant);
            end;
            Frase:=copy(apoio,inicio,Quant);
            Pstringlist.Add(frase);
            inicio:=inicio+Quant;
      End;
end;

procedure DividirValorSemQuebra(apoio: string;Quant:INteger;out Saida1,Saida2:String);
var
cont1,cont2:integer;
primeira,segunda:string;
begin
     primeira:='';
     segunda:='';
     If (Length(apoio)<=quant)
     Then Begin
               saida1:=apoio;
               saida2:='';
               exit;
     End;
     primeira:=copy(apoio,1,quant-1);
     primeira:=primeira+'-';
     segunda:=copy(apoio,quant,length(apoio));

     saida1:=primeira;
     saida2:=segunda;
end;


procedure PegaFiguraBotao(Parametro:TBitBtn;Nome:String);overload;
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';
     Path:=Path+'Images\';

     If Parametro<>Nil
     Then If (Fileexists(path+nome))
          Then parametro.Glyph.LoadFromFile(path+nome);
end;
procedure PegaFiguraBotao(Parametro:Tspeedbutton;Nome:String);overload;
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';
     Path:=Path+'Images\';

     If Parametro<>Nil
     Then If (Fileexists(path+nome))
          Then parametro.Glyph.LoadFromFile(path+nome);
end;


procedure PegaFiguraBotoes(Botaonovo,BotaoAlterar,BotaoCancelar,BotaoGravar,BotaoPesquisar,BotaoRelatorios,BotaoExcluir,BotaoSair:TBitBtn);
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';

     Path:=Path+'Images\';

     If BotaoNovo<>Nil
     Then If (FileExists(path+'Botaonovo.bmp'))
          Then Botaonovo.Glyph.LoadFromFile(path+'Botaonovo.bmp');

     If BotaoAlterar<>Nil
     Then If (FileExists(path+'BotaoAlterar.bmp'))
          Then BotaoAlterar.Glyph.LoadFromFile(path+'BotaoAlterar.bmp');

     If Botaocancelar<>Nil
     Then If (Fileexists(path+'BotaoCancelar.bmp'))
        Then BotaoCancelar.Glyph.LoadFromFile(path+'BotaoCancelar.bmp');

     If botaogravar<>nil
     Then If (Fileexists(path+'BotaoGravar.bmp'))
          Then BotaoGravar.Glyph.LoadFromFile(path+'BotaoGravar.bmp');

     If botaopesquisar<>nil
     Then If (Fileexists(path+'BotaoPesquisar.bmp'))
          Then BotaoPesquisar.Glyph.LoadFromFile(path+'BotaoPesquisar.bmp');

     If botaorelatorios<>nil
     Then If (fileexists(path+'BotaoRelatorios.bmp'))
          Then BotaoRelatorios.Glyph.LoadFromFile(path+'BotaoRelatorios.bmp');

     if botaoexcluir<>nil
     Then If (Fileexists(path+'BotaoExcluir.bmp'))
          Then BotaoExcluir.Glyph.LoadFromFile(path+'BotaoExcluir.bmp');

     If botaosair<>nil
     Then If (Fileexists(path+'BotaoSair.bmp'))
          Then BotaoSair.Glyph.LoadFromFile(path+'BotaoSair.bmp');

end;

procedure PegaFiguraBotoesPEquenos(Botaonovo,BotaoAlterar,BotaoCancelar,BotaoGravar,BotaoPesquisar,BotaoRelatorios,BotaoExcluir,BotaoSair:TBitBtn);
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';

     Path:=Path+'Images\Pequenos\';

     If BotaoNovo<>Nil
     Then If (FileExists(path+'Botaonovo.bmp'))
          Then Botaonovo.Glyph.LoadFromFile(path+'Botaonovo.bmp');

     If BotaoAlterar<>Nil
     Then If (FileExists(path+'BotaoAlterar.bmp'))
          Then BotaoAlterar.Glyph.LoadFromFile(path+'BotaoAlterar.bmp');

     If Botaocancelar<>Nil
     Then If (Fileexists(path+'BotaoCancelar.bmp'))
        Then BotaoCancelar.Glyph.LoadFromFile(path+'BotaoCancelar.bmp');

     If botaogravar<>nil
     Then If (Fileexists(path+'BotaoGravar.bmp'))
          Then BotaoGravar.Glyph.LoadFromFile(path+'BotaoGravar.bmp');

     If botaopesquisar<>nil
     Then If (Fileexists(path+'BotaoPesquisar.bmp'))
          Then BotaoPesquisar.Glyph.LoadFromFile(path+'BotaoPesquisar.bmp');

     If botaorelatorios<>nil
     Then If (fileexists(path+'BotaoRelatorios.bmp'))
          Then BotaoRelatorios.Glyph.LoadFromFile(path+'BotaoRelatorios.bmp');

     if botaoexcluir<>nil
     Then If (Fileexists(path+'BotaoExcluir.bmp'))
          Then BotaoExcluir.Glyph.LoadFromFile(path+'BotaoExcluir.bmp');

     If botaosair<>nil
     Then If (Fileexists(path+'BotaoSair.bmp'))
          Then BotaoSair.Glyph.LoadFromFile(path+'BotaoSair.bmp');

end;

procedure PegaFiguraBotoes(Botaonovo,BotaoAlterar,BotaoCancelar,BotaoGravar,BotaoPesquisar,BotaoRelatorios,BotaoExcluir,BotaoSair:TSpeedButton);
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';

     Path:=Path+'Images\';

     If BotaoNovo<>Nil
     Then If (FileExists(path+'Botaonovo.bmp'))
          Then Botaonovo.Glyph.LoadFromFile(path+'Botaonovo.bmp');

     If BotaoAlterar<>Nil
     Then If (FileExists(path+'BotaoAlterar.bmp'))
          Then BotaoAlterar.Glyph.LoadFromFile(path+'BotaoAlterar.bmp');

     If Botaocancelar<>Nil
     Then If (Fileexists(path+'BotaoCancelar.bmp'))
        Then BotaoCancelar.Glyph.LoadFromFile(path+'BotaoCancelar.bmp');

     If botaogravar<>nil
     Then If (Fileexists(path+'BotaoGravar.bmp'))
          Then BotaoGravar.Glyph.LoadFromFile(path+'BotaoGravar.bmp');

     If botaopesquisar<>nil
     Then If (Fileexists(path+'BotaoPesquisar.bmp'))
          Then BotaoPesquisar.Glyph.LoadFromFile(path+'BotaoPesquisar.bmp');

     If botaorelatorios<>nil
     Then If (fileexists(path+'BotaoRelatorios.bmp'))
          Then BotaoRelatorios.Glyph.LoadFromFile(path+'BotaoRelatorios.bmp');

     if botaoexcluir<>nil
     Then If (Fileexists(path+'BotaoExcluir.bmp'))
          Then BotaoExcluir.Glyph.LoadFromFile(path+'BotaoExcluir.bmp');

     If botaosair<>nil
     Then If (Fileexists(path+'BotaoSair.bmp'))
          Then BotaoSair.Glyph.LoadFromFile(path+'BotaoSair.bmp');
end;

procedure PegaFigura(Imagem:Timage;NomeArq:String);
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';

     Path:=Path+'Images\'+NomeArq;
     IF (FileExists(path)=True)
     Then Imagem.Picture.LoadFromFile(path)
     Else Imagem.Picture:=nil;

end;

procedure PegaFigura(Imagem:TPPimage;NomeArq:String);overload;
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';

     Path:=Path+'Images\'+NomeArq;
     IF (FileExists(path)=True)
     Then Imagem.Picture.LoadFromFile(path)
     Else Imagem.Picture:=nil;

end;

procedure PegaFigura(Imagem:TQrimage;NomeArq:String);
var
Path:String;
begin
     Path:='';
     Path:=ExtractFilePath(Application.exename);
     If (Path[length(PATH)]<>'\')
     Then Path:=Path+'\';

     Path:=Path+'Images\'+NomeArq;
     IF (FileExists(path)=True)
     Then Imagem.Picture.LoadFromFile(path)
     Else Imagem.Picture:=nil;

end;

Function VerificaBaseRemota(PathBase:String):Boolean;
var
conta,cont:integer;
Begin

     conta:=0;
     for cont:=1 to length(PathBase) do
     Begin
          If (PathBase[cont]=':')
          Then inc(conta,1);
     End;

     If (conta=2)
     Then result:=true
     Else Begin
                //soh tem um :
                //mas pode ser Linux
                //exemplo:
                //10.1.1.200:/exclaim/base/amanda.gdb ->tupy
                //procuro pela barra invertida se encontrar � linux e a distancia
                if (pos('/',PathBase)<>0)
                Then result:=true
                Else result:=False;
     End;

End;

procedure AjustaLArguraColunaGrid(PGrid: TStringGrid);
Begin
     AjustaLArguraColunaGrid(PGrid,'Courier New',8);
end;

procedure AjustaLArguraColunaGrid(PGrid: TStringGrid;PFonte:TFontName;Ptamanhofonte:integer);
var
Contcol,contlinha,maiortamanho:Integer;
begin
     //Ajusta as Colunas do StringGrid de acordo com a
     //quantidade de letras, foi usado Courier New tamanho 08 como padrao
     Pgrid.Ctl3D:=False;
     Pgrid.Font.Name:=Pfonte;
     Pgrid.Font.Size:=Ptamanhofonte;

     for contcol:=pgrid.ColCount-1 downto 0 do
     Begin
          //para cada coluna verifico o tamanho da palavra que esta dentro dela
          //se for maior a que eu tinha guardado eu aumento o tamanho da mesma
          maiortamanho:=0;
          for contlinha:=PGrid.RowCount-1 downto 0 do
          Begin
               If (length(Pgrid.Cells[contcol,contlinha])>maiortamanho)
               Then Begin
                         maiortamanho:=length(Pgrid.Cells[contcol,contlinha]);
                         Pgrid.ColWidths[contcol]:=maiortamanho*Ptamanhofonte;//a quantidade de caracteres * 8 pontos que � a fonte
                    End;
          End;

     End;
end;



procedure Formatadbgrid_MySql(grid:Tdbgrid);
var
i:integer;
tamanhomaximocoluna:integer;
begin

     tamanhomaximocoluna:=500;

     if(grid.Columns.Count-1<=0)
     then exit;

     grid.SelectedIndex:=0;
     //for i :=0 to grid.DataSource.DataSet.FieldCount-1

     for i :=0 to grid.FieldCount-1
     do begin
         if grid.DataSource.DataSet.Fields[i].datatype = ftbcd
         then TFloatField(grid.DataSource.DataSet.fields[i]).DisplayFormat:= '###,###,##0.00'
         Else Begin
                   if grid.DataSource.DataSet.Fields[i].datatype = ftstring
                   Then Begin
                             If (grid.DataSource.DataSet.Fields[i].DataSize>50)
                             Then grid.Columns[i].width:=tamanhomaximocoluna;
                   End;
         End;

     end;
end;




procedure Formatadbgrid(grid:Tdbgrid);
var
i:integer;
tamanhomaximocoluna:integer;
begin

     If (ObjParametroGlobal.ValidaParametro('TAMANHO MAXIMO PARA COLUNA')=False)
     Then tamanhomaximocoluna:=500
     Else Begin

               try
                  tamanhomaximocoluna:=strtoint(ObjParametroGlobal.get_valor);
               except
                     tamanhomaximocoluna:=500;
               end;
     End;



     grid.SelectedIndex:=0;
     (*for i :=0 to grid.DataSource.DataSet.FieldCount-1
     do begin
         if grid.DataSource.DataSet.Fields[i].datatype = ftbcd
         then TFloatField(grid.DataSource.DataSet.fields[i]).DisplayFormat:= '###,###,##0.00'
         Else Begin
                   if grid.DataSource.DataSet.Fields[i].datatype = ftstring
                   Then Begin
                             If (grid.DataSource.DataSet.Fields[i].DataSize>50)
                             Then grid.Columns[i].width:=tamanhomaximocoluna;
                   End;
         End;

     end;*)

     //era for i :=0 to grid.DataSource.DataSet.FieldCount-1 celio 10/02/10
     if(grid.Columns.Count-1<=0)
     then exit;

     for i :=0 to grid.Columns.Count-1
     do begin
         if grid.DataSource.DataSet.Fields[i].datatype = ftbcd
         then TFloatField(grid.DataSource.DataSet.fields[i]).DisplayFormat:= '###,###,##0.00'
         Else Begin
                   if grid.DataSource.DataSet.Fields[i].datatype = ftstring
                   Then Begin
                             If (grid.DataSource.DataSet.Fields[i].DataSize>50)
                             Then grid.Columns[i].width:=tamanhomaximocoluna;
                   End;
         End;

     end;


end;


procedure Formatadbgrid_3_casas(grid:Tdbgrid);
var
i:integer;
tamanhomaximocoluna:integer;
begin

     If (ObjParametroGlobal.ValidaParametro('TAMANHO MAXIMO PARA COLUNA')=False)
     Then tamanhomaximocoluna:=500
     Else Begin

               try
                  tamanhomaximocoluna:=strtoint(ObjParametroGlobal.get_valor);
               except
                     tamanhomaximocoluna:=500;
               end;
     End;



     grid.SelectedIndex:=0;
     if(grid.Columns.Count-1<=0)
     then exit;
     //era for i :=0 to grid.DataSource.DataSet.FieldCount-1 celio 10/02/10
     for i :=0 to grid.Columns.Count-1
     do begin
         if grid.DataSource.DataSet.Fields[i].datatype = ftbcd
         then TFloatField(grid.DataSource.DataSet.fields[i]).DisplayFormat:= '###,###,##0.000'
         Else Begin
                   if grid.DataSource.DataSet.Fields[i].datatype = ftstring
                   Then Begin
                             If (grid.DataSource.DataSet.Fields[i].DataSize>50)
                             Then grid.Columns[i].width:=tamanhomaximocoluna;
                   End;
         End;

     end;
end;


procedure Formatadbgrid_5_casas(grid:Tdbgrid);
var
i:integer;
tamanhomaximocoluna:integer;
begin

     If (ObjParametroGlobal.ValidaParametro('TAMANHO MAXIMO PARA COLUNA')=False)
     Then tamanhomaximocoluna:=500
     Else Begin

               try
                  tamanhomaximocoluna:=strtoint(ObjParametroGlobal.get_valor);
               except
                     tamanhomaximocoluna:=500;
               end;
     End;



     grid.SelectedIndex:=0;
     if(grid.Columns.Count-1<=0)
     then exit;
     //era for i :=0 to grid.DataSource.DataSet.FieldCount-1 celio 10/02/10
     for i :=0 to grid.Columns.Count-1
     do begin
         if grid.DataSource.DataSet.Fields[i].datatype = ftbcd
         then TFloatField(grid.DataSource.DataSet.fields[i]).DisplayFormat:= '###,###,##0.00000'
         Else Begin
                   if grid.DataSource.DataSet.Fields[i].datatype = ftstring
                   Then Begin
                             If (grid.DataSource.DataSet.Fields[i].DataSize>50)
                             Then grid.Columns[i].width:=tamanhomaximocoluna;
                   End;
         End;

     end;
end;


procedure Formatadbgrid(grid:Tdbgrid;PnumeroCasas:integer);
var
i:integer;
tamanhomaximocoluna:integer;
vcomplemento:string;
begin
     vcomplemento:='';
     for i:= 1 to pnumerocasas do
     Begin
          vcomplemento:=vcomplemento+'0';
     End;

     If (ObjParametroGlobal.ValidaParametro('TAMANHO MAXIMO PARA COLUNA')=False)
     Then tamanhomaximocoluna:=500
     Else Begin
               
               try
                  tamanhomaximocoluna:=strtoint(ObjParametroGlobal.get_valor);
               except
                     tamanhomaximocoluna:=500;
               end;
     End;



     grid.SelectedIndex:=0;
     if(grid.Columns.Count-1<=0)
     then exit;
     //era for i :=0 to grid.DataSource.DataSet.FieldCount-1 celio 10/02/10
     //era for i :=0 to grid.DataSource.DataSet.FieldCount-1 celio 10/02/10
     for i :=0 to grid.Columns.Count-1
     do begin
         if grid.DataSource.DataSet.Fields[i].datatype = ftbcd
         then TFloatField(grid.DataSource.DataSet.fields[i]).DisplayFormat:= '###,###,##0.'+vcomplemento
         Else Begin
                   if grid.DataSource.DataSet.Fields[i].datatype = ftstring
                   Then Begin
                             If (grid.DataSource.DataSet.Fields[i].DataSize>50)
                             Then grid.Columns[i].width:=tamanhomaximocoluna;
                   End;
         End;

     end;
end;

procedure formatadbgrid(grid:Tdbgrid;QUERY:TIBquery);
var
i:integer;
tamanhomaximocoluna:integer;
begin

     If (ObjParametroGlobal.ValidaParametro('TAMANHO MAXIMO PARA COLUNA')=False)
     Then tamanhomaximocoluna:=500
     Else Begin

               try
                  tamanhomaximocoluna:=strtoint(ObjParametroGlobal.get_valor);
               except
                     tamanhomaximocoluna:=500;
               end;
     End;

     if(grid.Columns.Count-1<=0)
     then exit;

     //era for i :=0 to grid.FieldCount-1

     for i :=0 to grid.Columns.Count-1
     do begin
           //if grid.SelectedField.datatype = ftbcd
           if (grid.DataSource.DataSet.Fields[i].datatype=ftbcd)
           then TFloatField(QUERY.FieldByName(query.FieldDefs.Items[i].Name)).DisplayFormat:= '###,###,##0.00'
           Else Begin
                     if (grid.DataSource.DataSet.Fields[i].datatype = ftstring)
                     Then Begin
                               If (grid.DataSource.DataSet.Fields[i].DataSize>50)
                               Then grid.Columns[i].width:=tamanhomaximocoluna;
                     End;
           End;
     end;

end;

function PegaPathBanco(nomechave:String): String;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'Path.Ini');
     Except
           Messagedlg('Erro na  Abertura do Arquivo Path.ini',mterror,[mbok],0);                
           Result:='';
           exit;
     End;

     Try
        Try
            Temp:='';
            if (nomechave='')
            Then Temp:=arquivo_ini.ReadString('SISTEMA','PATH','')
            Else Temp:=arquivo_ini.ReadString('SISTEMA',uppercase(nomechave),'')
        Except
              Messagedlg('Erro na Leitura da chave PATH do SISTEMA!',mterror,[mbok],0);
        End;
     finally
         freeandnil(arquivo_ini);
     End;

     Result:=Temp;
end;

function PegaPathBancoDAV(nomechave:String;grupo:String): String;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'Path.Ini');
     Except
           Messagedlg('Erro na  Abertura do Arquivo Path.ini',mterror,[mbok],0);
           Result:='';
           exit;
     End;

     Try
        Try
            Temp:='';
            if (nomechave='')
            Then Temp:=arquivo_ini.ReadString('SISTEMA','PATH','')
            Else If(grupo='')
                  Then Temp:=arquivo_ini.ReadString('SISTEMA',uppercase(nomechave),'')
                  Else Temp:=arquivo_ini.ReadString(uppercase(grupo),uppercase(nomechave),'');
        Except
              Messagedlg('Erro na Leitura da chave PATH do SISTEMA!',mterror,[mbok],0);
        End;
     finally
         freeandnil(arquivo_ini);
     End;

     Result:=Temp;
end;

function LeChaveIni(nomearquivo,secao,chave:String;var valor:String): Boolean;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     result:=False;
     valor:='';

     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+nomearquivo);
     Except
           Messagedlg('Erro na Abertura do Arquivo '+nomearquivo,mterror,[mbok],0);
           exit;
     End;

     Try
        Try
            valor:='';
            Valor:=arquivo_ini.ReadString(uppercase(secao),uppercase(chave),'');
            result:=true;
        Except
              Messagedlg('Erro na Leitura da se��o '+secao+' chave '+chave+' do arquivo '+nomearquivo,mterror,[mbok],0);
        End;
     finally
         freeandnil(arquivo_ini);
     End;
end;

function GravaChaveIni(nomearquivo,secao,chave,valor:String): Boolean;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     result:=False;
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+nomearquivo);
     Except
           Messagedlg('Erro na Abertura do Arquivo '+nomearquivo,mterror,[mbok],0);
           exit;
     End;

     Try
        Try
            arquivo_ini.WriteString(uppercase(secao),UPPERCASE(chave),valor);
            result:=true;
        Except
              Messagedlg('Erro na tentativa de Grava��o da se��o '+secao+' chave '+chave+' valor '+valor+' do arquivo '+nomearquivo,mterror,[mbok],0);
        End;
     finally
         freeandnil(arquivo_ini);
     End;
end;

function LeChaveIni(nomearquivo,secao,chave:String;var valor:String;silencioso:boolean): Boolean;
var
Temp:string;
arquivo_ini:Tinifile;
begin
  result:=False;
  valor:='';

  Temp:=ExtractFilePath(Application.ExeName);

  if (temp[length(temp)]<>'\') then
    temp:=temp+'\';

  try
    Arquivo_ini:=Tinifile.Create(Temp+nomearquivo);
  except
    if not silencioso then
      Messagedlg('Erro na Abertura do Arquivo '+nomearquivo,mterror,[mbok],0);
    exit;
  end;

  try
    try
      valor:='';
      Valor:=arquivo_ini.ReadString(uppercase(secao),uppercase(chave),'');
      result:=true;
    except
      if not silencioso then
        Messagedlg('Erro na Leitura da se��o '+secao+' chave '+chave+' do arquivo '+nomearquivo,mterror,[mbok],0);
    end;
  finally
    freeandnil(arquivo_ini);
  end;
end;

function GravaChaveIni(nomearquivo,secao,chave,valor:String;silencioso:boolean): Boolean;
var
Temp:string;
arquivo_ini:Tinifile;
begin
  result:=False;
  Temp:=ExtractFilePath(Application.ExeName);

  if (temp[length(temp)]<>'\') then
    temp:=temp+'\';

  if not(FileExists(Temp+nomearquivo)) then
  begin
    if not silencioso then
      MensagemErro('Arquivo '+Temp+nomearquivo+' n�o localizado');
    Exit;
  end;

  try
    Arquivo_ini:=Tinifile.Create(Temp+nomearquivo);
  except
    if not silencioso then
      MensagemErro('Erro na Abertura do Arquivo '+nomearquivo);
    exit;
  end;

  try
    try
      arquivo_ini.WriteString(uppercase(secao),UPPERCASE(chave),valor);
      result:=true;
    except
      if not silencioso then
        MensagemErro('Erro na tentativa de Grava��o da se��o '+secao+' chave '+chave+' valor '+valor+' do arquivo '+nomearquivo);
    end;
  finally
    freeandnil(arquivo_ini);
  end;
end;



function PegaPathBanco: String;
var
Temp:string;
arquivo_ini:Tinifile;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'Path.Ini');
     Except
           Messagedlg('Erro na Abertura do Arquivo Path.ini',mterror,[mbok],0);
           Result:='';
           exit;
     End;

     Try
        Try
            Temp:='';
            Temp:=arquivo_ini.ReadString('SISTEMA','PATH','');
        Except
              Messagedlg('Erro na Leitura da chave PATH do SISTEMA!',mterror,[mbok],0);
        End;
     finally
         freeandnil(arquivo_ini);
     End;
     
     Result:=Temp;
end;




Function preenche(palavra:string;quantidade:Integer;ValorASerUSado:string):String;
Begin
     result:=Palavra;
     If (length(palavra)>=quantidade)
     Then exit;

     While (length(palavra)<quantidade) do
     palavra:=palavra+Valoraserusado;

     result:=Palavra;
End;

Function LimitaPalavra(palavra:string;quantidade:Integer):String;
Begin
     result:='';

     if (length(palavra)>quantidade)
     then result:=copy(palavra,1,quantidade)
     Else result:=palavra;
End;

Function CompletaPalavra(palavra:string;quantidade:Integer;ValorASerUSado:string):String;
var
  apoio:String;
Begin

  result:='';

  apoio:='';

  try

    If (length(palavra)>=quantidade) Then
    Begin
                  If (QUANTIDADE=0)
                  Then EXIT;

                  apoio:=copy(palavra,1,quantidade);
                  result:=apoio;
                  exit;
    End;

    apoio:=Palavra;

    if (quantidade=0)//em casos de configuracao de rel de NF isso � necessario
    Then Apoio:='';

    While (length(apoio)<quantidade) do
    Begin
      apoio:=apoio+Valoraserusado;
    End;

  except

    on e:Exception do
    begin
      ShowMessage('Erro na fun��o CompletaPalavra: '+e.Message);
      raise;
      Exit;
    end;

  end;

 result:=apoio;

End;

Function CompletaPalavra_a_Esquerda(palavra:string;quantidade:Integer;ValorASerUSado:string):String;
var
apoio:String;
Begin
     result:='';
     apoio:='';
     If (length(palavra)>=quantidade)
     Then Begin
                apoio:=copy(palavra,1,quantidade);
                result:=apoio;
                exit;
     End;

     apoio:='';

     if (quantidade=0)//em casos de configuracao de rel de NF isso � necessario
     Then Apoio:='';

     While ((length(apoio)+length(palavra))<quantidade)
     do Begin
             apoio:=apoio+Valoraserusado;
     End;

     result:=apoio+palavra;
End;


Procedure ValorExtenso_DivididoDois(Valor:Currency;out Extenso1:String;out Extenso2:String);
var
apoio:string;
cont:integer;
valor1,valor2:string;
Begin
        apoio:='';
        extenso1:=''; 
        extenso2:='';
        if (Valor>0)
        Then Begin
                apoio:=valorextenso(valor);
                IF (apoio[1]=' ') and (apoio[2]='e')
                then delete(apoio,1,3);

                IF LENGTH(APOIO)>50
                Then Begin
                          valor1:='';
                          valor2:='';
                          DividirValor(apoio,50,valor1,valor2);
                          Extenso1:=valor1;
                          Extenso2:=valor2;
                          exit;
                End;
                extenso1:=apoio;
                exit;
             End;
        extenso1:='';
        extenso2:='';
end;



function Extenso(Valor: String):String;
const
  //o posi��o zero � a casa de unidades e, nesta n�o h� sufixo
  Nome_Casa: Array [0..6] of String = ('','Mil','Milh�o','Bilh�o','Trilh�o','Quadrilh�o','Quintilh�o');
  Nome_Casa_Plural: Array [0..6] of String = ('','Mil','Milh�es','Bilh�es','Trilh�es','Quadrilh�es','Quintilh�es');
  decimal: array [1..9] of String = ('Onze','Doze','Treze','Quatorze','Quinze','Dezesseis','Dezessete','Dezoito','Dezenove');
var
  FileNames: TStringList;
  Casas: Array [1..3,0..9] of String;

  casa, valor_casa, aux: String;
  n: integer;
  i, j, ncasa, tam: integer;
begin
  result:= '';

  if trim(valor) = '' then
     exit;
  casas[1,0]:= '';
  casas[1,1]:= 'Um';
  casas[1,2]:= 'Dois';
  casas[1,3]:= 'Tr�s';
  casas[1,4]:= 'Quatro';
  casas[1,5]:= 'Cinco';
  casas[1,6]:= 'Seis';
  casas[1,7]:= 'Sete';
  casas[1,8]:= 'Oito';
  casas[1,9]:= 'Nove';

  casas[2,0]:= '';
  casas[2,1]:= 'Dez';
  casas[2,2]:= 'Vinte';
  casas[2,3]:= 'Trinta';
  casas[2,4]:= 'Quarenta';
  casas[2,5]:= 'Cinquenta';
  casas[2,6]:= 'Sessenta';
  casas[2,7]:= 'Setenta';
  casas[2,8]:= 'Oitenta';
  casas[2,9]:= 'Noventa';

  casas[3,0]:= '';
  casas[3,1]:= 'Cem';
  casas[3,2]:= 'Duzentos';
  casas[3,3]:= 'Trezentos';
  casas[3,4]:= 'Quatrocentos';
  casas[3,5]:= 'Quinhentos';
  casas[3,6]:= 'Seiscentos';
  casas[3,7]:= 'Setecentos';
  casas[3,8]:= 'Oitocentos';
  casas[3,9]:= 'Novecentos';
  try
    valor_casa:= '';
    tam:= length(valor);
    j:= tam + 1;
    i:= 0;

    repeat
        inc(i);
        dec(j);
        if j mod 3 = 0 then
           ncasa:= 3
        else ncasa:= j mod 3;
        {a vari�vel valor_casa � o valor num�rico a cada separador de milhar (.)
         Exemplo 51.439  - valor da casa de milhar � 51 e o valor da casa de centana � 439}

        {a vari�vel ncasa � a posi��o do algarismo dentro da casa.
         Exemplo:
            valor da vari�vel ncasa para cada algarismo do n�  1 321
            n�mero a ser escrito por extenso                   7.428}

        valor_casa:= valor_casa + valor[i];
        n:= StrToInt(valor[i]);
        casa:= '';
        casa:= casas[ncasa, n];
        if AnsiSameText(casa,'cem') and ((valor[i+1] <> '0') or (valor[i+2] <> '0')) then
           casa:= 'Cento';

        {verificar se o valor est� entre 11 e 29, pois o formato deste valores foge do padr�o}
        {o valor s� � verificado se a vari�vel ncasa for igual a 2 (indicando que � casa de decimais)}
        if (ncasa = 2) and ((valor[i]= '1') and (valor[i+1] > '0')) then
        begin
          {se o valor da casa decimal for, por exemplo, igual a 15, o 2�
           d�gito vai representar a posi��o (5) no vetor de nome decimal
           que cont�m o nome por extenso do valor 15}
          casa:= decimal[StrToInt(valor[i+1])];
          {quando encontra-se um valor decimal entre 11 e 19, s�o processados
          dois algarismos do n�mero de uma �nica vez, ou seja, s�o
          escritos 2 algarismos por extenso de uma vez s�, assim,
          deve-se pular para a pr�xima posi��o do n�mero incrementando o I
          e decrementando o J.}
          inc(i);
          valor_casa:= valor_casa + valor[i];
          dec(j);
          ncasa:= 1;
        end;

        if (result <> '') and (casa <> '') then
        begin
           if (ncasa <> 3) or ((ncasa = 3) and (j = 3)) then
           begin
             result:= result + ' e ' + casa;
           end
           else  result:= result + ' ' + casa;
        end
        else if result = '' then
           result:= casa;



        {descobrir o nome da casa (se � de mil, milh�o, bilh�o, etc)}
        if (ncasa = 1) then
        begin
           if StrToInt(Valor_Casa) = 1 then
              aux:= trim(nome_casa[(j-1) div 3])
           else aux:= trim(nome_casa_plural[(j-1) div 3]);

           result:= result + ' ' + aux;
           valor_casa:= '';
        end;
        result:= trim(result);

        {para descobrir o nome da casa (se � mil, milh�o, bilh�o)
         usou-se a f�rmula do termo geral da PA (Progress�o Aritm�tica) que �
         Ai = A1 + (i * r) assim i = (Ai - A1)/r

         se o algarismo estiver na posi��o 4 da string ele estar� na
         casa de milhar (posi��o 1 do vetor nome_casa)

         se o algarismo estiver na posi��o 7 da string ele estar� na
         casa de milh�o (posi��o 2 do vetor nome_casa)

         se o algarismo estiver na posi��o 10 da string ele estar� na
         casa de milh�o (posi��o 3 do vetor nome_casa)

         e assim por diante. Note que existe uma PA de raz�o 3
         sendo que o primeiro elemento desta sequ�ncia seria o n�mero 1
         (estando este na posi��o 0)
         }
    until i = tam;

    result:= trim(result);

  finally
  end;
end;
procedure PegaCorForm(PForm:Tform);
Begin
     Pform.color:=UdataModulo.CorFormGlobal;
End;

procedure PegaCorForm(PForm:Tframe);
Begin
     Pform.color:=UdataModulo.CorFormGlobal;
End;


Function RetornaDigitoData(Str:String):integer;
var
cont,x,y,w,d:integer;
Begin
     result:=0;
     x:=strtoint(str[1])+strtoint(str[length(str)]);
     y:=0;
     for cont:=2 to length(str)-1 do
     Begin
          y:=y+strtoint(str[cont]);
     End;
     w:=(x+y)*12;
     d:=(w*2)+x+y;
     result:=d;
End;

Function VerificaSoNumeros(Pvalor:String):boolean;
var
cont:integer;
Begin
     result:=False;
     Pvalor:=Trim(Pvalor);

     if (Pvalor='')
     Then exit;

     for cont:=1 to length(Pvalor) do
     Begin
          if not(Pvalor[cont] in ['0'..'9',#8])
          Then exit;
     End;
     Result:=true;
End;

Procedure Filtradatas(out Plista:TStringList);
var
cont,cont2:integer;
lista2:TStringList;
temp:string;
Begin
     lista2:=TStringList.create;
     Try
        //ordenando a lista
        for cont:=0 to Plista.Count-1 do
        Begin

             for cont2:=cont+1 to Plista.Count-1 do
             Begin
                  if (strtodate(Plista[cont2]))<(strtodate(Plista[cont]))
                  Then Begin
                            temp:=Plista[cont];
                            Plista[cont]:=Plista[cont2];
                            Plista[cont2]:=Temp;
                  End;
             End;
        End;
        //deixando apenas os distintos

        temp:=Plista[0];
        lista2.clear;
        lista2.add(temp);
        for cont:=1 to Plista.Count-1 do
        Begin
             if (Plista[cont]<>temp)
             Then Begin
                       lista2.Add(plista[cont]);
                       temp:=Plista[cont];
             End;
        End;

        Plista.clear;
        for cont:=0 to lista2.Count-1
        do begin
             Plista.add(lista2[cont]);
        end;

        
     Finally
            FreeAndNil(lista2);
     End;


End;

Function Submit_ComboBox(PCombo:TComboBox):string;
Begin
     result:='';
     If (PCombo.ItemIndex>=0)
     Then Begin
               if (PCombo.Text<>'')
               Then Result:=PCombo.text[1];
     End;
End;


Function  AdicionaVirgulaDecimal(Pvalor:String;QtdeCasas:Integer):String;
var
pontovirgula,cont:Integer;
temp:String;
Begin
     for cont:=length(pvalor) downto 1 do
     begin
          dec(QtdeCasas,1);
          if (QtdeCasas=0)
          Then Begin
                    PontoVirgula:=Cont;
                    break;
          End;
     End;

     Temp:=copy(Pvalor,1,pontovirgula-1)+','+copy(Pvalor,pontovirgula,length(Pvalor)-1);
     result:=Temp;
End;

Function AdicionaZero(PValor:String):String;
Begin
     result:=Pvalor;
     if (length(pvalor)=1)
     then Result:='0'+PValor;
End;

function CalculaDigitoVerificador(PCodigo: string): string;
var
cont:integer;
Str1e3:string;
tres:Boolean;
multiplicacao:integer;
begin
{
D�gito Verificador (DV)

Como calcular o d�gito verificador para EAN/UCC-8, EAN/UCC-13, ucc-12 e EAN/UCC-14 e do identificador de aplica��o SSCC - AI(00)? 
O c�lculo do d�gito verificador encontra-se abaixo: 

PASSO 1: Abaixo do c�digo completo (sem o d�gito verificador) e come�ando pela direita, situe alternadamente os n�meros 3 e 1 para cada um dos d�gitos, ciando com peso 3.

7 8 9 1 2 3 4 5 0 0 0 1 DV
1 3 1 3 1 3 1 3 1 3 1 3 

PASSO 2: Multiplique todos os d�gitos pelos pesos correspondentes: 

7 8 9 1 2 3 4 5 0 0 0 1 DV 1 3 1 3 1 3 1 3 1 3 1 3 7 24 9 3 2 9 4 15 0 0 0 3 resultado das multiplica��es

PASSO 3: Some todos os resultados das multiplica��es:

7+24+9+3+2+9+4+15+0+0+0+3 = 76 

PASSO 4: O resultado obtido na soma(76) deve ser subtra�do do m�ltiplo de 10 imediatamente maior do que ele. 
Ent�o: 80 - 76 = 4 

CONCLUS�O: O d�gito de controle ser� o resultado dessa subtra��o. No exemplo acima, � o n�mero 4. 
OBS: Quando a soma total resultar em um m�ltiplo de 10 (EX: 80,100, 120 ETC), o d�gito de controle ser� "0" (zero). 


}
     Str1e3:=CompletaPalavra('',length(pcodigo),'0');
     tres:=true;
     for cont:=length(pcodigo) downto 1 do
     Begin
          if (tres=True)
          Then Begin
                  Str1e3[cont]:='3';
                  tres:=False;
          End
          Else Begin
                  Str1e3[cont]:='1';
                  tres:=true;
          End;
     End;
     Multiplicacao:=0;
     for cont:=1 to length(PCodigo) do
     Begin
          Multiplicacao:=Multiplicacao+(StrToInt(PCodigo[cont])*StrToInt(Str1e3[cont]));
     End;
     If (multiplicacao mod 10)=0
     Then Result:=PCodigo+'0'
     Else Begin
               result:=pcodigo;
               for cont:=1 to 9 do
               Begin
                    if (((multiplicacao+cont) mod 10) =0)
                    Then Begin
                              Result:=result+inttostr(cont);
                              exit;
                    End;
               End;
     End;


end;

Function Centraliza_String(Ptexto:String;Pquantidade:integer):string;
var
cont,meioquantidade,meiostring:integer;
temp:string;
Begin
     //este procedimento centraliza uma string,  usada em impressoes
     //se a string for maior ela trunca a string na quantidade de parametro

     temp:='';
     result:='';

     if (length(Ptexto)>=pquantidade)
     Then Begin
               result:=copy(Ptexto,1,pquantidade);
               exit;
     end;
     meioquantidade:=strtoint(floattostr(int(pquantidade/2)));
     meiostring:=strtoint(floattostr(int(length(ptexto)/2)));

     //123456789X123456789X=20/2=10 meio
     //      123456        =6/2=  3 meio


     temp:=completapalavra(' ',meioquantidade-meiostring,' ');
     temp:=temp+ptexto;

     result:=temp;
End;

Function Centraliza_String(Ptexto:String;Pquantidade:integer;PCompleta:string):string;
Begin
      result := Centraliza_String(Ptexto,Pquantidade,PCompleta,true);
End;

Function Centraliza_String(Ptexto:String;Pquantidade:integer;PCompleta:string;TruncaString:boolean):string;
var
cont,meioquantidade,meiostring:integer;
temp:string;
Begin
     //este procedimento centraliza uma string,  usada em impressoes
     //se a string for maior ela trunca a string na quantidade de parametro

     temp:='';
     result:='';

     if (length(Ptexto)>=pquantidade)
     Then Begin
               if(TruncaString)
               then begin
                     result:=copy(Ptexto,1,pquantidade);
                     exit;
               end;
     end;
     meioquantidade:=strtoint(floattostr(int(pquantidade/2)));
     meiostring:=strtoint(floattostr(int(length(ptexto)/2)));

     //123456789X123456789X=20/2=10 meio
     //      123456        =6/2=  3 meio


     temp:=completapalavra(pcompleta,meioquantidade-meiostring,pcompleta);
     temp:=temp+ptexto;
     result:=completapalavra(temp,pquantidade,pcompleta);
End;

//jonas
function trunca_string(palavra:string;pquantidade:integer):string;
var
  indice:integer;
begin

  if ( pquantidade > Length(palavra) )
  then
      result:=palavra
  else
  begin

    result:='';
    for indice:=1 to pquantidade do
       result:=Result + palavra[indice];

  end;

end;



function formata_CNPJ(CNPJ:string): string;
begin

   if (Length (RetornaSoNumeros (CNPJ)) = 14) then
   begin

    Insert ('.',CNPJ,3);
    Insert ('.',CNPJ,7);
    Insert ('/',CNPJ,11);
    Insert ('-',CNPJ,16);

    result := CNPJ;

   end else
   begin

    MensagemAviso ('CNPJ inv�lido');
    Result := '';

   end;

end;

function formata_cpf  (cpj:string):string;
begin

  if (Length (RetornaSoNumeros (cpj)) = 11) then
  begin

    Insert ('.',cpj,3);
    Insert ('.',cpj,6);
    Insert ('.',cpj,9);
    Insert ('-',cpj,11);

    result := cpj;

  end
  else
  begin

    MensagemAviso ('cpj inv�lido');
    Result := '';

  end;

end;

function fDate (parametro:string):string;
var

  dia,mes,ano:string;
begin

  if (Length (parametro) < 10) then Exit;

  dia:= '';
  mes:= '';
  ano:= '';

  ano := parametro[1] + parametro[2] + parametro[3] + parametro[4];
  mes := parametro[6] + parametro[7];
  dia := parametro[9] + parametro[10];
  result := dia+'/'+mes+'/'+ano;

end;

function fTime(parametro:string):string;
var
  hora,minuto,segundo:string;
begin
  hora := '';
  minuto := '';
  segundo := '';
  //2016-03-09T10:34:01-04:00
  if (Length (parametro) < 18) then Exit;
  hora := parametro[12] + parametro[13];
  minuto := parametro[15] + parametro[16];
  segundo := parametro[18] + parametro[19];
  Result := hora+':'+minuto+':'+segundo;
end;




Function  tira_espacos_final(palavra:string):String;
var
cont:integer;
temp:string;
begin
     for cont:=length(palavra) downto 1 do
     Begin
          if palavra[cont]<>' '
          then break;
     End;
     temp:=copy(palavra,1,cont);
     result:=temp;

End;

Function  StrReplace(Ptexto,Pantiga,Pnova:String):String;
var
  SelPos: Integer;
  temp,ptexto1:String;
begin
  ptexto1:=ptexto;
  selpos:=1;
      SelPos := Pos(pantiga, Ptexto1);
      if SelPos > 0 then
      begin
          temp:=copy(ptexto1,1,selpos-1);
          temp:=temp+pnova;
          temp:=temp+copy(ptexto1,selpos+length(pantiga),length(ptexto1)-selpos+length(pantiga));;
      end
      else temp:=ptexto1;

  Result:=temp;
End;

Function  tirazeroesquerda(parametro:string):string;
var
cont:integer;
temp:string;
Begin

     for cont:=1 to length(parametro) do
     Begin
          if (parametro[cont]<>'0')
          Then break;
     end;
     result:=copy(parametro,cont,length(parametro)-cont+1);
     
End;


procedure edtf9KeyPress(Sender: TObject; var Key: Char);
Begin
     Tedit(Sender).ShowHint:=True;
     Tedit(Sender).Hint:='Pressione F9 para pesquisa';

     if not(key in['0'..'9',#8])
     Then key:=#0;
End;







Function Arredonda(PValor:String):String;
var
pinteiro,PvalorStr:String;
depoisvirgula:string[3];
cont:integer;
Begin
     if pos(',',pvalor)=0
     Then Begin//nao tem virgula
                result:=pvalor;
                exit;
     end;


     pinteiro:=floattostr(int(strtofloat(pvalor)));

     for cont:=length(pvalor) downto 1 do
     Begin
          if (pvalor[cont]=',')
          Then break;
     End;

     depoisvirgula:=copy(pvalor,cont+1,length(pvalor)-cont+1);

     if (length(depoisvirgula)>=2)
     Then Begin
               pinteiro:=pinteiro+','+depoisvirgula[1]+depoisvirgula[2];

               if (length(depoisvirgula)>=3)
               Then Begin
                         if (strtoint(depoisvirgula[3])>=5)
                         Then pinteiro:=floattostr(strtofloat(pinteiro)+0.01);
               End;
               result:=pinteiro;
     end
     Else Result:=PValor;



End;

function Troca_13_10_por_pipeline(palavra:string):string;
var
cont:integer;
temp:string;
begin
     temp:='';
     for cont:=1 to length(palavra) do
     Begin
          if (ord(palavra[cont])=10)
          Then temp:=temp+'|'
          Else
             if (ord(palavra[cont])<>13)
             Then temp:=temp+palavra[cont];
     End;
     result:=temp;

end;


Function DesCriptografacaixa(PNumeroCaixa:String):String;
var
PNumeroCaixa2,temp:String;
cont:integer;
Begin
     PNumeroCaixa2:='';
     PNumeroCaixa2:=Useg.DesincriptaSenha(PNumeroCaixa);
     
     for cont:=1 to length(PNumeroCaixa2) do
     Begin
          case PNumeroCaixa2[cont] of
            'A':temp:=temp+'0';
            'M':temp:=temp+'1';
            'R':temp:=temp+'2';
            'F':temp:=temp+'3';
            'G':temp:=temp+'4';
            'H':temp:=temp+'5';
            'I':temp:=temp+'6';
            'K':temp:=temp+'7';
            'L':temp:=temp+'8';
            'B':temp:=temp+'9';
          End;
     End;
     Result:=temp;
End;

Function Criptografacaixa(PNumeroCaixa:String):String;
var
temp:String;
cont:integer;
Begin
     temp:='';
     for cont:=1 to length(PNumeroCaixa) do
     Begin
          case PNumeroCaixa[cont] of
            '0':temp:=temp+'A';
            '1':temp:=temp+'M';
            '2':temp:=temp+'R';
            '3':temp:=temp+'F';
            '4':temp:=temp+'G';
            '5':temp:=temp+'H';
            '6':temp:=temp+'I';
            '7':temp:=temp+'K';
            '8':temp:=temp+'L';
            '9':temp:=temp+'B';
          End;
     End;
     Result:=Useg.EncriptaSenha(temp);
End;
Function PegaPrimeiraPalavra(Palavra:String):String;
var
cont:integer;
temp1,temp2:String;
Begin

     //tirando os espacos
     for cont:=1 to length(palavra) do
     begin
          if palavra[cont]<>' '
          Then break;
     end;
     temp1:=copy(palavra,cont,length(palavra)-cont+1);
     temp2:='';

     for cont:=1 to length(temp1) do
     begin
          if temp1[cont]=' '
          Then break
          else temp2:=temp2+temp1[cont];
     end;

     result:=uppercase(trim(temp2));
End;


Function  RetornadiaExtenso(dia:integer):string;
Begin
     case dia of
     1:result:='Domingo';
     2:result:='Segunda';
     3:result:='Ter�a';
     4:result:='Quarta';
     5:result:='Quinta';
     6:result:='Sexta';
     7:result:='S�bado';
     End;
End;

Function AcertaMaiusculaMinusculo(Frase:String):String;
var
temp1,temp:String;
cont:integer;
Begin
     temp:=LowerCase(frase);
     temp1:='';
     for cont:=length(temp) downto 1 do
     Begin
          if (cont>1)
          Then Begin
                    if (temp[cont-1]=' ')
                    Then temp1:=temp1+uppercase(temp[cont])
                    Else temp1:=temp1+temp[cont];
          End
          Else temp1:=temp1+uppercase(temp[cont]);
     End;

     temp:=ReverseString(temp1);
     result:=Temp;
End;

Function PegaPrimeiroNome(Nome:String):String;
var
cont,posicao:integer;
Begin
     posicao:=pos(' ',Nome);

     if (posicao=0)//nao encontrou
     Then result:=nome
     Else result:=copy(nome,1,posicao);
End;




Function  Retorna_codigo(Pcodigos:String;var StrRetorno:TStringList):boolean;
Begin
     Result:=ExplodeSTR(Pcodigos,StrRetorno,';','integer');
End;


Function  ExplodeStr(Pcodigos:String;var StrRetorno:TStringList;PSeparador:string;PtipoValor:String):boolean;
var
cont:integer;
temp:String;
Begin
     ptipovalor:=uppercase(ptipovalor);

     result:=False;
     StrRetorno.Clear;
     temp:='';
     for cont:=1 to length(pcodigos) do
     Begin
          if ((Pcodigos[cont]=Pseparador) or (cont=length(Pcodigos)))
          then begin
                    if (cont=length(Pcodigos)) and
                    (Pcodigos[cont]<>Pseparador)
                    Then temp:=temp+Pcodigos[cont];

                    if (temp<>'')
                    Then Begin
                              Try
                                 if (PtipoValor='INTEGER')
                                 Then strtoint(temp)
                                 Else
                                    if (PtipoValor='DATE')
                                    Then strtodate(temp)
                                    Else
                                       if (PtipoValor='TIME')
                                       Then StrToTime(temp)
                                       Else
                                          if (Ptipovalor='FLOAT')
                                          Then StrToFloat(temp);
                                 Strretorno.add(temp);
                              Except
                                    on e:exception do
                                    begin
                                        Messagedlg('Valor inv�lido '+temp+#13+'Erro retornado: '+E.Message,mterror,[mbok],0);
                                        exit;
                                    End;
                              end;
                              temp:='';
                    end;
          end
          Else temp:=temp+Pcodigos[cont];
     end;

     if (temp<>'')
     Then Begin
               if (temp<>'')
               Then Begin
                         Try
                            if (PtipoValor='INTEGER')
                            Then strtoint(temp)
                            Else
                               if (PtipoValor='DATE')
                               Then strtodate(temp)
                               Else
                                  if (PtipoValor='TIME')
                                  Then StrToTime(temp)
                                  Else
                                     if (Ptipovalor='FLOAT')
                                     Then StrToFloat(temp);

                            Strretorno.add(temp);
                         Except
                               on e:exception do
                               begin
                                   Messagedlg('Valor inv�lido '+temp+#13+'Erro retornado: '+E.Message,mterror,[mbok],0);
                                   exit;
                               End;
                         end;
                         temp:='';
               end;
     end;


     result:=true;
end;


Function  ExplodeStr(Pcodigos:String;var StrRetorno:TStrings;PSeparador:string;PtipoValor:String):boolean;
var
cont:integer;
temp:String;
Begin
     ptipovalor:=uppercase(ptipovalor);

     result:=False;
     StrRetorno.Clear;
     temp:='';
     for cont:=1 to length(pcodigos) do
     Begin
          if (Pcodigos[cont]=Pseparador)
          then begin
                    if (temp<>'')
                    Then Begin
                              Try
                                 if (PtipoValor='INTEGER')
                                 Then strtoint(temp)
                                 Else
                                    if (PtipoValor='DATE')
                                    Then strtodate(temp)
                                    Else
                                       if (PtipoValor='TIME')
                                       Then StrToTime(temp)
                                       Else
                                          if (Ptipovalor='FLOAT')
                                          Then StrToFloat(temp);
                                 Strretorno.add(temp);
                              Except
                                    on e:exception do
                                    begin
                                        Messagedlg('Valor inv�lido '+temp+#13+'Erro retornado: '+E.Message,mterror,[mbok],0);
                                        exit;
                                    End;
                              end;
                              temp:='';
                    end;
          end
          Else temp:=temp+Pcodigos[cont];
     end;

     if (temp<>'')
     Then Begin
               if (temp<>'')
               Then Begin
                         Try
                            if (PtipoValor='INTEGER')
                            Then strtoint(temp)
                            Else
                               if (PtipoValor='DATE')
                               Then strtodate(temp)
                               Else
                                  if (PtipoValor='TIME')
                                  Then StrToTime(temp)
                                  Else
                                     if (Ptipovalor='FLOAT')
                                     Then StrToFloat(temp);

                            Strretorno.add(temp);
                         Except
                               on e:exception do
                               begin
                                   Messagedlg('Valor inv�lido '+temp+#13+'Erro retornado: '+E.Message,mterror,[mbok],0);
                                   exit;
                               End;
                         end;
                         temp:='';
               end;
     end;


     result:=true;
end;

Function  Implode(StringList:TStringList;var retorno:string):boolean;
var
cont:integer;
Begin
     result:=false;
     retorno:=StringList[0];

     for cont:=1 to StringList.count-1 do
     begin
          retorno:=retorno+';'+StringList[cont];
     End;
     result:=true;
End;


Function  ExplodeStr_COMVAZIOS(Pcodigos:String;var StrRetorno:TStringList;PSeparador:string;PtipoValor:String):boolean;
var
cont:integer;
temp:String;
Begin
     ptipovalor:=uppercase(ptipovalor);
     
     result:=False;
     StrRetorno.Clear;
     temp:='';
     for cont:=1 to length(pcodigos) do
     Begin
          if (Pcodigos[cont]=Pseparador)
          then begin
                    Try
                       if (PtipoValor='INTEGER')
                       Then strtoint(temp)
                       Else
                          if (PtipoValor='DATE')
                          Then strtodate(temp)
                          Else
                             if (PtipoValor='TIME')
                             Then StrToTime(temp)
                             Else
                                if (Ptipovalor='FLOAT')
                                Then StrToFloat(temp);
                       Strretorno.add(temp);
                    Except
                          on e:exception do
                          begin
                              Messagedlg('Valor inv�lido '+temp+#13+'Erro retornado: '+E.Message,mterror,[mbok],0);
                              exit;
                          End;
                    end;
                    temp:='';

          end
          Else temp:=temp+Pcodigos[cont];
     end;

     if (temp<>'')
     Then Strretorno.add(temp);

     result:=true;
end;


Procedure MensagemErro(Mensagem:String);
Begin
  //MessageDlg(Mensagem, mtError, [mbOk], 0);
  DesbloqueiaTecladoeMouse;
  Application.BringToFront;
  Application.MessageBox(pchar(Mensagem),'Erro',MB_ICONERROR + MB_OK);
End;

Procedure MensagemAviso(Mensagem:String);
Begin
  //MessageDlg(Mensagem, mtConfirmation, [mbOk], 0);
  DesbloqueiaTecladoeMouse;
  Application.BringToFront;
  Application.MessageBox(pchar(Mensagem),'Aviso',MB_ICONINFORMATION + MB_OK) ;
end;

Procedure MensagemSucesso(Mensagem:String);
Begin
  //MessageDlg(Mensagem, mtConfirmation, [mbOk], 0);
  DesbloqueiaTecladoeMouse;
  Application.BringToFront;
  Application.MessageBox(pchar(Mensagem),'Sucesso', MB_OK);
end;

function  MensagemPergunta(Mensagem:String):integer;
begin
  //result:=Messagedlg(Mensagem,mtconfirmation,[mbyes,mbno],0);
  DesbloqueiaTecladoeMouse;
  Application.BringToFront;
  result:=Application.MessageBox(pchar(Mensagem),'Pergunta',MB_ICONQUESTION + MB_YESNO);
End;


Function RetornaSoNUmeros(Palavra:String):String;
var
cont:integer;
temp:string;
Begin

     temp:='';
     for cont:=1 to length(palavra) do
     Begin
          if (palavra[cont] in ['0'..'9'])
          Then temp:=Temp+palavra[cont];
     End;
     result:=Temp; 
End;

Function RetornaNumeroseLetras(Palavra:String):String;
var
cont:integer;
temp:string;
Begin
     temp:='';
     for cont:=1 to length(Palavra) do
     Begin
          if (Palavra[cont] in ['0'..'9'])
          Then temp:=Temp+Palavra[cont];
          if (Palavra[cont] in ['A'..'Z'])
          Then temp:=Temp+Palavra[cont];
          if (Palavra[cont] in ['a'..'z'])
          Then temp:=Temp+Palavra[cont];
     End;
     result:=Temp;
end;

Function RetornaSoLetras(Palavra:String):String;
var
cont:integer;
temp:string;
Begin
     temp:='';
     for cont:=1 to length(Palavra) do
     Begin
          if (Palavra[cont] in ['A'..'Z'])
          Then temp:=Temp+Palavra[cont];
          if (Palavra[cont] in ['a'..'z'])
          Then temp:=Temp+Palavra[cont];
     End;
     result:=Temp;
end;

//Fun��o para substituir caracteres especiais.

function TrocaCaracterEspecial(aTexto : string; aLimExt : boolean) : string;
const
  //Lista de caracteres especiais
  xCarEsp: array[1..38] of String = ('�', '�', '�', '�', '�','�', '�', '�', '�', '�',
                                     '�', '�','�', '�','�', '�','�', '�',
                                     '�', '�', '�','�', '�','�', '�', '�', '�', '�',
                                     '�', '�', '�','�','�', '�','�','�','�','�');
  //Lista de caracteres para troca
  xCarTro: array[1..38] of String = ('a', 'a', 'a', 'a', 'a','A', 'A', 'A', 'A', 'A',
                                     'e', 'e','E', 'E','i', 'i','I', 'I',
                                     'o', 'o', 'o','o', 'o','O', 'O', 'O', 'O', 'O',
                                     'u', 'u', 'u','u','u', 'u','c','C','n', 'N');
  //Lista de Caracteres Extras
  xCarExt: array[1..48] of string = ('<','>','!','@','#','$','%','�','&','*',
                                     '(',')','_','+','=','{','}','[',']','?',
                                     ';',':',',','|','*','"','~','^','�','`',
                                     '�','�','�','�','�','�','�','�','�','�',
                                     '�','�','�','�','�','�','�','�');
var
  xTexto : string;
  i : Integer;
begin
   xTexto := aTexto;
   for i:=1 to 38 do
     xTexto := StringReplace(xTexto, xCarEsp[i], xCarTro[i], [rfreplaceall]);
   //De acordo com o par�metro aLimExt, elimina caracteres extras.  
   if (aLimExt) then
     for i:=1 to 48 do
       xTexto := StringReplace(xTexto, xCarExt[i], '', [rfreplaceall]);   
   Result := xTexto;
end;

procedure AbreCalendario(editenviou: TObject;Form:TForm);
begin
     Fcalendario.EditEnviouGlobal:=editenviou;
     Fcalendario.Left:=Form.Left+Tedit(editenviou).left;
     Fcalendario.top:=Form.Top+Tedit(editenviou).top;
     Fcalendario.Calendario.Date:=now;
     Fcalendario.showmodal;
end;


Function  CarregaAtualizacao:boolean;
var
Path:String;
Path_pchar:pchar;
Begin
     result:=False;

     path:=ExtractFilePath(Application.ExeName);
     if (path[length(path)]<>'\')
     Then path:=path+'\';

     path:=path+'AtualizaExe.Exe';

     if (FileExists(path)=False)
     Then Begin
               Messagedlg('O arquivo '+Path+' n�o foi encontrado',mterror,[mbok],0);
               exit;
     End;

    // Winexec(pchar(path),SW_SHOWNORMAL);
     ShellExecute(Application.Handle,'',pchar(path),'','',SW_SHOWNORMAL);
     result:=True;
End;



Function  PegaPathSistema:String;
var
Path:String;
Begin
     path:=ExtractFilePath(Application.ExeName);
     if (path[length(path)]<>'\')
     Then path:=path+'\';
     result:=path;
End;

Function PegaPathWindows:String;
var
Buffer : Array[0..144] of Char;
Begin
      GetSystemDirectory(Buffer,144);
      Result := IncludeTrailingBackslash(StrPas(Buffer))
End;

(******************************************************************************
    16/11/09 - Celio
    - Valida o Path+Diret�rio Passado como par�metro
*******************************************************************************)
Function  ValidaPathDiretorio(pDiretorio:String):boolean;
Begin
     Result := false;
     If(DirectoryExists(pDiretorio))
     then Result := True;
End;

(******************************************************************************
    16/11/09 - Celio
    - Valida o Path+Arquivo Passado como par�metro
*******************************************************************************)
Function  ValidaPathArquivo(pArquivo:String):Boolean;
Begin
     result:=false;
     If(FileExists(pArquivo))
     then Result := True;
End;




Function RETORNA_HORA_5_DIGITOS(Phora:String):string;
var
temp:string;
Begin
     Result:='';
     temp:=trim(phora);
     if (temp<>'')
     then Begin
               try
                  temp:=Formatdatetime('hh:mm',strtotime(temp));
               Except
                     temp:='';
               End;
     end;
     Result:=Temp;

End;

function AdiquiraOuCrieObjeto(const ClasseNome: String): IDispatch;
var Classifique_Id : TGUID;
begin
  Classifique_ID:=ProgIdToClassId(ClasseNome);
  Result:=CreateOleObject(ClasseNome);
end;

function ValidaCPF(const cCPF: String): boolean;
var
i, soma, multiplo: integer;
CPF: string;
begin
    Result:= false;
    CPF :=RetornaSoNUmeros(Trim(cCPF));

    if (Length(CPF) <> 11)
    Then exit;

    soma := 0;
    for i := 9 downto 1 do
    begin
        soma := soma + StrToInt(CPF[i]) * (11 - i);
    end;

    multiplo := soma mod 11;
    if multiplo <= 1
    then multiplo := 0
    else multiplo := 11 - multiplo;

    if (multiplo <> StrToInt(CPF[10]))
    then exit;

    soma := 0;
    for i := 10 downto 1 do
    begin
        soma := soma + StrToInt(CPF[i]) * (12 - i);
    end;
    multiplo := soma mod 11;

    if multiplo <= 1
    then multiplo := 11;

    Result:= STRToInt(CPF[11]) = (11 - multiplo);

end;


function ValidaCNPJ(const cCNPJ: string): boolean;
var
i, soma, mult: integer;
CGC: string;
begin
    result:= false;
    CGC := RetornaSoNumeros(trim(cCNPJ));
    if Length(CGC) <> 14
    then exit;

    soma := 0; mult := 2;
    for i := 12 downto 1 do
    begin
        soma := soma + StrToInt(CGC[i]) * mult;
        mult := mult + 1;
        if mult > 9
        then mult := 2;
    end;

    mult := soma mod 11;
    if mult <= 1
    then mult := 0
    else mult := 11 - mult;

    if mult <> StrToInt(CGC[13])
    then exit;

    soma := 0;
    mult := 2;

    for i := 13 downto 1 do
    begin
        soma := soma + StrToInt(CGC[i]) * mult;
        mult := mult + 1;
        if mult > 9
        then mult := 2;
    end;

    mult := soma mod 11;
    if mult <= 1
    then mult := 0
    else mult := 11 - mult;

    result:= mult = StrToInt(CGC[14]);
end;

function NumeroExtenso_Bilhao(const Valor: double; Moeda: Boolean = False): string;
const
  Centenas: array[1..9] of string[12] = ('Cem', 'Duzentos', 'Trezentos',
    'Quatrocentos', 'Quinhentos', 'Seiscentos', 'Setecentos',
    'Oitocentos', 'Novecentos');
  Dezenas: array[2..9] of string[10] = ('Vinte', 'Trinta', 'Quarenta',
    'Cinquenta', 'Sessenta', 'Setenta', 'Oitenta', 'Noventa');
  Dez: array[0..9] of string[10] = ('Dez', 'Onze', 'Doze', 'Treze', 'Quatorze',
    'Quinze', 'Dezesseis', 'Dezessete', 'Dezoito', 'Dezenove');
  Unidades: array[1..9] of string[10] = ('Um', 'Dois', 'Tr�s', 'Quatro', 'Cinco',
    'Seis', 'Sete', 'Oito', 'Nove');
  Zero = 'Zero';

  function Ext3(Parte: string): string;
  var
    Base: string;
    digito: integer;
  begin
    Base := '';
    digito := StrToInt(Parte[1]);
    if digito = 0 then
      Base := ''
    else
      Base := Centenas[digito];
    if (digito = 1) and (Parte > '100') then
      Base := 'Cento';
    Digito := StrToInt(Parte[2]);
    if digito = 1 then
    begin
      Digito := StrToInt(Parte[3]);
      if Base <> '' then
        Base := Base + ' e ';
      Base := Base + Dez[Digito];
    end
    else
    begin
      if (Base <> '') and (Digito > 0) then
        Base := Base + ' e ';
      if Digito > 1 then
        Base := Base + Dezenas[digito];
      Digito := StrToInt(Parte[3]);
      if Digito > 0 then
      begin
        if Base <> '' then
          Base := Base + ' e ';
        Base := Base + Unidades[Digito];
      end;
    end;
    Result := Base;
  end;

var
  ComoTexto: string;
  Parte: string;
  MoedaSingular: String;
  MoedaPlural: String;
  CentSingular: String;
  CentPlural: String;

begin
  if Moeda then
  begin
    MoedaSingular := 'Real';
    MoedaPlural := 'Reais';
    CentSingular := 'Centavo';
    CentPlural := 'Centavos';
  end
  else
  begin
    MoedaSingular := '';
    MoedaPlural := '';
    CentSingular := '';
    CentPlural := '';
  end;

  Result := '';
  ComoTexto := FloatToStrF(Abs(Valor), ffFixed, 18, 2);
  // Acrescenta zeros a esquerda ate 12 digitos
  while length(ComoTexto) < 15 do
    Insert('0', ComoTexto, 1);
  // Retira caracteres a esquerda para o m�ximo de 12 digitos
  while length(ComoTexto) > 15 do
    delete(ComoTexto, 1, 1);
  // Calcula os bilh�es
  Parte := Ext3(copy(ComoTexto, 1, 3));
  if StrToInt(copy(ComoTexto, 1, 3)) = 1 then
    Parte := Parte + ' Bilh�o'
  else
    if Parte <> '' then
      Parte := Parte + ' Bilh�es ';
  Result := Parte;
  // Calcula os nilh�es
  Parte := Ext3(copy(ComoTexto, 4, 3));
  if Parte <> '' then
  begin
    if Result <> '' then
      Result := Result + ', ';
    if StrToInt(copy(ComoTexto, 4, 3)) = 1 then
      Parte := Parte + ' Milh�o '
    else
      Parte := Parte + ' Milh�es ';
    Result := Result + Parte;
  end;
  // Calcula os nilhares
  Parte := Ext3(copy(ComoTexto, 7, 3));
  if Parte <> '' then
  begin
    if Result <> '' then
      Result := Result + ', ';
    Parte := Parte + ' Mil';
    Result := Result + Parte;
  end;
  // Calcula as unidades
  Parte := Ext3(copy(ComoTexto, 10, 3));
  if Parte <> '' then
  begin
    if Result <> '' then
      if Frac(Valor) = 0 then
        Result := Result + ' e '
      else
        Result := Result + ', ';
    Result := Result + Parte;
  end;
  // Acrescenta o texto da moeda
  if Int(Valor) = 1 then
    Parte := ' ' + MoedaSingular
  else
    Parte := ' ' + MoedaPlural;
  if copy(ComoTexto, 7, 6) = '000000' then
    Parte := 'de ' + MoedaPlural;
  Result := Result + Parte;
  // Se o valor for zero, limpa o resultado
  if int(Valor) = 0 then
    Result := '';
  //Calcula os centavos
  Parte := Ext3('0' + copy(ComoTexto, 14, 2));
  if Parte <> '' then
  begin
    if Result <> '' then
      Result := Result + ' e ';
    if Parte = Unidades[1] then
      Parte := Parte + ' ' + CentSingular
    else
      Parte := Parte + ' ' + CentPlural;
    Result := Result + Parte;
  end;
  // Se o valor for zero, assume a constante ZERO
  if Valor = 0 then
    Result := Zero;
end;


function numeroExtenso(Valor: String):String;
const
  //o posi��o zero � a casa de unidades e, nesta n�o h� sufixo
  Nome_Casa: Array [0..6] of String = ('','Mil','Milh�o','Bilh�o','Trilh�o','Quadrilh�o','Quintilh�o');
  Nome_Casa_Plural: Array [0..6] of String = ('','Mil','Milh�es','Bilh�es','Trilh�es','Quadrilh�es','Quintilh�es');
  decimal: array [1..9] of String = ('Onze','Doze','Treze','Quatorze','Quinze','Dezesseis','Dezessete','Dezoito','Dezenove');
var
  FileNames: TStringList;
  Casas: Array [1..3,0..9] of String;

  casa, valor_casa, aux: String;
  n: integer;
  i, j, ncasa, tam: integer;
begin
  result:= '';

  if trim(valor) = '' then
     exit;
  casas[1,0]:= '';
  casas[1,1]:= 'Um';
  casas[1,2]:= 'Dois';
  casas[1,3]:= 'Tr�s';
  casas[1,4]:= 'Quatro';
  casas[1,5]:= 'Cinco';
  casas[1,6]:= 'Seis';
  casas[1,7]:= 'Sete';
  casas[1,8]:= 'Oito';
  casas[1,9]:= 'Nove';

  casas[2,0]:= '';
  casas[2,1]:= 'Dez';
  casas[2,2]:= 'Vinte';
  casas[2,3]:= 'Trinta';
  casas[2,4]:= 'Quarenta';
  casas[2,5]:= 'Cinquenta';
  casas[2,6]:= 'Sessenta';
  casas[2,7]:= 'Setenta';
  casas[2,8]:= 'Oitenta';
  casas[2,9]:= 'Noventa';

  casas[3,0]:= '';
  casas[3,1]:= 'Cem';
  casas[3,2]:= 'Duzentos';
  casas[3,3]:= 'Trezentos';
  casas[3,4]:= 'Quatrocentos';
  casas[3,5]:= 'Quinhentos';
  casas[3,6]:= 'Seiscentos';
  casas[3,7]:= 'Setecentos';
  casas[3,8]:= 'Oitocentos';
  casas[3,9]:= 'Novecentos';
  try
    valor_casa:= '';
    tam:= length(valor);
    j:= tam + 1;
    i:= 0;

    repeat
        inc(i);
        dec(j);
        if j mod 3 = 0 then
           ncasa:= 3
        else ncasa:= j mod 3;
        {a vari�vel valor_casa � o valor num�rico a cada separador de milhar (.)
         Exemplo 51.439  - valor da casa de milhar � 51 e o valor da casa de centana � 439}

        {a vari�vel ncasa � a posi��o do algarismo dentro da casa.
         Exemplo:
            valor da vari�vel ncasa para cada algarismo do n�  1 321
            n�mero a ser escrito por extenso                   7.428}

        valor_casa:= valor_casa + valor[i];
        n:= StrToInt(valor[i]);
        casa:= '';
        casa:= casas[ncasa, n];
        if AnsiSameText(casa,'cem') and ((valor[i+1] <> '0') or (valor[i+2] <> '0')) then
           casa:= 'Cento';

        {verificar se o valor est� entre 11 e 29, pois o formato deste valores foge do padr�o}
        {o valor s� � verificado se a vari�vel ncasa for igual a 2 (indicando que � casa de decimais)}
        if (ncasa = 2) and ((valor[i]= '1') and (valor[i+1] > '0')) then
        begin
          {se o valor da casa decimal for, por exemplo, igual a 15, o 2�
           d�gito vai representar a posi��o (5) no vetor de nome decimal
           que cont�m o nome por extenso do valor 15}
          casa:= decimal[StrToInt(valor[i+1])];
          {quando encontra-se um valor decimal entre 11 e 19, s�o processados
          dois algarismos do n�mero de uma �nica vez, ou seja, s�o
          escritos 2 algarismos por extenso de uma vez s�, assim,
          deve-se pular para a pr�xima posi��o do n�mero incrementando o I
          e decrementando o J.}
          inc(i);
          valor_casa:= valor_casa + valor[i];
          dec(j);
          ncasa:= 1;
        end;

        if (result <> '') and (casa <> '') then
        begin
           if (ncasa <> 3) or ((ncasa = 3) and (j = 3)) then
           begin
             result:= result + ' e ' + casa;
           end
           else  result:= result + ' ' + casa;
        end
        else if result = '' then
           result:= casa;



        {descobrir o nome da casa (se � de mil, milh�o, bilh�o, etc)}
        if (ncasa = 1) then
        begin
           if StrToInt(Valor_Casa) = 1 then
              aux:= trim(nome_casa[(j-1) div 3])
           else aux:= trim(nome_casa_plural[(j-1) div 3]);

           result:= result + ' ' + aux;
           valor_casa:= '';
        end;
        result:= trim(result);

        {para descobrir o nome da casa (se � mil, milh�o, bilh�o)
         usou-se a f�rmula do termo geral da PA (Progress�o Aritm�tica) que �
         Ai = A1 + (i * r) assim i = (Ai - A1)/r

         se o algarismo estiver na posi��o 4 da string ele estar� na
         casa de milhar (posi��o 1 do vetor nome_casa)

         se o algarismo estiver na posi��o 7 da string ele estar� na
         casa de milh�o (posi��o 2 do vetor nome_casa)

         se o algarismo estiver na posi��o 10 da string ele estar� na
         casa de milh�o (posi��o 3 do vetor nome_casa)

         e assim por diante. Note que existe uma PA de raz�o 3
         sendo que o primeiro elemento desta sequ�ncia seria o n�mero 1
         (estando este na posi��o 0)
         }
    until i = tam;

    result:= trim(result);

  finally
  end;
end;

function valorextenso (valor: Currency): string;
begin
      Result:=NumeroExtenso_Bilhao(Valor, true);
end;

procedure MostraMensagemSistemaDemo;
Begin
     MostraMensagemSistemaDemo('');
End;

procedure MostraMensagemSistemaDemo(parametro:string);
begin
     if (parametro<>'')
     then MensagemAviso('O sistema est� sendo usado em Modo Demonstra��o, n�o � permitido utiliz�-lo comercialmente,  para isso adquira o sistema completo'+#13+parametro)
     Else MensagemAviso('O sistema est� sendo usado em Modo Demonstra��o, n�o � permitido utiliz�-lo comercialmente,  para isso adquira o sistema completo');
     exit;
End;

Function ProcuraFrase(Arquivo:string;frase:string):boolean;
var
arq:textfile;
palavra:string;
begin
     result:=False;
     try
        reset(arq,arquivo);
        //reset(arq);
     Except
           mensagemerro('Erro na tentativa de abrir o arquivo');
           exit;
     End;

     Try
         while not(eof(arq)) do
         begin
              read(arq,palavra);
              if (pos(frase,palavra)>0)
              Then Begin
                      result:=True;
                      exit;
              End;
         End;
     Finally
            closefile(arq);
     End;
End;

function RetornaDataServidor: Tdate;
var
STRP:TIBStoredProc;
DataHoraAtual:TdateTime;
begin
     Try
        STRP:=TIBStoredProc.create(nil);
        STRP.database:=FdataModulo.ibdatabase;
     Except
           Messagedlg('Erro na Cria��o da StoredProcedure de data do servidor!',mterror,[mbok],0);
           exit;
     End;

     Try
        STRP.StoredProcName:='PROCLOGVENCTOPEND';
        STRP.ExecProc;
        DataHoraAtual:=STRP.ParamByname('datahora').asdatetime;
        result:=strtodate(datetostr(datahoraatual));
     Finally
            FreeAndNil(STRP);
     End;
end;

function RetornaDataHoraServidor: Tdatetime;
var
STRP:TIBStoredProc;
DataHoraAtual:TdateTime;
begin
     Try
        STRP:=TIBStoredProc.create(nil);
        STRP.database:=FdataModulo.ibdatabase;
     Except
           Messagedlg('Erro na Cria��o da StoredProcedure de data do servidor!',mterror,[mbok],0);
           exit;
     End;

     Try
        STRP.StoredProcName:='PROCLOGVENCTOPEND';
        STRP.ExecProc;
        DataHoraAtual:=STRP.ParamByname('datahora').asdatetime;
        result:=DataHoraAtual;
     Finally
            FreeAndNil(STRP);
     End;
end;


function TotalLinhas(Pfrase:string;
  ColunasporLinha: integer): integer;
begin
      result:=0;
      If(Length(Pfrase)>ColunasporLinha)
      Then Begin
            result:=(Length(Pfrase)div ColunasporLinha);
            If(Length(Pfrase) mod ColunasporLinha>0)
            Then result:=result+1;
      End
      Else result:=1;
end;

function TotalLinhasStringlist(PSTL: TStringList;
  ColunasporLinha: integer): integer;
var cont:integer;
begin
      result:=0;
      For cont:=0 to PSTL.Count-1 do
      Begin
            If(Length(PSTL[cont])>ColunasporLinha)
            Then Begin
                  result:=result+(Length(PSTL[cont])div ColunasporLinha);
                  If(Length(PSTL[cont]) mod ColunasporLinha>0)
                  Then result:=result+1;
            End
            Else result:=1;
      End;

end;

Function DivideEmLinhas(pFrase:String;ColunasporLinha:integer):string;
var
      temp:string;
      cont : integer;
begin
      temp := '';
      cont := 1;
      while cont < length(pfrase) do
      begin
            if(cont+ColunasporLinha>length(pfrase))
            then temp := temp + copy(pFrase,cont,(length(pfrase)-cont)+1)
            else temp := temp + copy(pFrase,cont,ColunasporLinha)+#10+#13;
            cont := cont + ColunasporLinha;
      end;

      result := temp;

end;

procedure apagalinhagrid(Grid:TstringGrid;posicao: integer);
var
cont:integer;
begin
     if posicao<>(grid.RowCount-1)
     Then Begin
               for cont:=posicao+1 to grid.RowCount-1 do
               begin
                    grid.Rows[cont-1]:=grid.Rows[cont];
               End;
     End;

     grid.RowCount:=grid.RowCount-1
end;

function ContaRegistros(ptabela,pcampo: string): string;
var querylocal:TIBQuery;
begin
    result:='';
    try
        querylocal:=TIBQuery.create(nil);
        querylocal.Database:=FDataModulo.IBDatabase;
    Except
        Exit;
    End;

    try

         With querylocal do

          Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Count('+ pcampo +') from '+ ptabela );
           Open;
           result:=fieldbyname('count').AsString;
       End;
    finally
        FreeAndNil(querylocal);
    End;
end;



procedure ValidaNumeros(var texto:tedit;var key: char;tipo: string);
begin

    If(UpperCase(tipo) = 'INT')
    Then Begin
        if not(key in ['0'..'9',#8,#13])
        Then key:=#0;
    End
    Else If (UpperCase(tipo) = 'FLOAT')
    Then Begin
              if( ( key=',' ) Or ( Key='.' ) )
              then Begin
                         if (pos(',',texto.text)>0)
                         Then Begin
                                   key:=#0;
                                   exit;
                         End;
              End;

              If not (key in['0'..'9',#8,',',#45])
              Then
                  If key='.'
                  Then Key:=','
                  Else Key:=#0;
    End;

End;

procedure BarraDataKeyUP(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (Key = VK_Space)
    then if (TEdit(Sender).Text = '__/__/____')
         then  TEdit(Sender).Text:=DateToStr(RetornaDataServidor);

end;


procedure FormKeyPress13(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Tform(sender).Perform(Wm_NextDlgCtl,0,0);
end;
{
procedure edtKeyPress_SoNumeros(Sender: TObject;
  var Key: Char);
begin
     if not (Key in ['0'..'9',#8])
     Then key:=#0;
end;
}

Function CalculaCentesimos(Pinicial:String;PFinal:String):int64;
var
cont,cont2:int64;
temp:string;

begin
     //15:15:30:30
     //HH *60 *60 *100
     //MM *60 *100
     //SS*100
     //CC

     pinicial:=trim(pinicial);


     cont:=StrToInt64(pinicial[1]+pinicial[2])*60*60*100;
     cont:=cont+StrToInt64(pinicial[4]+pinicial[5])*60*100;
     cont:=cont+StrToInt64(pinicial[7]+pinicial[8])*100;
     cont:=cont+strtoint64(copy(pinicial,10,length(pinicial)-9));

     pfinal:=trim(pfinal);
     cont2:=StrToInt64(pfinal[1]+pfinal[2])*60*60*100;
     cont2:=cont2+StrToInt64(pfinal[4]+pfinal[5])*60*100;
     cont2:=cont2+StrToInt64(pfinal[7]+pfinal[8])*100;
     temp:=copy(pfinal,10,length(pfinal)-9);
     cont2:=cont2+strtoint64(temp);



     Result:=Cont2-cont;



End;



Function Cronometro(Pinicial:string;PFinal:String;out presultado:String):boolean;
var
cont,quantdias:integer;
phorainicial,phorafinal:string;
CentesimosTotal:Int64;
hora,minuto,segundo,centesimos:int64;
Begin
     result:=False;
     presultado:='';

{
 Primeiro calculo quantos dias tem entre as datas

 exemplo

 de 29/01
 a 31/01
 Sao 2 dias (esquecendo as horas iniciais e finais)

 Phorainicial:=HORAINICIAL;

 Fazer um for de cont:=0 to  2(qtdedias)
 {

  Data:=DataInicial+incrementa dias (cont);//o primeiro vai ser zero

  if (cont=qtdedias) //esta no ultimo dia
  Then final:=horariofinalcorreto
  Else final:=23:59:59:99;

  calculacentesimos(inicial,final);

  inicial:=00:00:00:00;

 }

Try

 quantdias:=DaysBetween(strtodate(formatdatetime('dd/mm/yyyy',strtodatetime(pfinal))),strtodate(formatdatetime('dd/mm/yyyy',strtodatetime(pinicial))));
 phorainicial:=trim(copy(Pinicial,11,length(pinicial)-10));
 CentesimosTotal:=0;
 for cont:=0 to quantdias do
 begin

      if (cont=quantdias)//ultimo dia
      Then phorafinal:=trim(copy(pfinal,11,length(pfinal)-10))
      Else phorafinal:='23:59:59:99';

      CentesimosTotal:=CentesimosTotal+CalculaCentesimos(Phorainicial,phorafinal);

      if (cont<>quantdias)
      Then CentesimosTotal:=CentesimosTotal+1;//porque usei at� 99 cent�simos no calculo

      phorainicial:='00:00:00:0000';

 End;

 //calculando hora:minuto:segundo:centesimo


 segundo:=strtoint(floattostr(int(centesimostotal/100)));
 centesimos:=centesimostotal-(segundo*100);

 minuto:=strtoint(floattostr(int(segundo/60)));
 segundo:=segundo-(minuto*60);


 hora:=strtoint(floattostr(int(minuto/60)));
 minuto:=minuto-(hora*60);

 presultado:=inttostr(hora)+':'+
         CompletaPalavra_a_Esquerda(inttostr(minuto),2,'0')+':'+
         CompletaPalavra_a_Esquerda(inttostr(segundo),2,'0')+':'+
         CompletaPalavra_a_Esquerda(inttostr(centesimos),2,'0');

 result:=True;

Except
      on e:exception do
      Begin
           presultado:=e.message;
      End;
End;





End;


Function InverteDataSQL(Pdata:String):string;
Begin
     Try
        strtodate(pdata);
        result:=#39+formatdatetime('mm/dd/yyyy',strtodate(pdata))+#39;
     Except
           result:='';
     End;
End;

function cdate2julian(pvencimento:String):string;
var
  vencimento:TDate;
begin
      vencimento := StrToDate(pvencimento);
      result := completapalavra_a_esquerda(inttostr(DaysBetween(vencimento,EncodeDate(STRTOINT(formatdatetime('YYYY',vencimento))-1,12,31))),3,'0')+pvencimento[length(pvencimento)];
end;

Function AbrirImpressao: Boolean;
Begin
     Result:=true;
     FreltxtRDPRINT.ConfiguraImpressao;
     FreltxtRDPRINT.RDprint.Abrir;

     if (FreltxtRDPRINT.RDprint.Setup = false)
     then Result:=false;                      
end;

Function AbrirImpressaoPaisagem: Boolean;
Begin
     Result:=true;
     FreltxtRDPRINT.ConfiguraImpressaoPapelPaisagem;
     FreltxtRDPRINT.RDprint.Abrir;

     if (FreltxtRDPRINT.RDprint.Setup = false)
     then Result:=false;
end;


Procedure ImprimirSimples(Var PLinha: Integer; PColuna:Integer;PTexto : String);
Begin
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,Plinha);
     FreltxtRDPRINT.RDprint.Imp(Plinha,PColuna,PTexto);
end;

Procedure ImprimirNegrito(Var PLinha: Integer; PColuna:Integer;PTexto : String);
Begin
     FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,Plinha);
     FreltxtRDPRINT.RDprint.ImpF(Plinha,PColuna,PTexto,[Negrito]);
end;

Function FecharImpressao: Boolean;
Begin
     Result:=true;

     if (FreltxtRDPRINT.RDprint.Fechar = false)
     then Begin
          Result:=false;
          exit;
     end;

     FDataModulo.IBDatabase.Close;   // Descarrega Memoria
     FDataModulo.IBDatabase.Open;
end;

Procedure InicializaBarradeProgressoRelatorio(PRecordCount:Integer; PMensagem:String);
Begin
    FMostraBarraProgresso.Height:=85;
    FMostraBarraProgresso.HorzScrollBar.Visible:=false;
    FMostraBarraProgresso.Caption:='Processando... Aguarde...';
    FMostraBarraProgresso.Lbmensagem.Caption:=PMensagem;
    FMostraBarraProgresso.Lbmensagem2.Caption:='';
    FMostraBarraProgresso.ConfiguracoesIniciais(Precordcount,0);
    //FMostraBarraProgresso.btcancelar.visible:=True;
end;

Procedure IncrementaBarraProgressoRelatorio;
Begin
    FMostraBarraProgresso.IncrementaBarra1(1);
    FMostraBarraProgresso.show;
    Application.ProcessMessages;
end;

Procedure FechaBarraProgressoRelatorio;
Begin
    FMostraBarraProgresso.Close;
end;


procedure IniciaProgresso(pMensagem: string; pMax: Integer);
begin
  FMostraBarraProgresso.ConfiguracoesIniciais( 0, 0 );
  Application.ProcessMessages;
  if pMax <= 0 then
    pMax := 1;
  FMostraBarraProgresso.ConfiguracoesIniciais( pMax, 0 );
  FMostraBarraProgresso.Lbmensagem.Caption := pMensagem;
  Application.ProcessMessages;
end;

procedure IncrementaProgresso(pQtde: integer);
begin
  FMostraBarraProgresso.IncrementaBarra1( pQtde );
  Application.ProcessMessages;
end;


Function LinhaVertical:string;
Begin
    Result:='_____________________________________________________________________________________________';

end;


procedure controlChange_focus(sender: TObject;pform:tform);
begin

      
      with regFocus do
      begin
            if Assigned(Edit)
            then  Edit.Color:=CorAnteriorGlobal
            else if Assigned(Mask)
            then Mask.Color:=CorAnteriorGlobal
            else if Assigned(memo)
            then memo.Color:=CorAnteriorGlobal
            else if Assigned(ListBox)
            then ListBox.Color:=CorAnteriorGlobal
            else if Assigned(ComboBox)
            then ComboBox.Color:=CorAnteriorGlobal;
            
           if pform.ActiveControl is TEdit
           then begin
                     CorAnteriorGlobal:=TEdit(pform.ActiveControl).Color;
                     TEdit(pform.ActiveControl).Color:=$00BEE7F8;
                     edit:=TEdit(pform.ActiveControl);
                     mask:=nil;
                     memo:=nil;
                     ListBox:=nil;
                     ComboBox:=nil;
           end
           else if pform.ActiveControl is TMaskEdit
           then begin
                     CorAnteriorGlobal:=TMaskEdit(pform.ActiveControl).Color;
                     TMaskEdit(pform.ActiveControl).Color:=$00BEE7F8;
                     mask:=TMaskEdit(pform.ActiveControl);
                     Edit:=nil;
                     memo:=nil;
                     ListBox:=nil;
                     ComboBox:=nil;
           end
           else if pform.ActiveControl is TMemo
           then begin
                     CorAnteriorGlobal:=TMemo(pform.ActiveControl).Color;
                     TMemo(pform.ActiveControl).Color:=$00BEE7F8;
                     memo:=TMemo(pform.ActiveControl);
                     Edit:=nil;
                     Mask:=nil;
                     ListBox:=nil;
                     ComboBox:=nil;
           end
           else if pform.ActiveControl is TListBox
           then begin
                     CorAnteriorGlobal:=TListBox(pform.ActiveControl).Color;
                     TListBox(pform.ActiveControl).Color:=$00BEE7F8;
                     ListBox:=TListBox(pform.ActiveControl);
                     Edit:=nil;
                     Mask:=nil;
                     memo:=nil;
                     ComboBox:=nil;
           end
           else if pform.ActiveControl is TComboBox
           then begin
                     CorAnteriorGlobal:=TComboBox(pform.ActiveControl).Color;
                     TComboBox(pform.ActiveControl).Color:=$00BEE7F8;
                     ComboBox:=TComboBox(pform.ActiveControl);
                     Edit:=nil;
                     Mask:=nil;
                     memo:=nil;
                     ListBox:=nil;
           end
           else Begin
                     Edit:=nil;
                     Mask:=nil;
                     memo:=nil;
                     ListBox:=nil;
                     ComboBox:=nil;
           End;

      end;
end;

procedure MakeRounded(Control: TWinControl);
//  mauricio 23/07/2009 (internet)
//essa fun��o deixa o canto dos objetos arredondados...basta chamar passando o objeto
// Ex.: MakeRounded(Edtteste);
var
R: TRect;
Rgn: HRGN;
begin
with Control do
begin
      R := ClientRect;
      rgn := CreateRoundRectRgn(R.Left, R.Top, R.Right, R.Bottom, 20, 20);
      Perform(EM_GETRECT, 0, lParam(@r));
      InflateRect(r, - 3, - 3);
      Perform(EM_SETRECTNP, 0, lParam(@r));
      SetWindowRgn(Handle, rgn, True);
      Invalidate;
end;
end;

function DataDeCriacaoExe: String;
var
  ffd: TWin32FindData;
  dft: DWORD;
  lft: TFileTime;
  h: THandle;
begin
  result:='';
  h := Windows.FindFirstFile(PChar(Application.exename), ffd);
  try
        if (INVALID_HANDLE_VALUE <> h)
        then begin
                FileTimeToLocalFileTime(ffd.ftCreationTime, lft);
                FileTimeToDosDateTime(lft, LongRec(dft).Hi, LongRec(dft).Lo);
                Result := Datetostr(FileDateToDateTime(dft));
        end;
  finally
        Windows.FindClose(h);
  end;
end;


function DataDeModificacaoExe: String;
var
  ffd: TWin32FindData;
  dft: DWORD;
  lft: TFileTime;
  h: THandle;
begin
  result:='';
  h := Windows.FindFirstFile(PChar(Application.exename), ffd);
  try
        if (INVALID_HANDLE_VALUE <> h)
        then begin
                FileTimeToLocalFileTime(ffd.ftLastWriteTime, lft);
                FileTimeToDosDateTime(lft, LongRec(dft).Hi, LongRec(dft).Lo);
                Result := Datetostr(FileDateToDateTime(dft));
        end;
  finally
        Windows.FindClose(h);
  end;
end;

function Submit_CheckBox(PcheckBox:TCheckBox):string;
begin
    result:='';
    if (PcheckBox.Checked=true)
    then result:='S'
    else result:='N';
end;
{
Procedure AtribuiCor(Var RichEdit:TRichEdit; Cor:TColor; Posicao :Integer);
Var  sStart: word;
begin
    sStart := RichEdit.SelStart;
    RichEdit.SelStart := Posicao-1;
    RichEdit.SelLength := 1;
    RichEdit.SelAttributes.color := Cor; // set color
    RichEdit.SelAttributes.style := [fsBold]; // set attributes
    Application.Processmessages;
    RichEdit.SelStart := sStart;
    RichEdit.SelLength := 0;
end; }

function RetiraAspas(pparametro: string): string;
var
i:Integer;
begin
      result:='';
      for i:=1 to Length(pparametro)+1 do
      begin
           if pparametro[i]=#39//if for aspas troco por uma crase
           then Result:=Result+'`'
           else Result:=Result+pparametro[i];
      end;
end;



function RetiraAspasInicio_e_Fim(parametro: string): string;
var
i:Integer;
begin
      result:=parametro;

      if (parametro='')
      then exit;

      if ((parametro[1]='"') and (parametro[length(parametro)]='"'))or((parametro[1]='''') and (parametro[length(parametro)]=''''))
      then result:=copy(parametro,2,length(parametro)-2);
end;

Procedure RetiraAspasInicio_e_Fim(parametro: TStringList);
var
cont:Integer;
temp:string;
begin
      for cont:=0 to parametro.count-1 do
      Begin
           temp:=parametro[cont];
           temp:=RetiraAspasinicio_e_fim(temp);
           parametro[cont]:=temp;
      End;
end;

function InsereInicio_e_Fim(pCaracterInserir,pTexto:string):string;
begin
      Result := '';
      Result := pCaracterInserir+pTexto+pCaracterInserir;
end;

function DiferencaDias(DataVenc:TDateTime; DataAtual:TDateTime): Currency;
{Retorna a diferenca de dias entre duas datas}
Var Data: TDateTime;
    dia, mes, ano: Word;
begin
       Data := DataAtual - DataVenc;
       DecodeDate( Data, ano, mes, dia);
       Result := Data;
end;

function ArredondaFloat(x : Real): Real;
{Arredonda um n�mero float para convert�-lo em String}
Begin
      if x > 0
      Then begin
           if Frac(x) > 0.5 Then
                begin
                x := x + 1 - Frac(x);
                end
           else
           begin
                x := x - Frac(x);
           end
         end
      else begin
              x := x - Frac(x);
      end;
      result := x;
end;


procedure PintaParenteses(REdit : TRichEdit);
// PA : Parentese Abrinbdo
// PF : Parentese Fechando
Var Cont, ContP, Topo: Integer;
    Texto:String;
    VetPA: Array [1..20] of Integer; // Vetor Parentese Abrindo
    VetP1: Array [1..20] of Integer; // Vetor Posicao 1
    VetP2: Array [1..20] of Integer; // Vetor Posicao 2
begin
    // Limpa os Vetores
    For Cont:=1 to 20 do
    Begin
         VetPA[Cont]:=0;
         VetP1[Cont]:=0;
         VetP2[Cont]:=0;
    end;


    Texto:=REdit.Text;
    Cont:=1;
    ContP:=0;  // Controla o Vetor de Parentese de Posicoes
    Topo:=0;// Controla o o topo do VetPA
    While (Cont<=Length(Texto)) do
    Begin
         if (Texto[Cont] = '(')
         then Begin
                 Inc(Topo,1);
                 VetPA[Topo]:=Cont;
         end;
         if (Texto[Cont] = ')')
         then Begin
                 inc(ContP,1);
                 VetP1[ContP]:=VetPA[Topo];
                 VetP2[ContP]:=Cont;

                 // Desemplilha o VetPA
                 dec(Topo,1);
         end;
         inc(cont,1);
    end;

    // Dentro dos Vetores de posicoes est�o as posicoes que eu preciso pintar
    Cont:=1;
    While (VetP1[Cont]<>0) do
    Begin
          //ColorBox1.ItemIndex:=Cont;
          //Self.AtribuiCor(REdit, ColorBox1.Selected, VetP1[Cont]);
          //Self.AtribuiCor(REdit, ColorBox1.Selected, VetP2[Cont]);
          //inc(cont,1);
    end;

end;

function Bissexto( pAno : String ): Boolean;
begin
      Result := (strtoint(pAno) mod 4 = 0) and ((strtoint(pAno) mod 100 <> 0) or (strtoint(pAno) mod 400 = 0));
end;

function DiasdoMes( pAno,pMes : String ) : integer;
const
  DiasdoMes: array[1..12] of Integer = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
begin
  Result := DiasdoMes[strtoint(pMes)];
  If (strtoint(pMes) = 2) and Bissexto(pAno)
  Then Inc(Result);
end;

Procedure LimpaStringGrid(Var StrGrid : TStringGrid);
Var Linha, Coluna : Integer;
Begin
    For linha:=1 to StrGrid.RowCount-1 do
       For  Coluna:=0 to StrGrid.ColCount-1 do
           StrGrid.Cells[Coluna, Linha]:='';

end;


Function CarregaCombo(var pCombo:TComboBox; pLista:TStringList):boolean;
var
      cont : integer;
begin

      Result := false;
      try
            pCombo.Clear;
            for cont:=0 to pLista.Count-1 do
            begin
                  pCombo.Items.Add(plista[cont]);
            end;
            pCombo.ItemIndex:=-1;
            Result := true;
      EXCEPT
            on e: Exception do
            begin

                  MensagemErro('Erro ao carregar valores para o combo');

                  Exit;
            end;
      end;
End;
Function tira_caracter(valor:string; PCaracter : string):string;
var
str_valor:string;
cont:integer;

begin
     result:='';
     If (valor='')
     then exit;
     str_valor:='';
     for cont :=1 to length(valor)
     do begin
        if valor[cont]=PCaracter
        then
        else str_valor:=str_valor + valor[cont]
     end;
     result:=str_valor;

end;

Function Deleta_Linha_StringGrid(out StrGrig : TStringGrid; linha : Integer):Boolean;
Var I, J : Integer;
Begin
   if (Linha < StrGrig.FixedRows)  or (Linha >= StrGrig.RowCount) then
      Begin
         Exception.Create('Tentativa de deletar uma linha acima do intervalo');
      end;
   if StrGrig.RowCount = 2 then // Pra naum tirar i t�tulo
     Begin
          for J:=0 to StrGrig.ColCount-1 do
             StrGrig.Cells[J,StrGrig.RowCount-1]:='';
             exit;
     end;

   if (Linha < StrGrig.RowCount - 1) then
      Begin
        for I:=Linha+1 to StrGrig.RowCount-1 do
           Begin
              for J:=0 to StrGrig.ColCount-1 do
                StrGrig.Cells[J, I-1 ]:=StrGrig.Cells[J,I];
           end;
      end;
      StrGrig.RowCount:=StrGrig.RowCount-1;
end;

function AjustaCentralizaObjetonaTela(Referencia:TControl;
  ObjetoaAjustar: TControl): boolean;
var
  NovoTOP,NovoLeft:integer;
begin
      NovoTOP:= strtoint(currtostr(trunc((Referencia.Height/2)-(ObjetoaAjustar.Height/2))));
      NovoLeft := strtoint(currtostr(trunc((Referencia.Width/2)-(ObjetoaAjustar.Width/2))));
      ObjetoaAjustar.Top :=  NovoTOP;
      ObjetoaAjustar.Left := NovoLeft;
end;

procedure Sublinha_Label(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
{15/05/2010 Celio - removido do upedido}
begin
      TLabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure Negrita_Label(Sender: TObject);
{15/05/2010 Celio - removido do upedido}
begin
     TLabel(Sender).Font.Style:=[fsbold];
end;

//jonas 10/11/2010 08:27
//obs, somente com fontes que os caracteres ocupam o mesmo tamanho, EX: courier new
function alinhaTexto(str1,str2:string;espacamento:integer):string;
begin

  result := str1 + StringOfChar(' ',espacamento-Length(str1)) + str2;

end;

function get_campoTabela (campoSelect, campoWhere, nomeTabela, parametro:string; alias:string):string;
var
  query:TIBQuery;
begin

  result:='';

  if (campoSelect = '') then
    Exit;

  if (parametro = '') then
    Exit;

   try

       query:=TIBQuery.Create (nil);

       try

          with (query) do
          begin

            Database:=FDataModulo.IBDatabase;

            Close;
            sql.Clear;
            sql.Add ('select '+campoSelect);
            sql.Add ('from '+nomeTabela);
            if(Trim(campoWhere) = '') then
              SQL.Add(parametro)
            else
              if(Copy(parametro,0,1) = #39) then
                sql.Add('where '+campoWhere+'='+parametro)
              else
                sql.Add ('where '+campoWhere+'='+#39+parametro+#39);

            //InputBox('','',sql.Text);
            Open;
            First;

            if (alias <> '') then
              result := fieldbyname (alias).AsString
            else
              result := fieldbyname (campoSelect).AsString


          end;


       finally

          FreeAndNil (query);

       end;

   except
      on e:Exception do
      begin
       raise Exception.Create(e.Message);
       result:='';
      end
   end;


end;

function update_campoTabela(nomeTabela,campoUpdate,valor,campoWhere,parametro:string):Boolean;
var
  queryTemp:TIBQuery;
begin

  result := False;

  queryTemp := TIBQuery.Create(nil);

  try

    try

      queryTemp.Database := FDataModulo.IBDatabase;

      with (queryTemp) do
      begin

        active := False;
        SQL.Clear;
        sql.Add('update '+nomeTabela+' set '+campoUpdate+' = '+#39+valor+#39+' where '+campoWhere+' = '+#39+parametro+#39);

        ExecSQL;

      end;

    except

      Exit;

    end;

  finally

    FreeAndNil(queryTemp);

  end;

  result := True;

end;

function exec_sql(pSql:string): Boolean;
var
  queryLocal:TIBQuery;
begin

  try

    result:=False;
    queryLocal:=TIBQuery.Create (nil);

    try

      queryLocal.Database := FDataModulo.IBDatabase;

      queryLocal.SQL.Add (pSql);
      //InputBox('','',queryLocal.SQL.Text);
      queryLocal.ExecSQL;

    finally
      FreeAndNil (queryLocal);
    end;

  except
    on e:exception do
    begin
      MensagemErro(e.Message+'. Erro ao executar sql: '+pSql);
      raise;
      Exit;
    end;
  end;

  result := True;

end;


{jonas}
procedure chamaFormulario(sender:TForm; tipo:TFormClass;Owner:TComponent=nil);overload;
var
  form:TForm;
begin

  try

    sender.Hide;
    form := tipo.Create (Owner);
    form.ShowModal;
    sender.Show;

  finally

    FreeAndNil (form);

  end;

end;

procedure chamaFormulario(tipo:TFormClass;Owner:TComponent;tag:string);overload;
var
  form:TForm;
begin

  try

    form := tipo.Create (Owner);

    if (tag <> '') then
      form.Tag := StrToIntDef (tag, 0);

    form.ShowModal;

  finally

    FreeAndNil (form);

  end;

end;

{jonas 24/06/2011 17:55}
function embaralha_desembaralha(parametro:string):string;
var
  str:string;
  tam,i,j,ini:integer;
begin

  result := '';

  ini:=Trunc (Length(parametro)/2);
  tam:=Length(parametro);

  for i := ini downto(1) do
  begin

    str:=str+parametro[i];

  end;

  for i:=tam  downto(ini+1) do
  begin

    str:=str+parametro[i];

  end;

  result := str;

  {uso a mesma fun��o para embaralhar e desembaralhar. lokura mano}

end;

function verificaCapsLock():Boolean;
begin

  if odd (GetKeyState(VK_CAPITAL)) then
    result:=true
  else
    result:=false

end;

function get_valorF(pValor, pSimbolo: string): string;
begin

  if (pValor = '') then pValor:= '0';

  Result:= FormatCurr('0.0,0 '+pSimbolo,StrToCurr(pValor));

end;

Procedure primeiroUltimoDia(mes,ano:String; var dataIni,dataFin:string);
var
 MesAux,AnoAux:Integer;
Begin

  {primeiro dia}
  dataIni:='01/'+FormatCurr('00',StrToInt(mes))+'/'+ano;

  {ultimo dia}
  MesAux:=StrToInt(mes);
  AnoAux:=StrToInt(ano);

  {se ja estivesse em dezembro}
  MesAux:=MesAux+1;
  if MesAux = 13 then
  begin
    MesAux:=1;
    AnoAux:=AnoAux+1;
  end;

  dataFin:='01/'+IntToStr(MesAux)+'/'+IntToStr(AnoAux);
  dataFin:=DateToStr(StrToDate(dataFin)-1);

End;

function trunca(str: Currency): Currency;//trunca duas casas
begin
  result:= Trunc(str * 100)/100;
end;

function get_cfop (pCodigoProduto,pCliente,estadoEmpresa:string):string;
var
  queryTemp:TIBQuery;
  ClienteNoEstado:Boolean;
  revenda,beneficiado:string;
  prodIsento,prodSubstituicao,
  prodNaoTributado:string;
begin

  result := '';

  if (estadoEmpresa = get_campoTabela('ESTADOFATURA','codigo','TABCLIENTE',pCliente) ) then
    ClienteNoEstado := True
  else
    ClienteNoEstado := False;

  revenda     := get_campoTabela('REVENDA','codigo','TABCLIENTE',pCliente);
  beneficiado := get_campoTabela('CLIENTEBENEFICIADO','codigo','TABCLIENTE',pCliente);

  if (revenda = 'S') and (beneficiado = 'S') then
  begin

    MensagemErro ('Configura��o invalida para o cliente: '+pCliente+'. Beneficiado e revenda');
    Exit;

  end;


  ObjParametroGlobal.ValidaParametro ('UTILIZA CFOP POR PRODUTO?');

  if (ObjParametroGlobal.Get_Valor () = 'SIM') then
  begin

    queryTemp := TIBQuery.Create (nil);

    try

      queryTemp.Database := FDataModulo.IBDatabase;
      queryTemp.Active := false;
      queryTemp.SQL.Clear;

      queryTemp.SQL.Text:=

      'select p.cfop_consumidor,p.cfop_revenda,p.cfop_revenda_fe, '+
      'p.cfop_beneficiado,p.cfop_beneficiado_fe '+
      'from tabproduto p '+
      'where p.codigo = '+pCodigoProduto;

      queryTemp.Active := True;

      if (ClienteNoEstado) then
      begin

        if (revenda = 'S') then
          Result := queryTemp.fieldbyname ('cfop_revenda').AsString

        else if (beneficiado = 'S') then
          result := queryTemp.fieldbyname ('cfop_beneficiado').AsString

        else
          Result := queryTemp.fieldbyname ('cfop_consumidor').AsString;

      end else
      begin

        if (revenda = 'S') then
          Result := queryTemp.fieldbyname ('cfop_revenda_fe').AsString

        else if (beneficiado = 'S') then
          result := queryTemp.fieldbyname ('cfop_beneficiado_fe').AsString

        else
          Result := queryTemp.fieldbyname ('cfop_consumidor').AsString;

      end;

    finally

      FreeAndNil (queryTemp);

    end;

  end else
  begin

    prodIsento       := get_campoTabela('icms_isento_consumidorfinal','codigo','TABPRODUTO',pCodigoProduto);
    prodSubstituicao := get_campoTabela('icms_substituto','codigo','TABPRODUTO',pCodigoProduto);
    prodNaoTributado := get_campoTabela('icms_nao_tributado','codigo','TABPRODUTO',pCodigoProduto);

    if (ClienteNoEstado) then
    begin

      if (prodIsento = 'S') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA DE PRODUTO ISENTO NO ESTADO');
        Result := ObjParametroGlobal.Get_Valor ();

      end
      else if (prodSubstituicao = 'S') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA DE PRODUTOS DE SUBSTITUI��O');
        Result := ObjParametroGlobal.Get_Valor ();

      end
      else if (prodNaoTributado = 'S') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA DE PRODUTO NAO TRIBUTADO NO ESTADO');
        Result := ObjParametroGlobal.Get_Valor ();

      end
      else if (prodIsento = 'N') and (prodSubstituicao = 'N') and (prodNaoTributado = 'N') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA DE PRODUTOS TRIBUTADOS');
        Result := ObjParametroGlobal.Get_Valor ();

      end;

    end
    else
    begin

      if (prodIsento = 'S') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA DE PRODUTO ISENTO FORA DO ESTADO');
        Result := ObjParametroGlobal.Get_Valor ();

      end
      else if (prodSubstituicao = 'S') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA DE PRODUTOS DE SUBSTITUI��O FORA DO ESTADO');
        Result := ObjParametroGlobal.Get_Valor ();

      end
      else if (prodNaoTributado = 'S') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA DE PRODUTO NAO TRIBUTADO FORA DO ESTADO');
        Result := ObjParametroGlobal.Get_Valor ();

      end
      else if (prodIsento = 'N') and (prodSubstituicao = 'N') and (prodNaoTributado = 'N') then
      begin

        ObjParametroGlobal.ValidaParametro ('CODIGO CFOP PARA VENDA FORA DO ESTADO DE PRODUTOS TRIBUTADOS');
        Result := ObjParametroGlobal.Get_Valor ();

      end;

    end;

  end;

end;  

Function AllTrim( str : string ) : string;
begin
  Result := StringReplace(str,' ','',[rfReplaceAll]);
end;

function validaData (data:string):Boolean;
begin

  try
    StrToDate (data);
  except
    Result := False;
    Exit;
  end;

  Result:=True;

end;

function format_db (str:string):string;
begin
  result := virgulaparaponto (tira_ponto (str));          
end;

function format_si (str:string):string;
begin
    result := pontoparavirgula (tira_virgula(str))
end;

function getEstoqueSazional(pProduto:string;pData:string):string;
var
  queryTemp:TIBQuery;
begin

  queryTemp := TIBQuery.Create(nil);

  try

    pData := troca(pData,'.','/');
    queryTemp.Database:=FDataModulo.IBDatabase;

    queryTemp.Close;
    queryTemp.SQL.Clear;
    queryTemp.SQL.Text :=
    'select e.produto,SUM(e.quantidade) quantidade '+
    'from tabestoque e '+
    'where (e.produto = :produto) and (e.data <= :pdata) '+
    'group by e.produto';

    if (pData = '') then
      pData := FormatDateTime('dd/mm/yyyy',date);

    queryTemp.Params[0].AsString := pProduto;
    queryTemp.Params[1].AsString := pData;

    queryTemp.Open;

    if queryTemp.fieldByName('quantidade').AsString = '' then
      Result := '0'
    else
      result := queryTemp.fieldByName('quantidade').AsString;

    result := virgulaparaponto(result);

  finally
    FreeAndNil(queryTemp);
  end;

end;

function validaInscricaoEstadual( inscricao, uf : string) : Boolean;
var

  Contador  : ShortInt;
  Casos     : ShortInt;
  Digitos   : ShortInt;

  Tabela_1  : String;
  Tabela_2  : String;
  Tabela_3  : String;

  Base_1    : String;
  Base_2    : String;
  Base_3    : String;

  Valor_1   : ShortInt;

  Soma_1    : Integer;
  Soma_2    : Integer;

  Erro_1    : ShortInt;
  Erro_2    : ShortInt;
  Erro_3    : ShortInt;

  Posicao_1 : string;
  Posicao_2 : String;

  Tabela    : String;
  Rotina    : String;
  Modulo    : ShortInt;
  Peso      : String;

  Digito    : ShortInt;

  Resultado : String;
  Retorno   : Boolean;

Begin

  Try

    Tabela_1 := ' ';
    Tabela_2 := ' ';
    Tabela_3 := ' ';

    {                                                                               }                                                                                                                 {                                                                                                }
    {         Valores possiveis para os digitos (j)                                 }
    {                                                                               }
    { 0 a 9 = Somente o digito indicado.                                            }
    {     N = Numeros 0 1 2 3 4 5 6 7 8 ou 9                                        }
    {     A = Numeros 1 2 3 4 5 6 7 8 ou 9                                          }
    {     B = Numeros 0 3 5 7 ou 8                                                  }
    {     C = Numeros 4 ou 7                                                        }
    {     D = Numeros 3 ou 4                                                        }
    {     E = Numeros 0 ou 8                                                        }
    {     F = Numeros 0 1 ou 5                                                      }
    {     G = Numeros 1 7 8 ou 9                                                    }
    {     H = Numeros 0 1 2 ou 3                                                    }
    {     I = Numeros 0 1 2 3 ou 4                                                  }
    {     J = Numeros 0 ou 9                                                        }
    {     K = Numeros 1 2 3 ou 9                                                    }
    {                                                                               }
    { -------------------------------------------------------- }
    {                                                                               }
    {         Valores possiveis para as rotinas (d) e (g)                           }
    {                                                                               }
    { A a E = Somente a Letra indicada.                                             }
    {     0 = B e D                                                                 }
    {     1 = C e E                                                                 }
    {     2 = A e E                                                                 }
    {                                                                               }
    { -------------------------------------------------------- }
    {                                                                               }
    {                                  C T  F R M  P  R M  P                        }
    {                                  A A  A O O  E  O O  E                        }
    {                                  S M  T T D  S  T D  S                        }
    {                                                                               }
    {                                  a b  c d e  f  g h  i  jjjjjjjjjjjjjj        }
    {                                  0000000001111111111222222222233333333        }
    {                                  1234567890123456789012345678901234567        }

    IF UF = 'AC'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     01NNNNNNX.14.00';
    IF UF = 'AC'   Then Tabela_2 := '2.13.0.E.11.02.E.11.01. 01NNNNNNNNNXY.13.14';
    IF UF = 'AL'   Then Tabela_1 := '1.09.0.0.11.01. .  .  .     24BNNNNNX.14.00';
    IF UF = 'AP'   Then Tabela_1 := '1.09.0.1.11.01. .  .  .     03NNNNNNX.14.00';
    IF UF = 'AP'   Then Tabela_2 := '2.09.1.1.11.01. .  .  .     03NNNNNNX.14.00';
    IF UF = 'AP'   Then Tabela_3 := '3.09.0.E.11.01. .  .  .     03NNNNNNX.14.00';
    IF UF = 'AM'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     0CNNNNNNX.14.00';
    IF UF = 'BA'   Then Tabela_1 := '1.08.0.E.10.02.E.10.03.      NNNNNNYX.14.13';
    IF UF = 'BA'   Then Tabela_2 := '2.08.0.E.11.02.E.11.03.      NNNNNNYX.14.13';
    IF UF = 'CE'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     0NNNNNNNX.14.13';
    IF UF = 'DF'   Then Tabela_1 := '1.13.0.E.11.02.E.11.01. 07DNNNNNNNNXY.13.14';
    IF UF = 'ES'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     0ENNNNNNX.14.00';
    IF UF = 'GO'   Then Tabela_1 := '1.09.1.E.11.01. .  .  .     1FNNNNNNX.14.00';
    IF UF = 'GO'   Then Tabela_2 := '2.09.0.E.11.01. .  .  .     1FNNNNNNX.14.00';
    IF UF = 'MA'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     12NNNNNNX.14.00';
    IF UF = 'MT'   Then Tabela_1 := '1.11.0.E.11.01. .  .  .   NNNNNNNNNNX.14.00';
    IF UF = 'MS'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     28NNNNNNX.14.00';
    IF UF = 'MG'   Then Tabela_1 := '1.13.0.2.10.10.E.11.11. NNNNNNNNNNNXY.13.14';
    IF UF = 'PA'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     15NNNNNNX.14.00';
    IF UF = 'PB'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     16NNNNNNX.14.00';
    IF UF = 'PR'   Then Tabela_1 := '1.10.0.E.11.09.E.11.08.    NNNNNNNNXY.13.14';
    IF UF = 'PE'   Then Tabela_1 := '1.14.1.E.11.07. .  .  .18ANNNNNNNNNNX.14.00';
    IF UF = 'PI'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     19NNNNNNX.14.00';
    IF UF = 'RJ'   Then Tabela_1 := '1.08.0.E.11.08. .  .  .      GNNNNNNX.14.00';
    IF UF = 'RN'   Then Tabela_1 := '1.09.0.0.11.01. .  .  .     20HNNNNNX.14.00';
    IF UF = 'RS'   Then Tabela_1 := '1.10.0.E.11.01. .  .  .    INNNNNNNNX.14.00';
    IF UF = 'RO'   Then Tabela_1 := '1.09.1.E.11.04. .  .  .     ANNNNNNNX.14.00';
    IF UF = 'RO'   Then Tabela_2 := '2.14.0.E.11.01. .  .  .NNNNNNNNNNNNNX.14.00';
    IF UF = 'RR'   Then Tabela_1 := '1.09.0.D.09.05. .  .  .     24NNNNNNX.14.00';
    IF UF = 'SC'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     NNNNNNNNX.14.00';
    IF UF = 'SP'   Then Tabela_1 := '1.12.0.D.11.12.D.11.13.  NNNNNNNNXNNY.11.14';
    IF UF = 'SP'   Then Tabela_2 := '2.12.0.D.11.12. .  .  .  NNNNNNNNXNNN.11.00';
    IF UF = 'SE'   Then Tabela_1 := '1.09.0.E.11.01. .  .  .     NNNNNNNNX.14.00';
    IF UF = 'TO'   Then Tabela_1 := '1.11.0.E.11.06. .  .  .   29JKNNNNNNX.14.00';

    IF UF = 'CNPJ' Then Tabela_1 := '1.14.0.E.11.21.E.11.22.NNNNNNNNNNNNXY.13.14';
    IF UF = 'CPF'  Then Tabela_1 := '1.11.0.E.11.31.E.11.32.   NNNNNNNNNXY.13.14';

    { Deixa somente os numeros }

    Base_1 := '';

    For Contador := 1 TO 30 Do IF Pos( Copy( Inscricao, Contador, 1 ), '0123456789' ) <> 0 Then Base_1 := Base_1 + Copy( Inscricao, Contador, 1 );

    { Repete 3x - 1 para cada caso possivel  }

    Casos  := 0;

    Erro_1 := 0;
    Erro_2 := 0;
    Erro_3 := 0;

    While Casos < 3 Do Begin

      Casos := Casos + 1;

      IF Casos = 1 Then Tabela := Tabela_1;
      IF Casos = 2 Then Erro_1 := Erro_3  ;
      IF Casos = 2 Then Tabela := Tabela_2;
      IF Casos = 3 Then Erro_2 := Erro_3  ;
      IF Casos = 3 Then Tabela := Tabela_3;

      Erro_3 := 0 ;

      IF Copy( Tabela, 1, 1 ) <> ' ' Then Begin

        { Verifica o Tamanho }

        IF Length( Trim( Base_1 ) ) <> ( StrToInt( Copy( Tabela,  3,  2 ) ) ) Then Erro_3 := 1;

        IF Erro_3 = 0 Then Begin

          { Ajusta o Tamanho }

          Base_2 := Copy( '              ' + Base_1, Length( '              ' + Base_1 ) - 13, 14 );

          { Compara com valores possivel para cada uma da 14 posi��es }

          Contador := 0 ;

          While ( Contador < 14 ) AND ( Erro_3 = 0 ) Do Begin

            Contador := Contador + 1;

            Posicao_1 := Copy( Copy( Tabela, 24, 14 ), Contador, 1 );
            Posicao_2 := Copy( Base_2                , Contador, 1 );

            IF ( Posicao_1  = ' '        ) AND (      Posicao_2                 <> ' ' ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'N'        ) AND ( Pos( Posicao_2, '0123456789' )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'A'        ) AND ( Pos( Posicao_2, '123456789'  )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'B'        ) AND ( Pos( Posicao_2, '03578'      )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'C'        ) AND ( Pos( Posicao_2, '47'         )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'D'        ) AND ( Pos( Posicao_2, '34'         )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'E'        ) AND ( Pos( Posicao_2, '08'         )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'F'        ) AND ( Pos( Posicao_2, '015'        )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'G'        ) AND ( Pos( Posicao_2, '1789'       )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'H'        ) AND ( Pos( Posicao_2, '0123'       )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'I'        ) AND ( Pos( Posicao_2, '01234'      )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'J'        ) AND ( Pos( Posicao_2, '09'         )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1  = 'K'        ) AND ( Pos( Posicao_2, '1239'       )  =   0 ) Then Erro_3 := 1;
            IF ( Posicao_1 <>  Posicao_2 ) AND ( Pos( Posicao_1, '0123456789' )  >   0 ) Then Erro_3 := 1;

          End;

          { Calcula os Digitos }

          Rotina  := ' ';
          Digitos := 000;
          Digito  := 000;

          While ( Digitos < 2 ) AND ( Erro_3 = 0 ) Do Begin

            Digitos := Digitos + 1;

            { Carrega peso }

            Peso := Copy( Tabela, 5 + ( Digitos * 8 ), 2 );

            IF Peso <> '  ' Then Begin

              Rotina :=           Copy( Tabela, 0 + ( Digitos * 8 ), 1 )  ;
              Modulo := StrToInt( Copy( Tabela, 2 + ( Digitos * 8 ), 2 ) );

              IF Peso = '01' Then Peso := '06.05.04.03.02.09.08.07.06.05.04.03.02.00';
              IF Peso = '02' Then Peso := '05.04.03.02.09.08.07.06.05.04.03.02.00.00';
              IF Peso = '03' Then Peso := '06.05.04.03.02.09.08.07.06.05.04.03.00.02';
              IF Peso = '04' Then Peso := '00.00.00.00.00.00.00.00.06.05.04.03.02.00';
              IF Peso = '05' Then Peso := '00.00.00.00.00.01.02.03.04.05.06.07.08.00';
              IF Peso = '06' Then Peso := '00.00.00.09.08.00.00.07.06.05.04.03.02.00';
              IF Peso = '07' Then Peso := '05.04.03.02.01.09.08.07.06.05.04.03.02.00';
              IF Peso = '08' Then Peso := '08.07.06.05.04.03.02.07.06.05.04.03.02.00';
              IF Peso = '09' Then Peso := '07.06.05.04.03.02.07.06.05.04.03.02.00.00';
              IF Peso = '10' Then Peso := '00.01.02.01.01.02.01.02.01.02.01.02.00.00';
              IF Peso = '11' Then Peso := '00.03.02.11.10.09.08.07.06.05.04.03.02.00';
              IF Peso = '12' Then Peso := '00.00.01.03.04.05.06.07.08.10.00.00.00.00';
              IF Peso = '13' Then Peso := '00.00.03.02.10.09.08.07.06.05.04.03.02.00';
              IF Peso = '21' Then Peso := '05.04.03.02.09.08.07.06.05.04.03.02.00.00';
              IF Peso = '22' Then Peso := '06.05.04.03.02.09.08.07.06.05.04.03.02.00';
              IF Peso = '31' Then Peso := '00.00.00.10.09.08.07.06.05.04.03.02.00.00';
              IF Peso = '32' Then Peso := '00.00.00.11.10.09.08.07.06.05.04.03.02.00';

              { Multiplica }

              Base_3 := Copy( ( '0000000000000000' + Trim( Base_2 ) ), Length( ( '0000000000000000' + Trim( Base_2 ) ) ) - 13, 14 );

              Soma_1 := 0;
              Soma_2 := 0;

              For Contador := 1 To 14 Do Begin

                Valor_1 := ( StrToInt( Copy( Base_3, Contador, 01 ) ) * StrToInt( Copy( Peso, Contador * 3 - 2, 2 ) ) );

                Soma_1  := Soma_1 + Valor_1;

                IF Valor_1 > 9 Then Valor_1 := Valor_1 - 9;

                Soma_2  := Soma_2 + Valor_1;

              End;

              { Ajusta valor da soma }

              IF Pos( Rotina, 'A2'  ) > 0 Then Soma_1 := Soma_2;
              IF Pos( Rotina, 'B0'  ) > 0 Then Soma_1 := Soma_1 * 10;
              IF Pos( Rotina, 'C1'  ) > 0 Then Soma_1 := Soma_1 + ( 5 + 4 * StrToInt( Copy( Tabela, 6, 1 ) ) );

              { Calcula o Digito }

              IF Pos( Rotina, 'D0'  ) > 0 Then Digito := Soma_1 Mod Modulo;
              IF Pos( Rotina, 'E12' ) > 0 Then Digito := Modulo - ( Soma_1 Mod Modulo);

              IF Digito < 10 Then Resultado := IntToStr( Digito );
              IF Digito = 10 Then Resultado := '0';
              IF Digito = 11 Then Resultado := Copy( Tabela, 6, 1 );

              { Verifica o Digito }

              IF ( Copy( Base_2, StrToInt( Copy( Tabela, 36 + ( Digitos * 3 ), 2 ) ), 1 ) <> Resultado ) Then Erro_3 := 1;

            End;

          End;

        End;

      End;

    End;

    { Retorna o resultado da Verifica��o }

    Retorno := FALSE;

    IF ( Trim( Tabela_1 ) <> '' ) AND ( ERRO_1 = 0 ) Then Retorno := TRUE;
    IF ( Trim( Tabela_2 ) <> '' ) AND ( ERRO_2 = 0 ) Then Retorno := TRUE;
    IF ( Trim( Tabela_3 ) <> '' ) AND ( ERRO_3 = 0 ) Then Retorno := TRUE;

    IF Trim( Inscricao ) = 'ISENTO' Then Retorno := TRUE;

    Result := Retorno;

  Except
    Result := False;
  End;

end;

function getUF(pCodigoUF : integer) : string;
begin

  case pCodigoUF of

    11:Result := 'RO';
    12:Result := 'AC';
    13:result := 'AM';
    14:Result := 'RR';
    15:result := 'PA';
    16:result := 'AP';
    17:Result := 'TO';

    21:result := 'MA';
    22:result := 'PI';
    23:result := 'CE';
    24:result := 'RN';
    25:result := 'PB';
    26:result := 'PE';
    27:result := 'AL';
    28:result := 'SE';
    29:result := 'BA';

    31:Result := 'MG';
    32:Result := 'ES';
    33:Result := 'RJ';
    35:Result := 'SP';

    41:Result := 'PR';
    42:Result := 'SC';
    43:Result := 'RS';

    50:result := 'MS';
    51:result := 'MT';
    52:result := 'GO';
    53:result := 'DF';

  end;

end;

function getCodUF(pUF : string) : Integer;
begin

  if pUF = 'RO' then result := 11 else
  if pUF = 'AC' then result := 12 else
  if pUF = 'AM' then result := 13 else
  if pUF = 'RR' then Result := 14 else
  if pUF = 'PA' then result := 15 else
  if pUF = 'AP' then result := 16 else
  if pUF = 'TO' then result := 17 else

  if pUF = 'MA' then result := 21  else
  if pUF = 'PI' then result := 22  else
  if pUF = 'CE' then result := 23  else
  if pUF = 'RN' then result := 24  else
  if pUF = 'PB' then result := 25  else
  if pUF = 'PE' then result := 26  else
  if pUF = 'AL' then result := 27  else
  if pUF = 'SE' then result := 28  else
  if pUF = 'BA' then result := 29  else

  if pUF = 'MG' then Result := 31 else
  if pUF = 'ES' then Result := 32 else
  if pUF = 'RJ' then Result := 33 else
  if pUF = 'SP' then result := 35 else

  if pUF = 'PR' then Result := 41 else
  if pUF = 'SC' then Result := 42 else
  if pUF = 'RS' then result := 43 else

  if pUF = 'MS' then result := 50 else
  if pUF = 'MT' then result := 51 else
  if pUF = 'GO' then Result := 52 else
  if pUF = 'DF' then result := 53 else
  MensagemAviso('UF inv�lida. UF: '+pUF);


end;

function copyStrFront(chrBack,str : string) :string;
var
  i: integer;
begin
  result := '';
  if Pos(chrBack,str) = 0 then
    Exit;
  for i := 1  to Length(str)   do
  begin
    if str[i] <> chrBack then
      str[i] := ' '
    else
    begin
      str[i] := ' ';
      Result := Trim(str);
      Exit;
    end;
  end;

end;

function tira_pontoVirgula(info:string):string;
var
  i:integer;
begin
  for i :=0  to Length(info)  do
    if (info[i] <> ';') and (info[i] <> #0) then
      result := Result + info[i];
end;

procedure trocaWhile(var info:string; indIni:Integer; chrBreak:string; pTroca:string);
var
  infoAux:string;
  indIniAux:integer;
begin

  infoAux := '';
  indIniAux := indIni;
  while info[indIniAux] <> chrBreak do
  begin
    infoAux := infoAux + info[indIniAux];
    indIniAux := indIniAux + 1;
  end;

  Delete(info,indIni,Length(infoAux));
  Insert(pTroca,info,indIni);


end;

function criarDiretorio(nomeDir:string;var msg:string):integer;
begin

  { 0 = erro au criar diretorio
    1 = criado com sucesso
   -1 = diretorio ja existe
  }

  if nomeDir = '' then
  begin
    msg := msg + 'Diret�rio vazio'+#13;
    Result := 0;
    Exit;
  end;

  if not DirectoryExists(nomeDir) then
    if ForceDirectories(nomeDir) then
      result := 1
    else
      result := 0
  else
    Result := -1;

  if result = 0 then
    msg := msg +  'Erro ao criar diret�rio: '+nomeDir+#13
  else
  if Result = 1 then
    msg := msg +  'Diret�rio: '+nomeDir+' Criado com sucesso'+#13
  else
  if Result = -1 then
    msg := msg + 'Diret�rio: '+nomeDir+' ja existe'+#13;

end;

procedure limpaSoEdit(form:TForm);
var
  i:integer;
begin
  for i := 0  to form.ComponentCount-1  do
  begin
    if form.Components[i] is TEdit then
      TEdit(form.Components[i]).Text := ''
    else
    if form.Components[i] is TMaskEdit then
      TMaskEdit(form.Components[i]).Text := '';
  end;
end;

function default(valor:string;default:string='0'):string;
begin
  Result := IfThen( valor='',default,valor );
end;

{-}


function ForceForegroundWindow(hwnd: THandle): Boolean;
const
  SPI_GETFOREGROUNDLOCKTIMEOUT = $2000;
  SPI_SETFOREGROUNDLOCKTIMEOUT = $2001;
var
  ForegroundThreadID: DWORD;
  ThisThreadID: DWORD;
  timeout: DWORD;
begin

  if IsIconic(hwnd) then ShowWindow(hwnd, SW_RESTORE);

  if GetForegroundWindow = hwnd then Result := True
  else
  begin
    // Windows 98/2000 doesn't want to foreground a window when some other
    // window has keyboard focus

    if ((Win32Platform = VER_PLATFORM_WIN32_NT) and (Win32MajorVersion > 4)) or
      ((Win32Platform = VER_PLATFORM_WIN32_WINDOWS) and
      ((Win32MajorVersion > 4) or ((Win32MajorVersion = 4) and
      (Win32MinorVersion > 0)))) then
    begin
      // Code from Karl E. Peterson, www.mvps.org/vb/sample.htm
      // Converted to Delphi by Ray Lischner
      // Published in The Delphi Magazine 55, page 16

      Result := False;
      ForegroundThreadID := GetWindowThreadProcessID(GetForegroundWindow, nil);
      ThisThreadID := GetWindowThreadPRocessId(hwnd, nil);
      if AttachThreadInput(ThisThreadID, ForegroundThreadID, True) then
      begin
        BringWindowToTop(hwnd); // IE 5.5 related hack
        SetForegroundWindow(hwnd);
        AttachThreadInput(ThisThreadID, ForegroundThreadID, False);
        Result := (GetForegroundWindow = hwnd);
      end;
      if not Result then
      begin
        // Code by Daniel P. Stasinski
        SystemParametersInfo(SPI_GETFOREGROUNDLOCKTIMEOUT, 0, @timeout, 0);
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(0),
          SPIF_SENDCHANGE);
        BringWindowToTop(hwnd); // IE 5.5 related hack
        SetForegroundWindow(hWnd);
        SystemParametersInfo(SPI_SETFOREGROUNDLOCKTIMEOUT, 0, TObject(timeout), SPIF_SENDCHANGE);
      end;
    end
    else
    begin
      BringWindowToTop(hwnd); // IE 5.5 related hack
      SetForegroundWindow(hwnd);
    end;

    Result := (GetForegroundWindow = hwnd);
  end;

end; { ForceForegroundWindow }


Procedure BloqueiaTecladoeMouse;
begin
  //Screen.Cursor := crHourGlass;
  BlockInput(true);
end;

Procedure DesbloqueiaTecladoeMouse;
begin
  //Screen.Cursor := crDefault;
  BlockInput(false);
end;

procedure ForcaGravacaoSemBuffer(Arquivo:String);
Var
      Fhandle:Longint;
begin
      //18032011 - VERIFICANDO GRAVA��O EM DISCO
      Fhandle := CreateFile(PChar(Arquivo),GENERIC_WRITE,0,0,OPEN_ALWAYS,FILE_FLAG_NO_BUFFERING,0);
      FlushFileBuffers(Fhandle);
      CloseHandle(Fhandle);

end;

function RetornaListadeDatasEmUmIntervalo(DataInicio,DataFim:string;var ListaDatas:TStringList;intervalo:string):Boolean;
Var
  Dia,Mes,Ano:Word;
  DiaFim,MesFim,AnoFim:Word;
  Data:string;
  DataMesInicio:string;
  DataMesFim:string;
  MesCont,AnoCont:Integer;
  ProxDay,UltimaData:TDateTime;
  ProxDaySTR,UltimaDataSTR:string;
begin
    try
         if(intervalo='M')
         then begin
            //retorna em um intervalo de datas uma lista de datas
            //ex:10/01/2011 a 12/05/2011 = {10/01/2011,01/02/2011,01/03/2011,01/04/2011,12/05/2011}
            result:=False;
            DecodeDate(StrToDate(DataInicio), Ano, Mes, Dia);
            DecodeDate(StrToDate(DataFim), AnoFim, MesFim, DiaFim);
            DataMesInicio:=IntToStr(mes)+'/'+IntToStr(ano);
            DataMesFim:=IntToStr(mesFim)+'/'+IntToStr(anofim);
            MesCont:=Mes;
            AnoCont:=Ano;
            ListaDatas.Add(DataInicio);
            if(DataMesInicio=DataMesFim)
            then ListaDatas.Add(DataFim)
            else begin
                while(Data<>DataMesFim)do
                begin
                    inc(MesCont,1);
                    if(MesCont>12) then
                    begin
                        mescont:=1;
                        inc(AnoCont,1);
                    end;
                    Data:=IntToStr(mescont)+'/'+IntToStr(anocont);
                    //if(Data=DataMesFim)
                   // then ListaDatas.Add(DataFim)
                    //else
                    ListaDatas.Add('01/'+Data);
                end;
                ListaDatas.Add(DataFim)
            end;
            result:=True;
         end;
         if(intervalo='D')
         then begin
               ProxDay:=StrToDate(DataInicio);
               UltimaData:=StrToDate(DataFim);
               UltimaData:=IncDay(StrToDate(DataFim));
               ProxDaySTR:= DateToStr(ProxDay) ;
               UltimaDataSTR:= DateToStr(UltimaData) ;

               while ProxDaySTR <> UltimaDataSTR
               do begin
                      ListaDatas.Add(ProxDaySTR);
                      ProxDay:=IncDay(ProxDay,1);
                      ProxDaySTR:= DateToStr(ProxDay);
               end;
         end;
    finally

    end;





end;




function GravaLOG(pNomeArquivo,pTexto:string):Boolean;
var ArquivoLog:TextFile;
begin
     AssignFile(ArquivoLog, pNomeArquivo);
     if (FileExists(pNomeArquivo))
     then Append(ArquivoLog)
     else begin
               Rewrite(ArquivoLog);
               WriteLn(ArquivoLog, '----------------');
     end;
     WriteLn(ArquivoLog, FormatDateTime('dd/mm/yy hh:nn:ss:zzz', Now) + ' > ' + pTexto);
     CloseFile(ArquivoLog);
end;

Function RetornaQuantidadeDeDiasIntervaloDatas(dataini,datafim:TDateTime):integer;
Var
  Dia,Mes,Ano:Word;
  DiaFim,MesFim,AnoFim:Word;
  Data:string;
  DataMesInicio:string;
  DataMesFim:string;
  MesCont,AnoCont:Integer;
  ProxDay,UltimaData:TDateTime;
  ProxDaySTR,UltimaDataSTR:string;
  Cont:Integer;
begin
    try
           Cont:=0;
           ProxDay:=DataIni;
           UltimaData:=DataFim;
           UltimaData:=IncDay(DataFim);
           ProxDaySTR:=DateToStr(ProxDay);
           UltimaDataSTR :=DateToStr(UltimaData);
           while ProxDaySTR <> UltimaDataSTR
           do begin
                   Inc(cont,1);
                   ProxDay:=IncDay(ProxDay,1);
                   ProxDaySTR:=DateToStr(ProxDay)
           end;
           result:=Cont;
    finally

    end;
end;

function IsNumeric(parametro: String) : boolean;
var
  tipo:Real;
  retorno:Integer;
begin
      Result := false;
      parametro:=troca(parametro,',','.');

      Val(parametro,tipo,retorno);

      if(retorno=0)
      then Result:=true;
      {
      try
            StrToCurr(parametro);
      except
            On EConvertError do result := False;
      else
            result := True;
      end;
      }
end;


function Criptografa(parametro: string):string;
var
      Simbolos : array [0..4] of String;
      x: Integer;
begin
         Simbolos[1]:=
        'ABCDEFGHIJLMNOPQRSTUVXZYWK ~!@#$%^&*()';

         Simbolos[2]:=
        '�����׃����5�����Ѫ�������������������';

         Simbolos[3]:='abcdefghijlmnopqrstuvxzywk1234567890.';

         Simbolos[4]:='���������龶����-+��߸������յ��졫';

         for x := 1 to Length(Trim(parametro)) do
         begin
             if pos(copy(parametro,x,1),Simbolos[1])>0 then
                Result := Result+copy(Simbolos[2],
                              pos(copy(parametro,x,1),Simbolos[1]),1)

             else
             if pos(copy(parametro,x,1),Simbolos[2])>0 then
                Result := Result+copy(Simbolos[1],
                              pos(copy(parametro,x,1),Simbolos[2]),1)

             else
             if pos(copy(parametro,x,1),Simbolos[3])>0 then
                Result := Result+copy(Simbolos[4],
                              pos(copy(parametro,x,1),Simbolos[3]),1)

             else if pos(copy(parametro,x,1),Simbolos[4])>0 then
                Result := Result+copy(Simbolos[3],
                              pos(copy(parametro,x,1),Simbolos[4]),1);
         end;
end;

function TerminarProcessoWord(sFile: String): Boolean;
var
verSystem: TOSVersionInfo;
hdlSnap,hdlProcess: THandle;
bPath,bLoop: Bool;
peEntry: TProcessEntry32;
arrPid: Array [0..1023] of DWORD;
iC: DWord;
k,iCount: Integer;
arrModul: Array [0..299] of Char;
hdlModul: HMODULE;
begin
Result := False;
if ExtractFileName(sFile)=sFile then
bPath:=false
else
bPath:=true;
verSystem.dwOSVersionInfoSize:=SizeOf(TOSVersionInfo);
GetVersionEx(verSystem);
if verSystem.dwPlatformId=VER_PLATFORM_WIN32_WINDOWS then
begin
hdlSnap:=CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
peEntry.dwSize:=Sizeof(peEntry);
bLoop:=Process32First(hdlSnap,peEntry);
while integer(bLoop)<>0 do
begin
if bPath then
begin
if CompareText(peEntry.szExeFile,sFile) = 0 then
begin
TerminateProcess(OpenProcess(PROCESS_TERMINATE,false,peEntry.th32ProcessID), 0);
Result := True;
end;
end
else
begin
if CompareText(ExtractFileName(peEntry.szExeFile),sFile) = 0 then
begin
TerminateProcess(OpenProcess(PROCESS_TERMINATE,false,peEntry.th32ProcessID), 0);
Result := True;
end;
end;
bLoop := Process32Next(hdlSnap,peEntry);
end;
CloseHandle(hdlSnap);
end
else
if verSystem.dwPlatformId=VER_PLATFORM_WIN32_NT then
begin
EnumProcesses(@arrPid,SizeOf(arrPid),iC);
iCount := iC div SizeOf(DWORD);
for k := 0 to Pred(iCount) do
begin
hdlProcess:=OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ,false,arrPid [k]);
if (hdlProcess<>0) then
begin
EnumProcessModules(hdlProcess,@hdlModul,SizeOf(hdlModul),iC);
GetModuleFilenameEx(hdlProcess,hdlModul,arrModul,SizeOf(arrModul));
if bPath then
begin
if CompareText(arrModul,sFile) = 0 then
begin
TerminateProcess(OpenProcess(PROCESS_TERMINATE or PROCESS_QUERY_INFORMATION,False,arrPid [k]), 0);
Result := True;
end;
end
else
begin
if CompareText(ExtractFileName(arrModul),sFile) = 0 then
begin
TerminateProcess(OpenProcess(PROCESS_TERMINATE or PROCESS_QUERY_INFORMATION,False,arrPid [k]), 0);
Result := True;
end;
end;
CloseHandle(hdlProcess);
end;
end;
end;
end;


function PesquisaStringList(pTexto:string;plista:TStringList):Integer;
var
  cont:Integer;
begin
      Result:=-1;
      for cont:=0 to plista.Count-1 do
      begin
            if(plista[cont]=pTexto)
            then begin
                  Result := cont;
                  Exit;
            end;
      end;
end;

function retornaPalavrasAntesSimbolo(str: string;Simbolo:string): string;
var
  i,j:Integer;
  aux:string;
begin

   result:='';

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> Simbolo) and (i <= Length (str))) do
  begin

    aux:=aux+str[i];
    i:=i+1;

  end;
  //aux:=aux+Simbolo;
  i:=Length(aux);
  result:='';


  //while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;
  while ( (i >= 1 ) and (aux[i] = ' ')) do
    I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];


end;

function retornaPalavrasDepoisSimbolo(str: string;Simbolo:string): string;
var
  i,j:Integer;
  aux:string;
begin

   result:='';

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> Simbolo) and (i <= Length (str))) do
  begin
    i:=i+1;
  end;
  i:=i+2;
  while(i <= Length (str))do
  begin
      aux:=aux+str[i];
      i:=i+1;
  end;


  if(aux='')
  then Exit;

  i:=Length(aux);
  result:='';
 
  while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];


end;

function retornaPalavrasDepoisSimbolo2(str: string;Simbolo:string): string;
var
  i,j:Integer;
  aux:string;
begin

   result:='';

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> Simbolo) and (i <= Length (str))) do
  begin
    i:=i+1;
  end;
   i:=i+1;
  while(i <= Length (str))do
  begin
      aux:=aux+str[i];
      i:=i+1;
  end;


  if(aux='')
  then Exit;

  i:=Length(aux);
  result:='';
 
  while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];


end;

function tbKeyIsDown(const Key: integer): boolean;
begin
  Result := GetKeyState(Key) and 128 > 0;
end;

function RetornaDiasUteisEntreDuasDatas(dataini,datafin:string):integer;
var a,b,c:tdatetime;
  ct,s:integer;
begin
      if StrToDate(DataFin) < StrtoDate(DataIni) then
      begin
            Result := 0;
            exit;
      end;
      ct := 0;
      s := 1;
      a := strtodate(dataFin);
      b := strtodate(dataIni);
      if a > b then
      begin
            c := a;
            a := b;
            b := c;
            s := 1;
      end;
      a := a + 1;
      while (dayofweek(a)<>2) and (a <= b) do
      begin
            if dayofweek(a) in [2..6] then
            begin
                inc(ct);
            end;
            a := a + 1;
      end;
      ct := ct + round((5*int((b-a)/7)));
      a := a + (7*int((b-a)/7));
      while a <= b do
      begin
            if dayofweek(a) in [2..6] then
            begin
                inc(ct);
            end;
            a := a + 1;
      end;
      if ct < 0 then
      begin
            ct := 0;
      end;
      result := s*ct;
end;
function VerificaDuplicidadeCPFCGCCadastro(NOMETABELA,NOMECAMPOTABELA,CFPCNPJVERIFICAR,Codigo:String):Boolean;
var
  QryPesquisa_Query:TIBQuery;
begin
  Result:=False;
  try
    QryPesquisa_Query:=TIBQuery.Create(nil);
    QryPesquisa_Query.Database:=FDataModulo.IBDatabase;
  except
    Exit;
  end;
  try
    QryPesquisa_Query.Close;
    QryPesquisa_Query.SQL.Clear;
    QryPesquisa_Query.SQL.Text:=
    'SELECT '+NOMECAMPOTABELA+' '+
    'FROM '+NOMETABELA+' '+
    'WHERE CODIGO<>'+Codigo;

    QryPesquisa_Query.Open;

    while not QryPesquisa_Query.eof do
    begin
      if(QryPesquisa_Query.Fields[0].AsString=CFPCNPJVERIFICAR) then
      begin
        Result:=True;
        Exit;
      end;
      QryPesquisa_Query.next;
    end;

    QryPesquisa_Query.open;
  finally
    FreeAndNil(QryPesquisa_Query);
  end;
end;

{Retorna dia da semana}
function DiaSemana(Data:TDateTime): String;
var
  NoDia : Integer;
  DiaDaSemana : array [1..7] of String[13];
begin
{ Dias da Semana }
  DiaDasemana [1]:= 'Domingo';
  DiaDasemana [2]:= 'Segunda-feira';
  DiaDasemana [3]:= 'Ter�a-feira';
  DiaDasemana [4]:= 'Quarta-feira';
  DiaDasemana [5]:= 'Quinta-feira';
  DiaDasemana [6]:= 'Sexta-feira';
  DiaDasemana [7]:= 'S�bado';
  NoDia:=DayOfWeek(Data);
  DiaSemana:=DiaDasemana[NoDia];
end;

{Rodolfo}
{Faz um replace substituindo 1 aspas simples por 2 aspas simples com intuito de resolver o problema de buscas no banco de dados}
function StrReplaceRef(pString: string): String;
begin
  result := StringReplace(pString, '''', ''''''  ,  [rfReplaceAll, rfIgnoreCase]);
end;

//Rodolfo - Adiciona duplo clique em edits
procedure TDuploClique.AdicionaDuploClique(Sender : TObject);
var
  i : Integer;
  a : TComponent;
begin

  if(Sender is TComponent)then
    a := TComponent(Sender)
  else  Exit;

  for i:=0 to a.ComponentCount-1 do
  begin
    if(a.Components[i] is TEdit)then  //verifica se eh edit
    begin
      if(Assigned (TEdit(a.Components[i]).OnKeyDown))then //verifica se possui o evento onkeydown
        TEdit(a.Components[i]).OnDblClick := edtDblClick;
    end;

    AdicionaDuploClique(a.Components[i]); //Chamo novamente para verificar se o componente eh proprietario de outros componentes
  end;
end;

//Rodolfo - Evento do duplo clique
procedure TDuploClique.edtDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
  tkey : TkeyEvent;
begin
   Key := VK_F9;
   tkey := TEdit(Sender).OnKeyDown;
   tkey(Sender, Key, Shift);
end;

//Rodolfo
procedure Explode(str, separador: string; var StrList: TStringList);
var
  p: integer;
begin

  p := Pos(separador, str);
  while (p > 0) do
  begin
    StrList.Add(Copy(str, 1, p-1));
    Delete(str, 1, p + Length(separador) - 1);
    p := Pos(separador, str);
  end;

  if (trim(str) <> '') then
    StrList.Add(str);
end;

//Rodolfo
Procedure RetiraAspasInicio(parametro: TStringList);
var
  cont:Integer;
  temp:string;
begin
  for cont:=0 to parametro.count-1 do
  Begin
    temp := parametro[cont];
    temp := RetiraAspasinicio(temp);
    parametro[cont] := temp;
  End;
end;

//Rodolfo
function RetiraAspasInicio(str: string): string;
begin
  result := str;

  if (str = '')
  then exit;

  if (str[1] = '"') then
    result := copy(str, 2, length(str));
end;

//Coment
{ TobjSqlTxt }

procedure TobjSqlTxt.fechaArquivo;
begin
  system.CloseFile(arq);
end;

constructor TobjSqlTxt.Create(AOwner: TComponent;pathArq:string);
begin
  self.str := TStringList.Create;
  self.pathArq := pathArq;
  self.inicializa;
end;

destructor TobjSqlTxt.destroy;
begin
  FreeAndNil(self.str);
  Self.fechaArquivo;
  inherited;
end;

procedure TobjSqlTxt.inicializa;
begin

  if FileExists(pathArq) then
  begin
    AssignFile(self.arq,self.pathArq);
    Reset(self.arq);
  end;

end;

function TobjSqlTxt.readString(const sectionName: string;tiraPontoVirgula:Boolean): string;
var
  info: string;
begin
  Result := '';
  if not contemSection(sectionName) then
  begin
    MensagemErro('Configura��o: '+sectionName+' n�o encontrada.');
    Exit;
  end;

  Reset(self.arq);
  while not Eof(self.arq) do
  begin
    readln(arq,info);
    if (Pos(sectionName,info)) <> 0 then
    begin
      Readln(self.arq,info);
      while (Pos(';',info)) = 0 do
      begin
        Trim(info);
        Result := Result + info+' ';
        Readln(arq,info);
      end;
      if tiraPontoVirgula then
        Result := Result + tira_pontoVirgula(info)
      else
        Result := Result + info;
      Exit;
    end;
  end;
end;

function TobjSqlTxt.readStringSilencioso(const sectionName: string;
  tiraPontoVirgula: Boolean): string;
var
  info: string;
begin
  result := '';
  Reset(self.arq);
  while not Eof(self.arq) do
  begin
    readln(arq,info);
    if (Pos(sectionName,info)) <> 0 then
    begin
      Readln(self.arq,info);
      while (Pos(';',info)) = 0 do
      begin
        Trim(info);
        Result := Result + info+' ';
        Readln(arq,info);
      end;
      if tiraPontoVirgula then
        Result := Result + tira_pontoVirgula(info)
      else
        Result := Result + info;
      Exit;
    end;
  end;
end;

function TobjSqlTxt.contemSection(const value: string):Boolean;
begin
  str.Text := '';
  str.LoadFromFile(self.pathArq);
  Result := (Pos(value,str.Text) <> 0);
end;

procedure TobjSqlTxt.SetpathArq(const Value: string);
begin
  FpathArq := Value;
end;

procedure FixDBGridColumnsWidth(const Grid: TDBGrid);

var Tamanhos : array of Integer;
    nI, Tam  : Integer;
    DataSet  : TDataSet;

begin

  SetLength(Tamanhos, Grid.Columns.Count);

  for nI := 0 to Grid.Columns.Count - 1 do
    Tamanhos[nI] := Grid.Canvas.TextWidth(Grid.Columns[nI].Title.Caption);

  DataSet := Grid.DataSource.DataSet;
  DataSet.DisableControls;

  try
    while not DataSet.Eof do
    begin
      for nI := 0 to Grid.Columns.Count - 1 do
      begin
        Tam := Grid.Canvas.TextWidth(DataSet.Fields[nI].Text);
        if Tam > Tamanhos[nI] then
          Tamanhos[nI] := Tam;
      end;
      DataSet.Next;
    end;

    for nI := 0 to Grid.Columns.Count - 1 do
    begin
      Grid.Columns[nI].Width := Tamanhos[nI] + 5;

      {checka espa�o para colocar a imagem de ordena��o na coluna}
      if Grid.Columns.Items[nI].Width < (Grid.Canvas.TextWidth(Grid.Columns[nI].Title.Caption)+ 5) then
         Grid.Columns[nI].Width := Grid.Columns[nI].Width + 5;
    end;
    DataSet.First;
  finally
    DataSet.EnableControls;
  end;
end;


Procedure AdjustColumnWidths(DBGrid: TDBGrid);
var
  TotalColumnWidth, ColumnCount, GridClientWidth, Filler, i: Integer;
begin
  ColumnCount := DBGrid.Columns.Count;
  if ColumnCount = 0 then
    Exit;

  // compute total width used by grid columns and vertical lines if any
  TotalColumnWidth := 0;
  for i := 0 to ColumnCount-1 do
    TotalColumnWidth := TotalColumnWidth + DBGrid.Columns[i].Width;
  if dgColLines in DBGrid.Options then
    // include vertical lines in total (one per column)
    TotalColumnWidth := TotalColumnWidth + ColumnCount;

  // compute grid client width by excluding vertical scroll bar, grid indicator,
  // and grid border
  GridClientWidth := DBGrid.Width - GetSystemMetrics(SM_CXVSCROLL);
  if dgIndicator in DBGrid.Options then
  begin
    GridClientWidth := GridClientWidth - IndicatorWidth;
    if dgColLines in DBGrid.Options then
      Dec(GridClientWidth);
  end;
  if DBGrid.BorderStyle = bsSingle then
  begin
    if DBGrid.Ctl3D then // border is sunken (vertical border is 2 pixels wide)
      GridClientWidth := GridClientWidth - 4
    else // border is one-dimensional (vertical border is one pixel wide)
      GridClientWidth := GridClientWidth - 2;
  end;

  // adjust column widths
  if TotalColumnWidth < GridClientWidth then
  begin
    Filler := (GridClientWidth - TotalColumnWidth) div ColumnCount;
    for i := 0 to ColumnCount-1 do
      DBGrid.Columns[i].Width := DBGrid.Columns[i].Width + Filler;
  end
  else if TotalColumnWidth > GridClientWidth then
  begin
    Filler := (TotalColumnWidth - GridClientWidth) div ColumnCount;
    if (TotalColumnWidth - GridClientWidth) mod ColumnCount <> 0 then
      Inc(Filler);
    for i := 0 to ColumnCount-1 do
      DBGrid.Columns[i].Width := DBGrid.Columns[i].Width - Filler;
  end;
end;

function GetVencimentoCertificadoDigital : string;
var
  //querylocal: TIBQuery;
  Certificado : string;
  confNFe:TobjSqlTxt;
begin
  Result:= '';
  Certificado := '';

  {try
    querylocal := TIBQuery.Create(nil);
    querylocal.Database := FDataModulo.IBDatabase;
  except
    exit;
  end;}

  {try
    with querylocal do
    begin
      close;
      Sql.Clear;
      sql.add('Select certificado from tabempresa where codigo = 1');
      open;

      Certificado := fieldbyname('certificado').AsString;
    end;
  finally
    FreeAndNil(querylocal);
  end;}



  try

    confNFe := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'sqlNFE.txt');
    Certificado := confNFe.readString('CERTIFICADO',true);

    FComponentesNfe := TFComponentesNfe.Create(nil);

    if(Certificado <> '')then
    begin

      FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie := Certificado;
      try
        //Result := DateToStr(FComponentesNfe.ACBrNFe.Configuracoes.Certificados.GetCertificado.ValidToDate); celio0712
        Result := DateToStr(FComponentesNfe.ACBrNFe.SSL.CertDataVenc);
      except
      end;

    end;

  finally
    FreeAndNil(FComponentesNfe);
    FreeAndNil(confNFe);
  end;

end;

procedure VerificaVencimentoCertificadoDigital;
var
  expiraEm : Integer;
  vencimento : string;
begin
    if ObjParametroGlobal.ValidaParametro('VERIFICA VENCIMENTO CERTIFICADO DIGITAL', True) then
  begin
    if( ObjParametroGlobal.Get_Valor =  'SIM' ) then
    begin
      vencimento := GetVencimentoCertificadoDigital;
      if(vencimento <> '') then
      begin
        expiraEm := DaysBetween(Now, StrToDate(vencimento) );
        if(expiraEm <= 30 )then
        begin
          case expiraEm of
            0 : MensagemAviso('Seu certificado digital expira hoje.!');
            1 : MensagemAviso('Seu certificado digital expira em 1 dia.!');
          else MensagemAviso('Seu certificado digital expira em ' + CurrToStr(expiraEm) + ' dias..');
          end;
        end;
      end;
    end;
  end;
end;

Function ordinal(numero: string): string;
var
  conta : smallint;
  Texto : string;
const
  Unidades: array[0..9] of string = ('','Primeiro ', 'Segundo ', 'Terceiro  ', 'Quarto', 'Quinto ',
    'Sexto ', 'S�timo ', 'Oitavo ', 'Nono ');
  Dezenas: array[0..9] of string = ('','D�cimo ', 'Vig�simo ', 'Trig�simo ', 'Quadrag�simo ',
    'Quinquag�simo ', 'Sexag�simo ', 'Setuag�simo ', 'Octog�simo ', 'Novag�simo ');
  Centenas: array[0..9] of string = ('','Cent�simo ', 'Ducent�simo ', 'Tricent�simo ',
    'Quadringent�simo ','Quingent�simo ','Sexcent�simo ','Septingent�simo ','Octingent�simo ','Noningent�simo ');

begin
  // tira os possiveis espacos em branco.
  numero:=trim(numero);
  // cria um la�o para caminhar na string da direita para esquerda pegando os numeros.
  for conta:= length(numero) downto 1  do
  Begin
    // unidades
    if length(numero)-conta =0 then  texto := unidades[strtoint(copy(numero,conta,1))];
    // dezenas
    if length(numero)-conta =1 then  texto := dezenas[strtoint(copy(numero,conta,1))]+texto;
    // centenas
    if length(numero)-conta =2 then  texto := centenas[strtoint(copy(numero,conta,1))]+texto;
    // milhares
    if length(numero)-conta >2 then  texto := 'nao sabe contar tanto,ainda.'+texto;
  end;
  result:=texto;
end;

function DecToBin(Decimal:integer): string;
var
binario: string;
begin
  while Decimal >= 1  do
  begin
    binario := inttostr(Decimal mod 2) + binario;
    Decimal := (Decimal div 2);
  end;
  result:=binario;
end;

procedure TobjSqlTxt.writeString(const sectionName: string; novoValor:string);
var
  strArq:TStringList;
  aux:string;
  ind:integer;
begin

  strArq := TStringList.Create;

  try

    strArq.LoadFromFile(self.pathArq);
    aux := strArq.Text;

    ind := Pos(sectionName,aux);

    if ind > 0 then
    begin
      ind := ind + Length(sectionName) + 2; {para pular o #13 #10}
      trocaWhile(aux,ind,';',novoValor);
    end
    else
    begin
      MensagemAviso('Configura��o n�o encontrada: '+sectionName);
      Exit;
    end;

    strArq.Clear;
    strArq.Text := aux;

    self.fechaArquivo;
    if not DeleteFile(self.pathArq) then
    begin
      MensagemErro('N�o foi possivel salvar as altera��es');
      Exit;
    end;

    strArq.SaveToFile(self.pathArq);
    Reset(self.arq);

  finally
    FreeAndNil(strArq);
  end;

end;

procedure Espera( pMensagem: string; Fecha: Boolean = False );
begin
  Application.ProcessMessages;
  if Fecha then
  begin
    fEspera.Close;
    Exit;
  end;
  fEspera.Mensagem := '  ' + pMensagem + ' ';
  fEspera.Show;
  Application.ProcessMessages;
end;

function TamanhoArquivo( pArquivo:string ): Integer;
begin
  //retorna o tamanho do arquivo em bytes
  Result := 0;
  try
    with TFileStream.Create(pArquivo, fmOpenRead or fmShareExclusive) do
    try
      Result := Size;
    finally
      Free;
    end;
  except
    on e:Exception do
      MensagemErro(e.message);
  end;
end;

{M�todo retidado do ACBrValidador}
function ValidaIE(pIE, pUF: string): string;
Const
   c0_9 : AnsiString = '0-9' ;
   cPesos : array[1..13] of array[1..14] of Integer =
      ((0 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,2 ,3 ,4 ,5 ,6 ),
       (0 ,0 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,2 ,3 ,4 ,5 ),
       (2 ,0 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,2 ,3 ,4 ,5 ,6 ),
       (0 ,2 ,3 ,4 ,5 ,6 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ),
       (0 ,8 ,7 ,6 ,5 ,4 ,3 ,2 ,1 ,0 ,0 ,0 ,0 ,0 ),
       (0 ,2 ,3 ,4 ,5 ,6 ,7 ,0 ,0 ,8 ,9 ,0 ,0 ,0 ),
       (0 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,1 ,2 ,3 ,4 ,5 ),
       (0 ,2 ,3 ,4 ,5 ,6 ,7 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ),
       (0 ,0 ,2 ,3 ,4 ,5 ,6 ,7 ,2 ,3 ,4 ,5 ,6 ,7 ),
       (0 ,0 ,2 ,1 ,2 ,1 ,2 ,1 ,2 ,1 ,1 ,2 ,1 ,0 ),
       (0 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,10,11,2 ,3 ,0 ),
       (0 ,0 ,0 ,0 ,10,8 ,7 ,6 ,5 ,4 ,3 ,1 ,0 ,0 ),
       (0 ,2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,10,2 ,3 ,0, 0 ) ) ;

Var
   vDigitos : array of {$IFDEF FPC}Variant{$ELSE} AnsiString{$ENDIF} ;
   xROT, yROT :  AnsiString ;
   Tamanho, FatorF, FatorG, I, xMD, xTP, yMD, yTP, DV, DVX, DVY : Integer ;
   SOMA, SOMAq, nD, M : Integer ;
   OK : Boolean ;
   Passo, D : AnsiChar ;
   fsMsgErro, fsDigitoCalculado: String;
begin
  Result := '';
  if UpperCase( Trim(pIE) ) = 'ISENTO' then
     exit ;

  //pIE := RetornaSoNumeros( pIE );
  pIE := StringReplace(  pIE, '.', '', [rfReplaceAll] );
  pIE := StringReplace(  pIE, '-', '', [rfReplaceAll] );
  pIE := StringReplace(  pIE, '/', '', [rfReplaceAll] );

  try
    if pUF = '' then
    begin
       fsMsgErro := 'Informe a UF no campo Complemento' ;
       exit ;
    end ;

    fsMsgErro := ValidarUF( pUF ) ;
    if fsMsgErro <> '' then
       exit ;

    { Somente digitos ou letra P na primeira posicao }
    { P � usado pela Insc.Estadual de Produtor Rural de SP }
    if ( not IsNumeric( copy(pIE,2,length(pIE) ))) or
       ( not IsNumeric(pIE[1]) and (pIE[1] <> 'P')) then
    begin
       fsMsgErro := 'Caracteres inv�lidos na Inscri��o Estadual' ;
       exit
    end ;

    Tamanho := 0  ;
    xROT    := 'E';
    xMD     := 11 ;
    xTP     := 1  ;
    yROT    := '' ;
    yMD     := 0  ;
    yTP     := 0  ;
    FatorF  := 0  ;
    FatorG  := 0  ;

    SetLength( vDigitos, 13);
    vDigitos := VarArrayOf(['','','','','','','','','','','','','','']) ;

    if pUF = 'AC' then
    begin
       if Length(pIE) = 9 then
        begin
          Tamanho := 9 ;
          vDigitos := VarArrayOf(
             ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'1','0','','','','',''] ) ;
        end
       else
        if Length(pIE) = 13 then
        begin
          Tamanho := 13 ;
          xTP := 2   ;   yROT := 'E'   ;   yMD  := 11   ;   yTP  := 1 ;
          vDigitos := VarArrayOf(
            ['DVY','DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'1','0','']);
        end ;
    end ;

    if pUF = 'AL' then
    begin
       Tamanho := 9 ;
       xROT := 'BD' ;
       vDigitos   := VarArrayOf(
          ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'4','2','','','','',''] ) ;
    end ;

    if pUF = 'AP' then
    begin
       if Length(pIE) = 9 then
        begin
          Tamanho := 9 ;
          xROT := 'CE' ;
          vDigitos   := VarArrayOf(
             ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'3','0','','','','',''] ) ;

          if (pIE >= '030170010') and (pIE <= '030190229') then
             FatorF := 1
          else if pIE >= '030190230' then
             xROT := 'E' ;
        end ;
    end ;

    if pUF = 'AM' then
    begin
       Tamanho := 9 ;
       vDigitos  := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','','','',''] ) ;
    end ;

    if pUF = 'BA' then
    begin
      if Length(pIE) < 9 then
        pIE := CompletaPalavra_a_Esquerda(pIE,9,'0') ;
         //pIE := padR(pIE,9,'0') ;

      Tamanho := 9 ;
      xTP := 2   ;   yTP  := 3   ;   yROT := 'E' ;
      vDigitos := VarArrayOf(
         ['DVX','DVY',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','','','',''] ) ;

      if pos(pIE[2],'0123458') > 0 then
       begin
         xMD := 10   ;   yMD := 10 ;
       end
      else
       begin
         xMD := 11   ;   yMD := 11 ;
       end ;
    end ;

    if pUF = 'CE' then
    begin
       Tamanho := 9 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'0','','','','',''] ) ;
    end ;

    if pUF = 'DF' then
    begin
       Tamanho := 13 ;
       xTP := 2   ;   yROT := 'E'  ;   yMD  := 11   ;   yTP  := 1 ;
       vDigitos  := VarArrayOf(
          ['DVY','DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'7','0','']);
    end ;

    if pUF = 'ES' then
    begin
       Tamanho  := 9 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','','','',''] ) ;
    end ;

    if pUF = 'GO' then
    begin
       if Length(pIE) = 9 then
       begin
          Tamanho  := 9 ;
          vDigitos := VarArrayOf(
             [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'0,1,5','1','','','','',''] ) ;

          if (pIE >= '101031050') and (pIE <= '101199979') then
             FatorG := 1 ;
       end ;
    end ;

    if pUF = 'MA' then
    begin
       Tamanho  := 9 ;
       vDigitos := VarArrayOf(
          ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'2','1','','','','',''] ) ;
    end ;

    if pUF = 'MT' then
    begin
       if Length(pIE) = 9 then
         pIE := CompletaPalavra_a_Esquerda(pIE,11,'0') ;
          //pIE := padR(pIE,11,'0') ;

       Tamanho := 11 ;
       vDigitos := VarArrayOf(
          ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','',''] ) ;
    end ;

    if pUF = 'MS' then
    begin
       Tamanho  := 9 ;
       vDigitos := VarArrayOf(
          ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'8','2','','','','',''] ) ;
    end ;

    if pUF = 'MG' then
    begin
       Tamanho  := 13 ;
       xROT := 'AE'    ;   xMD := 10   ;   xTP := 10 ;
       yROT := 'E'     ;   yMD := 11   ;   yTP := 11 ;
       vDigitos := VarArrayOf(
         ['DVY','DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'']);
    end ;

    if pUF = 'PA' then
    begin
       Tamanho  := 9 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'5','1','','','','',''] ) ;
    end ;

    if pUF = 'PB' then
    begin
       Tamanho  := 9 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'6','1','','','','',''] ) ;
    end ;

    if pUF = 'PR' then
    begin
       Tamanho := 10 ;
       xTP := 9   ;   yROT := 'E'   ;   yMD := 11   ;   yTP := 8 ;
       vDigitos := VarArrayOf(
          [ 'DVY','DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','','',''] ) ;
    end ;

    if pUF = 'PE' then
    begin
       if Length(pIE) = 14 then
       begin
          Tamanho := 14;
          xTP := 7  ;   FatorF := 1;
          vDigitos := VarArrayOf(
            ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'1-9','8','1']);
       end
       else
        if Length(pIE) = 9 then
        begin
          Tamanho := 9;
          xTP  :=  9   ;  xMD := 11;
          yROT := 'E'  ;  yMD := 11  ;   yTP := 7;
          vDigitos := VarArrayOf(
          [ 'DVY','DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','','','',''] );
        end;
    end;

    if pUF = 'PI' then
    begin
       Tamanho  := 9 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'9','1','','','','',''] ) ;
    end ;

    if pUF = 'RJ' then
    begin
       Tamanho := 8 ;
       xTP := 8 ;
       vDigitos := VarArrayOf(
          ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'1,7,8,9','','','','','',''] ) ;
    end ;

    if pUF = 'RN' then
    begin
        if Length(pIE) = 9 then
        begin
           Tamanho := 9 ;
           xROT := 'BD' ;
           vDigitos := VarArrayOf(
              [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'0','2','','','','',''] ) ;
        end
       else
         if Length(pIE) = 10 then
         begin
           Tamanho := 10 ;
           xROT := 'BD' ;
           xTP := 11 ;
           vDigitos := VarArrayOf(
              [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'0','2','','','',''] ) ;
         end;

    end ;

    if pUF = 'RS' then
    begin
       Tamanho := 10 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'0-4','','','',''] ) ;
    end ;

    if pUF = 'RO' then
    begin
       FatorF := 1 ;
       if Length(pIE) = 9 then
       begin
          Tamanho := 9 ;
          xTP := 4 ;
          vDigitos := VarArrayOf(
            [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'1-9','','','','',''] ) ;
       end ;

       if Length(pIE) = 14 then
       begin
          Tamanho  := 14 ;
          vDigitos := VarArrayOf(
          ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9]);
       end ;
    end ;
  
    if pUF = 'RR' then
    begin
       Tamanho  := 9 ;
       xROT := 'D'   ;   xMD := 9   ;   xTP := 5 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'4','2','','','','',''] ) ;
    end ;

    if (pUF = 'SC') or (pUF = 'SE') then
    begin
       Tamanho  := 9 ;
       vDigitos := VarArrayOf(
          [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','','','',''] ) ;
    end;

    if pUF = 'SP' then
    begin
       xROT := 'D'   ;   xTP := 12 ;
       if pIE[1] = 'P' then
        begin
          Tamanho  := 13 ;
          vDigitos := VarArrayOf(
           [c0_9,c0_9,c0_9,'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'P','']);
        end
       else
        begin
          Tamanho  := 12 ;
          yROT := 'D'   ;   yMD := 11   ;   yTP := 13 ;
          vDigitos := VarArrayOf(
           ['DVY',c0_9,c0_9,'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','']);
        end ;
    end ;

    if pUF = 'TO' then
    begin
       if Length(pIE)=11 then
        begin
          Tamanho := 11 ;
          xTP := 6 ;
          vDigitos := VarArrayOf(
            ['DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'1,2,3,9','0,9','9','2','','','']);
        end
       else
        begin
  {        Tamanho := 10 ;
          vDigitos := VarArrayOf(
            [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'0-4','','','',''] ) ; }
          Tamanho := 9 ;
          vDigitos := VarArrayOf(
            [ 'DVX',c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,c0_9,'','','','',''] )
        end;
    end ;

    { Verificando se o tamanho Total est� correto }
    //if fsAjustarTamanho then
    //   pIE := padR( pIE, Tamanho, '0') ;

    OK := (Tamanho > 0) and (Length(pIE) = Tamanho) ;
    if not OK then
       fsMsgErro := 'Tamanho Inv�lido' ;

    { Verificando os digitos nas posicoes s�o permitidos }
    //pIE := padR(pIE,14) ;
    pIE := CompletaPalavra_a_Esquerda( pIE, 14, ' ' ) ;
    DVX := 0  ;
    DVY := 0  ;
    I   := 13 ;
    while OK and (I >= 0) do
    begin
       D := pIE[14-I] ;

       if vDigitos[I] = '' then
          OK := (D = ' ')

       else if (vDigitos[I] = 'DVX') or (vDigitos[I] = 'DVY') or
               (vDigitos[I] = c0_9) then
        begin
          //OK := CharIsNum( D ) ;
          OK := IsNumeric( D ) ;

          if vDigitos[I] = 'DVX' then
             DVX := StrToIntDef( D, 0 )
          else
             if vDigitos[I] = 'DVY' then
                DVY := StrToIntDef( D, 0 ) ;
        end

       else if pos(',',vDigitos[I]) > 0 then   { Ex: '2,5,7,8' Apenas os da lista}
          OK := (pos( D, vDigitos[I] ) > 0)

       else if pos('-',vDigitos[I]) > 0 then
          OK := ( (D >= copy(vDigitos[I],1,1)) and (D <= copy(vDigitos[I],3,1)) )

       else
          OK := ( D = vDigitos[I] ) ;

       if not OK then
          fsMsgErro := Format('D�gito %d deveria ser %s ',
           [14-I-(14-Tamanho), vDigitos[I]]) ;

       I := I - 1 ;
    end ;

    Passo := 'X' ;
    while OK and (xTP > 0) do
    begin
       SOMA := 0  ;
       SOMAq:= 0  ;
       I    := 14 ;

       while OK and (I > 0) do
       begin
          D := pIE[15-I] ;

          //if CharIsNum(D) then
          if IsNumeric(D) then
          begin
             nD := StrToIntDef(D,0) ;
             M  := nD * cPesos[xTP,I] ;
             SOMA := SOMA + M ;

             if pos('A',xROT) > 0 then
                SOMAq := SOMAq + Trunc(M / 10) ;
          end ;

          I := I - 1 ;
       end ;

       if pos('A',xROT) > 0 then
          SOMA := SOMA + SOMAq

       else if pos('B',xROT) > 0 then
          SOMA := SOMA * 10

       else if pos('C',xROT) > 0 then
          SOMA := SOMA + (5 + (4 * FatorF) ) ;

       { Calculando digito verificador }
       DV := Trunc(SOMA mod xMD) ;
       if pos('E',xROT) > 0 then
          DV := Trunc(xMD - DV) ;

       if DV = 10 then
          DV := FatorG   { Apenas GO modifica o FatorG para diferente de 0 }
       else if DV = 11 then
          DV := FatorF ;

       if Passo = 'X' then
          OK := (DVX = DV)
       else
          OK := (DVY = DV) ;

       fsDigitoCalculado := IntToStr(DV) ;
       if not OK then
       begin
          fsMsgErro := 'D�gito verificador inv�lido.' ;

          //if fsExibeDigitoCorreto then
          //   fsMsgErro := fsMsgErro + '.. Calculado: '+fsDigitoCalculado ;
       end ;

       if PASSO = 'X' then
        begin
          PASSO := 'Y'  ;
          xROT  := yROT ;
          xMD   := yMD  ;
          xTP   := yTP  ;
        end
       else
          break ;
    end ;

    pIE := Trim( pIE ) ;
  finally
    if (fsMsgErro <> '') then
    begin
      fsMsgErro := 'Insc.Estadual inv�lida para '+pUF +' '+ fsMsgErro ;
      Result := fsMsgErro;
    end;
  end;

end;

function ValidarUF(UF: AnsiString):string ;
begin
 if pos( ','+UF+',', ',AC,AL,AP,AM,BA,CE,DF,ES,GO,MA,MT,MS,MG,PA,PB,PR,PE,PI,'+
                     'RJ,RN,RS,RO,RR,SC,SP,SE,TO,EX,') = 0 then
    result := 'UF inv�lido: '+UF ;
end;

function VerificaBarraFinalDiretorio( pvalor:string ):string;
begin
  if ( pvalor[ length( pvalor ) ] <> '\' ) then
    pvalor := pvalor + '\';
  result := pvalor;
end;

//retorna os valores dos campos de um arquivo xml de nfe
function RetornaValorCampos(Parquivo,PCampo: String): string;
var
  wnProt: TLeitor;
begin

  result:='';

  wnProt := TLeitor.Create;


  Try

    if (Pcampo='') Then
    Begin
      if (inputquery('Acessar campos','Digite o nome do campo',pcampo)=false) then
         exit;
    End;

    wnProt.CarregarArquivo(Parquivo);
    wnProt.Grupo := wnProt.Arquivo;

    result:=wnProt.rCampo(tcStr,pcampo);

  Finally
    wnProt.Free;
  End;

end;

procedure abreTXTnotepad(parquivo:String);
begin
  if not FileExists(parquivo) then
    exit;
  winExec(pansichar('Notepad.exe '+parquivo), sw_shownormal);
end;


procedure get_impostoPIS(valorF: string; var bcPIS, vPIS, pPIS: real;cstPIS:string);
begin

  try

    bcPIS := StrToCurr  (valorF);

    if bcPIS > 0 then
    begin

      if (CSTPIS = '4') or (CSTPIS = '6') then
        pPIS := 0
      else
        pPIS :=  StrToCurr  (ObjEmpresaGlobal.get_pis());

      vPIS := (bcPIS * pPIS / 100);

    end
    else
    begin
      pPIS := 0;
      vPIS := 0;
    end;

  except
    MensagemAviso ('N�o foi possivel cancular imposto PIS');
  end;

end;

procedure get_impostoCOFINS(valorF: string; var bcCOFINS, vCOFINS, pCOFINS: Real;cstCOFINS:string);
begin

  try

    bcCOFINS :=  StrToCurr  (valorF);

    if bcCOFINS > 0 then
    begin

      if (CSTCOFINS = '4') or (CSTCOFINS = '6') then
        pCOFINS := 0
      else
        pCOFINS := StrToCurr  (ObjEmpresaGlobal.Get_Cofins());

      vCOFINS       :=  (bcCOFINS * pCOFINS / 100);

    end
    else
    begin
      pCOFINS := 0;
      vCOFINS := 0;
    end;

  except

      MensagemAviso ('N�o foi possivel cancular imposto COFINS');

  end;

end;

{******************************************************************************}
{http://www.agnaldocarmo.com.br/home/comando-milagroso-para-reducao-de-memoria-delphi/}
{Reduz a memoria desalocada mas nao liberada - garbage collection????}
{******************************************************************************}
procedure TrimAppMemorySize;
var
MainHandle : THandle;
begin
  try
    MainHandle := OpenProcess(PROCESS_ALL_ACCESS, false, GetCurrentProcessID) ;
    SetProcessWorkingSetSize(MainHandle, $FFFFFFFF, $FFFFFFFF) ;
    CloseHandle(MainHandle) ;
  except
  end;
  Application.ProcessMessages;
end;

{
  Substitui caracteres especiais por equivalentes ASCII
}
Function ReplaceNonAscii(const s: String) : String;
var i, pos: Integer;
const undesiredchars : String = '/������������������������������������������������������������';
const replaces : String = '  AAAAAAACEEEEIIIIDNOOOOOxOUUUbBaaaaaaaceeeeiiiionooooo ouuuby';
Begin
  SetLength(Result, Length(s));
  for i := 1 to Length(s) do
    begin
      pos := ord(s[i]);
      if (s[i] in [#32, #48..#57, #65..#90, #97..#122]) then
        Result[i] := s[i]
      else
        begin
          pos := AnsiPos(s[i], undesiredchars);
          Result[i] := replaces[pos + 1];
        end;
    end;

end;
end.

