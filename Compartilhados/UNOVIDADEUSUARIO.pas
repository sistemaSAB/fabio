unit UNOVIDADEUSUARIO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjNOVIDADEUSUARIO,
  jpeg;

type
  TFNOVIDADEUSUARIO = class(TForm)
    Guia: TTabbedNotebook;
    Label1: TLabel;
    Edit1: TEdit;
    Btnovo: TSpeedButton;
    btgravar: TSpeedButton;
    btalterar: TSpeedButton;
    btcancelar: TSpeedButton;
    btexcluir: TSpeedButton;
    btpesquisar: TSpeedButton;
    btrelatorios: TSpeedButton;
    btsair: TSpeedButton;
    ImageRodapeInterno: TImage;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbNovidade: TLabel;
    EdtNovidade: TEdit;
    LbNomeNovidade: TLabel;
    procedure edtNovidadeKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtNovidadeExit(Sender: TObject);
    LbUsuario: TLabel;
    EdtUsuario: TEdit;
    LbNomeUsuario: TLabel;
    procedure edtUsuarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtUsuarioExit(Sender: TObject);
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure BtnovoClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FNOVIDADEUSUARIO: TFNOVIDADEUSUARIO;
  ObjNOVIDADEUSUARIO:TObjNOVIDADEUSUARIO;

implementation

uses Uessenciallocal, UessencialGlobal, Upesquisa;

{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFNOVIDADEUSUARIO.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjNOVIDADEUSUARIO do
    Begin
        Submit_Codigo(edtCodigo.text);
        Novidade.Submit_codigo(edtNovidade.text);
        Usuario.Submit_codigo(edtUsuario.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFNOVIDADEUSUARIO.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjNOVIDADEUSUARIO do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtNovidade.text:=Novidade.Get_codigo;
        EdtUsuario.text:=Usuario.Get_codigo;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFNOVIDADEUSUARIO.TabelaParaControles: Boolean;
begin
     If (ObjNOVIDADEUSUARIO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFNOVIDADEUSUARIO.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        ObjNOVIDADEUSUARIO:=TObjNOVIDADEUSUARIO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     PegaFigura(ImageRodapeInterno,'rodape_interno.jpg');
     PegaFiguraBotao(btnovo,'novo.bmp');
     PegaFiguraBotao(btpesquisar,'pesquisar.bmp');
     PegaFiguraBotao(btgravar,'salvar.bmp');
     PegaFiguraBotao(btrelatorios,'relatorios.bmp');
     PegaFiguraBotao(btalterar,'alterar.bmp');
     PegaFiguraBotao(btcancelar,'cancelar.bmp');
     PegaFiguraBotao(btexcluir,'excluir.bmp');
     PegaFiguraBotao(btsair,'sair.bmp');

     Coloca_Atalho_Botoes(BtNovo, 'N');
     Coloca_Atalho_Botoes(BtGravar, 'S');
     Coloca_Atalho_Botoes(BtAlterar, 'A');
     Coloca_Atalho_Botoes(BtCancelar, 'C');
     Coloca_Atalho_Botoes(BtExcluir, 'E');
     Coloca_Atalho_Botoes(BtPesquisar, 'P');
     Coloca_Atalho_Botoes(BtRelatorios, 'R');
     Coloca_Atalho_Botoes(BtSair, 'I');

end;

procedure TFNOVIDADEUSUARIO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjNOVIDADEUSUARIO=Nil)
     Then exit;

If (ObjNOVIDADEUSUARIO.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

ObjNOVIDADEUSUARIO.free;
end;

procedure TFNOVIDADEUSUARIO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFNOVIDADEUSUARIO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFNOVIDADEUSUARIO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;

procedure TFNOVIDADEUSUARIO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjNOVIDADEUSUARIO.Get_novocodigo;
     edtcodigo.enabled:=False;


     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjNOVIDADEUSUARIO.status:=dsInsert;
     Guia.pageindex:=0;
     EdtPrimeiro.setfocus;

end;

procedure TFNOVIDADEUSUARIO.btgravarClick(Sender: TObject);
begin

     If ObjNOVIDADEUSUARIO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjNOVIDADEUSUARIO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjNOVIDADEUSUARIO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFNOVIDADEUSUARIO.btalterarClick(Sender: TObject);
begin
    If (ObjNOVIDADEUSUARIO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjNOVIDADEUSUARIO.Status:=dsEdit;
                guia.pageindex:=0;
                edtPrimeiro.setfocus;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFNOVIDADEUSUARIO.btcancelarClick(Sender: TObject);
begin
     ObjNOVIDADEUSUARIO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFNOVIDADEUSUARIO.btexcluirClick(Sender: TObject);
begin
     If (ObjNOVIDADEUSUARIO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjNOVIDADEUSUARIO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjNOVIDADEUSUARIO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFNOVIDADEUSUARIO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjNOVIDADEUSUARIO.Get_pesquisa,ObjNOVIDADEUSUARIO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjNOVIDADEUSUARIO.status<>dsinactive
                                  then exit;

                                  If (ObjNOVIDADEUSUARIO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjNOVIDADEUSUARIO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFNOVIDADEUSUARIO.btrelatoriosClick(Sender: TObject);
begin
ObjNOVIDADEUSUARIO.Imprime;
end;

procedure TFNOVIDADEUSUARIO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFNOVIDADEUSUARIO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

end.

