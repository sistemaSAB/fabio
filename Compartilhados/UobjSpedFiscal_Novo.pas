unit UObjSpedFiscal_Novo;

interface

uses Classes,StdCtrls,ComCtrls,ACBrEFDBlocos,IBQuery, db,
     Dialogs, ibdatabase,ACBrSpedFiscal,ACBrEFDBloco_0_Class,
     ACBrEFDBloco_C_Class,ACBrEFDBloco_D_Class,ACBrEFDBloco_E_Class,
     ACBrEFDBloco_G_Class,ACBrEFDBloco_H_Class,ACBrEFDBloco_1_Class,
     ACBrEFDBloco_9_Class,IniFiles,Useg,StrUtils;

type
  TFonteDados = Class(TComponent)
  private
    campoErro:string;
    FQuery :TIBQuery;
    FSQL: String;
    FRegistro: String;
    FDatabase: TIBDatabase;
    FDataFIN: TDateTime;
    FDataINI: TDateTime;
    Fstatus: TStatusBar;
    FgeraRegistroH: Boolean;
    FDataFinEstoque: TDateTime;
    FDataIniEstoque: TDateTime;
    FindPerfil: string;
    procedure SetSQL(const Value: String);
    function AbreQuery:Boolean;Virtual;
    procedure SetRegistro(const Value: String);
    procedure SetDatabase(const Value: TIBDatabase);
    procedure SetDataFIN(const Value: TDateTime);
    procedure SetDataINI(const Value: TDateTime);
    procedure GetFieldSoNumeros(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldDesencripta(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldDesencriptaSoNumeros(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldTrim(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure Setstatus(const Value: TStatusBar);
    procedure SetgeraRegistroH(const Value: Boolean);
    procedure SetDataFinEstoque(const Value: TDateTime);
    procedure SetDataIniEstoque(const Value: TDateTime);
    function teveErros: Boolean;
    procedure SetindPerfil(const Value: string);
  protected
    function ValidaBrancosNulos:Boolean;Virtual;
    function ValidaRegistros:Boolean; Virtual;Abstract;
    procedure SetParametros;Virtual;Abstract;
    procedure SetCampos;Virtual;
  public
    strErros: TStringList;
    strAdvertencia: TStringList;
    strItem: TStringList;
    strServico: TStringList;
    strUnidade: TStringList;
    strNatOp: TStringList;
    strCodPart: TStringList;
    valor_total_debitos:Currency;
    VL_TOT_CREDITOS_E110:Currency;
    software:string;
    property SQL :String read FSQL write SetSQL;
    property Registro :String read FRegistro write SetRegistro;
    property Database :TIBDatabase read FDatabase write SetDatabase;
    property DataINI :TDateTime read FDataINI write SetDataINI;
    property DataFIN :TDateTime read FDataFIN write SetDataFIN;
    property DataIniEstoque :TDateTime read FDataIniEstoque write SetDataIniEstoque;
    property DataFinEstoque :TDateTime read FDataFinEstoque write SetDataFinEstoque;
    property status :TStatusBar read Fstatus write Setstatus;
    property indPerfil :string read FindPerfil write SetindPerfil;
    property geraRegistroH: Boolean read FgeraRegistroH write SetgeraRegistroH;
    procedure preenche;virtual;abstract;
    procedure addStr(str: TStringList; valor: string);
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
  end;

type
  TobjSqlSped = class(TComponent)
  private
    arq: TextFile;
    FpathArq: string;
    str: TStringList;
    procedure inicializa;
    procedure SetpathArq(const Value: string);
    function contemSection(const value: string):Boolean;
  protected
    property pathArq: string read FpathArq write SetpathArq;
  public
    constructor Create(AOwner: TComponent;pathArq:string = '');
    destructor destroy;override;
    function readString(const sectionName:string):string;
    procedure fechaArquivo();
end;


type
  TFonteDadosParamData = Class(TFonteDados)
  protected
    procedure SetParametros;override;
  public
  end;

type
  TFonteDadosBloco0 = Class(TFonteDados)
  protected
      procedure SetParametros;override;
  public
    FAcbrBloco0: TBloco_0;
end;

type
  TFonteDados0000 = Class(TFonteDadosBloco0)
  private
    FCodFin: string;
    FIndAtiv: string;
    function getCodFin: TACBrCodFinalidade;
    function getCodVer   :TACBrVersaoLeiaute;
    function getIndPerfil:TACBrPerfil;
    function getIindAtiv :TACBrAtividade;
    procedure SetCodFin(const Value: string);
    procedure SetIndAtiv(const Value: string);
  protected
    property CodFin: string read FCodFin write SetCodFin;
    property IndAtiv :string read FIndAtiv write SetIndAtiv;
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
  end;

type
  TFonteDados0005 = Class(TFonteDadosBloco0)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
public
end;

type
  TFonteDados0100= Class(TFonteDadosBloco0)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
public
end;

type
  TFonteDados0150 = Class(TFonteDadosBloco0)
  private
    pCodigoParticipante: string;
  protected
      function ValidaRegistros:Boolean; override;
      procedure SetParametros;override;
      procedure SetCampos;override;
      procedure preenche;override;
  public
end;

type
  TFonteDados0150Cliente = Class(TFonteDados0150)
  private
  protected
      function ValidaRegistros:Boolean; override;
      procedure SetParametros;override;
      procedure SetCampos;override;
      procedure preenche;override;
  public
end;

type
  TFonteDados0150Fornecedor = Class(TFonteDados0150)
  private
  protected
      function ValidaRegistros:Boolean; override;
      procedure SetParametros;override;
      procedure SetCampos;override;
      procedure preenche;override;
  public
end;

type
  TFonteDados0190= Class(TFonteDadosBloco0)
  private
    pSigla: string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
    procedure inicializa;
public
end;

type
  TFonteDados0200 = Class(TFonteDadosBloco0)
  private
    pCodigoItem: string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
public
end;


type
  TFonteDados0200Produto = class(TFonteDados0200)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0200Servico = class(TFonteDados0200)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;


type
  TFonteDados0220 = Class(TFonteDadosBloco0)
  private
    pCodigoItem: string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
public
end;

type
  TFonteDados0400= Class(TFonteDadosBloco0)
  private
    pCodigoNatOp: string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
    procedure inicializa;
public
end;

type
  TFonteDadosBlocoC = class(TFonteDadosParamData)
  private
    CodigoNF: string;
    FpPIS: Currency;
    FpCOFINS: Currency;
    function registroCorrespondenteCST(parametro: string;var valorFinal,BC_ICMS,VL_ICMS,VALOR_RED_BC,VALORIPI,BC_ICMS_ST,VL_ICMS_ST,VL_FRETE,VL_OUTROS:Currency; query:TIBQuery): Boolean;
    function registroCorrespondenteCSOSN(parametro: string;var valorFinal, BC_ICMS, VL_ICMS, VALOR_RED_BC: Currency;query: TIBQuery): Boolean;

    procedure SetpPIS(const Value: Currency);
    procedure SetpCOFINS(const Value: Currency);
  protected
    //function getValorPis(BCPIS:Currency):Currency;
    //function getValorCofins(BCCOFINS:Currency):Currency;
    function getSituacaoNF(pSituacao:string):TACBrSituacaoDocto;
    function getTipoFrete(pTipo: string): TACBrTipoFrete;
  public
    FAcbrBlocoC: TBloco_C;
    {Percentual de PIS/COFINS}
    property pPIS:Currency read FpPIS write SetpPIS;
    property pCOFINS:Currency read FpCOFINS write SetpCOFINS;
end;

type
  TFonteDadosC001= Class(TFonteDadosBlocoC)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
public
end;


type
  TFonteDadosC100 = class(TFonteDadosBlocoC)
  private
    function getIndPag(indPag: string):TACBrTipoPagamento;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC170 = class(TFonteDadosC100)
  private
    codigoNF:Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    function getVL_BC_ICMS(pReducao,baseCalculo: Currency): Currency;
    function getVL_ICMS(bc_icms, aliquota:currency): Currency;
end;

type
  TFonteDadosC170Saida = class(TFonteDadosC170)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC170Entrada = class(TFonteDadosC170)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC190 = class(TFonteDadosBlocoC)
  private
    codigoNF :Integer;
    situacaoNF :TACBrSituacaoDocto;
    pValorICMS :Currency; {esse parametro vem do registro C100}
    pNFentrada :string;   {esse parametro vem do registro C100}
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC190SaidaCST = class(TFonteDadosC190)
  private
    param:string;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC190EntradaCST = class(TFonteDadosC190)
  private
    param:string;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC190SaidaCSOSN = class(TFonteDadosC190)
  private
    param:string;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC190EntradaCSOSN = class(TFonteDadosC190)
  private
    param:string;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC100Saida = class(TFonteDadosC100)
  private
    FRegC170 :TFonteDadosC170Saida;
    FRegC190CST :TFonteDadosC190SaidaCST;
    FRegC190CSOSN :TFonteDadosC190SaidaCSOSN;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC100Entrada = class(TFonteDadosC100)
  private
    FRegC170 :TFonteDadosC170Entrada;
    FRegC190CST :TFonteDadosC190EntradaCST;
    FRegC190CSOSN :TFonteDadosC190EntradaCSOSN;
  protected
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC190Entrada = class(TFonteDadosC190)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC310 = class(TFonteDadosBlocoC)
  private
    FDataVenda: TDateTime;
    procedure SetDataVenda(const Value: TDateTime);
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    property DataVenda:TDateTime read FDataVenda write SetDataVenda;
end;

type
  TFonteDadosC321 = class(TFonteDadosBlocoC)
  private
    FOrigem: string;
    FCFOP: string;
    FAliquota: string;
    FCST: string;
    FdataVenda: TDateTime;
    procedure SetAliquota(const Value: string);
    procedure SetCFOP(const Value: string);
    procedure SetCST(const Value: string);
    procedure SetdataVenda(const Value: TDateTime);
    procedure SetOrigem(const Value: string);
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    property dataVenda:TDateTime read FdataVenda write SetdataVenda;
    property Origem:string read FOrigem write SetOrigem;
    property CST:string read FCST write SetCST;
    property CFOP:string read FCFOP write SetCFOP;
    property Aliquota:string read FAliquota write SetAliquota;
end;

type
  TFonteDadosC320 = class(TFonteDadosBlocoC)
  private
    FRegC321:TFonteDadosC321;
    FdataVenda: TDateTime;
    procedure SetdataVenda(const Value: TDateTime);
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    property dataVenda:TDateTime read FdataVenda write SetdataVenda;
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC370 = class(TFonteDadosBlocoC)
  private
    CodigoVenda:Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC390 = class(TFonteDadosBlocoC)
  private
    CodigoVenda:Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC350 = class(TFonteDadosBlocoC)
  private
    FRegC370 :TFonteDadosC370;
    FRegC390 :TFonteDadosC390;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC300= Class(TFonteDadosBlocoC)
  private
    FRegC310 :TFonteDadosC310;
    FRegC320 :TFonteDadosC320;
    function registroCorrespondenteNF_02(parametro:string;var valorFinal,valorpis,valorcofins:Currency;query:TIBQuery):Boolean;
    function get_NFFINAL(pData:string): string;
    function get_NFINICIAL(pData:string): string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC410 = class(TFonteDadosBlocoC)
  private
    dataMovimento:TDateTime;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC425 = class(TFonteDadosBlocoC)
  private
    dataMovimentoC405:TDateTime;
    totalizadorParcialC420:string;
    strVL_C425:TStringList;
    valorTotalC420:Currency;
    ECF :Integer;
    function acerta_c420_c425(var str:TStringList;list:TIBQuery;valorC420:Currency):Boolean;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC420 = class(TFonteDadosBlocoC)
  private
    FRegC425 :TFonteDadosC425;
    reducaoZ :Integer;
    dataMovimento :TDateTime;
    valorTotalC420:Currency;
    ECF :Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC470 = class(TFonteDadosBlocoC)
  private
    codigoVenda :Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC470Produto = class(TFonteDadosC470)
  private
  protected
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC470Servico = class(TFonteDadosC470)
  private
  protected
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC460 = class(TFonteDadosBlocoC)
  private
    FRegC470Produto :TFonteDadosC470Produto;
    FRegC470Servico :TFonteDadosC470Servico;
    dataMovimento :TDateTime;
    ECF :Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC490 = class(TFonteDadosBlocoC)
  private
    dataMovimento :TDateTime;
    ECF :Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC490Produto = class(TFonteDadosC490)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC490Servico = class(TFonteDadosC490)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC405 = class(TFonteDadosBlocoC)
  private
    FRegC410 :TFonteDadosC410;
    FRegC420 :TFonteDadosC420;
    FregC460 :TFonteDadosC460;
    FRegC490Produto :TFonteDadosC490Produto;
    FRegC490Servico :TFonteDadosC490Servico;
    codigoECF:Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC400= Class(TFonteDadosBlocoC)
  private
    FRegC405 :TFonteDadosC405;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosBlocoD = Class(TFonteDados)
  private
  protected
  public
    FAcbrBlocoD: TBloco_D;
end;

type
  TFonteDadosD001 = class(TFonteDadosBlocoD)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBlocoE = class(TFonteDados)
  private
  protected
    VL_OR:Currency; //campo do registro E116. Somatorio do campo VL_ICMS_RECOLHER do registro E110
    COD_OBRIGACOES_ICMS:string; //campo 2 do registro E116
    COD_RECEITAESTADUAL:string; //campo 5 do registro E116
  public
    FAcbrBlocoE: TBloco_E;
end;

type
  TFonteDadosE001 = class(TFonteDadosBlocoE)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
end;

type
  TFonteDadosE100 = class(TFonteDadosBlocoE)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosE110 = class(TFonteDadosBlocoE)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
    function get_VL_TOT_CREDITOS(registro:TFonteDados):Currency;
    function get_VL_TOT_DEBITOS(registro:TFonteDados;i:Integer = 0;soma:Currency = 0):Currency;
  public
end;

type
  TFonteDadosE116 = class(TFonteDadosBlocoE)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
    function get_VL_OR:currency;
  public
end;

type
  TFonteDadosBlocoG = Class(TFonteDados)
  private
  protected
  public
    FAcbrBlocoG: TBloco_G;
end;

type
  TFonteDadosG001 = class(TFonteDadosBlocoG)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBlocoH = class(TFonteDados)
  private
  protected
    valorTotalEstoque:Currency;
  public
    FAcbrBlocoH: TBloco_H;
end;

type
  TFonteDadosH001 = class(TFonteDadosBlocoH)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosH005 = class(TFonteDadosBlocoH)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosH010 = class(TFonteDadosBlocoH)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBloco1 = class(TFonteDados)
  private
  protected
  public
    FAcbrBloco1: TBloco_1;
end;

type
  TFonteDados1001 = class(TFonteDadosBloco1)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados1010 = class(TFonteDadosBloco1)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados1600 = class(TFonteDadosBloco1)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBloco9 = class(TFonteDados)
  private
  protected
  public
    FAcbrBloco9: TBloco_9;
end;

type
  TFonteDados9001 = class(TFonteDadosBloco9)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados9900 = class(TFonteDadosBloco9)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados9999 = class(TFonteDadosBloco9)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TObjSpedFiscal_Novo = class(TComponent)
  private

    FMsgErro: string;
    ACBrSpedFiscal1: TACBrSPEDFiscal;
    FDataIni: TDateTime;
    FDataFin: TDateTime;
    FIndAtiv: string;
    FIndPerfil: string;
    FCodFin: string;
    FDataFinEstoque: TDateTime;
    FDataIniEstoque: TDateTime;
    ObjQuery:TIBQuery;
    FDataBase: TIBDatabase;
    FPathArquivo: string;
    FpCOFINS: Currency;
    FpPIS: Currency;
    Fstatus: TStatusBar;
    FCOD_RECEITAESTADUAL: string;
    FCOD_OBRIGACOES_ICMS: string;
    FgeraRegistroH: Boolean;
    Fsoftware: string;
    sqlSped:TIniFile;
    FlinhasBuffer: Integer;
    FnotasBuffer: Integer;
    FmemoErros: TMemo;

    procedure LimpaRegistros;
    procedure addMessageERRO(pErro: string);
    procedure inicializa;
    procedure SetDataIni(const Value: TDateTime);
    procedure SetDataFin(const Value: TDateTime);
    procedure SetIndAtiv(const Value: string);
    procedure SetIndPerfil(const Value: string);
    procedure SetCodFin(const Value: string);
    procedure SetDataFinEstoque(const Value: TDateTime);
    procedure SetDataIniEstoque(const Value: TDateTime);
    procedure openQuery (Sql: string);
    procedure SetDataBase(const Value: TIBDatabase);
    procedure SetPathArquivo(const Value: string);
    procedure SetpCOFINS(const Value: Currency);
    procedure SetpPIS(const Value: Currency);
    procedure Setstatus(const Value: TStatusBar);
    procedure SetCOD_OBRIGACOES_ICMS(const Value: string);
    procedure SetCOD_RECEITAESTADUAL(const Value: string);
    procedure SetgeraRegistroH(const Value: Boolean);
    procedure Setsoftware(const Value: string);
    procedure SetlinhasBuffer(const Value: Integer);
    procedure SetnotasBuffer(const Value: Integer);
    procedure SetmemoErros(const Value: TMemo);

  public

    FReg0000  :TFonteDados0000;
    FReg0005  :TFonteDados0005;
    FReg0100  :TFonteDados0100;
    FReg0150C :TFonteDados0150Cliente;
    FReg0150F :TFonteDados0150Fornecedor;
    FReg0190  :TFonteDados0190;

    FReg0200Produto :TFonteDados0200Produto;
    FReg0200Servico :TFonteDados0200Servico;

    FReg0400 :TFonteDados0400;
    FRegC001 :TFonteDadosC001;

    FRegC100Saida   :TFonteDadosC100Saida;
    FRegC100Entrada :TFonteDadosC100Entrada;

    FRegC300 :TFonteDadosC300;
    FRegC350 :TFonteDadosC350;
    FregC400 :TFonteDadosC400;

    FRegD001                 :TFonteDadosD001;
    FRegE001                 :TFonteDadosE001;
    FRegE100                 :TFonteDadosE100;
    FRegE110                 :TFonteDadosE110;
    FRegE116                 :TFonteDadosE116;
    FRegG001                 :TFonteDadosG001;
    FRegH001                 :TFonteDadosH001;
    FRegH010                 :TFonteDadosH010;
    FReg1001                 :TFonteDados1001;
    FReg1010                 :TFonteDados1010;
    FReg1600                 :TFonteDados1600;
    FReg9001                 :TFonteDados9001;
    FReg9900                 :TFonteDados9900;
    FReg9999                 :TFonteDados9999;
    strErros: TStringList;
    strAdvertencia: TStringList;
    strItem: TStringList;
    strServico :TStringList;
    strUnidade: TStringList;
    strNatOp: TStringList;
    strCodPart: TStringList;

    property DataIni: TDateTime read FDataIni  write SetDataIni;
    property DataFin: TDateTime read FDataFin  write SetDataFin;
    property DataIniEstoque: TDateTime read FDataIniEstoque write SetDataIniEstoque;
    property DataFinEstoque: TDateTime read FDataFinEstoque write SetDataFinEstoque;
    property IndPerfil: string read FIndPerfil write SetIndPerfil;
    property IndAtiv: string read FIndAtiv write SetIndAtiv;
    property CodFin: string read FCodFin write SetCodFin;
    property DataBase :TIBDatabase read FDataBase write SetDataBase;
    property PathArquivo: string read FPathArquivo write SetPathArquivo;
    property pPIS: Currency read FpPIS write SetpPIS;
    property pCOFINS: Currency read FpCOFINS write SetpCOFINS;
    property status:TStatusBar read Fstatus write Setstatus;
    property COD_OBRIGACOES_ICMS: string read FCOD_OBRIGACOES_ICMS write SetCOD_OBRIGACOES_ICMS;
    property COD_RECEITAESTADUAL: string read FCOD_RECEITAESTADUAL write SetCOD_RECEITAESTADUAL;
    property geraRegistroH: Boolean read FgeraRegistroH write SetgeraRegistroH;
    property software: string read Fsoftware write Setsoftware;
    property linhasBuffer:Integer read FlinhasBuffer write SetlinhasBuffer;
    property notasBuffer:Integer read FnotasBuffer write SetnotasBuffer;
    property memoErros :TMemo read FmemoErros write SetmemoErros;

    procedure gravaTXT;
    procedure preencheComponente;
    function  geraSped:Boolean;
    procedure preencheSql;
    function teveErros:Boolean;
    procedure carregaErros(str: TStringList);
    procedure carregaErrosMemo();

    constructor Create(AOwner: TComponent;ibdatabase: TIBDatabase);overload;
    destructor destroy;override;
    
end;

implementation

uses SysUtils,Forms, UessencialGlobal, ACBrEFDBloco_E, ACBrEFDBloco_C,
  ACBrEFDBloco_0;

procedure TObjSpedFiscal_Novo.gravaTXT;
begin
  ACBrSPEDFiscal1.SaveFileTXT;
end;

procedure TObjSpedFiscal_Novo.limpaRegistros;
begin
  with (self.ACBrSPEDFiscal1) do
  begin
    Bloco_0.LimpaRegistros;
    Bloco_C.LimpaRegistros;
    Bloco_D.LimpaRegistros;
    Bloco_E.LimpaRegistros;
    Bloco_G.LimpaRegistros;
    Bloco_H.LimpaRegistros;
    Bloco_1.LimpaRegistros;
    Bloco_9.LimpaRegistros;
    Bloco_0.Gravado:=False;
    Bloco_C.Gravado:=False;
    Bloco_D.Gravado:=False;
    Bloco_E.Gravado:=False;
    Bloco_G.Gravado:=False;
    Bloco_H.Gravado:=False;
    Bloco_1.Gravado:=False;
    Bloco_9.Gravado:=False;
  end;

end;

procedure TObjSpedFiscal_Novo.addMessageERRO(pErro: string);
begin
  FMsgErro:=FMsgErro+pErro+#13;
end;

function TFonteDados0000.getCodFin: TACBrCodFinalidade;
begin
  if (FCodFin = '0') then
    result := raOriginal
  else
    result := raSubstituto;
end;

procedure TObjSpedFiscal_Novo.SetDataIni(const Value: TDateTime);
begin
  FDataIni := Value;
end;

procedure TObjSpedFiscal_Novo.SetDataFin(const Value: TDateTime);
begin
  FDataFin := Value;
end;

procedure TObjSpedFiscal_Novo.SetIndAtiv(const Value: string);
begin
  FIndAtiv := Value;
end;

procedure TObjSpedFiscal_Novo.SetIndPerfil(const Value: string);
begin
  FIndPerfil := Value;
end;

procedure TObjSpedFiscal_Novo.SetCodFin(const Value: string);
begin
  FCodFin := Value;
end;

procedure TObjSpedFiscal_Novo.SetDataFinEstoque(const Value: TDateTime);
begin
  FDataFinEstoque := Value;
end;

procedure TObjSpedFiscal_Novo.SetDataIniEstoque(const Value: TDateTime);
begin
  FDataIniEstoque := Value;
end;

procedure TObjSpedFiscal_Novo.openQuery (Sql: string);
begin

  ObjQuery.Active := False;
  ObjQuery.SQL.Text := '';
  ObjQuery.SQL.Text := Sql;
  ObjQuery.Active := True;
  ObjQuery.First;
end;

function TFonteDados0000.getIndPerfil: TACBrPerfil;
begin
  if (FIndPerfil = 'A') then
    result := pfPerfilA
  else
  if (FIndPerfil = 'B') then
    result := pfPerfilB
  else
    result := pfPerfilC;
end;

function TFonteDados0000.getIindAtiv: TACBrAtividade;
begin
  if (FIndAtiv = '1') then
    result := atOutros
  else
    result := atIndustrial;
end;

{ TFonteDados }

function TFonteDados.AbreQuery:Boolean;
begin
  result := False;
  if self.FSQL = 'vazio;' then
    Exit;

  Self.status.Panels[1].Text:='Gerando registro: '+Registro;
  Application.ProcessMessages;

  if FQuery = nil then
    FQuery := TIBQuery.Create(Self);

  FQuery.Close;

  FQuery.DisableControls;
  FQuery.Database := FDatabase;
  FQuery.SQL.Text := Self.FSQL;

  if FQuery.Params.Count > 0 then
    Self.SetParametros;

  FQuery.Open;
  if not (FQuery.IsEmpty) then
  begin
    Self.SetCampos;
    FQuery.First;
    while not FQuery.Eof do
    begin
      self.ValidaRegistros;
      self.ValidaBrancosNulos;
      FQuery.Next;
    end;
    FQuery.First;
  end
  else
    Result := False;
    
  result := True;
end;

procedure TFonteDados.addStr(str: TStringList; valor: string);
var
  _i:integer;
begin
  for _i := 0  to str.Count-1  do
    if (str[_i] = valor) then Exit;
  str.Add (valor);
end;

constructor TFonteDados.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDados.Destroy;
begin
  FreeAndNil (fquery);
  inherited;
end;

procedure TFonteDados.GetFieldDesencripta(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
  begin
    Text := DesincriptaSenha(Sender.AsString);
    text := Trim(text);
  end;
end;

procedure TFonteDados.GetFieldDesencriptaSoNumeros(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
  begin
    text := DesincriptaSenha(Sender.AsString);
    Text := RetornaSoNumeros(text);
  end
end;

procedure TFonteDados.GetFieldSoNumeros(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := RetornaSoNumeros(Sender.AsString);
end;

procedure TFonteDados.GetFieldTrim(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if sender.AsString <> '' then
    Text := Trim(sender.AsString);
end;

procedure TFonteDados.SetCampos;
var
  i: integer;
begin
  for i := 0  to FQuery.Fields.Count-1  do
    FQuery.Fields[i].Required := True;
end;

procedure TFonteDados.SetDatabase(const Value: TIBDatabase);
begin
  FDatabase := Value;
end;

procedure TFonteDados.SetDataFIN(const Value: TDateTime);
begin
  FDataFIN := Value;
end;

procedure TFonteDados.SetDataFinEstoque(const Value: TDateTime);
begin
  FDataFinEstoque := Value;
end;

procedure TFonteDados.SetDataINI(const Value: TDateTime);
begin
  FDataINI := Value;
end;

procedure TFonteDados.SetDataIniEstoque(const Value: TDateTime);
begin
  FDataIniEstoque := Value;
end;

procedure TFonteDados.SetgeraRegistroH(const Value: Boolean);
begin
  FgeraRegistroH := Value;
end;

procedure TFonteDados.SetindPerfil(const Value: string);
begin
  FindPerfil := Value;
end;

procedure TFonteDados.SetRegistro(const Value: String);
begin
  FRegistro := Value;
end;

procedure TFonteDados.SetSQL(const Value: String);
begin
  FSQL := Value;
end;

procedure TFonteDados.Setstatus(const Value: TStatusBar);
begin
  Fstatus := Value;
end;

function TFonteDados.teveErros: Boolean;
begin
result := (strErros.Count > 0);
end;

function TFonteDados.ValidaBrancosNulos: Boolean;
var
  i :Integer;
begin
  Self.status.Panels[1].Text:='Validando registro: '+Registro+'...';
  for i := 0  to FQuery.FieldCount - 1 do
  begin
    Application.ProcessMessages;
    if FQuery.Fields[i].Required then
    begin
      if FQuery.Fields[i].AsString = ''  then
        strErros.Add('Erro no registro' + FRegistro + ' campo: "'+FQuery.Fields[i].DisplayLabel+'" com valor em branco ou nulo. '+self.campoErro);
    end;
  end;
  Result := (strErros.Count <= 0);
end;

{ TFonteDadosBloco0 }

procedure TFonteDadosParamData.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsDateTime := FDataINI;
  FQuery.Params[1].AsDateTime := FDataFIN;
end;

constructor TObjSpedFiscal_Novo.Create(AOwner: TComponent;ibdatabase: TIBDatabase);
begin
  inherited Create(AOwner);
  self.ACBrSpedFiscal1 := TACBrSPEDFiscal.Create(AOwner);
  self.SetDataBase(ibdatabase);
  strErros := TStringList.Create;
  strAdvertencia := TStringList.Create;
  strItem := TStringList.Create;
  strServico := TStringList.Create;
  strUnidade := TStringList.Create;
  strNatOp := TStringList.Create;
  strCodPart := TStringList.Create;
end;

procedure TObjSpedFiscal_Novo.carregaErrosMemo();
begin
  self.carregaErros(strERROS);
  self.carregaErros(strAdvertencia);
end;

procedure TObjSpedFiscal_Novo.carregaErros(str: TStringList);
var
  i:integer;
begin
  for i := 0  to str.Count-1  do
    memoErros.Lines.Add(str[i]);
end;

procedure TObjSpedFiscal_Novo.SetDataBase(const Value: TIBDatabase);
begin
  FDataBase := Value;
end;

{ TFonteDados0000 }

procedure TFonteDados0000.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  with (FAcbrBloco0.Registro0000New) do
  begin
    COD_VER    := Self.getCodVer;
    COD_FIN    := self.getCodFin;
    DT_INI     := Self.FDataINI;
    DT_FIN     := Self.FDataFIN;
    NOME       := FQuery.Fields[0].Text;
    CNPJ       := FQuery.Fields[1].Text;
    UF         := FQuery.Fields[2].Text;
    IE         := FQuery.Fields[3].Text;
    COD_MUN    := FQuery.Fields[4].AsInteger;
    IND_PERFIL := Self.getIndPerfil;
    IND_ATIV   := self.getIindAtiv;
  end;
  FAcbrBloco0.Registro0001New.IND_MOV := imComDados;
end;

procedure TFonteDados0000.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Raz�o Social';
  FQuery.Fields[1].DisplayLabel := 'CNPJ';
  FQuery.Fields[2].DisplayLabel := 'Estado';
  FQuery.Fields[3].DisplayLabel := 'IE';
  FQuery.Fields[4].DisplayLabel := 'C�digo municipio';

  if software = 'PAF' then
  begin
    FQuery.Fields[0].OnGetText := GetFieldDesencripta;
    FQuery.Fields[1].OnGetText := GetFieldDesencriptaSoNumeros;
    FQuery.Fields[2].OnGetText := GetFieldDesencripta;
    FQuery.Fields[3].OnGetText := GetFieldDesencriptaSoNumeros;
    FQuery.Fields[4].OnGetText := GetFieldDesencripta;
  end
  else
  begin
    FQuery.Fields[0].OnGetText := GetFieldTrim;
    FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
    FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  end;
end;

procedure TFonteDados0000.SetParametros;
begin
  inherited;
end;

function TFonteDados0000.ValidaRegistros: Boolean;
begin

  if (Length(FQuery.Fields[0].Text) > 100) then
    strAdvertencia.Add('Nome empresarial da entidade ultrapassa o limite reservado');

  if not (ValidaCNPJ (FQuery.Fields[1].Text)) then
    strERROS.Add('N�mero de inscri��o da empresa no CNPJ inv�lido');

  if self.IndPerfil = '' then
    strErros.Add('Indicador de perfil n�o informado');

  if self.CodFin = '' then
    strErros.Add('C�digo da finalidade do arquivo n�o informado');

  if self.IndAtiv = '' then
    strErros.Add('Indicador de atividade n�o informado');

end;

procedure TFonteDados0005.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  with (FAcbrBloco0.Registro0005New) do
  begin
    FANTASIA := FQuery.Fields[0].Text;
    CEP      := FQuery.Fields[1].Text;
    ENDERECO := FQuery.Fields[2].Text;
    NUM      := FQuery.Fields[3].Text;
    COMPL    := FQuery.Fields[4].Text;
    BAIRRO   := FQuery.Fields[5].Text;
    FONE     := FQuery.Fields[6].Text;
    FAX      := FQuery.Fields[7].Text;

    EMAIL    := FQuery.Fields[8].Text;
  end;
end;

procedure TFonteDados0005.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Fantasia';
  FQuery.Fields[1].DisplayLabel := 'CEP';
  FQuery.Fields[2].DisplayLabel := 'Endere�o';
  FQuery.Fields[3].DisplayLabel := 'Numero';
  FQuery.Fields[4].DisplayLabel := 'Complemento';
  FQuery.Fields[5].DisplayLabel := 'Bairro';
  FQuery.Fields[6].DisplayLabel := 'Fone';
  FQuery.Fields[7].DisplayLabel := 'Fax';
  FQuery.Fields[8].DisplayLabel := 'Email';
  FQuery.Fields[4].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[8].Required := False;

  if software = 'PAF' then
  begin
    FQuery.Fields[0].OnGetText := GetFieldDesencripta;
    FQuery.Fields[1].OnGetText := GetFieldDesencriptaSoNumeros;
    FQuery.Fields[2].OnGetText := GetFieldDesencripta;
    FQuery.Fields[3].OnGetText := GetFieldDesencripta;
    FQuery.Fields[4].OnGetText := GetFieldDesencripta;
    FQuery.Fields[5].OnGetText := GetFieldDesencripta;
    FQuery.Fields[6].OnGetText := GetFieldDesencriptaSoNumeros;
    FQuery.Fields[7].OnGetText := GetFieldDesencriptaSoNumeros;
    FQuery.Fields[8].OnGetText := GetFieldDesencripta;
  end
  else
  begin
    FQuery.Fields[0].OnGetText := GetFieldTrim;
    FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
    FQuery.Fields[2].OnGetText := GetFieldTrim;
    FQuery.Fields[4].OnGetText := GetFieldTrim;
    FQuery.Fields[5].OnGetText := GetFieldTrim;
    FQuery.Fields[6].OnGetText := GetFieldSoNumeros;
    FQuery.Fields[7].OnGetText := GetFieldSoNumeros;
    FQuery.Fields[8].OnGetText := GetFieldTrim;
  end;

end;

procedure TFonteDados0005.SetParametros;
begin
  inherited;
end;

function TFonteDados0005.ValidaRegistros: Boolean;
begin
  if (Length(FQuery.Fields[0].Text) > 60) then
    strAdvertencia.Add('Nome de fantasia associado ao nome empresarial ultrapassa o limite reservado. Limite(60)');
  if (length(Trim(FQuery.Fields[6].Text)) <> 10) then
    strERROS.Add('Fone da empresa inv�lido. Formato v�lido: DDD+FONE (ex: 6734245874)');
end;

procedure TFonteDados0000.SetCodFin(const Value: string);
begin
  FCodFin := Value;
end;

procedure TFonteDados0000.SetIndAtiv(const Value: string);
begin
  FIndAtiv := Value;
end;

{ TFonteDadosBloco0 }

function TFonteDados0000.getCodVer:TACBrVersaoLeiaute;
begin

  if (FDataIni <= StrToDate ('31/12/2011')) then
    result := vlVersao103
  else
  if (FDataIni <= StrToDate ('31/12/2012')) then
    result := vlVersao105
  else
  if (FDataIni <= StrToDate ('31/12/2013')) then
    Result := vlVersao106 //007
  else
  if (FDataIni <= StrToDate ('31/12/2014')) then
    Result := vlVersao107//008
  else
  if (FDataIni <= StrToDate ('31/12/2015')) then
    Result := vlVersao108//009
  else
    Result := vlVersao109//010

end;

procedure TObjSpedFiscal_Novo.inicializa;
var
  i: integer;
begin
  ACBrSPEDFiscal1.LinhasBuffer := self.FlinhasBuffer;
  ACBrSPEDFiscal1.Arquivo := ExtractFileName(self.FPathArquivo);
  ACBrSPEDFiscal1.Path    := ExtractFileDir(self.FPathArquivo)+'\';

  self.limpaRegistros;
  ACBrSPEDFiscal1.DT_INI := DataIni;
  ACBrSPEDFiscal1.DT_FIN := DataFin;
  {zera campos}
  strErros.Clear;
  strAdvertencia.Clear;
  strItem.Clear;
  strServico.Clear;
  strUnidade.Clear;
  strNatOp.Clear;
  strCodPart.Clear;
  ACBrSPEDFiscal1.IniciaGeracao;

  FReg0000 := TFonteDados0000.Create(Self);
  FReg0000.Registro := '0000';

  FReg0000.CodFin := self.FCodFin;
  FReg0000.IndAtiv := self.FIndAtiv;

  FReg0005 := TFonteDados0005.Create(self);
  FReg0005.Registro := '0005 Empresa';

  FReg0100 := TFonteDados0100.Create(self);
  FReg0100.Registro := '0100 Contador';

  FReg0150C := TFonteDados0150Cliente.create(self);
  FReg0150C.Registro := '0150C';

  FReg0150F := TFonteDados0150Fornecedor.create(self);
  FReg0150F.Registro := '0150F';

  FReg0190 := TFonteDados0190.Create(self);
  FReg0190.Registro := '0190 Unidades de medida';

  FReg0200Produto := TFonteDados0200Produto.Create(self);
  FReg0200Produto.Registro := '0200 PRODUTO';

  FReg0200Servico := TFonteDados0200Servico.Create(self);
  FReg0200Servico.Registro := '0200 SERVI�O';

  FReg0400 := TFonteDados0400.Create(self);
  FReg0400.Registro := '0400 Natureza de opera��o';

  FRegC001 := TFonteDadosC001.Create(self);
  FRegC001.registro := 'C001';

  FRegC100Saida := TFonteDadosC100Saida.create(self);
  FRegC100Saida.Registro := 'C100 SAIDA';

  FRegC100Entrada := TFonteDadosC100Entrada.create(self);
  FRegC100Entrada.Registro := 'C100 ENTRADA';

  FRegC300 := TFonteDadosC300.Create(self);
  FRegC300.Registro := 'C300';

  FRegC350 := TFonteDadosC350.Create(self);
  FRegC350.Registro := 'C350';

  FregC400 := TFonteDadosC400.create(self);
  FregC400.Registro := 'C400';

  FRegD001 := TFonteDadosD001.Create(self);
  FRegD001.Registro := 'D001';

  FRegE001 := TFonteDadosE001.Create(self);
  FRegE001.Registro := 'E001';

  FRegE100 := TFonteDadosE100.Create(self);
  FRegE100.Registro := 'E100';

  FRegE110 := TFonteDadosE110.Create(self);
  FRegE110.Registro := 'E110';

  FRegE116 := TFonteDadosE116.Create(self);
  FRegE116.Registro := 'E116';

  FRegG001 := TFonteDadosG001.Create(self);
  FRegG001.Registro := 'G001';

  FRegH001 := TFonteDadosH001.Create(self);
  FRegH001.Registro := 'H001';

  FRegH010 := TFonteDadosH010.Create(self);
  FRegH010.Registro := 'H010';

  FReg1001 := TFonteDados1001.Create(self);
  FReg1001.Registro := '1001';

  FReg1010 := TFonteDados1010.Create(self);
  FReg1010.Registro := '1010';

  FReg1600 := TFonteDados1600.Create(self);
  FReg1600.Registro := '1600';

  FReg9001 := TFonteDados9001.Create(self);
  FReg9001.Registro := '9001';

  FReg9900 := TFonteDados9900.Create(self);
  FReg9900.Registro := '9900';

  FReg9999 := TFonteDados9999.Create(Self);
  FReg9999.Registro := '9999';

  for i := 0  to self.ComponentCount-1  do
  begin

    if self.Components[i] is TFonteDados then
    begin
      TFonteDados(self.Components[i]).Database := self.DataBase;
      TFonteDados(self.Components[i]).DataINI := self.FDataIni;
      TFonteDados(self.Components[i]).DataFIN := self.FDataFin;
      TFonteDados(self.Components[i]).DataIniEstoque := self.FDataIniEstoque;
      TFonteDados(self.Components[i]).DataFinEstoque := self.FDataFinEstoque;
      TFonteDados(self.Components[i]).strUnidade := self.strUnidade;
      TFonteDados(self.Components[i]).strItem := self.strItem;
      TFonteDados(self.Components[i]).strServico := self.strServico;
      TFonteDados(self.Components[i]).strNatOp := self.strNatOp;
      TFonteDados(self.Components[i]).strCodPart := self.strCodPart;
      TFonteDados(self.Components[i]).strErros := self.strErros;
      TFonteDados(self.Components[i]).strAdvertencia := self.strAdvertencia;
      TFonteDados(self.Components[i]).valor_total_debitos  := 0;
      TFonteDados(self.Components[i]).VL_TOT_CREDITOS_E110 := 0;
      TFonteDados(self.Components[i]).status := self.Fstatus;
      TFonteDados(self.components[i]).geraRegistroH := self.geraRegistroH;
      TFonteDados(self.Components[i]).software := self.software;
      TFonteDados(self.Components[i]).indPerfil := self.IndPerfil;
    end;

    if self.Components[i] is TFonteDadosBloco0 then
      TFonteDadosBloco0(self.Components[i]).FAcbrBloco0 := ACBrSpedFiscal1.Bloco_0;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := ACBrSpedFiscal1.Bloco_C;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pPIS;
    end;

    if self.Components[i] is TFonteDadosBlocoD then
      TFonteDadosBlocoD(self.Components[i]).FAcbrBlocoD := ACBrSpedFiscal1.Bloco_D;

    if self.Components[i] is TFonteDadosBlocoE then
      TFonteDadosBlocoE(self.Components[i]).FAcbrBlocoE := ACBrSpedFiscal1.Bloco_E;

    if self.Components[i] is TFonteDadosBlocoG then
      TFonteDadosBlocoG(self.Components[i]).FAcbrBlocoG := ACBrSpedFiscal1.Bloco_G;

    if self.Components[i] is TFonteDadosBlocoH then
      TFonteDadosBlocoH(self.Components[i]).FAcbrBlocoH := ACBrSpedFiscal1.Bloco_H;

    if self.Components[i] is TFonteDadosBloco1 then
      TFonteDadosBloco1(self.Components[i]).FAcbrBloco1 := ACBrSpedFiscal1.Bloco_1;

    if self.Components[i] is TFonteDadosC300 then
      TFonteDadosC300(self.Components[i]).inicializa;

    if Self.Components[i] is TFonteDadosC350 then
      TFonteDadosC350(self.Components[i]).inicializa;

    if Self.Components[i] is TFonteDadosC400 then
      TFonteDadosC400(self.Components[i]).inicializa;

    if Self.Components[i] is TFonteDadosC100Saida then
      TFonteDadosC100Saida(self.Components[i]).inicializa;

    if Self.Components[i] is TFonteDadosC100Entrada then
      TFonteDadosC100Entrada(self.Components[i]).inicializa;

    if self.Components[i] is TFonteDadosE001 then
      TFonteDadosE001(self.Components[i]).inicializa;

    if self.Components[i] is TFonteDadosBloco9 then
      TFonteDadosBloco9(self.Components[i]).FAcbrBloco9 := ACBrSpedFiscal1.Bloco_9;

  end;

  FRegE116.COD_OBRIGACOES_ICMS := self.FCOD_OBRIGACOES_ICMS;
  FRegE116.COD_RECEITAESTADUAL := self.FCOD_RECEITAESTADUAL;
  
end;

procedure TObjSpedFiscal_Novo.SetPathArquivo(const Value: string);
begin
  FPathArquivo := Value;
end;

destructor TObjSpedFiscal_Novo.destroy;
begin
  FreeAndNil (self.ACBrSpedFiscal1);
  FreeAndNil(FReg0000);
  FreeAndNil(FReg0005);
  FreeAndNil(FReg0100);
  FreeAndNil(FReg0150C);
  FreeAndNil(FReg0150F);
  FreeAndNil(FReg0190);
  FreeAndNil(FReg0200Produto);
  FreeAndNil(FReg0200Servico);
  FreeAndNil(FReg0400);
  FreeAndNil(FRegC001);
  FreeAndNil(FRegC100Saida);
  FreeAndNil(FRegC100Entrada);
  FreeAndNil(FRegC300);
  FreeAndNil(FRegC350);
  FreeAndNil(FregC400);
  FreeAndNil(FRegD001);
  FreeAndNil(FRegE001);
  FreeAndNil(FRegE100);
  FreeAndNil(FRegE110);
  FreeAndNil(FRegE116);
  FreeAndNil(FRegG001);
  FreeAndNil(FRegH001);
  FreeAndNil(FRegH010);
  FreeAndNil(FReg1010);
  FreeAndNil(FReg1600);
  FreeAndNil(FReg1001);
  FreeAndNil(FReg9001);
  FreeAndNil(FReg9900);
  FreeAndNil(FReg9999);
  FreeAndNil (self.strErros);
  FreeAndNil (self.strAdvertencia);
  FreeAndNil (self.strNatOp);
  FreeAndNil (self.strItem);
  FreeAndNil (self.strServico);
  FreeAndNil (self.strUnidade);
  FreeAndNil (self.strCodPart);
  inherited;
end;

{ TFonteDadosBloco0 }
procedure TFonteDadosBloco0.SetParametros;
begin
  inherited;
end;

{ TFonteDados0100 }
procedure TFonteDados0100.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  with (FAcbrBloco0.Registro0100New) do
  begin
    NOME     := Trim(FQuery.Fields[0].Text);
    CPF      := FQuery.Fields[1].Text;
    CRC      := FQuery.Fields[2].Text;
    CNPJ     := FQuery.Fields[3].Text;
    CEP      := RetornaSoNumeros(FQuery.Fields[4].Text);
    ENDERECO := Trim(FQuery.Fields[5].Text);
    NUM      := FQuery.Fields[6].Text;
    COMPL    := FQuery.Fields[7].Text;
    BAIRRO   := Trim(FQuery.Fields[8].Text);
    FONE     := FQuery.Fields[9].Text;
    FAX      := FQuery.Fields[10].Text;
    EMAIL    := FQuery.Fields[11].Text;
    COD_MUN  := FQuery.Fields[12].AsInteger;
  end;
end;

procedure TFonteDados0100.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Nome';
  FQuery.Fields[1].DisplayLabel := 'CPF';
  FQuery.Fields[2].DisplayLabel := 'CRC';
  FQuery.Fields[3].DisplayLabel := 'CNPJ';
  FQuery.Fields[4].DisplayLabel := 'CEP';
  FQuery.Fields[5].DisplayLabel := 'Endere�o';
  FQuery.Fields[6].DisplayLabel := 'N�mero';
  FQuery.Fields[7].DisplayLabel := 'Complemento';
  FQuery.Fields[8].DisplayLabel := 'Bairro';
  FQuery.Fields[9].DisplayLabel := 'Fone';
  FQuery.Fields[10].DisplayLabel := 'Fax';
  FQuery.Fields[11].DisplayLabel := 'Email';
  FQuery.Fields[12].DisplayLabel := 'C�digo do Munic�pio';
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[9].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[10].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[12].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[4].OnGetText := GetFieldTrim;
  FQuery.Fields[5].OnGetText := GetFieldTrim;
  FQuery.Fields[6].OnGetText := GetFieldTrim;
  FQuery.Fields[7].OnGetText := GetFieldTrim;
  FQuery.Fields[8].OnGetText := GetFieldTrim;
  FQuery.Fields[11].OnGetText := GetFieldTrim;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[10].Required := False;
  FQuery.Fields[11].Required := true;
end;

procedure TFonteDados0100.SetParametros;
begin
  inherited;
end;

function TFonteDados0100.ValidaRegistros: Boolean;
begin

  if not (ValidaCPF (FQuery.Fields[1].Text)) then
    strERROS.Add ('N�mero de inscri��o do contabilista no CPF inv�lido');

  if (FQuery.Fields[3].Text <> '') then
  begin
    if not (ValidaCNPJ(FQuery.Fields[3].Text)) then
      strERROS.Add ('CNPJ no cadastro do contador inv�lido');
  end;

  if (Length(FQuery.Fields[5].Text) > 60) then
    strAdvertencia.Add('Endere�o do im�vel do contador respons�vel ultrapassa o limite reservado. Limite(60)')

end;

{ TFonteDados0200 }

procedure TFonteDados0200.preenche;
begin
  inherited;
end;

procedure TFonteDados0200.SetCampos;
begin

  inherited;
  FQuery.Fields[0].DisplayLabel := 'Descri��o';
  FQuery.Fields[1].DisplayLabel := 'C�digo de barras';
  FQuery.Fields[2].DisplayLabel := 'Unidade';
  FQuery.Fields[3].DisplayLabel := 'NCM';

  FQuery.Fields[1].Required := False;
  FQuery.Fields[3].Required := False;

  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;

end;

procedure TFonteDados0200.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsString := self.pCodigoItem;
end;

function TFonteDados0200.ValidaRegistros: Boolean;
begin
  result := True;
end;
{ TFonteDados0150 }

procedure TFonteDados0150.preenche;
begin
  inherited;
end;

procedure TFonteDados0150.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Nome';
  FQuery.Fields[1].DisplayLabel := 'C�digo do pais';
  FQuery.Fields[2].DisplayLabel := 'Inscri��o estadual';
  FQuery.Fields[3].DisplayLabel := 'C�digo do munic�pio';
  FQuery.Fields[4].DisplayLabel := 'Endere�o';
  FQuery.Fields[5].DisplayLabel := 'N�mero';
  FQuery.Fields[6].DisplayLabel := 'Bairro';
  FQuery.Fields[7].DisplayLabel := 'CPF/CNPJ';
  FQuery.Fields[8].DisplayLabel := 'UF';

  FQuery.fields[5].Required := False;
  FQuery.fields[2].Required := False;

  FQuery.Fields[2].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[7].OnGetText := GetFieldSoNumeros;

  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[4].OnGetText := GetFieldTrim;
  FQuery.Fields[5].OnGetText := GetFieldTrim;
  FQuery.Fields[6].OnGetText := GetFieldTrim;
end;

procedure TFonteDados0150.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsString := self.pCodigoParticipante;
end;


function TFonteDados0150.ValidaRegistros: Boolean;
begin
end;

{ TFonteDados0400 }

procedure TFonteDados0400.inicializa;
begin
  if FQuery = nil then
    FQuery := TIBQuery.Create(Self);
  FQuery.DisableControls;
  FQuery.Database := FDatabase;
  FQuery.SQL.Text := self.FSQL;
end;

procedure TFonteDados0400.preenche;
  var
  i:Integer;
begin
  inherited;
  self.inicializa;
  for i:=0  to strNatOp.Count-1  do
  begin
    self.pCodigoNatOp := strNatOp[i];
    self.SetParametros;
    FQuery.Active := False;
    FQuery.Active := True;
    SetCampos;
    with (FAcbrBloco0.Registro0400New) do
    begin
      COD_NAT   := strNatOp[i];
      DESCR_NAT := FQuery.Fields[0].Text;
    end;
  end;
end;

procedure TFonteDados0400.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Descri��o';
  FQuery.Fields[0].OnGetText := GetFieldTrim;
end;

procedure TFonteDados0400.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsString := self.pCodigoNatOp;
end;

function TFonteDados0400.ValidaRegistros: Boolean;
begin
  result := True;
end;

{ TFonteDadosBlocoC }

function TFonteDadosBlocoC.getSituacaoNF(pSituacao: string): TACBrSituacaoDocto;
begin
  pSituacao:=UpperCase(pSituacao);
  if (pSituacao = 'I') or (pSituacao = 'G') or (pSituacao = 'N') then {situa��o "N" de nao cancelada}
    result := sdRegular
  else
  if (pSituacao = 'C') or (pSituacao = 'S') then {situa��o "S" de cancelada}
    result := sdCancelado
  else if (pSituacao = 'Z') then
    result := sdDoctoNumInutilizada
  else if (pSituacao = 'D') then
    result := sdDoctoDenegado;
end;

function TFonteDadosBlocoC.getTipoFrete(pTipo: string): TACBrTipoFrete;
begin
  if (pTipo = '1') then
    result := tfPorContaEmitente
  else
  if (pTipo = '2') then
    result := tfPorContaDestinatario
  else
    result := tfSemCobrancaFrete;
end;

{function TFonteDadosBlocoC.getValorCofins(BCCOFINS: Currency): Currency;
begin
  result := (BCCOFINS * pCOFINS / 100);
end;}

{function TFonteDadosBlocoC.getValorPis(BCPIS: Currency): Currency;
begin
  result := (bcPIS * pPIS / 100);
end;}

procedure TFonteDadosBlocoC.SetpCOFINS(const Value: Currency);
begin
  FpCOFINS := Value;
end;

procedure TFonteDadosBlocoC.SetpPIS(const Value: Currency);
begin
  FpPIS := Value;
end;


function TFonteDadosBlocoC.registroCorrespondenteCSOSN(parametro: string;
  var valorFinal, BC_ICMS, VL_ICMS, VALOR_RED_BC: Currency;
  query: TIBQuery): Boolean;
begin

  query.Next;

  if not (query.Eof) then
  begin

    if (parametro = query.Fields[0].Text + query.Fields[1].text + query.Fields[2].text) then   {CSOSN,CFOP,Aliquota}
    begin

      valorFinal   := valorFinal   + query.Fields[3].AsCurrency;
      BC_ICMS      := BC_ICMS      + query.Fields[4].AsCurrency;
      VL_ICMS      := VL_ICMS      + query.Fields[4].AsCurrency * query.Fields[2].AsCurrency / 100;
      VALOR_RED_BC := VALOR_RED_BC + query.Fields[3].AsCurrency - query.Fields[4].AsCurrency;

      if (query.Fields[4].AsCurrency > 0) then {BC ICMS}
      begin

        if trunca (query.Fields[3].AsCurrency) < trunca(query.Fields[4].AsCurrency) then  {VALOR FINAL,BCICMS}
          VALOR_RED_BC:= VALOR_RED_BC + (trunca(query.Fields[3].AsCurrency) + (trunca(query.Fields[4].AsCurrency) - trunca(query.Fields[3].AsCurrency))) - trunca(query.Fields[4].AsCurrency)
        else
          VALOR_RED_BC := VALOR_RED_BC + trunca(query.Fields[3].AsCurrency) - trunca(query.Fields[4].AsCurrency);

      end;

      Result := True;

    end
    else
      result := False;

  end;

end;

function TFonteDadosBlocoC.registroCorrespondenteCST(parametro: string;
  var valorFinal, BC_ICMS, VL_ICMS, VALOR_RED_BC, VALORIPI, BC_ICMS_ST,VL_ICMS_ST,VL_FRETE,
  VL_OUTROS: Currency;
  query: TIBQuery): Boolean;
begin

  query.Next;

  if not (query.Eof) then
  begin

    if (parametro = query.Fields[0].Text + query.Fields[1].Text + query.Fields[2].Text + query.Fields[3].Text ) then  {Origem,CST,CFOP,aliquota}
    begin

      valorFinal   := valorFinal   + trunca(query.Fields[4].AsCurrency);
      BC_ICMS      := BC_ICMS      + trunca(query.Fields[5].AsCurrency);
      VL_ICMS      := VL_ICMS      + trunca(query.Fields[5].AsCurrency) * query.Fields[3].AsCurrency / 100;
      VALORIPI     := VALORIPI     + trunca(query.Fields[6].AsCurrency);
      VL_FRETE     := VL_FRETE     + trunca(query.Fields[7].AsCurrency);
      BC_ICMS_ST   := BC_ICMS_ST   + trunca(query.Fields[8].AsCurrency);
      VL_ICMS_ST   := VL_ICMS_ST   + trunca(query.Fields[9].AsCurrency);
      VL_OUTROS    := VL_OUTROS    + trunca(query.fields[10].AsCurrency);

      if (query.Fields[5].AsCurrency > 0) then {BC ICMS}
      begin

        if trunca (query.Fields[4].AsCurrency) < trunca(query.Fields[5].AsCurrency) then  {VALOR FINAL,BCICMS}
          VALOR_RED_BC:= VALOR_RED_BC + (trunca(query.Fields[4].AsCurrency) + (trunca(query.Fields[5].AsCurrency) - trunca(query.Fields[4].AsCurrency))) - trunca(query.Fields[5].AsCurrency)
        else
          VALOR_RED_BC := VALOR_RED_BC + trunca(query.Fields[4].AsCurrency) - trunca(query.Fields[5].AsCurrency);

      end;

      Result := True;

    end
    else
      result := False;

  end;

end;


procedure TObjSpedFiscal_Novo.SetpCOFINS(const Value: Currency);
begin
  FpCOFINS := Value;
end;

procedure TObjSpedFiscal_Novo.SetpPIS(const Value: Currency);
begin
  FpPIS := Value;
end;

procedure TObjSpedFiscal_Novo.Setstatus(const Value: TStatusBar);
begin
  Fstatus := Value;
end;

{ TFonteDadosD001 }

procedure TFonteDadosD001.preenche;
begin

  FAcbrBlocoD.RegistroD001New.IND_MOV := imSemDados;

end;


procedure TFonteDadosD001.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosD001.SetParametros;
begin
  inherited;

end;

function TFonteDadosD001.ValidaRegistros: Boolean;
begin

end;
{ TFonteDadosE001 }

procedure TFonteDadosE001.inicializa;
begin
 self.VL_OR := 0;
end;

procedure TFonteDadosE001.preenche;
begin
  inherited;
  self.inicializa;
  FAcbrBlocoE.RegistroE001New.IND_MOV := imComDados;
end;

procedure TFonteDadosE001.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosE001.SetParametros;
begin
  inherited;

end;

function TFonteDadosE001.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosE100 }

procedure TFonteDadosE100.preenche;
begin
  inherited;
  with FAcbrBlocoE.RegistroE100New do
  begin
    DT_INI := FDataINI;
    DT_FIN := FDataFIN;
  end;
end;

procedure TFonteDadosE100.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosE100.SetParametros;
begin
  inherited;

end;

function TFonteDadosE100.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosE110 }

function TFonteDadosE110.get_VL_TOT_CREDITOS(registro:TFonteDados): Currency;
var
  i: integer;
begin
  Result := 0;
  for i := 0 to registro.ComponentCount - 1 do
    if registro.Components[i] is TFonteDados then
    begin
      if not (registro.Components[i] is TFonteDadosE110) then
      begin
        result := result + TFonteDados(registro.Components[i]).VL_TOT_CREDITOS_E110;
        result := result + self.get_VL_TOT_CREDITOS(TFonteDados(registro.Components[i]));
      end;
    end;
end;

function TFonteDadosE110.get_VL_TOT_DEBITOS(registro:TFonteDados;i:Integer;soma:Currency): Currency;
begin
  Result := 0;
  for i := 0 to registro.ComponentCount - 1 do
    if registro.Components[i] is TFonteDados then
    begin
      if not (registro.Components[i] is TFonteDadosE110) then
      begin
        result := result + TFonteDados(registro.Components[i]).valor_total_debitos;
        result := result + self.get_VL_TOT_DEBITOS(TFonteDados(registro.Components[i]));
      end;
    end;
end;

procedure TFonteDadosE110.preenche;
var
  expressaoCampo13:Currency;
  expressaoCampo14:Currency;
  expressaoCampo11:Currency;
  i :Integer;
begin
  inherited;

  with (FAcbrBlocoE.RegistroE110New) do
  begin

    VL_TOT_DEBITOS  := get_VL_TOT_DEBITOS(TFonteDados(Self.Owner));
    VL_TOT_CREDITOS := get_VL_TOT_CREDITOS(TFonteDados(Self.Owner));

    expressaoCampo11 :=  VL_TOT_DEBITOS + (VL_AJ_DEBITOS + VL_TOT_AJ_DEBITOS) + VL_ESTORNOS_CRED - VL_TOT_CREDITOS + (VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) + VL_ESTORNOS_DEB + VL_SLD_CREDOR_ANT;

    if (expressaoCampo11 >= 0) then
      VL_SLD_APURADO := expressaoCampo11
    else
      VL_SLD_APURADO := 0;

    expressaoCampo13 := VL_SLD_APURADO - VL_TOT_DED;

    if (expressaoCampo13 < 0) then
      VL_ICMS_RECOLHER := 0
    else
      VL_ICMS_RECOLHER := expressaoCampo13;


    expressaoCampo14 := VL_TOT_DEBITOS + (VL_AJ_DEBITOS + VL_TOT_AJ_DEBITOS) + VL_ESTORNOS_CRED - VL_TOT_CREDITOS + (VL_AJ_CREDITOS + VL_TOT_AJ_CREDITOS) + VL_ESTORNOS_DEB + VL_SLD_CREDOR_ANT;

    if ( expressaoCampo14 > 0 ) then
      VL_SLD_CREDOR_TRANSPORTAR:=0
    else
      VL_SLD_CREDOR_TRANSPORTAR:=Abs(expressaoCampo14);


    self.VL_OR := self.VL_OR + VL_ICMS_RECOLHER;

  end;

end;

procedure TFonteDadosE110.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosE110.SetParametros;
begin
  inherited;

end;

function TFonteDadosE110.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosE116 }

function TFonteDadosE116.get_VL_OR: currency;
var
  i: integer;
begin

  result:=0;
  for i := 0 to Self.Owner.ComponentCount - 1 do
    if not (Self.Owner.Components[i] is TFonteDadosE116) then
      if Self.Owner.Components[i] is TFonteDadosBlocoE then
        result :=  result + TFonteDadosBlocoE(Self.Owner.Components[i]).VL_OR;
end;

procedure TFonteDadosE116.preenche;
begin
  inherited;
  {REGISTRO E116: OBRIGA��ES DO ICMS A RECOLHER � OPERA��ES PR�PRIAS.}
  Self.ValidaRegistros;
  with (FAcbrBlocoE.RegistroE116New) do
  begin
    COD_OR   := COD_OBRIGACOES_ICMS;
    VL_OR    := self.get_VL_OR;
    DT_VCTO  := FDataFIN;
    COD_REC  := COD_RECEITAESTADUAL;
    IND_PROC := opNenhum;
    MES_REF :=  FormatDateTime('mmyyyy',FDataFIN);
  end;
end;

procedure TFonteDadosE116.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosE116.SetParametros;
begin
  inherited;

end;

function TFonteDadosE116.ValidaRegistros: Boolean;
begin
  Application.ProcessMessages;
  if (COD_OBRIGACOES_ICMS = '')         then  strERROS.Add('C�digo das obriga��es do ICMS n�o pode estar vazio');
  if (Length(COD_OBRIGACOES_ICMS) <> 3) then strERROS.Add('C�digo das obriga��es do ICMS inv�lido: '+self.COD_OBRIGACOES_ICMS);
  if (COD_RECEITAESTADUAL = '')         then strERROS.Add('C�digo de Receita n�o pode estar vazio');
end;

procedure TObjSpedFiscal_Novo.SetCOD_OBRIGACOES_ICMS(const Value: string);
begin
  FCOD_OBRIGACOES_ICMS := CompletaPalavra_a_Esquerda(Value,3,'0');
end;

procedure TObjSpedFiscal_Novo.SetCOD_RECEITAESTADUAL(const Value: string);
begin
  FCOD_RECEITAESTADUAL := Value;
end;

{ TFonteDadosG001 }

procedure TFonteDadosG001.preenche;
begin
  inherited;

  self.FAcbrBlocoG.RegistroG001New.IND_MOV := imSemDados;

  {tem que mandar aqui porq no componente � testado a data ini, se for maiors que 01/01/2001 ele grava o bloco G senao nao grava}

  Self.FAcbrBlocoG.DT_INI := DataINI;
  self.FAcbrBlocoG.DT_FIN := DataFIN;

end;

procedure TFonteDadosG001.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosG001.SetParametros;
begin
  inherited;

end;

function TFonteDadosG001.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosH }

procedure TObjSpedFiscal_Novo.SetgeraRegistroH(const Value: Boolean);
begin
  FgeraRegistroH := Value;
end;

{ TFonteDadosH001 }

procedure TFonteDadosH001.preenche;
begin
  inherited;
  if (FAcbrBlocoH.RegistroH001.RegistroH005.Count <= 0) then
    FAcbrBlocoH.RegistroH001New.IND_MOV := imSemDados
  else
    FAcbrBlocoH.RegistroH001New.IND_MOV := imComDados;
end;

procedure TFonteDadosH001.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosH001.SetParametros;
begin
  inherited;

end;

function TFonteDadosH001.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosH005 }

procedure TFonteDadosH005.preenche;
begin
  inherited;

end;

procedure TFonteDadosH005.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosH005.SetParametros;
begin
  inherited;
end;

function TFonteDadosH005.ValidaRegistros: Boolean;
begin

  if (self.geraRegistroH) then
  begin

    if (DataIniEstoque = 0) or (DataFinEstoque = 0) then
      strErros.Add('Periodo do inventario n�o � valido');
      
  end;

end;

{ TFonteDadosH010 }

procedure TFonteDadosH010.preenche;
var
  _VL_UNIT,_VL_ITEM:Currency;
begin
  inherited;

  if geraRegistroH then
  begin

    valorTotalEstoque := 0;

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrBlocoH.RegistroH005New) do  {componente da erro se chamar H010New sem um pai (registro H005)}
    begin

      VL_INV := 0;
      DT_INV := DataFinEstoque;

      FQuery.First;
      while not FQuery.Eof do
      begin

        with (FAcbrBlocoH.RegistroH010New) do
        begin

          COD_ITEM := FQuery.Fields[0].Text;
          addStr (strItem,COD_ITEM);

          _VL_UNIT  := StrToCurr(get_campoTabela ('custo_consumidor','codigo','TABPRODUTO',FQuery.Fields[0].Text));
          _VL_ITEM  := FQuery.Fields[2].AsCurrency * _VL_UNIT;


          UNID := FQuery.Fields[1].Text;;
          addStr (strUnidade,UpperCase(UNID));

          QTD      := FQuery.Fields[2].AsCurrency;;
          VL_UNIT  := _VL_UNIT;
          VL_ITEM  := _VL_ITEM;

          valorTotalEstoque:=valorTotalEstoque+VL_ITEM;
          IND_PROP := piInformante;

          COD_CTA:='1.01.03.01.01';

          if (QTD < 0) then
            strERROS.Add('Estoque negativo para o produto: '+COD_ITEM+' (Estoque: '+FQuery.Fields[2].text+')');

        end;

        FQuery.Next;

      end;

      if (valorTotalEstoque > 0) then
        VL_INV := valorTotalEstoque;

    end;

  end
  else
  begin
    with (FAcbrBlocoH.RegistroH005New) do
    begin
      DT_INV := DATAFIN;
      VL_INV := 0;
    end;
  end;

end;

procedure TFonteDadosH010.SetCampos;
begin
  inherited;

  FQuery.Fields[0].DisplayLabel := 'Produto';
  FQuery.Fields[1].DisplayLabel := 'Unidade';
  FQuery.Fields[2].DisplayLabel := 'Quantidade';
  FQuery.Fields[3].DisplayLabel := 'QuantidadeConsumo';

  FQuery.Fields[3].Required := False;

  FQuery.Fields[1].OnGetText := GetFieldTrim;

end;

procedure TFonteDadosH010.SetParametros;
var
  objsql:TobjSqlTxt;
begin
  inherited;

  objsql := TobjSqlTxt.Create(Self,ExtractFilePath(Application.ExeName)+'\SqlSped.txt');
  FQuery.SQL.Text := objsql.readString('REGH010');
  FQuery.SQL.Text := StrReplace(FQuery.SQL.Text,':dataInicial',troca(QuotedStr(DateToStr(FDataIniEstoque)),'/','.'));
  FQuery.SQL.Text := StrReplace(FQuery.SQL.Text,':dataFinal',troca(QuotedStr(DateToStr(FDataFinEstoque)),'/','.'));
  FreeAndNil(objsql);

end;

function TFonteDadosH010.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados1001 }

procedure TFonteDados1001.preenche;
begin
  inherited;

  self.status.Panels[1].Text:='Gerando bloco 1 registro 1001...';
  Application.ProcessMessages;
  
  //FAcbrBloco1.Registro1001New.IND_MOV := imSemDados;

  if (FAcbrBloco1.Registro1001.Registro1010.Count <= 0) then
    FAcbrBloco1.Registro1001New.IND_MOV := imSemDados
  else
    FAcbrBloco1.Registro1001New.IND_MOV := imComDados;

end;

procedure TFonteDados1001.SetCampos;
begin
  inherited;

end;

procedure TFonteDados1001.SetParametros;
begin
  inherited;

end;

function TFonteDados1001.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados9001 }

procedure TFonteDados9001.preenche;
begin
  inherited;

  self.status.Panels[1].Text:='Gerando bloco 9 registro 9001...';
  Application.ProcessMessages;
  FAcbrBloco9.Registro9001.IND_MOV := imComDados;
end;

procedure TFonteDados9001.SetCampos;
begin
  inherited;

end;

procedure TFonteDados9001.SetParametros;
begin
  inherited;

end;

function TFonteDados9001.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados9900 }

procedure TFonteDados9900.preenche;
begin
  inherited;
  self.status.Panels[1].Text:='Gerando bloco 9 registro 9900...';
  Application.ProcessMessages;
end;

procedure TFonteDados9900.SetCampos;
begin
  inherited;

end;

procedure TFonteDados9900.SetParametros;
begin
  inherited;

end;

function TFonteDados9900.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados9999 }

procedure TFonteDados9999.preenche;
begin
  inherited;

  self.status.Panels[1].Text:='Gerando bloco 9 registro 9999...';
  Application.ProcessMessages;

end;

procedure TFonteDados9999.SetCampos;
begin
  inherited;

end;

procedure TFonteDados9999.SetParametros;
begin
  inherited;

end;

function TFonteDados9999.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados0190 }

procedure TFonteDados0190.inicializa;
begin

  if FQuery = nil then
    FQuery := TIBQuery.Create(Self);

  FQuery.DisableControls;
  FQuery.Database := FDatabase;
  FQuery.SQL.Text := self.FSQL;

end;

procedure TFonteDados0190.preenche;
var
  _i:Integer;
begin
  inherited;

  self.inicializa;
  for _i:=0  to strUnidade.Count-1  do
  begin

    self.pSigla := strUnidade[_i];

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrBloco0.Registro0190New) do
    begin
      UNID  := strUnidade[_i];
      DESCR := Trim(FQuery.Fields[0].Text);

      if (DESCR = '') then
        strErros.Add('Falta descri��o na unidade: '+UNID);

      if (DESCR = UNID) then
        strErros.Add('Unidade de medida deve ser diferente de sua descri��o. Unidade: '+UNID+'. Descri��o: '+DESCR);

    end;

  end;

end;

procedure TFonteDados0190.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Descri��o';
end;

procedure TFonteDados0190.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsString := pSigla;
end;

function TFonteDados0190.ValidaRegistros: Boolean;
begin
  Result := True;
end;

procedure TObjSpedFiscal_Novo.preencheComponente;
begin

  FReg0000.preenche;
  FReg0005.preenche;
  FReg0100.preenche;
  FRegC100Saida.preenche;
  FRegC100Entrada.preenche;
  //FRegC001.preenche;

  if software = 'PAF' then
  begin

    if (IndPerfil = 'B') or (IndPerfil = 'C') then
      FRegC300.preenche
    else if IndPerfil = 'A' then
      FRegC350.preenche;

    FregC400.preenche;
    
  end;

  FRegC001.preenche;

                                                                                                                                  
  FRegD001.preenche;

  FRegE001.preenche;
  FRegE100.preenche;
  FRegE110.preenche;
  FRegE116.preenche;

  FRegG001.preenche;

  FRegH010.preenche;
  FRegH001.preenche;

  FReg1010.preenche;
  FReg1600.preenche;
  FReg1001.preenche;
  FReg9001.preenche;
  FReg9900.preenche;
  FReg9999.preenche;

  FReg0150C.preenche;
  FReg0150F.preenche;

  FReg0200Produto.preenche; {registro 0200 deve vir primeiro porque utiliza unidade de medida e o registro 0190 s�o as unidades de medida}
  FReg0200Servico.preenche;

  FReg0190.preenche;
  FReg0400.preenche;

  if not (teveErros) then
  begin
    gravaTXT;
    Self.status.Panels[1].Text:='Arquivo sped gerado com sucesso: "'+self.PathArquivo+'"';
  end
  else
    Self.status.Panels[1].Text:='Erro ao gerar arquivo: "'+self.PathArquivo+'"';

  Application.ProcessMessages;
  
end;

function TObjSpedFiscal_Novo.teveErros: Boolean;
begin
  result := (strErros.Count > 0);
end;

procedure TObjSpedFiscal_Novo.preencheSql;
var
  sqlSped:TobjSqlSped;
begin

  self.inicializa;

  sqlSped := TobjSqlSped.Create(self);

  FReg0000.SQL           := sqlSped.ReadString('REG0000');
  FReg0005.SQL           := sqlSped.ReadString('REG0005');
  FReg0100.SQL           := sqlSped.readString('REG0100');
  FReg0150C.SQL          := sqlSped.readString('REG0150C');
  FReg0150F.SQL          := sqlSped.readString('REG0150F');
  FReg0190.SQL           := sqlSped.readString('REG0190');
  FReg0200Produto.SQL    := sqlSped.readString('REG0200 PRODUTO');
  FReg0200Servico.SQL    := sqlSped.readString('REG0200 SERVICO');
  FReg0400.SQL           := sqlSped.readString('REG0400');

  FRegC100Saida.SQL               := sqlSped.readString('REGC100 SAIDA');
  FRegC100Saida.FRegC170.SQL      := sqlSped.readString('REGC170 SAIDA');
  FRegC100Saida.FRegC190CST.SQL   := sqlSped.readString('REGC190 SAIDA CST');
  FRegC100Saida.FRegC190CSOSN.SQL := sqlSped.readString('REGC190 SAIDA CSOSN');

  FRegC100Entrada.SQL               := sqlSped.readString('REGC100 ENTRADA');
  FRegC100Entrada.FRegC170.SQL      := sqlSped.readString('REGC170 ENTRADA');
  FRegC100Entrada.FRegC190CST.SQL   := sqlSped.readString('REGC190 ENTRADA CST');
  FRegC100Entrada.FRegC190CSOSN.SQL := sqlSped.readString('REGC190 ENTRADA CSOSN');

  FRegC300.SQL := sqlSped.readString('REGC300');
  FRegC300.FRegC310.SQL := sqlSped.readString('REGC310');
  FRegC300.FRegC320.SQL := sqlSped.readString('REGC320');
  FRegC300.FRegC320.FRegC321.SQL := sqlSped.readString('REGC321');

  FRegC350.SQL := sqlSped.readString('REGC350');
  FRegC350.FRegC370.SQL := sqlSped.readString('REGC370');
  FRegC350.FRegC390.SQL := sqlSped.readString('REGC390');

  FregC400.SQL := sqlSped.readString('REGC400');
  FregC400.FRegC405.SQL := sqlSped.readString('REGC405');
  FregC400.FRegC405.FRegC410.SQL := sqlSped.readString('REGC410');
  FregC400.FRegC405.FRegC420.SQL := sqlSped.readString('REGC420');
  FregC400.FRegC405.FRegC420.FRegC425.SQL := sqlSped.readString('REGC425');
  FregC400.FRegC405.FregC460.SQL := sqlSped.readString('REGC460');
  FregC400.FRegC405.FregC460.FRegC470Produto.SQL := sqlSped.readString('REGC470 PRODUTO');
  FregC400.FRegC405.FregC460.FRegC470Servico.SQL := sqlSped.readString('REGC470 SERVICO');
  FregC400.FRegC405.FRegC490Produto.SQL := sqlSped.readString('REGC490 PRODUTO');
  FregC400.FRegC405.FRegC490Servico.SQL := sqlSped.readString('REGC490 SERVICO');

  FRegH010.SQL := sqlSped.readString('REGH010');
  FReg1010.SQL := sqlSped.readString('REG1010');
  FReg1600.SQL := sqlSped.readString('REG1600');

  FreeAndNil(sqlSped);
  
end;
{ TFonteDadosC001 }
procedure TFonteDadosC001.preenche;
begin
  inherited;

  {if FAcbrBlocoC.RegistroC001New.RegistroC400.Count <= 0 then
    FAcbrBlocoC.RegistroC001New.IND_MOV := imSemDados
  else
    FAcbrBlocoC.RegistroC001New.IND_MOV := imComDados;

  if (FAcbrBlocoC.RegistroC001.RegistroC100.Count <= 0) then
    FAcbrBlocoC.RegistroC001New.IND_MOV := imSemDados
  else
    FAcbrBlocoC.RegistroC001New.IND_MOV := imComDados; }

  if (FAcbrBlocoC.RegistroC001New.RegistroC100.Count <=0) and (FAcbrBlocoC.RegistroC001New.RegistroC400.Count <= 0)
  and (FAcbrBlocoC.RegistroC001New.RegistroC300.Count <= 0) and (FAcbrBlocoC.RegistroC001New.RegistroC350.Count <= 0)then
    FAcbrBlocoC.RegistroC001New.IND_MOV := imSemDados
  else
    FAcbrBlocoC.RegistroC001New.IND_MOV := imComDados;

end;

procedure TFonteDadosC001.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC001.SetParametros;
begin
  inherited;
end;

function TFonteDadosC001.ValidaRegistros: Boolean;
begin
end;

procedure TObjSpedFiscal_Novo.Setsoftware(const Value: string);
begin
  Fsoftware := Value;
end;

function TObjSpedFiscal_Novo.geraSped: Boolean;
begin
  preencheSql;
  preencheComponente;
  Result := not (teveErros);
end;

{ TFonteDadosC300 }

constructor TFonteDadosC300.create(AOwner: TComponent);
begin
inherited;
end;

destructor TFonteDadosC300.destroy;
begin
  FreeAndNil(FregC310);
  FreeAndNil(FregC320);
  inherited;
end;

function TFonteDadosC300.get_NFFINAL(pData: string): string;
var
  queryTemp:TIBQuery;
begin

  queryTemp := TIBQuery.Create(nil);

  try
    queryTemp.Database := FQuery.Database;
    pData := troca(pData,'/','.');
    queryTemp.Active:=False;
    queryTemp.SQL.Clear;
    queryTemp.SQL.Add('select MAX (cast (v.nf as integer)) as NFFINAL');
    queryTemp.SQL.Add('from tabvenda v');
    queryTemp.SQL.Add('where (v.data = '+#39+pData+#39+') and (v.nf <> '''')');
    queryTemp.Active:=True;
    Result:=queryTemp.fieldbyname('NFFINAL').AsString;
  finally
    FreeAndNil(queryTemp);
  end;

end;

function TFonteDadosC300.get_NFINICIAL(pData: string): string;
var
  queryTemp:TIBQuery;
begin

  queryTemp := TIBQuery.Create(nil);

  try
    queryTemp.Database := FQuery.Database;
    pData :=troca(pData,'/','.');
    queryTemp.Active:=False;
    queryTemp.SQL.Clear;
    queryTemp.SQL.Add('select MIN (cast (v.nf as integer)) as NFINICIAL');
    queryTemp.SQL.Add('from tabvenda v');
    queryTemp.SQL.Add('where (v.data = '+#39+pData+#39+') and (v.nf <> '''')');
    queryTemp.Active:=True;
    Result:=queryTemp.fieldbyname('NFINICIAL').AsString;
  finally
    FreeAndNil(queryTemp);
  end;
end;

procedure TFonteDadosC300.inicializa;
var
  i:Integer;
begin

  FRegC310 := TFonteDadosC310.Create(self);
  FRegC310.registro := 'C310';

  FRegC320 := TFonteDadosC320.Create(self);
  FRegC320.Registro := 'C320';

  for i := 0  to self.ComponentCount-1  do
  begin

    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(Self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;

    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;

    if self.Components[i] is TFonteDadosC320 then
      TFonteDadosC320(self.Components[i]).inicializa;

  end;

end;

procedure TFonteDadosC300.preenche;{queryNF_02}
var
  pVALORFINAL,pVALORCOFINS,pVALORPIS:Currency;
  pModelo,pData,pSerieNF:string;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin
    pVALORFINAL  := FQuery.Fields[5].AsCurrency;
    pVALORCOFINS := StrToCurr (get_campoTabela('SUM(valorcofins) as VL_COFINS','venda','TABPRODUTOSVENDA',FQuery.Fields[0].Text,'VL_COFINS'));
    pVALORPIS    := StrToCurr (get_campoTabela('SUM(valorpis) as VL_PIS','venda','TABPRODUTOSVENDA',FQuery.Fields[0].Text,'VL_PIS'));

    repeat

      pMODELO   := FQuery.Fields[1].Text;
      pDATA     := FQuery.Fields[4].text;
      pSerieNF  := FQuery.Fields[2].Text;

    until not (registroCorrespondenteNF_02(pMODELO+pSerieNF+pDATA,pVALORFINAL,pVALORPIS,pVALORCOFINS,FQuery) and not FQuery.Eof);

    with (FAcbrBlocoC.RegistroC300New) do
    begin
      COD_MOD     := pMODELO;
      SER         := pSerieNF;
      SUB         := FQuery.Fields[3].Text;
      NUM_DOC_INI := self.get_NFINICIAL(pDATA);
      NUM_DOC_FIN := self.get_NFFINAL(pDATA);
      DT_DOC      := StrToDate (pDATA);
      VL_DOC      := pVALORFINAL;
      VL_PIS      := pVALORPIS;
      VL_COFINS   := pVALORCOFINS;
    end;

    {C310 notas canceladas do periodo}
    FRegC310.DataVenda := StrToDate(pData);
    FRegC310.preenche;

    {C320 REGISTRO ANAL�TICO DO RESUMO DI�RIO DAS NOTAS FISCAIS DE VENDA A CONSUMIDOR (C�DIGO 02) CST_ICMS, CFOP e Al�quota de ICMS.}
    FRegC320.dataVenda := StrToDate(pData);
    FRegC320.preenche;

    FQuery.Next;

  end;

end;

function TFonteDadosC300.registroCorrespondenteNF_02(parametro: string;
  var valorFinal, valorpis, valorcofins: Currency;
  query: TIBQuery): Boolean;
begin

  query.Next;
  if not (query.Eof) then
  begin

    if (parametro = FQuery.Fields[1].Text+FQuery.Fields[2].Text+FQuery.Fields[4].Text) then
    begin

      valorFinal  := valorFinal  + FQuery.Fields[5].AsCurrency;
      valorcofins := valorcofins + StrToCurr (get_campoTabela('SUM(valorcofins) as VL_COFINS','venda','TABPRODUTOSVENDA',FQuery.Fields[0].Text,'VL_COFINS'));
      valorpis    := valorpis    + StrToCurr (get_campoTabela('SUM(valorpis) as VL_PIS','venda','TABPRODUTOSVENDA',FQuery.Fields[0].Text,'VL_PIS'));

      Result := True;
    end
    else
      result := False;

  end;
end;

procedure TFonteDadosC300.SetCampos;
begin
  inherited;

  FQuery.Fields[0].DisplayLabel := 'Cod. Venda';
  FQuery.Fields[1].DisplayLabel := 'Modelo NF';
  FQuery.Fields[2].DisplayLabel := 'S�rie NF';
  FQuery.Fields[3].DisplayLabel := 'Sub-s�rie NF';
  FQuery.Fields[4].DisplayLabel := 'Data venda';
  FQuery.Fields[5].DisplayLabel := 'Valor final';

  FQuery.Fields[4].OnGetText := GetFieldTrim;
  FQuery.Fields[3].Required := False;

end;

procedure TFonteDadosC300.SetParametros;
begin
  inherited;
end;

function TFonteDadosC300.ValidaRegistros: Boolean;
begin
Result := True;
end;

{ TFonteDadosC310 }

procedure TFonteDadosC310.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not (FQuery.Eof) do
  begin

    with (FAcbrBlocoC.RegistroC310New) do
      NUM_DOC_CANC := FQuery.Fields[0].Text;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC310.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'N�mero NF';
end;

procedure TFonteDadosC310.SetParametros;
begin
  FQuery.Params[0].AsDateTime := DataVenda;
end;

procedure TFonteDadosC310.SetDataVenda(const Value: TDateTime);
begin
  FDataVenda := Value;
end;

function TFonteDadosC310.ValidaRegistros: Boolean;
begin

end;

{ TobjSqlSped }

procedure TobjSqlSped.fechaArquivo;
begin
  system.CloseFile(arq);
end;

constructor TobjSqlSped.Create(AOwner: TComponent;pathArq:string);
begin
  self.str := TStringList.Create;
  self.pathArq := pathArq;
  self.inicializa;
end;

destructor TobjSqlSped.destroy;
begin
  FreeAndNil(self.str);
  CloseFile(arq);
  inherited;
end;

procedure TobjSqlSped.inicializa;
begin
  if pathArq = '' then
    pathArq := ExtractFilePath(Application.ExeName)+'\SqlSped.txt';

  if FileExists(pathArq) then
  begin
    AssignFile(self.arq,self.pathArq);
    Reset(arq);
  end
end;

function TobjSqlSped.readString(const sectionName: string): string;
var
  info: string;
begin
  if not contemSection(sectionName) then Exit;
  while not Eof(self.arq) do
  begin
    readln(arq,info);
    if (Pos(sectionName,info)) <> 0 then
    begin
      Readln(self.arq,info);
      while (Pos(';',info)) = 0 do
      begin
        Trim(info);
        Result := Result + info+' ';
        Readln(arq,info);
      end;
      Result := Result + info;
      Exit;
    end;
  end;
end;

function TobjSqlSped.contemSection(const value: string):Boolean;
begin
  str.Text := '';
  str.LoadFromFile(self.pathArq);
  Result := (Pos(value,str.Text) <> 0);
end;

procedure TobjSqlSped.SetpathArq(const Value: string);
begin
  FpathArq := Value;
end;

{ TFonteDadosC320 }

constructor TFonteDadosC320.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC320.destroy;
begin
  FreeAndNil(FRegC321);
  inherited;
end;

procedure TFonteDadosC320.inicializa;
var
  i:Integer;
begin

  FRegC321 := TFonteDadosC321.Create(self);
  FRegC321.Registro := 'C321';

  for i := 0  to self.ComponentCount-1  do
  begin
    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;
  end;

end;

procedure TFonteDadosC320.preenche;
var
  ALIQ_ICMS:Currency;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not (FQuery.Eof) do
  begin

    with FAcbrBlocoC.RegistroC320New do
    begin

      CST_ICMS    := CompletaPalavra(FQuery.Fields[0].Text,1,'0') + CompletaPalavra((FQuery.Fields[1].Text),2,'0');
      CFOP        := FQuery.Fields[2].Text;
      ALIQ_ICMS   := FQuery.Fields[3].AsCurrency;
      VL_OPR      := FQuery.Fields[4].AsCurrency;

      if (ALIQ_ICMS > 0) then
      begin
        VL_BC_ICMS  := FQuery.Fields[5].AsCurrency;
        VL_ICMS     := FQuery.Fields[6].AsCurrency;
        valor_total_debitos := valor_total_debitos+VL_ICMS;
      end;

      VL_RED_BC   := FQuery.Fields[7].AsCurrency;

    end;

    if indPerfil <> 'C' then
    begin
      FRegC321.Origem    := FQuery.Fields[0].Text;
      FRegC321.CST       := FQuery.Fields[1].Text;
      FRegC321.CFOP      := FQuery.Fields[2].Text;
      FRegC321.Aliquota  := FQuery.Fields[3].Text;
      FRegC321.dataVenda := self.dataVenda;
      FRegC321.preenche;
    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC320.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Origem';
  FQuery.Fields[1].DisplayLabel := 'CST';
  FQuery.Fields[2].DisplayLabel := 'CFOP';
  FQuery.Fields[3].DisplayLabel := 'Aliquota';
  FQuery.Fields[4].DisplayLabel := 'Valor final';
  FQuery.Fields[5].DisplayLabel := 'BC_ICMS';
  FQuery.Fields[6].DisplayLabel := 'VL_ICMS';
  FQuery.Fields[7].DisplayLabel := 'VL_RED_BC';

  FQuery.Fields[3].Required := False;
  FQuery.Fields[5].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;
end;

procedure TFonteDadosC320.SetdataVenda(const Value: TDateTime);
begin
  FdataVenda := Value;
end;

procedure TFonteDadosC320.SetParametros;
begin
  FQuery.Params[0].AsDateTime := dataVenda;
end;

function TFonteDadosC320.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC321 }

procedure TFonteDadosC321.preenche;
var
  VL_PIS,VL_COFINS,VL_BC_ICMS,VL_ICMS:Currency;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not (FQuery.Eof) do
  begin

    with (FAcbrBlocoC.RegistroC321New) do
    begin

      COD_ITEM    := FQuery.Fields[0].Text;
      QTD         := FQuery.Fields[1].AsCurrency;
      UNID        := FQuery.Fields[2].Text;
      VL_ITEM     := FQuery.Fields[3].AsCurrency;
      VL_DESC     := FQuery.Fields[4].AsCurrency;

      if (FQuery.Fields[9].AsCurrency > 0) then
      begin
        VL_BC_ICMS  := FQuery.Fields[5].AsCurrency;
        VL_ICMS     := FQuery.Fields[6].AsCurrency;
      end;

      VL_PIS      := FQuery.Fields[7].AsCurrency;
      VL_COFINS   := FQuery.Fields[8].AsCurrency;

      addStr(strItem,COD_ITEM);
      addStr(strUnidade,UpperCase(UNID));

    end;
    FQuery.Next;
  end;

end;

procedure TFonteDadosC321.SetAliquota(const Value: string);
begin
  FAliquota := Value;
end;

procedure TFonteDadosC321.SetCampos;
begin
  inherited;

  FQuery.Fields[0].DisplayLabel := 'Produto';
  FQuery.Fields[1].DisplayLabel := 'Quantidade';
  FQuery.Fields[2].DisplayLabel := 'Unidade';
  FQuery.Fields[3].DisplayLabel := 'valor final';
  FQuery.Fields[4].DisplayLabel := 'Desconto';
  FQuery.Fields[5].DisplayLabel := 'BC_ICMS';
  FQuery.Fields[6].DisplayLabel := 'VL_ICMS';
  FQuery.Fields[7].DisplayLabel := 'VL PIS';
  FQuery.Fields[8].DisplayLabel := 'VL COFINS';
  FQuery.Fields[9].DisplayLabel := 'Aliquota';

  FQuery.Fields[4].Required := False;
  FQuery.Fields[5].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[9].Required := False;

  FQuery.Fields[2].OnGetText := GetFieldTrim;

end;

procedure TFonteDadosC321.SetCFOP(const Value: string);
begin
  FCFOP := Value;
end;

procedure TFonteDadosC321.SetCST(const Value: string);
begin
  FCST := Value;
end;

procedure TFonteDadosC321.SetdataVenda(const Value: TDateTime);
begin
  FdataVenda := Value;
end;

procedure TFonteDadosC321.SetOrigem(const Value: string);
begin
  FOrigem := Value;
end;

procedure TFonteDadosC321.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsDateTime := dataVenda;
  FQuery.Params[1].AsString   := FOrigem;
  FQuery.Params[2].AsString   := FCST;
  FQuery.Params[3].AsString   := FCFOP;
  FQuery.Params[4].AsString   := FAliquota;
end;

function TFonteDadosC321.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC350 }

constructor TFonteDadosC350.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC350.destroy;
begin
  FreeAndNil(FRegC370);
  FreeAndNil(FRegC390);
  inherited;
end;

procedure TFonteDadosC350.inicializa;
var
  i:Integer;
begin

  FRegC370 := TFonteDadosC370.Create(self);
  FRegC370.registro := 'C370';

  FRegC390 := TFonteDadosC390.Create(self);
  FRegC390.Registro := 'C390';

  for i := 0  to self.ComponentCount-1  do
  begin
    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;
      
  end;       

end;

procedure TFonteDadosC350.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC350New) do
    begin
      SER        := FQuery.Fields[1].Text;
      SUB_SER    := FQuery.Fields[2].Text;
      NUM_DOC    := FQuery.Fields[3].Text;
      DT_DOC     := FQuery.Fields[4].AsDateTime;
      CNPJ_CPF   := FQuery.Fields[5].Text;
      VL_MERC    := FQuery.Fields[6].AsCurrency;
      VL_DOC     := FQuery.Fields[7].AsCurrency;
      VL_DESC    := FQuery.Fields[8].AsCurrency;
      VL_PIS     := StrToCurr (get_campoTabela('SUM(valorpis) as VL_PIS','venda','TABPRODUTOSVENDA',FQuery.Fields[0].Text,'VL_PIS'));
      VL_COFINS  := StrToCurr (get_campoTabela('SUM(valorcofins) as VL_COFINS','venda','TABPRODUTOSVENDA',FQuery.Fields[0].Text,'VL_COFINS'));
    end;

    FRegC370.CodigoVenda := FQuery.Fields[0].AsInteger;
    FRegC370.preenche;

    FRegC390.CodigoVenda := FQuery.Fields[0].AsInteger;
    FRegC390.preenche;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC350.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo';
  FQuery.Fields[1].DisplayLabel := 'Serie NF';
  FQuery.Fields[2].DisplayLabel := 'Sub-S�rie NF';
  FQuery.Fields[3].DisplayLabel := 'N�mero NF';
  FQuery.Fields[4].DisplayLabel := 'Data venda';
  FQuery.Fields[5].DisplayLabel := 'CPF CGC';
  FQuery.Fields[6].DisplayLabel := 'Valor total';
  FQuery.Fields[7].DisplayLabel := 'Valor final';
  FQuery.Fields[8].DisplayLabel := 'Desconto';

  FQuery.Fields[2].Required := False;
  FQuery.Fields[8].Required := False;

  FQuery.Fields[5].OnGetText := GetFieldSoNumeros;

end;

procedure TFonteDadosC350.SetParametros;
begin
  inherited;
end;

function TFonteDadosC350.ValidaRegistros: Boolean;
begin

  if not (ValidaCPF(FQuery.fields[5].Text)) and not (ValidaCNPJ(FQuery.fields[5].Text)) then
    strErros.Add('CPF/CNPJ inv�lido. Registro: '+self.Registro); 

end;

{ TFonteDadosC370 }

procedure TFonteDadosC370.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC370New) do
    begin

      NUM_ITEM    := IntToStr (FQuery.RecNo);
      COD_ITEM    := FQuery.Fields[0].Text;
      QTD         := FQuery.Fields[1].AsCurrency;
      UNID        := FQuery.Fields[2].Text;
      VL_ITEM     := FQuery.Fields[3].AsCurrency;
      VL_DESC     := FQuery.Fields[4].AsCurrency;

      addStr(strItem,COD_ITEM);
      addStr(strUnidade,UpperCase(UNID));

    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC370.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo produto';
  FQuery.Fields[1].DisplayLabel := 'Quantidade';
  FQuery.Fields[2].DisplayLabel := 'Unidade';
  FQuery.Fields[3].DisplayLabel := 'Valor final';
  FQuery.Fields[4].DisplayLabel := 'Desconto';

  FQuery.Fields[4].Required := False;
  FQuery.Fields[2].OnGetText := GetFieldTrim;

end;

procedure TFonteDadosC370.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.CodigoVenda;
end;

function TFonteDadosC370.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC390 }

procedure TFonteDadosC390.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC390New) do
    begin

      CST_ICMS    := CompletaPalavra (FQuery.Fields[0].Text,1,'0') + CompletaPalavra((FQuery.Fields[1].Text),2,'0');
      CFOP        := FQuery.Fields[2].Text;
      ALIQ_ICMS   := FQuery.Fields[3].AsCurrency;
      VL_OPR      := FQuery.Fields[4].AsCurrency;

      if (ALIQ_ICMS > 0) then
      begin
        VL_BC_ICMS  := FQuery.Fields[5].AsCurrency;
        VL_ICMS     := FQuery.Fields[6].AsCurrency;
        {CAMPO 2 do registro E110}
        valor_total_debitos := valor_total_debitos+VL_ICMS;
      end;
      VL_RED_BC   := FQuery.Fields[7].AsCurrency;
    end;
    
    FQuery.Next

  end;

end;

procedure TFonteDadosC390.SetCampos;
begin
  inherited;

  FQuery.Fields[0].DisplayLabel := 'Origem';
  FQuery.Fields[1].DisplayLabel := 'CST';
  FQuery.Fields[2].DisplayLabel := 'CFOP';
  FQuery.Fields[3].DisplayLabel := 'Aliquota';
  FQuery.Fields[4].DisplayLabel := 'Valor final';
  FQuery.Fields[5].DisplayLabel := 'BC_ICMS';
  FQuery.Fields[6].DisplayLabel := 'VL_ICMS';
  FQuery.Fields[7].DisplayLabel := 'VL_RED_BC';

  FQuery.Fields[3].Required := False;
  FQuery.Fields[5].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;
end;

procedure TFonteDadosC390.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.CodigoVenda;
end;

function TFonteDadosC390.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC400 }

constructor TFonteDadosC400.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC400.destroy;
begin
  FreeAndNil(FRegC405);
  inherited;
end;

procedure TFonteDadosC400.inicializa;
var
  i:Integer;
begin

  FRegC405 := TFonteDadosC405.Create(self);
  FRegC405.Registro := 'C405';

  for i := 0  to self.ComponentCount-1  do
  begin

    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(Self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(Self.Components[i]).pCOFINS := self.pCOFINS;
    end;

    if self.Components[i] is TFonteDadosC405 then
      TFonteDadosC405(self.Components[i]).inicializa;

  end;
end;

procedure TFonteDadosC400.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC400New) do
    begin
      COD_MOD := '2D';
      ECF_MOD := FQuery.Fields[1].Text;
      ECF_FAB := FQuery.Fields[2].Text;
      ECF_CX  := RightStr(FQuery.Fields[3].Text,3);
    end;

    FRegC405.codigoECF := FQuery.Fields[0].AsInteger;
    FRegC405.preenche;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC400.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo ECF';
  FQuery.Fields[1].DisplayLabel := 'Modelo ECF';
  FQuery.Fields[2].DisplayLabel := 'N�mero fabrica��o ECF';
  FQuery.Fields[3].DisplayLabel := 'Numero sequencial';

  FQuery.Fields[1].OnGetText := GetFieldTrim;
  FQuery.Fields[2].OnGetText := GetFieldTrim;

end;

procedure TFonteDadosC400.SetParametros;
begin
  inherited;
end;

function TFonteDadosC400.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC425 }

function TFonteDadosC425.acerta_c420_c425(var str: TStringList;list: TIBQuery; valorC420: Currency): Boolean;
var
  totalItensC425:Currency;
  i,contProduto:integer;
begin

  result:= False;

  try

    str.Clear;
    list.FetchAll;
    contProduto:=list.RecordCount;

    if (contProduto = 0) then Exit;

    list.First;
    totalItensC425:=0;
    while not (list.Eof) do
    begin

      str.Add(list.Fields[4].text);
      totalItensC425 := totalItensC425 + list.Fields[4].AsCurrency;
      list.Next;

    end;

    while (totalItensC425 < valorC420) do
    begin

        totalItensC425:=0;
        str[0]:=CurrToStr (StrToCurr(str[0])+0.01);

        for i:= 0 to contProduto-1 do
          totalItensC425:=totalItensC425+StrToCurr(str[i]);

    end;

    while (totalItensC425 > valorC420) do
    begin

        totalItensC425:=0;
        str[0]:=CurrToStr (StrToCurr(str[0])-0.01);

        for i:= 0 to contProduto-1 do
          totalItensC425:=totalItensC425+StrToCurr(str[i]);

    end;

    if (totalItensC425 <> valorC420) then
    begin

      MensagemErro('Erro no rateio do registro C420_C425. Total C420: '+CurrToStr(valorC420)+' Total C425: '+CurrToStr(totalItensC425));
      Exit;

    end;

  except

    Exit;

  end;

  result:= True;

end;

constructor TFonteDadosC425.create(AOwner: TComponent);
begin
  inherited;
  self.strVL_C425 := TStringList.Create;
end;

destructor TFonteDadosC425.destroy;
begin
  FreeAndNil(strVL_C425);
  inherited;
end;

procedure TFonteDadosC425.preenche;
var
  i:Integer;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  i:=0;
  self.acerta_c420_c425(strVL_C425,FQuery,self.valorTotalC420);
  FQuery.First;
  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC425New) do
    begin

      if (FQuery.Fields[0].Text <> '') then
      begin
        COD_ITEM := FQuery.Fields[0].Text;
        addStr(strItem,COD_ITEM);
      end
      else
      begin
        COD_ITEM := FQuery.Fields[3].Text;
        addStr(strServico,COD_ITEM);
      end;

      QTD      := FQuery.Fields[1].AsCurrency;
      UNID     := FQuery.Fields[2].Text;
      VL_ITEM  := StrToCurr(strVL_C425[i]);

      addStr(strUnidade,UpperCase(UNID));

    end;
    i:=i+1;
    FQuery.Next
  end;

end;

procedure TFonteDadosC425.SetCampos;
begin
  inherited;

  FQuery.Fields[0].DisplayLabel := 'Produto';
  FQuery.Fields[1].DisplayLabel := 'Quantidade';
  FQuery.Fields[2].DisplayLabel := 'Unidade';
  FQuery.Fields[3].DisplayLabel := 'Servi�o';
  FQuery.Fields[4].DisplayLabel := 'Valor final';
  FQuery.Fields[5].DisplayLabel := 'Totalizador parcial';

  FQuery.Fields[0].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[2].OnGetText := GetFieldTrim;

end;

procedure TFonteDadosC425.SetParametros;
begin
  FQuery.Params[0].AsDateTime := self.dataMovimentoC405;
  FQuery.Params[1].AsString := self.totalizadorParcialC420;
  FQuery.Params[2].AsInteger := self.ECF;
end;

function TFonteDadosC425.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC405 }

constructor TFonteDadosC405.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC405.destroy;
begin
  FreeAndNil(FRegC410);
  FreeAndNil(FRegC420);
  FreeAndNil(FregC460);
  FreeAndNil(FRegC490Produto);
  FreeAndNil(FRegC490Servico);
  inherited;
end;

procedure TFonteDadosC405.inicializa;
var
  i:Integer;
begin

  FRegC410 := TFonteDadosC410.Create(self);
  FRegC410.registro := 'C410';

  FRegC420 := TFonteDadosC420.Create(self);
  FRegC420.Registro := 'C420';

  FregC460 := TFonteDadosC460.create(self);
  FregC460.Registro := 'C460';

  FRegC490Produto := TFonteDadosC490Produto.Create(self);
  FRegC490Produto.Registro := 'C490 PRODUTO';

  FRegC490Servico := TFonteDadosC490Servico.Create(self);
  FRegC490Servico.Registro := 'C490 SERVI�O';

  for i := 0  to self.ComponentCount-1  do
  begin
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(Self.Components[i]).pPIS := self.pPIS;
      TFonteDadosBlocoC(Self.Components[i]).pCOFINS := self.pCOFINS;
    end;

    if self.Components[i] is TFonteDadosC420 then
      TFonteDadosC420(self.Components[i]).inicializa;

    if self.Components[i] is TFonteDadosC460 then
      TFonteDadosC460(self.Components[i]).inicializa;

  end;

end;

procedure TFonteDadosC405.preenche;
begin
  inherited;
  
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC405New) do
    begin
      CRO         := FQuery.Fields[1].AsInteger;
      CRZ         := FQuery.Fields[2].AsInteger;
      NUM_COO_FIN := FQuery.Fields[3].AsInteger;
      DT_DOC      := FQuery.Fields[4].AsDateTime;
      VL_BRT      := FQuery.Fields[5].AsCurrency;
      GT_FIN      := FQuery.Fields[6].AsCurrency;
    end;

    if FQuery.Fields[5].AsCurrency > 0 then
    begin
      if (indPerfil <> 'C') then
      begin
        FRegC410.dataMovimento := FQuery.Fields[4].AsDateTime;
        FRegC410.preenche;
      end;
    end;

    FRegC420.reducaoZ      := FQuery.Fields[0].AsInteger;
    FRegC420.dataMovimento := FQuery.Fields[4].AsDateTime; {usado no C425}
    FRegC420.ECF := self.codigoECF;
    FRegC420.preenche;
    if (indPerfil = 'A') then  {somente perfil A monta o registro C460,C470}
    begin
      FregC460.dataMovimento := FQuery.Fields[4].AsDateTime;
      FregC460.ECF := self.codigoECF;
      FregC460.preenche;
    end;
    FRegC490Produto.dataMovimento := FQuery.Fields[4].AsDateTime;
    FRegC490Produto.ECF := self.codigoECF;
    FRegC490Produto.preenche;

    FRegC490Servico.dataMovimento := FQuery.Fields[4].AsDateTime;
    FRegC490Servico.ECF := self.codigoECF;
    FRegC490Servico.preenche;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC405.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo RZ';
  FQuery.Fields[1].DisplayLabel := 'CRO';
  FQuery.Fields[2].DisplayLabel := 'CRZ';
  FQuery.Fields[3].DisplayLabel := 'COO';
  FQuery.Fields[4].DisplayLabel := 'Data movimento';
  FQuery.Fields[5].DisplayLabel := 'VL_BRT';
  FQuery.Fields[6].DisplayLabel := 'GT_FIM';
end;

procedure TFonteDadosC405.SetParametros;
begin
  inherited;
  FQuery.Params[2].AsInteger := self.codigoECF;
end;

function TFonteDadosC405.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC410 }

procedure TFonteDadosC410.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC410New) do
    begin
      VL_PIS    := FQuery.Fields[0].AsCurrency;
      VL_COFINS := FQuery.Fields[1].AsCurrency;
    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC410.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Valor PIS';
  FQuery.Fields[1].DisplayLabel := 'valor COFINS';
end;

procedure TFonteDadosC410.SetParametros;
begin
  FQuery.Params[0].AsDateTime :=  self.dataMovimento; 
end;

function TFonteDadosC410.ValidaRegistros: Boolean;
begin
  inherited;
end;

{ TFonteDadosC420 }

constructor TFonteDadosC420.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC420.destroy;
begin
  FreeAndNil(FregC425);
  inherited;
end;

procedure TFonteDadosC420.inicializa;
var
  i:integer;
begin
  FRegC425 := TFonteDadosC425.Create(self);
  FRegC425.Registro := 'C425';

  for i := 0  to self.ComponentCount-1  do
  begin
    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;

  end;

end;

procedure TFonteDadosC420.preenche;
var
  i:integer;
begin
  
  inherited;
  if not abrequery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC420New) do
    begin

      COD_TOT_PAR  := FQuery.Fields[0].Text;

      if (Pos('DT',COD_TOT_PAR) <> 0) then{componente n�o aceita DT1}
        COD_TOT_PAR := COD_TOT_PAR[1] + COD_TOT_PAR[2];

      VLR_ACUM_TOT := FQuery.Fields[1].AsCurrency;

      if (Copy(COD_TOT_PAR,3,1) = 'T') or (Copy(COD_TOT_PAR,3,1) = 'S')  then
      begin

        NR_TOT       := StrToInt(COD_TOT_PAR[1] + COD_TOT_PAR[2]);
        DESCR_NR_TOT := 'Totalizador ' + IntToStr(NR_TOT);

      end;

      self.valorTotalC420 := VLR_ACUM_TOT;

    end;

    if(indPerfil <> 'A') then {quem � perfil A n�o pode montar o registro C425}
    begin
      if (Pos('DT',FQuery.Fields[0].Text) = 0) or ((Pos('Can',FQuery.Fields[0].Text) = 0)) then {totalizadores DT,Can-T n�o � obrigado}
      begin
        FRegC425.dataMovimentoC405      := self.dataMovimento;
        FRegC425.totalizadorParcialC420 := FQuery.Fields[0].Text;
        FRegC425.valorTotalC420 := self.valorTotalC420;
        FRegC425.ECF := self.ECF;
        FRegC425.preenche;
      end;
    end;


    FQuery.Next;

  end;
end;

procedure TFonteDadosC420.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Totalizador parcial';
  FQuery.Fields[1].DisplayLabel := 'Valor acumulado';
end;

procedure TFonteDadosC420.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.reducaoZ;
end;

function TFonteDadosC420.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC460 }

constructor TFonteDadosC460.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC460.destroy;
begin
  FreeAndNil(FRegC470Produto);
  FreeAndNil(FRegC470Servico);
  inherited;
end;

procedure TFonteDadosC460.inicializa;
var
  i:Integer;
begin

  FRegC470Produto := TFonteDadosC470Produto.create(self);
  FRegC470Produto.Registro := 'C470 PRODUTO';

  FRegC470Servico := TFonteDadosC470Servico.Create(self);
  FRegC470Servico.Registro := 'C470 SERVI�O';

  for i := 0  to self.ComponentCount-1  do
  begin
    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;

  end; 

end;

procedure TFonteDadosC460.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.eof do
  begin

    with (FAcbrBlocoC.RegistroC460New) do
    begin

      COD_MOD   := '2D';
      COD_SIT   := getSituacaoNF(FQuery.Fields[0].Text);
      NUM_DOC   := FQuery.Fields[1].Text;

      if (FQuery.Fields[0].Text = 'N') then {cupom cancelado informar somente os campos COD_MOD, COD_SIT e NUM_DOC, sem os registros filhos.}
      begin

        DT_DOC    := FQuery.Fields[2].AsDateTime;
        VL_DOC    := FQuery.Fields[3].AsCurrency;
        VL_PIS    := StrToCurr(get_campoTabela('SUM(valorpis) as VL_PIS','venda','TABPRODUTOSVENDA',FQuery.Fields[4].Text,'VL_PIS'));
        VL_COFINS := StrToCurr(get_campoTabela('SUM(valorpis) as VL_COFINS','venda','TABPRODUTOSVENDA',FQuery.Fields[4].Text,'VL_COFINS'));
        CPF_CNPJ  := FQuery.Fields[5].Text;
        NOM_ADQ   := FQuery.Fields[7].Text

      end;

    end;
    if (FQuery.Fields[0].Text = 'N') then
    begin

      FRegC470Produto.codigoVenda := FQuery.Fields[4].AsInteger;
      FRegC470Produto.preenche;

      FRegC470Servico.codigoVenda := FQuery.Fields[4].AsInteger;
      FRegC470Servico.preenche;

    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC460.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Cancelado S/N';
  FQuery.Fields[1].DisplayLabel := 'Cupom';
  FQuery.Fields[2].DisplayLabel := 'Data venda';
  FQuery.Fields[3].DisplayLabel := 'Valor final';
  FQuery.Fields[4].DisplayLabel := 'C�digo Venda';
  FQuery.Fields[5].DisplayLabel := 'CPF/CGC';
  FQuery.Fields[6].DisplayLabel := 'Fisica juridica';
  FQuery.Fields[7].DisplayLabel := 'Nome cliente';

  FQuery.Fields[5].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[5].Required := false;
end;

procedure TFonteDadosC460.SetParametros;
begin
  FQuery.Params[0].AsDateTime := self.dataMovimento;
  FQuery.Params[1].AsInteger  := self.ECF;
end;

function TFonteDadosC460.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC470 }

procedure TFonteDadosC470.preenche;
begin
  inherited;
end;

procedure TFonteDadosC470.SetCampos;
begin
  inherited;

  FQuery.Fields[1].DisplayLabel := 'Quantidade';
  FQuery.Fields[2].DisplayLabel := 'Unidade';
  FQuery.Fields[3].DisplayLabel := 'Origem';
  FQuery.Fields[4].DisplayLabel := 'CST';
  FQuery.Fields[5].DisplayLabel := 'CFOP';
  FQuery.Fields[6].DisplayLabel := 'Aliquota';
  FQuery.Fields[7].DisplayLabel := 'Valor PIS';
  FQuery.Fields[8].DisplayLabel := 'Valor COFINS';
  FQuery.Fields[9].DisplayLabel := 'Campo cancelado';
  FQuery.Fields[10].DisplayLabel := 'Valor final';

  FQuery.Fields[4].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[9].Required := False;
  FQuery.Fields[2].OnGetText := GetFieldTrim;

end;

procedure TFonteDadosC470.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.codigoVenda;
end;

function TFonteDadosC470.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC470Produto }

procedure TFonteDadosC470Produto.preenche;
begin

  if not AbreQuery then Exit;
  if teveErros then exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.registroC470New) do
    begin

      COD_ITEM      := FQuery.Fields[0].Text;
      QTD           := FQuery.Fields[1].AsCurrency;
      UNID          := FQuery.Fields[2].Text;
      VL_ITEM       := FQuery.Fields[10].AsCurrency;
      CST_ICMS      := CompletaPalavra(FQuery.Fields[3].Text,1,'0') + CompletaPalavra((FQuery.Fields[4].Text),2,'0');
      CFOP          := FQuery.Fields[5].Text;
      ALIQ_ICMS     := FQuery.Fields[6].AsCurrency;
      VL_PIS        := FQuery.Fields[7].AsCurrency;
      VL_COFINS     := FQuery.Fields[8].AsCurrency;

      addStr(strItem,COD_ITEM);
      addStr(strUnidade,UpperCase(UNID));

    end;
    //GravaLOG('logSped.txt',IntToStr(self.codigoVenda));

    FQuery.Next;

  end;

end;

procedure TFonteDadosC470Produto.SetCampos;
begin
  FQuery.Fields[0].DisplayLabel := 'Produto';
  inherited;
end;

procedure TFonteDadosC470Produto.SetParametros;
begin
  inherited;
end;

{ TFonteDadosC470Servico }

procedure TFonteDadosC470Servico.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC470New) do
    begin

      COD_ITEM      := FQuery.Fields[0].text;
      QTD           := FQuery.Fields[1].AsCurrency;
      UNID          := FQuery.Fields[2].text;
      VL_ITEM       := FQuery.Fields[10].AsCurrency;
      CST_ICMS      := CompletaPalavra('0',1,'0') + CompletaPalavra(('90'),2,'0');
      CFOP          := FQuery.Fields[5].text;
      ALIQ_ICMS     := 0;
      VL_PIS        := FQuery.Fields[7].AsCurrency;
      VL_COFINS     := FQuery.Fields[8].AsCurrency;

      addStr(strServico,COD_ITEM);
      addStr(strUnidade,UpperCase(UNID));

    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC470Servico.SetCampos;
begin
  FQuery.Fields[0].DisplayLabel := 'Servi�o';
  inherited;
end;

procedure TFonteDadosC470Servico.SetParametros;
begin
  inherited;
end;

{ TFonteDadosC490 }

procedure TFonteDadosC490.preenche;
begin
  inherited;

end;

procedure TFonteDadosC490.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Origem';
  FQuery.Fields[1].DisplayLabel := 'CST';
  FQuery.Fields[2].DisplayLabel := 'CFOP';
  FQuery.Fields[3].DisplayLabel := 'Valor final';

  FQuery.Fields[1].Required := False;
end;

procedure TFonteDadosC490.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsDateTime := dataMovimento;
  FQuery.Params[1].AsInteger := ECF;
end;

function TFonteDadosC490.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC490Produto }

procedure TFonteDadosC490Produto.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC490New) do
    begin

      CST_ICMS   :=  CompletaPalavra(FQuery.Fields[0].text,1,'0') + CompletaPalavra((FQuery.Fields[1].text),2,'0');
      CFOP       := FQuery.Fields[2].text;
      ALIQ_ICMS  := FQuery.Fields[4].AsCurrency;
      VL_OPR     := FQuery.Fields[3].AsCurrency;

      if (ALIQ_ICMS > 0 ) then
      begin

        VL_BC_ICMS := VL_OPR;
        VL_ICMS    := ALIQ_ICMS/100*VL_OPR;

        valor_total_debitos := valor_total_debitos + VL_ICMS;

      end
      else
      begin
        VL_BC_ICMS := 0;
        VL_ICMS    := 0;
      end;

    end;

    FQuery.Next;

  end;
end;

procedure TFonteDadosC490Produto.SetCampos;
begin
  inherited;
  FQuery.Fields[4].DisplayLabel := 'Aliquota';
  FQuery.Fields[5].DisplayLabel := 'BC_ICMS';
  FQuery.Fields[6].DisplayLabel := 'VL_ICMS';
  FQuery.Fields[7].DisplayLabel := 'Totalizador parcial';
end;

procedure TFonteDadosC490Produto.SetParametros;
begin
  inherited;

end;

function TFonteDadosC490Produto.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC490Servico }

procedure TFonteDadosC490Servico.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC490New) do
    begin

      CST_ICMS   := CompletaPalavra(FQuery.Fields[0].text,1,'0') + CompletaPalavra((FQuery.Fields[1].text),2,'0');
      CFOP       := FQuery.Fields[2].text;
      ALIQ_ICMS  := 0;
      VL_OPR     := FQuery.Fields[3].AsCurrency;

      if (ALIQ_ICMS > 0 ) then
      begin
        VL_BC_ICMS := VL_OPR;
        VL_ICMS    := ALIQ_ICMS/100*VL_OPR;
        valor_total_debitos:= valor_total_debitos+VL_ICMS;
      end
      else
      begin
        VL_BC_ICMS := 0;
        VL_ICMS    := 0;
      end;

    end;
    FQuery.Next;
  end;

end;

procedure TFonteDadosC490Servico.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC490Servico.SetParametros;
begin
  inherited;
end;

function TFonteDadosC490Servico.ValidaRegistros: Boolean;
begin

end;

procedure TObjSpedFiscal_Novo.SetlinhasBuffer(const Value: Integer);
begin
  FlinhasBuffer := Value;
end;

procedure TObjSpedFiscal_Novo.SetnotasBuffer(const Value: Integer);
begin
  FnotasBuffer := Value;
end;

{ TFonteDados0200Produto }

procedure TFonteDados0200Produto.preenche;
var
  i:Integer;
begin
  inherited;

  for i:=0  to strItem.Count-1  do
  begin

    self.pCodigoItem := strItem[i];

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrBloco0.Registro0200New) do
    begin
      COD_ITEM   := strItem[i];
      DESCR_ITEM := FQuery.Fields[0].Text;
      COD_BARRA  := FQuery.Fields[1].Text;
      UNID_INV   := FQuery.Fields[2].Text;
      COD_NCM    := FQuery.Fields[3].Text;
      TIPO_ITEM  := tiMercadoriaRevenda;
      addStr (strUnidade,UpperCase(UNID_INV));
    end;

  end;

end;

procedure TFonteDados0200Produto.SetCampos;
begin
  inherited;
end;

procedure TFonteDados0200Produto.SetParametros;
begin
  inherited;
end;

function TFonteDados0200Produto.ValidaRegistros: Boolean;
begin

  if  ( Length(Trim(FQuery.Fields[3].Text)) > 8 )  then
    strErros.add('Campo: '+FQuery.Fields[3].DisplayLabel+' maior que 8 digitos. Produto: '+FQuery.Fields[0].text);

  if (FQuery.Fields[3].Text = '') then
        strErros.Add('Campo NCM vazio para produto: '+self.pCodigoItem+' - '+FQuery.Fields[0].Text);

end;

{ TFonteDados0200Servico }

procedure TFonteDados0200Servico.preenche;
var
  i:Integer;
begin
  inherited;

  for i:=0  to strServico.Count-1  do
  begin
  
    self.pCodigoItem := strServico[i];

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrBloco0.Registro0200New) do
    begin
      COD_ITEM   := strServico[i];
      DESCR_ITEM := FQuery.Fields[0].Text;
      COD_BARRA  := FQuery.Fields[1].Text;
      UNID_INV   := FQuery.Fields[2].Text;
      COD_NCM    := FQuery.Fields[3].Text;
      TIPO_ITEM  := tiServicos;
      addStr (strUnidade,UpperCase(UNID_INV));
    end;

  end;

end;

procedure TFonteDados0200Servico.SetCampos;
begin
  inherited;
  FQuery.Fields[3].Required := False;
end;

procedure TFonteDados0200Servico.SetParametros;
begin
  inherited;
end;

function TFonteDados0200Servico.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC100 }

procedure TFonteDadosC100Saida.inicializa;
var
  i:Integer;
begin

  FRegC170 := TFonteDadosC170Saida.Create(self);
  FRegC170.Registro := 'C170 SAIDA';

  FRegC190CST := TFonteDadosC190SaidaCST.Create(self);
  FRegC190CST.Registro := 'C190 SAIDACST';

  FRegC190CSOSN := TFonteDadosC190SaidaCSOSN.Create(self);
  FRegC190CSOSN.Registro := 'C190 SAIDACSOSN';


  for i := 0  to self.ComponentCount-1  do
  begin
    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;
      
  end;

end;

function TFonteDadosC100.getIndPag(indPag: string): TACBrTipoPagamento;
begin
  if indPag = '0' then
    result := tpVista
  else
  if indPag = '1' then
    Result := tpPrazo
  else
    Result := tpSemPagamento; {2}
end;

procedure TFonteDadosC100.preenche;
begin
  inherited;
end;

procedure TFonteDadosC100.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC100.SetParametros;
begin
  inherited;
end;

function TFonteDadosC100.ValidaRegistros: Boolean;
begin
  inherited;
end;

{ TFonteDadosC170 }

function TFonteDadosC170.getVL_BC_ICMS(pReducao,
  baseCalculo: Currency): Currency;
begin
  Result := baseCalculo - (pReducao * baseCalculo/100);
  Result := StrToCurrDef(FormatFloat('0.00',Result),0);
end;

function TFonteDadosC170.getVL_ICMS(bc_icms, aliquota: currency): Currency;
begin
  result := bc_icms * aliquota / 100;
  result := StrToCurrDef(FormatFloat('0.00',result),0);
end;

procedure TFonteDadosC170.preenche;
begin
  inherited;
end;

procedure TFonteDadosC170.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC170.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.codigoNF;
end;

function TFonteDadosC170.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC190 }

procedure TFonteDadosC190.preenche;
begin
  inherited;
end;

procedure TFonteDadosC190.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC190.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.codigoNF;
end;

function TFonteDadosC190.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC170Saida }

procedure TFonteDadosC170Saida.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC170New) do
    begin

      NUM_ITEM      :=  IntToStr(FQuery.recNO);
      COD_ITEM      :=  FQuery.Fields[1].Text;

      addStr (strItem,COD_ITEM);

      DESCR_COMPL   :=  FQuery.Fields[2].Text;
      QTD           :=  FQuery.Fields[3].AsCurrency;
      UNID          :=  FQuery.Fields[4].Text;

      addStr (strUnidade,UpperCase(UNID));

      VL_ITEM       :=  FQuery.Fields[5].AsCurrency;
      VL_DESC       :=  FQuery.Fields[6].AsCurrency;
      IND_MOV       :=  mfSim;
      CFOP          :=  FQuery.Fields[10].Text;;
      COD_NAT       :=  FQuery.Fields[10].Text;

      addStr (strNatOp,COD_NAT);

      IND_APUR := iaMensal;

      if (FQuery.Fields[8].Text <> '') then
        CST_ICMS := FQuery.Fields[7].Text + FQuery.Fields[8].Text
      else
        CST_ICMS := FQuery.Fields[9].Text;

      ALIQ_ICMS :=  FQuery.Fields[13].AsCurrency;

      if (ALIQ_ICMS > 0) then
      begin
        //VL_BC_ICMS := getVL_BC_ICMS(FQuery.Fields[11].AsCurrency,FQuery.Fields[12].AsCurrency);
        //VL_ICMS    := getVL_ICMS(VL_BC_ICMS,ALIQ_ICMS);
          VL_ICMS    := FQuery.Fields[28].AsCurrency;
          VL_BC_ICMS := FQuery.Fields[29].AsCurrency;
      end
      else
      begin
        VL_BC_ICMS := 0;
        VL_ICMS    := 0;
      end;

      {ICMS ST}
      VL_BC_ICMS_ST :=  0;//FQuery.Fields[15].AsCurrency;
      VL_ICMS_ST    :=  0;//FQuery.Fields[16].AsCurrency;
      ALIQ_ST       :=  0;//FQuery.Fields[17].AsCurrency;

      {IPI}
      COD_ENQ       :=  '';
      CST_IPI       :=  IfThen(FQuery.Fields[24].Text='','',CompletaPalavra_a_Esquerda(FQuery.Fields[24].Text,2,'0'));
      VL_BC_IPI     :=  FQuery.Fields[25].AsCurrency;
      ALIQ_IPI      :=  FQuery.Fields[26].AsCurrency;
      VL_IPI        :=  FQuery.Fields[27].AsCurrency;

      //

      {PIS}
      VL_BC_PIS     :=  FQuery.Fields[12].AsCurrency;
      ALIQ_PIS_PERC :=  FQuery.Fields[18].AsCurrency;
      VL_PIS        :=  FQuery.Fields[19].AsCurrency;
      CST_PIS       :=  IfThen(FQuery.Fields[22].Text='','',CompletaPalavra_a_Esquerda(FQuery.Fields[22].Text,2,'0'));

      {COFINS}
      VL_BC_COFINS      :=  FQuery.Fields[12].AsCurrency;
      ALIQ_COFINS_PERC  :=  FQuery.Fields[20].AsCurrency;
      VL_COFINS         :=  FQuery.Fields[21].AsCurrency;
      CST_COFINS        :=  IfThen(FQuery.Fields[23].Text='','',CompletaPalavra_a_Esquerda(FQuery.Fields[23].Text,2,'0'));

      COD_CTA   := '';

    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC170Saida.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo da NF';
  FQuery.Fields[1].DisplayLabel := 'Produto';
  FQuery.Fields[2].DisplayLabel := 'Nome produto';
  FQuery.Fields[3].DisplayLabel := 'Quantidade';
  FQuery.Fields[4].DisplayLabel := 'Unidade';
  FQuery.Fields[5].DisplayLabel := 'Valor';
  FQuery.Fields[6].DisplayLabel := 'Desconto';
  FQuery.Fields[7].DisplayLabel := 'Origem da mercadoria';
  FQuery.Fields[8].DisplayLabel := 'CST ICMS';
  FQuery.Fields[9].DisplayLabel := 'CSOSN';
  FQuery.Fields[10].DisplayLabel := 'CFOP';
  FQuery.Fields[11].DisplayLabel := 'Redu��o BC ICMS';
  FQuery.Fields[12].DisplayLabel := 'Valor Final';
  FQuery.Fields[13].DisplayLabel := 'Aliquota';
  FQuery.Fields[14].DisplayLabel := 'NCM';
  FQuery.Fields[15].DisplayLabel := 'BC ICMS ST';
  FQuery.Fields[16].DisplayLabel := 'VALOR ICMS ST';
  FQuery.Fields[17].DisplayLabel := 'ALIQUOTA ST';

  FQuery.Fields[18].DisplayLabel := '% PIS';
  FQuery.Fields[19].DisplayLabel := 'VALOR PIS';
  FQuery.Fields[20].DisplayLabel := '% COFINS';
  FQuery.Fields[21].DisplayLabel := 'VALOR COFINS';
  FQuery.Fields[22].DisplayLabel := 'CST PIS';
  FQuery.Fields[23].DisplayLabel := 'CST COFINS';
  FQuery.Fields[24].DisplayLabel := 'CST IPI';
  FQuery.Fields[25].DisplayLabel := 'BC IPI';
  FQuery.Fields[26].DisplayLabel := '% IPI';
  FQuery.Fields[27].DisplayLabel := 'VALOR IPI';
  FQuery.Fields[28].DisplayLabel := 'VALOR ICMS';
  FQuery.Fields[29].DisplayLabel := 'BC ICMS';


  FQuery.Fields[2].OnGetText := GetFieldTrim;
  FQuery.Fields[4].OnGetText := GetFieldTrim;
  
  FQuery.Fields[6].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[9].Required := False;
  FQuery.Fields[11].Required := False;
  FQuery.Fields[13].Required := False;
  FQuery.Fields[15].Required := False;
  FQuery.Fields[16].Required := False;
  FQuery.Fields[17].Required := False;
  FQuery.Fields[18].Required := False;
  FQuery.Fields[19].Required := False;
  FQuery.Fields[20].Required := False;
  FQuery.Fields[21].Required := False;
  FQuery.Fields[22].Required := False;
  FQuery.Fields[23].Required := False;

  FQuery.Fields[24].Required := False;
  FQuery.Fields[25].Required := False;
  FQuery.Fields[26].Required := False;
  FQuery.Fields[27].Required := False;
  FQuery.Fields[28].Required := False;
  FQuery.Fields[29].Required := False;
  
end;

procedure TFonteDadosC170Saida.SetParametros;
begin
  inherited;
end;

function TFonteDadosC170Saida.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC190Saida }

procedure TFonteDadosC190SaidaCST.preenche;
var
  STA,STB,CSOSN,pCFOP,ALIQUOTA:string;
  VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI,
  BC_ICMS_ST,pVL_ICMS_ST,pVL_FRETE,pVL_OUTROS:Currency;
begin
  inherited;

  if (situacaoNF <> sdCancelado) and (situacaoNF <> sdDoctoNumInutilizada) and (situacaoNF <> sdCanceladoExtemp) and (situacaoNF <> sdDoctoDenegado)  then
  begin

    if not AbreQuery then Exit;
    if teveErros then Exit;

    while not FQuery.Eof do
    begin

      VALORFINAL   := StrToCurr (tira_ponto (formata_valor (FQuery.Fields[4].Text)));
      BC_ICMS      := FQuery.Fields[5].AsCurrency;
      //pVL_ICMS     := BC_ICMS * FQuery.Fields[3].AsCurrency / 100;
      pVL_ICMS     := FQuery.Fields[11].AsCurrency;
      
      VALORIPI     := FQuery.Fields[6].AsCurrency;
      pVL_FRETE    := FQuery.Fields[7].AsCurrency;
      BC_ICMS_ST   := FQuery.Fields[8].AsCurrency;
      pVL_ICMS_ST  := FQuery.Fields[9].AsCurrency;
      pVL_OUTROS   := FQuery.Fields[10].AsCurrency;

      if situacaoNF <> sdFiscalCompl then
      begin
        //if (BC_ICMS > VALORFINAL) then
          //strERROS.Add('Base de calculo do ICMS n�o pode ser maior que o valor final do produto (BC ICMS: '+currToStr(BC_ICMS)+' Valor produto: '+currToStr(VALORFINAL)+')'+' Saida c�digo: '+FQuery.Fields[0].Text+'. REG 190SaidaCST');
      end;

      VALOR_RED_BC := 0;
      
      if (VALORFINAL > 0) then
        VALOR_RED_BC := Abs(VALORFINAL - BC_ICMS);

      repeat

        STA      := FQuery.Fields[0].Text;
        STB      := FQuery.Fields[1].Text;
        pCFOP    := FQuery.Fields[2].Text;
        ALIQUOTA := FQuery.Fields[3].Text;

      until not (registroCorrespondenteCST(STA+STB+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI,BC_ICMS_ST,pVL_ICMS_ST,pVL_FRETE,pVL_OUTROS,FQuery) and not FQuery.Eof);


      if (self.pValorICMS <= 0) then
      begin
        BC_ICMS:=0;
        pVL_ICMS:=0;
        VALOR_RED_BC:=0;
      end;

      with (FAcbrBlocoC.RegistroC190New) do
      begin


        CST_ICMS      := CompletaPalavra((STA+STB),3,'0');
        CFOP          := pCFOP;
        ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
        VL_OPR        := VALORFINAL + pVL_ICMS_ST + pVL_FRETE + VALORIPI + pVL_OUTROS; {valorfinal + valoricmsst + valorfrete}

        if (ALIQ_ICMS <= 0) then
          VL_BC_ICMS := 0
        else
          VL_BC_ICMS    := BC_ICMS;


        VL_ICMS       := pVL_ICMS;
        VL_BC_ICMS_ST :=0;
        VL_ICMS_ST    :=0;
        VL_RED_BC     :=VALOR_RED_BC;
        VL_IPI        := VALORIPI;
        VL_BC_ICMS_ST := 0;//BC_ICMS_ST;
        VL_ICMS_ST    := 0;//pVL_ICMS_ST;

      end;

      {campo do registro E110 � o somatorio de todos os campos VL_ICMS do registro C190 - inclui somente as opera��es saida}
      {saida- valor_total_debitos}
      {entrada -  vl_tot_creditos}

       if (self.pNFentrada = 'S') then
        self.VL_TOT_CREDITOS_E110 := VL_TOT_CREDITOS_E110 + StrToCurrDef(FormatFloat('0.00',pVL_ICMS),0)
       else
         self.valor_total_debitos := valor_total_debitos + StrToCurrDef(FormatFloat('0.00',pVL_ICMS),0);


      {aqui nao preciza do next, a fun��o registroCorrespondenteCST se encarrega}

    end;

  end;

end;

procedure TFonteDadosC190SaidaCST.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Origem merc.';
  FQuery.Fields[1].DisplayLabel := 'CST';
  FQuery.Fields[2].DisplayLabel := 'CFOP';
  FQuery.Fields[3].DisplayLabel := 'Aliquota';
  FQuery.Fields[4].DisplayLabel := 'Valor final';
  FQuery.Fields[5].DisplayLabel := 'BC_ICMS';
  FQuery.Fields[6].DisplayLabel := 'Valor IPI';
  FQuery.Fields[7].DisplayLabel := 'VALOR FRETE';
  FQuery.Fields[8].DisplayLabel := 'BC ICMS ST';
  FQuery.Fields[9].DisplayLabel := 'VALOR ICMS ST';
  FQuery.Fields[10].DisplayLabel := 'VALOR OUTROS';
  FQuery.Fields[11].DisplayLabel := 'VALOR ICMS';

  FQuery.Fields[3].required := false;
  FQuery.Fields[5].required := false;
  FQuery.Fields[6].Required  := False;
  FQuery.Fields[7].Required  := False;
  FQuery.Fields[8].Required  := False;
  FQuery.Fields[9].Required  := False;
  FQuery.Fields[10].Required := False;
  FQuery.Fields[11].Required := False;
end;

procedure TFonteDadosC190SaidaCST.SetParametros;
begin
  inherited;
end;

function TFonteDadosC190SaidaCST.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC190Entrada }

procedure TFonteDadosC190Entrada.preenche;
begin
  inherited;
end;

procedure TFonteDadosC190Entrada.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosC190Entrada.SetParametros;
begin
  inherited;

end;

function TFonteDadosC190Entrada.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC170Entrada }

procedure TFonteDadosC170Entrada.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC170New) do
    begin

      NUM_ITEM      :=  IntToStr(FQuery.RecNo);
      COD_ITEM      :=  FQuery.Fields[0].Text;

      addStr (strItem,COD_ITEM);

      DESCR_COMPL   :=  Trim(FQuery.Fields[1].Text);
      QTD           :=  FQuery.Fields[3].AsCurrency;

      UNID          :=  FQuery.Fields[4].Text;
      addStr (strUnidade,UpperCase(UNID));

      VL_ITEM       :=  FQuery.Fields[5].AsCurrency;
      VL_DESC       :=  FQuery.Fields[6].AsCurrency;
      IND_MOV       :=  mfSim;
      CFOP          :=  FQuery.Fields[9].Text;

      COD_NAT       :=  FQuery.Fields[9].Text;
      addStr (strNatOp,COD_NAT);

      VL_BC_ICMS_ST :=  0;
      ALIQ_ST       :=  0;
      VL_ICMS_ST    :=  0;
      IND_APUR      :=  iaMensal;

      {ICMS}

      if (FQuery.Fields[8].Text <> '') then
        CST_ICMS :=  CompletaPalavra (FQuery.Fields[7].Text + FQuery.Fields[8].Text,3,'0')
      else
        CST_ICMS :=  FQuery.Fields[13].Text;

      {VL_BC_ICMS    :=  self.get_VL_BC_ICMS(self.objquery.fieldbyname('porcentagemreducao').AsCurrency,self.objquery.fieldbyname('valorfinal').AsCurrency);
      ALIQ_ICMS     :=  self.objquery.fieldbyname('aliquota').AsCurrency;
      VL_ICMS       :=  self.get_VL_ICMS(VL_BC_ICMS,ALIQ_ICMS);}

      VL_BC_ICMS := FQuery.Fields[14].AsCurrency;
      ALIQ_ICMS  := FQuery.Fields[12].AsCurrency;
      VL_ICMS    := FQuery.Fields[15].AsCurrency;

      {ICMS ST}
      VL_BC_ICMS_ST := 0;//FQuery.Fields[16].AsCurrency;
      VL_ICMS_ST    := 0;//FQuery.Fields[17].AsCurrency;
      ALIQ_ST       := 0;//FQuery.Fields[18].AsCurrency;

      {IPI}
      COD_ENQ       :=  '';
      CST_IPI       :=  IfThen(FQuery.Fields[25].Text='','',CompletaPalavra_a_Esquerda(FQuery.Fields[25].Text,2,'0'));
      VL_BC_IPI     :=  FQuery.Fields[26].AsCurrency;
      ALIQ_IPI      :=  FQuery.Fields[27].AsCurrency;
      VL_IPI        := FQuery.Fields[28].AsCurrency;

      {PIS}
      VL_BC_PIS     :=  FQuery.Fields[11].AsCurrency;
      ALIQ_PIS_PERC :=  FQuery.Fields[19].AsCurrency;
      VL_PIS        :=  FQuery.Fields[20].AsCurrency;
      CST_PIS       :=  IfThen(FQuery.Fields[23].Text = '','',CompletaPalavra_a_Esquerda(FQuery.Fields[23].Text,2,'0'));

      {COFINS}
      VL_BC_COFINS      :=  FQuery.Fields[11].AsCurrency;
      ALIQ_COFINS_PERC  :=  FQuery.Fields[21].AsCurrency;
      VL_COFINS         :=  FQuery.Fields[22].AsCurrency;
      CST_COFINS        :=  IfThen(FQuery.Fields[24].Text='','',CompletaPalavra_a_Esquerda(FQuery.Fields[24].Text,2,'0'));

      COD_CTA   := '';

    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC170Entrada.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo produto';
  FQuery.Fields[1].DisplayLabel := 'Descri��o produto';
  FQuery.Fields[2].DisplayLabel := 'NCM';
  FQuery.Fields[3].DisplayLabel := 'Quantidade';
  FQuery.Fields[4].DisplayLabel := 'Unidade';
  FQuery.Fields[5].DisplayLabel := 'Valor total';
  FQuery.Fields[6].DisplayLabel := 'Desconto';
  FQuery.Fields[7].DisplayLabel := 'Origem da mercadoria';
  FQuery.Fields[8].DisplayLabel := 'CST ICMS';
  FQuery.Fields[9].DisplayLabel := 'CFOP';
  FQuery.Fields[10].DisplayLabel := 'Percentual RED BC ICMS';
  FQuery.Fields[11].DisplayLabel := 'Valor final';
  FQuery.Fields[12].DisplayLabel := 'Aliquota';
  FQuery.Fields[13].DisplayLabel := 'CSOSN';
  FQuery.Fields[14].DisplayLabel := 'BC ICMS';
  FQuery.Fields[15].DisplayLabel := 'VL ICMS';
  FQuery.Fields[16].DisplayLabel := 'BC ICMS ST';
  FQuery.Fields[17].DisplayLabel := 'VL ICMS ST';
  FQuery.Fields[18].DisplayLabel := 'ALIQUOTA ST';
  FQuery.Fields[19].DisplayLabel := '% PIS';
  FQuery.Fields[20].DisplayLabel := 'VALOR PIS';
  FQuery.Fields[21].DisplayLabel := '% COFINS';
  FQuery.Fields[22].DisplayLabel := 'VALOR COFINS';
  FQuery.Fields[23].DisplayLabel := 'CST PIS';
  FQuery.Fields[24].DisplayLabel := 'CST COFINS';
  FQuery.Fields[25].DisplayLabel := 'CST IPI';
  FQuery.Fields[26].DisplayLabel := 'BC IPI';
  FQuery.Fields[27].DisplayLabel := '% IPI';
  FQuery.Fields[28].DisplayLabel := 'VALOR IPI';


  FQuery.Fields[2].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[10].Required := False;
  FQuery.Fields[12].Required := False;
  FQuery.Fields[13].Required := False;
  FQuery.Fields[16].Required := False;
  FQuery.Fields[17].Required := False;
  FQuery.Fields[18].Required := False;
  FQuery.Fields[23].Required := False;
  FQuery.Fields[24].Required := False;
  FQuery.Fields[25].Required := False;
  FQuery.Fields[26].Required := False;
  FQuery.Fields[27].Required := False;
  FQuery.Fields[28].Required := False;

  FQuery.Fields[1].OnGetText := GetFieldTrim;
  FQuery.Fields[4].OnGetText := GetFieldTrim;


end;

procedure TFonteDadosC170Entrada.SetParametros;
begin
  inherited;
end;

function TFonteDadosC170Entrada.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC100Saida }

constructor TFonteDadosC100Saida.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC100Saida.destroy;
begin
  FreeAndNil(FRegC170);
  FreeAndNil(FRegC190CST);
  FreeAndNil(FRegC190CSOSN);
  inherited;
end;


procedure TFonteDadosC100Saida.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo NF';
  FQuery.Fields[1].DisplayLabel := 'C�digo cliente';
  FQuery.Fields[2].DisplayLabel := 'C�digo fornecedor';
  FQuery.Fields[3].DisplayLabel := 'C�digo fiscal';
  FQuery.Fields[4].DisplayLabel := 'Situa��o da NF';
  FQuery.Fields[5].DisplayLabel := 'Campo NF complementar';
  FQuery.Fields[6].DisplayLabel := 'N�mero da NF';
  FQuery.Fields[7].DisplayLabel := 'Chave de acesso';
  FQuery.Fields[8].DisplayLabel := 'Data de emiss�o da NF';
  FQuery.Fields[9].DisplayLabel := 'Data de saida da NF';
  FQuery.Fields[10].DisplayLabel := 'Valor final da NF';
  FQuery.Fields[11].DisplayLabel := 'Valor total da NF';
  FQuery.Fields[12].DisplayLabel := 'Desconto da NF';
  FQuery.Fields[13].DisplayLabel := 'Campo fretePorContaTransportadora';
  FQuery.Fields[14].DisplayLabel := 'Valor do seguro da NF';
  FQuery.Fields[15].DisplayLabel := 'Valor das outras despesas';
  FQuery.Fields[16].DisplayLabel := 'Valor frete da NF';
  FQuery.Fields[17].DisplayLabel := 'BC ICMS da NF';
  FQuery.Fields[18].DisplayLabel := 'VL ICMS da NF';
  FQuery.Fields[19].DisplayLabel := 'BC ICMS ST da NF';
  FQuery.Fields[20].DisplayLabel := 'VL ICMS ST da NF';
  FQuery.Fields[21].DisplayLabel := 'VL IPI da NF';
  FQuery.Fields[22].DisplayLabel := 'Indicador de pagamento';
  FQuery.Fields[23].DisplayLabel := 'S�rie da NF';
  FQuery.Fields[24].DisplayLabel := 'VALOR PIS';
  FQuery.Fields[25].DisplayLabel := 'VALOR COFINS';
  FQuery.Fields[26].DisplayLabel := 'NF ENTRADA';

  FQuery.Fields[7].OnGetText := GetFieldTrim;

  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[5].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[10].Required := False;
  FQuery.Fields[11].Required := False;
  FQuery.Fields[12].Required := False;
  FQuery.Fields[13].Required := False;
  FQuery.Fields[14].Required := False;
  FQuery.Fields[15].Required := False;
  FQuery.Fields[16].Required := False;
  FQuery.Fields[17].Required := False;
  FQuery.Fields[18].Required := False;
  FQuery.Fields[19].Required := False;
  FQuery.Fields[20].Required := False;
  FQuery.Fields[21].Required := False;
  FQuery.Fields[22].Required := False;
  FQuery.Fields[23].Required := false;
  FQuery.Fields[24].Required := false;
  FQuery.Fields[25].Required := false;
  FQuery.Fields[26].Required := false;
end;

procedure TFonteDadosC100Saida.preenche;
var
  situacaoNF:TACBrSituacaoDocto;
  STA,STB,CSOSN,pCFOP,ALIQUOTA:string;
  VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI:Currency;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC100New) do
    begin

      if (FQuery.Fields[26].Text = 'S') then
        IND_OPER := tpEntradaAquisicao
      else
        IND_OPER   := tpSaidaPrestacao;

      IND_EMIT   := edEmissaoPropria;
      COD_MOD    := CompletaPalavra_a_Esquerda(FQuery.fields[3].Text,2,'0');

      if (FQuery.fields[5].Text = 'S') then
        COD_SIT := sdFiscalCompl
      else
        COD_SIT := getSituacaoNF(FQuery.fields[4].Text);

      {usado no registro C190}
      situacaoNF:=cod_sit;

      SER := FQuery.Fields[23].text;

      {se o codigo da situa��o do documento estiver em uma das situa��es abaixo:
       preencher somente os campos REG, IND_OPER, IND_EMIT, COD_MOD, COD_SIT, SER e NUM_DOC.
      Demais campos dever�o ser apresentados com conte�do VAZIO �||�. N�o informar registros filhos (C170 e C190)}

      if (COD_SIT = sdCancelado) or (COD_SIT = sdDoctoNumInutilizada) or (COD_SIT = sdCanceladoExtemp) or (COD_SIT = sdDoctoDenegado) then
        begin

          NUM_DOC  := FQuery.fields[6].Text;
          CHV_NFE  := FQuery.fields[7].Text; //inutilizada pode deixar vazio
          IND_PGTO := tpNenhum;
          IND_FRT  := tfNenhum;

        end
      else
      begin

        if (FQuery.fields[1].Text <> '') then
          COD_PART := FQuery.fields[1].Text+'C'
        else
          COD_PART := FQuery.fields[2].Text+'F';

        addStr(strCodPart,COD_PART);

        NUM_DOC       := FQuery.fields[6].Text;
        CHV_NFE       := FQuery.fields[7].Text;
        DT_DOC        := FQuery.fields[8].AsDateTime;
        DT_E_S        := FQuery.fields[9].AsDateTime;
        VL_DOC        := FQuery.fields[10].AsCurrency;
        IND_PGTO      := self.getIndPag(FQuery.Fields[22].text);
        VL_DESC       := FQuery.fields[12].AsCurrency;
        VL_MERC       := FQuery.fields[11].AsCurrency;
        IND_FRT       := self.getTipoFrete(FQuery.fields[13].Text);
        VL_FRT        := FQuery.fields[16].AsCurrency;
        VL_SEG        := FQuery.fields[14].AsCurrency;
        VL_OUT_DA     := FQuery.fields[15].AsCurrency;
        VL_BC_ICMS    := FQuery.fields[17].AsCurrency;
        VL_ICMS       := FQuery.fields[18].AsCurrency;
        VL_BC_ICMS_ST := 0;//FQuery.fields[19].AsCurrency;
        VL_ICMS_ST    := 0;//FQuery.fields[20].AsCurrency;
        VL_IPI        := FQuery.fields[21].AsCurrency;
        VL_PIS        := FQuery.fields[24].ascurrency;
        VL_COFINS     := FQuery.fields[25].ascurrency;
        VL_COFINS_ST  := StrToCurr('0,00');
        VL_PIS_ST     := StrToCurr('0,00');
      end;
    end;

    if (self.indPerfil <> 'C') then
    begin
      FRegC170.campoErro := 'Nota fiscal de saida c�digo: '+fquery.Fields[0].Text;
      FRegC170.codigoNF := FQuery.Fields[0].AsInteger;
      FRegC170.preenche;
    end;

    FRegC190CST.codigoNF := FQuery.Fields[0].AsInteger;
    FRegC190CST.pValorICMS := FQuery.fields[18].AsCurrency;
    FRegC190CST.pNFentrada := FQuery.fields[26].AsString;
    FRegC190CST.situacaoNF := situacaoNF;
    FRegC190CST.preenche;

    FRegC190CSOSN.codigoNF := FQuery.Fields[0].AsInteger;
    FRegC190CSOSN.pValorICMS := FQuery.fields[18].AsCurrency;
    FRegC190CSOSN.pNFentrada := FQuery.fields[26].AsString;
    FRegC190CSOSN.situacaoNF := situacaoNF;
    FRegC190CSOSN.preenche;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC100Saida.SetParametros;
begin
  inherited;
end;

function TFonteDadosC100Saida.ValidaRegistros: Boolean;
begin

  if not (FQuery.Fields[9].AsDateTime >= FQuery.Fields[8].AsDateTime) then
      strERROS.Add ('Data de saida deve ser maior ou igual a data de emiss�o do documento fiscal. NF de saida n�mero: '+FQuery.Fields[6].AsString);

end;


{ TFonteDadosC190SaidaCSOSN }

procedure TFonteDadosC190SaidaCSOSN.preenche;
var
  STA,STB,CSOSN,pCFOP,ALIQUOTA:string;
  VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI:Currency;
begin
  inherited;

  if (situacaoNF <> sdCancelado) and (situacaoNF <> sdDoctoNumInutilizada) and (situacaoNF <> sdCanceladoExtemp) and (situacaoNF <> sdDoctoDenegado)  then
  begin

    if not abrequery then Exit;
    if teveErros then Exit;

    while not (FQuery.Eof) do
    begin

      VALORFINAL   := StrToCurr (tira_ponto (formata_valor (FQuery.Fields[3].text)));
      BC_ICMS      := FQuery.Fields[4].AsCurrency;
      //pVL_ICMS      := BC_ICMS * FQuery.Fields[3].AsCurrency / 100;
      pVL_ICMS     := FQuery.Fields[5].AsCurrency;

      //if (BC_ICMS > VALORFINAL) then
        //strERROS.Add('Base de calculo do ICMS n�o pode ser maior que o valor final do produto (BC ICMS: '+currtostr(BC_ICMS)+' Valor produto: '+currtostr(VALORFINAL)+')'+' Saida c�digo: '+FQuery.Fields[0].Text+'. REG C190SaidaCSOSN');

      if BC_ICMS > 0 then
        VALOR_RED_BC := VALORFINAL - BC_ICMS
      else
        VALOR_RED_BC := 0;

      repeat

        CSOSN     := FQuery.Fields[0].text;
        pCFOP     := FQuery.Fields[1].text;
        ALIQUOTA  := FQuery.Fields[2].text;

      until not (registroCorrespondenteCSOSN(CSOSN+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,FQuery) and not FQuery.Eof);


      if (self.pValorICMS <= 0) then {valor ICMS}
      begin
        BC_ICMS:=0;
        pVL_ICMS:=0;
        VALOR_RED_BC:=0;
      end;


      with (FAcbrBlocoC.RegistroC190New) do
      begin

        CST_ICMS      := CSOSN;
        CFOP          := pCFOP;
        ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
        VL_OPR        := VALORFINAL;

        if (ALIQ_ICMS <= 0) then
          VL_BC_ICMS := 0
        else
          VL_BC_ICMS    := BC_ICMS;

        VL_ICMS       := pVL_ICMS;
        VL_BC_ICMS_ST :=0;
        VL_ICMS_ST    :=0;
        VL_RED_BC     :=VALOR_RED_BC;
        VL_IPI        :=0;

      end;

       {campo do registro E110 � o somatorio de todos os campos VL_ICMS do registro C190 - inclui somente as opera��es saida}
       {saida- valor_total_debitos}
       {entrada - valor_total_creditos}

       if (self.pNFentrada = 'S') then
         VL_TOT_CREDITOS_E110 := VL_TOT_CREDITOS_E110 + StrToCurrDef(FormatFloat('0.00',pVL_ICMS),0)
       else
         valor_total_debitos := valor_total_debitos + StrToCurrDef(FormatFloat('0.00',pVL_ICMS),0);

      {aqui n�o preciza de next a fun��o registroCorrespondenteCSOSN se encarrega}

    end;

  end;

end;

procedure TFonteDadosC190SaidaCSOSN.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CSOSN';
  FQuery.Fields[1].DisplayLabel := 'CFOP';
  FQuery.Fields[2].DisplayLabel := 'Aliquota';
  FQuery.Fields[3].DisplayLabel := 'valorFinal';
  FQuery.Fields[4].DisplayLabel := 'BC ICMS';
  FQuery.Fields[5].DisplayLabel := 'VALOR ICMS';


  FQuery.Fields[4].Required := false;
  FQuery.Fields[5].Required := false;

end;

procedure TFonteDadosC190SaidaCSOSN.SetParametros;
begin
  inherited;
end;

function TFonteDadosC190SaidaCSOSN.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC100Entrada }

constructor TFonteDadosC100Entrada.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC100Entrada.destroy;
begin
  FreeAndNil(FRegC170);
  FreeAndNil(FRegC190CST);
  FreeAndNil(FRegC190CSOSN);
  inherited;
end;

procedure TFonteDadosC100Entrada.inicializa;
var
  i:Integer;
begin

  FRegC170 := TFonteDadosC170Entrada.Create(self);
  FRegC170.Registro := 'C170 ENTRADA';

  FRegC190CST := TFonteDadosC190EntradaCST.Create(self);
  FRegC190CST.Registro := 'C190 ENTRADA CST';

  FRegC190CSOSN := TFonteDadosC190EntradaCSOSN.Create(self);
  FRegC190CSOSN.Registro := 'C190 ENTRADA CSOSN';


  for i := 0  to self.ComponentCount-1  do
  begin
    TFonteDadosBlocoC(self.Components[i]).strErros := self.strErros;
    TFonteDadosBlocoC(self.Components[i]).valor_total_debitos := self.valor_total_debitos;
    TFonteDadosBlocoC(self.Components[i]).VL_TOT_CREDITOS_E110:= self.VL_TOT_CREDITOS_E110;
    TFonteDadosBlocoC(self.Components[i]).indPerfil := self.indPerfil;
    TFonteDadosBlocoC(self.Components[i]).status := self.status;
    TFonteDadosBlocoC(self.Components[i]).Database := self.Database;
    TFonteDadosBlocoC(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDadosBlocoC(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDadosBlocoC(self.Components[i]).strItem := self.strItem;
    TFonteDadosBlocoC(self.Components[i]).strServico := self.strServico;
    TFonteDadosBlocoC(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDadosBlocoC(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDadosBlocoC(self.Components[i]).DataINI := self.DataINI;
    TFonteDadosBlocoC(self.Components[i]).DataFIN := self.DataFIN;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrBlocoC := Self.FAcbrBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;
      
  end;

end;

procedure TFonteDadosC100Entrada.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrBlocoC.RegistroC100New) do
    begin

      IND_OPER := tpEntradaAquisicao;
      IND_EMIT := edTerceiros;

      COD_PART := FQuery.Fields[1].Text+'F';
      addStr(strCodPart,COD_PART);
      COD_MOD  := CompletaPalavra_a_Esquerda(FQuery.Fields[2].Text,2,'0');
      COD_SIT  := sdRegular;
      NUM_DOC  := FQuery.Fields[3].Text;
      CHV_NFE  := FQuery.Fields[18].Text;
      DT_DOC   := FQuery.Fields[4].AsDateTime;
      DT_E_S   := FQuery.Fields[5].AsCurrency;
      VL_DOC   := FQuery.Fields[6].AsCurrency;
      IND_PGTO := self.getIndPag(FQuery.Fields[19].text);
      VL_DESC  := FQuery.Fields[7].AsCurrency;
      VL_MERC  := FQuery.Fields[8].AsCurrency;


      if (FQuery.Fields[9].AsString = 'S') then
        IND_FRT := tfPorContaEmitente
      else
        IND_FRT := tfPorContaDestinatario;

      VL_FRT        := FQuery.Fields[10].AsCurrency;
      VL_SEG        := FQuery.Fields[11].AsCurrency;
      VL_OUT_DA     := FQuery.Fields[12].AsCurrency;
      VL_BC_ICMS    := FQuery.Fields[13].AsCurrency;
      VL_ICMS       := FQuery.Fields[14].AsCurrency;
      VL_BC_ICMS_ST := 0;//FQuery.Fields[15].AsCurrency;
      VL_ICMS_ST    := 0;//FQuery.Fields[17].AsCurrency;
      VL_IPI        := FQuery.Fields[16].AsCurrency;
      VL_PIS        := FQuery.Fields[21].AsCurrency;
      VL_COFINS     := FQuery.Fields[22].AsCurrency;

      SER := FQuery.Fields[20].text;

      {VL_COFINS     := self.get_valorCofins(objquery.fieldbyname('valorfinal').AsCurrency);
      VL_COFINS_ST  := StrToCurr('0,00');
      VL_PIS_ST     := StrToCurr('0,00');}



    end;

    if (self.indPerfil <> 'C') then
    begin
      FRegC170.codigoNF := FQuery.Fields[0].AsInteger;
      FRegC170.preenche;
    end;

    FRegC190CST.codigoNF := FQuery.Fields[0].AsInteger;
    FRegC190CST.preenche;

    FRegC190CSOSN.codigoNF := FQuery.Fields[0].AsInteger;
    FRegC190CSOSN.preenche;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC100Entrada.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo entrada';
  FQuery.Fields[1].DisplayLabel := 'C�digo fornecedor';
  FQuery.Fields[2].DisplayLabel := 'C�digo fiscal';
  FQuery.Fields[3].DisplayLabel := 'N�mero NF';
  FQuery.Fields[4].DisplayLabel := 'Data emiss�o';
  FQuery.Fields[5].DisplayLabel := 'Data entrada';
  FQuery.Fields[6].DisplayLabel := 'Valor final';
  FQuery.Fields[7].DisplayLabel := 'Desconto';
  FQuery.Fields[8].DisplayLabel := 'Valor dos produtos';
  FQuery.Fields[9].DisplayLabel := 'Campo incluiFreteFornecedor';
  FQuery.Fields[10].DisplayLabel := 'Valor frete';
  FQuery.Fields[11].DisplayLabel := 'Valor seguro';
  FQuery.Fields[12].DisplayLabel := 'Despesas administrativas';
  FQuery.Fields[13].DisplayLabel := 'BC ICMS';
  FQuery.Fields[14].DisplayLabel := 'VL ICMS';
  FQuery.Fields[15].DisplayLabel := 'BC ICMS ST';
  FQuery.Fields[16].DisplayLabel := 'VL IPI';
  FQuery.Fields[17].DisplayLabel := 'VL ICMS ST';
  FQuery.Fields[18].DisplayLabel := 'Chave acesso';
  FQuery.Fields[19].DisplayLabel := 'Indicador de pagamento';
  FQuery.Fields[20].DisplayLabel := 'S�rie da NF';
  FQuery.Fields[21].DisplayLabel := 'VALOR PIS';
  FQuery.Fields[22].DisplayLabel := 'VALOR COFINS';


  FQuery.Fields[7].Required := False;
  FQuery.Fields[9].Required := False;
  FQuery.Fields[10].Required := False;
  FQuery.Fields[11].Required := False;
  FQuery.Fields[12].Required := False;
  FQuery.Fields[13].Required := False;
  FQuery.Fields[14].Required := False;
  FQuery.Fields[15].Required := False;
  FQuery.Fields[16].Required := False;
  FQuery.Fields[17].Required := False;
  FQuery.Fields[18].Required := False;
  FQuery.Fields[19].Required := False;
  FQuery.Fields[20].Required := False;
  FQuery.Fields[21].Required := False;
  FQuery.Fields[22].Required := False;

  FQuery.Fields[3].OnGetText  := GetFieldTrim;
  FQuery.Fields[18].OnGetText := GetFieldTrim;

end;

procedure TFonteDadosC100Entrada.SetParametros;
begin
  inherited;

end;

{ TFonteDadosC190EntradaCST }

procedure TFonteDadosC190EntradaCST.preenche;
var
  STA,STB,CSOSN,pCFOP,ALIQUOTA:string;
  VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,
  VALORIPI,BC_ICMS_ST,pVL_ICMS_ST,pVL_FRETE,pVL_OUTROS:Currency;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    while not(FQuery.Eof) do
    begin

      self.status.Panels[1].Text:='Gerando Bloco C registro C190 entrada (CST). Nota: '+FQuery.Fields[3].Text;
      Application.ProcessMessages;

      VALORFINAL   := FQuery.Fields[4].AsCurrency;
      BC_ICMS      := FQuery.Fields[5].AsCurrency;
      //pVL_ICMS     := BC_ICMS * FQuery.Fields[3].AsCurrency / 100;
      pVL_ICMS     := FQuery.Fields[11].AsCurrency;
      VALORIPI     := FQuery.Fields[6].AsCurrency;
      pVL_FRETE    := FQuery.Fields[7].AsCurrency;
      BC_ICMS_ST   := FQuery.Fields[8].AsCurrency;
      pVL_ICMS_ST  := FQuery.Fields[9].AsCurrency;
      pVL_OUTROS   := FQuery.Fields[10].AsCurrency;



      VALORFINAL := VALORFINAL;

      if (BC_ICMS > 0) then
        begin

          if (trunca (VALORFINAL) < trunca (BC_ICMS)) then
            VALOR_RED_BC:=(VALORFINAL + (BC_ICMS - VALORFINAL)) - BC_ICMS
          else
            VALOR_RED_BC := VALORFINAL - BC_ICMS;
        end
      else
        VALOR_RED_BC:=0;

      repeat

        STA       := FQuery.Fields[0].text;
        STB       := FQuery.Fields[1].text;
        pCFOP     := FQuery.Fields[2].Text;
        ALIQUOTA  := FQuery.Fields[3].Text;

      until not (registroCorrespondenteCST(STA+STB+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI,BC_ICMS_ST,pVL_ICMS_ST,pVL_FRETE,pVL_OUTROS,FQuery) and not FQuery.Eof);

      with (FAcbrBlocoC.RegistroC190New) do
      begin

        CST_ICMS      := CompletaPalavra((STA+STB),3,'0'); {fa�o isso porq o campo tem que ser tamanho 3 ex: cst a = 0 e cst b = 0, neste caso sao apenas 2 00 e tem que ser 000}
        CFOP          := pCFOP;
        ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
        VL_OPR        := VALORFINAL + pVL_ICMS_ST + pVL_FRETE + VALORIPI + pVL_OUTROS;

        if (ALIQ_ICMS <= 0) then
          VL_BC_ICMS := 0
        else
          VL_BC_ICMS    := BC_ICMS;

        VL_ICMS       := pVL_ICMS;
        VL_BC_ICMS_ST := 0;
        VL_ICMS_ST    := 0;
        VL_RED_BC     := VALOR_RED_BC;
        VL_IPI        := VALORIPI;
        VL_BC_ICMS_ST := 0;//bc_ICMS_ST;
        VL_ICMS_ST    := 0;//pVL_ICMS_ST;

        {o valor deste campo deve corresponder ao somat�rio de todos os documentos fiscais de entrada que geram cr�dito de ICMS}
        {O valor neste campo deve ser igual � soma dos VL_ICMS de todos os registros C190}
        {este somat�rio, est�o exclu�dos os documentos fiscais com CFOP 1605 e inclu�dos os documentos fiscais com CFOP 5605}
        if (pCFOP <> '1605') then
          VL_TOT_CREDITOS_E110:=VL_TOT_CREDITOS_E110+VL_ICMS;

      end;
      {aqui n�o preciza de next a fun��o registroCorrespondenteCST se encarrega}
    end;

  end;

end;

procedure TFonteDadosC190EntradaCST.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Origem da mercadoria';
  FQuery.Fields[1].DisplayLabel := 'CST ICMS';
  FQuery.Fields[2].DisplayLabel := 'CFOP';
  FQuery.Fields[3].DisplayLabel := 'Aliquota';
  FQuery.Fields[4].DisplayLabel := 'Valor final';
  FQuery.Fields[5].DisplayLabel := 'BC ICMS';
  FQuery.Fields[6].DisplayLabel := 'VL IPI';
  FQuery.Fields[7].DisplayLabel := 'VL FRETE';
  FQuery.Fields[8].DisplayLabel := 'BC ICMS ST';
  FQuery.Fields[9].DisplayLabel := 'VL ICMS ST';
  FQuery.Fields[10].DisplayLabel := 'VALOR OUTROS';
  FQuery.Fields[11].DisplayLabel := 'VALOR ICMS';

  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[9].Required := False;
  FQuery.Fields[10].Required := False;
  FQuery.Fields[11].Required := False;

end;

procedure TFonteDadosC190EntradaCST.SetParametros;
begin
  inherited;
end;

function TFonteDadosC190EntradaCST.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC190EntradaCSOSN }

procedure TFonteDadosC190EntradaCSOSN.preenche;
var
  STA,STB,CSOSN,pCFOP,ALIQUOTA:string;
  VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,VALORIPI:Currency;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    VALORFINAL   := StrToCurr (tira_ponto (formata_valor (FQuery.Fields[3].AsCurrency)));
    BC_ICMS      := FQuery.Fields[4].AsCurrency;
    //pVL_ICMS     := BC_ICMS * FQuery.Fields[2].AsCurrency / 100;
    pVL_ICMS     := FQuery.Fields[5].AsCurrency;

    if BC_ICMS > 0 then
      VALOR_RED_BC := VALORFINAL - BC_ICMS
    else
      VALOR_RED_BC := 0;

    repeat

      CSOSN     := FQuery.Fields[0].Text;
      pCFOP     := FQuery.Fields[1].Text;
      ALIQUOTA  := FQuery.Fields[2].Text;

    until not (registroCorrespondenteCSOSN(CSOSN+pCFOP+ALIQUOTA,VALORFINAL,BC_ICMS,pVL_ICMS,VALOR_RED_BC,FQuery) and not FQuery.Eof);

    //if (BC_ICMS > VALORFINAL) then
      //strERROS.Add('Base de calculo do ICMS n�o pode ser maior que o valor final do produto (BC ICMS: '+currtostr(BC_ICMS)+' Valor produto: '+currtostr(VALORFINAL)+')'+' Entrada c�digo: '+IntToStr(self.codigoNF)+'. REG C190EntradaCSOSN');

    with (FAcbrBlocoC.RegistroC190New) do
    begin

      CST_ICMS      := CSOSN;
      CFOP          := pCFOP;
      ALIQ_ICMS     := StrToCurrDef(ALIQUOTA,0);
      VL_OPR        := VALORFINAL;

      if (ALIQ_ICMS <= 0) then
        VL_BC_ICMS := 0
      else
        VL_BC_ICMS    := BC_ICMS;

      VL_ICMS       := pVL_ICMS;
      VL_BC_ICMS_ST :=0;
      VL_ICMS_ST    :=0;
      VL_RED_BC     :=VALOR_RED_BC;
      VL_IPI        :=0;

       {o valor deste campo deve corresponder ao somat�rio de todos os documentos fiscais de entrada que geram cr�dito de ICMS}
      {O valor neste campo deve ser igual � soma dos VL_ICMS de todos os registros C190}
      {este somat�rio, est�o exclu�dos os documentos fiscais com CFOP 1605 e inclu�dos os documentos fiscais com CFOP 5605}
      if (pCFOP <> '1605') then
        VL_TOT_CREDITOS_E110:=VL_TOT_CREDITOS_E110+VL_ICMS;

    end;

    {aqui n�o preciza do next a fun��o registroCorrespondenteCSOSN se encarrega}

  end;
end;

procedure TFonteDadosC190EntradaCSOSN.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CSOSN';
  FQuery.Fields[1].DisplayLabel := 'CFOP';
  FQuery.Fields[2].DisplayLabel := 'Aliquota';
  FQuery.Fields[3].DisplayLabel := 'Valor final';
  FQuery.Fields[4].DisplayLabel := 'BC ICMS';
  FQuery.Fields[5].DisplayLabel := 'VALOR ICMS';

  FQuery.Fields[4].Required := False;
  FQuery.Fields[5].Required := False;

end;

procedure TFonteDadosC190EntradaCSOSN.SetParametros;
begin
  inherited;
end;

function TFonteDadosC190EntradaCSOSN.ValidaRegistros: Boolean;
begin

end;

procedure TObjSpedFiscal_Novo.SetmemoErros(const Value: TMemo);
begin
  FmemoErros := Value;
end;

{ TFonteDados0150Cliente }

procedure TFonteDados0150Cliente.preenche;
var
  i:Integer;
begin
  inherited;

  for i:=0 to strCodPart.Count-1 do
  begin

    if Pos('C',strCodPart[i]) <> 0 then
    begin

      self.pCodigoParticipante := RetornaSoNumeros(strCodPart[i]);
      campoErro := 'Cliente c�digo: '+pCodigoParticipante;

      if not AbreQuery then Exit;
      if teveErros then Exit;

      with (FAcbrBloco0.Registro0150New) do
      begin

        COD_PART := strCodPart[i];
        NOME     := FQuery.Fields[0].Text;
        COD_PAIS := FQuery.Fields[1].Text;
        COD_MUN  := FQuery.Fields[3].AsInteger;
        ENDERECO := FQuery.Fields[4].Text;
        NUM      := FQuery.Fields[5].Text;
        COMPL    := '';
        BAIRRO   := FQuery.Fields[6].Text;
        SUFRAMA  := '';

        if ValidaCPF(FQuery.Fields[7].Text) then
          CPF := Trim(FQuery.Fields[7].Text)
        else
        begin
          CNPJ := Trim(FQuery.Fields[7].Text);
          IE   := FQuery.Fields[2].Text;
        end;

      end;

    end;

  end;
  
end;

procedure TFonteDados0150Cliente.SetCampos;
begin
  inherited;
end;

procedure TFonteDados0150Cliente.SetParametros;
begin
  inherited;
end;

function TFonteDados0150Cliente.ValidaRegistros: Boolean;
begin
  if not IsNumeric(FQuery.Fields[1].Text) then
    strErros.Add('Campo '+FQuery.Fields[1].DisplayLabel+' com valor inv�lido. Valor: '+fquery.Fields[1].Text+'. Cliente: '+fquery.Fields[0].Text);

  if not ValidaCPF(FQuery.Fields[7].Text) and not ValidaCNPJ(FQuery.Fields[7].text) then
    strErros.Add('Campo '+FQuery.fields[7].DisplayLabel+' com valor inv�lido. Valor: '+FQuery.Fields[7].Text+'. Cliente: '+fquery.Fields[0].Text);

end;

{ TFonteDados0150Fornecedor }

procedure TFonteDados0150Fornecedor.preenche;
var
  i:Integer;
begin
  inherited;

  for i:=0 to strCodPart.Count-1 do
  begin

    if Pos('F',strCodPart[i]) <> 0 then
    begin

      self.pCodigoParticipante := RetornaSoNumeros(strCodPart[i]);
      campoErro := 'Fornecedor c�digo: '+pCodigoParticipante;

      if not AbreQuery then Exit;
      if teveErros then Exit;

      with (FAcbrBloco0.Registro0150New) do
      begin

        COD_PART := strCodPart[i];
        NOME     := FQuery.Fields[0].Text;
        COD_PAIS := FQuery.Fields[1].Text;
        IE       := FQuery.Fields[2].Text;
        COD_MUN  := FQuery.Fields[3].AsInteger;
        ENDERECO := FQuery.Fields[4].Text;
        NUM      := FQuery.Fields[5].Text;
        COMPL    := '';
        BAIRRO   := FQuery.Fields[6].Text;
        SUFRAMA  := '';

        if ValidaCPF(FQuery.Fields[7].Text) then
          CPF := Trim(FQuery.Fields[7].Text)
        else
          CNPJ := Trim(FQuery.Fields[7].Text);

      end;

    end;

  end;

end;

procedure TFonteDados0150Fornecedor.SetCampos;
begin
  inherited;
end;

procedure TFonteDados0150Fornecedor.SetParametros;
begin
  inherited;
end;

function TFonteDados0150Fornecedor.ValidaRegistros: Boolean;
begin
  if not IsNumeric(FQuery.Fields[1].Text) then
    strErros.Add('Campo '+FQuery.Fields[1].DisplayLabel+' com valor inv�lido. Valor: '+fquery.Fields[1].Text+'. Fornecedor: '+fquery.Fields[0].Text);

  if not ValidaCPF(FQuery.Fields[7].Text) and not ValidaCNPJ(FQuery.Fields[7].text) then
    strErros.Add('Campo '+FQuery.fields[7].DisplayLabel+' com valor inv�lido. Valor: '+FQuery.Fields[7].Text+'. Fornecedor: '+fquery.Fields[0].Text);

  //if FQuery.Fields[2].Text <> '' then
    //if not validaInscricaoEstadual(FQuery.Fields[2].Text,FQuery.Fields[8].Text) then
      //strErros.Add('Campo '+Fquery.Fields[2].DisplayLabel+' com valor inv�lido. Valor: '+FQuery.Fields[2].Text+'. Cliente: '+fquery.Fields[0].Text);
end;

{ TFonteDados1010 }

procedure TFonteDados1010.preenche;
begin
  inherited;

  //if not AbreQuery then Exit;
  //if teveErros then Exit;

  with FAcbrBloco1.Registro1010New do
  begin

    IND_EXP   := 'N';
    IND_CCRF  := 'N';
    IND_COMB  := 'N';
    IND_USINA := 'N';
    IND_VA    := 'N';
    IND_EE    := 'N';
    IND_CART  := 'N';
    IND_FORM  := 'N';
    IND_AER   := 'N';

  end;

end;

procedure TFonteDados1010.SetCampos;
begin
  inherited;

end;

procedure TFonteDados1010.SetParametros;
begin
  inherited;

end;

function TFonteDados1010.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados1600 }

procedure TFonteDados1600.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  with FAcbrBloco1.Registro1600New do
  begin

    COD_PART := '';
    addStr(strCodPart,COD_PART);

    TOT_CREDITO := 0;
    TOT_DEBITO  := 0;
    
    FQuery.Next;

  end;

end;

procedure TFonteDados1600.SetCampos;
begin
  inherited;

end;

procedure TFonteDados1600.SetParametros;
begin
  inherited;

end;

function TFonteDados1600.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados0220 }

procedure TFonteDados0220.preenche;
begin
  inherited;

end;

procedure TFonteDados0220.SetCampos;
begin
  inherited;

end;

procedure TFonteDados0220.SetParametros;
begin
  inherited;
  //FQuery.Params[0].AsString := self.pCodigoItem;
end;

function TFonteDados0220.ValidaRegistros: Boolean;
begin

end;

end.
