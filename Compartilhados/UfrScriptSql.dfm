object FrScriptSql: TFrScriptSql
  Left = 0
  Top = 0
  Width = 914
  Height = 493
  TabOrder = 0
  object DBGridScript: TDBGrid
    Left = 0
    Top = 168
    Width = 914
    Height = 325
    Align = alClient
    DataSource = DsScript
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object PanelBotoes: TPanel
    Left = 0
    Top = 0
    Width = 914
    Height = 41
    Align = alTop
    TabOrder = 3
    object BtExecutaScript: TBitBtn
      Left = 256
      Top = 7
      Width = 129
      Height = 25
      Caption = '&Executa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtExecutaScriptClick
    end
    object BitBtn1: TBitBtn
      Left = 128
      Top = 7
      Width = 121
      Height = 25
      Caption = '&Save Script'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BitBtn1Click
    end
    object BtAbrirScript: TBitBtn
      Left = 0
      Top = 7
      Width = 121
      Height = 25
      Caption = '&Load Script'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtAbrirScriptClick
    end
  end
  object LbScript: TListBox
    Left = 0
    Top = 41
    Width = 914
    Height = 38
    Align = alTop
    ItemHeight = 13
    TabOrder = 2
    OnDblClick = LbScriptDblClick
  end
  object MemoScript: TRichEdit
    Left = 0
    Top = 79
    Width = 914
    Height = 89
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'MemoScript')
    ParentFont = False
    TabOrder = 0
    OnKeyDown = RichEdit1KeyDown
  end
  object IBDataSetScript: TIBDataSet
    Left = 224
    Top = 48
  end
  object OpenDialogScript: TOpenDialog
    Filter = '*.sql'
    Left = 288
    Top = 48
  end
  object DsScript: TDataSource
    DataSet = IBDataSetScript
    Left = 256
    Top = 48
  end
  object IBScript: TIBScript
    Dataset = IBDataSetScript
    Terminator = ';'
    Left = 184
    Top = 48
  end
  object SaveDialogScript: TSaveDialog
    Left = 320
    Top = 48
  end
end
