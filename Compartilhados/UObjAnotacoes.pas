unit UObjAnotacoes;
Interface
Uses Urelanotacoes,Classes,Db,Uessencialglobal,Ibcustomdataset,IBStoredProc,quickrpt,UobjOrigemAnotacoes,mask,forms,stdctrls;

Type
   TObjAnotacoes=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Origem        :tobjOrigemAnotacoes;
                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(codigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                //function    Get_PesquisaConcluidas: TStringList;
                //function    Get_PesquisaNaoConcluidas: TStringList;
                Function    Get_TituloPesquisa              :string;
                Function    Get_PesquisaOrigem              :TStringList;
                Function    Get_TituloPesquisaOrigem        :string;
                Procedure   TabelaparaObjeto;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO        :string;
                Function Get_Contato       :string;
                Function Get_Fone          :string;
                Function Get_Assunto       :string;
                Function Get_Data          :string;
                Function Get_DataRetorno   :string;
                Function Get_Observacoes   :String;
                Function Get_Concluido     :string;
                Function Get_Origem        :string;
                Function Get_TecnicoResponsavel:string;
                Function Get_Endereco          :string;

                Procedure Submit_CODIGO       (parametro:string);
                Procedure Submit_Contato       (parametro:string);
                Procedure Submit_Fone          (parametro:string);
                Procedure Submit_Assunto       (parametro:string);
                Procedure Submit_Data          (parametro:string);
                Procedure Submit_DataRetorno   (parametro:string);
                Procedure Submit_Observacoes   (parametro:string);
                Procedure Submit_Concluido     (parametro:string);
                Procedure Submit_Origem        (parametro:string);
                Procedure Submit_TecnicoResponsavel(parametro:string);
                Procedure Submit_Endereco             (parametro:string);

                Function  Get_NovoCodigo:string;
                Procedure Imprime(Parametro:string);
                Function PegaOrigemPadrao:string;
                procedure edtorigemExit(Sender: TObject;out lborigem:Tlabel);



         Private
                ObjDataset:Tibdataset;

                CODIGO        :String[09];
                Contato       :String[50];
                Fone          :String[20];
                Assunto       :String[50];
                Data          :String[10];
                DataRetorno   :String[10];
                Observacoes   :string;
                Concluido     :String[1];

                TecnicoResponsavel:string;
                Endereco          :string;

                Frelanotacoes:TFrelanotacoes;

               //CodTurma         :TobjTurma;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                procedure CampoContatoPrint(sender: TObject; var Value: String);
                procedure edtorigemKeyDown(Sender: TObject; var Key: Word;
                Shift: TShiftState);



   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,
     Controls,UopcaoRel,Ufiltraimp,Upesquisa,
     UorigemAnotacoes,windows,uobjconfrelatorio;


{ TTabTitulo }


Procedure  TObjAnotacoes.TabelaparaObjeto;//ok
begin
     With ObjDataset do
     Begin
        Self.CODIGO      :=FieldByName('CODIGO').asstring     ;
        Self.Contato     :=FieldByName('Contato').asstring    ;
        Self.Fone        :=FieldByName('Fone').asstring       ;
        Self.Assunto     :=FieldByName('Assunto').asstring    ;
        Self.Data        :=FieldByName('Data').asstring       ;
        Self.DataRetorno :=FieldByName('DataRetorno').asstring;
        Self.Observacoes:=FieldByName('Observacoes').asstring;
        Self.Concluido   :=FieldByName('Concluido').asstring  ;
        Self.TecnicoResponsavel:=fieldbyname('TecnicoResponsavel').asstring;
        Self.Endereco:=fieldbyname('endereco').asstring;
        If (Self.Origem.LocalizaCodigo(fieldbyname('origem').asstring)=False)
        Then Begin
                  Messagedlg('Origem N�o Localizada!',mterror,[mbok],0);
                  Origem.ZerarTabela;//Self.Zeraratabela;
             End
        Else Self.Origem.TabelaparaObjeto;
     End;
end;


Procedure TObjAnotacoes.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin
     FieldByName('CODIGO').asstring       :=Self.CODIGO      ;
     FieldByName('Contato').asstring      :=Self.Contato     ;
     FieldByName('Fone').asstring         :=Self.Fone        ;
     FieldByName('Assunto').asstring      :=Self.Assunto     ;
     FieldByName('Data').asstring         :=Self.Data        ;
     FieldByName('DataRetorno').asstring  :=Self.DataRetorno ;
     FieldByName('Observacoes').asstring  :=Self.Observacoes;
     FieldByName('Concluido').asstring    :=Self.Concluido   ;
     FieldByName('Origem').asstring       :=Self.Origem.Get_codigo;
     Fieldbyname('TecnicoResponsavel').asstring:=Self.TecnicoResponsavel;
     Fieldbyname('endereco').asstring     :=Self.Endereco;
  End;
End;

//***********************************************************************

function TObjAnotacoes.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjAnotacoes.ZerarTabela;//Ok
Begin
     With Self do
     Begin
        Self.CODIGO     :='';
        Self.Contato    :=''; 
        Self.Fone       :=''; 
        Self.Assunto    :=''; 
        Self.Data       :=''; 
        Self.DataRetorno:=''; 
        Self.Observacoes:='';
        Self.Concluido  :='';
        Self.Origem.zerartabela;
        Self.Endereco:='';
        Self.TecnicoResponsavel:='';
     End;
end;

Function TObjAnotacoes.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';
       If (Origem.Get_CODIGO='')
       Then Mensagem:=mensagem+'/Origem';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjAnotacoes.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.Origem.LocalizaCodigo(Self.Origem.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Origem n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjAnotacoes.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        Inteiros:=Strtoint(Self.Origem.get_codigo);
     Except
           Mensagem:=mensagem+'/Origem';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjAnotacoes.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjAnotacoes.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try
     Mensagem:='';

     If (Self.Concluido<>'S') and (Self.Concluido<>'N')
     Then mensagem:=Mensagem+'Campo Concluido com valor Inv�lido!';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjAnotacoes.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSql.add('Select CODIGO,Contato,Fone,Assunto,Data,DataRetorno,Observacoes,Concluido,origem,endereco,tecnicoresponsavel from TabAnotacoes where codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;


procedure TObjAnotacoes.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjAnotacoes.Exclui(codigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(codigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjAnotacoes.create;
begin

        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        //Self.ObjDatasource:=TDataSource.Create(nil);
        Self.Origem     :=TObjOrigemAnotacoes.create;
        Self.ParametroPesquisa:=TStringList.create;
        Frelanotacoes:=TFrelanotacoes.create(nil);

        ZerarTabela;

        With Self.ObjDataset do
        Begin
                SelectSQL.clear;
                SelectSql.add('Select CODIGO,Contato,Fone,Assunto,Data,DataRetorno,Observacoes,Concluido,origem,endereco,tecnicoresponsavel from TabAnotacoes where codigo=0');


                Self.SqlInicial:=SelectSQL.text;

                InsertSQL.clear;
                InsertSQL.add('Insert into TabAnotacoes (CODIGO,Contato,Fone,Assunto,Data,DataRetorno,Observacoes,Concluido,origem,endereco,tecnicoresponsavel) values (:CODIGO,:Contato,:Fone,:Assunto,:Data,:DataRetorno,:Observacoes,:Concluido,:origem,:endereco,:tecnicoresponsavel) ');

                ModifySQL.clear;
                ModifySQL.add(' UPdate TabAnotacoes set CODIGO=:CODIGO,Contato=:Contato,Fone=:Fone,Assunto=:Assunto,Data=:Data,DataRetorno=:DataRetorno,Observacoes=:Observacoes,Concluido=:Concluido,origem=:origem,');
                ModifySQL.add(' endereco=:endereco,tecnicoresponsavel=:tecnicoresponsavel where codigo=:codigo ');


                DeleteSQL.clear;
                DeleteSQL.add('Delete from TAbAnotacoes where codigo=:codigo ');

                RefreshSQL.clear;
                RefreshSQL.add('Select CODIGO,Contato,Fone,Assunto,Data,DataRetorno,Observacoes,Concluido,origem,endereco,tecnicoresponsavel from TabAnotacoes where codigo=0');

                open;

                Self.ObjDataset.First ;
                Self.status          :=dsInactive;

        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjAnotacoes.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjAnotacoes.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjAnotacoes.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjAnotacoes.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabAnotacoes');
     Result:=Self.ParametroPesquisa;
end;

{function TObjAnotacoes.Get_PesquisaConcluidas: TStringList;
begin

     Self.ParametroPesquisa.clear;
     //Self.ParametroPesquisa.add('Select codigo,contato,fone,concluido,data,Assunto,DataRetorno,Observacoes,origem,datac,userc,datam,userm from TabAnotacoes');
     Self.ParametroPesquisa.add('Select * from TabAnotacoes where concluido=''S''');
     Result:=Self.ParametroPesquisa;
end;

function TObjAnotacoes.Get_PesquisaNaoConcluidas: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabAnotacoes where concluido=''N''');
     Result:=Self.ParametroPesquisa;
end;
 }
function TObjAnotacoes.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Anota��es ';
end;



{
function TObjAnotacoes.Get_PesquisaTurma: string;
begin
     Result:=Self.CodTurma.get_pesquisa;
end;

function TObjAnotacoes.Get_TituloPesquisaTurma: string;
begin
     Result:=Self.Codturma.get_titulopesquisa;
end;
}

function TObjAnotacoes.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_ANOTACOES';

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para Anota��es',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;
end;


function TObjAnotacoes.Get_Assunto: string;
begin
     Result:=Self.Assunto;
end;

function TObjAnotacoes.Get_Concluido: string;
begin
     Result:=Self.Concluido;
end;

function TObjAnotacoes.Get_Contato: string;
begin
     Result:=Self.Contato;
end;

function TObjAnotacoes.Get_Data: string;
begin
     Result:=Self.Data;
end;

function TObjAnotacoes.Get_DataRetorno: string;
begin
     Result:=Self.DataRetorno;
end;

function TObjAnotacoes.Get_Fone: string;
begin
     Result:=Self.Fone;
end;

function TObjAnotacoes.Get_Observacoes: string;
begin
     Result:=Self.observacoes;
end;

procedure TObjAnotacoes.Submit_Assunto(parametro: string);
begin
     Self.Assunto:=parametro;
end;

procedure TObjAnotacoes.Submit_Concluido(parametro: string);
begin
     Self.Concluido:=parametro;
end;

procedure TObjAnotacoes.Submit_Contato(parametro: string);
begin
     Self.Contato:=parametro;
end;

procedure TObjAnotacoes.Submit_Data(parametro: string);
begin
     Self.data:=parametro;
end;

procedure TObjAnotacoes.Submit_DataRetorno(parametro: string);
begin
     Self.DataRetorno:=parametro;
     If (trim(comebarra(Self.dataretorno))='')
     Then Self.dataretorno:='';
end;

procedure TObjAnotacoes.Submit_Fone(parametro: string);
begin
     Self.Fone:=parametro;
end;

procedure TObjAnotacoes.Submit_Observacoes(parametro: string);
begin
     Self.Observacoes:=parametro;
end;



procedure TObjAnotacoes.Imprime(Parametro: string);
var
complemento:String;
Retorno1,Retorno2,Data1,Data2:Tdate;
OrigemTemp:INteger;
ObjConfRelatorio:TObjConfRelatorio;

Begin

	

        With Self.ObjDataset do
     Begin
          close;
          selectsql.clear;
          selectsql.add('Select tabanotacoes.*,TaborigemAnotacoes.nome from tabanotacoes');
          selectsql.add('left join TabOrigemAnotacoes on TabAnotacoes.origem=Taborigemanotacoes.codigo');
          selectsql.add('where tabanotacoes.codigo>-1  ');
     End;



     With FopcaoRel do
     Begin
               With RgOpcoes do
                Begin
                     items.clear;
                     items.add('Todas Anota��es');//0
                     items.add('Anota��es Conclu�das');//1
                     items.add('Anota��es N�o Conclu�das');//2
                     items.add('Espec�fica');//3
                End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;
     End;

     complemento:='';
     Case FopcaoRel.RgOpcoes.ItemIndex of
           1:Self.ObjDataset.SelectSQL.add(' and tabanotacoes.concluido=''S''');//concluidas
           2:Self.ObjDataset.SelectSQL.add(' and tabanotacoes.concluido=''N''');//nao concluidas
           3:Begin
                   If parametro=''
                   Then exit;
                   Self.ObjDataset.SelectSQL.add(' and tabanotacoes.codigo='+#39+Parametro+#39);//especifica
             End;
     End;

     If (FopcaoRel.RgOpcoes.itemindex<>3)
     Then Begin
               limpaedit(Ffiltroimp);



               //Ativo s� os que vou usar
               With FfiltroImp do
               Begin
                    DesativaGrupos;
                    Grupo01.enabled:=True;
                    Grupo02.enabled:=True;
                    Grupo03.enabled:=True;
                    grupo04.enabled:=True;
                    grupo05.Enabled:=True;

                    LbGrupo01.Caption:='Data 01';
                    LbGrupo02.Caption:='Data 02';
                    LbGrupo03.Caption:='Retorno 01';
                    Lbgrupo04.Caption:='Retorno 02';
                    lbgrupo05.Caption:='Origem';

                    edtGrupo01.EditMask:='!99/99/9999;1;_';
                    edtGrupo02.EditMask:='!99/99/9999;1;_';
                    edtGrupo03.EditMask:='!99/99/9999;1;_';
                    edtGrupo04.EditMask:='!99/99/9999;1;_';
                    edtGrupo05.EditMask:='';

                    edtgrupo05.OnKeyDown:=Self.edtorigemKeyDown;

                    //Chamo o form de Configura��o
                    showmodal;

                   If tag=0
                   Then exit;
                   //testo as datas e o retorno
                   //se estiverem certas utilizo-as no comando
                   Try
                      data1:=strtodate(edtgrupo01.text);
                      data2:=strtodate(edtgrupo02.text);
                      Self.ObjDataset.SelectSQL.add(' and (tabanotacoes.data>='+#39+formatdatetime('mm/dd/yyyy',data1)+#39+' and tabanotacoes.data<='+#39+formatdatetime('mm/dd/yyyy',data2)+#39+')');
                   Except
                   End;

                   Try
                      Retorno1:=strtodate(edtgrupo03.text);
                      Retorno2:=strtodate(edtgrupo04.text);
                      Self.ObjDataset.SelectSQL.add(' and (tabanotacoes.dataretorno>='+#39+formatdatetime('mm/dd/yyyy',data1)+#39+' and tabanotacoes.dataretorno<='+#39+formatdatetime('mm/dd/yyyy',data2)+#39+')');
                   Except
                   End;

                   Try
                      OrigemTemp:=Strtoint(edtgrupo05.text);
                      Self.ObjDataset.SelectSQL.add(' and tabanotacoes.origem='+inttostr(origemtemp));
                   Except
                   End;

               end;
          End;

     Self.ObjDataset.SelectSQL.add('order by TabAnotacoes.codigo');
     Self.ObjDataset.open;
     IF (Self.ObjDataset.Recordcount=0)
     Then Begin
               Messagedlg('Nenhum dado foi selecionado na pesquisa!',mtinformation,[mbok],0);
               exit;
     End;

     {Try
        ObjConfRelatorio:=TObjConfRelatorio.create;
        ObjConfRelatorio.RecuperaArquivoRelatorio(Frelanotacoes,'CertificadoAnotacoes');
     Finally
            If ObjConfRelatorio<>nil
            Then Freeandnil(ObjConfRelatorio);
     End;  }



     With Frelanotacoes do
     Begin
          QR.DataSet:=Self.ObjDataset;

          CampoContato.DataSet:=QR.DataSet;
          CampoContato.DataField:='Contato';

          CampoFone.DataSet:=QR.DataSet;
          campofone.DataField:='Fone';

          CampoAssunto.DataSet:=QR.DataSet;
          CampoAssunto.DataField:='Assunto';

          CampoOrigem.DataSet:=QR.DataSet;
          CampoOrigem.DataField:='Origem';

          CampoOrigemNome.DataSet:=QR.DataSet;
          CampoOrigemNome.DataField:='Nome';

          Campodata.DataSet:=QR.DataSet;
          Campodata.DataField:='data';

          campousuario.DataSet:=QR.DataSet;
          campousuario.DataField:='USERC';

          CampoEndereco.DataSet:=QR.DataSet;
          CampoEndereco.DataField:='ENDERECO';

          campotecnico.DataSet:=QR.DataSet;
          campotecnico.DataField:='TECNICORESPONSAVEL';


          CampoDataRetorno.DataSet:=QR.DataSet;
          Campodataretorno.DataField:='dataretorno';
          CampoContato.OnPrint:=Self.CampoContatoPrint;
          qr.preview;


     End;
end;



procedure TObjAnotacoes.CampoContatoPrint(sender: TObject;
  var Value: String);
begin
     Frelanotacoes.CampoObservacao.Lines.clear;
     Frelanotacoes.CampoObservacao.Lines.text:=Self.ObjDataset.fieldbyname('observacoes').asstring;
     //aumentando o tamanho conforme a quantidade de caracteres do memo
     Frelanotacoes.BandaDetalhes.Height:=98+Frelanotacoes.CampoObservacao.Height;
end;






function TObjAnotacoes.Get_Origem: string;
begin
     Result:=Self.Origem.Get_codigo;
end;

procedure TObjAnotacoes.Submit_Origem(parametro: string);
begin
     Self.Origem.Submit_codigo(parametro);
end;

function TObjAnotacoes.Get_PesquisaOrigem: TStringList;
begin
     Result:=Self.Origem.Get_Pesquisa;
end;

function TObjAnotacoes.Get_TituloPesquisaOrigem: string;
begin
     Result:=Self.Origem.Get_TituloPesquisa;
end;

procedure TObjAnotacoes.edtorigemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FOrigemAnotacoes:TFOrigemAnotacoes;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FOrigemAnotacoes:=TFOrigemAnotacoes.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.Get_PesquisaOrigem,Self.Get_TituloPesquisaOrigem,FOrigemAnotacoes)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FOrigemAnotacoes);
     End;

end;



destructor TObjAnotacoes.Free;
begin
     Freeandnil(Self.ObjDataset);
     Self.Origem.free;
     Freeandnil(Self.ParametroPesquisa);
     Freeandnil(Frelanotacoes);
end;

function TObjAnotacoes.PegaOrigemPadrao: string;
begin
     If (ObjParametroGlobal.Validaparametro('ORIGEM PADR�O DAS ANOTA��ES')=False)
     Then Begin
               result:='';
               exit;
     End;
     result:=ObjParametroGlobal.get_valor;
end;

function TObjAnotacoes.Get_Endereco: string;
begin
     result:=Self.Endereco;
end;

function TObjAnotacoes.Get_TecnicoResponsavel: string;
begin
     result:=Self.TecnicoResponsavel;
end;

procedure TObjAnotacoes.Submit_Endereco(parametro: string);
begin
     self.Endereco:=parametro;
end;

procedure TObjAnotacoes.Submit_TecnicoResponsavel(parametro: string);
begin
     Self.TecnicoResponsavel:=parametro;
end;

procedure TObjAnotacoes.edtorigemExit(Sender: TObject; out lborigem: Tlabel);
begin
     lborigem.Caption:='';

     if (TEdit(Sender).Text='')
     Then exit;

     Self.Origem.LocalizaCodigo(TEdit(Sender).Text);
     Self.Origem.TabelaparaObjeto;
     lbOrigem.caption:=Self.Origem.get_nome;
end;

end.
