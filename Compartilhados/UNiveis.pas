unit UNiveis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjniveis,IBDatabase;

type
  TFNiveis = class(TForm)
    edtNome: TEdit;
    lbNome: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb2: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    Label1: TLabel;
    edtNivel: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         DATABASELOCAL:TIBDatabase;
         TRANSACTIONLOCAL:TIBTransaction;

         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
    { Private declarations }
  public
    { Public declarations }
        procedure SetaDatabaseeTransaction(pDatabase: TIBDatabase;
          pTransaction: TIBTransaction);
  end;

var
  FNiveis: TFNiveis;
  Objniveis:TObjniveis;

implementation

uses Uessencialglobal, Upesquisa, UDataModulo, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFNiveis.ControlesParaObjeto: Boolean;
Begin
        Try
        With Objniveis do
        Begin
             Submit_CODIGO(edtNivel.Text);
             Submit_nome(edtnome.text);
             result:=true;
        End;
        Except
          result:=False;
        End;

End;

function TFNiveis.ObjetoParaControles: Boolean;
Begin

        Try
        With Objniveis do
        Begin
             edtNivel.Text:=Get_CODIGO;
             edtnome.text:=Get_nome;
             lbCodigo.Caption:=Get_CODIGO;
             result:=true;
        End;
        Except
              result:=False;
        End;

End;

function TFNiveis.TabelaParaControles: Boolean;
begin
     If (Objniveis.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFNiveis.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  If (Objniveis=Nil) Then
    exit;

  If (Objniveis.status<>dsinactive) Then
  Begin
    Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
    abort;
    exit;
  End;

  Objniveis.free;
  if(Self.Owner = nil) then
    FDataModulo.IBTransaction.Commit;  
end;

procedure TFNiveis.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFNiveis.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
    // desab_botoes(Self);

     //edtcodigo.text:='0';
     lbCodigo.Caption:=Objniveis.Get_novocodigo;
     //edtcodigo.enabled:=False;

     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;

     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Objniveis.status:=dsInsert;

     EdtNome.setfocus;

end;


procedure TFNiveis.btalterarClick(Sender: TObject);
begin
    If (Objniveis.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then
     Begin
                habilita_campos(Self);

                Objniveis.Status:=dsEdit;

                edtnome.setfocus;
               // desab_botoes(Self);

                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible:=false;
                btalterar.Visible:=False;
                btexcluir.Visible:=false;
                btrelatorios.Visible:=false;
                btopcoes.Visible:=false;
                btsair.Visible:=False;
                btpesquisar.visible:=False;
     End;


end;

procedure TFNiveis.btgravarClick(Sender: TObject);
begin

     If Objniveis.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Objniveis.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFNiveis.btexcluirClick(Sender: TObject);
begin
     If (Objniveis.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (Objniveis.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (Objniveis.exclui(lbCodigo.Caption,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFNiveis.btcancelarClick(Sender: TObject);
begin
     Objniveis.cancelar;

     limpaedit(Self);
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     lbCodigo.Caption:='';

end;

procedure TFNiveis.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFNiveis.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Objniveis.Get_pesquisa,Objniveis.Get_TituloPesquisa,Nil,Self.DATABASELOCAL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Objniveis.status<>dsinactive
                                  then exit;

                                  If (Objniveis.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Objniveis.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;

procedure TFNiveis.FormCreate(Sender: TObject);
begin
      self.DATABASELOCAL := FDataModulo.IBDatabase;
      self.TRANSACTIONLOCAL := FDataModulo.IBTransaction;
end;

procedure TFNiveis.SetaDatabaseeTransaction(pDatabase: TIBDatabase;
  pTransaction: TIBTransaction);
begin
      self.DATABASELOCAL:=pDatabase;
      Self.TRANSACTIONLOCAL:=pTransaction;
end;

procedure TFNiveis.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     desabilita_campos(Self);


     Try
        Objniveis:=TObjniveis.create(self.DATABASELOCAL,self.TRANSACTIONLOCAL);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;


     If (Objniveis.VerificaPermissao=False)
     Then desab_botoes(self)
     Else habilita_botoes(Self);
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');
     lbCodigo.caption:='';

end;

end.

