unit UFORMASPAGAMENTO_FC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjFORMASPAGAMENTO_FC,
  jpeg;

type
  TFFORMASPAGAMENTO_FC = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btopcoes: TBitBtn;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    Panel1: TPanel;
    ImagemFundo: TImage;
    LbCODIGO: TLabel;
    LbNOME: TLabel;
    LbDINHEIRO_CHEQUE: TLabel;
    LbAPRAZO: TLabel;
    Lbportador: TLabel;
    lbnomeportador: TLabel;
    EdtCODIGO: TEdit;
    EdtNOME: TEdit;
    Edtportador: TEdit;
    comboDINHEIRO_CHEQUE: TComboBox;
    comboAPRAZO: TComboBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtportadorExit(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFORMASPAGAMENTO_FC: TFFORMASPAGAMENTO_FC;
  ObjFORMASPAGAMENTO_FC:TObjFORMASPAGAMENTO_FC;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFFORMASPAGAMENTO_FC.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjFORMASPAGAMENTO_FC do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_NOME(edtNOME.text);
        Submit_DINHEIRO_CHEQUE(Submit_ComboBox(comboDINHEIRO_CHEQUE));
        Submit_APRAZO(Submit_ComboBox(comboAPRAZO));
        portador.Submit_CODIGO(edtportador.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFFORMASPAGAMENTO_FC.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjFORMASPAGAMENTO_FC do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNOME.text:=Get_NOME;
        if (get_dinheiro_cheque='D')
        Then comboDINHEIRO_CHEQUE.itemindex:=0
        Else comboDINHEIRO_CHEQUE.itemindex:=1;

        if (Get_APRAZO='S')
        Then comboAPRAZO.itemindex:=1
        Else comboAPRAZO.itemindex:=0;
        Edtportador.text:=portador.Get_CODIGO;
        lbnomeportador.caption:=portador.Get_Nome;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFFORMASPAGAMENTO_FC.TabelaParaControles: Boolean;
begin
     If (ObjFORMASPAGAMENTO_FC.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFFORMASPAGAMENTO_FC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjFORMASPAGAMENTO_FC=Nil)
     Then exit;

     If (ObjFORMASPAGAMENTO_FC.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

     ObjFORMASPAGAMENTO_FC.free;
end;

procedure TFFORMASPAGAMENTO_FC.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
            //key:=#0;
      end;
end;



procedure TFFORMASPAGAMENTO_FC.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';

     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjFORMASPAGAMENTO_FC.status:=dsInsert;
     Edtcodigo.setfocus;

end;


procedure TFFORMASPAGAMENTO_FC.btalterarClick(Sender: TObject);
begin
    If (ObjFORMASPAGAMENTO_FC.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjFORMASPAGAMENTO_FC.Status:=dsEdit;
                edtnome.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
          End;


end;

procedure TFFORMASPAGAMENTO_FC.btgravarClick(Sender: TObject);
begin

     If ObjFORMASPAGAMENTO_FC.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjFORMASPAGAMENTO_FC.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjFORMASPAGAMENTO_FC.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFORMASPAGAMENTO_FC.btexcluirClick(Sender: TObject);
begin
     If (ObjFORMASPAGAMENTO_FC.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjFORMASPAGAMENTO_FC.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjFORMASPAGAMENTO_FC.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFFORMASPAGAMENTO_FC.btcancelarClick(Sender: TObject);
begin
     ObjFORMASPAGAMENTO_FC.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFFORMASPAGAMENTO_FC.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFFORMASPAGAMENTO_FC.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjFORMASPAGAMENTO_FC.Get_pesquisa,ObjFORMASPAGAMENTO_FC.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjFORMASPAGAMENTO_FC.status<>dsinactive
                                  then exit;

                                  If (ObjFORMASPAGAMENTO_FC.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjFORMASPAGAMENTO_FC.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFFORMASPAGAMENTO_FC.LimpaLabels;
begin
    lbnomeportador.caption:='';
end;

procedure TFFORMASPAGAMENTO_FC.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        ObjFORMASPAGAMENTO_FC:=TObjFORMASPAGAMENTO_FC.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
     retira_fundo_labels(self);
end;

procedure TFFORMASPAGAMENTO_FC.EdtportadorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjFORMASPAGAMENTO_FC.EdtportadorKeyDown(sender,key,shift,lbnomeportador);
end;

procedure TFFORMASPAGAMENTO_FC.EdtportadorExit(Sender: TObject);
begin
     ObjFORMASPAGAMENTO_FC.EdtportadorExit(sender,lbnomeportador);
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(ObjFORMASPAGAMENTO_FC.OBJETO.Get_Pesquisa,ObjFORMASPAGAMENTO_FC.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(OBJFORMASPAGAMENTO_FC.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If OBJFORMASPAGAMENTO_FC.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(OBJFORMASPAGAMENTO_FC.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
