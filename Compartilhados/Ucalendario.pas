unit Ucalendario;

interface

uses
  StdCtrls,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls;

type
  TFcalendario = class(TForm)
    Calendario: TMonthCalendar;
    procedure CalendarioDblClick(Sender: TObject);
  private
    { Private declarations }
  public
      EditEnviouGlobal:Tobject;
    { Public declarations }
  end;

var
  Fcalendario: TFcalendario;


implementation

{$R *.dfm}

procedure TFcalendario.CalendarioDblClick(Sender: TObject);
begin
     Tedit(EditEnviouGlobal).text:=datetostr(Calendario.date);
     Close;
end;

end.
