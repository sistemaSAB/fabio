unit UobjNCM;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjNCM=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaporCampo(pCampo,pParametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);
                procedure opcoes(pCodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_DESCRICAO(parametro: string);
                Function Get_DESCRICAO: string;
                Procedure Submit_OBSERVACAO(parametro: string);
                procedure submit_CODIGONCM(parametro:string);
                Function Get_OBSERVACAO: string;
                function get_CODIGONCM: string;
                function pegaNCM():string;

                function get_percentualTributo:string;
                procedure submit_percentualTributo(p:string);

                function get_percentualTributoimp:string;
                procedure submit_percentualTributoimp(parametro:string);

                function importaImpostos(pathArquivo:string): Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;

               Codigo:string;
               DESCRICAO:string;
               OBSERVACAO:string;
               CODIGONCM:string;
               percentualTributo:string;
               percentualTributoImp: string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

               procedure importaTabela;overload;
               function importaTabela(pathTable:string):boolean;overload;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios,forms, UNCM, StrUtils, UMostraBarraProgresso;





Function  TObjNCM.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.DESCRICAO:=fieldbyname('DESCRICAO').asstring;
        Self.OBSERVACAO:=fieldbyname('OBSERVACAO').asstring;
        CODIGONCM := fieldbyname('CODIGONCM').AsString;
        percentualTributo := fieldbyname('percentualTributo').AsString;
        percentualTributoimp := fieldbyname('percentualTributoimp').AsString;
        result:=True;
     End;                      
end;


Procedure TObjNCM.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
    ParamByName('Codigo').asstring:=Self.Codigo;
    ParamByName('DESCRICAO').asstring:=Self.DESCRICAO;
    ParamByName('OBSERVACAO').asstring:=Self.OBSERVACAO;
    ParamByName('CODIGONCM').AsString := self.CODIGONCM;
    ParamByName('percentualTributo').AsString := virgulaparaponto(tira_ponto(percentualTributo));
    ParamByName('percentualTributoimp').AsString := virgulaparaponto(tira_ponto(percentualTributoImp));
  End;
End;

//***********************************************************************

function TObjNCM.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjNCM.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        DESCRICAO:='';
        OBSERVACAO:='';
        CODIGONCM:='';
        percentualTributo:='';
        percentualTributoimp := '';
     End;
end;

Function TObjNCM.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
    If (Codigo='') Then
      Mensagem:=mensagem+'/C�digo';

    If (DESCRICAO='') Then
      Mensagem:=mensagem+'/Descri��o';

    if (CODIGONCM = '') then
      Mensagem := Mensagem + '/C�digo NCM';

  End;

  if mensagem<>'' Then
  Begin
    messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
    exit;
  End;

  result:=false;

end;


function TObjNCM.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjNCM.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjNCM.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjNCM.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjNCM.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro NCM vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,CODIGONCM,DESCRICAO,OBSERVACAO,percentualTributo,percentualTributoimp');
           SQL.ADD(' from  TABNCM');
           SQL.ADD(' WHERE codigo='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjNCM.LocalizaporCampo(pCampo, pParametro: string): boolean;
begin
  if (pParametro='') then
  begin
    MensagemErro('Par�metro de busca por campo NCM vazio');
    exit;
  End;
  if (pCampo='') then
  begin
    MensagemErro('Par�metro "Nome do Campo" na busca por campo vazio');
    exit;
  End;

  with Self.Objquery do
  begin
    close;
    Sql.Clear;
    SQL.ADD('Select Codigo,CODIGONCM,DESCRICAO,OBSERVACAO,percentualTributo,percentualTributoimp');
    SQL.ADD(' from  TABNCM');
    SQL.ADD(' WHERE ' + pCampo + '=' + QuotedStr( pParametro ) );

    Open;
    if (recordcount>0) then
      Result:=True
    else
      Result:=False;
  end;

end;

procedure TObjNCM.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjNCM.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjNCM.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjNCM.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABNCM(Codigo,DESCRICAO,OBSERVACAO,CODIGONCM,percentualTributo,percentualTributoimp)');
                InsertSQL.add('values (:Codigo,:DESCRICAO,:OBSERVACAO,:CODIGONCM,:percentualTributo,:percentualTributoimp)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABNCM set Codigo=:Codigo,DESCRICAO=:DESCRICAO');
                ModifySQL.add(',OBSERVACAO=:OBSERVACAO,CODIGONCM=:CODIGONCM,percentualTributo=:percentualTributo');
                ModifySQl.Add(',percentualTributoimp=:percentualTributoimp');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABNCM where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjNCM.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjNCM.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select codigo,codigoncm,percentualtributo,percentualtributoimp,descricao,observacao from TabNCM');
     Result:=Self.ParametroPesquisa;
end;



function TObjNCM.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de NCM ';
end;


function TObjNCM.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENNCM,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENNCM,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjNCM.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjNCM.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjNCM.RetornaCampoNome: string;
begin
      result:='DESCRICAO';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjNCM.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjNCM.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjNCM.Submit_DESCRICAO(parametro: string);
begin
        Self.DESCRICAO:=Parametro;
end;
function TObjNCM.Get_DESCRICAO: string;
begin
        Result:=Self.DESCRICAO;
end;
procedure TObjNCM.Submit_OBSERVACAO(parametro: string);
begin
        Self.OBSERVACAO:=Parametro;
end;
function TObjNCM.Get_OBSERVACAO: string;
begin
        Result:=Self.OBSERVACAO;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjNCM.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJNCM';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TObjNCM.opcoes(pCodigo: string);
begin

  With FOpcaorel do
  Begin

    RgOpcoes.Items.clear;
    RgOpcoes.Items.add('Importar tabela NCM');

    showmodal;

    if (tag=0) Then
      exit;

    case RgOpcoes.ItemIndex of

      0:
      begin
        self.importaTabela;  
      end;

    End;

  End;
  
end;

procedure TObjNCM.importaTabela;
var
  openFile:TOpenDialog;
begin

  openFile := TOpenDialog.Create(nil);
  try

    openFile.Filter := '*.txt';

    if openFile.Execute then
    begin

      try

        Screen.Cursor := crHourGlass;
        if self.importaTabela(openFile.FileName) then
        begin
          FDataModulo.IBTransaction.CommitRetaining;
          MensagemSucesso('Tabela importada!');
        end
        else
          FDataModulo.IBTransaction.RollbackRetaining;

      finally
        Screen.Cursor := crDefault;
      end

    end;

  finally
    FreeAndNil(openFile);
  end;
  
end;

function TObjNCM.importaTabela(pathTable: string):Boolean;
var
  tabela :TextFile;
  descNCM,codNCM,aux :string;
  qery:TIBQuery;
begin

  Result := False;
  if FileExists(pathTable) then
  begin
    AssignFile(tabela,pathTable);
    Reset(tabela);
  end;

  if not exec_sql('delete from TABNCM') then
  begin
    MensagemErro('N�o foi possivel deletar a tabela de NCM para atualiza��o');
    Exit;
  end;

  if not exec_sql('set generator genncm to 0') then
  begin
    MensagemErro('N�o foi possivel alterar GENNCM para 0');
    Exit;
  end;

  qery := TIBQuery.Create(nil);
  qery.Database := FDataModulo.IBDatabase;
  
  while not Eof(tabela) do
  begin
    Application.ProcessMessages;
    Readln(tabela,aux);
    aux := Trim(aux);
    codNCM  := Copy(aux,1,8);
    descNCM := Copy(aux,19,Length(aux));
    descNCM := troca(descNCM,',',' ');

    qery.Active := False;
    qery.SQL.Text := 'insert into TABNCM(CODIGO,CODIGONCM,DESCRICAO) values (0,'+QuotedStr(codNCM)+','+QuotedStr(descNCM)+')';

    try
      qery.ExecSQL;
    except
      on e:Exception do
      begin
        MensagemErro(e.Message);
        Exit;
      end;
    end;

  end;

  result := True;
  FreeAndNil(qery);

end;

function TObjNCM.get_CODIGONCM: string;
begin
  result := Self.CODIGONCM;
end;

procedure TObjNCM.submit_CODIGONCM(parametro: string);
begin
  self.CODIGONCM := parametro;
end;

function TObjNCM.pegaNCM: string;
var
   FpesquisaLocal:TFpesquisa;
   Fncm:TFNCM;
begin


  Fpesquisalocal:=Tfpesquisa.create(Self.Owner);
  FNCM:=TFNCM.Create(Self.Owner);
  
  Try



    If (FpesquisaLocal.PreparaPesquisa(Get_pesquisa,Get_TituloPesquisa,FNCM)) Then
    Begin

      Try

        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          If self.status<>dsinactive then
            exit;

          If (LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False) Then
          Begin
            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
            exit;
          End;

          TabelaparaObjeto;
          Result := self.CODIGONCM;

        End;
      Finally
        FpesquisaLocal.QueryPesq.close;
      End;
    End;

  Finally
    FreeandNil(FPesquisaLocal);
    FreeandNil(FNCM);
  End;

end;

function TObjNCM.get_percentualTributo: string;
begin
  Result := self.percentualTributo;
end;

procedure TObjNCM.submit_percentualTributo(p: string);
begin
  self.percentualTributo := p;
end;

function TObjNCM.importaImpostos(pathArquivo: string): Boolean;
var
  Arquivo, Linha: TStringList;
  cont : Integer;
  NCM,aliquotaNacional,aliquotaImportado,CodNCM, Tmp : string;
  qry : TIBQuery;

  nacionalFed,importadosFed,Estadual,Municipal:Currency;
begin
  {Carregar o txt para a stl
  verifico se � NCM(Nomenclatura Comum Mercosul) ou NBS(Nomenclatura Brasileira Servi�os)
  se o NCM tiver 8 caracteres ent�o localizo e atualizo. neste primeiro momento
  somente os NCM ser�o atualizados
  verificar se o ncm existe, caso exista atualizo
  exemplo do arquivo 0.0.0.1
  codigo;ex;tabela;aliqNac;aliqImp;
  97060000;;0;32.09;36.69;

  exemplo arquivo 0.0.0.2
      0     1    2                3                4      5
  codigo  ;ex;tabela;descricao                ;aliqNac;aliqImp;
  01012100;  ;0     ;Reprodutores de ra�a pura;26.75  ;27.35;

  }
  Result := False;
  Arquivo := TStringList.Create;
  Linha := TStringList.Create;
  qry := TIBQuery.Create(nil);
  qry.Database := FDataModulo.IBDatabase;
  qry.DisableControls;
  try
    Arquivo.LoadFromFile(pathArquivo);
    FMostraBarraProgresso.ConfiguracoesIniciais(Arquivo.Count - 1,0);
    FMostraBarraProgresso.Lbmensagem.Caption := 'Importando percentuais de tributos';
    FMostraBarraProgresso.btcancelar.Visible := True;
    FMostraBarraProgresso.Show;
    Application.ProcessMessages;
    for cont := 0 to Arquivo.Count - 1 do
    begin
      if(ExplodeStr_COMVAZIOS(Arquivo[Cont],Linha,';','string')) then
      begin
        if Linha.Count = 6 then
        begin
          MensagemErro('Este arquivo cont�m tabela de impostos desatualizada. Utilize um arquivo com vig�ncia a partir de 01/01/2015');
          exit;
        end;

        if(Linha.Count > 6) then //se tem a qtde de parametros correto
        begin
          if(Linha[2] = '0') then//� NCM  1 = NBS Nomenclatura Brasileira de Servi�os
          begin
            Tmp := Linha[0];
            nacionalFed := 0;
            importadosFed := 0;
            Estadual := 0;
            Municipal := 0;
            
            if(Length(Tmp) < 8) then //se o ncm est� completo
            begin
              //tenta completar com zeros � esquerda
              Tmp := CompletaPalavra_a_Esquerda( Tmp, 8, '0' );
            end;

            if(Length(Tmp) = 8) then
            begin
              nacionalFed := StrToCurrDef( pontoparavirgula( Linha[4] ), 0 );
              importadosFed := StrToCurrDef( pontoparavirgula( Linha[5] ), 0 );
              Estadual := StrToCurrDef( pontoparavirgula( Linha[6] ), 0 );
              Municipal := StrToCurrDef( pontoparavirgula( Linha[7] ), 0 );

              Self.ZerarTabela;
              if LocalizaporCampo('CODIGONCM', Tmp) then
              begin
                TabelaparaObjeto;
                self.Status := dsEdit;
              end
              else
              begin
                self.Status := dsInsert;
                self.Submit_Codigo(Self.Get_NovoCodigo);
              end;


              if Length( Trim( Get_DESCRICAO ) ) = 0 then //t� limpo, ent�o pego do txt
                Self.Submit_DESCRICAO( Copy( Linha[3], 1, 150) );
              Self.submit_CODIGONCM( Tmp );
              self.submit_percentualTributo( CurrToStr( nacionalFed + Estadual + Municipal ) );  //somar trib federal + estadual + municipal
              self.submit_percentualTributoimp( CurrToStr( importadosFed + Estadual + Municipal ) ); //somar trib federal importados + estadual + municipal

              if not Self.Salvar( true ) then
              begin
                MensagemErro( 'N�o foi poss�vel atualizar Tributos do NCM: ' + Linha[0] );
                Exit;
              end;

              {
              qry.Close;
              qry.SQL.Clear;
              qry.SQL.Add('SELECT CODIGO FROM TABNCM WHERE CODIGONCM='+QuotedStr(Tmp));
              qry.Open;
              if not(qry.IsEmpty) then
              begin
                CodNCM := Tmp;
                aliquotaNacional := Linha[3];
                aliquotaImportado := Linha[4];
                qry.Close;
                qry.SQL.Clear;
                qry.SQL.Add('UPDATE TABNCM SET PERCENTUALTRIBUTO=:pPercentual, PERCENTUALTRIBUTOIMP=:pPercentualimp');
                qry.SQL.Add('WHERE CODIGO=:pCodigoNCM');
                qry.Params[0].AsString := aliquotaNacional;
                qry.Params[1].asstring := aliquotaImportado;
                qry.Params[2].AsString := CodNCM;
                try
                  qry.ExecSQL;
                  qry.Transaction.Commit;
                except
                  raise Exception.Create('N�o foi poss�vel atualizar Tributos do NCM: '+Linha[0]);
                end;
              end;
              }


            end;
          end
          else //1 = NBS Servi�os
          begin
          end;
        end;
      end;
      FMostraBarraProgresso.IncrementaBarra1(1);
      if(FMostraBarraProgresso.Cancelar) then
        Exit;
      Application.ProcessMessages;
    end;
    Result := True;
  finally
    FMostraBarraProgresso.Close;
    if(Arquivo <> nil) then
      FreeAndNil(Arquivo);
    if(Linha <> nil) then
      FreeAndNil(Linha);
    if(qry <> nil) then
      FreeAndNil(qry);
  end;
end;

function TObjNCM.get_percentualTributoimp: string;
begin
  Result := self.percentualTributoImp;
end;

procedure TObjNCM.submit_percentualTributoimp(parametro: string);
begin
  self.percentualTributoImp := parametro;
end;



end.



