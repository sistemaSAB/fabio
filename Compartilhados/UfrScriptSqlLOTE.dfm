object FrScriptSqlLOTE: TFrScriptSqlLOTE
  Left = 0
  Top = 0
  Width = 800
  Height = 600
  Constraints.MinHeight = 600
  Constraints.MinWidth = 800
  TabOrder = 0
  object PanelBotoes: TPanel
    Left = 0
    Top = 0
    Width = 800
    Height = 219
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 456
      Top = 17
      Width = 24
      Height = 13
      Caption = 'Erros'
    end
    object Label2: TLabel
      Left = 716
      Top = 17
      Width = 51
      Height = 13
      Caption = 'Anota'#231#245'es'
    end
    object Panelbotoesbaixo: TPanel
      Left = 1
      Top = 194
      Width = 798
      Height = 24
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object lblocalizar: TLabel
        Left = 17
        Top = 6
        Width = 56
        Height = 13
        Hint = 'Localizar (CTRL+F)'
        Caption = 'Localizar  |  '
        ParentShowHint = False
        ShowHint = True
        OnClick = lblocalizarClick
      end
      object lblocalizaresubstituir: TLabel
        Left = 86
        Top = 6
        Width = 97
        Height = 13
        Hint = 'Localizar e Substituir (CTRL+R)'
        Caption = 'Localizar e Substituir'
        ParentShowHint = False
        ShowHint = True
        OnClick = lblocalizaresubstituirClick
      end
      object Button1: TButton
        Left = 460
        Top = 2
        Width = 75
        Height = 20
        Caption = 'Commit'
        TabOrder = 0
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 540
        Top = 2
        Width = 75
        Height = 20
        Caption = 'Rollback'
        TabOrder = 1
        OnClick = Button2Click
      end
    end
    object PanelBotosCima: TPanel
      Left = 1
      Top = 1
      Width = 798
      Height = 30
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object BtExecutaScript: TBitBtn
        Left = 2
        Top = 2
        Width = 106
        Height = 25
        Caption = '&Executa'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = BtExecutaScriptClick
      end
      object btexecutasqllote: TBitBtn
        Left = 110
        Top = 2
        Width = 136
        Height = 25
        Caption = 'Executa Todos Sqls'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = btexecutasqlloteClick
      end
      object btexecutasqlpasta: TBitBtn
        Left = 248
        Top = 2
        Width = 136
        Height = 25
        Caption = 'Executa Sqls Pasta'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = btexecutasqlpastaClick
      end
    end
    object PanelBotoesMeio: TPanel
      Left = 1
      Top = 31
      Width = 798
      Height = 163
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Panel2: TPanel
        Left = 505
        Top = 0
        Width = 293
        Height = 163
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Panel2'
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 133
          Top = 0
          Width = 5
          Height = 163
          Align = alRight
        end
        object Memoerro: TMemo
          Left = 0
          Top = 0
          Width = 133
          Height = 163
          Align = alClient
          Lines.Strings = (
            'Memoerro')
          ScrollBars = ssVertical
          TabOrder = 0
          WordWrap = False
          OnKeyDown = MemoerroKeyDown
        end
        object memoanotacoes: TMemo
          Left = 138
          Top = 0
          Width = 155
          Height = 163
          Align = alRight
          Lines.Strings = (
            'Memoerro')
          TabOrder = 1
          WordWrap = False
          OnKeyDown = MemoerroKeyDown
        end
      end
      object PanelBotoesMeioEsq: TPanel
        Left = 0
        Top = 0
        Width = 505
        Height = 163
        Align = alLeft
        TabOrder = 1
        object FileListBox: TFileListBox
          Left = 282
          Top = 1
          Width = 222
          Height = 161
          Align = alClient
          ItemHeight = 13
          Mask = '*.sql'
          TabOrder = 0
          OnDblClick = FileListBoxDblClick
        end
        object Panel1: TPanel
          Left = 1
          Top = 1
          Width = 281
          Height = 161
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object DirectoryListBox: TDirectoryListBox
            Left = 8
            Top = 20
            Width = 262
            Height = 139
            ItemHeight = 16
            TabOrder = 0
            OnChange = DirectoryListBoxChange
          end
          object DriveComboBox: TDriveComboBox
            Left = 8
            Top = -2
            Width = 265
            Height = 19
            DirList = DirectoryListBox
            TabOrder = 1
          end
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 581
    Width = 800
    Height = 19
    Panels = <
      item
        Text = 
          'Ctrl+S = Salvar    |   Ctrl+F = Procurar  |  F3 = Localiza Pr'#243'xi' +
          'ma | Ctrl+R = Substituir | Ctrl + Enter = Executa SQL'
        Width = 50
      end>
  end
  object Panel3: TPanel
    Left = 0
    Top = 219
    Width = 800
    Height = 362
    Align = alClient
    BevelOuter = bvNone
    Caption = 'Panel3'
    TabOrder = 2
    object DBGridScript: TDBGrid
      Left = 0
      Top = 204
      Width = 800
      Height = 158
      Align = alBottom
      DataSource = DsScript
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 800
      Height = 204
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel4'
      TabOrder = 1
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 800
        Height = 204
        Align = alClient
        Caption = 'Panel5'
        TabOrder = 0
        object LbScript: TListBox
          Left = 1
          Top = 1
          Width = 798
          Height = 17
          Align = alTop
          ItemHeight = 13
          TabOrder = 0
          Visible = False
          OnDblClick = LbScriptDblClick
        end
        object memo_script: TSynMemo
          Left = 1
          Top = 18
          Width = 798
          Height = 185
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          TabOrder = 1
          OnKeyDown = memo_scriptKeyDown
          Gutter.Font.Charset = DEFAULT_CHARSET
          Gutter.Font.Color = clWindowText
          Gutter.Font.Height = -11
          Gutter.Font.Name = 'Courier New'
          Gutter.Font.Style = []
          Highlighter = SynSQLSyn1
          Lines.Strings = (
            'memo_script')
        end
      end
    end
  end
  object DsScript: TDataSource
    DataSet = IBDataSetScript
    Left = 736
    Top = 37
  end
  object IBScript: TIBScript
    AutoDDL = False
    Database = IBDatabase
    Transaction = IBTransaction
    Terminator = ';'
    OnExecuteError = IBScriptExecuteError
    Left = 664
    Top = 37
  end
  object SaveDialogScript: TSaveDialog
    Left = 800
    Top = 37
  end
  object IBDatabase: TIBDatabase
    LoginPrompt = False
    DefaultTransaction = IBTransaction
    Left = 576
    Top = 37
  end
  object IBDataSetScript: TIBDataSet
    Database = IBDatabase
    Transaction = IBTransaction
    Left = 704
    Top = 37
  end
  object SynEditRegexSearch: TSynEditRegexSearch
    Left = 424
    Top = 72
  end
  object SynEditSearch: TSynEditSearch
    Left = 392
    Top = 72
  end
  object IBTransaction: TIBTransaction
    DefaultDatabase = IBDatabase
    DefaultAction = TARollback
    Params.Strings = (
      'read_committed'
      'rec_version'
      'nowait')
    Left = 616
    Top = 37
  end
  object SynSQLSyn1: TSynSQLSyn
    Left = 664
    Top = 184
  end
end
