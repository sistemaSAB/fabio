unit UobjCODIGORECEITAESTADUAL;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjCODIGORECEITAESTADUAL=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Descricao:string;
//CODIFICA VARIAVEIS PRIVADAS




               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjCODIGORECEITAESTADUAL.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Descricao:=fieldbyname('Descricao').asstring;
//CODIFICA TABELAPARAOBJETO


        result:=True;
     End;
end;


Procedure TObjCODIGORECEITAESTADUAL.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Descricao').asstring:=Self.Descricao;
//CODIFICA OBJETOPARATABELA


  End;
End;

//***********************************************************************

function TObjCODIGORECEITAESTADUAL.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCODIGORECEITAESTADUAL.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Descricao:='';
//CODIFICA ZERARTABELA


     End;
end;

Function TObjCODIGORECEITAESTADUAL.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCODIGORECEITAESTADUAL.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCODIGORECEITAESTADUAL.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCODIGORECEITAESTADUAL.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCODIGORECEITAESTADUAL.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCODIGORECEITAESTADUAL.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CODIGORECEITAESTADUAL vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Descricao');
           SQL.ADD(' from  TABCODIGORECEITAESTADUAL');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCODIGORECEITAESTADUAL.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCODIGORECEITAESTADUAL.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCODIGORECEITAESTADUAL.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCODIGORECEITAESTADUAL.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCODIGORECEITAESTADUAL(Codigo,Descricao');
                InsertSQL.add(' )');
                InsertSQL.add('values (:Codigo,:Descricao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCODIGORECEITAESTADUAL set Codigo=:Codigo,Descricao=:Descricao');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCODIGORECEITAESTADUAL where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCODIGORECEITAESTADUAL.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCODIGORECEITAESTADUAL.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCODIGORECEITAESTADUAL');
     Result:=Self.ParametroPesquisa;
end;

function TObjCODIGORECEITAESTADUAL.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CODIGORECEITAESTADUAL ';
end;


function TObjCODIGORECEITAESTADUAL.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCODIGORECEITAESTADUAL,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCODIGORECEITAESTADUAL,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCODIGORECEITAESTADUAL.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCODIGORECEITAESTADUAL.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCODIGORECEITAESTADUAL.RetornaCampoNome: string;
begin
      result:='DESCRICAO';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCODIGORECEITAESTADUAL.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjCODIGORECEITAESTADUAL.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjCODIGORECEITAESTADUAL.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjCODIGORECEITAESTADUAL.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjCODIGORECEITAESTADUAL.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCODIGORECEITAESTADUAL';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

end.



