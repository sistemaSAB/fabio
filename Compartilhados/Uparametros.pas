unit Uparametros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjParametros, UDataModulo,
  Grids, Menus;

type
  TFparametros = class(TForm)
    Guia: TTabbedNotebook;
    StrGrid: TStringGrid;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    btrelatorios: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    memofuncao: TMemo;
    Panel2: TPanel;
    btCarregar: TBitBtn;
    btAtualizar: TBitBtn;
    ComboValor: TComboBox;
    Memoopcoes: TMemo;
    lbopcoes: TLabel;
    PopupMenuOpcoes: TPopupMenu;
    Editarvalorcomformatao1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure memofuncaoKeyPress(Sender: TObject; var Key: Char);
    procedure btCarregarClick(Sender: TObject);
    procedure btAtualizarClick(Sender: TObject);
    procedure StrGridDblClick(Sender: TObject);
    procedure Editarvalorcomformatao1Click(Sender: TObject);
  private
    ObjParametros:TObjParametros;

    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Function  atualizaQuantidade:string;
  public
    Function Criar:boolean;
    Procedure Destruir;
  end;

var
  Fparametros: TFparametros;

implementation

uses Uessencialglobal, Upesquisa, UescolheImagemBotao, uTextoFormatado;


{$R *.DFM}

//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFparametros.ControlesParaObjeto: Boolean;
begin
  try
    with Self.ObjParametros do
    begin
      Submit_PCODIGO( edtCODIGO.text);
      Submit_Pnome(edtnome.text);
      Submit_PVALOR(ComboValor.Text);
      Submit_PFuncao(memofuncao.lines.text);
      Submit_POpcoes(Memoopcoes.Lines.Text);
      result:=true;
    end;
  except
    result:=False;
  end;
end;

function TFparametros.ObjetoParaControles: Boolean;
var
  items:TStringList;
begin
  try
    items:=TstringList.Create;
  except
    MensagemErro('Erro na cria��o da String List');
    exit;
  end;

  try
    try
      with Self.ObjParametros do
      begin
        edtCODIGO.text          :=GET_PCODIGO;
        edtnome.text            :=GET_Pnome;
        //edtvalor.text           :=GET_Pvalor;
        memofuncao.lines.Text   :=GET_PFuncao;
        Memoopcoes.Lines.text   :=GET_Popcoes;

        ComboValor.Items.Clear;
        ExplodeStr(GeT_Popcoes,Items,'$','STRING');    //Corrigido o separador de $$ para $ - Rodolfo
        ComboValor.Items.text:=items.Text;
        if ComboValor.Items.text <> '' then
        begin
          ComboValor.Style:=csDropDownList;
          ComboValor.ItemIndex:=ComboValor.Items.IndexOf(GET_PValor);
        end
        else
        begin
          ComboValor.Style:=csDropDown;
          ComboValor.Text:=GET_PValor;
        end;
        result:=True;
      End;
    except
      Result:=False;
    end;
  finally
    FreeAndNil(items);
  end;
end;

function TFparametros.TabelaParaControles: Boolean;
begin
  if (Self.ObjParametros.PTabelaparaObjeto=False) then
  begin
    result:=False;
    exit;
  end;

  if (ObjetoParaControles=False) then
  begin
    result:=False;
    exit;
  end;
  
  Result:=True;
end;

//****************************************
//****************************************


procedure TFparametros.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (Self.ObjParametros=Nil)
  then exit;

  if (Self.ObjParametros.status<>dsinactive) then
  begin
    Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
    abort;
    exit;
  end;

  Self.ObjParametros.free;
  if(Self.Owner = nil) then
    FDataModulo.IBTransaction.Commit;
  
end;

procedure TFparametros.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13 then
  begin
    Perform(Wm_NextDlgCtl,0,0);
  end;
end;

procedure TFparametros.BtnovoClick(Sender: TObject);
begin
  limpaedit(Self);

  ComboValor.Style:=csDropDown;
  ComboValor.text:='';
  ComboValor.Items.text:='';
  habilita_campos(Self);
  esconde_botoes(Self);

  edtcodigo.text:='0';
  //edtcodigo.text:=Self.ObjParametros.GET_Pnovocodigo;
  edtcodigo.enabled:=False;


  Btgravar.Visible:=True;
  BtCancelar.Visible:=True;
  btpesquisar.Visible:=True;

  Self.ObjParametros.status:=dsInsert;
  Guia.pageindex:=0;
  Edtnome.setfocus;
end;


procedure TFparametros.btalterarClick(Sender: TObject);
begin
  if (Self.ObjParametros.Status=dsinactive) and (EdtCodigo.text<>'') then
  begin
    habilita_campos(Self);
    EdtCodigo.enabled:=False;
    Self.ObjParametros.Status:=dsEdit;
    guia.pageindex:=0;
    edtnome.setfocus;
    esconde_botoes(Self);
    Btgravar.Visible:=True;
    BtCancelar.Visible:=True;
    btpesquisar.Visible:=True;
  end;
end;

procedure TFparametros.btgravarClick(Sender: TObject);
begin
  if Self.ObjParametros.Status=dsInactive
  then exit;

  if ControlesParaObjeto=False then
  begin
    Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
    exit;
  end;

  if (Self.ObjParametros.salvar(true)=False)
  then exit;

  self.ObjetoParaControles;

  mostra_botoes(Self);
  //limpaedit(Self);
  //ComboValor.text:='';
  desabilita_campos(Self);
  lbquantidade.caption:=atualizaQuantidade;
  Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFparametros.btexcluirClick(Sender: TObject);
begin
  if (Self.ObjParametros.status<>dsinactive) or (Edtcodigo.text='')
  then exit;

  if (Self.ObjParametros.LocalizaCodigo(edtcodigo.text)=False)  then
  begin
    Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
    exit;
  end;

  if (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
  then exit;

  if (Self.ObjParametros.Pexclui(edtcodigo.text,true)=False) then
  begin
    Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
    exit;
  end;
  
  limpaedit(Self);
  ComboValor.text:='';
  lbquantidade.caption:=atualizaQuantidade;
  Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFparametros.btcancelarClick(Sender: TObject);
begin
  Self.ObjParametros.Status:=dsInactive;

  limpaedit(Self);
  ComboValor.text:='';
  ComboValor.Items.Text:='';
  ComboValor.Style:=csDropDown;
  desabilita_campos(Self);
  mostra_botoes(Self);
end;

procedure TFparametros.btsairClick(Sender: TObject);
begin
  Close;
end;

procedure TFparametros.btpesquisarClick(Sender: TObject);
var
  FpesquisaLocal:TFpesquisa;
begin

  try
    Fpesquisalocal:=Tfpesquisa.create(Self);

    if (FpesquisaLocal.PreparaPesquisa(Self.ObjParametros.GET_pesquisa,Self.ObjParametros.GET_TituloPesquisa,Nil)=True) then
    begin
      try
        if (FpesquisaLocal.showmodal=mrok) then
        begin
          if Self.ObjParametros.status<>dsinactive
          then exit;

          if (Self.ObjParametros.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False) then
          begin
            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
            exit;
          end;

          if (TabelaParaControles=False) then
          begin
            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
            limpaedit(Self);
            ComboValor.text:='';
            exit;
          end;
        end;
      finally
        FpesquisaLocal.QueryPesq.close;
      end;
    end;

  finally
    FreeandNil(FPesquisaLocal);
  end;
end;

function TFparametros.Criar: boolean;
begin

end;

procedure TFparametros.Destruir;
begin

end;

procedure TFparametros.btopcoesClick(Sender: TObject);
begin
     Self.ObjParametros.opcoes;
end;

procedure TFparametros.FormShow(Sender: TObject);
begin
  UessencialGlobal.PegaCorForm(Self);
  limpaedit(Self);
  ComboValor.text:='';
  desabilita_campos(Self);

  if(self.Tag<>99)
  then mostra_botoes(self);
  Guia.PageIndex:=0;

  try
    Self.ObjParametros:=TObjParametros.create;

    if (Self.ObjParametros.VerificaPermissao=false)
    then esconde_botoes(self);
  except
    esconde_botoes(self);
    Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
    exit;
  end;

  FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
  FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
  FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
  Self.Color:=clwhite;
  lbquantidade.caption:=atualizaQuantidade;

  if ObjUsuarioGlobal.GET_nome ='SYSDBA' then
  begin
    Memoopcoes.Visible:=True;
    lbopcoes.Visible:=True;
  end
  else
  begin
    Memoopcoes.Visible:=False;
    lbopcoes.Visible:=False;
  end;
end;

procedure TFparametros.btrelatoriosClick(Sender: TObject);
begin
  Self.ObjParametros.Imprime;
end;

procedure TFparametros.memofuncaoKeyPress(Sender: TObject; var Key: Char);
begin
  if key=#13
  then memofuncao.SetFocus;
end;

procedure TFparametros.btCarregarClick(Sender: TObject);
begin
  Self.ObjParametros.ResgataTodosOsParametros(StrGrid);
end;

procedure TFparametros.btAtualizarClick(Sender: TObject);
begin
  if MessageDlg('Tem certeza que deseja Atualizar todos os parametros de uma s� vez?', mtConfirmation, [mbYes, mbNo], 0) = mrNo
  then exit;

  Self.ObjParametros.AtualizaTodosOsParametros(StrGrid);
end;

function TFparametros.atualizaQuantidade: string;
begin
  result:='Existem '+ContaRegistros('TABPARAMETROS','CODIGO')+' Par�metros Cadastrados';
end;

procedure TFparametros.StrGridDblClick(Sender: TObject);
begin
  //if (Self.ObjParametros.LocalizaCodigo()
end;

procedure TFparametros.Editarvalorcomformatao1Click(Sender: TObject);
var
  fTexto: TfTextoFormatado;
begin
  fTexto := TfTextoFormatado.Create(self);
  try
    fTexto.Memo1.Lines.Text := ComboValor.Text;
    fTexto.ShowModal;
    if(fTexto.Tag = 1) then
      ComboValor.Text := fTexto.Memo1.Lines.Text;
  finally
    FreeAndNil(fTexto);
  end;

end;

end.
