unit UObjProcedimentosPesquisa;
interface
uses udatamodulo,uessencialglobal,ibquery,controls,dialogs,db,sysutils,classes,mask,dbgrids;
type
         TobjProcedimentosPesquisa=class
         Public
               edtbusca: TMaskEdit;
               DBGrid: TDBGrid;
               str_pegacampo:String;
               comandosql:String;
               QueryPesq:Tibquery;
               ComplementoPorcentagem:string;//usado para por % na pesquisa de nome automaticamente de acordo com um parametro
               constructor create;
               Procedure AcertaParametros(Pdbgrid:tdbgrid;Pedtbusca:tmaskedit;PQueryPesq:Tibquery);


               procedure DBGridKeyPress(Sender: TObject; var Key: Char);
               procedure edtbuscaKeyPress(Sender: TObject; var Key: Char);
               procedure DBGridKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
               procedure edtbuscaExit(Sender: TObject);
               procedure DBGridDblClick(Sender: TObject);

         Private

         End;


implementation

{ TobjProcedimentosPesquisa }


procedure TobjProcedimentosPesquisa.AcertaParametros(Pdbgrid: tdbgrid;
  Pedtbusca: tmaskedit; PQueryPesq: Tibquery);
begin
     self.edtbusca:=pedtbusca;
     Self.DBGrid:=pdbgrid;
     Pdbgrid.OnDblClick:=Self.DBGridDblClick;
     Pdbgrid.OnKeyPress:=Self.DBGridKeyPress;
     Self.QueryPesq:=PQueryPesq;
     comandosql:=Pquerypesq.sql.text;

     ComplementoPorcentagem:='';
     formatadbgrid(dbgrid);
     If (ObjParametroGlobal.ValidaParametro('% PADRAO NA PESQUISA EM CAMPOS NOME')=True)
     Then Begin
               
               If (ObjParametroGlobal.Get_Valor='SIM')
               Then ComplementoPorcentagem:='%'
               Else ComplementoPorcentagem:='';
     End;


end;

constructor TobjProcedimentosPesquisa.create;
begin
end;

procedure TobjProcedimentosPesquisa.DBGridDblClick(Sender: TObject);
begin

end;

procedure TobjProcedimentosPesquisa.DBGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

end;

procedure TobjProcedimentosPesquisa.DBGridKeyPress(Sender: TObject;
  var Key: Char);
begin

  Case Key of

  #32: Begin
               str_pegacampo:=dbgrid.SelectedField.FieldName;
               Case dbgrid.SelectedField.datatype of

                 ftdatetime :  Begin
                                 edtBusca.EditMask:='00/00/0000 00:00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftdate    :  Begin
                                 edtBusca.EditMask:='00/00/0000;1;_';
                                 EdtBusca.width:=70;
                              End;
                 ftTime    :  Begin
                                 edtBusca.EditMask:='00:00';
                                 EdtBusca.width:=70;
                              End;
                 ftInteger  :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;

                 ftbcd      :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftfloat    :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=250;
                                EdtBusca.maxlength:=8
                             End;
                 ftString   :Begin
                                edtBusca.EditMask:='';
                                EdtBusca.width:=400;
                                EdtBusca.maxlength:=255;
                             End;
                 Else Begin
                           Messagedlg('Est� tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                           DBGrid.setfocus;
                           exit;
                      End;

               End;
               EdtBusca.Text :='';
               EdtBusca.Visible :=true;
               EdtBusca.SetFocus;
       End;
  End;




end;

procedure TobjProcedimentosPesquisa.edtbuscaExit(Sender: TObject);
begin

end;

procedure TobjProcedimentosPesquisa.edtbuscaKeyPress(Sender: TObject;
  var Key: Char);
var
 int_busca:integer;
 str_busca:string;
 flt_busca:Currency;
 data_busca:Tdate;
 hora_busca:Ttime;
 datahora_busca:tdatetime;
 indice_grid:integer;
begin

   if key =#27//ESC
   then Begin
           EdtBusca.Visible := false;
           dbgrid.SetFocus;
           exit;
        End;
   If Key=#13//procura
   Then Begin
          //ordenando pela coluna antes de procurar
          If ( Self.QueryPesq.recordcount>0)
          Then Begin
            try
             indice_grid:=self.DBGrid.SelectedIndex;
             Self.QueryPesq.close;
             Self.QueryPesq.sql.clear;
             Self.QueryPesq.sql.add(comandosql+' order by '+str_pegacampo);
             Self.QueryPesq.open;
             formatadbgrid(dbgrid,querypesq);
             self.DBGrid.SelectedIndex:=indice_grid;
           except
           end;
          end;



             Case dbgrid.SelectedField.DataType of


             ftstring    :Begin
                               //utilizar a pesquisa com like "testar"S
                               try
                                        indice_grid:=self.DBGrid.SelectedIndex;
                                        Self.QueryPesq.close;
                                        Self.QueryPesq.sql.clear;
                                        If(Pos('WHERE',UpperCase(comandosql))<>0)
                                        Then Self.QueryPesq.sql.add(comandosql+' and UPPER('+str_pegacampo+') like '+#39+ComplementoPorcentagem+edtbusca.text+ComplementoPorcentagem+#39)
                                        Else Self.QueryPesq.sql.add(comandosql+' where UPPER('+str_pegacampo+') like '+#39+ComplementoPorcentagem+edtbusca.text+ComplementoPorcentagem+#39);

                                        Self.QueryPesq.sql.add(' order by '+str_pegacampo);
                                        Self.QueryPesq.open;
                                        formatadbgrid(dbgrid,querypesq);
                                        self.DBGrid.SelectedIndex:=indice_grid;
                                except
                                end;
                                //Self.QueryPesq.Locate(str_pegacampo,edtbusca.text,[Lopartialkey]);
                          End;
             ftinteger   :Begin
                              try int_busca:=Strtoint(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,int_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftfloat     :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftBcd      :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftDate     :Begin
                              try data_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,data_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             fttime     :Begin
                              try hora_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,hora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;

             ftDateTime :Begin
                              try datahora_busca:=Strtodatetime(edtbusca.text); except end;
                              if (Self.QueryPesq.Locate(str_pegacampo,datahora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             Else Begin
                       Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                  End;
             End;


             Dbgrid.SetFocus;
             exit;
        End;

//Aqui defino as regras que podem ser digitadas
//como estou utilizando maskEdit Para Datas e Horas
//N�o preciso pois defini uma mascara, mas para
//float e Integer preciso

        Case dbgrid.SelectedField.DataType OF

                ftInteger:  Begin
                                 if not (Key in ['0'..'9',#8])
                                 Then key:=#0;
                            End;
                ftFloat  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftCurrency  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftBCD  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;


        End;
End;



end.
