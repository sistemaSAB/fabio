unit UObjABASMENURELATORIOS;
Interface
Uses windows,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc;

Type
   TObjABASMENURELATORIOS=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaObjetoAba(PObjeto:string;Paba:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Objeto(parametro: string);
                Function Get_Objeto: string;
                Procedure Submit_Aba(parametro: string);
                Function Get_Aba: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                //CODIFICA DECLARA GETSESUBMITS

                
         Private
               ObjDataset:Tibdataset;
               CODIGO:string;
               Objeto:string;
               Aba:string;
               Nome:string;
//CODIFICA VARIAVEIS PRIVADAS




               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjABASMENURELATORIOS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Objeto:=fieldbyname('Objeto').asstring;
        Self.Aba:=fieldbyname('Aba').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
//CODIFICA TABELAPARAOBJETO




        result:=True;
     End;
end;


Procedure TObjABASMENURELATORIOS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('Objeto').asstring:=Self.Objeto;
        fieldbyname('Aba').asstring:=Self.Aba;
        fieldbyname('Nome').asstring:=Self.Nome;
//CODIFICA OBJETOPARATABELA




  End;
End;

//***********************************************************************

function TObjABASMENURELATORIOS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjABASMENURELATORIOS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Objeto:='';
        Aba:='';
        Nome:='';
//CODIFICA ZERARTABELA




     End;
end;

Function TObjABASMENURELATORIOS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjABASMENURELATORIOS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjABASMENURELATORIOS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtoint(Self.Aba);
     Except
           Mensagem:=mensagem+'/Aba';
     End;
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;
     result:=true;

end;

function TObjABASMENURELATORIOS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjABASMENURELATORIOS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;
function TObjABASMENURELATORIOS.LocalizaObjetoAba(PObjeto: string;
  Paba: string): boolean;
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Objeto,Aba,Nome');
           SelectSQL.ADD(' from  TabAbasMenuRelatorios');
           SelectSQL.ADD(' WHERE Objeto='+#39+uppercase(PObjeto)+#39);
           SelectSQL.ADD(' and aba='+#39+Paba+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjABASMENURELATORIOS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Objeto,Aba,Nome');
           SelectSQL.ADD(' from  TabAbasMenuRelatorios');
           SelectSQL.ADD(' WHERE Codigo='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjABASMENURELATORIOS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjABASMENURELATORIOS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjABASMENURELATORIOS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

//CODIFICA CRIACAO DE OBJETOS

        ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select CODIGO,Objeto,Aba,Nome');
                SelectSQL.ADD(' from  TabAbasMenuRelatorios');
                SelectSQL.ADD(' WHERE Codigo=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TabAbasMenuRelatorios(CODIGO,Objeto,Aba');
                InsertSQL.add(' ,Nome)');
                InsertSQL.add('values (:CODIGO,:Objeto,:Aba,:Nome)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabAbasMenuRelatorios set CODIGO=:CODIGO,Objeto=:Objeto');
                ModifySQL.add(',Aba=:Aba,Nome=:Nome');
                ModifySQL.add('where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabAbasMenuRelatorios where Codigo=:Codigo ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,Objeto,Aba,Nome');
                RefreshSQL.ADD(' from  TabAbasMenuRelatorios');
                RefreshSQL.ADD(' WHERE Codigo=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjABASMENURELATORIOS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjABASMENURELATORIOS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabABASMENURELATORIOS');
     Result:=Self.ParametroPesquisa;
end;

function TObjABASMENURELATORIOS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ABASMENURELATORIOS ';
end;


function TObjABASMENURELATORIOS.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
      StrTemp.StoredProcName:='PROC_GERA_ABASMENURELATORIOS';
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o ABASMENURELATORIOS',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjABASMENURELATORIOS.Free;
begin
Freeandnil(Self.ObjDataset);
Freeandnil(Self.ParametroPesquisa);
//CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjABASMENURELATORIOS.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjABASMENURELATORIOS.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjAbasMenuRelatorios.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjAbasMenuRelatorios.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjAbasMenuRelatorios.Submit_Objeto(parametro: string);
begin
        Self.Objeto:=uppercase(Parametro);
end;
function TObjAbasMenuRelatorios.Get_Objeto: string;
begin
        Result:=Self.Objeto;
end;
procedure TObjAbasMenuRelatorios.Submit_Aba(parametro: string);
begin
        Self.Aba:=Parametro;
end;
function TObjAbasMenuRelatorios.Get_Aba: string;
begin
        Result:=Self.Aba;
end;
procedure TObjAbasMenuRelatorios.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjAbasMenuRelatorios.Get_Nome: string;
begin
        Result:=Self.Nome;

end;
//CODIFICA GETSESUBMITS

//CODIFICA EXITONKEYDOWN




end.


