object Fbackup: TFbackup
  Left = 337
  Top = 227
  Width = 421
  Height = 181
  Caption = 'M'#243'dulo de Backup - EXCLAIM TECNOLOGIA'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object BtGeraBackup: TBitBtn
    Left = 308
    Top = 64
    Width = 75
    Height = 34
    Caption = 'Gerar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = BtGeraBackupClick
  end
  object ListaArquivos: TFileListBox
    Left = 123
    Top = 8
    Width = 179
    Height = 123
    ItemHeight = 16
    Mask = '*.gbk'
    ShowGlyphs = True
    TabOrder = 1
  end
  object Diretorio: TDirectoryListBox
    Left = 1
    Top = 8
    Width = 121
    Height = 97
    Enabled = False
    ItemHeight = 16
    TabOrder = 0
    OnChange = DiretorioChange
  end
  object Drive: TDriveComboBox
    Left = 1
    Top = 112
    Width = 121
    Height = 19
    Enabled = False
    TabOrder = 3
    OnChange = DriveChange
  end
  object BackupService: TIBBackupService
    LoginPrompt = False
    TraceFlags = []
    Verbose = True
    BackupFile.Strings = (
      'C:\teste.gbk')
    BlockingFactor = 0
    DatabaseName = 'd:\inacabados\programa'#231#227'o\sysmaster\sysmaster\dados\dados.gdb'
    Options = [NoGarbageCollection]
    Left = 344
    Top = 8
  end
  object RestoreService: TIBRestoreService
    TraceFlags = []
    DatabaseName.Strings = (
      'TEste.gdb')
    PageBuffers = 0
    Options = [Replace, CreateNewDB]
    Left = 312
    Top = 8
  end
end
