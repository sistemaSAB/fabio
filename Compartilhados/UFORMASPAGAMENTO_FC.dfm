object FFORMASPAGAMENTO_FC: TFFORMASPAGAMENTO_FC
  Left = 455
  Top = 322
  Width = 662
  Height = 294
  Caption = 'Formas de Pagamento do Frente de Caixa- EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 646
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 456
      Top = 0
      Width = 190
      Height = 52
      Align = alRight
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Formas de Pagamento FC'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btopcoes: TBitBtn
      Left = 350
      Top = 0
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object Btnovo: TBitBtn
      Left = 0
      Top = 0
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 50
      Top = 0
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 100
      Top = 0
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 150
      Top = 0
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 250
      Top = 0
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 300
      Top = 0
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btexcluir: TBitBtn
      Left = 200
      Top = 0
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 400
      Top = 0
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 206
    Width = 646
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 646
      Height = 50
      Align = alClient
      Stretch = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 52
    Width = 646
    Height = 154
    Align = alClient
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 644
      Height = 152
      Align = alClient
      Stretch = True
    end
    object LbCODIGO: TLabel
      Left = 35
      Top = 26
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbNOME: TLabel
      Left = 35
      Top = 49
      Width = 32
      Height = 14
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbDINHEIRO_CHEQUE: TLabel
      Left = 35
      Top = 73
      Width = 43
      Height = 14
      Caption = 'Esp'#233'cie'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbAPRAZO: TLabel
      Left = 35
      Top = 98
      Width = 42
      Height = 14
      Caption = 'A Prazo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Lbportador: TLabel
      Left = 35
      Top = 122
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbnomeportador: TLabel
      Left = 182
      Top = 123
      Width = 48
      Height = 14
      Caption = 'Portador'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EdtCODIGO: TEdit
      Left = 99
      Top = 24
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object EdtNOME: TEdit
      Left = 99
      Top = 47
      Width = 365
      Height = 19
      MaxLength = 30
      TabOrder = 1
    end
    object Edtportador: TEdit
      Left = 99
      Top = 120
      Width = 72
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 4
      OnExit = EdtportadorExit
      OnKeyDown = EdtportadorKeyDown
    end
    object comboDINHEIRO_CHEQUE: TComboBox
      Left = 99
      Top = 70
      Width = 145
      Height = 21
      BevelKind = bkTile
      ItemHeight = 13
      TabOrder = 2
      Items.Strings = (
        'Dinheiro'
        'Cheque')
    end
    object comboAPRAZO: TComboBox
      Left = 99
      Top = 95
      Width = 145
      Height = 21
      BevelKind = bkTile
      ItemHeight = 13
      TabOrder = 3
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
  end
end
