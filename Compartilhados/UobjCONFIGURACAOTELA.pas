unit UobjCONFIGURACAOTELA;
Interface
Uses mask,forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,extctrls,comctrls;
//USES_INTERFACE


Type
   TObjCONFIGURACAOTELA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaNomeFormulario(Parametro:string) :boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_NomeFormulario(parametro: string);
                Function Get_NomeFormulario: string;
                Procedure Submit_Configuracoes(parametro: String);
                Function Get_Configuracoes: String;
                //CODIFICA DECLARA GETSESUBMITS
                function ResgataConfiguracoes(Pformulario: Tform): Boolean;overload;
                function GravaConfiguracoes(Pformulario: Tform): Boolean;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               NomeFormulario:string;

               Configuracoes:TStringList;
               Configuracoes_Stream:TmemoryStream;
//CODIFICA VARIAVEIS PRIVADAS
               //**************************************************************
               Left:integer;
               Top:integer;
               visible:boolean;
               //**************************************************************
               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;
               Function  ResgataConfiguracoes(PnomeComponente:String):boolean;overload;


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjCONFIGURACAOTELA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
var
  Temp:TStream;
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.NomeFormulario:=fieldbyname('NomeFormulario').asstring;
        try
           try
             Temp:=CreateBlobStream(FieldByName('configuracoes'),bmRead);
             Self.Configuracoes.LoadFromStream(temp);
           Except
                 Messagedlg('Erro na Tentativa de Recuperar o valor do Campo Configura��es!',mterror,[mbok],0);
                 exit;
           End;
        Finally
               freeandnil(temp);
        End;

//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjCONFIGURACAOTELA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NomeFormulario').asstring:=Self.NomeFormulario;
        Self.Configuracoes_Stream.Clear;
        Self.Configuracoes.SaveToStream(Self.Configuracoes_Stream);
        ParamByName('configuracoes').LoadFromStream(Self.Configuracoes_Stream,ftBlob);
  End;
End;

//***********************************************************************

function TObjCONFIGURACAOTELA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCONFIGURACAOTELA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NomeFormulario:='';
        Configuracoes.Text:='';
//CODIFICA ZERARTABELA



     End;
end;

Function TObjCONFIGURACAOTELA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (NomeFormulario='')
      Then Mensagem:=mensagem+'/Nome do Formul�rio';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCONFIGURACAOTELA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCONFIGURACAOTELA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCONFIGURACAOTELA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCONFIGURACAOTELA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCONFIGURACAOTELA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CONFIGURACAOTELA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NomeFormulario,Configuracoes');
           SQL.ADD(' from  TabConfiguracaoTela');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjCONFIGURACAOTELA.LocalizaNomeFormulario(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro Nome do Formul�rio vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NomeFormulario,Configuracoes');
           SQL.ADD(' from  TabConfiguracaoTela');
           SQL.ADD(' WHERE NomeFormulario='+#39+uppercase(parametro)+#39);
//CODIFICA LOCALIZACODIGO
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;
procedure TObjCONFIGURACAOTELA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCONFIGURACAOTELA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCONFIGURACAOTELA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.Configuracoes       :=TStringList.Create;
        Self.Configuracoes_Stream:=TmemoryStream.Create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabConfiguracaoTela(CODIGO,NomeFormulario');
                InsertSQL.add(' ,Configuracoes)');
                InsertSQL.add('values (:CODIGO,:NomeFormulario,:Configuracoes)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabConfiguracaoTela set CODIGO=:CODIGO,NomeFormulario=:NomeFormulario');
                ModifySQL.add(',Configuracoes=:Configuracoes');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabConfiguracaoTela where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCONFIGURACAOTELA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCONFIGURACAOTELA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCONFIGURACAOTELA');
     Result:=Self.ParametroPesquisa;
end;

function TObjCONFIGURACAOTELA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CONFIGURACAOTELA ';
end;


function TObjCONFIGURACAOTELA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCONFIGURACAOTELA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCONFIGURACAOTELA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCONFIGURACAOTELA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Freeandnil(Configuracoes);
    freeandnil(Configuracoes_Stream);
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCONFIGURACAOTELA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCONFIGURACAOTELA.RetornaCampoNome: string;
begin
      result:='nomeformulario';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjConfiguracaoTela.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjConfiguracaoTela.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjConfiguracaoTela.Submit_NomeFormulario(parametro: string);
begin
        Self.NomeFormulario:=Parametro;
end;
function TObjConfiguracaoTela.Get_NomeFormulario: string;
begin
        Result:=Self.NomeFormulario;
end;
procedure TObjConfiguracaoTela.Submit_Configuracoes(parametro: String);
begin
        Self.Configuracoes.text:=Parametro;
end;
function TObjConfiguracaoTela.Get_Configuracoes: String;
begin
        Result:=Self.Configuracoes.text;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjCONFIGURACAOTELA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCONFIGURACAOTELA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;


end;



function TObjConfiguracaoTela.GravaConfiguracoes(Pformulario:Tform): Boolean;
var
  limpa:integer;
  pgrava:boolean;
  pvisible,pleft,ptop,pnome:String;

begin
   result:=False;
   Self.Configuracoes.Clear;
   
   for limpa:=0 to Pformulario.ComponentCount -1
   do begin
           pgrava:=False;

           if uppercase(PFormulario.Components [limpa].ClassName) = 'TEDIT'
           then Begin
                     pgrava:=true;
                     pnome:=uppercase(Tedit(PFormulario.Components [limpa]).name);
                     pleft:=inttostr(Tedit(PFormulario.Components [limpa]).left);
                     ptop:=inttostr(Tedit(PFormulario.Components [limpa]).top);
                     if (Tedit(PFormulario.Components [limpa]).visible=True)
                     Then pvisible:='TRUE'
                     Else pvisible:='FALSE';
           End
           Else
               if uppercase(PFormulario.Components [limpa].ClassName) = 'TLABEL'
               then Begin
                         pgrava:=true;
                         pnome:=uppercase(TLabel(PFormulario.Components [limpa]).name);
                         pleft:=inttostr(Tlabel(PFormulario.Components [limpa]).left);
                         ptop:=inttostr(Tlabel(PFormulario.Components [limpa]).top);
                         if (Tlabel(PFormulario.Components [limpa]).visible=True)
                         Then pvisible:='TRUE'
                         Else pvisible:='FALSE';
               End
               Else
                   if uppercase(PFormulario.Components [limpa].ClassName) = 'TMASKEDIT'
                   then Begin
                             pgrava:=true;
                             pnome:=uppercase(TMaskEdit(PFormulario.Components [limpa]).name);
                             pleft:=inttostr(TMaskEdit(PFormulario.Components [limpa]).left);
                             ptop:=inttostr(TMaskEdit(PFormulario.Components [limpa]).top);
                             if (TMaskEdit(PFormulario.Components [limpa]).visible=True)
                             Then pvisible:='TRUE'
                             Else pvisible:='FALSE';
                   End
                   Else
                       if uppercase(PFormulario.Components [limpa].ClassName) = 'TCOMBOBOX'
                       then Begin
                                 pgrava:=true;
                                 pnome:=uppercase(TComboBox(PFormulario.Components [limpa]).name);
                                 pleft:=inttostr(TComboBox(PFormulario.Components [limpa]).left);
                                 ptop:=inttostr(TComboBox(PFormulario.Components [limpa]).top);
                                 if (TComboBox(PFormulario.Components [limpa]).visible=True)
                                 Then pvisible:='TRUE'
                                 Else pvisible:='FALSE';
                       End
                       Else
                           if uppercase(PFormulario.Components [limpa].ClassName) = 'TMEMO'
                           then Begin
                                     pgrava:=true;
                                     pnome:=uppercase(TMEMO(PFormulario.Components [limpa]).name);
                                     pleft:=inttostr(TMEMO(PFormulario.Components [limpa]).left);
                                     ptop:=inttostr(TMEMO(PFormulario.Components [limpa]).top);
                                     if (TMEMO(PFormulario.Components [limpa]).visible=True)
                                     Then pvisible:='TRUE'
                                     Else pvisible:='FALSE';
                           End
                           Else
                               if uppercase(PFormulario.Components [limpa].ClassName) = 'TRADIOBUTTON'
                               then Begin
                                         pgrava:=true;
                                         pnome:=uppercase(TRADIOBUTTON(PFormulario.Components [limpa]).name);
                                         pleft:=inttostr(TRADIOBUTTON(PFormulario.Components [limpa]).left);
                                         ptop:=inttostr(TRADIOBUTTON(PFormulario.Components [limpa]).top);
                                         if (TRADIOBUTTON(PFormulario.Components [limpa]).visible=True)
                                         Then pvisible:='TRUE'
                                         Else pvisible:='FALSE';
                               End
                               Else
                                   if uppercase(PFormulario.Components [limpa].ClassName) = 'TCHECKBOX'
                                   then Begin
                                             pgrava:=true;
                                             pnome:=uppercase(TCHECKBOX(PFormulario.Components [limpa]).name);
                                             pleft:=inttostr(TCHECKBOX(PFormulario.Components [limpa]).left);
                                             ptop:=inttostr(TCHECKBOX(PFormulario.Components [limpa]).top);
                                             if (TCHECKBOX(PFormulario.Components [limpa]).visible=True)
                                             Then pvisible:='TRUE'
                                             Else pvisible:='FALSE';
                                   End
                                   Else
                                       if uppercase(PFormulario.Components [limpa].ClassName) = 'TLISTBOX'
                                       then Begin
                                                 pgrava:=true;
                                                 pnome:=uppercase(TLISTBOX(PFormulario.Components [limpa]).name);
                                                 pleft:=inttostr(TLISTBOX(PFormulario.Components [limpa]).left);
                                                 ptop:=inttostr(TLISTBOX(PFormulario.Components [limpa]).top);
                                                 if (TLISTBOX(PFormulario.Components [limpa]).visible=True)
                                                 Then pvisible:='TRUE'
                                                 Else pvisible:='FALSE';
                                       End
                                       Else
                                           if uppercase(PFormulario.Components [limpa].ClassName) = 'TRADIOGROUP'
                                           then Begin
                                                     pgrava:=true;
                                                     pnome:=uppercase(TRADIOGROUP(PFormulario.Components [limpa]).name);
                                                     pleft:=inttostr(TRADIOGROUP(PFormulario.Components [limpa]).left);
                                                     ptop:=inttostr(TRADIOGROUP(PFormulario.Components [limpa]).top);
                                                     if (TRADIOGROUP(PFormulario.Components [limpa]).visible=True)
                                                     Then pvisible:='TRUE'
                                                     Else pvisible:='FALSE';
                                           End
                                           Else
                                               if uppercase(PFormulario.Components [limpa].ClassName) = 'TRICHEDIT'
                                               then Begin
                                                         pgrava:=true;
                                                         pnome:=uppercase(TRICHEDIT(PFormulario.Components [limpa]).name);
                                                         pleft:=inttostr(TRICHEDIT(PFormulario.Components [limpa]).left);
                                                         ptop:=inttostr(TRICHEDIT(PFormulario.Components [limpa]).top);
                                                         if (TRICHEDIT(PFormulario.Components [limpa]).visible=True)
                                                         Then pvisible:='TRUE'
                                                         Else pvisible:='FALSE';
                                               End
                                               Else
                                                   if uppercase(PFormulario.Components [limpa].ClassName) = 'TSHAPE'
                                                   then Begin
                                                             pgrava:=true;
                                                             pnome:=uppercase(TSHAPE(PFormulario.Components [limpa]).name);
                                                             pleft:=inttostr(TSHAPE(PFormulario.Components [limpa]).left);
                                                             ptop:=inttostr(TSHAPE(PFormulario.Components [limpa]).top);
                                                             if (TSHAPE(PFormulario.Components [limpa]).visible=True)
                                                             Then pvisible:='TRUE'
                                                             Else pvisible:='FALSE';
                                                   End;

           if (PGrava=true)
           Then Begin
                     Self.Configuracoes.Add(uppercase(pnome)+'.LEFT='+pleft);
                     Self.Configuracoes.Add(uppercase(pnome)+'.TOP='+ptop);
                     Self.Configuracoes.Add(uppercase(pnome)+'.VISIBLE='+pvisible);
           End;
   End;
   result:=True;
end;



function TObjConfiguracaoTela.ResgataConfiguracoes(Pformulario:Tform): Boolean;
var
  limpa:integer;
begin
   for limpa:=0 to Pformulario.ComponentCount -1
   do begin
        if uppercase(PFormulario.Components [limpa].ClassName) = 'TEDIT'
        then Begin
                  if (Self.ResgataConfiguracoes(Tedit(PFormulario.Components [limpa]).Name)=True)
                  Then Begin
                            Tedit(PFormulario.Components [limpa]).left:=Self.Left;
                            Tedit(PFormulario.Components [limpa]).top:=Self.top;
                            Tedit(PFormulario.Components [limpa]).visible:=Self.visible;
                  End;
        End
        Else
            if uppercase(PFormulario.Components [limpa].ClassName) = 'TLABEL'
            then Begin
                      if (Self.ResgataConfiguracoes(TLabel(PFormulario.Components [limpa]).Name)=True)
                      Then Begin
                                TLabel(PFormulario.Components [limpa]).left:=Self.Left;
                                TLabel(PFormulario.Components [limpa]).top:=Self.top;
                                TLabel(PFormulario.Components [limpa]).visible:=Self.visible;
                      End;
            End
            Else
                if uppercase(PFormulario.Components [limpa].ClassName) = 'TMASKEDIT'
                then Begin
                          if (Self.ResgataConfiguracoes(TMaskEdit(PFormulario.Components [limpa]).Name)=True)
                          Then Begin
                                  TMaskEdit(PFormulario.Components [limpa]).left:=Self.Left;
                                  TMaskEdit(PFormulario.Components [limpa]).top:=Self.top;
                                  TMaskEdit(PFormulario.Components [limpa]).visible:=Self.visible;
                          End;
                End
                Else
                    if uppercase(PFormulario.Components [limpa].ClassName) = 'TCOMBOBOX'
                    then Begin
                              if (Self.ResgataConfiguracoes(TComboBox(PFormulario.Components [limpa]).Name)=True)
                              Then Begin
                                      TComboBox(PFormulario.Components [limpa]).left:=Self.Left;
                                      TComboBox(PFormulario.Components [limpa]).top:=Self.top;
                                      TComboBox(PFormulario.Components [limpa]).visible:=Self.visible;
                              End;
                    End
                    Else
                        if uppercase(PFormulario.Components [limpa].ClassName) = 'TMEMO'
                        then Begin
                                  if (Self.ResgataConfiguracoes(TMEMO(PFormulario.Components [limpa]).Name)=True)
                                  Then Begin
                                          TMEMO(PFormulario.Components [limpa]).left:=Self.Left;
                                          TMEMO(PFormulario.Components [limpa]).top:=Self.top;
                                          TMEMO(PFormulario.Components [limpa]).visible:=Self.visible;
                                  End;
                        End
                        Else
                            if uppercase(PFormulario.Components [limpa].ClassName) = 'TRADIOBUTTON'
                            then Begin
                                      if (Self.ResgataConfiguracoes(TRADIOBUTTON(PFormulario.Components [limpa]).Name)=True)
                                      Then Begin
                                              TRADIOBUTTON(PFormulario.Components [limpa]).left:=Self.Left;
                                              TRADIOBUTTON(PFormulario.Components [limpa]).top:=Self.top;
                                              TRADIOBUTTON(PFormulario.Components [limpa]).visible:=Self.visible;
                                      End;
                            End
                            Else
                                if uppercase(PFormulario.Components [limpa].ClassName) = 'TCHECKBOX'
                                then Begin
                                          if (Self.ResgataConfiguracoes(TCHECKBOX(PFormulario.Components [limpa]).Name)=True)
                                          Then Begin
                                                  TCHECKBOX(PFormulario.Components [limpa]).left:=Self.Left;
                                                  TCHECKBOX(PFormulario.Components [limpa]).top:=Self.top;
                                                  TCHECKBOX(PFormulario.Components [limpa]).visible:=Self.visible;
                                          End;
                                End
                                Else
                                    if uppercase(PFormulario.Components [limpa].ClassName) = 'TLISTBOX'
                                    then Begin
                                              if (Self.ResgataConfiguracoes(TLISTBOX(PFormulario.Components [limpa]).Name)=True)
                                              Then Begin
                                                      TLISTBOX(PFormulario.Components [limpa]).left:=Self.Left;
                                                      TLISTBOX(PFormulario.Components [limpa]).top:=Self.top;
                                                      TLISTBOX(PFormulario.Components [limpa]).visible:=Self.visible;
                                              End;
                                    End
                                    Else
                                        if uppercase(PFormulario.Components [limpa].ClassName) = 'TRADIOGROUP'
                                        then Begin
                                                  if (Self.ResgataConfiguracoes(TRADIOGROUP(PFormulario.Components [limpa]).Name)=True)
                                                  Then Begin
                                                          TRADIOGROUP(PFormulario.Components [limpa]).left:=Self.Left;
                                                          TRADIOGROUP(PFormulario.Components [limpa]).top:=Self.top;
                                                          TRADIOGROUP(PFormulario.Components [limpa]).visible:=Self.visible;
                                                  End;
                                        End
                                        Else
                                            if uppercase(PFormulario.Components [limpa].ClassName) = 'TRICHEDIT'
                                            then Begin
                                                      if (Self.ResgataConfiguracoes(TRICHEDIT(PFormulario.Components [limpa]).Name)=True)
                                                      Then Begin
                                                              TRICHEDIT(PFormulario.Components [limpa]).left:=Self.Left;
                                                              TRICHEDIT(PFormulario.Components [limpa]).top:=Self.top;
                                                              TRICHEDIT(PFormulario.Components [limpa]).visible:=Self.visible;
                                                      End;
                                            End
                                            Else
                                                if uppercase(PFormulario.Components [limpa].ClassName) = 'TSHAPE'
                                                then Begin
                                                          if (Self.ResgataConfiguracoes(TSHAPE(PFormulario.Components [limpa]).Name)=True)
                                                          Then Begin
                                                                  TSHAPE(PFormulario.Components [limpa]).left:=Self.Left;
                                                                  TSHAPE(PFormulario.Components [limpa]).top:=Self.top;
                                                                  TSHAPE(PFormulario.Components [limpa]).visible:=Self.visible;
                                                          End;
                                                End

   End;
end;




function TObjCONFIGURACAOTELA.ResgataConfiguracoes(
  PnomeComponente: String): boolean;
var
cont:integer;
Plinha,ptemp:String;
quant,posicao,final:integer;
begin
     result:=False;

     left:=-1;
     top:=-1;
     visible:=False;
     
     PnomeComponente:=Uppercase(PnomeComponente);
     //essa funcao procura dentro do StringList as configuracoes para o componente em questao
     for cont:=0 to Self.Configuracoes.Count-1 do
     Begin
          plinha:=Self.Configuracoes[cont];

          //PROCURANDO A POSICAO DO LEFT DO COMPONENTE ATUAL
          posicao:=pos(pnomecomponente+'.LEFT=',plinha);
          if (posicao>0)
          Then Begin
                    Quant:=length(pnomecomponente+'.LEFT=');
                    final:=length(plinha)-(posicao+quant-1);
                    ptemp:=copy(plinha,posicao+quant,final);
                    Try
                       Self.left:=Strtoint(Ptemp);
                       result:=True;
                    Except
                          result:=False;
                          exit;
                    End;
          End
          Else Begin
                    //PROCURANDO A POSICAO DO TOP DO COMPONENTE ATUAL
                    posicao:=pos(pnomecomponente+'.TOP=',plinha);
                    if (posicao>0)
                    Then Begin
                              Quant:=length(pnomecomponente+'.TOP=');
                              final:=length(plinha)-(posicao+quant-1);
                              ptemp:=copy(plinha,posicao+quant,final);
                              Try
                                 Self.top:=Strtoint(Ptemp);
                                 result:=True;
                              Except
                                    result:=False;
                                    exit;
                              End;
                    End
                    Else Begin
                              //PROCURANDO A POSICAO DO TOP DO COMPONENTE ATUAL
                              posicao:=pos(pnomecomponente+'.VISIBLE=',plinha);
                              if (posicao>0)
                              Then Begin
                                        Quant:=length(pnomecomponente+'.VISIBLE=');
                                        final:=length(plinha)-(posicao+quant-1);
                                        ptemp:=copy(plinha,posicao+quant,final);
                                        if (Ptemp='TRUE')
                                        Then Self.visible:=True
                                        Else
                                            if (Ptemp='FALSE')
                                            Then Self.visible:=False
                                            Else Begin
                                                      result:=False;
                                                      exit;
                                            End;
                                        result:=true;
                                        exit;
                              End;
                    End
          End;
     End;
end;

end.



