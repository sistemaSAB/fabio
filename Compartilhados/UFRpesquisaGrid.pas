unit UFRpesquisaGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, Mask, Grids, DBGrids, DB, IBCustomDataSet, IBQuery,uessencialglobal,
  ExtCtrls;

type
  TFRpesquisaGrid = class(TFrame)
    QueryPesquisa: TIBQuery;
    DsPesquisa: TDataSource;
    DBGridPesquisa: TDBGrid;
    Panel1: TPanel;
    edtbusca: TMaskEdit;
    procedure DBGridPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure edtbuscaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtbuscaExit(Sender: TObject);
  private
    { Private declarations }
    str_pegacampo:String;
    ComplementoPorcentagem:string;//usado para por % na pesquisa de nome automaticamente de acordo com um parametro
    ComandoSql:String;

  public
    { Public declarations }
    Procedure Inicializa;
    Procedure ExecutaPesquia(PComandoSql:String;PcampoOrdenar:string);
  end;

implementation

uses UDataModulo;

{$R *.dfm}

procedure TFRpesquisaGrid.DBGridPesquisaKeyPress(Sender: TObject; var Key: Char);
begin
     if (Self.QueryPesquisa.Active=False)
     Then exit;

     Case Key of

        #32: Begin
                  str_pegacampo:=DBGridPesquisa.SelectedField.FieldName;

                  Case DBGridPesquisa.SelectedField.datatype of

                    ftdatetime :  Begin
                                    edtBusca.EditMask:='00/00/0000 00:00:00';
                                    EdtBusca.width:=70;
                                 End;
                    ftdate    :  Begin
                                    edtBusca.EditMask:='00/00/0000;1;_';
                                    EdtBusca.width:=70;
                                 End;
                    ftTime    :  Begin
                                    edtBusca.EditMask:='00:00';
                                    EdtBusca.width:=70;
                                 End;
                    ftInteger  :Begin
                                   edtBusca.EditMask:='';
                                   EdtBusca.width:=250;
                                   EdtBusca.maxlength:=8
                                End;
                  
                    ftbcd      :Begin
                                   edtBusca.EditMask:='';
                                   EdtBusca.width:=250;
                                   EdtBusca.maxlength:=8
                                End;
                    ftfloat    :Begin
                                   edtBusca.EditMask:='';
                                   EdtBusca.width:=250;
                                   EdtBusca.maxlength:=8
                                End;
                    ftString   :Begin
                                   edtBusca.EditMask:='';
                                   EdtBusca.width:=400;
                                   EdtBusca.maxlength:=255;
                                End;
                    Else Begin
                              Messagedlg('Est� tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                              DBGridPesquisa.setfocus;
                              exit;
                         End;

                  End;
                  EdtBusca.Text :='';
                  EdtBusca.Visible :=true;
                  EdtBusca.SetFocus;
        End;
     End;
end;

procedure TFRpesquisaGrid.ExecutaPesquia(PComandoSql:STRING;PcampoOrdenar:string);
begin
     Self.ComandoSql:=Pcomandosql;
     QueryPesquisa.Close;
     QueryPesquisa.sql.clear;
     QueryPesquisa.sql.add(Self.ComandoSql);

     if (Pcampoordenar<>'')
     Then QueryPesquisa.sql.add(' order by '+pcampoordenar);

     QueryPesquisa.Open;
     formatadbgrid(Self.DBGridPesquisa);
end;

procedure TFRpesquisaGrid.Inicializa;
var
cont:integer;
begin
          
     edtbusca.Visible:=False;
     QueryPesquisa.Database:=FDataModulo.ibdatabase;

         
     ComplementoPorcentagem:='';
     If (ObjParametroGlobal.ValidaParametro('% PADRAO NA PESQUISA EM CAMPOS NOME')=True)
     Then Begin
               If (ObjParametroGlobal.Get_Valor='SIM')
               Then ComplementoPorcentagem:='%'
               Else ComplementoPorcentagem:='';
     End;

end;
     
procedure TFRpesquisaGrid.edtbuscaKeyPress(Sender: TObject; var Key: Char);
var
 int_busca:integer;
 str_busca:string;
 flt_busca:Currency;
 data_busca:Tdate;
 hora_busca:Ttime;
 datahora_busca:tdatetime;
 indice_grid:integer;
begin

   if key =#27//ESC
   then Begin
           EdtBusca.Visible := false;
           DBGridPesquisa.SetFocus;
           exit;
        End;
   If Key=#13//procura
   Then Begin
          //ordenando pela coluna antes de procurar
          If ( Self.QueryPesquisa.recordcount>0)
          Then Begin
            try
             indice_grid:=self.DBGridPesquisa.SelectedIndex;
             Self.querypesquisa.close;
             Self.querypesquisa.sql.clear;
             Self.querypesquisa.sql.add(comandosql+' order by '+str_pegacampo);
             Self.querypesquisa.open;
             formatadbgrid(DBGridPesquisa,querypesquisa);
             self.DBGridPesquisa.SelectedIndex:=indice_grid;
           except
           end;
          end;

          Case DBGridPesquisa.SelectedField.DataType of


             ftstring    :Begin
                               //utilizar a pesquisa com like "testar"
                               try
                                        indice_grid:=self.DBGridPesquisa.SelectedIndex;
                                        Self.querypesquisa.close;
                                        Self.querypesquisa.sql.clear;
                                        If(Pos('WHERE',UpperCase(comandosql))<>0)
                                        Then Self.querypesquisa.sql.add(comandosql+' and UPPER('+str_pegacampo+') like '+#39+ComplementoPorcentagem+edtbusca.text+ComplementoPorcentagem+#39)
                                        Else Self.querypesquisa.sql.add(comandosql+' where UPPER('+str_pegacampo+') like '+#39+ComplementoPorcentagem+edtbusca.text+ComplementoPorcentagem+#39);
                                        Self.querypesquisa.sql.add(' order by codigo,'+str_pegacampo);
                                        Self.querypesquisa.open;
                                        formatadbgrid(DBGridPesquisa,querypesquisa);
                                        self.DBGridPesquisa.SelectedIndex:=indice_grid;
                                except
                                end;
                                //Self.querypesquisa.Locate(str_pegacampo,edtbusca.text,[Lopartialkey]);
                          End;

             ftinteger   :Begin
                              try int_busca:=Strtoint(edtbusca.text); except end;
                              if (Self.querypesquisa.Locate(str_pegacampo,int_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftfloat     :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.querypesquisa.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftBcd      :Begin
                              try flt_busca:=Strtofloat(edtbusca.text); except end;
                              if (Self.querypesquisa.Locate(str_pegacampo,flt_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             ftDate     :Begin
                              try data_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.querypesquisa.Locate(str_pegacampo,data_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             fttime     :Begin
                              try hora_busca:=Strtodate(edtbusca.text); except end;
                              if (Self.querypesquisa.Locate(str_pegacampo,hora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;

             ftDateTime :Begin
                              try datahora_busca:=Strtodatetime(edtbusca.text); except end;
                              if (Self.querypesquisa.Locate(str_pegacampo,datahora_busca,[loCaseInsensitive])=False)
                              Then Messagedlg('Valor n�o encontrado!',mtinformation,[mbok],0);
                         End;
             Else Begin
                       Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                  End;
             End;


             DBGridPesquisa.SetFocus;
             exit;
        End;

//Aqui defino as regras que podem ser digitadas
//como estou utilizando maskEdit Para Datas e Horas
//N�o preciso pois defini uma mascara, mas para
//float e Integer preciso

        Case DBGridPesquisa.SelectedField.DataType OF

                ftInteger:  Begin
                                 if not (Key in ['0'..'9',#8])
                                 Then key:=#0;
                            End;
                ftFloat  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftCurrency  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftBCD  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;


        End;
End;

procedure TFRpesquisaGrid.DBGridPesquisaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
indice_grid:integer;
cont,marca_registro:integer ;
comandoordena:String;
usaupper:boolean;
ColunaAtual:integer;
begin
     if (key=VK_Delete)
     then begin
               dbgridpesquisa.Columns.Items[dbgridpesquisa.SelectedIndex].Visible :=false;
               dbgridpesquisa.SelectedIndex :=dbgridpesquisa.SelectedIndex +1;
     end;



     if (key=VK_f12)
     Then Begin
               try
                   ColunaAtual:=dbgridpesquisa.SelectedIndex;
                   str_pegacampo:=dbgridpesquisa.SelectedField.FieldName;
                   if (DBGridPesquisa.SelectedField.DataType=ftstring)
                   then usaupper:=true
                   else usaupper:=false;

                   
                   if (Length(str_pegacampo)>4)
                   Then Begin
                             if (uppercase(copy(str_pegacampo,1,2))='XX')
                             and (uppercase(copy(str_pegacampo,length(str_pegacampo)-1,2))='XX')
                             Then exit;
                   End;

                   indice_grid:=self.dbgridpesquisa.SelectedIndex;
                   marca_registro:= self.dbgridpesquisa.DataSource.DataSet.fieldbyname('codigo').AsInteger;
                   ComandoOrdena:=comandosql+' order by ';

                   If (ssCtrl in Shift)
                   Then Begin  //Control pressionado, sinal de ordem por mais de uma coluna
                             //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
                             If(Pos('order ',Self.querypesquisa.sql.text)<>0)
                             Then ComandoOrdena:=Self.querypesquisa.sql.text+','
                             Else ComandoOrdena:=comandosql+' order by ';
                   End;

                   Self.querypesquisa.close;
                   Self.querypesquisa.sql.clear;
                   if (usaupper)
                   then Self.querypesquisa.sql.add(Comandoordena+'UPPER('+str_pegacampo+')')
                   Else Self.querypesquisa.sql.add(Comandoordena+str_pegacampo);
                   Self.querypesquisa.open;
                   dbgridpesquisa.SelectedIndex:=ColunaAtual;
                   formatadbgrid(dbgridpesquisa,querypesquisa);

               except

               end;
               dbgridpesquisa.setfocus;
     End
     Else Begin
               if key=VK_F11
               Then Begin
                         for cont:=0 to dbgridpesquisa.Columns.Count-1 do
                         dbgridpesquisa.Columns.Items[cont].Visible :=true;
               End;
     End;
end;


procedure TFRpesquisaGrid.edtbuscaExit(Sender: TObject);
begin
     edtbusca.Visible:=False;
end;


end.
