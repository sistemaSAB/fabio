unit UReltxtRDPRINT;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, RDprint,printers;

type
  TFreltxtRDPRINT = class(TForm)
    botaoimprimir: TBitBtn;
    RDprint: TRDprint;
    procedure RDprintBeforeNewPage(Sender: TObject; Pagina: Integer);
    procedure RDprintNewPage(Sender: TObject; Pagina: Integer);

  private

    { Private declarations }

  public
    { Public declarations }
    LinhaLocal:Integer;

    ImprimeCabecalho:boolean;
    procedure preview(Imprime:Boolean);
    Procedure ConfiguraImpressao;
    Procedure ConfiguraImpressaoRepasseProp_BIA(PQtdeLinhas:Integer);
    Procedure ConfiguraImpressaoSEMCabecalho;
    procedure ConfiguraImpressaoPapelPaisagem;
    Procedure VerificaLinha(Componente:TrdPrint;out linha:integer);overload;
    Procedure VerificaLinha;overload;
    Procedure VerificaLinha(QtdeLinhasSobrar:Integer);overload;
    Function  VerificaLinha_comretorno:Boolean;
    Procedure IncrementaLinha(PQuantidade:Integer);
    Function  VerificaLinhaImprimeSubTotal(Componente:TrdPrint;out linha:integer; out PSubTotal : Currency):Boolean;
    Procedure DesenhaLinha;
  end;

var
  FreltxtRDPRINT: TFreltxtRDPRINT;
  cabecalho:String;
implementation

uses URelTXT, UDataModulo, UessencialGlobal;

{$R *.DFM}

procedure TFreltxtRDPRINT.preview(Imprime:Boolean);
var
cont,cont2:integer;
valorparametro,value,apoio:string;
linha,posicao:Integer;
Tmpnegrito:Boolean;
begin

     Self.ConfiguraImpressao;
     rdprint.Orientacao:=poPortrait;
     RDprint.OpcoesPreview.Preview :=iMPRIME;
     rdprint.Abrir;
     //a linha 1 e 2 � do cabe�alho, na funcao rdprintNEWPAGE
     linha            := 3;
     //Imprimindo o titulo e os subtitulos
     if (Freltxt.LBTITULO.enabled=true)
     Then Begin
               rdprint.impc(linha,48,Freltxt.LBTITULO.Caption,[negrito]);
               inc(linha,1);
     End;
     if (Freltxt.LBSUBTITULO1.enabled=true)
     Then Begin
               rdprint.imp (linha,01,Freltxt.LBSUBTITULO1.Caption);
               inc(linha,1);
     End;
     if (Freltxt.LBSUBTITULO2.enabled=true)
     Then Begin
               rdprint.imp (linha,01,Freltxt.LBSUBTITULO2.Caption);
               inc(linha,1);
     End;
     //imprimindo uma linha que ira separar
     rdprint.imp (linha,01,'-----------------------------------------------------------------------------------------------');
     inc(linha,1);

     for cont:=0 to Freltxt.sb.items.count-1 do
     Begin
        if (linha=(rdprint.TamanhoQteLinhas-3))
        Then Begin
                rdprint.novapagina;
                linha:=3;//as duas primeiras saum do cabecalho
        End;
        //antes de imprimir tenho que verificar
        //se naum tem algum caracter especial de controle
        //que era usado na Freltxt para negrito, cor entre outros
        value:='';
        if (length(Freltxt.sb.items[cont])<>0)
        Then Begin
                case Freltxt.sb.items[cont][1] of
                     '?':Begin//negritpo
                              value:=copy(Freltxt.sb.items[cont],2,length(Freltxt.sb.items[cont]));
                              rdprint.impf (linha,01,value,[negrito]);
                     end;
                     '�':Begin//era usado para cor, no caso da matricial naum tem cor
                                apoio:=copy(Freltxt.sb.items[cont],2,length(Freltxt.sb.items[cont]));
                                posicao:=pos('�',apoio);
                                If (posicao=0)//tem que ter o fecho e entre eles a cor
                                Then value:=Freltxt.sb.items[cont]
                                Else Begin
                                        apoio:=copy(apoio,1,posicao-1);
                                        value:=copy(Freltxt.sb.items[cont],posicao+2,length(Freltxt.sb.items[cont]));
                                End;
                                rdprint.imp (linha,01,value);
                     end;
                     '�':Begin//indica que vai ter negrito mesclado na linha
                              //para funcionar basta abrir com � onde quer que inicie
                              //o negrito e fechar com � caso queira que pare

                              apoio:=copy(Freltxt.sb.items[cont],2,length(Freltxt.sb.items[cont]));
                              posicao:=pos('�',apoio);
                              If (posicao=0)//naum encontrou o sinal de abertura de negrito
                              Then rdprint.imp (linha,01,apoio)
                              Else Begin
                                        tmpnegrito:=False;
                                        for cont2:=1 to length(apoio) do
                                        Begin
                                             if (apoio[cont2]='�')
                                             Then Tmpnegrito:=not(Tmpnegrito)
                                             Else Begin
                                                       if (Tmpnegrito=true)
                                                       Then rdprint.impf (linha,cont2,apoio[cont2],[negrito])
                                                       Else rdprint.impf (linha,cont2,apoio[cont2],[]); 
                                             End;
                                        End;
                              End;
                     End;
                     Else rdprint.imp(linha,01,Freltxt.sb.items[cont]);
                End;
        End;
        inc(linha,1);
     End;
     rdprint.fechar;  // Encerra o relat�rio...
end;

procedure TFreltxtRDPRINT.ConfiguraImpressao;
begin
     rdprint.OpcoesPreview.PreviewZoom:=91;
     rdprint.OpcoesPreview.Preview := True;
     rdprint.TitulodoRelatorio:='';
     rdprint.CaptionSetup:='';
     rdprint.FonteTamanhoPadrao:=S12cpp;
     //19/10/2012 celio. Ap�s utilizar o posicionamento landcape(paisagem), alguns relat�rios acabam sendo impressos nesta forma.
     //para isso estou setando as configura��es padr�o sempre que chama o configura iompressao
     rdprint.Orientacao:=poPortrait;
     ImprimeCabecalho:=True;
     cabecalho:='';

     if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then cabecalho:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin

               cabecalho:=ObjParametroGlobal.get_valor;
     End;

     cabecalho:=cabecalho+'  - '+datetostr(now)+'-'+timetostr(now);

     if (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE COLUNAS PAPEL RETRATO')=FALSE)
     Then Begin
                rdprint.TamanhoQteColunas:=96;
                Messagedlg('O Par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO" n�o foi encontrado!'+#13+'Ser� usado 93 linhas',mterror,[mbok],0);
     End
     Else Begin
               
               TRY
               rdprint.TamanhoQteColunas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO"'+#13+'Ser� usado 96 colunas',mterror,[mbok],0);
                     rdprint.TamanhoQteColunas:=96;
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE LINHAS PAPEL RETRATO')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "QUANTIDADE DE LINHAS PAPEL RETRATO" n�o foi encontrado!'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
               rdprint.TamanhoQteLinhas:=66;
     End
     Else Begin
               
               TRY
                 rdprint.TamanhoQteLinhas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE LINHAS PAPEL RETRATO"'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
                     rdprint.TamanhoQteLinhas:=66;
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('PORTA DE IMPRESS�O')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "PORTA DE IMPRESS�O" n�o foi encontrado!'+#13+'Ser� usado "LPT1"',mterror,[mbok],0);
               rdprint.PortaComunicacao:='LPT1';
     End
     Else Begin
               
               TRY
                 rdprint.PortaComunicacao:=ObjParametroGlobal.get_valor;
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "PORTA DE IMPRESS�O"'+#13+'Ser� usado  "LPT1"',mterror,[mbok],0);
                     rdprint.PortaComunicacao:='LPT1';
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('ACENTUA��O NOS RELAT�RIOS?')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "ACENTUA��O NOS RELAT�RIOS?" n�o foi encontrado!'+#13+'Ser� usado "N�O"',mterror,[mbok],0);
               rdprint.Acentuacao:=SemAcento;
     End
     Else Begin
               
               IF (ObjParametroGlobal.get_valor='SIM')
               THEN rdprint.Acentuacao:=Transliterate
               Else rdprint.Acentuacao:=SemAcento;
     End;

     if (ObjParametroGlobal.ValidaParametro('USA GERENCIADOR DE IMPRESS�O DO WINDOWS')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS" n�o foi encontrado!'+#13+'Ser� usado "SIM"',mterror,[mbok],0);
               rdprint.UsaGerenciadorImpr:=True;
     End
     Else Begin
               
               if (ObjParametroGlobal.get_valor='SIM')
               Then rdprint.UsaGerenciadorImpr:=True
               Else Begin
                  If (ObjParametroGlobal.get_valor='N�O')
                  Then rdprint.UsaGerenciadorImpr:=False
                  Else Begin
                            Messagedlg('Valor inv�lido no par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS"'+#13+'Ser� usado  "SIM"',mterror,[mbok],0);
                            rdprint.UsaGerenciadorImpr:=true;
                  End;
               End;
     End;

end;

procedure TFreltxtRDPRINT.RDprintBeforeNewPage(Sender: TObject;
  Pagina: Integer);
begin
     // Rodap�...
{     rdprint.imp (64,01,'===============================================================================================');
     rdprint.impf(65,01,'Deltress Inform�tica Ltda',[italico]);
     rdprint.impf(65,65,'Demonstra��o RdPrint 3.0',[comp17]);
     rdprint.imp (66, conta * 3,inttostr(conta));}
end;

procedure TFreltxtRDPRINT.RDprintNewPage(Sender: TObject;
  Pagina: Integer);
begin
     if (ImprimeCabecalho=True)
     Then Begin
           // Cabe�alho...
           rdprint.imp (01,01,cabecalho);

           if (RDprint.FonteTamanhoPadrao=s20cpp)//160 colunas
           Then rdprint.impD(02,160,'P�gina: ' + formatfloat('000',pagina),[])
           Else
               if (RDprint.FonteTamanhoPadrao=s17cpp)//130 colunas
               Then rdprint.impD(02,130,'P�gina: ' + formatfloat('000',pagina),[])
               Else rdprint.impf(02,82,'P�gina: ' + formatfloat('000',pagina),[]);
     End;
     
     //Linha  := 8;
end;

procedure TFreltxtRDPRINT.VerificaLinha(Componente: TrdPrint; out linha: integer);
begin
     if (linha>=(Componente.TamanhoQteLinhas-3))
     Then Begin
               linha:=3;
               componente.Novapagina;
     End;
end;




Function TFreltxtRDPRINT.VerificaLinhaImprimeSubTotal(Componente: TrdPrint; out linha: integer; out PSubTotal: Currency): Boolean;
begin
Result := False;
     if (linha>=(Componente.TamanhoQteLinhas-3))
     Then Begin
               Componente.ImpF(linha,75,'SubTotal: '+formata_valor(CurrToStr(PSubTotal)),[Negrito]);
               PSubTotal:=0;
               linha:=3;
               componente.Novapagina;
               Result:=true;
     End;
end;

procedure TFreltxtRDPRINT.VerificaLinha;
begin
     if (Self.LinhaLocal>=(Self.rdprint.TamanhoQteLinhas-3))
     Then Begin
               Self.LinhaLocal:=3;
               Self.rdprint.Novapagina;
     End;

end;


procedure TFreltxtRDPRINT.VerificaLinha(QtdeLinhasSobrar:Integer);
begin
     if (Self.LinhaLocal>=(Self.rdprint.TamanhoQteLinhas-QtdeLinhasSobrar))
     Then Begin
               Self.LinhaLocal:=3;
               Self.rdprint.Novapagina;
     End;
end;


procedure TFreltxtRDPRINT.IncrementaLinha(PQuantidade: Integer);
begin
     inc(Self.LinhaLocal,PQuantidade);
end;

procedure TFreltxtRDPRINT.ConfiguraImpressaoPapelPaisagem;
begin

     rdprint.OpcoesPreview.PreviewZoom:=91;
     rdprint.OpcoesPreview.Preview := True;
     rdprint.TitulodoRelatorio:='';
     rdprint.CaptionSetup:='';
     rdprint.FonteTamanhoPadrao:=S12cpp;
     rdprint.Orientacao:=poLandscape;
     ImprimeCabecalho:=True;
     cabecalho:='';

     if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then cabecalho:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin
               
               cabecalho:=ObjParametroGlobal.get_valor;
     End;
     cabecalho:=cabecalho+'  - '+datetostr(now)+'-'+timetostr(now);

     if (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE COLUNAS PAPEL PAISAGEM')=FALSE)
     Then Begin
                rdprint.TamanhoQteColunas:=96;
                Messagedlg('O Par�metro "QUANTIDADE DE COLUNAS PAPEL PAISAGEM" n�o foi encontrado!'+#13+'Ser� usado 93 linhas',mterror,[mbok],0);
     End
     Else Begin
               
               TRY
               rdprint.TamanhoQteColunas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE COLUNAS PAPEL PAISAGEM"'+#13+'Ser� usado 96 colunas',mterror,[mbok],0);
                     rdprint.TamanhoQteColunas:=96;
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE LINHAS PAPEL PAISAGEM')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "QUANTIDADE DE LINHAS PAPEL PAISAGEM" n�o foi encontrado!'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
               rdprint.TamanhoQteLinhas:=66;
     End
     Else Begin
               
               TRY
                 rdprint.TamanhoQteLinhas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE LINHAS PAPEL PAISAGEM"'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
                     rdprint.TamanhoQteLinhas:=66;
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('PORTA DE IMPRESS�O')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "PORTA DE IMPRESS�O" n�o foi encontrado!'+#13+'Ser� usado "LPT1"',mterror,[mbok],0);
               rdprint.PortaComunicacao:='LPT1';
     End
     Else Begin
               
               TRY
                 rdprint.PortaComunicacao:=ObjParametroGlobal.get_valor;
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "PORTA DE IMPRESS�O"'+#13+'Ser� usado  "LPT1"',mterror,[mbok],0);
                     rdprint.PortaComunicacao:='LPT1';
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('ACENTUA��O NOS RELAT�RIOS?')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "ACENTUA��O NOS RELAT�RIOS?" n�o foi encontrado!'+#13+'Ser� usado "N�O"',mterror,[mbok],0);
               rdprint.Acentuacao:=SemAcento;
     End
     Else Begin
               
               IF (ObjParametroGlobal.get_valor='SIM')
               THEN rdprint.Acentuacao:=Transliterate
               Else rdprint.Acentuacao:=SemAcento;
     End;

     if (ObjParametroGlobal.ValidaParametro('USA GERENCIADOR DE IMPRESS�O DO WINDOWS')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS" n�o foi encontrado!'+#13+'Ser� usado "SIM"',mterror,[mbok],0);
               rdprint.UsaGerenciadorImpr:=True;
     End
     Else Begin
               
               if (ObjParametroGlobal.get_valor='SIM')
               Then Begin
//                      rdprint.TestarPorta:=False;//naum pode testar a porta se usar o gerenciador
                      rdprint.UsaGerenciadorImpr:=True
               End
               Else Begin
                  If (ObjParametroGlobal.get_valor='N�O') 
                  Then Begin
//                            rdprint.testarporta:=True;
                            rdprint.UsaGerenciadorImpr:=False;
                  End
                  Else Begin
                            Messagedlg('Valor inv�lido no par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS"'+#13+'Ser� usado  "SIM"',mterror,[mbok],0);
//                            rdprint.TestarPorta:=False;
                            rdprint.UsaGerenciadorImpr:=true;
                  End;
               End;
     End;  

End;


procedure TFreltxtRDPRINT.DesenhaLinha;
begin
      Self.VerificaLinha;
      RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',Self.RDprint.TamanhoQteColunas,'_'));
      IncrementaLinha(1);
end;

function TFreltxtRDPRINT.VerificaLinha_comretorno: Boolean;
begin
     //Retorna True se pulou a pagina
     result:=False;
     if (Self.LinhaLocal>=(Self.rdprint.TamanhoQteLinhas-3))
     Then Begin
               Self.LinhaLocal:=3;
               Self.rdprint.Novapagina;
               result:=True;
     End;

end;

procedure TFreltxtRDPRINT.ConfiguraImpressaoSEMCabecalho;
begin
     rdprint.OpcoesPreview.PreviewZoom:=91;
     rdprint.OpcoesPreview.Preview := True;
     rdprint.TitulodoRelatorio:='';
     rdprint.CaptionSetup:='';
     rdprint.FonteTamanhoPadrao:=S12cpp;
     rdprint.Orientacao:=poPortrait;
     ImprimeCabecalho:=false;
     cabecalho:='';

     if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then cabecalho:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin
               
               cabecalho:=ObjParametroGlobal.get_valor;
     End;
     cabecalho:=cabecalho+'  - '+datetostr(now)+'-'+timetostr(now);

     if (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE COLUNAS PAPEL RETRATO')=FALSE)
     Then Begin
                rdprint.TamanhoQteColunas:=96;
                Messagedlg('O Par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO" n�o foi encontrado!'+#13+'Ser� usado 93 linhas',mterror,[mbok],0);
     End
     Else Begin
               
               TRY
               rdprint.TamanhoQteColunas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO"'+#13+'Ser� usado 96 colunas',mterror,[mbok],0);
                     rdprint.TamanhoQteColunas:=96;
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE LINHAS PAPEL RETRATO')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "QUANTIDADE DE LINHAS PAPEL RETRATO" n�o foi encontrado!'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
               rdprint.TamanhoQteLinhas:=66;
     End
     Else Begin
               
               TRY
                 rdprint.TamanhoQteLinhas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE LINHAS PAPEL RETRATO"'+#13+'Ser� usado 66 linhas',mterror,[mbok],0);
                     rdprint.TamanhoQteLinhas:=66;
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('PORTA DE IMPRESS�O')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "PORTA DE IMPRESS�O" n�o foi encontrado!'+#13+'Ser� usado "LPT1"',mterror,[mbok],0);
               rdprint.PortaComunicacao:='LPT1';
     End
     Else Begin
               
               TRY
                 rdprint.PortaComunicacao:=ObjParametroGlobal.get_valor;
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "PORTA DE IMPRESS�O"'+#13+'Ser� usado  "LPT1"',mterror,[mbok],0);
                     rdprint.PortaComunicacao:='LPT1';
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('ACENTUA��O NOS RELAT�RIOS?')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "ACENTUA��O NOS RELAT�RIOS?" n�o foi encontrado!'+#13+'Ser� usado "N�O"',mterror,[mbok],0);
               rdprint.Acentuacao:=SemAcento;
     End
     Else Begin
               
               IF (ObjParametroGlobal.get_valor='SIM')
               THEN rdprint.Acentuacao:=Transliterate
               Else rdprint.Acentuacao:=SemAcento;
     End;

     if (ObjParametroGlobal.ValidaParametro('USA GERENCIADOR DE IMPRESS�O DO WINDOWS')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS" n�o foi encontrado!'+#13+'Ser� usado "SIM"',mterror,[mbok],0);
               rdprint.UsaGerenciadorImpr:=True;
     End
     Else Begin
               
               if (ObjParametroGlobal.get_valor='SIM')
               Then rdprint.UsaGerenciadorImpr:=True
               Else Begin
                  If (ObjParametroGlobal.get_valor='N�O') 
                  Then rdprint.UsaGerenciadorImpr:=False
                  Else Begin
                            Messagedlg('Valor inv�lido no par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS"'+#13+'Ser� usado  "SIM"',mterror,[mbok],0);
                            rdprint.UsaGerenciadorImpr:=true;
                  End;
               End;
     End;

end;

procedure TFreltxtRDPRINT.ConfiguraImpressaoRepasseProp_BIA(PQtdeLinhas: Integer);
begin
     rdprint.OpcoesPreview.PreviewZoom:=91;
     rdprint.OpcoesPreview.Preview := True;
     rdprint.TitulodoRelatorio:='';
     rdprint.CaptionSetup:='';
     rdprint.FonteTamanhoPadrao:=S12cpp;
     rdprint.Orientacao:=poPortrait;
     ImprimeCabecalho:=True;
     cabecalho:='';

     if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then cabecalho:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin
               
               cabecalho:=ObjParametroGlobal.get_valor;
     End;
     cabecalho:=cabecalho+'  - '+datetostr(now)+'-'+timetostr(now);

     if (ObjParametroGlobal.ValidaParametro('QUANTIDADE DE COLUNAS PAPEL RETRATO')=FALSE)
     Then Begin
                rdprint.TamanhoQteColunas:=96;
                Messagedlg('O Par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO" n�o foi encontrado!'+#13+'Ser� usado 93 linhas',mterror,[mbok],0);
     End
     Else Begin
               
               TRY
               rdprint.TamanhoQteColunas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE COLUNAS PAPEL RETRATO"'+#13+'Ser� usado 96 colunas',mterror,[mbok],0);
                     rdprint.TamanhoQteColunas:=96;
               END;
     End;



     rdprint.TamanhoQteLinhas:=PQtdeLinhas;



     if (ObjParametroGlobal.ValidaParametro('PORTA DE IMPRESS�O')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "PORTA DE IMPRESS�O" n�o foi encontrado!'+#13+'Ser� usado "LPT1"',mterror,[mbok],0);
               rdprint.PortaComunicacao:='LPT1';
     End
     Else Begin
               
               TRY
                 rdprint.PortaComunicacao:=ObjParametroGlobal.get_valor;
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "PORTA DE IMPRESS�O"'+#13+'Ser� usado  "LPT1"',mterror,[mbok],0);
                     rdprint.PortaComunicacao:='LPT1';
               END;
     End;

     if (ObjParametroGlobal.ValidaParametro('ACENTUA��O NOS RELAT�RIOS?')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "ACENTUA��O NOS RELAT�RIOS?" n�o foi encontrado!'+#13+'Ser� usado "N�O"',mterror,[mbok],0);
               rdprint.Acentuacao:=SemAcento;
     End
     Else Begin
               
               IF (ObjParametroGlobal.get_valor='SIM')
               THEN rdprint.Acentuacao:=Transliterate
               Else rdprint.Acentuacao:=SemAcento;
     End;

     if (ObjParametroGlobal.ValidaParametro('USA GERENCIADOR DE IMPRESS�O DO WINDOWS')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS" n�o foi encontrado!'+#13+'Ser� usado "SIM"',mterror,[mbok],0);
               rdprint.UsaGerenciadorImpr:=True;
     End
     Else Begin
               
               if (ObjParametroGlobal.get_valor='SIM')
               Then rdprint.UsaGerenciadorImpr:=True
               Else Begin
                  If (ObjParametroGlobal.get_valor='N�O')
                  Then rdprint.UsaGerenciadorImpr:=False
                  Else Begin
                            Messagedlg('Valor inv�lido no par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS"'+#13+'Ser� usado  "SIM"',mterror,[mbok],0);
                            rdprint.UsaGerenciadorImpr:=true;
                  End;
               End;
     End;

end;

end.
