unit UvisualizaTabela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBTable, StdCtrls, Grids, DBGrids,
  ExtCtrls, DBCtrls, ComCtrls, Buttons, UfrScriptSql, IBDatabase;

type
  TFvisualizatabela = class(TForm)
    Ibtabela: TIBTable;
    DsTabela: TDataSource;
    Guia: TPageControl;
    TabSheet1: TTabSheet;
    DBNavigatorTabela: TDBNavigator;
    DBGridTabela: TDBGrid;
    TabSheet2: TTabSheet;
    FrScriptSql1: TFrScriptSql;
    Panel1: TPanel;
    BtCommit_tabela: TButton;
    btRollback_Tabela: TButton;
    Combotabela: TComboBox;
    Button1: TButton;
    procedure CombotabelaKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure BtCommit_tabelaClick(Sender: TObject);
    procedure btRollback_TabelaClick(Sender: TObject);
    procedure FrScriptSql1BtExecutaScriptClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fvisualizatabela: TFvisualizatabela;

implementation

uses UessencialGlobal, UDataModulo, UFiltraImp, //UmostraStringGrid,
  UmostraStringList;

{$R *.dfm}

procedure TFvisualizatabela.CombotabelaKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Begin
               Try
                  Ibtabela.Close;
                  Ibtabela.TableName:=Combotabela.text;
                  Ibtabela.open;
               Except
                     on e:exception do
                     Begin
                          mensagemerro(E.message);
                          exit;
                     End;
               End;
     End;

end;

procedure TFvisualizatabela.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Ibtabela.close;
end;

procedure TFvisualizatabela.FormShow(Sender: TObject);
begin
    Try
     Combotabela.Enabled:=True;
     Combotabela.text:='';
     Combotabela.Items.clear;
     Ibtabela.Database:=FDataModulo.IBDatabase;
     Combotabela.Items.text:=Ibtabela.TableNames.text;
     Combotabela.ItemIndex:=0;

    Except
          on e:exception do
          Begin
               Combotabela.Enabled:=False;
               mensagemerro(e.message);
               exit;
          End;
    End;

    FrScriptSql1.InicializaFrame;

    Guia.TabIndex:=0;

end;

procedure TFvisualizatabela.BtCommit_tabelaClick(Sender: TObject);
begin
    FDataModulo.IBTransaction.CommitRetaining;


end;

procedure TFvisualizatabela.btRollback_TabelaClick(Sender: TObject);
begin
FDataModulo.IBTransaction.RollbackRetaining;
end;

procedure TFvisualizatabela.FrScriptSql1BtExecutaScriptClick(
  Sender: TObject);
begin
  FrScriptSql1.BtExecutaScriptClick(Sender);

end;

procedure TFvisualizatabela.Button1Click(Sender: TObject);
var
ptemp:string;
cont,cont2:integer;
begin
     Inputquery('Localiza','Digite o nome do campo',ptemp);

     FmostraStringList.Memo.lines.clear;
     
Try
     Self.DBGridTabela.DataSource:=nil;

     
     for cont:=0 to Combotabela.Items.count-1 do
     Begin
        Try
           Ibtabela.Close;
           Ibtabela.TableName:=Combotabela.Items[cont];
           Ibtabela.open;

           for cont2:=0 to Ibtabela.Fields.count-1 do
           Begin
             Try
                  if (uppercase(Ibtabela.Fields[cont2].FieldName)=uppercase(ptemp))
                  Then FmostraStringList.Memo.Lines.add(Combotabela.Items[cont]);

             Except
             End;
           End;

           
        Except
                     on e:exception do
                     Begin
                          mensagemerro(E.message);
                          exit;
                     End;
        End;


     End;
Finally
     Self.DBGridTabela.DataSource:=DsTabela;
     FmostraStringList.showmodal;
End;

     
end;

end.

