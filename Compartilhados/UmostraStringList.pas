unit UmostraStringList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Menus;

type
  TFmostraStringList = class(TForm)
    Memo: TMemo;
    SaveDialog: TSaveDialog;
    Panel1: TPanel;
    btimprimir: TBitBtn;
    btsalvar: TBitBtn;
    Btok: TBitBtn;
    btcancelar: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BtokClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btimprimirClick(Sender: TObject);
    procedure btsalvarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure MemoClick(Sender: TObject);
  private
    procedure selecionarLinha;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmostraStringList: TFmostraStringList;

implementation

uses UReltxtRDPRINT, Uprincipal, UpesquisaMenu;

{$R *.dfm}

procedure TFmostraStringList.FormShow(Sender: TObject);
begin
     Self.tag:=0;
end;

procedure TFmostraStringList.BtokClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;
end;

procedure TFmostraStringList.btcancelarClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TFmostraStringList.btimprimirClick(Sender: TObject);
var
cont,linha:integer;

begin
     With FreltxtRDPRINT do
     Begin
          ConfiguraImpressao;
          RDprint.Abrir;
          if (RDprint.Setup=False)
          Then Begin
                    RDprint.fechar;
                    exit;
          End;
          linha:=3;
          for cont:=0 to Memo.lines.count-1 do
          Begin
               VerificaLinha(RDprint,linha);
               RDprint.Imp(linha,1,Memo.lines[cont]);
               inc(linha,1);
          End;
          rdprint.fechar;
     End;
end;

procedure TFmostraStringList.btsalvarClick(Sender: TObject);
begin
     if (SaveDialog.Execute=False)
     Then exit;

     Try
        Memo.Lines.SaveToFile(SaveDialog.FileName+'.txt');
     Except
           on E:Exception do
           begin
                Messagedlg('Erro na tentativa de Salvar. Erro:'+#13+E.message,mterror,[mbok],0);
                exit;
           End;
     End;
     
end;

procedure TFmostraStringList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
      //Habilitar F1
    if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(self);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;
end;

procedure TFmostraStringList.FormCreate(Sender: TObject);
begin
  self.Memo.Clear;
end;

procedure TFmostraStringList.MemoClick(Sender: TObject);
begin
  selecionarLinha();
end;

procedure TFmostraStringList.selecionarLinha();
var Linha: integer;
begin

   with Memo do
   begin
     Linha := Perform(EM_LINEFROMCHAR, SelStart, 0);
     SelStart := Perform(EM_LINEINDEX, Linha, 0);
     SelLength := Length(Lines[Linha]);
   end;

end;

end.
