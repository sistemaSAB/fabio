unit UarquivoIni;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjarquivoini;

type
  TFarquivoIni = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    memoarquivo: TMemo;
    Panel2: TPanel;
    lbarquivo: TLabel;
    EdtNome: TEdit;
    LbNome: TLabel;
    EdtCODIGO: TEdit;
    LbCODIGO: TLabel;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure memoarquivoKeyPress(Sender: TObject; var Key: Char);
  private
         Objarquivoini:TObjarquivoini;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FarquivoIni: TFarquivoIni;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFarquivoIni.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.Objarquivoini do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_nome(edtnome.text);
        Submit_arquivo(memoarquivo.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFarquivoIni.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.Objarquivoini do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        Edtnome.text:=Get_nome;
        memoarquivo.text:=Get_arquivo;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFarquivoIni.TabelaParaControles: Boolean;
begin
     If (Self.Objarquivoini.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFarquivoIni.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.Objarquivoini:=TObjarquivoini.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFarquivoIni.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.Objarquivoini=Nil)
     Then exit;

If (Self.Objarquivoini.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.Objarquivoini.free;
end;

procedure TFarquivoIni.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFarquivoIni.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.Objarquivoini.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.Objarquivoini.status:=dsInsert;
     Guia.pageindex:=0;
     edtnome.setfocus;

end;


procedure TFarquivoIni.btalterarClick(Sender: TObject);
begin
    If (Self.Objarquivoini.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.Objarquivoini.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtnome.setfocus;
                
          End;


end;

procedure TFarquivoIni.btgravarClick(Sender: TObject);
begin

     If Self.Objarquivoini.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.Objarquivoini.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.Objarquivoini.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFarquivoIni.btexcluirClick(Sender: TObject);
begin
     If (Self.Objarquivoini.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.Objarquivoini.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.Objarquivoini.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFarquivoIni.btcancelarClick(Sender: TObject);
begin
     Self.Objarquivoini.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFarquivoIni.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFarquivoIni.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.Objarquivoini.Get_pesquisa,Self.Objarquivoini.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.Objarquivoini.status<>dsinactive
                                  then exit;

                                  If (Self.Objarquivoini.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.Objarquivoini.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFarquivoIni.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFarquivoIni.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFarquivoIni.memoarquivoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Memoarquivo.SetFocus;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.Objarquivoini.OBJETO.Get_Pesquisa,Self.Objarquivoini.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Objarquivoini.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.Objarquivoini.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Objarquivoini.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
