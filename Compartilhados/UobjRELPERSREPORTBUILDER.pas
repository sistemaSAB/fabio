unit UobjRELPERSREPORTBUILDER;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal, UEssencialLocal,IBStoredProc, DBTables, sysutils,
     dialogs,QRCTRLS,graphics,quickrpt,printers, Forms,controls,Ibdatabase,inifiles;

Type
   TObjRELPERSREPORTBUILDER=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                SQLCabecalhoPreenchido:String;
                SQLRepeticaoPreenchido:String;
                SQLRepeticao_2Preenchido:String;
                pDatabaseExterna : TIBDatabase;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaNOme(Parametro:String) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function    TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Procedure Opcoes(PCodigo:String);

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Nome(parametro: String);
                Function Get_Nome: String;
                Procedure Submit_SQLCabecalho(parametro: String);
                Function Get_SQLCabecalho: String;
                Procedure Submit_SQLRepeticao(parametro: String);
                Function Get_SQLRepeticao: String;
                Procedure Submit_Arquivo(parametro: string);
                Function Get_Arquivo: string;
                PRocedure Submit_SQLRepeticao_2(parametro:String);
                Function Get_SQLRepeticao_2:String;

                procedure Configurar(PCodigo: String);

                Function ChamaRelatorio(Configurar:Boolean; Quantidade:Integer):Boolean;Overload;
                Function ChamaRelatorio(Configurar:Boolean):Boolean; Overload;
                Function ChamaRelatorio(Configurar:Boolean;pDatabase:TIBDatabase):Boolean;Overload;
                Function ChamaRelatorio(Configurar:Boolean;CodigoBarras:Boolean):Boolean; Overload;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Nome:String;
               Arquivo:string;
               SQLCabecalho:String;
               SQLRepeticao:String;
               SQLRepeticao_2:String;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,UDatamodulo,
  UrelpersonalizadoReportBuilder;


{ TTabTitulo }


Function  TObjRELPERSREPORTBUILDER.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
Var

     MemoryStream:TMemoryStream;
     TempPath:String;
begin

     TempPath:='';
     TempPath:=PegaPathSistema;

     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.SQLCabecalho:=fieldbyname('SQLCabecalho').asstring;
        Self.SQLRepeticao:=fieldbyname('SQLRepeticao').asstring;
        Self.SQLRepeticao_2:=fieldbyname('SQLRepeticao_2').AsString;
        Self.Arquivo:=fieldbyname('Arquivo').asstring;
        try
              try
                    // transferindo o arquivo para o objeto TStream

                    MemoryStream:=TMemoryStream.Create;
                    MemoryStream.LoadFromStream(Objquery.CreateBlobStream(Objquery.FieldByName('ARQUIVOBINARIO'),bmRead));
                    MemoryStream.SaveToFile(TempPath+'CONFRELS\'+Self.Arquivo);

              except
                    MensagemErro('Erro ao tentar recriar o arquivo "'+TempPath+'CONFRELS\'+Self.Arquivo+'"');
                    Result:=false;
                    exit;
              end;
        finally
              // Liberando memoria
              freeandnil(MemoryStream);
        end;

        result:=True;
     End;
end;


Procedure TObjRELPERSREPORTBUILDER.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
Var Temppath:String;
begin
  TempPath:=PegaPathSistema;

  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('SQLCabecalho').asstring:=Self.SQLCabecalho;
        ParamByName('SQLRepeticao').asstring:=Self.SQLRepeticao;
        ParamByName('SQLRepeticao_2').AsString:=Self.SQLRepeticao_2;
        ParamByName('Arquivo').asstring:=Self.Arquivo;

        try
            ParamByName('ArquivoBinario').LoadFromFile(TempPath+'CONFRELS\'+Self.Arquivo,ftBlob);
        except
            MensagemErro('Erro ao tentar Relacionar o arquivo "'+TempPath+'CONFRELS\'+Self.Arquivo+'" com o Banco de Dados');
            ParamByName('Codigo').asstring:=''; // S� pra nuam Gravar
        end;
  end;

End;

//***********************************************************************

function TObjRELPERSREPORTBUILDER.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
     Self.Objquery.ExecSQL;
 Except
       On e:exception do
       Begin
            if (Self.Status=dsInsert)
            Then MensagemErro('Erro na  tentativa de Inserir. Erro: '+e.Message)
            Else MensagemErro('Erro na  tentativa de Editar. Erro: '+e.Message);
            exit;
       End;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjRELPERSREPORTBUILDER.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Nome:='';
        SQLCabecalho:='';
        SQLRepeticao:='';
        SQLRepeticao_2:='';
        Arquivo:='';
     End;
end;

Function TObjRELPERSREPORTBUILDER.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
    result:=true;
    mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';

      if (Arquivo='')
      then Mensagem:=mensagem+'\� necess�rio escolher um arquivo'
      Else BEgin
                if (FileExists(PegaPathSistema+'CONFRELS\'+Self.Arquivo)=False)
                then Begin
                              MensagemErro('O arquivo '+PegaPathSistema+'CONFRELS\'+Self.Arquivo+' n�o existe');
                              exit;
                End;
      End;
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);

            exit;
  End;
   result:=false;
end;


function TObjRELPERSREPORTBUILDER.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjRELPERSREPORTBUILDER.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjRELPERSREPORTBUILDER.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjRELPERSREPORTBUILDER.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjRELPERSREPORTBUILDER.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabRELPERSREPORTBUILDER est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Nome,SQLCabecalho,SQLRepeticao,Arquivo,ArquivoBinario,SQLRepeticao_2');
           SQL.ADD(' from  TabRelPersReportBuilder');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjRELPERSREPORTBUILDER.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjRELPERSREPORTBUILDER.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjRELPERSREPORTBUILDER.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabRelPersReportBuilder(Codigo,Nome,SQLCabecalho');
                InsertSQL.add(' ,SQLRepeticao,Arquivo,ArquivoBinario,SQLRepeticao_2)');
                InsertSQL.add('values (:Codigo,:Nome,:SQLCabecalho,:SQLRepeticao,:Arquivo,:ArquivoBinario,:SQLRepeticao_2');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabRelPersReportBuilder set Codigo=:Codigo,Nome=:Nome');
                ModifySQL.add(',SQLCabecalho=:SQLCabecalho,SQLRepeticao=:SQLRepeticao');
                ModifySQL.add(',Arquivo=:Arquivo,ArquivoBinario=:ArquivoBinario,SQLRepeticao_2=:SQLRepeticao_2');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabRelPersReportBuilder where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjRELPERSREPORTBUILDER.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjRELPERSREPORTBUILDER.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabRELPERSREPORTBUILDER');
     Result:=Self.ParametroPesquisa;
end;

function TObjRELPERSREPORTBUILDER.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de RELPERSREPORTBUILDER ';
end;


function TObjRELPERSREPORTBUILDER.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENRELPERSREPORTBUILDER,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENRELPERSREPORTBUILDER,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjRELPERSREPORTBUILDER.Free;
begin
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    freeandnil(ParametroPesquisa);
    FreeAndNil(Self.Objquery);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjRELPERSREPORTBUILDER.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjRELPERSREPORTBUILDER.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjRelPersReportBuilder.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjRelPersReportBuilder.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjRelPersReportBuilder.Submit_Nome(parametro: String);
begin
        Self.Nome:=Parametro;
end;
function TObjRelPersReportBuilder.Get_Nome: String;
begin
        Result:=Self.Nome;
end;
procedure TObjRelPersReportBuilder.Submit_SQLCabecalho(parametro: String);
begin
        Self.SQLCabecalho:=Parametro;
end;
function TObjRelPersReportBuilder.Get_SQLCabecalho: String;
begin
        Result:=Self.SQLCabecalho;
end;
procedure TObjRelPersReportBuilder.Submit_SQLRepeticao(parametro: String);
begin
        Self.SQLRepeticao:=Parametro;
end;
function TObjRelPersReportBuilder.Get_SQLRepeticao: String;
begin
        Result:=Self.SQLRepeticao;
end;
procedure TObjRelPersReportBuilder.Submit_Arquivo(parametro: string);
begin
        Self.Arquivo:=Parametro;
end;
function TObjRelPersReportBuilder.Get_Arquivo: string;
begin
        Result:=Self.Arquivo;
end;

function TObjRELPERSREPORTBUILDER.ChamaRelatorio(Configurar:Boolean): Boolean;
Var   QueryCabecalho, QueryRepeticao, QueryRepeticao_2 :TIBQuery;
      FRelPersonalizadoReportBuilder:TFRelPersonalizadoReportBuilder;
begin
try
      try
           QueryCabecalho:=TIBQuery.Create(nil);
           QueryRepeticao:=TIBQuery.Create(nil);

           QueryCabecalho.Database:=FDataModulo.IBDatabase;
           QueryRepeticao.Database:=FDataModulo.IBDatabase;

           QueryRepeticao_2:=TIBQuery.Create(nil);
           QueryRepeticao_2.Database:=FDataModulo.IBDatabase;

           FRelPersonalizadoReportBuilder:=TFRelPersonalizadoReportBuilder.Create(nil);
      except
           MensagemErro('Erro ao tentar criar as Querys');
           exit;
      end;

      if (Self.SQLCabecalhoPreenchido <> '')
      then Begin
            QueryCabecalho.Close;
            QueryCabecalho.SQL.Clear;
            QueryCabecalho.SQL.Add(Self.SQLCabecalhoPreenchido);
            //inputbox('','',QueryCabecalho.sql.text);
            QueryCabecalho.Open;
      end;

      if (Self.SQLRepeticaoPreenchido <> '')
      then Begin
            QueryRepeticao.Close;
            QueryRepeticao.SQL.Clear;
            QueryRepeticao.SQL.Add(Self.SQLRepeticaoPreenchido);
            //inputbox('','',Self.SQLRepeticaoPreenchido);
            QueryRepeticao.Open;
      end;

      if (Self.SQLRepeticao_2Preenchido <> '')
      then Begin
            QueryRepeticao_2.Close;
            QueryRepeticao_2.SQL.Clear;
            QueryRepeticao_2.SQL.Add(Self.SQLRepeticao_2Preenchido);
            QueryRepeticao_2.Open;
      end;

      FRelPersonalizadoReportBuilder.DATASOURCECABECALHO.DataSet:=QueryCabecalho;
      FRelPersonalizadoReportBuilder.DATASOURCEREPETICAO.DataSet:=QueryRepeticao;
      FRelPersonalizadoReportBuilder.DataSourceRepeticao_2.DataSet:=QueryRepeticao_2;

      FRelPersonalizadoReportBuilder.PassaObjeto(Self);

      if (Configurar = true)
      then FRelPersonalizadoReportBuilder.ShowModal
      else FRelPersonalizadoReportBuilder.BtImprimir.Click;

finally
     FreeAndNil(QueryCabecalho);
     FreeAndNil(QueryRepeticao);
     FreeAndNil(QueryRepeticao_2);
     FreeAndNil(FRelPersonalizadoReportBuilder);
end;

 // PReencer as Query e dar open, passar i o pbjeto pra o Furmulario
 // e ShowmModal no form
end;

function TObjRELPERSREPORTBUILDER.ChamaRelatorio(Configurar:Boolean;CodigoBarras:Boolean): Boolean;
Var   QueryCabecalho, QueryRepeticao, QueryRepeticao_2 :TIBQuery;
      FRelPersonalizadoReportBuilder:TFRelPersonalizadoReportBuilder;
      NOMECAMPO,Temp:string;
      arquivo_ini:Tinifile;
      cont:integer;
      tmpnegrito,tmpsublinhado,tmpitalico:Boolean;
begin
  try
      try
           QueryCabecalho:=TIBQuery.Create(nil);
           QueryRepeticao:=TIBQuery.Create(nil);

           QueryCabecalho.Database:=FDataModulo.IBDatabase;
           QueryRepeticao.Database:=FDataModulo.IBDatabase;

           QueryRepeticao_2:=TIBQuery.Create(nil);
           QueryRepeticao_2.Database:=FDataModulo.IBDatabase;

           FRelPersonalizadoReportBuilder:=TFRelPersonalizadoReportBuilder.Create(nil);
      except
           MensagemErro('Erro ao tentar criar as Querys');
           exit;
      end;

      if (Self.SQLCabecalhoPreenchido <> '')
      then Begin
            QueryCabecalho.Close;
            QueryCabecalho.SQL.Clear;
            QueryCabecalho.SQL.Add(Self.SQLCabecalhoPreenchido);
            QueryCabecalho.Open;
      end;

      if (Self.SQLRepeticaoPreenchido <> '')
      then Begin
            QueryRepeticao.Close;
            QueryRepeticao.SQL.Clear;
            QueryRepeticao.SQL.Add(Self.SQLRepeticaoPreenchido);
            QueryRepeticao.Open;
      end;

      if (Self.SQLRepeticao_2Preenchido <> '')
      then Begin
            QueryRepeticao_2.Close;
            QueryRepeticao_2.SQL.Clear;
            QueryRepeticao_2.SQL.Add(Self.SQLRepeticao_2Preenchido);
            QueryRepeticao_2.Open;
      end;

      FRelPersonalizadoReportBuilder.DATASOURCECABECALHO.DataSet:=QueryCabecalho;
      FRelPersonalizadoReportBuilder.DATASOURCEREPETICAO.DataSet:=QueryRepeticao;
      FRelPersonalizadoReportBuilder.DataSourceRepeticao_2.DataSet:=QueryRepeticao_2;
      FRelPersonalizadoReportBuilder.PassaObjeto(Self);

      if (Configurar = true)
      then FRelPersonalizadoReportBuilder.ShowModal
      else FRelPersonalizadoReportBuilder.BtImprimir.Click;

  finally
     FreeAndNil(QueryCabecalho);
     FreeAndNil(QueryRepeticao);
     FreeAndNil(QueryRepeticao_2);
     FreeAndNil(FRelPersonalizadoReportBuilder);
  end;

 // PReencer as Query e dar open, passar i o pbjeto pra o Furmulario
 // e ShowmModal no form
end;

function TObjRELPERSREPORTBUILDER.LocalizaNOme(Parametro: String): boolean;
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabRELPERSREPORTBUILDER est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Nome,SQLCabecalho,SQLRepeticao,Arquivo,ArquivoBinario,SQLRepeticao_2');
           SQL.ADD(' from  TabRelPersReportBuilder');
           SQL.ADD(' WHERE NOME='+#39+parametro+#39);
           Open;

           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

procedure TObjRELPERSREPORTBUILDER.Opcoes(PCodigo: String);
begin
      With FOpcaorel do
      Begin
           RgOpcoes.Items.Clear;
           RgOpcoes.Items.Add('Configurar');

           ShowModal;
           if (Tag=0)
           then exit;

           Case RgOpcoes.ItemIndex of
                   0: Self.Configurar(PCodigo);
           end;
      end;
end;

procedure TObjRELPERSREPORTBUILDER.Configurar(PCodigo:String);
Begin
     if (Self.LocalizaCodigo(PCodigo)=false)
     then exit;

     Self.TabelaparaObjeto;
     Self.SQLCabecalhoPreenchido:=StrReplace(Self.Get_SQLCabecalho,':PCODIGO','-1');
     Self.SQLRepeticaoPreenchido:=StrReplace(Self.get_SQLRepeticao,':PCODIGO','-1');
     Self.SQLRepeticao_2Preenchido:=StrReplace(Self.Get_SQLRepeticao_2,':PCODIGO','-1');

     If(pDatabaseExterna <> nil)
     Then Self.ChamaRelatorio(true,pDatabaseExterna)
     Else Self.ChamaRelatorio(true);
end;

function TObjRELPERSREPORTBUILDER.ChamaRelatorio(Configurar: Boolean;pDatabase: TIBDatabase): Boolean;
Var  QueryCabecalho, QueryRepeticao:TIBQuery;
    FRelPersonalizadoReportBuilder:TFRelPersonalizadoReportBuilder;
begin
try
      // Esse form FRelPersonalizadoReportBuilder precisa ser criado na inicializacao do sistema
      try
           QueryCabecalho:=TIBQuery.Create(nil);
           QueryRepeticao:=TIBQuery.Create(nil);
           QueryCabecalho.Database:=pDatabase;
           QueryRepeticao.Database:=pDatabase;
           FRelPersonalizadoReportBuilder:=TFRelPersonalizadoReportBuilder.Create(nil);
      except
           MensagemErro('Erro ao tentar criar as Querys');
           exit;
      end;

      QueryCabecalho.Close;
      QueryCabecalho.SQL.Clear;
      QueryCabecalho.SQL.Add(Self.SQLCabecalhoPreenchido);
      QueryCabecalho.Open;

      QueryRepeticao.Close;
      QueryRepeticao.SQL.Clear;
      QueryRepeticao.SQL.Add(Self.SQLRepeticaoPreenchido);
      QueryRepeticao.Open;

      FRelPersonalizadoReportBuilder.DATASOURCECABECALHO.DataSet:=QueryCabecalho;
      FRelPersonalizadoReportBuilder.DATASOURCEREPETICAO.DataSet:=QueryRepeticao;

      FRelPersonalizadoReportBuilder.PassaObjeto(Self);

      if (Configurar = true)
      then FRelPersonalizadoReportBuilder.ShowModal
      else FRelPersonalizadoReportBuilder.BtImprimir.Click;

finally
     FreeAndNil(QueryCabecalho);
     FreeAndNil(QueryRepeticao);
     FreeAndNIl(FRelPersonalizadoReportBuilder);
end;

end;

function TObjRELPERSREPORTBUILDER.Get_SQLRepeticao_2: String;
begin
    Result:=Self.SQLRepeticao_2;
end;

procedure TObjRELPERSREPORTBUILDER.Submit_SQLRepeticao_2(parametro: String);
begin
    Self.SQLRepeticao_2:=parametro;
end;

function TObjRELPERSREPORTBUILDER.ChamaRelatorio(Configurar:Boolean; Quantidade:Integer): Boolean;
Var   QueryCabecalho, QueryRepeticao, QueryRepeticao_2 :TIBQuery;
      FRelPersonalizadoReportBuilder:TFRelPersonalizadoReportBuilder;
begin
try
      try
           QueryCabecalho:=TIBQuery.Create(nil);
           QueryRepeticao:=TIBQuery.Create(nil);

           QueryCabecalho.Database:=FDataModulo.IBDatabase;
           QueryRepeticao.Database:=FDataModulo.IBDatabase;

           QueryRepeticao_2:=TIBQuery.Create(nil);
           QueryRepeticao_2.Database:=FDataModulo.IBDatabase;

           FRelPersonalizadoReportBuilder:=TFRelPersonalizadoReportBuilder.Create(nil);
      except
           MensagemErro('Erro ao tentar criar as Querys');
           exit;
      end;

      if (Self.SQLCabecalhoPreenchido <> '')
      then Begin
            QueryCabecalho.Close;
            QueryCabecalho.SQL.Clear;
            QueryCabecalho.SQL.Add(Self.SQLCabecalhoPreenchido);
            //inputbox('','',QueryCabecalho.sql.text);
            QueryCabecalho.Open;
      end;

      if (Self.SQLRepeticaoPreenchido <> '')
      then Begin
            QueryRepeticao.Close;
            QueryRepeticao.SQL.Clear;
            QueryRepeticao.SQL.Add(Self.SQLRepeticaoPreenchido);
            //inputbox('','',QueryRepeticao.sql.text);
            QueryRepeticao.Open;
      end;

      if (Self.SQLRepeticao_2Preenchido <> '')
      then Begin
            QueryRepeticao_2.Close;
            QueryRepeticao_2.SQL.Clear;
            QueryRepeticao_2.SQL.Add(Self.SQLRepeticao_2Preenchido);
            //inputbox('','',QueryRepeticao_2.sql.text);
            QueryRepeticao_2.Open;
      end;

      FRelPersonalizadoReportBuilder.DATASOURCECABECALHO.DataSet:=QueryCabecalho;
      FRelPersonalizadoReportBuilder.DATASOURCEREPETICAO.DataSet:=QueryRepeticao;
      FRelPersonalizadoReportBuilder.DataSourceRepeticao_2.DataSet:=QueryRepeticao_2;


      FRelPersonalizadoReportBuilder.PassaObjeto(Self);

      if (Configurar = true)
      then FRelPersonalizadoReportBuilder.ShowModal
      else FRelPersonalizadoReportBuilder.ImprimeVariasCopias(Quantidade);

finally
     FreeAndNil(QueryCabecalho);
     FreeAndNil(QueryRepeticao);
     FreeAndNil(QueryRepeticao_2);
     FreeAndNil(FRelPersonalizadoReportBuilder);
end;

 // PReencer as Query e dar open, passar i o pbjeto pra o Furmulario
 // e ShowmModal no form
end;

end.


RG_IE=NULL;
PORTADOR=NULL;
REPRESENTANTE=NULL;
RAMOATIVIDADE=NULL;
CONTATO=NULL;
EMAIL=NULL; FONE=NULL;
CELULAR=NULL;
ENDERECOFATURA=NULL;
BAIRROFATURA=NULL;
CIDADEFATURA=NULL;
ENDERECOCOBRANCA=NULL;
BAIRROCOBRANCA=NULL;
CIDADECOBRANCA=NULL;
ESTADOCOBRANCA=NULL;
ENDERECOENTREGA=NULL;
BAIRROENTREGA=NULL;
CIDADEENTREGA=NULL;
ESTADOENTREGA=NULL;
SITUACAOSERASA=NULL;
DATASERASA=NULL;
ATRASOMAXIMO=NULL;
PRAZOMAXIMO=NULL;
LIMITECREDITO=NULL;
DATAC=NULL;
USERC=NULL;
DATAM=NULL;
USERM=NULL;
CEPFATURA=NULL;
CEPENTREGA=NULL;
CEPCOBRANCA=NULL;
CODIGOPLANODECONTAS=NULL;
BANCO=NULL;
AGENCIA=NULL;
CONTACORRENTE=NULL;
ESTADOFATURA=NULL;
DATANASC=NULL;
REFERENCIAS=NULL;
SUBREGIAO=NULL;
FAX=NULL;
PERMITEVENDAAPRAZO=NULL;
PERMITEVENDAEMCARTEIRA=NULL;
ULTIMAATUALIZACAO=NULL;
ATIVO=NULL; REVENDA=NULL;
VENDEDOR=NULL;
ESTADOCIVIL=NULL;
CONJUGE=NULL;
TELEFONECONJUGE=NULL;
NOMEREFERENCIAPESSOAL=NULL;
ENDERECOREFERENCIAPESSOAL=NULL;
TELEFONEREFERENCIAPESSOAL=NULL;
NOMEREFERENCIAPROFISSIONAL=NULL;
PROFISSAOREFERENCIAPROFISSIONAL=NULL;
EMPRESAREFERENCIAPROFISSIONAL=NULL;
ENDERECOREFERENCIAPROFISSIONAL=NULL;
TELEFONEREFERENCIAPROFISSIONAL=NULL;
GRUPO=NULL; CLIENTECOMCADASTRO=NULL;
NOMEPAI=NULL;
NOMEMAE=NULL;
CLIENTEBENEFICIADO=NULL;
BONIFICACAO=NULL;
CODIGOALTERNATIVO=NULL;
SUBSTITUTOTRIBUTARIOISS=NULL;
INSCRICAOMUNICIPAL=NULL;
CNAI=NULL; NIRE=NULL;
OBSERVACAO=NULL;
CODIGOCIDADEFATURA=NULL;
NUMERO=NULL;
IEPRODUTORRURAL=NULL;
CODIGOPAIS=NULL;
VERSAO=NULL;
