unit UconfiguracoesVisuaisFormulario;

interface

uses
  db,mask,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus,UessencialGlobal,extctrls,comctrls;

const PquantidadeComponentesVetorMemoria=5000;

type

  TFconfiguracoesVisuaisFormulario = class(TForm)
    BtGravaConfiguracoes: TButton;
    Label1: TLabel;
    LbnomeComponente: TLabel;
    BtOcultaCampo: TButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtOcultaCampoClick(Sender: TObject);
    procedure BtGravaConfiguracoesClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
  private
    PpermiteAlterar:Boolean;
    PCorInvisivel:TColor;
    FormChamou:Tform;
    ClasseComponente:String;
    FormOnKeyDown_Backup:TKeyEvent;
    FormClose_Backup:TCloseEvent;
    FormDragOver_backup:TDragOverEvent;

    VetorOnMouseDown:array[0..PquantidadeComponentesVetorMemoria] of TMouseEvent;//onmousedown
    VetorOnClick:array[0..PquantidadeComponentesVetorMemoria] of TNotifyEvent;//onclick

    VetorOnEnter:array[0..PquantidadeComponentesVetorMemoria] of TNotifyEvent;//onenter
    VetorOnExit:array[0..PquantidadeComponentesVetorMemoria] of TNotifyEvent;//onexit

    VetorOnKeyDown:array[0..PquantidadeComponentesVetorMemoria] of TKeyEvent;//keydown
    VetorOnKeyUP:array[0..PquantidadeComponentesVetorMemoria] of TKeyEvent;//keydown
    VetorOnKeyPress:array[0..PquantidadeComponentesVetorMemoria] of TKeyPressEvent;//keypress

    Procedure FormCloseChamou(Sender:TObject;var Action:TCloseAction);
    procedure FormDragOverChamou(Sender, Source: TObject;X, Y: Integer; State: TDragState; var Accept: Boolean);

    procedure Relacionamento(HabilitaDesabilita:string);
    Procedure HabilitaModoConfiguracao;

    //******CLIQUES EM COMPONENTES**********
    procedure PersonalizaMouseDown(Sender: TObject;Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    Procedure Clicou(Sender:TObject);
    procedure Oculta_Movimenta(Sender: Tobject; Key: Word; Tipo: Char);
    //**************************************

    { Private declarations }
  public
    { Public declarations }
    ConfiguracaoAtivada:Boolean;
    Procedure Inicializa(Pform:TForm);
    Procedure KeyDownConfiguracao(Sender: TObject; var Key: Word;  Shift: TShiftState);

  end;
  
type TMyControl = class(TControl);
type TMyControlClass = class of TMyControl;

var
  FconfiguracoesVisuaisFormulario: TFconfiguracoesVisuaisFormulario;

implementation

uses UDataModulo;


{$R *.dfm}

procedure TFconfiguracoesVisuaisFormulario.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
cont:integer;
begin
     if (Self.tag=0)
     Then Begin
               If (messagedlg('Tem certeza que deseja sair sem gravar as altera��es?',mtconfirmation,[mbyes,mbno],0)=mrno)
               Then Begin
                        Action:=caNone;
                        exit;
               End;
     End;

     Self.ConfiguracaoAtivada:=False;
     desabilita_campos(Self.FormChamou);
     habilita_botoes(Self.FormChamou);
     Self.Relacionamento('D');
     //Retornando o OnClose e o OnKeyDown do Form que chamou
     Self.formchamou.OnClose:=Self.FormClose_Backup;
     Self.FormChamou.OnKeyDown:=Self.FormOnKeyDown_Backup;
     Self.formchamou.OnDragOver:=Self.FormDragOver_backup;
     //******************************************************

     //zerando os vetores que cuidarao dos eventos 
     for cont:=0 to PquantidadeComponentesVetorMemoria do
     begin
          VetorOnMouseDown[cont]:=nil;
          VetorOnClick[cont]:=nil;
          VetorOnEnter[cont]:=nil;
          VetorOnExit[cont]:=nil;
          VetorOnKeyDown[cont]:=nil;
          VetorOnKeyUP[cont]:=nil;
          VetorOnKeyPress[cont]:=nil;
     End;
     //****************************************************

end;

procedure TFconfiguracoesVisuaisFormulario.BtOcultaCampoClick(
  Sender: TObject);
Begin
     Self.Oculta_Movimenta(Sender,0,'O');
End;

procedure TFconfiguracoesVisuaisFormulario.Oculta_Movimenta(Sender:Tobject;Key:Word;Tipo:Char);
var
QualquerCoisa:TMyControlClass;
PtipoComponente:Integer;
begin
     if (LbnomeComponente.Caption='')
     Then exit;

     PtipoComponente:=0;


     If (Self.ClasseComponente='TEDIT')
     Then PtipoComponente:=1
     Else
        If (Self.ClasseComponente='TLABEL')
        Then PtipoComponente:=2
        Else
           If (Self.ClasseComponente='TMASKEDIT')
           Then PtipoComponente:=3
           else
              If (Self.ClasseComponente='TCOMBOBOX')
              Then PtipoComponente:=4
              else
                 If (Self.ClasseComponente='TMEMO')
                 Then PtipoComponente:=5
                 else
                    If (Self.ClasseComponente='TRADIOBUTTON')
                    Then PtipoComponente:=6
                    else
                       If (Self.ClasseComponente='TCHECKBOX')
                       Then PtipoComponente:=7
                       else
                          If (Self.ClasseComponente='TLISTBOX')
                          Then PtipoComponente:=8
                          else
                             If (Self.ClasseComponente='TRADIOGROUP')
                             Then PtipoComponente:=9
                             else
                                If (Self.ClasseComponente='TRICHEDIT')
                                Then PtipoComponente:=10
                                else
                                   If (Self.ClasseComponente='TSHAPE')
                                   Then PtipoComponente:=11;

     case PtipoComponente of
        1:Begin
               QualquerCoisa:=TMyControlClass(TEdit);
               Case Tipo of
                 'O':Begin//ocultar
                           if (TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                           Then TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                           Else TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                           Self.ClasseComponente:='';
                           LbnomeComponente.caption:='';
                           BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin//movimentar
                           if (key=vk_up)
                           Then TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                           Else
                               if (Key=vk_down)
                               Then TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                               Else
                                   if (Key=vk_left)
                                   Then TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                   Else
                                       if (Key=vk_right)
                                       Then TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=TEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                 End;
               End;

        End;
        2:Begin
               QualquerCoisa:=TMyControlClass(TLabel);
               Case Tipo of
                 'O':Begin//ocultar
                            if (Tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                            Then Tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                            Else Tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                            Self.ClasseComponente:='';
                            LbnomeComponente.caption:='';
                            BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin//movimentar
                           if (key=vk_up)
                           Then tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                           Else
                               if (Key=vk_down)
                               Then tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                               Else
                                   if (Key=vk_left)
                                   Then tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                   Else
                                       if (Key=vk_right)
                                       Then tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tlabel(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;

                 End;
               End;
        End;

        3:Begin
               QualquerCoisa:=TMyControlClass(TMaskEdit);
               Case Tipo of
                 'O':Begin//ocultar
                            if (TmaskEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                            Then TmaskEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=TmaskEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                            Else TmaskEdit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                            Self.ClasseComponente:='';
                            LbnomeComponente.caption:='';
                            BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin
                            if (key=vk_up)
                            Then tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                            Else
                                if (Key=vk_down)
                                Then tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                                Else
                                    if (Key=vk_left)
                                    Then tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                    Else
                                        if (Key=vk_right)
                                        Then tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tmaskedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                 End;
               End;
        End;
        4:Begin
               QualquerCoisa:=TMyControlClass(TComboBox);
               Case Tipo of
                 'O':Begin//ocultar
                            if (Tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                            Then Tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                            Else Tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                            Self.ClasseComponente:='';
                            LbnomeComponente.caption:='';
                            BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin
                            if (key=vk_up)
                            Then tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                            Else
                                if (Key=vk_down)
                                Then tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                                Else
                                    if (Key=vk_left)
                                    Then tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                    Else
                                        if (Key=vk_right)
                                        Then tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tcombobox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                 End;
               End;
        End;
        5:Begin
               QualquerCoisa:=TMyControlClass(TMemo);
               Case Tipo of
                 'O':Begin//ocultar
                            if (Tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                            Then Tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                            Else Tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                            Self.ClasseComponente:='';
                            LbnomeComponente.caption:='';
                            BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin
                          if (key=vk_up)
                          Then tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                          Else
                              if (Key=vk_down)
                              Then tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                              Else
                                  if (Key=vk_left)
                                  Then tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                  Else
                                      if (Key=vk_right)
                                      Then tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tmemo(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                 End;
               End;
        End;
        6:Begin
               QualquerCoisa:=TMyControlClass(TRadioButton);
               Case Tipo of
                 'O':Begin//ocultar
                            if (Tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                            Then Tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                            Else Tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                            Self.ClasseComponente:='';
                            LbnomeComponente.caption:='';
                            BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin
                            if (key=vk_up)
                            Then tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                            Else
                                if (Key=vk_down)
                                Then tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                                Else
                                    if (Key=vk_left)
                                    Then tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                    Else
                                        if (Key=vk_right)
                                        Then tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tradiobutton(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                 End;
               End;


        End;
        7:Begin
               QualquerCoisa:=TMyControlClass(TcheckBox);
               Case Tipo of
                 'O':Begin//ocultar
                            if (Tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                            Then Tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                            Else Tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                            Self.ClasseComponente:='';
                            LbnomeComponente.caption:='';
                            BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin
                            if (key=vk_up)
                            Then tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                            Else
                                if (Key=vk_down)
                                Then tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                                Else
                                    if (Key=vk_left)
                                    Then tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                    Else
                                        if (Key=vk_right)
                                        Then tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tcheckbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                 End;
               End;
        End;
        8:Begin
               QualquerCoisa:=TMyControlClass(TLISTBOX);
               Case Tipo of
                 'O':Begin//ocultar
                            if (tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                            Then tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                            Else tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                            Self.ClasseComponente:='';
                            LbnomeComponente.caption:='';
                            BtOcultaCampo.enabled:=False;
                 End;
                 'M':Begin
                            if (key=vk_up)
                            Then tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                            Else
                                if (Key=vk_down)
                                Then tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                                Else
                                    if (Key=vk_left)
                                    Then tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                    Else
                                        if (Key=vk_right)
                                        Then tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tlistbox(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                 End;
               End;
        End;
        9:Begin
              QualquerCoisa:=TMyControlClass(TRadioGroup);
              Case Tipo of
                'O':Begin//ocultar
                           if (TRadioGroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                           Then TRadioGroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=TRadioGroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                           Else TRadioGroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                           Self.ClasseComponente:='';
                           LbnomeComponente.caption:='';
                           BtOcultaCampo.enabled:=False;
                End;
                'M':Begin
                           if (key=vk_up)
                           Then tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                           Else
                               if (Key=vk_down)
                               Then tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                               Else
                                   if (Key=vk_left)
                                   Then tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                   Else
                                       if (Key=vk_right)
                                       Then tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tradiogroup(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                End;
              End;
        End;
        10:Begin
              QualquerCoisa:=TMyControlClass(TRICHEDIT);
              Case Tipo of
                'O':Begin//ocultar
                           if (TRICHEDIT(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Color=Self.PCorInvisivel)
                           Then TRICHEDIT(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=TRICHEDIT(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                           Else TRICHEDIT(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).color:=Self.PCorInvisivel;
                           Self.ClasseComponente:='';
                           LbnomeComponente.caption:='';
                           BtOcultaCampo.enabled:=False;
                End;
                'M':Begin
                           if (key=vk_up)
                           Then trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                           Else
                               if (Key=vk_down)
                               Then trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                               Else
                                   if (Key=vk_left)
                                   Then trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                   Else
                                       if (Key=vk_right)
                                       Then trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=trichedit(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                End;
              End;
        End;
        11:Begin
              QualquerCoisa:=TMyControlClass(TSHAPE);
              Case Tipo of
                'O':Begin//ocultar
                           if (TSHAPE(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Brush.Color=Self.PCorInvisivel)
                           Then TSHAPE(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Brush.Color:=TSHAPE(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).tag
                           Else TSHAPE(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).Brush.Color:=Self.PCorInvisivel;
                           Self.ClasseComponente:='';
                           LbnomeComponente.caption:='';
                           BtOcultaCampo.enabled:=False;
                End;
                'M':Begin
                           if (key=vk_up)
                           Then tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top-1
                           Else
                               if (Key=vk_down)
                               Then tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top:=tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).top+1
                               Else
                                   if (Key=vk_left)
                                   Then tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left-1
                                   Else
                                       if (Key=vk_right)
                                       Then tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left:=tshape(Self.FormChamou.FindComponent(LbnomeComponente.caption) as qualquercoisa).left+1;
                End;
              End;
        End;

     End;//case
end;

procedure TFconfiguracoesVisuaisFormulario.BtGravaConfiguracoesClick(
  Sender: TObject);
begin
     Self.Relacionamento('D');
     ObjConfiguracaoTelaGlobal.zerartabela;
     if (ObjConfiguracaoTelaGlobal.LocalizaNomeFormulario(uppercase(Self.FormChamou.ClassName))=True)
     Then Begin
               //Se nao existir uma configuracao de tela, tenho que gravar uma
               //pois precisarei alterar as configuracoes no memo
               ObjConfiguracaoTelaGlobal.TabelaparaObjeto;
               ObjConfiguracaoTelaGlobal.status:=dsedit;
     End
     Else Begin
               ObjConfiguracaoTelaGlobal.submit_Codigo('0');
               ObjConfiguracaoTelaGlobal.status:=dsinsert;
     End;

     ObjConfiguracaoTelaGlobal.Submit_NomeFormulario(uppercase(Self.FormChamou.ClassName));
     if (ObjConfiguracaoTelaGlobal.GravaConfiguracoes(Self.FormChamou)=False)
     Then Begin
               MensagemErro('Erro na tentativa de gravar as configura��es dos componentes visuais');
               exit;
     End;
     if (ObjConfiguracaoTelaGlobal.Salvar(True)=False)
     Then Begin
               MensagemErro('Erro na tentativa de Salvar a configura��o de tela para este formul�rio');
               exit;
     End;
     Self.Tag:=1;
     Self.close;

end;



procedure TFconfiguracoesVisuaisFormulario.Clicou(Sender: TObject);
begin
     if (Self.ConfiguracaoAtivada=True)
     Then Begin
               if (uppercase(Sender.ClassName)='TEDIT')
               Then Begin
                        Self.LbnomeComponente.caption:=uppercase(TEdit(Sender).name);
                        Self.ClasseComponente:=uppercase(sender.classname);
                        BtOcultaCampo.Enabled:=True;
               End
               Else
                   if (uppercase(Sender.ClassName)='TLABEL')
                   Then Begin
                            Self.LbnomeComponente.caption:=TLabel(Sender).name;
                            Self.ClasseComponente:=uppercase(sender.classname);
                            BtOcultaCampo.Enabled:=True;
                   End
                   Else
                       if (uppercase(Sender.ClassName)='TMASKEDIT')
                       Then Begin
                                Self.LbnomeComponente.caption:=TMaskEdit(Sender).name;
                                Self.ClasseComponente:=uppercase(sender.classname);
                                BtOcultaCampo.Enabled:=True;
                       End
                       Else Begin
                                 if (uppercase(Sender.ClassName)='TCOMBOBOX')
                                 Then Begin
                                          Self.LbnomeComponente.caption:=TComboBox(Sender).name;
                                          Self.ClasseComponente:=uppercase(sender.classname);
                                          BtOcultaCampo.Enabled:=True;
                                 End
                                 Else Begin
                                           if (uppercase(Sender.ClassName)='TMEMO')
                                           Then Begin
                                                    Self.LbnomeComponente.caption:=TMEMO(Sender).name;
                                                    Self.ClasseComponente:=uppercase(sender.classname);
                                                    BtOcultaCampo.Enabled:=True;
                                           End
                                           Else Begin
                                                     if (uppercase(Sender.ClassName)='TRADIOBUTTON')
                                                     Then Begin
                                                              Self.LbnomeComponente.caption:=TRadioButton(Sender).name;
                                                              Self.ClasseComponente:=uppercase(sender.classname);
                                                              BtOcultaCampo.Enabled:=True;
                                                     End
                                                     Else Begin
                                                               if (uppercase(Sender.ClassName)='TCHECKBOX')
                                                               Then Begin
                                                                        Self.LbnomeComponente.caption:=TCHECKBOX(Sender).name;
                                                                        Self.ClasseComponente:=uppercase(sender.classname);
                                                                        BtOcultaCampo.Enabled:=True;
                                                               End
                                                               Else Begin
                                                                         if (uppercase(Sender.ClassName)='TLISTBOX')
                                                                         Then Begin
                                                                                  Self.LbnomeComponente.caption:=TLISTBOX(Sender).name;
                                                                                  Self.ClasseComponente:=uppercase(sender.classname);
                                                                                  BtOcultaCampo.Enabled:=True;
                                                                         End
                                                                         Else Begin
                                                                                   if (uppercase(Sender.ClassName)='TRADIOGROUP')
                                                                                   Then Begin
                                                                                            Self.LbnomeComponente.caption:=TRadioGroup(Sender).name;
                                                                                            Self.ClasseComponente:=uppercase(sender.classname);
                                                                                            BtOcultaCampo.Enabled:=True;
                                                                                   End
                                                                                   Else Begin
                                                                                             if (uppercase(Sender.ClassName)='TRICHEDIT')
                                                                                             Then Begin
                                                                                                      Self.LbnomeComponente.caption:=TRICHEDIT(Sender).name;
                                                                                                      Self.ClasseComponente:=uppercase(sender.classname);
                                                                                                      BtOcultaCampo.Enabled:=True;
                                                                                             End
                                                                                             Else Begin
                                                                                                       if (uppercase(Sender.ClassName)='TSHAPE')
                                                                                                       Then Begin
                                                                                                                Self.LbnomeComponente.caption:=TSHAPE(Sender).name;
                                                                                                                Self.ClasseComponente:=uppercase(sender.classname);
                                                                                                                BtOcultaCampo.Enabled:=True;
                                                                                                       End
                                                                                             End;
                                                                                   End;
                                                                         End;
                                                               End;
                                                     End;
                                           End;
                                 End;
                       End;
     End;

end;


procedure TFconfiguracoesVisuaisFormulario.Relacionamento(HabilitaDesabilita:string);
var
  ptipocomponente,cont:integer;
begin
     {Esse procedimento � usado tanto no momento de entrar em modo de configuracao
     como no momento de sair
     Na Entrada (habilita)
          Guardo a Cor atual
          Guardo a ref. dos onclick e mousedown para resgatar posteriormente
          se tiver invisivel coloco para visivel e pinto o fundo com uma cor Diferente
     Na saida (desabilita)
          Se tiver com a cor diferente jogo para invisivel
          Retorno a Cor original
          Retorno a Referencia dos onclick e mousedown}
          
   for cont:=0 to Self.FormChamou.ComponentCount -1
   do begin
           PtipoComponente:=0;

           If (uppercase(Self.FormChamou.Components [cont].ClassName)='TEDIT')
           Then PtipoComponente:=1
           Else
              If (uppercase(Self.FormChamou.Components [cont].ClassName)='TLABEL')
              Then PtipoComponente:=2
              Else
                 If (uppercase(Self.FormChamou.Components [cont].ClassName)='TMASKEDIT')
                 Then PtipoComponente:=3
                 else
                    If (uppercase(Self.FormChamou.Components [cont].ClassName)='TCOMBOBOX')
                    Then PtipoComponente:=4
                    else
                       If (uppercase(Self.FormChamou.Components [cont].ClassName)='TMEMO')
                       Then PtipoComponente:=5
                       else
                          If (uppercase(Self.FormChamou.Components [cont].ClassName)='TRADIOBUTTON')
                          Then PtipoComponente:=6
                          else
                             If (uppercase(Self.FormChamou.Components [cont].ClassName)='TCHECKBOX')
                             Then PtipoComponente:=7
                             else
                                If (uppercase(Self.FormChamou.Components [cont].ClassName)='TLISTBOX')
                                Then PtipoComponente:=8
                                else
                                   If (uppercase(Self.FormChamou.Components [cont].ClassName)='TRADIOGROUP')
                                   Then PtipoComponente:=9
                                   else
                                      If (uppercase(Self.FormChamou.Components [cont].ClassName)='TRICHEDIT')
                                      Then PtipoComponente:=10
                                      else
                                         If (uppercase(Self.FormChamou.Components [cont].ClassName)='TSHAPE')
                                         Then PtipoComponente:=11;

           Case PtipoComponente of 

                1:begin
                      if (HabilitaDesabilita='H')//habilitando no inicio
                      Then Begin
                               //guardando os eventos de click
                               VetorOnMouseDown[cont]:=Tedit(Self.FormChamou.Components [cont]).OnMouseDown;
                               VetorOnClick[cont]:=Tedit(Self.FormChamou.Components [cont]).OnClick;
                               VetorOnEnter[cont]:=Tedit(Self.FormChamou.Components [cont]).OnEnter;
                               VetorOnExit[cont]:=Tedit(Self.FormChamou.Components [cont]).OnExit;
                               VetorOnKeyDown[cont]:=Tedit(Self.FormChamou.Components [cont]).OnKeyDown;
                               VetorOnKeyUP[cont]:=Tedit(Self.FormChamou.Components [cont]).OnKeyUp;
                               VetorOnKeyPress[cont]:=Tedit(Self.FormChamou.Components [cont]).OnKeyPress;
                               //zerando os eventos
                               Tedit(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                               Tedit(Self.FormChamou.Components [cont]).OnClick:=nil;
                               Tedit(Self.FormChamou.Components [cont]).OnEnter:=nil;
                               Tedit(Self.FormChamou.Components [cont]).OnExit:=nil;
                               Tedit(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                               Tedit(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                               Tedit(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                               //*************************************************
                      
                               //passando o click
                               Tedit(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                               //passando para poder arrastar
                               Tedit(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                               //guardando a cor original
                               Tedit(Self.FormChamou.Components [cont]).tag:=Tedit(Self.FormChamou.Components [cont]).color;
                               //colocando o visible para true e deixando com a cor de fundo
                               if(Tedit(Self.FormChamou.Components [cont]).Visible=False)
                               Then Begin
                                         Tedit(Self.FormChamou.Components [cont]).Visible:=True;
                                         Tedit(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                               End;
                      End
                      Else Begin//no momento de desabilitar
                               //Retorno o evento de click
                               Tedit(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                               Tedit(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                               Tedit(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                               Tedit(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                               Tedit(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                               Tedit(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                               Tedit(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                               //****************************************************
                               //para n�o poder arrastar
                               Tedit(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                      
                               //se tiver com o fundo colorido
                               if(Tedit(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                               Then Tedit(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                               //retornando a cor original
                               Tedit(Self.FormChamou.Components [cont]).color:=Tedit(Self.FormChamou.Components [cont]).tag;
                      End;
                End;
                2:Begin//label
                      if (HabilitaDesabilita='H')//habilitando no inicio
                      Then Begin
                                //guardando os eventos de click
                                VetorOnMouseDown[cont]:=tlabel(Self.FormChamou.Components [cont]).OnMouseDown;
                                VetorOnClick[cont]:=tlabel(Self.FormChamou.Components [cont]).OnClick;
                                //VetorOnEnter[cont]:=tlabel(Self.FormChamou.Components [cont]).OnEnter;
                                //VetorOnExit[cont]:=tlabel(Self.FormChamou.Components [cont]).OnExit;
                                //VetorOnKeyDown[cont]:=tlabel(Self.FormChamou.Components [cont]).OnKeyDown;
                                //VetorOnKeyUP[cont]:=tlabel(Self.FormChamou.Components [cont]).OnKeyUp;
                                //VetorOnKeyPress[cont]:=tlabel(Self.FormChamou.Components [cont]).OnKeyPress;
                                //zerando os eventos
                                tlabel(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                tlabel(Self.FormChamou.Components [cont]).OnClick:=nil;
                                //tlabel(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                //tlabel(Self.FormChamou.Components [cont]).OnExit:=nil;
                                //tlabel(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                //tlabel(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                //tlabel(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                //*************************************************
                               //para poder arrastar
                               TLabel(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                               //passando o click
                               TLabel(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                               //guardando a cor original
                               Tlabel(Self.FormChamou.Components [cont]).tag:=Tlabel(Self.FormChamou.Components [cont]).color;
                               //colocando o visible para true e deixando com a cor de fundo
                               if(Tlabel(Self.FormChamou.Components [cont]).Visible=False)
                               Then Begin
                                         Tlabel(Self.FormChamou.Components [cont]).Visible:=True;
                                         Tlabel(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                               End;
                      End
                      Else Begin//no momento de desabilitar
                                //Retorno o evento de click
                                tlabel(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                tlabel(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                {tlabel(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                tlabel(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                tlabel(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                tlabel(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                tlabel(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];}
                                //****************************************************
                                //para n�o poder arrastar
                                TLabel(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                //se tiver com o fundo colorido
                                if(Tlabel(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                Then Tlabel(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                //retornando a cor original
                                Tlabel(Self.FormChamou.Components [cont]).color:=Tlabel(Self.FormChamou.Components [cont]).tag;
                      End;
                End;
                3:Begin//mask
                      if (HabilitaDesabilita='H')//habilitando no inicio
                      Then Begin
                                //guardando os eventos de click
                                VetorOnMouseDown[cont]:=tmaskEdit(Self.FormChamou.Components [cont]).OnMouseDown;
                                VetorOnClick[cont]:=tmaskEdit(Self.FormChamou.Components [cont]).OnClick;
                                VetorOnEnter[cont]:=tmaskEdit(Self.FormChamou.Components [cont]).OnEnter;
                                VetorOnExit[cont]:=tmaskEdit(Self.FormChamou.Components [cont]).OnExit;
                                VetorOnKeyDown[cont]:=tmaskEdit(Self.FormChamou.Components [cont]).OnKeyDown;
                                VetorOnKeyUP[cont]:=tmaskEdit(Self.FormChamou.Components [cont]).OnKeyUp;
                                VetorOnKeyPress[cont]:=tmaskEdit(Self.FormChamou.Components [cont]).OnKeyPress;
                                //zerando os eventos
                                tmaskEdit(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                tmaskEdit(Self.FormChamou.Components [cont]).OnClick:=nil;
                                tmaskEdit(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                tmaskEdit(Self.FormChamou.Components [cont]).OnExit:=nil;
                                tmaskEdit(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                tmaskEdit(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                tmaskEdit(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                //*************************************************
                                //para poder arrastar
                                TmaskEdit(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                               //guardando o evento de click
                               TMaskEdit(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                               //guardando a cor original
                               TmaskEdit(Self.FormChamou.Components [cont]).tag:=TmaskEdit(Self.FormChamou.Components [cont]).color;
                               //colocando o visible para true e deixando com a cor de fundo
                               if(TmaskEdit(Self.FormChamou.Components [cont]).Visible=False)
                               Then Begin
                                         TmaskEdit(Self.FormChamou.Components [cont]).Visible:=True;
                                         TmaskEdit(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                               End;
                      End
                      Else Begin//no momento de desabilitar
                                //Retorno o evento de click
                                tmaskedit(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                tmaskedit(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                tmaskedit(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                tmaskedit(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                tmaskedit(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                tmaskedit(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                tmaskedit(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                //****************************************************
                                //para n�o poder arrastar
                                TMaskEdit(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                //se tiver com o fundo colorido
                                if(tmaskEdit(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                Then tmaskEdit(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                //retornando a cor original
                                tmaskEdit(Self.FormChamou.Components [cont]).color:=tmaskEdit(Self.FormChamou.Components [cont]).tag;
                      End;
                End;
                4:Begin//combo
                       if (HabilitaDesabilita='H')//habilitando no inicio
                       Then Begin
                                 //guardando os eventos de click
                                 //VetorOnMouseDown[cont]:=tcombobox(Self.FormChamou.Components [cont]).OnMouseDown;
                                 VetorOnClick[cont]:=tcombobox(Self.FormChamou.Components [cont]).OnClick;
                                 VetorOnEnter[cont]:=tcombobox(Self.FormChamou.Components [cont]).OnEnter;
                                 VetorOnExit[cont]:=tcombobox(Self.FormChamou.Components [cont]).OnExit;
                                 VetorOnKeyDown[cont]:=tcombobox(Self.FormChamou.Components [cont]).OnKeyDown;
                                 VetorOnKeyUP[cont]:=tcombobox(Self.FormChamou.Components [cont]).OnKeyUp;
                                 VetorOnKeyPress[cont]:=tcombobox(Self.FormChamou.Components [cont]).OnKeyPress;
                                 //zerando os eventos
                                 //tcombobox(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                 tcombobox(Self.FormChamou.Components [cont]).OnClick:=nil;
                                 tcombobox(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                 tcombobox(Self.FormChamou.Components [cont]).OnExit:=nil;
                                 tcombobox(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                 tcombobox(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                 tcombobox(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                 //*************************************************
                                 //para poder arrastar
                                 TComboBox(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                 //guardando o evento de click//Combobox nao possui MouseDown, nesse caso uso onclick
                                 TComboBox(Self.FormChamou.Components [cont]).OnClick:=Self.Clicou;
                                 //guardando a cor original
                                 Tcombobox(Self.FormChamou.Components [cont]).tag:=Tcombobox(Self.FormChamou.Components [cont]).color;
                                 //colocando o visible para true e deixando com a cor de fundo
                                 if(Tcombobox(Self.FormChamou.Components [cont]).Visible=False)
                                 Then Begin
                                          Tcombobox(Self.FormChamou.Components [cont]).Visible:=True;
                                          Tcombobox(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                                 End;
                       End
                       Else Begin//no momento de desabilitar
                                 //Retorno o evento de click
                                 //tcombobox(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                 tcombobox(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                 tcombobox(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                 tcombobox(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                 tcombobox(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                 tcombobox(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                 tcombobox(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                 //****************************************************
                                 //para n�o poder arrastar
                                 TComboBox(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                 //se tiver com o fundo colorido
                                 if(tcombobox(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                 Then tcombobox(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                 //retornando a cor original
                                 tcombobox(Self.FormChamou.Components [cont]).color:=tcombobox(Self.FormChamou.Components [cont]).tag;
                       End;
                End;
                5:Begin//memo
                       if (HabilitaDesabilita='H')//habilitando no inicio
                       Then Begin
                                 //guardando os eventos de click
                                 VetorOnMouseDown[cont]:=tmemo(Self.FormChamou.Components [cont]).OnMouseDown;
                                 VetorOnClick[cont]:=tmemo(Self.FormChamou.Components [cont]).OnClick;
                                 VetorOnEnter[cont]:=tmemo(Self.FormChamou.Components [cont]).OnEnter;
                                 VetorOnExit[cont]:=tmemo(Self.FormChamou.Components [cont]).OnExit;
                                 VetorOnKeyDown[cont]:=tmemo(Self.FormChamou.Components [cont]).OnKeyDown;
                                 VetorOnKeyUP[cont]:=tmemo(Self.FormChamou.Components [cont]).OnKeyUp;
                                 VetorOnKeyPress[cont]:=tmemo(Self.FormChamou.Components [cont]).OnKeyPress;
                                 //zerando os eventos
                                 tmemo(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                 tmemo(Self.FormChamou.Components [cont]).OnClick:=nil;
                                 tmemo(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                 tmemo(Self.FormChamou.Components [cont]).OnExit:=nil;
                                 tmemo(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                 tmemo(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                 tmemo(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                 //*************************************************
                                 //para  poder arrastar
                                 TMemo(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                 //guardando o evento de click
                                 TMemo(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                                 //guardando a cor original
                                 Tmemo(Self.FormChamou.Components [cont]).tag:=Tmemo(Self.FormChamou.Components [cont]).color;
                                 //colocando o visible para true e deixando com a cor de fundo
                                 if(Tmemo(Self.FormChamou.Components [cont]).Visible=False)
                                 Then Begin
                                           Tmemo(Self.FormChamou.Components [cont]).Visible:=True;
                                           Tmemo(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                                 End;
                       End
                       Else Begin//no momento de desabilitar
                                 //Retorno o evento de click
                                 tmemo(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                 tmemo(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                 tmemo(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                 tmemo(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                 tmemo(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                 tmemo(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                 tmemo(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                 //****************************************************
                                 //para n�o poder arrastar
                                 TMemo(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                 //se tiver com o fundo colorido
                                 if(tmemo(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                 Then tmemo(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                 //retornando a cor original
                                 tmemo(Self.FormChamou.Components [cont]).color:=tmemo(Self.FormChamou.Components [cont]).tag;
                       End;
                End;
                6:Begin//radiobutton
                       if (HabilitaDesabilita='H')//habilitando no inicio
                       Then Begin
                                 //guardando os eventos de click
                                 VetorOnMouseDown[cont]:=tradiobutton(Self.FormChamou.Components [cont]).OnMouseDown;
                                 VetorOnClick[cont]:=tradiobutton(Self.FormChamou.Components [cont]).OnClick;
                                 VetorOnEnter[cont]:=tradiobutton(Self.FormChamou.Components [cont]).OnEnter;
                                 VetorOnExit[cont]:=tradiobutton(Self.FormChamou.Components [cont]).OnExit;
                                 VetorOnKeyDown[cont]:=tradiobutton(Self.FormChamou.Components [cont]).OnKeyDown;
                                 VetorOnKeyUP[cont]:=tradiobutton(Self.FormChamou.Components [cont]).OnKeyUp;
                                 VetorOnKeyPress[cont]:=tradiobutton(Self.FormChamou.Components [cont]).OnKeyPress;
                                 //zerando os eventos
                                 tradiobutton(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                 tradiobutton(Self.FormChamou.Components [cont]).OnClick:=nil;
                                 tradiobutton(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                 tradiobutton(Self.FormChamou.Components [cont]).OnExit:=nil;
                                 tradiobutton(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                 tradiobutton(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                 tradiobutton(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                 //*************************************************
                                 //para poder arrastar
                                 TRadioButton(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                 //passando o click
                                 TRadioButton(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                                 //guardando a cor original
                                 Tradiobutton(Self.FormChamou.Components [cont]).tag:=Tradiobutton(Self.FormChamou.Components [cont]).color;
                                 //colocando o visible para true e deixando com a cor de fundo
                                 if(Tradiobutton(Self.FormChamou.Components [cont]).Visible=False)
                                 Then Begin
                                           Tradiobutton(Self.FormChamou.Components [cont]).Visible:=True;
                                           Tradiobutton(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                                 End;
                       End
                       Else Begin//no momento de desabilitar
                                 //Retorno o evento de click
                                 tradiobutton(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                 tradiobutton(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                 tradiobutton(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                 tradiobutton(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                 tradiobutton(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                 tradiobutton(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                 tradiobutton(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                 //****************************************************
                                 //para n�o poder arrastar
                                 TRadioButton(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                 //se tiver com o fundo colorido
                                 if(tradiobutton(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                 Then tradiobutton(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                 //retornando a cor original
                                 tradiobutton(Self.FormChamou.Components [cont]).color:=tradiobutton(Self.FormChamou.Components [cont]).tag;
                       End;
                End;
                7:Begin//check
                       if (HabilitaDesabilita='H')//habilitando no inicio
                       Then Begin
                                 //guardando os eventos de click
                                 VetorOnMouseDown[cont]:=tcheckbox(Self.FormChamou.Components [cont]).OnMouseDown;
                                 VetorOnClick[cont]:=tcheckbox(Self.FormChamou.Components [cont]).OnClick;
                                 VetorOnEnter[cont]:=tcheckbox(Self.FormChamou.Components [cont]).OnEnter;
                                 VetorOnExit[cont]:=tcheckbox(Self.FormChamou.Components [cont]).OnExit;
                                 VetorOnKeyDown[cont]:=tcheckbox(Self.FormChamou.Components [cont]).OnKeyDown;
                                 VetorOnKeyUP[cont]:=tcheckbox(Self.FormChamou.Components [cont]).OnKeyUp;
                                 VetorOnKeyPress[cont]:=tcheckbox(Self.FormChamou.Components [cont]).OnKeyPress;
                                 //zerando os eventos
                                 tcheckbox(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                 tcheckbox(Self.FormChamou.Components [cont]).OnClick:=nil;
                                 tcheckbox(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                 tcheckbox(Self.FormChamou.Components [cont]).OnExit:=nil;
                                 tcheckbox(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                 tcheckbox(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                 tcheckbox(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                 //*************************************************
                                 //para poder arrastar
                                 TCheckBox(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                 //guardando o evento de click
                                 TCHECKBOX(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                                 //guardando a cor original
                                 Tcheckbox(Self.FormChamou.Components [cont]).tag:=Tcheckbox(Self.FormChamou.Components [cont]).color;
                                 //colocando o visible para true e deixando com a cor de fundo
                                 if(Tcheckbox(Self.FormChamou.Components [cont]).Visible=False)
                                 Then Begin
                                           Tcheckbox(Self.FormChamou.Components [cont]).Visible:=True;
                                           Tcheckbox(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                                 End;
                       End
                       Else Begin//no momento de desabilitar
                                 //Retorno o evento de click
                                 tcheckbox(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                 tcheckbox(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                 tcheckbox(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                 tcheckbox(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                 tcheckbox(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                 tcheckbox(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                 tcheckbox(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                 //****************************************************
                                 //para n�o poder arrastar
                                 TCheckBox(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                 //se tiver com o fundo colorido
                                 if(tcheckbox(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                 Then tcheckbox(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                 //retornando a cor original
                                 tcheckbox(Self.FormChamou.Components [cont]).color:=tcheckbox(Self.FormChamou.Components [cont]).tag;
                       End;
                End;
                8:Begin//listbox
                       if (HabilitaDesabilita='H')//habilitando no inicio
                       Then Begin
                                 //guardando os eventos de click
                                 VetorOnMouseDown[cont]:=tlistbox(Self.FormChamou.Components [cont]).OnMouseDown;
                                 VetorOnClick[cont]:=tlistbox(Self.FormChamou.Components [cont]).OnClick;
                                 VetorOnEnter[cont]:=tlistbox(Self.FormChamou.Components [cont]).OnEnter;
                                 VetorOnExit[cont]:=tlistbox(Self.FormChamou.Components [cont]).OnExit;
                                 VetorOnKeyDown[cont]:=tlistbox(Self.FormChamou.Components [cont]).OnKeyDown;
                                 VetorOnKeyUP[cont]:=tlistbox(Self.FormChamou.Components [cont]).OnKeyUp;
                                 VetorOnKeyPress[cont]:=tlistbox(Self.FormChamou.Components [cont]).OnKeyPress;
                                 //zerando os eventos
                                 tlistbox(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                 tlistbox(Self.FormChamou.Components [cont]).OnClick:=nil;
                                 tlistbox(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                 tlistbox(Self.FormChamou.Components [cont]).OnExit:=nil;
                                 tlistbox(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                 tlistbox(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                 tlistbox(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                 //*************************************************
                                 //para poder arrastar
                                 TListBox(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                 //guardando o evento de click
                                 TLISTBOX(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                                 //guardando a cor original
                                 tlistbox(Self.FormChamou.Components [cont]).tag:=tlistbox(Self.FormChamou.Components [cont]).color;
                                 //colocando o visible para true e deixando com a cor de fundo
                                 if(tlistbox(Self.FormChamou.Components [cont]).Visible=False)
                                 Then Begin
                                           tlistbox(Self.FormChamou.Components [cont]).Visible:=True;
                                           tlistbox(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                                 End;
                       End
                       Else Begin//no momento de desabilitar
                                 //Retorno o evento de click
                                 tlistbox(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                 tlistbox(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                 tlistbox(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                 tlistbox(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                 tlistbox(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                 tlistbox(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                 tlistbox(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                 //****************************************************
                                 //para n�o poder arrastar
                                 TListBox(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                 //se tiver com o fundo colorido
                                 if(tlistbox(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                 Then tlistbox(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                 //retornando a cor original
                                 tlistbox(Self.FormChamou.Components [cont]).color:=tlistbox(Self.FormChamou.Components [cont]).tag;
                       End;
                End;
                9:Begin//radiogroup
                       if (HabilitaDesabilita='H')//habilitando no inicio
                       Then Begin
                                 //guardando os eventos de click
                                 //VetorOnMouseDown[cont]:=tradiogroup(Self.FormChamou.Components [cont]).OnMouseDown;
                                 VetorOnClick[cont]:=tradiogroup(Self.FormChamou.Components [cont]).OnClick;
                                 VetorOnEnter[cont]:=tradiogroup(Self.FormChamou.Components [cont]).OnEnter;
                                 VetorOnExit[cont]:=tradiogroup(Self.FormChamou.Components [cont]).OnExit;
                                 //VetorOnKeyDown[cont]:=tradiogroup(Self.FormChamou.Components [cont]).OnKeyDown;
                                 //VetorOnKeyUP[cont]:=tradiogroup(Self.FormChamou.Components [cont]).OnKeyUp;
                                 //VetorOnKeyPress[cont]:=tradiogroup(Self.FormChamou.Components [cont]).OnKeyPress;
                                 //zerando os eventos
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                 tradiogroup(Self.FormChamou.Components [cont]).OnClick:=nil;
                                 tradiogroup(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                 tradiogroup(Self.FormChamou.Components [cont]).OnExit:=nil;
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                 //*************************************************
                                 //para poder arrastar
                                 TradioGroup(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                 //guardando o evento de click
                                 TRadioGroup(Self.FormChamou.Components [cont]).OnClick:=Self.Clicou;
                                 //guardando a cor original
                                 TRadioGroup(Self.FormChamou.Components [cont]).tag:=TRadioGroup(Self.FormChamou.Components [cont]).color;
                                 //colocando o visible para true e deixando com a cor de fundo
                                 if(TRadioGroup(Self.FormChamou.Components [cont]).Visible=False)
                                 Then Begin
                                           TRadioGroup(Self.FormChamou.Components [cont]).Visible:=True;
                                           TRadioGroup(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                                 End;
                       End
                       Else Begin//no momento de desabilitar
                                 //Retorno o evento de click
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                 tradiogroup(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                 tradiogroup(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                 tradiogroup(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                 //tradiogroup(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                 //****************************************************
                                 //para n�o poder arrastar
                                 TRadioGroup(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                 //se tiver com o fundo colorido
                                 if(TRadioGroup(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                 Then TRadioGroup(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                 //retornando a cor original
                                 TRadioGroup(Self.FormChamou.Components [cont]).color:=TRadioGroup(Self.FormChamou.Components [cont]).tag;
                       End;
                End;
                10:Begin//richedit
                        if (HabilitaDesabilita='H')//habilitando no inicio
                        Then Begin
                                  //guardando os eventos de click
                                  VetorOnMouseDown[cont]:=trichedit(Self.FormChamou.Components [cont]).OnMouseDown;
                                  //VetorOnClick[cont]:=trichedit(Self.FormChamou.Components [cont]).OnClick;
                                  VetorOnEnter[cont]:=trichedit(Self.FormChamou.Components [cont]).OnEnter;
                                  VetorOnExit[cont]:=trichedit(Self.FormChamou.Components [cont]).OnExit;
                                  VetorOnKeyDown[cont]:=trichedit(Self.FormChamou.Components [cont]).OnKeyDown;
                                  VetorOnKeyUP[cont]:=trichedit(Self.FormChamou.Components [cont]).OnKeyUp;
                                  VetorOnKeyPress[cont]:=trichedit(Self.FormChamou.Components [cont]).OnKeyPress;
                                  //zerando os eventos
                                  trichedit(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                  //trichedit(Self.FormChamou.Components [cont]).OnClick:=nil;
                                  trichedit(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                  trichedit(Self.FormChamou.Components [cont]).OnExit:=nil;
                                  trichedit(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                  trichedit(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                  trichedit(Self.FormChamou.Components [cont]).OnKeyPress:=nil;
                                  //*************************************************
                                  //para poder arrastar
                                  TRichEdit(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                  //guardando o evento de click
                                  TRICHEDIT(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                                  //guardando a cor original
                                  TRICHEDIT(Self.FormChamou.Components [cont]).tag:=TRICHEDIT(Self.FormChamou.Components [cont]).color;
                                  //colocando o visible para true e deixando com a cor de fundo
                                  if(TRICHEDIT(Self.FormChamou.Components [cont]).Visible=False)
                                  Then Begin
                                            TRICHEDIT(Self.FormChamou.Components [cont]).Visible:=True;
                                            TRICHEDIT(Self.FormChamou.Components [cont]).Color:=Self.PCorInvisivel;
                                  End;
                        End
                        Else Begin//no momento de desabilitar
                                  //Retorno o evento de click
                                  trichedit(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                  //trichedit(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                  trichedit(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                  trichedit(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                  trichedit(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                  trichedit(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                  trichedit(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];
                                  //****************************************************
                                  //para n�o poder arrastar
                                  TRichEdit(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                  //se tiver com o fundo colorido
                                  if(TRICHEDIT(Self.FormChamou.Components [cont]).color=Self.PCorInvisivel)
                                  Then TRICHEDIT(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                  //retornando a cor original
                                  TRICHEDIT(Self.FormChamou.Components [cont]).color:=TRICHEDIT(Self.FormChamou.Components [cont]).tag;
                        End;
                End;
                11:begin//shape
                        if (HabilitaDesabilita='H')//habilitando no inicio
                        Then Begin
                                  //guardando os eventos de click
                                  VetorOnMouseDown[cont]:=tshape(Self.FormChamou.Components [cont]).OnMouseDown;
                                  //VetorOnClick[cont]:=tshape(Self.FormChamou.Components [cont]).OnClick;
                                  //VetorOnEnter[cont]:=tshape(Self.FormChamou.Components [cont]).OnEnter;
                                  //VetorOnExit[cont]:=tshape(Self.FormChamou.Components [cont]).OnExit;
                                  //VetorOnKeyDown[cont]:=tshape(Self.FormChamou.Components [cont]).OnKeyDown;
                                  //VetorOnKeyUP[cont]:=tshape(Self.FormChamou.Components [cont]).OnKeyUp;
                                  //VetorOnKeyPress[cont]:=tshape(Self.FormChamou.Components [cont]).OnKeyPress;
                                  //zerando os eventos
                                  tshape(Self.FormChamou.Components [cont]).OnMouseDown:=nil;
                                  {tshape(Self.FormChamou.Components [cont]).OnClick:=nil;
                                  tshape(Self.FormChamou.Components [cont]).OnEnter:=nil;
                                  tshape(Self.FormChamou.Components [cont]).OnExit:=nil;
                                  tshape(Self.FormChamou.Components [cont]).OnKeyDown:=nil;
                                  tshape(Self.FormChamou.Components [cont]).OnKeyUp:=nil;
                                  tshape(Self.FormChamou.Components [cont]).OnKeyPress:=nil;}
                                  //*************************************************
                                  //para poder arrastar
                                  TShape(Self.FormChamou.Components [cont]).DragMode:=dmAutomatic;
                                  //guardando o evento de click
                                  TSHAPE(Self.FormChamou.Components [cont]).OnMouseDown:=Self.PersonalizaMouseDown;
                                  //guardando a cor original
                                  TSHAPE(Self.FormChamou.Components [cont]).tag:=TSHAPE(Self.FormChamou.Components [cont]).brush.color;
                                  //colocando o visible para true e deixando com a cor de fundo
                                  if(TSHAPE(Self.FormChamou.Components [cont]).Visible=False)
                                  Then Begin
                                            TSHAPE(Self.FormChamou.Components [cont]).Visible:=True;
                                            TSHAPE(Self.FormChamou.Components [cont]).brush.Color:=Self.PCorInvisivel;
                                  End;
                        End
                        Else Begin//no momento de desabilitar
                                  //Retorno o evento de click
                                  tshape(Self.FormChamou.Components [cont]).OnMouseDown:=VetorOnMouseDown[cont];
                                  {tshape(Self.FormChamou.Components [cont]).OnClick:=VetorOnClick[cont];
                                  tshape(Self.FormChamou.Components [cont]).OnEnter:=VetorOnEnter[cont];
                                  tshape(Self.FormChamou.Components [cont]).OnExit:=VetorOnExit[cont];
                                  tshape(Self.FormChamou.Components [cont]).OnKeyDown:=VetorOnKeyDown[cont];
                                  tshape(Self.FormChamou.Components [cont]).OnKeyUp:=VetorOnKeyUP[cont];
                                  tshape(Self.FormChamou.Components [cont]).OnKeyPress:=VetorOnKeyPress[cont];}
                                  //****************************************************
                                  //para n�o poder arrastar
                                  TShape(Self.FormChamou.Components [cont]).DragMode:=dmManual;
                                  //se tiver com o fundo colorido
                                  if(TSHAPE(Self.FormChamou.Components [cont]).brush.color=Self.PCorInvisivel)
                                  Then TSHAPE(Self.FormChamou.Components [cont]).Visible:=False;//coloco invisivel
                                  //retornando a cor original
                                  TSHAPE(Self.FormChamou.Components [cont]).brush.color:=TSHAPE(Self.FormChamou.Components [cont]).tag;
                        End;
                End;
           End;//case
   End;//for

end;

procedure TFconfiguracoesVisuaisFormulario.HabilitaModoConfiguracao;
var
cont:integer;
begin
     if (Messagedlg('Deseja Habilitar o modo de Configura��o de Tela?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     if (ObjConfiguracaoTelaGlobal.LocalizaNomeFormulario(uppercase(Self.FormChamou.ClassName))=False)
     Then Begin
               //****Caso nao tenha uma configuracao salva, � necess�rio
               //salvar pois caso cancele as alteracoes � necessarios
               //abrir as que foram salvas
               ObjConfiguracaoTelaGlobal.ZerarTabela;
               ObjConfiguracaoTelaGlobal.submit_Codigo('0');
               ObjConfiguracaoTelaGlobal.status:=dsinsert;
               ObjConfiguracaoTelaGlobal.Submit_NomeFormulario(uppercase(Self.FormChamou.ClassName));
               if (ObjConfiguracaoTelaGlobal.GravaConfiguracoes(Self.FormChamou)=False)
               Then Begin
                        MensagemErro('Erro na tentativa de gravar as configura��es dos componentes visuais');
                        exit;
              End;
              if (ObjConfiguracaoTelaGlobal.Salvar(True)=False)
              Then Begin
                        MensagemErro('Erro na tentativa de Salvar a configura��o de tela para este formul�rio');
                        exit;
              End;
     End;

     //zerando os vetores que cuidarao dos eventos 
     for cont:=0 to PquantidadeComponentesVetorMemoria do
     begin
          VetorOnMouseDown[cont]:=nil;
          VetorOnClick[cont]:=nil;
          VetorOnEnter[cont]:=nil;
          VetorOnExit[cont]:=nil;
          VetorOnKeyDown[cont]:=nil;
          VetorOnKeyUP[cont]:=nil;
          VetorOnKeyPress[cont]:=nil;
     End;
     //****************************************************

     //Guardo o OnClose e o onkeydown do form que chamou e substituo por um desse form
     Self.FormClose_Backup:=nil;
     Self.FormClose_Backup:=Self.FormChamou.OnClose;
     Self.FormChamou.onclose:=Self.FormCloseChamou;

     Self.FormOnKeyDown_Backup:=nil;
     Self.FormOnKeyDown_Backup:=Self.FormChamou.OnKeyDown;
     Self.formchamou.OnKeyDown:=Self.KeyDownConfiguracao;

     Self.FormDragOver_backup:=nil;
     FormDragOver_backup:=Self.FormChamou.OnDragOver;
     Self.formchamou.OnDragOver:=Self.FormDragOverChamou;

     //*************************************************************************

     habilita_campos(Self.FormChamou);
     desab_botoes(Self.FormChamou);
     Self.lbnomecomponente.Caption:='';
     Self.classecomponente:='';
     Self.ConfiguracaoAtivada:=True;
     Self.Relacionamento('H');
     Self.Tag:=0;
     Self.BtOcultaCampo.Enabled:=False;
     Self.Show;
end;


procedure TFconfiguracoesVisuaisFormulario.Inicializa(Pform:TForm);
var
cont:integer;
begin
     Self.PCorInvisivel:=RGB(218,56,0);

     for cont:=0 to PquantidadeComponentesVetorMemoria do
     begin
          VetorOnMouseDown[cont]:=nil;
          VetorOnClick[cont]:=nil;
     End;

     Self.ConfiguracaoAtivada:=False;
     Self.PpermiteAlterar:=False;
     if (ObjConfiguracaoTelaGlobal.LocalizaNomeFormulario(Uppercase(Pform.ClassName))=True)
     Then Begin
               //se tiver uma configuracao de tela gravada eu chamo as configura��es para cada edit e label
               ObjConfiguracaoTelaGlobal.TabelaparaObjeto;
               ObjConfiguracaoTelaGlobal.ResgataConfiguracoes(Pform);
     End;
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ALTERAR CONFIGURA��ES VISUAIS DE FORMUL�RIOS')=true)
     Then Self.PpermiteAlterar:=True;
     Self.FormChamou:=Pform;
     Self.Tag:=0;

end;

procedure TFconfiguracoesVisuaisFormulario.KeyDownConfiguracao(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
     if (Self.PpermiteAlterar=False)
     then exit;

     if (Self.ConfiguracaoAtivada=False)
     Then Begin
                if (key=vk_f6)
                Then Self.HabilitaModoconfiguracao;
     End
     Else Begin
               if (key=vk_f6)
               Then Self.show
               Else Begin
                         if ((key=Vk_up) or (Key=vk_down) or (key=vk_left) or (key=vk_right))
                         Then Self.Oculta_Movimenta(sender,key,'M');
               End;

     End;
end;

procedure TFconfiguracoesVisuaisFormulario.FormCloseChamou(Sender: TObject;
  var Action: TCloseAction);
begin
     if (Self.ConfiguracaoAtivada=True)
     Then Action:=caNone;
end;


procedure TFconfiguracoesVisuaisFormulario.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.KeyDownConfiguracao(sender,key,shift);
end;

procedure TFconfiguracoesVisuaisFormulario.PersonalizaMouseDown(
  Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
     if (Self.ConfiguracaoAtivada=True)
     Then Begin
               //reaproveito a funcao de OnClick
               Self.clicou(Sender);
     End;
end;





procedure TFconfiguracoesVisuaisFormulario.FormDragOverChamou(Sender,
  Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
PtipoComponente:integer;
PComponente:String;
begin

     Pcomponente:=UpperCase(Source.ClassName);
     PtipoComponente:=0;


     If (Pcomponente='TEDIT')
     Then PtipoComponente:=1
     Else
        If (Pcomponente='TLABEL')
        Then PtipoComponente:=2
        Else
           If (Pcomponente='TMASKEDIT')
           Then PtipoComponente:=3
           else
              If (Pcomponente='TCOMBOBOX')
              Then PtipoComponente:=4
              else
                 If (Pcomponente='TMEMO')
                 Then PtipoComponente:=5
                 else
                    If (Pcomponente='TRADIOBUTTON')
                    Then PtipoComponente:=6
                    else
                       If (Pcomponente='TCHECKBOX')
                       Then PtipoComponente:=7
                       else
                          If (Pcomponente='TLISTBOX')
                          Then PtipoComponente:=8
                          else
                             If (Pcomponente='TRADIOGROUP')
                             Then PtipoComponente:=9
                             else
                                If (Pcomponente='TRICHEDIT')
                                Then PtipoComponente:=10
                                else
                                   If (Pcomponente='TSHAPE')
                                   Then PtipoComponente:=11;

     case PtipoComponente of
        1:Begin
               TEdit(Source).Left:=X;
               TEdit(Source).top:=y;
        End;
        2:Begin
               TLabel(Source).Left:=X;
               TLabel(Source).top:=y;
        End;
        3:Begin
               TmaskEdit(Source).Left:=X;
               TmaskEdit(Source).top:=y;
        End;
        4:Begin
               TComboBox(Source).Left:=X;
               TComboBox(Source).top:=y;
        End;
        5:Begin
               Tmemo(Source).Left:=X;
               Tmemo(Source).top:=y;
        End;
        6:Begin
               TRadioButton(Source).Left:=X;
               TRadioButton(Source).top:=y;
        End;
        7:Begin
               TCheckBox(Source).Left:=X;
               TCheckBox(Source).top:=y;
        End;
        8:Begin
               TListBox(Source).Left:=X;
               TListBox(Source).top:=y;
        End;
        9:Begin
               TRadioGroup(Source).Left:=X;
               TRadioGroup(Source).top:=y;
        End;
        10:Begin
                TRichEdit(Source).Left:=X;
                TRichEdit(Source).top:=y;
        End;
        11:Begin
               TShape(Source).Left:=X;
               TShape(Source).top:=y;
        End;
     End;//case
     Accept:=true;
end;

end.


