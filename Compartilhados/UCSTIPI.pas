unit UCSTIPI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCSTIPI,
  jpeg;

type
  TFCSTIPI = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Label1: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbDescricao: TLabel;
    EdtDescricao: TEdit;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCSTIPI:TObjCSTIPI;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCSTIPI: TFCSTIPI;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCSTIPI.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCSTIPI do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Descricao(edtDescricao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCSTIPI.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCSTIPI do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtDescricao.text:=Get_Descricao;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCSTIPI.TabelaParaControles: Boolean;
begin
     If (Self.ObjCSTIPI.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFCSTIPI.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCSTIPI=Nil)
     Then exit;

     If (Self.ObjCSTIPI.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjCSTIPI.free;
     if(Self.Owner = nil) then
       FDataModulo.CommitGeral;
     
end;

procedure TFCSTIPI.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFCSTIPI.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjCSTIPI.status:=dsInsert;
     edtDescricao.setfocus;

end;


procedure TFCSTIPI.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCSTIPI.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCSTIPI.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtDescricao.setfocus;
                
    End;


end;

procedure TFCSTIPI.btgravarClick(Sender: TObject);
begin

     If Self.ObjCSTIPI.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCSTIPI.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCSTIPI.Get_codigo;
     Self.ObjCSTIPI.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCSTIPI.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjCSTIPI.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCSTIPI.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCSTIPI.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCSTIPI.btcancelarClick(Sender: TObject);
begin
     Self.ObjCSTIPI.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCSTIPI.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFCSTIPI.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCSTIPI.Get_pesquisa,Self.ObjCSTIPI.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCSTIPI.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCSTIPI.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjCSTIPI.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFCSTIPI.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCSTIPI.FormShow(Sender: TObject);
begin

   limpaedit(Self);
   Self.limpaLabels;
   desabilita_campos(Self);

   Try
      Self.ObjCSTIPI:=TObjCSTIPI.create(Self);
   Except
         Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
         esconde_botoes(self);
         exit;
   End;
   FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
   FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
   FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
   Self.Color:=Clwhite;
   retira_fundo_labels(self);
   PegaCorForm(Self);

end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

