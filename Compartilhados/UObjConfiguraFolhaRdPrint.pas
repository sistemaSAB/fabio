unit UObjConfiguraFolhaRdPrint;
interface
uses RDprint, inifiles,sysutils,dialogs,controls;

Type
   TObjConfiguraFolhaRdPrint=class

          Public
                fonte:string;
                Function ResgataConfiguracoesFolha(RDPrint1:TRdPrint;NomeINI:String):Boolean;
                constructor create;
          Private
                Function GravaNovoINI(Caminho:String):Boolean;
          End;


implementation
uses forms;



constructor TObjConfiguraFolhaRdPrint.create;
begin
     Self.fonte:='';
end;

function TObjConfiguraFolhaRdPrint.GravaNovoINI(Caminho: String): Boolean;
var
arq:TextFile;
arquivo_ini:Tinifile;
Begin
     result:=False;
     if (FileExists(caminho)=False)
     Then Begin
               Try
                  AssignFile(arq,caminho);
                  Rewrite(arq);
                  closefile(arq);
               Except
                        Messagedlg('Erro na Cria��o do Arquivo '+caminho,mterror,[mbok],0);
                        exit;
                End;

     End;

     Try
        Arquivo_ini:=Tinifile.Create(caminho);
     Except
           Messagedlg('Erro na Abertura de '+caminho,mterror,[mbok],0);
           exit;
     End;
     Try
        arquivo_ini.WriteString('CONFIGURACOES','PreviewZoom','90');
        arquivo_ini.WriteString('CONFIGURACOES','Preview','SIM');
        arquivo_ini.WriteString('CONFIGURACOES','CaptionSetup','XXX');
        arquivo_ini.WriteString('CONFIGURACOES','TestarPorta','N�O');
        arquivo_ini.WriteString('CONFIGURACOES','#','OPCOES S12CPP,S05CPP,S10CPP,S17CPP,S20CPP');
        arquivo_ini.WriteString('CONFIGURACOES','FonteTamanhoPadrao','S20cpp');
        arquivo_ini.WriteString('CONFIGURACOES','QUANTIDADECOLUNAS','160');
        arquivo_ini.WriteString('CONFIGURACOES','QUANTIDADELINHAS','60');
        arquivo_ini.WriteString('CONFIGURACOES','PortaComunicacao','LPT1');
        arquivo_ini.WriteString('CONFIGURACOES','Acentuacao','SIM');
        arquivo_ini.WriteString('CONFIGURACOES','UsaGerenciadorImpressao','SIM');
        arquivo_ini.WriteString('CONFIGURACOES','IMPRESSORA','');
        arquivo_ini.WriteString('CONFIGURACOES','##','OPCOES SEIS E OITO LINHA POR POLEGADA');
        arquivo_ini.WriteString('CONFIGURACOES','ENTRELINHAS','OITO');
        result:=True;
     Finally
        FreeAndNil(arquivo_ini);
     End;
end;

Function TObjConfiguraFolhaRdPrint.ResgataConfiguracoesFolha(RDPrint1:TRdPrint;NomeINI:String):boolean;
var
NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin
     result:=False;
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     temp:=Temp+'CONFRELS\'+nomeini;

     if (FileExists(temp)=False)
     Then Begin
               If (Messagedlg('O Arquivo '+Temp+' n�o foi encontrado, deseja criar este arquivo com configura��es padr�es?',mtconfirmation,[mbyes,mbno],0)=mrno)
               Then exit
               Else Begin
                         If (Self.GravaNovoINI(Temp)=False)
                         Then Begin
                                   Messagedlg('N�o foi poss�vel criar o INI '+temp,mterror,[mbok],0);
                                   exit;
                        End;

               End;
     End;


     Try
        Arquivo_ini:=Tinifile.Create(temp);
     Except
           Messagedlg('Erro na Abertura do Arquivo CONFRELS\'+nomeini,mterror,[mbok],0);
           exit;
     End;
Try
     Try


            NOMECAMPO:='PREVIEWZOOM';
            Temp:=arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,'');

            if (temp<>'')
            Then Begin
                      strtoint(temp);
                      rdprint1.OpcoesPreview.PreviewZoom:=strtoint(temp);
            End;

            NOMECAMPO:='PREVIEW';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.OpcoesPreview.Preview := True;
            if (temp='N�O')
            Then rdprint1.OpcoesPreview.Preview := False;

            NOMECAMPO:='CAPTIONSETUP';
            Temp:=arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,'');
            rdprint1.CaptionSetup:=temp;

            NOMECAMPO:='FONTETAMANHOPADRAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.FonteTamanhoPadrao:=S12cpp;

            if (Temp='S12CPP')
            Then rdprint1.FonteTamanhoPadrao:=S12cpp
            Else
            if (temp='S05CPP')
            Then rdprint1.FonteTamanhoPadrao:=S05cpp
            Else
            if (temp='S10CPP')
            Then rdprint1.FonteTamanhoPadrao:=S10cpp
            eLSE
            if (temp='S17CPP')
            Then rdprint1.FonteTamanhoPadrao:=S17cpp
            eLSE
            if (temp='S20CPP')
            Then rdprint1.FonteTamanhoPadrao:=S20cpp;


            NOMECAMPO:='TIPOFONTE';
            temp:='';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));

            if (temp='NEGRITO') or (temp='ITALICO') or (temp='SUBLINHADO')
            or (temp='EXPANDIDO') or (temp='NORMAL')  or (temp='COMP12')
            or (temp='COMP17') or (temp='COMP20')
            Then Self.fonte:=temp;
 
            NOMECAMPO:='QUANTIDADELINHAS';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.TamanhoQteLinhas:=66;
            if (temp<>'')
            Then Begin
                      try
                         StrToInt(temp);
                         RDPrint1.TamanhoQteLinhas:=StrToInt(temp);
                      except
                            Messagedlg('Erro  no valor da QUANTIDADE DE LINHAS',mterror,[mbok],0);
                      end;
            End;

            NOMECAMPO:='QUANTIDADECOLUNAS';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.TamanhoQteColunas:=96;
            if (temp<>'')
            Then Begin
                      try
                         StrToInt(temp);
                         RDPrint1.TamanhoQteColunas:=StrToInt(temp);
                      except
                            Messagedlg('Erro  no valor da QUANTIDADE DE COLUNAS',mterror,[mbok],0);
                      end;
            End;


            NOMECAMPO:='PORTACOMUNICACAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.PortaComunicacao:=TEMP;


            NOMECAMPO:='ACENTUACAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.Acentuacao:=SemAcento;
            IF (TEMP='SIM')
            tHEN rdprint1.Acentuacao:=Transliterate;

            NOMECAMPO:='USAGERENCIADORIMPRESSAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.UsaGerenciadorImpr:=FALSE;
            IF (TEMP='SIM')
            tHEN rdprint1.UsaGerenciadorImpr:=TRUE;

            NOMECAMPO:='ENTRELINHAS';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.TamanhoQteLPP:=Seis;
            IF (TEMP='SEIS')
            tHEN rdprint1.TamanhoQteLPP:=Seis
            Else
            IF (TEMP='OITO')
            tHEN rdprint1.TamanhoQteLPP:=Oito;

            NOMECAMPO:='IMPRESSORA';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));

            IF (TEMP<>'')
            tHEN rdprint1.SetPrinterbyName(Temp);

     Except
           Messagedlg('Erro na leitura do CHAVE '+NOMECAMPO,mterror,[mbok],0);
           exit;
     End;

     result:=True;

Finally
     FreeAndNil(arquivo_ini);
End;

end;


end.
