{$I ACBr.inc}

unit ubalanca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, ACBrBase, ACBrBAL,AcbrDevice;

type
  TFbalanca = class(TForm)
    ACBrBAL1: TACBrBAL;
    pnlConf: TPanel;
    Panel1: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    cmbBalanca: TComboBox;
    cmbPortaSerial: TComboBox;
    cmbBaudRate: TComboBox;
    cmbDataBits: TComboBox;
    cmbHandShaking: TComboBox;
    cmbParity: TComboBox;
    cmbStopBits: TComboBox;
    CmbUsaBalanca: TComboBox;
    Label16: TLabel;
    btnCarregarINI: TButton;
    btnSalvarINI: TButton;
    edtTimeOut: TEdit;
    Label9: TLabel;
    pnlGeral: TPanel;
    btnSair: TButton;
    chbMonitorar: TCheckBox;
    Memo1: TMemo;
    Label10: TLabel;
    sttResposta: TStaticText;
    Label2: TLabel;
    sttPeso: TStaticText;
    Label3: TLabel;
    btnLerPeso: TButton;
    btnConectar: TButton;
    btnDesconectar: TButton;
    lblTESTE: TLabel;
    procedure btnConectarClick(Sender: TObject);
    procedure btnLerPesoClick(Sender: TObject);
    procedure btnSairClick(Sender: TObject);
    procedure btnDesconectarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtTimeOutKeyPress(Sender: TObject; var Key: Char);
    procedure chbMonitorarClick(Sender: TObject);
    procedure ACBrBAL1LePeso(Peso: Double; Resposta: String);
    procedure btnCarregarINIClick(Sender: TObject);
    procedure btnSalvarINIClick(Sender: TObject);

  private
    function Converte( cmd : String) : String;
    function CarregaItem(Pnome:String;Pcombo: Tcombobox; pchave:string): boolean;

  public
    UsaBalanca:Boolean;
    function CarregaIni: boolean;
    function VerificaUsaBalanca:Boolean;
  end;

var
  Fbalanca: TFbalanca;

const
  ArquivoIni = 'balanca.ini';

implementation

uses UessencialGlobal, UDataModulo;

{$R *.dfm}


function TFbalanca.Converte(cmd: String): String;
var
  A : Integer ;
begin
  Result := '' ;
  for A := 1 to length( cmd ) do
  begin
    if not (cmd[A] in ['A'..'Z','a'..'z','0'..'9',
                      ' ','.',',','/','?','<','>',';',':',']','[','{','}',
                      '\','|','=','+','-','_',')','(','*','&','^','%','$',
                      '#','@','!','~',']' ]) then
      Result := Result + '#' + IntToStr(ord( cmd[A] )) + ' '
    else
      Result := Result + cmd[A] + ' ';
  end ;
end;

procedure TFbalanca.btnConectarClick(Sender: TObject);
begin
  // se houver conec��o aberta, Fecha a conec��o
  if acbrBal1.Ativo then
    ACBrBAL1.Desativar;

  // configura porta de comunica��o
  ACBrBAL1.Modelo           := TACBrBALModelo( cmbBalanca.ItemIndex );
  ACBrBAL1.Device.HandShake := TACBrHandShake( cmbHandShaking.ItemIndex );
  ACBrBAL1.Device.Parity    := TACBrSerialParity( cmbParity.ItemIndex );
  ACBrBAL1.Device.Stop      := TACBrSerialStop( cmbStopBits.ItemIndex );
  ACBrBAL1.Device.Data      := StrToInt( cmbDataBits.text );
  ACBrBAL1.Device.Baud      := StrToInt( cmbBaudRate.Text );
  ACBrBAL1.Device.Porta     := cmbPortaSerial.Text;

  // Conecta com a balan�a
  ACBrBAL1.Ativar;

  btnConectar.Enabled    := False;
  pnlConf.Enabled        := False;
  btnDesconectar.Enabled := True;
  btnLerPeso.Enabled     := True;
end;

procedure TFbalanca.btnLerPesoClick(Sender: TObject);
var
  TimeOut : Integer ;
begin
  try
    TimeOut := StrToInt( edtTimeOut.Text ) ;
  except
    TimeOut := 2000 ;
  end ;

  ACBrBAL1.LePeso( TimeOut );
end;

procedure TFbalanca.btnSairClick(Sender: TObject);
begin
  close;
end;

procedure TFbalanca.btnDesconectarClick(Sender: TObject);
begin
  ACBrBAL1.Desativar;

  btnConectar.Enabled    := True;
  pnlConf.Enabled        := True;
  btnDesconectar.Enabled := False;
  btnLerPeso.Enabled     := False;
end;

procedure TFbalanca.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ACBrBAL1.Desativar ;
  if(Self.Owner = nil) then
    if(FDataModulo.IBTransaction.InTransaction) then
      FDataModulo.IBTransaction.Commit;
end;

procedure TFbalanca.edtTimeOutKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in ['0'..'9',#13,#8]) then
    Key := #0 ;
end;

procedure TFbalanca.chbMonitorarClick(Sender: TObject);
begin
   ACBrBAL1.MonitorarBalanca := chbMonitorar.Checked ;
end;

procedure TFbalanca.ACBrBAL1LePeso(Peso: Double; Resposta: String);
var
  valid : Integer;
begin
  sttPeso.Caption     := formatFloat('##0.000', Peso );
  sttResposta.Caption := Converte( Resposta ) ;

  if Peso > 0 then
    Memo1.Lines.Text := 'Leitura OK !'
  else
  begin
    valid := Trunc(ACBrBAL1.UltimoPesoLido);
    case valid of
       0  : Memo1.Lines.Text := 'TimeOut !' + sLineBreak+
                               'Coloque o produto sobre a Balan�a!' ;
      -1  : Memo1.Lines.Text := 'Peso Instavel ! ' + sLineBreak+
                               'Tente Nova Leitura' ;
      -2  : Memo1.Lines.Text := 'Peso Negativo !' ;
      -10 : Memo1.Lines.Text := 'Sobrepeso !' ;
    end;
  end ;
end;


function TfBalanca.CarregaItem(pNome:String; pCombo:TComboBox; pChave:String): Boolean;
var
  pValor : string;
begin
  Result := False;

  pValor := '';
  LeChaveIni(ArquivoIni, 'BALANCA', UpperCase( pChave ), pValor);
  pValor := UpperCase( pValor );

  pCombo.ItemIndex := pCombo.Items.IndexOf(pValor);

  //showmessage(Pnome+' Chave: '+pchave+' Valor: '+pvalor+' Indice: '+inttostr(pcombo.Items.IndexOf(pvalor)));

  if (pCombo.Itemindex = -1 ) then
  begin
    MensagemErro( pNome + ' INV�LIDA(o) NO PAR�METRO' );
    exit;
  end;

  Result := True;
end;

procedure TFbalanca.btnCarregarINIClick(Sender: TObject);
begin
  Self.CarregaIni;
end;

function TfBalanca.CarregaIni : Boolean;
var
  pValor : String;
begin
  Result := False;

  if (Self.CarregaItem('Balan�a', cmbBalanca, 'BALANCA') = False) then
    exit;

  if (Self.CarregaItem('Porta Serial', cmbPortaSerial, 'PORTA') = False) then
    exit;

  if (Self.CarregaItem('Baud Rate', cmbBaudRate, 'BAUDRATE') = False) then
    exit;

  if (Self.CarregaItem('Data Bits', cmbDataBits, 'DATABITS') = False) then
    exit;

  if (Self.CarregaItem('Parity', cmbParity, 'PARITY') = False) then
    exit;

  if (Self.CarregaItem('Stop Bits', cmbStopBits, 'STOPBITS') = False) then
    exit;

  if (Self.CarregaItem('HandShaking', cmbHandShaking, 'HANDSHAKING') = False) then
    exit;

  if (Self.CarregaItem('Usa Balanca', cmbUsaBalanca, 'USABALANCA') = False) then
    exit;

  pValor:='';
  LeChaveIni(ArquivoIni, 'BALANCA', 'TIMEOUT', pValor);

  try
    strtoint(pValor);
  except
    MensagemErro('Valor de TimeOut Inv�lido no par�metro');
    exit;
  end;

  edtTimeOut.Text := pValor;
  LeChaveIni(ArquivoIni, 'BALANCA', 'TIMEOUT', pValor);
     
  Result := True;
end;

procedure TFbalanca.btnSalvarINIClick(Sender: TObject);
begin
  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'BALANCA', cmbBalanca.text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'PORTA', cmbPortaSerial.text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'BAUDRATE', cmbBaudRate.text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'DATABITS', cmbDataBits.text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'PARITY', cmbParity.Text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'STOPBITS', cmbStopBits.text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'HANDSHAKING', cmbHandShaking.text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'TIMEOUT', edtTimeOut.text) = False) then
    exit;

  if (GravaChaveIni(ArquivoIni, 'BALANCA', 'USABALANCA', cmbUsaBalanca.text) = False) then
    exit;
end;

function TFbalanca.VerificaUsaBalanca: Boolean;
var
  pvalor : String;
begin
  Result := False;

  pvalor := '';
  LeChaveIni(ArquivoIni, 'BALANCA', 'USABALANCA', pvalor);
  pvalor := UpperCase(pvalor);

  if (pvalor='SIM') then
    Result := True
  else Result := False;

  Self.UsaBalanca := Result;
end;


end.
