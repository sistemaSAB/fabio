unit UObjREQUISICAOSENHALOCAL;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal;
//USES_INTERFACE


Type
   TObjREQUISICAOSENHALOCAL=class
          Public
                Objquery:Tibquery;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure  ZerarTabela;
                Procedure  Cancelar;
                Procedure  Commit;

                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;


                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_RESPOSTA(parametro: String);
                Function Get_RESPOSTA: String;
                Procedure Submit_MENSAGEMRESPOSTA(parametro: string);
                Function Get_MENSAGEMRESPOSTA: string;
                //CODIFICA DECLARA GETSESUBMITS

         Private

               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               RESPOSTA:String;
               MENSAGEMRESPOSTA:string;
//CODIFICA VARIAVEIS PRIVADAS





               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation

uses SysUtils,Dialogs,UDatamodulo,Controls;





Function  TObjREQUISICAOSENHALOCAL.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.RESPOSTA:=fieldbyname('RESPOSTA').asstring;
        Self.MENSAGEMRESPOSTA:=fieldbyname('MENSAGEMRESPOSTA').asstring;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjREQUISICAOSENHALOCAL.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('RESPOSTA').asstring:=Self.RESPOSTA;
        ParamByName('MENSAGEMRESPOSTA').asstring:=Self.MENSAGEMRESPOSTA;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjREQUISICAOSENHALOCAL.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjREQUISICAOSENHALOCAL.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        RESPOSTA:='';
        MENSAGEMRESPOSTA:='';
//CODIFICA ZERARTABELA



     End;
end;

Function TObjREQUISICAOSENHALOCAL.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjREQUISICAOSENHALOCAL.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjREQUISICAOSENHALOCAL.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjREQUISICAOSENHALOCAL.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjREQUISICAOSENHALOCAL.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjREQUISICAOSENHALOCAL.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro REQUISICAOSENHA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,RESPOSTA,MENSAGEMRESPOSTA');
           SQL.ADD(' from  TABREQUISICAOSENHA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjREQUISICAOSENHALOCAL.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjREQUISICAOSENHALOCAL.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjREQUISICAOSENHALOCAL.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


    Try
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABREQUISICAOSENHA(CODIGO,RESPOSTA,MENSAGEMRESPOSTA');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:RESPOSTA,:MENSAGEMRESPOSTA)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABREQUISICAOSENHA set CODIGO=:CODIGO,RESPOSTA=:RESPOSTA');
                ModifySQL.add(',MENSAGEMRESPOSTA=:MENSAGEMRESPOSTA');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABREQUISICAOSENHA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

    Except
          on e:exception do
          Begin
                raise Exception.Create('Erro na tentativa de cria��o do Objeto OBJREQUISICAOSENHALOCAL'+#13+e.message);
          End;

    End;


end;
procedure TObjREQUISICAOSENHALOCAL.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjREQUISICAOSENHALOCAL.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabREQUISICAOSENHA');
     Result:=Self.ParametroPesquisa;
end;

function TObjREQUISICAOSENHALOCAL.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Requisi��o de Senha Local ';
end;




destructor TObjREQUISICAOSENHALOCAL.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjREQUISICAOSENHALOCAL.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjREQUISICAOSENHALOCAL.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjREQUISICAOSENHALOCAL.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjREQUISICAOSENHALOCAL.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjREQUISICAOSENHALOCAL.Submit_RESPOSTA(parametro: String);
begin
        Self.RESPOSTA:=Parametro;
end;
function TObjREQUISICAOSENHALOCAL.Get_RESPOSTA: String;
begin
        Result:=Self.RESPOSTA;
end;
procedure TObjREQUISICAOSENHALOCAL.Submit_MENSAGEMRESPOSTA(parametro: string);
begin
        Self.MENSAGEMRESPOSTA:=Parametro;
end;
function TObjREQUISICAOSENHALOCAL.Get_MENSAGEMRESPOSTA: string;
begin
        Result:=Self.MENSAGEMRESPOSTA;
end;
//CODIFICA GETSESUBMITS

end.



