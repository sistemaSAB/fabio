{jonas}
unit UobjExtraiXML;

interface

uses Classes, SysUtils,XMLDoc,XMLIntf;


type
  TICMS = class

  private


  public

    orig    :string;
    CST     :string;
    CSOSN   :string;
    modBC   :string;
    vBC     :string;
    pICMS   :string;
    vICMS   :string;
    pRedBC  :string;
    modBCST :string;
    vBCST   :string;
    pICMSST :string;
    vICMSST :string;


    constructor create ();
    destructor free ();

    procedure limpaCampos ();

end;

type
  TIPITrib = class

  private


  public

    CST     :string;
    vBC     :string;
    pIPI    :string;
    vIPI    :string;


    constructor create ();
    destructor free ();

    procedure limpaCampos ();

end;

type
  TIPI = class
  private
  public
    cEnq    :string;
    IPITrib :TIPITrib;
    constructor create ();
    destructor free ();
    procedure limpaCampos ();
end;

type
  TII = class
  private
  public
    vBC :string;
    vDespAdu :string;
    vII :string;
    vIOF :string;
    constructor create();
    destructor free();
    procedure limpaCampos();
end;

type
  TPIS = class

  private


  public

    CST       :string;
    vBC       :string;
    pPIS      :string;
    vPIS      :string;
    qBCProd   :string;
    vAliqProd :string;

    constructor create ();
    destructor free ();

    procedure limpaCampos ();

end;


type
  TCOFINS = class

  private


  public

    CST       :string;
    vBC       :string;
    pCOFINS   :string;
    vCOFINS   :string;
    vBCProd   :string;
    qBCProd   :string;
    vAliqProd :string;

    constructor create ();
    destructor free ();

     procedure limpaCampos ();

end;

type
  Tprodutos = class

  private


  public

    {prod}
    cProd   :string;
    xProd   :string;
    NCM     :string;
    CFOP    :string;
    uCom    :string;
    qCom    :string;
    vUnCom  :string;
    vProd   :string;
    uTrib   :string;
    qTrib   :string;
    vUnTrib :string;
    vDesc   :string;
    vFrete  :string;
    vSeg    :string;
    vOutro  :string;

    ICMS    : TICMS;
    IPI     : TIPI;
    II      : TII;
    PIS     : TPIS;
    COFINS  : TCOFINS;

    constructor create ();
    destructor free ();

    procedure limpaCampos ();

end;

type
    TIDE = class

    private

    public

     cUF     :string;
     cNF     :string;
     natOp   :string;
     indPag  :string;
     modelo  :string;
     serie   :string;
     nNF     :string;
     dEmi    :string;
     dSaiEnt :string;
     hSaiEnt :string;
     dhEmi   :string;
     dhSaiEnt:string;
     tpNF    :string;
     cMunFG  :string;
     tpImp   :string;
     tpEmis  :string;
     cDV     :string;
     tpAmb   :string;
     finNFe  :string;
     procEmi :string;
     verProc :string;

     constructor create ();
     destructor free ();

     procedure limpaCampos ();

end;

type
    TenderEmit = class

    private

    public

      xLgr    :string;
      nro     :string;
      xBairro :string;
      cMun    :string;
      xMun    :string;
      UF      :string;
      CEP     :string;
      cPais   :string;
      xPais   :string;
      fone    :string;

      constructor create ();
      destructor free ();

      procedure limpaCampos ();

end;

type
    TEmit = class
    private
    public
      CNPJ  :string;
      xNome :string;
      xFant :string;
      IE    :string;
      CRT   :string;

      enderEmit : TenderEmit;

      constructor create ();
      destructor free ();
      procedure limpaCampos ();
end;

type
    TenderDest = class

    private

    public

      xLgr    :string;
      nro     :string;
      xBairro :string;
      cMun    :string;
      xMun    :string;
      UF      :string;
      CEP     :string;
      cPais   :string;
      xPais   :string;
      fone    :string;

      constructor create ();
      destructor free ();

      procedure limpaCampos ();

end;

type
    TDest = class
    private
    public
      CNPJ  :string;
      CPF   :string;
      xNome :string;
      IE    :string;
      email :string;
      enderDest : TenderDest;

      constructor create ();
      destructor free ();
      procedure limpaCampos ();
end;

type
    TICMSTot = class

    private

    public

      vBC     :string;
      vICMS   :string;
      vICMSST :string;
      vBCST   :string;
      vST     :string;
      vProd   :string;
      vFrete  :string;
      vSeg    :string;
      vDesc   :string;
      vII     :string;
      vIPI    :string;
      vPIS    :string;
      vCOFINS :string;
      vOutro  :string;
      vNF     :string;

      constructor create ();
      destructor free ();

      procedure limpaCampos ();


end;

type
    TVeicTransp = class

    private

    public

       placa :string;
       UF    :string;

      constructor create ();
      destructor free ();

      procedure limpaCampos ();


end;


type
    TTransporta = class

    private

    public

      CNPJ    :string;
      xNome   :string;
      IE      :string;
      xEnder  :string;
      xMun    :string;
      UF      :string;

      veicTransp:TVeicTransp;

      constructor create ();
      destructor free ();

      procedure limpaCampos ();


end;

type
  TVolumeTransportado = class

  private

  public

    qVol:string;
    nVol:string;
    esp:string;
    marca:string;
    pesoL:string;
    pesoB:string;

    constructor create();
    destructor  free();

    procedure limpaCampos();

end;


type
    TTransp = class

    private

    public

      modFrete  :string;
      Transportadora:TTransporta;
      Vol:TVolumeTransportado;

      constructor create ();
      destructor free ();

      procedure limpaCampos ();


end;




type
    TobjExtraiXML = class

    private

      caminhoArquivo:string;

      contTipoICMS:Integer;

      XMLDocument:TXMLDocument;

      totalProdutos:Integer;

      procedure get_tagFinal (var tipo:string);

      function copia(str:string; inicio:string; fim:string):string;

      function get_tagDoICMS   (no :IXMLNode):IXMLNode;
      function get_tagDoIPI    (no :IXMLNode):IXMLNode;
      function get_tagDoII     (no :IXMLNode):IXMLNode;
      function get_tagDoIPITrib(no :IXMLNode):IXMLNode;
      function get_tagdoPIS    (no :IXMLNode):IXMLNode;
      function get_tagDoCOFINS (no :IXMLNode):IXMLNode;

      procedure retornaProdutos                  ();
      procedure retornaIde                       ();

      procedure retornaEmit                      ();
      procedure retornaEnderEmit                 ();
      procedure retornaDest                      ();
      procedure retornaEnderDest                 ();


      procedure retornaICMSTot                   ();
      procedure retornaTransp                    ();
      procedure retornaTransporta                ();
      procedure retornaVeicTransp                ();
      procedure retornaDadosVolumesTransportados ();



    public

      produtos: array of Tprodutos;
      IDE     :TIDE;
      Emit    :TEmit;
      Dest    :TDest;
      ICMSTot :TICMSTot;
      transp  :TTransp;

      constructor create ();
      destructor  free   ();

      procedure submit_caminhoArquivo (caminhoArquivo:string);
      procedure submit_XMLDocument (objeto:TXMLDocument);

      function get_caminhoArquivo():string;
      function get_totalprodutos ():Integer;
      function abreArquivo  ():Boolean;
      function retornaCampo    (tipo:string;campo:string):string;


      procedure retornaDados    ();
      procedure teste ();
      procedure limpaCampos ();

end;

implementation

uses Dialogs, Types,UComponentesNfe, UessencialGlobal;

function TobjExtraiXML.abreArquivo: Boolean;
var
  arq:TextFile;
  auxInfo:string;
  i,cont:integer;
  arquivo:TStringList;
begin





end;

function TobjExtraiXML.copia(str:string; inicio:string; fim: string): string;
var
i:Integer;
begin

    Result := '';

    i:=Pos(str,inicio);

    i:= i+length(inicio);

    while (str[i] <> fim) and (i<=Length(str)) do
    begin

      Result := Result + str[i];
      i:=i+1;

    end;

end;

constructor TobjExtraiXML.create;
begin

  self.caminhoArquivo := '';

  self.IDE     := TIDE.create  ();
  self.Emit    := TEmit.create ();
  self.Dest    := TDest.create();
  self.ICMSTot := TICMSTot.create ();
  self.transp  := TTransp.create ();

end;


destructor TobjExtraiXML.free;
var
  i:integer;
begin

  self.IDE.free  ();
  self.Emit.free ();
  self.Dest.free ();
  self.ICMSTot.free ();
  self.transp.free ();

  for i := 0  to totalProdutos-1  do
     self.produtos[i].free;

end;


procedure TobjExtraiXML.get_tagFinal(var tipo: string);
begin

  Insert ('/',tipo,2);

end;

function TobjExtraiXML.get_totalprodutos: Integer;
var
  noIDE:IXMLNode;
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
  totalProdutos:Integer;
begin

  totalProdutos:=0;

  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai := self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('det');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('det');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;



  repeat

    noTemp := noSec.ChildNodes['prod'];

    noTemp.ChildNodes.First;

    if ( Trim (noTemp.ChildNodes['cProd'].Text) <> '') then
    begin

      totalProdutos := totalProdutos + 1 ;

    end;


    NoSec := NoSec.NextSibling;

  until (noSec = nil);

  self.totalProdutos:=totalProdutos;
  result := totalProdutos;


end;


function TobjExtraiXML.retornaCampo(tipo, campo: string): string;
var
  str:string;
  i:Integer;
begin

  Result := '';
  str:=RetornaValorCampos (Self.caminhoArquivo,tipo);

  if (Trim (str) = '') then
  begin
    ShowMessage ('Tipo: '+tipo+' n�o encontrado');
    Exit;
  end;

  i:=Pos(campo,str);

  if (i = 0) then
  begin
    ShowMessage ('Campo: '+campo+' n�o encontrado');
    Exit;
  end;

  i:=i+length(campo);

  while (str[i] <> '<') do
  begin

     result := Result + str[i];
     i:=i+1;

  end;

end;


procedure TobjExtraiXML.submit_caminhoArquivo(caminhoArquivo: string);
begin

  self.caminhoArquivo := caminhoArquivo;

end;

procedure TobjExtraiXML.submit_XMLDocument(objeto: TXMLDocument);
begin

  self.XMLDocument:=objeto;

end;

procedure TobjExtraiXML.retornaProdutos();
var
  noIDE:IXMLNode;
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noICMS:IXMLNode;
  noIPI:IXMLNode;
  noII:IXMLNode;
  noIPITrib:IXMLNode;
  noPIS :IXMLNode;
  noCOFINS :IXMLNode;
  noFilho:IXMLNode;
  contProd:integer;
  cont:Integer;
  i:Integer;
begin


  contProd := self.get_totalprodutos ();

  SetLength (self.produtos,contProd);


  for i := 0  to contProd-1  do
  begin

    self.produtos[i]:=Tprodutos.create ();

  end;

  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('det');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('det');
  end;


  noSec := noFilho;
  noSec.ChildNodes.First;

  cont:=-1;

  repeat

    noTemp   := noSec.ChildNodes['prod'];

    noICMS   := get_tagDoICMS   (noSec);
    noIPI    := get_tagDoIPI    (noSec);
    noII     := get_tagDoII     (noSec);
    noIPITrib:= get_tagDoIPITrib(noSec);
    noPIS    := get_tagDoPIS    (noSec);
    noCOFINS := get_tagDoCOFINS (noSec);


    if (noTemp <> nil) then
      noTemp.ChildNodes.First;

    if (noICMS <> nil) then
      noICMS.ChildNodes.First;

    //if (noIPI <> nil) then
      //noIPI.ChildNodes.First;

    if (noPIS <> nil) then
      noPIS.ChildNodes.First;

    if (noCOFINS <> nil) then
      noCOFINS.ChildNodes.First;

    if ( Trim (noTemp.ChildNodes['cProd'].Text) <> '') then
    begin

      cont := cont + 1 ;

      {produtos}
      self.produtos[cont].cProd   := noTemp.ChildNodes['cProd'].Text;
      self.produtos[cont].xProd   := noTemp.ChildNodes['xProd'].Text;
      self.produtos[cont].NCM     := noTemp.ChildNodes['NCM'].Text;
      self.produtos[cont].CFOP    := noTemp.ChildNodes['CFOP'].Text;
      self.produtos[cont].uCom    := noTemp.ChildNodes['uCom'].Text;
      self.produtos[cont].qCom    := noTemp.ChildNodes['qCom'].Text;
      self.produtos[cont].vUnCom  := noTemp.ChildNodes['vUnCom'].Text;
      self.produtos[cont].vProd   := noTemp.ChildNodes['vProd'].Text;
      self.produtos[cont].uTrib   := noTemp.ChildNodes['uTrib'].Text;
      self.produtos[cont].qTrib   := noTemp.ChildNodes['qTrib'].Text;
      self.produtos[cont].vUnTrib := noTemp.ChildNodes['vUnTrib'].Text;
      self.produtos[cont].vDesc   := noTemp.ChildNodes['vDesc'].Text;
      self.produtos[cont].vFrete  := noTemp.ChildNodes['vFrete'].Text;
      self.produtos[cont].vSeg    := noTemp.ChildNodes['vSeg'].Text;
      self.produtos[cont].vOutro  := noTemp.ChildNodes['vOutro'].Text;

      {ICMS normal}
      if Assigned(noICMS) then
      begin
        self.produtos[cont].ICMS.orig    := noICMS.ChildNodes['orig'].Text;
        self.produtos[cont].ICMS.CST     := noICMS.ChildNodes['CST'].Text;
        self.produtos[cont].ICMS.CSOSN   := noICMS.ChildNodes['CSOSN'].Text;
        self.produtos[cont].ICMS.modBC   := noICMS.ChildNodes['modBC'].Text;
        self.produtos[cont].ICMS.vBC     := noICMS.ChildNodes['vBC'].Text;
        self.produtos[cont].ICMS.pICMS   := noICMS.ChildNodes['pICMS'].Text;
        self.produtos[cont].ICMS.vICMS   := noICMS.ChildNodes['vICMS'].Text;
        self.produtos[cont].ICMS.pRedBC  := noICMS.ChildNodes['pRedBC'].Text;

        {ICMS ST}
        self.produtos[cont].ICMS.modBCST := noICMS.ChildNodes['modBCST'].Text;
        self.produtos[cont].ICMS.vBCST   := noICMS.ChildNodes['vBCST'].Text;
        self.produtos[cont].ICMS.pICMSST := noICMS.ChildNodes['pICMSST'].Text;
        self.produtos[cont].ICMS.vICMSST := noICMS.ChildNodes['vICMSST'].Text;

      end;

      {IPI}
      if (noIPI <> nil) then
      begin

        self.produtos[cont].IPI.cEnq := noIPI.ChildNodes['cEnq'].Text;

        self.produtos[cont].IPI.IPITrib.CST  := noIPITrib.ChildNodes['CST'].Text;
        self.produtos[cont].IPI.IPITrib.vBC  := noIPITrib.ChildNodes['vBC'].Text;
        self.produtos[cont].IPI.IPITrib.pIPI := noIPITrib.ChildNodes['pIPI'].Text;
        self.produtos[cont].IPI.IPITrib.vIPI := noIPITrib.ChildNodes['vIPI'].Text;

      end;

      {II - imposto de importa��o}
      if (noII <> nil) then
      begin
        self.produtos[cont].II.vBC      := noII.ChildNodes['vBC'].Text;
        self.produtos[cont].II.vDespAdu := noII.ChildNodes['vDespAdu'].Text;
        self.produtos[cont].II.vII      := noII.ChildNodes['vII'].Text;
        self.produtos[cont].II.vIOF     := noII.ChildNodes['vIOF'].Text;
      end;

      {PIS}
      self.produtos[cont].PIS.CST       := noPIS.ChildNodes['CST'].Text;
      self.produtos[cont].PIS.vBC       := noPIS.ChildNodes['vBC'].Text;
      self.produtos[cont].PIS.pPIS      := noPIS.ChildNodes['pPIS'].Text;
      self.produtos[cont].PIS.vPIS      := noPIS.ChildNodes['vPIS'].Text;
      self.produtos[cont].PIS.qBCProd   := noPIS.ChildNodes['qBCProd'].Text;
      self.produtos[cont].PIS.vAliqProd := noPIS.ChildNodes['vAliqProd'].Text;

      {COFINS}
      self.produtos[cont].COFINS.CST       := noCOFINS.ChildNodes['CST'].Text;
      self.produtos[cont].COFINS.vBC       := noCOFINS.ChildNodes['vBC'].Text;
      self.produtos[cont].COFINS.pCOFINS   := noCOFINS.ChildNodes['pCOFINS'].Text;
      self.produtos[cont].COFINS.vCOFINS   := noCOFINS.ChildNodes['vCOFINS'].Text;
      self.produtos[cont].COFINS.vBCProd   := noCOFINS.ChildNodes['vBCProd'].Text;
      self.produtos[cont].COFINS.qBCProd   := noCOFINS.ChildNodes['qBCProd'].Text;
      self.produtos[cont].COFINS.vAliqProd := noCOFINS.ChildNodes['vAliqProd'].Text;

    end;


    NoSec := NoSec.NextSibling;

  until (noSec = nil);

end;



{ Tprodutos }

constructor Tprodutos.create;
begin
  self.ICMS   := TICMS.create   ();
  self.IPI    := TIPI.create    ();
  self.II     := TII.create     ();
  self.PIS    := TPIS.create    ();
  Self.COFINS := TCOFINS.create ();
end;

destructor Tprodutos.free;
begin
  self.ICMS.free;
  self.IPI.free;
  Self.II.free;
  self.PIS.free;
  self.COFINS.free;
end;

procedure TobjExtraiXML.teste;
var
  noIDE:IXMLNode;
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
  contProd:integer;
  cont:Integer;
  i:Integer;
begin

  contProd := self.get_totalprodutos ();

  SetLength (self.produtos,contProd);

  for i := 0  to contProd-1  do
  begin

    self.produtos[i]:=Tprodutos.create ();

  end;



  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('det');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('det');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  cont:=-1;

  repeat


    //noTemp := noSec.ChildNodes['imposto'].ChildNodes['ICMS'].ChildNodes['ICMS40'];
    noTemp := noSec.ChildNodes['imposto'].ChildNodes['ICMS'].ChildNodes.First;

    if (notemp <> nil) then
    begin

        noTemp.ChildNodes.First;

        if ( Trim (noTemp.ChildNodes['orig'].Text) <> '') then
        begin

          cont := cont + 1 ;
          ShowMessage (noTemp.ChildNodes['orig'].Text);
          ShowMessage (noTemp.ChildNodes['CST'].Text);

        end;

    end;


    NoSec := NoSec.NextSibling;

  until (noSec = nil);



end;

{ TICMS00 }

constructor TICMS.create;
begin

end;

destructor TICMS.free;
begin

end;

function TobjExtraiXML.get_tagDoICMS(no: IXMLNode): IXMLNode;
begin

   Result := no.ChildNodes['imposto'].ChildNodes['ICMS'].ChildNodes.First;

end;

function TobjExtraiXML.get_tagDoIPI(no: IXMLNode): IXMLNode;
begin
  Result := no.ChildNodes['imposto'].ChildNodes['IPI'];
end;

function TobjExtraiXML.get_tagDoII(no :IXMLNode):IXMLNode;
begin
  result := no.ChildNodes['imposto'].ChildNodes['II'];
end;

function TobjExtraiXML.get_tagDoIPITrib(no: IXMLNode): IXMLNode;
begin

  Result := no.ChildNodes['imposto'].ChildNodes['IPI'].ChildNodes['IPITrib'];

end;

procedure Tprodutos.limpaCampos;
begin

    cProd:='';
    xProd:='';
    NCM:='';
    CFOP:='';
    uCom:='';
    qCom:='';
    vUnCom:='';
    vProd:='';
    uTrib:='';
    qTrib:='';
    vUnTrib:='';
    vDesc:='';
    vFrete:='';
    vSeg:='';
    vOutro:='';

    self.ICMS.limpaCampos   ();
    self.IPI.limpaCampos    ();
    self.II.limpaCampos     ();
    self.PIS.limpaCampos    ();
    self.COFINS.limpaCampos ();

end;

{ TPIS }

constructor TPIS.create;
begin

end;

destructor TPIS.free;
begin

end;

function TobjExtraiXML.get_tagdoPIS(no: IXMLNode): IXMLNode;
begin

  Result := no.ChildNodes['imposto'].ChildNodes['PIS'].ChildNodes.First;

end;

procedure TPIS.limpaCampos;
begin

    CST:='';
    vBC:='';
    pPIS:='';
    vPIS:='';
    qBCProd:='';
    vAliqProd:='';

end;

{ TCOFINS }

constructor TCOFINS.create;
begin

end;

destructor TCOFINS.free;
begin

end;

function TobjExtraiXML.get_tagDoCOFINS(no: IXMLNode): IXMLNode;
begin

   Result := no.ChildNodes['imposto'].ChildNodes['COFINS'].ChildNodes.First;

end;


procedure TobjExtraiXML.retornaIde;
var
  noIDE:IXMLNode;
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
  contProd:integer;
  cont:Integer;
  i:Integer;
begin


  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai := self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.ChildNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('ide');
  end
  else
  begin

    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('ide');

  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  cont:=-1;

  repeat

    noTemp   := noSec;


    if (noTemp <> nil) then
      noTemp.ChildNodes.First;


    if ( Trim (noTemp.ChildNodes['cUF'].Text) <> '') then
    begin

      self.IDE.cUF      := noTemp.ChildNodes['cUF'].Text;
      self.IDE.cNF      := noTemp.ChildNodes['cNF'].Text;
      self.IDE.natOp    := noTemp.ChildNodes['natOp'].Text;
      self.IDE.indPag   := noTemp.ChildNodes['indPag'].Text;
      self.IDE.modelo   := noTemp.ChildNodes['mod'].Text;
      self.IDE.serie    := noTemp.ChildNodes['serie'].Text;
      self.IDE.nNF      := noTemp.ChildNodes['nNF'].Text;
      self.IDE.dEmi     := noTemp.ChildNodes['dEmi'].Text;
      self.IDE.dSaiEnt  := noTemp.ChildNodes['dSaiEnt'].Text;
      self.ide.hSaiEnt  := noTemp.ChildNodes['hSaiEnt'].Text;
      self.IDE.dhEmi    := noTemp.ChildNodes['dhEmi'].Text;
      self.IDE.dhSaiEnt := noTemp.ChildNodes['dhSaiEnt'].Text;
      self.IDE.tpNF     := noTemp.ChildNodes['tpNF'].Text;
      self.IDE.cMunFG   := noTemp.ChildNodes['cMunFG'].Text;
      self.IDE.tpImp    := noTemp.ChildNodes['tpImp'].Text;
      self.IDE.tpEmis   := noTemp.ChildNodes['tpEmis'].Text;
      self.IDE.cDV      := noTemp.ChildNodes['cDV'].Text;
      self.IDE.tpAmb    := noTemp.ChildNodes['tpAmb'].Text;
      self.IDE.finNFe   := noTemp.ChildNodes['finNFe'].Text;
      self.IDE.procEmi  := noTemp.ChildNodes['procEmi'].Text;
      self.IDE.verProc  := noTemp.ChildNodes['verProc'].Text;

    end;


    NoSec := NoSec.NextSibling;

  until (noSec = nil);



end;
procedure TCOFINS.limpaCampos;
begin

    CST:='';
    vBC:='';
    pCOFINS:='';
    vCOFINS:='';
    vBCProd:='';
    qBCProd:='';
    vAliqProd:='';

end;

{ TIDE }

constructor TIDE.create;
begin

end;

destructor TIDE.free;
begin

end;


procedure TobjExtraiXML.retornaDados;
begin


  self.retornaIde ();
  self.retornaProdutos ();

  self.retornaEmit ();
  self.retornaEnderEmit ();
  self.retornaDest ();
  self.retornaEnderDest ();
  self.retornaICMSTot ();
  self.retornaTransp ();
  self.retornaTransporta();
  self.retornaVeicTransp();
  self.retornaDadosVolumesTransportados();


end;

procedure TIDE.limpaCampos;
begin

  self.cUF:='';
  self.cNF:='';
  self.natOp:='';
  self.indPag:='';
  self.modelo:='';
  self.serie:='';
  self.nNF:='';
  self.dEmi:='';
  self.dSaiEnt:='';
  self.hSaiEnt:='';
  self.tpNF:='';
  self.cMunFG:='';
  self.tpImp:='';
  self.tpEmis:='';
  self.cDV:='';
  self.tpAmb:='';
  self.finNFe:='';
  Self.procEmi:='';
  self.verProc:='';
  self.dhEmi:='';
  self.dhSaiEnt:='';

end;

{ TEmit }

constructor TEmit.create;
begin

  Self.enderEmit := TenderEmit.create ();

end;

destructor TEmit.free;
begin

  self.enderEmit.free ();

end;

procedure TEmit.limpaCampos;
begin

  self.CNPJ:='';
  self.xNome:='';
  Self.xFant:='';
  self.IE   :='';
  self.CRT  :='';
  self.enderEmit.limpaCampos ();

end;

{ TenderEmit }

constructor TenderEmit.create;
begin

end;

destructor TenderEmit.free;
begin

end;

procedure TobjExtraiXML.retornaEmit;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin


  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('emit');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('emit');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;


  noTemp   := noSec;


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;


  if ( Trim (noTemp.ChildNodes['xNome'].Text) <> '') then
  begin
    self.Emit.CNPJ  := noTemp.ChildNodes['CNPJ'].Text;
    self.Emit.xNome := noTemp.ChildNodes['xNome'].Text;
    self.Emit.xFant := noTemp.ChildNodes['xFant'].Text;
    self.Emit.IE    := noTemp.ChildNodes['IE'].Text;
    self.Emit.CRT   := noTemp.ChildNodes['CRT'].Text;
  end;


end;

procedure TobjExtraiXML.retornaEnderEmit;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin


  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai := self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('emit');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('emit');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  noTemp   := noSec.ChildNodes['enderEmit'];


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;


  if ( Trim (noTemp.ChildNodes['cPais'].Text) <> '') then
  begin

    self.Emit.enderEmit.xLgr    :=  noTemp.ChildNodes['xLgr'].Text;
    self.Emit.enderEmit.nro     :=  noTemp.ChildNodes['nro'].Text;
    self.Emit.enderEmit.xBairro :=  noTemp.ChildNodes['xBairro'].Text;
    self.Emit.enderEmit.cMun    :=  noTemp.ChildNodes['cMun'].Text;
    self.Emit.enderEmit.xMun    :=  noTemp.ChildNodes['xMun'].Text;
    self.Emit.enderEmit.UF      :=  noTemp.ChildNodes['UF'].Text;
    self.Emit.enderEmit.CEP     :=  noTemp.ChildNodes['CEP'].Text;
    self.Emit.enderEmit.cPais   :=  noTemp.ChildNodes['cPais'].Text;
    self.Emit.enderEmit.xPais   :=  noTemp.ChildNodes['xPais'].Text;
    self.Emit.enderEmit.fone    :=  noTemp.ChildNodes['fone'].Text;

  end;



end;

procedure TobjExtraiXML.retornaICMSTot;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin


  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('total');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('total');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  noTemp   := noSec.ChildNodes['ICMSTot'];


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;


  if ( Trim (noTemp.ChildNodes['vNF'].Text) <> '') then
  begin

    self.ICMSTot.vBC     := noTemp.ChildNodes['vBC'].Text;
    self.ICMSTot.vICMS   := noTemp.ChildNodes['vICMS'].Text;
    self.ICMSTot.vICMSST := noTemp.ChildNodes['vICMSST'].Text;
    self.ICMSTot.vBCST   := noTemp.ChildNodes['vBCST'].Text;
    self.ICMSTot.vST     := noTemp.ChildNodes['vST'].Text;
    self.ICMSTot.vProd   := noTemp.ChildNodes['vProd'].Text;
    self.ICMSTot.vFrete  := noTemp.ChildNodes['vFrete'].Text;
    self.ICMSTot.vSeg    := noTemp.ChildNodes['vSeg'].Text;
    self.ICMSTot.vDesc   := noTemp.ChildNodes['vDesc'].Text;
    self.ICMSTot.vII     := noTemp.ChildNodes['vII'].Text;
    self.ICMSTot.vIPI    := noTemp.ChildNodes['vIPI'].Text;
    self.ICMSTot.vII     := notemp.ChildNodes['vII'].Text;
    self.ICMSTot.vPIS    := noTemp.ChildNodes['vPIS'].Text;
    self.ICMSTot.vCOFINS := noTemp.ChildNodes['vCOFINS'].Text;
    self.ICMSTot.vOutro  := noTemp.ChildNodes['vOutro'].Text;
    self.ICMSTot.vNF     := noTemp.ChildNodes['vNF'].Text;

  end;


end;

procedure TenderEmit.limpaCampos;
begin

  self.xLgr:='';
  self.nro:='';
  self.xBairro:='';
  self.cMun:='';
  self.xMun:='';
  self.UF:='';
  self.CEP:='';
  self.cPais:='';
  self.xPais:='';
  self.fone:='';

end;

{ TICMSTot }

constructor TICMSTot.create;
begin

end;

destructor TICMSTot.free;
begin

end;

procedure TobjExtraiXML.limpaCampos;
var
  i:Integer;
begin

  self.IDE.limpaCampos  ();
  self.Emit.limpaCampos ();
  self.ICMSTot.limpaCampos ();

  for i:=0  to self.totalProdutos-1  do
      self.produtos[i].limpaCampos ();

end;

procedure TICMSTot.limpaCampos;
begin

    vBC     :='';
    vICMS   :='';
    vICMSST :='';
    vBCST   :='';
    vST     :='';
    vProd   :='';
    vFrete  :='';
    vSeg    :='';
    vDesc   :='';         
    vII     :='';
    vIPI    :='';
    vII     :='';
    vPIS    :='';
    vCOFINS :='';
    vOutro  :='';
    vNF     :='';

end;

procedure TICMS.limpaCampos;
begin

    self.orig:='';
    self.CST:='';
    self.CSOSN:='';
    self.modBC:='';
    self.vBC:='';
    self.pICMS:='';
    self.vICMS:='';
    self.pRedBC:='';
    self.modBCST:='';
    self.vBCST   := '';
    self.pICMSST :='';
    self.vICMSST :='';

end;

{ TTransp }

constructor TTransp.create;
begin

  self.Transportadora := TTransporta.create();
  self.Vol:=TVolumeTransportado.create;

end;

destructor TTransp.free;
begin

  self.Transportadora.free;
  self.Vol.free;

end;

procedure TTransp.limpaCampos;
begin

  self.modFrete := '';
  self.Transportadora.limpaCampos ();

end;

{ TTransporta }

constructor TTransporta.create;
begin

  self.veicTransp := TVeicTransp.create;

end;

destructor TTransporta.free;
begin

  self.veicTransp.free;

end;

procedure TTransporta.limpaCampos;
begin

  self.CNPJ    :='';
  self.xNome   :='';
  self.IE      :='';
  self.xEnder  :='';
  self.xMun    :='';
  self.UF      :='';
  self.veicTransp.limpaCampos ();

end;

{ TVeicTransp }

constructor TVeicTransp.create;
begin

end;

destructor TVeicTransp.free;
begin

end;

procedure TVeicTransp.limpaCampos;
begin

  Self.placa := '';
  self.UF    :='';

end;

{-}

procedure TobjExtraiXML.retornaTransp;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin


  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;


  noTemp   := noSec;


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;


  self.transp.modFrete := noTemp.ChildNodes['modFrete'].Text


end;

procedure TobjExtraiXML.retornaTransporta;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin

  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  noTemp   := noSec.ChildNodes['transporta'];


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;

    self.transp.Transportadora.CNPJ    :=  noTemp.ChildNodes['CNPJ'].Text;
    self.transp.Transportadora.xNome   :=  noTemp.ChildNodes['xNome'].Text;
    self.transp.Transportadora.IE      :=  noTemp.ChildNodes['IE'].Text;
    self.transp.Transportadora.xEnder  :=  noTemp.ChildNodes['xEnder'].Text;
    self.transp.Transportadora.xMun    :=  noTemp.ChildNodes['xMun'].Text;
    self.transp.Transportadora.UF      :=  noTemp.ChildNodes['UF'].Text;

end;

procedure TobjExtraiXML.retornaVeicTransp;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin

  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  noTemp   := noSec.ChildNodes['veicTransp'];


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;

    self.transp.Transportadora.veicTransp.placa := noTemp.ChildNodes['placa'].Text;
    self.transp.Transportadora.veicTransp.UF    := noTemp.ChildNodes['UF'].Text;


end;

{ TVolumeTransportado }

constructor TVolumeTransportado.create;
begin

end;

destructor TVolumeTransportado.free;
begin

end;

procedure TVolumeTransportado.limpaCampos;
begin

  self.qVol:='';
  self.nVol:='';
  self.esp:='';
  self.marca:='';
  self.pesoL:='';
  self.pesoB:='';

end;

procedure TobjExtraiXML.retornaDadosVolumesTransportados;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin

  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('transp');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  noTemp   := noSec.ChildNodes['vol'];


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;

    self.transp.Vol.qVol    := noTemp.ChildNodes['qVol'].Text;
    self.transp.Vol.nVol    := noTemp.ChildNodes['nVol'].Text;
    self.transp.Vol.esp     := noTemp.ChildNodes['esp'].Text;
    self.transp.Vol.marca   := noTemp.ChildNodes['marca'].Text;
    self.transp.Vol.pesoL   := noTemp.ChildNodes['pesoL'].Text;
    self.transp.Vol.pesoB   := noTemp.ChildNodes['pesoB'].Text;


end;

{ TIPI }

constructor TIPI.create;
begin

  self.IPITrib:=TIPITrib.create;

end;

destructor TIPI.free;
begin

  self.IPITrib.free;

end;

procedure TIPI.limpaCampos;
begin

  self.cEnq    := '';
  self.IPITrib.limpaCampos();

end;



{ TIPITrib }

constructor TIPITrib.create;
begin

end;

destructor TIPITrib.free;
begin

end;

procedure TIPITrib.limpaCampos;
begin

  self.CST := '';
  self.vBC := '';
  self.pIPI:= '';
  Self.vIPI:= '';

end;

{ TII }

constructor TII.create;
begin

end;

destructor TII.free;
begin

end;

procedure TII.limpaCampos;
begin
  self.vBC      := '';
  self.vDespAdu := '';
  self.vII      := '';
  self.vIOF     := '';
end;

{ TDest }

constructor TDest.create;
begin
  self.enderDest := TenderDest.create;
end;

destructor TDest.free;
begin
  self.enderDest.free;
end;

procedure TDest.limpaCampos;
begin
  self.CNPJ  := '';
  self.CPF   := '';
  self.xNome := '';
  self.IE    := '';
  self.email := '';
  self.enderDest.limpaCampos ();
end;

{ TenderDest }

constructor TenderDest.create;
begin

end;

destructor TenderDest.free;
begin

end;

procedure TenderDest.limpaCampos;
begin
  Self.xLgr    := '';
  self.nro     := '';
  self.xBairro := '';
  self.cMun    := '';
  self.xMun    := '';
  self.UF      := '';
  self.CEP     := '';
  self.cPais   := '';
  self.xPais   := '';
  Self.fone    := '';
end;

procedure TobjExtraiXML.retornaDest;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin


  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if (noPai = nil) then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('dest');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('dest');
  end;                         

  noSec := noFilho;

  noSec.ChildNodes.First;


  noTemp   := noSec;


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;


  if ( Trim (noTemp.ChildNodes['xNome'].Text) <> '') then
  begin
    self.Dest.CNPJ  := noTemp.ChildNodes['CNPJ'].Text;
    self.Dest.CPF   := noTemp.ChildNodes['CPF'].Text;
    self.Dest.xNome := noTemp.ChildNodes['xNome'].Text;
    self.Dest.IE    := noTemp.ChildNodes['IE'].Text;
    self.Dest.email := noTemp.ChildNodes['email'].Text;
  end;


end;

procedure TobjExtraiXML.retornaEnderDest;
var
  noPai:IXMLNode;
  noSec:IXMLNode;
  noTemp:IXMLNode;
  noFilho:IXMLNode;
begin


  self.XMLDocument.LoadFromFile (self.caminhoArquivo);

  noPai:= self.XMLDocument.DocumentElement.ChildNodes.FindNode('NFe');

  if noPai = nil then
  begin
    noPai:= self.XMLDocument.ChildNodes.FindNode('NFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.childNodes.First.ChildNodes.First.ChildNodes.FindNode('dest');
  end
  else
  begin
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.FindNode('infNFe');
    noFilho := self.XMLDocument.DocumentElement.childNodes.First.ChildNodes.First.ChildNodes.FindNode('dest');
  end;

  noSec := noFilho;

  noSec.ChildNodes.First;

  noTemp   := noSec.ChildNodes['enderDest'];


  if (noTemp <> nil) then
    noTemp.ChildNodes.First;


  if ( Trim (noTemp.ChildNodes['cPais'].Text) <> '') then
  begin

    self.Dest.enderDest.xLgr    :=  noTemp.ChildNodes['xLgr'].Text;
    self.Dest.enderDest.nro     :=  noTemp.ChildNodes['nro'].Text;
    self.Dest.enderDest.xBairro :=  noTemp.ChildNodes['xBairro'].Text;
    self.Dest.enderDest.cMun    :=  noTemp.ChildNodes['cMun'].Text;
    self.Dest.enderDest.xMun    :=  noTemp.ChildNodes['xMun'].Text;
    self.Dest.enderDest.UF      :=  noTemp.ChildNodes['UF'].Text;
    self.Dest.enderDest.CEP     :=  noTemp.ChildNodes['CEP'].Text;
    self.Dest.enderDest.cPais   :=  noTemp.ChildNodes['cPais'].Text;
    self.Dest.enderDest.xPais   :=  noTemp.ChildNodes['xPais'].Text;
    self.Dest.enderDest.fone    :=  noTemp.ChildNodes['fone'].Text;

  end;

end;

function TobjExtraiXML.get_caminhoArquivo: string;
begin
  Result := self.caminhoArquivo;
end;

end.
