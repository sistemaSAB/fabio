unit URelPedidoRdPrint;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, Db, RDprint, Printers;

type
  TFRelPedidoRdPrint = class(TForm)
    RDprint1: TRDprint;
    procedure RDprint1BeforeNewPage(Sender: TObject; Pagina: Integer);
    procedure RDprint1NewPage(Sender: TObject; Pagina: Integer);
    procedure FormCreate(Sender: TObject);

  private


    { Private declarations }

  public
    { Public declarations }
    linhalocal: integer; // variavel global para controle de salto de pagina...
    imprimecabecalho:boolean;
    negritocabecalho:boolean;
    Procedure ConfiguraImpressao;
    Procedure ImprimeLinha(Pinicio,Pquantidade:integer);overload;
    Procedure ImprimeLinha;overload;
    procedure VerificaLinha;
    procedure DesenhaLinha;
    procedure IncrementaLinha(PQuantidade: Integer);
  end;

var
  FRelPedidoRdPrint: TFRelPedidoRdPrint;

  cabecalho:String;
  ImprimeNumeroPagina:Boolean;
implementation

uses UDataModulo, Uessencialglobal;

{$R *.DFM}

procedure TFRelPedidoRdPrint.ConfiguraImpressao;
var
      imprimedataehoracabecalho:boolean;
begin
     Self.imprimecabecalho:=True;
     rdprint1.OpcoesPreview.PreviewZoom:=91;
     rdprint1.TitulodoRelatorio:='';
     rdprint1.CaptionSetup:='';
     //22/10/2012 celio. Ap�s utilizar o posicionamento landcape(paisagem), alguns relat�rios acabam sendo impressos nesta forma.
     //para isso estou setando as configura��es padr�o sempre que chama o configura iompressao
     rdprint1.Orientacao:= poPortrait;
     ImprimeNumeroPagina:=True;
     Self.linhalocal:=3;

     cabecalho:='';

     if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then cabecalho:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin
               cabecalho:=ObjParametroGlobal.get_valor;
     End;
     If(ObjParametroGlobal.ValidaParametro('DATA E HORA DA IMPRESSAO NO CABECALHO DOS RELATORIOS')=false)
     Then Begin
          Messagedlg('O Par�metro "DATA E HORA DA IMPRESSAO NO CABECALHO DOS RELATORIOS" n�o foi encontrado!',mterror,[mbok],0);
          Exit;
     End
     Else Begin
          If (ObjParametroGlobal.get_valor='SIM')
          Then  cabecalho:=cabecalho+'  - '+datetostr(now)+'-'+timetostr(now);
     End;

     If (ObjParametroGlobal.validaparametro('QUANTIDADE DE COLUNAS PAPEL PEDIDO')=FALSE)
     Then Begin
                rdprint1.TamanhoQteColunas:=96;
                Messagedlg('O Par�metro "QUANTIDADE DE COLUNAS PAPEL PEDIDO" n�o foi encontrado!'+#13+'Ser� usado 93 linhas',mterror,[mbok],0);
     End
     Else Begin
               TRY
               rdprint1.TamanhoQteColunas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE COLUNAS PAPEL PEDIDO"'+#13+'Ser� usado 96 colunas',mterror,[mbok],0);
                     rdprint1.TamanhoQteColunas:=96;
               END;
     End;

     if (ObjParametroGlobal.validaparametro('QUANTIDADE DE LINHAS PAPEL PEDIDO')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "QUANTIDADE DE LINHAS PAPEL PEDIDO" n�o foi encontrado!'+#13+'Ser� usado 33 linhas',mterror,[mbok],0);
               rdprint1.TamanhoQteLinhas:=33;
     End
     Else Begin
               TRY
                 rdprint1.TamanhoQteLinhas:=STRTOINT(ObjParametroGlobal.get_valor);
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "QUANTIDADE DE LINHAS PAPEL PEDIDO"'+#13+'Ser� usado 33 linhas',mterror,[mbok],0);
                     rdprint1.TamanhoQteLinhas:=33;
               END;
     End;

     if (ObjParametroGlobal.Validaparametro('PORTA DE IMPRESS�O')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "PORTA DE IMPRESS�O" n�o foi encontrado!'+#13+'Ser� usado "LPT1"',mterror,[mbok],0);
               rdprint1.PortaComunicacao:='LPT1';
     End
     Else Begin
               TRY
                 rdprint1.PortaComunicacao:=ObjParametroGlobal.get_valor;
               EXCEPT
                     Messagedlg('Valor inv�lido no par�metro "PORTA DE IMPRESS�O"'+#13+'Ser� usado  "LPT1"',mterror,[mbok],0);
                     rdprint1.PortaComunicacao:='LPT1';
               END;
     End;

     if (ObjParametroGlobal.Validaparametro('ACENTUA��O NOS RELAT�RIOS?')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "ACENTUA��O NOS RELAT�RIOS?" n�o foi encontrado!'+#13+'Ser� usado "N�O"',mterror,[mbok],0);
               rdprint1.Acentuacao:=SemAcento;
     End
     Else Begin
               IF (ObjParametroGlobal.get_valor='SIM')
               THEN rdprint1.Acentuacao:=Transliterate
               Else rdprint1.Acentuacao:=SemAcento;
     End;

     if (ObjParametroGlobal.Validaparametro('USA GERENCIADOR DE IMPRESS�O DO WINDOWS')=FALSE)
     Then Begin
               Messagedlg('O Par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS" n�o foi encontrado!'+#13+'Ser� usado "SIM"',mterror,[mbok],0);
               rdprint1.UsaGerenciadorImpr:=True;
     End
     Else Begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then Begin
                      rdprint1.UsaGerenciadorImpr:=True
               End
               Else Begin
                  If (ObjParametroGlobal.get_valor='N�O')
                  Then Begin
                            rdprint1.UsaGerenciadorImpr:=False;
                  End
                  Else Begin
                            Messagedlg('Valor inv�lido no par�metro "USA GERENCIADOR DE IMPRESS�O DO WINDOWS"'+#13+'Ser� usado  "SIM"',mterror,[mbok],0);
                            rdprint1.UsaGerenciadorImpr:=true;
                  End;
               End;
     End;


     negritocabecalho:=false;
     if (ObjParametroGlobal.Validaparametro('NEGRITO NO CABE�ALHO DO PEDIDO?')=True)
     Then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               then negritocabecalho:=true;
     End;
     
end;

procedure TFRelPedidoRdPrint.RDprint1BeforeNewPage(Sender: TObject;
  Pagina: Integer);
begin
     // Rodap�...
{     rdprint1.imp (64,01,'===============================================================================================');
     rdprint1.impf(65,01,'Deltress Inform�tica Ltda',[italico]);
     rdprint1.impf(65,65,'Demonstra��o RdPrint 3.0',[comp17]);
     rdprint1.imp (66, conta * 3,inttostr(conta));}
end;

procedure TFRelPedidoRdPrint.RDprint1NewPage(Sender: TObject;
  Pagina: Integer);
begin
     if (ImprimeCabecalho=True)
     Then Begin
           // Cabe�alho...
           rdprint1.imp (01,01,cabecalho);

           if (RDprint1.FonteTamanhoPadrao=s20cpp)//160 colunas
           Then rdprint1.impD(02,160,'P�gina: ' + formatfloat('000',pagina),[])
           Else
               if (RDprint1.FonteTamanhoPadrao=s17cpp)//130 colunas
               Then rdprint1.impD(02,130,'P�gina: ' + formatfloat('000',pagina),[])
               Else rdprint1.impf(02,82,'P�gina: ' + formatfloat('000',pagina),[]);
     End;
     
end;

procedure TFRelPedidoRdPrint.FormCreate(Sender: TObject);
begin
     Self.Imprimecabecalho:=True;
end;

procedure TFRelPedidoRdPrint.ImprimeLinha(pinicio,Pquantidade: integer);
begin
      if (Pquantidade=0) and (Pinicio=0)
      Then RDprint1.Imp(LinhaLocal,1,CompletaPalavra('_',Self.RDprint1.TamanhoQteColunas,'_'))
      Else RDprint1.Imp(LinhaLocal,pinicio,CompletaPalavra('_',pquantidade,'_'));
      inc(linhalocal,1);
end;

procedure TFRelPedidoRdPrint.ImprimeLinha;
begin
      Self.ImprimeLinha(0,0);
end;

procedure TFRelPedidoRdPrint.VerificaLinha;
begin
     if (Self.LinhaLocal>=(Self.rdprint1.TamanhoQteLinhas-3))
     Then Begin
               Self.LinhaLocal:=3;
               Self.rdprint1.Novapagina;
     End;
End;

procedure TFRelPedidoRdPrint.DesenhaLinha;
begin
      Self.VerificaLinha;
      RDprint1.Imp(LinhaLocal,1,CompletaPalavra('_',Self.RDprint1.TamanhoQteColunas,'_'));
      IncrementaLinha(1);
end;

procedure TFRelPedidoRdPrint.IncrementaLinha(PQuantidade: Integer);
begin
     inc(Self.LinhaLocal,PQuantidade);
end;

end.


