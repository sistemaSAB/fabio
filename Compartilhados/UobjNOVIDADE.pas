unit UobjNOVIDADE;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialLocal, Uessencialglobal,IBStoredProc;

Type
   TObjNOVIDADE=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                StrListNovidades :TStringList;

//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime;
                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Versao(parametro:string);
                Function Get_Versao:string;

                Procedure Submit_texto(parametro:STRing);
                Function Get_texto:STRing;

                Function VerificaNovidade:Boolean;
                procedure Visualizado(PCodigo: string);
                procedure DesVisualizar(PCodigo:string);
                function  PreencheStrlNovidade(PSomenteNovasUsuario:boolean): Boolean;
                function CheckNaoMostrar(pnovidade: string): Boolean;
         Private
               Objquery:Tibquery;
               Objquery2:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Versao:string;
               texto:string;
               ParametroPesquisa:TStringList;

               Function  VerificaBrancos:Boolean;
               Function  VerificaRelacionamentos:Boolean;
               Function  VerificaNumericos:Boolean;
               Function  VerificaData:Boolean;
               Function  VerificaFaixa:boolean;
               Procedure ObjetoparaTabela;



   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios
//USES IMPLEMENTATION
, UobjNOVIDADEUSUARIO;


{ TTabTitulo }


Function  TObjNOVIDADE.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Versao:=fieldbyname('Versao').asstring;
        Self.texto:=fieldbyname('texto').asstring;

        Self.texto:=StringReplace(self.texto,'@@13@@',#13,[rfReplaceAll]);
        Self.texto:=StringReplace(self.texto,'@@10@@',#10,[rfReplaceAll]);
        result:=True;
     End;
end;


Procedure TObjNOVIDADE.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Versao').AsString:=Self.Versao;
        ParamByName('texto').AsString:=Self.texto;
  End;
End;

//***********************************************************************

function TObjNOVIDADE.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjNOVIDADE.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Versao:='';
        texto:='';
     End;
end;

Function TObjNOVIDADE.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjNOVIDADE.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjNOVIDADE.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjNOVIDADE.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjNOVIDADE.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjNOVIDADE.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabNOVIDADE est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Versao,texto');
           SQL.ADD(' from  TabNovidade');
           SQL.ADD(' WHERE codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjNOVIDADE.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjNOVIDADE.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjNOVIDADE.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.Objquery2:=TIBQuery.create(nil);
        Self.Objquery2.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;

        Self.StrListNovidades:=TStringList.Create;
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabNovidade(Codigo,Versao,texto)');
                InsertSQL.add('values (:Codigo,:Versao,:texto)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabNovidade set Codigo=:Codigo,Versao=:Versao,texto=:texto');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabNovidade where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjNOVIDADE.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjNOVIDADE.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabNOVIDADE');
     Result:=Self.ParametroPesquisa;
end;

function TObjNOVIDADE.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de NOVIDADE ';
end;


function TObjNOVIDADE.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENNOVIDADE,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENNOVIDADE,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjNOVIDADE.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.Objquery2);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    FreeAndNil(StrListNovidades);

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjNOVIDADE.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjNOVIDADE.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjNovidade.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjNovidade.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjNOVIDADE.Imprime;
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJNOVIDADE';
          With RgOpcoes do
          Begin
               items.clear;
                // Addicionar os Items.Add
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;       // Chamada de Func�es
          end;
     end;
end;

function TObjNOVIDADE.VerificaNovidade: Boolean;
begin
    Result:=false;
    With Self.Objquery do
    Begin
        Close;
        Sql.Clear;
        Sql.Add('Select count(codigo) as conta from TabNovidade');
        Sql.Add('Where Codigo not in (Select Novidade from TabNovidadeUsuario');
        Sql.Add('                     Where   Usuario = '+ObjUsuarioGlobal.Get_CODIGO+')');
        Open;
        if (fieldbyname('conta').asinteger>0)
        Then result:=True;
        close;
    end;
end;


function TObjNOVIDADE.CheckNaoMostrar(pnovidade:string): Boolean;
begin
    Result:=False;
    With Self.Objquery2 do
    Begin
        Close;
        Sql.Clear;
        Sql.Add('Select TNU.Novidade from TabNovidadeUsuario TNU');
        Sql.Add('join tabnovidade on TNU.novidade=tabnovidade.codigo');
        Sql.Add('Where TNU.Usuario = '+ObjUsuarioGlobal.Get_CODIGO);
        Sql.Add('and   Tabnovidade.codigo='+pnovidade);
        Open;
        if (recordcount>0)
        Then result:=true;
        close;
    end;
end;



function TObjNOVIDADE.PreencheStrlNovidade(PSomenteNovasUsuario:boolean): Boolean;
begin
    Result:=false;
    Self.StrListNovidades.Clear;
    With Self.Objquery do
    Begin
        Close;
        Sql.Clear;
        if (PSomenteNovasUsuario=False)
        Then Sql.Add('Select Codigo from TabNovidade order by codigo')
        Else Begin
                  sql.add('Select codigo from tabnovidade where codigo not in (');
                  sql.add('select distinct(novidade) from tabnovidadeusuario ');
                  sql.add('where usuario='+ObjUsuarioGlobal.Get_CODIGO+')');
        End;


        Open;
        While not (eof) do
        Begin
             Self.StrListNovidades.Add(fieldbyname('Codigo').AsString);
             Next;
        end;
        Result:=true;
    end;
end;


procedure TObjNOVIDADE.Visualizado(PCodigo:string);
Var  ObjNovidadeUsuario:TObjNOVIDADEUSUARIO;
Begin
try
   try
      ObjNovidadeUsuario:=TObjNOVIDADEUSUARIO.Create;
   except
      MensagemErro('Erro ao tentar criar o Objeto Novidade Usuario');
      exit;
   end;

   if (ObjNovidadeUsuario.LocalizaCodigo(pcodigo,ObjUsuarioGlobal.Get_CODIGO)=True)
   Then exit;

   ObjNovidadeUsuario.ZerarTabela;
   ObjNovidadeUsuario.Status:=dsInsert;
   ObjNovidadeUsuario.Submit_Codigo(ObjNovidadeUsuario.Get_NovoCodigo);
   ObjNovidadeUsuario.Novidade.Submit_Codigo(PCodigo);
   ObjNovidadeUsuario.Usuario.Submit_CODIGO(ObjUsuarioGlobal.Get_CODIGO);
   if (ObjNovidadeUsuario.Salvar(true)=false)
   then Begin
           MensagemErro('Erro ao tentar salvar');
           exit;
   end;

finally
   ObjNovidadeUsuario.Free;
end;
end;

function TObjNOVIDADE.Get_Versao: string;
begin
    Result:=Self.Versao;
end;

procedure TObjNOVIDADE.Submit_Versao(parametro: string);
begin
    Self.Versao:=parametro;
end;


function TObjNOVIDADE.Get_texto: String;
begin
    Result:=Self.texto;
end;

procedure TObjNOVIDADE.Submit_texto(parametro: String);
begin
    Self.texto:=parametro;
end;

procedure TObjNOVIDADE.DesVisualizar(PCodigo: string);
Begin
     With Self.Objquery2 do
     Begin
          close;
          sql.clear;
          sql.add('Delete from TabNovidadeUsuario where novidade='+pcodigo+' and usuario='+ObjUsuarioGlobal.Get_CODIGO);
          ExecSQL;
          FDataModulo.IBTransaction.CommitRetaining;
   end;
end;

end.



