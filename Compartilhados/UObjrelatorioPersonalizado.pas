unit UobjRELATORIOPERSONALIZADO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJSUBMENURELPERSONALIZADO,uobjpermissoesuso
;
//USES_INTERFACE



Type
   TObjRELATORIOPERSONALIZADO=class
  private

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                SubMenu:TOBJSUBMENURELPERSONALIZADO;
                Permissao:TObjPermissoesUso;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;virtual;
                Destructor  Free;virtual;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;virtual;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;

                Procedure Submit_SqlPrincipal(parametro: string);
                Function Get_SqlPrincipal: string;
                Procedure Submit_SqlRepeticao(parametro: string);
                Function Get_SqlRepeticao: string;
                Procedure Submit_TabelaPrincipal(parametro: string);
                Function Get_TabelaPrincipal: string;

                Procedure Submit_UltimoSqlPrincipal(parametro: String);
                Function Get_UltimoSqlPrincipal: String;


                Procedure Submit_orderbypersonalizado(parametro: string);
                Function Get_orderbypersonalizado: string;


                Procedure Submit_TabelaRepeticao(parametro: string);
                Function Get_TabelaRepeticao: string;

                Procedure Submit_UltimoSqlrepeticao(parametro: String);
                Function Get_UltimoSqlrepeticao: String;

                Procedure Submit_Sqlgroupby(parametro:String);
                Function Get_Sqlgroupby:String;


                procedure EdtSubMenuExit(Sender: TObject;LABELNOME:TLABEL);

                procedure EdtSubMenuKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtSubMenuKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                                 
                procedure EdtpermissaoExit(Sender: TObject; LABELNOME: TLABEL);

                procedure EdtpermissaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);overload;
                procedure EdtpermissaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;


                //CODIFICA DECLARA GETSESUBMITS


         Protected
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;


               CODIGO:string;
               Nome:string;
               SqlPrincipal:string;
               SqlRepeticao:string;
               TabelaPrincipal:string;
               TabelaRepeticao:string;
               Sqlgroupby:String;

               ParametroPesquisa:TStringList;
               orderbypersonalizado:string;

               UltimoSqlPrincipal:String;
               UltimoSqlrepeticao:String;


                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;





   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, USUBMENURELPERSONALIZADO, UPermissoesUso;





Function  TObjRELATORIOPERSONALIZADO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('SubMenu').asstring<>'')
        Then Begin
                 If (Self.SubMenu.LocalizaCodigo(FieldByName('SubMenu').asstring)=False)
                 Then Begin
                          Messagedlg('Sub-Menu N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.SubMenu.TabelaparaObjeto;
        End;


        If(FieldByName('permissao').asstring<>'')
        Then Begin
                 If (Self.permissao.LocalizaCodigo(FieldByName('permissao').asstring)=False)
                 Then Begin
                          Messagedlg('Permiss�o N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.permissao.TabelaparaObjeto;
        End;
        
        Self.Nome:=fieldbyname('Nome').asstring;

        Self.SqlPrincipal:=fieldbyname('SqlPrincipal').asstring;
        Self.SqlRepeticao:=fieldbyname('SqlRepeticao').asstring;
        Self.sqlgroupby:=fieldbyname('sqlgroupby').asstring;

        Self.TabelaPrincipal:=fieldbyname('TabelaPrincipal').asstring;
        Self.UltimoSqlPrincipal:=fieldbyname('UltimoSqlPrincipal').asstring;
        Self.orderbypersonalizado:=fieldbyname('orderbypersonalizado').asstring;
        Self.TabelaRepeticao:=fieldbyname('TabelaRepeticao').asstring;
        Self.UltimoSqlrepeticao:=fieldbyname('UltimoSqlrepeticao').asstring;


//CODIFICA TABELAPARAOBJETO








        result:=True;
     End;
end;


Procedure TObjRELATORIOPERSONALIZADO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('SubMenu').asstring:=Self.SubMenu.GET_CODIGO;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('Permissao').asstring:=Self.Permissao.get_codigo;
        ParamByName('SqlPrincipal').asstring:=Self.SqlPrincipal;
        ParamByName('SqlRepeticao').asstring:=Self.SqlRepeticao;
        ParamByName('TabelaPrincipal').asstring:=Self.TabelaPrincipal;
        ParamByName('UltimoSqlPrincipal').asstring:=Self.UltimoSqlPrincipal;
        ParamByName('orderbypersonalizado').asstring:=Self.orderbypersonalizado;
        ParamByName('TabelaRepeticao').asstring:=Self.TabelaRepeticao;
        ParamByName('UltimoSqlrepeticao').asstring:=Self.UltimoSqlrepeticao;
        ParamByName('sqlgroupby').asstring:=Self.sqlgroupby;
//CODIFICA OBJETOPARATABELA








  End;
End;

//***********************************************************************

function TObjRELATORIOPERSONALIZADO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjRELATORIOPERSONALIZADO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        SubMenu.ZerarTabela;
        Nome:='';
        Self.Permissao.ZerarTabela;
        SqlPrincipal:='';
        SqlRepeticao:='';
        TabelaPrincipal:='';
        UltimoSqlPrincipal:='';
        orderbypersonalizado:='';
        TabelaRepeticao:='';
        UltimoSqlrepeticao:='';
        sqlgroupby:='';
//CODIFICA ZERARTABELA








     End;
end;

Function TObjRELATORIOPERSONALIZADO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (SubMenu.get_Codigo='')
      Then Mensagem:=mensagem+'/Sub-Menu';
      If (Nome='')
      Then Mensagem:=mensagem+'/Nome';
      If (Permissao.Get_Codigo='')
      Then Mensagem:=mensagem+'/Permiss�o';
      If (SqlPrincipal='')
      Then Mensagem:=mensagem+'/Sql Principal';
      If (SqlRepeticao='')
      Then Mensagem:=mensagem+'/Sql Repeti��o';

      If (TabelaPrincipal='')
      Then Mensagem:=mensagem+'/Tabela Principal';



      If (orderbypersonalizado='')
      Then Mensagem:=mensagem+'/Order by Principal';

      If (TabelaRepeticao='')
      Then Mensagem:=mensagem+'/Tabela Repeti��o';




      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjRELATORIOPERSONALIZADO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.SubMenu.LocalizaCodigo(Self.SubMenu.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Sub-Menu n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjRELATORIOPERSONALIZADO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.SubMenu.Get_Codigo<>'')
        Then Strtoint(Self.SubMenu.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Sub-Menu';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjRELATORIOPERSONALIZADO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjRELATORIOPERSONALIZADO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjRELATORIOPERSONALIZADO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       result:=False;
       
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro RELATORIOPERSONALIZADO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,SubMenu,Nome,Permissao,SqlPrincipal,SqlRepeticao');
           SQL.ADD(' ,TabelaPrincipal,orderbypersonalizado,TabelaRepeticao,UltimoSqlPrincipal,UltimoSqlRepeticao,Sqlgroupby');
           SQL.ADD(' from  TabRelatorioPersonalizado');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjRELATORIOPERSONALIZADO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjRELATORIOPERSONALIZADO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjRELATORIOPERSONALIZADO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.SubMenu:=TOBJSUBMENURELPERSONALIZADO.create;

        Self.Permissao:=TObjPermissoesUso.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabRelatorioPersonalizado(CODIGO,SubMenu');
                InsertSQL.add(' ,Nome,Permissao,SqlPrincipal,SqlRepeticao,TabelaPrincipal');
                InsertSQL.add(' ,orderbypersonalizado,TabelaRepeticao,UltimoSqlPrincipal,UltimoSqlRepeticao,Sqlgroupby)');
                InsertSQL.add('values (:CODIGO,:SubMenu,:Nome,:Permissao,:SqlPrincipal');
                InsertSQL.add(' ,:SqlRepeticao,:TabelaPrincipal,:orderbypersonalizado,:TabelaRepeticao,:UltimoSqlPrincipal,:UltimoSqlRepeticao,:Sqlgroupby)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabRelatorioPersonalizado set CODIGO=:CODIGO');
                ModifySQL.add(',SubMenu=:SubMenu,Nome=:Nome,Permissao=:Permissao,SqlPrincipal=:SqlPrincipal');
                ModifySQL.add(',SqlRepeticao=:SqlRepeticao,TabelaPrincipal=:TabelaPrincipal');
                ModifySQL.add(',orderbypersonalizado=:orderbypersonalizado,TabelaRepeticao=:TabelaRepeticao,UltimoSqlPrincipal=:UltimoSqlPrincipal,UltimoSqlRepeticao=:UltimoSqlRepeticao,Sqlgroupby=:Sqlgroupby');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabRelatorioPersonalizado where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjRELATORIOPERSONALIZADO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjRELATORIOPERSONALIZADO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabRELATORIOPERSONALIZADO');
     Result:=Self.ParametroPesquisa;
end;

function TObjRELATORIOPERSONALIZADO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de RELATORIOPERSONALIZADO ';
end;


function TObjRELATORIOPERSONALIZADO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENRELATORIOPERSONALIZADO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENRELATORIOPERSONALIZADO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjRELATORIOPERSONALIZADO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.SubMenu.FREE;
    Self.Permissao.FREE;

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjRELATORIOPERSONALIZADO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjRELATORIOPERSONALIZADO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjRelatorioPersonalizado.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjRelatorioPersonalizado.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjRelatorioPersonalizado.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjRelatorioPersonalizado.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
procedure TObjRelatorioPersonalizado.Submit_SqlPrincipal(parametro: string);
begin
        Self.SqlPrincipal:=Parametro;
end;
function TObjRelatorioPersonalizado.Get_SqlPrincipal: string;
begin
        Result:=Self.SqlPrincipal;
end;
procedure TObjRelatorioPersonalizado.Submit_SqlRepeticao(parametro: string);
begin
        Self.SqlRepeticao:=Parametro;
end;
function TObjRelatorioPersonalizado.Get_SqlRepeticao: string;
begin
        Result:=Self.SqlRepeticao;
end;
procedure TObjRelatorioPersonalizado.Submit_TabelaPrincipal(parametro: string);
begin
        Self.TabelaPrincipal:=Parametro;
end;
function TObjRelatorioPersonalizado.Get_TabelaPrincipal: string;
begin
        Result:=Self.TabelaPrincipal;
end;

procedure TObjRelatorioPersonalizado.Submit_UltimoSqlPrincipal(parametro: String);
begin
        Self.UltimoSqlPrincipal:=Parametro;
end;

function TObjRelatorioPersonalizado.Get_UltimoSqlPrincipal: String;
begin
        Result:=Self.UltimoSqlPrincipal;
end;



procedure TObjRelatorioPersonalizado.Submit_orderbypersonalizado(parametro: string);
begin
        Self.orderbypersonalizado:=Parametro;
end;
function TObjRelatorioPersonalizado.Get_orderbypersonalizado: string;
begin
        Result:=Self.orderbypersonalizado;
end;


procedure TObjRelatorioPersonalizado.Submit_TabelaRepeticao(parametro: string);
begin
        Self.TabelaRepeticao:=Parametro;
end;
function TObjRelatorioPersonalizado.Get_TabelaRepeticao: string;
begin
        Result:=Self.TabelaRepeticao;
end;



procedure TObjRelatorioPersonalizado.Submit_UltimoSqlrepeticao(parametro: String);
begin
        Self.UltimoSqlrepeticao:=Parametro;
end;

function TObjRelatorioPersonalizado.Get_UltimoSqlrepeticao: String;
begin
        Result:=Self.UltimoSqlrepeticao;
end;



//CODIFICA GETSESUBMITS


procedure TObjRELATORIOPERSONALIZADO.EdtSubMenuExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.SubMenu.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.SubMenu.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.SubMenu.GET_NOME;
End;
procedure TObjRELATORIOPERSONALIZADO.EdtSubMenuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FSUBMENURELPERSONALIZADO:TFSUBMENURELPERSONALIZADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FSUBMENURELPERSONALIZADO:=TFSUBMENURELPERSONALIZADO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.SubMenu.Get_Pesquisa,Self.SubMenu.Get_TituloPesquisa,FSUBMENURELPERSONALIZADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SubMenu.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.SubMenu.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SubMenu.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FSUBMENURELPERSONALIZADO);
     End;
end;


procedure TObjRELATORIOPERSONALIZADO.EdtSubMenuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FSUBMENURELPERSONALIZADO:TFSUBMENURELPERSONALIZADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FSUBMENURELPERSONALIZADO:=TFSUBMENURELPERSONALIZADO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.SubMenu.Get_Pesquisa,Self.SubMenu.Get_TituloPesquisa,FSUBMENURELPERSONALIZADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.SubMenu.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FSUBMENURELPERSONALIZADO);
     End;
end;

//CODIFICA EXITONKEYDOWN


procedure TObjRELATORIOPERSONALIZADO.EdtpermissaoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.permissao.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.permissao.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.permissao.GET_NOME;
End;

procedure TObjRELATORIOPERSONALIZADO.EdtpermissaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FpermissaoRELPERSONALIZADO:TFpermissoesuso;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpermissaoRELPERSONALIZADO:=TFpermissoesuso.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.permissao.Get_Pesquisa,Self.permissao.Get_TituloPesquisa,FpermissaoRELPERSONALIZADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.permissao.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.permissao.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.permissao.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FpermissaoRELPERSONALIZADO);
     End;
end;



procedure TObjRELATORIOPERSONALIZADO.EdtpermissaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FpermissaoRELPERSONALIZADO:TFpermissoesuso;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FpermissaoRELPERSONALIZADO:=TFpermissoesuso.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.permissao.Get_Pesquisa,Self.permissao.Get_TituloPesquisa,FpermissaoRELPERSONALIZADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.permissao.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FpermissaoRELPERSONALIZADO);
     End;
end;

procedure TObjRELATORIOPERSONALIZADO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJRELATORIOPERSONALIZADO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;


function TObjRELATORIOPERSONALIZADO.Get_Sqlgroupby: String;
begin
     Result:=Self.Sqlgroupby;
end;

procedure TObjRELATORIOPERSONALIZADO.Submit_Sqlgroupby(parametro: String);
begin
     Self.Sqlgroupby:=Parametro;
end;

end.



