unit uGENERATORS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TFgenerators = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fgenerators: TFgenerators;

implementation

uses UobjUsuarioIB;

{$R *.DFM}

procedure TFgenerators.Button1Click(Sender: TObject);
var
objusuario:TObjUsuarioIB;
begin
     Try
        objusuario:= TObjUsuarioIB.create;
        objusuario.AcertarGenerator;
     Finally
            objusuario.free;
     end;

end;

end.
