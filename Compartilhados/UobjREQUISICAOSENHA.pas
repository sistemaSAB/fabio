unit UobjREQUISICAOSENHA;
Interface
Uses forms,Ibquery,windows,Classes,Db,ibdatabase,uutils;
//USES_INTERFACE

Type
   TObjREQUISICAOSENHA=class

          Public
                //ObjDatasource                               :TDataSource;
                QuerySenha:Tibquery;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                dia,mes,ano,hora,minuto,segundo,rnd,plugue,Comsem:String;
                PmensagemExterna                            :String;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(Caminhobanco:String);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:Str09) :boolean;
                Function    Exclui(Pcodigo:str09;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :Str100;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:Str09;
                Function  RetornaCampoCodigo:Str100;
                Function  RetornaCampoNome:Str100;


                Procedure Submit_CODIGO(parametro: STR09);
                Function Get_CODIGO: STR09;
                Procedure Submit_REQUISICAO(parametro: STR255);
                Function Get_REQUISICAO: STR200;
                Procedure Submit_RESPOSTA(parametro: STR255);
                Function Get_RESPOSTA: STR200;
                Procedure Submit_REQUISITANTE(parametro: STR255);
                Function Get_REQUISITANTE: STR200;
                Function Get_MensagemResposta:String;
                Procedure Submit_MensagemResposta(parametro:string);

                Procedure Submit_CodigoBaseSite(parametro:String);
                Function Get_CodigoBaseSite:String;

                //CODIFICA DECLARA GETSESUBMITS


                Function CodificoRequisicao(PnumeroPlugue:String;PComPLugue:Str01):Boolean;
                Function ValidaEntrada(PnumeroPlugue:String;PcomPlugue: Str01;Prequisitante:string;var Pdemo:Boolean;PcodigoBaseSite:String;PqueryLocal:tibquery):Boolean;
                Function ExcluiRequisicoesPendentes:boolean;
                function  LocalizaNovaRequisicao: boolean;
                Procedure Separavariaveis(original:string);
                Function  GeraContraSenha:Boolean;
                Function  DeCodificoRequisicao:Boolean;

                function CompletaPalavra_a_Esquerda(palavra: string;quantidade: Integer; ValorASerUSado: Str01): String;
                procedure MensagemErro(Mensagem: String);

                Function VersaoLara(pversao:integer): Boolean;

                //05/11/2010 Celio - Valida��o de senha autom�tica
                Function RecuperaSenhaVencimento:boolean;
         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               IBDatabase: TIBDatabase;
               IBTransaction: TIBTransaction;


               CODIGO:STR09;
               REQUISICAO:STR255;
               RESPOSTA:STR255;
               REQUISITANTE:STR255;
               CodigoBaseSite:String;
               MensagemResposta:string;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                




   End;


implementation
uses SysUtils,Dialogs,Controls,Math, U_Cipher, UmostraMEnsagem;

Function  TObjREQUISICAOSENHA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.REQUISICAO:=fieldbyname('REQUISICAO').asstring;
        Self.RESPOSTA:=fieldbyname('RESPOSTA').asstring;
        Self.REQUISITANTE:=fieldbyname('REQUISITANTE').asstring;
        Self.MensagemResposta:=fieldbyname('mensagemresposta').asstring;
        Self.CodigoBaseSite:=DesincriptaSenha(fieldbyname('CodigoBaseSite').asstring);

//CODIFICA TABELAPARAOBJETO




        result:=True;
     End;
end;


Procedure TObjREQUISICAOSENHA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('REQUISICAO').asstring:=Self.REQUISICAO;
        ParamByName('RESPOSTA').asstring:=Self.RESPOSTA;
        ParamByName('REQUISITANTE').asstring:=Self.REQUISITANTE;
        ParamByname('mensagemresposta').asstring:=Self.MensagemResposta;
        ParamByname('CodigoBaseSite').asstring:=EncriptaSenha(Self.CodigoBaseSite);

//CODIFICA OBJETOPARATABELA




  End;
End;

//***********************************************************************

function TObjREQUISICAOSENHA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True Then
  //Self.IBTransaction.CommitRetaining;
  Self.IBTransaction.Commit;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjREQUISICAOSENHA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        REQUISICAO:='';
        RESPOSTA:='';
        REQUISITANTE:='';
        Self.MensagemResposta:='';
        CodigoBaseSite:='';
//CODIFICA ZERARTABELA




     End;
end;

Function TObjREQUISICAOSENHA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      
      If (REQUISICAO='')
      Then Mensagem:=mensagem+'/Requisi��o';

      if (CodigoBaseSite='')
      Then mensagem:=Mensagem+'/C�digo do Cliente no Site';

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjREQUISICAOSENHA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjREQUISICAOSENHA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     Try
        strtoint(CodigoBaseSite);
     Except
           mensagem:=mensagem+'/C�digo do Cliente no Site';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjREQUISICAOSENHA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjREQUISICAOSENHA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjREQUISICAOSENHA.LocalizaCodigo(parametro: Str09): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro REQUISICAOSENHA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,REQUISICAO,RESPOSTA,REQUISITANTE,MensagemResposta,CodigoBaseSite');
           SQL.ADD(' from  TabRequisicaoSenha');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0) Then
            Result:=True
           Else
           begin
            self.Objquery.Transaction.Commit;
            Result:=False;
           end


       End;
end;

function TObjREQUISICAOSENHA.LocalizaNovaRequisicao: boolean;//ok
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,REQUISICAO,RESPOSTA,REQUISITANTE,MensagemResposta,CodigoBaseSite');
           SQL.ADD(' from  TabRequisicaoSenha');
           SQL.ADD(' WHERE RESPOSTA IS NULL or resposta='''' ');
           Open;
           last;
           
           If (recordcount>0) Then
            Result:=True
           Else
           begin
             Self.Objquery.Transaction.Commit;
            Result:=False;
           end;
       End;
end;

procedure TObjREQUISICAOSENHA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjREQUISICAOSENHA.Exclui(Pcodigo: str09;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True) Then
                  //Self.IBTransaction.CommitRetaining;
                  Self.IBTransaction.Commit;
        End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjREQUISICAOSENHA.create(Caminhobanco:String);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin
        Try

            Self.IBTransaction:=TIBTransaction.create(nil);
            Self.IBTransaction.DefaultAction:=TARollback;
            Self.IBTransaction.Params.add('read_committed');
            Self.IBTransaction.Params.add('rec_version');
            Self.IBTransaction.Params.add('nowait');
            Self.IBDatabase:=TIBDatabase.create(nil);
            Self.IBDatabase.SQLDialect:=3;
            Self.IBDatabase.LoginPrompt:=False;
            Self.IBDatabase.DefaultTransaction:=Self.IBTransaction;
            Self.IBDatabase.Databasename:=CaminhoBanco;
            //USUARIO PROTECAO
            //SENHA   PROTECAO
            Self.IbDatabase.Params.clear;
            Self.IbDatabase.Params.Add('User_name='+DesincriptaSenha('��������'));
            Self.IbDatabase.Params.Add('password='+DesincriptaSenha('��������'));

            Self.IbDatabase.Open;
            //************************************************************************
    
    
    
            Self.Objquery:=TIBQuery.create(nil);
            Self.Objquery.Database:=Self.IbDatabase;
    
            Self.QuerySenha:=TIBQuery.create(nil);
            Self.QuerySenha.Database:=Self.IbDatabase;
    
            Self.ParametroPesquisa:=TStringList.create;
    
    
            InsertSql:=TStringList.create;
            DeleteSql:=TStringList.create;
            ModifySQl:=TStringList.create;
    //CODIFICA CRIACAO DE OBJETOS
    
            Self.ZerarTabela;
    
            With Self do
            Begin
    
                    InsertSQL.clear;
                    InsertSQL.add('Insert Into TabRequisicaoSenha(CODIGO,REQUISICAO,RESPOSTA');
                    InsertSQL.add(' ,REQUISITANTE,MensagemResposta,CodigoBaseSite)');
                    InsertSQL.add('values (:CODIGO,:REQUISICAO,:RESPOSTA,:REQUISITANTE,:MensagemResposta,:CodigoBaseSite');
                    InsertSQL.add(' )');
    //CODIFICA INSERTSQL
    
                    ModifySQL.clear;
                    ModifySQL.add('Update TabRequisicaoSenha set CODIGO=:CODIGO,REQUISICAO=:REQUISICAO');
                    ModifySQL.add(',RESPOSTA=:RESPOSTA,REQUISITANTE=:REQUISITANTE,MensagemResposta=:MensagemResposta,CodigoBaseSite=:CodigoBaseSite');
                    ModifySQL.add('where codigo=:codigo');
    //CODIFICA MODIFYSQL
    
                    DeleteSQL.clear;
                    DeleteSql.add('Delete from TabRequisicaoSenha where codigo=:codigo ');
    //CODIFICA DELETESQL
    
                    Self.status          :=dsInactive;
            End;
    Except
          on e:exception do
          Begin
                raise Exception.Create('Erro na tentativa de cria��o do Objeto OBJREQUISICAOSENHA'+#13+e.message);
          End;

    End;


end;
procedure TObjREQUISICAOSENHA.Commit;
begin
     //Self.IBTransaction.CommitRetaining;
     Self.IBTransaction.Commit;
end;

function TObjREQUISICAOSENHA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabREQUISICAOSENHA');
     Result:=Self.ParametroPesquisa;
end;

function TObjREQUISICAOSENHA.Get_TituloPesquisa: Str100;
begin
     Result:=' Pesquisa de Requisi��o de Senha';
end;


function TObjREQUISICAOSENHA.Get_NovoCodigo: Str09;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=Self.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENREQUISICAOSENHA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENREQUISICAOSENHA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
        IbQueryGen.Transaction.Commit;
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjREQUISICAOSENHA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.QuerySenha);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    freeandnil(Self.ibtransaction);
    freeandnil(Self.IBDatabase);




    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjREQUISICAOSENHA.RetornaCampoCodigo: Str100;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjREQUISICAOSENHA.RetornaCampoNome: Str100;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjRequisicaoSenha.Submit_CODIGO(parametro: STR09);
begin
        Self.CODIGO:=Parametro;
end;
function TObjRequisicaoSenha.Get_CODIGO: STR09;
begin
        Result:=Self.CODIGO;
end;
procedure TObjRequisicaoSenha.Submit_REQUISICAO(parametro: STR255);
begin
        Self.REQUISICAO:=Parametro;
end;
function TObjRequisicaoSenha.Get_REQUISICAO: STR200;
begin
        Result:=Self.REQUISICAO;
end;
procedure TObjRequisicaoSenha.Submit_RESPOSTA(parametro: STR255);
begin
        Self.RESPOSTA:=Parametro;
end;
function TObjRequisicaoSenha.Get_RESPOSTA: STR200;
begin
        Result:=Self.RESPOSTA;
end;
procedure TObjRequisicaoSenha.Submit_REQUISITANTE(parametro: STR255);
begin
        Self.REQUISITANTE:=Parametro;
end;
function TObjRequisicaoSenha.Get_REQUISITANTE: STR200;
begin
        Result:=Self.REQUISITANTE;
end;
//CODIFICA GETSESUBMITS








function TObjREQUISICAOSENHA.CodificoRequisicao(PnumeroPlugue:String;PComPLugue:Str01): Boolean;
var
cont:integer;
original:string;
Pdatahora:Tdatetime;
begin
     result:=False;


     //Gero a requisicao criptografada e a contra senha armazenando na Self.resposta


         //dia ano mes hora  minuto  segundo random n�plugue  Plugue(1/0)
         //2    4   2    2       2    2      3      8           1        =26 bytes
//posicao  1    3   7    9       11   13     15     18          19

     Randomize;
     cont:=RandomRange(1,100);
     PdataHora:=now;
     Self.REQUISICAO:=formatdatetime('ddyyyymmhhmmss',PDataHora);
     Self.requisicao:=Self.REQUISICAO+Self.CompletaPalavra_a_Esquerda(inttostr(cont),3,'0');
     //Numero do Plugue
     Self.requisicao:=Self.REQUISICAO+completapalavra_a_esquerda(pnumeroplugue,8,'0');
     //Indica se vai ter ou nao o plugue
     Self.requisicao:=Self.REQUISICAO+PComPLugue;
     Original:=Self.requisicao;

     Self.Separavariaveis(original);

     if (Self.GeraContraSenha=False)
     Then exit;

     
     result:=True;

end;

function TObjREQUISICAOSENHA.DeCodificoRequisicao: Boolean;
var
contrasenha,original:String;
begin
     result:=False;

     if (length(self.requisicao)<>26)
     then Begin
               //Erro no tamanho da  requisi��o
               Self.PmensagemExterna:=DesincriptaSenha('����������������������������');
               exit;
     End;

     //primeiro descriptografo
     original:=DesincriptaSenha(Self.REQUISICAO);
     Self.SeparaVariaveis(original);
     result:=True;

end;

procedure TObjREQUISICAOSENHA.Separavariaveis(original: string);
begin
     //Separo as variaveis
     Self.dia:=copy(original,1,2);
     Self.ano:=copy(original,3,4);
     Self.mes:=copy(original,7,2);
     Self.hora:=copy(original,9,2);
     Self.minuto:=copy(original,11,2);
     Self.segundo:=copy(original,13,2);
     Self.rnd:=copy(original,15,2);
     Self.plugue:=copy(original,18,8);
     Self.Comsem:=copy(original,26,1);
     //***************************
end;

function TObjREQUISICAOSENHA.geraContraSenha: Boolean;
begin
     result:=False;
     //gero a senha com as variaveis e gravo no Self.resposta
     Self.RESPOSTA:=MD5Hash(Self.REQUISICAO);
end;


function TObjREQUISICAOSENHA.ValidaEntrada(PnumeroPlugue:String;PcomPlugue: Str01;Prequisitante:string;var Pdemo:Boolean;PcodigoBaseSite:String;PqueryLocal:tibquery): Boolean;
var
Pcodigo:Str09;
pmensagemresposta,presposta,pcontrasenha:string;
cont:integer;
FmostraMensagemX:TFmostramensagem;
begin
         result:=False;
         Pdemo:=False;

         PCodigo:=Self.Get_NovoCodigo;
         Self.Status:=dsinsert;
         Self.ZerarTabela;
         Self.Submit_CODIGO(Pcodigo);
         Self.CodificoRequisicao(PnumeroPlugue,PcomPlugue);
         Self.GeraContraSenha;
         PcontraSenha:=Self.RESPOSTA;
         Self.RESPOSTA:='';
         Self.REQUISITANTE:=prequisitante;
         Self.requisicao:=EncriptaSenha(Self.REQUISICAO);
         Self.CodigoBaseSite:=PcodigoBaseSite;

         if (Self.Salvar(True)=False)
         Then Begin
                   Self.PmensagemExterna:='Erro na tentativa de gravar a requisi��o';
                   exit;
         End;

         Try
            FmostramensagemX:=Tfmostramensagem.create(nil);
         Except
               Self.PmensagemExterna:='Erro na tentativa de gerar o formul�rio de mensagem';
               exit;
         End;

         Try
             FmostraMensagemX.lbmensagemprincipal.caption:='Validando seu software';
             FmostraMensagemX.Lbmensagemsecundaria.caption:='Aguarde ';
             FmostraMensagemX.lboriginal_demo.caption:='';
             FmostraMensagemX.Im_estrela.visible:=False;
             FmostraMensagemX.Im_mao.visible:=False;

             For cont:=1 to 60 do
             Begin
                  FmostraMensagemX.Lbmensagemsecundaria.caption:=FmostraMensagemX.Lbmensagemsecundaria.caption+'.';
                  FmostraMensagemX.show;
                  Application.processmessages;

                  PQueryLocal.close;
                  PQueryLocal.sql.clear;
                  PQueryLocal.sql.add('Select resposta,mensagemresposta from tabrequisicaosenha where codigo='+pcodigo);
                  Try
                      presposta:='';
                      pmensagemresposta:='';
                      PQueryLocal.open;
                      presposta:=PQuerylocal.fieldbyname('resposta').asstring;
                      pmensagemresposta:=PQuerylocal.fieldbyname('mensagemresposta').asstring;
                  Except
                        ON E:EXCEPTION DO
                        Begin
                              MensagemErro(E.message);//pode ser problema de
                              presposta:='';
                              pmensagemresposta:='';
                        End;
                  End;

                  (*if (Self.LocalizaCodigo(pcodigo)=False)
                  Then Begin
                            Self.PmensagemExterna:='Registro de valida��o gerado n�o foi localizado. C�digo '+Pcodigo;
                            exit;
                  End;

                  Self.TabelaparaObjeto;
                  if (self.RESPOSTA<>'')
                  then Begin*)
                  if (presposta<>'')
                  Then Begin
                            //ja tenho a resposta, verifico se esta correto
                            //if (Self.RESPOSTA=pcontrasenha)

                            if (presposta=pcontrasenha)
                            Then Begin
                                      //DEU OK, porem verifico se o Plugue n�o � DEMO
                                      //����༻��  "MODO DEMO"

                                      //if (Uutils.DesincriptaSenha(Self.Get_MensagemResposta)=Uutils.DesincriptaSenha('����༻��'))
                                      if (Uutils.DesincriptaSenha(pmensagemresposta)=Uutils.DesincriptaSenha('����༻��'))
                                      Then Begin
                                                Pdemo:=true;
                                                FmostraMensagemX.lboriginal_demo.caption:='Produto Demo';
                                      End
                                      Else FmostraMensagemX.lboriginal_demo.caption:='Produto original';
                                      FmostraMensagemX.Im_estrela.visible:=true;
                                      FmostraMensagemX.Im_mao.visible:=False;

                                      FmostraMensagemX.show;
                                      Application.ProcessMessages;
                                      sleep(1000);
                                      result:=True;
                                      exit;
                            End
                            Else Begin
                                      //if (self.MensagemResposta='')
                                      if (pmensagemresposta='')
                                      Then Self.PmensagemExterna:='Valida��o Incorreta. Tente novamente'
                                      Else Self.PmensagemExterna:=pmensagemresposta; //Self.PmensagemExterna:=Self.MensagemResposta;

                                      FmostraMensagemX.lboriginal_demo.caption:=Self.PmensagemExterna;
                                      break;
                            End;
                  End;
                  sleep(500);//espera 1/2 s
             End;

             if (Self.PmensagemExterna='')
             then Self.PmensagemExterna:='Tempo Estourado - O Aplicativo Lara n�o respondeu';

             FmostraMensagemX.lboriginal_demo.caption:='Erro';
             FmostraMensagemX.Im_estrela.visible:=False;
             FmostraMensagemX.Im_mao.visible:=True;
             FmostraMensagemX.show;
             Application.ProcessMessages;
             sleep(1000);


             if (Self.Exclui(pcodigo,true)=False)
             Then Self.PmensagemExterna:=Self.PmensagemExterna+'Estourado o tempo limite  de verifica��o. Erro na tentativa de Excluir o c�digo gerado. C�digo '+pcodigo;
         Finally
             FmostraMensagemX.close;
             freeandnil(Fmostramensagemx);
         End;
         exit;
end;

function TObjREQUISICAOSENHA.ExcluiRequisicoesPendentes: boolean;
begin
     result:=False;
     //Esse procedimento � chamado quando o Software Lara � aberto
     //pois caso tenha ficado alguma pendencia ele ja exclui para nao ficar tratando
     //Isso podera ocasionar um erro em Softwares quie estiverem esperando uma requisicao
     //porem � s� logar de novo , ou seja, pela teoria o Lara tem q ser aberto antes
     //dos outros softwares

  Try

     With Self.Objquery do

     Begin
          close;
          sql.clear;
          sql.add('update Tabrequisicaosenha set resposta=''0'' where resposta is null or resposta='''' ');
          Try
              execsql;
              //Self.IBTransaction.CommitRetaining;


          Except
                on e:exception do
                Begin
                      Self.PmensagemExterna:=e.message;
                      Self.IBTransaction.RollbackRetaining;
                      exit;
                End;
          End;
     End;
     
  Finally
    Self.IBTransaction.Commit;
  End;

end;

function TObjREQUISICAOSENHA.Get_MensagemResposta: String;
Begin
    Result:=Self.MensagemResposta;
end;

procedure TObjREQUISICAOSENHA.Submit_MensagemResposta(parametro: string);
begin
     Self.MensagemResposta:=parametro;
end;

Function TobjRequisicaoSenha.CompletaPalavra_a_Esquerda(palavra:string;quantidade:Integer;ValorASerUSado:Str01):String;
var
apoio:String;
Begin
     result:='';
     apoio:='';
     If (length(palavra)>=quantidade)
     Then Begin
                apoio:=copy(palavra,1,quantidade);
                result:=apoio;
                exit;
     End;

     apoio:='';

     if (quantidade=0)//em casos de configuracao de rel de NF isso � necessario
     Then Apoio:='';

     While ((length(apoio)+length(palavra))<quantidade)
     do Begin
             apoio:=apoio+Valoraserusado;
     End;

     result:=apoio+palavra;
End;


Procedure TobjRequisicaoSenha.MensagemErro(Mensagem:String);
Begin
  MessageDlg(Mensagem, mtError, [mbOk], 0);
End;

function TObjREQUISICAOSENHA.VersaoLara(pversao:integer): Boolean;
var
temp:integer;
begin
     result:=False;

     With Self.Objquery do
     Begin
          close;
          sql.Clear;
          sql.add('Select versao from tabversaoexecutavel where codigo=0');
          open;

          try
            Try
                temp:=Strtoint(trim(StringReplace(fieldbyname('versao').asstring,'.','',[rfReplaceAll])));



                if (temp<Pversao)
                Then Begin
                          mensagemErro('Esta vers�o do sistema foi concebida para trabalhar com o Lara V'+inttostr(pversao)+' ou superior ');
                          exit;
                End;
                result:=True;
            Except
                  mensagemErro('Erro na tentativa de resgatar a vers�o do Software Lara');
            End;
          finally
            self.Objquery.Transaction.Commit;
          end



     end;
end;

function TObjREQUISICAOSENHA.Get_CodigoBaseSite: String;
begin
     Result:=Self.CodigoBaseSite;
end;

procedure TObjREQUISICAOSENHA.Submit_CodigoBaseSite(parametro: String);
begin
     Self.CodigoBaseSite:=parametro;
end;

function TObjREQUISICAOSENHA.RecuperaSenhaVencimento: boolean;
begin


end;

end.



