object FselecionaCampos: TFselecionaCampos
  Left = 148
  Top = 61
  Width = 476
  Height = 509
  Caption = 'Configurador de Relat'#243'rios'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Abas: TPageControl
    Left = 7
    Top = 9
    Width = 458
    Height = 414
    ActivePage = TabSheet1
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = '&1 - Seleciona Campos'
      object CheckCampos: TCheckListBox
        Left = 0
        Top = 101
        Width = 449
        Height = 284
        ItemHeight = 13
        TabOrder = 0
      end
      object btselecionatodos: TButton
        Left = 1
        Top = 77
        Width = 448
        Height = 25
        Caption = 'Seleciona Todos/Retira Sele'#231#227'o'
        TabOrder = 1
        OnClick = btselecionatodosClick
      end
      object painelextra: TPanel
        Left = 3
        Top = 3
        Width = 446
        Height = 66
        TabOrder = 2
        Visible = False
        object Check01: TCheckBox
          Left = 6
          Top = 4
          Width = 97
          Height = 17
          Caption = 'Check01'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object LabeledEdit1: TLabeledEdit
          Left = 8
          Top = 40
          Width = 433
          Height = 21
          EditLabel.Width = 70
          EditLabel.Height = 14
          EditLabel.Caption = 'LabeledEdit1'
          EditLabel.Font.Charset = ANSI_CHARSET
          EditLabel.Font.Color = clWindowText
          EditLabel.Font.Height = -11
          EditLabel.Font.Name = 'Arial'
          EditLabel.Font.Style = [fsBold]
          EditLabel.ParentFont = False
          TabOrder = 1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = '&2 - Nomes Campos'
      ImageIndex = 1
      object Label1: TLabel
        Left = 0
        Top = 0
        Width = 449
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = 'Pressione <ENTER> sobre o campo deseja alterar o nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ListaNomeOriginal: TListBox
        Left = 8
        Top = 16
        Width = 169
        Height = 365
        ItemHeight = 13
        TabOrder = 0
        OnClick = ListaNomeOriginalClick
        OnDblClick = ListaNovoNomeDblClick
        OnKeyPress = ListaNomeOriginalKeyPress
      end
      object ListaNovoNome: TListBox
        Left = 176
        Top = 16
        Width = 273
        Height = 365
        ItemHeight = 13
        TabOrder = 1
        OnClick = ListaNovoNomeClick
        OnDblClick = ListaNovoNomeDblClick
        OnKeyPress = ListaNovoNomeKeyPress
      end
    end
    object TabSheet3: TTabSheet
      Caption = '&3 - Abrir/Salvar Configura'#231#227'o'
      ImageIndex = 2
      object Label2: TLabel
        Left = 0
        Top = 8
        Width = 119
        Height = 14
        Caption = 'Configura'#231#245'es Salvas'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 0
        Top = 72
        Width = 102
        Height = 14
        Caption = 'Nova Configura'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ComboRelSalvos: TComboBox
        Left = 0
        Top = 24
        Width = 385
        Height = 21
        ItemHeight = 0
        TabOrder = 0
        Text = 'ComboRelSalvos'
      end
      object abrirrelsalvo: TBitBtn
        Left = 392
        Top = 24
        Width = 57
        Height = 21
        Caption = 'ABRIR'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = abrirrelsalvoClick
        NumGlyphs = 2
      end
      object edtnovorel: TEdit
        Left = 0
        Top = 88
        Width = 385
        Height = 21
        TabOrder = 2
        Text = 'edtnovorel'
      end
      object btsalvarnovorel: TBitBtn
        Left = 393
        Top = 88
        Width = 57
        Height = 21
        Caption = 'SALVAR'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = btsalvarnovorelClick
        NumGlyphs = 2
      end
      object BitBtn1: TBitBtn
        Left = 0
        Top = 144
        Width = 449
        Height = 25
        Caption = 'Desenha Rel'
        TabOrder = 4
        OnClick = BitBtn1Click
      end
    end
  end
  object btcancel: TBitBtn
    Left = 322
    Top = 429
    Width = 143
    Height = 48
    Caption = 'CANCELAR'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = btcancelClick
  end
  object btok: TBitBtn
    Left = 177
    Top = 429
    Width = 144
    Height = 48
    Caption = 'OK'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = btokClick
  end
end
