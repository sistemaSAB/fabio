unit Uformata_String_Grid;

interface

uses grids,classes,sysutils,mask,dialogs;

Procedure Pesquisa_StringGrid(Pgrid:TStringGrid;PEdit:TmaskEdit;PTipoColunas:TStrings);
Procedure PreparaPesquisa_StringGrid(Pgrid:TStringGrid;PedtBusca:TMaskEdit;PTipoColunas:TStrings);
Procedure Formata_StringGrid(PGrid:TStringGrid;PTipoColunas:TStrings);
Procedure Ordena_StringGrid(PGrid:TStringGrid;PTipoColunas:TStrings);
Function Pesquisa_StringGrid_Todas_Ocorrencias(Pgrid:TStringGrid;PEdit:TmaskEdit;PTipoColunas:TStrings;var pLinha,pColuna:integer):boolean;


implementation

uses UessencialGlobal;

const TamanhoDecimal=12;


//*****************PROCEDIMENTOS PARA STRING GRIDS***********************
Procedure Formata_StringGrid(PGrid:TStringGrid;PTipoColunas:TStrings);
var
coluna,linha:integer;
Begin

    for coluna:=0 to PTipoColunas.count-1 do
    Begin
         if (UpperCase(PTipoColunas[coluna])='DECIMAL')
         Then Begin
                   for linha:=1 to PGrid.RowCount-1 do
                   Begin
                        //esses 12 caracteres sao usados para alinhar a direita
                        PGrid.Cells[coluna,linha]:=CompletaPalavra_a_Esquerda(formata_valor(PGrid.Cells[coluna,linha]),TamanhoDecimal,' ');
                   End;
         End;
    End;
    AjustaLArguraColunaGrid(pgrid);
End;

Procedure Ordena_StringGrid(PGrid:TStringGrid;PTipoColunas:TStrings);
var
colunaatual:integer;
linha1,linha2,colunas:integer;

inteiro1,inteiro2:integer;
decimal1,decimal2:currency;
data1,data2:Tdatetime;

trocalinha:boolean;
StrApoio:String;

Begin
{     Try
        StrApoio:=TStringList.create;
     Except
           MEssagedlg('Erro na tentativa de Cria��o da StringList',mterror,[mbok],0);
           exit;
     End;

Try
}
     //ordena pela coluna atual
     colunaatual:=PGrid.Col;

     for linha1:=1 to pgrid.RowCount-1 do
     Begin
          for linha2:=linha1+1 to pgrid.RowCount-1 do
          Begin
               TrocaLinha:=False;

               if (uppercase(PTipoColunas[colunaatual])='INTEGER')
               Then Begin
                         Try
                            if (PGrid.Cells[colunaatual,linha1]<>'')
                            Then inteiro1:=strtoint(PGrid.Cells[colunaatual,linha1])
                            else inteiro1:=0;
                         Except
                            inteiro1:=0;
                         End;

                         Try
                            if (PGrid.Cells[colunaatual,linha2]<>'')
                            Then inteiro2:=strtoint(PGrid.Cells[colunaatual,linha2])
                            else inteiro2:=0;
                         Except
                            inteiro2:=0;
                         End;

                         if (inteiro1>inteiro2)
                         Then TrocaLinha:=True;
               End;

               if (uppercase(PTipoColunas[colunaatual])='DECIMAL')
               Then Begin
                         Try
                            decimal1:=strtofloat(tira_ponto(PGrid.Cells[colunaatual,linha1]));
                         Except
                            decimal1:=0;
                         End;

                         Try
                            decimal2:=strtofloat(tira_ponto(PGrid.Cells[colunaatual,linha2]));
                         Except
                            decimal2:=0;
                         End;

                         if (decimal1>decimal2)
                         Then TrocaLinha:=True;
               End;

               if (uppercase(PTipoColunas[colunaatual])='DATE')
               Then Begin
                         Try
                            data1:=strtodate(PGrid.Cells[colunaatual,linha1]);
                         Except
                            data1:=strtodate('01/01/1500');
                         End;

                         Try
                            data2:=strtodate(PGrid.Cells[colunaatual,linha2]);
                         Except
                            data2:=strtodate('01/01/1500');
                         End;

                         if (data1>data2)
                         Then TrocaLinha:=True;
               End;

               if (uppercase(PTipoColunas[colunaatual])='TIME')
               Then Begin
                         Try
                            data1:=StrToTime(PGrid.Cells[colunaatual,linha1]);
                         Except
                            data1:=strtotime('00:00');
                         End;

                         Try
                            data2:=strtotime(PGrid.Cells[colunaatual,linha2]);
                         Except
                            data2:=strtotime('00:00');
                         End;

                         if (data1>data2)
                         Then TrocaLinha:=True;
               End;

               if (uppercase(PTipoColunas[colunaatual])='DATETIME')
               Then Begin
                         Try
                            data1:=StrTodateTime(PGrid.Cells[colunaatual,linha1]);
                         Except
                            data1:=strtodatetime('01/01/1500 00:00');
                         End;

                         Try
                            data2:=strtodatetime(PGrid.Cells[colunaatual,linha2]);
                         Except
                            data2:=strtodatetime(' 01/01/1500 00:00');
                         End;

                         if (data1>data2)
                         Then TrocaLinha:=True;
               End;

               if (uppercase(PTipoColunas[colunaatual])='STRING')
               Then Begin
                         if (PGrid.Cells[colunaatual,linha1]>PGrid.Cells[colunaatual,linha2])
                         Then TrocaLinha:=True;
               End;

               if (Trocalinha=True)
               Then Begin
                         StrApoio:='';

                         //significa que a linha1 � maior q a 2
                         //preciso troca-las de lugar

                         //Percorro todas as colunas da linha atual
                         //jogo a informaca da linha1 para uma variavel
                         //coloco a da linha2 no lugar e na linha2
                         //o que estava na variavel
                         
                         for colunas:=0 to PGrid.ColCount-1 do
                         Begin
                              StrApoio:='';
                              StrApoio:=PGrid.Cells[colunas,linha1];
                              PGrid.Cells[colunas,linha1]:=PGrid.Cells[colunas,linha2];
                              PGrid.Cells[colunas,linha2]:=Strapoio;
                         End;
               End;


          End;//do for do linha2
     End;//do for do linha1
{Finally
       freeandnil(StrApoio);
End;}
End;

Procedure Pesquisa_StringGrid(Pgrid:TStringGrid;PEdit:TmaskEdit;PTipoColunas:TStrings);
var
indiceencontrado,ColunaAtual:INteger;
pvalor:String;
Begin
     pvalor:=Pedit.Text;
     pvalor:='%'+pvalor;

     //Esse procedimento � chamando quando um ENTER � pressionado no
     //edit de pesquisa de uma StringGrid

     //a coluna atual indica que deve ser pesquisada
     ColunaAtual:=Pgrid.Col;

     if (uppercase(PTipoColunas[ColunaAtual])='INTEGER')
     Then Begin
               try
                  Strtoint(pvalor);
               except
                     Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                     PEdit.setfocus;
                     exit;
               end;
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='DATE')
     Then Begin
               try
                  StrtoDATE(pvalor);
               except
                     Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                     PEdit.setfocus;
                     exit;
               end;
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='TIME')
     Then Begin
               try
                  Strtotime(pvalor);
               except
                     Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                     PEdit.setfocus;
                     exit;
               end;
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='DATETIME')
     Then Begin
               try
                  Strtodatetime(pvalor);
               except
                     Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                     PEdit.setfocus;
                     exit;
               end;
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='DECIMAL')
     Then Begin
               try
                  pvalor:=tira_ponto(pvalor);
                  Strtofloat(pvalor);
               except
                     Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                     PEdit.setfocus;
                     exit;
               end;
               //esses 12 caracteres sao usados para alinhar a direita
               //padrao do procedimento Formata_StringGrid
               pvalor:=CompletaPalavra_a_Esquerda(formata_valor(pvalor),TamanhoDecimal,' ');
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='STRING')
     Then Begin
               //tem a situacao de procurar parcial
               //e a total
               //uso o % para especificar que � parcial
               if (pvalor<>'')
               Then Begin
                         if (pvalor[1]='%')
                         Then Begin
                                  if (length(pvalor)>1)
                                  Then Begin
                                           pvalor:=copy(pvalor,2,length(pvalor)-1);
                                           //procurando parcial

                                           for indiceencontrado:=1 to Pgrid.RowCount-1 do
                                           Begin
                                                if (pos(uppercase(pvalor),uppercase(Pgrid.cells[colunaatual,indiceencontrado]))<>0)
                                                Then Begin
                                                         Pgrid.Row:=indiceencontrado;
                                                         Pgrid.SetFocus;
                                                         exit;
                                                 End
                                           End;

                                           PEdit.setfocus;
                                           Messagedlg('Valor n�o encontrado!',mterror,[mbok],0);
                                           exit;
                                  End
                                  Else pvalor:='';
                         End;
               End;
     End;



     indiceencontrado:=Pgrid.Cols[ColunaAtual].IndexOf(pvalor);

     if (indiceencontrado<>-1)
     Then Begin
              Pgrid.Row:=indiceencontrado;
              Pgrid.SetFocus;
              exit;
     End
     Else BEgin
              PEdit.visible:=True;
              PEdit.setfocus;
              Messagedlg('Valor n�o encontrado!',mterror,[mbok],0);
              exit;
     End;

End;

Procedure PreparaPesquisa_StringGrid(Pgrid:TStringGrid;PedtBusca:TMaskEdit;PTipoColunas:TStrings);
var
ColunaAtual:Integer;
Begin
     //este procedimento � chamado quando uma barra de espa�o � pressionada dentro de uma StringGrid

     ColunaAtual:=pgrid.Col;
     PEdtBusca.Text :='';


     if (uppercase(PTipoColunas[ColunaAtual])='DATETIME')
     Then Begin
               PedtBusca.EditMask:='00/00/0000 00:00:00';
               PEdtBusca.width:=70;
               PEdtBusca.Visible :=true;
               PEdtBusca.SetFocus;
     End;
     if (uppercase(PTipoColunas[ColunaAtual])='DATE')
     Then Begin
               PedtBusca.EditMask:='00/00/0000;1;_';
               PEdtBusca.width:=70;
               PEdtBusca.Visible :=true;
               PEdtBusca.SetFocus;
     End;
     if (uppercase(PTipoColunas[ColunaAtual])='TIME')
     Then Begin
               PedtBusca.EditMask:='00:00';
               PEdtBusca.width:=70;
               PEdtBusca.Visible :=true;
               PEdtBusca.SetFocus;
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='INTEGER')
     Then Begin
               PedtBusca.EditMask:='';
               PEdtBusca.width:=250;
               PEdtBusca.maxlength:=8;
               PEdtBusca.Visible :=true;
               PEdtBusca.SetFocus;
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='DECIMAL')
     Then Begin
               PedtBusca.EditMask:='';
               PedtBusca.width:=250;
               PedtBusca.maxlength:=8;
               PEdtBusca.Visible :=true;
               PEdtBusca.SetFocus;
     End;

     if (uppercase(PTipoColunas[ColunaAtual])='STRING')
     Then Begin
               PedtBusca.EditMask:='';
               PEdtBusca.width:=400;
               PEdtBusca.maxlength:=255;
               PEdtBusca.Visible :=true;
               PEdtBusca.SetFocus;
     End;
End;

Function Pesquisa_StringGrid_Todas_Ocorrencias(Pgrid:TStringGrid;PEdit:TmaskEdit;PTipoColunas:TStrings; var pLinha,pColuna:integer):boolean;
var
indiceencontrado,ColunaAtual:INteger;
pvalor:String;
Begin
     Result := false;
     pvalor:=Pedit.Text;

     //Esse procedimento � chamando quando um ENTER � pressionado no
     //edit de pesquisa de uma StringGrid

     //a coluna atual indica que deve ser pesquisada
     ColunaAtual:=Pgrid.Col;
     if(PTipoColunas<>nil)
     then begin
           if (uppercase(PTipoColunas[ColunaAtual])='INTEGER')
           Then Begin
                     try
                        Strtoint(pvalor);
                     except
                           Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                           PEdit.setfocus;
                           exit;
                     end;
           End;

           if (uppercase(PTipoColunas[ColunaAtual])='DATE')
           Then Begin
                     try
                        StrtoDATE(pvalor);
                     except
                           Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                           PEdit.setfocus;
                           exit;
                     end;
           End;

           if (uppercase(PTipoColunas[ColunaAtual])='TIME')
           Then Begin
                     try
                        Strtotime(pvalor);
                     except
                           Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                           PEdit.setfocus;
                           exit;
                     end;
           End;

           if (uppercase(PTipoColunas[ColunaAtual])='DATETIME')
           Then Begin
                     try
                        Strtodatetime(pvalor);
                     except
                           Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                           PEdit.setfocus;
                           exit;
                     end;
           End;

           if (uppercase(PTipoColunas[ColunaAtual])='DECIMAL')
           Then Begin
                     try
                        pvalor:=tira_ponto(pvalor);
                        Strtofloat(pvalor);
                     except
                           Messagedlg('Valor Inv�lido',mterror,[mbok],0);
                           PEdit.setfocus;
                           exit;
                     end;
                     //esses 12 caracteres sao usados para alinhar a direita
                     //padrao do procedimento Formata_StringGrid
                     pvalor:=CompletaPalavra_a_Esquerda(formata_valor(pvalor),TamanhoDecimal,' ');
           End;

     end;
     {
     if (uppercase(PTipoColunas[ColunaAtual])='STRING')
     Then Begin}
               //tem a situacao de procurar parcial
               //e a total
               //uso o % para especificar que � parcial
               if (pvalor<>'')
               Then Begin
                     //pvalor:=copy(pvalor,2,length(pvalor)-1);
                     //procurando parcial

                     for indiceencontrado:=pLinha to Pgrid.RowCount-1 do
                     Begin
                          if (pos(uppercase(pvalor),uppercase(Pgrid.cells[colunaatual,indiceencontrado]))<>0)
                          Then Begin
                                   Pgrid.Row:=indiceencontrado;
                                   Pgrid.Col:=ColunaAtual;
                                   Pgrid.SetFocus;
                                   pLinha:=indiceencontrado+1;
                                   result := True;
                                   exit;
                           End
                     End;

                     PEdit.setfocus;
                     Messagedlg('Valor n�o encontrado!',mterror,[mbok],0);
                     exit;
               End;
     //End;

      ShowMessage('N�o h� mais itens');
      plinha:=1;
      Pgrid.SetFocus;
      Exit;

End;

//************************************************************************

end.
