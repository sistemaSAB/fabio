object FrelatorioPersonalizado: TFrelatorioPersonalizado
  Left = 359
  Top = 206
  Width = 871
  Height = 604
  Caption = 'FrelatorioPersonalizado'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnDragDrop = FormDragDrop
  OnDragOver = FormDragOver
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object modelo: TLabel
    Tag = 666
    Left = 1
    Top = 1
    Width = 42
    Height = 14
    Caption = 'MODELO'
    Color = clMenuText
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = [fsBold]
    ParentColor = False
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    Transparent = True
    OnClick = modeloClick
    OnMouseMove = modeloMouseMove
  end
  object PanelPOsicoes: TPanel
    Left = 666
    Top = 6
    Width = 135
    Height = 78
    DockSite = True
    DragMode = dmAutomatic
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnFace
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    OnDockOver = PanelPOsicoesDockOver
    object LbColuna: TLabel
      Tag = 666
      Left = 6
      Top = 7
      Width = 40
      Height = 13
      Caption = 'Coluna'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblinha: TLabel
      Tag = 666
      Left = 6
      Top = 31
      Width = 32
      Height = 13
      Caption = 'Linha'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbposicao: TLabel
      Tag = 666
      Left = 78
      Top = 55
      Width = 55
      Height = 13
      Caption = 'Qt.Carac.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtleft: TEdit
      Left = 73
      Top = 3
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 0
      Text = 'EDTLEFT'
      OnKeyPress = edtleftKeyPress
    end
    object edttop: TEdit
      Left = 73
      Top = 27
      Width = 52
      Height = 17
      AutoSize = False
      BiDiMode = bdLeftToRight
      BorderStyle = bsNone
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      MaxLength = 9
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 1
      Text = 'EDTTOP'
      OnKeyPress = edttopKeyPress
    end
  end
  object STRGCampos: TStringGrid
    Left = 0
    Top = 324
    Width = 855
    Height = 203
    Align = alBottom
    Color = clBtnFace
    ColCount = 1
    FixedCols = 0
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine]
    TabOrder = 1
    OnDblClick = STRGCamposDblClick
    OnDrawCell = STRGCamposDrawCell
    ColWidths = (
      762)
  end
  object Panel1: TPanel
    Left = 0
    Top = 527
    Width = 855
    Height = 19
    Align = alBottom
    Caption = 
      'F5 - Altera quantidade de impressao | F6 - Altera tipo  |  F7 - ' +
      'Altera Valor | F8 - Configura Repeti'#231#227'o | F9 - Atributos | F10 -' +
      ' Visualiza Campos Dispon'#237'veis | F11 - Campos Subtotal'
    TabOrder = 2
  end
  object Rdprint: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Rdprint Preview'
    OpcoesPreview.PreviewZoom = 100
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = False
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Ativo
    OpcoesPreview.BotaoLer = Ativo
    OpcoesPreview.BotaoProcurar = Ativo
    Margens.Left = 10
    Margens.Right = 10
    Margens.Top = 10
    Margens.Bottom = 10
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'Exclaim Tecnologia'
    RegistroUsuario.SerieProduto = 'SINGLE-0706/01006'
    RegistroUsuario.AutorizacaoKey = 'BTXV-5778-EZCC-2224-OESY'
    About = 'RDprint 4.0c - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = False
    CorForm = clBtnFace
    CorFonte = clBlack
    Impressora = Epson
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    PortaComunicacao = 'LPT1'
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S05cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    OnNewPage = RdprintNewPage
    Left = 48
    Top = 258
  end
  object MainMenu1: TMainMenu
    Left = 16
    Top = 258
    object Opes1: TMenuItem
      Caption = 'Op'#231#245'es'
      object IMPRIMEFOLHADEGUIA1: TMenuItem
        Caption = 'Imprime Folha Guia'
        OnClick = IMPRIMEFOLHADEGUIA1Click
      end
      object SalvaPosies1: TMenuItem
        Caption = 'Salva Posi'#231#245'es'
        ShortCut = 16467
        OnClick = SalvaPosies1Click
      end
      object DestriCampoAtual1: TMenuItem
        Caption = 'Destr'#243'i Campo Atual'
        OnClick = DestriCampoAtual1Click
      end
      object DestriLabels1: TMenuItem
        Caption = 'Destr'#243'i Todos os Campos'
        OnClick = DestriLabels1Click
      end
      object AbreTeladeConfiguraesGerais1: TMenuItem
        Caption = 'Abre Tela de Configura'#231#245'es Gerais'
        OnClick = AbreTeladeConfiguraesGerais1Click
      end
    end
  end
end
