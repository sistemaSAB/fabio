unit UobjICONES;
Interface
Uses menus,controls,classes,graphics,extctrls,buttons,forms,Ibquery,windows,stdctrls,Db,UessencialGlobal,IBStoredProc;
//USES_INTERFACE


Type
   TObjICONES=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Control:boolean;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(PformPrincipal:tform;PImagemPrincipal:Timage);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_PROCEDIMENTO(parametro: string);
                Function Get_PROCEDIMENTO: string;
                Procedure Submit_USUARIO(parametro: string);
                Function Get_USUARIO: string;
                Procedure Submit_GRUPOUSUARIO(parametro: string);
                Function Get_GRUPOUSUARIO: string;
                Procedure Submit_TamanhoHorizontal(parametro: string);
                Function Get_TamanhoHorizontal: string;
                Procedure Submit_TamanhoVertical(parametro: string);
                Function Get_TamanhoVertical: string;
                Procedure Submit_PosicaoEsquerda(parametro: string);
                Function Get_PosicaoEsquerda: string;
                Procedure Submit_PosicaoSuperior(parametro: string);
                Function Get_PosicaoSuperior: string;
                Procedure Submit_NomeImagem(parametro: STRING);
                Function Get_NomeImagem: STRING;
                Procedure Submit_Texto(parametro: string);
                Function Get_Texto: string;
                //CODIFICA DECLARA GETSESUBMITS

                Function VerificaClick(PnomeProcedimento:string):boolean;
                function ExcluiTodosIcones: boolean;
                procedure CriaTodosIcones;

         Private
               Pformulario:Tform;
               Pimagem:timage;
               PopupMenu:TPopupMenu;
               MenuExclui:TMenuItem;
               NomeIconeAtual3D:String;
               IconeAtual3D:Timage;
               //Medidas do Logo
               EsquerdaInicial,EsquerdaFinal,SuperiorInicial,SuperiorFinal:integer;
               


               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               PROCEDIMENTO:string;
               USUARIO:string;
               GRUPOUSUARIO:string;
               TamanhoHorizontal:string;
               TamanhoVertical:string;
               PosicaoEsquerda:string;
               PosicaoSuperior:string;
               NomeImagem:STRING;
               Texto:String;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function LocalizaProcedimento_Usuario(Pprocedimento,Pusuario: string): boolean;
                Function CriaIcone:boolean;
                procedure DblClickBotao(Sender: TObject);
                procedure ClickBotao(Sender: TObject);
                procedure ExecMethod(MethodName: string);

                procedure ImageDragDrop(Sender, Source: TObject; X, Y: Integer);
                procedure ImageDragOver(Sender, Source: TObject; X, Y: Integer;State: TDragState; var Accept: Boolean);
                procedure MouseDownBotao(Sender: TObject; Button: TMouseButton;Shift: TShiftState; X, Y: Integer);
                procedure MouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);

                procedure ExcluirIconeClick(Sender: TObject);
                function ExcluiIcone(PnomeProcedimento: string): boolean;
                procedure OnEndDrag(Sender, Target: TObject; X, Y: Integer);
                procedure AlinhaLabel(source: tobject);
                Function ExtraiNome(Pnome: String):String;
                Procedure Retira3Dicone(Sender: TObject; Shift: TShiftState; X,Y: Integer);
                function Localiza_Posicao_Usuario(Px, Py, Pusuario: string): boolean;


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,UmenuRelatorios, UescolheImagemBotao, UescolheIcones,
  UpopupIcone, UinputQuery;


Function  TObjICONES.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.PROCEDIMENTO:=fieldbyname('PROCEDIMENTO').asstring;
        Self.USUARIO:=fieldbyname('USUARIO').asstring;
        Self.GRUPOUSUARIO:=fieldbyname('GRUPOUSUARIO').asstring;
        Self.TamanhoHorizontal:=fieldbyname('TamanhoHorizontal').asstring;
        Self.TamanhoVertical:=fieldbyname('TamanhoVertical').asstring;
        Self.PosicaoEsquerda:=fieldbyname('PosicaoEsquerda').asstring;
        Self.PosicaoSuperior:=fieldbyname('PosicaoSuperior').asstring;
        Self.NomeImagem:=fieldbyname('NomeImagem').asstring;
        Self.Texto:=fieldbyname('Texto').asstring;
//CODIFICA TABELAPARAOBJETO










        result:=True;
     End;
end;


Procedure TObjICONES.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('PROCEDIMENTO').asstring:=Self.PROCEDIMENTO;
        ParamByName('USUARIO').asstring:=Self.USUARIO;
        ParamByName('GRUPOUSUARIO').asstring:=Self.GRUPOUSUARIO;
        ParamByName('TamanhoHorizontal').asstring:=Self.TamanhoHorizontal;
        ParamByName('TamanhoVertical').asstring:=Self.TamanhoVertical;
        ParamByName('PosicaoEsquerda').asstring:=Self.PosicaoEsquerda;
        ParamByName('PosicaoSuperior').asstring:=Self.PosicaoSuperior;
        ParamByName('NomeImagem').asstring:=Self.NomeImagem;
        ParamByName('Texto').asstring:=Self.Texto;
//CODIFICA OBJETOPARATABELA










  End;
End;

//***********************************************************************

function TObjICONES.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjICONES.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        PROCEDIMENTO:='';
        USUARIO:='';
        GRUPOUSUARIO:='';
        TamanhoHorizontal:='';
        TamanhoVertical:='';
        PosicaoEsquerda:='';
        PosicaoSuperior:='';
        NomeImagem:='';
        Texto:='';
//CODIFICA ZERARTABELA










     End;
end;

Function TObjICONES.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (PROCEDIMENTO='')
      Then Mensagem:=mensagem+'/Procedimento';
      If (TamanhoHorizontal='')
      Then Mensagem:=mensagem+'/Tamanho Horizontal';
      If (TamanhoVertical='')
      Then Mensagem:=mensagem+'/Tamanho Vertical';
      If (PosicaoEsquerda='')
      Then Mensagem:=mensagem+'/Posi��o Esquerda';
      If (PosicaoSuperior='')
      Then Mensagem:=mensagem+'/Posi��o Superior';
      If (NomeImagem='')
      Then Mensagem:=mensagem+'/C�digo da Imagem';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjICONES.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjICONES.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        if (Self.GrupoUsuario<>'')
        Then Strtoint(Self.GRUPOUSUARIO);
     Except
           Mensagem:=mensagem+'/Grupo do Usu�rio';
     End;
     try
        Strtoint(Self.TamanhoHorizontal);
     Except
           Mensagem:=mensagem+'/Tamanho Horizontal';
     End;
     try
        Strtoint(Self.TamanhoVertical);
     Except
           Mensagem:=mensagem+'/Tamanho Vertical';
     End;
     try
        Strtoint(Self.PosicaoEsquerda);
     Except
           Mensagem:=mensagem+'/Posi��o Esquerda';
     End;
     try
        Strtoint(Self.PosicaoSuperior);
     Except
           Mensagem:=mensagem+'/Posi��o Superior';
     End;
     

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjICONES.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjICONES.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjICONES.LocalizaCodigo(parametro: string): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro ICONES vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,PROCEDIMENTO,USUARIO,GRUPOUSUARIO,TamanhoHorizontal');
           SQL.ADD(' ,TamanhoVertical,PosicaoEsquerda,PosicaoSuperior,NomeImagem');
           SQL.ADD(' ,Texto');
           SQL.ADD(' from  TabIcones');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;



function TObjICONES.LocalizaProcedimento_Usuario(Pprocedimento:String;Pusuario:string): boolean;//ok
begin
       Pusuario:=UpperCase(Pusuario);
       
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,PROCEDIMENTO,USUARIO,GRUPOUSUARIO,TamanhoHorizontal');
           SQL.ADD(' ,TamanhoVertical,PosicaoEsquerda,PosicaoSuperior,NomeImagem');
           SQL.ADD(' ,Texto');
           SQL.ADD(' from  TabIcones');
           SQL.ADD(' WHERE procedimento=:pprocedimento and usuario=:pusuario');
           ParamByName('pprocedimento').asstring:=Pprocedimento;
           ParamByName('pusuario').asstring:=pusuario;
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjICONES.Localiza_Posicao_Usuario(Px,Py:String;Pusuario:string): boolean;//ok
begin
       Pusuario:=UpperCase(Pusuario);
       
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,PROCEDIMENTO,USUARIO,GRUPOUSUARIO,TamanhoHorizontal');
           SQL.ADD(' ,TamanhoVertical,PosicaoEsquerda,PosicaoSuperior,NomeImagem');
           SQL.ADD(' ,Texto');
           SQL.ADD(' from  TabIcones');
           SQL.ADD(' WHERE PosicaoEsquerda=:pPosicaoEsquerda and PosicaoSuperior=:PPosicaoSuperior and usuario=:pusuario');

           ParamByName('pPosicaoEsquerda').asstring:=Px;
           ParamByName('PPosicaoSuperior').asstring:=PY;
           ParamByName('pusuario').asstring:=pusuario;
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjICONES.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjICONES.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


procedure TObjIcones.ExcluirIconeClick(Sender: TObject);
var
   Pnome,temp:String;
   posicao:integer;
begin
     Self.Control:=False;
     Pnome:=Timage(sender).Name;
     Pnome:=copy(pnome,3,length(pnome)-2);


     if (Self.LocalizaProcedimento_Usuario(Pnome,ObjUsuarioGlobal.Get_nome)=False)
     Then exit;

     Self.TabelaparaObjeto;
     if (Self.Exclui(Self.CODIGO,true)=True)
     then Begin
              Self.ExcluiIcone(pnome);
     End;

end;


constructor TObjICONES.create(PformPrincipal:tform;PImagemPrincipal:Timage);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
var
ptemp:currency;
begin




        Self.Pformulario:=PformPrincipal;
        Self.PImagem:=PimagemPrincipal;
        Self.Pimagem.PopupMenu:=Self.PopupMenu;

        Self.Pimagem.OnDragDrop:=Self.ImageDragDrop;
        Self.Pimagem.OnDragOver:=Self.ImageDragOver;


        Self.PImagem.OnMouseMove:=Self.Retira3DIcone;
        Self.NomeIconeAtual3D:='';
        Self.IconeAtual3D:=nil;

        //Calculando a Area Proibida
        //Resgato a Largura da Resolucao e Divido por 2 para encontrar o Meio
        //Resgato Tamanho da IMagem e Divido por 2 para encontrar o Meio da Imagem

         Try
            ptemp:=int(Screen.Width/2)-int(Self.Pimagem.Picture.width/2);
            if (ptemp<0)
            then ptemp:=0;

            //O 100 � o tamanho da imagem  100x100 entao comeco 100 antes  a verificar


            Self.EsquerdaInicial:=strtoint(floattostr(ptemp))-100;
            Self.EsquerdaFinal:=Self.EsquerdaInicial+Self.Pimagem.Picture.width+100;

            ptemp:=int(Screen.Height/2)-int(Self.Pimagem.Picture.Height/2);
            if (ptemp<0)
            then ptemp:=0;
            Self.SuperiorInicial:=strtoint(floattostr(ptemp))-100;
            Self.SuperiorFinal:=Self.SuperiorInicial+Self.Pimagem.Picture.Height+100;
         Except
               Self.EsquerdaInicial:=0;
               Self.EsquerdaFinal:=0;
               Self.SuperiorInicial:=0;
               Self.SuperiorFinal:=0;
         End;

         if (Self.EsquerdaInicial<0)
         Then Self.EsquerdaInicial:=0;;

         if (Self.SuperiorInicial<0)
         then Self.Superiorinicial:=0;

        Self.control:=False;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        //FescolheIcones.MenuExcluirIcone.onclick:=Self.ExcluirIconeClick;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabIcones(CODIGO,PROCEDIMENTO,USUARIO,GRUPOUSUARIO');
                InsertSQL.add(' ,TamanhoHorizontal,TamanhoVertical,PosicaoEsquerda');
                InsertSQL.add(' ,PosicaoSuperior,NomeImagem,Texto)');
                InsertSQL.add('values (:CODIGO,:PROCEDIMENTO,:USUARIO,:GRUPOUSUARIO');
                InsertSQL.add(' ,:TamanhoHorizontal,:TamanhoVertical,:PosicaoEsquerda');
                InsertSQL.add(' ,:PosicaoSuperior,:NomeImagem,:Texto)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabIcones set CODIGO=:CODIGO,PROCEDIMENTO=:PROCEDIMENTO');
                ModifySQL.add(',USUARIO=:USUARIO,GRUPOUSUARIO=:GRUPOUSUARIO,TamanhoHorizontal=:TamanhoHorizontal');
                ModifySQL.add(',TamanhoVertical=:TamanhoVertical,PosicaoEsquerda=:PosicaoEsquerda');
                ModifySQL.add(',PosicaoSuperior=:PosicaoSuperior,NomeImagem=:NomeImagem');
                ModifySQL.add(',Texto=:Texto');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabIcones where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;


end;

procedure TObjICONES.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjICONES.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabICONES');
     Result:=Self.ParametroPesquisa;
end;

function TObjICONES.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de ICONES ';
end;


function TObjICONES.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENICONES,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENICONES,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjICONES.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjICONES.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjICONES.RetornaCampoNome: string;
begin
      result:='Procedimento';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjIcones.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjIcones.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjIcones.Submit_PROCEDIMENTO(parametro: string);
begin
        Self.PROCEDIMENTO:=Parametro;
end;
function TObjIcones.Get_PROCEDIMENTO: string;
begin
        Result:=Self.PROCEDIMENTO;
end;
procedure TObjIcones.Submit_USUARIO(parametro: string);
begin
        Self.USUARIO:=Parametro;
end;
function TObjIcones.Get_USUARIO: string;
begin
        Result:=Self.USUARIO;
end;
procedure TObjIcones.Submit_GRUPOUSUARIO(parametro: string);
begin
        Self.GRUPOUSUARIO:=Parametro;
end;
function TObjIcones.Get_GRUPOUSUARIO: string;
begin
        Result:=Self.GRUPOUSUARIO;
end;
procedure TObjIcones.Submit_TamanhoHorizontal(parametro: string);
begin
        Self.TamanhoHorizontal:=Parametro;
end;
function TObjIcones.Get_TamanhoHorizontal: string;
begin
        Result:=Self.TamanhoHorizontal;
end;
procedure TObjIcones.Submit_TamanhoVertical(parametro: string);
begin
        Self.TamanhoVertical:=Parametro;
end;
function TObjIcones.Get_TamanhoVertical: string;
begin
        Result:=Self.TamanhoVertical;
end;
procedure TObjIcones.Submit_PosicaoEsquerda(parametro: string);
begin
        Self.PosicaoEsquerda:=Parametro;
end;
function TObjIcones.Get_PosicaoEsquerda: string;
begin
        Result:=Self.PosicaoEsquerda;
end;
procedure TObjIcones.Submit_PosicaoSuperior(parametro: string);
begin
        Self.PosicaoSuperior:=Parametro;
end;
function TObjIcones.Get_PosicaoSuperior: string;
begin
        Result:=Self.PosicaoSuperior;
end;
procedure TObjIcones.Submit_NomeImagem(parametro: STRING);
begin
        Self.NomeImagem:=Parametro;
end;
function TObjIcones.Get_NomeImagem: STRING;
begin
        Result:=Self.NomeImagem;
end;
procedure TObjIcones.Submit_Texto(parametro: string);
begin
        Self.Texto:=Parametro;
end;
function TObjIcones.Get_Texto: string;
begin
        Result:=Self.Texto;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjICONES.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJICONES';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

Function TobjIcones.ExcluiTodosIcones:boolean;
var
QueryTemp:tibquery;
begin
     Try
        QueryTemp:=Tibquery.create(nil);
        QueryTemp.database:=FdataModulo.IBDatabase;
     Except

     End;

     Try
        With QueryTemp do
        Begin
            close;
            sql.clear;
            sql.add('Select * from tabicones where usuario='+#39+uppercase(ObjUsuarioGlobal.Get_nome)+#39);
            open;
            first;

            While not(eof) do
            Begin
                 Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                 Self.TabelaparaObjeto;
                 try
                    Self.ExcluiIcone(Self.PROCEDIMENTO);
                 Except

                 End;

                 next;
            End;

        End;
     Finally
            Freeandnil(QueryTemp);
     End;

End;

Function TobjIcones.ExcluiIcone(PnomeProcedimento:string):boolean;
var
IMteste:Timage;
Lbteste:TLabel;
Begin
     Imteste:=nil;
     Imteste:=(Self.pformulario.FindComponent('IM'+PnomeProcedimento) as Timage);

     LBteste:=nil;
     Lbteste:=(Self.pformulario.FindComponent('LB'+PnomeProcedimento) as Tlabel);


     if (Imteste<>nil)
     Then FreeAndNil(Imteste);

     if (Lbteste<>nil)
     Then FreeAndNil(Lbteste);

End;

function TObjICONES.VerificaClick(PnomeProcedimento: string): boolean;

begin
     result:=False;

     if (Self.Control=False)
     then exit;

     Self.Control:=false;

     FescolheIcones.ShowModal;
     
     if (FescolheIcones.NOMEIMAGEM='')
     Then Begin
              result:=true;
              exit;
     End;

     //Usou o Control, entao significa que � para cadastrar
     if (self.LocalizaProcedimento_Usuario(PnomeProcedimento,ObjUsuarioGlobal.Get_nome)=True)
     Then Begin
               //ja existe preciso localizar se existe e excluir

               Self.ExcluiIcone(PnomeProcedimento);

               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
     End
     Else Begin
               Self.ZerarTabela;
               Self.status:=dsinsert;
               Self.Submit_CODIGO('0');
     End;


     if (FescolheIcones.NOMEIMAGEM='')
     Then self.Submit_NomeImagem('IMGPADRAO')
     Else self.Submit_NomeImagem(FescolheIcones.NOMEIMAGEM);

     if (FinputQuery.MeuInputQuery('�cone','Texto do �cone',Self.texto,20)=False)
     Then Self.Submit_Texto('Atalho');

     if (length(Self.Texto)>20)
     Then Self.Texto:=copy(Self.Texto,1,20);


     Self.Submit_PROCEDIMENTO(PnomeProcedimento);//nao pode deixar uppercase sen�o n�o encontra
     Self.Submit_USUARIO(uppercase(ObjUsuarioGlobal.Get_nome));
     Self.Submit_GRUPOUSUARIO('');
     Self.Submit_TamanhoHorizontal('80');
     Self.Submit_TamanhoVertical('80');
     Self.Submit_PosicaoEsquerda('0');
     Self.Submit_PosicaoSuperior('330');

     if (self.Salvar(true)=False)
     Then Begin
               MensagemErro('Erro na tentativa de criar o �cone');
               exit;
     End;

     //cria o icone atual
     Self.CriaIcone;
     result:=True;
end;

function TObjICONES.CriaIcone: boolean;
begin

     //essa fun��o cria um icone no formulario
     //com os dados do objeto

     With TImage.Create(Pformulario) do
     Begin
           Parent:=Pformulario;
           Height:=strtoint(Self.TamanhoVertical);
           Width:=strtoint(Self.TamanhoHorizontal);

           Left:=strtoint(Self.PosicaoEsquerda);
           Top:=strtoint(Self.PosicaoSuperior);

           DragMode:=dmmanual;

           Name:='IM'+Self.PROCEDIMENTO;//uso o nome do procedimento


           Transparent:=True;
           FescolheIcones.PegaFigura(Picture,Self.NomeImagem);

           OnDblClick:=Self.DblClickBotao;
           onClick:=Self.clickbotao;
           OnMouseDown:=self.MouseDownBotao;
           OnEndDrag:=Self.OnEndDrag;
           OnMouseMove:=Self.MouseMove;

          // PopupMenu:=FescolheIcones.PopupMenuIcone;


     End;


     with TLabel.Create(Self.Pformulario)  do
     begin

         Parent:=Self.Pformulario;
         Font.Name:='Verdana';
         Font.Style:=[fsBold];

         Alignment:=taCenter;
         WordWrap:=True;
         Width:=strtoint(Self.TamanhoHorizontal);
         Caption:=Self.Texto;
         Left:=strtoint(Self.PosicaoEsquerda)+5;
         Top:=strtoint(Self.TamanhoVertical)+strtoint(Self.PosicaoSuperior)+5;
         Name:='LB'+Self.PROCEDIMENTO;//uso o nome do procedimento
         Transparent:=True;
         OnClick:=Self.ClickBotao;
     End;

end;

procedure TObjICONES.DblClickBotao(Sender: TObject);
var
   Pnome,temp:String;
   posicao:integer;
begin
     Self.Control:=False;
     Pnome:=Timage(sender).Name;

     //extraindo o IM do nome do icone
     Pnome:=copy(pnome,3,length(pnome)-2);


     //verificando se tem o __i__ que indica que � outro m�dulo que gerencia o click e n�o o principal
     posicao:=pos('__i__',pnome);

     if (posicao>0)
     Then Begin
              //caso exista eu envio para o datamodulo o nome do procedimento
              //e o modulo que gerencia
              //la no datamodulo pelo nome ele chama o executametodo do modulo correto
              temp:=pnome;
              pnome:=copy(pnome,1,posicao-1);
              temp:=copy(temp,posicao+5,length(temp)-posicao+5);
              FDataModulo.ExecutaMetodoExterno(pnome,temp);
     End
     Else Self.ExecMethod(pnome);//nao eh externo � do form principal mesmo
end;

procedure TObjICONES.ClickBotao(Sender: TObject);
begin

end;

procedure TObjICONES.MouseDownBotao(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
Var
   Pnome:string;
   Teste:Tlabel;
begin
    If (ssCtrl in Shift)
    Then Begin
              //Se o Control estiver pressionado Inicia o DRAG (arrastar)
              Timage(Sender).BeginDrag(true,0);
    End;

     if (button=mbRight)
     Then Begin

               //chamo as opcoes
               FpopupIcone.Left:=Timage(sender).left+80;
               FpopupIcone.Top:=50+Timage(sender).top;
               FpopupIcone.showModal;

               if (FpopupIcone.Tag=0)
               Then exit;

               case  FpopupIcone.tag of

                1:Begin
                       //excluir

                       if (MensagemPergunta('Certeza que deseja excluir esse �cone?')=mrno)
                       Then exit;

                       Pnome:=Timage(Sender).name;
                       Pnome:=copy(Pnome,3,length(pnome)-2);

                       Self.ExcluiIcone(Pnome);

                       if (Self.NomeIconeAtual3D=pnome)
                       Then Self.NomeIconeAtual3D:='';

                       if (self.LocalizaProcedimento_Usuario(pnome,ObjUsuarioGlobal.Get_nome)=True)
                       Then Begin
                                 Self.TabelaparaObjeto;

                                 Self.Exclui(self.codigo,true);

                       End;

                 End;
                 2:Begin
                      //renomear
                       Pnome:=Timage(Sender).name;
                       Pnome:=copy(Pnome,3,length(pnome)-2);

                       if (self.LocalizaProcedimento_Usuario(pnome,ObjUsuarioGlobal.Get_nome)=True)
                       Then Begin
                                 Self.TabelaparaObjeto;

                                 if (FinputQuery.MeuInputQuery('�cone','Texto do �cone',Self.texto,20)=True)
                                 Then Begin
                                           Self.Status:=dsedit;
                                           Self.Salvar(True);

                                           teste:=nil;
                                           teste:=(Self.pformulario.FindComponent('LB'+Pnome) as Tlabel);

                                           if (teste<>nil)
                                           Then teste.caption:=Self.texto;

                                           
                                 End;
                       End;


                 End;

               End;
     
     End;

end;


procedure TObjIcones.ExecMethod(MethodName: string) ;
Type
    Texec = procedure of object;
var
   Routine: TMethod;
   Exec: TExec;
begin
   Routine.Data := Pformulario ;
   Routine.Code := Pformulario.MethodAddress(MethodName) ;

   If NOT Assigned(Routine.Code)
   Then Begin
              Messagedlg('O Procedimento"'+MethodName+'" N�o encontrado',mterror,[mbok],0);
              Exit;
   End;

   Exec := TExec(Routine) ;

   Exec;

End;

procedure TObjIcones.ImageDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
     //Aceita o Fim do arrasto
     accept:=true;
end;

procedure TObjIcones.ImageDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     //Conforme ele se movimenta, movimento a imagem e a Label
     TImage(Source).Left:=x;
     TImage(Source).Top:=Y+Timage(Sender).top;
     Self.AlinhaLabel(source);
End;

Procedure Tobjicones.AlinhaLabel(source:tobject);
var
   Pnome:String;
    teste:Tlabel;
Begin
     //localiza a label da imagem e movimenta ela
     Pnome:=Timage(source).Name;
     Pnome:=copy(pnome,3,length(pnome)-2);

     teste:=nil;
     teste:=(Self.pformulario.FindComponent('LB'+Pnome) as Tlabel);

     teste.left:= TImage(Source).Left;
     teste.top:=TImage(Source).top+TImage(Source).Height+5;
end;


Procedure TobjIcones.CriaTodosIcones;
var
QueryTemp:tibquery;
begin
     Try
        QueryTemp:=Tibquery.create(nil);
        QueryTemp.database:=FdataModulo.IBDatabase;
     Except

     End;

     Try
        With QueryTemp do
        Begin
            close;
            sql.clear;
            sql.add('Select * from tabicones where usuario='+#39+uppercase(ObjUsuarioGlobal.Get_nome)+#39);
            open;
            first;

            While not(eof) do
            Begin
                 Self.LocalizaCodigo(fieldbyname('codigo').asstring);
                 Self.TabelaparaObjeto;
                 try
                    Self.CriaIcone;
                 Except

                 End;

                 next;
            End;

        End;
     Finally
            Freeandnil(QueryTemp);
     End;

End;

procedure TObjIcones.OnEndDrag(Sender, Target: TObject; X,
  Y: Integer);
var
   Pnome,temp:String;
   px,py,posicao:integer;

begin
     //O Sender contem o nome do icone que foi arrastado
     //guardando o novo X e Y no BD

     if (Target=nil) or (Sender=nil)
     Then exit;

     Pnome:=Timage(sender).Name;
     Pnome:=copy(pnome,3,length(pnome)-2);
     
     Self.ZerarTabela;

     if (Self.LocalizaProcedimento_Usuario(Pnome,ObjUsuarioGlobal.Get_nome)=False)
     Then exit;

     Self.TabelaparaObjeto;


     Px:=X;
     Py:=Y+Timage(Target).top;

     //alinhando na grade

     Px:=strtoint(floattostr(int(Px/110)))*110;
     Py:=strtoint(floattostr(int(Py/110)))*110;


     if (Py<110)
     Then Py:=0;

     //Showmessage(inttostr(px)+ ' '+inttostr(py));

     if (((Self.EsquerdaInicial)<(Px)) and (Self.EsquerdaFinal>(Px)))
     and ((Self.SuperiorInicial<(Py)) and (Self.SuperiorFinal>(Py)))
     Then Begin
               //area proibida
               Timage(Sender).left:=strtoint(Self.PosicaoEsquerda);
               Timage(Sender).Top:=strtoint(Self.PosicaoSuperior);
               Self.AlinhaLabel(sender);
               exit;
     End; 

     //verificando se ja nao tem uma label nesta posicao
     if (Self.Localiza_Posicao_Usuario(inttostr(px),inttostr(py),ObjUsuarioGlobal.Get_nome)=False)
     Then Begin
            Timage(Sender).left:=PX;
            Timage(Sender).Top:=PY;
            Self.AlinhaLabel(sender);

            Self.Status:=dsedit;
            Self.Submit_PosicaoEsquerda(inttostr(PX));
            Self.Submit_PosicaoSuperior(inttostr(PY));//adiciona o TOP da IMagem que cobre o formulario principal
            Self.Salvar(true);
     End
     Else BEgin//tem uma label no lugar
                //pega a posicao original
                Timage(Sender).left:=strtoint(Self.PosicaoEsquerda);
                Timage(Sender).Top:=strtoint(Self.PosicaoSuperior);
                Self.AlinhaLabel(sender);
     End;

end;




Function TObjICONES.ExtraiNome(Pnome:String):string;
var
posicao:integer;
temp:string;
begin
     posicao:=pos('__i__',pnome);

     if (posicao>0)
     Then pnome:=copy(pnome,1,posicao-1);

     result:=pnome;
End;




procedure TObjICONES.MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
PNome:String;
Pusuario:string;
begin
     (*Esse procedimento � acionado no momento que o mouse � passado por cima
     de um �cone(timage) devo trocar a imagem dele para 3D *)

     Pnome:=Timage(Sender).name;
     Pnome:=copy(Pnome,3,length(pnome)-2);


     //jonas 14/12/2010 15:14
       if (PNome = 'NotaFiscalEletrnica1Click') then
          exit;
     //end


     if (Self.NomeIconeAtual3D=Pnome)//ele mesmo
     then exit;

     Pusuario:=ObjUsuarioGlobal.Get_nome;

     Self.Retira3Dicone(Sender,shift,X,Y);//retirna o anterior

(*   Preciso localizar o registro desse icone na TabIcones *)
     if (self.LocalizaProcedimento_Usuario(Pnome,pusuario)=True)
     Then Begin
                 Self.TabelaparaObjeto;
                 FescolheIcones.PegaFigura(Timage(Sender).picture,Self.NomeImagem+'_3D');
                 Self.NomeIconeAtual3D:=Pnome;
                 Self.IconeAtual3D:=Timage(Sender);
     End;
end;

procedure TObjICONES.Retira3Dicone(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
Pusuario:string;
begin
     (*Self.Pformulario.caption:='X '+inttostr(x)+' Y '+inttostr(y+Timage(sender).top)+' '+
     inttostr(EsquerdaInicial)+' '+inttostr(Esquerdafinal)+' '+
     inttostr(superiorInicial)+' '+inttostr(superiorfinal)
     ;
     *)

     //FpopupIcone.Close;

     if (Self.NomeIconeAtual3D='')
     Then exit;

     Pusuario:=ObjUsuarioGlobal.Get_nome;

(*   Preciso localizar o registro desse icone na TabIcones *)
       if (self.LocalizaProcedimento_Usuario(Self.NomeIconeAtual3D,pusuario)=True)
     Then Begin
                 Self.TabelaparaObjeto;
                 FescolheIcones.PegaFigura(Self.IconeAtual3D.picture,Self.NomeImagem);
                 Self.NomeIconeAtual3D:='';//indicando que ninguem esta com o 3D
     End;

end;


end.



