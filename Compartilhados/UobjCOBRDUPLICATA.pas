unit UobjCOBRDUPLICATA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJCOBRFATURA
;
//USES_INTERFACE



Type
   TObjCOBRDUPLICATA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               COBRFATURA:TOBJCOBRFATURA;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_observacao(parametro: string);
                Function Get_observacao: string;
                Procedure Submit_nDup(parametro: string);
                Function Get_nDup: string;
                Procedure Submit_dVenc(parametro: string);
                Function Get_dVenc: string;
                Procedure Submit_vDup(parametro: string);
                Function Get_vDup: string;
                procedure EdtCOBRFATURAExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtCOBRFATURAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               observacao:string;
               nDup:string;
               dVenc:string;
               vDup:string;
//CODIFICA VARIAVEIS PRIVADAS







               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjCOBRDUPLICATA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.observacao:=fieldbyname('observacao').asstring;
        Self.nDup:=fieldbyname('nDup').asstring;
        Self.dVenc:=fieldbyname('dVenc').asstring;
        Self.vDup:=fieldbyname('vDup').asstring;
        If(FieldByName('COBRFATURA').asstring<>'')
        Then Begin
                 If (Self.COBRFATURA.LocalizaCodigo(FieldByName('COBRFATURA').asstring)=False)
                 Then Begin
                          Messagedlg('COBRFATURA N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.COBRFATURA.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO






        result:=True;
     End;
end;


Procedure TObjCOBRDUPLICATA.ObjetoparaTabela;
begin

  With Objquery do
  Begin
    ParamByName('Codigo').asstring:=Self.Codigo;
    ParamByName('observacao').asstring:=Self.observacao;
    ParamByName('nDup').asstring:=Self.nDup;
    ParamByName('dVenc').asstring:=Self.dVenc;
    ParamByName('vDup').asstring:=format_db(Self.vDup);
    ParamByName('COBRFATURA').asstring:=Self.COBRFATURA.GET_CODIGO;
  End;

End;

//***********************************************************************

function TObjCOBRDUPLICATA.Salvar(ComCommit:Boolean): Boolean;//Ok
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOBRDUPLICATA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        observacao:='';
        nDup:='';
        dVenc:='';
        vDup:='';
        COBRFATURA.ZerarTabela;
//CODIFICA ZERARTABELA






     End;
end;

Function TObjCOBRDUPLICATA.VerificaBrancos:boolean;
var
  Mensagem:string;
begin

  Result:=True;
  mensagem:='';


  if self.nDup = '' then
    Mensagem := Mensagem+'N�mero duplicata';

  if self.dVenc = '' then
    Mensagem := Mensagem+'Data vencimento duplicata';

  if self.vDup = '' then
    Mensagem := Mensagem+'Valor duplicata';

  If (Codigo='') Then
    Mensagem:=mensagem+'/Codigo';

  if mensagem<>'' Then
  Begin
    messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
    exit;
  End;

  result:=false;

end;


function TObjCOBRDUPLICATA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.COBRFATURA.LocalizaCodigo(Self.COBRFATURA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ COBRFATURA n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCOBRDUPLICATA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        Strtofloat(tira_ponto(Self.vDup));
     Except
           Mensagem:=mensagem+'/vDup';
     End;
     try
        If (Self.COBRFATURA.Get_Codigo<>'')
        Then Strtoint(Self.COBRFATURA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/COBRFATURA';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCOBRDUPLICATA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.dVenc);
     Except
           Mensagem:=mensagem+'/dVenc';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCOBRDUPLICATA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCOBRDUPLICATA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COBRDUPLICATA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,observacao,nDup,dVenc,vDup,COBRFATURA');
           SQL.ADD(' from  TABCOBRDUPLICATA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOBRDUPLICATA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCOBRDUPLICATA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCOBRDUPLICATA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCOBRDUPLICATA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.COBRFATURA:=TOBJCOBRFATURA.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCOBRDUPLICATA(Codigo,observacao,nDup');
                InsertSQL.add(' ,dVenc,vDup,COBRFATURA)');
                InsertSQL.add('values (:Codigo,:observacao,:nDup,:dVenc,:vDup,:COBRFATURA');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCOBRDUPLICATA set Codigo=:Codigo,observacao=:observacao');
                ModifySQL.add(',nDup=:nDup,dVenc=:dVenc,vDup=:vDup,COBRFATURA=:COBRFATURA');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCOBRDUPLICATA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOBRDUPLICATA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOBRDUPLICATA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOBRDUPLICATA');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOBRDUPLICATA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de COBRDUPLICATA ';
end;


function TObjCOBRDUPLICATA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCOBRDUPLICATA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCOBRDUPLICATA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOBRDUPLICATA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.COBRFATURA.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOBRDUPLICATA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOBRDUPLICATA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCOBRDUPLICATA.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjCOBRDUPLICATA.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjCOBRDUPLICATA.Submit_observacao(parametro: string);
begin
        Self.observacao:=Parametro;
end;
function TObjCOBRDUPLICATA.Get_observacao: string;
begin
        Result:=Self.observacao;
end;
procedure TObjCOBRDUPLICATA.Submit_nDup(parametro: string);
begin
        Self.nDup:=Parametro;
end;
function TObjCOBRDUPLICATA.Get_nDup: string;
begin
        Result:=Self.nDup;
end;
procedure TObjCOBRDUPLICATA.Submit_dVenc(parametro: string);
begin
        Self.dVenc:=Parametro;
end;
function TObjCOBRDUPLICATA.Get_dVenc: string;
begin
        Result:=Self.dVenc;
end;
procedure TObjCOBRDUPLICATA.Submit_vDup(parametro: string);
begin
        Self.vDup:=Parametro;
end;
function TObjCOBRDUPLICATA.Get_vDup: string;
begin
        Result:=Self.vDup;
end;
//CODIFICA GETSESUBMITS


procedure TObjCOBRDUPLICATA.EdtCOBRFATURAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.COBRFATURA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.COBRFATURA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.COBRFATURA.Get_observacao;
End;
procedure TObjCOBRDUPLICATA.EdtCOBRFATURAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   //FCOBRFATURA:TFCOBRFATURA;
begin

     (*If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCOBRFATURA:=TFCOBRFATURA.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.COBRFATURA.Get_Pesquisa,Self.COBRFATURA.Get_TituloPesquisa,FCOBRFATURA)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.COBRFATURA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.COBRFATURA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.COBRFATURA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCOBRFATURA);
     End;    *)
end;
//CODIFICA EXITONKEYDOWN

procedure TObjCOBRDUPLICATA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOBRDUPLICATA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



end.



