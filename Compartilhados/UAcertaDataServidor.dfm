object FacertaDataServidor: TFacertaDataServidor
  Left = 262
  Top = 190
  BorderStyle = bsToolWindow
  Caption = 'Acerto de Data do Servidor'
  ClientHeight = 71
  ClientWidth = 283
  Color = 3355443
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 23
    Height = 14
    Caption = 'Data'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 88
    Top = 16
    Width = 25
    Height = 14
    Caption = 'Hora'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 136
    Top = 16
    Width = 61
    Height = 14
    Caption = 'Verificador'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object edtdata: TMaskEdit
    Left = 8
    Top = 32
    Width = 73
    Height = 19
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
  end
  object edthora: TMaskEdit
    Left = 88
    Top = 32
    Width = 41
    Height = 19
    EditMask = '!90:00;1;_'
    MaxLength = 5
    TabOrder = 1
    Text = '  :  '
  end
  object edtverificador: TEdit
    Left = 136
    Top = 32
    Width = 89
    Height = 19
    TabOrder = 2
    Text = 'edtverificador'
  end
  object BitBtn1: TBitBtn
    Left = 232
    Top = 30
    Width = 48
    Height = 21
    Caption = 'OK'
    TabOrder = 3
    OnClick = BitBtn1Click
  end
end
