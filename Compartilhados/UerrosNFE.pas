unit UerrosNFE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls, Buttons;

type
  TFerrosNFE = class(TForm)
    stat1: TStatusBar;
    pnl1: TPanel;
    memoMensagem: TMemo;
    label2: TLabel;
    labelTotalErros: TLabel;
    label4: TLabel;
    labelTotalAtencao: TLabel;
    btConfirma: TBitBtn;
    btCancela: TBitBtn;
    labelTitle: TLabel;
    pnlImages: TPanel;
    ImagemErros: TImage;
    ImagemAtencao: TImage;
    imagemTopo: TImage;
    procedure FormShow(Sender: TObject);
    procedure btConfirmaClick(Sender: TObject);
    procedure btCancelaClick(Sender: TObject);
  private
    { Private declarations }
  public

    contErro:string;
    contAtencao:string;
    mensagem:string;

    procedure submit_contErro    (parametro:string);
    procedure submit_contAtencao (parametro:string);

    procedure adicionaMemo (parametro:string);

    procedure adicionaFigura ();

    procedure addOnClique (Sender:TObject);

  end;

var
  FerrosNFE: TFerrosNFE;

implementation

{$R *.dfm}

{ TFerrosNFE }

procedure TFerrosNFE.adicionaMemo(parametro: string);
begin

  self.memoMensagem.Lines.Clear;
  Self.memoMensagem.Lines.Text := parametro;

end;

procedure TFerrosNFE.submit_contAtencao(parametro: string);
begin

  self.contAtencao := parametro;


end;

procedure TFerrosNFE.submit_contErro(parametro: string);
begin

  self.contErro := parametro;

end;

procedure TFerrosNFE.FormShow(Sender: TObject);
begin

  if ( StrToInt (contErro) = 0) and ( StrToInt (contAtencao) > 0) then
     btConfirma.Enabled := True
  else
  begin
     btConfirma.Enabled := False;
  end;

  labelTotalErros.Caption   := contErro;
  labelTotalAtencao.Caption := contAtencao;

  //self.adicionaFigura ();

end;

procedure TFerrosNFE.btConfirmaClick(Sender: TObject);
begin

  self.Tag := 1;

end;

procedure TFerrosNFE.btCancelaClick(Sender: TObject);
begin

  Self.Tag := 2;

end;

procedure TFerrosNFE.adicionaFigura;
var
  i:Integer;
  image:TImage;
  cont:integer;
begin

  cont:=0;

  for i:=2 to memoMensagem.Lines.Count-1  do
  begin

    cont:=cont+1;
    image := TImage.Create (nil);
    image.Parent := pnlImages;
    image.Align := alTop;
    image.Transparent:=True;
    image.Height:=18;

    if (Pos('ERRO:',memoMensagem.Lines.Strings[i]) <> 0) then
    begin
        image.Picture.Bitmap := ImagemErros.Picture.Bitmap;
        image.Name:='imgErro_'+IntToStr(cont);

    end
    else if (Pos('ATEN��O:',memoMensagem.Lines.Strings[i]) <> 0) then
    begin
        image.Picture.Bitmap := ImagemAtencao.Picture.Bitmap;
        image.Name:='imgAtencao_'+IntToStr(cont);
    end;

    image.Width:=21;{n�o surte efeito}

    image.OnClick := self.addOnClique;

  end;

  imagemTopo.Top:=0;   {se tirar esse trosso daqui, ent�o: zoa todo o plant�o}
                       {espa�amento necessario para as flechas indicativas ficarem alinhadas}




end;

procedure TFerrosNFE.addOnClique(Sender: TObject);
begin

  //ShowMessage ('adicionar um evento');

end;

end.
