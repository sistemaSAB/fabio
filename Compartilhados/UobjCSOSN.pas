unit UobjCSOSN;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
;
//USES_INTERFACE


Type
   TObjCSOSN=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_codigo(parametro: string);
                Function Get_codigo: string;
                Procedure Submit_Descricao(parametro: string);
                Function Get_Descricao: string;

                function get_descricao2:string;
                procedure submit_descricao2(parametro:string);

                procedure edtCSOSNkeydown(Sender: TObject; var Key: Word; Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure edtCSOSNkeydown(Sender: TObject; var Key: Word; Shift: TShiftState);overload;


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;
               
               codigo:string;
               Descricao:string;
               descricao2:string;
//CODIFICA VARIAVEIS PRIVADAS




               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UCSOSN;





Function  TObjCSOSN.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.codigo:=fieldbyname('codigo').asstring;
        Self.Descricao:=fieldbyname('Descricao').asstring;
        self.descricao2:=fieldbyname('descricao2').AsString;
//CODIFICA TABELAPARAOBJETO


        result:=True;
     End;
end;

procedure TObjCSOSN.edtCSOSNkeydown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   Fcsosn:TFCSOSN;
begin

     If (key <>vk_f9)
     Then exit;

     Try

      Fpesquisalocal:=Tfpesquisa.create(Self.Owner);
      Fcsosn :=TFCSOSN .create(Self.Owner);

      If (FpesquisaLocal.PreparaPesquisa('select * from tabcsosn','PESQUISA DE CSOSN',Fcsosn)=True) Then
      Begin

        Try

          If (FpesquisaLocal.showmodal=mrok) Then
          Begin

            TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

          End;

        Finally

          FpesquisaLocal.QueryPesq.close;

        End;
        
      End;

     Finally

      FreeandNil(FPesquisaLocal);
      Freeandnil(Fcsosn);

     End;

end;


Procedure TObjCSOSN.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('codigo').asstring:=Self.codigo;
        ParamByName('Descricao').asstring:=Self.Descricao;
        ParamByName('descricao2').AsString:=self.descricao2;
//CODIFICA OBJETOPARATABELA


  End;
End;

//***********************************************************************

function TObjCSOSN.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCSOSN.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        codigo:='';
        Descricao:='';
        descricao2:='';
//CODIFICA ZERARTABELA


     End;
end;

Function TObjCSOSN.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (codigo='')
      Then Mensagem:=mensagem+'/codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCSOSN.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCSOSN.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/codigo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCSOSN.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCSOSN.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCSOSN.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CSOSN vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select codigo,Descricao,descricao2');
           SQL.ADD(' from  TABCSOSN');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCSOSN.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjCSOSN.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjCSOSN.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjCSOSN.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABCSOSN(codigo,Descricao,descricao2)');
                InsertSQL.add('values (:codigo,:Descricao,:descricao2)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABCSOSN set codigo=:codigo,Descricao=:Descricao,descricao2=:descricao2');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABCSOSN where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCSOSN.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCSOSN.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCSOSN');
     Result:=Self.ParametroPesquisa;
end;

function TObjCSOSN.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CSOSN ';
end;


function TObjCSOSN.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCSOSN,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCSOSN,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCSOSN.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCSOSN.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCSOSN.RetornaCampoNome: string;
begin
      result:='Descricao';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCSOSN.Submit_codigo(parametro: string);
begin
        Self.codigo:=Parametro;
end;
function TObjCSOSN.Get_codigo: string;
begin
        Result:=Self.codigo;
end;
procedure TObjCSOSN.Submit_Descricao(parametro: string);
begin
        Self.Descricao:=Parametro;
end;
function TObjCSOSN.Get_Descricao: string;
begin
        Result:=Self.Descricao;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjCSOSN.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCSOSN';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



function TObjCSOSN.get_descricao2: string;
begin
  result := self.descricao2;
end;

procedure TObjCSOSN.submit_descricao2(parametro: string);
begin
  self.descricao2:=parametro;
end;


procedure TObjCSOSN.edtCSOSNkeydown(Sender: TObject; var Key: Word; Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fcsosn:TFCSOSN;
begin

     If (key <>vk_f9)
     Then exit;

     Try

      Fpesquisalocal:=Tfpesquisa.create(Self.Owner);
      Fcsosn :=TFCSOSN .create(Self.Owner);

      If (FpesquisaLocal.PreparaPesquisa('select * from tabcsosn','PESQUISA DE CSOSN',Fcsosn)=True) Then
      Begin

        Try

          If (FpesquisaLocal.showmodal=mrok) Then
          Begin

            TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;

            If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) Then
              LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('descricao').asstring


          End;

        Finally

          FpesquisaLocal.QueryPesq.close;

        End;
        
      End;

     Finally

      FreeandNil(FPesquisaLocal);
      Freeandnil(Fcsosn);

     End;

end;

end.



