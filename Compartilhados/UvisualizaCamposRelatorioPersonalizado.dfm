object FvisualizaCamposRelatorioPersonalizado: TFvisualizaCamposRelatorioPersonalizado
  Left = 183
  Top = 179
  BorderStyle = bsToolWindow
  Caption = 'Campos que podem ser utilizados no Relat'#243'rio Personalizado Atual'
  ClientHeight = 395
  ClientWidth = 586
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnDblClick = FormShow
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LbCamposRepeticao: TListBox
    Left = 282
    Top = 41
    Width = 304
    Height = 354
    Align = alRight
    ItemHeight = 13
    TabOrder = 0
    OnDblClick = LbCamposRepeticaoDblClick
  end
  object LbCampos: TListBox
    Left = 0
    Top = 41
    Width = 281
    Height = 354
    Align = alLeft
    ItemHeight = 13
    TabOrder = 1
    OnDblClick = LbCamposDblClick
  end
  object lbtitulo: TPanel
    Left = 0
    Top = 0
    Width = 586
    Height = 41
    Align = alTop
    Alignment = taLeftJustify
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
  end
  object QueryCampos: TIBQuery
    Left = 144
  end
  object QueryCamposRepeticao: TIBQuery
    Left = 184
  end
end
