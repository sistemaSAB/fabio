unit ULabelRel;

interface
uses StdCtrls,ExtCtrls,sysutils,graphics,Uregua;

type

    TLabelRel = class(TLabel)
    private
           //variaveis que armazenar�o a posi��o da linha
           //e da coluna na r�gua
           linha:integer;
           coluna:integer;
    public
           //Indice que a label ocupar� no vetor
           posicaovetor:integer;
           //Quantidade de caracteres que ser� impresso
           QuantidadeCaracteres:Integer;
           //Se for um campo do Banco de Dados, o nome do mesmo
           nomecampoBD:String;
           //Semelhante ao Caption
           rotulo:String;
           //Indica se � uma label de R�tulo ou Dados
           tipo:String[1];//R  (r�tulo) ou D (Dados)
           //Se Ser� impressa ou n�o
           Imprime:Boolean;
           //Fun��es que retornam a LInha e a Coluna Atual
           Function Get_linha:integer;
           Function Get_coluna:integer;
           //Fun��o que reposiciona a Label dentro da R�gua
           procedure Reposiciona(Pcoluna,Plinha:integer);
    End;


implementation


{ TLabelRel }

//*****Fun��es que retornam a coluna e a linha atual
function TLabelRel.Get_coluna: integer;
begin
     Result:=Self.Coluna;
end;

function TLabelRel.Get_linha: integer;
begin
     Result:=Self.linha;
end;
//***************************************************

procedure TLabelRel.Reposiciona(Pcoluna,Plinha:integer);
begin
     //Fun��o que reposiciona a label na r�gua.
     Self.Coluna:=Pcoluna;
     Self.Linha:=Plinha;
     Self.Left:=Uregua.RetornaColunaPixel(Self.coluna);
     Self.Top :=Uregua.RetornaLinhaPixel(Self.linha);
end;

end.
