unit UmostraStringGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, ExtCtrls, FileCtrl;

type
  TFmostraStringGrid = class(TForm)
    StringGrid: TStringGrid;
    Panel1: TPanel;
    btcancelar: TBitBtn;
    Btok: TBitBtn;
    LabelMensagem: TLabel;
    PanelRodape: TPanel;
    lbrodape: TLabel;
    FileListBoxIMportacao: TFileListBox;
    procedure btcancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtokClick(Sender: TObject);
    procedure StringGridDblClick(Sender: TObject);
    procedure StringGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure Configuracoesiniciais;
  end;

var
  FmostraStringGrid: TFmostraStringGrid;

implementation

{$R *.dfm}

procedure TFmostraStringGrid.btcancelarClick(Sender: TObject);
begin
     Self.Tag:=0;
     Self.Close;
end;

procedure TFmostraStringGrid.FormShow(Sender: TObject);
begin
     Self.Tag:=0;
     Self.StringGrid.SetFocus;
end;

procedure TFmostraStringGrid.BtokClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;
end;

procedure TFmostraStringGrid.StringGridDblClick(Sender: TObject);
begin
     BtokClick(sender);
end;

procedure TFmostraStringGrid.StringGridKeyPress(Sender: TObject;
  var Key: Char);
begin
     if (key=#13)
     Then BtokClick(sender);
end;

procedure TFmostraStringGrid.Configuracoesiniciais;
begin
     Self.StringGrid.ColCount:=1;
     Self.StringGrid.RowCount:=2;
     Self.StringGrid.FixedCols:=0;
     Self.StringGrid.FixedRows:=1;
     Self.StringGrid.Cols[0].clear;
     Self.caption:='Escolha uma Op��o';
end;

procedure TFmostraStringGrid.FormCreate(Sender: TObject);
begin
     Self.panelrodape.visible:=False;
end;

procedure TFmostraStringGrid.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Self.panelrodape.visible:=False;
     Self.lbrodape.caption:='';
end;

procedure TFmostraStringGrid.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if (key=#27)
     then Self.btcancelarClick(sender);

end;

end.
