unit UvisualizaCamposRelatorioPersonalizado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, IBCustomDataSet, IBQuery, StdCtrls;

type
  TFvisualizaCamposRelatorioPersonalizado = class(TForm)
    QueryCampos: TIBQuery;
    QueryCamposRepeticao: TIBQuery;
    LbCamposRepeticao: TListBox;
    LbCampos: TListBox;
    lbtitulo: TPanel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure LbCamposDblClick(Sender: TObject);
    procedure LbCamposRepeticaoDblClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }

    pTIPO,PVALOR:string;
    TabelaCampos:string;
    TabelaCamposRepeticao:string;
    CampoEscolhido:String;
  end;

var
  FvisualizaCamposRelatorioPersonalizado: TFvisualizaCamposRelatorioPersonalizado;

implementation

uses UDataModulo;

{$R *.dfm}

procedure TFvisualizaCamposRelatorioPersonalizado.FormShow(
  Sender: TObject);
var
cont:integer;
begin
     Self.CampoEscolhido:='';
     Self.LbCampos.Items.clear;
     Self.LbCamposRepeticao.Items.clear;

     Self.LbCampos.Visible:=False;
     Self.LbCamposRepeticao.Visible:=False;

     Self.QueryCampos.close;
     Self.QueryCamposRepeticao.close;
     Self.QueryCampos.Database:=FdataModulo.ibdatabase;
     Self.QueryCamposRepeticao.Database:=FdataModulo.ibdatabase;

     Self.QueryCampos.sql.clear;


     if (Self.pTIPO<>'R')
     Then Begin 
               if ((pos('SELECT ',uppercase(Self.TabelaCampos))>0)
               and (pos('FROM ',uppercase(Self.TabelaCampos))>0))
               Then Self.QueryCampos.sql.add(Self.TabelaCampos)//se tiver select e from � comando separado
               Else Self.QueryCampos.sql.add('Select * from '+TabelaCampos);
               
               
               if (tabelacampos<>'')
               Then Begin
                         Try
                            Self.QueryCampos.open;
                            for cont:=0 to Self.QueryCampos.Fields.Count-1 do
                            Begin
                                 Self.LbCampos.Items.add(Self.QueryCampos.Fields[cont].FieldName);
                            End;
                            Self.QueryCampos.Close;
                         Except
               
                         End;
               End;
               Self.LbCampos.Visible:=True;
               Self.LbCampos.Align:=alClient;
     End
     Else Begin//repeticao
              Self.QueryCamposRepeticao.sql.clear;

              if ((pos('SELECT ',uppercase(Self.TabelaCamposRepeticao))>0)
              and (pos('FROM ',uppercase(Self.TabelaCamposRepeticao))>0))
              Then Self.QueryCamposRepeticao.sql.add(Self.TabelaCamposRepeticao)//se tiver select e from � comando separado
              Else Self.QueryCamposRepeticao.sql.add('Select * from '+TabelaCamposRepeticao);
              
              
              if (TabelaCamposRepeticao<>'')
              Then Begin
                        Try
                           Self.QueryCamposRepeticao.open;
                           for cont:=0 to Self.QueryCamposRepeticao.Fields.Count-1 do
                           Begin
                                Self.LbCamposRepeticao.Items.add(Self.QueryCamposRepeticao.Fields[cont].FieldName);
                           End;
                           Self.QueryCamposRepeticao.Close;
                        Except
              
                        End;
              End;
              Self.LbCamposRepeticao.Visible:=True;
              Self.LbCamposRepeticao.Align:=alClient;
     End;

     if (Self.PVALOR<>'')
     Then Begin
               if (self.pTIPO='R')
               Then Begin
                        Self.LbCamposRepeticao.ItemIndex:=Self.LbCamposRepeticao.items.IndexOf(self.PVALOR);
                        self.lbtitulo.Caption:='CAMPOS COM REPETI��O';
               End 
               Else Begin
                        Self.LbCampos.ItemIndex:=Self.LbCampos.items.IndexOf(self.PVALOR);
                        self.lbtitulo.Caption:='CAMPOS SEM REPETI��O';
               End;

     End;
end;

procedure TFvisualizaCamposRelatorioPersonalizado.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Self.QueryCampos.close;
     Self.QueryCamposRepeticao.close;

     if (Self.CampoEscolhido='')
     Then Begin
               if (Self.LbCampos.Visible=true)
               and (self.LbCampos.ItemIndex>=0)
               Then Self.CampoEscolhido:=Self.LbCampos.Items[Self.LbCampos.itemindex];

               if (Self.LbCamposRepeticao.Visible=true)
               and (self.LbCamposRepeticao.ItemIndex>=0)
               Then Self.CampoEscolhido:=Self.LbCamposRepeticao.Items[Self.LbCamposRepeticao.itemindex];
     End;

end;

procedure TFvisualizaCamposRelatorioPersonalizado.LbCamposDblClick(
  Sender: TObject);
begin
     Self.campoescolhido:=LbCampos.Items[LbCampos.itemindex];
     Self.Close;
end;

procedure TFvisualizaCamposRelatorioPersonalizado.LbCamposRepeticaoDblClick(
  Sender: TObject);
begin
     Self.campoescolhido:=LbCamposRepeticao.Items[LbCamposRepeticao.itemindex];
     Self.Close;
end;

procedure TFvisualizaCamposRelatorioPersonalizado.FormKeyPress(
  Sender: TObject; var Key: Char);
begin
     if (key=#27)
     Then Self.Close;
end;

end.
