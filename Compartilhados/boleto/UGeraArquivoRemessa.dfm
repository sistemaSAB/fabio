object FGeraArquivoRemessa: TFGeraArquivoRemessa
  Left = 468
  Top = 277
  Width = 746
  Height = 432
  Caption = 'Gera Arquivo de Remessa'
  Color = clMenu
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbnomeformulario: TLabel
    Left = 299
    Top = 0
    Width = 417
    Height = 32
    Caption = 'Gera'#231#227'o de arquivo de remessa'
    Font.Charset = ANSI_CHARSET
    Font.Color = 6710886
    Font.Height = -27
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Guia: TTabbedNotebook
    Left = 11
    Top = 33
    Width = 712
    Height = 360
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    TabFont.Charset = ANSI_CHARSET
    TabFont.Color = clBlack
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = [fsBold]
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = 'BOLETOS SEM REMESSA'
      object StrGrid: TStringGrid
        Left = 8
        Top = 34
        Width = 691
        Height = 256
        FixedCols = 0
        TabOrder = 0
      end
      object btGerar: TBitBtn
        Left = 540
        Top = 295
        Width = 158
        Height = 32
        Caption = 'Gerar Remessa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = btGerarClick
      end
      object btBoletosSemRemessa: TBitBtn
        Left = 9
        Top = 5
        Width = 162
        Height = 25
        Caption = 'Mostrar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = btBoletosSemRemessaClick
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = 'BOLETOS COM REMESSA (Instru'#231#245'es)'
      object Label1: TLabel
        Left = 4
        Top = 35
        Width = 30
        Height = 14
        Caption = 'Boleto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 2
        Width = 45
        Height = 14
        Caption = 'Convenio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 4
        Top = 88
        Width = 45
        Height = 14
        Caption = 'Instru'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
      end
      object lbBoleto: TLabel
        Left = 179
        Top = 31
        Width = 5
        Height = 16
        Caption = '.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbConvenio: TLabel
        Left = 178
        Top = 5
        Width = 5
        Height = 16
        Caption = '.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clMaroon
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object ComboBoxIntrucoes: TComboBox
        Left = 73
        Top = 82
        Width = 280
        Height = 24
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 16
        ParentFont = False
        TabOrder = 2
        OnKeyUp = ComboBoxIntrucoesKeyUp
      end
      object ComboBoxCodigoInstrucoes: TComboBox
        Left = 643
        Top = 2
        Width = 56
        Height = 21
        ItemHeight = 13
        TabOrder = 3
        Visible = False
      end
      object StrGridIntrucao: TStringGrid
        Left = 5
        Top = 111
        Width = 694
        Height = 188
        ColCount = 6
        FixedCols = 0
        RowCount = 2
        TabOrder = 4
      end
      object EdtBoleto: TEdit
        Left = 73
        Top = 30
        Width = 103
        Height = 19
        TabOrder = 1
        OnExit = EdtBoletoExit
        OnKeyDown = EdtBoletoKeyDown
      end
      object EdtConvenio: TEdit
        Left = 73
        Top = 3
        Width = 103
        Height = 19
        TabOrder = 0
        OnExit = EdtConvenioExit
        OnKeyDown = EdtConvenioKeyDown
      end
      object btAdicionarBoleto: TBitBtn
        Left = 370
        Top = 81
        Width = 162
        Height = 25
        Caption = 'Adicionar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        OnClick = btAdicionarBoletoClick
      end
      object btExcluirLinha: TBitBtn
        Left = 536
        Top = 80
        Width = 162
        Height = 25
        Caption = 'Excluir Linha'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        OnClick = btExcluirLinhaClick
      end
      object btGeraRemessaInstrucao: TBitBtn
        Left = 543
        Top = 300
        Width = 158
        Height = 32
        Caption = 'Gerar Remessa'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        OnClick = btGeraRemessaInstrucaoClick
      end
    end
  end
end
