unit UREGISTRORETORNOCOBRANCA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjREGISTRORETORNOCOBRANCA;

type
  TFREGISTRORETORNOCOBRANCA = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel2: TPanel;
    ImagemFundo: TImage;
    LbLoteRetorno: TLabel;
    LbNomeLoteRetorno: TLabel;
    LbNossoNumero: TLabel;
    LbASeuNumero: TLabel;
    LbADataVencimento: TLabel;
    LbAvalorDocumento: TLabel;
    LbTipoOcorrencia: TLabel;
    LbValorJuros: TLabel;
    LbValorDesconto: TLabel;
    LbValorAbatimento: TLabel;
    LbValorIOF: TLabel;
    LbValorOutrasDespesas: TLabel;
    LbValorOutrosCreditos: TLabel;
    LbDataOcorrencia: TLabel;
    LbDataCredito: TLabel;
    LbCODIGO: TLabel;
    Lbnomeocorrencia: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lbnomelancamento: TLabel;
    EdtLoteRetorno: TEdit;
    EdtNossoNumero: TEdit;
    EdtSeuNumero: TEdit;
    EdtDataVencimento: TMaskEdit;
    EdtvalorDocumento: TEdit;
    EdtOcorrencia: TEdit;
    EdtValorJuros: TEdit;
    EdtValorDesconto: TEdit;
    EdtValorAbatimento: TEdit;
    EdtValorIOF: TEdit;
    EdtValorOutrasDespesas: TEdit;
    EdtValorOutrosCreditos: TEdit;
    EdtDataOcorrencia: TMaskEdit;
    EdtDataCredito: TMaskEdit;
    EdtCODIGO: TEdit;
    ComboProcessado: TComboBox;
    edtboleto: TEdit;
    edtsituacao: TEdit;
    edtlancamento: TEdit;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btalterar: TBitBtn;
    btsair: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btpesquisar: TBitBtn;
    btopcoes: TBitBtn;
    procedure edtLoteRetornoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtLoteRetornoExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdtOcorrenciaExit(Sender: TObject);
    procedure EdtOcorrenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtboletoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtlancamentoExit(Sender: TObject);
    procedure edtlancamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         ObjREGISTRORETORNOCOBRANCA:TObjREGISTRORETORNOCOBRANCA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FREGISTRORETORNOCOBRANCA: TFREGISTRORETORNOCOBRANCA;


implementation

uses UessencialGlobal, Upesquisa, UObjLancamento, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFREGISTRORETORNOCOBRANCA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjREGISTRORETORNOCOBRANCA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        LoteRetorno.Submit_codigo(edtLoteRetorno.text);
        Submit_NossoNumero(edtNossoNumero.text);
        Submit_SeuNumero(edtSeuNumero.text);
        Submit_DataVencimento(edtDataVencimento.text);
        Submit_valorDocumento(edtvalorDocumento.text);
        Ocorrencia.Submit_codigo(edtOcorrencia.text);
        Submit_ValorJuros(edtValorJuros.text);
        Submit_ValorDesconto(edtValorDesconto.text);
        Submit_ValorAbatimento(edtValorAbatimento.text);
        Submit_ValorIOF(edtValorIOF.text);
        Submit_ValorOutrasDespesas(edtValorOutrasDespesas.text);
        Submit_ValorOutrosCreditos(edtValorOutrosCreditos.text);
        Submit_DataOcorrencia(edtDataOcorrencia.text);
        Submit_DataCredito(edtDataCredito.text);
        Submit_Processado(Submit_ComboBox(ComboProcessado));
        Boleto.Submit_CODIGO(edtboleto.Text);
        Submit_Situacao(edtsituacao.Text);
        Lancamento.Submit_CODIGO(edtlancamento.text);

         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFREGISTRORETORNOCOBRANCA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjREGISTRORETORNOCOBRANCA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtLoteRetorno.text:=LoteRetorno.Get_codigo;
        EdtNossoNumero.text:=Get_NossoNumero;
        EdtSeuNumero.text:=Get_SeuNumero;
        EdtDataVencimento.text:=Get_DataVencimento;
        EdtvalorDocumento.text:=Get_valorDocumento;

        EdtOcorrencia.text        :=Ocorrencia.Get_Codigo;
        lbnomeocorrencia.caption  :=Ocorrencia.get_nome;

        EdtValorJuros.text:=Get_ValorJuros;
        EdtValorDesconto.text:=Get_ValorDesconto;
        EdtValorAbatimento.text:=Get_ValorAbatimento;
        EdtValorIOF.text:=Get_ValorIOF;
        EdtValorOutrasDespesas.text:=Get_ValorOutrasDespesas;
        EdtValorOutrosCreditos.text:=Get_ValorOutrosCreditos;
        EdtDataOcorrencia.text:=Get_DataOcorrencia;
        EdtDataCredito.text:=Get_DataCredito;
        edtboleto.Text:=Boleto.Get_CODIGO;
        if (Get_Processado='S')
        Then ComboProcessado.itemindex:=1
        Else ComboProcessado.itemindex:=0;

        edtsituacao.Text:=Get_Situacao;
        edtlancamento.text:=lancamento.Get_CODIGO;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFREGISTRORETORNOCOBRANCA.TabelaParaControles: Boolean;
begin
     If (Self.ObjREGISTRORETORNOCOBRANCA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFREGISTRORETORNOCOBRANCA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjREGISTRORETORNOCOBRANCA=Nil)
     Then exit;

    If (Self.ObjREGISTRORETORNOCOBRANCA.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjREGISTRORETORNOCOBRANCA.free;
end;

procedure TFREGISTRORETORNOCOBRANCA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFREGISTRORETORNOCOBRANCA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjREGISTRORETORNOCOBRANCA.Get_novocodigo;
     edtcodigo.enabled:=False;
    
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjREGISTRORETORNOCOBRANCA.status:=dsInsert;
     Edtboleto.setfocus;

end;


procedure TFREGISTRORETORNOCOBRANCA.btalterarClick(Sender: TObject);
begin
    If (Self.ObjREGISTRORETORNOCOBRANCA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjREGISTRORETORNOCOBRANCA.Status:=dsEdit;
                Edtboleto.setfocus;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
          End;


end;

procedure TFREGISTRORETORNOCOBRANCA.btgravarClick(Sender: TObject);
begin

     If Self.ObjREGISTRORETORNOCOBRANCA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjREGISTRORETORNOCOBRANCA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjREGISTRORETORNOCOBRANCA.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFREGISTRORETORNOCOBRANCA.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjREGISTRORETORNOCOBRANCA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjREGISTRORETORNOCOBRANCA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjREGISTRORETORNOCOBRANCA.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFREGISTRORETORNOCOBRANCA.btcancelarClick(Sender: TObject);
begin
     Self.ObjREGISTRORETORNOCOBRANCA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFREGISTRORETORNOCOBRANCA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFREGISTRORETORNOCOBRANCA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjREGISTRORETORNOCOBRANCA.Get_pesquisa,Self.ObjREGISTRORETORNOCOBRANCA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjREGISTRORETORNOCOBRANCA.status<>dsinactive
                                  then exit;

                                  If (Self.ObjREGISTRORETORNOCOBRANCA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjREGISTRORETORNOCOBRANCA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFREGISTRORETORNOCOBRANCA.LimpaLabels;
begin
     LbNomeLoteRetorno.caption:='';
     LbNomeOcorrencia.caption :='';
     lbnomelancamento.caption :='';
end;

procedure TFREGISTRORETORNOCOBRANCA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjREGISTRORETORNOCOBRANCA:=TObjREGISTRORETORNOCOBRANCA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
      FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
      FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
      FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
      Self.Color:=clwhite;
      retira_fundo_labels(self);
      Uessencialglobal.PegaCorForm(Self);
end;
procedure TFREGISTRORETORNOCOBRANCA.edtLoteRetornoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjREGISTRORETORNOCOBRANCA.edtLoteRetornokeydown(sender,key,shift,lbnomeLoteRetorno);
end;
 
procedure TFREGISTRORETORNOCOBRANCA.edtLoteRetornoExit(Sender: TObject);
begin
    ObjREGISTRORETORNOCOBRANCA.edtLoteRetornoExit(sender,lbnomeLoteRetorno);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFREGISTRORETORNOCOBRANCA.EdtOcorrenciaExit(Sender: TObject);
begin
     ObjREGISTRORETORNOCOBRANCA.EdtOcorrenciaExit(sender,Lbnomeocorrencia);
end;

procedure TFREGISTRORETORNOCOBRANCA.EdtOcorrenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjREGISTRORETORNOCOBRANCA.EdtOcorrenciaKeyDown(sender,key,shift,Lbnomeocorrencia);
end;

procedure TFREGISTRORETORNOCOBRANCA.edtboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjREGISTRORETORNOCOBRANCA.edtboletokeydown(sender,key,shift,nil);
end;

procedure TFREGISTRORETORNOCOBRANCA.edtlancamentoExit(Sender: TObject);
begin
     ObjREGISTRORETORNOCOBRANCA.EdtlancamentoExit(sender,lbnomelancamento);
end;

procedure TFREGISTRORETORNOCOBRANCA.edtlancamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjREGISTRORETORNOCOBRANCA.EdtlancamentoKeyDown(sender,key,shift,lbnomelancamento);
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjREGISTRORETORNOCOBRANCA.OBJETO.Get_Pesquisa,Self.ObjREGISTRORETORNOCOBRANCA.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_
NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjREGISTRORETORNOCOBRANCA.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjREGISTRORETORNOCOBRANCA.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjREGISTRORETORNOCOBRANCA.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
