unit UBOLETOINSTRUCOES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjBOLETOINSTRUCOES,
  jpeg;

type
  TFBOLETOINSTRUCOES = class(TForm)
    Guia: TTabbedNotebook;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    painel_imagem: TPanel;
    imcesta: TImage;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    ImagemFundo: TImage;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbBoleto: TLabel;
    EdtBoleto: TEdit;
    LbNomeBoleto: TLabel;
    LbInstrucao: TLabel;
    EdtInstrucao: TEdit;
    LbNomeInstrucao: TLabel;
    LbOcorrencia: TLabel;
    EdtOcorrencia: TEdit;
    LbNomeOcorrencia: TLabel;
    LbArquivoRemessaRetorno: TLabel;
    EdtArquivoRemessaRetorno: TEdit;
    LbNomeArquivoRemessaRetorno: TLabel;
    LbData: TLabel;
    EdtData: TMaskEdit;

    procedure edtBoletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtBoletoExit(Sender: TObject);
    procedure edtOcorrenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtOcorrenciaExit(Sender: TObject);
    procedure edtArquivoRemessaRetornoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtArquivoRemessaRetornoExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjBOLETOINSTRUCOES:TObjBOLETOINSTRUCOES;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FBOLETOINSTRUCOES: TFBOLETOINSTRUCOES;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFBOLETOINSTRUCOES.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjBOLETOINSTRUCOES do
    Begin
        Submit_Codigo(edtCodigo.text);
        BoletoBancario.Submit_codigo(edtBoleto.text);
        Submit_Instrucao(edtInstrucao.text);
        Ocorrencia.Submit_codigo(edtOcorrencia.text);
        ArquivoRemessaRetorno.Submit_codigo(edtArquivoRemessaRetorno.text);
        Submit_Data(edtData.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFBOLETOINSTRUCOES.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjBOLETOINSTRUCOES do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtBoleto.text:=BoletoBancario.Get_codigo;
        EdtInstrucao.text:=Get_Instrucao;
        EdtOcorrencia.text:=Ocorrencia.Get_codigo;
        EdtArquivoRemessaRetorno.text:=ArquivoRemessaRetorno.Get_codigo;
        EdtData.text:=Get_Data;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFBOLETOINSTRUCOES.TabelaParaControles: Boolean;
begin
     If (Self.ObjBOLETOINSTRUCOES.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFBOLETOINSTRUCOES.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjBOLETOINSTRUCOES:=TObjBOLETOINSTRUCOES.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
end;

procedure TFBOLETOINSTRUCOES.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjBOLETOINSTRUCOES=Nil)
     Then exit;

     If (Self.ObjBOLETOINSTRUCOES.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjBOLETOINSTRUCOES.free;
end;

procedure TFBOLETOINSTRUCOES.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFBOLETOINSTRUCOES.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjBOLETOINSTRUCOES.status:=dsInsert;
     Guia.pageindex:=0;
     edtBoleto.setfocus;

end;


procedure TFBOLETOINSTRUCOES.btalterarClick(Sender: TObject);
begin
    If (Self.ObjBOLETOINSTRUCOES.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjBOLETOINSTRUCOES.Status:=dsEdit;
                guia.pageindex:=0;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtBoleto.setfocus;
                
    End;


end;

procedure TFBOLETOINSTRUCOES.btgravarClick(Sender: TObject);
begin

     If Self.ObjBOLETOINSTRUCOES.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjBOLETOINSTRUCOES.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjBOLETOINSTRUCOES.Get_codigo;
     Self.ObjBOLETOINSTRUCOES.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFBOLETOINSTRUCOES.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjBOLETOINSTRUCOES.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjBOLETOINSTRUCOES.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjBOLETOINSTRUCOES.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFBOLETOINSTRUCOES.btcancelarClick(Sender: TObject);
begin
     Self.ObjBOLETOINSTRUCOES.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFBOLETOINSTRUCOES.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFBOLETOINSTRUCOES.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjBOLETOINSTRUCOES.Get_pesquisa,Self.ObjBOLETOINSTRUCOES.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjBOLETOINSTRUCOES.status<>dsinactive
                                  then exit;

                                  If (Self.ObjBOLETOINSTRUCOES.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjBOLETOINSTRUCOES.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFBOLETOINSTRUCOES.LimpaLabels;
begin
     LbNomeBoleto.Caption:='';
     LbNomeInstrucao.Caption:='';
     LbNomeOcorrencia.Caption:='';
     LbNomeArquivoRemessaRetorno.Caption:='';
end;

procedure TFBOLETOINSTRUCOES.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
procedure TFBOLETOINSTRUCOES.edtBoletoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjBOLETOINSTRUCOES.edtBoletobancariokeydown(sender,key,shift,lbnomeBoleto);
end;
 
procedure TFBOLETOINSTRUCOES.edtBoletoExit(Sender: TObject);
begin
    ObjBOLETOINSTRUCOES.edtBoletoBancarioExit(sender,lbnomeBoleto);
end;
procedure TFBOLETOINSTRUCOES.edtOcorrenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjBOLETOINSTRUCOES.edtOcorrenciakeydown(sender,key,shift,lbnomeOcorrencia);
end;
 
procedure TFBOLETOINSTRUCOES.edtOcorrenciaExit(Sender: TObject);
begin
    ObjBOLETOINSTRUCOES.edtOcorrenciaExit(sender,lbnomeOcorrencia);
end;
procedure TFBOLETOINSTRUCOES.edtArquivoRemessaRetornoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjBOLETOINSTRUCOES.edtArquivoRemessaRetornokeydown(sender,key,shift,lbnomeArquivoRemessaRetorno);
end;
 
procedure TFBOLETOINSTRUCOES.edtArquivoRemessaRetornoExit(Sender: TObject);
begin
    ObjBOLETOINSTRUCOES.edtArquivoRemessaRetornoExit(sender,lbnomeArquivoRemessaRetorno);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

