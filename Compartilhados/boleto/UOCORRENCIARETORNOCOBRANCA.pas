unit UOCORRENCIARETORNOCOBRANCA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjOCORRENCIARETORNOCOBRANCA;

type
  TFOCORRENCIARETORNOCOBRANCA = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btalterar: TBitBtn;
    btrelatorios: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    LbCODIGO: TLabel;
    LbNome: TLabel;
    LbLiquidaPendencia: TLabel;
    EdtCODIGO: TEdit;
    EdtNome: TEdit;
    EdtLiquidaPendencia: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjOCORRENCIARETORNOCOBRANCA:TObjOCORRENCIARETORNOCOBRANCA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         function  atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOCORRENCIARETORNOCOBRANCA: TFOCORRENCIARETORNOCOBRANCA;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFOCORRENCIARETORNOCOBRANCA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjOCORRENCIARETORNOCOBRANCA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
        Submit_LiquidaPendencia(edtLiquidaPendencia.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFOCORRENCIARETORNOCOBRANCA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjOCORRENCIARETORNOCOBRANCA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtLiquidaPendencia.text:=Get_LiquidaPendencia;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFOCORRENCIARETORNOCOBRANCA.TabelaParaControles: Boolean;
begin
     If (Self.ObjOCORRENCIARETORNOCOBRANCA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFOCORRENCIARETORNOCOBRANCA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjOCORRENCIARETORNOCOBRANCA=Nil)
     Then exit;

    If (Self.ObjOCORRENCIARETORNOCOBRANCA.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjOCORRENCIARETORNOCOBRANCA.free;
end;

procedure TFOCORRENCIARETORNOCOBRANCA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFOCORRENCIARETORNOCOBRANCA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     Self.ObjOCORRENCIARETORNOCOBRANCA.status:=dsInsert;
     Edtcodigo.setfocus;

end;


procedure TFOCORRENCIARETORNOCOBRANCA.btalterarClick(Sender: TObject);
begin
    If (Self.ObjOCORRENCIARETORNOCOBRANCA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjOCORRENCIARETORNOCOBRANCA.Status:=dsEdit;
                edtnome.setfocus;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
          End;


end;

procedure TFOCORRENCIARETORNOCOBRANCA.btgravarClick(Sender: TObject);
begin

     If Self.ObjOCORRENCIARETORNOCOBRANCA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjOCORRENCIARETORNOCOBRANCA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjOCORRENCIARETORNOCOBRANCA.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFOCORRENCIARETORNOCOBRANCA.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjOCORRENCIARETORNOCOBRANCA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjOCORRENCIARETORNOCOBRANCA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjOCORRENCIARETORNOCOBRANCA.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFOCORRENCIARETORNOCOBRANCA.btcancelarClick(Sender: TObject);
begin
     Self.ObjOCORRENCIARETORNOCOBRANCA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFOCORRENCIARETORNOCOBRANCA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFOCORRENCIARETORNOCOBRANCA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjOCORRENCIARETORNOCOBRANCA.Get_pesquisa,Self.ObjOCORRENCIARETORNOCOBRANCA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjOCORRENCIARETORNOCOBRANCA.status<>dsinactive
                                  then exit;

                                  If (Self.ObjOCORRENCIARETORNOCOBRANCA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjOCORRENCIARETORNOCOBRANCA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFOCORRENCIARETORNOCOBRANCA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFOCORRENCIARETORNOCOBRANCA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjOCORRENCIARETORNOCOBRANCA:=TObjOCORRENCIARETORNOCOBRANCA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;
//CODIFICA ONKEYDOWN E ONEXIT


function TFOCORRENCIARETORNOCOBRANCA.atualizaQuantidade: string;
begin
     result:='Existe '+ContaRegistros('TABOCORRENCIARETORNOCOBRANCA','codigo')+' Ocorr�ncias Cadastradas';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjOCORRENCIARETORNOCOBRANCA.OBJETO.Get_Pesquisa,Self.ObjOCORRENCIARETORNOCOBRANCA.OBJETO.Get_TituloPesquisa,NOMEDOFORM
_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjOCORRENCIARETORNOCOBRANCA.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjOCORRENCIARETORNOCOBRANCA.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjOCORRENCIARETORNOCOBRANCA.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
