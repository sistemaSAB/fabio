object FHistoricoBoleto: TFHistoricoBoleto
  Left = 177
  Top = 85
  Width = 861
  Height = 480
  Caption = 'Historico Boleto'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbConvenio: TLabel
    Left = 112
    Top = 22
    Width = 5
    Height = 16
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbBoleto: TLabel
    Left = 112
    Top = 57
    Width = 5
    Height = 16
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 4
    Top = 3
    Width = 66
    Height = 16
    Caption = 'Convenio'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 4
    Top = 40
    Width = 46
    Height = 16
    Caption = 'Boleto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object EdtConvenio: TEdit
    Left = 4
    Top = 19
    Width = 103
    Height = 21
    TabOrder = 0
    OnExit = EdtConvenioExit
    OnKeyDown = EdtConvenioKeyDown
  end
  object EdtBoleto: TEdit
    Left = 4
    Top = 56
    Width = 103
    Height = 21
    TabOrder = 1
    OnExit = EdtBoletoExit
    OnKeyDown = EdtBoletoKeyDown
  end
  object Memo: TMemo
    Left = 6
    Top = 84
    Width = 845
    Height = 367
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      'Memo')
    ParentFont = False
    TabOrder = 2
  end
end
