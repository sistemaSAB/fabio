unit ULOTERETORNOCOBRANCA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjRegistroRETORNOCOBRANCA,
  Grids, DBGrids;

type
  TFLOTERETORNOCOBRANCA = class(TForm)
    Guia: TTabbedNotebook;
    STRGPendencias: TStringGrid;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    BtOpcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    ImagemFundo: TImage;
    DBGridRegistros: TDBGrid;
    LbCODIGO: TLabel;
    LbDATAHORA: TLabel;
    LbNOMEARQUIVO: TLabel;
    LbDATAARQUIVO: TLabel;
    LbNUMEROARQUIVO: TLabel;
    LbCGCEMPRESA: TLabel;
    LbCODIGOCEDENTE: TLabel;
    LbAGENCIA: TLabel;
    LbDIGITOAGENCIA: TLabel;
    LbCONTA: TLabel;
    LbDIGITOCONTA: TLabel;
    Label1: TLabel;
    lbnomeconvenio: TLabel;
    Label2: TLabel;
    EdtDATAHORA: TMaskEdit;
    EdtNOMEARQUIVO: TEdit;
    EdtDATAARQUIVO: TEdit;
    EdtNUMEROARQUIVO: TEdit;
    EdtCGCEMPRESA: TEdit;
    EdtCODIGOCEDENTE: TEdit;
    EdtAGENCIA: TEdit;
    EdtDIGITOAGENCIA: TEdit;
    EdtCONTA: TEdit;
    EdtDIGITOCONTA: TEdit;
    EdtCODIGO: TEdit;
    edtconvenioboleto: TEdit;
    ComboProcessado: TComboBox;
    Shape1: TShape;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure edtconvenioboletoExit(Sender: TObject);
    procedure edtconvenioboletoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btrelatoriosClick(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         Procedure RetornaRegistros;
         Procedure RetornaPendencias;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLOTERETORNOCOBRANCA: TFLOTERETORNOCOBRANCA;
  ObjRegistroRETORNOCOBRANCA:TObjRegistroRETORNOCOBRANCA;

implementation

uses UessencialGlobal, Upesquisa, UobjLOTERETORNOCOBRANCA,
  UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFLOTERETORNOCOBRANCA.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjRegistroRETORNOCOBRANCA.LoteRetorno do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_DATAHORA(edtDATAHORA.text);
        Submit_NOMEARQUIVO(edtNOMEARQUIVO.text);
        Submit_DATAARQUIVO(edtDATAARQUIVO.text);
        Submit_NUMEROARQUIVO(edtNUMEROARQUIVO.text);
        Submit_CGCEMPRESA(edtCGCEMPRESA.text);
        Submit_CODIGOCEDENTE(edtCODIGOCEDENTE.text);
        Submit_AGENCIA(edtAGENCIA.text);
        Submit_DIGITOAGENCIA(edtDIGITOAGENCIA.text);
        Submit_CONTA(edtCONTA.text);
        Submit_DIGITOCONTA(edtDIGITOCONTA.text);
        Convenioboleto.Submit_CODIGO(edtconvenioboleto.text);
        Submit_Processado(Submit_ComboBox(ComboProcessado));
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFLOTERETORNOCOBRANCA.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjRegistroRETORNOCOBRANCA.LoteRetorno do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtDATAHORA.text:=Get_DATAHORA;
        EdtNOMEARQUIVO.text:=Get_NOMEARQUIVO;
        EdtDATAARQUIVO.text:=Get_DATAARQUIVO;
        EdtNUMEROARQUIVO.text:=Get_NUMEROARQUIVO;
        EdtCGCEMPRESA.text:=Get_CGCEMPRESA;
        EdtCODIGOCEDENTE.text:=Get_CODIGOCEDENTE;
        EdtAGENCIA.text:=Get_AGENCIA;
        EdtDIGITOAGENCIA.text:=Get_DIGITOAGENCIA;
        EdtCONTA.text:=Get_CONTA;
        EdtDIGITOCONTA.text:=Get_DIGITOCONTA;
        edtconvenioboleto.text:=Convenioboleto.Get_CODIGO;
        lbnomeconvenio.caption:=Convenioboleto.Get_Nome;

        ComboProcessado.ItemIndex:=0;
        if (Get_processado='S')
        Then ComboProcessado.ItemIndex:=1;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFLOTERETORNOCOBRANCA.TabelaParaControles: Boolean;
begin
     If (ObjRegistroRETORNOCOBRANCA.LoteRetorno.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFLOTERETORNOCOBRANCA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjRegistroRETORNOCOBRANCA=Nil)
     Then exit;

If (ObjRegistroRETORNOCOBRANCA.LoteRetorno.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

ObjRegistroRETORNOCOBRANCA.free;
end;

procedure TFLOTERETORNOCOBRANCA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
           Perform(Wm_NextDlgCtl,0,0);
      end;
end;



procedure TFLOTERETORNOCOBRANCA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;
  

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     ObjRegistroRETORNOCOBRANCA.LoteRetorno.status:=dsInsert;
     Guia.pageindex:=0;
     EdtDATAHORA.setfocus;

end;


procedure TFLOTERETORNOCOBRANCA.btalterarClick(Sender: TObject);
begin
    If (ObjRegistroRETORNOCOBRANCA.LoteRetorno.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjRegistroRETORNOCOBRANCA.LoteRetorno.Status:=dsEdit;
                guia.pageindex:=0;
                EdtDATAHORA.setfocus;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
          End;


end;

procedure TFLOTERETORNOCOBRANCA.btgravarClick(Sender: TObject);
begin

     If ObjRegistroRETORNOCOBRANCA.LoteRetorno.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjRegistroRETORNOCOBRANCA.LoteRetorno.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjRegistroRETORNOCOBRANCA.LoteRetorno.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFLOTERETORNOCOBRANCA.btexcluirClick(Sender: TObject);
begin
     If (ObjRegistroRETORNOCOBRANCA.LoteRetorno.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjRegistroRETORNOCOBRANCA.LoteRetorno.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     
     If (ObjRegistroRETORNOCOBRANCA.excluilote(edtcodigo.text)=False)
     Then Begin
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFLOTERETORNOCOBRANCA.btcancelarClick(Sender: TObject);
begin
     ObjRegistroRETORNOCOBRANCA.LoteRetorno.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFLOTERETORNOCOBRANCA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFLOTERETORNOCOBRANCA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjRegistroRETORNOCOBRANCA.LoteRetorno.Get_pesquisa,ObjRegistroRETORNOCOBRANCA.LoteRetorno.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjRegistroRETORNOCOBRANCA.LoteRetorno.status<>dsinactive
                                  then exit;

                                  If (ObjRegistroRETORNOCOBRANCA.LoteRetorno.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjRegistroRETORNOCOBRANCA.LoteRetorno.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFLOTERETORNOCOBRANCA.LimpaLabels;
begin
//LIMPA LABELS
  lbnomeconvenio.caption:='';
end;

procedure TFLOTERETORNOCOBRANCA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        ObjRegistroRETORNOCOBRANCA:=TObjRegistroRETORNOCOBRANCA.create;
        DBGridRegistros.DataSource:=ObjRegistroRETORNOCOBRANCA.ObjDatasource;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFLOTERETORNOCOBRANCA.BtOpcoesClick(Sender: TObject);
Var UltimoLote:String;
begin
     UltimoLote:='';
     UltimoLote:=ObjRegistroRETORNOCOBRANCA.Opcoes(edtcodigo.Text);

     if (UltimoLote <> '')
     then Begin
             if (ObjRegistroRETORNOCOBRANCA.LoteRetorno.LocalizaCodigo(UltimoLote)=false)
             then exit;

             ObjRegistroRETORNOCOBRANCA.LoteRetorno.TabelaparaObjeto;
             Self.ObjetoParaControles;
     end;

end;

procedure TFLOTERETORNOCOBRANCA.GuiaChange(Sender: TObject;
  NewTab: Integer; var AllowChange: Boolean);
begin
     if (NewTab=0)
     Then Begin
               if (Edtcodigo.text='')
               Then exit;
               Self.Retornaregistros;
     End
     Else Begin
               if (Newtab=1)//pendencias
               then Self.retornapendencias;
     End;
end;

procedure TFLOTERETORNOCOBRANCA.RetornaRegistros;
begin
     ObjRegistroRETORNOCOBRANCA.PesquisaRegistros(edtcodigo.Text);
     formatadbgrid(DBGridRegistros);
end;

procedure TFLOTERETORNOCOBRANCA.edtconvenioboletoExit(Sender: TObject);
begin
     ObjRegistroRETORNOCOBRANCA.LoteRetorno.edtconvenioboletoExit(sender,lbnomeconvenio);
end;

procedure TFLOTERETORNOCOBRANCA.edtconvenioboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjRegistroRETORNOCOBRANCA.LoteRetorno.edtconvenioboletoKeyDown(sender,key,shift,lbnomeconvenio);
end;

procedure TFLOTERETORNOCOBRANCA.btrelatoriosClick(Sender: TObject);
begin
     ObjRegistroRETORNOCOBRANCA.Imprime(edtcodigo.text);
end;

procedure TFLOTERETORNOCOBRANCA.RetornaPendencias;
var
Ppendencias,Pvalor:TStringlist;
cont:integer;
begin
     STRGPendencias.ColCount:=1;
     STRGPendencias.rowcount:=1;
     STRGPendencias.cells[0,0]:='';
     
     try
        Ppendencias:=TStringlist.create;
        Pvalor:=TStringList.Create;
     except
        mensagemErro('Erro na Tentativa de criar as StringLists');
        exit;
     End;

     Try
         ObjRegistroRETORNOCOBRANCA.retornapendencias(edtcodigo.text,Ppendencias,pvalor);
         if (Ppendencias.count=0)
         then exit;

         STRGPendencias.ColCount:=3;
         STRGPendencias.rowcount:=Ppendencias.Count+1;
         for cont:=0 to Ppendencias.Count-1 do
         begin
              STRGPendencias.rows[cont+1].clear;
              STRGPendencias.Cells[0,cont+1]:=ppendencias[cont];
              STRGPendencias.Cells[1,cont+1]:=formata_valor(pvalor[cont]);
              if (ObjRegistroRETORNOCOBRANCA.Lancamento.Pendencia.LocalizaCodigo(ppendencias[cont])=true)
              then begin
                        ObjRegistroRETORNOCOBRANCA.Lancamento.Pendencia.TabelaparaObjeto;
                        STRGPendencias.Cells[2,cont+1]:=ObjRegistroRETORNOCOBRANCA.Lancamento.Pendencia.titulo.Get_HISTORICO;
              End;
         End;
         STRGPendencias.FixedRows:=1;
         STRGPendencias.Cells[0,0]:='PEND';
         STRGPendencias.Cells[1,0]:='VALOR';
         STRGPendencias.Cells[2,0]:='HIST�RICO';
         AjustaLArguraColunaGrid(STRGPendencias);
     Finally
         freeandnil(Ppendencias);
         Freeandnil(Pvalor);
     End;
end;

function TFLOTERETORNOCOBRANCA.atualizaQuantidade: string;
begin
     result:='Existem '+Contaregistros('TABLOTERETORNOCOBRANCA','codigo')+' Lotes de Retorno de Cobran�a Cadastrados';
end;

end.
