unit UINSTRUCAO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjINSTRUCAO,
  jpeg;

type
  TFINSTRUCAO = class(TForm)
    Guia: TTabbedNotebook;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    painel_imagem: TPanel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    ImagemFundo: TImage;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbNome: TLabel;
    EdtNome: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjINSTRUCAO:TObjINSTRUCAO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FINSTRUCAO: TFINSTRUCAO;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFINSTRUCAO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjINSTRUCAO do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Nome(edtNome.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFINSTRUCAO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjINSTRUCAO do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtNome.text:=Get_Nome;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFINSTRUCAO.TabelaParaControles: Boolean;
begin
     If (Self.ObjINSTRUCAO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFINSTRUCAO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjINSTRUCAO=Nil)
     Then exit;

     If (Self.ObjINSTRUCAO.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjINSTRUCAO.free;
end;

procedure TFINSTRUCAO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFINSTRUCAO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjINSTRUCAO.status:=dsInsert;
     Guia.pageindex:=0;
     edtNome.setfocus;

end;


procedure TFINSTRUCAO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjINSTRUCAO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjINSTRUCAO.Status:=dsEdit;
                guia.pageindex:=0;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtNome.setfocus;
                
    End;


end;

procedure TFINSTRUCAO.btgravarClick(Sender: TObject);
begin

     If Self.ObjINSTRUCAO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjINSTRUCAO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjINSTRUCAO.Get_codigo;
     Self.ObjINSTRUCAO.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFINSTRUCAO.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjINSTRUCAO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjINSTRUCAO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjINSTRUCAO.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFINSTRUCAO.btcancelarClick(Sender: TObject);
begin
     Self.ObjINSTRUCAO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFINSTRUCAO.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFINSTRUCAO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjINSTRUCAO.Get_pesquisa,Self.ObjINSTRUCAO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjINSTRUCAO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjINSTRUCAO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjINSTRUCAO.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFINSTRUCAO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFINSTRUCAO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

      limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjINSTRUCAO:=TObjINSTRUCAO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

