unit UrelboletoSemRegistroModelo2;

interface

uses Classes, Controls, ExtCtrls, QuickRpt, Qrctrls, Graphics, Menus;

type
  TFRelBoletoSemregistroModelo2 = class(TQuickRep)
    bandDetalhe: TQRBand;
    QRShape1: TQRShape;
    QRImage18: TQRImage;
    lblFichaCaixa: TQRLabel;
    txtNomeCedente2: TQRLabel;
    txtDataDocumento2: TQRLabel;
    txtNumeroDocumento2: TQRLabel;
    txtEspecieDocumento2: TQRLabel;
    txtAceite2: TQRLabel;
    txtUsoBanco2: TQRLabel;
    txtCarteira2: TQRLabel;
    txtQuantidadeMoeda2: TQRLabel;
    txtEspecieMoeda2: TQRLabel;
    txtSacadoNome2: TQRLabel;
    txtSacadoRuaNumeroComplemento2: TQRLabel;
    txtSacadoCEPBairroCidadeEstado2: TQRLabel;
    txtSacadoCPFCGC2: TQRLabel;
    txtCodigoBaixa2: TQRLabel;
    txtLocalPagamento2: TQRLabel;
    txtValorCobrado2: TQRLabel;
    txtValorMoraMultaB2: TQRLabel;
    txtValorMoraMulta2: TQRLabel;
    txtValorDescontoAbatimentoB2: TQRLabel;
    txtValorDescontoAbatimento2: TQRLabel;
    txtValorDocumento2: TQRLabel;
    txtNossoNumero2: TQRLabel;
    txtAgenciaCodigoCedente2: TQRLabel;
    txtDataVencimento2: TQRLabel;
    txtDataProcessamento2: TQRLabel;
    txtValorMoeda2: TQRLabel;
    lblLocalPagamento2: TQRLabel;
    lblDataDocumento2: TQRLabel;
    lblNumeroDocumento2: TQRLabel;
    lblDataVencimento2: TQRLabel;
    lblAgenciaCodigoCedente2: TQRLabel;
    lblNossoNumero2: TQRLabel;
    lblEspecieDocumento2: TQRLabel;
    lblAceite2: TQRLabel;
    lblDataProcessamento2: TQRLabel;
    lblUsoBanco2: TQRLabel;
    lblCarteira2: TQRLabel;
    lblEspecieMoeda2: TQRLabel;
    lblQuantidadeMoeda2: TQRLabel;
    lblValorMoeda2: TQRLabel;
    lblValorDocumento2: TQRLabel;
    lblInstrucoes2: TQRLabel;
    lblValorDescontoAbatimento2: TQRLabel;
    lblMoraMulta2: TQRLabel;
    lblValorCobrado2: TQRLabel;
    lblSacado2: TQRLabel;
    lblCodigoBaixa2: TQRLabel;
    lblSacadoCPFCGC2: TQRLabel;
    lblNomeCedente2: TQRLabel;
    QRImage19: TQRImage;
    QRImage20: TQRImage;
    QRImage21: TQRImage;
    QRImage22: TQRImage;
    QRImage23: TQRImage;
    QRImage24: TQRImage;
    QRImage25: TQRImage;
    QRImage26: TQRImage;
    QRImage27: TQRImage;
    QRImage28: TQRImage;
    QRImage29: TQRImage;
    QRImage30: TQRImage;
    QRImage31: TQRImage;
    QRImage32: TQRImage;
    QRImage33: TQRImage;
    QRImage34: TQRImage;
    txtCodigoBanco2: TQRLabel;
    QRShape2: TQRShape;
    lblAutenticacaoMecanica2: TQRLabel;
    QRImage35: TQRImage;
    txtNomeBanco3: TQRLabel;
    txtLinhaDigitavel3: TQRLabel;
    txtNomeCedente3: TQRLabel;
    txtDataDocumento3: TQRLabel;
    txtNumeroDocumento3: TQRLabel;
    txtEspecieDocumento3: TQRLabel;
    txtAceite3: TQRLabel;
    txtUsoBanco3: TQRLabel;
    txtCarteira3: TQRLabel;
    txtQuantidadeMoeda3: TQRLabel;
    txtEspecieMoeda3: TQRLabel;
    txtSacadoNome3: TQRLabel;
    txtSacadoRuaNumeroComplemento3: TQRLabel;
    txtSacadoCEPBairroCidadeEstado3: TQRLabel;
    txtSacadoCPFCGC3: TQRLabel;
    txtCodigoBaixa3: TQRLabel;
    txtLocalPagamento3: TQRLabel;
    txtValorCobrado3: TQRLabel;
    txtValorMoraMultaB3: TQRLabel;
    txtValorMoraMulta3: TQRLabel;
    txtValorDescontoAbatimentoB3: TQRLabel;
    txtValorDescontoAbatimento3: TQRLabel;
    txtValorDocumento3: TQRLabel;
    txtNossoNumero3: TQRLabel;
    txtAgenciaCodigoCedente3: TQRLabel;
    txtDataVencimento3: TQRLabel;
    txtDataProcessamento3: TQRLabel;
    txtValorMoeda3: TQRLabel;
    lblLocalPagamento3: TQRLabel;
    lblDataDocumento3: TQRLabel;
    lblNumeroDocumento3: TQRLabel;
    lblDataVencimento3: TQRLabel;
    lblAgenciaCodigoCedente3: TQRLabel;
    lblNossoNumero3: TQRLabel;
    lblEspecieDocumento3: TQRLabel;
    lblAceite3: TQRLabel;
    lblDataProcessamento3: TQRLabel;
    lblUsoBanco3: TQRLabel;
    lblCarteira3: TQRLabel;
    lblEspecieMoeda3: TQRLabel;
    lblQuantidadeMoeda3: TQRLabel;
    lblValorMoeda3: TQRLabel;
    lblValorDocumento3: TQRLabel;
    lblInstrucoes3: TQRLabel;
    lblValorDescontoAbatimento3: TQRLabel;
    lblMoraMulta3: TQRLabel;
    lblValorCobrado3: TQRLabel;
    lblSacado3: TQRLabel;
    lblCodigoBaixa3: TQRLabel;
    lblSacadoCPFCGC3: TQRLabel;
    lblNomeCedente3: TQRLabel;
    QRImage36: TQRImage;
    QRImage37: TQRImage;
    QRImage38: TQRImage;
    QRImage39: TQRImage;
    QRImage40: TQRImage;
    QRImage41: TQRImage;
    QRImage42: TQRImage;
    QRImage43: TQRImage;
    QRImage44: TQRImage;
    QRImage45: TQRImage;
    QRImage46: TQRImage;
    QRImage47: TQRImage;
    QRImage48: TQRImage;
    QRImage49: TQRImage;
    QRImage50: TQRImage;
    QRImage51: TQRImage;
    txtCodigoBanco3: TQRLabel;
    lblAutenticacaoMecanica3: TQRLabel;
    imgCodigoBarras3: TQRImage;
    txtInstrucoes2: TQRMemo;
    txtInstrucoes3: TQRMemo;
    txtcodigobarras: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QrMemoObservacoesSuperior: TQRMemo;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRLabel4: TQRLabel;
    txtNomeBanco2: TQRLabel;
  private

  public

  end;

var
  FRelBoletoSemregistroModelo2: TFRelBoletoSemregistroModelo2;

implementation


{$R *.DFM}

end.

