unit UGeraArquivoRemessa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, UObjCobreBem, UObjBoletoBancario,
  ComCtrls, TabNotBk, ExtCtrls;

type
  TFGeraArquivoRemessa = class(TForm)
    Guia: TTabbedNotebook;
    StrGrid: TStringGrid;
    btGerar: TBitBtn;
    btBoletosSemRemessa: TBitBtn;
    ComboBoxIntrucoes: TComboBox;
    ComboBoxCodigoInstrucoes: TComboBox;
    StrGridIntrucao: TStringGrid;
    EdtBoleto: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdtConvenio: TEdit;
    btAdicionarBoleto: TBitBtn;
    Label3: TLabel;
    btExcluirLinha: TBitBtn;
    btGeraRemessaInstrucao: TBitBtn;
    lbnomeformulario: TLabel;
    lbBoleto: TLabel;
    lbConvenio: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CheckMarcarDesmarcarTodasClick(Sender: TObject);
    procedure ComboBoxIntrucoesKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btBoletosSemRemessaClick(Sender: TObject);
    procedure EdtConvenioExit(Sender: TObject);
    procedure EdtConvenioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtBoletoExit(Sender: TObject);
    procedure EdtBoletoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAdicionarBoletoClick(Sender: TObject);
    procedure btExcluirLinhaClick(Sender: TObject);
    procedure btGerarClick(Sender: TObject);
    procedure btGeraRemessaInstrucaoClick(Sender: TObject);
  private
    PcodigoConvenio:String;
    PrimeiraLinhaStrGrid:Boolean;
    ObjBoletoBancario:TObjBoletoBancario;
    ObjCobreBem:TObjCobreBem;


    { Private declarations }
  public
    { Public declarations }
  end;

var
  FGeraArquivoRemessa: TFGeraArquivoRemessa;

implementation

uses UessencialGlobal, UDataModulo, UessencialLocal, UescolheImagemBotao;

{$R *.dfm}

procedure TFGeraArquivoRemessa.FormShow(Sender: TObject);
Var Cont:Integer;
begin

    try
        ObjBoletoBancario:=TObjBoletoBancario.Create;
    except
        MensagemErro('Erro ao tentar criar o Objeto Boleto Bancario');
        desab_botoes(self);
        exit;
    end;

    if (Self.ObjBoletoBancario.Convenioboleto.Pegaconvenio=False)
    Then Begin
            desab_botoes(self);
            exit;
    end;

    Self.PcodigoConvenio:=Self.ObjBoletoBancario.Convenioboleto.get_codigo;

    if (Self.ObjBoletoBancario.Convenioboleto.Get_CobreBem <> 'S')
    then Begin
            MensagemErro('Esse Convenio n�o � do tipo CobreBem');
            desab_botoes(self);
            exit;
    end;

    try
        ObjCobreBem:=TObjCobreBem.Create();
        if (ObjCobreBem.ConfiguraContaCorrente(Self.PcodigoConvenio)=false)
        then StrToInt('A');
    except
        MensagemErro('Erro ao tentar criar o Objeto CobreBem');
        desab_botoes(self);
        exit;
    end;

    for Cont := 0 to ObjCobreBem.CobreBemX.AcoesCobrancaRemessa.Count - 1 do
    begin
          ComboBoxIntrucoes.Items.Add(ObjCobreBem.CobreBemX.AcoesCobrancaRemessa.Item[Cont].Codigo + ' - ' + ObjCobreBem.CobreBemX.AcoesCobrancaRemessa.Item[Cont].Descricao);
          ComboBoxCodigoInstrucoes.Items.Add(ObjCobreBem.CobreBemX.AcoesCobrancaRemessa.Item[Cont].Codigo);
    end;

    ComboBoxIntrucoes.Items.SaveToFile('C:\Instrucoes.txt');

    Guia.PageIndex:=0;
    Self.PrimeiraLinhaStrGrid:=true;

end;

procedure TFGeraArquivoRemessa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if (ObjBoletoBancario <> nil)
    then ObjBoletoBancario.Free;

    if (ObjCobreBem <> nil)
    then ObjCobreBem.free;

end;

procedure TFGeraArquivoRemessa.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if key=#13
    Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFGeraArquivoRemessa.CheckMarcarDesmarcarTodasClick( Sender: TObject);
Var Cont:Integer;
begin
     For Cont:=1 to StrGrid.RowCount-1 do
     Begin
           if (StrGrid.Cells[0,cont]='>>')
           then StrGrid.Cells[0,cont]:=''
           else StrGrid.Cells[0,cont]:='>>';
     end;

end;

procedure TFGeraArquivoRemessa.ComboBoxIntrucoesKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ComboBoxCodigoInstrucoes.ItemIndex:=ComboBoxIntrucoes.ItemIndex;
end;

procedure TFGeraArquivoRemessa.btBoletosSemRemessaClick(Sender: TObject);
begin
     Self.ObjBoletoBancario.PreencheStrGridBoletosSemRemessaCobreBem(Self.PcodigoConvenio, StrGrid);
end;

procedure TFGeraArquivoRemessa.EdtConvenioExit(Sender: TObject);
begin
     Self.ObjBoletoBancario.edtconvenioboletoExit(Sender, lbConvenio);
end;

procedure TFGeraArquivoRemessa.EdtConvenioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjBoletoBancario.edtconvenioboletoKeyDown(Sender, key, Shift, lbConvenio);
end;

procedure TFGeraArquivoRemessa.EdtBoletoExit(Sender: TObject);
begin
     ObjBoletoBancario.edtBoletoPorConvenioExit(Sender, lbBoleto, EdtConvenio.Text );
end;

procedure TFGeraArquivoRemessa.EdtBoletoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
      Self.ObjBoletoBancario.edtBoletoPorConvenioKeyDown(Sender, key, Shift, EdtConvenio.Text);
end;

procedure TFGeraArquivoRemessa.btAdicionarBoletoClick(Sender: TObject);
begin
      if (Self.ObjBoletoBancario.LocalizaCodigo(EdtBoleto.Text)=false)
      then Begin
              MensagemErro('Boleto n�o encontrado');
              exit;
      end;
      Self.ObjBoletoBancario.TabelaparaObjeto;

      if (Self.PrimeiraLinhaStrGrid = false)
      then Self.StrGridIntrucao.RowCount:=Self.StrGridIntrucao.RowCount+1;
      Self.PrimeiraLinhaStrGrid:=false;

      Self.StrGridIntrucao.Cells[0,0]:='CODIGO';
      Self.StrGridIntrucao.Cells[1,0]:='NUMERO';
      Self.StrGridIntrucao.Cells[2,0]:='VENCIMENTO';
      Self.StrGridIntrucao.Cells[3,0]:='VALOR';
      Self.StrGridIntrucao.Cells[4,0]:='INSTRUCAO';
      Self.StrGridIntrucao.Cells[5,0]:='COD.INST.';

      Self.StrGridIntrucao.Cells[0,Self.StrGridIntrucao.RowCount-1]:=Self.ObjBoletoBancario.Get_CODIGO;
      Self.StrGridIntrucao.Cells[1,Self.StrGridIntrucao.RowCount-1]:=Self.ObjBoletoBancario.Get_NumeroBoleto;
      Self.StrGridIntrucao.Cells[2,Self.StrGridIntrucao.RowCount-1]:=Self.ObjBoletoBancario.Get_Vencimento;
      Self.StrGridIntrucao.Cells[3,Self.StrGridIntrucao.RowCount-1]:=formata_valor(Self.ObjBoletoBancario.Get_ValorDoc);
      Self.StrGridIntrucao.Cells[4,Self.StrGridIntrucao.RowCount-1]:=ComboBoxIntrucoes.Text;

      ComboBoxCodigoInstrucoes.ItemIndex:=ComboBoxIntrucoes.ItemIndex;
      Self.StrGridIntrucao.Cells[5,Self.StrGridIntrucao.RowCount-1]:=ComboBoxCodigoInstrucoes.Text;

      AjustaLArguraColunaGrid(StrGridIntrucao);

end;

procedure TFGeraArquivoRemessa.btExcluirLinhaClick(Sender: TObject);
begin
    Deleta_Linha_StringGrid(StrGridIntrucao,StrGridIntrucao.Row);
    if (Self.StrGridIntrucao.RowCount<=2)
    then Self.PrimeiraLinhaStrGrid:=true;

end;


procedure TFGeraArquivoRemessa.btGerarClick(Sender: TObject);
Var PArquivoRemessaRetorno: String;
    Cont:Integer;
begin                        

    PArquivoRemessaRetorno:=ObjCobreBem.RetornaNovoArquivoRemessaRetorno;


    Self.ObjCobreBem.Boleto:=Self.ObjCobreBem.CobreBemX.DocumentosCobranca.Clear; 
    For Cont:=1 to StrGrid.RowCount-1 do
    Begin
        if (ObjCobreBem.GeraBoletoCobreBem(StrGrid.Cells[0,Cont] , PArquivoRemessaRetorno , '01 - Entrada de T�tulo', True)=false)
        then Begin
                MensagemErro('Erro ao tentar Gerar o Boleto '+StrGrid.Cells[1,Cont]+' no CobreBem');
                FDataModulo.IBTransaction.RollbackRetaining;
                exit;
        end;
    end;

    if (Self.ObjCobreBem.GeraArquivoRemessa(PArquivoRemessaRetorno, PcodigoConvenio)=false)
    then Begin
             MensagemErro('Erro ao tentar criar o Aquivo de Remessa');
             FDataModulo.IBTransaction.RollbackRetaining;
             exit;
    end;
    
    FDataModulo.IBTransaction.CommitRetaining;
    MensagemAviso('Arquivo de Remessa gerado com SUCESSO!');
    Self.ObjBoletoBancario.PreencheStrGridBoletosSemRemessaCobreBem(Self.PcodigoConvenio, StrGrid);

end;

procedure TFGeraArquivoRemessa.btGeraRemessaInstrucaoClick(  Sender: TObject);
Var PArquivoRemessaRetorno: String;
    Cont:Integer;
begin

    try
          PArquivoRemessaRetorno:=ObjCobreBem.RetornaNovoArquivoRemessaRetorno;

          For Cont:=1 to StrGridIntrucao.RowCount-1 do
          Begin                                                                                      //Self.StrGridIntrucao.cells[5,Cont],
              if (ObjCobreBem.GeraBoletoCobreBem(StrGridIntrucao.Cells[0,Cont],PArquivoRemessaRetorno,Self.StrGridIntrucao.cells[4,Cont], True)=false)
              then Begin
                      MensagemErro('Erro ao tentar Gerar o Boleto '+StrGrid.Cells[1,Cont]+' no CobreBem');
                      FDataModulo.IBTransaction.RollbackRetaining;
                      exit;
              end;
          end;

          if (Self.ObjCobreBem.GeraArquivoRemessa(PArquivoRemessaRetorno,PcodigoConvenio)=false)
          then Begin
                   MensagemErro('Erro ao tentar criar o Aquivo de Remessa');
                   FDataModulo.IBTransaction.RollbackRetaining;
                   exit;
          end;

          FDataModulo.IBTransaction.CommitRetaining;
          MensagemAviso('Arquivo de Remessa gerado com SUCESSO!');

    finally
          {??N�o entendi o motivo de destruir o objeto logo apos utilizalo. No close do form tbem tem um free.
          if (ObjCobreBem <> nil)
          then ObjCobreBem.Free;
          }
    end;

end;

end.

