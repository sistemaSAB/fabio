unit UHistoricoBoleto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, UObjBoletoBancario;

type
  TFHistoricoBoleto = class(TForm)
    lbConvenio: TLabel;
    lbBoleto: TLabel;
    Label2: TLabel;
    EdtConvenio: TEdit;
    Label3: TLabel;
    EdtBoleto: TEdit;
    Memo: TMemo;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdtConvenioExit(Sender: TObject);
    procedure EdtConvenioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtBoletoExit(Sender: TObject);
    procedure EdtBoletoKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    ObjBoletoBancario:TObjBoletoBancario;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FHistoricoBoleto: TFHistoricoBoleto;

implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TFHistoricoBoleto.FormShow(Sender: TObject);
begin

   try
       ObjBoletoBancario:=TObjBoletoBancario.Create;
   except
       MensagemErro('Erro ao tentar criar o Objeto Boleto Bancario');
       exit;
   end;

end;

procedure TFHistoricoBoleto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (ObjBoletoBancario <> nil)
     then ObjBoletoBancario.Free;
end;

procedure TFHistoricoBoleto.EdtConvenioExit(Sender: TObject);
begin
     Self.ObjBoletoBancario.edtconvenioboletoExit(Sender, lbConvenio);
end;

procedure TFHistoricoBoleto.EdtConvenioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjBoletoBancario.edtconvenioboletoKeyDown(Sender, key, Shift, lbConvenio);   
end;

procedure TFHistoricoBoleto.EdtBoletoExit(Sender: TObject);
begin
     ObjBoletoBancario.edtBoletoPorConvenioExit(Sender, lbBoleto, EdtConvenio.Text );
     Self.ObjBoletoBancario.MostraHistorico(Memo, EdtBoleto.Text);
end;

procedure TFHistoricoBoleto.EdtBoletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
      Self.ObjBoletoBancario.edtBoletoPorConvenioKeyDown(Sender, key, Shift, EdtConvenio.Text);
end;

procedure TFHistoricoBoleto.FormKeyPress(Sender: TObject; var Key: Char);
begin

      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);

end;

end.
