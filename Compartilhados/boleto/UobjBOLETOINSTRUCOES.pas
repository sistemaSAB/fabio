unit UobjBOLETOINSTRUCOES;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJBOLETOBANCARIO,UOBJINSTRUCAO,UOBJOCORRENCIARETORNOCOBRANCA,UOBJARQUIVOREMESSARETORNO;
Type
   TObjBOLETOINSTRUCOES=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                BoletoBancario:TObjBoletoBancario;

                //Instrucao:TOBJINSTRUCAO;
                {Eu tinha colocado a Istru��o como chave estrangeira, mas pra cada convenio vairas
                os codigos, ent�o vou gravar apenas como um historico}
                Ocorrencia:TOBJOCORRENCIARETORNOCOBRANCA;
                ArquivoRemessaRetorno:TOBJARQUIVOREMESSARETORNO;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: String);
                Function Get_Codigo: String;
                Procedure Submit_Data(parametro: String);
                Function Get_Data: String;
                Procedure Submit_Instrucao(Parametro:String);
                Function Get_Instrucao:String;
                procedure EdtBoletoBancarioExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtBoletoBancarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtOcorrenciaExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtOcorrenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtArquivoRemessaRetornoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtArquivoRemessaRetornoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
          Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:String;
               Data:String;
               Instrucao:String;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UBoletoBancario, UINSTRUCAO, UOCORRENCIARETORNOCOBRANCA, UARQUIVOREMESSARETORNO;


Function  TObjBOLETOINSTRUCOES.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('BoletoBancario').asstring<>'')
        Then Begin
                 If (Self.BoletoBancario.LocalizaCodigo(FieldByName('BoletoBancario').asstring)=False)
                 Then Begin
                          Messagedlg('BoletoBancario N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.BoletoBancario.TabelaparaObjeto;
        End;
        {If(FieldByName('Instrucao').asstring<>'')
        Then Begin
                 If (Self.Instrucao.LocalizaCodigo(FieldByName('Instrucao').asstring)=False)
                 Then Begin
                          Messagedlg('Instrucao N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Instrucao.TabelaparaObjeto;
        End;
        }
        If(FieldByName('Ocorrencia').asstring<>'')
        Then Begin
                 If (Self.Ocorrencia.LocalizaCodigo(FieldByName('Ocorrencia').asstring)=False)
                 Then Begin
                          Messagedlg('Ocorrencia N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Ocorrencia.TabelaparaObjeto;
        End;
        If(FieldByName('ArquivoRemessaRetorno').asstring<>'')
        Then Begin
                 If (Self.ArquivoRemessaRetorno.LocalizaCodigo(FieldByName('ArquivoRemessaRetorno').asstring)=False)
                 Then Begin
                          Messagedlg('ArquivoRemessaRetorno N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ArquivoRemessaRetorno.TabelaparaObjeto(false);
        End;
        Self.Data:=fieldbyname('Data').asstring;
        Self.Instrucao:=fieldbyname('Instrucao').AsString;


        result:=True;
     End;
end;


Procedure TObjBOLETOINSTRUCOES.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('BoletoBancario').asstring:=Self.BoletoBancario.GET_CODIGO;
        ParamByName('Instrucao').asstring:=Self.Instrucao;
        ParamByName('Ocorrencia').asstring:=Self.Ocorrencia.GET_CODIGO;
        ParamByName('ArquivoRemessaRetorno').asstring:=Self.ArquivoRemessaRetorno.GET_CODIGO;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('Instrucao').AsString:=Self.Instrucao;
  End;
End;

//***********************************************************************

function TObjBOLETOINSTRUCOES.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjBOLETOINSTRUCOES.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        BoletoBancario.ZerarTabela;
        Ocorrencia.ZerarTabela;
        ArquivoRemessaRetorno.ZerarTabela;
        Data:='';
        Instrucao:='';
     End;
end;

Function TObjBOLETOINSTRUCOES.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjBOLETOINSTRUCOES.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      if (Self.BoletoBancario.Get_CODIGO <> '')
      then Begin
            If (Self.BoletoBancario.LocalizaCodigo(Self.BoletoBancario.Get_CODIGO)=False)
            Then Mensagem:=mensagem+'/ BoletoBancario n�o Encontrado!';
      end;

      if (Self.Ocorrencia.Get_CODIGO <> '')
      then Begin
            If (Self.Ocorrencia.LocalizaCodigo(Self.Ocorrencia.Get_CODIGO)=False)
            Then Mensagem:=mensagem+'/ Ocorrencia n�o Encontrado!';
      end;


      if (Self.ArquivoRemessaRetorno.Get_CODIGO <> '')
      then Begin
            If (Self.ArquivoRemessaRetorno.LocalizaCodigo(Self.ArquivoRemessaRetorno.Get_CODIGO)=False)
            Then Mensagem:=mensagem+'/ ArquivoRemessaRetorno n�o Encontrado!';
      end;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjBOLETOINSTRUCOES.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.BoletoBancario.Get_Codigo<>'')
        Then Strtoint(Self.BoletoBancario.Get_Codigo);
     Except
           Mensagem:=mensagem+'/BoletoBancario';
     End;
     try
        If (Self.Ocorrencia.Get_Codigo<>'')
        Then Strtoint(Self.Ocorrencia.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Ocorrencia';
     End;
     try
        If (Self.ArquivoRemessaRetorno.Get_Codigo<>'')
        Then Strtoint(Self.ArquivoRemessaRetorno.Get_Codigo);
     Except
           Mensagem:=mensagem+'/ArquivoRemessaRetorno';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjBOLETOINSTRUCOES.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjBOLETOINSTRUCOES.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjBOLETOINSTRUCOES.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro BOLETOINSTRUCOES vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD(' Select * from  TABBOLETOINSTRUCOES');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjBOLETOINSTRUCOES.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjBOLETOINSTRUCOES.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjBOLETOINSTRUCOES.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjBOLETOINSTRUCOES.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.BoletoBancario:=TOBJBOLETOBANCARIO.create;
        //Self.Instrucao:=TOBJINSTRUCAO.create;
        Self.Ocorrencia:=TOBJOCORRENCIARETORNOCOBRANCA.create;
        Self.ArquivoRemessaRetorno:=TOBJARQUIVOREMESSARETORNO.create;
//CODIFICA CRIACAO DE OBJETOS





        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABBOLETOINSTRUCOES(Codigo,BoletoBancario,Instrucao');
                InsertSQL.add(' ,Ocorrencia,ArquivoRemessaRetorno,Data)');
                InsertSQL.add('values (:Codigo,:BoletoBancario,:Instrucao,:Ocorrencia,:ArquivoRemessaRetorno');
                InsertSQL.add(' ,:Data)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABBOLETOINSTRUCOES set Codigo=:Codigo,BoletoBancario=:BoletoBancario');
                ModifySQL.add(',Instrucao=:Instrucao,Ocorrencia=:Ocorrencia,ArquivoRemessaRetorno=:ArquivoRemessaRetorno');
                ModifySQL.add(',Data=:Data');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABBOLETOINSTRUCOES where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjBOLETOINSTRUCOES.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjBOLETOINSTRUCOES.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabBOLETOINSTRUCOES');
     Result:=Self.ParametroPesquisa;
end;

function TObjBOLETOINSTRUCOES.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de BOLETOINSTRUCOES ';
end;


function TObjBOLETOINSTRUCOES.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENBOLETOINSTRUCOES,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENBOLETOINSTRUCOES,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjBOLETOINSTRUCOES.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.BoletoBancario.FREE;
//Self.Instrucao.FREE;
Self.Ocorrencia.FREE;
Self.ArquivoRemessaRetorno.FREE;
//CODIFICA DESTRUICAO DE OBJETOS





end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjBOLETOINSTRUCOES.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjBOLETOINSTRUCOES.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjBoletoInstrucoes.Submit_Codigo(parametro: String);
begin
        Self.Codigo:=Parametro;
end;
function TObjBoletoInstrucoes.Get_Codigo: String;
begin
        Result:=Self.Codigo;
end;
procedure TObjBoletoInstrucoes.Submit_Data(parametro: String);
begin
        Self.Data:=Parametro;
end;
function TObjBoletoInstrucoes.Get_Data: String;
begin
        Result:=Self.Data;
end;
//CODIFICA GETSESUBMITS


procedure TObjBOLETOINSTRUCOES.EdtBoletoBancarioExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.BoletoBancario.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.BoletoBancario.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.BoletoBancario.Get_NumeroBoleto;
End;
procedure TObjBOLETOINSTRUCOES.EdtBoletoBancarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FBOLETOBANCARIO:TFBOLETOBANCARIO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FBOLETOBANCARIO:=TFBOLETOBANCARIO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.BoletoBancario.Get_Pesquisa,Self.BoletoBancario.Get_TituloPesquisa,FBOLETOBANCARIO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.BoletoBancario.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.BoletoBancario.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.BoletoBancario.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FBOLETOBANCARIO);
     End;
end;
procedure TObjBOLETOINSTRUCOES.EdtOcorrenciaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Ocorrencia.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Ocorrencia.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Ocorrencia.GET_NOME;
End;

procedure TObjBOLETOINSTRUCOES.EdtOcorrenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FOCORRENCIARETORNOCOBRANCA:TFOCORRENCIARETORNOCOBRANCA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FOCORRENCIARETORNOCOBRANCA:=TFOCORRENCIARETORNOCOBRANCA.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Ocorrencia.Get_Pesquisa,Self.Ocorrencia.Get_TituloPesquisa,FOCORRENCIARETORNOCOBRANCA)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Ocorrencia.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Ocorrencia.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Ocorrencia.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FOCORRENCIARETORNOCOBRANCA);
     End;
end;
procedure TObjBOLETOINSTRUCOES.EdtArquivoRemessaRetornoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ArquivoRemessaRetorno.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ArquivoRemessaRetorno.tabelaparaobjeto(false);
     LABELNOME.CAPTION:=Self.ArquivoRemessaRetorno.Get_Codigo;;
End;
procedure TObjBOLETOINSTRUCOES.EdtArquivoRemessaRetornoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FARQUIVOREMESSARETORNO:TFARQUIVOREMESSARETORNO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FARQUIVOREMESSARETORNO:=TFARQUIVOREMESSARETORNO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ArquivoRemessaRetorno.Get_Pesquisa,Self.ArquivoRemessaRetorno.Get_TituloPesquisa,FArquivoRemessaRetorno)
=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ArquivoRemessaRetorno.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.ArquivoRemessaRetorno.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ArquivoRemessaRetorno.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FARQUIVOREMESSARETORNO);
     End;
end;
//CODIFICA EXITONKEYDOWN




procedure TObjBOLETOINSTRUCOES.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJBOLETOINSTRUCOES';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;




function TObjBOLETOINSTRUCOES.Get_Instrucao: String;
begin
    Result:=Self.Instrucao;
end;

procedure TObjBOLETOINSTRUCOES.Submit_Instrucao(Parametro: String);
begin
    Self.Instrucao:=Parametro;
end;

end.



