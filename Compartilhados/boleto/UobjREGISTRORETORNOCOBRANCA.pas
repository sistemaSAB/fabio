unit UobjREGISTRORETORNOCOBRANCA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJLOTERETORNOCOBRANCA,UobjOCORRENCIARETORNOCOBRANCA,
  UobjBoletoBancario,UObjLancamento,forms, UObjCobreBem;


Type
   TObjREGISTRORETORNOCOBRANCA=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                     :TDataSetState;
                SqlInicial                                  :String[200];
                LoteRetorno:TOBJLOTERETORNOCOBRANCA;
                OCorrencia:TObjOCORRENCIARETORNOCOBRANCA;
                Boleto:tobjBoletobancario;
                Lancamento:TObjLancamento;

                ObjCobreBem:TObjCobreBem;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);
                Procedure Imprime_Cadastro_boleto(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_NossoNumero(parametro: string);
                Function Get_NossoNumero: string;
                Procedure Submit_seunumero(parametro: string);
                Function Get_seunumero: string;
                Procedure Submit_datavencimento(parametro: string);
                Function Get_datavencimento: string;
                Procedure Submit_valordocumento(parametro: string);
                Function Get_valordocumento: string;
                Procedure Submit_ValorJuros(parametro: string);
                Function Get_ValorJuros: string;
                Procedure Submit_ValorDesconto(parametro: string);
                Function Get_ValorDesconto: string;
                Procedure Submit_ValorAbatimento(parametro: string);
                Function Get_ValorAbatimento: string;
                Procedure Submit_ValorIOF(parametro: string);
                Function Get_ValorIOF: string;
                Procedure Submit_ValorOutrasDespesas(parametro: string);
                Function Get_ValorOutrasDespesas: string;
                Procedure Submit_ValorOutrosCreditos(parametro: string);
                Function Get_ValorOutrosCreditos: string;
                Procedure Submit_DataOcorrencia(parametro: string);
                Function Get_DataOcorrencia: string;
                Procedure Submit_DataCredito(parametro: string);
                Function Get_DataCredito: string;

                Function Get_Processado:string;
                Procedure Submit_Processado(parametro:string);

                Function Get_Situacao:string;
                Procedure Submit_Situacao(parametro:string);

                procedure EdtLoteRetornoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtLoteRetornoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtOcorrenciaExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtOcorrenciaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                procedure EdtBoletoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtBoletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtBoletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                procedure EdtlancamentoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtlancamentoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function Opcoes(Plote: string):String;
                //CODIFICA DECLARA GETSESUBMITS
                Procedure PesquisaRegistros(Plote:string);
                Procedure imprimeInformacoesLote(PcodigoLote:string);
                Function  ExcluiLote(Plote:string):boolean;
                Function  retornapendencias(PcodigoLote: string;PPendencia,PvalorLancamento: TStringList): boolean;
                function  AbreArquivo(var PnumeroLote:string):boolean;overload;
                function  AbreArquivo(var PnumeroLote:string;PReprocessa:Boolean):boolean;overload;
                Procedure ImprimeEnderecos;

                Function  QuitaPendencias(Plote:string):boolean;

                Function  Calcula_juros_Pendencias(Ppendencia,Psaldo,Pvalor:TStringList;PvalorJuros:Currency):Boolean;
                Function  Calcula_Desconto_Pendencias(Ppendencia, Psaldo,Pvalor,PDesconto: TStringList; PvalorDesconto: Currency): Boolean;

                //********** F�bio ***************
                function AbreArquivo_CobreBem(Var PUltimoLote:String): boolean;

         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               MemoRetorno:TStringList;
               Arqretorno:TStringList;

               CODIGO:string;
               NossoNumero:string;
               seunumero:string;
               datavencimento:string;
               valordocumento:string;
               ValorJuros:string;
               ValorDesconto:string;
               ValorAbatimento:string;
               ValorIOF:string;
               ValorOutrasDespesas:string;
               ValorOutrosCreditos:string;
               DataOcorrencia:string;
               DataCredito:string;
               Processado:string;
               Situacao:string;



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


                Function ReprocessaLote(Plote: string):boolean; Overload;
                Function ReprocessaLote(Plote:string;ComCommit:boolean):Boolean; Overload;
                Procedure ExtornaReprocessamento(Plote:string); Overload;
                Function ExtornaReprocessamento(Plote:string;ComCommit:boolean):Boolean; Overload;

                Procedure Re_imprimeboleto(Pcodigo:string);

                function  Formatar(Texto: string; TamanhoDesejado: integer; AcrescentarADireita: boolean = true; CaracterAcrescentar: char = ' '): string;
                Procedure RastreioBoletos;


                //*****RETORNO*****************
                function  LerRetornoCNAB400_Bradesco(PnumeroConvenio:string;PathArquivo: String):boolean;
                function  LerRetornoCNAB240_BancodoBRasil(PnumeroConvenio:string;PathArquivo: String): boolean;
                function  LerRetornoCNAB240_CaixaEconomica(PnumeroConvenio: string;PathArquivo: String): boolean;
                function  LerRetornoCNAB400_Unibanco(PnumeroConvenio: string;PathArquivo: String): boolean;
                function  LerRetornoCNAB400_Sicred(PnumeroConvenio: string;PathArquivo: String): boolean;
                function  LerRetornoCNAB400_HSBC(PnumeroConvenio: string;PathArquivo: String): boolean;
                function  LerRetornoCNAB400_SICOOB(PnumeroConvenio: string;PathArquivo: String): boolean;
                //*****************************

                //****RETORNO COBRE BEM *************
                function  LerRetorno_CobreBem(PnumeroConvenio:string;PathArquivo: String; Var PUltimoLote:String): boolean;






                //adicionado por celio 22/08/08
                Procedure ReprocessaTodosLotes;
                Procedure ExtornaTodosLotes;

                function RetornaNomeOcorrencia(POcorrencia: String): String;
                function ReprocessaLote_CobreBem(Plote: string): boolean;
                function RetornaVencimentoPendencia(PBoleto: String): String;

   End;


implementation
uses rdprint,UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UmostraStringList, UOCORRENCIARETORNOCOBRANCA,
  UBoletoBancario, UObjValores, UReltxtRDPRINT, ExtCtrls,
  UMostraBarraProgresso, UObjTitulo, UObjPendencia, UobjEMPRESA,
  UObjGeraLancamento;





Function  TObjREGISTRORETORNOCOBRANCA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('LoteRetorno').asstring<>'')
        Then Begin
                 If (Self.LoteRetorno.LocalizaCodigo(FieldByName('LoteRetorno').asstring)=False)
                 Then Begin
                          Messagedlg('Lote de Retorno N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.LoteRetorno.TabelaparaObjeto;
        End;


        If(FieldByName('OCorrencia').asstring<>'')
        Then Begin
                 If (Self.OCorrencia.LocalizaCodigo(FieldByName('OCorrencia').asstring)=False)
                 Then Begin
                          Messagedlg('Tipo de Ocorre�ncia',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.OCorrencia.TabelaparaObjeto;
        End;

        Self.NossoNumero:=fieldbyname('NossoNumero').asstring;
        Self.seunumero:=fieldbyname('seunumero').asstring;
        Self.datavencimento:=fieldbyname('datavencimento').asstring;
        Self.valordocumento:=fieldbyname('valordocumento').asstring;
        Self.ValorJuros:=fieldbyname('ValorJuros').asstring;
        Self.ValorDesconto:=fieldbyname('ValorDesconto').asstring;
        Self.ValorAbatimento:=fieldbyname('ValorAbatimento').asstring;
        Self.ValorIOF:=fieldbyname('ValorIOF').asstring;
        Self.ValorOutrasDespesas:=fieldbyname('ValorOutrasDespesas').asstring;
        Self.ValorOutrosCreditos:=fieldbyname('ValorOutrosCreditos').asstring;
        Self.DataOcorrencia:=fieldbyname('DataOcorrencia').asstring;
        Self.DataCredito:=fieldbyname('DataCredito').asstring;

        Self.Processado:=Fieldbyname('processado').asstring;
        Self.Situacao:=fieldbyname('Situacao').asstring;

        If(FieldByName('boleto').asstring<>'')
        Then Begin
                 If (Self.boleto.LocalizaCodigo(FieldByName('boleto').asstring)=False)
                 Then Begin
                          Messagedlg('Boleto n�o encontrado',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.boleto.TabelaparaObjeto;
        End;

        If(FieldByName('lancamento').asstring<>'')
        Then Begin
                 If (Self.lancamento.LocalizaCodigo(FieldByName('lancamento').asstring)=False)
                 Then Begin
                          Messagedlg('Lan�amento n�o encontrado',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.lancamento.TabelaparaObjeto;
        End;

        result:=True;
     End;
end;


Procedure TObjREGISTRORETORNOCOBRANCA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('LoteRetorno').asstring:=Self.LoteRetorno.GET_CODIGO;
        ParamByName('NossoNumero').asstring:=Self.NossoNumero;
        ParamByName('seunumero').asstring:=Self.seunumero;
        ParamByName('datavencimento').asstring:=Self.datavencimento;
        ParamByName('valordocumento').asstring:=virgulaparaponto(Self.valordocumento);
        ParamByName('OCorrencia').asstring:=Self.OCorrencia.get_Codigo;
        ParamByName('Processado').asstring:=Self.Processado;
        ParamByName('Situacao').asstring:=Self.Situacao;
        ParamByName('Boleto').asstring:=Self.Boleto.Get_CODIGO;
        ParamByName('lancamento').asstring:=Self.lancamento.Get_CODIGO;
        ParamByName('ValorJuros').asstring:=virgulaparaponto(Self.ValorJuros);
        ParamByName('ValorDesconto').asstring:=virgulaparaponto(Self.ValorDesconto);
        ParamByName('ValorAbatimento').asstring:=virgulaparaponto(Self.ValorAbatimento);
        ParamByName('ValorIOF').asstring:=virgulaparaponto(Self.ValorIOF);
        ParamByName('ValorOutrasDespesas').asstring:=virgulaparaponto(Self.ValorOutrasDespesas);
        ParamByName('ValorOutrosCreditos').asstring:=virgulaparaponto(Self.ValorOutrosCreditos);
        ParamByName('DataOcorrencia').asstring:=Self.DataOcorrencia;
        ParamByName('DataCredito').asstring:=Self.DataCredito;

  End;
End;

//***********************************************************************

function TObjREGISTRORETORNOCOBRANCA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
  //  InputBox('','',Self.Objquery.SQL.Text);
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjREGISTRORETORNOCOBRANCA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        LoteRetorno.ZerarTabela;
        NossoNumero:='';
        seunumero:='';
        datavencimento:='';
        valordocumento:='';
        OCorrencia.ZerarTabela;
        ValorJuros:='';
        ValorDesconto:='';
        ValorAbatimento:='';
        ValorIOF:='';
        ValorOutrasDespesas:='';
        ValorOutrosCreditos:='';
        DataOcorrencia:='';
        DataCredito:='';
        Boleto.ZerarTabela;
        lancamento.ZerarTabela;
        Processado:='';
        Situacao:='';
     End;
end;

Function TObjREGISTRORETORNOCOBRANCA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (LoteRetorno.Get_codigo='')
      Then Mensagem:=mensagem+'/Lote de Retorno';
      If (NossoNumero='')
      Then Mensagem:=mensagem+'/Nosso N�mero';
      If (OCorrencia.get_codigo='')
      Then Mensagem:=mensagem+'/Tipo de Ocorr�ncia';
      If (ValorJuros='')
      Then Mensagem:=mensagem+'/Valor Juros';
      If (ValorDesconto='')
      Then Mensagem:=mensagem+'/Valor Desconto';
      If (ValorAbatimento='')
      Then Mensagem:=mensagem+'/Valor Abatimento';
      If (ValorIOF='')
      Then Mensagem:=mensagem+'/Valor IOF';
      If (ValorOutrasDespesas='')
      Then Mensagem:=mensagem+'/Valor Outras Despesas';
      If (ValorOutrosCreditos='')
      Then Mensagem:=mensagem+'/Valor Outros Cr�ditos';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjREGISTRORETORNOCOBRANCA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.LoteRetorno.LocalizaCodigo(Self.LoteRetorno.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Lote de Retorno n�o Encontrado!';

     If (Self.Boleto.get_codigo<>'')
     Then begin
         If (Self.Boleto.LocalizaCodigo(Self.Boleto.Get_CODIGO)=False)
         Then Mensagem:=mensagem+'/ Boleto n�o Encontrado!';
     End;

     If (Self.lancamento.get_codigo<>'')
     Then begin
         If (Self.lancamento.LocalizaCodigo(Self.lancamento.Get_CODIGO)=False)
         Then Mensagem:=mensagem+'/ Lancamento n�o Encontrado!';
     End;

     if (self.OCorrencia.localizaCodigo(Self.OCorrencia.Get_CODIGO)=False)
     Then Mensagem:=Mensagem+'/Ocorr�ncia N� '+Self.OCorrencia.Get_CODIGO+' n�o encontrada na tabela de ocorr�ncias';

//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjREGISTRORETORNOCOBRANCA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.LoteRetorno.Get_Codigo<>'')
        Then Strtoint(Self.LoteRetorno.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Lote de Retorno';
     End;
     try
        Strtofloat(Self.valordocumento);
     Except
           Mensagem:=mensagem+'/Valor do Documento';
     End;

     try
        Strtoint(Self.OCorrencia.get_codigo);
     Except
           Mensagem:=mensagem+'/Tipo de Ocorr�ncia';
     End;
     
     try
        Strtofloat(Self.ValorJuros);
     Except
           Mensagem:=mensagem+'/Valor Juros';
     End;
     try
        Strtofloat(Self.ValorDesconto);
     Except
           Mensagem:=mensagem+'/Valor Desconto';
     End;
     try
        Strtofloat(Self.ValorAbatimento);
     Except
           Mensagem:=mensagem+'/Valor Abatimento';
     End;
     try
        Strtofloat(Self.ValorIOF);
     Except
           Mensagem:=mensagem+'/Valor IOF';
     End;
     try
        Strtofloat(Self.ValorOutrasDespesas);
     Except
           Mensagem:=mensagem+'/Valor Outras Despesas';
     End;
     try
        Strtofloat(Self.ValorOutrosCreditos);
     Except
           Mensagem:=mensagem+'/Valor Outros Cr�ditos';
     End;

     try
        if(Self.Boleto.get_codigo<>'')
        Then Strtoint(Self.Boleto.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Boleto';
     End;

     try
        if(Self.lancamento.get_codigo<>'')
        Then Strtoint(Self.lancamento.Get_CODIGO);
     Except
           Mensagem:=mensagem+'/Lancamento';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjREGISTRORETORNOCOBRANCA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        if (Self.datavencimento<>'')
        Then Strtodate(Self.datavencimento);
     Except
           Mensagem:=mensagem+'/Data de Vencimento';
     End;
     try
        if (Self.DataOcorrencia<>'')
        Then Strtodate(Self.DataOcorrencia);
     Except
           Mensagem:=mensagem+'/Data de Ocorr�ncia';
     End;
     try
        if (Self.DataCredito<>'')
        Then Strtodate(Self.DataCredito);
     Except
           Mensagem:=mensagem+'/Data de Cr�dito';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjREGISTRORETORNOCOBRANCA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';

        if (Self.Processado<>'S') and (Self.Processado<>'N')and (Self.Processado<>'E')
        then mensagem:=mensagem+'\Valor Inv�lido no Campo Processado';

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjREGISTRORETORNOCOBRANCA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro REGISTRORETORNOCOBRANCA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,LoteRetorno,NossoNumero,seunumero,datavencimento');
           SQL.ADD(' ,valordocumento,OCorrencia,ValorJuros,ValorDesconto,ValorAbatimento');
           SQL.ADD(' ,ValorIOF,ValorOutrasDespesas,ValorOutrosCreditos,DataOcorrencia');
           SQL.ADD(' ,DataCredito,Processado,situacao,boleto,lancamento');

           SQL.ADD(' from  TabRegistroRetornoCobranca');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjREGISTRORETORNOCOBRANCA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjREGISTRORETORNOCOBRANCA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjREGISTRORETORNOCOBRANCA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjqueryPesquisa;

        Self.ParametroPesquisa:=TStringList.create;

        Self.MemoRetorno:=TStringList.create;
        Self.Arqretorno:=TStringList.create;

        Self.OCorrencia:=TObjOCORRENCIARETORNOCOBRANCA.create;
        Self.Lancamento:=TObjLancamento.create;
        Self.Boleto:=tobjBoletobancario.Create;
        Self.ObjCobreBem:=TObjCobreBem.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.LoteRetorno:=TOBJLOTERETORNOCOBRANCA.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabRegistroRetornoCobranca(CODIGO,LoteRetorno');
                InsertSQL.add(' ,NossoNumero,seunumero,datavencimento,valordocumento');
                InsertSQL.add(' ,OCorrencia,ValorJuros,ValorDesconto,ValorAbatimento');
                InsertSQL.add(' ,ValorIOF,ValorOutrasDespesas,ValorOutrosCreditos,DataOcorrencia');
                InsertSQL.add(' ,DataCredito,Processado,situacao,boleto,lancamento)');
                InsertSQL.add('values (:CODIGO,:LoteRetorno,:NossoNumero,:seunumero');
                InsertSQL.add(' ,:datavencimento,:valordocumento,:OCorrencia');
                InsertSQL.add(' ,:ValorJuros,:ValorDesconto,:ValorAbatimento,:ValorIOF');
                InsertSQL.add(' ,:ValorOutrasDespesas,:ValorOutrosCreditos,:DataOcorrencia');
                InsertSQL.add(' ,:DataCredito,:Processado,:situacao,:boleto,:lancamento)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabRegistroRetornoCobranca set CODIGO=:CODIGO');
                ModifySQL.add(',LoteRetorno=:LoteRetorno,NossoNumero=:NossoNumero,seunumero=:seunumero');
                ModifySQL.add(',datavencimento=:datavencimento,valordocumento=:valordocumento');
                ModifySQL.add(',OCorrencia=:OCorrencia,ValorJuros=:ValorJuros');
                ModifySQL.add(',ValorDesconto=:ValorDesconto,ValorAbatimento=:ValorAbatimento');
                ModifySQL.add(',ValorIOF=:ValorIOF,ValorOutrasDespesas=:ValorOutrasDespesas');
                ModifySQL.add(',ValorOutrosCreditos=:ValorOutrosCreditos,DataOcorrencia=:DataOcorrencia');
                ModifySQL.add(',DataCredito=:DataCredito,Processado=:Processado,situacao=:situacao,Boleto=:boleto,lancamento=:lancamento');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabRegistroRetornoCobranca where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjREGISTRORETORNOCOBRANCA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjREGISTRORETORNOCOBRANCA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewRegistroRetornoCobranca');
     Result:=Self.ParametroPesquisa;
end;

function TObjREGISTRORETORNOCOBRANCA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de REGISTRORETORNOCOBRANCA ';
end;


function TObjREGISTRORETORNOCOBRANCA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENREGISTRORETORNOCOBRANCA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENREGISTRORETORNOCOBRANCA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjREGISTRORETORNOCOBRANCA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Freeandnil(objquerypesquisa);
    Freeandnil(objdatasource);
    Self.LoteRetorno.FREE;
    Self.OCorrencia.free;
    Self.Boleto.free;
    Self.Lancamento.free;
    Freeandnil(MemoRetorno);
    Freeandnil(Arqretorno);
    ObjCobreBem.Free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjREGISTRORETORNOCOBRANCA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjREGISTRORETORNOCOBRANCA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjRegistroRetornoCobranca.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjRegistroRetornoCobranca.Submit_NossoNumero(parametro: string);
begin
        Self.NossoNumero:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_NossoNumero: string;
begin
        Result:=Self.NossoNumero;
end;
procedure TObjRegistroRetornoCobranca.Submit_seunumero(parametro: string);
begin
        Self.seunumero:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_seunumero: string;
begin
        Result:=Self.seunumero;
end;
procedure TObjRegistroRetornoCobranca.Submit_datavencimento(parametro: string);
begin
        Self.datavencimento:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_datavencimento: string;
begin
        Result:=Self.datavencimento;
end;
procedure TObjRegistroRetornoCobranca.Submit_valordocumento(parametro: string);
begin
        Self.valordocumento:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_valordocumento: string;
begin
        Result:=Self.valordocumento;
end;

procedure TObjRegistroRetornoCobranca.Submit_ValorJuros(parametro: string);
begin
        Self.ValorJuros:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_ValorJuros: string;
begin
        Result:=Self.ValorJuros;
end;
procedure TObjRegistroRetornoCobranca.Submit_ValorDesconto(parametro: string);
begin
        Self.ValorDesconto:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_ValorDesconto: string;
begin
        Result:=Self.ValorDesconto;
end;
procedure TObjRegistroRetornoCobranca.Submit_ValorAbatimento(parametro: string);
begin
        Self.ValorAbatimento:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_ValorAbatimento: string;
begin
        Result:=Self.ValorAbatimento;
end;
procedure TObjRegistroRetornoCobranca.Submit_ValorIOF(parametro: string);
begin
        Self.ValorIOF:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_ValorIOF: string;
begin
        Result:=Self.ValorIOF;
end;
procedure TObjRegistroRetornoCobranca.Submit_ValorOutrasDespesas(parametro: string);
begin
        Self.ValorOutrasDespesas:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_ValorOutrasDespesas: string;
begin
        Result:=Self.ValorOutrasDespesas;
end;
procedure TObjRegistroRetornoCobranca.Submit_ValorOutrosCreditos(parametro: string);
begin
        Self.ValorOutrosCreditos:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_ValorOutrosCreditos: string;
begin
        Result:=Self.ValorOutrosCreditos;
end;
procedure TObjRegistroRetornoCobranca.Submit_DataOcorrencia(parametro: string);
begin
        Self.DataOcorrencia:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_DataOcorrencia: string;
begin
        Result:=Self.DataOcorrencia;
end;
procedure TObjRegistroRetornoCobranca.Submit_DataCredito(parametro: string);
begin
        Self.DataCredito:=Parametro;
end;
function TObjRegistroRetornoCobranca.Get_DataCredito: string;
begin
        Result:=Self.DataCredito;
end;
//CODIFICA GETSESUBMITS


procedure TObjREGISTRORETORNOCOBRANCA.EdtLoteRetornoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.LoteRetorno.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.LoteRetorno.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.LoteRetorno.GET_NOME;
End;
procedure TObjREGISTRORETORNOCOBRANCA.EdtLoteRetornoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            //FLOTERETORNOCOBRANCA:=TFLOTERETORNOCOBRANCA.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.LoteRetorno.Get_Pesquisa,Self.LoteRetorno.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.LoteRetorno.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.LoteRetorno.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.LoteRetorno.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           //Freeandnil(FLOTERETORNOCOBRANCA);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjREGISTRORETORNOCOBRANCA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJREGISTRORETORNOCOBRANCA';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Informa��es do lote');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
                   Self.imprimeInformacoesLote(pcodigo);
            End;
          End;
     end;

end;

procedure TObjREGISTRORETORNOCOBRANCA.Imprime_Cadastro_boleto(Pcodigo: string);
Begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJREGISTRORETORNOCOBRANCA';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Boletos Emitidos');
                items.add('2� Via Boleto');
                items.add('Boletos Impressos Anal�tico');
                items.add('Endere�os');
                items.add('2� Via de V�rios Boletos');

          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
                   Self.Boleto.ImprimeBoletosEmitidos;
            End;
            1:begin
                   Self.Re_imprimeboleto(Pcodigo);
            End;
            2:begin
                   Self.RastreioBoletos;
            End;
            3:Self.ImprimeEnderecos;
            4:Self.Re_imprimeboleto('');

          End;
     end;
End;

function TObjRegistroRetornoCobranca.LerRetornoCNAB240_BancodoBrasil(PnumeroConvenio:string;PathArquivo:String) : boolean;
var
   ACodigoBanco,
   ANomeCedente,
   ATipoInscricao,
   ANumeroCPFCGC,
   ACodigoCedente,
   ACodigoAgencia,
   ADigitoCodigoAgencia,
   ANumeroConta,
   ADigitoNumeroConta,
   Aseunumero,AOCorrencia,ANossoNumero : string;
   Adatavencimento:Tdatetime;
   Avalordocumento:Currency;
   NumeroRegistro : integer;
   AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
   ADataOcorrencia,ADataCredito:Tdatetime;
   ADataArquivo:Tdatetime;
   ANumeroArquivo:integer;
   Plote:string;

begin
     result:=False;

Try
     Self.MemoRetorno.clear;

     Try
       Self.Arqretorno.LoadFromFile(PathArquivo);
     Except
           Messagedlg ('Erro na tentativa de Abrir o Arquivo',mterror,[mbok],0);
           exit;
     End;
     NumeroRegistro:=0;//n� da linha

     if Self.Arqretorno.Count <= 0
     then Begin
              Messagedlg('O retorno est� vazio. N�o h� dados para processar',mterror,[mbok],0);
              exit;
     End;

     if length(Self.Arqretorno[0]) <> 240 then
     begin
          Messagedlg('Tamanho de registro diferente de 240 bytes. Tamanho = '+inttostr(length(Self.Arqretorno[0]))+' bytes',mterror,[mbok],0);
          exit;
     End;

     //Ver se o arquivo � mesmo RETORNO DE COBRAN�A
     if Copy(Self.Arqretorno.Strings[NumeroRegistro],143,1) <> '2' then
     Begin
          Messagedlg(PathArquivo+'N�o � um arquivo de retorno de cobran�a com layout CNAB240',mterror,[mbok],0);
          exit;
     End;

     //L� registro HEADER
     ACodigoBanco := Copy(Self.Arqretorno.Strings[NumeroRegistro],1,3);

     if Copy(Self.Arqretorno.Strings[NumeroRegistro],8,1) <> '0'
     then Begin
              Messagedlg('Este n�o � um registro HEADER v�lido para arquivo de retorno de cobran�a com layout CNAB240',mterror,[mbok],0);
              exit;
     End;

     //*******Dados do cedente do t�tulo*****************************
     ATipoInscricao := Copy(Self.Arqretorno.Strings[NumeroRegistro],18,1);
     ANumeroCPFCGC := Copy(Self.Arqretorno.Strings[NumeroRegistro],19,14);
     ACodigoCedente := Copy(Self.Arqretorno.Strings[NumeroRegistro],33,16);
     ACodigoAgencia := Copy(Self.Arqretorno.Strings[NumeroRegistro],53,5);
     ADigitoCodigoAgencia := Copy(Self.Arqretorno.Strings[NumeroRegistro],58,1);
     ANumeroConta := Copy(Self.Arqretorno.Strings[NumeroRegistro],59,12);
     ADigitoNumeroConta := Copy(Self.Arqretorno.Strings[NumeroRegistro],71,1);
     ANomeCedente := Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],73,30));

     Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
     Memoretorno.Add(PathArquivo);
     Memoretorno.Add('');
     Memoretorno.Add('C�digo do Banco  : '+ACodigoBanco);

     if ATipoInscricao = '1'
     then Memoretorno.Add('Tipo de Inscri��o: '+'F�sica')
     else
         if ATipoInscricao = '2'
         then MemoRetorno.Add('Tipo de Inscri��o: '+'Jur�dica')
         else MemoRetorno.Add('Tipo de Inscri��o: '+'Outro');
     MemoRetorno.Add('N�mero CGC       : '+ANumeroCPFCGC);
     MemoRetorno.Add('C�digo Cedente   : '+ACodigoCedente);
     MemoRetorno.Add('Ag�ncia          : '+ACodigoAgencia+'-'+ADigitoCodigoAgencia);
     MemoRetorno.Add('Conta            : '+ANumeroConta+'-'+ADigitoNumeroConta);
     MemoRetorno.Add('Cedente          : '+ANomeCedente);
    //*****************************************************************
     NumeroRegistro := 1;//proxima linha


     //L� registro HEADER DE LOTE
     //Verifica se � um lote de retorno de cobran�a
     if Copy(Self.Arqretorno.Strings[NumeroRegistro],9,3) <> 'T01'
     then Begin
               Messagedlg('Este n�o � um lote de retorno de cobran�a',mterror,[mbok],0);
               exit;
     End;
     ADataArquivo := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],196,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],194,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],192,2)));
     ANumeroArquivo := StrToInt(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],184,8)));
     Self.MemoRetorno.Add('Data do Arquivo  :'+datetostr(ADataArquivo));
     Self.MemoRetorno.Add('N�   do Arquivo  :'+IntToStr(ANumeroArquivo));
     //********************************************************************
     FMostraStringList.caption:='Dados do Arquivo de Retorno';
     FMostraStringList.Memo.Lines.clear;
     FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;
     FMostraStringList.showmodal;


     //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
     //se confirmar gravo um novo Lote
     if (FMostraStringList.tag=0)
     Then exit;


     if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,datetoStr(AdataArquivo),Inttostr(AnumeroArquivo))=True)
     Then Begin
               Self.LoteRetorno.tabelaparaobjeto;
               if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then exit;
     End;
     //Criando um Novo Lote de Retorno de Cobranca

     Self.ZerarTabela;

     //if (Self.LoteRetorno.Convenioboleto.Pegaconvenio(ACodigoBanco)=False)
     //Then exit;

     Self.LoteRetorno.Status:=dsinsert;
     Self.LoteRetorno.Submit_CODIGO(Self.LoteRetorno.Get_NovoCodigo);
     Self.LoteRetorno.Submit_DATAHORA(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');
     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************

     //L� os registros DETALHE
     NumeroRegistro := NumeroRegistro + 1;//proxima linha

     //L� at� o antepen�ltimo registro porque o pen�ltimo cont�m apenas o TRAILER DO LOTE e o �ltimo cont�m apenas o TRAILER DO ARQUIVO
     while (NumeroRegistro < Self.Arqretorno.Count - 2) do
     begin
            Self.MemoRetorno.Add('----------------------------------');

            //Registro detalhe com tipo de segmento = T
            If Copy(Self.Arqretorno.Strings[NumeroRegistro],14,1) = 'T'
            Then begin
                  AOCorrencia := Copy(Self.Arqretorno.Strings[NumeroRegistro],16,2);

                  case StrToInt(AOCorrencia) of
                     2 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Confirmado');
                     3 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
                     6 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
                     9 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixado');
                     12: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Abatimento');
                     13: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Abatimento');
                     14: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Vencimento');
                     17: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
                     19: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Protestar');
                     20: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Sustar Protesto');
                     23: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Encaminhado A Cartorio');
                     24: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Retirado De Cartorio');
                     25: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protestado');
                     26: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Comando Recusado');
                     27: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Dados');
                     28: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Debito Tarifas');
                     30: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
                     36: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Desconto');
                     37: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Desconto');
                     43: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protesto Ou Sustacao Estornado');
                     44: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixa Ou Liquidacao Estornada');
                     45: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Dados Alterados');
                  else Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Outras Ocorrencias');

                  end; {case StrToInt(AOCorrencia)}

                  //Nosso n�mero SEM D�GITO
                  ANossoNumero := Copy(trim(Self.Arqretorno.Strings[NumeroRegistro]),38,20);
                  Aseunumero := Copy(trim(Self.Arqretorno.Strings[NumeroRegistro]),106,25);

                  Try
                     If(Copy(Self.Arqretorno.Strings[NumeroRegistro],78,4)<>'0000')
                     Then Adatavencimento := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],78,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],76,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],74,2)))
                     Else Adatavencimento := 0;
                  Except
                        ADatavencimento:=0;
                  End;

                  Try
                    //Avalordocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],82,15))/100;
                    //250808 - Alterado por celio
                    //o Valordocumento recebia o valor integral do boleto, nao o valor de quitacao
                    //como estou na linha superior � das informa��es do recebimento, terei que aumentar 01 linha e resgatar
                    //as informa��es do valor recebido
                    Avalordocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro+1],78,15))/100;
                  Except

                  End;

                  Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
                  Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
                  Self.MemoRetorno.Add('Data Vencimento  :'+datetostr(Adatavencimento));
                  Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));
            end;

            //pegando a proxima linha
            NumeroRegistro := NumeroRegistro + 1;

            //Registro detalhe com tipo de segmento = U
            if Copy(Self.Arqretorno.Strings[NumeroRegistro],14,1) = 'U' then
            begin
                  AValorMoraJuros := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],18,15))/100;
                  AValorDesconto := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],33,15))/100;
                  AValorAbatimento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],48,15))/100;
                  AValorIOF := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],63,15))/100;
                  AValorOutrasDespesas := StrToFloat(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],108,15)))/100;
                  AValorOutrosCreditos := StrToFloat(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],123,15)))/100;
                  Try
                      ADataOcorrencia:= EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],142,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],140,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],138,2)));
                  Except
                      ADataOcorrencia:=0;
                  End;

                  Try
                    ADataCredito := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],150,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],148,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],146,2)));
                  Except
                        ADataCredito:=0;
                  End;
                  Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
                  Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
                  Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
                  Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
                  Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
                  Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

                  if (ADataOcorrencia<>0)
                  Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
                  Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

                  if (ADataCredito<>0)
                  Then Self.MemoRetorno.Add('Data  Cr�dito    :'+datetostr(ADataCredito))
                  Else Self.MemoRetorno.Add('Data  Cr�dito    :');


                  NumeroRegistro := NumeroRegistro + 1;


            End; //if Copy(Retorno.Strings[NumeroRegistro],14,1) = 'U'
            //Gravando no banco de Dados

            Self.ZerarTabela;
            Self.Status:=dsinsert;
            Self.LoteRetorno.Submit_CODIGO(Plote);
            Self.Submit_CODIGO            (Self.Get_NovoCodigo);
            Self.Submit_NossoNumero       (ANossoNumero);
            Self.Submit_seunumero         (ASeuNumero);
            //Self.Submit_datavencimento    (datetostr(ADataVencimento));
            Self.Submit_valordocumento    (floattostr(AValorDocumento));
            Self.OCorrencia.Submit_codigo (AOCorrencia);
            Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
            Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
            Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
            Self.Submit_ValorIOF          (floattostr(AValorIOF));
            Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
            Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
            Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
            Self.Submit_DataCredito(datetostr(ADataCredito));
            Self.Submit_Processado('N');

            if (Self.Salvar(False)=False)
            then Begin
                      Messagedlg('Erro na tentativa de Salvar o Registro Linha N�'+inttostr(numeroregistro),mterror,[mbok],0);
                      exit;
            End;
      end;
      FDataModulo.IBTransaction.CommitRetaining;
      Self.MemoRetorno.Add('----------------------------------');
      Result := TRUE;
      FmostraStringList.Memo.Clear;
      FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
      FmostraStringList.ShowModal;

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

end;

function TObjRegistroRetornoCobranca.LerRetornoCNAB240_CaixaEconomica(PnumeroConvenio:string;PathArquivo:String) : boolean;
var
   ACodigoBanco,
   ANomeCedente,
   ATipoInscricao,
   ANumeroCPFCGC,
   ACodigoCedente,
   ACodigoAgencia,
   ADigitoCodigoAgencia,
   ANumeroConta,
   ADigitoNumeroConta,
   temp,pcodigocarteicacaixa,Aseunumero,AOCorrencia,ANossoNumero : string;
   Adatavencimento:Tdatetime;
   Avalordocumento:Currency;
   NumeroRegistro : integer;
   AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
   ADataOcorrencia,ADataCredito:Tdatetime;
   ADataArquivo:Tdatetime;
   ANumeroArquivo:integer;
   Plote:string;
   PPosicao:Integer; 

begin
// A COBRANCA SINCO UTILIZA O CNAB240

     result:=False;

Try
     Self.MemoRetorno.clear;

     // Alterado por F�bio
     PPosicao:=146; // Posicao Padrao
     if (ObjParametroGlobal.Validaparametro('POSICAO ARQUIVO DE RETORNO BOLETO CEF DATA CREDITO')=true)
     then Begin
                PPosicao:=StrToInt(ObjParametroGlobal.Get_Valor);
     end;

     Try
       Self.Arqretorno.LoadFromFile(PathArquivo);
     Except
           Messagedlg ('Erro na tentativa de Abrir o Arquivo',mterror,[mbok],0);
           exit;
     End;
     NumeroRegistro:=0;//n� da linha

     if Self.Arqretorno.Count <= 0
     then Begin
              Messagedlg('O retorno est� vazio. N�o h� dados para processar',mterror,[mbok],0);
              exit;
     End;

     if length(Self.Arqretorno[0]) <> 240 then
     begin
          Messagedlg('Tamanho de registro diferente de 240 bytes. Tamanho = '+inttostr(length(Self.Arqretorno[0]))+' bytes',mterror,[mbok],0);
          exit;
     End;

     //Ver se o arquivo � mesmo RETORNO DE COBRAN�A
     if Copy(Self.Arqretorno.Strings[NumeroRegistro],143,1) <> '2' then
     Begin
          Messagedlg(PathArquivo+'N�o � um arquivo de retorno de cobran�a com layout CNAB240',mterror,[mbok],0);
          exit;
     End;

     //L� registro HEADER
     ACodigoBanco := Copy(Self.Arqretorno.Strings[NumeroRegistro],1,3);

     if Copy(Self.Arqretorno.Strings[NumeroRegistro],8,1) <> '0'
     then Begin
              Messagedlg('Este n�o � um registro HEADER v�lido para arquivo de retorno de cobran�a com layout CNAB240',mterror,[mbok],0);
              exit;
     End;

     //*******Dados do cedente do t�tulo*****************************
     ATipoInscricao := Copy(Self.Arqretorno.Strings[NumeroRegistro],18,1);
     ANumeroCPFCGC := Copy(Self.Arqretorno.Strings[NumeroRegistro],19,14);
     ACodigoCedente := Copy(Self.Arqretorno.Strings[NumeroRegistro],33,16);
     ACodigoAgencia := Copy(Self.Arqretorno.Strings[NumeroRegistro],53,5);
     ADigitoCodigoAgencia := Copy(Self.Arqretorno.Strings[NumeroRegistro],58,1);
     ANumeroConta := Copy(Self.Arqretorno.Strings[NumeroRegistro],59,12);
     ADigitoNumeroConta := Copy(Self.Arqretorno.Strings[NumeroRegistro],71,1);
     ANomeCedente := Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],73,30));

     Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
     Memoretorno.Add(PathArquivo);
     Memoretorno.Add('');
     Memoretorno.Add('C�digo do Banco  : '+ACodigoBanco);

     if ATipoInscricao = '1'
     then Memoretorno.Add('Tipo de Inscri��o: '+'F�sica')
     else
         if ATipoInscricao = '2'
         then MemoRetorno.Add('Tipo de Inscri��o: '+'Jur�dica')
         else MemoRetorno.Add('Tipo de Inscri��o: '+'Outro');
     MemoRetorno.Add('N�mero CGC       : '+ANumeroCPFCGC);
     MemoRetorno.Add('C�digo Cedente   : '+ACodigoCedente);
     MemoRetorno.Add('Ag�ncia          : '+ACodigoAgencia+'-'+ADigitoCodigoAgencia);
     MemoRetorno.Add('Conta            : '+ANumeroConta+'-'+ADigitoNumeroConta);
     MemoRetorno.Add('Cedente          : '+ANomeCedente);
    //*****************************************************************
     NumeroRegistro := 1;//proxima linha


     //L� registro HEADER DE LOTE
     //Verifica se � um lote de retorno de cobran�a
     if Copy(Self.Arqretorno.Strings[NumeroRegistro],9,3) <> 'T01'
     then Begin
               Messagedlg('Este n�o � um lote de retorno de cobran�a',mterror,[mbok],0);
               exit;
     End;
     ADataArquivo := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],196,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],194,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],192,2)));
     ANumeroArquivo := StrToInt(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],184,8)));
     Self.MemoRetorno.Add('Data do Arquivo  :'+datetostr(ADataArquivo));
     Self.MemoRetorno.Add('N�   do Arquivo  :'+IntToStr(ANumeroArquivo));
     //********************************************************************
     FMostraStringList.caption:='Dados do Arquivo de Retorno';
     FMostraStringList.Memo.Lines.clear;
     FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;

     FMostraStringList.showmodal;

     //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
     //se confirmar gravo um novo Lote
     if (FMostraStringList.tag=0)
     Then exit;

     if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,datetoStr(AdataArquivo),Inttostr(AnumeroArquivo))=True)
     Then Begin
               Self.LoteRetorno.tabelaparaobjeto;
               if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then exit;
     End;

     //Criando um Novo Lote de Retorno de Cobranca

     Self.ZerarTabela;

     //if (Self.LoteRetorno.Convenioboleto.Pegaconvenio(ACodigoBanco)=False)
     //Then exit;

     Self.LoteRetorno.Status:=dsinsert;
     Self.LoteRetorno.Submit_CODIGO(Self.LoteRetorno.Get_NovoCodigo);
     Self.LoteRetorno.Submit_DATAHORA(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_DATAARQUIVO(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');
     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************

     //L� os registros DETALHE
     NumeroRegistro := NumeroRegistro + 1;//proxima linha

     //L� at� o antepen�ltimo registro porque o pen�ltimo cont�m apenas o TRAILER DO LOTE e o �ltimo cont�m apenas o TRAILER DO ARQUIVO
     while (NumeroRegistro < Self.Arqretorno.Count - 2) do
     begin
            //******************************************************************
            Self.LoteRetorno.localizacodigo(Plote);
            Self.LoteRetorno.tabelaparaobjeto;
            pcodigocarteicacaixa:=Self.loteretorno.Convenioboleto.Get_CodigoCarteira_Caixa;
            //******************************************************************

            Self.MemoRetorno.Add('----------------------------------');

            //Registro detalhe com tipo de segmento = T
            If Copy(Self.Arqretorno.Strings[NumeroRegistro],14,1) = 'T'
            Then begin
                  AOCorrencia := Copy(Self.Arqretorno.Strings[NumeroRegistro],16,2);

                  case StrToInt(AOCorrencia) of
                     2 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Confirmado');
                     3 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
                     6 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
                     9 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixado');
                     12: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Abatimento');
                     13: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Abatimento');
                     14: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Vencimento');
                     17: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
                     19: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Protestar');
                     20: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Sustar Protesto');
                     23: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Encaminhado A Cartorio');
                     24: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Retirado De Cartorio');
                     25: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protestado');
                     26: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Comando Recusado');
                     27: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Dados');
                     28: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Debito Tarifas');
                     30: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
                     36: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Desconto');
                     37: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Desconto');
                     43: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protesto Ou Sustacao Estornado');
                     44: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixa Ou Liquidacao Estornada');
                     45: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Dados Alterados');
                  else Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Outras Ocorrencias');

                  end; {case StrToInt(AOCorrencia)}

                  //Nosso n�mero SEM D�GITO
                  IF (Self.LoteRetorno.Convenioboleto.Get_CodigoCarteira_Caixa<>'8')
                  Then Begin
                            ANossoNumero := trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],38,20));
          
                            //O nosso numero da Caixa SINCO � gerado da seguinte maneira
                            
                            //Self.NossoNumeroGerado:=Self.CodigoCarteira_caixa+Self.NossoNumero_partedois;
                            //Self.DGNossoNUmero:=Self.Modulo11_CAIXAECONOMICA(Self.NossoNumeroGerado);
                            
                            if (length(AnossoNumero)>=length(Self.LoteRetorno.Convenioboleto.Get_CodigoCarteira_Caixa))
                            Then Begin
          
                                      temp:=copy(AnossoNumero,1,length(pcodigocarteicacaixa));
                                      if (temp=pcodigocarteicacaixa)
                                      Then Begin
                                                //O nosso numero que veio do banco esta com o codigo da carteira no inicio, tenho que tirar ele
                                                ANossoNumero:=Copy(AnossoNumero,length(pcodigocarteicacaixa)+1,length(AnossoNumero)-length(pcodigocarteicacaixa)-1);//o -1 � por causa do digito verificador
                                      End;
                            End;
                  End
                  Else Begin
                            //espaco notebook
                            //EspecificacaoCodBarrasCobranca_Sem_Registro_16posicoes_NN
                            //8000000000001076        X
                            //Uso da Empresa Identifica��o do T�tulo na Empresa 106 130
                            //o 8 � codigo da carteira
                            ANossoNumero := trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],106,25));
          


                            if (length(AnossoNumero)>=length(Self.LoteRetorno.Convenioboleto.Get_CodigoCarteira_Caixa))
                            Then Begin

                                      temp:=copy(AnossoNumero,1,length(pcodigocarteicacaixa));
                                      if (temp=pcodigocarteicacaixa)
                                      Then Begin
                                                //O nosso numero que veio do banco esta com o codigo da carteira no inicio, tenho que tirar ele
                                                ANossoNumero:=Copy(AnossoNumero,length(pcodigocarteicacaixa)+1,length(AnossoNumero)-length(pcodigocarteicacaixa)-1);//o -1 � por causa do digito verificador
                                      End;
                            End;


                  End;

                  Aseunumero := Copy(Self.Arqretorno.Strings[NumeroRegistro],106,25);
                  {Try
                    Adatavencimento := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],78,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],76,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],74,2)));
                  Except

                  End;}
                  ADatavencimento:=0;

                  Try
                      {********alterado por celio 20/10/2009****************************
                      Vou resgatar o valor pago pelo cliente e enviar como valor do doc
                       adicionado +1 ao numeroregistro pelo fato de estar na linha T, sendo que o valor
                        pago esta na linha U
                        
                      original ate 20/10/09
                      Avalordocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],82,15))/100;
                      }
                      Avalordocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro+1],78,15))/100;
                  Except

                  End;

                  Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
                  Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
                  Self.MemoRetorno.Add('Data Vencimento  :'+datetostr(Adatavencimento));
                  Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));
            end;

            //pegando a proxima linha
            NumeroRegistro := NumeroRegistro + 1;

            //Registro detalhe com tipo de segmento = U
            if Copy(Self.Arqretorno.Strings[NumeroRegistro],14,1) = 'U' then
            begin
                  AValorMoraJuros := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],18,15))/100;
                  AValorDesconto := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],33,15))/100;
                  AValorAbatimento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],48,15))/100;
                  AValorIOF := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],63,15))/100;
                  AValorOutrasDespesas := StrToFloat(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],108,15)))/100;
                  AValorOutrosCreditos := StrToFloat(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],123,15)))/100;
                  Try
                    ADataOcorrencia:= EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],142,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],140,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],138,2)));
                  Except
                        ADataOcorrencia:=0;
                  End;

                  Try                                                                                 //150                                                                //148                                                                //146
                    //MensagemAviso(Copy(Self.Arqretorno.Strings[NumeroRegistro],PPosicao+4,4)+'/'+Copy(Self.Arqretorno.Strings[NumeroRegistro],PPosicao+2,2)+'/'+Copy(Self.Arqretorno.Strings[NumeroRegistro],PPosicao,2));
                    ADataCredito := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],PPosicao+4,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],PPosicao+2,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],PPosicao,2)));
                  Except
                        ADataCredito:=0;
                  End;
                  Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
                  Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
                  Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
                  Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
                  Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
                  Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

                  if (ADataOcorrencia<>0)
                  Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
                  Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

                  if (ADataCredito<>0)
                  Then Self.MemoRetorno.Add('Data  Cr�dito    :'+datetostr(ADataCredito))
                  Else Self.MemoRetorno.Add('Data  Cr�dito    :');


                  NumeroRegistro := NumeroRegistro + 1;


            End; //if Copy(Retorno.Strings[NumeroRegistro],14,1) = 'U'
            //Gravando no banco de Dados

            Self.ZerarTabela;
            Self.Status:=dsinsert;
            Self.LoteRetorno.Submit_CODIGO(Plote);
            Self.Submit_CODIGO            (Self.Get_NovoCodigo);
            Self.Submit_NossoNumero       (ANossoNumero);
            Self.Submit_seunumero         (ASeuNumero);
            //Self.Submit_datavencimento    (datetostr(ADataVencimento));
            Self.Submit_valordocumento    (floattostr(AValorDocumento));
            Self.OCorrencia.Submit_codigo (AOCorrencia);
            Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
            Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
            Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
            Self.Submit_ValorIOF          (floattostr(AValorIOF));
            Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
            Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
            Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
            Self.Submit_DataCredito(datetostr(ADataCredito));
            Self.Submit_Processado('N');

            if (Self.Salvar(False)=False)
            then Begin
                      Messagedlg('Erro na tentativa de Salvar o Registro Linha N�'+inttostr(numeroregistro),mterror,[mbok],0);
                      exit;
            End;
      end;
      FDataModulo.IBTransaction.CommitRetaining;
      Self.MemoRetorno.Add('----------------------------------');
      Result := TRUE;
      FmostraStringList.Memo.Clear;
      FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
      FmostraStringList.ShowModal;

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

end;

Function TobjRegistroRetornoCobranca.AbreArquivo(var PnumeroLote:string):boolean;
Begin
     result:=Self.AbreArquivo(PnumeroLote,True);
End;

Function TobjRegistroRetornoCobranca.AbreArquivo(var PnumeroLote:string;PReprocessa:Boolean):boolean;
var
Dialogo:TopenDialog;
begin
     result:=False;
     try
        Dialogo:=TopenDialog.create(nil);
     Except
           Messagedlg('Erro na tentativa de criar o objeto de abertura de arquivo',mterror,[mbok],0);
           exit;
     End;
     Try
        if (Dialogo.execute=False)
        Then exit;

        if (Self.LoteRetorno.Convenioboleto.Pegaconvenio=True)
        Then Begin
                if (Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco='001')
                Then Begin
                          if (Self.LerRetornoCNAB240_BancodoBrasil(Self.LoteRetorno.Convenioboleto.Get_CODIGO,dialogo.filename)=True)
                          then Pnumerolote:=Self.LoteRetorno.Get_CODIGO //deu certo a importa��o ent�o mando reprocessar
                          Else exit;
                End
                Else
                   if (Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco='237') //bradesco
                   Then Begin
                            if (Self.LerRetornoCNAB400_Bradesco(Self.LoteRetorno.Convenioboleto.Get_CODIGO,dialogo.filename)=True)
                            then Pnumerolote:=Self.LoteRetorno.Get_CODIGO //deu certo a importa��o ent�o mando reprocessar
                            Else exit;
                   End
                   Else Begin
                               if (Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco='104')//caixa economica
                               Then Begin
                                         if (Self.LerRetornoCNAB240_CaixaEconomica(Self.LoteRetorno.Convenioboleto.Get_CODIGO,dialogo.filename)=True)
                                         then Pnumerolote:=Self.LoteRetorno.Get_CODIGO //deu certo a importa��o ent�o mando reprocessar
                                         Else exit;
                               End
                               Else Begin
                                         if (Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco='748') //sicredi
                                         Then Begin
                                                  if (Self.LerRetornoCNAB400_Sicred(Self.LoteRetorno.Convenioboleto.Get_CODIGO,dialogo.filename)=True)
                                                  then Pnumerolote:=Self.LoteRetorno.Get_CODIGO //deu certo a importa��o ent�o mando reprocessar
                                                  Else exit;
                                         End
                                         Else Begin
                                              If(Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco='399')//HSBC
                                              Then Begin
                                                  if (Self.LerRetornoCNAB400_HSBC(Self.LoteRetorno.Convenioboleto.Get_CODIGO,dialogo.filename)=True)
                                                  then Pnumerolote:=Self.LoteRetorno.Get_CODIGO //deu certo a importa��o ent�o mando reprocessar
                                                  Else exit;
                                              End
                                              else
                                                If(Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco='756') Then //SICOOB
                                                Begin
                                                  if (Self.LerRetornoCNAB400_SICOOB(Self.LoteRetorno.Convenioboleto.Get_CODIGO,dialogo.filename)=True) then
                                                    Pnumerolote:=Self.LoteRetorno.Get_CODIGO //deu certo a importa��o ent�o mando reprocessar
                                                  else
                                                    exit;
                                                End
                                                  Else Begin
                                                       Messagedlg('O sistema n�o foi implementado para reprocessar retornos do banco escolhido',mterror,[mbok],0);
                                                       exit;
                                                  End;
                                         End;
                               End;
                   End;
        End
        Else exit;

        if (PReprocessa)
        Then result:=Self.ReprocessaLote(Self.LoteRetorno.Get_CODIGO);

        result:=true;
     Finally
            Freeandnil(Dialogo);
     End;
end;


Function TobjRegistroRetornoCobranca.AbreArquivo_CobreBem(Var PUltimoLote:String):boolean;
var
Dialogo:TopenDialog;
begin
     result:=False;
     try
        Dialogo:=TopenDialog.create(nil);
     Except
           Messagedlg('Erro na tentativa de criar o objeto de abertura de arquivo',mterror,[mbok],0);
           exit;
     End;
     Try
        if (Dialogo.execute=False)
        Then exit;

        if (Self.LoteRetorno.Convenioboleto.Pegaconvenio=True)
        Then Begin
               if (Self.LoteRetorno.Convenioboleto.Get_CobreBem <> 'S')
               then Begin
                        MensagemErro('Esse conv�nio n�o � um conv�nio CobreBem');
                        exit
               end;

               if (Self.LerRetorno_CobreBem(Self.LoteRetorno.Convenioboleto.Get_CODIGO,dialogo.filename,PUltimoLote)=false)
               then Begin
                        MensagemErro('Erro ao tentar Ler o Arquivo de Retorno no CobreBem');
                        exit;
               end;

        End
        Else exit;


        result:=true;
     Finally
            Freeandnil(Dialogo);
     End;
end;


Function TObjRegistroRetornoCobranca.Opcoes(Plote: string):String;
var
PnumeroLote:string;
begin
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Importar Arquivo');//0
          RgOpcoes.Items.add('Reprocessar Lote');//1
          RgOpcoes.Items.add('Estorna Reprocessamento de  Lote');//2
          RgOpcoes.Items.add('Reprocessa Todos os Lotes');//3
          RgOpcoes.Items.add('Estorna Reprocesamento de Todos os Lotes');//4
          RgOpcoes.Items.add('Importar Arquivo CobreBem');//5
          RgOpcoes.Items.Add('Reprocessar Lote CobreBem');//6
          showmodal;
          if (Tag=0)
          Then exit;

          Case RgOpcoes.ItemIndex of

            0: Begin
                    PnumeroLote:='';
                    Self.AbreArquivo(PnumeroLote);
            End;
            1: Begin
                    Self.ReprocessaLote(Plote);
            End;
            2:Begin
                   Self.ExtornaReprocessamento(Plote);
            End;
            3:Begin
                  Self.ReprocessaTodosLotes;
            End;
            4:Begin
                  Self.ExtornaTodosLotes;
            End;
            5:Begin
                  Self.AbreArquivo_CobreBem(Result);
            end;
            6:Begin
                  Self.ReprocessaLote_CobreBem(Plote);
            end;

          End;
     End;
end;

procedure TObjREGISTRORETORNOCOBRANCA.EdtOcorrenciaExit(Sender: TObject;
  LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.OCorrencia.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ocorrencia.tabelaparaobjeto;

end;

procedure TObjREGISTRORETORNOCOBRANCA.EdtOcorrenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FOcorrencia:TFOCORRENCIARETORNOCOBRANCA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FOcorrencia:=TFOCORRENCIARETORNOCOBRANCA.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Ocorrencia.Get_Pesquisa,Self.Ocorrencia.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Ocorrencia.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Ocorrencia.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Ocorrencia.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Focorrencia);
     End;
end;

procedure TObjREGISTRORETORNOCOBRANCA.PesquisaRegistros(Plote: string);
begin
     With Self.ObjqueryPesquisa do
     begin
        close;
        SQL.clear;
        SQL.add('Select * from ViewRegistroRetornoCobranca where LoteRetorno='+Plote);
        if Plote=''
        Then exit;
        open;
     End;
end;

function TObjREGISTRORETORNOCOBRANCA.Get_Processado: string;
begin
     result:=Self.Processado;
end;

procedure TObjREGISTRORETORNOCOBRANCA.Submit_Processado(parametro: string);
begin
     Self.Processado:=parametro;
end;

function TObjREGISTRORETORNOCOBRANCA.Get_Situacao: string;
begin
     Result:=Self.Situacao;
end;

procedure TObjREGISTRORETORNOCOBRANCA.Submit_Situacao(parametro: string);
begin
     Self.Situacao:=parametro;
end;

procedure TObjREGISTRORETORNOCOBRANCA.EdtBoletoExit(Sender: TObject;
  LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Boleto.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Boleto.tabelaparaobjeto;

end;

procedure TObjREGISTRORETORNOCOBRANCA.EdtlancamentoExit(Sender: TObject;
  LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.lancamento.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.lancamento.tabelaparaobjeto;

end;




procedure TObjREGISTRORETORNOCOBRANCA.EdtBoletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FBoleto:TFBoletoBancario;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FBoleto:=TFBoletoBancario.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Boleto.Get_Pesquisa,Self.boleto.Get_TituloPesquisa,FBoleto)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.boleto.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.boleto.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.boleto.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fboleto);
     End;
end;


procedure TObjREGISTRORETORNOCOBRANCA.EdtBoletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FBoleto:TFBoletoBancario;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FBoleto:=TFBoletoBancario.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Boleto.Get_Pesquisa,Self.boleto.Get_TituloPesquisa,FBoleto)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.boleto.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fboleto);
     End;
end;

procedure TObjREGISTRORETORNOCOBRANCA.EdtlancamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.lancamento.Get_Pesquisa,Self.lancamento.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('historico').asstring;
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

Function TObjREGISTRORETORNOCOBRANCA.ReprocessaLote(Plote: string;ComCommit:boolean):boolean;
var
PNumeroConvenio,PCampoLivre,PNossoNumero:String;
Pposicao:integer;
pjuros,PValorBoletoSIS,PValorBoletobanco,pdesconto:Currency;
LocalizaPorNUmeroBanco,Achou:boolean;

begin
     result:=False;


     LocalizaPorNUmeroBanco:=False;

     if (ObjParametroGlobal.ValidaParametro('LOCALIZA BOLETO PELO NUMERO DO BANCO E NAO PELO CONVENIO')=true)
     Then begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then LocalizaPorNUmeroBanco:=True;
     End;

     {Usado apos a captura do arquivo, a captura apenas preenche registros
     na tabela Lote e Registros

     Passo para todos para processado, os que eu encontro o boleto coloco o numero do boleto
     e deixo o situacoa em brnaxco para ser processado no momento de quitar as pendencias

     os que nao encontram pendencias coloco como NE(nao encontrado boleto)}

     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.add('PROCESSAMENTO DOS REGISTROS DE RETORNO');
     FmostraStringList.Memo.lines.add(' ');
     FmostraStringList.Memo.lines.add('LOCALIZANDO BOLETOS ...');


     if (plote='')
     Then Begin
               Messagedlg('Escolha um lote',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.LoteRetorno.LocalizaCodigo(plote)=False)
     Then Begin
               Messagedlg('Lote N� '+Plote+' n�o encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.LoteRetorno.TabelaparaObjeto;
     PNumeroConvenio:=Self.LoteRetorno.Convenioboleto.Get_numeroconvenio;


     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          Sql.add('Select * from TabRegistroRetornoCobranca where LoteRetorno='+plote+' and Processado=''N'' ');
          open;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('N�o foi encontrado nenhum registro deste lote que n�o tenha sido processado',mterror,[mbok],0);
                    exit;
          End;
          first;

          Self.LoteRetorno.Status:=dsedit;
          Self.LoteRetorno.Submit_Processado('S');
          if (Self.LoteRetorno.Salvar(False)=False)
          Then Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Erro na tentativa de Passar o Lote para Processado=''S'' ',mterror,[mbok],0);
                    exit;
          End;



          While not(eof) do
          Begin
               //pega o nosso numero
               //separa a parte do conv�nio
               //pega apenas o campo livre
               //procura no cadastro de boleto
               //se encontrar guarda o numero do boleto no registroretornocobranca
               //para posteriormente efetuar as quita��es

               Self.ZerarTabela;
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               Self.Processado:='S';
               PnossoNumero:=Fieldbyname('nossonumero').asstring;
               PCampoLivre:=trim(PnossoNumero);

               Pposicao:=pos(PNumeroConvenio,PNossoNumero);
               PnossoNumero:=trim(Pnossonumero);

               if (Pposicao<>0)
               Then Begin//encontrou o numero do convenio
                         PCampoLivre:=Trim(copy(pnossonumero,pposicao+length(pnumeroconvenio),length(pnossonumero)-(pposicao+length(pnumeroconvenio))+1));
               End;
               //O tratamento se deu necessario porque no dia 18/10/07 ao fazer um boleto de teste em tres lagoas
               //o retorno do cobranca envia o numero 0004X  provalmente porque o digito verificador
               //no nosso numero foi 10

               if (PCampoLivre[length(Pcampolivre)]='X')
               Then PcampoLivre:=copy(pcampolivre,1,length(pcampolivre)-1);

               achou:=True;
               if (LocalizaPorNUmeroBanco)
               Then Begin
                         if (Self.Boleto.LocalizaBoletoBanco(PCampolivre,Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco)=False)
                         Then Begin
                                   FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' N�o Encontrado');
                                   Self.Boleto.ZerarTabela;
                                   Self.Situacao:='BOLETO N�O ENCONTRADO';//n�o encontrado
                                   achou:=false;
                         End;
               End
               Else BEgin
                         if (Self.Boleto.LocalizaBoletoConvenio(PCampolivre,Self.LoteRetorno.Convenioboleto.get_codigo)=False)
                         Then Begin
                                   FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' N�o Encontrado');
                                   Self.Boleto.ZerarTabela;
                                   Self.Situacao:='BOLETO N�O ENCONTRADO';//n�o encontrado
                                   achou:=false;
                         End;
               End;

               if (achou)
               Then Begin
                         Self.Boleto.TabelaparaObjeto;
                         try
                            PValorBoletoSis:=0;
                            if (trim(Self.Boleto.get_valordoc)<>'')
                            Then PValorBoletoSIS:=Strtofloat(Self.Boleto.get_valordoc);
                         Except
                            PValorBoletoSis:=0;
                         End;

                         Try
                            PValorBoletobanco:=Strtofloat(Self.Get_valordocumento);
                         Except
                            PValorBoletobanco:=0;
                         End;

                         pjuros:=0;
                         if (PValorBoletoSIS<PValorBoletobanco)
                         Then Begin
                                   //O valor que veio do banco � maior, pode ter sido o juro
                                   //que nao foi adicionado no campo de juros
                                   //e sim no campo de valor
                                   Try
                                     pjuros:=strtocurr(Self.Get_ValorJuros);
                                   Except
                                     pjuros:=0;
                                   End;

                                   Self.Submit_ValorJuros(currtostr(PvalorBoletoBanco-Pvalorboletosis));
                                   Self.Submit_valordocumento(FloatToStr(Pvalorboletosis+(PvalorBoletoBanco-Pvalorboletosis)));
                         End;

                         pdesconto:=0;
                         if (PValorBoletoSIS>PValorBoletobanco)
                         Then Begin

                                {********************110808 alterado por celio
                                Self.Situacao:='DIVERG. DE VALOR ENTRE BOLETO NO SIST. E RETORNO';
                                FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' DIVERG�NCIA DE VALOR ENTRE BOLETO NO SISTEMA E NO RETORNO. Boleto Sistema N� '+Self.Boleto.get_codigo);
                                Self.Boleto.ZerarTabela;}

                                Try
                                      pdesconto:=StrToCurr(self.Get_ValorDesconto);
                                Except
                                      pdesconto:=0;
                                End;

                                Self.Submit_ValorDesconto(currtostr(Pvalorboletosis-PvalorBoletoBanco));
                                Self.Submit_valordocumento(FloatToStr(Pvalorboletosis-(Pvalorboletosis-PvalorBoletoBanco)));
                                FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' Encontrado Boleto N� '+Self.Boleto.get_codigo);
                         End
                         Else FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' Encontrado Boleto N� '+Self.Boleto.get_codigo);
               End;

               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Salvar o Registro N�'+Self.codigo,mterror,[mbok],0);
                         FmostraStringList.Memo.Lines.add('Erro ao tentar atualizar o Registro N�'+Self.codigo+', nosso numero='+Self.NossoNumero);
               End;

               next;
          End;

          if (Self.QuitaPendencias(Plote)=False)
          Then
          //alterado por celio 220808 comcommit adiconado
          If(ComCommit=true)
          Then FdataModulo.IBTransaction.RollbackRetaining
          Else FdataModulo.IBTransaction.CommitRetaining;

          //FmostraStringList.showmodal;
          result:=true;
     End;
end;

function TObjREGISTRORETORNOCOBRANCA.QuitaPendencias(Plote: string): boolean;
var
  ObjqueryBoleto:TibQuery;
  Pvalorrecebido,PsomaJuros,PsomaDesconto, vdoc,vdesc,vjuros,VBolTEMP,VBolSis:Currency;
  TmptipoLanctoLigado,PCodigoLancamentoPai,TmpTipoLanctoDesconto:string;
  PStrpendencia,PStrValorLancto:TStringList;
  Ppendencia,PSaldo,Pvalor,Pdesconto:TStringList;
  cont:integer;
  ObjValorestemp:TObjValores;
  TmpHistoricoDesconto:String;
begin
     result:=False;
     {
     Este procedimento, seleciona os registros com PROCESSADO='S', com boletos e situacao=''
     ou seja, atraves desse procedimento, ele tenta quintar as pendencias
     ligadas a estes boletos
     }

     Try
        ObjValorestemp:=TObjValores.Create;

        ObjqueryBoleto:=TibQuery.create(nil);
        ObjqueryBoleto.Database:=Self.Objquery.Database;
        Ppendencia:=TStringList.create;
        PSaldo:=TStringList.create;
        Pvalor:=TStringList.create;
        Pdesconto:=TStringList.create;
     Except
        on e:Exception do
        Begin
              MensagemErro('Erro na tentativa de Criar o Objeto de Lan�amento. Erro : '+e.Message);
              exit;
        End;
     End;

     FmostraStringList.Memo.lines.add(' ');
     FmostraStringList.Memo.lines.add('QUITANDO AS PEND�NCIAS DOS BOLETOS...');
     FmostraStringList.Memo.lines.add(' ');

Try

     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select Codigo,boleto from TabRegistroRetornoCobranca where Processado=''S'' and LoteRetorno='+Plote+' and (Situacao is null or Situacao='''') and not(Boleto is null) and lancamento is null');
          open;

          While not(eof) do
          begin
               Self.LocalizaCodigo(fieldbyname('codigo').Asstring);
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               PCodigoLancamentoPai:='';
               //***************************************************************
               if (ObjValorestemp.Lancamento.Pendencia.LocalizaporBoleto(Self.Boleto.get_codigo)=False)
               Then Begin
                         FmostraStringList.Memo.Lines.add('Pend�ncia n�o Localizada para o Boleto '+Self.Boleto.Get_CODIGO);
                         Self.Situacao:='PEND�NCIA N�O ENCONTRADA';
                         FmostraStringList.Memo.lines.add(Self.Boleto.Get_Codigo+' PEND�NCIA N�O ENCONTRADA');
               End
               Else Begin
                         //somando os saldos das pend�ncias
                         //para ver se bate com o valor do boleto
                        ObjQueryBoleto.close;
                        ObjQueryBoleto.sql.clear;
                        ObjQueryBoleto.sql.add('select sum(saldo) as SOMA from Tabpendencia where Boletobancario='+Self.Boleto.get_codigo);
                        ObjQueryBoleto.open;

                        vdoc:=0;
                        vdesc:=0;
                        vjuros:=0;
                        If(Self.Get_valordocumento<>'')
                        Then  vdoc:=StrToCurr(Self.Get_valordocumento);
                        If(Self.Get_ValorDesconto<>'')
                        Then vdesc:=StrToCurr(Self.Get_ValorDesconto);
                        If(Self.Get_ValorJuros<>'')
                        Then vjuros:=StrToCurr(Self.Get_ValorJuros);

                        {*******************************************************
                        devo verificar o valor da soma das pendencias com o valor do boleto.
                        Se for diferente, alteraram o valor das pendencias ou o boleto veio com valor errado
                        No procedimento reprocessalote, se o caixa que recebeu o boleto colocou o juros
                        direto no valor do doc e nao preencheu o campo juros, o sistema efetua o registro do
                        juros ou desconto no registroretornocobranca.
                        Ent�o para verificar se o valor da pendencia nao foi alterado, somo o desconto e subtraio
                        o juros para ter o valor original do doc.
                        *******************************************************}
                        VBolSis := ObjQueryBoleto.Fieldbyname('soma').AsCurrency;

                        If(VBolSis=vdoc)
                        Then VBolTEMP:=vdoc
                        Else VBolTEMP:=vdoc-vjuros+vdesc;

                        if (VBolSis<>VBolTEMP)
                        Then Begin
                                  Self.Situacao:='SALDO DAS PEND�NCIAS DIFER. DO VALOR DO BOLETO';
                                  FmostraStringList.Memo.Lines.add('BOLETO: '+Self.Boleto.Get_CODIGO+' - SALDO DAS PEND�NCIAS DIFER. DO VALOR DO BOLETO');
                                  FmostraStringList.Memo.lines.add(' ');
                        End
                        Else Begin

                                 if(OCorrencia.Get_LiquidaPendencia='S')
                                 then begin

                                       FmostraStringList.Memo.Lines.add('QUITANDO PENDENCIAS DO BOLETO '+Self.Boleto.Get_CODIGO);

                                       //verificando se teve juros
                                       {
                                       PsomaJuros:=( strtofloat(Self.Get_ValorJuros)+ strtofloat(Self.Get_ValorIOF)+ strtofloat(Self.Get_ValorOutrasDespesas)+ strtofloat(Self.Get_ValorOutrosCreditos));
                                       PsomaDesconto:=(strtofloat(Self.Get_ValorDesconto)+ strtofloat(Self.Get_ValorAbatimento));
                                       }
                                       PsomaJuros := vdoc - VBolSis;
                                       PsomaDesconto := VBolSis - vdoc;
                                       If(PsomaJuros<0)
                                       Then PsomaJuros := 0;
                                       If(PsomaDesconto<0)
                                       Then PsomaDesconto := 0;

                                       If(PsomaJuros <> ( strtofloat(Self.Get_ValorJuros)+ strtofloat(Self.Get_ValorIOF)+ strtofloat(Self.Get_ValorOutrasDespesas)+ strtofloat(Self.Get_ValorOutrosCreditos)))
                                       Then Begin
                                            FmostraStringList.Memo.Lines.add('   * BOLETO: '+Self.Boleto.Get_CODIGO+' - Valor de Juros/IOF/Outros Cr�ditos lan�ados incorretamente.');
                                            FmostraStringList.Memo.Lines.add('   Registro de retorno : '+ObjqueryPesquisa.FieldByName('Codigo').asstring+'. Boleto quitado com o valor pago');
                                       End;

                                       If(PsomaDesconto <> (strtofloat(Self.Get_ValorDesconto)+ strtofloat(Self.Get_ValorAbatimento)))
                                       Then Begin
                                            FmostraStringList.Memo.Lines.add('   * BOLETO: '+Self.Boleto.Get_CODIGO+' - Valor de Desconto/Abatimento lan�ados incorretamente.');
                                            FmostraStringList.Memo.Lines.add(' Registro de retorno : '+ObjqueryPesquisa.FieldByName('Codigo').asstring+'. Boleto quitado com o valor pago');
                                       End;

                                       {
                                       Este era a f�rmula utilizada at� 11/08/08
                                       PsomaJuros:=(
                                       strtofloat(Self.Get_ValorJuros)+
                                       strtofloat(Self.Get_ValorIOF)+
                                       strtofloat(Self.Get_ValorOutrasDespesas))-
                                       (strtofloat(Self.Get_ValorDesconto)+
                                       strtofloat(Self.Get_ValorAbatimento)+
                                       strtofloat(Self.Get_ValorOutrosCreditos));

                                       Pvalorrecebido:=strtofloat(Self.valordocumento)+PsomaJuros;
                                       }
                                       //Alterado por celio 110808 -  psomajuros recebia juros + desconto
                                       Pvalorrecebido:=strtofloat(Self.valordocumento);
                                 
                                       //******************lancamento pai********************
                                       //Grava o Valor Recebido no Boleto, com juros ou descontos
                                       //o  valor que realmente sera creditado no banco

                                       PcodigolancamentoPai:=ObjValorestemp.Lancamento.Get_NovoCodigo;
                                       ObjValorestemp.Lancamento.ZerarTabela;
                                       ObjValorestemp.Lancamento.Submit_CODIGO(PCodigoLancamentoPai);
                                       ObjValorestemp.Lancamento.submit_Data(Self.Get_DataCredito);


                                       ObjValorestemp.Lancamento.submit_Historico('REC.BOL N�'+Self.Boleto.Get_NumeroBoleto+' '+get_campoTabela(Self.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_CampoNome,'codigo',Self.Lancamento.Pendencia.Titulo.CREDORDEVEDOR.Get_Tabela,Self.Lancamento.Pendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
                                       ObjValorestemp.Lancamento.submit_Valor(floattostr(pvalorrecebido));
                                       ObjValorestemp.Lancamento.submit_LancamentoPai('');
                                       if (ObjValorestemp.Lancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO SEM PEND�NCIA')=fALSE)
                                       Then Begin
                                               Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO SEM PEND�NCIA", n�o foi encontrado!',mterror,[mbok],0);
                                               exit;
                                       End;
                                       ObjValorestemp.Lancamento.ObjGeraLancamento.TabelaparaObjeto;
                                       ObjValorestemp.Lancamento.TipoLancto.Submit_CODIGO(ObjValorestemp.Lancamento.ObjGeraLancamento.TipoLAncto.Get_CODIGO);
                                       ObjValorestemp.Lancamento.status:=dsinsert;
                                       if (ObjValorestemp.Lancamento.SalvarSemPendencia(False)=False)
                                       Then Begin
                                                Messagedlg('Erro na Tentativa de Salvar o Lan�amento!',mterror,[mbok],0);
                                                exit;
                                       End;
                                       //**********************************************************

                                       //GUARDANDO AS PENDENCIAS EM STRINGLIST, ASSIM COMO
                                       //SEU SALDO E VALOR QUE SERA QUITADO DELAS

                                       Ppendencia.clear;
                                       PSaldo.Clear;
                                       Pvalor.clear;

                                       ObjQueryBoleto.close;
                                       ObjQueryBoleto.sql.clear;
                                       ObjQueryBoleto.sql.add('select codigo,saldo from Tabpendencia where Boletobancario='+Self.Boleto.get_codigo);
                                       ObjQueryBoleto.open;
                                 
                                       Objqueryboleto.first;
                                       While not(Objqueryboleto.eof) do
                                       begin
                                           //gravo o numero da pendencia, o saldo e o valor que sera
                                           //quitado dela
                                           Ppendencia.add(Objqueryboleto.fieldbyname('codigo').asstring);
                                           psaldo.add(Objqueryboleto.fieldbyname('saldo').asstring);
                                           pvalor.add(Objqueryboleto.fieldbyname('saldo').asstring);
                                           Pdesconto.Add('0');
                                           Objqueryboleto.next;
                                       End;
                                       //********************************************************************
                                 
                                       if (PsomaJuros>0)//juros
                                       Then Begin
                                                 //Calculo dos Juros
                                                 {o Juro tem q ser rateado entre todas as pend�ncias desse boleto, caso seja uma �nica pend�ncia, sem problema
                                                  agora se for mais de uma, preciso, descobrir a porcentagem que ela representa em relacao ao total
                                                  e depois lancar essa porcentagem de Juros, exemplo:
                                                  Duas Pendencias
                                                  01 - Valor R$ 100,00
                                                  02 - Valor R$ 200,00
                                 
                                                  Total do Boleto R$ 300,00
                                 
                                                  01 - Representa 1/3
                                                  02 - Representa 2/3

                                                  Juros de 20,00

                                                  Lanco 1/3 de 20,00 de Juros na 01
                                                  Lanco 2/3 de 20,00 de Juros na 02
                                                  }
                                                  if (Self.Calcula_juros_Pendencias(Ppendencia,Psaldo,Pvalor,psomajuros)=False)
                                                  then exit;
                                       End
                                       Else begin
                                                 If (PsomaDesconto>0)//desconto
                                                 Then begin
                                                           //lan�amento do desconto, procedimento idem ao juros>0 porem, o desconto tem que lan�ar o valor mesmo
                                                           If (Self.Calcula_Desconto_Pendencias(Ppendencia,Psaldo,Pvalor,Pdesconto,PsomaDesconto)=False)
                                                           Then exit;
                                                 End;
                                       End;

                                       //Ja tenho as pendencias e os valores que deverao ser quitadas dela
                                       //executando no lancamento filho
                                       If (ObjValorestemp.Lancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO')=fALSE)
                                       Then Begin
                                               Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="SALVAR LAN�AMENTO LIGADO A OUTRO LAN�AMENTO", n�o foi encontrado!',mterror,[mbok],0);
                                               exit;
                                       End;
                                       ObjValorestemp.Lancamento.ObjGeraLancamento.TabelaparaObjeto;
                                       TmptipoLanctoLigado:=ObjValorestemp.Lancamento.ObjGeraLancamento.Get_TipoLAncto;

                                       //*******************************

                                       //******290808 - celio
                                       //localizando informacoes para lancar desconto

                                       If (ObjValorestemp.Lancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','DESCONTO A RECEBER')=False)
                                       Then Begin
                                            Messagedlg('N�o foi encontrado o Gerador de Lan�amento, dados: '+#13+'Objeto:"OBJLANCAMENTO" /HISTORICOGERADOR="DESCONTO A RECEBER"',mterror,[mbok],0);
                                            exit;
                                       End;
                                       ObjValorestemp.Lancamento.ObjGeraLancamento.TabelaparaObjeto;
                                       TmpTipoLanctoDesconto:=ObjValorestemp.Lancamento.ObjGeraLancamento.Get_TipoLAncto;
                                       TmpHistoricoDesconto:=ObjValorestemp.Lancamento.ObjGeraLancamento.Get_Historico;

                                       //********************

                                       for cont:=0 to Ppendencia.count-1 do
                                       Begin
                                            FmostraStringList.Memo.Lines.add('   PEND�NCIA '+ppendencia[cont]);

                                            Try
                                 
                                               //localizando a pendencia
                                               ObjValorestemp.Lancamento.ZerarTabela;
                                               ObjValorestemp.Lancamento.Pendencia.LocalizaCodigo(Ppendencia[cont]);
                                               ObjValorestemp.Lancamento.Pendencia.TabelaparaObjeto;
                                 
                                               If(PsomaDesconto>0)
                                               Then Begin
                                                    //lancando o desconto
                                                    ObjValorestemp.Lancamento.Submit_CODIGO(ObjValorestemp.Lancamento.get_novocodigo);
                                                    ObjValorestemp.Lancamento.Submit_TipoLancto(TmpTipoLanctoDesconto);
                                                    ObjValorestemp.Lancamento.Submit_Valor(Pdesconto[cont]);
                                                    ObjValorestemp.Lancamento.Submit_Historico(TmpHistoricoDesconto);
                                                    ObjValorestemp.Lancamento.Submit_Data(Self.Get_DataCredito);
                                                    ObjValorestemp.Lancamento.Submit_LancamentoPai(PCodigoLAncamentoPai);
                                                    ObjValorestemp.Lancamento.Status:=dsinsert;
                                              
                                                    If (ObjValorestemp.Lancamento.Salvar(False,False,'',false,true)=False)
                                                    Then Begin
                                                              Messagedlg('Lan�amentos Desconto Cancelados!',mterror,[mbok],0);
                                                              ObjValorestemp.Lancamento.Status:=dsInactive;
                                                              exit;
                                                    End;
                                               End;
                                               //lancando o valor recebido
                                               ObjValorestemp.Lancamento.Submit_CODIGO(ObjValorestemp.Lancamento.get_novocodigo);
                                               ObjValorestemp.Lancamento.Submit_TipoLancto(tmptipolanctoligado);
                                               ObjValorestemp.Lancamento.Submit_Valor(Pvalor[cont]);
                                               ObjValorestemp.Lancamento.Submit_Historico('REC.BOLETO '+ObjValorestemp.Lancamento.Pendencia.Titulo.Get_HISTORICO);
                                               ObjValorestemp.Lancamento.Submit_Data(Self.Get_DataCredito);
                                               ObjValorestemp.Lancamento.Submit_LancamentoPai(PCodigoLAncamentoPai);
                                               ObjValorestemp.Lancamento.Status:=dsinsert;

                                               If (ObjValorestemp.Lancamento.Salvar(False,False,'',true,true)=False)
                                               Then Begin
                                                         Messagedlg('Lan�amentos Filhos Cancelados!',mterror,[mbok],0);
                                                         ObjValorestemp.Lancamento.Status:=dsInactive;
                                                         exit;
                                               End;

                                            Except
                                            End;
                                       End;//for de todas as pendencias filhas
                                 
                                       //lancando o recebimento no banco
                                       ObjValorestemp.ZerarTabela;
                                       ObjValorestemp.status:=dsinsert;
                                       ObjValorestemp.Portador.Submit_CODIGO(self.Boleto.Convenioboleto.Portador.Get_CODIGO);
                                       ObjValorestemp.Lancamento.Submit_CODIGO(PCodigoLancamentoPai);
                                       ObjValorestemp.Submit_CODIGO(ObjValorestemp.Get_NovoCodigo);
                                       ObjValorestemp.Submit_Tipo('D');
                                       ObjValorestemp.Submit_Historico('REC.BOLETO BANC�RIO N�'+Self.Boleto.Get_NumeroBoleto);
                                       ObjValorestemp.Submit_Valor(floattostr(pvalorrecebido));
                                       if (ObjValorestemp.salvar(false,True)=False)
                                       Then Begin
                                                 Messagedlg('Erro na tentativa de gravar o lancto no portador',mterror,[mbok],0);
                                                 exit;
                                 
                                       end;
                                 
                                       //lancando a contabilidade
                                       if (ObjValorestemp.Lancamento.ExportaContabilidade_Recebimento_Lote(PCodigoLAncamentoPai)=False)
                                       Then Begin
                                                 Messagedlg('Erro na tentativa de exportar a contabilidade do recebimento',mterror,[mbok],0);
                                                 exit;
                                       End;
                                       Self.Situacao:='PEND�NCIAS QUITADAS COM SUCESSO';
                                       FmostraStringList.Memo.Lines.add(Self.Situacao+' LAN�AMENTO '+PCodigoLancamentoPai);
                                       FmostraStringList.Memo.lines.add(' ');
                                 end;

                        End;//else  - o saldo nao diverge

               End;//else pendencia encontrada

               Self.lancamento.submit_codigo(PCodigoLancamentoPai);
               if (self.Salvar(False)=False)
               then Begin
                         Messagedlg('Erro na tentativa de gravar a SITUACAO no registro de retorno',mterror,[mbok],0);
                         exit;
               end;

               next;
          End;//while
          result:=true;
          FmostraStringList.Memo.lines.add(' ');
          FmostraStringList.Memo.lines.add('REPROCESSAMENTO CONCLU�DO!!');

     End;

Finally
       ObjValorestemp.free;
       Freeandnil(ObjqueryBoleto);
       Freeandnil(Ppendencia);
       Freeandnil(PSaldo);
       Freeandnil(Pvalor);
       Freeandnil(Pdesconto);
End;

end;


function TObjREGISTRORETORNOCOBRANCA.Calcula_juros_Pendencias(Ppendencia,
  Psaldo, Pvalor: TStringList; PvalorJuros: Currency): Boolean;
var
cont:integer;
pdiferenca,pporcento,PsomaTotal,Psomafinal:Currency;
pvaloratual:string;
begin
     result:=False;
     {este procedimento rateia em % os juros para cada pendencia}
     //somando todos os saldos
     PsomaTotal:=0;
     For cont:=0 to Ppendencia.count-1 do
     Begin
          PsomaTotal:=PsomaTotal+strtofloat(psaldo[cont]);
     End;

     //distribuindo os juros de acordo com a %

     for cont:=0 to ppendencia.Count-1 do
     Begin
          //descobrindo quantos % a pendencia representa em relacao ao total
          //PSOMATOTAL        100%
          //saldopendencia       X
          //X=(saldopendencia*100)/PSOMATOTAL
          pporcento:=(Strtofloat(Psaldo[cont])*100)/PSomaTotal;
          //aumentando o Valor na porcentagem escolhida com o Juro
          PValorAtual:=Pvalor[cont];
          PvalorAtual:=tira_ponto(formata_valor(strtofloat(Pvalor[cont])+((pvalorjuros*pporcento)/100)));
          Pvalor[cont]:=PvalorAtual;
     End;
     //verificando se o valor final bate

     PsomaFinal:=0;
     For cont:=0 to Ppendencia.count-1 do
     Begin
          PsomaFinal:=PsomaFinal+strtofloat(pvalor[cont]);
     End;

     Pdiferenca:=(PsomaTotal+PvalorJuros)-PSomaFinal;

     if (Pdiferenca>0)//se sobrar alguma coisa lanco na ultima pendencia
     Then Pvalor[Pvalor.count-1]:=floattostr(strtofloat(Pvalor[pvalor.count-1])+Pdiferenca);

     result:=true;

end;


function TObjREGISTRORETORNOCOBRANCA.Calcula_Desconto_Pendencias(Ppendencia,
  Psaldo, Pvalor, Pdesconto: TStringList; PvalorDesconto: Currency): Boolean;
var
cont:integer;
pdiferenca,pporcento,PsomaTotal,Psomafinal:Currency;
pvaloratual:string;
begin
     result:=False;
     {este procedimento rateia em % o desconto para cada pendencia}
     //somando todos os saldos
     PsomaTotal:=0;
     For cont:=0 to Ppendencia.count-1 do
     Begin
          PsomaTotal:=PsomaTotal+strtofloat(psaldo[cont]);
     End;

     //distribuindo os descontos de acordo com a %

     for cont:=0 to ppendencia.Count-1 do
     Begin
          //descobrindo quantos % a pendencia representa em relacao ao total
          //PSOMATOTAL        100%
          //saldopendencia       X
          //X=(saldopendencia*100)/PSOMATOTAL
          pporcento:=(Strtofloat(Psaldo[cont])*100)/PSomaTotal;
          //aumentando o Valor na porcentagem escolhida com o Juro
          PValorAtual := Pvalor[cont];
          PvalorAtual := tira_ponto(formata_valor(strtofloat(Pvalor[cont])-((PvalorDesconto*pporcento)/100)));
          Psaldo[cont] := CurrToStr(StrToCurr(Pvalor[cont])-StrToCurr(PvalorAtual));
          Pvalor[cont] := PvalorAtual;
          PDesconto[cont] := Currtostr((PvalorDesconto*pporcento)/100);
     End;
     //verificando se o valor final bate

     PsomaFinal:=0;
     For cont:=0 to Ppendencia.count-1 do
     Begin
          PsomaFinal:=PsomaFinal+strtofloat(pvalor[cont]);
     End;

     Pdiferenca:=(PsomaTotal-PvalorDesconto)-PSomaFinal;

     if (Pdiferenca>0)//se sobrar alguma coisa lanco na ultima pendencia
     Then Pvalor[Pvalor.count-1]:=floattostr(strtofloat(Pvalor[pvalor.count-1])+Pdiferenca);

     result:=true;

end;


Function TObjREGISTRORETORNOCOBRANCA.ExtornaReprocessamento(Plote: string;ComCommit:boolean):Boolean;
var
pcodigolancamento:string;
begin
     //esse procedimento ira pegar todos os Registros
     //zerar a situacao e o processado
     //e no caso de registro que possuam LANCAMENTO de quitacao
     //excluir
     Result:=false;
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo from TabRegistroRetornoCobranca Where LoteRetorno='+Plote);
          open;
          if (recordcount=0)
          Then Begin
                    Messagedlg('N�o existe nenhum registro para este Lote escolhido',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          While not(eof) do
          Begin
               Self.ZerarTabela;
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               Self.Processado:='N';
               Self.Boleto.ZerarTabela;
               Self.Situacao:='';
               PcodigoLancamento:=self.Lancamento.Get_CODIGO;
               self.Lancamento.ZerarTabela;
               if (Self.Salvar(False)=False)
               then begin
                         FDataModulo.IBTransaction.RollbackRetaining;
                         Messagedlg('N�o foi poss�vel salvar as altera��es no registro '+Self.CODIGO,mterror,[mbok],0);
                         exit;
               end;

               if (pcodigolancamento<>'')
               Then begin
                         if (Self.Lancamento.ApagaLancamento(pcodigolancamento,False)=False)
                         then begin
                                   FDataModulo.IBTransaction.RollbackRetaining;
                                   Messagedlg('N�o foi poss�vel excluir o lan�amento do registro '+Self.CODIGO,mterror,[mbok],0);
                                   exit;
                         end;
               End;
               next;
          End;


          if (Self.LoteRetorno.LocalizaCodigo(Plote)=False)
          Then begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    mensagemaviso('Lote n�o encontrado para ser estornado');
                    exit;
          End;

          Self.LoteRetorno.TabelaparaObjeto;
          Self.LoteRetorno.Status:=dsedit;
          Self.LoteRetorno.Submit_Processado('N');

          if (Self.LoteRetorno.Salvar(False)=False)
          Then Begin
                    mensagemerro('Erro na tentativa de Passar o Campo Processado para "N" do lote');
                    exit;
          end;

          //alterado por celio 220808 comcommit adiconado. antes salvava ao final da execucao do proc.
          If(ComCommit=true)
          Then FDataModulo.IBTransaction.CommitRetaining;
          result:=true;
     End;
end;

procedure TObjREGISTRORETORNOCOBRANCA.imprimeInformacoesLote(PcodigoLote:string);
var
cont,linha:integer;
pcodigo:string;
begin
     if (PcodigoLote='')
     Then begin
               Messagedlg('Escolha o Lote que deseja Imprimir as Informa��es',mtinformation,[mbok],0);
               exit;
     end;

     if (Self.LoteRetorno.LocalizaCodigo(PcodigoLote)=False)
     Then Begin
               Messagedlg('Lote n�o encontrado',mtinformation,[mbok],0);
               exit;
     End;
     Self.LoteRetorno.TabelaparaObjeto;
     //*************************************************************************
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select TRRC.CODIGO,TRRC.LoteRetorno,TRRC.OCorrencia,TabOcorrenciaRetornoCobranca.nome as NOMEOCORRENCIA,');
          sql.add('TRRC.NossoNumero,TRRC.seunumero,TRRC.datavencimento,');
          sql.add('TRRC.valordocumento,');
          sql.add('TRRC.ValorJuros,TRRC.ValorDesconto,TRRC.ValorAbatimento,');
          sql.add('TRRC.ValorIOF,TRRC.ValorOutrasDespesas,TRRC.ValorOutrosCreditos,TRRC.DataOcorrencia,');
          sql.add('TRRC.DataCredito,');
          sql.add('TRRC.processado,');
          sql.add('TRRC.situacao,');
          sql.add('TRRC.boleto,TRRC.lancamento,');
          sql.add('Tabpendencia.Codigo as PENDENCIA,');
          sql.add('proctitulopendencias.CD_razaosocial as NOMECREDORDEVEDOR,');
          sql.add('proctitulopendencias.CodigoCredorDevedor,');
          sql.add('proctitulopendencias.historico');

          sql.add('from  TabRegistroRetornoCobranca TRRC');
          sql.add('join TabOcorrenciaRetornoCobranca on TRRC.Ocorrencia=TabOcorrenciaRetornoCobranca.codigo');
          sql.add('left join TabBoletoBancario on TRRC.Boleto=TabBoletoBancario.codigo');
          sql.add('left join tabpendencia on TabBoletoBancario.codigo=Tabpendencia.boletobancario');
          sql.add('left join proctitulopendencias on tabpendencia.codigo=proctitulopendencias.pendencia');
          sql.add('where TRRC.loteretorno='+pcodigolote);
          sql.add('order by TRRC.codigo');
          open;
          first;
          if (recordcount=0)
          Then begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mtinformation,[mbok],0);
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.abrir;


          if (FreltxtRDPRINT.RDprint.Setup=False)
          then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;

          FreltxtRDPRINT.RDprint.impc(linha,45,'REGISTROS DO LOTE N�'+PcodigoLote,[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.Imp(linha,1,'Conv�nio  : '+Self.LoteRetorno.Convenioboleto.Get_Nome);
          inc(linha,1);

          FreltxtRDPRINT.RDprint.Imp(linha,1,'Processado: '+Self.LoteRetorno.Get_Processado);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('OCORR�NCIA',15,' ')+' '+
                                              CompletaPalavra('NOSSO N�MERO',20,' ')+' '+
                                              CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                              CompletaPalavra('DT CR�DITO',10,' ')+' '+
                                              CompletaPalavra('PROC',4,' ')+' '+
                                              CompletaPalavra('SITUA��O',20,' ')+' '+
                                              CompletaPalavra('BOLETO',6,' '),[negrito]);
          inc(linha,1);

          PCodigo:='';
          While not(eof) do
          Begin
              if (PCodigo<>Fieldbyname('codigo').asstring)//trocou o registro
              Then Begin
                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                        FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('ocorrencia').asstring+'-'+fieldbyname('nomeocorrencia').asstring,15,' ')+' '+
                                                           CompletaPalavra(fieldbyname('nossonumero').asstring,20,' ')+' '+
                                                           CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valordocumento').asstring),12,' ')+' '+
                                                           CompletaPalavra(fieldbyname('datacredito').asstring,10,' ')+' '+
                                                           CompletaPalavra(fieldbyname('processado').asstring,4,' ')+' '+
                                                           CompletaPalavra(fieldbyname('situacao').asstring,20,' ')+' '+
                                                           CompletaPalavra(fieldbyname('boleto').asstring,6,' '));
                                                           //CompletaPalavra(fieldbyname('lancamento').asstring,10,' ')+' '+
                                                           //CompletaPalavra(fieldbyname('pendencia').asstring,10,' '));
                        inc(linha,1);
                        PCodigo:=Fieldbyname('codigo').asstring;
              End;
              //Imprimindo a pend�ncia e o lan�amento de quita��o
              if (Fieldbyname('pendencia').asstring<>'')
              Then Begin
                        FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                        FreltxtRDPRINT.RDprint.Impf(linha,5,'PEND: '+CompletaPalavra(fieldbyname('pendencia').asstring,06,' ')+' '+
                                                           'NOME: '+completapalavra(Fieldbyname('codigocredordevedor').asstring+'-'+Fieldbyname('nomecredordevedor').asstring,20,' ')+' '+
                                                           'LANCTO: '+CompletaPalavra(fieldbyname('lancamento').asstring,6,' ')+' '+
                                                           'HIST�RICO: '+CompletaPalavra(fieldbyname('historico').asstring,20,' '),[italico]);
                        inc(linha,1);
              end;

              next;
          End;
          FreltxtRDPRINT.RDprint.Fechar;

     End;
end;


Function TObjREGISTRORETORNOCOBRANCA.retornapendencias(PcodigoLote:string;PPendencia,PvalorLancamento:TStringList):boolean;
var
cont,linha:integer;
pcodigo:string;
begin
     result:=False;
     Ppendencia.clear;
     PvalorLancamento.clear;

     if (PcodigoLote='')
     Then begin
               Messagedlg('Escolha o Lote que deseja Imprimir as Informa��es',mtinformation,[mbok],0);
               exit;
     end;

     if (Self.LoteRetorno.LocalizaCodigo(PcodigoLote)=False)
     Then Begin
               Messagedlg('Lote n�o encontrado',mtinformation,[mbok],0);
               exit;
     End;
     Self.LoteRetorno.TabelaparaObjeto;
     //*************************************************************************
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select Filho.Pendencia,Filho.valor');
          sql.add('from  TabRegistroRetornoCobranca TRRC');
          sql.add('join Tablancamento PAI on TRRC.lancamento=PAI.codigo');
          sql.add('join TabLancamento FILHO on Pai.codigo=Filho.lancamentopai');
          sql.add('join tabTipolancto on filho.tipolancto=tabtipolancto.codigo');
          sql.add('where TabTipolancto.Classificacao=''Q'' ');
          sql.add('and TRRC.loteretorno='+pcodigolote);
          open;
          first;
          While not(eof) do
          begin
               Ppendencia.add(fieldbyname('pendencia').asstring);
               PvalorLancamento.add(fieldbyname('valor').asstring);
               next;
          End;
          result:=true;
     End;
end;


function TObjRegistroRetornoCobranca.LerRetornoCNAB400_Bradesco(PnumeroConvenio:string;PathArquivo:String) : boolean;
var
   ACarteira,
   ACodigoBanco,
   ANomeCedente,
   ATipoInscricao,
   ANumeroCPFCGC,
   ACodigoCedente,
   ACodigoAgencia,
   ADigitoCodigoAgencia,
   ANumeroConta,
   ADigitoNumeroConta,
   Aseunumero,AOCorrencia,ANossoNumero: string;
   Adatavencimento:Tdatetime;
   Avalordocumento:Currency;
   NumeroRegistro : integer;
   AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
   ADataOcorrencia,ADataCredito:Tdatetime;
   ADataArquivo:Tdatetime;
   ANumeroArquivo:integer;
   Plote:string;
begin
     result:=False;

Try
     Self.MemoRetorno.clear;

     Try
       Self.Arqretorno.LoadFromFile(PathArquivo);
     Except
           Messagedlg ('Erro na tentativa de Abrir o Arquivo',mterror,[mbok],0);
           exit;
     End;
     NumeroRegistro:=0;//n� da linha

     if Self.Arqretorno.Count <= 0
     then Begin
              Messagedlg('O retorno est� vazio. N�o h� dados para processar',mterror,[mbok],0);
              exit;
     End;

     if (Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,19) <> '02RETORNO01COBRANCA')
     Then Begin
              Messagedlg(PathArquivo+' n�o � um arquivo de retorno de cobran�a',mterror,[mbok],0);
              exit;
     End;


     //L� registro HEADER
     //*******Dados do cedente do t�tulo*****************************
     ATipoInscricao := '';
     ANumeroCPFCGC := '';
     ACodigoCedente := '';
     ACodigoAgencia := '';
     ADigitoCodigoAgencia := '';
     ANumeroConta := '';
     ADigitoNumeroConta := '';
     ANomeCedente := '';

     ACodigoBanco := Copy(Self.ArqRetorno.Strings[NumeroRegistro],77,3);
     if (ACodigoBanco <> '237')
     then Begin
            Messagedlg('Este n�o � um retorno de cobran�a do banco Bradesco',mterror,[mbok],0);
            exit;
     End;
     ANomeCedente := Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],47,30));

     Try
        if StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)) <= 69
         Then ADataArquivo := EncodeDate(StrToInt('20'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)))
         else ADataArquivo := EncodeDate(StrToInt('19'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)));
     Except
           AdataArquivo:=0;
     End;

     ANumeroArquivo := StrToInt(Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,5)));


     Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
     Memoretorno.Add(PathArquivo);
     Memoretorno.Add('');
     Memoretorno.Add('Nome Cedente     : '+ANomeCedente);
     MemoRetorno.Add('Data do Arquivo  : '+datetimetostr(AdataArquivo));
     MemoRetorno.Add('N�mero do Arquivo: '+inttostr(Anumeroarquivo));
    //*****************************************************************
     NumeroRegistro := 1;//proxima linha

     FMostraStringList.caption:='Dados do Arquivo de Retorno';
     FMostraStringList.Memo.Lines.clear;
     FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;
     FMostraStringList.showmodal;

     //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
     //se confirmar gravo um novo Lote
     if (FMostraStringList.tag=0)
     Then exit;
     //Criando um Novo Lote de Retorno de Cobranca

     if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,datetoStr(AdataArquivo),Inttostr(AnumeroArquivo))=True)
     Then Begin
               Self.LoteRetorno.tabelaparaobjeto;
               if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then exit;
     End;

     Self.ZerarTabela;

     //if (Self.LoteRetorno.Convenioboleto.Pegaconvenio(ACodigoBanco)=False)
     //Then exit;

     Self.LoteRetorno.Status:=dsinsert;
     Self.LoteRetorno.Submit_CODIGO(Self.LoteRetorno.Get_NovoCodigo);
     Self.LoteRetorno.Submit_DATAHORA(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');

     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************
     //L� os registros DETALHE
     //Processa at� o pen�ltimo registro porque o �ltimo cont�m apenas o TRAILLER
     for NumeroRegistro:=1 to Self.Arqretorno.Count - 2 do
     begin
            Self.MemoRetorno.Add('----------------------------------');
            {
            {Confirmar se o tipo do registro � 1}
            if Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,1) <> '1'
            then Continue; {N�o processa o registro atual}

            {ATipoInscricao := Copy(Retorno.Strings[NumeroRegistro],2,2);
            if ATipoInscricao = '01'
            then TipoInscricao := tiPessoaFisica
            else
               if ATipoInscricao = '02'
               then TipoInscricao := tiPessoaJuridica
               else TipoInscricao := tiOutro;
            NumeroCPFCGC := Copy(Retorno.Strings[NumeroRegistro],4,14);
            ContaBancaria.Banco.Codigo := ACodigoBanco;
            Nome := ANomeCedente;}

            AOCorrencia :=Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,2);
            //DescricaoOcorrenciaOriginal := VerificaOcorrenciaOriginal(OcorrenciaOriginal);
            case StrToInt(AOCorrencia) of
               2 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Confirmado');
               3 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               6 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               9 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixado');
               12: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Abatimento');
               13: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Abatimento');
               14: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Vencimento');
               17: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               19: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Protestar');
               20: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Sustar Protesto');
               23: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Encaminhado A Cartorio');
               24: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Retirado De Cartorio');
               25: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protestado');
               26: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Comando Recusado');
               27: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Dados');
               28: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Debito Tarifas');
               30: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               36: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Desconto');
               37: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Desconto');
               43: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protesto Ou Sustacao Estornado');
               44: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixa Ou Liquidacao Estornada');
               45: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Dados Alterados');
            else Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Outras Ocorrencias');
            End;
            {





               //Dados que variam de acordo com o banco


               CodigoAgencia := Copy(Retorno.Strings[NumeroRegistro],25,5);
               NumeroConta := Copy(Retorno.Strings[NumeroRegistro],30,7);
               DigitoConta := Copy(Retorno.Strings[NumeroRegistro],37,1);
               ValorDespesaCobranca := StrToFloat(Copy(Retorno.Strings[NumeroRegistro],176,13))/100;

}

           //Nosso n�mero SEM D�GITO

           //ANumeroDocumento:= Copy(Self.ArqRetorno.Strings[NumeroRegistro],111,10);
           //Aseunumero:=Copy(Self.ArqRetorno.Strings[NumeroRegistro],111,10);;
           ANossoNumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],71,11);
           Aseunumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],117,10);
           ACarteira := Copy(Self.ArqRetorno.Strings[NumeroRegistro],22,3);

           // Banco nao retorna a data de vencimento de boletos sem regisotro
           Adatavencimento :=0;

           Try
              {********alterado por celio 20/10/2009****************************
              Vou resgatar o valor pago pelo cliente e enviar como valor do doc

              original ate 20/10/09
              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],153,13))/100;
              }
              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],254,13))/100;
           Except
              AValorDocumento:=0;
           End;

           Try
               if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)) <= 69
               then ADataOcorrencia := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)))
               else ADataOcorrencia := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)));
           Except

           End;

            Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
            Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
            Self.MemoRetorno.Add('Carteira         :'+Acarteira);
            //Self.MemoRetorno.Add('Numero Documento :'+ANumeroDocumento);
            Self.MemoRetorno.Add('Data Ocorr�ncia  :'+datetostr(Adataocorrencia));
            Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));


            AValorMoraJuros := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],267,13))/100;
            AValorDesconto := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],241,13))/100;
            AValorAbatimento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],228,13))/100;
            AValorIOF := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],215,13))/100;
            AValorOutrasDespesas := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],189,13))/100;
            AValorOutrosCreditos := StrToFloat(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],280,13)));

            Try
               ADataCredito:=0;
               if Self.Formatar(Copy(Self.Arqretorno.Strings[NumeroRegistro],296,6),6,false,'0') <> '000000'
               Then Begin
                        if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],300,2)) <= 69
                        then ADataCredito := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],300,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],298,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],296,2)))
                        else ADataCredito := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],300,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],298,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],296,2)));
               End;

            Except
                  ADataCredito:=0;
            End;

            Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
            Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
            Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
            Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
            Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
            Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

            if (ADataOcorrencia<>0)
            Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
            Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

            if (ADataCredito<>0)
            Then Self.MemoRetorno.Add('Data  Cr�dito    :'+datetostr(ADataCredito))
            Else Self.MemoRetorno.Add('Data  Cr�dito    :');


            Self.ZerarTabela;
            Self.Status:=dsinsert;
            Self.LoteRetorno.Submit_CODIGO(Plote);
            Self.Submit_CODIGO            (Self.Get_NovoCodigo);
            Self.Submit_NossoNumero       (ANossoNumero);
            Self.Submit_seunumero         (ASeuNumero);
            Self.Submit_datavencimento    (datetostr(ADataVencimento));
            Self.Submit_valordocumento    (floattostr(AValorDocumento));
            Self.OCorrencia.Submit_codigo (AOCorrencia);
            Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
            Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
            Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
            Self.Submit_ValorIOF          (floattostr(AValorIOF));
            Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
            Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
            Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
            Self.Submit_DataCredito(datetostr(ADataCredito));
            Self.Submit_Processado('N');

            if (Self.Salvar(False)=False)
            then Begin
                      Messagedlg('Erro na tentativa de Salvar o Registro Linha N�'+inttostr(numeroregistro),mterror,[mbok],0);
                      exit;
            End;
     end;//for



     FDataModulo.IBTransaction.CommitRetaining;
     Self.MemoRetorno.Add('----------------------------------');
     Result := TRUE;
     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
     FmostraStringList.ShowModal;
Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

end;


function TObjRegistroRetornoCobranca.LerRetornoCNAB400_Unibanco(PnumeroConvenio:string;PathArquivo:String) : boolean;
var
   ACarteira,
   ACodigoBanco,
   ANomeCedente,
   ATipoInscricao,
   ANumeroCPFCGC,
   ACodigoCedente,
   ACodigoAgencia,
   ADigitoCodigoAgencia,
   ANumeroConta,
   ADigitoNumeroConta,
   Aseunumero,AOCorrencia,ANossoNumero: string;
   Adatavencimento:Tdatetime;
   Avalordocumento:Currency;
   NumeroRegistro : integer;
   AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
   ADataOcorrencia,ADataCredito:Tdatetime;
   ADataArquivo:Tdatetime;
   ANumeroArquivo:integer;
   Plote:string;
begin
     result:=False;

Try     
     Self.MemoRetorno.clear;

     Try
       Self.Arqretorno.LoadFromFile(PathArquivo);
     Except
           Messagedlg ('Erro na tentativa de Abrir o Arquivo',mterror,[mbok],0);
           exit;
     End;
     NumeroRegistro:=0;//n� da linha

     if Self.Arqretorno.Count <= 0
     then Begin
              Messagedlg('O retorno est� vazio. N�o h� dados para processar',mterror,[mbok],0);
              exit;
     End;
     

     if (Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,19) <> '02RETORNO01COBRANCA')
     Then Begin
              Messagedlg(PathArquivo+' n�o � um arquivo de retorno de cobran�a',mterror,[mbok],0);
              exit;
     End;


     //L� registro HEADER
     //*******Dados do cedente do t�tulo*****************************
     ATipoInscricao := '';
     ANumeroCPFCGC := '';
     ACodigoCedente := '';
     ACodigoAgencia := '';
     ADigitoCodigoAgencia := '';
     ANumeroConta := '';
     ADigitoNumeroConta := '';
     ANomeCedente := '';

     ACodigoBanco := Copy(Self.ArqRetorno.Strings[NumeroRegistro],77,3);
     if ACodigoBanco <> '409'
     then Begin
            Messagedlg('Este n�o � um retorno de cobran�a do banco Unibanco',mterror,[mbok],0);
            exit;
     End;
     ANomeCedente := Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],47,30));

     Try
        if StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)) <= 69
         Then ADataArquivo := EncodeDate(StrToInt('20'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)))
         else ADataArquivo := EncodeDate(StrToInt('19'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)));
     Except
           AdataArquivo:=0;
     End;

     ANumeroArquivo := StrToInt(Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,5)));


     Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
     Memoretorno.Add(PathArquivo);
     Memoretorno.Add('');
     Memoretorno.Add('Nome Cedente     : '+ANomeCedente);
     MemoRetorno.Add('Data do Arquivo  : '+datetimetostr(AdataArquivo));
     MemoRetorno.Add('N�mero do Arquivo: '+inttostr(Anumeroarquivo));
    //*****************************************************************
     NumeroRegistro := 1;//proxima linha

     FMostraStringList.caption:='Dados do Arquivo de Retorno';
     FMostraStringList.Memo.Lines.clear;
     FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;
     FMostraStringList.showmodal;

     //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
     //se confirmar gravo um novo Lote
     if (FMostraStringList.tag=0)
     Then exit;
     //Criando um Novo Lote de Retorno de Cobranca

     if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,datetoStr(AdataArquivo),Inttostr(AnumeroArquivo))=True)
     Then Begin
               Self.LoteRetorno.tabelaparaobjeto;
               if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then exit;
     End;

     Self.ZerarTabela;

     //if (Self.LoteRetorno.Convenioboleto.Pegaconvenio(ACodigoBanco)=False)
     //Then exit;

     Self.LoteRetorno.Status:=dsinsert;
     Self.LoteRetorno.Submit_CODIGO(Self.LoteRetorno.Get_NovoCodigo);
     Self.LoteRetorno.Submit_DATAHORA(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');

     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************
     //L� os registros DETALHE
     //Processa at� o pen�ltimo registro porque o �ltimo cont�m apenas o TRAILLER
     for NumeroRegistro:=1 to Self.Arqretorno.Count - 2 do
     begin
            Self.MemoRetorno.Add('----------------------------------');
            {
            {Confirmar se o tipo do registro � 1}
            if Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,1) <> '1'
            then Continue; {N�o processa o registro atual}
            
            {ATipoInscricao := Copy(Retorno.Strings[NumeroRegistro],2,2);
            if ATipoInscricao = '01'
            then TipoInscricao := tiPessoaFisica
            else
               if ATipoInscricao = '02'
               then TipoInscricao := tiPessoaJuridica
               else TipoInscricao := tiOutro;
            NumeroCPFCGC := Copy(Retorno.Strings[NumeroRegistro],4,14);
            ContaBancaria.Banco.Codigo := ACodigoBanco;
            Nome := ANomeCedente;}

            AOCorrencia :=Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,2);
            //DescricaoOcorrenciaOriginal := VerificaOcorrenciaOriginal(OcorrenciaOriginal);
            case StrToInt(AOCorrencia) of
               2 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Confirmado');
               3 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               6 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               9 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixado');
               12: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Abatimento');
               13: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Abatimento');
               14: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Vencimento');
               17: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               19: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Protestar');
               20: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Sustar Protesto');
               23: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Encaminhado A Cartorio');
               24: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Retirado De Cartorio');
               25: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protestado');
               26: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Comando Recusado');
               27: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Dados');
               28: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Debito Tarifas');
               30: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               36: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Desconto');
               37: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Desconto');
               43: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protesto Ou Sustacao Estornado');
               44: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixa Ou Liquidacao Estornada');
               45: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Dados Alterados');
            else Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Outras Ocorrencias');
            End;

           //Nosso n�mero SEM D�GITO

           ANossoNumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],71,11);
           Aseunumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],117,10);
           ACarteira := Copy(Self.ArqRetorno.Strings[NumeroRegistro],22,3);

           // Banco nao retorna a data de vencimento de boletos sem regisotro
           Adatavencimento :=0;

           Try
              {********alterado por celio 20/10/2009****************************
              Vou resgatar o valor pago pelo cliente e enviar como valor do doc

              original ate 20/10/09
              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],153,13))/100;
              }

              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],254,13))/100;
           Except
              AValorDocumento:=0;
           End;

           Try
               if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)) <= 69
               then ADataOcorrencia := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)))
               else ADataOcorrencia := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)));
           Except

           End;

            Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
            Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
            Self.MemoRetorno.Add('Carteira         :'+Acarteira);
            //Self.MemoRetorno.Add('Numero Documento :'+ANumeroDocumento);
            Self.MemoRetorno.Add('Data Ocorr�ncia  :'+datetostr(Adataocorrencia));
            Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));


            AValorMoraJuros := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],267,13))/100;
            AValorDesconto := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],241,13))/100;
            AValorAbatimento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],228,13))/100;
            AValorIOF := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],215,13))/100;
            AValorOutrasDespesas := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],189,13))/100;
            AValorOutrosCreditos := StrToFloat(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],280,13)));

            Try
               ADataCredito:=0;
               if Self.Formatar(Copy(Self.Arqretorno.Strings[NumeroRegistro],296,6),6,false,'0') <> '000000'
               Then Begin
                        if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],300,2)) <= 69
                        then ADataCredito := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],300,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],298,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],296,2)))
                        else ADataCredito := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],300,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],298,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],296,2)));
               End;

            Except
                  ADataCredito:=0;
            End;

            Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
            Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
            Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
            Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
            Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
            Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

            if (ADataOcorrencia<>0)
            Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
            Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

            if (ADataCredito<>0)
            Then Self.MemoRetorno.Add('Data  Cr�dito    :'+datetostr(ADataCredito))
            Else Self.MemoRetorno.Add('Data  Cr�dito    :');


            Self.ZerarTabela;
            Self.Status:=dsinsert;
            Self.LoteRetorno.Submit_CODIGO(Plote);
            Self.Submit_CODIGO            (Self.Get_NovoCodigo);
            Self.Submit_NossoNumero       (ANossoNumero);
            Self.Submit_seunumero         (ASeuNumero);
            Self.Submit_datavencimento    (datetostr(ADataVencimento));
            Self.Submit_valordocumento    (floattostr(AValorDocumento));
            Self.OCorrencia.Submit_codigo (AOCorrencia);
            Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
            Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
            Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
            Self.Submit_ValorIOF          (floattostr(AValorIOF));
            Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
            Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
            Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
            Self.Submit_DataCredito(datetostr(ADataCredito));
            Self.Submit_Processado('N');

            if (Self.Salvar(False)=False)
            then Begin
                      Messagedlg('Erro na tentativa de Salvar o Registro Linha N�'+inttostr(numeroregistro),mterror,[mbok],0);
                      exit;
            End;
     end;//for


     
     FDataModulo.IBTransaction.CommitRetaining;
     Self.MemoRetorno.Add('----------------------------------');
     Result := TRUE;
     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
     FmostraStringList.ShowModal;
Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

end;

function TObjRegistroRetornoCobranca.LerRetornoCNAB400_Sicred(PnumeroConvenio:string;PathArquivo:String) : boolean;
var
   ACarteira,
   ACodigoBanco,
   ANomeCedente,
   ATipoInscricao,
   ANumeroCPFCGC,
   ACodigoCedente,
   ACodigoAgencia,
   ADigitoCodigoAgencia,
   ANumeroConta,
   ADigitoNumeroConta,
   Aseunumero,AOCorrencia,ANossoNumero: string;
   Adatavencimento:Tdatetime;
   Avalordocumento:Currency;
   NumeroRegistro : integer;
   AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
   ADataOcorrencia,ADataCredito:Tdatetime;
   ADataArquivo:Tdatetime;
   ANumeroArquivo:integer;
   Plote:string;
begin
     result:=False;

Try
     Self.MemoRetorno.clear;

     Try
       Self.Arqretorno.LoadFromFile(PathArquivo);
     Except
           Messagedlg ('Erro na tentativa de Abrir o Arquivo',mterror,[mbok],0);
           exit;
     End;
     NumeroRegistro:=0;//n� da linha

     if Self.Arqretorno.Count <= 0
     then Begin
              Messagedlg('O retorno est� vazio. N�o h� dados para processar',mterror,[mbok],0);
              exit;
     End;


     if (Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,19) <> '02RETORNO01COBRANCA')
     Then Begin
              Messagedlg(PathArquivo+' n�o � um arquivo de retorno de cobran�a',mterror,[mbok],0);
              exit;
     End;


     //L� registro HEADER
     //*******Dados do cedente do t�tulo*****************************
     ATipoInscricao := '';
     ANumeroCPFCGC := '';
     ACodigoCedente := '';
     ACodigoAgencia := '';
     ADigitoCodigoAgencia := '';
     ANumeroConta := '';
     ADigitoNumeroConta := '';
     ANomeCedente := '';

     ACodigoBanco := Copy(Self.ArqRetorno.Strings[NumeroRegistro],77,3);
     if ACodigoBanco <> '748'
     then Begin
            Messagedlg('Este n�o � um retorno de cobran�a do banco Sicred',mterror,[mbok],0);
            exit;
     End;
     ANomeCedente := Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],47,30));

     Try
        if StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)) <= 69
         Then ADataArquivo := EncodeDate(StrToInt('20'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)))
         else ADataArquivo := EncodeDate(StrToInt('19'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)));
     Except
           AdataArquivo:=0;
     End;

     ANumeroArquivo := StrToInt(Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,5)));


     Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
     Memoretorno.Add(PathArquivo);
     Memoretorno.Add('');
     Memoretorno.Add('Nome Cedente     : '+ANomeCedente);
     MemoRetorno.Add('Data do Arquivo  : '+datetimetostr(AdataArquivo));
     MemoRetorno.Add('N�mero do Arquivo: '+inttostr(Anumeroarquivo));
    //*****************************************************************
     NumeroRegistro := 1;//proxima linha

     FMostraStringList.caption:='Dados do Arquivo de Retorno';
     FMostraStringList.Memo.Lines.clear;
     FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;
     FMostraStringList.showmodal;

     //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
     //se confirmar gravo um novo Lote
     if (FMostraStringList.tag=0)
     Then exit;
     //Criando um Novo Lote de Retorno de Cobranca

     if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,datetoStr(AdataArquivo),Inttostr(AnumeroArquivo))=True)
     Then Begin
               Self.LoteRetorno.tabelaparaobjeto;
               if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then exit;
     End;

     Self.ZerarTabela;

     //if (Self.LoteRetorno.Convenioboleto.Pegaconvenio(ACodigoBanco)=False)
     //Then exit;

     Self.LoteRetorno.Status:=dsinsert;
     Self.LoteRetorno.Submit_CODIGO(Self.LoteRetorno.Get_NovoCodigo);
     Self.LoteRetorno.Submit_DATAHORA(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');

     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************
     //L� os registros DETALHE
     //Processa at� o pen�ltimo registro porque o �ltimo cont�m apenas o TRAILLER
     for NumeroRegistro:=1 to Self.Arqretorno.Count - 2 do
     begin
            Self.MemoRetorno.Add('----------------------------------');
            {
            {Confirmar se o tipo do registro � 1}
            if Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,1) <> '1'
            then Continue; {N�o processa o registro atual}

            {ATipoInscricao := Copy(Retorno.Strings[NumeroRegistro],2,2);
            if ATipoInscricao = '01'
            then TipoInscricao := tiPessoaFisica
            else
               if ATipoInscricao = '02'
               then TipoInscricao := tiPessoaJuridica
               else TipoInscricao := tiOutro;
            NumeroCPFCGC := Copy(Retorno.Strings[NumeroRegistro],4,14);
            ContaBancaria.Banco.Codigo := ACodigoBanco;
            Nome := ANomeCedente;}

            AOCorrencia :=Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,2);
            //DescricaoOcorrenciaOriginal := VerificaOcorrenciaOriginal(OcorrenciaOriginal);
            case StrToInt(AOCorrencia) of
               2 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Confirmado');
               3 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               6 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               9 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixado');
               12: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Abatimento');
               13: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Abatimento');
               14: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Vencimento');
               17: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               19: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Protestar');
               20: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Sustar Protesto');
               23: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Encaminhado A Cartorio');
               24: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Retirado De Cartorio');
               25: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protestado');
               26: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Comando Recusado');
               27: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Dados');
               28: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Debito Tarifas');
               30: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               36: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Desconto');
               37: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Desconto');
               43: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protesto Ou Sustacao Estornado');
               44: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixa Ou Liquidacao Estornada');
               45: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Dados Alterados');
            else Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Outras Ocorrencias');
            End;

           //Nosso n�mero SEM D�GITO

           ANossoNumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],48,15);
           Aseunumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],117,10);
           ACarteira := '';

           // Banco nao retorna a data de vencimento de boletos sem regisotro
           Adatavencimento :=0;

           Try
              {********alterado por celio 20/10/2009****************************
              Vou resgatar o valor pago pelo cliente e enviar como valor do doc

              original ate 20/10/09
              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],153,13))/100;
              }

              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],254,13))/100;
           Except
              AValorDocumento:=0;
           End;

           Try
               if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)) <= 69
               then ADataOcorrencia := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)))
               else ADataOcorrencia := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)));
           Except

           End;

            Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
            Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
            Self.MemoRetorno.Add('Carteira         :'+Acarteira);
            //Self.MemoRetorno.Add('Numero Documento :'+ANumeroDocumento);
            Self.MemoRetorno.Add('Data Ocorr�ncia  :'+datetostr(Adataocorrencia));
            Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));


            AValorMoraJuros := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],267,13))/100;
            AValorDesconto := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],241,13))/100;
            AValorAbatimento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],228,13))/100;
            AValorIOF := 0;
            AValorOutrasDespesas := StrToFloat(CompletaPalavra_a_Esquerda(trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],189,13)),12,'0'))/100;
            AValorOutrosCreditos := StrToFloat(CompletaPalavra_a_Esquerda(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],280,13)),12,'0'))/100;

            Try
               ADataCredito:=0;
               ADataCredito := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],329,4)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],333,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],335,2)))

            Except
                  ADataCredito:=0;
            End;

            Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
            Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
            Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
            Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
            Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
            Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

            if (ADataOcorrencia<>0)
            Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
            Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

            if (ADataCredito<>0)
            Then Self.MemoRetorno.Add('Data  Cr�dito    :'+datetostr(ADataCredito))
            Else Self.MemoRetorno.Add('Data  Cr�dito    :');


            Self.ZerarTabela;
            Self.Status:=dsinsert;
            Self.LoteRetorno.Submit_CODIGO(Plote);
            Self.Submit_CODIGO            (Self.Get_NovoCodigo);
            Self.Submit_NossoNumero       (ANossoNumero);
            Self.Submit_seunumero         (ASeuNumero);
            Self.Submit_datavencimento    (datetostr(ADataVencimento));
            Self.Submit_valordocumento    (floattostr(AValorDocumento));
            Self.OCorrencia.Submit_codigo (AOCorrencia);
            Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
            Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
            Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
            Self.Submit_ValorIOF          (floattostr(AValorIOF));
            Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
            Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
            Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
            Self.Submit_DataCredito(datetostr(ADataCredito));
            Self.Submit_Processado('N');

            if (Self.Salvar(False)=False)
            then Begin
                      Messagedlg('Erro na tentativa de Salvar o Registro Linha N�'+inttostr(numeroregistro),mterror,[mbok],0);
                      exit;
            End;
     end;//for



     FDataModulo.IBTransaction.CommitRetaining;
     Self.MemoRetorno.Add('----------------------------------');
     Result := TRUE;
     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
     FmostraStringList.ShowModal;
Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

end;

function TObjRegistroRetornoCobranca.Formatar(Texto: string; TamanhoDesejado: integer; AcrescentarADireita: boolean = true; CaracterAcrescentar: char = ' '): string;
{
   OBJETIVO: Eliminar caracteres inv�lidos e acrescentar caracteres � esquerda ou � direita do texto original para que a string resultante fique com o tamanho desejado

   Texto : Texto original
   TamanhoDesejado: Tamanho que a string resultante dever� ter
   AcrescentarADireita: Indica se o car�cter ser� acrescentado � direita ou � esquerda
      TRUE - Se o tamanho do texto for MENOR que o desejado, acrescentar car�cter � direita
             Se o tamanho do texto for MAIOR que o desejado, eliminar �ltimos caracteres do texto
      FALSE - Se o tamanho do texto for MENOR que o desejado, acrescentar car�cter � esquerda
             Se o tamanho do texto for MAIOR que o desejado, eliminar primeiros caracteres do texto
   CaracterAcrescentar: Car�cter que dever� ser acrescentado
}
var
  QuantidadeAcrescentar,
    TamanhoTexto,
    PosicaoInicial,
    i: integer;

begin
  case CaracterAcrescentar of
    '0'..'9', 'a'..'z', 'A'..'Z': ; {N�o faz nada}
  else
    CaracterAcrescentar := ' ';
  end;

  Texto := Trim(AnsiUpperCase(Texto));
  TamanhoTexto := Length(Texto);
  for i := 1 to (TamanhoTexto) do
  begin
    if Pos(Texto[i], ' 0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`~''"!@#$%^&*()_-+=|/\{}[]:;,.<>') = 0 then
    begin
      case Texto[i] of
        '�', '�', '�', '�', '�': Texto[i] := 'A';
        '�', '�', '�', '�': Texto[i] := 'E';
        '�', '�', '�', '�': Texto[i] := 'I';
        '�', '�', '�', '�', '�': Texto[i] := 'O';
        '�', '�', '�', '�': Texto[i] := 'U';
        '�': Texto[i] := 'C';
        '�': Texto[i] := 'N';
      else
        Texto[i] := ' ';
      end;
    end;
  end;

  QuantidadeAcrescentar := TamanhoDesejado - TamanhoTexto;
  if QuantidadeAcrescentar < 0
  then QuantidadeAcrescentar := 0;
  if CaracterAcrescentar = '' then
    CaracterAcrescentar := ' ';
  if TamanhoTexto >= TamanhoDesejado then
    PosicaoInicial := TamanhoTexto - TamanhoDesejado + 1
  else
    PosicaoInicial := 1;

  if AcrescentarADireita
  then Texto := Copy(Texto, 1, TamanhoDesejado) + StringOfChar(CaracterAcrescentar, QuantidadeAcrescentar)
  else Texto := StringOfChar(CaracterAcrescentar, QuantidadeAcrescentar) + Copy(Texto, PosicaoInicial, TamanhoDesejado);

  Result := AnsiUpperCase(Texto);
end;



procedure TObjREGISTRORETORNOCOBRANCA.Re_imprimeboleto(Pcodigo:string);
var
PBoletos:TStringList;
cont:integer;
pboletoinicial,pboletofinal:string;
ppreview:boolean;
modelo:integer;
begin
     Try
        PBoletos:=TStringList.create;
        PBoletos.clear;
     Except
           MensagemErro('Erro na tentativa de criar a StringList Pboletos');
           exit;
     End;

Try

     if (Pcodigo='')
     then Begin
               With FfiltroImp do
               begin
                    DesativaGrupos;
                    Grupo01.Enabled:=True;
                    Grupo02.Enabled:=True;
                    LbGrupo01.caption:='Boleto Inicial';
                    LbGrupo02.caption:='Boleto Final';
                    edtgrupo01.OnKeyDown:=Self.EdtBoletoKeyDown;
                    edtgrupo02.OnKeyDown:=Self.EdtBoletoKeyDown;
          
                    Showmodal;
          
                    if (tag=0)
                    then exit;
          
                    Pboletoinicial:=edtgrupo01.text;
                    Pboletofinal:=edtgrupo02.text;
          
               End;
     End
     Else Begin
                Pboletoinicial:=pcodigo;
                Pboletofinal:=pcodigo;
     End;


     With FOpcaorel do
     begin
          RgOpcoes.items.clear;
          RgOpcoes.items.add('Visualizar');
          RgOpcoes.items.add('Imprimir Direto');
          Showmodal;

          if (rgopcoes.ItemIndex=0)
          Then ppreview:=true
          Else ppreview:=False;
     End;


     With FOpcaorel do
     begin
          RgOpcoes.items.clear;
          RgOpcoes.items.add('Modelo 1');
          RgOpcoes.items.add('Modelo 2');
          Showmodal;

          if (rgopcoes.ItemIndex=0)
          Then modelo:=1
          Else modelo:=2;
     End;

     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo from tabboletobancario where codigo>='+pboletoinicial+'  and codigo<='+pboletofinal);
          open;
          while not(eof) do
          Begin
                if (Self.Boleto.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
                then Begin
                          Messagedlg('Boleto '+fieldbyname('codigo').asstring+' n�o encontrado',mterror,[mbok],0);
                          exit;
                End;
                Self.Boleto.TabelaparaObjeto;
           
                //****localizando uma pendencia para este boleto*******
                if (Self.Lancamento.Pendencia.LocalizaporBoleto(fieldbyname('codigo').asstring)=False)
                Then begin
                          Messagedlg('Nenhuma pend�ncia est� ligada a esta boleto',mterror,[mbok],0);
                          exit;
                End;
                Self.Lancamento.Pendencia.TabelaparaObjeto;
                //****reimprimindo o boleto***********
                Self.Lancamento.Pendencia.Re_imprimeboleto(Self.Lancamento.Pendencia.get_codigo,ppreview,MODELO);
                next;
          End;
     End;

Finally
       freeandnil(Pboletos);
End;


end;

procedure TObjREGISTRORETORNOCOBRANCA.RastreioBoletos;
var
Popcao:integer;
pdata1,pdata2:string;
Pconvenio:string;
PcodigoBoleto:string;
psomasaldo,psomaboleto:currency;
begin
     With FOpcaorel do
     begin
           RgOpcoes.items.clear;
           RgOpcoes.items.Add('Todos');
           RgOpcoes.items.Add('Pend�ncias Sem Saldo');
           RgOpcoes.items.Add('Pend�ncias Com Saldo');
           Showmodal;
           if (tag=0)
           Then exit;
           popcao:=RgOpcoes.ItemIndex;
     End;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          Grupo03.Enabled:=True;
          LbGrupo01.caption:='Vencimento Inicial';
          edtgrupo01.EditMask:=MascaraData;
          LbGrupo02.caption:='Vencimento Final';
          edtgrupo02.EditMask:=MascaraData;
          LbGrupo03.caption:='Conv�nio';
          edtgrupo03.OnKeyDown:=Self.Boleto.edtconvenioboletoKeyDown;

          Showmodal;

          if (tag=0)
          Then exit;

          pdata1:='';
          pdata2:='';
          Pconvenio:='';

          Try
             if (trim(comebarra(edtgrupo01.text))<>'')
             Then Begin
                       strtodate(edtgrupo01.Text);
                       Pdata1:=edtgrupo01.Text;
             End;
          Except
                Messagedlg('Vencimento inicial inv�lido',mterror,[mbok],0);
                exit;
          End;

          Try
             if (trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                       strtodate(edtgrupo02.Text);
                       Pdata2:=edtgrupo02.Text;
             End;
          Except
                Messagedlg('Vencimento final inv�lido',mterror,[mbok],0);
                exit;
          End;

          Try
              if (edtgrupo03.text<>'')
              Then Begin
                        strtoint(edtgrupo03.Text);

                        if (Self.Boleto.Convenioboleto.LocalizaCodigo(edtgrupo03.Text)=false)
                        Then Begin
                                  Messagedlg('Conv�nio n�o encontrado',mterror,[mbok],0);
                                  exit;
                        end;
                        Pconvenio:=edtgrupo03.text;
              End;
          Except
                Messagedlg('Conv�nio Inv�lido',mterror,[mbok],0);
                exit;
          End;


     End;


     //esse procedimento vai trazer o boleto
     //mostrar as pendencias que estaum liagads a eles
     //e seus respectivos saldos
     //caso ele tenha sido quitado pelo sistema automatico
     //mostrar o lote que ele esteja ligado

     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select procboleto.*,');
          sql.add('TRRCB.loteretorno,');
          sql.add('TRRCB.datacredito,');
          sql.add('TRRCB.situacao  as SITUACAOREG,');
          sql.add('TRRCB.lancamento,');
          sql.add('TRRCB.valordocumento+');
          sql.add('((TRRCB.ValorJuros+TRRCB.ValorIOF+TRRCB.ValorOutrasDespesas)-');
          sql.add('(TRRCB.ValorDesconto+TRRCB.ValorAbatimento+TRRCB.ValorOutrosCreditos))');
          sql.add('as ValorRecebido');
          sql.add('from procboleto');
          sql.add('left join tabregistroretornocobranca TRRCB on TRRCB.boleto=procboleto.codigo');
          
          //sql.add('left join tabpendencia on tabpendencia.boletobancario=procboleto.codigo');
          //sql.add('left join proctitulos on tabpendencia.titulo=proctitulos.codigo');}
          sql.add('Where Procboleto.Situacao='+#39+'I'+#39);
          //SQL.Add('and TRRCB.processado=''S'' ');

          if (Popcao=1)//sem saldo
          Then Sql.add('and Procboleto.SaldoPendencias=0')
          Else
             if (Popcao=2)//com saldo
             Then Sql.add('and Procboleto.SaldoPendencias>0');

          if (pdata1<>'')
          Then Sql.add('and ProcBoleto.vencimento>='+#39+Formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);

          if (pdata2<>'')
          Then Sql.add('and ProcBoleto.vencimento<='+#39+Formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39);

          if (Pconvenio<>'')
          Then Sql.add('and ProcBoleto.ConvenioBoleto='+Pconvenio);

          sql.add('order by procboleto.codigo');

          //InputBox('','',sql.text);

          open;
          last;
          
          if (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mterror,[mbok],0);
                    exit;
          End;
          FMostraBarraProgresso.Lbmensagem.caption:='PROCESSANDO BOLETOS';
          FMostraBarraProgresso.BarradeProgresso.MaxValue:=recordcount;
          FMostraBarraProgresso.BarradeProgresso.MinValue:=0;
          FMostraBarraProgresso.BarradeProgresso.Progress:=0;
          first;


          With FreltxtRDPRINT do
          Begin
              //***********************************************
              ConfiguraImpressao;
              RDprint.FonteTamanhoPadrao:=S17cpp;
              RDprint.TamanhoQteColunas:=130;
              //***********************************************

              LinhaLocal:=3;
              RDprint.Abrir;
              if (RDprint.Setup=False)
              Then Begin
                        RDprint.Fechar;
                        exit;
              End;

              FMostraBarraProgresso.BarradeProgresso.Progress:=0;
              FMostraBarraProgresso.Show;
              FMostraBarraProgresso.Repaint;

              RDprint.ImpC(LinhaLocal,65,'BOLETOS IMPRESSOS - ANAL�TICO',[negrito]);
              IncrementaLinha(2);
              RDprint.Impf(LinhaLocal,1,CompletaPalavra('C�DIGO',6,' ')+' '+
                                            CompletaPalavra('NUMERO BOLETO',20,' ')+' '+
                                            CompletaPalavra('CONV�NIO',23,' ')+' '+
                                            CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                            CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                            CompletaPalavra('LOTE',4,' ')+' '+
                                            CompletaPalavra('DT.CRED.',10,' ')+' '+
                                            CompletaPalavra('VALOR PAGO',12,' ')+' '+
                                            CompletaPalavra('SIT.LOTE',25,' '),[negrito]);
              IncrementaLinha(1);
              RDprint.Impf(LinhaLocal,8,CompletaPalavra('PEND.',6,' ')+' '+
                                        CompletaPalavra('TITULO',13,' ')+' '+
                                        CompletaPalavra('CLIENTE',23,' ')+' '+
                                        CompletaPalavra('VENCIMENTO',10,' ')+' '+
                                        CompletaPalavra_a_Esquerda('SALDO',12,' '),[italico]);
              IncrementaLinha(1);

              PcodigoBoleto:='';
              PsomaBoleto:=0;
              PsomaSaldo:=0;
              While not(Self.ObjqueryPesquisa.Eof) do
              Begin
                   FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
                   FMostraBarraProgresso.Show;
                   FMostraBarraProgresso.Repaint;

                   IncrementaLinha(1);

                   VerificaLinha;
                   RDprint.Impf(LinhaLocal,1,CompletaPalavra(fieldbyname('codigo').asstring,6,' ')+' '+
                                            CompletaPalavra(fieldbyname('numeroboleto').asstring,20,' ')+' '+
                                            CompletaPalavra(fieldbyname('convenioboleto').asstring+'-'+fieldbyname('nomeconvenio').asstring,23,' ')+' '+
                                            CompletaPalavra(fieldbyname('vencimento').asstring,10,' ')+' '+
                                            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valor').asstring),12,' ')+' '+
                                            CompletaPalavra(fieldbyname('loteretorno').asstring,4,' ')+' '+
                                            CompletaPalavra(fieldbyname('datacredito').asstring,10,' ')+' '+
                                            CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorrecebido').asstring),12,' ')+' '+
                                            CompletaPalavra(fieldbyname('situacaoreg').asstring,25,' '),[negrito]);
                   IncrementaLinha(1);
                   PsomaBoleto:=PsomaBoleto+fieldbyname('valor').asfloat;
                   PsomaSaldo:=PsomaSaldo+fieldbyname('saldopendencias').asfloat;
                   Pcodigoboleto:=Fieldbyname('codigo').asstring;
                   //*****PEND�NCIAS DESSE BOLETO*******************************
                   Self.Objquery.close;
                   Self.Objquery.sql.clear;
                   Self.Objquery.sql.add('Select Codigo From Tabpendencia where boletobancario='+pcodigoboleto);
                   Self.Objquery.open;
                   Self.Objquery.last;
                   if (Self.Objquery.recordcount>0)
                   Then Begin
                             Self.Objquery.first;
                             While not(Self.Objquery.eof) do
                             Begin
                                 Self.Lancamento.Pendencia.LocalizaCodigo(Self.Objquery.fieldbyname('codigo').asstring);
                                 Self.Lancamento.Pendencia.TabelaparaObjeto;
                                 
                                 VerificaLinha;
                                 RDprint.Impf(LinhaLocal,8,CompletaPalavra(Self.lancamento.pendencia.get_codigo,6,' ')+' '+
                                                     CompletaPalavra(Self.lancamento.pendencia.titulo.get_codigo,13,' ')+' '+
                                                     CompletaPalavra(Self.lancamento.pendencia.titulo.Get_CODIGOCREDORDEVEDOR+'-'+Self.lancamento.pendencia.Titulo.CREDORDEVEDOR.Get_RazaoSocialCredorDevedor(Self.lancamento.pendencia.titulo.CREDORDEVEDOR.Get_CODIGO,Self.lancamento.pendencia.TITULO.Get_CODIGOCREDORDEVEDOR),23,' ')+' '+
                                                     CompletaPalavra(Self.lancamento.pendencia.get_vencimento,10,' ')+' '+
                                                     CompletaPalavra_a_Esquerda(formata_valor(Self.lancamento.pendencia.get_saldo),12,' '),[italico]);
                                 IncrementaLinha(1);
                                 Self.Objquery.next;
                             End;
                             Self.Objquery.close;
                             //Imprimindo a Soma do Saldo das Pend�ncias
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,64,CompletaPalavra_a_Esquerda(formata_valor(FIELDBYNAME('SALDOPENDENCIAS').ASSTRING),12,' '),[negrito,italico]);
                             IncrementaLinha(1);
                   End;
                   //**********************************************************
                   Self.ObjqueryPesquisa.next;
              end;
              FMostraBarraProgresso.close;
              
              VerificaLinha;
              RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',90,'_'));
              IncrementaLinha(1);
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,completapalavra('SOMA VALOR DOS    BOLETOS',63,' ')+CompletaPalavra_a_Esquerda(Formata_valor(psomaboleto),12,' '),[NEGRITO]);
              IncrementaLinha(1);
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,completapalavra('SOMA SALDO DAS PEND�NCIAS',63,' ')+CompletaPalavra_a_Esquerda(Formata_valor(psomaSALDO),12,' '),[negrito]);
              RDprint.Fechar;
          End;
     end;
end;

function TObjREGISTRORETORNOCOBRANCA.ExcluiLote(Plote: string): boolean;
begin
     Result:=False;

     if (Self.LoteRetorno.LocalizaCodigo(plote)=False)
     Then Begin
               mensagemaviso('Lote '+plote+' n�o encontrado para ser exclu�do');
               exit;
     End;
     Self.LoteRetorno.TabelaparaObjeto;

     if (Self.LoteRetorno.Get_Processado='S')
     then Begin
               mensagemerro('Esse lote j� foi reprocessado, estorne o reprocessamento antes de tentar excluir');
               exit; 
     End;

Try

     With Self.Objquery do
     Begin
          //primeiro excluindo os registros do lote para depois excluir o lote
          close;
          sql.clear;
          sql.add('Delete from TabRegistroRetornoCobranca where loteRetorno='+plote);
          Try
             execsql;
          Except
             on E:Exception do
             begin
                  mensagemerro('Erro na tentativa de Excluir os Registros do Lote '+#13+'Erro:'+E.message);
                  exit;
             End;
          End;

          if (Self.LoteRetorno.Exclui(plote,True)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Excluir o Lote');
                    exit;
          End;
          FDataModulo.IBTransaction.CommitRetaining;
          result:=True;
     End;

Finally
       Fdatamodulo.ibtransaction.rollbackretaining;
End;

end;

procedure TObjREGISTRORETORNOCOBRANCA.ImprimeEnderecos;
var
Pboletoinicial,pboletofinal:string;
ObjPendencia:TObjPendencia;
objempresa:TObjEMPRESA;
cont:integer;
begin
     With FfiltroImp do
     begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo01.caption:='Boleto Inicial';
          LbGrupo02.caption:='Boleto Final';
          edtgrupo01.OnKeyDown:=Self.EdtBoletoKeyDown;
          edtgrupo02.OnKeyDown:=Self.EdtBoletoKeyDown;

          Showmodal;

          if (tag=0)
          then exit;

          Pboletoinicial:=edtgrupo01.text;
          Pboletofinal:=edtgrupo02.text;

     End;

     Try
        ObjPendencia:=TObjPendencia.create;
        Objempresa:=Tobjempresa.create;
     Except
        mensagemerro('Erro na tentativa de criar o objeto de Pendencia');
        exit;
     End;

Try

     With Self.ObjqueryPesquisa do
     begin
          close;
          sql.clear;
          sql.add('Select tabboletobancario.codigo from Tabboletobancario ');
          sql.add('where Tabboletobancario.codigo>='+Pboletoinicial+' and Tabboletobancario.codigo<='+pboletofinal);
          sql.add('and tabboletobancario.situacao=''I'' ');
          sql.add('order by Tabboletobancario.codigo');
          open;
          last;
          if (recordcount=0)
          Then begin
                    mensagemaviso('Nenhum boleto foi selecionado');
                    exit;
          End;
          first;


          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.ImprimeCabecalho:=false;
          FreltxtRDPRINT.RDprint.Abrir;
          IF (FreltxtRDPRINT.RDprint.Setup=False)
          then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
          End;

          objempresa.LocalizaCodigo('1');
          objempresa.TabelaparaObjeto;

          While not(eof) do
          begin
               ObjPendencia.LocalizaporBoleto(fieldbyname('codigo').asstring);
               ObjPendencia.TabelaparaObjeto;

               FreltxtRDPRINT.RDprint.Imp(30,5,'Destinatario: '+ObjPendencia.Titulo.credordevedor.Get_NomeCredorDevedor(ObjPendencia.Titulo.credordevedor.Get_CODIGO,ObjPendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
               FreltxtRDPRINT.RDprint.Imp(31,5,'Endereco    : '+ObjPendencia.Titulo.credordevedor.Get_EnderecoCredorDevedor(ObjPendencia.Titulo.credordevedor.Get_CODIGO,ObjPendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
               FreltxtRDPRINT.RDprint.Imp(32,5,'Bairro      : '+ObjPendencia.Titulo.credordevedor.Get_BairroCredorDevedor(ObjPendencia.Titulo.credordevedor.Get_CODIGO,ObjPendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
               FreltxtRDPRINT.RDprint.Imp(33,5,'CEP         : '+ObjPendencia.Titulo.credordevedor.Get_CepCredorDevedor(ObjPendencia.Titulo.credordevedor.Get_CODIGO,ObjPendencia.Titulo.Get_CODIGOCREDORDEVEDOR));
               FreltxtRDPRINT.RDprint.Imp(34,5,'Cidade      : '+ObjPendencia.Titulo.credordevedor.Get_CidadeCredorDevedor(ObjPendencia.Titulo.credordevedor.Get_CODIGO,ObjPendencia.Titulo.Get_CODIGOCREDORDEVEDOR)+' \ '+ObjPendencia.Titulo.credordevedor.Get_BairroCredorDevedor(ObjPendencia.Titulo.credordevedor.get_codigo,ObjPendencia.Titulo.Get_CODIGOCREDORDEVEDOR));

               FreltxtRDPRINT.RDprint.Imp(60,5,'Remetente   : '+objempresa.Get_RAZAOSOCIAL);
               FreltxtRDPRINT.RDprint.Imp(61,5,'Endereco    : '+objempresa.Get_ENDERECO);

               FreltxtRDPRINT.RDprint.Imp(62,5,'Bairro      : '+Objempresa.get_bairro);
               FreltxtRDPRINT.RDprint.Imp(63,5,'CEP         : '+objempresa.get_cep);
               FreltxtRDPRINT.RDprint.Imp(64,5,'Cidade      : '+objempresa.get_cidade+' \ '+objempresa.Get_ESTADO);
               FreltxtRDPRINT.RDprint.Novapagina;

               next;
          End;
          //FreltxtRDPRINT.RDprint.gerarHTM('c:\teste.htm');
          FreltxtRDPRINT.RDprint.fechar;
     End;
Finally
       ObjPendencia.free;
       Objempresa.free;
End;

end;

procedure TObjREGISTRORETORNOCOBRANCA.ReprocessaTodosLotes;
var
  ObjQueryTEMP:tibquery;
begin
      Try
            ObjQueryTEMP:=TIBQuery.create(nil);
            ObjQueryTEMP.Database:=FDataModulo.IbDatabase;

      Except
            MensagemErro('Erro na Tentativa de Criar Query Tempor�ria');
            Exit;
      End;
      //resgato todos os lotes
      //extorno todos os lotes
      //reprocesso todos os lotes
      //salvo
      If(MensagemPergunta('Este procedimento Estornar� TODOS os recebimentos de boletos e Reprocessar� um a um.'+#13+' Deseja Continuar?')=mrno)
      Then Exit;

      Try
            With ObjQueryTEMP do
            Begin
                  //resgatando todos os lotes a serem reprocessados
                  Close;
                  Sql.Clear;
                  SQL.Add('Select codigo from TABLOTERETORNOCOBRANCA where processado=''S'' order by codigo');
                  open;
                  Last;

                  FMostraBarraProgresso.ConfiguracoesIniciais(recordcount*2,0);
                  FMostraBarraProgresso.Lbmensagem.caption:='Reprocessando Lotes de Retorno';
                  FMostraBarraProgresso.btcancelar.Visible:=True;

                  If(ObjQueryTEMP.RecordCount=0)
                  Then Begin
                        MensagemErro('N�o existem lotes a serem reprocessados!');
                        Exit;
                  End;
                  First;
                  //extornando todos os lotes
                  While not(eof) do
                  Begin
                        FMostraBarraProgresso.IncrementaBarra1(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        If (FMostraBarraProgresso.Cancelar=True)
                        Then Begin
                                  FMostraBarraProgresso.close;
                                  close;
                                  exit;
                        End;
                        Self.ExtornaReprocessamento(fieldbyname('codigo').asstring,false);
                        {Then Begin
                              MensagemErro('Erro ao estornar lote:'+Plote+' Desfazendo altera��es...');
                              FDataModulo.IBTransaction.RollbackRetaining;
                              Exit;
                        End;}
                        Next;
                  End;
                  //reprocessando todos os lotes
                  First;
                  While not(eof) do
                  Begin
                        FMostraBarraProgresso.IncrementaBarra1(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        If (FMostraBarraProgresso.Cancelar=True)
                        Then Begin
                                  FMostraBarraProgresso.close;
                                  close;
                                  exit;
                        End;
                        If(Self.ReprocessaLote(fieldbyname('codigo').asstring,false)=False)
                        Then Begin
                              MensagemErro('Erro ao reprocessar lote:'+fieldbyname('codigo').asstring+' Desfazendo altera��es...');
                              FDataModulo.IBTransaction.RollbackRetaining;
                              Exit;
                        End;
                        Next;
                  End;
            End;
            FMostraBarraProgresso.btcancelar.Visible:=false;
            //Se chegou aqui entao posso comimtar
            FDataModulo.IBTransaction.CommitRetaining;
            MensagemAviso('Reprocessamento de Lotes Conclu�do com sucesso!');
      Except
            on E:Exception do
            Begin
                messagedlg('N�o foi poss�vel Reprocessar Todos os lotes'+#13+'Erro: '+E.message,mterror,[mbok],0);
                FDataModulo.IBTransaction.RollbackRetaining;
                exit;
            End;
      End;//except geral

      FreeAndNil(ObjQueryTEMP);
      FMostraBarraProgresso.close;
end;

function TObjREGISTRORETORNOCOBRANCA.ReprocessaLote(Plote: string): boolean;
var
PNumeroConvenio,PCampoLivre,PNossoNumero:String;
Pposicao:integer;
pjuros,PValorBoletoSIS,PValorBoletobanco,pdesconto:Currency;
LocalizaPorNUmeroBanco,Achou:boolean;

begin
     result:=False;


     LocalizaPorNUmeroBanco:=False;

     if (ObjParametroGlobal.ValidaParametro('LOCALIZA BOLETO PELO NUMERO DO BANCO E NAO PELO CONVENIO')=true)
     Then begin
               if (ObjParametroGlobal.get_valor='SIM')
               Then LocalizaPorNUmeroBanco:=True;
     End;

     {Usado apos a captura do arquivo, a captura apenas preenche registros
     na tabela Lote e Registros

     Passo para todos para processado, os que eu encontro o boleto coloco o numero do boleto
     e deixo o situacoa em brnaxco para ser processado no momento de quitar as pendencias

     os que nao encontram pendencias coloco como NE(nao encontrado boleto)}

     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.add('PROCESSAMENTO DOS REGISTROS DE RETORNO');
     FmostraStringList.Memo.lines.add(' ');
     FmostraStringList.Memo.lines.add('LOCALIZANDO BOLETOS ...');


     if (plote='')
     Then Begin
               Messagedlg('Escolha um lote',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.LoteRetorno.LocalizaCodigo(plote)=False)
     Then Begin
               Messagedlg('Lote N� '+Plote+' n�o encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.LoteRetorno.TabelaparaObjeto;
     PNumeroConvenio:=Self.LoteRetorno.Convenioboleto.Get_numeroconvenio;


     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          Sql.add('Select * from TabRegistroRetornoCobranca where LoteRetorno='+plote+' and Processado=''N'' ');
          //InputBox('','',SQL.Text);
          open;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('N�o foi encontrado nenhum registro deste lote que n�o tenha sido processado',mterror,[mbok],0);
                    exit;
          End;
          first;

          Self.LoteRetorno.Status:=dsedit;
          Self.LoteRetorno.Submit_Processado('S');
          if (Self.LoteRetorno.Salvar(False)=False)
          Then Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Erro na tentativa de Passar o Lote para Processado=''S'' ',mterror,[mbok],0);
                    exit;
          End;



          While not(eof) do
          Begin
               //pega o nosso numero
               //separa a parte do conv�nio
               //pega apenas o campo livre
               //procura no cadastro de boleto
               //se encontrar guarda o numero do boleto no registroretornocobranca
               //para posteriormente efetuar as quita��es

               Self.ZerarTabela;
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               Self.Processado:='S';
               PnossoNumero:=Fieldbyname('nossonumero').asstring;
               PCampoLivre:=trim(PnossoNumero);

               Pposicao:=pos(PNumeroConvenio,PNossoNumero);
               PnossoNumero:=trim(Pnossonumero);

               if (Pposicao<>0)
               Then Begin//encontrou o numero do convenio
                         PCampoLivre:=Trim(copy(pnossonumero,pposicao+length(pnumeroconvenio),length(pnossonumero)-(pposicao+length(pnumeroconvenio))+1));
               End;
               //O tratamento se deu necessario porque no dia 18/10/07 ao fazer um boleto de teste em tres lagoas
               //o retorno do cobranca envia o numero 0004X  provalmente porque o digito verificador
               //no nosso numero foi 10

               if (PCampoLivre[length(Pcampolivre)]='X')
               Then PcampoLivre:=copy(pcampolivre,1,length(pcampolivre)-1);

               achou:=True;
               if (LocalizaPorNUmeroBanco)
               Then Begin
                         if (Self.Boleto.LocalizaBoletoBanco(PCampolivre,Self.LoteRetorno.Convenioboleto.Get_CodigoBAnco)=False)
                         Then Begin
                                   FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' N�o Encontrado');
                                   Self.Boleto.ZerarTabela;
                                   Self.Situacao:='BOLETO N�O ENCONTRADO';//n�o encontrado
                                   achou:=false;
                         End;
               End
               Else BEgin
                         if (Self.Boleto.LocalizaBoletoConvenio(PCampolivre,Self.LoteRetorno.Convenioboleto.get_codigo)=False)
                         Then Begin
                                   FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' N�o Encontrado');
                                   Self.Boleto.ZerarTabela;
                                   Self.Situacao:='BOLETO N�O ENCONTRADO';//n�o encontrado
                                   achou:=false;
                         End;
               End;

               if (achou)
               Then Begin
                         Self.Boleto.TabelaparaObjeto;
                         try
                            PValorBoletoSis:=0;
                            if (trim(Self.Boleto.get_valordoc)<>'')
                            Then PValorBoletoSIS:=Strtofloat(Self.Boleto.get_valordoc);
                         Except
                            PValorBoletoSis:=0;
                         End;

                         Try
                            PValorBoletobanco:=Strtofloat(Self.Get_valordocumento);
                         Except
                            PValorBoletobanco:=0;
                         End;

                         pjuros:=0;
                         if (PValorBoletoSIS<PValorBoletobanco)
                         Then Begin
                                   //O valor que veio do banco � maior, pode ter sido o juro
                                   //que nao foi adicionado no campo de juros
                                   //e sim no campo de valor
                                   Try
                                     pjuros:=strtocurr(Self.Get_ValorJuros);
                                   Except
                                     pjuros:=0;
                                   End;

                                   Self.Submit_ValorJuros(currtostr(PvalorBoletoBanco-Pvalorboletosis));
                                   Self.Submit_valordocumento(FloatToStr(Pvalorboletosis+(PvalorBoletoBanco-Pvalorboletosis)));
                         End;

                         pdesconto:=0;
                         if (PValorBoletoSIS>PValorBoletobanco)
                         Then Begin

                                {********************110808 alterado por celio
                                Self.Situacao:='DIVERG. DE VALOR ENTRE BOLETO NO SIST. E RETORNO';
                                FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' DIVERG�NCIA DE VALOR ENTRE BOLETO NO SISTEMA E NO RETORNO. Boleto Sistema N� '+Self.Boleto.get_codigo);
                                Self.Boleto.ZerarTabela;}

                                Try
                                      pdesconto:=StrToCurr(self.Get_ValorDesconto);
                                Except
                                      pdesconto:=0;
                                End;

                                Self.Submit_ValorDesconto(currtostr(Pvalorboletosis-PvalorBoletoBanco));
                                Self.Submit_valordocumento(FloatToStr(Pvalorboletosis-(Pvalorboletosis-PvalorBoletoBanco)));
                                FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' Encontrado Boleto N� '+Self.Boleto.get_codigo);
                         End
                         Else FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' Encontrado Boleto N� '+Self.Boleto.get_codigo);
               End;

               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Salvar o Registro N�'+Self.codigo,mterror,[mbok],0);
                         FmostraStringList.Memo.Lines.add('Erro ao tentar atualizar o Registro N�'+Self.codigo+', nosso numero='+Self.NossoNumero);
               End;

               next;
          End;

          if (Self.QuitaPendencias(Plote)=False)
          Then FdataModulo.IBTransaction.RollbackRetaining
          Else FdataModulo.IBTransaction.CommitRetaining;

          FmostraStringList.showmodal;
          result:=true;
     End;
end;

procedure TObjREGISTRORETORNOCOBRANCA.ExtornaReprocessamento(Plote: string);
var
pcodigolancamento:string;
begin
     //esse procedimento ira pegar todos os Registros
     //zerar a situacao e o processado
     //e no caso de registro que possuam LANCAMENTO de quitacao
     //excluir
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo from TabRegistroRetornoCobranca Where LoteRetorno='+Plote);
          open;
          if (recordcount=0)
          Then Begin
                    Messagedlg('N�o existe nenhum registro para este Lote escolhido',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          While not(eof) do
          Begin
               Self.ZerarTabela;
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               Self.Processado:='N';
               Self.Boleto.ZerarTabela;
               Self.Situacao:='';
               PcodigoLancamento:=self.Lancamento.Get_CODIGO;
               self.Lancamento.ZerarTabela;
               if (Self.Salvar(False)=False)
               then begin
                         FDataModulo.IBTransaction.RollbackRetaining;
                         Messagedlg('N�o foi poss�vel salvar as altera��es no registro '+Self.CODIGO,mterror,[mbok],0);
                         exit;
               end;

               if (pcodigolancamento<>'')
               Then begin
                         if (Self.Lancamento.ApagaLancamento(pcodigolancamento,False)=False)
                         then begin
                                   FDataModulo.IBTransaction.RollbackRetaining;
                                   Messagedlg('N�o foi poss�vel excluir o lan�amento do registro '+Self.CODIGO,mterror,[mbok],0);
                                   exit;
                         end;
               End;
               next;
          End;


          if (Self.LoteRetorno.LocalizaCodigo(Plote)=False)
          Then begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    mensagemaviso('Lote n�o encontrado para ser estornado');
                    exit;
          End;

          Self.LoteRetorno.TabelaparaObjeto;
          Self.LoteRetorno.Status:=dsedit;
          Self.LoteRetorno.Submit_Processado('N');

          if (Self.LoteRetorno.Salvar(False)=False)
          Then Begin
                    mensagemerro('Erro na tentativa de Passar o Campo Processado para "N" do lote');
                    exit;
          end;

          FDataModulo.IBTransaction.CommitRetaining;
          Messagedlg('Reprocessamento Estornado com Sucesso!',mtinformation,[mbok],0);
          exit;
     End;
end;

procedure TObjREGISTRORETORNOCOBRANCA.ExtornaTodosLotes;
var
  ObjQueryTEMP:tibquery;
begin
      Try
            ObjQueryTEMP:=TIBQuery.create(nil);
            ObjQueryTEMP.Database:=FDataModulo.IbDatabase;

      Except
            MensagemErro('Erro na Tentativa de Criar Query Tempor�ria');
            Exit;
      End;
      //resgato todos os lotes
      //extorno todos os lotes
      //reprocesso todos os lotes
      //salvo
      If(MensagemPergunta('Este procedimento Estornar� TODOS os retornos de Boletos.'+#13+' Deseja Continuar?')=mrno)
      Then Exit;

      Try
            With ObjQueryTEMP do
            Begin
                  //resgatando todos os lotes a serem reprocessados
                  Close;
                  Sql.Clear;
                  SQL.Add('Select codigo from TABLOTERETORNOCOBRANCA where processado=''S'' order by codigo');
                  open;
                  Last;

                  FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
                  FMostraBarraProgresso.Lbmensagem.caption:='Estornando Lancamentos de Quita��o';
                  FMostraBarraProgresso.btcancelar.Visible:=True;

                  If(ObjQueryTEMP.RecordCount=0)
                  Then Begin
                        MensagemErro('N�o existem lotes a serem reprocessados!');
                        Exit;
                  End;
                  First;
                  //extornando todos os lotes
                  While not(eof) do
                  Begin
                        FMostraBarraProgresso.IncrementaBarra1(1);
                        FMostraBarraProgresso.show;
                        Application.ProcessMessages;

                        If (FMostraBarraProgresso.Cancelar=True)
                        Then Begin
                                  FMostraBarraProgresso.close;
                                  close;
                                  exit;
                        End;
                        Self.ExtornaReprocessamento(fieldbyname('codigo').asstring,false);
                        {Then Begin
                              MensagemErro('Erro ao estornar lote:'+Plote+' Desfazendo altera��es...');
                              FDataModulo.IBTransaction.RollbackRetaining;
                              Exit;
                        End;}
                        Next;
                  End;
            End;
            FMostraBarraProgresso.btcancelar.Visible:=false;
            //Se chegou aqui entao posso comimtar
            FDataModulo.IBTransaction.CommitRetaining;
            MensagemAviso('Reprocessamento de Lotes Conclu�do com sucesso!');
      Except
            on E:Exception do
            Begin
                messagedlg('N�o foi poss�vel Reprocessar Todos os lotes'+#13+'Erro: '+E.message,mterror,[mbok],0);
                FDataModulo.IBTransaction.RollbackRetaining;
                exit;
            End;
      End;//except geral

      FreeAndNil(ObjQueryTEMP);
      FMostraBarraProgresso.close;

end;

function TObjREGISTRORETORNOCOBRANCA.LerRetornoCNAB400_HSBC(
  PnumeroConvenio: string; PathArquivo: String): boolean;
var
   ACarteira,
   ACodigoBanco,
   ANomeCedente,
   ATipoInscricao,
   ANumeroCPFCGC,
   ACodigoCedente,
   ACodigoAgencia,
   ADigitoCodigoAgencia,
   ANumeroConta,
   ADigitoNumeroConta,
   Aseunumero,AOCorrencia,ANossoNumero: string;
   Adatavencimento:Tdatetime;
   Avalordocumento:Currency;
   NumeroRegistro : integer;
   AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
   ADataOcorrencia,ADataCredito:Tdatetime;
   ADataArquivo:Tdatetime;
   ANumeroArquivo:integer;
   Plote:string;
begin
     result:=False;

Try
     Self.MemoRetorno.clear;

     Try
       Self.Arqretorno.LoadFromFile(PathArquivo);
     Except
           Messagedlg ('Erro na tentativa de Abrir o Arquivo',mterror,[mbok],0);
           exit;
     End;
     NumeroRegistro:=0;//n� da linha

     if Self.Arqretorno.Count <= 0
     then Begin
              Messagedlg('O retorno est� vazio. N�o h� dados para processar',mterror,[mbok],0);
              exit;
     End;


     if (Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,19) <> '02RETORNO01COBRANCA')
     Then Begin
              Messagedlg(PathArquivo+' n�o � um arquivo de retorno de cobran�a',mterror,[mbok],0);
              exit;
     End;


     //L� registro HEADER
     //*******Dados do cedente do t�tulo*****************************
     ATipoInscricao := '';
     ANumeroCPFCGC := '';
     ACodigoCedente := '';
     ACodigoAgencia := '';
     ADigitoCodigoAgencia := '';
     ANumeroConta := '';
     ADigitoNumeroConta := '';
     ANomeCedente := '';

     ACodigoBanco := Copy(Self.ArqRetorno.Strings[NumeroRegistro],77,3);
     if ACodigoBanco <> '399'
     then Begin
            Messagedlg('Este n�o � um retorno de cobran�a do banco HSBC',mterror,[mbok],0);
            exit;
     End;
     ANomeCedente := Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],47,30));

(*
02RETORNO01COBRANCA CNR   020160000000000712  BAENA E CIA LTDA              399HSBC           26060901600BPI0003925366CAARAPO                                                                                                                                                                                                                                                                             000001
*)
     Try
        if StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)) <= 69
         Then ADataArquivo := EncodeDate(StrToInt('20'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)))
         else ADataArquivo := EncodeDate(StrToInt('19'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)));
     Except
           AdataArquivo:=0;
     End;

     ANumeroArquivo := StrToInt(Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,5)));


     Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
     Memoretorno.Add(PathArquivo);
     Memoretorno.Add('');
     Memoretorno.Add('Nome Cedente     : '+ANomeCedente);
     MemoRetorno.Add('Data do Arquivo  : '+datetimetostr(AdataArquivo));
     MemoRetorno.Add('N�mero do Arquivo: '+inttostr(Anumeroarquivo));
    //*****************************************************************
     NumeroRegistro := 1;//proxima linha

     FMostraStringList.caption:='Dados do Arquivo de Retorno';
     FMostraStringList.Memo.Lines.clear;
     FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;
     FMostraStringList.showmodal;

     //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
     //se confirmar gravo um novo Lote
     if (FMostraStringList.tag=0)
     Then exit;
     //Criando um Novo Lote de Retorno de Cobranca

     if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,datetoStr(AdataArquivo),Inttostr(AnumeroArquivo))=True)
     Then Begin
               Self.LoteRetorno.tabelaparaobjeto;
               if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then exit;
     End;

     Self.ZerarTabela;

     //if (Self.LoteRetorno.Convenioboleto.Pegaconvenio(ACodigoBanco)=False)
     //Then exit;

     Self.LoteRetorno.Status:=dsinsert;
     Self.LoteRetorno.Submit_CODIGO(Self.LoteRetorno.Get_NovoCodigo);
     Self.LoteRetorno.Submit_DATAHORA(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');

     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************
     //L� os registros DETALHE
     //Processa at� o pen�ltimo registro porque o �ltimo cont�m apenas o TRAILLER
     for NumeroRegistro:=1 to Self.Arqretorno.Count - 2 do
     begin
(*
19900000003925366020160000000000712  0000000001528849         0000000001528849    2606099                  106250609000000                        0107090000000000149399020169900000000000                                                      0000000000000000000000014900000000000000                            0990520            0000000000000 21                                                   000002
*)
            Self.MemoRetorno.Add('----------------------------------');
            {
            {Confirmar se o tipo do registro � 1}
            if Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,1) <> '1'
            then Continue; {N�o processa o registro atual}

            {ATipoInscricao := Copy(Retorno.Strings[NumeroRegistro],2,2);
            if ATipoInscricao = '01'
            then TipoInscricao := tiPessoaFisica
            else
               if ATipoInscricao = '02'
               then TipoInscricao := tiPessoaJuridica
               else TipoInscricao := tiOutro;
            NumeroCPFCGC := Copy(Retorno.Strings[NumeroRegistro],4,14);
            ContaBancaria.Banco.Codigo := ACodigoBanco;
            Nome := ANomeCedente;}

            AOCorrencia :=Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,2);
            //DescricaoOcorrenciaOriginal := VerificaOcorrenciaOriginal(OcorrenciaOriginal);
            case StrToInt(AOCorrencia) of
               2 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Confirmado');
               3 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               6 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               9 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixado');
               12: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Abatimento');
               13: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Abatimento');
               14: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Vencimento');
               17: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               19: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Protestar');
               20: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Sustar Protesto');
               23: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Encaminhado A Cartorio');
               24: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Retirado De Cartorio');
               25: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protestado');
               26: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Comando Recusado');
               27: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Alterar Dados');
               28: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Debito Tarifas');
               30: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               36: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Conceder Desconto');
               37: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Recebimento Instrucao Cancelar Desconto');
               43: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Protesto Ou Sustacao Estornado');
               44: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixa Ou Liquidacao Estornada');
               45: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Dados Alterados');
            else Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Outras Ocorrencias');
            End;

           //Nosso n�mero SEM D�GITO

           ANossoNumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],38,13);
           Aseunumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],117,10);
           ACarteira := '';

           // Banco nao retorna a data de vencimento de boletos sem regisotro
           Adatavencimento :=0;

           Try
              {********alterado por celio 20/10/2009****************************
              Vou resgatar o valor pago pelo cliente e enviar como valor do doc

              original ate 20/10/09
              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],153,13))/100;
              }

              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],254,13))/100;
           Except
              AValorDocumento:=0;
           End;

           Try
               if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)) <= 69
               then ADataOcorrencia := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)))
               else ADataOcorrencia := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)));
           Except

           End;

            Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
            Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
            Self.MemoRetorno.Add('Carteira         :'+Acarteira);
            //Self.MemoRetorno.Add('Numero Documento :'+ANumeroDocumento);
            Self.MemoRetorno.Add('Data Ocorr�ncia  :'+datetostr(Adataocorrencia));
            Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));


            AValorMoraJuros := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],267,13))/100;
            AValorDesconto := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],241,13))/100;
            tRY
                AValorAbatimento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],228,13))/100;
            eXCEPT
                AValorAbatimento := 0;
            eND;

            AValorIOF := 0;
            Try
                AValorOutrasDespesas := StrToFloat(CompletaPalavra_a_Esquerda(trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],189,13)),12,'0'))/100;
            Except
                AValorOutrasDespesas := 0;
            End;

            Try
                AValorOutrosCreditos := StrToFloat(CompletaPalavra_a_Esquerda(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],280,13)),12,'0'))/100;
            Except
                AValorOutrosCreditos := 0;
            End;

            Try
               ADataCredito:=0;
               ADataCredito := EncodeDate(StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],87,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],85,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],83,2)))

            Except
                  ADataCredito:=0;
            End;

            Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
            Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
            Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
            Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
            Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
            Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

            if (ADataOcorrencia<>0)
            Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
            Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

            if (ADataCredito<>0)
            Then Self.MemoRetorno.Add('Data  Cr�dito    :'+datetostr(ADataCredito))
            Else Self.MemoRetorno.Add('Data  Cr�dito    :');


            Self.ZerarTabela;
            Self.Status:=dsinsert;
            Self.LoteRetorno.Submit_CODIGO(Plote);
            Self.Submit_CODIGO            (Self.Get_NovoCodigo);
            Self.Submit_NossoNumero       (ANossoNumero);
            Self.Submit_seunumero         (ASeuNumero);
            Self.Submit_datavencimento    (datetostr(ADataVencimento));
            Self.Submit_valordocumento    (floattostr(AValorDocumento));
            Self.OCorrencia.Submit_codigo (AOCorrencia);
            Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
            Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
            Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
            Self.Submit_ValorIOF          (floattostr(AValorIOF));
            Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
            Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
            Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
            Self.Submit_DataCredito(datetostr(ADataCredito));
            Self.Submit_Processado('N');

            if (Self.Salvar(False)=False)
            then Begin
                      Messagedlg('Erro na tentativa de Salvar o Registro Linha N�'+inttostr(numeroregistro),mterror,[mbok],0);
                      exit;
            End;
     end;//for



     FDataModulo.IBTransaction.CommitRetaining;
     Self.MemoRetorno.Add('----------------------------------');
     Result := TRUE;
     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
     FmostraStringList.ShowModal;
Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;


end;

function TObjREGISTRORETORNOCOBRANCA.LerRetorno_CobreBem(PnumeroConvenio: string; PathArquivo: String; Var PUltimoLote:String): boolean;
var
   ACodigoBanco,
   ANomeCedente,
   ATipoInscricao,
   ANumeroCPFCGC,
   ACodigoCedente,
   ACodigoAgencia,
   ADigitoCodigoAgencia,
   ANumeroConta,
   ADigitoNumeroConta,
   Aseunumero,AOCorrencia,ANossoNumero : string;
   Adatavencimento:Tdatetime;
   Avalordocumento:Currency;
   NumeroRegistro : integer;
   AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
   ADataOcorrencia:Tdatetime;
   ADataArquivo,ADataCredito:String;
   ANumeroArquivo,COnt,Cont2, ATipoOcorrencia:integer;
   Plote:string;
   PArquivoRemessaRetorno, PBoletoBancario:String;

begin
     result:=False;
Try
     Self.MemoRetorno.clear;
     Self.LoteRetorno.Convenioboleto.LocalizaCodigo(PnumeroConvenio);
     Self.LoteRetorno.Convenioboleto.TabelaparaObjeto;

     if (ObjCobreBem.ConfiguraContaCorrente(PnumeroConvenio)=false)
     then exit;

     ObjCobreBem.CobreBemX.ArquivoRetorno.Diretorio:=ExtractFileDir(PathArquivo)+'\';
     ObjCobreBem.CobreBemX.ArquivoRetorno.Arquivo:=ExtractFileName(PathArquivo);
     ObjCobreBem.CobreBemX.ArquivoRetorno.Layout:=Self.LoteRetorno.Convenioboleto.Get_CobreBemLayoutRetorno; //'FEBRABAN240';
     ObjCobreBem.CobreBemX.CarregaArquivosRetorno;

     if (ObjCobreBem.CobreBemX.UltimaMensagemErro <> '')
     then  MensagemErro(ObjCobreBem.CobreBemX.UltimaMensagemErro);

     if (ObjCobreBem.CobreBemX.OcorrenciasCobranca.Count <= 0)
     then Begin
              MensagemErro('O retorno est� vazio. N�o h� dados para processar');
              exit;
     end;

     ANomeCedente     := ObjCobreBem.CobreBemX.NomeCedente ;
     ACodigoCedente   := ObjCobreBem.CobreBemX.CodigoCedente ;
     ANumeroCPFCGC    := ObjCobreBem.CobreBemX.CnpjCpfCedente;
     ACodigoAgencia   := ObjCobreBem.CobreBemX.CodigoAgencia;
     ANumeroConta     := ObjCobreBem.CobreBemX.NumeroContaCorrente;
     ACodigoBanco     := ObjCobreBem.CobreBemX.NumeroBanco;

     Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
     Memoretorno.Add(PathArquivo);

     Memoretorno.Add('');
     Memoretorno.Add('C�digo do Banco  : '+ACodigoBanco);
     MemoRetorno.Add('N�mero CNPJ      : '+ANumeroCPFCGC);
     MemoRetorno.Add('C�digo Cedente   : '+ACodigoCedente);
     MemoRetorno.Add('Banco            : '+ACodigoBanco);
     MemoRetorno.Add('Ag�ncia          : '+ACodigoAgencia);
     MemoRetorno.Add('Conta            : '+ANumeroConta);
     MemoRetorno.Add('Cedente          : '+ANomeCedente);
    //*****************************************************************



     ADataArquivo :=  ObjCobreBem.CobreBemX.ArquivoRetorno.DataGravacao;
     ANumeroArquivo := ObjCobreBem.CobreBemX.ArquivoRetorno.Sequencia;
     Self.MemoRetorno.Add('Data do Arquivo  :'+ADataArquivo);
     Self.MemoRetorno.Add('N�   do Arquivo  :'+IntToStr(ANumeroArquivo));
     //********************************************************************
     FMostraStringList.caption:='Dados do Arquivo de Retorno';
     FMostraStringList.Memo.Lines.clear;
     FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;
     FMostraStringList.showmodal;


     //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
     //se confirmar gravo um novo Lote
     if (FMostraStringList.tag=0)
     Then exit;


     if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,AdataArquivo,Inttostr(AnumeroArquivo))=True)
     Then Begin
               Self.LoteRetorno.tabelaparaobjeto;
               if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then exit;
     End;
     //Criando um Novo Lote de Retorno de Cobranca

     Self.ZerarTabela;
     Self.LoteRetorno.Status:=dsinsert;
     PUltimoLote:=Self.LoteRetorno.Get_NovoCodigo;
     Self.LoteRetorno.Submit_CODIGO(PUltimoLote);
     Self.LoteRetorno.Submit_DATAHORA(ADataArquivo);
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');
     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************

     PArquivoRemessaRetorno:=ObjCobreBem.RetornaNovoArquivoRemessaRetorno;


     For Cont:=0 to ObjCobreBem.CobreBemX.OcorrenciasCobranca.Count-1 do
     Begin
            Self.MemoRetorno.Add('----------------------------------');
            PBoletoBancario:=ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].NumeroDocumento;

            // Se n�o achar o Boleto

            Self.MemoRetorno.Add('BOLETO.....: '+PBoletoBancario);
            if (PBoletoBancario <> '')
            then Begin
                  AOCorrencia := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].CodigoOcorrencia;
                  Self.MemoRetorno.Add('Ocorrencia : '+AOCorrencia+' - '+Self.RetornaNomeOcorrencia(AOCorrencia));

                  For Cont2:=0 to ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].MotivosOcorrencia.Count-1 do
                  Begin
                       Self.MemoRetorno.Add('    Motivo'+IntToStr(Cont2)+': '+ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].MotivosOcorrencia.Item[Cont2].Codigo+' - '+
                                                                              ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].MotivosOcorrencia.Item[Cont2].Descricao);
                  end;


                  //Nosso n�mero SEM D�GITO
                  ANossoNumero := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].NossoNumero;

                  // Esses dois campos eu n�o achei no cobrebem
                  Aseunumero := '';
                  Adatavencimento := StrToDate(Self.RetornaVencimentoPendencia(PBoletoBancario));

                  Avalordocumento := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].ValorPago;

                  Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
                  Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
                  Self.MemoRetorno.Add('Data Vencimento  :'+DateToStr(Adatavencimento));
                  Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));

                  AValorMoraJuros := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].ValorJurosPago;
                  AValorDesconto := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].ValorDesconto;
                  AValorAbatimento := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].ValorAbatimento;
                  AValorIOF := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].ValorIOF;
                  AValorOutrasDespesas := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].ValorOutrasDespesas;
                  AValorOutrosCreditos := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].ValorOutrosAcrescimos;

                  Try
                      ADataOcorrencia:=StrToDate(ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].DataOcorrencia);
                  Except
                      ADataOcorrencia:=0;
                  End;


                  Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
                  Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
                  Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
                  Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
                  Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
                  Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

                  if (ADataOcorrencia<>0)
                  Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
                  Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

                  ADataCredito := ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].DataCredito;
                  Self.MemoRetorno.Add('Data  Cr�dito    :'+ADataCredito);


                  if(Self.Boleto.LocalizaCodigo(PBoletoBancario))
                  then begin
                        // Gravando na Tabela RegistroRetornoCobran�a, Porem somente posso gravar nessa tabela os
                        // Boletos que ser�o quuitados, para aproveitar a fun��o de QuitaPendencia do Ronnei
                        if (Self.OCorrencia.LocalizaCodigo(AOCorrencia)=false)
                        then Begin
                                 MensagemErro('Ocorrencia n�o encontrada');
                                 exit;
                        end;
                        Self.OCorrencia.TabelaparaObjeto;
                        if (Self.OCorrencia.Get_LiquidaPendencia = 'S')
                        then Begin

                                      Self.ZerarTabela;
                                      Self.Status:=dsinsert;
                                      Self.LoteRetorno.Submit_CODIGO(Plote);
                                      Self.Boleto.Submit_CODIGO(PBoletoBancario);
                                      Self.Submit_CODIGO            (Self.Get_NovoCodigo);
                                      Self.Submit_NossoNumero       (ANossoNumero);
                                      Self.Submit_seunumero         (ASeuNumero);
                                      Self.Submit_datavencimento    (datetostr(ADataVencimento));
                                      Self.Submit_valordocumento    (floattostr(AValorDocumento));
                                      Self.OCorrencia.Submit_codigo (AOCorrencia);
                                      Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
                                      Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
                                      Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
                                      Self.Submit_ValorIOF          (floattostr(AValorIOF));
                                      Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
                                      Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
                                      Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
                                      try
                                         StrToDate(ADataCredito);
                                      except
                                         ADataCredito:='';
                                      end;
                                      Self.Submit_DataCredito(ADataCredito);
                                      Self.Submit_Processado('N');

                                      if (Self.Salvar(False)=False)
                                      then Begin
                                                Messagedlg('Erro na tentativa de Salvar o Registro',mterror,[mbok],0);
                                                exit;
                                      End;

                        end;

                        // Gravando na TabBoletoInstrucos pra ter um acompanhamento desse boleto
                        // Pode Acontecer de Um Boleto ter varias Ocorrencias por isso eu Gravo cada
                        // ocorrencia separado
                        For Cont2:=0 to ObjCobreBem.CobreBemX.OcorrenciasCobranca[Cont].MotivosOcorrencia.Count-1 do
                        Begin


                             Self.ObjCobreBem.ObjBoletoInstrucoes.ZerarTabela;
                             Self.ObjCobreBem.ObjBoletoInstrucoes.Status:=dsInsert;
                             Self.ObjCobreBem.ObjBoletoInstrucoes.Submit_Codigo(Self.ObjCobreBem.ObjBoletoInstrucoes.Get_NovoCodigo);
                             Self.ObjCobreBem.ObjBoletoInstrucoes.BoletoBancario.Submit_CODIGO(PBoletoBancario);
                             Self.ObjCobreBem.ObjBoletoInstrucoes.ArquivoRemessaRetorno.Submit_Codigo(PArquivoRemessaRetorno);
                             Self.ObjCobreBem.ObjBoletoInstrucoes.Submit_Instrucao('');
                             Self.ObjCobreBem.ObjBoletoInstrucoes.Ocorrencia.Submit_CODIGO(AOCorrencia);
                             Self.ObjCobreBem.ObjBoletoInstrucoes.Submit_Data(DateToStr(Now));
                             if (Self.ObjCobreBem.ObjBoletoInstrucoes.Salvar(false)=false)
                             then Begin
                                     MensagemErro('Erro ao tentar salvar Boleto Instrucoes');
                                     exit;
                             end;

                        end;
                  end
                  else begin
                        Self.MemoRetorno.Add('Boleto n� '+PBoletoBancario+' N�o localizado!')
                  end;
            end else
            Begin
                   Self.MemoRetorno.Add('Existe um item do arquivo de retorno sem o Codigo do Boleto');
                   Self.MemoRetorno.Add('Provavelmente n�o seja um boleto Registrado');
            end;

     end;
     // Atualizando o ArquivoRemessaRetorno
     ObjCobreBem.ObjPendencia.BoletoBancario.ArquivoRemessaRetorno.LocalizaCodigo(PArquivoRemessaRetorno);
     ObjCobreBem.ObjPendencia.BoletoBancario.ArquivoRemessaRetorno.TabelaparaObjeto(false);
     ObjCobreBem.ObjPendencia.BoletoBancario.ArquivoRemessaRetorno.Status:=dsEdit;
     ObjCobreBem.ObjPendencia.BoletoBancario.ArquivoRemessaRetorno.Submit_ArquivoRetorno(PathArquivo);
     if (ObjCobreBem.ObjPendencia.BoletoBancario.ArquivoRemessaRetorno.Salvar(false)=false)
     then Begin
             MensagemErro('Erro ao tentar salvar o ArquivoRemessaRetorno');
             exit;
     end;         

     FDataModulo.IBTransaction.CommitRetaining;
     Self.MemoRetorno.Add('----------------------------------');
     Result := TRUE;
     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
     FmostraStringList.ShowModal;

Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;

end;


Function TObjREGISTRORETORNOCOBRANCA.RetornaNomeOcorrencia(POcorrencia:String):String;
Begin
     With Self.Objquery do
     Begin
          Close;
          SQL.Clear;
          SQL.Add('Select Nome from TABOCORRENCIARETORNOCOBRANCA Where Codigo = '+POcorrencia);
          Open;

          Result:=fieldbyname('Nome').AsString;
     end;
end;


function TObjREGISTRORETORNOCOBRANCA.ReprocessaLote_CobreBem(Plote: string): boolean;
var
PNumeroConvenio,PCampoLivre,PNossoNumero:String;
Pposicao:integer;
pjuros,PValorBoletoSIS,PValorBoletobanco,pdesconto:Currency;
LocalizaPorNUmeroBanco,Achou:boolean;

begin
     result:=False;

     {Usado apos a captura do arquivo, a captura apenas preenche registros
     na tabela Lote e Registros

     Passo para todos para processado, os que eu encontro o boleto coloco o numero do boleto
     e deixo o situacoa em brnaxco para ser processado no momento de quitar as pendencias

     os que nao encontram pendencias coloco como NE(nao encontrado boleto)}

     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.add('PROCESSAMENTO DOS REGISTROS DE RETORNO');
     FmostraStringList.Memo.lines.add(' ');
     FmostraStringList.Memo.lines.add('LOCALIZANDO BOLETOS ...');


     if (plote='')
     Then Begin
               Messagedlg('Escolha um lote',mtinformation,[mbok],0);
               exit;
     End;

     if (Self.LoteRetorno.LocalizaCodigo(plote)=False)
     Then Begin
               Messagedlg('Lote N� '+Plote+' n�o encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.LoteRetorno.TabelaparaObjeto;
     PNumeroConvenio:=Self.LoteRetorno.Convenioboleto.Get_numeroconvenio;


     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          Sql.add('Select * from TabRegistroRetornoCobranca where LoteRetorno='+plote+' and Processado=''N'' ');
          open;
          if (Recordcount=0)
          Then Begin
                    Messagedlg('N�o foi encontrado nenhum registro deste lote que n�o tenha sido processado',mterror,[mbok],0);
                    exit;
          End;
          first;

          Self.LoteRetorno.Status:=dsedit;
          Self.LoteRetorno.Submit_Processado('S');
          if (Self.LoteRetorno.Salvar(False)=False)
          Then Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Erro na tentativa de Passar o Lote para Processado=''S'' ',mterror,[mbok],0);
                    exit;
          End; 

          While not(eof) do
          Begin

               Self.ZerarTabela;
               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               Self.Processado:='S';

               Achou:=true;
               if (Self.Boleto.LocalizaCodigo(Self.Boleto.Get_CODIGO)=False)
               Then Begin
                         FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' N�o Encontrado');
                         Self.Boleto.ZerarTabela;
                         Self.Situacao:='BOLETO N�O ENCONTRADO';//n�o encontrado
                         achou:=false;
               End;

               if (achou)
               Then Begin
                         Self.Boleto.TabelaparaObjeto;
                         try
                            PValorBoletoSis:=0;
                            if (trim(Self.Boleto.get_valordoc)<>'')
                            Then PValorBoletoSIS:=Strtofloat(Self.Boleto.get_valordoc);
                         Except
                            PValorBoletoSis:=0;
                         End;

                         Try
                            PValorBoletobanco:=Strtofloat(Self.Get_valordocumento);
                         Except
                            PValorBoletobanco:=0;
                         End;

                         pjuros:=0;
                         if (PValorBoletoSIS<PValorBoletobanco)
                         Then Begin
                                   //O valor que veio do banco � maior, pode ter sido o juro
                                   //que nao foi adicionado no campo de juros
                                   //e sim no campo de valor
                                   Try
                                     pjuros:=strtocurr(Self.Get_ValorJuros);
                                   Except
                                     pjuros:=0;
                                   End;

                                   Self.Submit_ValorJuros(currtostr(PvalorBoletoBanco-Pvalorboletosis));
                                   Self.Submit_valordocumento(FloatToStr(Pvalorboletosis+(PvalorBoletoBanco-Pvalorboletosis)));
                         End;

                         pdesconto:=0;
                         if (PValorBoletoSIS>PValorBoletobanco)
                         Then Begin

                                {********************110808 alterado por celio
                                Self.Situacao:='DIVERG. DE VALOR ENTRE BOLETO NO SIST. E RETORNO';
                                FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' DIVERG�NCIA DE VALOR ENTRE BOLETO NO SISTEMA E NO RETORNO. Boleto Sistema N� '+Self.Boleto.get_codigo);
                                Self.Boleto.ZerarTabela;}

                                Try
                                      pdesconto:=StrToCurr(self.Get_ValorDesconto);
                                Except
                                      pdesconto:=0;
                                End;

                                Self.Submit_ValorDesconto(currtostr(Pvalorboletosis-PvalorBoletoBanco));
                                Self.Submit_valordocumento(FloatToStr(Pvalorboletosis-(Pvalorboletosis-PvalorBoletoBanco)));
                                FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' Encontrado Boleto N� '+Self.Boleto.get_codigo);
                         End
                         Else FmostraStringList.Memo.lines.add('BOLETO: '+PCampoLivre+' Encontrado Boleto N� '+Self.Boleto.get_codigo);
               End;

               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Salvar o Registro N�'+Self.codigo,mterror,[mbok],0);
                         FmostraStringList.Memo.Lines.add('Erro ao tentar atualizar o Registro N�'+Self.codigo+', nosso numero='+Self.NossoNumero);
               End;

               next;
          End;

          if (Self.QuitaPendencias(Plote)=False)
          Then FdataModulo.IBTransaction.RollbackRetaining
          Else FdataModulo.IBTransaction.CommitRetaining;

          FmostraStringList.showmodal;
          result:=true;
     End;
end;

function TObjREGISTRORETORNOCOBRANCA.RetornaVencimentoPendencia(PBoleto:String):String;
Begin
     With Self.Objquery do
     Begin
           Close;
           SQL.Clear;
           SQL.Add('Select Vencimento from TabPendencia Where BoletoBancario = '+PBoleto);
           Open;

           if (RecordCount > 0)
           then Result:=fieldbyname('Vencimento').AsString
           else Result:='01/01/1500';

     end;

end;

function TObjREGISTRORETORNOCOBRANCA.LerRetornoCNAB400_SICOOB(
  PnumeroConvenio: string; PathArquivo: String): boolean;
var
  ACarteira,
  ACodigoBanco,
  ANomeCedente,
  ATipoInscricao,
  ANumeroCPFCGC,
  ACodigoCedente,
  ACodigoAgencia,
  ADigitoCodigoAgencia,
  ANumeroConta,
  ADigitoNumeroConta,
  Aseunumero,AOCorrencia,ANossoNumero: string;
  Adatavencimento:Tdatetime;
  Avalordocumento:Currency;
  NumeroRegistro : integer;
  AValorMoraJuros,AValorDesconto,AValorAbatimento,AValorIOF,AValorOutrasDespesas,AValorOutrosCreditos:Currency;
  ADataOcorrencia,ADataCredito:Tdatetime;
  ADataArquivo:Tdatetime;
  ANumeroArquivo:integer;
  Plote:string;
begin
  result:=False;
  Try
    Self.MemoRetorno.clear;
    Try
      Self.Arqretorno.LoadFromFile(PathArquivo);
    Except
      Messagedlg ('Erro na tentativa de Abrir o Arquivo',mterror,[mbok],0);
      exit;
    End;
    NumeroRegistro:=0;//n� da linha

    if Self.Arqretorno.Count <= 0 then
    Begin
      Messagedlg('O retorno est� vazio. N�o h� dados para processar',mterror,[mbok],0);
      exit;
    End;
    if (Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,19) <> '02RETORNO01COBRAN�A') then
    begin
      Messagedlg(PathArquivo+' n�o � um arquivo de retorno de cobran�a',mterror,[mbok],0);
      exit;
    end;

    //L� registro HEADER
    //*******Dados do cedente do t�tulo*****************************
    ATipoInscricao := '';
    ANumeroCPFCGC := '';
    ACodigoCedente := '';
    ACodigoAgencia := '';
    ADigitoCodigoAgencia := '';
    ANumeroConta := '';
    ADigitoNumeroConta := '';
    ANomeCedente := '';

    ACodigoBanco := Copy(Self.ArqRetorno.Strings[NumeroRegistro],77,3);

    if (ACodigoBanco <> '756') then
    begin
      Messagedlg('Este n�o � um retorno de cobran�a do banco Sicoob',mterror,[mbok],0);
      exit;
    End;
    ANomeCedente := Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],47,30));

    Try
      if StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)) <= 69 Then
        ADataArquivo := EncodeDate(StrToInt('20'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)))
      else
        ADataArquivo := EncodeDate(StrToInt('19'+Copy(Self.ArqRetorno.Strings[NumeroRegistro],99,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],97,2)),StrToInt(Copy(Self.ArqRetorno.Strings[NumeroRegistro],95,2)));
    except
      AdataArquivo:=0;
    end;

    ANumeroArquivo := StrToInt(Trim(Copy(Self.ArqRetorno.Strings[NumeroRegistro],101,7)));


    Memoretorno.Add('DADOS DO ARQUIVO DE RETORNO');
    Memoretorno.Add(PathArquivo);
    Memoretorno.Add('');
    Memoretorno.Add('Nome Cedente     : '+ANomeCedente);
    MemoRetorno.Add('Data do Arquivo  : '+datetimetostr(AdataArquivo));
    MemoRetorno.Add('N�mero do Arquivo: '+inttostr(Anumeroarquivo));
    //*****************************************************************
    NumeroRegistro := 1;//proxima linha

    FMostraStringList.caption:='Dados do Arquivo de Retorno';
    FMostraStringList.Memo.Lines.clear;
    FMostraStringList.Memo.Lines.text:=MemoRetorno.Text;
    FMostraStringList.showmodal;

    //Mostra os Dados de cabecalho do Arquivo e pede Confirmacao
    //se confirmar gravo um novo Lote
    if (FMostraStringList.tag=0)
    Then exit;
     //Criando um Novo Lote de Retorno de Cobranca

    if (Self.LoteRetorno.LocalizaArquivo(pnumeroconvenio,datetoStr(AdataArquivo),Inttostr(AnumeroArquivo))=True) then
    Begin
      Self.LoteRetorno.tabelaparaobjeto;
      if (Messagedlg('O lote '+Self.loteretorno.Get_codigo+' possui a mesma data e n�mero desse arquivo, certeza que deseja Import�-lo?',mtconfirmation,[mbyes,mbno],0)=Mrno) Then
        exit;
    End;

     Self.ZerarTabela;

     //if (Self.LoteRetorno.Convenioboleto.Pegaconvenio(ACodigoBanco)=False)
     //Then exit;

     Self.LoteRetorno.Status:=dsinsert;
     Self.LoteRetorno.Submit_CODIGO(Self.LoteRetorno.Get_NovoCodigo);
     Self.LoteRetorno.Submit_DATAHORA(datetostr(ADataArquivo));
     Self.LoteRetorno.Submit_NOMEARQUIVO(ExtractFileName(PathArquivo));
     Self.LoteRetorno.Submit_NUMEROARQUIVO(IntToStr(ANumeroArquivo));
     Self.LoteRetorno.Submit_CGCEMPRESA(ANumeroCPFCGC);
     Self.LoteRetorno.Submit_CODIGOCEDENTE(ACodigoCedente);
     Self.LoteRetorno.Submit_AGENCIA(ACodigoAgencia);
     Self.LoteRetorno.Submit_DIGITOAGENCIA(ADigitoCodigoAgencia);
     Self.LoteRetorno.Submit_CONTA(ANumeroConta);
     Self.LoteRetorno.Submit_DIGITOCONTA(ADigitoNumeroConta);
     Self.LoteRetorno.Convenioboleto.Submit_CODIGO(PnumeroConvenio);
     Self.LoteRetorno.Submit_Processado('N');

     if (Self.LoteRetorno.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na tentativa de Salvar um Novo lote de Retorno de Cobran�a',mterror,[mbok],0);
               exit;
     End;
     Plote:=Self.LoteRetorno.Get_CODIGO;

     //*******************************************************************
     //L� os registros DETALHE
     //Processa at� o pen�ltimo registro porque o �ltimo cont�m apenas o TRAILLER
     for NumeroRegistro:=1 to Self.Arqretorno.Count - 2 do
     begin
            Self.MemoRetorno.Add('----------------------------------');
            {
            {Confirmar se o tipo do registro � 1}
            if Copy(Self.ArqRetorno.Strings[NumeroRegistro],1,1) <> '1' then
              Continue; {N�o processa o registro atual}

            {ATipoInscricao := Copy(Retorno.Strings[NumeroRegistro],2,2);
            if ATipoInscricao = '01'
            then TipoInscricao := tiPessoaFisica
            else
               if ATipoInscricao = '02'
               then TipoInscricao := tiPessoaJuridica
               else TipoInscricao := tiOutro;
            NumeroCPFCGC := Copy(Retorno.Strings[NumeroRegistro],4,14);
            ContaBancaria.Banco.Codigo := ACodigoBanco;
            Nome := ANomeCedente;}

            AOCorrencia :=Copy(Self.ArqRetorno.Strings[NumeroRegistro],109,2);
            //DescricaoOcorrenciaOriginal := VerificaOcorrenciaOriginal(OcorrenciaOriginal);
            case StrToInt(AOCorrencia) of
               2 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Confirmado');
               3 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Registro Recusado');
               5 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado sem registro');
               6 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Liquidado');
               9 : Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixado');
               10: Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Baixa Solicitada');
            else Self.MemoRetorno.Add('Tipo de Ocorr�nc.:Outras Ocorrencias');
            End;
            {





               //Dados que variam de acordo com o banco


               CodigoAgencia := Copy(Retorno.Strings[NumeroRegistro],25,5);
               NumeroConta := Copy(Retorno.Strings[NumeroRegistro],30,7);
               DigitoConta := Copy(Retorno.Strings[NumeroRegistro],37,1);
               ValorDespesaCobranca := StrToFloat(Copy(Retorno.Strings[NumeroRegistro],176,13))/100;

}

           //Nosso n�mero SEM D�GITO

           //ANumeroDocumento:= Copy(Self.ArqRetorno.Strings[NumeroRegistro],111,10);
           //Aseunumero:=Copy(Self.ArqRetorno.Strings[NumeroRegistro],111,10);;

           ANossoNumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],63,11);
//           Aseunumero := Copy(Self.ArqRetorno.Strings[NumeroRegistro],117,10);
           ACarteira := Copy(Self.ArqRetorno.Strings[NumeroRegistro],106,1);

           // Banco nao retorna a data de vencimento de boletos sem regisotro
           Adatavencimento :=0;

           Try
              {********alterado por celio 20/10/2009****************************
              Vou resgatar o valor pago pelo cliente e enviar como valor do doc

              original ate 20/10/09
              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],153,13))/100;
              }
              AValorDocumento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],254,13))/100;
           Except
              AValorDocumento:=0;
           End;

           Try
               if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)) <= 69
               then ADataOcorrencia := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)))
               else ADataOcorrencia := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],115,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],113,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],111,2)));
           Except

           End;

            Self.Memoretorno.Add('Nosso N�mero     :'+ANossoNumero);
            Self.MemoRetorno.Add('Seu N�mero       :'+Aseunumero);
            Self.MemoRetorno.Add('Carteira         :'+Acarteira);
            //Self.MemoRetorno.Add('Numero Documento :'+ANumeroDocumento);
            Self.MemoRetorno.Add('Data Ocorr�ncia  :'+datetostr(Adataocorrencia));
            Self.MemoRetorno.Add('Valor Documento  :'+formata_valor(Avalordocumento));


            AValorMoraJuros := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],267,13))/100;
            AValorDesconto := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],241,13))/100;
            AValorAbatimento := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],228,13))/100;
            AValorIOF := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],215,13))/100;
            AValorOutrasDespesas := StrToFloat(Copy(Self.Arqretorno.Strings[NumeroRegistro],189,13))/100;
            AValorOutrosCreditos := StrToFloat(Trim(Copy(Self.Arqretorno.Strings[NumeroRegistro],280,13)));

            Try
               ADataCredito:=0;
               if Self.Formatar(Copy(Self.Arqretorno.Strings[NumeroRegistro],176,6),6,false,'0') <> '000000'
               Then Begin
                        if StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],300,2)) <= 69
                        then ADataCredito := EncodeDate(StrToInt('20'+Copy(Self.Arqretorno.Strings[NumeroRegistro],180,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],178,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],176,2)))
                        else ADataCredito := EncodeDate(StrToInt('19'+Copy(Self.Arqretorno.Strings[NumeroRegistro],180,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],178,2)),StrToInt(Copy(Self.Arqretorno.Strings[NumeroRegistro],176,2)));
               End;

            Except
                  ADataCredito:=0;
            End;

            Self.MemoRetorno.Add('Valor Juros      :'+formata_valor(AValorMoraJuros));
            Self.MemoRetorno.Add('Valor Desconto   :'+formata_valor(AValorDesconto));
            Self.MemoRetorno.Add('Valor Abatimento :'+formata_valor(AValorAbatimento));
            Self.MemoRetorno.Add('Valor IOF        :'+formata_valor(AValorIOF));
            Self.MemoRetorno.Add('Valor Outras Desp:'+formata_valor(AValorOutrasDespesas));
            Self.MemoRetorno.Add('Valor Outros Cred:'+formata_valor(AValorOutrosCreditos));

            if (ADataOcorrencia<>0)
            Then Self.MemoRetorno.Add('Data  Ocorr�ncia :'+datetostr(ADataOcorrencia))
            Else Self.MemoRetorno.Add('Data  Ocorr�ncia :');

            if (ADataCredito<>0)
            Then Self.MemoRetorno.Add('Data  Cr�dito    :'+datetostr(ADataCredito))
            Else Self.MemoRetorno.Add('Data  Cr�dito    :');


            Self.ZerarTabela;
            Self.Status:=dsinsert;
            Self.LoteRetorno.Submit_CODIGO(Plote);
            Self.Submit_CODIGO            (Self.Get_NovoCodigo);
            Self.Submit_NossoNumero       (ANossoNumero);
            Self.Submit_seunumero         (ASeuNumero);
            Self.Submit_datavencimento    (datetostr(ADataVencimento));
            Self.Submit_valordocumento    (floattostr(AValorDocumento));
            Self.OCorrencia.Submit_codigo (AOCorrencia);
            Self.Submit_ValorJuros        (floattostr(AValorMoraJuros));
            Self.Submit_ValorDesconto     (floattostr(AValorDesconto));
            Self.Submit_ValorAbatimento   (floattostr(AvalorAbatimento));
            Self.Submit_ValorIOF          (floattostr(AValorIOF));
            Self.Submit_ValorOutrasDespesas(floattostr(AValorOutrasDespesas));
            Self.Submit_ValorOutrosCreditos(floattostr(AValorOutrosCreditos));
            Self.Submit_DataOcorrencia(DateToStr(ADataOcorrencia));
            Self.Submit_DataCredito(datetostr(ADataCredito));
            Self.Submit_Processado('N');

            if (Self.Salvar(False)=False)
            then Begin
                      Messagedlg('Erro na tentativa de Salvar o Registro Linha N�'+inttostr(numeroregistro),mterror,[mbok],0);
                      exit;
            End;
     end;//for



     FDataModulo.IBTransaction.CommitRetaining;
     Self.MemoRetorno.Add('----------------------------------');
     Result := TRUE;
     FmostraStringList.Memo.Clear;
     FmostraStringList.Memo.lines.Text:=Self.MemoRetorno.Text;
     FmostraStringList.ShowModal;
Finally
       FDataModulo.IBTransaction.RollbackRetaining;
End;
end;

end.

02RETORNO01COBRANCA       1026903065554000165                               748BANSICREDI     20100326        0000167                                                                                                                                                                                                                                                                                 1.00000001
1            C0000000000                       102000703                                                    06250310          090302                    0000000100000                                                              00000000000000000000000000000000010000000000000000000000000000000                          00        20100326                                                          000002
1            C0000000000                       102000703                                                    28250310          090302                    0000000000165                                                              00000000000000000000000000000000010000000000000000000000000000000                          B3        20100325                                                          000003
1            C0000000000                       108000888                                                    06250310          COMPE                     0000000012000                                                              00000000000000000000000000000000001200000000000000000000000000000                          H5        20100326                                                          000004
1            C0000000000                       108000888                                                    28250310          COMPE                     0000000000165                                                              00000000000000000000000000000000001200000000000000000000000000000                          B3        20100325                                                          000005
1            C0000000000                       108001795                                                    06250310          COMPE                     0000000012000                                                              00000000000000000000000000000000001200000000000000000000000000000                          H5        20100326                                                          000006
1            C0000000000                       108001795                                                    28250310          COMPE                     0000000000165                                                              00000000000000000000000000000000001200000000000000000000000000000                          B3        20100325                                                          000007
9274810269                                                                                                                                                                                                                                                                                                                                                                                                000008




