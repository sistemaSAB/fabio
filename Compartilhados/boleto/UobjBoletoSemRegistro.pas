unit UobjBoletoSemRegistro;

interface

uses extctrls,dateutils,dialogs,URelBoletoSemRegistro,sysutils,classes,graphics;

type

  TObjBoletoSemRegistro=Class
        public

            Vencimento:TDatetime;
            NossoNumero_partedois,
            NomeBanco,CodigoBAnco,DgBanco,
            LocalPagamento,numeroconvenio,
            Agencia,DGAgencia,ContaCorrente,DgContaCorrente,
            DataDoc,NumeroDoc,EspecieDoc,Aceite,DataProc,NumContaResponsavel,
            Carteira,compcarteira,EspecieMoeda,QuantidadeMoeda,ValorMoeda,
            ValorDoc,Desconto,OutrasDeducoes,Juros,Multa,outrosacrescimos,
            ValorCobrado,
            Nomecedente,
            FisicajuridicaSacado,
            NomeSacado,EnderecoSacado,BairroSacado,CepSacado,CidadeSacado,EstadoSacado,
            CpfSacado,RgSacado,numerocasasacado,
            Codigocedente:String;

            //exclusivos  da caixa economica
            CodigoCarteira_caixa,CodigoSICOB_Caixa:String;
            //**********************************

            //exclusivos do Unibanco
            codigotransacao_unibanco,posicoes_28_a_29_UNIBANCO,numerocliente_UNIBANCO:String;
            //*************************

            //exclusivos do Sicred
            posicao20_sicred:string;
            posicao21_sicred:string;
            posicao31_34_sicred:string;
            posicao35_36_sicred:string;
            posicao37_41_sicred:string;
            posicao42_sicred:string;
            posicao43_sicred:string;
            //***********************


            //exclusivos do HSBC
            tipoidentificador_HSBC : string;
            codigocedente_HSBC : string;
            codigoCNR_HSBC : string;
            //***********************

            ObservacoesSuperior:String;
            Instrucoes:TStringList;
            QtdeDigitosNossoNumero:Integer;

            Constructor create;
            Destructor  Free;
            Procedure   ImprimeBoleto(PPreview:Boolean);overload;
            Procedure   ImprimeBoleto(PPreview:Boolean;Modelo:Integer);overload;
            procedure   ZerarCampos;
        private
            DGNossoNUmero,NossoNumeroGerado:String;
            linhadigitavel,CodigoBarras:String;
            function  Modulo10(PValor: String): Integer;
            function  Modulo11_9(PValor: String): string;

            function  Retiracaractere(Pvalor: String; Pcaractere: char): string;
            Function  GeraLinhaDigitavel:Boolean;
            //*****Funcoes para Banco do Brasil***********

            Function GeraCodigoBarrasBB:Boolean;
            Function GeranossonumeroBB:boolean;
            //*****Funcoes para Bradesco***********
            Function GeraCodigoBarrasBradesco:Boolean;
            Function GeranossonumeroBradesco:boolean;
            function GeraDigitoVerificadornossoNumeroBradesco(PValor: String): string;
            //**********************************************

            //*****Funcoes para Caixa Economica***********
            Function GeraCodigoBarrasCaixaEconomica:Boolean;
            Function GeraNossoNumeroCaixaEconomica:boolean;
            //**********************************************

            //*****Funcoes para Unibanco***********
            Function GeraCodigoBarrasUnibanco:Boolean;
            Function GeraNossoNumeroUnibanco:boolean;
            function  Modulo11_2_a_9_Unibanco_nosso_numero(PValor: String): string;
            //**********************************************

            //*****Funcoes para Sicred***********
            function GeraNossoNumeroSIcred: boolean;
            function GeraCodigoBarrasSicred: Boolean;
            Function CalculaDVCampoLivreSicred(pvalor:string):string;
            //************************************

            //*****Funcoes para HSBC***********
            Function GeraCodigoBarrasHSBC:Boolean;
            Function GeraNossoNumeroHSBC:boolean;
            //**********************************************



            function  CompletaPalavra_Esquerda(palavra: string; quantidade: Integer;ValorASerUSado: String): String;
            function  Formata_valor(Pvalor: string): string;
            function  Modulo11_DVG(PValor: String): string;
            function  Modulo11_CAIXAECONOMICA(PValor: String): string;
            function  CalculaFatorVencimento(PVencimento:Tdatetime):String;
            //********************************************
            Function ComePonto(Pvalor:String):String;
    function Modulo11_nosso_numero_HSBC(PValor: String): string;





  End;

   TgbCobCodBar = class
   private

      function Define2de5 : string; {Define o formato do c�digo de barras INTERCALADO 2 DE 5, retornando a seq��ncia de 0 e 1 que ser� usada para gerar a imagem do c�digo de barras}
      function GetImagem    : TImage; {Gera a imagem do c�digo de barras}
   public
      CodigoBarras: string; {Dados que ser�o inclu�dos no c�digo de barras}
      property Imagem    : TImage read GetImagem;

   end;



implementation

uses Controls, QuickRpt, UessencialGlobal, UrelboletoSemRegistroModelo2;

{TgbCobCodBar}

function TgbCobCodBar.Define2de5 : string;
//Traduz d�gitos do c�digo de barras para valores de 0 e 1, formando um c�digo do tipo Intercalado 2 de 5
var
   CodigoAuxiliar : string;
   Start   : string;
   Stop    : string;
   T2de5   : array[0..9] of string;
   Codifi  : string;
   I       : integer;

begin
   Result := 'Erro';
   Start    := '0000';
   Stop     := '100';
   T2de5[0] := '00110';
   T2de5[1] := '10001';
   T2de5[2] := '01001';
   T2de5[3] := '11000';
   T2de5[4] := '00101';
   T2de5[5] := '10100';
   T2de5[6] := '01100';
   T2de5[7] := '00011';
   T2de5[8] := '10010';
   T2de5[9] := '01010';

   // Digitos
   for I := 1 to length(Self.CodigoBarras) do
   begin
      if pos(Self.CodigoBarras[I],'0123456789') <> 0 then
         Codifi := Codifi + T2de5[StrToInt(Self.CodigoBarras[I])]
      else
         Exit;
   end;

   //Se houver um n�mero �mpar de d�gitos no C�digo, acrescentar um ZERO no in�cio
   if odd(length(Self.CodigoBarras)) then
      Codifi := T2de5[0] + Codifi;

   //Intercalar n�meros - O primeiro com o segundo, o terceiro com o quarto, etc...
   I := 1;
   CodigoAuxiliar := '';
   while I <= (length(Codifi) - 9)do
   begin
      CodigoAuxiliar := CodigoAuxiliar + Codifi[I] + Codifi[I+5] + Codifi[I+1] + Codifi[I+6] + Codifi[I+2] + Codifi[I+7] + Codifi[I+3] + Codifi[I+8] + Codifi[I+4] + Codifi[I+9];
      I := I + 10;
   end;

   // Acrescentar caracteres Start e Stop
   Result := Start + CodigoAuxiliar + Stop;
end;



function TgbCobCodBar.GetImagem : TImage;
const
   CorBarra           = clBlack;
   CorEspaco          = clWhite;
   LarguraBarraFina   = 1;
   LarguraBarraGrossa = 3;
   AlturaBarra        = 50;
var
   X            : integer;
   Col          : integer;
   Lar          : integer;
   CodigoAuxiliar : string;
begin

   CodigoAuxiliar := Define2de5;
   Result := TImage.Create(nil);
   Result.Height := AlturaBarra;
   Result.Width := 0;
   For X := 1 to Length(CodigoAuxiliar) do
      case CodigoAuxiliar[X] of
         '0' : Result.Width := Result.Width + LarguraBarraFina;
         '1' : Result.Width := Result.Width + LarguraBarraGrossa;
      end;

   Col    := 0;

   if CodigoAuxiliar <> 'Erro' then
   begin
      for X := 1 to length(CodigoAuxiliar) do
      begin
         //Desenha barra
         with Result.Canvas do
         begin
            if Odd(X) then
               Pen.Color := CorBarra
            else
               Pen.Color := CorEspaco;

            if CodigoAuxiliar[X] = '0' then
            begin
               for Lar := 1 to LarguraBarraFina do
               begin
                  MoveTo(Col,0);
                  LineTo(Col,AlturaBarra);
                  Col := Col + 1;
               end;
            end
            else
            begin
               for Lar := 1 to LarguraBarraGrossa do
               begin
                  MoveTo(Col,0);
                  LineTo(Col,AlturaBarra);
                  Col := Col + 1;
               end;
            end;
         end;
      end;
   end
   else
      Result.Canvas.TextOut(0,0,'Erro');
end;

{ TObjBoletoSemRegistro }

constructor TObjBoletoSemRegistro.create;
begin
     //Criacoes
     Instrucoes                 :=TStringList.create;
     Self.zerarcampos;
end;

destructor TObjBoletoSemRegistro.Free;
begin
     Freeandnil(Self.Instrucoes);
end;

procedure TObjBoletoSemRegistro.ZerarCampos;
Begin
      //limpando variaveis
     numeroconvenio             :='';
     NomeBanco                  :='';
     CodigoBAnco                :='';
     DgBanco                    :='';
     LocalPagamento             :='';
     Vencimento:=0;
     Agencia                    :='';
     DGAgencia                  :='';
     ContaCorrente              :='';
     DgContaCorrente            :='';
     NossoNumeroGerado          :='';
     DGNossoNUmero              :='';
     DataDoc                    :='';
     NumeroDoc                  :='';
     EspecieDoc                 :='DM';
     Aceite                     :='N';
     DataProc                   :='';
     NumContaResponsavel        :='';
     Carteira                   :='';
     compcarteira               :='';
     EspecieMoeda               :='R$';
     QuantidadeMOeda            :='0,00';
     ValorMoeda                 :='';
     ValorDoc                   :='0,00';
     Desconto                   :='';
     OutrasDeducoes             :='';
     Juros                      :='';
     Multa                      :='';
     outrosacrescimos           :='';
     ValorCobrado               :='';
     Instrucoes.Clear;
     Nomecedente                :='';
     NomeSacado                 :='';
     numerocasasacado           :='';
     EnderecoSacado             :='';
     BairroSacado               :='';
     CepSacado                  :='';
     CidadeSacado               :='';
     EstadoSacado               :='';
     CpfSacado                  :='';
     RgSacado                   :='';
     linhadigitavel             :='';
     NossoNumero_partedois:='';
End;

Function TObjBoletoSemRegistro.GeraLinhaDigitavel:Boolean;
var
temp:string;
begin
      Result:=False;
{****Usado para gerar a linha digital dos boletos do BB
******Carteira 18 com 7 posicoes no convenio

linha digit�vel (Elite)
00198.13740   90001.709030   00005.010186   5   31600000000351
nosso n�mero (elite)
81374900017-4


    09. Linha digitavel. Composicao dos campos..

        f) editar os tres primeiros campos com um ponto;
        g) entre cada campo deverah haver espaco equivalente a 01
           caracter;
        h) a disposicao dos dados na linha digitavel nao se
           apresenta na mesma ordem encontrada no codigo de barras;
        i) os digitos verificadores referentes aos PRIMEIRO, SEGUNDO e
           TERCEIRO campo nao sao representados no codigo de barras.
}


     Self.linhadigitavel:='';

     {PRIMEIRO CAMPO - composto pelo codigo do Banco / posicoes 1 a
     3 do codigo de barras/, codigo da moeda /posicao 4 do codigo
     de barras/, as cinco primeiras posicoes do campo livre /posi     -
     coes 20 a 24 do codigo de barras/ e digito verificador deste
     campo;
     OBS: editar com ponto, cfe. Exemplo - 00192.34566}
     //********************************************
     //BANCO+MOEDA+0.+00000+DV
     //antes era assim-> Self.linhadigitavel:='00190.00009 ';
     temp:=
     Self.CodigoBarras[1]+
     Self.CodigoBarras[2]+
     Self.CodigoBarras[3]+
     Self.CodigoBarras[4]+
     Self.CodigoBarras[20]+'.'+
     Self.CodigoBarras[21]+
     Self.CodigoBarras[22]+
     Self.CodigoBarras[23]+
     Self.CodigoBarras[24];
     Self.linhadigitavel:=temp+inttostr(Self.Modulo10(Self.comeponto(temp)))+' ';
     //*********************************************

     {b) SEGUNDO CAMPO - composto pelas posicoes 6 /sexta/ a 15 (decima
      quinta) do campo livre /posicoes 25 a 34 do codigo de barras e
      digito verificador deste campo;
      OBS: editar com ponto, cfe. exemplo - 52800.783459}
     //**************************************************
     //0+4posi numeroconvenio+.+3posiconv+2campolivreNN+dv
     //Self.linhadigitavel:=Self.linhadigitavel+'0'+copy(Self.numeroconvenio,1,4)+'.'+copy(Self.numeroconvenio,5,3)+copy(Self.campolivreNN,1,2)+
     //inttostr(Self.Modulo10(copy(Self.numeroconvenio,1,4)+copy(Self.numeroconvenio,5,3)+copy(Self.campolivreNN,1,2)))+' ';
     temp:=Self.CodigoBarras[25]+
     Self.CodigoBarras[26]+
     Self.CodigoBarras[27]+
     Self.CodigoBarras[28]+
     Self.CodigoBarras[29]+'.'+
     Self.CodigoBarras[30]+
     Self.CodigoBarras[31]+
     Self.CodigoBarras[32]+
     Self.CodigoBarras[33]+
     Self.CodigoBarras[34];

     Self.linhadigitavel:=Self.LinhaDigitavel+
     temp+Inttostr(Self.Modulo10(Self.Comeponto(temp)))+' ';

     //**************************************************

     {
     c) TERCEIRO CAMPO - composto pelas posicoes 16 /decima sexta/ a
     25 /vigesima quinta/ do campo livre /posicoes 35 a 44 do codi
     go de barras e digito verificador deste campo;
     OBS: editar com ponto. cfe. exemplo - 97856.895335
     00000.004-2

     //Self.linhadigitavel:=Self.linhadigitavel+copy(Self.campolivreNN,3,5)+'.'+copy(Self.campolivreNN,8,3)+Self.Carteira+
     //                    inttostr(Self.modulo10(copy(Self.campolivreNN,3,8)+Self.Carteira))+' ';

     }
     //**************************************************
     //8 digitos do campo livre+cart
     temp:=
     Self.CodigoBarras[35]+
     Self.CodigoBarras[36]+
     Self.CodigoBarras[37]+
     Self.CodigoBarras[38]+
     Self.CodigoBarras[39]+
     '.'+
     Self.CodigoBarras[40]+
     Self.CodigoBarras[41]+
     Self.CodigoBarras[42]+
     Self.CodigoBarras[43]+
     Self.CodigoBarras[44];

     Self.linhadigitavel:=Self.LinhaDigitavel+temp+
     inttostr(Self.Modulo10(Self.Comeponto(temp)))
     +' ';
     //**************************************************

     {d) QUARTO CAMPO - Digito Verificador geral do Codigo de Barras
           (posicao 5 do codigo de barras);

           OBS: observar o metodo de calculo de DV modulo 11 especifico
           para Codigo de Barras, previsto do item 11;
     }

     Self.linhadigitavel:=Self.linhadigitavel+' '+Self.CodigoBarras[5]+' ';
     //**************************************************

     {e) QUINTO CAMPO - composto pelo fator de vencimento /posicoes 6 a
      9 do codigo de barras/ e pelo valor nominal do documento
      (posicoes 10 a 19 do codigo de barras), com a indicacao de
      zeros entre eles ateh compor as 14 posicoes do campo e sem
      edicao /sem ponto ou virgula/. Quando se tratar de bloqueto sem
      discriminacao de valor no codigo de barras a representacao deve
      ser com zeros;}

      temp:=
      Self.CodigoBarras[6]+
      Self.CodigoBarras[7]+
      Self.CodigoBarras[8]+
      Self.CodigoBarras[9]+
      Self.CodigoBarras[10]+
      Self.CodigoBarras[11]+
      Self.CodigoBarras[12]+
      Self.CodigoBarras[13]+
      Self.CodigoBarras[14]+
      Self.CodigoBarras[15]+
      Self.CodigoBarras[16]+
      Self.CodigoBarras[17]+
      Self.CodigoBarras[18]+
      Self.CodigoBarras[19];
      Self.linhadigitavel:=Self.linhadigitavel+temp;
      Result:=true;
end;

Function TObjBoletoSemRegistro.GeranossonumeroBB:Boolean;
begin
      Result:=False;
     //Composicao do nosso numero
      Self.NossoNumeroGerado:=Self.numeroconvenio+Self.NossoNumero_partedois;
      Self.DGNossoNUmero:=Self.modulo11_9(Self.numeroconvenio+Self.NossoNumero_partedois);
      result:=True;

end;


procedure TObjBoletoSemRegistro.ImprimeBoleto(PPreview:Boolean);
Begin
     Self.ImprimeBoleto(Ppreview,1);//imprime modelo convencional
End;

procedure TObjBoletoSemRegistro.ImprimeBoleto(PPreview:Boolean;Modelo:Integer);
var
DesenhoCodBar:TgbCobCodBar;
begin

   //De Acordo com o banco ele Gera o Nosso NUmero
   //o codigo de barras e a linha digitavel

   if (CodigoBanco='001')//brasil
   Then begin
             if (Self.GeranossonumeroBB=False)
             Then exit;

             if (Self.GeraCodigoBarrasBB=False)
             Then exit;

   End;

   if (CodigoBanco='237')//bradesco
   Then begin
             if (Self.GeranossonumeroBradesco=False)
             Then exit;

             if (Self.GeraCodigoBarrasBradesco=False)
             Then exit;

   End;

   if (CodigoBanco='104')//caixa economica
   Then begin
             if (Self.GeranossonumeroCaixaEconomica=False)
             Then exit;

             if (Self.GeraCodigoBarrasCaixaEconomica=False)
             Then exit;
   End;

   if (CodigoBanco='409')//unibanco
   Then begin
             if (Self.GeranossonumeroUnibanco=False)
             Then exit;

             if (Self.GeraCodigoBarrasUnibanco=False)
             Then exit;
   End;

   if (CodigoBanco='748')//sicred
   Then begin
             if (Self.GeranossonumeroSicred=False)
             Then exit;

             if (Self.GeraCodigoBarrasSicred=False)
             Then exit;
   End;

   if (CodigoBanco='399')//HSBC
   Then begin
             if (Self.GeranossonumeroHSBC=False)
             Then exit;

             if (Self.GeraCodigoBarrasHSBC=False)
             Then exit;
   End;


   //A linha digitavel � padrao, ela pega sempre as mesmas posicoes do c�digo de barras
   //independente do banco
   if (Self.GeraLinhaDigitavel=False)
   Then exit;


   if (Modelo=1)
   Then Begin
             with FRelBoletoSemregistro do
             begin
                ReportTitle := Self.NumeroDoc;

                //Primeira via do boleto
                txtNomeBanco.Caption :=Self.NomeBanco;
                txtCodigoBanco.Caption :=Self.CodigoBAnco+'-'+Self.DgBanco;
                txtLocalPagamento.Caption :=Self.LocalPagamento;
                txtDataVencimento.Caption :=datetostr(Self.Vencimento);
                txtNomeCedente.Caption :=Self.Nomecedente;

                txtDataDocumento.Caption :=Self.DataDoc;
                txtNumeroDocumento.Caption := Self.NumeroDoc;
                txtEspecieDocumento.Caption :=Self.EspecieDoc;
                txtAceite.Caption :=Self.Aceite;
                txtDataProcessamento.Caption :=Self.DataProc;

                if (CodigoBanco='748') then//sicred
                begin
                  txtNossoNumero.Caption :=
                    copy(Self.NossoNumeroGerado, 1, 2) + '/'+
                    copy(Self.NossoNumeroGerado, 3, length(self.NossoNumeroGerado) - 2) +
                    '-' + Self.DGNossoNUmero;

                  txtAgenciaCodigoCedente.Caption :=Self.Agencia+'.'+Self.DGAgencia+'.'+Self.CompletaPalavra_Esquerda(Self.ContaCorrente,5,'0');
                end
                else begin
                  txtNossoNumero.Caption :=Self.NossoNumeroGerado+'-'+Self.DGNossoNUmero;
                  //to usando agencia e conta corrente, pois tem banco que utiliza conta corrente
                  //e outros que utilizam o codigo do cedente
                  txtAgenciaCodigoCedente.Caption :=Self.Agencia+'-'+Self.DGAgencia+'/'+Self.CompletaPalavra_Esquerda(Self.ContaCorrente,12,'0')+'-'+Self.DgContaCorrente;
                end;




                txtUsoBanco.Caption := '';

                txtCarteira.Caption := Self.Carteira;
                if (Self.compcarteira<>'')
                Then txtCarteira.Caption := txtCarteira.Caption+'-'+Self.compcarteira;

                txtEspecieMoeda.Caption := Self.EspecieMoeda;
                txtQuantidadeMoeda.Caption :=Self.Quantidademoeda;
                txtValorMoeda.Caption :=Self.ValorMoeda;
                txtValorDocumento.Caption :=FormatCurr('#,##0.00',strtofloat(Self.ValorDoc));
                txtInstrucoes.Lines.Clear;
                txtInstrucoes.Lines.AddStrings(Self.Instrucoes);
                txtValorDescontoAbatimento.Caption :=Self.Desconto;
                txtValorDescontoAbatimentoB.Caption :=Self.OutrasDeducoes;
                txtValorMoraMulta.Caption :=Self.Juros;
                txtValorMoraMultaB.Caption :=Self.outrosacrescimos;
                txtValorCobrado.Caption :=Self.ValorCobrado;
                txtSacadoNome.Caption := AnsiUpperCase(Self.NomeSacado);
          
                if (Self.FisicajuridicaSacado='F')
                Then txtSacadoCPFCGC.Caption := 'CPF: ' + Self.CpfSacado
                else
                    if (Self.FisicajuridicaSacado='J')
                    Then txtSacadoCPFCGC.Caption := 'CNPJ: ' + Self.CpfSacado
                    Else txtSacadoCPFCGC.Caption :=Self.CpfSacado;
          
                if (Self.numerocasasacado<>'')
                then txtSacadoRuaNumeroComplemento.Caption :=Self.EnderecoSacado+', '+Self.numerocasasacado
                Else txtSacadoRuaNumeroComplemento.Caption :=Self.EnderecoSacado;
          
          
                txtSacadoCEPBairroCidadeEstado.Caption := Self.CepSacado+ '    ' + Self.BairroSacado+ '    ' + Self.CidadeSacado+ '    ' + Self.EstadoSacado;
                txtCodigoBaixa.Caption :='';
          
                //Segunda via do boleto
                txtNomeBanco2.Caption                   :=txtNomeBanco.Caption                    ;                   
                txtCodigoBanco2.Caption                 :=txtCodigoBanco.Caption                  ;
                txtLocalPagamento2.Caption              :=txtLocalPagamento.Caption               ;
                txtDataVencimento2.Caption              :=txtDataVencimento.Caption               ;
                txtNomeCedente2.Caption                 :=txtNomeCedente.Caption                  ;
                txtAgenciaCodigoCedente2.Caption        :=txtAgenciaCodigoCedente.Caption         ;
                txtDataDocumento2.Caption               :=txtDataDocumento.Caption                ;
                txtNumeroDocumento2.Caption             :=txtNumeroDocumento.Caption              ;
                txtEspecieDocumento2.Caption            :=txtEspecieDocumento.Caption             ;
                txtAceite2.Caption                      :=txtAceite.Caption                       ;
                txtDataProcessamento2.Caption           :=txtDataProcessamento.Caption            ;
                txtNossoNumero2.Caption                 :=txtNossoNumero.Caption                  ;
                txtUsoBanco2.Caption                    :=txtUsoBanco.Caption                     ;
                txtCarteira2.Caption                    :=txtCarteira.Caption                     ;
                txtEspecieMoeda2.Caption                :=txtEspecieMoeda.Caption                 ;
                txtQuantidadeMoeda2.Caption             :=txtQuantidadeMoeda.Caption              ;
                txtValorMoeda2.Caption                  :=txtValorMoeda.Caption                   ;
                txtValorDocumento2.Caption              :=txtValorDocumento.Caption               ;
                txtInstrucoes2.Lines                    :=txtInstrucoes.Lines                     ;
                txtValorDescontoAbatimento2.Caption     :=txtValorDescontoAbatimento.Caption      ;
                txtValorDescontoAbatimentoB2.Caption    :=txtValorDescontoAbatimentoB.Caption     ;
                txtValorMoraMulta2.Caption              :=txtValorMoraMulta.Caption               ;
                txtValorMoraMultaB2.Caption             :=txtValorMoraMultaB.Caption              ;
                txtValorCobrado2.Caption                :=txtValorCobrado.Caption                 ;
                txtSacadoNome2.Caption                  :=txtSacadoNome.Caption                   ;
                txtSacadoCPFCGC2.Caption                :=txtSacadoCPFCGC.Caption                 ;
                txtSacadoCPFCGC2.Caption                :=txtSacadoCPFCGC.Caption                 ;
                txtSacadoCPFCGC2.Caption                :=txtSacadoCPFCGC.Caption                 ;
                txtSacadoRuaNumeroComplemento2.Caption  :=txtSacadoRuaNumeroComplemento.Caption   ;
                txtSacadoCEPBairroCidadeEstado2.Caption :=txtSacadoCEPBairroCidadeEstado.Caption  ;
                txtCodigoBaixa2.Caption                 :=txtCodigoBaixa.Caption                  ;
          
                //Terceira via do boleto
                txtNomeBanco3.Caption                   :=txtNomeBanco.Caption                    ;                   
                txtCodigoBanco3.Caption                 :=txtCodigoBanco.Caption                  ;
                txtLocalPagamento3.Caption              :=txtLocalPagamento.Caption               ;
                txtDataVencimento3.Caption              :=txtDataVencimento.Caption               ;
                txtNomeCedente3.Caption                 :=txtNomeCedente.Caption                  ;
                txtAgenciaCodigoCedente3.Caption        :=txtAgenciaCodigoCedente.Caption         ;
                txtDataDocumento3.Caption               :=txtDataDocumento.Caption                ;
                txtNumeroDocumento3.Caption             :=txtNumeroDocumento.Caption              ;
                txtEspecieDocumento3.Caption            :=txtEspecieDocumento.Caption             ;
                txtAceite3.Caption                      :=txtAceite.Caption                       ;
                txtDataProcessamento3.Caption           :=txtDataProcessamento.Caption            ;
                txtNossoNumero3.Caption                 :=txtNossoNumero.Caption                  ;
                txtUsoBanco3.Caption                    :=txtUsoBanco.Caption                     ;
                txtCarteira3.Caption                    :=txtCarteira.Caption                     ;
                txtEspecieMoeda3.Caption                :=txtEspecieMoeda.Caption                 ;
                txtQuantidadeMoeda3.Caption             :=txtQuantidadeMoeda.Caption              ;
                txtValorMoeda3.Caption                  :=txtValorMoeda.Caption                   ;
                txtValorDocumento3.Caption              :=txtValorDocumento.Caption               ;
                txtInstrucoes3.Lines                    :=txtInstrucoes.Lines                     ;
                txtValorDescontoAbatimento3.Caption     :=txtValorDescontoAbatimento.Caption      ;
                txtValorDescontoAbatimentoB3.Caption    :=txtValorDescontoAbatimentoB.Caption     ;
                txtValorMoraMulta3.Caption              :=txtValorMoraMulta.Caption               ;
                txtValorMoraMultaB3.Caption             :=txtValorMoraMultaB.Caption              ;
                txtValorCobrado3.Caption                :=txtValorCobrado.Caption                 ;
                txtSacadoNome3.Caption                  :=txtSacadoNome.Caption                   ;
                txtSacadoCPFCGC3.Caption                :=txtSacadoCPFCGC.Caption                 ;
                txtSacadoCPFCGC3.Caption                :=txtSacadoCPFCGC.Caption                 ;
                txtSacadoCPFCGC3.Caption                :=txtSacadoCPFCGC.Caption                 ;
                txtSacadoRuaNumeroComplemento3.Caption  :=txtSacadoRuaNumeroComplemento.Caption   ;
                txtSacadoCEPBairroCidadeEstado3.Caption :=txtSacadoCEPBairroCidadeEstado.Caption  ;
                txtCodigoBaixa3.Caption                 :=txtCodigoBaixa.Caption                  ;
                //*****************************************
                //imgCodigoBarras3.Picture.Assign(CodigoBarra.Imagem.Picture);
                txtLinhaDigitavel3.Caption:=Self.linhadigitavel;
                //txtCodigobarras.caption:=Self.CodigoBarras;
                //imgCodigoBarras3.Visible:=False;
          
                try
                   DesenhoCodBar:=TgbCobCodBar.create;
                Except
                      Messagedlg('Erro na tentativa de Cria��o do C�digo de Barras',mterror,[mbok],0);
                      exit;
                End;
                try
                   DesenhoCodBar.CodigoBarras:=Self.CodigoBarras;
                   imgCodigoBarras3.Picture.Assign(DesenhoCodBar.Imagem.Picture);
          
                   if (Ppreview=true)
                   then Preview
                   Else Print;

          
                Finally
                       FreeAndNil(DesenhoCodBar);
                End;

             end;
   End
   Else Begin
             with FRelBoletoSemregistroModelo2 do
             begin
                ReportTitle := Self.NumeroDoc;
                QrMemoObservacoesSuperior.lines.text:=Self.ObservacoesSuperior;

          
                //Segunda via do boleto
                txtNomeBanco2.caption :=Self.NomeBanco;
                txtCodigoBanco2.caption :=Self.CodigoBAnco+'-'+Self.DgBanco;
                txtLocalPagamento2.caption :=Self.LocalPagamento;
                txtDataVencimento2.caption :=datetostr(Self.Vencimento);
                txtNomeCedente2.caption :=Self.Nomecedente;
                //to usando agencia e conta corrente, pois tem banco que utiliza conta corrente
                //e outros que utilizam o codigo do cedente
                txtAgenciaCodigoCedente2.caption :=Self.Agencia+'-'+Self.DGAgencia+'/'+Self.CompletaPalavra_Esquerda(Self.ContaCorrente,12,'0')+'-'+Self.DgContaCorrente;
          
                txtDataDocumento2.caption :=Self.DataDoc;
                txtNumeroDocumento2.caption := Self.NumeroDoc;
                txtEspecieDocumento2.caption :=Self.EspecieDoc;
                txtAceite2.caption :=Self.Aceite;
                txtDataProcessamento2.caption :=Self.DataProc;
                txtNossoNumero2.caption :=Self.NossoNumeroGerado+'-'+Self.DGNossoNUmero;
                txtUsoBanco2.caption := '';
          
                txtCarteira2.caption := Self.Carteira;
                if (Self.compcarteira<>'')
                Then txtCarteira2.caption := txtCarteira2.caption+'-'+Self.compcarteira;
          
                txtEspecieMoeda2.caption := Self.EspecieMoeda;
                txtQuantidadeMoeda2.caption :=Self.Quantidademoeda;
                txtValorMoeda2.caption :=Self.ValorMoeda;
                txtValorDocumento2.caption :=FormatCurr('#,##0.00',strtofloat(Self.ValorDoc));
                txtInstrucoes2.Lines.Clear;
                txtInstrucoes2.Lines.AddStrings(Self.Instrucoes);
                txtValorDescontoAbatimento2.caption :=Self.Desconto;
                txtValorDescontoAbatimentoB2.caption :=Self.OutrasDeducoes;
                txtValorMoraMulta2.caption :=Self.Juros;
                txtValorMoraMultaB2.caption :=Self.outrosacrescimos;
                txtValorCobrado2.caption :=Self.ValorCobrado;
                txtSacadoNome2.caption := AnsiUpperCase(Self.NomeSacado);
          
                if (Self.FisicajuridicaSacado='F')
                Then txtSacadoCPFCGC2.caption := 'CPF: ' + Self.CpfSacado
                else
                    if (Self.FisicajuridicaSacado='J')
                    Then txtSacadoCPFCGC2.caption := 'CNPJ: ' + Self.CpfSacado
                    Else txtSacadoCPFCGC2.caption :=Self.CpfSacado;
          
                if (Self.numerocasasacado<>'')
                then txtSacadoRuaNumeroComplemento2.caption :=Self.EnderecoSacado+', '+Self.numerocasasacado
                Else txtSacadoRuaNumeroComplemento2.caption :=Self.EnderecoSacado;
          
          
                txtSacadoCEPBairroCidadeEstado2.Caption := Self.CepSacado+ '    ' + Self.BairroSacado+ '    ' + Self.CidadeSacado+ '    ' + Self.EstadoSacado;
                txtCodigoBaixa2.Caption :='';
          

                //Terceira via do boleto
                txtNomeBanco3.Caption                   :=txtNomeBanco2.caption                    ;                   
                txtCodigoBanco3.Caption                 :=txtCodigoBanco2.caption                  ;
                txtLocalPagamento3.Caption              :=txtLocalPagamento2.caption               ;
                txtDataVencimento3.Caption              :=txtDataVencimento2.caption               ;
                txtNomeCedente3.Caption                 :=txtNomeCedente2.caption                  ;
                txtAgenciaCodigoCedente3.Caption        :=txtAgenciaCodigoCedente2.caption         ;
                txtDataDocumento3.Caption               :=txtDataDocumento2.caption                ;
                txtNumeroDocumento3.Caption             :=txtNumeroDocumento2.caption              ;
                txtEspecieDocumento3.Caption            :=txtEspecieDocumento2.caption             ;
                txtAceite3.Caption                      :=txtAceite2.caption                       ;
                txtDataProcessamento3.Caption           :=txtDataProcessamento2.caption            ;
                txtNossoNumero3.Caption                 :=txtNossoNumero2.caption                  ;
                txtUsoBanco3.Caption                    :=txtUsoBanco2.caption                     ;
                txtCarteira3.Caption                    :=txtCarteira2.caption                     ;
                txtEspecieMoeda3.Caption                :=txtEspecieMoeda2.caption                 ;
                txtQuantidadeMoeda3.Caption             :=txtQuantidadeMoeda2.caption              ;
                txtValorMoeda3.Caption                  :=txtValorMoeda2.caption                   ;
                txtValorDocumento3.Caption              :=txtValorDocumento2.caption               ;
                txtInstrucoes3.Lines                    :=txtInstrucoes2.Lines                     ;
                txtValorDescontoAbatimento3.Caption     :=txtValorDescontoAbatimento2.caption      ;
                txtValorDescontoAbatimentoB3.Caption    :=txtValorDescontoAbatimentoB2.caption     ;
                txtValorMoraMulta3.Caption              :=txtValorMoraMulta2.caption               ;
                txtValorMoraMultaB3.Caption             :=txtValorMoraMultaB2.caption              ;
                txtValorCobrado3.Caption                :=txtValorCobrado2.caption                 ;
                txtSacadoNome3.Caption                  :=txtSacadoNome2.caption                   ;
                txtSacadoCPFCGC3.Caption                :=txtSacadoCPFCGC2.caption                 ;
                txtSacadoCPFCGC3.Caption                :=txtSacadoCPFCGC2.caption                 ;
                txtSacadoCPFCGC3.Caption                :=txtSacadoCPFCGC2.caption                 ;
                txtSacadoRuaNumeroComplemento3.Caption  :=txtSacadoRuaNumeroComplemento2.caption   ;
                txtSacadoCEPBairroCidadeEstado3.Caption :=txtSacadoCEPBairroCidadeEstado2.caption  ;
                txtCodigoBaixa3.Caption                 :=txtCodigoBaixa2.caption                  ;
                //*****************************************
                //imgCodigoBarras3.Picture.Assign(CodigoBarra.Imagem.Picture);
                txtLinhaDigitavel3.Caption:=Self.linhadigitavel;
                //txtCodigobarras.caption:=Self.CodigoBarras;
                //imgCodigoBarras3.Visible:=False;
          
                try
                   DesenhoCodBar:=TgbCobCodBar.create;
                Except
                      Messagedlg('Erro na tentativa de Cria��o do C�digo de Barras',mterror,[mbok],0);
                      exit;
                End;
                try
                   DesenhoCodBar.CodigoBarras:=Self.CodigoBarras;
                   imgCodigoBarras3.Picture.Assign(DesenhoCodBar.Imagem.Picture);
          
                   if (Ppreview=true)
                   then Preview
                   Else Print;

          
                Finally
                       FreeAndNil(DesenhoCodBar);
                End;

             end;



   End;
end;

function TObjBoletoSemRegistro.Modulo10(PValor: String) : Integer;
var
   Auxiliar : integer;
   doisdigitos:String[2];
   Contador, Peso : integer;
   Digito : integer;
begin

   //multiplico do fim pro inicio por 2 e 1 respectivamente
   //se a multiplicacao for maior q 9 somo os digitos do resultado
   //exemplo 2*9=18-> 1+ 8 = 9
   //somo todos os resultados

   //deduzo o resultado da diferenca do proximoi valor multiplo de 10
   //exemplo  se deu 25  deduzo de 30
   //30 - 25 = 5(dv)
   //se fosse 30-20=10 entao o digito � 0


   Auxiliar := 0;
   Peso := 2;
   for Contador:=length(pvalor) downto 1 do
   Begin
        if ((StrToInt(PValor[Contador]) * Peso)>9)
        Then Begin
                  //maior q nove somo os dois digitos
                  //o maximo q pode dar � 2*8=18
                  //entao o maior valor vai  ser 1+8 = "9"
                  doisdigitos:=inttostr((StrToInt(PValor[Contador]) * Peso));
                  auxiliar:=auxiliar+strtoint(doisdigitos[1])+strtoint(doisdigitos[2]);
        End
        Else Auxiliar :=Auxiliar+(StrToInt(PValor[Contador])*Peso);

        if Peso = 1
        then Peso := 2
        else Peso := 1;
   end;

   //se deu 10,20,30 .... entao o DV � 0

   if ((Auxiliar mod 10)=0)
   Then Result:=0
   Else Begin
             //procurando o proximo multiplo de 10
             for Contador:=1 to 10 do
             begin
                  if (((auxiliar+contador) mod 10)=0)
                  Then Begin
                            //encontrou entao o dv � ele - o resultado
                            result:=(auxiliar+contador)-auxiliar;
                            break;
                  End;
             End;
   End;

end;

function TObjBoletoSemRegistro.Modulo11_9(PValor: String): string;
var
   Soma : integer;
   Contador, Peso, Digito : integer;
begin
   Soma := 0;
   Peso := 2;

   //***Modificado por mim, pelo que entendi a multiplicacao
   //nao pode comecar pelo 2 e sim pela base que � 9
   //ficaria algo assim
   // 1 2  5  8 9 7 6 3
   // 9 2 3 4 5 6 7 8 9 -> comeca no nove da dir p/ esq

   peso:=9;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
      Soma := Soma + (StrToInt(PValor[Contador]) * Peso);
      if Peso >2
      then Peso := Peso-1
      else Peso := 9;
   end;

   Result := IntToStr(Soma mod 11);
End;


function TObjBoletoSemRegistro.Modulo11_nosso_numero_HSBC(PValor: String): string;
var
   Soma : integer;
   Contador, Peso, Digito : integer;
begin
   Soma := 0;
   Peso := 9;


   (*Soma da direita pra esquerda de 9 a 2
   divido por 11
   o Resto � o resultado
   porem se o resto for maior que 9
   retorno 0*)
   

   peso:=9;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
      Soma := Soma + (StrToInt(PValor[Contador]) * Peso);

      if Peso =2
      then Peso := 9
      else Peso := peso-1;
   end;

   if ((Soma mod 11)>9)
   Then result:='0'
   Else Result := IntToStr(Soma mod 11);

   
End;

function TObjBoletoSemRegistro.GeraDigitoVerificadornossoNumeroBradesco(PValor: String): string;
var
   Soma : integer;
   Contador, Peso, Digito,resultado : integer;
begin
   Soma := 0;
   Peso := 2;
   //Multiplica do 2 ate o 7 ao contrario

   peso:=2;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
       Soma := Soma + (StrToInt(PValor[Contador]) * Peso);
       if Peso =7
       then Peso:=2
       else Peso:=peso+1;
   end;

   Resultado:= Soma mod 11;

   //Obs.: Se o resto da divis�o for "1", desprezar a diferen�a entre o dividendo
   //menos o resto que ser� "10" e considerar o d�gito como "P".
   if (resultado=1)
   Then Begin
             result:='P';
             exit;
   End;

   //Obs.:   Se o resto da divis�o for "0", desprezar o c�lculo de subtra��o
   //entre dividendo e resto, e considerar o "0" como d�gito.
   if(resultado=0)
   Then Begin
            result:='0';
            exit;
   End;

   Digito:=11-Resultado;

   Result:=Inttostr(digito);
End;

function TObjBoletoSemRegistro.Modulo11_DVG(PValor: String): string;
var
   Soma : integer;
   Contador, Peso, Digito : integer;
begin
   peso:=2;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
        Soma := Soma + (StrToInt(PValor[Contador]) * Peso);
        if Peso =9
        then Peso :=2
        else Peso :=peso+1;
   End;
      

   peso:=Soma mod 11;//pega o resto

   Digito:=11-peso;

   if (Digito=0) or (Digito=1) or (Digito>9)
   Then result:='1'
   Else result:=inttostr(Digito);//abaixo disso o proprio resultado

End;

function TObjBoletoSemRegistro.Retiracaractere(Pvalor: String;Pcaractere:char): string;
var
temp:string;
cont:integer;
begin
     temp:='';
     for cont:=1 to length(pvalor) do
     Begin
          if (pvalor[cont]<>Pcaractere)
          Then temp:=temp+pvalor[cont];
     End;
     result:=temp;
end;


function TObjBoletoSemRegistro.CalculaFatorVencimento(PVencimento:Tdatetime): String;
var
temp:integer;
begin
{05. Metodologia de calculo para o fator de vencimento:
 a) data base de 07.10.1997, calculando o numero de dias entre essa
    data e a do vencimento  - data de vencimento menos data base e
    igual a fator.

                       EMISSAO
                 VENCIMENTO - 04-07-2000
                 DATA BASE  - 07-10-1997
                 FATOR DE VENCIMENTO 1001

 b) tabela de correlacao data X fator, iniciando pelo fator
    1000,que corresponde a data de vencimento 03.07.2000, e assim
    sucessiva mente:

 FATOR                               VENCIMENTO

 1000................................03/07/2000
 1002................................05/07/2000
 1667................................01/05/2002
 4758................................17/10/2010
 9999................................21/02/2025

Exemplo:

99997.77213 // 30530.150082 // 18975.000003 // 3 // 10010000035000
-------------------------------------------------------------------     -
  1o campo  //  2o campo    //  3o campo   // dv // Fator venc/valor

  c) Quando a primeira posicao do campo "fator de vencimento/valor"
     for zero, significara que esse bloqueto nao contera o fator de
     vencimento no codigo de barras/linha digitavel, tratando-se
     portanto as 14 (catorze) posicoes como valor.
}


  temp:=DaysBetween(Pvencimento,strtodate('07/10/1997'));
  result:=inttostr(temp);
  
end;

Function TObjBoletoSemRegistro.CompletaPalavra_Esquerda(palavra:string;quantidade:Integer;ValorASerUSado:String):String;
var
apoio:String;
Begin

     apoio:='';
     If (length(palavra)>=quantidade)
     Then Begin
                apoio:=copy(palavra,1,quantidade);
                result:=apoio;
                exit;
     End;

     apoio:='';
     While ((length(apoio)+length(palavra))<quantidade) do
     apoio:=apoio+Valoraserusado;
     result:=apoio+palavra;
End;

Function TObjBoletoSemRegistro.formata_valor(Pvalor:string):string;
var
decimal:Currency;
begin
    result:='';
    
    If (PValor='')
    Then exit;
    
    Pvalor:=Self.Retiracaractere(Pvalor,'.');
    try
       decimal:=strtofloat(Pvalor);
       result:=formatfloat('###,##0.00',decimal);
    except
          messagedlg('Valor Inv�lido!',mterror,[mbok],0);
       exit;
    end;

end;



Function TObjBoletoSemRegistro.GeraCodigoBarrasBB:Boolean;
var
temp:string;
begin
     result:=False;
     {
     04. LEIAUTE DO C�DIGO DE BARRAS                                       
     ...............................................................
     N.    POSI��ES     PICTURE     USAGE        CONTE�O              
     ...............................................................  
     01    001 a 003    9/003/      Display      001                  
     02    004 a 004    9/001/      Display      9 /Real/             
(a)  03    005 a 005    9/001/      Display      DV /*/               
(b)  04    006 a 009    9/004/      Display      fator de vencimento  
     05    010 a 019    9/008/v99   Display      Valor                
     06    020 a 044    9/025/      Display      CAMPO LIVRE          
     ...............................................................  
                                                                      
    a) o d�gito verificador da 5 (quinta) posi��o � calculado com base
       no m�dulo 11 espec�fico previsto no item 12;                   
    b) o fator de vencimento � calculado com base na metodologia      
       descrita no item 9.                                            
                                                                      
21156001740/3
05. Leiaute e descri��o do CAMPO LIVRE do c�digo de barras para
    emiss�o de bloquetos com nosso-n�mero de 11 posi��es - EXCLUSIVO
    PARA CONV�NIOS DE SEIS POSI��ES.

       .............................................................
         N.    POSI��ES  PICTURE   USAGE      CONTE�DO
       .............................................................
 (a)     01   020 a 030  9/011/    Display    Nosso-n�mero, sem DV
         02   031 a 034  9/004/    Display    Ag�ncia, sem DV
         03   035 a 042  9/008/    Display    C�digo cedente, Sem DV
         04   043 a 044  9/002/    Display    Carteira

    a) Observar as regras contidas no item 14 para a composi��o do
       nosso-n�mero;
       OBS: Os d�gitos verificadores - dv -  dos campos ag�ncia,
            c�digo   do cedente e nosso-n�mero, calculados com base no
            m�dulo 11   previsto no item 11, s�o impressos nos
            bloquetos e n�o devem estar representados no c�digo de
            barras e linha digit�vel.
    b) Este leiaute tamb�m � v�lido para os bloquetos emitidos na
       Carteira 12 - Tipos de Conv�nio 2, 3, 4 ou 5.


06. Leiaute e descri��o do CAMPO LIVRE do c�digo de barras para
    emiss�o de bloquetos com nosso-n�mero de 17 posi��es - EXCLUSIVO
    PARA AS CARTEIRAS 16 E 18, VINCULADAS � CONV�NIOS COM SEIS        
    POSI��ES.                                                         
                                                                      
    ......................................................            
    N.       POSICOES    PICTURE  USAGE   CONTEUDO                    
    ......................................................            
    01       020 a 025   9/006/   Display N�mero conv�nio             
(a) 02       026 a 042   9/017/   Display Nosso-n�mero                
    03       043 a 044   9/002/   Display 21 /servi�o/                
                                                                      
    a) Observar as regras contidas no item 14 para a composi��o do    
       nosso-n�mero;                                                  
    b) Informar zeros a esquerda se o nosso-n�mero for inferior a  17 
       posi��es.                                                      
                                                                      
                                                                      
07. Leiaute e descri��o do CAMPO LIVRE do c�digo de barras para       
    emiss�o de bloquetos na Carteira 17 e 18 - VINCULADOS � CONV�NIOS 
    COM NUMERA��O SUPERIOR A 1.000.000 (um milh�o).                   

    .............................................................
    N.       POSICOES    PICTURE  USAGE   CONTEUDO                    
    .............................................................     
    01       020 a 025   9/006/   Display  Zeros                      
(a) 02       026 a 042   9/017/   Display  Nosso-N�mero, sem DV       
             026 a 032   9/007/   Display  N�mero do conv�nio         
             033 a 042   9/010/   Display  Complemento do             
                                           Nosso-N�mero, sem DV       
    03       043 a 044   9/002/   Display  Carteira/Modalidade de     
                                           Cobran�a.                  

    a) Observar as regras contidas no item 14 para a composi��o do    
       nosso-n�mero.                                                  
       OBS: Os d�gitos verificadores - dv -  dos campos ag�ncia,      
            c�digo   do cedente e nosso-n�mero, calculados com base no
            modulo 11   previsto no item 11, s�o impressos nos        
            bloquetos e n�o devem estar representados no c�digo de    
            barras e linha digit�vel.                                 
    b) Este leiaute tamb�m � v�lido para os bloquetos emitidos na     
       Carteira 12 - Tipos de Conv�nio 2, 3, 4 ou 5.                  

     }

     Self.CodigoBarras:='';
     //banco+moeda+dv+fatorvencimento
     //deixo sem o DV calculo no final

     Self.CodigoBarras:='0019'+Self.CalculaFatorVencimento(Self.Vencimento);
     //+valor+campolivre
     temp:=Self.formata_valor(Self.ValorDoc);
     temp:=Retiracaractere(temp,'.');
     temp:=Retiracaractere(temp,',');
     TEMP:=CompletaPalavra_Esquerda(temp,10,'0');
     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //**************************campo livre*******************************************
     //Self.CodigoBarras:=Self.CodigoBarras+'000000'+Self.NossoNumeroGerado+Self.Carteira;

     if (Self.QtdeDigitosNossoNumero=11)
     Then Begin
            {05. Leiaute e descri��o do CAMPO LIVRE do c�digo de barras para       
                 emiss�o de bloquetos com nosso-n�mero de 11 posi��es - EXCLUSIVO
                 PARA CONV�NIOS DE SEIS POSI��ES.

                    .............................................................
                      N.    POSI��ES  PICTURE   USAGE      CONTE�DO
                    .............................................................
              (a)     01   020 a 030  9/011/    Display    Nosso-n�mero, sem DV
                      02   031 a 034  9/004/    Display    Ag�ncia, sem DV
                      03   035 a 042  9/008/    Display    C�digo cedente, Sem DV
                      04   043 a 044  9/002/    Display    Carteira

              a) Observar as regras contidas no item 14 para a composi��o do
                 nosso-n�mero;
                 OBS: Os d�gitos verificadores - dv -  dos campos ag�ncia,
                      c�digo   do cedente e nosso-n�mero, calculados com base no
                      m�dulo 11   previsto no item 11, s�o impressos nos
                      bloquetos e n�o devem estar representados no c�digo de
                      barras e linha digit�vel.                                 
              b) Este leiaute tamb�m � v�lido para os bloquetos emitidos na     
                    Carteira 12 - Tipos de Conv�nio 2, 3, 4 ou 5.}                 
             
            if (Length(Self.NossoNumeroGerado)<>11)
            Then Begin
                      Messagedlg('O Nosso n�mero deveria possuir 11 d�gitos devido ao conv�nio do Boleto',mterror,[mbok],0);
                      exit;
            End;

            if length(Self.Agencia)<>4
            Then Begin
                      Messagedlg('A Ag�ncia deve ter 4 digitos',mterror,[mbok],0);
                      exit;
            End;

            if length(Self.Codigocedente)<>8
            Then Begin
                      Messagedlg('O C�digo do Cedente deve ter 8 digitos',mterror,[mbok],0);
                      exit;
            End;

            if length(Self.Carteira)<>2
            Then Begin
                      Messagedlg('O C�digo da Carteira deve ter 2 digitos',mterror,[mbok],0);
                      exit;
            End;

            Self.CodigoBarras:=Self.CodigoBarras+Self.NossoNumeroGerado+Self.Agencia+Self.Codigocedente+Self.Carteira;
     End
     Else Begin
          if (Self.QtdeDigitosNossoNumero=17)
          Then Begin
                    //Aqui tem duas situacoes, convenio com 6 ou superior a 6 (1 milhao)

                    {06. Leiaute e descri��o do CAMPO LIVRE do c�digo de barras para
                         emiss�o de bloquetos com nosso-n�mero de 17 posi��es - EXCLUSIVO
                         PARA AS CARTEIRAS 16 E 18, VINCULADAS � CONV�NIOS COM SEIS
                         POSI��ES.

                         ......................................................
                         N.       POSICOES    PICTURE  USAGE   CONTEUDO
                         ......................................................
                         01       020 a 025   9/006/   Display N�mero conv�nio
                     (a) 02       026 a 042   9/017/   Display Nosso-n�mero
                         03       043 a 044   9/002/   Display 21 /servi�o/

                         a) Observar as regras contidas no item 14 para a composi��o do
                            nosso-n�mero;
                         b) Informar zeros a esquerda se o nosso-n�mero for inferior a  17
                            posi��es.}

                     if (length(Self.numeroconvenio)=6)
                     Then Self.CodigoBarras:=Self.CodigoBarras+Self.numeroconvenio+CompletaPalavra_Esquerda(Self.NossoNumeroGerado,17,'0')+Self.Carteira;

                     {
                     07. Leiaute e descri��o do CAMPO LIVRE do c�digo de barras para
                         emiss�o de bloquetos na Carteira 17 e 18 - VINCULADOS � CONV�NIOS 
                         COM NUMERA��O SUPERIOR A 1.000.000 (um milh�o).                   
                                                                                           
                         .............................................................     
                         N.       POSICOES    PICTURE  USAGE   CONTEUDO                    
                         .............................................................     
                         01       020 a 025   9/006/   Display  Zeros                      
                     (a) 02       026 a 042   9/017/   Display  Nosso-N�mero, sem DV       
                                  026 a 032   9/007/   Display  N�mero do conv�nio         
                                  033 a 042   9/010/   Display  Complemento do             
                                                                Nosso-N�mero, sem DV
                         03       043 a 044   9/002/   Display  Carteira/Modalidade de
                                                                Cobran�a.

                         a) Observar as regras contidas no item 14 para a composi��o do
                            nosso-n�mero.
                            OBS: Os d�gitos verificadores - dv -  dos campos ag�ncia,
                                 c�digo   do cedente e nosso-n�mero, calculados com base no
                                 modulo 11   previsto no item 11, s�o impressos nos
                                 bloquetos e n�o devem estar representados no c�digo de
                                 barras e linha digit�vel.
                         b) Este leiaute tamb�m � v�lido para os bloquetos emitidos na
                            Carteira 12 - Tipos de Conv�nio 2, 3, 4 ou 5.}


                     if (length(Self.numeroconvenio)>6)
                     Then Self.CodigoBarras:=Self.CodigoBarras+'000000'+CompletaPalavra_Esquerda(Self.NossoNumeroGerado,17,'0')+Self.Carteira;
          End
          Else Begin
                    Messagedlg('O Conv�nio Banco do Brasil deve se 11 ou 17 D�gitos na Qtde de d�gitos do Nosso N�mero',mterror,[mbok],0);
                    exit;
          End;
     End;

     //*********************************************************************************
     //agora calculando o DV e colocando na quinta posicao
     temp:=Self.Modulo11_DVG(Self.CodigoBarras);
     temp:=copy(Self.CodigoBarras,1,4)+temp+copy(Self.CodigoBarras,5,39);

     Self.CodigoBarras:=Temp;
     Result:=True;
end;

function TObjBoletoSemRegistro.ComePonto(Pvalor:String): String;
var
temp:String;
cont:integer;
begin
     temp:='';
     for cont:=1 to length(Pvalor) do
     Begin
          if (Pvalor[cont]<>'.') and (Pvalor[cont]<>' ')
          Then temp:=temp+Pvalor[cont];
     End;
     result:=temp;
end;

function TObjBoletoSemRegistro.GeraCodigoBarrasBradesco: Boolean;
var
temp:string;
begin
{
O c�digo de barra para cobran�a cont�m 44 posi��es dispostas da seguinte forma:

Posi��o	    Tamanho	  Conte�do
01 a 03       3         CODIGO DO BANCO
04 a 04       1         CODIGO DA MOEDA
05 a 05       1         DIGITO VERIFICADOR DO CODIGO DE BARRAS
06 a  9       4         fator de vencimento
10 a 19       10        VALOR
20 a 44       25        CAMPO LIVRE

"	As posi��es de campo livre ficam a crit�rio de cada Banco arrecadador, sendo que o padr�o do Bradesco �:

Posi��o	    Tamanho	  Conte�do
20 a 23       4         Agencia Cedente
24 a 25       2         Carteira
26 a 36       11        Nosso Numero (sem digito)
37 a 43       7         Conta do cedente (sem digito)
44 a 44       1         Zero Fixo
}
     RESULT:=FALSE;
     //banco+moeda+dv+fatorvencimento
     //deixo sem o DV calculo no final

     Self.CodigoBarras:='2379'+Self.CalculaFatorVencimento(Self.Vencimento);
     //+valor+campolivre
     temp:=Self.formata_valor(Self.ValorDoc);
     temp:=Retiracaractere(temp,'.');
     temp:=Retiracaractere(temp,',');
     TEMP:=CompletaPalavra_Esquerda(temp,10,'0');
     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //**************************campo livre*******************************************

     if (length(Self.NossoNumero_partedois)<>11)
     Then Begin
               Messagedlg('O Nosso n�mero deveria possuir 11 d�gitos',mterror,[mbok],0);
               exit;
     End;

     if (length(Self.Agencia)<>4)
     Then Begin
               Messagedlg('A ag�ncia deveria possuir 4 d�gitos',mterror,[mbok],0);
               exit;
     End;

     if (length(Self.Carteira)<>2)
     Then Begin
               Messagedlg('A carteira deveria possuir 2 d�gitos',mterror,[mbok],0);
               exit;
     End;

     if (length(Self.ContaCorrente)<>7)
     Then Begin
               Messagedlg('A conta corrente deveria possuir 7 d�gitos',mterror,[mbok],0);
               exit;
     End;

     Self.CodigoBarras:=Self.CodigoBarras+Self.Agencia+Self.Carteira+Self.NossoNumero_partedois+Self.ContaCorrente+'0';
     //agora calculando o DV e colocando na quinta posicao
     temp:=Self.Modulo11_DVG(Self.CodigoBarras);
     temp:=copy(Self.CodigoBarras,1,4)+temp+copy(Self.CodigoBarras,5,39);
     Self.CodigoBarras:=Temp;
     Result:=True;
end;

function TObjBoletoSemRegistro.GeranossonumeroBradesco: boolean;
begin
      Result:=False;
     //Composicao do nosso numero
      Self.NossoNumeroGerado:=Self.Carteira+'/'+Self.NossoNumero_partedois;
      Self.DGNossoNUmero:=Self.GeraDigitoVerificadornossoNumeroBradesco(Self.carteira+Self.NossoNumero_partedois);
      result:=True;
end;

function TObjBoletoSemRegistro.GeraNossoNumeroSIcred: boolean;
begin
      Result:=False;
     //Composicao do nosso numero
      Self.NossoNumeroGerado := copy(Self.NossoNumero_partedois, 1, length(self.NossoNumero_partedois) - 1);
      Self.DGNossoNUmero:= copy(Self.NossoNumero_partedois, length(self.NossoNumero_partedois), 1);
      result:=True;
end;


function TObjBoletoSemRegistro.GeraCodigoBarrasSicred: Boolean;
var
DigitoVCampoLivre,temp:string;
begin
     RESULT:=FALSE;

     Self.CodigoBarras:='7489'+Self.CalculaFatorVencimento(Self.Vencimento);
     //+valor+campolivre
     temp:=Self.formata_valor(Self.ValorDoc);
     temp:=Retiracaractere(temp,'.');
     temp:=Retiracaractere(temp,',');
     TEMP:=CompletaPalavra_Esquerda(temp,10,'0');
     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //**************************campo livre*******************************************

     temp:=Self.posicao20_sicred+Self.posicao21_sicred+Self.NossoNumero_partedois+Self.posicao31_34_sicred+Self.posicao35_36_sicred+Self.posicao37_41_sicred+Self.posicao42_sicred+Self.posicao43_sicred;
     DigitoVCampoLivre:=Self.calculaDVCampoLivreSicred(temp);
     temp:=temp+DigitoVCampoLivre;

     if (length(temp)<>25)
     Then begin
                MensagemErro('O Campo Livre do C�digo de Barras do Sicred n�o cont�m 25 posi��es');
               exit;
     End;

     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //*********************************************************************
     //agora calculando o DV e colocando na quinta posicao
     temp:=Self.Modulo11_DVG(Self.CodigoBarras);
     temp:=copy(Self.CodigoBarras,1,4)+temp+copy(Self.CodigoBarras,5,39);
     Self.CodigoBarras:=Temp;
     Result:=True;
end;


function TObjBoletoSemRegistro.GeraCodigoBarrasCaixaEconomica: Boolean;
var
Temp:String;
begin
{
O c�digo de barra para cobran�a cont�m 44 posi��es dispostas da seguinte forma:

Posi��o	    Tamanho	  Conte�do
01 a 03       3         CODIGO DO BANCO
04 a 04       1         CODIGO DA MOEDA
05 a 05       1         DIGITO VERIFICADOR DO CODIGO DE BARRAS
06 a  9       4         fator de vencimento
10 a 19       10        VALOR
20 a 44       25        CAMPO LIVRE

As posi��es de campo livre ficam a crit�rio de cada Banco arrecadador, sendo que o padr�o da caixa economica �:

*********************CONFORME SISTEMA SICOB************************

IX - CAMPO LIVRE (posicoes 20 a 44)
Para as posi��es do Campo Livre, informar:


-Se carteira Sem registro: Nosso n�mero com 10 posi��es e C�digo do Cedente, ambos em o DV
Ex: 82NNNNNNNN AAAA YYY XXXXXXXX

Onde:

82-Identificador da carteira sem Registro
NNNNNNNN-Nosso numero do Cliente
AAAA-CPPJ da Agencia Cedente
XXXXXXXX-C�digo Fornecido pela Ag�ncia (ronnei->SICOB)

Se  carteira r�pida: Nosso n�mero com 10 posi��es e C�digo do Cedente, ambos sem o DV

EX: 9NNNNNNNNN AAAA YYY XXXXXXXX

Onde:

9-Identificador da carteira r�pida
NNNNNNNN-Nosso numero do Cliente
AAAA-CPPJ da Agencia Cedente
XXXXXXXX-C�digo Fornecido pela Ag�ncia (ronnei->SICOB)


//********ENCONTRADO UMA NOVA FORMA 12/02/09 - ESPACO NOTEBOOK
EspecificacaoCodBarrasCobranca_Sem_Registro_16posicoes_NN.pdf

COBRANCA SEM REGISTRO 16 POSICOES

IX - CAMPO LIVRE (posi��es 20 a 44)
Para as posi��es do Campo Livre, informar:
XXXXX AAAA C K NNNNNNNNNNNNNN
Onde:
XXXXX - C�digo do Cliente Cedente fornecido pela CAIXA
AAAA - CNPJ da Ag�ncia da Conta do Cliente Cedente
C - C�digo da Carteira = 8
K - Constante = 7

OU SEJA 11 digitos constantes


NNNNNNNNNNNNNN - Nosso N�mero do Cliente com 14 posi��es.

OBS: SEM o DV

X - C�LCULO DO DV DO NOSSO N�MERO DA COBRAN�A SEM REGISTRO - 16 POSI��ES

- Campo com 15 posi��es sempre iniciando com 8.
- Utiliza 1 d�gito verificador calculado atrav�s do m�dulo 11, com peso 2 a 9.

8NNNNNNNNNNNNNN - D1

- C�lculo do D1
8 0 1 0 0 0 9 0 1 2 0 0 2 0 0 - N�mero a calcular
��������������������AAAAAAAAA
8 7 6 5 4 3 2 9 8 7 6 5 4 3 2 - �ndice de Multiplica��o
Divis�o � 118/11 = 10 Resto = 8
Subtra��o� 11- 8 = 3
Digito = 3
- N�mero com o d�gito calculado
8 0 1 0 0 0 9 0 1 2 0 0 2 0 0 3 - N�mero com o d�gito calculado
Obs.: Se o resultado da subtra��o for maior que 9 (nove) o DV ser� 0 (zero), caso contr�rio o resultado ser� o DV.



*******NO CASO DO SINCO (OUTRO SISTEMA DA CAIXA ECONOMICA)****************


No amanda eu padronizei da seguinte maneira
se preencher o campo Codigo_SICOB � porque �
SICOB senao preencher � porque � SINCO

IX - CAMPO LIVRE (posi��es 20 a 44)
Para as posi��es do Campo Livre, informar:
1XXXXXX NNNNNNNNNNNNNNNNNN

onde
1 - Fixo
XXXXXX - C�digo do Cliente Cedente fornecido pela CAIXA
NNNNNNNNNNNNNNNNN - Nosso N�mero do Cliente com 18 posi��es

*********************SIGB********************************************
4.1.1 Detalhamento da composi��o do campo livre do C�digo de Barras para
bloqueto de Cobran�a Banc�ria CAIXA no SIGCB

POSI��O       TAMANHO PICTURE CONTE�DO
20 � 25       6       9       (6) C�digo do Cedente
26 � 26       1       9       (1) D�gito Verificador do C�digo do Cedente
27 � 29       3       9       (3) Nosso N�mero � Seq��ncia 1 (vide Nota 2)
30 � 30       1       9       (1) Constante 1 (vide Nota 2)
31 � 33       3       9       (3) Nosso N�mero � Seq��ncia 2 (vide Nota 2)
34 � 34       1       9       (1) Constante 2 (vide Nota 2)
35 � 43       9       9       (9) Nosso N�mero � Seq��ncia 3 (vide Nota 2)
44 � 44       1       9       (1) D�gito Verificador do Campo Livre

Nota 1 - Os d�gitos verificadores do C�digo do Cedente (posi��o �26�) e do campo
livre do C�digo de Barras (posi��o "44"), s�o calculados da seguinte forma:

� M�dulo "11", com peso de 2 a 9, em que se o resultado da subtra��o for maior
que 9, o DV ser� �0�, caso contr�rio o resultado ser� o pr�prio DV, podendo ser
de 1 a 9;

� Para calcular o DV do c�digo do cedente, considerar o conte�do das posi��es
20 a 25, sendo o sentido do c�lculo da direita para a esquerda.

� Para calcular o DV do campo livre do C�digo de Barras, considerar o conte�do
das posi��es 20 a 43, sendo o sentido do c�lculo da direita para a esquerda.


Nota 2 � Nosso N�mero do SIGCB:

� � composto de 17 posi��es, sendo as 02 posi��es iniciais para identificar a
Carteira e as 15 posi��es restantes s�o para livre utiliza��o pelo Cedente.

� Est� disposto no C�digo de Barras da seguinte maneira:

� Constante 1: 1� posi��o do Nosso Numero � Tipo de Cobran�a (1-
Registrada / 2-Sem Registro)

� Constante 2: 2� posi��o do Nosso N�mero � Emiss�o do Bloqueto (4-
Cedente)

� Seq��ncia 1: Posi��o 3 a 5 do Nosso N�mero

� Seq��ncia 2: Posi��o 6 a 8 do Nosso N�mero

� Seq��ncia 3: Posi��o 9 a 17 do Nosso N�mero

}
     RESULT:=FALSE;
     //banco+moeda+dv+fatorvencimento
     //deixo sem o DV calculo no final

     Self.CodigoBarras:='1049'+Self.CalculaFatorVencimento(Self.Vencimento);
     //+valor+campolivre
     temp:=Self.formata_valor(Self.ValorDoc);
     temp:=Retiracaractere(temp,'.');
     temp:=Retiracaractere(temp,',');
     TEMP:=CompletaPalavra_Esquerda(temp,10,'0');
     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //**************************campo livre*******************************************

     if (Self.CodigoSICOB_Caixa<>'')//� SICOB
     Then Begin
              if (length(Self.CodigoSICOB_Caixa)=15)
              Then Begin
                        if (length(Self.NossoNumeroGerado)<>10)
                        Then Begin
                                  Messagedlg('O Nosso n�mero deveria possuir 10 d�gitos',mterror,[mbok],0);
                                  exit;
                        End;

                        Temp:=Self.NossoNumeroGerado+Self.CodigoSICOB_Caixa;
              End
              Else begin
                        if (length(Self.CodigoSICOB_Caixa)=11)
                        Then Begin
                                   if (length(Self.NossoNumeroGerado)<>14)
                                   Then Begin
                                             Messagedlg('O Nosso n�mero deveria possuir 14 d�gitos. '+Self.NossoNumeroGerado+' Qtde '+inttostr(length(Self.NossoNumeroGerado)),mterror,[mbok],0);
                                             exit;
                                   End;

                                   temp:=Self.CodigoSICOB_Caixa+Self.NossoNumeroGerado;
                        End
                        Else Begin
                                   Messagedlg('O c�digo fornecido pela ag�ncia (SICOB) deveria possuir 11 ou 15 d�gitos '+#13+'C�digo atual: '+Self.CodigoSICOB_Caixa,mterror,[mbok],0);
                                   exit;
                        End;
              End;



     End
     Else Begin//� SINCO ou SIGB

              if ((length(Self.NossoNumeroGerado)=17) and (Self.CodigoCarteira_caixa='24'))//SIGB
              Then Begin
                        //Codigo do Cedente e Digito, usei os campos Conta Corrente e DV Conta Corrente no COnvenio (posicoes 25 e 26)
                        temp:=CompletaPalavra_Esquerda(Self.ContaCorrente,6,'0')+Self.DgContaCorrente;
                        //nosso numero sequencia 01 (posicoes 27 a 29)
                        temp:=temp+copy(self.NossoNumero_partedois,1,3);
                        //Constante 01 (posicao 30) usei o campo carteira cx posicao 1 (2=sem registro)
                        temp:=temp+Self.CodigoCarteira_caixa[1];
                        //nosso numero sequencia 2 (posicoes 31 a 33)
                        temp:=temp+copy(self.NossoNumero_partedois,4,3);
                        //Constante 2 (posicao 34) usei o campo carteira cx posicao 2 (4=emissao cedente)
                        temp:=temp+Self.CodigoCarteira_caixa[2];
                        //Nosso numero sequencia 3 (posicao 35 a 43)
                        temp:=temp+copy(self.NossoNumero_partedois,7,9);

                        //Digito Verificador do Campo livre
                        //Aplicar o Modulo 11 sobre os dados informados at� agora no campo livre

                        temp:=temp+Self.Modulo11_CAIXAECONOMICA(temp);
              End
              Else BEgin//SINCO

                         if (length(Self.NossoNumeroGerado)<>18)
                         Then Begin
                                  Messagedlg('O Nosso n�mero gerado deveria possuir 18 d�gitos',mterror,[mbok],0);
                                  exit;
                         End;

                         if (length(Self.Codigocedente)<>6)
                         Then Begin
                                  Messagedlg('O C�digo do Cedente deveria possuir 06 d�gitos',mterror,[mbok],0);
                                  exit;
                         End;

                        temp:='1'+Self.Codigocedente+Self.NossoNumeroGerado
              End;
     End;

      if (length(temp)<>25)
      then begin
                mensagemerro('O campo livre do c�digo de barras deveria possuir 25 d�gitos'+#13+'Campo Livre atual:'+temp);
                exit;
      End;

     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //*********************************************************************
     //agora calculando o DV e colocando na quinta posicao
     temp:=Self.Modulo11_DVG(Self.CodigoBarras);
     temp:=copy(Self.CodigoBarras,1,4)+temp+copy(Self.CodigoBarras,5,39);
     Self.CodigoBarras:=Temp;
     Result:=True;
end;

function TObjBoletoSemRegistro.GeraNossoNumeroCaixaEconomica: boolean;
begin
      Result:=False;

      
     //Composicao do nosso numero
     //Ele usa o Codigo da Carteira da Caixa e nao o campo carteira
     //pois pode acontecer de num boleto o campo
     //carteira aparecer 1 mas no nossonumero 9

     {
     Segundo Manual

     No Campo Nosso N�mero Informar:

     82NNNNNNNN-D1 (no caso de Cobran�a sem Registro)
     9NNNNNNNNN-D1 (no cao de cobran�a r�pida)

     ronnei: ou seja 10 digitos - qtde digitos da carteira (9,80,81,82...)
     os 10 digitos tem q ter sido configurados no convenio (qtde digitos nosso numero)

*****************NOVA SITUACAO 12/02/09 - ESPACO NOTEBOOK***********************

     EspecificacaoCodBarrasCobranca_Sem_Registro_16posicoes_NN.PDF

     X - C�LCULO DO DV DO NOSSO N�MERO DA COBRAN�A SEM REGISTRO - 16 POSI��ES
    - Campo com 15 posi��es sempre iniciando com 8.
    - Utiliza 1 d�gito verificador calculado atrav�s do m�dulo 11,
     com peso 2 a 9.

    8NNNNNNNNNNNNNN - D1

    - C�lculo do D1
    8 0 1 0 0 0 9 0 1 2 0 0 2 0 0 - N�mero a calcular
    ��������������������AAAAAAAAA

    8 7 6 5 4 3 2 9 8 7 6 5 4 3 2 - �ndice de Multiplica��o
                                    Divis�o � 118/11 = 10 Resto = 8
                                    Subtra��o� 11- 8 = 3
                                    Digito = 3
    - N�mero com o d�gito calculado
    8 0 1 0 0 0 9 0 1 2 0 0 2 0 0 3 - N�mero com o d�gito calculado

    Obs.: Se o resultado da subtra��o for maior que 9 (nove) o DV ser� 0 (zero),
    caso contr�rio o resultado ser� o DV.

    POR�M NO CALCULO DO CODIGO DE BARRAS UTILIZA APENAS 14 DIGITOS
    SEM O 8 INICIAL E SEM O DV FINAL
    
********************************************************************************
     no Caso de SINCO sao 18 DIGITOs

     Exemplo

     821000000000001048-7

     onde:

     821 (carteira)

     000000000001048 (nosso numero)

*********************************************************************
     Nova Situacao TIPo  SIGB

     onde s�o 17 digitos 02 para carteira da caixa e 15 gerado pelo cliente

}

      if (Self.CodigoCarteira_caixa='8')//situacao da espaconotebook
      Then Begin
                //o nosso numero gerado vai sem o 8 pro c�digo de barras
                Self.NossoNumeroGerado:=Self.NossoNumero_partedois;
                //por�m ele � utilizando no calculo do DV e na impressao do nosso numero
                Self.DGNossoNUmero:=Self.Modulo11_CAIXAECONOMICA(Self.CodigoCarteira_caixa+Self.NossoNumero_partedois);
      End
      Else Begin
                if (Self.CodigoCarteira_caixa='24')and (length(Self.NossoNumero_partedois)=15)
                Then Begin
                          (*Tipo SIGB
                            17 Digitos 02 pra carteira e 15 pro cliente
                            2 = Sem Registro
                            4 = Emiss�o pelo cedente  
                          *)
                          Self.NossoNumeroGerado:=Self.CodigoCarteira_caixa+Self.NossoNumero_partedois;
                          Self.DGNossoNUmero:=Self.Modulo11_CAIXAECONOMICA(Self.NossoNumeroGerado);//usa as 17 posicoes (carteira+cliente)
                End
                Else Begin
                          Self.NossoNumeroGerado:=Self.CodigoCarteira_caixa+Self.NossoNumero_partedois;
                          Self.DGNossoNUmero:=Self.Modulo11_CAIXAECONOMICA(Self.NossoNumero_partedois);//usa apenas os numeros do cliente
                End;
      End;
      
      result:=True;
end;

function TObjBoletoSemRegistro.Modulo11_CAIXAECONOMICA(
  PValor: String): string;
var
   Soma : integer;
   Contador, Peso, Digito : integer;
begin
   {Segundo Manual
    Calculos do D1
    Exemplo
    9 0 0 1 2 0 0 2 0 0 numero a calcular
    3 2 9 8 7 6 5 4 3 2 indice de multiplicacao
    --------------------
    (27+0+0+8+14+0+0+8+0+0)=57
    Divisao 57/11=5 Resto=2
    Subtracao=11-2=9

    Digito=9

    Observacao: Se o resultado da subtracao for maior que 9(nove) o DV sera 0 (zero),
    caso contrario o resultado sera o DV}

   peso:=2;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
        Soma := Soma + (StrToInt(PValor[Contador]) * Peso);
        if Peso =9
        then Peso :=2
        else Peso :=peso+1;
   End;
      
   if (Soma<11)
   Then Begin
             result:=inttostr(11-Soma);
             exit;
   End;

   peso:=Soma mod 11;//pega o resto

   Digito:=11-Peso;

   if (Digito>9)
   Then result:='0'
   Else result:=Inttostr(digito);
end;

function TObjBoletoSemRegistro.GeraCodigoBarrasUnibanco: Boolean;
var
temp:string;
begin
     RESULT:=FALSE;
     //banco+moeda+dv+fatorvencimento
     //deixo sem o DV calculo no final

     Self.CodigoBarras:='4099'+Self.CalculaFatorVencimento(Self.Vencimento);
     //+valor+campolivre
     temp:=Self.formata_valor(Self.ValorDoc);
     temp:=Retiracaractere(temp,'.');
     temp:=Retiracaractere(temp,',');
     TEMP:=CompletaPalavra_Esquerda(temp,10,'0');
     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //**************************campo livre*******************************************

{
Posicao  QTDE  ESPECIFICACAO
20	      1	  c�digo para transa��o CVT: 5 (n�mero FIXO) (5=7744-5)
21 a 27	  7	  n�mero do cliente no C�DIGO DE BARRAS (Consultar par�metro junto ao Banco)
28 a 29	  2	  vago. Usar 00 (n�mero FIXO)
30 a 43	  14	N�mero de refer�ncia (Nosso N�mero do cliente -  NNNNNNNNNNNNNN 14 posi��es livres) do cliente
44	      1	  D�gito verificador do N�mero de Refer�ncia,   calculado pelo m�dulo 11 (p�gina 11)  = D
}

     temp:=Self.codigotransacao_unibanco+Self.numerocliente_UNIBANCO+Self.posicoes_28_a_29_UNIBANCO+Self.NossoNumeroGerado+Self.DGNossoNUmero;

     if (length(temp)<>25)
     Then begin
               MensagemErro('O Campo Livre do C�digo de Barras do unibanco n�o cont�m 25 posi��es');
               exit;
     End;

     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //*********************************************************************
     //agora calculando o DV e colocando na quinta posicao
     temp:=Self.Modulo11_DVG(Self.CodigoBarras);
     temp:=copy(Self.CodigoBarras,1,4)+temp+copy(Self.CodigoBarras,5,39);
     Self.CodigoBarras:=Temp;
     Result:=True;
end;

function TObjBoletoSemRegistro.GeraNossoNumeroUnibanco: boolean;
begin
     //O Nosso numero no Unibanco � livre, conforme manual sao 14 digitos
     //mais o verificador

     Self.NossoNumeroGerado:=Self.NossoNumero_partedois;
     Self.DGNossoNUmero:=Self.Modulo11_2_a_9_Unibanco_nosso_numero(Self.NossoNumeroGerado);
     result:=True;
end;

function TObjBoletoSemRegistro.Modulo11_2_a_9_Unibanco_nosso_numero(PValor: String): string;
var
   Soma : integer;
   Contador, Peso, Digito : integer;
begin
   {Segundo Manual

M�DULO 11 (de 2 a 9)

Considerando o n�mero 29875782123:

4.	Multiplica-se cada n�mero por 2, 3, 4, 5, 6, 7, 8 ou 9, nesta ordem
5.	Inicia-se a multiplica��o a partir do �ltimo n�mero (direita para esquerda)
6.	Inicia-se a altern�ncia dos fatores multiplicadores a partir do 2 e segue-se em ordem crescente de multiplicadores

Exemplo:

    2     9     8     7     5     7     8     2     1     2     3
X   4     3     2     9     8     7     6     5     4     3     2    .
----------------------------------------------------------------------
    8    27    16    63    40     49    48    10    4     6     6

Em seguida somamos os resultados:
8  +  27  +  16  +  63  +  40  +  49  +  48  +  10  +  4  +  6  +  6  =  277
Multiplica-se a soma por 10:
277  X  10  =  2770
Divide-se o resultado por 11. O resto da divis�o � o d�gito:
2770/11= 251  ,  resto  9

Logo: d�gito = 9

Observa��o 1:	Defini��o para c�lculo do D�gito Verificador do C�DIGO DE BARRAS:
Se o D�gito Verificador do c�digo de barras for igual a '0', '1' ou '10',
ser� assumido o valor '1'.

---> Observa��o 2:	Defini��o para c�lculo do D�gito Verificador do NOSSO N�MERO/REF�RENCIA:
Se o d�gito do  "Nosso N�mero / Refer�ncia do Cliente", for igual a '0' ou '10', 
ser� assumido o valor '0'.
}

   peso:=2;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
        Soma := Soma + (StrToInt(PValor[Contador]) * Peso);
        if Peso =9
        then Peso :=2
        else Peso :=peso+1;
   End;


   peso:=Soma mod 11;//pega o resto

   Digito:=11-Peso;

   if (Digito>9)
   Then result:='0'
   Else result:=Inttostr(digito);
end;

function TObjBoletoSemRegistro.calculaDVCampoLivreSicred(
  pvalor: string): string;
var
   Soma : integer;
   Contador, Peso, Digito : integer;
begin
   {Segundo Manual
    Calculos do D1
    Exemplo
    9 0 0 1 2 0 0 2 0 0 numero a calcular
    3 2 9 8 7 6 5 4 3 2 indice de multiplicacao
    --------------------

    Exemplo do Manual

    241=resuoltado da somatoria de cada multiplicacao

    241/11=21,91
    21X11=231
    241-231= 10 (caso "0" ou "1" o dv zera zero)
    11-10 =1  dv=1
}

   peso:=2;
   soma:=0;
   for Contador := Length(PValor) downto 1 do
   begin
        Soma := Soma + (StrToInt(PValor[Contador]) * Peso);
        if Peso =9
        then Peso :=2
        else Peso :=peso+1;
   End;
      

   peso:=strtoint(floattostr(int(Soma/11)));//pega a parte inteira do result. da divisao

   Digito:=soma-(peso*11);

   if (digito=0) or (digito=1)
   Then result:='0'
   Else result:=inttostr(11-digito);
end;


function TObjBoletoSemRegistro.GeraCodigoBarrasHSBC: Boolean;

var
DigitoVCampoLivre,temp:string;
begin
     RESULT:=FALSE;

     Self.CodigoBarras:='3999'+Self.CalculaFatorVencimento(Self.Vencimento);
     //+valor+campolivre
     temp:=Self.formata_valor(Self.ValorDoc);
     temp:=Retiracaractere(temp,'.');
     temp:=Retiracaractere(temp,',');
     TEMP:=CompletaPalavra_Esquerda(temp,10,'0');
     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //**************************campo livre*******************************************

     (*

As 44 posi��es do c�digo de barras dever�o conter, obrigatoriamente, as seguintes
informa��es referentes ao t�tulo:
POSI��O
DE AT� TAMANHO CONTE�DO
01 03 03 C�digo do HSBC na C�mara de Compensa��o, igual a 399.
04 04 01 Tipo de Moeda (9 para moeda Real ou 0 para Moeda
Vari�vel).
05 05 01 D�gito de Autoconfer�ncia (DAC).
06 09 04 Fator de Vencimento.
10 19 10 Valor do Documento.
Se Moeda Vari�vel, o valor dever� ser igual a zeros.

------------------------------------
20 26 07 C�digo do Cedente
27 39 13 N�mero Banc�rio, igual ao C�digo do Documento, sem os
d�gitos verificadores e tipo identificador.
40 43 04 Data de Vencimento no Formato Juliano.
44 44 01 C�digo do Produto CNR, igual a 2

    *)

     temp:=Self.codigocedente_HSBC+Self.NossoNumero_partedois+cdate2julian(datetostr(Self.Vencimento))+Self.codigoCNR_HSBC;

     Self.CodigoBarras:=Self.CodigoBarras+temp;
     //*********************************************************************
     //agora calculando o DV e colocando na quinta posicao
     temp:=Self.Modulo11_DVG(Self.CodigoBarras);
     temp:=copy(Self.CodigoBarras,1,4)+temp+copy(Self.CodigoBarras,5,39);
     Self.CodigoBarras:=Temp;
     Result:=True;
end;

function TObjBoletoSemRegistro.GeraNossoNumeroHSBC: boolean;
var
Dg1,Dg2,temp:string;
begin
     result := false;

(*

5. 3 � EXEMPLO DE COMPOSI��O DO C�DIGO DO DOCUMENTO COM USO DO
TIPO IDENTIFICADOR �4�

Consiste: �C�digo do Documento�, �C�digo do Cedente� e �Data do Vencimento�.
Exemplo:
C�digo do Documento 239104761
C�digo do Cedente 8351202
Data do Vencimento 04/07/2008

5. 3. 1 � C�lculo do Primeiro D�gito Verificador

1 - Aplicar pesos de 9 a 2, da direita para a esquerda, no c�digo num�rico escolhido para
identifica��o do sacado (composto de 1 a 13 caracteres).
2 - Multiplicar os algarismos do c�digo num�rico escolhido pelo respectivo peso.
3 - Calcular o somat�rio dos produtos das multiplica��es realizadas.
4 - Dividir o somat�rio por 11.
5 - Obter o resto da divis�o.
6 - Considerar o resto da divis�o como sendo o primeiro d�gito verificador do c�digo do
documento.
Ent�o, tomando-se o C�digo do Sacado = 239104761, teremos:
2 3 9 1 0 4 7 6 1 185 11
Pesos x 9 x 2 x 3 x 4 x 5 x 6 x 7 x 8 x 9 176 16
------------------------------------------------------ 9 ......(resto = primeiro d�gito)
18 + 06 + 27 + 04 + 00 + 24 + 49 + 48 + 09 = 185
O resto da divis�o ser� o primeiro d�gito verificador.
Nota:
Se o resto da divis�o for igual a 0 (zero) ou 10, o primeiro d�gito verificador ser� igual a
0 (zero).

5. 3. 2 � C�lculo do Segundo D�gito Verificador

1 - Tomar o c�digo num�rico escolhido para identifica��o do sacado (composto de 1 a 13
caracteres), acrescentando � direita o primeiro d�gito verificador obtido e o tipo identificador
igual a 4; o c�digo do cedente fornecido pelo Banco e a data de vencimento com somente os
dois �ltimos d�gitos do ano.

2 - Efetuar o somat�rio algarismo a algarismo desses 3 par�metros, sendo que quando a soma
ultrapassar a 9, a unidade da dezena deve ser acrescida ao somat�rio dos algarismos
imediatamente � esquerda.
3 - Aplicar pesos de 9 a 2, da direita para a esquerda, no c�digo num�rico resultante do
somat�rio do item 2.
4 - Multiplicar os algarismos do c�digo num�rico escolhido pelo respectivo peso.
5 - Calcular o somat�rio dos produtos das multiplica��es realizadas.
6 - Dividir o somat�rio por 11.
7 - Obter o resto da divis�o.
6 - Considerar o resto da divis�o como sendo o segundo d�gito verificador do c�digo do
documento.

Ent�o, tomando-se o C�digo do Sacado = 239104761, o primeiro d�gito verificador calculado
= 9, o tipo identificador = 4, o C�digo do Cedente = 8351202 e a Data de Vencimento =
040708 (utiliza-se somente os dois �ltimos d�gitos do ano), teremos:
2 3 9 1 0 4 7 6 1 9 4 (4 � o tipo identificador)
+8 +3 +5 +1 +2 +0 +2 (C�digo do cedente)
+0 +4 +0 +7 +0 +8 (Data de vencimento)
--------------------------------------------------------------------------
2 3 9 1 8 8 6 8 1 0 4
2 3 9 1 8 8 6 8 1 0 4 298 11
x 7 x 8 x 9 x 2 x 3 x 4 x 5 x 6 x 7 x 8 x 9 297 27
--------------------------------------------------------------------------- 1...(resto = segundo
14 + 24 + 81 + 02 + 24 + 32 + 30 + 48 + 07 + 00 + 36 = 298 d�gito)
O resto da divis�o ser� o segundo d�gito verificador.
Nota:
Se o resto da divis�o for igual a 0 (zero) ou 10, o primeiro d�gito verificador ser� igual a
0 (zero).
5.3.3 � C�digo do Documento Final com os D�gitos Calculados
2 3 9 1 0 4 7 6 1 9 4 1


*)


  if (Self.tipoidentificador_HSBC='4')
  Then Begin
            Dg1:=Self.Modulo11_nosso_numero_HSBC(Self.NossoNumero_partedois);
            temp:=Self.NossoNumero_partedois+dg1+'4';

            temp:=inttostr(StrToInt64(temp)+strtoint64(Self.codigocedente_HSBC)+strtoint64(FormatDateTime('ddmmyy',self.Vencimento)));
            Dg2:=Self.Modulo11_nosso_numero_HSBC(temp);

            Self.NossoNumeroGerado:=Self.NossoNumero_partedois+dg1+'4'+dg2;
  End
  Else if (Self.tipoidentificador_HSBC='5')
        Then Begin
            Dg1:=Self.Modulo11_nosso_numero_HSBC(Self.NossoNumero_partedois);
            temp:=Self.NossoNumero_partedois+dg1+'5';

            temp:=inttostr(StrToInt64(temp)+StrToInt64(Self.codigocedente_HSBC));
            Dg2:=Self.Modulo11_nosso_numero_HSBC(temp);

            Self.NossoNumeroGerado:=Self.NossoNumero_partedois+dg1+'5'+dg2;

        End
        Else Begin
              MensagemErro('O tipo de identificador deve ser 4 ou 5 no conv�nio do banco HSBC');
              Exit;
        End;

  result := true;

end;

end.



