unit UobjLOTERETORNOCOBRANCA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UobjCONVENIOSBOLETO;

Type
   TObjLOTERETORNOCOBRANCA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Convenioboleto:TObjCONVENIOSBOLETO;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaArquivo(pnumeroconvenio,PdataArquivo,PnumeroArquivo:String):Boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_DATAHORA(parametro: string);
                Function Get_DATAHORA: string;
                Procedure Submit_NOMEARQUIVO(parametro: string);
                Function Get_NOMEARQUIVO: string;
                Procedure Submit_DATAARQUIVO(parametro: string);
                Function Get_DATAARQUIVO: string;
                Procedure Submit_NUMEROARQUIVO(parametro: string);
                Function Get_NUMEROARQUIVO: string;
                Procedure Submit_CGCEMPRESA(parametro: string);
                Function Get_CGCEMPRESA: string;
                Procedure Submit_CODIGOCEDENTE(parametro: string);
                Function Get_CODIGOCEDENTE: string;
                Procedure Submit_AGENCIA(parametro: string);
                Function Get_AGENCIA: string;
                Procedure Submit_DIGITOAGENCIA(parametro: string);
                Function Get_DIGITOAGENCIA: string;
                Procedure Submit_CONTA(parametro: string);
                Function Get_CONTA: string;
                Procedure Submit_DIGITOCONTA(parametro: string);
                Function Get_DIGITOCONTA: string;

                Function Get_Processado:string;
                Procedure Submit_Processado(parametro:string);

                procedure edtconvenioboletoExit(Sender: TObject; LbNome: TLabel);
                procedure edtconvenioboletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure edtconvenioboletoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LbNome:TLabel);overload;

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;


               CODIGO:string;
               DATAHORA:string;
               NOMEARQUIVO:string;
               DATAARQUIVO:string;
               NUMEROARQUIVO:string;
               CGCEMPRESA:string;
               CODIGOCEDENTE:string;
               AGENCIA:string;
               DIGITOAGENCIA:string;
               CONTA:string;
               DIGITOCONTA:string;
               Processado:string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;



   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
, UMenuRelatorios, UCONVENIOSBOLETO;


Function  TObjLOTERETORNOCOBRANCA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.DATAHORA:=fieldbyname('DATAHORA').asstring;
        Self.NOMEARQUIVO:=fieldbyname('NOMEARQUIVO').asstring;
        Self.DATAARQUIVO:=fieldbyname('DATAARQUIVO').asstring;
        Self.NUMEROARQUIVO:=fieldbyname('NUMEROARQUIVO').asstring;
        Self.CGCEMPRESA:=fieldbyname('CGCEMPRESA').asstring;
        Self.CODIGOCEDENTE:=fieldbyname('CODIGOCEDENTE').asstring;
        Self.AGENCIA:=fieldbyname('AGENCIA').asstring;
        Self.DIGITOAGENCIA:=fieldbyname('DIGITOAGENCIA').asstring;
        Self.CONTA:=fieldbyname('CONTA').asstring;
        Self.DIGITOCONTA:=fieldbyname('DIGITOCONTA').asstring;
        if (fieldbyname('Convenioboleto').asstring<>'')
        Then Begin
                  if (Self.Convenioboleto.LocalizaCodigo(fieldbyname('Convenioboleto').asstring)=False)
                  Then begin
                            Self.ZerarTabela;
                            Messagedlg('Conv�nio N� '+fieldbyname('Convenioboleto').asstring+' n�o encontrado',mterror,[mbok],0);
                            exit;
                  End
                  Else Self.Convenioboleto.TabelaparaObjeto;
        End;
        Self.Processado:=Fieldbyname('processado').asstring;
        result:=True;
     End;
end;


Procedure TObjLOTERETORNOCOBRANCA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('DATAHORA').asstring:=Self.DATAHORA;
        ParamByName('NOMEARQUIVO').asstring:=Self.NOMEARQUIVO;
        ParamByName('DATAARQUIVO').asstring:=Self.DATAARQUIVO;
        ParamByName('NUMEROARQUIVO').asstring:=Self.NUMEROARQUIVO;
        ParamByName('CGCEMPRESA').asstring:=Self.CGCEMPRESA;
        ParamByName('CODIGOCEDENTE').asstring:=Self.CODIGOCEDENTE;
        ParamByName('AGENCIA').asstring:=Self.AGENCIA;
        ParamByName('DIGITOAGENCIA').asstring:=Self.DIGITOAGENCIA;
        ParamByName('CONTA').asstring:=Self.CONTA;
        ParamByName('DIGITOCONTA').asstring:=Self.DIGITOCONTA;
        ParamByName('ConvenioBoleto').asstring    :=Self.Convenioboleto.Get_CODIGO;
        ParamByName('Processado').asstring        :=Self.Processado;
  End;
End;

//***********************************************************************

function TObjLOTERETORNOCOBRANCA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjLOTERETORNOCOBRANCA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        DATAHORA:='';
        NOMEARQUIVO:='';
        DATAARQUIVO:='';
        NUMEROARQUIVO:='';
        CGCEMPRESA:='';
        CODIGOCEDENTE:='';
        AGENCIA:='';
        DIGITOAGENCIA:='';
        CONTA:='';
        DIGITOCONTA:='';
        ConvenioBoleto.ZerarTabela;
        Processado:='';
     End;
end;

Function TObjLOTERETORNOCOBRANCA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (Convenioboleto.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Conv�nio Boleto';

      if (Processado='')
      Then Mensagem:=Mensagem+'/Processado';
      //CODIFICA VERIFICABRANCOS
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjLOTERETORNOCOBRANCA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS
     if (Self.Convenioboleto.Get_CODIGO<>'')
     Then Begin
               if (Self.Convenioboleto.LocalizaCodigo(Self.Convenioboleto.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'\Conv�nio n�o localizado';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjLOTERETORNOCOBRANCA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        If (Self.Convenioboleto.Get_CODIGO<>'')
        Then Strtoint(Self.Convenioboleto.Get_CODIGO);
     Except
           mensagem:=Mensagem+'\Conv�nio';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjLOTERETORNOCOBRANCA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodatetime(Self.DATAHORA);
     Except
           Mensagem:=mensagem+'/Data e Hora';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjLOTERETORNOCOBRANCA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA
        if (Self.Processado<>'S') and (Self.Processado<>'N')
        Then Mensagem:=Mensagem+'O campo Processado cont�m um valor inv�lido';

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjLOTERETORNOCOBRANCA.LocalizaArquivo(pnumeroconvenio,
  PdataArquivo, PnumeroArquivo: String): Boolean;
begin
       result:=False;
       
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,convenioboleto,DATAHORA,NOMEARQUIVO,DATAARQUIVO,NUMEROARQUIVO,CGCEMPRESA');
           SQL.ADD(' ,CODIGOCEDENTE,AGENCIA,DIGITOAGENCIA,CONTA,DIGITOCONTA,processado');
           SQL.ADD(' from  TABLOTERETORNOCOBRANCA');
           SQL.ADD(' WHERE ConvenioBoleto='+PnumeroConvenio);
           SQL.ADD(' and DataArquivo='+#39+Pdataarquivo+#39);
           SQL.ADD(' and NumeroArquivo='+#39+PNumeroarquivo+#39);
           Try
              Open;
           Except
              on e:exception do
              Begin
                  Mensagemerro('Erro na tentativa de Localizar um lote '+#13+E.message);
                  exit;
              End;
           End;

           If (recordcount>0)
           Then Result:=True;
       End;
end;

function TObjLOTERETORNOCOBRANCA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro LOTERETORNOCOBRANCA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,convenioboleto,DATAHORA,NOMEARQUIVO,DATAARQUIVO,NUMEROARQUIVO,CGCEMPRESA');
           SQL.ADD(' ,CODIGOCEDENTE,AGENCIA,DIGITOAGENCIA,CONTA,DIGITOCONTA,processado');
           SQL.ADD(' from  TABLOTERETORNOCOBRANCA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjLOTERETORNOCOBRANCA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjLOTERETORNOCOBRANCA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjLOTERETORNOCOBRANCA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin



        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;


        Self.Convenioboleto:=TObjCONVENIOSBOLETO.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABLOTERETORNOCOBRANCA(CODIGO,convenioboleto,DATAHORA,NOMEARQUIVO');
                InsertSQL.add(' ,DATAARQUIVO,NUMEROARQUIVO,CGCEMPRESA,CODIGOCEDENTE');
                InsertSQL.add(' ,AGENCIA,DIGITOAGENCIA,CONTA,DIGITOCONTA,processado)');
                InsertSQL.add('values (:CODIGO,:convenioboleto,:DATAHORA,:NOMEARQUIVO,:DATAARQUIVO');
                InsertSQL.add(' ,:NUMEROARQUIVO,:CGCEMPRESA,:CODIGOCEDENTE,:AGENCIA');
                InsertSQL.add(' ,:DIGITOAGENCIA,:CONTA,:DIGITOCONTA,:processado)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABLOTERETORNOCOBRANCA set CODIGO=:CODIGO,convenioboleto=:convenioboleto,DATAHORA=:DATAHORA');
                ModifySQL.add(',NOMEARQUIVO=:NOMEARQUIVO,DATAARQUIVO=:DATAARQUIVO,NUMEROARQUIVO=:NUMEROARQUIVO');
                ModifySQL.add(',CGCEMPRESA=:CGCEMPRESA,CODIGOCEDENTE=:CODIGOCEDENTE');
                ModifySQL.add(',AGENCIA=:AGENCIA,DIGITOAGENCIA=:DIGITOAGENCIA,CONTA=:CONTA');
                ModifySQL.add(',DIGITOCONTA=:DIGITOCONTA,processado=:processado');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABLOTERETORNOCOBRANCA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjLOTERETORNOCOBRANCA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjLOTERETORNOCOBRANCA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabLOTERETORNOCOBRANCA');
     Result:=Self.ParametroPesquisa;
end;

function TObjLOTERETORNOCOBRANCA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de LOTERETORNOCOBRANCA ';
end;


function TObjLOTERETORNOCOBRANCA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENLOTERETORNOCOBRANCA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjLOTERETORNOCOBRANCA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Convenioboleto.free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjLOTERETORNOCOBRANCA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;


procedure TObjLOTERETORNOCOBRANCA.edtconvenioboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FconveniosBoleto1:TFCONVENIOSBOLETO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FconveniosBoleto1:=TFCONVENIOSBOLETO.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Convenioboleto.Get_Pesquisa,Self.Convenioboleto.Get_TituloPesquisa,FconveniosBoleto1)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FconveniosBoleto1);
     End;
end;

procedure TObjLOTERETORNOCOBRANCA.edtconvenioboletoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState;LbNome:TLabel);
var
   FpesquisaLocal:Tfpesquisa;
   FconveniosBoleto1:TFCONVENIOSBOLETO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FconveniosBoleto1:=TFCONVENIOSBOLETO.Create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Convenioboleto.Get_Pesquisa,Self.Convenioboleto.Get_TituloPesquisa,FconveniosBoleto1)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                Tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                LbNome.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FconveniosBoleto1);
     End;
end;

procedure TObjLOTERETORNOCOBRANCA.edtconvenioboletoExit(Sender: TObject;
  LbNome: TLabel);
begin
     LbNome.caption:='';
     if (TEdit(Sender).text='')
     Then exit;

     Try
        strtoint(TEdit(Sender).text);
     Except
           TEdit(Sender).text:='';
     End;

     if (Self.Convenioboleto.LocalizaCodigo(TEdit(Sender).text)=False)
     Then begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Convenioboleto.TabelaparaObjeto;
     LbNome.caption:=Self.Convenioboleto.Get_Nome;
end;



//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjLOTERETORNOCOBRANCA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjLOTERETORNOCOBRANCA.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_DATAHORA(parametro: string);
begin
        Self.DATAHORA:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_DATAHORA: string;
begin
        Result:=Self.DATAHORA;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_NOMEARQUIVO(parametro: string);
begin
        Self.NOMEARQUIVO:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_NOMEARQUIVO: string;
begin
        Result:=Self.NOMEARQUIVO;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_DATAARQUIVO(parametro: string);
begin
        Self.DATAARQUIVO:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_DATAARQUIVO: string;
begin
        Result:=Self.DATAARQUIVO;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_NUMEROARQUIVO(parametro: string);
begin
        Self.NUMEROARQUIVO:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_NUMEROARQUIVO: string;
begin
        Result:=Self.NUMEROARQUIVO;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_CGCEMPRESA(parametro: string);
begin
        Self.CGCEMPRESA:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_CGCEMPRESA: string;
begin
        Result:=Self.CGCEMPRESA;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_CODIGOCEDENTE(parametro: string);
begin
        Self.CODIGOCEDENTE:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_CODIGOCEDENTE: string;
begin
        Result:=Self.CODIGOCEDENTE;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_AGENCIA(parametro: string);
begin
        Self.AGENCIA:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_AGENCIA: string;
begin
        Result:=Self.AGENCIA;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_DIGITOAGENCIA(parametro: string);
begin
        Self.DIGITOAGENCIA:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_DIGITOAGENCIA: string;
begin
        Result:=Self.DIGITOAGENCIA;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_CONTA(parametro: string);
begin
        Self.CONTA:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_CONTA: string;
begin
        Result:=Self.CONTA;
end;
procedure TObjLOTERETORNOCOBRANCA.Submit_DIGITOCONTA(parametro: string);
begin
        Self.DIGITOCONTA:=Parametro;
end;
function TObjLOTERETORNOCOBRANCA.Get_DIGITOCONTA: string;
begin
        Result:=Self.DIGITOCONTA;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjLOTERETORNOCOBRANCA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJLOTERETORNOCOBRANCA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:Begin
          End;

          End;
     end;

end;


function TObjLOTERETORNOCOBRANCA.Get_Processado: string;
begin
     Result:=Self.Processado;
end;

procedure TObjLOTERETORNOCOBRANCA.Submit_Processado(parametro: string);
begin
     Self.Processado:=Parametro;
end;


end.


