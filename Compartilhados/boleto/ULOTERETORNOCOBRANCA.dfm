object FLOTERETORNOCOBRANCA: TFLOTERETORNOCOBRANCA
  Left = 535
  Top = 204
  Caption = 'Cadastro de Lote de Retorno de Cobran'#231'a - EXCLAIM TECNOLOGIA'
  ClientHeight = 455
  ClientWidth = 734
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 213
    Width = 734
    Height = 192
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 2
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Boletos'
      object DBGridRegistros: TDBGrid
        Left = 0
        Top = 0
        Width = 726
        Height = 164
        Align = alClient
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'Verdana'
        TitleFont.Style = []
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Pend'#234'ncias'
      object STRGPendencias: TStringGrid
        Left = 0
        Top = 0
        Width = 726
        Height = 164
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 734
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 540
      Top = 0
      Width = 194
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Lote de Retorno de Cobran'#231'a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object Btnovo: TBitBtn
      Left = 1
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 101
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 401
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object BtOpcoes: TBitBtn
      Left = 351
      Top = -1
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = BtOpcoesClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 405
    Width = 734
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 3
    DesignSize = (
      734
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 734
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 436
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 905
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 905
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 50
    Width = 734
    Height = 163
    Align = alTop
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 732
      Height = 161
      Align = alClient
      Stretch = True
    end
    object LbCODIGO: TLabel
      Left = 11
      Top = 14
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbDATAHORA: TLabel
      Left = 11
      Top = 40
      Width = 61
      Height = 14
      Caption = 'Data e Hora'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNOMEARQUIVO: TLabel
      Left = 11
      Top = 64
      Width = 95
      Height = 14
      Caption = 'Nome do Arquivo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbDATAARQUIVO: TLabel
      Left = 11
      Top = 88
      Width = 86
      Height = 14
      Caption = 'Data do Arquivo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNUMEROARQUIVO: TLabel
      Left = 248
      Top = 88
      Width = 107
      Height = 14
      Caption = 'N'#250'mero do Arquivo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbCGCEMPRESA: TLabel
      Left = 11
      Top = 112
      Width = 92
      Height = 14
      Caption = 'CGC da Empresa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbCODIGOCEDENTE: TLabel
      Left = 248
      Top = 112
      Width = 106
      Height = 14
      Caption = 'C'#243'digo do Cedente'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbAGENCIA: TLabel
      Left = 11
      Top = 136
      Width = 44
      Height = 14
      Caption = 'Ag'#234'ncia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbDIGITOAGENCIA: TLabel
      Left = 180
      Top = 137
      Width = 13
      Height = 13
      Caption = ' - '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object LbCONTA: TLabel
      Left = 248
      Top = 136
      Width = 32
      Height = 14
      Caption = 'Conta'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbDIGITOCONTA: TLabel
      Left = 422
      Top = 137
      Width = 13
      Height = 13
      Caption = ' - '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label1: TLabel
      Left = 248
      Top = 40
      Width = 52
      Height = 14
      Caption = 'Conv'#234'nio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbnomeconvenio: TLabel
      Left = 479
      Top = 40
      Width = 94
      Height = 14
      Caption = 'lbNomeConvenio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 248
      Top = 14
      Width = 66
      Height = 14
      Caption = 'Processado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Shape1: TShape
      Left = 0
      Top = 158
      Width = 2000
      Height = 1
      Pen.Mode = pmWhite
    end
    object EdtDATAHORA: TMaskEdit
      Left = 115
      Top = 38
      Width = 115
      Height = 19
      EditMask = '!99/99/9999  90:00;1;_'
      MaxLength = 17
      TabOrder = 2
      Text = '  /  /        :  '
    end
    object EdtNOMEARQUIVO: TEdit
      Left = 115
      Top = 62
      Width = 357
      Height = 19
      MaxLength = 100
      TabOrder = 4
    end
    object EdtDATAARQUIVO: TEdit
      Left = 115
      Top = 85
      Width = 115
      Height = 19
      MaxLength = 10
      TabOrder = 5
    end
    object EdtNUMEROARQUIVO: TEdit
      Left = 357
      Top = 85
      Width = 115
      Height = 19
      MaxLength = 10
      TabOrder = 6
    end
    object EdtCGCEMPRESA: TEdit
      Left = 115
      Top = 109
      Width = 115
      Height = 19
      MaxLength = 20
      TabOrder = 7
    end
    object EdtCODIGOCEDENTE: TEdit
      Left = 357
      Top = 110
      Width = 115
      Height = 19
      MaxLength = 20
      TabOrder = 8
    end
    object EdtAGENCIA: TEdit
      Left = 115
      Top = 134
      Width = 65
      Height = 19
      MaxLength = 20
      TabOrder = 9
    end
    object EdtDIGITOAGENCIA: TEdit
      Left = 194
      Top = 134
      Width = 36
      Height = 19
      MaxLength = 1
      TabOrder = 10
    end
    object EdtCONTA: TEdit
      Left = 357
      Top = 134
      Width = 65
      Height = 19
      MaxLength = 20
      TabOrder = 11
    end
    object EdtDIGITOCONTA: TEdit
      Left = 436
      Top = 134
      Width = 36
      Height = 19
      MaxLength = 1
      TabOrder = 12
    end
    object EdtCODIGO: TEdit
      Left = 115
      Top = 12
      Width = 115
      Height = 19
      MaxLength = 9
      TabOrder = 0
    end
    object edtconvenioboleto: TEdit
      Left = 357
      Top = 38
      Width = 115
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 3
      OnExit = edtconvenioboletoExit
      OnKeyDown = edtconvenioboletoKeyDown
    end
    object ComboProcessado: TComboBox
      Left = 356
      Top = 12
      Width = 115
      Height = 21
      BevelKind = bkSoft
      TabOrder = 1
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
  end
end
