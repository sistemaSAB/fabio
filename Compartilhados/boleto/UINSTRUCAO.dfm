object FINSTRUCAO: TFINSTRUCAO
  Left = 356
  Top = 132
  Width = 790
  Height = 377
  Caption = 'Cadastro de INSTRUCAO - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 774
    Height = 339
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      DesignSize = (
        766
        311)
      object ImagemFundo: TImage
        Left = 0
        Top = 50
        Width = 766
        Height = 211
        Align = alClient
        Stretch = True
      end
      object LbCodigo: TLabel
        Left = 13
        Top = 100
        Width = 44
        Height = 13
        Caption = 'Codigo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbNome: TLabel
        Left = 13
        Top = 140
        Width = 37
        Height = 13
        Caption = 'Nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 59
        Top = 100
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 3
      end
      object EdtNome: TEdit
        Left = 59
        Top = 140
        Width = 560
        Height = 19
        MaxLength = 200
        TabOrder = 4
      end
      object panelbotes: TPanel
        Left = 0
        Top = 0
        Width = 766
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 0
        DesignSize = (
          766
          50)
        object lbnomeformulario: TLabel
          Left = 459
          Top = 8
          Width = 215
          Height = 38
          Alignment = taRightJustify
          Anchors = []
          Caption = 'INSTRUCAO'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -32
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Btnovo: TBitBtn
          Left = 3
          Top = -3
          Width = 50
          Height = 52
          Caption = '&n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BtnovoClick
        end
        object btpesquisar: TBitBtn
          Left = 253
          Top = -3
          Width = 50
          Height = 52
          Caption = '&p'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnClick = btpesquisarClick
        end
        object btrelatorios: TBitBtn
          Left = 303
          Top = -3
          Width = 50
          Height = 52
          Caption = '&r'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
        end
        object btalterar: TBitBtn
          Left = 53
          Top = -3
          Width = 50
          Height = 52
          Caption = '&a'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btalterarClick
        end
        object btexcluir: TBitBtn
          Left = 203
          Top = -3
          Width = 50
          Height = 52
          Caption = '&e'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = btexcluirClick
        end
        object btgravar: TBitBtn
          Left = 103
          Top = -3
          Width = 50
          Height = 52
          Caption = '&g'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btgravarClick
        end
        object btcancelar: TBitBtn
          Left = 153
          Top = -3
          Width = 50
          Height = 52
          Caption = '&c'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = btcancelarClick
        end
        object btsair: TBitBtn
          Left = 403
          Top = -3
          Width = 50
          Height = 52
          Caption = '&s'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 8
          OnClick = btsairClick
        end
        object btopcoes: TBitBtn
          Left = 353
          Top = -3
          Width = 50
          Height = 52
          Caption = '&o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -1
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
        end
      end
      object painel_imagem: TPanel
        Left = 682
        Top = 15
        Width = 92
        Height = 77
        Anchors = [akTop, akRight]
        BevelOuter = bvNone
        Color = clWindow
        Ctl3D = False
        ParentCtl3D = False
        TabOrder = 1
      end
      object panelrodape: TPanel
        Left = 0
        Top = 261
        Width = 766
        Height = 50
        Align = alBottom
        BevelOuter = bvNone
        Color = clWindow
        TabOrder = 2
        DesignSize = (
          766
          50)
        object ImagemRodape: TImage
          Left = 0
          Top = 0
          Width = 766
          Height = 50
          Align = alClient
          Stretch = True
        end
        object lbquantidadeformulario: TLabel
          Left = 442
          Top = 16
          Width = 312
          Height = 18
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          Caption = 'Existem X INSTRUCAO cadastrados'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -16
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
end
