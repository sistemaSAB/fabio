object FREGISTRORETORNOCOBRANCA: TFREGISTRORETORNOCOBRANCA
  Left = 230
  Top = 187
  Width = 700
  Height = 361
  Caption = 
    'Cadastro de Registros de Retorno de Cobran'#231'a - EXCLAIM TECNOLOGI' +
    'A'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelrodape: TPanel
    Left = 0
    Top = 273
    Width = 684
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      684
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 684
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 386
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 855
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 855
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 684
    Height = 273
    Align = alClient
    BevelOuter = bvNone
    Color = clSilver
    TabOrder = 1
    object ImagemFundo: TImage
      Left = 0
      Top = 49
      Width = 684
      Height = 224
      Align = alClient
      Stretch = True
    end
    object LbLoteRetorno: TLabel
      Left = 8
      Top = 58
      Width = 89
      Height = 14
      Caption = 'Lote de Retorno'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbNomeLoteRetorno: TLabel
      Left = 198
      Top = 58
      Width = 89
      Height = 14
      Caption = 'Lote de Retorno'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbNossoNumero: TLabel
      Left = 8
      Top = 107
      Width = 82
      Height = 14
      Caption = 'Nosso N'#250'mero'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbASeuNumero: TLabel
      Left = 8
      Top = 127
      Width = 68
      Height = 14
      Caption = 'Seu N'#250'mero'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbADataVencimento: TLabel
      Left = 8
      Top = 150
      Width = 109
      Height = 14
      Caption = 'Data de Vencimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbAvalorDocumento: TLabel
      Left = 198
      Top = 151
      Width = 111
      Height = 14
      Caption = 'Valor do Documento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbTipoOcorrencia: TLabel
      Left = 8
      Top = 173
      Width = 104
      Height = 14
      Caption = 'Tipo de Ocorr'#234'ncia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbValorJuros: TLabel
      Left = 8
      Top = 196
      Width = 63
      Height = 14
      Caption = 'Valor Juros'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbValorDesconto: TLabel
      Left = 198
      Top = 197
      Width = 83
      Height = 14
      Caption = 'Valor Desconto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbValorAbatimento: TLabel
      Left = 387
      Top = 197
      Width = 95
      Height = 14
      Caption = 'Valor Abatimento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbValorIOF: TLabel
      Left = 8
      Top = 219
      Width = 48
      Height = 14
      Caption = 'Valor IOF'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbValorOutrasDespesas: TLabel
      Left = 198
      Top = 220
      Width = 91
      Height = 14
      Caption = 'Valor Outr. Desp.'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbValorOutrosCreditos: TLabel
      Left = 387
      Top = 220
      Width = 120
      Height = 14
      Caption = 'Valor Outros Cr'#233'ditos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbDataOcorrencia: TLabel
      Left = 8
      Top = 242
      Width = 103
      Height = 14
      Caption = 'Data de Ocorr'#234'ncia'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbDataCredito: TLabel
      Left = 198
      Top = 243
      Width = 84
      Height = 14
      Caption = 'Data de Cr'#233'dito'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbCODIGO: TLabel
      Left = 8
      Top = 8
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Lbnomeocorrencia: TLabel
      Left = 198
      Top = 174
      Width = 89
      Height = 14
      Caption = 'Lote de Retorno'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 192
      Top = 8
      Width = 66
      Height = 14
      Caption = 'Processado'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 84
      Top = 8
      Width = 35
      Height = 14
      Caption = 'Boleto'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 345
      Top = 3
      Width = 46
      Height = 14
      Caption = 'Situa'#231#227'o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 81
      Width = 68
      Height = 14
      Caption = 'Lan'#231'amento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbnomelancamento: TLabel
      Left = 198
      Top = 81
      Width = 68
      Height = 14
      Caption = 'Lan'#231'amento'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EdtLoteRetorno: TEdit
      Left = 122
      Top = 56
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 0
      OnExit = EdtLoteRetornoExit
      OnKeyDown = EdtLoteRetornoKeyDown
    end
    object EdtNossoNumero: TEdit
      Left = 122
      Top = 102
      Width = 458
      Height = 19
      MaxLength = 30
      TabOrder = 1
    end
    object EdtSeuNumero: TEdit
      Left = 122
      Top = 125
      Width = 458
      Height = 19
      MaxLength = 30
      TabOrder = 2
    end
    object EdtDataVencimento: TMaskEdit
      Left = 122
      Top = 148
      Width = 70
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object EdtvalorDocumento: TEdit
      Left = 314
      Top = 148
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 4
    end
    object EdtOcorrencia: TEdit
      Left = 122
      Top = 171
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 5
      OnExit = EdtOcorrenciaExit
      OnKeyDown = EdtOcorrenciaKeyDown
    end
    object EdtValorJuros: TEdit
      Left = 122
      Top = 194
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 6
    end
    object EdtValorDesconto: TEdit
      Left = 314
      Top = 194
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 7
    end
    object EdtValorAbatimento: TEdit
      Left = 512
      Top = 194
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 8
    end
    object EdtValorIOF: TEdit
      Left = 122
      Top = 217
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 9
    end
    object EdtValorOutrasDespesas: TEdit
      Left = 314
      Top = 217
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 10
    end
    object EdtValorOutrosCreditos: TEdit
      Left = 512
      Top = 217
      Width = 70
      Height = 19
      MaxLength = 9
      TabOrder = 11
    end
    object EdtDataOcorrencia: TMaskEdit
      Left = 122
      Top = 240
      Width = 70
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 12
      Text = '  /  /    '
    end
    object EdtDataCredito: TMaskEdit
      Left = 314
      Top = 240
      Width = 70
      Height = 19
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 13
      Text = '  /  /    '
    end
    object EdtCODIGO: TEdit
      Left = 8
      Top = 24
      Width = 72
      Height = 19
      MaxLength = 9
      TabOrder = 14
    end
    object ComboProcessado: TComboBox
      Left = 193
      Top = 24
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 15
      Text = '   '
      Items.Strings = (
        'N'#227'o'
        'Sim')
    end
    object edtboleto: TEdit
      Left = 84
      Top = 24
      Width = 72
      Height = 19
      Color = clSkyBlue
      MaxLength = 9
      TabOrder = 16
      OnKeyDown = edtboletoKeyDown
    end
    object edtsituacao: TEdit
      Left = 344
      Top = 24
      Width = 337
      Height = 19
      MaxLength = 30
      TabOrder = 17
    end
    object edtlancamento: TEdit
      Left = 122
      Top = 79
      Width = 70
      Height = 19
      Color = 6073854
      MaxLength = 9
      TabOrder = 18
      OnExit = edtlancamentoExit
      OnKeyDown = edtlancamentoKeyDown
    end
    object panelbotes: TPanel
      Left = 0
      Top = 0
      Width = 684
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      Color = clWindow
      TabOrder = 19
      object lbnomeformulario: TLabel
        Left = 548
        Top = 0
        Width = 136
        Height = 49
        Align = alRight
        Caption = 'Retorno de Cobran'#231'as'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGray
        Font.Height = -21
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
        WordWrap = True
      end
      object Btnovo: TBitBtn
        Left = 2
        Top = -3
        Width = 50
        Height = 52
        Caption = '&N'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = BtnovoClick
      end
      object btcancelar: TBitBtn
        Left = 152
        Top = -3
        Width = 50
        Height = 52
        Caption = '&C'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = btcancelarClick
      end
      object btgravar: TBitBtn
        Left = 102
        Top = -3
        Width = 50
        Height = 52
        Caption = '&G'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = btgravarClick
      end
      object btalterar: TBitBtn
        Left = 52
        Top = -3
        Width = 50
        Height = 52
        Caption = '&A'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = btalterarClick
      end
      object btsair: TBitBtn
        Left = 402
        Top = -3
        Width = 50
        Height = 52
        Caption = '&S'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 8
        OnClick = btsairClick
      end
      object btexcluir: TBitBtn
        Left = 202
        Top = -3
        Width = 50
        Height = 52
        Caption = '&E'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        OnClick = btexcluirClick
      end
      object btrelatorios: TBitBtn
        Left = 302
        Top = -3
        Width = 50
        Height = 52
        Caption = '&R'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
      end
      object btpesquisar: TBitBtn
        Left = 252
        Top = -3
        Width = 50
        Height = 52
        Caption = '&P'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        OnClick = btpesquisarClick
      end
      object btopcoes: TBitBtn
        Left = 352
        Top = -3
        Width = 50
        Height = 52
        Caption = '&O'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -1
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
      end
    end
  end
end
