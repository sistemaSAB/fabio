unit UobjCobreBem;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,ComObj,UObjPendencia, UDataModulo, UObjBoletoInstrucoes,
dialogs;

Type
   TObjCobreBem=class

          Public

                ObjBoletoInstrucoes : TObjBoletoINstrucoes;
                ObjPendencia:TObjPendencia;

                CobreBemX : Variant;
                Boleto:Variant;
                BoletoMeusDados:Variant;
                MDado : Variant;

                Constructor Create;
                Destructor  Free;

                Function ConfiguraContaCorrente(PConvenioBoleto:String):Boolean;
                Function RetornaNovoArquivoRemessaRetorno: String;
                Function GeraBoletoCobreBem(PBoletoBancario, PArquivoRemessaRetorno, PInstrucao:String;
                         ZerarMultaJuros :Boolean = False):Boolean;

//                function GeraBoletoCobreBem(PBoletoBancario, PArquivoRemessaRetorno, PInstrucao: String):Boolean;
                Function GeraArquivoRemessa(PArquivoRemessaRetorno,PConvenio:String):Boolean;

                Function ImprimeBoletos:Boolean;

         Private
              function RetornaNomeArquivo :String;
              procedure Corrige13CampoArquivoRemessa( pDiretorio, pArquivo: string );

   End;

implementation

uses Variants, SysUtils, UobjCONVENIOSBOLETO, UObjBoletoBancario,
  UobjARQUIVOREMESSARETORNO;

{ TObjCobreBem }


constructor TObjCobreBem.Create();
begin

    try
        Self.ObjPendencia:=TObjPendencia.Create;
        ObjBoletoInstrucoes := TObjBoletoINstrucoes.Create;
    except
        MensagemErro('Erro ao tentar criar o Objeto Cobre Bem');
        exit;
    end;

end;

function TObjCobreBem.ConfiguraContaCorrente(PConvenioBoleto:String): Boolean;
var
  codigoConvenio:string;
begin
  {  // Essas informa��es s�o cadastradas no cadastro de Convenio
     // Assim foi feito o Exemplo
     CobreBemX:=CreateOleObject('CobreBemX.ContaCorrente');
     //CobreBemX.ArquivoLicenca:='C:\CobreBemX\Exemplos\Licencas\001-11.conf';
     CobreBemX.ArquivoLicenca:='C:\CobreBemX\Exemplos\Licencas\05537538000153-001-11.conf';

     CobreBemX.CodigoAgencia:='3153-4';
     CobreBemX.NumeroContaCorrente:='00006432-7';
     CobreBemX.CodigoCedente:='001055143'; // Esse � o Codigo do Convenio est� no BBCobranca,
                                           // Completato com Zero A esquerda com 9 Digitos, Usar o CompletaPalavra_a_esquerda
     CobreBemX.InicioNossoNumero:='00001';
     CobreBemX.FimNossoNumero:='99999';
     CobreBemX.ProximoNossoNumero:='00013';
     CobreBemX.OutroDadoConfiguracao1:='019';
  }
     Result:=false;

     if (Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.LocalizaCodigo(PConvenioBoleto)=false)
     then Begin
              MensagemErro('Convenio n�o encontrado');
              exit;
     end;
     Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.TabelaparaObjeto;

     if (Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBem <> 'S')
     then Begin
              MensagemErro('Esse Convenio '+Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_Nome+' N�O � um convenio CobreBEM');
              exit;
     end;

     try
          Self.CobreBemX:=CreateOleObject('CobreBemX.ContaCorrente');
          Self.CobreBemX.ArquivoLicenca:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemArquivoLicenca;
          Self.CobreBemX.CodigoAgencia:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemCodigoAgencia;
          Self.CobreBemX.NumeroContaCorrente:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemNumeroContaCorrente;
          Self.CobreBemX.CodigoCedente:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemCodigoCedente;
          Self.CobreBemX.InicioNossoNumero:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemInicioNossoNumero;
          Self.CobreBemX.FimNossoNumero:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemFimNossoNumero;
          Self.CobreBemX.OutroDadoConfiguracao1:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemOutroDadoConfiguracao1;
          Self.CobreBemX.OutroDadoConfiguracao2:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemOutroDadoConfiguracao2;
          //codigoConvenio:=ObjPendencia.BoletoBancario.Convenioboleto.Get_CodigoBAnco;
          codigoConvenio:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco;

(*          if (codigoConvenio = '748')
          then begin
              Self.CobreBemX.OutroDadoConfiguracao2:= '3'; // Sicredi
          end
          else if (codigoConvenio = '001' )
          then begin //banco do brasil

              Self.CobreBemX.OutroDadoConfiguracao2:='';

              {boleto banco do brasil}
          end;

 *)
     except
          Exit;
     end;

     Result:=true;
end;


destructor TObjCobreBem.Free;
begin
     if (Self.ObjBoletoInstrucoes <> nil)
     then ObjBoletoInstrucoes.Free;

     if (Self.ObjPendencia <> nil)
     then Self.ObjPendencia.Free;

     Self.CobreBemX := Unassigned;
end;

function TObjCobreBem.GeraArquivoRemessa(PArquivoRemessaRetorno, PConvenio:String ): Boolean;
Var  PArquivo, PDiretorio, PCodigoBanco:String;
     NumeroRemessa:Integer;
begin
   Result:=false;
   try
       Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.LocalizaCodigo(PConvenio);
       Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.TabelaparaObjeto;

       PDiretorio:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemDiretorioRemessa;
       PCodigoBanco:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco;
       PArquivo  :=Self.RetornaNomeArquivo;

       Self.CobreBemX.ArquivoRemessa.Diretorio:=PDiretorio;
       Self.CobreBemX.ArquivoRemessa.Arquivo:=PArquivo;
       Self.CobreBemX.ArquivoRemessa.Layout:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemLayoutRemessa;
       Self.CobreBemX.ArquivoRemessa.Sequencia:=StrToInt(Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemNumeroRemessa);

       if (Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '756') then
       begin
         Self.Boleto.ValorJurosDiaAtraso :=  '0';
         Self.Boleto.ValorMultaAtraso := '0';

         Self.Boleto.PercentualJurosDiaAtraso :=  '0';
         Self.Boleto.PercentualMultaAtraso := '0';

       end;
       Self.CobreBemX.GravaArquivoRemessa;

       self.Corrige13CampoArquivoRemessa( PDiretorio, PArquivo );

   except
       exit;
   end;

   // O Numero do Arquivo de Remessa dever� ser Incrementa em 1
   // Como o Objeto j� est� preenchido � s� alterar
   Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Status:=dsEdit;
   NumeroRemessa:=StrToInt(Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemNumeroRemessa);
   Inc(NumeroRemessa,1);
   Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Submit_CobreBemNumeroRemessa(IntToStr(NumeroRemessa));
   if (Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Salvar(false)=false)
   then Begin
            MensagemErro('Erro ao tentar Atualizar o campo Numero da Remessa no cadastro do convenio');
            exit;
   end;

   if (Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.LocalizaCodigo(PArquivoRemessaRetorno)=false)
   then Begin
           MensagemErro('Codigo Arquivo Remessa n�o encontrado');
           exit;
   end;

   Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.TabelaparaObjeto(false);
   Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Status:=dsEdit;
   Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Submit_CodigoBanco(PCodigoBanco);
   //AKI //JONATAN MEDINA // ALTERA��O FEITA 01/07/2011
   Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Submit_ArquivoRemessa(PDiretorio+'/'+PArquivo);

   if (Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Salvar(false)=false)
   then Begin
           MensagemErro('Erro ao tentar Gravar o Arquivo de Remessa no Banco de Dados');
           exit;
   end;

   Result:=true;

end;

Function TObjCobreBem.RetornaNomeArquivo:String;
Begin

    if (Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '341') // ITAU
    then Begin
          Result:=Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.RetornaNomeArquivoItau(Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco,Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.get_codigocedente);
          exit;
    end;

    if (Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '104') then // CEF
    Begin
      Result:=Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.RetornaNomeArquivoCEF(Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco,Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.get_codigocedente);
      exit;
    end;

    // Sempre que esta fun��o for chamada o Objeto Convenio j� estar� preenchido
    if (Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemArquivoRemessa = '')
    then Begin
          MensagemErro('N�o existe um nome para o arquivo definido no cadastro de convenio. O arquivo ser� gerado com a data e a hora.');
          Result:= FormatDateTime('ddmmyyy',Now)+'_'+FormatDateTime('hhmmss',Time)+'.txt';
          exit;
    end;

    if (Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '748') // Sicredi
    then Begin
          Result:=Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.RetornaNomeArquivoSicredi(Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CodigoBAnco,Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.get_codigocedente);
          exit;
    end;


    Result:= Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemArquivoRemessa+'_'+FormatDateTime('ddmmyyy',Now)+'_'+FormatDateTime('hhmmss',Time)+'.txt';

end;



function TObjCobreBem.ImprimeBoletos: Boolean;
begin
     Result:=false;

     try
         CobreBemX.PadroesBoleto.PadroesBoletoImpresso.ArquivoLogotipo:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemArquivoLogotipo;
         CobreBemX.PadroesBoleto.PadroesBoletoImpresso.CaminhoImagensCodigoBarras:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemCaminhoImagensCodBarras;
         CobreBemX.PadroesBoleto.PadroesBoletoImpresso.LayoutBoleto:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemLayoutRemessa;
         CobreBemX.PadroesBoleto.PadroesBoletoImpresso.LayoutBoleto:=Self.ObjBoletoInstrucoes.BoletoBancario.Convenioboleto.Get_CobreBemLayoutBoleto;


         CobreBemX.ImprimeBoletos;



     except;
         exit;
     end;

     Result:=true;
end;

function TObjCobreBem.GeraBoletoCobreBem(PBoletoBancario, PArquivoRemessaRetorno, PInstrucao: String;
         ZerarMultaJuros :Boolean = False): Boolean;
//function TObjCobreBem.GeraBoletoCobreBem(PBoletoBancario, PArquivoRemessaRetorno, PInstrucao: String):Boolean;
Var NomeSacado,CpfSacado,RgSacado, Pinstrucoes, ProximoNossoNumeroAmanda:String;
    EnderecoSacado,BairroSacado,CepSacado,CidadeSacado,EstadoSacado,numerocasasacado:String;

  Valor, Juros, Multa :Currency;
begin
     // Essa funcao � usada por todos os metodos que precisam gerar boleto, tanto pra imprimir
     // quanto pra gerar remessa

     Result:=false;
     try
             if (Self.ObjBoletoInstrucoes.BoletoBancario.LocalizaCodigo(PBoletoBancario)=false)
             then Begin
                     MensagemErro('Boleto n�o encontrado');
                     exit;
             end;
             Self.ObjBoletoInstrucoes.BoletoBancario.TabelaparaObjeto;

             // Se vir o PArquivoRemessaRetorno � pooque ser� gerado o arquivo de Remessa
               if (PArquivoRemessaRetorno <> '')
             then Begin
                      Self.ObjBoletoInstrucoes.BoletoBancario.Status:=dsEdit;
                      Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Submit_Codigo(PArquivoRemessaRetorno);
                      Self.ObjBoletoInstrucoes.BoletoBancario.Salvar(false,'');

                      // Gravando na TabBoletoInstrucos pra ter um acompanhamento desse boleto
                      Self.ObjBoletoInstrucoes.ZerarTabela;
                      Self.ObjBoletoInstrucoes.Status:=dsInsert;
                      Self.ObjBoletoInstrucoes.Submit_Codigo(Self.ObjBoletoInstrucoes.Get_NovoCodigo);
                      Self.ObjBoletoInstrucoes.BoletoBancario.Submit_CODIGO(PBoletoBancario);
                      Self.ObjBoletoInstrucoes.ArquivoRemessaRetorno.Submit_Codigo(PArquivoRemessaRetorno);
                      Self.ObjBoletoInstrucoes.Submit_Instrucao(PInstrucao);
                      Self.ObjBoletoInstrucoes.Ocorrencia.Submit_CODIGO('');
                      Self.ObjBoletoInstrucoes.Submit_Data(DateToStr(Now));
                      if (Self.ObjBoletoInstrucoes.Salvar(false)=false)
                      then Begin
                              MensagemErro('Erro ao tentar salvar Boleto Instrucoes');
                              exit;
                      end;
             end;

             if (Self.ObjPendencia.LocalizaporBoleto(Self.ObjBoletoInstrucoes.BoletoBancario.Get_CODIGO)=false)
             then Begin
                       MensagemErro('Nenhuma pend�ncia localizada para o Boleto '+Self.ObjBoletoInstrucoes.BoletoBancario.Get_CODIGO);
                       exit;
             End;
             Self.ObjPendencia.TabelaparaObjeto;

             Self.Boleto:=Self.CobreBemX.DocumentosCobranca.Add;


             if (Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '748')
             then Begin
                     BoletoMeusDados:=Self.Boleto.MeusDados.Add; // Exigencia do Banco Sicred
                     BoletoMeusDados.Nome := 'CodigoPracaSacado'; // Instruido pelo pessoal do CobreBem
                     BoletoMeusDados.Valor := '000000';

                     MDado := Self.Boleto.MeusDados.Add;
                     MDado.Nome := 'NumeroBancoXMaiusculo';
                     MDado.Valor := 'S'; //;


                     Self.Boleto.TipoDocumentoCobranca:='DM';// Duplicata Mercantil por Indica��o, Manual deo Sicredi Pagina 10
             end;
             if (Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '756') then
             Begin

//                Boleto := CobreBemX.DocumentosCobranca.Add;

                MDado := Boleto.MeusDados.Add;
                MDado.Nome := 'CodigoConvenioLider';
                MDado.Valor := StringReplace(Self.CobreBemX.NumeroContaCorrente, '-', '', [rfReplaceAll]);
//                MDado.Valor := '0103900';

                Boleto.Aceite := '0';

             end;



             // ************* Informa��es do Convenio ****************
             if (Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CobreBemBancoImprimeBoleto = 'S')
             then Self.Boleto.BancoEmiteBoleto := true
             else Self.Boleto.BancoEmiteBoleto := false;

             Self.Boleto.DiasProtesto := StrToInt(Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CobreBemDiasProtesto);

             Self.Boleto.NumeroDocumento:=Self.ObjBoletoInstrucoes.BoletoBancario.Get_CODIGO;

             //*******************************************SACADO***********************************************************
             NomeSacado    :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_NomeCredorDevedor     (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
             CpfSacado     :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CPFCNPJCredorDevedor  (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
             RgSacado      :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_RGIECredorDevedor     (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);

             If(UsaEnderecoEnderecoCobrancaGlobal =false)
             Then Begin
                   EnderecoSacado:=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedor (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
                   BairroSacado  :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_BairroCredorDevedor   (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);;
                   CepSacado     :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CepCredorDevedor      (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);;
                   CidadeSacado  :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CidadeCredorDevedor   (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
                   EstadoSacado  :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_EstadoCredorDevedor   (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);;
                   numerocasasacado:=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_NumeroCasaCredorDevedor(Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
             End
             Else Begin
                   EnderecoSacado:=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_EnderecoCredorDevedorCobranca (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
                   BairroSacado  :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_BairroCredorDevedorCobranca   (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);;
                   CepSacado     :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CepCredorDevedorCobranca      (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);;
                   CidadeSacado  :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CidadeCredorDevedorCobranca   (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
                   EstadoSacado  :=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_EstadoCredorDevedorCobranca   (Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);;
                   numerocasasacado:=Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_NumeroCasaCredorDevedorCobranca(Self.ObjPendencia.Titulo.CREDORDEVEDOR.Get_CODIGO,Self.ObjPendencia.Titulo.get_CODIGOCREDORDEVEDOR);
             End;

             Self.Boleto.NomeSacado:=NomeSacado;
             Self.Boleto.EnderecoSacado:=EnderecoSacado+', '+Numerocasasacado;
             Self.Boleto.BairroSacado:=BairroSacado;
             Self.Boleto.CidadeSacado:=CidadeSacado;
             Self.Boleto.EstadoSacado:=EstadoSacado;
             Self.Boleto.CEPSacado:=Trim(StrReplace(CepSacado,'-',''));
             if (CepSacado = '')
             then  CepSacado:='00000000';
             CpfSacado:=tira_caracter(CpfSacado,'.');
             CpfSacado:=tira_caracter(CpfSacado,'/');
             CpfSacado:=tira_caracter(CpfSacado,'-');
             Self.Boleto.CPFSacado:= CpfSacado;
             //***********************************************************************************************************
             Self.Boleto.DataVencimento:=Self.ObjPendencia.BoletoBancario.Get_Vencimento;
             Self.Boleto.ValorDocumento:=Self.ObjPendencia.BoletoBancario.Get_ValorDoc;
             Self.Boleto.PadroesBoleto.Demonstrativo:=''; // Esse demonstrativo � para aquele casop do MArcelo de colocar as contas do condominio no boleto
             Self.Boleto.DataDocumento := Self.ObjPendencia.BoletoBancario.Get_DataDoc;
             Self.Boleto.DataProcessamento := Self.ObjPendencia.BoletoBancario.Get_DataProc;

             //gambiarra passada pelo suporte cobrebem para tentar resolver problema do layout na VGM
             if (Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '756')  and (ZerarMultaJuros) then
             begin
               Self.Boleto.ValorJurosDiaAtraso := 0;
               Self.Boleto.ValorMultaAtraso := 0;
               Self.Boleto.PercentualJurosDiaAtraso := 0;
               Self.Boleto.PercentualMultaAtraso := 0;
             end
             else begin
               Self.Boleto.PercentualJurosDiaAtraso :=  Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CobreBemPercJurosDiaAtraso;
               Self.Boleto.PercentualMultaAtraso := Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CobreBemPercMultaAtraso;
             end;

             Self.Boleto.PercentualDesconto := Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CobreBemPercentualDesconto;
             Self.Boleto.ValorOutrosAcrescimos := '0';

             // A Gera��o do Nosso Numero � feita assim: Sempre o nosso nbumro gerado estar� no cadasdtro
             // do convenio, e a cada vez que eu emitir um boleto
             // eu preciso atualizar esse campo no cadastro convenuoi
             Self.CobreBemX.ProximoNossoNumero:=CompletaPalavra_a_Esquerda(Self.ObjPendencia.BoletoBancario.Get_NossoNumeroCobreBem,4,'0');
             Self.CobreBemX.CalcularDadosBoletos;

             //gravar o mesmo nosso n�mero que esta indo no boleto

             if Self.ObjPendencia.BoletoBancario.Convenioboleto.Get_CodigoBAnco = '104' then
             begin
               Self.ObjPendencia.BoletoBancario.Status := dsEdit;
               Self.ObjPendencia.BoletoBancario.Submit_NumeroBoleto(Self.Boleto.NossoNumero);
               Self.ObjPendencia.BoletoBancario.Salvar(True, Self.Boleto.NossoNumero);
             end;


             // Para pular linha no cobre bem usa-se codigo HTML
             Pinstrucoes:=strreplace(Self.ObjPendencia.BoletoBancario.Get_Instrucoes,#13,'<br/>');
             Self.Boleto.PadroesBoleto.InstrucoesCaixa := Pinstrucoes;
             Self.Boleto.InstrucaoCobranca3:='2'; // Esse 2 � pra nao baixar titulo


             if (PInstrucao <> '')
             then Self.Boleto.AcaoCobrancaRemessa := PInstrucao;

     except
            exit;
     End;

     Result:=true;

end;


Function TObjCobreBem.RetornaNovoArquivoRemessaRetorno:String;
Var PArquivoRemessaRetorno:String;
Begin
    // Gerando um Registro na TabArquivoRemessaRetorno para ser gravado no beleto
    // como se fosse um LOTE de Boletos
    Result:='';
    Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.ZerarTabela;
    Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Status:=dsInsert;
    PArquivoRemessaRetorno:=Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Get_NovoCodigo;
    Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Submit_Codigo(PArquivoRemessaRetorno);
    Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Submit_Data(DateToStr(Now));
    if (Self.ObjBoletoInstrucoes.BoletoBancario.ArquivoRemessaRetorno.Salvar(false)=false)
    then Begin
             MensagemErro('Erro ao tentar gravar em ArquivoRemssaRetorno');
             exit;
    end;

    Result:=PArquivoRemessaRetorno;

end;


procedure TObjCobreBem.Corrige13CampoArquivoRemessa( pDiretorio, pArquivo: string );
var
  Arquivo: TStringList;
  linha, campo13: string;
begin
  {O objetivo deste m�todo � remover os caracteres em branco do nome do banco:
  "001BANCO DO BRASIL" que deve ser enviado "001BANCODOBRASIL  "
  }
  Arquivo := TStringList.Create;
  try
    pDiretorio := IncludeTrailingBackslash( pDiretorio );
    if not FileExists( pDiretorio + pArquivo ) then
      Exit;
    Arquivo.LoadFromFile( pDiretorio + pArquivo );
    if Arquivo.Count = 0 then
      Exit;
    linha := Copy( Arquivo[ 0 ], 1, 76 );
    campo13 := StringReplace( Copy( Arquivo[ 0 ], 77, 18 ), ' ', EmptyStr, [rfReplaceAll] );
    campo13 := CompletaPalavra( campo13, 18, ' ' );
    linha := linha + campo13 + Copy( Arquivo[ 0 ], 95, Length( Arquivo[ 0 ] ) );
    if Copy( campo13, 1, 3 ) <> '001' then
      Exit;
    if Length( linha ) <> 400 then
    begin
      MensagemErro( 'Erro ao remover espa�os em branco do campo 13 - Nome do Banco ' );
      Exit;
    end;
    Arquivo[ 0 ] := linha;
    DeleteFile(  pDiretorio + pArquivo  );
    Arquivo.SaveToFile(  pDiretorio + pArquivo );
  finally
    if Assigned( Arquivo ) then
      FreeAndNil( Arquivo );
  end;
end;

end.



