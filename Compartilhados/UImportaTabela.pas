unit UImportaTabela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Buttons,
  ComCtrls, ExtCtrls, 
  UfrImportaTXT, UfrImportaFirebird, StdCtrls;

type
  TFImportaTabela = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    FrImportaFirebird1: TFrImportaFirebird;
    FrImportaTXT1: TFrImportaTXT;
    TabSheet3: TTabSheet;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FrImportaTXT1btcaminhobancoClick(Sender: TObject);
    procedure FrImportaTXT1BitBtn2Click(Sender: TObject);
    procedure FrImportaFirebird1BitBtn2Click(Sender: TObject);
    procedure FrImportaTXT1Button2Click(Sender: TObject);
    procedure FrImportaTXT1btPegaLinhaClick(Sender: TObject);
    procedure FrImportaTXT1Button1Click(Sender: TObject);
    procedure FrImportaTXT1BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FImportaTabela: TFImportaTabela;

implementation



{$R *.dfm}


procedure TFImportaTabela.FormShow(Sender: TObject);
begin
     FrImportaFirebird1.InicializaFrame;
     FrImportaTXT1.InicializaFrame;
end;

procedure TFImportaTabela.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     FrImportaFirebird1.DestroiFrame;
     FrImportaTXT1.DestroiFrame;
end;

procedure TFImportaTabela.FrImportaTXT1btcaminhobancoClick(
  Sender: TObject);
begin
  FrImportaTXT1.btcaminhobancoClick(Sender);

end;

procedure TFImportaTabela.FrImportaTXT1BitBtn2Click(Sender: TObject);
begin
  FrImportaTXT1.BitBtn2Click(Sender);

end;



procedure TFImportaTabela.FrImportaFirebird1BitBtn2Click(Sender: TObject);
begin
  FrImportaFirebird1.BitBtn2Click(Sender);

end;

procedure TFImportaTabela.FrImportaTXT1Button2Click(Sender: TObject);
begin
  FrImportaTXT1.Button3Click(Sender);

end;

procedure TFImportaTabela.FrImportaTXT1btPegaLinhaClick(Sender: TObject);
begin
     FrImportaTXT1.btPegaLinhaClick(sender);
end;

procedure TFImportaTabela.FrImportaTXT1Button1Click(Sender: TObject);
begin
  FrImportaTXT1.Button1Click(Sender);

end;

procedure TFImportaTabela.FrImportaTXT1BitBtn1Click(Sender: TObject);
begin
  FrImportaTXT1.BitBtn1Click(Sender);

end;

end.
