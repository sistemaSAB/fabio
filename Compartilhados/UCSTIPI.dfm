object FCSTIPI: TFCSTIPI
  Left = 442
  Top = 308
  Width = 978
  Height = 268
  Caption = 'Cadastro de CSTIPI - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ImagemFundo: TImage
    Left = 0
    Top = 50
    Width = 962
    Height = 130
    Align = alClient
    Stretch = True
  end
  object LbCodigo: TLabel
    Left = 29
    Top = 76
    Width = 44
    Height = 13
    Caption = 'C'#243'digo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbDescricao: TLabel
    Left = 141
    Top = 76
    Width = 64
    Height = 13
    Caption = 'Descri'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object panelrodape: TPanel
    Left = 0
    Top = 180
    Width = 962
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      962
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 962
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidadeformulario: TLabel
      Left = 683
      Top = 16
      Width = 267
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X CSTIPI cadastrados'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 962
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    DesignSize = (
      962
      50)
    object lbnomeformulario: TLabel
      Left = 464
      Top = 7
      Width = 448
      Height = 18
      Alignment = taRightJustify
      Anchors = []
      Caption = 'C'#243'digo da situa'#231#227'o tribut'#225'ria referente ao imposto '
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 492
      Top = 27
      Width = 369
      Height = 18
      Alignment = taRightJustify
      Anchors = []
      Caption = 'sobre produtos industrializados (CST-IPI)'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Btnovo: TBitBtn
      Left = 3
      Top = -3
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btpesquisar: TBitBtn
      Left = 253
      Top = -3
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 303
      Top = -3
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btalterar: TBitBtn
      Left = 53
      Top = -3
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btexcluir: TBitBtn
      Left = 203
      Top = -3
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btgravar: TBitBtn
      Left = 103
      Top = -3
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 153
      Top = -3
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btsair: TBitBtn
      Left = 403
      Top = -3
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object btopcoes: TBitBtn
      Left = 353
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
  end
  object EdtCodigo: TEdit
    Left = 29
    Top = 92
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 2
  end
  object EdtDescricao: TEdit
    Left = 141
    Top = 92
    Width = 444
    Height = 19
    MaxLength = 300
    TabOrder = 3
  end
end
