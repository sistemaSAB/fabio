unit Ufiltraordem;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ExtCtrls, jpeg, ComCtrls;

type
  TFfiltraOrdem = class(TForm)
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    Grupo01: TPanel;
    LbGrupo01: TLabel;
    ComboGrupo01: TComboBox;
    Grupo02: TPanel;
    LbGrupo02: TLabel;
    ComboGrupo02: TComboBox;
    Grupo03: TPanel;
    LbGrupo03: TLabel;
    ComboGrupo03: TComboBox;
    Grupo04: TPanel;
    LbGrupo04: TLabel;
    ComboGrupo04: TComboBox;
    Grupo05: TPanel;
    LbGrupo05: TLabel;
    ComboGrupo05: TComboBox;
    Grupo06: TPanel;
    LbGrupo06: TLabel;
    ComboGrupo06: TComboBox;
    Grupo07: TPanel;
    LbGrupo07: TLabel;
    ComboGrupo07: TComboBox;
    Grupo08: TPanel;
    LbGrupo08: TLabel;
    ComboGrupo08: TComboBox;
    Grupo09: TPanel;
    LbGrupo09: TLabel;
    ComboGrupo09: TComboBox;
    Grupo10: TPanel;
    LbGrupo10: TLabel;
    ComboGrupo10: TComboBox;
    procedure FormActivate(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private

    { Private declarations }
  Public
    { Public declarations }
    Procedure DesativaGrupos;
  End;

var
  FfiltraOrdem: TFfiltraOrdem;


implementation

uses UessencialGlobal, UescolheImagemBotao;

{$R *.DFM}

procedure TFfiltraOrdem.FormActivate(Sender: TObject);
var
poptop:integer;
Conta:integer;
primeiro:Boolean;
verificatamanho:boolean;
begin
     verificatamanho:=False;
     FescolheImagemBotao.PegaFiguraBotoes(nil,nil,btcancelar,btgravar,nil,nil,nil,nil);
     primeiro:=false;
     Tag:=0;
     conta:=0;
     poptop:=0;//seria o primeiro grupo
     //aqui eu verifico grupo a grupo quais estao enabled=true
     //os que nao estiverem eu dou um visible=false
     //os que estiverem eu alinho-os de acordo com propriedade top
     //entre um e outro � 42


     If (Grupo01.Enabled=True)
     Then Begin
               Grupo01.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo01.Visible:=True;
               combogrupo01.setfocus;
               primeiro:=True;

     End
     Else Grupo01.Visible:=False;

     If (Grupo02.Enabled=True)
     Then Begin
               Grupo02.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo02.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         combogrupo02.setfocus;
               End;
     End
     Else Grupo02.Visible:=False;

     If (Grupo03.Enabled=True)
     Then Begin
               Grupo03.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo03.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         combogrupo03.setfocus;
               End;

          End
     Else Grupo03.Visible:=False;

     If (Grupo04.Enabled=True)
     Then Begin
               Grupo04.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo04.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        combogrupo04.setfocus;
               End;

          End
     Else Grupo04.Visible:=False;

     If (Grupo05.Enabled=True)
     Then Begin
               Grupo05.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo05.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        combogrupo05.setfocus;
               End;

          End
     Else Grupo05.Visible:=False;

     If (Grupo06.Enabled=True)
     Then Begin
               Grupo06.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo06.Visible:=True;
               If (primeiro=False)
               Then Begin
                        primeiro:=True;
                        combogrupo06.setfocus;
               End;
          End
     Else Grupo06.Visible:=False;

     If (Grupo07.Enabled=True)
     Then Begin
               Grupo07.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo07.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        combogrupo07.setfocus;
               End;

          End
     Else Grupo07.Visible:=False;

     If (Grupo08.Enabled=True)
     Then Begin
               Grupo08.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo08.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         combogrupo08.setfocus;
               End;
          End
     Else Grupo08.Visible:=False;

     If (Grupo09.Enabled=True)
     Then Begin
               Grupo09.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo09.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         combogrupo09.setfocus;
               End;

          End
     Else Grupo09.Visible:=False;

     If (Grupo10.Enabled=True)
     Then Begin
               Grupo10.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo10.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         combogrupo10.setfocus;
               End;

          End
     Else Grupo10.Visible:=False;

     Self.Height:=conta*41;


     If (conta<=1)
     Then self.Height:=100;
     Self.Height:=Self.Height+50;

     {if FfiltroImp.Height<189
     Then FfiltroImp.Height:=189;}

end;

procedure TFfiltraOrdem.BtgravarClick(Sender: TObject);
begin
     Tag:=1;
     close;
End;

procedure TFfiltraOrdem.BtCancelarClick(Sender: TObject);
begin
     Tag:=0;
     close;
end;

procedure TFfiltraOrdem.DesativaGrupos;
begin
     limpaedit(self);

     Grupo01.Enabled:=False;
     lbgrupo01.Caption:='';
     ComboGrupo01.Items.clear;

     Grupo02.Enabled:=False;
     lbgrupo02.Caption:='';
     ComboGrupo02.Items.clear;

     Grupo03.Enabled:=False;
     lbgrupo03.Caption:='';
     ComboGrupo03.Items.clear;

     Grupo04.Enabled:=False;
     lbgrupo04.Caption:='';
     ComboGrupo04.Items.clear;

     Grupo05.Enabled:=False;
     lbgrupo05.Caption:='';
     ComboGrupo05.Items.clear;

     Grupo06.Enabled:=False;
     lbgrupo06.Caption:='';
     ComboGrupo06.Items.clear;

     Grupo07.Enabled:=False;
     lbgrupo07.Caption:='';
     ComboGrupo07.Items.clear;

     Grupo08.Enabled:=False;
     lbgrupo08.Caption:='';
     ComboGrupo08.Items.clear;

     Grupo09.Enabled:=False;
     lbgrupo09.Caption:='';
     ComboGrupo09.Items.clear;

     Grupo10.Enabled:=False;
     lbgrupo10.Caption:='';
     ComboGrupo10.Items.clear;


End;

procedure TFfiltraOrdem.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else
         if (key=#27)
         Then Begin
                  Self.Tag:=0;
                  Self.close;
         End;
end;




procedure TFfiltraOrdem.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
     
end;


end.
