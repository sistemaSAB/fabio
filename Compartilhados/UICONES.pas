unit UICONES;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjICONES,
  jpeg;

type
  TFICONES = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbPROCEDIMENTO: TLabel;
    EdtPROCEDIMENTO: TEdit;
    LbUSUARIO: TLabel;
    EdtUSUARIO: TEdit;
    LbGRUPOUSUARIO: TLabel;
    EdtGRUPOUSUARIO: TEdit;
    LbTamanhoHorizontal: TLabel;
    EdtTamanhoHorizontal: TEdit;
    LbTamanhoVertical: TLabel;
    EdtTamanhoVertical: TEdit;
    LbPosicaoEsquerda: TLabel;
    EdtPosicaoEsquerda: TEdit;
    LbPosicaoSuperior: TLabel;
    EdtPosicaoSuperior: TEdit;
    LbCodigoImagem: TLabel;
    edtnomeimagem: TEdit;
    LbTexto: TLabel;
    EdtTexto: TEdit;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjICONES:TObjICONES;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FICONES: TFICONES;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFICONES.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjICONES do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_PROCEDIMENTO(edtPROCEDIMENTO.text);
        Submit_USUARIO(edtUSUARIO.text);
        Submit_GRUPOUSUARIO(edtGRUPOUSUARIO.text);
        Submit_TamanhoHorizontal(edtTamanhoHorizontal.text);
        Submit_TamanhoVertical(edtTamanhoVertical.text);
        Submit_PosicaoEsquerda(edtPosicaoEsquerda.text);
        Submit_PosicaoSuperior(edtPosicaoSuperior.text);
        Submit_NomeImagem(edtNomeImagem.text);
        Submit_Texto(edtTexto.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFICONES.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjICONES do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtPROCEDIMENTO.text:=Get_PROCEDIMENTO;
        EdtUSUARIO.text:=Get_USUARIO;
        EdtGRUPOUSUARIO.text:=Get_GRUPOUSUARIO;
        EdtTamanhoHorizontal.text:=Get_TamanhoHorizontal;
        EdtTamanhoVertical.text:=Get_TamanhoVertical;
        EdtPosicaoEsquerda.text:=Get_PosicaoEsquerda;
        EdtPosicaoSuperior.text:=Get_PosicaoSuperior;
        EdtNomeImagem.text:=Get_NomeImagem;
        EdtTexto.text:=Get_Texto;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFICONES.TabelaParaControles: Boolean;
begin
     If (Self.ObjICONES.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFICONES.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjICONES:=TObjICONES.create(nil,nil);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFICONES.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjICONES=Nil)
     Then exit;

If (Self.ObjICONES.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjICONES.free;
end;

procedure TFICONES.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFICONES.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjICONES.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjICONES.status:=dsInsert;
     Guia.pageindex:=0;
     edtPROCEDIMENTO.setfocus;

end;


procedure TFICONES.btalterarClick(Sender: TObject);
begin
    If (Self.ObjICONES.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjICONES.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtPROCEDIMENTO.setfocus;
                
          End;


end;

procedure TFICONES.btgravarClick(Sender: TObject);
begin

     If Self.ObjICONES.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjICONES.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjICONES.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFICONES.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjICONES.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjICONES.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjICONES.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFICONES.btcancelarClick(Sender: TObject);
begin
     Self.ObjICONES.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFICONES.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFICONES.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjICONES.Get_pesquisa,Self.ObjICONES.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjICONES.status<>dsinactive
                                  then exit;

                                  If (Self.ObjICONES.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjICONES.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFICONES.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFICONES.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Self.ObjICONES.OBJETO.Get_Pesquisa,Self.ObjICONES.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjICONES.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjICONES.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjICONES.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
