unit uTextoFormatado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls;

type
  TfTextoFormatado = class(TForm)
    Panel1: TPanel;
    Memo1: TMemo;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    procedure BitBtn2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fTextoFormatado: TfTextoFormatado;

implementation

{$R *.dfm}

procedure TfTextoFormatado.BitBtn2Click(Sender: TObject);
begin
  self.close;
end;

procedure TfTextoFormatado.FormShow(Sender: TObject);
begin
  Self.Tag := 0;
end;

procedure TfTextoFormatado.BitBtn1Click(Sender: TObject);
begin
  self.Tag := 1;
  Self.Close;
end;

end.
