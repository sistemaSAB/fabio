object FICONES: TFICONES
  Left = 260
  Top = 229
  Width = 506
  Height = 381
  Caption = 'Cadastro de '#205'cones - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 264
    Width = 498
    Height = 90
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object Btnovo: TBitBtn
      Left = 3
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 115
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 227
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 339
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 115
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 227
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 339
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btsairClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 498
    Height = 264
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = 'Principal'
      object LbCODIGO: TLabel
        Left = 21
        Top = 12
        Width = 44
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbPROCEDIMENTO: TLabel
        Left = 21
        Top = 34
        Width = 90
        Height = 13
        Caption = 'Procedimento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbUSUARIO: TLabel
        Left = 21
        Top = 56
        Width = 50
        Height = 13
        Caption = 'Usu'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbGRUPOUSUARIO: TLabel
        Left = 21
        Top = 78
        Width = 113
        Height = 13
        Caption = 'Grupo do Usu'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbTamanhoHorizontal: TLabel
        Left = 21
        Top = 100
        Width = 131
        Height = 13
        Caption = 'Tamanho Horizontal'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbTamanhoVertical: TLabel
        Left = 21
        Top = 122
        Width = 114
        Height = 13
        Caption = 'Tamanho Vertical'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbPosicaoEsquerda: TLabel
        Left = 21
        Top = 144
        Width = 115
        Height = 13
        Caption = 'Posi'#231#227'o Esquerda'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbPosicaoSuperior: TLabel
        Left = 21
        Top = 166
        Width = 110
        Height = 13
        Caption = 'Posi'#231#227'o Superior'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbCodigoImagem: TLabel
        Left = 21
        Top = 188
        Width = 115
        Height = 13
        Caption = 'Nome da Imagem'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbTexto: TLabel
        Left = 21
        Top = 210
        Width = 37
        Height = 13
        Caption = 'Texto'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 160
        Top = 12
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 9
      end
      object EdtPROCEDIMENTO: TEdit
        Left = 160
        Top = 34
        Width = 300
        Height = 19
        MaxLength = 200
        TabOrder = 0
      end
      object EdtUSUARIO: TEdit
        Left = 160
        Top = 56
        Width = 300
        Height = 19
        MaxLength = 100
        TabOrder = 1
      end
      object EdtGRUPOUSUARIO: TEdit
        Left = 160
        Top = 78
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 2
      end
      object EdtTamanhoHorizontal: TEdit
        Left = 160
        Top = 100
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 3
      end
      object EdtTamanhoVertical: TEdit
        Left = 160
        Top = 122
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 4
      end
      object EdtPosicaoEsquerda: TEdit
        Left = 160
        Top = 144
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 5
      end
      object EdtPosicaoSuperior: TEdit
        Left = 160
        Top = 166
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 6
      end
      object edtnomeimagem: TEdit
        Left = 160
        Top = 188
        Width = 299
        Height = 19
        MaxLength = 9
        TabOrder = 7
      end
      object EdtTexto: TEdit
        Left = 160
        Top = 210
        Width = 300
        Height = 19
        MaxLength = 20
        TabOrder = 8
      end
    end
  end
end
