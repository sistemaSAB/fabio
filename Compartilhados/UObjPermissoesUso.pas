unit UObjPermissoesUso;
Interface
Uses Classes,Db,Ibcustomdataset,UObjNiveis,uessencialglobal,uobjpermissoesextras,ibquery;

Type

   TObjPermissoesUso=class

          Public
                //ObjDatasource                             :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ObjpermissoesExtras:tobjpermissoesextras;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;


                Function    exclui(Pcodigo:string;ComCommit:boolean):Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                //Function    Get_PesquisaTurma             :TStringList;
               // Function    Get_TituloPesquisaTurma       :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function Get_CODIGO           :string;
                Procedure Submit_CODIGO           (parametro:string);
                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                function Get_nome: string;
                procedure Submit_nome(parametro: string);
                Function VerificaPermissao: boolean;
                Function Get_nivel:string;
                Function Get_nomenivel:String;

                Procedure  Submit_nivel(parametro:string);

                Function Get_ControlaPorNivel  :string;
                Procedure Submit_ControlaPorNivel(Parametro:string);

                function Get_NIvelPesquisa: TStringList;
                function Get_NivelTituloPesquisa: string;
                procedure Opcoes;
                Function ValidaPermissao(NOMEPERMISSAO:string):boolean;overload;
                Function ValidaPermissao_Silenciosa(NOMEPERMISSAO:string):boolean;
                function PedePermissao(Pcodigo: string; out PUsuarioAutorizou: String;Pfrase: String): boolean;

         Private
               ObjDataset:Tibdataset;
               Objquery:Tibquery;
               //por causa de erros de excecao joguei paraprivate
               nivel	                                    :TObjNiveis;

               CODIGO           :string;
               nome             :string;
               ControlaPorNivel  :string;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                procedure Exporta;
                Function  ValidaPermissao(NOMEPERMISSAO:string;silenciosa:boolean):boolean;overload;
                procedure ApagaPermissoesDuplicadas;
                Function  LocalizaNome(Pnome:String) :boolean;

   End;


implementation
uses forms,uopcaorel,stdctrls,SysUtils,Dialogs,UDatamodulo,Controls,
  UFiltraImp, UMostraBarraProgresso;


{ TTabTitulo }


Function  TObjPermissoesUso.TabelaparaObjeto:Boolean;//ok
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;

        If(FieldByName('nivel').asstring<>'')
        Then Begin
                If (Self.nivel.LocalizaCodigo(FieldByName('nivel').asstring)=False)
                Then Begin
                          Messagedlg('nivel N�o encontrado!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                     End
                Else Self.nivel.TabelaparaObjeto;
        End;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.nome:=fieldbyname('nome').asstring;
        Self.ControlaPorNivel:=fieldbyname('ControlaPorNivel').asstring;
        result:=True;

     End;
end;


Procedure TObjPermissoesUso.ObjetoparaTabela;//ok
begin
  With ObjDataset do
  Begin

        fieldbyname('nivel').asstring:=Self.nivel.Get_codigo;
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('nome').asstring:=Self.nome;
        fieldbyname('ControlaPorNivel').asstring:=Self.ControlaPorNivel;

  End;
End;

//***********************************************************************

function TObjPermissoesUso.Salvar(ComCommit:Boolean): Boolean;//Ok
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;






if Self.status=dsinsert
then begin
          if (Self.codigo='0')
          Then Self.codigo:=Self.Get_NovoCodigo;
            Self.ObjDataset.Insert//libera para insercao
End
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjPermissoesUso.ZerarTabela;//Ok
Begin
     With Self do
     Begin

        Self.nivel.ZerarTabela;
        Self.CODIGO:='';
        Self.nome:='';
        Self.ControlaPorNivel:='';

     End;
end;

Function TObjPermissoesUso.VerificaBrancos:boolean;
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
       If (codigo='')
       Then Mensagem:=mensagem+'/C�digo';

       if (Self.ControlaPorNivel='')
       Then Self.ControlaPorNivel:='S';

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjPermissoesUso.VerificaRelacionamentos: Boolean;
var
mensagem:string;
Begin
     mensagem:='';
     If (Self.nivel.LocalizaCodigo(Self.nivel.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ N�vel n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjPermissoesUso.VerificaNumericos: Boolean;
var
   Inteiros:Integer;
   Reais:Currency;
   Mensagem:string;
begin
     Mensagem:='';

     try
        Inteiros:=Strtoint(Self.codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjPermissoesUso.VerificaData: Boolean;
var
Datas:Tdate;
Horas:Ttime;
Mensagem:string;
begin
     mensagem:='';

{     Try
        Datas:=strtodate(Self.data);
     Except
           Mensagem:=mensagem+'/Data';
     End;

     Try
        Horas:=strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;}

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjPermissoesUso.VerificaFaixa: boolean;
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     if ((Self.ControlaPorNivel<>'S') and (Self.ControlaPorNivel<>'N'))
     Then mensagem:=mensagem+'/ O campo Controla por N�vel cont�m valores inv�lidos';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;


function TObjPermissoesUso.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
                SelectSql.clear;
                SelectSQL.ADD('Select nivel,CODIGO,nome,ControlaPorNivel');
                SelectSQL.ADD(' from  TabPermissoesUso');
                SelectSQL.ADD(' WHERE codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjPermissoesUso.LocalizaNome(Pnome:String): boolean;//ok
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.clear;
           SelectSQL.ADD('Select nivel,CODIGO,nome,ControlaPorNivel');
           SelectSQL.ADD(' from  TabPermissoesUso');
           SelectSQL.ADD(' WHERE nome='+#39+pnome+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;



procedure TObjPermissoesUso.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPermissoesUso.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPermissoesUso.create;
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;

        Self.Objquery:=Tibquery.create(nil);
        Self.Objquery.database:=FdataModulo.ibdatabase;


        Self.ParametroPesquisa:=TStringList.create;
        Self.nivel:=TObjNiveis.create;
        Self.ObjpermissoesExtras:=tobjpermissoesextras.create;
        ZerarTabela;

        With Self.ObjDataset do
        Begin
              SelectSql.clear;
                SelectSQL.ADD('Select CODIGO,nivel,nome,ControlaPorNivel');
                SelectSQL.ADD(' from  TabPermissoesUso');
                SelectSQL.ADD(' WHERE codigo=0');
                InsertSql.clear;
                InsertSQL.add('Insert Into TabPermissoesUso(CODIGO,nivel,nome,ControlaPorNivel)');
                InsertSQL.add('values (:CODIGO,:nivel,:nome,:ControlaPorNivel)');
                ModifySQL.clear;
                ModifySQL.add('Update TabPermissoesUso set CODIGO=:CODIGO,nivel=:nivel,nome=:nome,ControlaPorNivel=:ControlaPorNivel');
                ModifySQL.add('where codigo=:codigo');
                DeleteSql.clear;
                DeleteSql.add('Delete from TabPermissoesUso where codigo=:codigo ');
                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,nivel,nome,ControlaPorNivel');
                RefreshSQL.ADD(' from  TabPermissoesUso');
                RefreshSQL.ADD(' WHERE codigo=0');
                open;
                Self.ObjDataset.First ;
                Self.status:=dsInactive;
        End;

end;


//FUNCOES QUE ENVIAM DADOS DA TABELA PARA OBJETO E DO OBJETO PAR TABELA
function TObjPermissoesUso.Get_CODIGO: string;
begin
        Result:=Self.Codigo;
end;

procedure TObjPermissoesUso.Submit_CODIGO(parametro: string);
begin
        Self.Codigo:=parametro;
end;


procedure TObjPermissoesUso.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPermissoesUso.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPermissoesUso');
     Result:=Self.ParametroPesquisa;
end;

function TObjPermissoesUso.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Permiss�es de Uso ';
end;


function TObjPermissoesUso.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(Genpermissoesuso,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
end;


destructor TObjPermissoesUso.Free;
begin
   Freeandnil(Self.ObjDataset);
   Freeandnil(Self.ParametroPesquisa);
   Freeandnil(Self.Objquery);

   Self.nivel.Free;
   Self.ObjpermissoesExtras.free;
   
end;

function TObjPermissoesUso.RetornaCampoCodigo: string;
begin
     result:='codigo';
end;

function TObjPermissoesUso.RetornaCampoNome: string;
begin
     result:='nome';
end;

procedure TObjPermissoesUso.Submit_nome(parametro: string);
begin
        Self.nome:=Parametro;
end;
function TObjPermissoesUso.Get_nome: string;
begin
        Result:=Self.nome;
end;

function TObjPermissoesUso.VerificaPermissao: boolean;
begin
     result:=Self.ValidaPermissao('ACESSO AO CADASTRO DE PERMISSOES DE USO');
end;


function TObjPermissoesUso.Get_nivel: string;
begin
     Result:=Self.nivel.get_codigo;
end;

procedure TObjPermissoesUso.Submit_nivel(parametro: string);
begin
     Self.nivel.submit_codigo(parametro);
end;
function TObjPermissoesUso.Get_NIvelPesquisa: TStringList;
begin
     result:=Self.nivel.get_pesquisa;
end;

function TObjPermissoesUso.Get_NivelTituloPesquisa: string;
begin
     result:=Self.nivel.get_titulopesquisa;
end;



procedure TObjPermissoesUso.Opcoes;
begin
     With FopcaoRel do
     Begin
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Exporta todas as permiss�es');//1
                items.add('Apaga permiss�es iguais');//2

          End;
          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of

                0:Self.Exporta;
                1:Self.ApagaPermissoesDuplicadas;

          End;

     end;


end;

procedure TObjPermissoesUso.ApagaPermissoesDuplicadas;
Begin
     With Self.Objquery do
     Begin
          close;
          SQL.clear;
          SQL.add('Select nome,count(codigo) from tabpermissoesuso');
          SQL.add('group by nome having count(Codigo)>1');
          open;
          if (recordcount=0)
          Then Begin
                    MensagemAviso('N�o foi encontrado permiss�es duplicadas');
                    exit;
          End;
          last;
          FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
          FMostraBarraProgresso.lbmensagem.caption:='Aguarde....';
          first;



          Try
            While not(eof) do
            Begin
                 FMostraBarraProgresso.IncrementaBarra1(1);
                 FMostraBarraProgresso.show;
                 Application.ProcessMessages;

                 if (Self.LocalizaNome(fieldbyname('nome').asstring)=True)
                 Then Begin
                           Self.TabelaparaObjeto;
                           if(Self.exclui(Self.codigo,true)=false)
                           Then Begin
                                     MensagemErro('Erro na tentativa de Excluir a Permiss�o '+Self.codigo+'-'+Self.nome);
                                     exit;
                           End;
                 End;
                 next;
              End;
          Finally
                FMostraBarraProgresso.close;
          End;
          



     End;
End;

procedure TObjPermissoesUso.Exporta;
var
arq:textfile;
path:string;
begin
     path:='';
     path:=ExtractFilePath(application.exename);
     if (path[length(path)]<>'\')
     Then path:=path+'\';
     path:=path+'PERMISSOESUSO.txt';
     AssignFile(arq,path);
     Try
        Rewrite(arq);
     Except
           Messagedlg('N�o foi poss�vel criar o arquivo '+path,mterror,[mbok],0);
           exit;
     End;
     Try
        With Self.objdataset do
        Begin
             close;
             SelectSQL.clear;
             SelectSQL.add('Select * from Tabpermissoesuso order by codigo');
             open;
             first;
             While not(eof) do
             Begin
                Writeln(arq,'Insert into tabPermissoesuso (codigo,nome,nivel) values (0,'''+fieldbyname('nome').asstring+''','''+fieldbyname('nivel').asstring+''');');
                next;
             End;
             Messagedlg('Exporta��o Conclu�da!',mtinformation,[mbok],0);
             exit;
        End;
     Finally
            CloseFile(arq);
     End;

end;

function TObjPermissoesUso.ValidaPermissao_Silenciosa(
  NOMEPERMISSAO: string): boolean;
begin
     result:=Self.ValidaPermissao(nomepermissao,true)
end;

function TObjPermissoesUso.ValidaPermissao(NOMEPERMISSAO: string): boolean;
Begin
     result:=Self.validapermissao(nomepermissao,false);
End;

function TObjPermissoesUso.ValidaPermissao(NOMEPERMISSAO: string;silenciosa:boolean): boolean;
var
erro:boolean;
TmpNivelUsuario,TmpNIvelPermissao:integer;
begin
     result:=False;

     If (Self.LocalizaNome(uppercase(nomepermissao))=False)
     Then Begin
               Messagedlg('Permiss�o n�o encontrada!'+#13+'"'+NOMEPERMISSAO+'"',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;


     //Verifico se tem uma permissao extra, se tiver nem me preocupo com o nivel do usuario 
     if (Self.ObjpermissoesExtras.LocalizaPermissaoUsuario(Self.CODIGO,ObjUsuarioGlobal.get_codigo)=True)
     Then erro:=False
     Else Begin
              erro:=True;
              //nao tem permissao extra
              if (Self.Get_ControlaPorNivel='S')
              Then Begin
                       TmpNivelUsuario:=strtoint(ObjUsuarioGlobal.get_nivel);
                       TmpNIvelPermissao:=strtoint(Self.Get_nivel);

                       If (Tmpnivelusuario<=Tmpnivelpermissao)
                       Then Erro:=False;//tem permissao para isso
              End;
     End;


     if (erro=True)
     Then Begin
               if (silenciosa=False)
               Then Begin
                         Messagedlg('O usu�rio atual ('+ObjUsuarioGlobal.get_nome+') n�o tem permiss�o para Realizar a Opera��o:'+Self.codigo+#13+
                         '"'+NOMEPERMISSAO+'"',mtinformation,[mbok],0);
               End;
               exit;
     End
     Else result:=True;
End;

function TObjPermissoesUso.Get_ControlaPorNivel: string;
begin
     Result:=Self.ControlaPorNivel;
end;

procedure TObjPermissoesUso.Submit_ControlaPorNivel(Parametro: string);
begin
     Self.ControlaPorNivel:=parametro;
end;

function TObjPermissoesUso.Get_nomenivel: String;
begin
     result:=Self.nivel.Get_nome;
end;


function TobjpermissoesUso.PedePermissao(Pcodigo:string;out PUsuarioAutorizou: String;Pfrase:String): boolean;
var
PCaptionAnterior,UsuarioAtual:String;
PsenhaTemp:String;
begin
     result:=False;
     //Pede permissao para a permissao em parametro

     Self.LocalizaCodigo(pcodigo);
     Self.TabelaparaObjeto;

     UsuarioAtual:=ObjUsuarioGlobal.get_codigo;

     Try
        limpaedit(FfiltroImp);
        With FfiltroImp do
        Begin
             DesativaGrupos;

             Grupo01.enabled:=True;
             Grupo02.enabled:=True;

             edtgrupo01.EditMask:='';
             edtgrupo02.EditMask:='';
             edtgrupo02.PasswordChar:='#';
             edtgrupo01.OnKeyDown:=nil;
             edtgrupo01.OnkeyPress:=Nil;
             edtgrupo02.OnKeyDown:=nil;
             edtgrupo02.OnkeyPress:=Nil;

             edtgrupo01.CharCase:=ecUpperCase;
             LbGrupo01.caption:='Usu�rio';
             LbGrupo02.caption:='Senha';

             PcaptionAnterior:=Caption;
             caption:=pfrase;
             showmodal;
             
             caption:=PCaptionAnterior;

             if (tag=0)
             then exit;

             if (edtgrupo01.text='')
             Then Begin
                       Messagedlg('Usu�rio n�o encontrado!',mterror,[mbok],0);
                       exit;
             End;

             if (ObjUsuarioGlobal.LocalizaNome(edtgrupo01.text)=False)
             Then Begin
                       MEssagedlg('Usu�rio n�o encontrado!',mterror,[mbok],0);
                       exit;
              End;

              ObjUsuarioGlobal.TabelaparaObjeto;
              
              //a senha ja foi desincriptada no tabela para objeto
              //so falta trocar as letras
              PsenhaTemp:=ObjUsuarioGlobal.get_Senha;

              if (PsenhaTemp<>edtgrupo02.text)
              Then Begin
                        Messagedlg('Senha Inv�lida!',mterror,[mbok],0);
                        exit;
              End;
              

              if (Self.ValidaPermissao_Silenciosa(Self.nome)=False)
              Then Begin
                       Messagedlg('Usu�rio com N�vel Inv�lido para esta permiss�o!',mterror,[mbok],0);
                       exit;
              End
              Else result:=True;

        End;
     Finally
            if (Result=True)
            Then PUsuarioAutorizou:=ObjUsuarioGlobal.Get_nome
            Else PUsuarioAutorizou:='';

            Ffiltroimp.edtgrupo02.PasswordChar:=#0;
            
            ObjUsuarioGlobal.LocalizaCodigo(UsuarioAtual);
            ObjUsuarioGlobal.TabelaparaObjeto;
     End;
end;


end.
