unit uClassBase;

interface

uses Classes,IBQuery,IBDatabase,uClassCampoTabela;

type
  TDataSetState = (dsInactive, dsBrowse, dsEdit, dsInsert);
  
  TClassBase = class(TComponent)
  private
    txnConsulta,txnExecuta: TIBTransaction;
    FConectado: Boolean;
    FTabela: string;
    FGenerator: string;
    FSQLInsert: string;
    FSQLUpdate: string;
    FEditando: Boolean;
    FChavePrimaria: string;
    FStatus: TDataSetState;
    procedure setConectado(const Value: Boolean);
    procedure setGenerator(const Value: string);
    procedure setTabela(const Value: string);
    procedure setSQLInsert(const Value: string);
    procedure setSQLUpdate(const Value: string);
    procedure setEditando(const Value: Boolean);
    procedure setChavePrimaria(const Value: string);

    procedure MontaSQLInsert(pLista: TListaCampos);
    procedure MontaSQLUpdate(pLista: TListaCampos);
    procedure CarregaChavePrimaria;
    procedure CarregaCamposTabela(pLista: TListaCampos);

    procedure VerificaDependencias;
    procedure setFStatus(const Value: TDataSetState);

  protected
    ListaCampos: TListaCampos;

    procedure ConfiguraSQLs;
    procedure TabelaParaObjeto;
    procedure ObjetoParaTabela;

  public
    qryConsulta,qryExecuta: TIBQuery;

    property Conectado: Boolean read FConectado write setConectado;
    property Tabela: string read FTabela write setTabela;
    property Generator: string read FGenerator write setGenerator;
    property ChavePrimaria: string read FChavePrimaria write setChavePrimaria;
    property SQLInsert: string read FSQLInsert write setSQLInsert;
    property SQLUpdate: string read FSQLUpdate write setSQLUpdate;
    property Editando: Boolean Read FEditando write setEditando;
    property Status: TDataSetState read FStatus write setFStatus;

    constructor create;
    destructor destroy;override;

    procedure SetaDatabase(pDatabase: tibdatabase);

    function get_SQL(pID: string): string;

    procedure Pesquisa(pID: string; preenche: boolean);
    function get_campoTabela(campoSelect, campoWhere, nomeTabela, parametro:string; alias:string):string;

    procedure Cancelar;
    procedure Salvar(pCommit: boolean);
    function get_NovoID:string;
    function Deletar(pId: string; pCommit: boolean): Boolean;

    procedure MostraCamposTabela;
    procedure Inicializa;

  end;

implementation

uses SysUtils, Dialogs, uGlobal, TypInfo;

{ TClassBase }

procedure TClassBase.ConfiguraSQLs;
begin
  Self.CarregaChavePrimaria;
  Self.CarregaCamposTabela(ListaCampos);

  Self.MontaSQLInsert(ListaCampos);
  Self.MontaSQLUpdate(ListaCampos);
end;

constructor TClassBase.create;
begin
{Cria query�s e suas respectivas transactions. O database � anexado
  executando o m�todo SetaDatabase, a partir de onde o objeto foi criado.
Isso serve para diminuir a depend�ncia do database, utilizando o principio
  setter dependency injection
}
  try
    txnConsulta := TIBTransaction.Create(nil);
    txnExecuta := TIBTransaction.Create(nil);
    qryConsulta := TIBQuery.Create(nil);
    qryExecuta := TIBQuery.Create(nil);
    qryConsulta.Transaction := txnConsulta;
    qryExecuta.Transaction := txnExecuta;
    qryExecuta.DisableControls;//Para desabilitar a atualiza��o de qualquer componente visual
  except
    raise Exception.Create('N�o foi poss�vel instanciar a conex�o');
  end;

  ListaCampos := TListaCampos.Create(Tcampo);
  self.Status := dsInactive;
end;

destructor TClassBase.destroy;
begin
  if Assigned(qryConsulta) then
    FreeAndNil(qryConsulta);
  if Assigned(qryExecuta) then
    FreeAndNil(qryExecuta);
  if Assigned(txnConsulta) then
    FreeAndNil(txnConsulta);
  if Assigned(txnExecuta) then
    FreeAndNil(txnExecuta);
  if Assigned(ListaCampos) then
    FreeAndNil(ListaCampos);
  inherited;
end;

function TClassBase.get_NovoID: string;
begin
  result := '';
  qryExecuta.Close;
  qryExecuta.SQL.Clear;
  qryExecuta.SQL.Add('SELECT GEN_ID(' + Self.Generator + ',1) CODIGO FROM RDB$DATABASE');
  try
    qryExecuta.Open;
    Result := qryExecuta.Fields[0].AsString;
    qryExecuta.Transaction.Commit;
  except
    raise Exception.Create('Erro ao gerar novo ID para tabela: '+self.Tabela);
  end;
end;

procedure TClassBase.Inicializa;
var
  proplist: TPropList;
  propcount,cont: Integer;
  propriedade: PPropInfo;
begin
  //pega lista das propriedades da classe do objeto
  propcount := GetPropList(self.ClassInfo, tkProperties, @proplist);

  for cont:=0 to propcount-1 do
  begin
    //exemplo do blog do luiz clonando um objeto
    //pega a propriedade X
    propriedade := GetPropInfo(Self,proplist[cont]^.Name);
    if propriedade.PropType^.Kind in [tkString, tkWChar, tkLString, tkWString] then
      SetPropValue(Self,proplist[cont]^.Name,'')
    else
      if propriedade.PropType^.Kind in [tkInteger, tkFloat, tkInt64] then
        SetPropValue(Self,proplist[cont]^.Name,0)
      else
        raise Exception.Create('Tipo de dado n�o relacionado');
  end;
end;

procedure TClassBase.MontaSQLInsert(pLista: TListaCampos);
var
  cont: Integer;
  Temp: string;
begin
  Temp := 'INSERT INTO ' + Self.Tabela + ' (' ;

  for cont := 0 to pLista.Count -1 do
  begin
    if(pLista.Item[cont].Escrita) then
      Temp := Temp + pLista.Item[cont].Nome + ',';
  end;

  Delete(Temp,length(Temp),1);

  Temp := Temp + ') VALUES (';

  for cont := 0 to pLista.Count -1 do
  begin
    if(pLista.Item[cont].Escrita) then
      Temp := Temp + ':' + pLista.Item[cont].Nome + ',';
  end;

  Delete(Temp,length(Temp),1);

  Temp := Temp + ')';

  self.SQLInsert := Temp;
end;

procedure TClassBase.MontaSQLUpdate(pLista: TListaCampos);
var
  cont: Integer;
  Temp: string;
begin
  Temp := 'UPDATE ' + Self.Tabela + ' SET ';

  for cont := 0 to pLista.Count -1 do
  begin
    if pLista.Item[cont].Nome <> ChavePrimaria then
      if(pLista.Item[cont].Escrita) then
        Temp := Temp + pLista.Item[cont].Nome + ' = :'+pLista.Item[cont].Nome + ',';
  end;

  Delete(Temp,length(Temp),1);

  Temp := Temp + ' WHERE ' + ChavePrimaria + ' =:'+ChavePrimaria;

  self.SQLUpdate := Temp;
end;

function TClassBase.get_SQL(pID: string): string;
begin
  Result := '';
  Editando := False;
  if(Trim(pID) = '') then
    Result := SQLInsert
  else
    if(Trim(get_campoTabela('CODIGO','CODIGO',Self.Tabela,pID,'')) = '') then
      Result := SQLInsert
    else
    begin
      Result := SQLUpdate;
      Editando := True;
    end;
end;

procedure TClassBase.Pesquisa(pID: string; preenche: boolean);
begin
{M�todo destinado a pesquisas onde se deseja exibir os dados
  para o usu�rio, n�o podendo fechar a transa��o com commit.
  Devido a isso a transa��o somente � comitada ao realizar outra
  pesquisa.
Caso precise buscar algum dado que n�o seja exibido (ex: dbgrid)
  ent�o utilize a pesquisa r�pida (get_CampoTabela).
}
  if(qryConsulta.Transaction.InTransaction) then
    qryConsulta.Transaction.Commit;
  qryConsulta.Transaction.StartTransaction;

  qryConsulta.Close;
  qryConsulta.SQL.Clear;
  qryConsulta.SQL.Add('select * from '+ self.Tabela);
  if(Trim(pID) <> '') then
  begin
    qryConsulta.SQL.Add('where codigo=:codigo');
    qryConsulta.Params[0].AsString := pID;
  end;
  try
    qryConsulta.Open;
    if preenche and not(qryConsulta.IsEmpty) then
    begin
      Inicializa;
      TabelaParaObjeto;
    end;
  except
    raise Exception.Create('Erro na pesquisa');
  end;
end;

function TClassBase.get_campoTabela(campoSelect, campoWhere, nomeTabela,
  parametro, alias: string): string;
begin
{M�todo utilizado para consultas r�pidas, onde ap�s a consulta
  pode-se commitar a transa��o.
}
  result := '';
  if (campoSelect = '') then
    Exit;
  if (parametro = '') then
    Exit;
  try
    qryExecuta.Close;
    qryExecuta.sql.Clear;
    qryExecuta.sql.Add ('select '+campoSelect);
    qryExecuta.sql.Add ('from '+nomeTabela);
    if(Trim(campoWhere) = '') then
      qryExecuta.SQL.Add(parametro)
    else
      if(Copy(parametro,0,1) = #39) then
        qryExecuta.sql.Add('where '+campoWhere+'='+parametro)
      else
        qryExecuta.sql.Add ('where '+campoWhere+'='+#39+parametro+#39);
    //InputBox('','',sql.Text);
    try
      qryExecuta.Open;
      qryExecuta.First;
      if (alias <> '') then
        result := qryExecuta.fieldbyname(alias).AsString
      else
        result := qryExecuta.fieldbyname(campoSelect).AsString;
    finally
      if qryExecuta.Transaction.InTransaction then
        qryExecuta.Transaction.Commit;
    end;
  except
    on e:Exception do
    begin
      raise Exception.Create(e.Message);
      result:='';
    end
  end;
end;

procedure TClassBase.CarregaChavePrimaria;
begin
  ChavePrimaria := '';
  if qryExecuta.Transaction.InTransaction then
    qryExecuta.Transaction.Commit;
  qryExecuta.Transaction.StartTransaction;
  qryExecuta.Close;
  qryExecuta.SQL.Clear;
  qryExecuta.SQL.Add('SELECT');
  qryExecuta.SQL.Add('IDX.RDB$FIELD_NAME');
  qryExecuta.SQL.Add('FROM');
  qryExecuta.SQL.Add('RDB$RELATION_CONSTRAINTS TC');
  qryExecuta.SQL.Add('JOIN RDB$INDEX_SEGMENTS IDX ON (IDX.RDB$INDEX_NAME =');
  qryExecuta.SQL.Add('TC.RDB$INDEX_NAME)');
  qryExecuta.SQL.Add('WHERE');
  qryExecuta.SQL.Add('TC.RDB$CONSTRAINT_TYPE = ' + QuotedStr('PRIMARY KEY'));
  qryExecuta.SQL.Add('AND');
  qryExecuta.SQL.Add('TC.RDB$RELATION_NAME = ' + QuotedStr(self.Tabela));
  qryExecuta.SQL.Add('ORDER BY');
  qryExecuta.SQL.Add('IDX.RDB$FIELD_POSITION');
  qryExecuta.Open;
  if qryExecuta.IsEmpty then
    raise Exception.Create('Chave prim�ria da tabela ' + self.Tabela
                          + ' n�o encontrada');
  ChavePrimaria := Trim(qryExecuta.Fields[0].AsString);
  qryExecuta.Transaction.Commit;
end;

procedure TClassBase.Cancelar;
begin
  if(qryExecuta.Transaction.InTransaction) then
    qryExecuta.Transaction.Rollback;
  self.Status := dsInactive;
end;

procedure TClassBase.Salvar(pCommit: boolean);
begin
  if(pCommit) then
    qryExecuta.Transaction.Commit;
  self.Status := dsInactive;
end;

procedure TClassBase.SetaDatabase(pDatabase: tibdatabase);
begin
  try
    txnConsulta.DefaultDatabase := pDatabase;
    txnExecuta.DefaultDatabase := pDatabase;
  except
    raise Exception.Create('N�o foi poss�vel configurar a conex�o');
  end;
  //setConectado(true);
  self.VerificaDependencias;
  Self.ConfiguraSQLs;
end;

procedure TClassBase.setChavePrimaria(const Value: string);
begin
  FChavePrimaria := Value;
end;

procedure TClassBase.setConectado(const Value: Boolean);
begin
  Conectado := Value;
end;

procedure TClassBase.setEditando(const Value: Boolean);
begin
  FEditando := Value;
end;

procedure TClassBase.setGenerator(const Value: string);
begin
  FGenerator := Value;
end;

procedure TClassBase.setSQLInsert(const Value: string);
begin
  FSQLInsert := Value;
end;

procedure TClassBase.setSQLUpdate(const Value: string);
begin
  FSQLUpdate := Value;
end;

procedure TClassBase.setTabela(const Value: string);
begin
  FTabela := Value;
end;

function TClassBase.Deletar(pId: string; pCommit: boolean): Boolean;
begin
  Result := False;
  if(Trim(pId) = '') then
    exit;
  qryExecuta.Close;
  qryExecuta.SQL.Clear;
  qryExecuta.SQL.Add('DELETE FROM ' + Self.Tabela + ' WHERE CODIGO = :CODIGO');
  qryExecuta.Params[0].AsString := pId;
  try
    qryExecuta.ExecSQL;

    if(pCommit) then
      qryExecuta.Transaction.Commit;
  except
    on e: Exception do
      raise Exception.Create(e.Message);
  end;
  Result := True;
end;

procedure TClassBase.MostraCamposTabela;
var
  cont: Integer;
  temp: string;
begin
  try
    temp := '';
    //property Editando: Boolean Read FEditando write setEditando;
    for cont := 0 to ListaCampos.Count - 1 do
      temp := temp + 'property ' + ListaCampos.Item[cont].Nome + ': string Read F'+
      ListaCampos.Item[cont].Nome + ' Write setF' + ListaCampos.Item[cont].Nome + ';'+#13+#13;

    InputBox('Exibindo campos da tabela '+self.Tabela,'',temp);
  finally
  end;
end;

procedure TClassBase.CarregaCamposTabela(pLista: TListaCampos);
var
  Obrigatorio, Escrita: Boolean;
begin
  pLista.Clear;
  try
    if qryExecuta.Transaction.InTransaction then
      qryExecuta.Transaction.Commit;
    qryExecuta.Transaction.StartTransaction;
    qryExecuta.Close;
    qryExecuta.SQL.Clear;
    qryExecuta.SQL.Add('SELECT');
    qryExecuta.SQL.Add('  A.RDB$FIELD_NAME NOME_DO_CAMPO,');
    qryExecuta.SQL.Add('  C.RDB$TYPE_NAME TIPO,');
    qryExecuta.SQL.Add('  B.RDB$FIELD_SUB_TYPE SUBTIPO,');
    qryExecuta.SQL.Add('  B.RDB$FIELD_LENGTH TAMANHO,');
    qryExecuta.SQL.Add('  B.RDB$SEGMENT_LENGTH SEGMENTO,');
    qryExecuta.SQL.Add('  B.RDB$FIELD_PRECISION PRECISAO,');
    qryExecuta.SQL.Add('  B.RDB$FIELD_SCALE CASAS_DECIMAIS,');
    qryExecuta.SQL.Add('  A.RDB$DEFAULT_SOURCE VALOR_PADRAO,');
    qryExecuta.SQL.Add('  A.RDB$NULL_FLAG OBRIGATORIO,');
    qryExecuta.SQL.Add('  B.RDB$COMPUTED_SOURCE');
    qryExecuta.SQL.Add('FROM');
    qryExecuta.SQL.Add('  RDB$RELATION_FIELDS A,');
    qryExecuta.SQL.Add('  RDB$FIELDS B,');
    qryExecuta.SQL.Add('  RDB$TYPES C');
    qryExecuta.SQL.Add('WHERE');
    qryExecuta.SQL.Add('  (A.RDB$RELATION_NAME = ' + QuotedStr(self.Tabela) + ') AND');
    qryExecuta.SQL.Add('  (B.RDB$FIELD_NAME = A.RDB$FIELD_SOURCE) AND');
    qryExecuta.SQL.Add('  (C.RDB$TYPE = B.RDB$FIELD_TYPE) AND');
    qryExecuta.SQL.Add('  (C.RDB$FIELD_NAME = ' + QuotedStr('RDB$FIELD_TYPE') +')');
    qryExecuta.SQL.Add('ORDER BY');
    qryExecuta.SQL.Add('  RDB$FIELD_POSITION');
    qryExecuta.Open;
    while not(qryExecuta.Eof) do
    begin
      Obrigatorio := False;
      if(Trim(qryExecuta.Fields[8].AsString) = '1') then
        Obrigatorio := True;

      Escrita := false;
      if(Trim(qryExecuta.Fields[9].AsString) = '') then
        Escrita := True;
      //pLista.Add(Trim(qryExecuta.Fields[0].AsString));
      pLista.AddCampo(Trim(qryExecuta.Fields[0].AsString),Trim(qryExecuta.Fields[3].AsString),
                      pLista.RetornaTipoCampo(Trim(qryExecuta.Fields[1].AsString),''),Obrigatorio,Escrita);
      qryExecuta.Next;
    end;
    qryExecuta.Transaction.Commit;

    if(pLista.Count = 0) then
      raise Exception.Create('N�o foi poss�vel retornar campos da tabela: '+self.Tabela);
      
  except
    on e:Exception do
      raise Exception.Create(e.Message);
  end;
end;

procedure TClassBase.TabelaParaObjeto;
var
  proplist: TPropList;
  propcount,cont: Integer;
  propriedade: PPropInfo;
begin
{Para chegar a carregar os dados da tabela, j� deve ter ocorrido
uma consulta na qryconsulta para ent�o este m�todo percorrer os campos
e carregar os dados para o objeto
}
  //pega lista das propriedades da classe do objeto
  propcount := GetPropList(self.ClassInfo, tkProperties, @proplist);

  for cont:=0 to propcount-1 do
  begin
    //pega a propriedade X
    propriedade := GetPropInfo(Self,proplist[cont]^.Name);
    if(proplist[cont]^.Name <> 'Name') and (proplist[cont]^.Name <> 'Tag') then
      //Essas 2 propriedades s�o herdadas de Tcomponent, ent�o posso descarta-las
      if propriedade.PropType^.Kind in [tkString, tkWChar, tkLString, tkWString] then
        SetPropValue(Self,proplist[cont]^.Name, qryConsulta.fieldbyname(proplist[cont]^.Name).asstring )
      else
        if propriedade.PropType^.Kind in [tkInteger, tkFloat, tkInt64] then
          SetPropValue(Self,proplist[cont]^.Name,qryConsulta.fieldbyname(proplist[cont]^.Name).AsVariant)
        else
          raise Exception.Create('Tipo de dado n�o relacionado');
  end;
end;

procedure TClassBase.ObjetoParaTabela;
var
  proplist: TPropList;
  propcount,cont: Integer;
begin
  qryExecuta.Close;
  //qryExecuta.SQL.Text := Self.sql o sql foi escolhido com base
  //na consulta anterior que verificou se � um insert ou update

  //pega lista das propriedades da classe do objeto
  propcount := GetPropList(self.ClassInfo, tkProperties, @proplist);

  for cont:=0 to ListaCampos.Count - 1 do
    if(ListaCampos.Item[cont].Nome <> 'Name') and (ListaCampos.Item[cont].Nome <> 'Tag') then
      if(ListaCampos.Item[cont].Escrita) then
        if(ListaCampos.Item[cont].Tipo = 'INTEGER') then
          qryExecuta.ParamByName(ListaCampos.Item[cont].Nome).AsString := StringReplace(StringReplace(GetPropValue(Self,ListaCampos.Item[cont].Nome),'.','',[rfReplaceAll]),',','.',[rfReplaceAll])
        else
          qryExecuta.ParamByName(ListaCampos.Item[cont].Nome).AsString := GetPropValue(Self,ListaCampos.Item[cont].Nome);

  {
  for cont:=0 to propcount-1 do
    if(proplist[cont]^.Name <> 'Name') and (proplist[cont]^.Name <> 'Tag') then
      if(ListaCampos.Item[cont].Escrita) then
        qryExecuta.ParamByName(proplist[cont]^.Name).AsString := GetPropValue(Self,proplist[cont]^.Name);
  }
end;

procedure TClassBase.VerificaDependencias;
begin
  if(Trim(self.Tabela) = '') then
    raise Exception.Create('Configure a Tabela ao instanciar o Objeto');

  if(Trim(self.Generator) = '') then
    raise Exception.Create('Configure o Generator da Tabela ao instanciar o Objeto');

  if(txnConsulta.DefaultDatabase.DatabaseName = '') or (txnExecuta.DefaultDatabase.DatabaseName = '') then
    raise Exception.Create('Configure a conex�o do Objeto '+self.ClassName);

end;



procedure TClassBase.setFStatus(const Value: TDataSetState);
begin
  FStatus := Value;
end;

end.
 