object Fanotacoes: TFanotacoes
  Left = 278
  Top = 246
  Width = 681
  Height = 403
  Caption = 'Cadastro de Anota'#231#245'es - EXCLAIM TECNOLOGIA'
  Color = clSilver
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ImagemFundo: TImage
    Left = 0
    Top = 50
    Width = 665
    Height = 265
    Align = alClient
    Stretch = True
  end
  object Label1: TLabel
    Left = 25
    Top = 61
    Width = 39
    Height = 14
    Caption = 'C'#243'digo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 25
    Top = 105
    Width = 100
    Height = 14
    Caption = 'Pessoa p/ Contato'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 395
    Top = 105
    Width = 27
    Height = 14
    Caption = 'Fone'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Assunto: TLabel
    Left = 25
    Top = 127
    Width = 47
    Height = 14
    Caption = 'Assunto'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 25
    Top = 193
    Width = 93
    Height = 14
    Caption = 'Data do Cadastro'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label5: TLabel
    Left = 214
    Top = 193
    Width = 95
    Height = 14
    Caption = 'Data para Entrega'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label6: TLabel
    Left = 395
    Top = 193
    Width = 55
    Height = 14
    Caption = 'Conclu'#237'do'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label7: TLabel
    Left = 25
    Top = 83
    Width = 41
    Height = 14
    Caption = 'Origem'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label8: TLabel
    Left = 25
    Top = 149
    Width = 46
    Height = 14
    Caption = 'Endere'#231'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 25
    Top = 171
    Width = 43
    Height = 14
    Caption = 'T'#233'cnico'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label10: TLabel
    Left = 25
    Top = 216
    Width = 65
    Height = 14
    Caption = 'Observa'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbOrigem: TLabel
    Left = 217
    Top = 83
    Width = 51
    Height = 14
    Caption = 'lbOrigem'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object panelrodape: TPanel
    Left = 0
    Top = 315
    Width = 665
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 12
    DesignSize = (
      665
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 665
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 360
      Top = 16
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 836
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 836
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 665
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 526
      Top = 0
      Width = 139
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Anota'#231#245'es'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -24
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object BtNovo: TBitBtn
      Left = 1
      Top = -3
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtNovoClick
    end
    object btalterar: TBitBtn
      Left = 51
      Top = -3
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object Btgravar: TBitBtn
      Left = 101
      Top = -3
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = BtgravarClick
    end
    object BtCancelar: TBitBtn
      Left = 151
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = BtCancelarClick
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = -3
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = -3
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
    end
    object btOpcoes: TBitBtn
      Left = 351
      Top = -3
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
    object btsair: TBitBtn
      Left = 401
      Top = -3
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
  end
  object EdtCodigo: TEdit
    Left = 137
    Top = 59
    Width = 70
    Height = 19
    CharCase = ecUpperCase
    MaxLength = 9
    TabOrder = 1
  end
  object edtcontato: TEdit
    Left = 137
    Top = 103
    Width = 250
    Height = 19
    CharCase = ecUpperCase
    MaxLength = 50
    TabOrder = 4
  end
  object edtfone: TEdit
    Left = 456
    Top = 102
    Width = 188
    Height = 19
    CharCase = ecUpperCase
    MaxLength = 20
    TabOrder = 3
  end
  object edtassunto: TEdit
    Left = 137
    Top = 125
    Width = 508
    Height = 19
    CharCase = ecUpperCase
    MaxLength = 50
    TabOrder = 5
  end
  object edtdata: TMaskEdit
    Left = 137
    Top = 191
    Width = 70
    Height = 19
    CharCase = ecUpperCase
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object edtdataretorno: TMaskEdit
    Left = 316
    Top = 191
    Width = 70
    Height = 19
    CharCase = ecUpperCase
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object comboconcluido: TComboBox
    Left = 456
    Top = 190
    Width = 188
    Height = 21
    BevelKind = bkSoft
    ItemHeight = 13
    TabOrder = 8
    Items.Strings = (
      'S - Sim'
      'N - N'#227'o')
  end
  object edtorigem: TEdit
    Left = 137
    Top = 81
    Width = 70
    Height = 19
    CharCase = ecUpperCase
    Color = 6073854
    MaxLength = 9
    TabOrder = 2
    OnExit = edtorigemExit
    OnKeyDown = edtorigemKeyDown
    OnKeyPress = edtorigemKeyPress
  end
  object edtendereco: TEdit
    Left = 137
    Top = 147
    Width = 508
    Height = 19
    CharCase = ecUpperCase
    MaxLength = 80
    TabOrder = 6
  end
  object edttecnicoresponsavel: TEdit
    Left = 137
    Top = 169
    Width = 508
    Height = 19
    CharCase = ecUpperCase
    MaxLength = 30
    TabOrder = 7
  end
  object memoobservacoes: TMemo
    Left = 25
    Top = 234
    Width = 620
    Height = 63
    MaxLength = 1000
    TabOrder = 11
    OnKeyPress = memoobservacoesKeyPress
  end
end
