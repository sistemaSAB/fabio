unit UCONFCAMPOSPESQUISA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCONFCAMPOSpesquisa,
  jpeg;

type
  TFCONFCAMPOSPESQUISA = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    Lbusuario: TLabel;
    Edtusuario: TEdit;
    LbIDENTIFICADOR: TLabel;
    EdtIDENTIFICADOR: TEdit;
    Lbcampo: TLabel;
    Edtcampo: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCONFCAMPOSpesquisa:TObjCONFCAMPOSpesquisa;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCONFCAMPOSPESQUISA: TFCONFCAMPOSPESQUISA;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCONFCAMPOSPESQUISA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCONFCAMPOSpesquisa do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_usuario(edtusuario.text);
        Submit_IDENTIFICADOR(edtIDENTIFICADOR.text);
        Submit_campo(edtcampo.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCONFCAMPOSPESQUISA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCONFCAMPOSpesquisa do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        Edtusuario.text:=Get_usuario;
        EdtIDENTIFICADOR.text:=Get_IDENTIFICADOR;
        Edtcampo.text:=Get_campo;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCONFCAMPOSPESQUISA.TabelaParaControles: Boolean;
begin
     If (Self.ObjCONFCAMPOSpesquisa.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCONFCAMPOSPESQUISA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCONFCAMPOSpesquisa=Nil)
     Then exit;

    If (Self.ObjCONFCAMPOSpesquisa.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    Self.ObjCONFCAMPOSpesquisa.free;
end;

procedure TFCONFCAMPOSPESQUISA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCONFCAMPOSPESQUISA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCONFCAMPOSpesquisa.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjCONFCAMPOSpesquisa.status:=dsInsert;
     Guia.pageindex:=0;
     edtusuario.setfocus;

end;


procedure TFCONFCAMPOSPESQUISA.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCONFCAMPOSpesquisa.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCONFCAMPOSpesquisa.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtusuario.setfocus;
                
          End;


end;

procedure TFCONFCAMPOSPESQUISA.btgravarClick(Sender: TObject);
begin

     If Self.ObjCONFCAMPOSpesquisa.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCONFCAMPOSpesquisa.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCONFCAMPOSpesquisa.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCONFCAMPOSPESQUISA.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCONFCAMPOSpesquisa.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCONFCAMPOSpesquisa.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCONFCAMPOSpesquisa.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCONFCAMPOSPESQUISA.btcancelarClick(Sender: TObject);
begin
     Self.ObjCONFCAMPOSpesquisa.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCONFCAMPOSPESQUISA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCONFCAMPOSPESQUISA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCONFCAMPOSpesquisa.Get_pesquisa,Self.ObjCONFCAMPOSpesquisa.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCONFCAMPOSpesquisa.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCONFCAMPOSpesquisa.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCONFCAMPOSpesquisa.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCONFCAMPOSPESQUISA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCONFCAMPOSPESQUISA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

      limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjCONFCAMPOSpesquisa:=TObjCONFCAMPOSpesquisa.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjCONFCAMPOSpesquisa.OBJETO.Get_Pesquisa,Self.ObjCONFCAMPOSpesquisa.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCONFCAMPOSpesquisa.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjCONFCAMPOSpesquisa.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCONFCAMPOSpesquisa.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
