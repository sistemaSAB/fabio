unit UobjRelatorioPersonalizadoExtendido;
Interface
Uses controls,buttons,graphics,extctrls,forms,sysutils,Ibquery,windows,stdctrls,Classes,Db,
UessencialGlobal,menus,uobjrelatoriopersonalizado,
Uobjfiltrorelpersonalizadoextendido,mask,UobjOrderByRelPersonalizadoextendido,dialogs,uobjarquivoini;
//USES_INTERFACE



Type
   TobjRelatorioPersonalizadoExtendido=class(TobjRelatorioPErsonalizado)

          Public
                Objfiltro:tobjfiltrorelpersonalizadoextendido;
                ObjOrderBy:TobjOrderByRelPersonalizadoextendido;
                ObjArquivoIniX:TObjarquivoini;

                procedure CriaMenu(Menu: Tmenuitem);
                procedure onClick(Sender: Tobject);
                procedure ImprimeRelatorio(Pcodigo: integer);
                constructor create;override;
                destructor  free;override;
                Function Exclui(Pcodigo:string;ComCommit:boolean):Boolean;override;

                Procedure RetornaOrderByegravaordem(prelatorio:string;ListboxX:TListBox;var OrderbyRepeticao:string;GravaDuasOrdens:Boolean);
                Procedure ImportaExporta(prelatorio:string);



         Private
               VetorMenu:array[0..1000] of TMenuItem;
               VetorSubMenu:array[0..1000] of TMenuItem;
               VetorRel:array[0..1000] of TMenuItem;
               contmenu,contsubmenu,contrel:integer;
               ObjqueryLocal:tibquery;
               OrdenacaoPadrao:TStringList;

               Procedure GravaUltimoSqlRepeticao(pcodigo:integer;psql:string);
               Procedure GravaUltimoSqlPrincipal(pcodigo:integer;psql:string);
               Procedure BotaoRestauraPadraoClick(Sender: TObject);

               Procedure ExportaRelAtual(prelatorio:string);
               Procedure ImportaRel;


   End;


implementation

uses udatamodulo,UrelatorioPersonalizado, UMostraBarraProgresso,
  UFILTRORELPERSONALIZADO, UobjORDERBYRELPERSONALIZADO, UopcaoRel,
  UFiltraImp, UmostraStringList,IBScript, IBCustomDataSet;


Procedure TobjRelatorioPersonalizadoExtendido.ImprimeRelatorio(Pcodigo:integer);
var
folhaatual,pquantidadefolhas,contalinharepeticao:integer;
Objquerylocal,objqueryrepeticao:tibquery;

PFiltroStr:TStringList;

pcampostotalfinal,pvalorcampostotalfinal,PcamposExtras,PvalorCamposExtras:TStringList;
pcampoauditorasubtotal,pcampossubtotal,pvalorcampossubtotal,pvalorcampoauditoriasubtotal:TStringList;


OrderbyRepeticao,sql_and_principal,sql_and_repeticao,pvalor,pparcelas:string;
contfiltromultiplaescolha,pconta,cont:integer;

pdata : string;
//plinhatemp:integer;

procedure verificaLinhalocal;
var
temp:integer;                                              
Begin

      if ( contalinharepeticao>=Frelatoriopersonalizado.QtdeRepeticaoFolha)
      Then Begin
                Frelatoriopersonalizado.Rdprint.Novapagina;
                temp:=0;

                contalinharepeticao:=Frelatoriopersonalizado.PLinhaPrimeiroProdutoRepeticao;
                
                if (FrelatorioPersonalizado.Imprime_Sem_Repeticao_todas_Folhas=True)
                Then Frelatoriopersonalizado.ImprimeCamposPreImpresso(Objquerylocal,false,temp,PcamposExtras,PvalorCamposExtras,'',true,'')
                Else Begin
                          //nao imprime o cabecalho nas outras folhas, entao a repetica comeca na 3
                          contalinharepeticao:=3;
                          Frelatoriopersonalizado.QtdeRepeticaoFolha:=Frelatoriopersonalizado.Rdprint.TamanhoQteLinhas-3;
                End;

                //contaprodutos:=1;//na linha de baixo ele usa contaprodutos-1
      End;

End;

Begin


     sql_and_principal:='';
     sql_and_repeticao:='';
     
     if (Self.LocalizaCodigo(inttostr(pcodigo))=False)
     Then exit;
     Self.TabelaparaObjeto;


     if (Self.Permissao.ValidaPermissao(Self.Permissao.Get_nome)=False)
     then exit;



     With Self.Objquery do
     Begin
          Self.OrdenacaoPadrao.clear;
          close;
          sql.clear;
          sql.add('Select comando,nome from taborderbyrelpersonalizado where relatorio='+inttostr(pcodigo));
          sql.add('order by ordemdefault');
          open;
          if (recordcount>0)
          Then Begin
                    OrderbyRepeticao:='order by '+fieldbyname('comando').asstring;
                    Self.OrdenacaoPadrao.add(fieldbyname('nome').asstring);
          End
          Else OrderbyRepeticao:='';
          next;

          While not(eof) do
          Begin
                OrderbyRepeticao:=OrderbyRepeticao+','+fieldbyname('comando').asstring;
                Self.OrdenacaoPadrao.add(fieldbyname('nome').asstring);
                next;
          End;



          close;
          sql.clear;
          sql.add('Select * from tabfiltrorelpersonalizado where relatorio='+inttostr(pcodigo));
          open;
          if (recordcount>0)
          Then begin
                    pconta:=0;

                    Try
                       PFiltroStr:=TStringList.create;
                    Except
                          MensagemErro('Erro na tentativa de criar a StringList Pfiltro');
                          exit;
                    End;


                    Try

                      //********************************************************
                      While not(eof) do
                      Begin
                          pconta:=pconta+1;
                          with TPanel.Create(Ffiltrorelpersonalizado) do
                          begin
                               Parent:=Ffiltrorelpersonalizado;
                               Name:='PANELX'+IntToStr(pconta);
                               Ffiltrorelpersonalizado.Grupo[pconta]:=nil;
                               Ffiltrorelpersonalizado.Grupo[pconta]:=(Ffiltrorelpersonalizado.FindComponent('PANELX'+IntToStr(pconta)) as TPanel);
                               Ffiltrorelpersonalizado.Grupo[pconta].Top:=(40*(Pconta-1))+pconta;
                               Ffiltrorelpersonalizado.Grupo[pconta].Height:=41;
                               Ffiltrorelpersonalizado.Grupo[pconta].Width:=345;
                               Ffiltrorelpersonalizado.Grupo[pconta].caption:='';
                               Ffiltrorelpersonalizado.grupo[pconta].tag:=pconta;
                               Ffiltrorelpersonalizado.grupo[pconta].Align:=altop;
                          end;

                          With TLabel.Create(Ffiltrorelpersonalizado.Grupo[pconta]) do
                          begin
                               Parent:=Ffiltrorelpersonalizado.Grupo[pconta];
                               Name:='LABELX'+IntToStr(pconta);
                               Ffiltrorelpersonalizado.lbgrupo[pconta]:=nil;
                               Ffiltrorelpersonalizado.lbgrupo[pconta]:=(Ffiltrorelpersonalizado.Grupo[pconta].FindComponent('LABELX'+IntToStr(pconta)) as TLabel);
                               Ffiltrorelpersonalizado.lbgrupo[pconta].Top:=12;
                               Ffiltrorelpersonalizado.lbgrupo[pconta].Left:=8;
                               Ffiltrorelpersonalizado.lbgrupo[pconta].Font.Name:='Times New Roman';
                               Ffiltrorelpersonalizado.lbgrupo[pconta].Font.color:=clnavy;
                               Ffiltrorelpersonalizado.lbgrupo[pconta].Font.size:=12;
                               Ffiltrorelpersonalizado.lbgrupo[pconta].Font.Style:=[fsbold];
                               if (fieldbyname('requerido').asstring='S')
                               Then Ffiltrorelpersonalizado.lbgrupo[pconta].caption:='*'+fieldbyname('captionlabel').asstring
                               Else Ffiltrorelpersonalizado.lbgrupo[pconta].caption:=fieldbyname('captionlabel').asstring;
                               Ffiltrorelpersonalizado.lbgrupo[pconta].tag:=pconta;

                          End;
                        
                          With Tmaskedit.Create(Ffiltrorelpersonalizado.Grupo[pconta]) do
                          begin
                               Parent:=Ffiltrorelpersonalizado.Grupo[pconta];
                               Name:='EDITX'+IntToStr(pconta);
                               Ffiltrorelpersonalizado.edtgrupo[pconta]:=nil;
                               Ffiltrorelpersonalizado.edtgrupo[pconta]:=(Ffiltrorelpersonalizado.Grupo[pconta].FindComponent('EDITX'+IntToStr(pconta)) as TmaskEdit);
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Top:=12;
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Left:=240;
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Height:=19;
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Width:=81;
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Font.Name:='MS Sans Serif';
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Font.color:=clBlack;
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Font.size:=8;
                               Ffiltrorelpersonalizado.edtgrupo[pconta].Font.Style:=[];
                               Ffiltrorelpersonalizado.edtgrupo[pconta].text:='';
                        
                               //o unico que tem o tag diferente � o edit que guarda o codigo do filtro
                               //os demais guardam a ordem de criacao
                               Ffiltrorelpersonalizado.edtgrupo[pconta].tag:=fieldbyname('codigo').asinteger;
                        
                               Ffiltrorelpersonalizado.edtgrupo[pconta].editmask:=fieldbyname('mascara').asstring;
                               Ffiltrorelpersonalizado.edtgrupo[pconta].OnKeyDown:=Ffiltrorelpersonalizado.EditKeyDown;
                          End;


                          With TspeedButton.Create(Ffiltrorelpersonalizado.Grupo[pconta]) do
                          begin
                               Parent:=Ffiltrorelpersonalizado.Grupo[pconta];
                               Name:='BOTAOX'+IntToStr(pconta);
                               Ffiltrorelpersonalizado.btgrupo[pconta]:=nil;
                               Ffiltrorelpersonalizado.btgrupo[pconta]:=(Ffiltrorelpersonalizado.Grupo[pconta].FindComponent('BOTAOX'+IntToStr(pconta)) as TspeedButton);
                               Ffiltrorelpersonalizado.btgrupo[pconta].Top:=12;
                               Ffiltrorelpersonalizado.btgrupo[pconta].Left:=323;
                               Ffiltrorelpersonalizado.btgrupo[pconta].Height:=19;
                               Ffiltrorelpersonalizado.btgrupo[pconta].Width:=20;
                               Ffiltrorelpersonalizado.btgrupo[pconta].Font.Name:='Times New Roman';
                               Ffiltrorelpersonalizado.btgrupo[pconta].caption:='';
                               Ffiltrorelpersonalizado.btgrupo[pconta].tag:=pconta;
                               Ffiltrorelpersonalizado.btgrupo[pconta].Glyph:=Ffiltrorelpersonalizado.BotaoModelo.Glyph;
                               Ffiltrorelpersonalizado.btgrupo[pconta].OnClick:=Ffiltrorelpersonalizado.AbreCalendario;
                          End;

                          next;
                      End;
                      Ffiltrorelpersonalizado.ContaEdits:=pconta;
                      Ffiltrorelpersonalizado.ContaTotal:=pconta;


                      //*************************************************
                      (*            ORDER BY                            *)
                      if (Self.Get_orderbypersonalizado='S')
                      and (OrderbyRepeticao<>'')//tem order by
                      Then Begin
                              with TPanel.Create(Ffiltrorelpersonalizado)  do
                              begin
                                   Parent:=Ffiltrorelpersonalizado;
                                   Name:='PANELX'+IntToStr(pconta+1);
                                   Ffiltrorelpersonalizado.Grupo[pconta+1]:=nil;
                                   Ffiltrorelpersonalizado.Grupo[pconta+1]:=(Ffiltrorelpersonalizado.FindComponent('PANELX'+IntToStr(pconta+1)) as TPanel);
                                   Ffiltrorelpersonalizado.Grupo[pconta+1].Top:=(40*(Pconta-1))+pconta;
                                   Ffiltrorelpersonalizado.Grupo[pconta+1].Height:=200;
                                   Ffiltrorelpersonalizado.Grupo[pconta+1].Width:=345;
                                   Ffiltrorelpersonalizado.Grupo[pconta+1].caption:='';
                                   Ffiltrorelpersonalizado.grupo[pconta+1].tag:=pconta+1;
                                   Ffiltrorelpersonalizado.grupo[pconta+1].Align:=altop;
                                   Ffiltrorelpersonalizado.ContaTotal:=pconta+1;

                                   With TLabel.Create(Ffiltrorelpersonalizado.Grupo[pconta+1]) do
                                   begin
                                        Parent:=Ffiltrorelpersonalizado.Grupo[pconta+1];
                                        Name:='LABELX'+IntToStr(pconta+1);
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1]:=nil;
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1]:=(Ffiltrorelpersonalizado.Grupo[pconta+1].FindComponent('LABELX'+IntToStr(pconta+1)) as TLabel);
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].Top:=01;
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].Left:=8;
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].Font.Name:='Times New Roman';
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].Font.color:=clnavy;
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].Font.size:=12;
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].Font.Style:=[fsbold];
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].caption:='Ordem';
                                        Ffiltrorelpersonalizado.lbgrupo[pconta+1].tag:=pconta+1;
                                   End;

                                    With TspeedButton.Create(Ffiltrorelpersonalizado.Grupo[pconta+1]) do
                                    begin
                                         Parent:=Ffiltrorelpersonalizado.Grupo[pconta+1];
                                         Name:='BOTAOX'+IntToStr(pconta+1);
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1]:=nil;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1]:=(Ffiltrorelpersonalizado.Grupo[pconta+1].FindComponent('BOTAOX'+IntToStr(pconta+1)) as TspeedButton);
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].Top:=50;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].Left:=150;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].Height:=Ffiltrorelpersonalizado.botaosetacima.Height;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].Width:=Ffiltrorelpersonalizado.botaosetacima.Width;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].Font.Name:='Times New Roman';
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].caption:='';
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].tag:=pconta+1;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].Glyph:=Ffiltrorelpersonalizado.botaosetacima.Glyph;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+1].OnClick:=Ffiltrorelpersonalizado.botaosetacimaClick;
                                    End;

                                    With TspeedButton.Create(Ffiltrorelpersonalizado.Grupo[pconta+1]) do
                                    begin
                                         Parent:=Ffiltrorelpersonalizado.Grupo[pconta+1];
                                         Name:='BOTAOX'+IntToStr(pconta+2);
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2]:=nil;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2]:=(Ffiltrorelpersonalizado.Grupo[pconta+1].FindComponent('BOTAOX'+IntToStr(pconta+2)) as TspeedButton);
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].Top:=50+Ffiltrorelpersonalizado.botaosetabaixo.Height+3;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].Left:=150;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].Height:=Ffiltrorelpersonalizado.botaosetabaixo.Height;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].Width:=Ffiltrorelpersonalizado.botaosetabaixo.Width;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].Font.Name:='Times New Roman';
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].caption:='';
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].tag:=pconta+1;//pra indicar o grupo correto e o listbox correto
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].Glyph:=Ffiltrorelpersonalizado.botaosetabaixo.Glyph;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+2].OnClick:=Ffiltrorelpersonalizado.botaosetabaixoClick;

                                    End;

                                    With TspeedButton.Create(Ffiltrorelpersonalizado.Grupo[pconta+1]) do
                                    begin
                                         Parent:=Ffiltrorelpersonalizado.Grupo[pconta+1];
                                         Name:='BOTAOX'+IntToStr(pconta+3);
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3]:=nil;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3]:=(Ffiltrorelpersonalizado.Grupo[pconta+1].FindComponent('BOTAOX'+IntToStr(pconta+3)) as TspeedButton);
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].Top:=50;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].Left:=200;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].Height:=22;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].Width:=100;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].Font.Name:='Times New Roman';
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].caption:='Restaurar Padr�o';
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].tag:=pconta+1;
                                         Ffiltrorelpersonalizado.btgrupo[pconta+3].OnClick:=Self.BotaoRestauraPadraoClick;
                                    End;



                                   With TListBox.Create(Ffiltrorelpersonalizado.Grupo[pconta+1]) do
                                   begin
                                        Parent:=Ffiltrorelpersonalizado.Grupo[pconta+1];
                                        Name:='LISTBOXX'+IntToStr(pconta+1);
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1]:=nil;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1]:=(Ffiltrorelpersonalizado.Grupo[pconta+1].FindComponent('LISTBOXX'+IntToStr(pconta+1)) as TListBox);
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].Top:=30;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].Left:=08;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].Height:=160;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].Width:=140;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].Font.Name:='Times New Roman';
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].tag:=pconta+1;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].DragMode:=dmAutomatic;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].OnDragDrop:=Ffiltrorelpersonalizado.ListBoxDragDrop;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].OnDragOver:=Ffiltrorelpersonalizado.ListBoxDragOver;
                                        Ffiltrorelpersonalizado.listboxgrupo[pconta+1].OnMouseDown:=Ffiltrorelpersonalizado.ListBoxMouseDown;
                                                  
                                        close;
                                        sql.clear;
                                        sql.add('Select codigo,nome,comando from taborderbyrelpersonalizado where relatorio='+inttostr(pcodigo));
                                        sql.add('order by ordem');
                                        open;
                                        while not(eof) do
                                        Begin
                                             Ffiltrorelpersonalizado.listboxgrupo[pconta+1].Items.add(fieldbyname('nome').asstring);
                                             next;
                                        End;
                                   End;

                              end;
                      End;

                      Ffiltrorelpersonalizado.Height:=(pconta*40)+100;
                      if (Ffiltrorelpersonalizado.ContaTotal>pconta)//teve order by
                      Then Ffiltrorelpersonalizado.Height:=Ffiltrorelpersonalizado.Height+200;

                      //**************************************************

                      Ffiltrorelpersonalizado.ObjFiltro:=Self.Objfiltro;
                      Ffiltrorelpersonalizado.showmodal;

                      if (Ffiltrorelpersonalizado.Tag=0)
                      Then exit;

                      //validando os filtros e gerando os comandos Sqls  principais, secundarios e trocando
                      //os filtros nos comandos sem repeticao e com repetico pra executar depois

                      for cont:=1 to pconta do
                      Begin

                           PFiltroStr.clear;
                           
                           Pvalor:=Ffiltrorelpersonalizado.edtgrupo[cont].text;

                           if (Self.Objfiltro.LocalizaCodigo(inttostr(Ffiltrorelpersonalizado.edtgrupo[cont].tag))=False)
                           Then Begin
                                     MensagemErro('Filtro n�o localizado '+inttostr(Ffiltrorelpersonalizado.edtgrupo[cont].tag));
                                     exit;
                           End;

                           Self.Objfiltro.TabelaparaObjeto;

                           if (self.Objfiltro.Get_requerido='S')
                           Then Begin
                                     if (trim(Pvalor)='')
                                     Then Begin
                                               MensagemErro('O Filtro '+Self.Objfiltro.Get_captionlabel+' n�o pode estar em branco');
                                               exit;
                                     End;

                                     //se for data e hora, provavelmente tem a mascara que atrapalha verificar o branco
                                     if ((Self.Objfiltro.Get_tipo='DATA') or (Self.Objfiltro.Get_tipo='HORA') or (Self.Objfiltro.Get_tipo='DATA E HORA'))
                                     Then Begin
                                               if (trim(comebarra(pvalor))='')
                                               Then Begin
                                                      MensagemErro('O Filtro '+Self.Objfiltro.Get_captionlabel+' n�o pode estar em branco');
                                                      exit;
                                               End;
                                     End;
                           End
                           Else Begin//nao � requerido
                                     if ((Self.Objfiltro.Get_tipo='DATA') or (Self.Objfiltro.Get_tipo='HORA') or (Self.Objfiltro.Get_tipo='DATA E HORA'))
                                     Then Begin
                                               if (trim(comebarra(pvalor))='')
                                               Then Begin
                                                         //se for data ou hora e estiver vazio, preciso tirar a mascara pra deixar vazio mesmo
                                                         pvalor:='';
                                               End;
                                     End;
                           End;

                           if (pvalor<>'')//ta preenchido
                           or ((Pvalor='') and (self.Objfiltro.Get_usamesmoembranco='S')) //ou ta vazio mais usa em branco
                           Then Begin
                                     Try
                                       if (pvalor<>'')
                                       Then Begin
                                                 if (self.objfiltro.get_tipo='INTEIRO')
                                                 Then Begin
                                                           if (Self.Objfiltro.Get_multiplaescolha='N')
                                                           Then Strtoint(pvalor)
                                                           Else BEgin
                                                                     //em caso de multipla escolha preciso quebrar isso numa stringlist temp
                                                                     if (ExplodeStr(pvalor,PFiltroStr,';','integer')=false)
                                                                     Then Begin
                                                                               MensagemErro('Erro na tentativa de Separar '+Self.Objfiltro.Get_captionlabel);
                                                                               exit;
                                                                     End;

                                                                     if (PFiltroStr.Count>0)
                                                                     Then Begin
                                                                               for contfiltromultiplaescolha:=0 to PFiltroStr.Count-1 do
                                                                               Begin
                                                                                    strtoint(PFiltroStr[contfiltromultiplaescolha]);

                                                                                    if (contfiltromultiplaescolha=0)
                                                                                    Then pvalor:='and ('+Self.Objfiltro.Get_campomultiplaescolha+'='+PFiltroStr[contfiltromultiplaescolha]
                                                                                    Else pvalor:=pvalor+' or '+Self.Objfiltro.Get_campomultiplaescolha+'='+PFiltroStr[contfiltromultiplaescolha];

                                                                               End;
                                                                               pvalor:=pvalor+')';
                                                                     End;
                                                           End;
                                                 End;

                                                 if(self.objfiltro.get_tipo='DECIMAL')
                                                 Then Begin
                                                          Strtofloat(pvalor);
                                                          pvalor:=virgulaparaponto(floattostr(Strtofloat(pvalor)));
                                                 End;

                                                 if (self.objfiltro.get_tipo='DATA')
                                                 Then Begin
                                                          Strtodate(pvalor);
                                                          pdata :=#39+pvalor+#39;
                                                          pvalor:=#39+formatdatetime('mm/dd/yyyy',strtodate(pvalor))+#39;
                                                 End;

                                                  if (self.objfiltro.get_tipo='HORA')
                                                  Then Begin
                                                            strtotime(pvalor);
                                                            pvalor:=#39+pvalor+#39;
                                                  End;

                                                  if(self.objfiltro.get_tipo='DATA E HORA')
                                                  Then Begin
                                                            strtodatetime(pvalor);
                                                            pdata :=#39+pvalor+#39;
                                                            pvalor:=#39+formatdatetime('mm/dd/yyyy hh:mm',strtodatetime(pvalor))+#39;
                                                  End;
                                       End;
                                     Except
                                           MensagemErro('O Filtro '+Self.Objfiltro.Get_captionlabel+' cont�m um valor para '+self.objfiltro.get_tipo+' inv�lido(a)!');
                                           exit;
                                     End;

                                     //se chegou ate aqui � porque ta preenchido ou pode usar vazio
                                     //agora basta fazer as devidas substituicoes e conversoes no caso de data  e 'data e hora'

                                     //se for string tem q ter Apostrofo
                                     if(self.objfiltro.get_tipo='STRING')
                                     Then pvalor:=#39+pvalor+#39;



                                     if  ((self.objfiltro.get_tipo='DATA') or (self.objfiltro.get_tipo='DATA E HORA'))
                                     Then Begin

                                               //Substituindo o Sql principal
                                               Self.SqlPrincipal:=StringReplace(uppercase(Self.SqlPrincipal),':'+uppercase(Self.objfiltro.Get_nome+'_NAOINVERTER'),pdata,[rfreplaceall]);
                                               Self.SqlRepeticao:=StringReplace(uppercase(Self.SqlRepeticao),':'+uppercase(Self.objfiltro.Get_nome+'_NAOINVERTER'),pdata,[rfreplaceall]);
                                               Self.Objfiltro.Submit_comandoandprincipal(StringReplace(uppercase(Self.Objfiltro.Get_comandoandprincipal),':'+uppercase(Self.objfiltro.Get_nome)+'_NAOINVERTER',pdata,[rfreplaceall]));
                                               Self.Objfiltro.Submit_comandoandrepeticao(StringReplace(uppercase(Self.Objfiltro.Get_comandoandrepeticao),':'+uppercase(Self.objfiltro.Get_nome)+'_NAOINVERTER',pdata,[rfreplaceall]));
                                     End;

                                     //Substituindo o Sql principal
                                     Self.SqlPrincipal:=StringReplace(uppercase(Self.SqlPrincipal),':'+uppercase(Self.objfiltro.Get_nome),pvalor,[rfreplaceall]);
                                     Self.SqlRepeticao:=StringReplace(uppercase(Self.SqlRepeticao),':'+uppercase(Self.objfiltro.Get_nome),pvalor,[rfreplaceall]);
                                     Self.Objfiltro.Submit_comandoandprincipal(StringReplace(uppercase(Self.Objfiltro.Get_comandoandprincipal),':'+uppercase(Self.objfiltro.Get_nome),pvalor,[rfreplaceall]));
                                     Self.Objfiltro.Submit_comandoandrepeticao(StringReplace(uppercase(Self.Objfiltro.Get_comandoandrepeticao),':'+uppercase(Self.objfiltro.Get_nome),pvalor,[rfreplaceall]));
                                     sql_and_principal:=sql_and_principal+' '+Self.Objfiltro.Get_comandoandprincipal;
                                     sql_and_repeticao:=sql_and_repeticao+' '+Self.Objfiltro.Get_comandoandrepeticao;


                           End;


                      End;// for dos edits do filtro

                      if (Self.Get_orderbypersonalizado='S')
                      and (OrderbyRepeticao<>'')//teve order by
                      Then Self.RetornaOrderByegravaordem(inttostr(pcodigo),Ffiltrorelpersonalizado.listboxgrupo[pconta+1],OrderbyRepeticao,False);

                    Finally
                          Ffiltrorelpersonalizado.destroi;
                          freeandnil(PFiltroStr);

                    End;

          End;
     End;



     Try
        PcamposExtras:=TStringList.create;
        PvalorCamposExtras:=TStringList.create;
        PcamposExtras.Clear;
        PvalorCamposExtras.clear;

        Pcampostotalfinal:=TStringList.create;
        PvalorCampostotalfinal:=TStringList.create;

        Pcampossubtotal:=TStringList.create;
        PvalorCampossubtotal:=TStringList.create;
        pcampoauditorasubtotal:=TStringList.create;
        pvalorcampoauditoriasubtotal:=TStringList.create;



     Except
           MensagemErro('Erro na tentativa de criar duas stringlists para campos extras');
           exit;
     End;

     Try
        Objquerylocal:=tibquery.create(nil);
        Objquerylocal.database:=FdataModulo.ibdatabase;

        objqueryrepeticao:=tibquery.create(nil);
        objqueryrepeticao.database:=FdataModulo.ibdatabase;

     Except
           Freeandnil(PCamposExtras);
           Freeandnil(PvalorCamposExtras);
           mensagemerro('Erro na tentativa de criar a Querylocal');
           exit;
     End;
Try
     With ObjqueryLocal do
     Begin
         close;
         SQL.clear;
         SQL.add(Self.SqlPrincipal+' '+sql_and_principal);
         //sql.SaveToFile('D:\mauricio.txt');
         Try
            Self.GravaUltimoSqlPrincipal(pcodigo,sql.text);
            open;
         Except
               on e:exception do
               Begin
                    mensagemErro('Erro no Sql Principal: '+e.message);
                    exit;
               End;
         End;

         if (recordcount=0)
         then begin
                   Mensagemerro('Nenhuma Informa��o Selecionada');
                   exit;
         End;

         Frelatoriopersonalizado.Nomerelatorio:='REL_PERSONALIZADO_'+Self.codigo;
         if (Frelatoriopersonalizado.Inicializa=False)
         then Begin
                   mensagemerro('N�o foi poss�vel inicializar o relat�rio personalizado');
                   exit;
         End;

         if (Frelatoriopersonalizado.STRGCampos.RowCount=2)//linha de cabecalho e de quantidade
         then Begin
                   mensagemerro('Configure o relat�rio no m�dulo de configura��o');
                   exit;
         End;

         objqueryrepeticao.close;
         objqueryrepeticao.sql.clear;
         objqueryrepeticao.sql.add(Self.SqlRepeticao+' '+sql_and_repeticao+' '+Self.Sqlgroupby+' '+OrderbyRepeticao);
         Try
            Self.GravaUltimoSqlRepeticao(pcodigo,objqueryrepeticao.sql.text);
            objqueryrepeticao.open;
         Except
               on e:exception do
               Begin
                    mensagemErro('Erro no Sql Repeti��o: '+e.message);
                    exit;
               End;
         End;
         objqueryrepeticao.last;



         Frelatoriopersonalizado.Rdprint.Abrir;
         if (Frelatoriopersonalizado.Rdprint.Setup=False)
         then begin
                   Frelatoriopersonalizado.Rdprint.Fechar;
                   exit;
         End;
         FMostraBarraProgresso.ConfiguracoesIniciais(objqueryrepeticao.RecordCount,0);
         FMostraBarraProgresso.Lbmensagem.caption:='Gerando relat�rio....';
         FMostraBarraProgresso.btcancelar.Visible:=True;
         objqueryrepeticao.first;

         //resgatando a linha do primeiro produto de repeticao
         Frelatoriopersonalizado.RetornaPrimeiroProdutorepeticao;
         
         //PprimeiroProduto:=Plinhaproduto;

         //Retorna Lista de Campos TotalFinal

         PcamposExtras.clear;
         PvalorCamposExtras.clear;

         pcampostotalfinal.clear;
         pvalorcampostotalfinal.clear;

         pcampossubtotal.clear;
         pvalorcampossubtotal.clear;

         pcampoauditorasubtotal.clear;
         pvalorcampoauditoriasubtotal.clear;

         FrelatorioPersonalizado.RetornaCamposporTipo('TOTALFINAL',pcampostotalfinal);
         for cont:=0 to pcampostotalfinal.Count-1 do
           pvalorcampostotalfinal.add('0');

         {FrelatorioPersonalizado.RetornaCamposporTipo('TOTALFINAL_MEDIA',pcampostotalfinal_media);
         for cont:=0 to pcampostotalfinal_media.Count-1 do
         Begin
              pvalorcampostotalfinal_media.add('0');
         End;}


         FrelatorioPersonalizado.RetornaCamposporTipo('SUBTOTAL',pcampossubtotal,pcampoauditorasubtotal);
         for cont:=0 to pcampossubtotal.Count-1 do
           pvalorcampossubtotal.add('0');

         {FrelatorioPersonalizado.RetornaCamposporTipo('SUBTOTAL_MEDIA',pcampossubtotal_media,pcampoauditorasubtotal_media);
         for cont:=0 to pcampossubtotal.Count-1 do
           pvalorcampossubtotal.add('0');
          }

         //guardando o valor inicial dos campos de auditoria
         (*Exemplo

         Em um relatorio de conta a receber quero subtotalizar por cliente
         entao o campo a ser somando � o campo saldo da parcela
         por�m o campo a ser auditorado � c�digo do cliente

         imaginemos a situacao


         Cliente      Vencimento      Saldo
         -------------------------------------
         01             01/01        100,00
         01             01/02         50,00
         02             01/03         70,00
         02             01/05         30,00
         03             01/02         10,00
         -------------------------------------
         TOTAL FINAL                 260,00

         No exemplo acima o primeiro cliente � o n�mero 01
         sendo assim, preciso inicialir o campo valor auditoria
         com01 e vir verificando se ele mudou
         caso tenha mudado imprimo o valor acumulado do saldo 
         *)
         for cont:=0 to pcampoauditorasubtotal.count-1 do
         begin
              pvalorcampoauditoriasubtotal.add(objqueryrepeticao.fieldbyname(pcampoauditorasubtotal[cont]).asstring);
         End;


         pquantidadefolhas:=1;
         contalinharepeticao:=0;//mando com zero pra indicar que � pra usar a linha configurada e nao a atual
         Frelatoriopersonalizado.ImprimeCamposPreImpresso(Objquerylocal,false,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'');

          //ITENS DE REPETICAO
         contalinharepeticao:=FrelatorioPersonalizado.PLinhaPrimeiroProdutoRepeticao;
         folhaatual:=1;


         Try
            While not(objqueryrepeticao.Eof) do
            begin
                 FMostraBarraProgresso.IncrementaBarra1(1);

                 if ((FMostraBarraProgresso.BarradeProgresso.Progress mod 15)=0)
                 Then Begin
                          FMostraBarraProgresso.show;
                          Application.ProcessMessages;
                 End;

                 if (FMostraBarraProgresso.Cancelar=True)
                 Then Begin
                           Objquerylocal.Close;
                           objqueryrepeticao.Close;
                           FrelatorioPersonalizado.Rdprint.Abortar;
                           exit;
                 End;


                 //Verificando os subtotais
                 for cont:= 0 to pcampossubtotal.count-1 do
                 Begin
                      //verificando se o valor atual � diferente do que foi acumulado
                      if (objqueryrepeticao.fieldbyname(pcampoauditorasubtotal[cont]).asstring<>pvalorcampoauditoriasubtotal[cont])
                      Then Begin
                                //Imprimindo a Label do Sub-total
                                VerificaLinhaLocal;
                                //plinhatemp:=(Plinhaproduto+contaprodutos-1);
                                Frelatoriopersonalizado.ImprimeCamposPreImpresso(objqueryrepeticao,true,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'LABELSUBTOTAL');
                                //inc(contaprodutos,1);


                                //Imprimindo o valor do Sub-Total
                                PcamposExtras.clear;
                                PvalorCamposExtras.clear;
                                //passando as totalizacoes para os campos extras de impressao
                                PcamposExtras.add(pcampossubtotal[cont]);
                                PvalorCamposExtras.add(formata_valor(pvalorcampossubtotal[cont]));
                                
                                VerificaLinhaLocal;
                                //plinhatemp:=(Plinhaproduto+contaprodutos-1);
                                Frelatoriopersonalizado.ImprimeCamposPreImpresso(objqueryrepeticao,true,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'SUBTOTAL');
                                contalinharepeticao:=contalinharepeticao+1;//pulando uma linha a mais
                                
                                //inc(contaprodutos,2);//para dar uma linha em branco

                                PcamposExtras.clear;
                                PvalorCamposExtras.clear;
                                //guardando o novo valor
                                pvalorcampoauditoriasubtotal[cont]:=objqueryrepeticao.fieldbyname(pcampoauditorasubtotal[cont]).asstring;
                                //zerando o valor acumulado
                                pvalorcampossubtotal[cont]:='0';
                      End;


                 End;



                 //imprimindo os campos de repeticao
                 VerificaLinhaLocal;
                 //plinhatemp:=(Plinhaproduto+contaprodutos-1);
                 Frelatoriopersonalizado.ImprimeCamposPreImpresso(objqueryrepeticao,true,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'');

                 (*if (plinhatemp>(Plinhaproduto+contaprodutos-1))
                 Then Begin                          //foi modificado dentro da impressao(um ou mais linhas am mais)
                           contaprodutos:=contaprodutos+(plinhatemp-(Plinhaproduto+contaprodutos-1));
                 End
                 else inc(contaprodutos,1);*)

                 contalinharepeticao:=contalinharepeticao+1;

                 //somando os campos de Total Final
                 for cont:= 0 to pcampostotalfinal.count-1 do
                 Begin
                      pvalorcampostotalfinal[cont]:=currtostr(strtocurr(pvalorcampostotalfinal[cont])+objqueryrepeticao.fieldbyname(pcampostotalfinal[cont]).ascurrency);

                 End;


                 //somando os campos de Sub Final
                 for cont:= 0 to pcamposSubtotal.count-1 do
                 Begin  
                      pvalorcampossubtotal[cont]:=currtostr(strtocurr(pvalorcampossubtotal[cont])+objqueryrepeticao.fieldbyname(pcampossubtotal[cont]).ascurrency);
                 End;
                 
                         
                 objqueryrepeticao.next;

            End;(*While repeticao*)


            //**************SUBTOTAL DO ULTIMO*************
            //Verificando os subtotais
            for cont:= 0 to pcampossubtotal.count-1 do
            Begin
                 //Imprimindo a Label do Sub-total
                 VerificaLinhaLocal;
                 //plinhatemp:=(Plinhaproduto+contaprodutos-1);
                 Frelatoriopersonalizado.ImprimeCamposPreImpresso(objqueryrepeticao,true,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'LABELSUBTOTAL');
                 //inc(contaprodutos,1);


                 //Imprimindo o valor do Sub-Total
                 PcamposExtras.clear;
                 PvalorCamposExtras.clear;
                 //passando as totalizacoes para os campos extras de impressao
                 PcamposExtras.add(pcampossubtotal[cont]);
                 PvalorCamposExtras.add(formata_valor(pvalorcampossubtotal[cont]));
                 
                 VerificaLinhaLocal;
                 //plinhatemp:=(Plinhaproduto+contaprodutos-1);
                 Frelatoriopersonalizado.ImprimeCamposPreImpresso(objqueryrepeticao,true,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'SUBTOTAL');
                 //inc(contaprodutos,2);//para dar uma linha em branco
                 contalinharepeticao:=contalinharepeticao+1;

                 PcamposExtras.clear;
                 PvalorCamposExtras.clear;
                 //guardando o novo valor
                 pvalorcampoauditoriasubtotal[cont]:=objqueryrepeticao.fieldbyname(pcampoauditorasubtotal[cont]).asstring;
                 //zerando o valor acumulado
                 pvalorcampossubtotal[cont]:='0';
             End;


            (*Imprimindo uma LInha*)
            verificaLinhalocal;
            Frelatoriopersonalizado.Rdprint.Imp(contalinharepeticao,1,CompletaPalavra('_',Frelatoriopersonalizado.Rdprint.TamanhoQteColunas,'_'));
            //inc(contaprodutos,1);
            contalinharepeticao:=contalinharepeticao+1;



            (*Totalizando*)

            //imprimindo labels de totalfinal
            verificaLinhalocal;
            //plinhatemp:=(Plinhaproduto+contaprodutos-1);
            Frelatoriopersonalizado.ImprimeCamposPreImpresso(objqueryrepeticao,true,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'LABELTOTALFINAL');
            //passando as totalizacoes para os campos extras de impressao
            PcamposExtras.text:=pcampostotalfinal.Text;
            for cont:=0 to pvalorcampostotalfinal.Count-1 do
            Begin
                 Try
                    pvalorcampostotalfinal[cont]:=formata_valor(pvalorcampostotalfinal[cont]);
                 Except

                 End;
            End;
            PvalorCamposExtras.text:=pvalorcampostotalfinal.text;
            verificaLinhalocal;
            //plinhatemp:=(Plinhaproduto+contaprodutos-1);
            Frelatoriopersonalizado.ImprimeCamposPreImpresso(objqueryrepeticao,true,contalinharepeticao,PcamposExtras,PvalorCamposExtras,'',true,'TOTALFINAL');
            //******************************************************************
         Finally
                FMostraBarraProgresso.Close;
                FMostraBarraProgresso.btcancelar.Visible:=False;
         End;
            Frelatoriopersonalizado.Rdprint.Fechar;
            Frelatoriopersonalizado.Encerra;
     End;

Finally
       freeandnil(objquerylocal);
       Freeandnil(PCamposExtras);
       Freeandnil(PvalorCamposExtras);
       Freeandnil(PCamposTotalFinal);
       Freeandnil(PvalorCamposTotalfinal);
       Freeandnil(PCampossubtotal);
       Freeandnil(PvalorCampossubtotal);
       Freeandnil(pcampoauditorasubtotal);
       Freeandnil(pvalorcampoauditoriasubtotal);
End;

End;


Procedure TobjRelatorioPersonalizadoExtendido.onClick(Sender:Tobject);
Begin
     if (ObjIconesglobal.VerificaClick('imprime__i__objrelatoriopersonalizacaoextendido_numero_'+inttostr(Tmenuitem(sender).tag)))
     then exit;


     //Foi utilizado a propriedade Tag para guardar o codigo da TabRelatorioPersonalizado
     Self.ImprimeRelatorio(Tmenuitem(sender).tag);
end;



procedure TobjRelatorioPersonalizadoExtendido.CriaMenu(Menu: Tmenuitem);
var
ContNIvel0:integer;
teste1,teste2,teste3:TMenuItem;
strmenuatual,strsubmenuatual,strrelatual:string;

begin
     //Criacao do Menu, para isso seleciono a TabMenu e para cada um os submenus dever�o ser criados dinamicamente
     //somente os sub-menus ter�o onclick para dar acesso ao relat�rio, utilizarei a propriedade Tag de cada um

     {ontNIvel0:=0;
     teste1:=TMenuItem.Create(Menu);
     teste2:=TMenuItem.Create(teste1);
     teste3:=TMenuItem.Create(teste1);
     Menu.Add(teste1);
     Menu.Add(teste2);
     teste1.Add(teste3);

     teste1.caption:='teste1';
     teste2.caption:='teste2';
     teste3.caption:='teste3';

     teste2.tag:=2;
     teste3.tag:=3;

     teste2.OnClick:=Self.OnClick;
     teste3.OnClick:=Self.OnClick;}

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select TABMENURELPERSONALIZADO.codigo as CODMENU,TABMENURELPERSONALIZADO.nome as MENU,');
          sql.add('TABSUBMENURELPERSONALIZADO.codigo as CODSUBMENU,TABSUBMENURELPERSONALIZADO.nome as SUBMENU,');
          sql.add('tabrelatoriopersonalizado.codigo as CODREL,tabrelatoriopersonalizado.nome as RELATORIO');
          sql.add('from TABMENURELPERSONALIZADO');
          sql.add('left join TABSUBMENURELPERSONALIZADO on TABMENURELPERSONALIZADO.codigo=TABSUBMENURELPERSONALIZADO.menu');
          sql.add('left join tabrelatoriopersonalizado on tabrelatoriopersonalizado.submenu=TABSUBMENURELPERSONALIZADO.codigo');
          sql.add('order by');
          sql.add('TABMENURELPERSONALIZADO.codigo,');
          sql.add('TABSUBMENURELPERSONALIZADO.codigo,');
          sql.add('tabrelatoriopersonalizado.codigo');
          open;

          if (recordcount=0)
          then exit;

          strmenuatual:='';
          strsubmenuatual:='';
          strrelatual:='';
          contmenu:=0;
          contsubmenu:=0;
          contrel:=0;

          While not(eof) do
          Begin
               if (fieldbyname('codmenu').asstring<>strmenuatual)
               Then Begin
                         //criando um novo Menu
                         VetorMenu[contmenu]:=TMenuItem.Create(Menu);
                         menu.add(VetorMenu[contmenu]);
                         VetorMenu[contmenu].caption:=fieldbyname('menu').asstring;
                         strmenuatual:=fieldbyname('codmenu').asstring;
                         inc(contmenu,1);
               End;

               if ((fieldbyname('codsubmenu').asstring<>strsubmenuatual)
               and (fieldbyname('codsubmenu').asstring<>''))
               Then Begin
                         //criando um novo Sub-Menu
                         VetorSubMenu[contsubmenu]:=TMenuItem.Create(VetorMenu[contmenu-1]);
                         VetorMenu[contmenu-1].add(VetorSubMenu[contsubmenu]);
                         VetorSubMenu[contsubmenu].caption:=fieldbyname('submenu').asstring;
                         strsubmenuatual:=fieldbyname('codsubmenu').asstring;
                         inc(contsubmenu,1);
               End;

               if ((fieldbyname('codrel').asstring<>strrelatual)
               and (fieldbyname('codrel').asstring<>''))
               Then Begin
                         //criando um novo Relatorio
                         Vetorrel[contrel]:=TMenuItem.Create(VetorSubMenu[contsubmenu-1]);
                         VetorSubMenu[contsubmenu-1].add(Vetorrel[contrel]);
                         VetorRel[contrel].caption:=fieldbyname('relatorio').asstring;
                         VetorRel[contrel].tag:=fieldbyname('codrel').asinteger;
                         VetorRel[contrel].OnClick:=Self.OnClick;
                         strrelatual:=fieldbyname('codrel').asstring;
                         inc(contrel,1);
               End;

               next;
          End;
     End;
End;

constructor TobjRelatorioPersonalizadoExtendido.create;
begin
     Self.Objfiltro:=tobjfiltrorelpersonalizadoextendido.create;
     Self.ObjqueryLocal:=tibquery.create(nil);
     Self.ObjqueryLocal.database:=Fdatamodulo.IBDatabase;
     Self.ObjOrderBy:=TobjOrderByRelPersonalizadoextendido.Create;
     Self.OrdenacaoPadrao:=TStringList.create;
     Self.objarquivoiniX:=Tobjarquivoini.create;
     inherited create;

end;

procedure TobjRelatorioPersonalizadoExtendido.GravaUltimoSqlPrincipal(pcodigo:integer;psql:string);
begin
     With Self.ObjqueryLocal do
     Begin
          close;
          sql.clear;
          sql.add('Update tabrelatoriopersonalizado set ultimosqlprincipal=:ultimosqlprincipal where codigo='+inttostr(pcodigo));
          ParamByName('ultimosqlprincipal').asstring:=psql;
          Try
              ExecSQL;
              FDataModulo.IBTransaction.CommitRetaining;
          Except
                on e:exception do
                Begin
                      mensagemerro('Erro na tentativa de gravar o �ltimo sql principal. Erro: '+E.message+#13+'SQL: '+psql);
                      exit;
                End;
          End;
     End;

end;

procedure TobjRelatorioPersonalizadoExtendido.GravaUltimoSqlRepeticao(pcodigo:integer;psql:string);
begin
      With Self.ObjqueryLocal do
     Begin
          close;
          sql.clear;
          sql.add('Update tabrelatoriopersonalizado set ultimosqlrepeticao=:ultimosqlrepeticao where codigo='+inttostr(pcodigo));
          ParamByName('ultimosqlrepeticao').asstring:=psql;
          Try
              ExecSQL;
              FDataModulo.IBTransaction.CommitRetaining;
          Except
                on e:exception do
                Begin
                      mensagemerro('Erro na tentativa de gravar o �ltimo sql Repeti��o. Erro: '+E.message+#13+'SQL: '+psql);
                      exit;
                End;
          End;
     End;
end;

destructor TobjRelatorioPersonalizadoExtendido.free;
var
cont:integer;
begin
     inherited free;

(*
     for cont:=1 to contmenu do
     freeandnil(VetorMenu[cont]);

     for cont:=1 to contsubmenu do
     freeandnil(VetorsubMenu[cont]);

     for cont:=1 to contrel do
     freeandnil(VetorRel[cont]);
     *)

     Self.ObjOrderBy.free;
     Self.objarquivoiniX.Free;
     Freeandnil(Self.ObjqueryLocal);
     Freeandnil(Self.OrdenacaoPadrao);

     //adicionado 301109 - celio
     Self.Objfiltro.free;

end;

procedure TobjRelatorioPersonalizadoExtendido.BotaoRestauraPadraoClick(
  Sender: TObject);
begin
     Ffiltrorelpersonalizado.ListBoxGrupo[TSpeedButton(sender).Tag].Items.Text:=Self.OrdenacaoPadrao.Text;
end;

procedure TobjRelatorioPersonalizadoExtendido.RetornaOrderByegravaordem(prelatorio:string;
  ListboxX: TListBox; var OrderbyRepeticao: string;GravaDuasOrdens:Boolean);
var
cont:integer;
begin
     for cont:=0 to ListBoxX.items.count-1 do
     Begin
          //ordem definida pelo cliente
          if (Self.ObjOrderBy.LocalizaOrderBy(ListBoxX.items[cont],prelatorio)=False)
          Then Begin
                    MensagemErro('Filtro de Ordem "'+ListBoxX.items[cont]+'" n�o encontrado');
                    exit;
          End;
          Self.ObjOrderBy.TabelaparaObjeto;

          if (cont=0)
          Then OrderbyRepeticao:='order by '+self.ObjOrderBy.Get_comando
          Else OrderbyRepeticao:=OrderbyRepeticao+','+self.ObjOrderBy.Get_comando;

          Self.ObjOrderBy.Status:=dsedit;
          Self.ObjOrderBy.Submit_ordem(inttostr(cont+1));

          if (GravaDuasOrdens)
          Then Self.ObjOrderBy.Submit_ordemdefault(inttostr(cont+1));

          if (Self.ObjOrderBy.Salvar(true)=False)
          Then Begin
                    MensagemErro('Erro na tentativa de Alterar a Ordem');
                    exit;
          End;
     End;

end;

procedure TobjRelatorioPersonalizadoExtendido.ImportaExporta(
  prelatorio: string);
begin
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Exportar Relat�rio Atual');
          RgOpcoes.Items.add('Importar Relat�rio em Arquivo');

          showmodal;

          if (tag=0)
          Then exit;

          if (RgOpcoes.ItemIndex=0)
          Then Self.ExportaRelAtual(prelatorio)
          Else Self.ImportaRel;
     End;
end;

procedure TobjRelatorioPersonalizadoExtendido.ExportaRelAtual(prelatorio: string);
var
Parquivo:TStringList;
SaveDialog:TSaveDialog;
cont:integer;
temp:string;
begin
     if (prelatorio='')
     Then Begin
               MensagemAviso('Escolha o relat�rio que deseja exportar');
               exit;
     End;

     if (Self.LocalizaCodigo(prelatorio)=False)
     Then Begin
               MensagemErro('Relat�rio '+prelatorio+' n�o encontrado');
               exit;
     End;
     Self.TabelaparaObjeto;

     Try
        cont:=1;
        Parquivo:=TStringList.Create;
        cont:=2;
        SaveDialog:=TSaveDialog.create(nil);
     Except
           if (cont=1)
           Then MensagemErro('Erro na tentativa de criar uma String List')
           Else Begin
                     freeandnil(Parquivo);
                     MensagemErro('Erro na tentativa de criar a Save Dialog');
           End;
           exit;
     End;

     Try

        if (SaveDialog.execute=False)
        Then exit;

        With PArquivo do
        Begin
            (*  Estou usando   ##@##  no lugar de ap�strofo '

            depois na hora de importar eu fa�o um stringreplace para ap�strofo de novo

            o Campo Relat�rio, SubMenu e Permiss�o pode mudar em cada cliente
            por isso uso IDENTIFICADORES  CODIGOX, SUBMENUX  e PERMISSAOX

            na hora de importar

            depois que criar um novo c�digo para o relat�rio que sera importado
            e escolher o submenu e a permissao
            fa�o um stringreplace nestes identificadores para os c�digos
            corretos no cliente

            *)




            Clear;
            add('Insert Into TabRelatorioPersonalizado(CODIGO,SubMenu');
            add(' ,Nome,Permissao,SqlPrincipal,SqlRepeticao,TabelaPrincipal');
            add(' ,orderbypersonalizado,TabelaRepeticao,UltimoSqlPrincipal,UltimoSqlRepeticao)');
            add('values (##@##CODIGOX##@##,##@##SUBMENUX##@##,##@##'+Self.Nome+'##@##,##@##PERMISSAOX##@##,##@##'+Self.SqlPrincipal+'##@##');
            add(' ,##@##'+Self.SqlRepeticao+'##@##,##@##'+Self.TabelaPrincipal+'##@##,##@##'+Self.orderbypersonalizado+'##@##,##@##'+Self.TabelaRepeticao+'##@##,');
            add('##@##'+Self.UltimoSqlPrincipal+'##@##,##@##'+Self.UltimoSqlRepeticao+'##@##);');


            Self.ObjqueryLocal.close;
            Self.ObjqueryLocal.sql.clear;
            Self.ObjqueryLocal.sql.add('Select * from taborderbyrelpersonalizado');
            Self.ObjqueryLocal.sql.add('where relatorio='+Self.CODIGO);
            Self.ObjqueryLocal.open;

            while not(Self.ObjqueryLocal.eof) do
            Begin
                 add('Insert Into Taborderbyrelpersonalizado(CODIGO,relatorio');
                 add(' ,Nome,comando,ordem,ordemdefault)');
                 add('values (0,CODIGOX,##@##'+Self.objquerylocal.fieldbyname('Nome').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('comando').asstring+'##@##,');
                 add('##@##'+Self.objquerylocal.fieldbyname('ordem').asstring+'##@##,'+'##@##'+Self.objquerylocal.fieldbyname('ordemdefault').asstring+'##@## );');
                 Self.ObjqueryLocal.next;
            End;

            Self.ObjqueryLocal.close;
            Self.ObjqueryLocal.sql.clear;
            Self.ObjqueryLocal.sql.add('Select * from tabfiltrorelpersonalizado');
            Self.ObjqueryLocal.sql.add('where relatorio='+Self.CODIGO);
            Self.ObjqueryLocal.open;

            while not(Self.ObjqueryLocal.eof) do
            Begin
                add('Insert Into TabFiltroRelpersonalizado(CODIGO,relatorio');
                add(' ,tipo,nome,captionlabel,mascara,requerido,usamesmoembranco,comandosqlchaveestrangeira');
                add(' ,campochavestrangeira,comandoandprincipal,comandoandrepeticao,MULTIPLAESCOLHA,CAMPOMULTIPLAESCOLHA');
                add(' )');
                add('values (##@##'+'0'+'##@##,##@##'+'CODIGOX'+'##@##,##@##'+Self.objquerylocal.fieldbyname('tipo').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('nome').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('captionlabel').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('mascara').asstring+'##@##');
                add(',##@##'+Self.objquerylocal.fieldbyname('requerido').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('usamesmoembranco').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('comandosqlchaveestrangeira').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('campochavestrangeira').asstring+'##@##');
                add(',##@##'+Self.objquerylocal.fieldbyname('comandoandprincipal').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('comandoandrepeticao').asstring+'##@##,##@##'+Self.objquerylocal.fieldbyname('MULTIPLAESCOLHA').asstring+'##@##');
                add(',##@##'+Self.objquerylocal.fieldbyname('CAMPOMULTIPLAESCOLHA').asstring+'##@##);');



                Self.ObjqueryLocal.next;
            End;

            if (Self.objarquivoiniX.Localizanome('CONF_PAGINA_REL_PERSONALIZADO_'+Self.codigo)=True)
            Then BEgin
                      Self.objarquivoiniX.TabelaparaObjeto;
                      Self.objarquivoiniX.Arquivo.SaveToFile(SaveDialog.FileName+'CONFPAGINA');
            End;

            if (Self.objarquivoiniX.Localizanome('REL_PERSONALIZADO_'+Self.codigo)=True)
            Then Begin
                      Self.objarquivoiniX.TabelaparaObjeto;
                      Self.objarquivoiniX.Arquivo.SaveToFile(SaveDialog.FileName+'CONFCAMPOS');
            End;

            SaveToFile(SaveDialog.FileName);
            MensagemAviso('Conclu�do');
        End;

     Finally
            Freeandnil(Parquivo);
            Freeandnil(SaveDialog);
     End;

end;

procedure TobjRelatorioPersonalizadoExtendido.ImportaRel;
var
Parquivo:TStringList;
OpenDialog:TOpenDialog;
cont:integer;
pcodigo,psubmenu,ppermissao:string;

IBScript: TIBScript;
IBDataSetScript: TIBDataSet;

begin
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=True;
          Grupo02.Enabled:=True;
          LbGrupo01.Caption:='Sub-Menu';
          LbGrupo02.Caption:='Permiss�o';
          edtgrupo01.OnKeyDown:=Self.EdtSubMenuKeyDown;
          edtgrupo02.OnKeyDown:=Self.EdtpermissaoKeyDown;

          showmodal;

          if (tag=0)
          Then exit;
          psubmenu:=edtgrupo01.Text;

          if(psubmenu='')
          Then BEgin
                    MensagemErro('Sub-Menu n�o localizado');
                    exit;

          End;


          if (Self.SubMenu.LocalizaCodigo(psubmenu)=False)
          Then Begin
                    MensagemErro('Sub-Menu n�o localizado');
                    exit;
          End;



          ppermissao:=edtgrupo02.Text;

          if (ppermissao='')
          Then BEgin
                    MEnsagemErro('Escolha uma permiss�o');
                    exit;
          End;

          if (ObjPermissoesUsoGlobal.LocalizaCodigo(edtgrupo02.Text)=False)
          Then Begin
                    MensagemErro('Permiss�o n�o localizada');
                    exit;
          End;

     End;




     Try
        cont:=1;
        Parquivo:=TStringList.Create;
        cont:=2;
        OpenDialog:=TOpenDialog.create(nil);
        cont:=3;
        ibDATASETScript:=TIBDataSet.Create(nil);
        cont:=4;
        IBScript:=TIBScript.Create(nil);
     Except
           case cont of

           1: MensagemErro('Erro na tentativa de criar uma String List');
           2:Begin
                  freeandnil(Parquivo);
                  MensagemErro('Erro na tentativa de criar a OPen Dialog');
              End;
           3:Begin
                  freeandnil(Parquivo);
                  freeandnil(OpenDialog);
                  MensagemErro('Erro na tentativa de criar o DataSet Script');
              End;
           4:Begin
                  freeandnil(Parquivo);
                  freeandnil(OpenDialog);
                  freeandnil(IBDataSetScript);
                  MensagemErro('Erro na tentativa de criar o IBSCRIPT');
              End;
           End;
           exit;
     End;

     Try
        if (OpenDialog.execute=False)
        Then exit;
        
        Parquivo.LoadFromFile(OpenDialog.FileName);
        pcodigo:=Self.Get_NovoCodigo;
        parquivo.text:=StringReplace(Parquivo.Text,'SUBMENUX',psubmenu,[rfReplaceAll, rfIgnoreCase]);
        parquivo.text:=StringReplace(Parquivo.Text,'PERMISSAOX',ppermissao,[rfReplaceAll, rfIgnoreCase]);
        parquivo.text:=StringReplace(Parquivo.Text,'''','''''',[rfReplaceAll, rfIgnoreCase]);
        parquivo.text:=StringReplace(Parquivo.Text,'##@##','''',[rfReplaceAll, rfIgnoreCase]);
        parquivo.text:=StringReplace(Parquivo.Text,'CODIGOX',pcodigo,[rfReplaceAll, rfIgnoreCase]);

        FmostraStringList.Memo.Lines.Text:=Parquivo.Text;
        FmostraStringList.ShowModal;
        //executa SQL




        ibDATASETScript.database:=FdataModulo.Ibdatabase;
        IBScript.Dataset:=IBDataSetScript;
        IBScript.Database:=FdataModulo.Ibdatabase;
        IBScript.Transaction:=FdataModulo.IBTransaction;
        IBScript.Terminator:=';';
        IBScript.Script:=parquivo;
        try
            IBScript.ExecuteScript;
        Except
               on e:exception do
               begin
                    Messagedlg('Erro na tentativa de Executar o SQL '+#13+E.message,mterror,[mbok],0);
                    exit;
               End;
        End;

        Self.objarquivoiniX.ZerarTabela;
        Self.objarquivoiniX.Submit_CODIGO('0');
        Self.objarquivoiniX.Submit_nome('CONF_PAGINA_REL_PERSONALIZADO_'+pcodigo);
        Self.objarquivoiniX.Arquivo.LoadFromFile(OpenDialog.FileName+'CONFPAGINA');
        Self.objarquivoiniX.Status:=dsInsert;
        if (Self.objarquivoiniX.Salvar(False)=False)
        Then Begin
                  mensagemErro('Erro na tentativa de salvar a Configura��o de p�gina na Tabela de Arquivo INI');
                  exit;
        End;

        Self.objarquivoiniX.ZerarTabela;
        Self.objarquivoiniX.Submit_CODIGO('0');
        Self.objarquivoiniX.Submit_nome('REL_PERSONALIZADO_'+pcodigo);
        Self.objarquivoiniX.Arquivo.LoadFromFile(OpenDialog.FileName+'CONFCAMPOS');
        Self.objarquivoiniX.Status:=dsInsert;
        if (Self.objarquivoiniX.Salvar(False)=False)
        Then Begin
                  mensagemErro('Erro na tentativa de salvar a Configura��o de campos na Tabela de Arquivo INI');
                  exit;
        End;

        FDataModulo.ibtransaction.CommitRetaining;
        MensagemAviso('Relat�rio Importado com Sucesso. N� do Novo Relat�rio: '+pcodigo);
     Finally
            FDataModulo.ibtransaction.rollbackretaining;
            Freeandnil(Parquivo);
            Freeandnil(OpenDialog);
            freeandnil(IBDataSetScript);
            freeandnil(IBScript);
     End;

end;

function TobjRelatorioPersonalizadoExtendido.Exclui(Pcodigo: string;
  ComCommit: boolean): Boolean;
begin
     Try
         Try
             With Self.ObjqueryLocal do
             Begin
                  close;
                  SQL.clear;
                  sql.add('Delete from taborderbyrelpersonalizado where relatorio='+pcodigo);
                  ExecSQL;

                  close;
                  SQL.clear;
                  sql.add('Delete from tabfiltrorelpersonalizado where relatorio='+pcodigo);
                  ExecSQL;

                  if (Self.objarquivoiniX.Localizanome('CONF_PAGINA_REL_PERSONALIZADO_'+pcodigo)=True)
                  Then Begin
                            Self.objarquivoiniX.TabelaparaObjeto;
                            if (Self.objarquivoiniX.Exclui(Self.objarquivoiniX.get_codigo,false)=False)
                            Then Begin
                                      MensagemErro('Erro na tentativa de excluir a Configura��o da P�gina');
                                      exit;
                            End;
                  End;

                  if (Self.objarquivoiniX.Localizanome('REL_PERSONALIZADO_'+pcodigo)=True)
                  Then Begin
                            Self.objarquivoiniX.TabelaparaObjeto;
                            if (Self.objarquivoiniX.Exclui(Self.objarquivoiniX.get_codigo,false)=False)
                            Then Begin
                                      MensagemErro('Erro na tentativa de excluir a Configura��o da dos Campos');
                                      exit;
                            End;
                  End;

             End;

             inherited exclui(pcodigo,ComCommit);
         Except
               on e:exception do
               Begin
                    MensagemErro(e.message);
                    exit;
               End;
         End;
         
     Finally
            FDataModulo.IBTransaction.RollbackRetaining;
     End;
end;

end.
 

