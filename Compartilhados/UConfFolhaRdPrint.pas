unit UConfFolhaRdPrint;
interface
uses RDprint, inifiles,sysutils,forms,dialogs;
Type
   TObjConfiguraFolhaRdPrint=class

          Public
                Procedure ResgataRDPRINTINI(RDPrint1:TRdPrint;NomeINI:String);
          Private
          End;


implementation


procedure TObjConfiguraFolhaRdPrint.ResgataRDPRINTINI(RDPrint1:TRdPrint;NomeINI:String);
var
NOMECAMPO,Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin
     Temp:='';
     Temp:=ExtractFilePath(Application.ExeName);

     If (temp[length(temp)]<>'\')
     Then temp:=temp+'\';

     Try
        Arquivo_ini:=Tinifile.Create(Temp+'CONFRELS\'+nomeini);
     Except
           Messagedlg('Erro na Abertura do Arquivo CONFRELS\'+nomeini,mterror,[mbok],0);
           exit;
     End;
Try
     Try


            NOMECAMPO:='PREVIEWZOOM';
            Temp:=arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,'');

            if (temp<>'')
            Then Begin
                      strtoint(temp);
                      rdprint1.OpcoesPreview.PreviewZoom:=strtoint(temp);
            End;



            NOMECAMPO:='PREVIEW';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.OpcoesPreview.Preview := True;
            if (temp='N�O')
            Then rdprint1.OpcoesPreview.Preview := False;

            NOMECAMPO:='CAPTIONSETUP';
            Temp:=arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,'');
            rdprint1.CaptionSetup:=temp;

            NOMECAMPO:='TESTARPORTA';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.TestarPorta:=False;
            IF (TEMP='SIM')
            tHEN rdprint1.TestarPorta:=True;

            NOMECAMPO:='FONTETAMANHOPADRAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.FonteTamanhoPadrao:=S12cpp;
            if (Temp='S12CPP')
            Then rdprint1.FonteTamanhoPadrao:=S12cpp
            Else
            if (temp='S05CPP')
            Then rdprint1.FonteTamanhoPadrao:=S05cpp
            Else
            if (temp='S10CPP')
            Then rdprint1.FonteTamanhoPadrao:=S10cpp
            eLSE
            if (temp='S17CPP')
            Then rdprint1.FonteTamanhoPadrao:=S17cpp
            eLSE
            if (temp='S20CPP')
            Then rdprint1.FonteTamanhoPadrao:=S20cpp;

            NOMECAMPO:='QUANTIDADELINHAS';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.TamanhoQteLinhas:=66;
            if (temp<>'')
            Then Begin
                      try
                         StrToInt(temp);
                         RDPrint1.TamanhoQteLinhas:=StrToInt(temp);
                      except
                            Messagedlg('Erro  no valor da QUANTIDADE DE LINHAS',mterror,[mbok],0);
                      end;
            End;

            NOMECAMPO:='QUANTIDADECOLUNAS';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.TamanhoQteColunas:=96;
            if (temp<>'')
            Then Begin
                      try
                         StrToInt(temp);
                         RDPrint1.TamanhoQteColunas:=StrToInt(temp);
                      except
                            Messagedlg('Erro  no valor da QUANTIDADE DE COLUNAS',mterror,[mbok],0);
                      end;
            End;


            NOMECAMPO:='PORTACOMUNICACAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.PortaComunicacao:=TEMP;


            NOMECAMPO:='ACENTUACAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.Acentuacao:=SemAcento;
            IF (TEMP='SIM')
            tHEN rdprint1.Acentuacao:=Transliterate;

            NOMECAMPO:='USAGERENCIADORIMPRESSAO';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.UsaGerenciadorImpr:=FALSE;
            IF (TEMP='SIM')
            tHEN rdprint1.UsaGerenciadorImpr:=TRUE;

            NOMECAMPO:='ENTRELINHAS';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));
            rdprint1.TamanhoQteLPP:=Seis;
            IF (TEMP='SEIS')
            tHEN rdprint1.TamanhoQteLPP:=Seis
            Else
            IF (TEMP='OITO')
            tHEN rdprint1.TamanhoQteLPP:=Oito;

            NOMECAMPO:='IMPRESSORA';
            Temp:=UPPERCASE(arquivo_ini.ReadString('CONFIGURACOES',NOMECAMPO,''));

            IF (TEMP<>'')
            tHEN rdprint1.SetPrinterbyName(Temp);

     Except
           Messagedlg('Erro na leitura do CHAVE '+NOMECAMPO,mterror,[mbok],0);
           exit;
     End;
Finally
     FreeAndNil(arquivo_ini);
End;
end;


end.
