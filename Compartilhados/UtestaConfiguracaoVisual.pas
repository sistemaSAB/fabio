unit UtestaConfiguracaoVisual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, ComCtrls;

type
  TFtestaConfiguracaoVisual = class(TForm)
    Label1: TLabel;
    Edit1: TEdit;
    Memo1: TMemo;
    CheckBox1: TCheckBox;
    RadioButton1: TRadioButton;
    ComboBox1: TComboBox;
    MaskEdit1: TMaskEdit;
    ListBox1: TListBox;
    RadioGroup1: TRadioGroup;
    RichEdit1: TRichEdit;
    Shape1: TShape;
    StatusBar1: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RichEdit1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure CheckBox1Click(Sender: TObject);
    procedure ListBox1Enter(Sender: TObject);
    procedure Memo1Exit(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FtestaConfiguracaoVisual: TFtestaConfiguracaoVisual;

implementation

uses UconfiguracoesVisuaisFormulario, UessencialGlobal;

{$R *.dfm}

procedure TFtestaConfiguracaoVisual.FormShow(Sender: TObject);
begin
     Habilita_campos(Self);

     //*************aqui**********
     FconfiguracoesVisuaisFormulario.Inicializa(Self);
     //*************************************************************************
end;

procedure TFtestaConfiguracaoVisual.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    //************aqui************
    FconfiguracoesVisuaisFormulario.KeyDownConfiguracao(sender,Key,shift);
    //****************************
end;

procedure TFtestaConfiguracaoVisual.RichEdit1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
      MensagemAviso('Voc� Clicou e ativou o OnMouseDown');
end;

procedure TFtestaConfiguracaoVisual.CheckBox1Click(Sender: TObject);
begin
     MensagemAviso('Voc� Clicou e ativou o OnClick');
end;

procedure TFtestaConfiguracaoVisual.ListBox1Enter(Sender: TObject);
begin
     Showmessage('On enter');
end;

procedure TFtestaConfiguracaoVisual.Memo1Exit(Sender: TObject);
begin
     Showmessage('on exit');
end;

procedure TFtestaConfiguracaoVisual.Edit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Showmessage('Teste de OnKeyDown');
end;

end.
