unit UObjVENCTOPEND;
Interface
Uses controls,forms,windows,Classes,Db,Uessencialglobal,Ibcustomdataset,IBStoredProc
//USES
;

Type
   TObjVENCTOPEND=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                EncerrarAplicacao:Boolean;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Pendencia(parametro: string);
                Function Get_Pendencia: string;
                Procedure Submit_Vencimento(parametro: string);
                Function Get_Vencimento: string;

                Function ValidaEntrada(out PVenctoSistema:Tdatetime):Boolean;
                Function SenhaVencimento:boolean;
                Function ApagaVencimento:Boolean;
                Function RetornaDataServidor:Tdate;

         Private
               ObjDataset:Tibdataset;
               Codigo:string;
               Pendencia:string;
               Vencimento:string;
               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Function SenhaData:boolean;

End;


implementation
uses SysUtils,Dialogs,UDatamodulo
//USES IMPLEMENTATION
, Useg, UAcertaDataServidor, UAcertaVenctoServidor;


{ TTabTitulo }


Function  TObjVENCTOPEND.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Pendencia:=fieldbyname('Pendencia').asstring;
        Self.Vencimento:=fieldbyname('Vencimento').asstring;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjVENCTOPEND.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('Codigo').asstring:=Self.Codigo;
        fieldbyname('Pendencia').asstring:=Self.Pendencia;
        fieldbyname('Vencimento').asstring:=Self.Vencimento;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjVENCTOPEND.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjVENCTOPEND.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Pendencia:='';
        Vencimento:='';
//CODIFICA ZERARTABELA



     End;
end;

Function TObjVENCTOPEND.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      If (Pendencia='')
      Then Mensagem:=mensagem+'/Pendencia';
      If (Vencimento='')
      Then Mensagem:=mensagem+'/Vencimento';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjVENCTOPEND.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjVENCTOPEND.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        Strtoint(Self.Pendencia);
     Except
           Mensagem:=mensagem+'/Pendencia';
     End;
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjVENCTOPEND.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjVENCTOPEND.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjVENCTOPEND.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select Codigo,Pendencia,Vencimento');
           SelectSQL.ADD(' from  TabVenctoPend');
           SelectSQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjVENCTOPEND.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjVENCTOPEND.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjVENCTOPEND.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.EncerrarAplicacao:=False;
        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

//CODIFICA CRIACAO DE OBJETOS

        ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select Codigo,Pendencia,Vencimento');
                SelectSQL.ADD(' from  TabVenctoPend');
                SelectSQL.ADD(' WHERE codigo=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TabVenctoPend(Codigo,Pendencia,Vencimento');
                InsertSQL.add(' )');
                InsertSQL.add('values (:Codigo,:Pendencia,:Vencimento)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabVenctoPend set Codigo=:Codigo,Pendencia=:Pendencia');
                ModifySQL.add(',Vencimento=:Vencimento');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabVenctoPend where codigo=:codigo ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select Codigo,Pendencia,Vencimento');
                RefreshSQL.ADD(' from  TabVenctoPend');
                RefreshSQL.ADD(' WHERE codigo=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjVENCTOPEND.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjVENCTOPEND.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabVENCTOPEND');
     Result:=Self.ParametroPesquisa;
end;

function TObjVENCTOPEND.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de VENCTOPEND ';
end;


function TObjVENCTOPEND.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
           StrTemp.StoredProcName:='PROC_GERA_VENCTOPEND';
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o VENCTOPEND',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjVENCTOPEND.Free;
begin
Freeandnil(Self.ObjDataset);
Freeandnil(Self.ParametroPesquisa);
//CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjVENCTOPEND.RetornaCampoCodigo: string;
begin
      result:='';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjVENCTOPEND.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjVenctoPend.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjVenctoPend.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjVenctoPend.Submit_Pendencia(parametro: string);
begin
        Self.Pendencia:=Parametro;
end;
function TObjVenctoPend.Get_Pendencia: string;
begin
        Result:=Self.Pendencia;
end;
procedure TObjVenctoPend.Submit_Vencimento(parametro: string);
begin
        Self.Vencimento:=Parametro;
end;
function TObjVenctoPend.Get_Vencimento: string;
begin
        Result:=Self.Vencimento;
end;

function TObjVENCTOPEND.ValidaEntrada(out PVenctoSistema:TdateTime): Boolean;
var
  STRP:TIBStoredProc;
  DataHoraAtual:TDateTime;
  strvencimento,tmpultimoacesso:String;
  erro:boolean;
begin

  result:=False;

  Try
    STRP:=TIBStoredProc.create(nil);
    STRP.database:=ObjDataset.Database;
  Except
    Messagedlg('Erro na Cria��o da StoredProcedure!',mterror,[mbok],0);
    exit;
  End;

  Try
    STRP.StoredProcName:='PROCLOGVENCTOPEND';
    STRP.ExecProc;
    DataHoraAtual:=STRP.ParamByname('datahora').asdatetime;
    tmpultimoacesso:='';
    tmpultimoacesso:=STRP.ParamByname('maior').asstring;
  Finally
    FreeAndNil(STRP);
  End;

  if (tmpultimoacesso='') Then
  Begin
    Messagedlg('Erro ao recuperar a data',mterror,[mbok],0);
    result:=Self.Senhadata;

    if (result = False) Then
      exit;

    tmpultimoacesso:=FacertaDataServidor.edtdata.Text+' '+FacertaDataServidor.edthora.Text+':00';
  End
  Else
    tmpultimoacesso:=useg.DesincriptaSenha(tmpultimoacesso);

  Try
    strtodatetime(tmpultimoacesso);
  Except
    MEssagedlg('Erro na valida��o da data',mterror,[mbok],0);
    result:=Self.Senhadata;
    exit;
  End;

  if (DataHoraAtual<StrToDateTime(tmpultimoacesso)) Then
  Begin
    Messagedlg('A data do Servidor Alterada!',mterror,[mbok],0);
    result:=Self.Senhadata;
    exit;
  End;

  {Gravando este acesso}
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add('Delete from TabVenctoPend');
    execsql;
  End;

  Self.ZerarTabela;
  Self.Status:=dsInsert;
  Self.Submit_Codigo('0');
  Self.Submit_Pendencia('0');
  Self.Submit_Vencimento(useg.EncriptaSenha(datetimetostr(DataHoraAtual)));

  if (Self.Salvar(True) = False) Then
  Begin
    Messagedlg('Erro na Grava��o da VenctoPend!',mterror,[mbok],0);
    exit;
  End;

  {Recuperando o Vencimento}
  With Self.ObjDataset do
  Begin
    close;
    SelectSQL.clear;
    SelectSQL.add('Select vencimento from TabVenctoTitulo where codigo=0');
    open;

    if (recordcount = 0)Then
    Begin
      Messagedlg('Data de Vencimento n�o encontrada!',mterror,[mbok],0);
      result:=Self.senhavencimento;

      if (result = False) Then
        exit;

      StrVencimento:=FacertaVenctoServidor.edtvencimento.text;
      PVenctoSistema:=strtodate(strvencimento);

      if  (strtodate(datetostr(DataHoraAtual))>strtodate(strvencimento)) Then
      Begin
        Messagedlg('Senha vencida!',mterror,[mbok],0);
        result:=False;
        exit;
      End
      Else
        exit;

    End;

    StrVencimento:=Fieldbyname('vencimento').asstring;
    StrVencimento:=useg.DesincriptaSenha(StrVencimento);

    try
      erro := False;
      strtodate(strvencimento);
    Except
      Messagedlg('Valor Inv�lido para o Vencimento!',mterror,[mbok],0);
      erro:=true;
    End;

    if (erro=true) Then
    Begin
      result:=Self.senhavencimento;

      if (result=False) Then
        exit;

      StrVencimento:=FacertaVenctoServidor.edtvencimento.text;
    End;

    PVenctoSistema:=strtodate(strvencimento);

    if  (strtodate(datetostr(DataHoraAtual))>strtodate(StrVencimento)) Then
    Begin
      Messagedlg('Senha Vencida!',mterror,[mbok],0);
      Result:=Self.senhavencimento;

      if (result = False) Then
        exit;

      StrVencimento:=FacertaVenctoServidor.edtvencimento.text;
      PVenctoSistema:=strtodate(strvencimento);

      if  (strtodate(datetostr(DataHoraAtual))>strtodate(strvencimento)) Then
      Begin
        Messagedlg('Senha vencida!',mterror,[mbok],0);
        result:=False;
        exit;
      End;

      exit;

    End;

    result:=true;

  End;

end;

function TObjVENCTOPEND.SenhaData: boolean;
begin
     result:=False;

     FacertaDataServidor.showmodal;

     if (FacertaDataServidor.tag<>1)
     Then exit;

     //gravando a nova data
     With Self.ObjDataset do
     Begin
             close;
             SelectSQL.clear;
             SelectSQL.add('Delete from TabVenctoPend');
             execsql;
     End;

     Self.ZerarTabela;
     Self.Status:=dsInsert;
     Self.Submit_Codigo('0');
     Self.Submit_Pendencia('0');
     Self.Submit_Vencimento(useg.EncriptaSenha(FacertaDataServidor.edtdata.Text+' '+FacertaDataServidor.edthora.Text+':00'));

     if (Self.Salvar(True)=False)
     Then Begin
               Messagedlg('Erro na Grava��o da VenctoPend!',mterror,[mbok],0);
               exit;
     End;

     result:=True;

end;


function TObjVENCTOPEND.SenhaVencimento: boolean;
var
temp:string;
begin
     result:=False;

     FacertaVenctoServidor.showmodal;

     if (FacertaVenctoServidor.tag<>1)
     Then exit;

     temp:=Useg.EncriptaSenha(FacertaVenctoServidor.edtvencimento.Text);

     //gravando o novo vencimento
     With Self.ObjDataset do
     Begin
             close;
             SelectSQL.clear;
             SelectSQL.add('Delete from TabVenctoTitulo');
             execsql;

             close;
             SelectSQL.clear;
             SelectSQL.add('Insert into TabVenctoTitulo (codigo,titulo,vencimento)');
             SelectSQL.add('values (0,0,'+#39+temp+#39+')');
             try
                 execsql;
                 FDataModulo.IBTransaction.CommitRetaining;
                 result:=true;
                 Self.EncerrarAplicacao:=True;
             Except
               exit;
             End;

     End;
end;

function TObjVENCTOPEND.ApagaVencimento: Boolean;
begin
     Result:=False;
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Delete from TabVenctoTitulo');
          Try
              ExecSQL;
              result:=true;
          Except
          End;
     End;
end;

function TObjVENCTOPEND.RetornaDataServidor: Tdate;
var
STRP:TIBStoredProc;
DataHoraAtual:TdateTime;
begin
     Try
        STRP:=TIBStoredProc.create(nil);
        STRP.database:=ObjDataset.Database;
     Except
           Messagedlg('Erro na Cria��o da StoredProcedure de data do servidor!',mterror,[mbok],0);
           exit;
     End;
     
     Try
        STRP.StoredProcName:='PROCLOGVENCTOPEND';
        STRP.ExecProc;
        DataHoraAtual:=STRP.ParamByname('datahora').asdatetime;
        result:=strtodate(datetostr(datahoraatual));
     Finally
            FreeAndNil(STRP);
     End;
end;

end.


