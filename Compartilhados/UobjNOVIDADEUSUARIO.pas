unit UobjNOVIDADEUSUARIO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialLocal, Uessencialglobal,IBStoredProc,uNOVIDADE, UobjNovidade,
uUSUARIOS, UobjUsuarios;

Type
   TObjNOVIDADEUSUARIO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Novidade:TOBJNOVIDADE;
                Usuario:TOBJUSUARIOS;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean; Overload;
                Function    LocalizaCodigo(PNovidade, PUsuario:string) :boolean;OverLoad;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime;
                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                procedure EdtNovidadeExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtNovidadeKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
procedure EdtUsuarioExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtUsuarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
//CODIFICA DECLARA GETSESUBMITS




                
         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios;


Function  TObjNOVIDADEUSUARIO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If(FieldByName('Novidade').asstring<>'')
        Then Begin
                 If (Self.Novidade.LocalizaCodigo(FieldByName('Novidade').asstring)=False)
                 Then Begin
                          Messagedlg('Novidade N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Novidade.TabelaparaObjeto;
        End;
        If(FieldByName('Usuario').asstring<>'')
        Then Begin
                 If (Self.Usuario.LocalizaCodigo(FieldByName('Usuario').asstring)=False)
                 Then Begin
                          Messagedlg('Usuario N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Usuario.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjNOVIDADEUSUARIO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Novidade').asstring:=Self.Novidade.GET_CODIGO;
        ParamByName('Usuario').asstring:=Self.Usuario.GET_CODIGO;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjNOVIDADEUSUARIO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjNOVIDADEUSUARIO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Novidade.ZerarTabela;
        Usuario.ZerarTabela;
//CODIFICA ZERARTABELA



     End;
end;

Function TObjNOVIDADEUSUARIO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
            If (Codigo='')
      Then Mensagem:=mensagem+'/Codigo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjNOVIDADEUSUARIO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
      If (Self.Novidade.LocalizaCodigo(Self.Novidade.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Novidade n�o Encontrado!';
      If (Self.Usuario.LocalizaCodigo(Self.Usuario.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Usuario n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjNOVIDADEUSUARIO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/Codigo';
     End;
     try
        If (Self.Novidade.Get_Codigo<>'')
        Then Strtoint(Self.Novidade.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Novidade';
     End;
     try
        If (Self.Usuario.Get_Codigo<>'')
        Then Strtoint(Self.Usuario.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Usuario';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjNOVIDADEUSUARIO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjNOVIDADEUSUARIO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA


     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjNOVIDADEUSUARIO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabNOVIDADEUSUARIO est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Novidade,Usuario');
           SQL.ADD(' from  TabNovidadeUsuario');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjNOVIDADEUSUARIO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjNOVIDADEUSUARIO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjNOVIDADEUSUARIO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Novidade:=TOBJNOVIDADE.create;
        Self.Usuario:=TOBJUSUARIOS.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabNovidadeUsuario(Codigo,Novidade,Usuario');
                InsertSQL.add(' )');
                InsertSQL.add('values (:Codigo,:Novidade,:Usuario)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabNovidadeUsuario set Codigo=:Codigo,Novidade=:Novidade');
                ModifySQL.add(',Usuario=:Usuario');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabNovidadeUsuario where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjNOVIDADEUSUARIO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjNOVIDADEUSUARIO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabNOVIDADEUSUARIO');
     Result:=Self.ParametroPesquisa;
end;

function TObjNOVIDADEUSUARIO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de NOVIDADEUSUARIO ';
end;


function TObjNOVIDADEUSUARIO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENNOVIDADEUSUARIO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENNOVIDADEUSUARIO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjNOVIDADEUSUARIO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Novidade.FREE;
Self.Usuario.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjNOVIDADEUSUARIO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjNOVIDADEUSUARIO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjNovidadeUsuario.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjNovidadeUsuario.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
//CODIFICA GETSESUBMITS


procedure TObjNOVIDADEUSUARIO.EdtNovidadeExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Novidade.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Novidade.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.Novidade.Get_Texto;
End;
procedure TObjNOVIDADEUSUARIO.EdtNovidadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FNOVIDADE:TFNOVIDADE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FNOVIDADE:=TFNOVIDADE.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Novidade.Get_Pesquisa,Self.Novidade.Get_TituloPesquisa,FNovidade)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Novidade.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Novidade.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Novidade.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FNOVIDADE);
     End;
end;
procedure TObjNOVIDADEUSUARIO.EdtUsuarioExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Usuario.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Usuario.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Usuario.GET_NOME;
End;
procedure TObjNOVIDADEUSUARIO.EdtUsuarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FUSUARIOS:TFUSUARIOS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FUSUARIOS:=TFUSUARIOS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Usuario.Get_Pesquisa,Self.Usuario.Get_TituloPesquisa,FUsuarios)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Usuario.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Usuario.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Usuario.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FUSUARIOS);
     End;
end;
//CODIFICA EXITONKEYDOWN


procedure TObjNOVIDADEUSUARIO.Imprime;
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJNOVIDADEUSUARIO';
          With RgOpcoes do
          Begin
               items.clear;
                // Addicionar os Items.Add
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               -1 : exit;       // Chamada de Func�es
          end;
     end;
end;

function TObjNOVIDADEUSUARIO.LocalizaCodigo(PNovidade,PUsuario: string): boolean;
begin
       if (PNovidade = '')
       then begin
           MessageDlg('Par�metro para localizar o codigo na TabNOVIDADEUSUARIO est� vazio.', mtError,[mbOk], 0);
           Result:=false;
           exit;
       end;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,Novidade,Usuario');
           SQL.ADD(' from  TabNovidadeUsuario');
           SQL.ADD(' WHERE Novidade = '+PNovidade);
           SQL.ADD(' AND  Usuario = '+PUsuario);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

end.



