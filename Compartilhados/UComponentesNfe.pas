unit UComponentesNfe;

interface

uses IniFiles,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, ComCtrls, OleCtrls, SHDocVw,
  ACBrNFe, ACBrNFeDANFEClass, ACBrDANFCeFortesFr, //ACBrNFeDANFERave
  ACBrBase, ACBrDFe, ACBrNFeDANFeRLClass, ACBrMail;

type
  TFComponentesNfe = class(TForm)
    OpenDialog1: TOpenDialog;
    Panel2: TPanel;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    MemoResp: TMemo;
    TabSheet6: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet1: TTabSheet;
    Memoxmlnfe: TMemo;
    ACBrNFe: TACBrNFe;
    SaveDialog: TSaveDialog;
    TabSheet2: TTabSheet;
    ACBrMail: TACBrMail;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    procedure LoadXML(MyWebBrowser: TWebBrowser);
    //Function RetornaValorCampos(Parquivo,PCampo:String):string;
  end;

var
  FComponentesNfe: TFComponentesNfe;

implementation

uses FileCtrl, ufrmStatus,pcnLeitor,pcnConversao;

const
  SELDIRHELP = 1000;

{$R *.dfm}


{ TFComponentesNfe }

procedure TFComponentesNfe.LoadXML(MyWebBrowser: TWebBrowser);
begin
    MemoResp.lines.SaveToFile(ExtractFileDir(application.ExeName)+'temp.xml');
    MyWebBrowser.Navigate(ExtractFileDir(application.ExeName)+'temp.xml');
end;

{function TFComponentesNfe.RetornaValorCampos(Parquivo,PCampo: String): string;
var
  wnProt: TLeitor;
begin
    result:='';
    Try
      wnProt := TLeitor.Create;
    Except
          on e:exception do
          Begin
               MessageDlg('Erro na tentativa de criar o Objeto WnProt', mtError, [mbOk], 0);
               exit;
          End;
    End;

    Try
       if (Pcampo='')
       Then Begin
               if (inputquery('Acessar campos','Digite o nome do campo',pcampo)=false)
               then exit;
       End;

       wnProt.CarregarArquivo(Parquivo);
       wnProt.Grupo := wnProt.Arquivo;

       result:=wnProt.rCampo(tcStr,pcampo);
    Finally
           wnProt.Free;
    End;

end;}


procedure TFComponentesNfe.FormCreate(Sender: TObject);
begin
  ACBrNFeDANFCeFortes1.PathPDF := ExtractFilePath(Application.ExeName);


end;

procedure TFComponentesNfe.FormDestroy(Sender: TObject);
begin

  {FreeAndNil(ACBrNFe);
  FreeAndNil(SaveDialog);
  FreeAndNil(ACBrNFeDANFERave1);
  FreeAndNil(OpenDialog1);
  FreeAndNil(ACBrNFeDANFCeFortes1);
  FreeAndNil(MemoResp);
  FreeAndNil(WBResposta);
  FreeAndNil(Memoxmlnfe);
  FreeAndNil(pagecontrol2);
  FreeAndNil(panel2);}

end;

end.
