object FAjuda: TFAjuda
  Left = 550
  Top = 132
  Width = 922
  Height = 665
  BorderIcons = [biSystemMenu]
  Caption = 'Ajuda'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 906
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    Color = 10643006
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    object lb1: TLabel
      Left = 382
      Top = 5
      Width = 73
      Height = 37
      Caption = 'Ajuda'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -32
      Font.Name = 'Arial Narrow'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object pnl2: TPanel
    Left = 0
    Top = 572
    Width = 906
    Height = 55
    Align = alBottom
    BevelOuter = bvNone
    Color = 10643006
    Ctl3D = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 1
  end
  object pnlItensAjuda: TPanel
    Left = 0
    Top = 51
    Width = 906
    Height = 521
    Align = alClient
    Color = clWhite
    TabOrder = 2
    object tv1: TTreeView
      Left = 1
      Top = 1
      Width = 205
      Height = 519
      Cursor = crHandPoint
      Align = alLeft
      BevelInner = bvNone
      BevelOuter = bvNone
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGray
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      Indent = 19
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
      OnClick = tv1Click
      OnKeyDown = tv1KeyDown
    end
    object mmoAjuda: TRichEdit
      Left = 206
      Top = 1
      Width = 699
      Height = 519
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      BorderStyle = bsNone
      Ctl3D = False
      ParentCtl3D = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 1
    end
  end
end
