unit Uobjvencimento;

interface
uses ibquery,SysUtils,ibdatabase,IBStoredProc;

Type
    TobjVencimento=class
      public
            constructor Create;
            destructor Free;
            function VerificaVencimento:boolean;

      private
            IbTransactionV:TIBTransaction;
            IBDataBaseV:TIBDatabase;
            objQuery,objQueryTEMP:TIBQuery;
    end;
implementation

uses UDataModulo, UessencialGlobal, DateUtils, DB, UobjParametros, Useg;


{ TobjVencimento }

constructor TobjVencimento.Create;
var
      Banco:string;
begin
      Banco := PegaPathBanco('PATH_SENHA');
      try
            objQuery:=TIBQuery.Create(nil);
            objQuery.Database := FDataModulo.IBDatabase;
      except
            on e:exception do
                  MensagemErro(e.message);
      end;

      try

            Self.IbTransactionV := TIBTransaction.Create(nil);
            Self.IbTransactionV.DefaultAction:=TARollback;
            Self.IbTransactionV.Params.Add('read_committed');
            Self.IbTransactionV.Params.Add('rec_version');
            Self.IbTransactionV.Params.Add('nowait');

            Self.IBDataBaseV:=TIBDatabase.create(nil);
            Self.IBDataBaseV.SQLDialect:=3;
            Self.IBDataBaseV.LoginPrompt:=False;
            Self.IBDataBaseV.DefaultTransaction:=Self.IbTransactionV;
            Self.IBDataBaseV.Databasename:=Banco;
            //USUARIO PROTECAO
            //SENHA   PROTECAO
            Self.IBDataBaseV.Params.clear;
            Self.IBDataBaseV.Params.Add('User_name='+DesincriptaSenha('��������'));
            Self.IBDataBaseV.Params.Add('password='+DesincriptaSenha('��������'));
            Self.IBDataBaseV.Open;

      except
            on e:exception do
                  MensagemErro(e.message);
      end;

      try
            objQueryTEMP:=TIBQuery.Create(nil);
            objQueryTEMP.Database := Self.IBDataBaseV;
      except
            on e:exception do
                  MensagemErro(e.message);
      end;
end;

destructor TobjVencimento.Free;
begin
      try
            FreeAndNil(objQuery);
            FreeAndNil(objQueryTEMP);
            FreeAndNil(IbTransactionV);
            FreeAndNil(IBDataBaseV);
      except
            on e:exception do
                  MensagemErro(e.Message);
      end;
end;

function TobjVencimento.VerificaVencimento: boolean;
var
      DataServidor,DataVencimento:TDateTime;
      STRP:TIBStoredProc;
      Dia,Mes,Ano:word;
      Temp:string;
begin
      Result := true;
      with objQuery do
      begin
            //resgato o vencimento do banco
            close;
            sql.Clear;
            sql.add('SELECT VENCIMENTO FROM TABVENCTOTITULO WHERE CODIGO=0');
            try
                  open;
            except
                  on e:exception do
                  begin
                        MensagemErro(e.Message);
                        exit;
                  end;
            end;
            if recordcount=0 //se n�o encontrou o registro, sai como true para poder atualizar
            then exit;
            Try
                  if(DesincriptaSenha(FieldByName('VENCIMENTO').AsString)='')
                  then DataVencimento := StrToDateTime('01/01/2000')
                  else DataVencimento := StrToDateTime(DesincriptaSenha(FieldByName('VENCIMENTO').AsString));
            Except
                  on e:exception do
                  begin
                        MensagemErro('Erro ao resgatar Data de Vencimento do Sistema'+#13+'Erro: '+e.message);
                        exit;
                  end;
            End;

            //resgato a data atual do servidor
            Try
                  STRP:=TIBStoredProc.create(nil);
                  STRP.database:=FDataModulo.IBDatabase;
            Except
                  MensagemErro('Erro na Cria��o da StoredProcedure!');
                  exit;
            End;
            Try
                  STRP.StoredProcName:='PROCLOGVENCTOPEND';
                  STRP.ExecProc;
                  DataServidor:=STRP.ParamByname('datahora').asdatetime;
            Finally
                  FreeAndNil(STRP);
            End;

            //resgato o codigo da base do cliente no site
            Temp := '';
            if not(ObjParametroGlobal.ValidaParametro('CODIGO BASE DO CLIENTE NO SITE'))
            then begin
                  MensagemErro('Configure o par�metro "CODIGO BASE DO CLIENTE NO SITE"');
                  exit;
            end;
            Temp := DesincriptaSenha(ObjParametroGlobal.Get_Valor);

            try
                  StrToInt(temp);
            except
                  MensagemErro('Valor inv�lido para o par�metro "CODIGO BASE DO CLIENTE NO SITE"');
                  exit;
            end;

            //verifico se a data est� para vencer com 3 dias de antecedencia ou esta vencida
            if(IncDay(DataServidor,3)>=DataVencimento)
            then begin

                  //se estiver resgato da base do lara, a nova data de vencimento
                  objQueryTEMP.Close;
                  objQueryTEMP.sql.Clear;
                  objQueryTEMP.sql.add('SELECT * FROM TABBACKUP WHERE CLIENTE_SITE='+Temp);
                  try
                        objQueryTEMP.Open;
                  except
                        on e:Exception do
                        begin
                              MensagemErro(e.Message);
                              exit;
                        end;
                  end;

                  if(trim(objQueryTEMP.FieldByName('DATAVENCIMENTO').AsString)<>'')
                  then begin
                        try
                              strtodate(objQueryTEMP.FieldByName('DATAVENCIMENTO').AsString);
                        except
                              exit;
                        end;
                  end
                  else exit;


                  if(strtodate(objQueryTEMP.FieldByName('DATAVENCIMENTO').AsString)>DataVencimento)
                  then begin
                        Close;
                        sql.clear;
                        sql.add('UPDATE TABVENCTOTITULO SET VENCIMENTO='+#39+EncriptaSenha(objQueryTEMP.FieldByName('DATAVENCIMENTO').AsString)+#39+'WHERE CODIGO=0');
                        try
                              ExecSQL;
                        except
                              on e:exception do
                              begin
                                    MensagemErro(e.Message);
                                    FDataModulo.IBTransaction.RollbackRetaining;
                                    exit;
                              end;
                        end;
                  end;
            end;
            FDataModulo.IBTransaction.CommitRetaining;
            Result := false;
      end;
end;

end.
