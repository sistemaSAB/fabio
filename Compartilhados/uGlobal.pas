{******************************************************************************}
{   Utilizada em todo o sistema                                                }
{   Evitar acoplamento de unit�s de acesso a banco                             }
{******************************************************************************}

unit uGlobal;

interface

uses Forms,SysUtils,Classes,StdCtrls,Mask,ExtCtrls,ComCtrls,Dialogs,Controls;

procedure chamaFormulario(sender:TForm; tipo:TFormClass);
procedure limpaedit(FORMULARIO:TForm); overload;
procedure limpaedit(FORMULARIO:TPanel); overload;
procedure limpaedit(FORMULARIO:TGroupBox); overload;
procedure limpaedit(FORMULARIO:TFrame); overload;

procedure habilita_campos(FORMULARIO:Tform; habilita:Boolean);overload;
procedure habilita_campos(FORMULARIO:TFrame; habilita:Boolean);overload;
procedure habilita_campos(FORMULARIO:TPanel; habilita:Boolean);overload;
procedure habilita_campos(FORMULARIO:TGroupBox; habilita:Boolean);overload;

Function RetornaSoNUmeros(Palavra:String):String;
function testaVazio( valor: string ):boolean;


implementation

procedure chamaFormulario(sender:TForm; tipo:TFormClass);
var
  form:TForm;
begin
  try
    //sender.Hide;
    form := tipo.Create (nil);
    form.ShowModal;
    sender.Show;
  finally
    FreeAndNil (form);
  end;
end;
{ TODO -oCelio -cPOO : Melhorar LimpaEdit, usar P.O.O }
procedure limpaedit(FORMULARIO:TForm);//PROC PARA LIMPAR OS
                                           //EDITS
var
  limpa:integer;
begin
  for limpa:=0 to FORMULARIO.ComponentCount -1 do
  begin
    if FORMULARIO.Components [limpa].ClassName = 'TEdit' then
    begin
      Tedit(FORMULARIO.Components [limpa]).text:='';
      Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
    end
    else
      if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit' then
      begin
        TMaskEdit(FORMULARIO.Components [limpa]).text:='';
        TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
      end
      else
        if FORMULARIO.Components [limpa].ClassName = 'TComboBox' then
        begin
          TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
          //TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
        end
        else
          if FORMULARIO.Components [limpa].ClassName = 'TMemo' then
          begin
            TMemo(FORMULARIO.Components [limpa]).lines.clear;
            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
          end
          else
            if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit' then
            begin
              TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
              TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            end
            else
              if FORMULARIO.Components [limpa].ClassName = 'TRichEdit' then
              begin
                TRichEdit(FORMULARIO.Components [limpa]).text:='';
                TRichEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
              end
              else
                if FORMULARIO.Components [limpa].ClassName = 'TCheckBox' then
                  TCheckBox(FORMULARIO.Components [limpa]).Checked:=False;

  end;
end;

procedure limpaedit(FORMULARIO:TPanel);//PROC PARA LIMPAR OS
                                           //EDITS
var
  limpa:integer;
begin
  for limpa:=0 to FORMULARIO.ControlCount -1 do
  begin
    if FORMULARIO.Controls [limpa].ClassName = 'TEdit' then
    begin
      Tedit(FORMULARIO.Controls [limpa]).text:='';
      Tedit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
    end
    else
      if FORMULARIO.Controls [limpa].ClassName = 'TMaskEdit' then
      begin
        TMaskEdit(FORMULARIO.Controls [limpa]).text:='';
        TMaskEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
      end
      else
        if FORMULARIO.Controls [limpa].ClassName = 'TComboBox' then
        begin
          TCombobox(FORMULARIO.Controls [limpa]).itemindex:=-1;
          //TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
        end
        else
          if FORMULARIO.Controls [limpa].ClassName = 'TMemo' then
          begin
            TMemo(FORMULARIO.Controls [limpa]).lines.clear;
            TMemo(FORMULARIO.Controls [limpa]).Ctl3D:=False;
          end
          else
            if FORMULARIO.Controls [limpa].ClassName = 'TLabeledEdit' then
            begin
              TLabeledEdit(FORMULARIO.Controls [limpa]).text:='';
              TLabeledEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
            end
            else
              if FORMULARIO.Controls [limpa].ClassName = 'TRichEdit' then
              begin
                TRichEdit(FORMULARIO.Controls [limpa]).text:='';
                TRichEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
              end
              else
                if FORMULARIO.Controls [limpa].ClassName = 'TCheckBox' then
                  TCheckBox(FORMULARIO.Controls [limpa]).Checked:=False;
  end;
end;

procedure limpaedit(FORMULARIO:TGroupBox);//PROC PARA LIMPAR OS
                                           //EDITS
var
  limpa:integer;
begin
  for limpa:=0 to FORMULARIO.ControlCount -1 do
  begin
    if FORMULARIO.Controls [limpa].ClassName = 'TEdit' then
    begin
      Tedit(FORMULARIO.Controls [limpa]).text:='';
      Tedit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
    end
    else
      if FORMULARIO.Controls [limpa].ClassName = 'TMaskEdit' then
      begin
        TMaskEdit(FORMULARIO.Controls [limpa]).text:='';
        TMaskEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
      end
      else
        if FORMULARIO.Controls [limpa].ClassName = 'TComboBox' then
        begin
          TCombobox(FORMULARIO.Controls [limpa]).itemindex:=-1;
          //TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
        end
        else
          if FORMULARIO.Controls [limpa].ClassName = 'TMemo' then
          begin
            TMemo(FORMULARIO.Controls [limpa]).lines.clear;
            TMemo(FORMULARIO.Controls [limpa]).Ctl3D:=False;
          end
          else
            if FORMULARIO.Controls [limpa].ClassName = 'TLabeledEdit' then
            begin
              TLabeledEdit(FORMULARIO.Controls [limpa]).text:='';
              TLabeledEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
            end
            else
              if FORMULARIO.Controls [limpa].ClassName = 'TRichEdit' then
              begin
                TRichEdit(FORMULARIO.Controls [limpa]).text:='';
                TRichEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
              end
              else
                if FORMULARIO.Controls [limpa].ClassName = 'TCheckBox' then
                  TCheckBox(FORMULARIO.Controls [limpa]).Checked:=False;
  end;
end;

procedure limpaedit(FORMULARIO:TFrame);//PROC PARA LIMPAR OS
                                           //EDITS
var
  limpa:integer;
begin
  for limpa:=0 to FORMULARIO.ComponentCount -1 do
  begin
    if FORMULARIO.Components [limpa].ClassName = 'TEdit' then
    begin
      Tedit(FORMULARIO.Components [limpa]).text:='';
      Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
    end
    else
      if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit' then
      begin
        TMaskEdit(FORMULARIO.Components [limpa]).text:='';
        TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
      end
      else
        if FORMULARIO.Components [limpa].ClassName = 'TComboBox' then
        begin
          TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
          //TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
        end
        else
          if FORMULARIO.Components [limpa].ClassName = 'TMemo' then
          begin
            TMemo(FORMULARIO.Components [limpa]).lines.clear;
            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
          end
          else
            if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit' then
            begin
              TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
              TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            end
            else
              if FORMULARIO.Components [limpa].ClassName = 'TRichEdit' then
              begin
                TRichEdit(FORMULARIO.Components [limpa]).text:='';
                TRichEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
              end
              else
                if FORMULARIO.Components [limpa].ClassName = 'TCheckBox' then
                  TCheckBox(FORMULARIO.Components [limpa]).Checked:=False;

  end;
end;


procedure habilita_campos(FORMULARIO:Tform; habilita:Boolean);//PROC PARA HABILITAR OS EDITS
var
  int_habilita:integer;
begin
  for int_habilita:=0 to FORMULARIO.ComponentCount -1   do
  Begin
    if FORMULARIO.Components [int_habilita].ClassName = 'TEdit' then
      Tedit(FORMULARIO.Components [int_habilita]).enabled := habilita
    else
      if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit' then
        TMaskEdit(FORMULARIO.Components [int_habilita]).enabled := habilita
      else
        if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox' then
          TComboBox(FORMULARIO.Components [int_habilita]).enabled := habilita
        else
          if FORMULARIO.Components [int_habilita].ClassName = 'TMemo' then
            TMemo(FORMULARIO.Components [int_habilita]).enabled := habilita
          Else
            if FORMULARIO.Components [int_habilita].ClassName = 'TRadioButton' then
              TRadioButton(FORMULARIO.Components [int_habilita]).enabled := habilita
            else
              if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit' then
                TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled := habilita
              Else
                if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit' then
                  TRichEdit(FORMULARIO.Components [int_habilita]).enabled := habilita
                else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TCheckBox' then
                    TCheckBox (FORMULARIO.Components [int_habilita]).Enabled := habilita
                  else
                    if FORMULARIO.Components [int_habilita].ClassName = 'TRadioGroup' then
                      TRadioGroup (FORMULARIO.Components [int_habilita]).Enabled := habilita;
  End;

end;

procedure habilita_campos(FORMULARIO:TFrame; habilita:Boolean);//PROC PARA HABILITAR OS EDITS
var
  int_habilita:integer;
begin
  for int_habilita:=0 to FORMULARIO.ComponentCount -1   do
  Begin
    if FORMULARIO.Components [int_habilita].ClassName = 'TEdit' then
      Tedit(FORMULARIO.Components [int_habilita]).enabled := habilita
    else
      if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit' then
        TMaskEdit(FORMULARIO.Components [int_habilita]).enabled := habilita
      else
        if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox' then
          TComboBox(FORMULARIO.Components [int_habilita]).enabled := habilita
        else
          if FORMULARIO.Components [int_habilita].ClassName = 'TMemo' then
            TMemo(FORMULARIO.Components [int_habilita]).enabled := habilita
          Else
            if FORMULARIO.Components [int_habilita].ClassName = 'TRadioButton' then
              TRadioButton(FORMULARIO.Components [int_habilita]).enabled := habilita
            else
              if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit' then
                TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled := habilita
              Else
                if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit' then
                  TRichEdit(FORMULARIO.Components [int_habilita]).enabled := habilita
                else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TCheckBox' then
                    TCheckBox (FORMULARIO.Components [int_habilita]).Enabled := habilita
                  else
                    if FORMULARIO.Components [int_habilita].ClassName = 'TRadioGroup' then
                      TRadioGroup (FORMULARIO.Components [int_habilita]).Enabled := habilita;
  End;

end;

procedure habilita_campos(FORMULARIO:TPanel; habilita:Boolean);//PROC PARA HABILITAR OS EDITS
var
  int_habilita:integer;
begin
  for int_habilita:=0 to FORMULARIO.ControlCount -1   do
  Begin
    if FORMULARIO.Controls [int_habilita].ClassName = 'TEdit' then
      Tedit(FORMULARIO.Controls [int_habilita]).enabled := habilita
    else
      if FORMULARIO.Controls [int_habilita].ClassName = 'TMaskEdit' then
        TMaskEdit(FORMULARIO.Controls [int_habilita]).enabled := habilita
      else
        if FORMULARIO.Controls [int_habilita].ClassName = 'TComboBox' then
          TComboBox(FORMULARIO.Controls [int_habilita]).enabled := habilita
        else
          if FORMULARIO.Controls [int_habilita].ClassName = 'TMemo' then
            TMemo(FORMULARIO.Controls [int_habilita]).enabled := habilita
          Else
            if FORMULARIO.Controls [int_habilita].ClassName = 'TRadioButton' then
              TRadioButton(FORMULARIO.Controls [int_habilita]).enabled := habilita
            else
              if FORMULARIO.Controls [int_habilita].ClassName = 'TLabeledEdit' then
                TLabeledEdit(FORMULARIO.Controls [int_habilita]).enabled := habilita
              Else
                if FORMULARIO.Controls [int_habilita].ClassName = 'TRichEdit' then
                  TRichEdit(FORMULARIO.Controls [int_habilita]).enabled := habilita
                else
                  if FORMULARIO.Controls [int_habilita].ClassName = 'TCheckBox' then
                    TCheckBox (FORMULARIO.Controls [int_habilita]).Enabled := habilita
                  else
                    if FORMULARIO.Controls [int_habilita].ClassName = 'TRadioGroup' then
                      TRadioGroup (FORMULARIO.Controls [int_habilita]).Enabled := habilita;
  End;

end;
procedure habilita_campos(FORMULARIO:TGroupBox; habilita:Boolean);//PROC PARA HABILITAR OS EDITS
var
  int_habilita:integer;
begin
  for int_habilita:=0 to FORMULARIO.ControlCount -1   do
  Begin
    if FORMULARIO.Controls [int_habilita].ClassName = 'TEdit' then
      Tedit(FORMULARIO.Controls [int_habilita]).enabled := habilita
    else
      if FORMULARIO.Controls [int_habilita].ClassName = 'TMaskEdit' then
        TMaskEdit(FORMULARIO.Controls [int_habilita]).enabled := habilita
      else
        if FORMULARIO.Controls [int_habilita].ClassName = 'TComboBox' then
          TComboBox(FORMULARIO.Controls [int_habilita]).enabled := habilita
        else
          if FORMULARIO.Controls [int_habilita].ClassName = 'TMemo' then
            TMemo(FORMULARIO.Controls [int_habilita]).enabled := habilita
          Else
            if FORMULARIO.Controls [int_habilita].ClassName = 'TRadioButton' then
              TRadioButton(FORMULARIO.Controls [int_habilita]).enabled := habilita
            else
              if FORMULARIO.Controls [int_habilita].ClassName = 'TLabeledEdit' then
                TLabeledEdit(FORMULARIO.Controls [int_habilita]).enabled := habilita
              Else
                if FORMULARIO.Controls [int_habilita].ClassName = 'TRichEdit' then
                  TRichEdit(FORMULARIO.Controls [int_habilita]).enabled := habilita
                else
                  if FORMULARIO.Controls [int_habilita].ClassName = 'TCheckBox' then
                    TCheckBox (FORMULARIO.Controls [int_habilita]).Enabled := habilita
                  else
                    if FORMULARIO.Controls [int_habilita].ClassName = 'TRadioGroup' then
                      TRadioGroup (FORMULARIO.Controls [int_habilita]).Enabled := habilita;
  End;

end;

Function RetornaSoNUmeros(Palavra:String):String;
var
cont:integer;
temp:string;
Begin
  temp:='';
  for cont:=1 to length(palavra) do
  begin
    if (palavra[cont] in ['0'..'9']) then
      temp := Temp + palavra[cont];
  end;
  result:=Temp;
End;

function testaVazio( valor: string ):boolean;
begin
  Result := False;
  if( Trim( valor ) = '' ) then
    Result := True;
end;



end.
