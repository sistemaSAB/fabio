unit UUtils;

interface
uses graphics,grids,dialogs,sysutils,classes,forms,stdctrls,mask,extctrls,comctrls,buttons;

Type
  str01=string[1];
  str09=string[9];
  str20=string[20];
  Str100=string[100];
  Str150=string[150];
  STR255=string[255];
  STR200=string[200];

Function RetornaSoNUmeros(Palavra:String):String;
Function CompletaPalavra_a_Esquerda(palavra:string;quantidade:Integer;ValorASerUSado:Str01):String;
Function CompletaPalavra(palavra:string;quantidade:Integer;ValorASerUSado:Str01):String;
Function TrocaLetra(Original:string):string;
Function DesincriptaSenha(original:string):string;
Function EncriptaSenha(original:string):string;
procedure desabilita_campos(FORMULARIO:Tform);//PROC PARA DESABILITAR OS EDITS
procedure habilita_campos(FORMULARIO:Tform);//PROC PARA DESABILITAR OS EDITS
procedure desab_botoes(formulario:Tform);//PROC PARA HABILITAR OS BOTOES
procedure habilita_botoes(formulario:Tform);//PROC PARA HABILITAR OS BOTOES
Function  ExplodeStr(Pcodigos:String;var StrRetorno:TStringList;PSeparador:Str01;PtipoValor:String):boolean;

function comebarra(texto:string):string;
Function SeparaIp(PathBase:String):String;
Function SeparaPath(PathBase:String):String;

PRocedure Mensagemerro (pfrase:string);
PRocedure MensagemAviso(pfrase:string);

procedure limpaedit(FORMULARIO:Tform);overload;
procedure limpaedit(FORMULARIO:Tpanel);overload;
procedure limpaedit(FORMULARIO:TGroupBox);overload;
procedure limpaedit(FORMULARIO:Tframe);overload;

function converte_boolean(pboolean:boolean):string;
Function RetornaPathSistema:String;
procedure AjustaLArguraColunaGrid(PGrid: TStringGrid);overload;
procedure AjustaLArguraColunaGrid(PGrid: TStringGrid;PFonte:TFontName;Ptamanhofonte:integer);overload;

implementation

uses UdataMOdulo;


Function RetornaPathSistema:String;
var
temp:string;
begin
     temp:=ExtractFilePath(Application.ExeName);

     if (temp[length(temp)]<>'\')
     then temp:=temp+'\';

     result:=temp;

End;

function converte_boolean(pboolean:boolean):string;
begin
     if (pboolean)
     Then result:='S'
     Else result:='N';
End;



Function DesincriptaSenha(original:string):string;
var
final:string;
cont:integer;
palavra:string;
begin
     final:='';
     palavra:='';
     palavra:=original;
     for cont:=1 to length(palavra) do
     begin
	        final:=final+chr(256-ord(copy(palavra,cont,1)[1]));
     end;

     palavra:='';
     palavra:=trocaletra(final);

     result:=palavra;

end;


Function EncriptaSenha(original:string):string;
var
final:string;
cont:integer;
palavra:string;
begin
     palavra:='';
     palavra:=trocaletra(original);

     final:='';
     for cont:=1 to length(palavra) do
     begin
	        final:=final+chr(256-ord(copy(palavra,cont,1)[1]));
     end;
     result:=final;

end;

Function TrocaLetra(Original:string):string;
var
cont:integer;
palavra:string;
Begin
 palavra:='';
 palavra:=original;

 for cont:=1 to length(palavra) do
 Begin
        case palavra[cont] of

        '1': palavra[cont]:='5';
        '5': palavra[cont]:='1';

        '2': palavra[cont]:='3';
        '3': palavra[cont]:='2';

        '4': palavra[cont]:='6';
        '6': palavra[cont]:='4';

        '7': palavra[cont]:='9';
        '9': palavra[cont]:='7';
        'a': palavra[cont]:='Z';
        'Z': palavra[cont]:='a';
        'e': palavra[cont]:='X';
        'X': palavra[cont]:='e';
        'i': palavra[cont]:='R';
        'R': palavra[cont]:='i';
        'o': palavra[cont]:='K';
        'K': palavra[cont]:='o';
        'u': palavra[cont]:='H';
        'H': palavra[cont]:='u';
        End;

 End;
 result:=palavra;

End;

procedure limpaedit(FORMULARIO:Tform);//PROC PARA LIMPAR OS
                                      //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ComponentCount -1
   do begin
        if FORMULARIO.Components [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.Components [limpa]).text:='';
                Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.Components [limpa]).text:='';
                    TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.Components [limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
                        //TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
                End
                else
                    if FORMULARIO.Components [limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.Components [limpa]).lines.clear;
                            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
                    End
                    else Begin
                          if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit'
                          then Begin
                                    TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
                                    TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                          End
                          Else Begin
                                    if FORMULARIO.Components [limpa].ClassName = 'TRichEdit'
                                    then Begin
                                            TRichEdit(FORMULARIO.Components [limpa]).text:='';
                                            TRichEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                                    End
                          End;
                    End;

   end;

end;


procedure desabilita_campos(FORMULARIO:Tform);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1
   do Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).enabled:=false
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).enabled:=false
            else
               if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
               then TComboBox(FORMULARIO.Components [int_habilita]).enabled:=false
               else
                  if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                  then TMemo(FORMULARIO.Components [int_habilita]).enabled:=false
                  else
                      if FORMULARIO.Components [int_habilita].ClassName = 'TRadioButton'
                      then TRadioButton(FORMULARIO.Components [int_habilita]).enabled:=false
                      else
                          if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled:=false
                          Else
                              if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Components [int_habilita]).enabled:=false;


      End;

end;


procedure habilita_campos(FORMULARIO:Tform);//PROC PARA DESABILITAR OS EDITS
var
  int_habilita:integer;
begin
   for int_habilita:=0 to FORMULARIO.ComponentCount -1   do
   Begin
        if FORMULARIO.Components [int_habilita].ClassName = 'TEdit'
        then Tedit(FORMULARIO.Components [int_habilita]).enabled:=true
        else
            if FORMULARIO.Components [int_habilita].ClassName = 'TMaskEdit'
            then TMaskEdit(FORMULARIO.Components [int_habilita]).enabled:=true
            else
                if FORMULARIO.Components [int_habilita].ClassName = 'TComboBox'
                then TComboBox(FORMULARIO.Components [int_habilita]).enabled:=True
                else
                    if FORMULARIO.Components [int_habilita].ClassName = 'TMemo'
                    then TMemo(FORMULARIO.Components [int_habilita]).enabled:=true
                    Else
                        if FORMULARIO.Components [int_habilita].ClassName = 'TRadioButton'
                        then TRadioButton(FORMULARIO.Components [int_habilita]).enabled:=True
                        else
                          if FORMULARIO.Components [int_habilita].ClassName = 'TLabeledEdit'
                          then TLabeledEdit(FORMULARIO.Components [int_habilita]).enabled:=True
                          Else
                              if FORMULARIO.Components [int_habilita].ClassName = 'TRichEdit'
                              then TRichEdit(FORMULARIO.Components [int_habilita]).enabled:=True;

   End;

end;

procedure habilita_botoes(formulario:Tform);//PROC PARA HABILITAR OS BOTOES
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton') or
        (formulario.Components [int_habilita].ClassName = 'TSpeedButton')
        then
          TBitBtn(formulario.Components [int_habilita]).enabled:=true;
    end;
end;


procedure desab_botoes(formulario:Tform);//PROC PARA HABILITAR OS BOTOES
var
int_habilita:integer;
begin
   for int_habilita:=0 to formulario.ComponentCount -1
   do begin
        if (formulario.Components [int_habilita].ClassName = 'TBitBtn') or
        (formulario.Components [int_habilita].ClassName = 'TButton') or
        (formulario.Components [int_habilita].ClassName = 'TSpeedButton')
        then
          TBitBtn(formulario.Components [int_habilita]).enabled:=False;
    end;
end;

Function  ExplodeStr(Pcodigos:String;var StrRetorno:TStringList;PSeparador:Str01;PtipoValor:String):boolean;
var
cont:integer;
temp:String;
Begin
     ptipovalor:=uppercase(ptipovalor);

     result:=False;
     StrRetorno.Clear;
     temp:='';
     for cont:=1 to length(pcodigos) do
     Begin
          if ((Pcodigos[cont]=Pseparador) or (cont=length(Pcodigos)))
          then begin
                    if (cont=length(Pcodigos)) and
                    (Pcodigos[cont]<>Pseparador)
                    Then temp:=temp+Pcodigos[cont];

                    if (temp<>'')
                    Then Begin
                              Try
                                 if (PtipoValor='INTEGER')
                                 Then strtoint(temp)
                                 Else
                                    if (PtipoValor='DATE')
                                    Then strtodate(temp)
                                    Else
                                       if (PtipoValor='TIME')
                                       Then StrToTime(temp)
                                       Else
                                          if (Ptipovalor='FLOAT')
                                          Then StrToFloat(temp);
                                 Strretorno.add(temp);
                              Except
                                    on e:exception do
                                    begin
                                        Messagedlg('Valor inv�lido '+temp+#13+'Erro retornado: '+E.Message,mterror,[mbok],0);
                                        exit;
                                    End;
                              end;
                              temp:='';
                    end;
          end
          Else temp:=temp+Pcodigos[cont];
     end;

     if (temp<>'')
     Then Begin
               if (temp<>'')
               Then Begin
                         Try
                            if (PtipoValor='INTEGER')
                            Then strtoint(temp)
                            Else
                               if (PtipoValor='DATE')
                               Then strtodate(temp)
                               Else
                                  if (PtipoValor='TIME')
                                  Then StrToTime(temp)
                                  Else
                                     if (Ptipovalor='FLOAT')
                                     Then StrToFloat(temp);

                            Strretorno.add(temp);
                         Except
                               on e:exception do
                               begin
                                   Messagedlg('Valor inv�lido '+temp+#13+'Erro retornado: '+E.Message,mterror,[mbok],0);
                                   exit;
                               End;
                         end;
                         temp:='';
               end;
     end;


     result:=true;
end;


function comebarra(texto:string):string;
var //funcao que retira as barras de data de um tipo data passando para string
i :integer;
textosai:string;
begin

   textosai:='';
   for i :=1 to length(texto)
   do begin

      if ((texto[i] ='/') or (texto[i]=' ') or (texto[i]=':'))
      then
      else
         textosai:=textosai+texto[i];
   end;
   result:=textosai;
end;
Function SeparaPath(PathBase:String):String;
var
cont:integer;
Begin
     if (pathbase[2]=':')
     Then Begin
              (*se tiver um : no segundo caracter � porque � local no windows c:...*)
              result:=pathbase;
              exit;
     End;


     for cont:=1 to length(PathBase) do
     Begin
          If (PathBase[cont]=':')
          Then Break;
     End;
     result:=copy(pathbase,cont+1,length(pathbase));


End;

Function SeparaIp(PathBase:String):String;

var
cont:integer;
Begin

     (*se tiver um : no segundo caracter � porque � local no windows c:...*)
     if (pathbase[2]=':')
     Then Begin
                result:='';
                exit;
     End;

     for cont:=1 to length(PathBase) do
     Begin
          If (PathBase[cont]=':')
          Then Break;
     End;
     result:=copy(pathbase,1,cont-1);
End;

PRocedure Mensagemerro(pfrase:string);
Begin
     MessageDlg(pfrase,mterror,[mbok],0);
End;

PRocedure MensagemAviso(pfrase:string);
Begin
     MessageDlg(pfrase,mtinformation,[mbok],0);
End;


procedure limpaedit(FORMULARIO:Tframe);//PROC PARA LIMPAR OS
                                    //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ComponentCount -1
   do begin
        if FORMULARIO.Components [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.Components [limpa]).text:='';
                Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.Components [limpa]).text:='';
                    TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.Components [limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
                        TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
                End
                else
                    if FORMULARIO.Components [limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.Components [limpa]).lines.clear;
                            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
                    End
                    else Begin
                          if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit'
                          then Begin
                                    TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
                                    TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                          End
                          Else Begin
                                    if FORMULARIO.components [limpa].ClassName = 'TRichEdit'
                                    then Begin
                                            TRichEdit(FORMULARIO.components [limpa]).text:='';
                                            TRichEdit(FORMULARIO.components [limpa]).Ctl3D:=False;
                                    End
                          End;
                    End;
   end;

end;

procedure limpaedit(Formulario:TPanel);//PROC PARA LIMPAR OS
                                     //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ControlCount -1
   do begin
        if FORMULARIO.Controls [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.controls[limpa]).text:='';
                Tedit(FORMULARIO.controls[limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.controls[limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.controls[limpa]).text:='';
                    TMaskEdit(FORMULARIO.controls[limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.controls[limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.controls[limpa]).itemindex:=-1;
                        TCombobox(FORMULARIO.controls[limpa]).Ctl3D:=False;
                End
                else
                    if FORMULARIO.controls[limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.controls[limpa]).lines.clear;
                            TMemo(FORMULARIO.controls[limpa]).Ctl3D:=False;
                    End
                    else Begin
                          if FORMULARIO.Controls [limpa].ClassName = 'TLabeledEdit'
                          then Begin
                                    TLabeledEdit(FORMULARIO.Controls [limpa]).text:='';
                                    TLabeledEdit(FORMULARIO.Controls [limpa]).Ctl3D:=False;
                          End
                          Else Begin
                                    if FORMULARIO.controls [limpa].ClassName = 'TRichEdit'
                                    then Begin
                                            TRichEdit(FORMULARIO.controls [limpa]).text:='';
                                            TRichEdit(FORMULARIO.controls [limpa]).Ctl3D:=False;
                                    End
                          End;
                    End;
   end;

end;

procedure limpaedit(Formulario:TGroupBox);//PROC PARA LIMPAR OS
                                     //EDITS
var
  limpa:integer;
begin
   for limpa:=0 to FORMULARIO.ComponentCount -1
   do begin
        if FORMULARIO.Components [limpa].ClassName = 'TEdit'
        then Begin
                Tedit(FORMULARIO.Components [limpa]).text:='';
                Tedit(FORMULARIO.Components [limpa]).Ctl3D:=False;
        End
        else
            if FORMULARIO.Components [limpa].ClassName = 'TMaskEdit'
            then BEgin
                    TMaskEdit(FORMULARIO.Components [limpa]).text:='';
                    TMaskEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
            End
            Else
                if FORMULARIO.Components [limpa].ClassName = 'TComboBox'
                then Begin
                        TCombobox(FORMULARIO.Components [limpa]).itemindex:=-1;
                        TCombobox(FORMULARIO.Components [limpa]).Ctl3D:=False;
                End
                else
                    if FORMULARIO.Components [limpa].ClassName = 'TMemo'
                    then Begin
                            TMemo(FORMULARIO.Components [limpa]).lines.clear;
                            TMemo(FORMULARIO.Components [limpa]).Ctl3D:=False;
                    End
                    else Begin
                          if FORMULARIO.Components [limpa].ClassName = 'TLabeledEdit'
                          then Begin
                                    TLabeledEdit(FORMULARIO.Components [limpa]).text:='';
                                    TLabeledEdit(FORMULARIO.Components [limpa]).Ctl3D:=False;
                          End
                          Else Begin
                                    if FORMULARIO.components [limpa].ClassName = 'TRichEdit'
                                    then Begin
                                            TRichEdit(FORMULARIO.components [limpa]).text:='';
                                            TRichEdit(FORMULARIO.components [limpa]).Ctl3D:=False;
                                    End
                          End;                          
                    End;
   end;

end;


Function CompletaPalavra(palavra:string;quantidade:Integer;ValorASerUSado:Str01):String;
var
apoio:String;
Begin
     result:='';
     apoio:='';
     If (length(palavra)>=quantidade)
     Then Begin
                If (QUANTIDADE=0)
                Then EXIT;
                apoio:=copy(palavra,1,quantidade);
                result:=apoio;
                exit;
     End;

     apoio:=Palavra;

     if (quantidade=0)//em casos de configuracao de rel de NF isso � necessario
     Then Apoio:='';

     While (length(apoio)<quantidade) do
     Begin
         apoio:=apoio+Valoraserusado;
     End;

     result:=apoio;
End;

Function CompletaPalavra_a_Esquerda(palavra:string;quantidade:Integer;ValorASerUSado:Str01):String;
var
apoio:String;
Begin
     result:='';
     apoio:='';
     If (length(palavra)>=quantidade)
     Then Begin
                apoio:=copy(palavra,1,quantidade);
                result:=apoio;
                exit;
     End;

     apoio:='';

     if (quantidade=0)//em casos de configuracao de rel de NF isso � necessario
     Then Apoio:='';

     While ((length(apoio)+length(palavra))<quantidade)
     do Begin
             apoio:=apoio+Valoraserusado;
     End;

     result:=apoio+palavra;
End;

procedure AjustaLArguraColunaGrid(PGrid: TStringGrid);
Begin
     AjustaLArguraColunaGrid(PGrid,'Courier New',8);
end;

procedure AjustaLArguraColunaGrid(PGrid: TStringGrid;PFonte:TFontName;Ptamanhofonte:integer);
var
Contcol,contlinha,maiortamanho:Integer;
begin
     //Ajusta as Colunas do StringGrid de acordo com a
     //quantidade de letras, foi usado Courier New tamanho 08 como padrao
     Pgrid.Ctl3D:=False;
     Pgrid.Font.Name:=Pfonte;
     Pgrid.Font.Size:=Ptamanhofonte;

     for contcol:=pgrid.ColCount-1 downto 0 do
     Begin
          //para cada coluna verifico o tamanho da palavra que esta dentro dela
          //se for maior a que eu tinha guardado eu aumento o tamanho da mesma
          maiortamanho:=0;
          for contlinha:=PGrid.RowCount-1 downto 0 do
          Begin
               If (length(Pgrid.Cells[contcol,contlinha])>maiortamanho)
               Then Begin
                         maiortamanho:=length(Pgrid.Cells[contcol,contlinha]);
                         Pgrid.ColWidths[contcol]:=maiortamanho*8;//a quantidade de caracteres * 8 pontos que � a fonte
                    End;
          End;

     End;
end;


Function RetornaSoNUmeros(Palavra:String):String;
var
cont:integer;
temp:string;
Begin
     temp:='';
     for cont:=1 to length(palavra) do
     Begin
          if (palavra[cont] in ['0'..'9'])
          Then temp:=Temp+palavra[cont];
     End;
     result:=Temp;
End;


end.
