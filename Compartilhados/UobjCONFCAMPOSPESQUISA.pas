unit UobjCONFCAMPOSPESQUISA;
Interface
Uses graphics,dbgrids,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc;

Type
   TObjCONFCAMPOSpesquisa=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_usuario(parametro: string);
                Function Get_usuario: string;

                Procedure Submit_grupousuario(parametro: string);
                Function Get_grupousuario: string;


                Procedure Submit_IDENTIFICADOR(parametro: string);
                Function Get_IDENTIFICADOR: string;
                Procedure Submit_campo(parametro: string);
                Function Get_campo: string;



                procedure Personalizagrid(PGrid:TdbGrid;PIdentificador:string);
                procedure ConfiguraColunas(PGrid: TdbGrid; PIdentificador: string);
                Function TransfereCamposDoParametroParaBanco:Boolean;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               ObjqueryPesquisa:Tibquery;
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               usuario:string;
               grupousuario:string;
               IDENTIFICADOR:string;
               campo:string;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UConfordemcamposPesquisa, ExtCtrls;





Function  TObjCONFCAMPOSpesquisa.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.usuario:=fieldbyname('usuario').asstring;
        Self.grupousuario:=fieldbyname('grupousuario').asstring;
        Self.IDENTIFICADOR:=fieldbyname('IDENTIFICADOR').asstring;
        Self.campo:=fieldbyname('campo').asstring;
//CODIFICA TABELAPARAOBJETO




        result:=True;
     End;
end;


Procedure TObjCONFCAMPOSpesquisa.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('usuario').asstring:=Self.usuario;
        ParamByName('grupousuario').asstring:=Self.grupousuario;
        ParamByName('IDENTIFICADOR').asstring:=Self.IDENTIFICADOR;
        ParamByName('campo').asstring:=Self.campo;
//CODIFICA OBJETOPARATABELA




  End;
End;

//***********************************************************************

function TObjCONFCAMPOSpesquisa.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCONFCAMPOSpesquisa.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        usuario:='';
        grupousuario:='';
        IDENTIFICADOR:='';
        campo:='';
//CODIFICA ZERARTABELA




     End;
end;

Function TObjCONFCAMPOSpesquisa.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjCONFCAMPOSpesquisa.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCONFCAMPOSpesquisa.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCONFCAMPOSpesquisa.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCONFCAMPOSpesquisa.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCONFCAMPOSpesquisa.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CONFCAMPOSpesquisa vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,usuario,grupousuario,IDENTIFICADOR,campo');
           SQL.ADD(' from  TabConfCampospesquisa');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCONFCAMPOSpesquisa.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCONFCAMPOSpesquisa.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCONFCAMPOSpesquisa.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabConfCampospesquisa(CODIGO,usuario,grupousuario,IDENTIFICADOR');
                InsertSQL.add(' ,campo)');
                InsertSQL.add('values (:CODIGO,:usuario,:grupousuario,:IDENTIFICADOR,:campo)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabConfCampospesquisa set CODIGO=:CODIGO,usuario=:usuario,grupousuario=:grupousuario');
                ModifySQL.add(',IDENTIFICADOR=:IDENTIFICADOR,campo=:campo');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabConfCampospesquisa where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCONFCAMPOSpesquisa.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCONFCAMPOSpesquisa.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCONFCAMPOSpesquisa');
     Result:=Self.ParametroPesquisa;
end;

function TObjCONFCAMPOSpesquisa.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CONFCAMPOSpesquisa ';
end;


function TObjCONFCAMPOSpesquisa.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCONFCAMPOSpesquisa,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCONFCAMPOSpesquisa,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCONFCAMPOSpesquisa.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);

    //alterado celio 301109
    Freeandnil(Self.ObjqueryPesquisa);

    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCONFCAMPOSpesquisa.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCONFCAMPOSpesquisa.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjConfCampospesquisa.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjConfCampospesquisa.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjConfCampospesquisa.Submit_usuario(parametro: string);
begin
        Self.usuario:=Parametro;
end;
function TObjConfCampospesquisa.Get_usuario: string;
begin
        Result:=Self.usuario;
end;
procedure TObjConfCampospesquisa.Submit_IDENTIFICADOR(parametro: string);
begin
        Self.IDENTIFICADOR:=Parametro;
end;
function TObjConfCampospesquisa.Get_IDENTIFICADOR: string;
begin
        Result:=Self.IDENTIFICADOR;
end;
procedure TObjConfCampospesquisa.Submit_campo(parametro: string);
begin
        Self.campo:=Parametro;
end;
function TObjConfCampospesquisa.Get_campo: string;
begin
        Result:=Self.campo;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjCONFCAMPOSpesquisa.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCONFCAMPOSpesquisa';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

procedure TObjCONFCAMPOSpesquisa.ConfiguraColunas(PGrid:TdbGrid;PIdentificador:string);
var
cont:integer;
temp:string;
tipoConfiguracao:integer;
begin   
     pidentificador:=UpperCase(PIdentificador);

     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('CONFIGURAR COLUNAS NO FORMULARIO DE PESQUISA')=False)
     Then Begin
               if (ObjPermissoesUsoGlobal.PedePermissao(ObjPermissoesUsoGlobal.Get_CODIGO,temp,'SENHA PARA CONFIGURAR COLUNAS')=false)
               Then exit;

     End;


     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Para o usu�rio atual');
          RgOpcoes.Items.add('Para o N�vel do usu�rio atual');
          showmodal;

          if (Tag=0)
          Then exit;

          TipoConfiguracao:=RgOpcoes.ItemIndex;
     End;


     
     FConfordemcamposPesquisa.LbCamposDisponiveis.Items.clear;
     FConfordemcamposPesquisa.LbCamposSelecionados.Items.Clear;


     //seleciono os campos disponiveis e preencho a StringList Campos Disponiveis
     PGrid.DataSource.DataSet.Fields.GetFieldNames(FConfordemcamposPesquisa.LbCamposDisponiveis.Items);
     //selecionando os campos que ja foram configurados anteriormente
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo,campo from TabConfCamposPesquisa where identificador='+#39+PIdentificador+#39);

          if (tipoConfiguracao=0)
          Then sql.add('and usuario='+#39+uppercase(ObjUsuarioGlobal.Get_nome)+#39)
          Else sql.add('and grupousuario='+ObjUsuarioGlobal.Get_Nivel);

          sql.add('order by codigo');

          open;

          first;
          FConfordemcamposPesquisa.LbCamposDisponiveis.Items.text:=uppercase(FConfordemcamposPesquisa.LbCamposDisponiveis.Items.text);

          while not(eof) do
          Begin
                if (FConfordemcamposPesquisa.LbCamposDisponiveis.Items.IndexOf(uppercase(fieldbyname('campo').asstring))>=0)
                Then Begin
                          FConfordemcamposPesquisa.LbCamposSelecionados.Items.add(uppercase(fieldbyname('campo').asstring));
                End
                Else Begin
                         Self.Exclui(fieldbyname('codigo').asstring,true);
                End;
                
                next;
          End;

          if (FConfordemcamposPesquisa.LbCamposSelecionados.Items.Count=0)
          Then FConfordemcamposPesquisa.LbCamposSelecionados.Items.add('CODIGO');


          FConfordemcamposPesquisa.SHOWMODAL;

          if (FConfordemcamposPesquisa.tag=0)
          Then exit;
          
          //quando volta tenho os campos na ListBox

          close;
          sql.clear;
          sql.add('Delete from TabConfCamposPesquisa where identificador='+#39+PIdentificador+#39);

          if (tipoConfiguracao=0)
          Then sql.add('and usuario='+#39+uppercase(ObjUsuarioGlobal.Get_nome)+#39)
          Else sql.add('and grupousuario='+ObjUsuarioGlobal.Get_Nivel);

          try
             ExecSQL;
             FDataModulo.IBTransaction.CommitRetaining;
          Except
                on e:exception do
                Begin
                     MensagemErro(E.message);
                     exit;
                End;
          End;

          for cont:=0 to FConfordemcamposPesquisa.LbCamposSelecionados.Items.Count-1 do
          Begin
               Self.ZerarTabela;
               Self.Submit_CODIGO('0');

               if (tipoConfiguracao=0)
               Then Self.Submit_usuario(uppercase(ObjUsuarioGlobal.Get_nome))
               Else Self.Submit_grupousuario(ObjUsuarioGlobal.Get_Nivel);

               Self.Submit_IDENTIFICADOR(uppercase(PIdentificador));
               Self.Submit_campo(uppercase(FConfordemcamposPesquisa.LbCamposSelecionados.Items[cont]));
               Self.Status:=dsInsert;

               if (Self.Salvar(False)=False)
               Then Begin
                        FDataModulo.IBTransaction.RollbackRetaining;
                        MensagemErro('Erro na tentativa de adicionar o campo '+Self.campo);
                        exit;
               End;
          End;
          FDataModulo.IBTransaction.CommitRetaining;
     End;



End;


procedure TObjCONFCAMPOSpesquisa.Personalizagrid(PGrid:TdbGrid;PIdentificador:string);
begin

     if (Pidentificador='')
     Then exit;

     PIdentificador:=uppercase(pidentificador);


     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select max(codigo) from TabConfCamposPesquisa');
          Try
              open;
          Except
                //tabela nao existe
                exit;
          End;


          //procuro pelo usuario
          close;
          sql.clear;
          sql.add('Select codigo,campo from TabConfCamposPesquisa where identificador='+#39+PIdentificador+#39);
          sql.add('and usuario='+#39+uppercase(ObjUsuarioGlobal.Get_nome)+#39);
          sql.add('order by codigo');
          
          open;
          
          if (recordcount=0)
          Then Begin
                    //caso nao encontre procuro pelo grupo
                    close;
                    sql.clear;
                    sql.add('Select codigo,campo from TabConfCamposPesquisa where identificador='+#39+PIdentificador+#39);
                    sql.add('and grupousuario='+ObjUsuarioGlobal.Get_Nivel);
                    sql.add('order by codigo');
                    open;

                    if (recordcount=0)
                    Then exit;
          End;
          
          first;

          PGrid.DataSource.DataSet.Fields.GetFieldNames(Self.ParametroPesquisa);

          Self.ParametroPesquisa.Text:=uppercase(Self.ParametroPesquisa.Text);
          PGrid.Columns.clear;
          
          while not(eof) do
          Begin
                if (Self.ParametroPesquisa.IndexOf(uppercase(fieldbyname('campo').asstring))>=0)
                Then Begin
                          PGrid.Columns.add;
                          PGrid.Columns[PGrid.Columns.Count-1].FieldName:=fieldbyname('campo').asstring;

                End
                Else Begin
                         Self.Exclui(fieldbyname('codigo').asstring,true);
                End;

                next;
          End;

     End;
end;


function TObjCONFCAMPOSpesquisa.Get_grupousuario: string;
begin
     result:=Self.grupousuario;
end;

procedure TObjCONFCAMPOSpesquisa.Submit_grupousuario(parametro: string);
begin
     Self.grupousuario:=Parametro;
end;

function TObjCONFCAMPOSpesquisa.TransfereCamposDoParametroParaBanco:Boolean;
Var StrListCampos:TStringList;
    Cont:Integer;
begin
     // Feito por Fabio dia 11/09/2009 por Ordem do Sadol
     // Porque: Para nuam ter mais duas telas de pesquisa eu preciso ler o Parametro
     // e colocar isso nesse objeto
     Result:=false;
try
     try
         StrListCampos:=TStringList.Create;
     except
         MensagemErro('Erro ao tentar criar a Stringlist');
         exit;
     end;

     if (ObjParametroGlobal.ValidaParametro('CAMPOS A SEREM MOSTRADOS EM PESQUISA DE PRODUTO PARA USUARIOS QUE N�O TEM ACESSO A TODOS OS CAMPOS')=false)
     then exit;

     ExplodeStr(ObjParametroGlobal.Get_Valor,StrListCampos,',','STRING');

     for cont:=0 to StrListCampos.Count-1 do
     Begin
          Self.ZerarTabela;
          Self.Submit_CODIGO('0');
          Self.Submit_grupousuario('2');
          Self.Submit_IDENTIFICADOR('FPEDIDO.PESQUISAPRODUTO');
          Self.Submit_campo(uppercase(StrListCampos[cont]));
          Self.Status:=dsInsert;

          if (Self.Salvar(False)=False)
          Then Begin
                   FDataModulo.IBTransaction.RollbackRetaining;
                   MensagemErro('Erro na tentativa de adicionar o campo '+Self.campo);
                   exit;
          End;

          //************************************************************


     End;
     FDataModulo.IBTransaction.CommitRetaining;
     Result:=true;    

finally
      FreeAndNil(StrListCampos);
end;

end;

end.



