object FmostraStringGrid: TFmostraStringGrid
  Left = 762
  Top = 199
  Width = 716
  Height = 480
  Caption = 'Escolha a Op'#231#227'o'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid: TStringGrid
    Left = 0
    Top = 68
    Width = 700
    Height = 333
    Align = alClient
    FixedCols = 0
    TabOrder = 0
    OnDblClick = StringGridDblClick
    OnKeyPress = StringGridKeyPress
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 700
    Height = 68
    Align = alTop
    TabOrder = 1
    object LabelMensagem: TLabel
      Left = 1
      Top = 54
      Width = 698
      Height = 13
      Align = alBottom
      Caption = '....'
    end
    object btcancelar: TBitBtn
      Left = 118
      Top = 4
      Width = 111
      Height = 47
      Caption = '&CANCELAR'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btcancelarClick
    end
    object Btok: TBitBtn
      Left = 6
      Top = 4
      Width = 111
      Height = 47
      Caption = '&OK'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = BtokClick
    end
    object FileListBoxIMportacao: TFileListBox
      Left = 344
      Top = 8
      Width = 145
      Height = 49
      ItemHeight = 13
      TabOrder = 2
      Visible = False
    end
  end
  object PanelRodape: TPanel
    Left = 0
    Top = 401
    Width = 700
    Height = 41
    Align = alBottom
    TabOrder = 2
    object lbrodape: TLabel
      Left = 1
      Top = 1
      Width = 698
      Height = 39
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
  end
end
