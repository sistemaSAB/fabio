unit UrelpersonalizadoReportBuilder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ppPrnabl, ppClass, ppCtrls, ppEndUsr, ppBands, ppCache, ppProd,
  ppReport, StdCtrls, Buttons, ppComm, ppRelatv, ppDB, ppDBPipe, DB, UobjRELPERSREPORTBUILDER;

type
  TFrelpersonalizadoReportBuilder = class(TForm)
    DATASOURCECABECALHO: TDataSource;
    DBCABECALHO: TppDBPipeline;
    BtConfigurar: TBitBtn;
    BtImprimir: TBitBtn;
    ppReport: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppDesigner: TppDesigner;
    DATASOURCEREPETICAO: TDataSource;
    DBREPETICAO: TppDBPipeline;
    DataSourceRepeticao_2: TDataSource;
    DBRepeticao_2: TppDBPipeline;
    ppImage1: TppImage;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    procedure BtImprimirClick(Sender: TObject);
    procedure BtConfigurarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    Objrelpersonalizado:TObjRELPERSREPORTBUILDER;
    StrConf:TStringList;
    procedure EventoTrataLabelConfiguracao(Sender: TObject;var Text: String);
    Procedure EventoOnPrintImage(Sender: TObject);

  public
    { Public declarations }
    PcaminhoImagem:String;
    Procedure PassaObjeto(Pobjeto:TObjRELPERSREPORTBUILDER);
    procedure ImprimeVariasCopias(Quantidade:Integer);

  end;

var
  FrelpersonalizadoReportBuilder: TFrelpersonalizadoReportBuilder;


implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TFrelpersonalizadoReportBuilder.BtImprimirClick(Sender: TObject);
VAr
  TempPath:String;
  cont:integer;
begin

     TempPath:='';
     TempPath:=extractfilepath(Application.ExeName);

     If TempPath[length(TempPath)]<>'\'
     Then TempPath:=TempPath+'\';

     ppReport.Template.FileName:=TempPath+'CONFRELS\'+Self.Objrelpersonalizado.Get_Arquivo;
     ppReport.Template.Load;

     for cont:=0 to Self.ComponentCount-1 do
     Begin
          if (uppercase(Self.Components[cont].ClassName)='TPPLABEL')
          Then begin
                    if (length(TppLabel(Self.Components[cont]).Caption)>2)
                    Then Begin
                              if ((TppLabel(Self.Components[cont]).Caption[1]='@')
                              and (TppLabel(Self.Components[cont]).Caption[2]='@'))
                              Then Begin
                                        //campo de configuracao
                                        TppLabel(Self.Components[cont]).Font.Color:=clWhite;
                                        TppLabel(Self.Components[cont]).OnGetText:=Self.EventoTrataLabelConfiguracao;
                              End;
                    End;
          End;
     End;

     ppReport.Print;
end;

procedure TFrelpersonalizadoReportBuilder.ImprimeVariasCopias(Quantidade:Integer);
VAr
  TempPath:String;
  cont:integer;
  F:thandle;
begin

     TempPath:='';
     TempPath:=extractfilepath(Application.ExeName);

     If TempPath[length(TempPath)]<>'\'
     Then TempPath:=TempPath+'\';

     ppReport.Template.FileName:=TempPath+'CONFRELS\'+Self.Objrelpersonalizado.Get_Arquivo;
     ppReport.Template.Load;

     for cont:=0 to Self.ComponentCount-1 do
     Begin
          if (uppercase(Self.Components[cont].ClassName)='TPPLABEL')
          Then begin
                    if (length(TppLabel(Self.Components[cont]).Caption)>2)
                    Then Begin
                              if ((TppLabel(Self.Components[cont]).Caption[1]='@')
                              and (TppLabel(Self.Components[cont]).Caption[2]='@'))
                              Then Begin
                                        //campo de configuracao
                                        TppLabel(Self.Components[cont]).Font.Color:=clWhite;
                                        TppLabel(Self.Components[cont]).OnGetText:=Self.EventoTrataLabelConfiguracao;
                              End;
                    End;
          End;
     End;
     Self.ppReport.PrinterSetup.Copies:=Quantidade;
     F := Application.Handle;
     ForceForegroundWindow(F);
     ppReport.Print;
     Self.ppReport.PrinterSetup.Copies:=1;
end;                           


procedure TFrelpersonalizadoReportBuilder.PassaObjeto(PObjeto: TObjRELPERSREPORTBUILDER);
begin
     Self.Objrelpersonalizado:=Pobjeto;
end;

procedure TFrelpersonalizadoReportBuilder.BtConfigurarClick(Sender: TObject);
Var TempPath:String;
begin

     TempPath:='';
     TempPath:=extractfilepath(Application.ExeName);

     If TempPath[length(TempPath)]<>'\'
     Then TempPath:=TempPath+'\';

     ppReport.Template.FileName:=TempPath+'CONFRELS\'+Self.Objrelpersonalizado.Get_Arquivo;
     ppReport.Template.Load;
     ppDesigner.Report:=ppReport;
     ppDesigner.ShowModal;

     // Apos Configurar eu preciso salvar esse arquivo no banco
     // O Objeto j� estara preenchid   

   Objrelpersonalizado.Status:=dsEdit;
     if (Self.Objrelpersonalizado.Salvar(true)=false)

      then Begin
             MensagemErro('Erro ao tentar salvar o Arquivo (rtm) no banco de dados');
             exit;
     end;

end;

procedure TFrelpersonalizadoReportBuilder.EventoTrataLabelConfiguracao(Sender: TObject; var Text: String);
var
posicao:integer;
temp:string;
begin
     //tratando label de configuracao
     if (length(text)>2)
     then Begin
               if ((text[1]='@') and (text[2]='@'))
               Then Begin
                         Self.strconf.clear;
                         ExplodeStr(text,Self.StrConf,'@','string'); //Corrigido o separador de @@ para @ - Rodolfo

                         for posicao:=0 to Self.ComponentCount-1 do
                         Begin
                             if ((uppercase(Self.Components[posicao].Name)=uppercase(Self.StrConf[0]))
                             and (uppercase(Self.Components[posicao].ClassName)='TPPIMAGE'))
                             then Begin
                                       Try
                                          (*
                                            na configuracao do caption da label
                                            vem
                                            @@NOMECOMPONENTEIMAGE@@CAMPODOBANCODEDADOSCOMOCAMINNHO@@QUERYQUEESTAESSECAMPO@@
                                          *)

                                          if (Self.StrConf[2]='R')
                                          then Begin
                                                    TppImage(Self.Components[posicao]).Picture.LoadFromFile(Self.PcaminhoImagem+Self.DBREPETICAO.DataSource.DataSet.FieldByName(Self.StrConf[1]).asstring);
                                          End
                                          Else begin
                                                    TppImage(Self.Components[posicao]).Picture.LoadFromFile(Self.PcaminhoImagem+Self.DBCABECALHO.DataSource.DataSet.FieldByName(Self.StrConf[1]).asstring);
                                          End;
                                       Except

                                       End;
                             End;
                         End;
               End;
     End;

end;

procedure TFrelpersonalizadoReportBuilder.FormCreate(Sender: TObject);
begin
     Self.StrConf:=TStringList.Create;
end;

procedure TFrelpersonalizadoReportBuilder.FormDestroy(Sender: TObject);
begin
     Self.StrConf.free;
end;

procedure TFrelpersonalizadoReportBuilder.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
Self.PcaminhoImagem:='';
end;

procedure TFrelpersonalizadoReportBuilder.EventoOnPrintImage(
  Sender: TObject);
begin

end;

end.
