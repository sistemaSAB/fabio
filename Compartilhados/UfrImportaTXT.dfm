object FrImportaTXT: TFrImportaTXT
  Left = 0
  Top = 0
  Width = 790
  Height = 543
  TabOrder = 0
  object GuiaTXT: TPageControl
    Left = 0
    Top = 0
    Width = 790
    Height = 543
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChanging = GuiaTXTChanging
    object TabSheet1: TTabSheet
      Caption = 'Principal'
      object BarradeProgresso: TProgressBar
        Left = 0
        Top = 499
        Width = 782
        Height = 16
        Align = alBottom
        TabOrder = 4
      end
      object STRGDados: TStringGrid
        Left = 0
        Top = 304
        Width = 782
        Height = 170
        Align = alClient
        FixedCols = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        TabOrder = 2
      end
      object MemoArquivo: TRichEdit
        Left = 0
        Top = 0
        Width = 782
        Height = 180
        Align = alTop
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          'MemoArquivo')
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
        WantReturns = False
        WordWrap = False
      end
      object PanelCaminhotabela: TPanel
        Left = 0
        Top = 180
        Width = 782
        Height = 124
        Align = alTop
        TabOrder = 1
        DesignSize = (
          782
          124)
        object Label1: TLabel
          Left = 489
          Top = 81
          Width = 48
          Height = 16
          Anchors = [akTop, akRight]
          Caption = 'Tabela'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 13
          Top = 49
          Width = 93
          Height = 13
          Caption = 'Caracter delimitador'
        end
        object Label7: TLabel
          Left = 649
          Top = 48
          Width = 54
          Height = 13
          Anchors = [akTop, akRight]
          Caption = 'Linha Proc.'
        end
        object LblinhaAtual: TLabel
          Left = 721
          Top = 48
          Width = 57
          Height = 13
          Alignment = taRightJustify
          Anchors = [akTop, akRight]
          AutoSize = False
          Caption = ' 0'
          OnDblClick = LblinhaAtualDblClick
        end
        object BtAbrirArquivo: TBitBtn
          Left = 3
          Top = 2
          Width = 161
          Height = 25
          Caption = 'Abrir Arquivo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BtAbrirArquivoClick
        end
        object ComboTabela: TComboBox
          Left = 489
          Top = 97
          Width = 145
          Height = 21
          Anchors = [akTop, akRight]
          ItemHeight = 13
          TabOrder = 7
          Text = '   '
          OnKeyPress = ComboTabelaKeyPress
        end
        object BitBtn2: TBitBtn
          Left = 638
          Top = 94
          Width = 137
          Height = 25
          Anchors = [akTop, akRight]
          Caption = 'Iniciar Importa'#231#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
          OnClick = BitBtn2Click
        end
        object edtcaminhobanco: TEdit
          Left = 168
          Top = 4
          Width = 521
          Height = 21
          TabOrder = 2
        end
        object btcaminhobanco: TBitBtn
          Left = 694
          Top = 2
          Width = 29
          Height = 25
          Caption = '...'
          TabOrder = 1
          OnClick = btcaminhobancoClick
        end
        object edtcaracterdelimitador: TEdit
          Left = 118
          Top = 46
          Width = 33
          Height = 21
          TabOrder = 3
        end
        object checkcabecalho: TCheckBox
          Left = 16
          Top = 74
          Width = 120
          Height = 17
          Caption = 'Linha de Cabe'#231'alho'
          TabOrder = 4
        end
        object BtLerArquivo: TBitBtn
          Left = 3
          Top = 95
          Width = 209
          Height = 25
          Caption = 'Ler Arquivo e Separar Colunas'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          OnClick = BtLerArquivoClick
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 474
        Width = 782
        Height = 25
        Align = alBottom
        TabOrder = 3
        DesignSize = (
          782
          25)
        object Button3: TButton
          Left = 721
          Top = 7
          Width = 57
          Height = 18
          Anchors = [akTop, akRight]
          Caption = 'Ir para'
          TabOrder = 2
          OnClick = Button3Click
        end
        object Button2: TButton
          Left = 593
          Top = 7
          Width = 101
          Height = 18
          Anchors = [akTop, akRight]
          Caption = 'Mudar linha Proc.'
          TabOrder = 1
          OnClick = Button3Click
        end
        object btPegaLinha: TButton
          Left = 480
          Top = 7
          Width = 101
          Height = 18
          Caption = 'Pegar Linha Atual'
          TabOrder = 0
          OnClick = btPegaLinhaClick
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Conf. Campos'
      ImageIndex = 1
      object PanelPrincipalConfiguracao: TPanel
        Left = 525
        Top = 0
        Width = 257
        Height = 515
        Align = alRight
        Caption = 'PanelPrincipalConfiguracao'
        TabOrder = 1
        object PanelNovoValor: TPanel
          Left = 1
          Top = 100
          Width = 255
          Height = 126
          Align = alTop
          TabOrder = 1
          object Button1: TButton
            Left = 32
            Top = 98
            Width = 209
            Height = 25
            Caption = 'Altera &Valor do Campo'
            TabOrder = 0
            OnClick = Button1Click
          end
          object Rg_Opcoes_valor: TRadioGroup
            Left = 1
            Top = 1
            Width = 253
            Height = 94
            Align = alTop
            Caption = 'Op'#231#245'es de Valor'
            TabOrder = 1
          end
        end
        object PanelConfExtras: TPanel
          Left = 1
          Top = 226
          Width = 255
          Height = 165
          Align = alTop
          TabOrder = 2
          object Label4: TLabel
            Left = 1
            Top = 1
            Width = 253
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Extras'
          end
          object CLB_ConfExtras: TCheckListBox
            Left = 1
            Top = 14
            Width = 253
            Height = 121
            Align = alTop
            ItemHeight = 13
            Items.Strings = (
              'Retira Aspas'
              'Retira Ap'#243'strofo'
              'Retira Ponto'
              'Adiciona Virgula 2 Casas da Direita'
              'Adiciona Virgula 3 Casas da Direita'
              'Acrescenta Barra de Data')
            TabOrder = 0
          end
          object BtAdicionaConfExtra: TButton
            Left = 32
            Top = 137
            Width = 208
            Height = 25
            Caption = 'Adiciona Configura'#231#227'o &Extra'
            TabOrder = 1
            OnClick = BtAdicionaConfExtraClick
          end
        end
        object Panel4: TPanel
          Left = 1
          Top = 391
          Width = 255
          Height = 136
          Align = alTop
          Caption = 'Panel4'
          TabOrder = 3
          object Label3: TLabel
            Left = 1
            Top = 1
            Width = 146
            Height = 13
            Align = alTop
            Caption = 'Campos dispon'#237'veis na Tabela'
          end
          object Lbnometabela: TLabel
            Left = 1
            Top = 14
            Width = 30
            Height = 13
            Align = alTop
            Caption = '..........'
          end
          object LbCamposTabela: TListBox
            Left = 1
            Top = 27
            Width = 253
            Height = 108
            Align = alClient
            ItemHeight = 13
            Sorted = True
            TabOrder = 0
            OnClick = LbCamposTabelaClick
          end
        end
        object Panel6: TPanel
          Left = 1
          Top = 1
          Width = 255
          Height = 99
          Align = alTop
          TabOrder = 0
          object LbSelecionado: TLabel
            Left = 1
            Top = 1
            Width = 3
            Height = 13
            Align = alTop
            Alignment = taCenter
          end
          object BitBtn3: TBitBtn
            Left = 0
            Top = 17
            Width = 240
            Height = 25
            Caption = 'Carregar Campos da Primeira Linha Grid'
            TabOrder = 0
            OnClick = BitBtn3Click
          end
          object BitBtn4: TBitBtn
            Left = 0
            Top = 68
            Width = 240
            Height = 25
            Caption = 'Limpa Campos'
            TabOrder = 1
            OnClick = BitBtn4Click
          end
          object BitBtn1: TBitBtn
            Left = 1
            Top = 42
            Width = 240
            Height = 25
            Caption = 'Carregar Campos Extras da Tabela'
            TabOrder = 2
            OnClick = BitBtn1Click
          end
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 525
        Height = 515
        Align = alClient
        Caption = 'Panel5'
        TabOrder = 0
        object Panel2: TPanel
          Left = 1
          Top = 1
          Width = 523
          Height = 299
          Align = alTop
          TabOrder = 0
          object Label6: TLabel
            Left = 1
            Top = 1
            Width = 124
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'CAMPOS DO GRID (TXT)'
          end
          object LbCamposGRID: TListBox
            Left = 1
            Top = 14
            Width = 248
            Height = 243
            TabStop = False
            Align = alClient
            ItemHeight = 13
            TabOrder = 0
            OnClick = LbCamposGRIDClick
            OnKeyDown = LbCamposGRIDKeyDown
          end
          object LbValorGrid: TListBox
            Left = 249
            Top = 14
            Width = 273
            Height = 243
            TabStop = False
            Align = alRight
            ItemHeight = 13
            TabOrder = 1
            OnClick = LbValorGridClick
            OnKeyDown = LbValorGridKeyDown
          end
          object Panel7: TPanel
            Left = 1
            Top = 257
            Width = 521
            Height = 41
            Align = alBottom
            TabOrder = 2
            object BtSalvaConf_GRID: TButton
              Left = 325
              Top = 8
              Width = 113
              Height = 25
              Caption = 'Salvar Configura'#231#227'o'
              TabOrder = 0
              OnClick = BtSalvaConf_GRIDClick
            end
            object BtAbreConf_GRID: TButton
              Left = 437
              Top = 8
              Width = 113
              Height = 25
              Caption = 'Abrir Configura'#231#227'o'
              TabOrder = 1
              OnClick = BtAbreConf_GRIDClick
            end
          end
        end
        object Panel3: TPanel
          Left = 1
          Top = 300
          Width = 523
          Height = 214
          Align = alClient
          TabOrder = 1
          object Label5: TLabel
            Left = 1
            Top = 1
            Width = 91
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'CAMPOS EXTRAS'
          end
          object LbCamposExtras: TListBox
            Left = 1
            Top = 14
            Width = 247
            Height = 158
            TabStop = False
            Align = alClient
            ItemHeight = 13
            TabOrder = 0
            OnClick = LbCamposExtrasClick
            OnDblClick = LbCamposExtrasDblClick
            OnKeyDown = LbCamposExtrasKeyDown
          end
          object LbValorExtras: TListBox
            Left = 248
            Top = 14
            Width = 274
            Height = 158
            TabStop = False
            Align = alRight
            ItemHeight = 13
            TabOrder = 1
            OnClick = LbValorExtrasClick
            OnDblClick = LbCamposExtrasDblClick
            OnKeyDown = LbValorExtrasKeyDown
          end
          object Panel8: TPanel
            Left = 1
            Top = 172
            Width = 521
            Height = 41
            Align = alBottom
            TabOrder = 2
            object Button5: TButton
              Left = 325
              Top = 8
              Width = 113
              Height = 25
              Caption = 'Salvar Configura'#231#227'o'
              TabOrder = 0
              OnClick = Button5Click
            end
            object Button6: TButton
              Left = 437
              Top = 8
              Width = 113
              Height = 25
              Caption = 'Abrir Configura'#231#227'o'
              TabOrder = 1
              OnClick = Button6Click
            end
          end
        end
      end
    end
  end
  object OpenDialog: TOpenDialog
    Left = 664
    Top = 120
  end
  object SaveDialog: TSaveDialog
    Left = 696
    Top = 112
  end
end
