unit UCONFIGURACAOTELA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCONFIGURACAOTELA,
  jpeg;

type
  TFCONFIGURACAOTELA = class(TForm)
    panelbotes: TPanel;
    Label1: TLabel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    MemoConfiguracoes: TMemo;
    LbConfiguracoes: TLabel;
    EdtNomeFormulario: TEdit;
    LbNomeFormulario: TLabel;
    EdtCODIGO: TEdit;
    LbCODIGO: TLabel;
    Shape1: TShape;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MemoConfiguracoesKeyPress(Sender: TObject; var Key: Char);
  private
         ObjCONFIGURACAOTELA:TObjCONFIGURACAOTELA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCONFIGURACAOTELA: TFCONFIGURACAOTELA;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCONFIGURACAOTELA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCONFIGURACAOTELA do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_NomeFormulario(edtNomeFormulario.text);
        Submit_Configuracoes(memoConfiguracoes.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCONFIGURACAOTELA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCONFIGURACAOTELA do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNomeFormulario.text:=Get_NomeFormulario;
        memoConfiguracoes.text:=Get_Configuracoes;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCONFIGURACAOTELA.TabelaParaControles: Boolean;
begin
     If (Self.ObjCONFIGURACAOTELA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCONFIGURACAOTELA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCONFIGURACAOTELA=Nil)
     Then exit;

    If (Self.ObjCONFIGURACAOTELA.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    Self.ObjCONFIGURACAOTELA.free;

end;

procedure TFCONFIGURACAOTELA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCONFIGURACAOTELA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCONFIGURACAOTELA.Get_novocodigo;
     edtcodigo.enabled:=False;


     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     Self.ObjCONFIGURACAOTELA.status:=dsInsert;
     edtNomeFormulario.setfocus;

end;

procedure TFCONFIGURACAOTELA.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCONFIGURACAOTELA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCONFIGURACAOTELA.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
                edtNomeFormulario.setfocus;
                
          End;


end;

procedure TFCONFIGURACAOTELA.btgravarClick(Sender: TObject);
begin

     If Self.ObjCONFIGURACAOTELA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCONFIGURACAOTELA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCONFIGURACAOTELA.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCONFIGURACAOTELA.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCONFIGURACAOTELA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCONFIGURACAOTELA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCONFIGURACAOTELA.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCONFIGURACAOTELA.btcancelarClick(Sender: TObject);
begin
     Self.ObjCONFIGURACAOTELA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCONFIGURACAOTELA.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCONFIGURACAOTELA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCONFIGURACAOTELA.Get_pesquisa,Self.ObjCONFIGURACAOTELA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCONFIGURACAOTELA.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCONFIGURACAOTELA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCONFIGURACAOTELA.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCONFIGURACAOTELA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCONFIGURACAOTELA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCONFIGURACAOTELA:=TObjCONFIGURACAOTELA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCONFIGURACAOTELA.MemoConfiguracoesKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then MemoConfiguracoes.SetFocus;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjCONFIGURACAOTELA.OBJETO.Get_Pesquisa,Self.ObjCONFIGURACAOTELA.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCONFIGURACAOTELA.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjCONFIGURACAOTELA.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCONFIGURACAOTELA.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
