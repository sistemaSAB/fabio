unit UCSTPIS;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCSTPIS,
  jpeg;

type
  TFCSTPIS = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidadeformulario: TLabel;
    LbCODIGO: TLabel;
    LbDESCRICAO: TLabel;
    EdtCODIGO: TEdit;
    EdtDESCRICAO: TEdit;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    ImagemFundo: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCSTPIS:TObjCSTPIS;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCSTPIS: TFCSTPIS;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCSTPIS.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCSTPIS do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_DESCRICAO(edtDESCRICAO.text);

         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCSTPIS.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCSTPIS do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtDESCRICAO.text:=Get_DESCRICAO;

        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCSTPIS.TabelaParaControles: Boolean;
begin
     If (Self.ObjCSTPIS.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFCSTPIS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCSTPIS=Nil)
     Then exit;

     If (Self.ObjCSTPIS.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjCSTPIS.free;

     if(Self.Owner = nil) then
       FDataModulo.CommitGeral;
     
end;

procedure TFCSTPIS.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFCSTPIS.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjCSTPIS.status:=dsInsert;
     edtDESCRICAO.setfocus;

end;


procedure TFCSTPIS.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCSTPIS.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCSTPIS.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtDESCRICAO.setfocus;
                
    End;


end;

procedure TFCSTPIS.btgravarClick(Sender: TObject);
begin

     If Self.ObjCSTPIS.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCSTPIS.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCSTPIS.Get_codigo;
     Self.ObjCSTPIS.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCSTPIS.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjCSTPIS.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCSTPIS.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCSTPIS.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCSTPIS.btcancelarClick(Sender: TObject);
begin
     Self.ObjCSTPIS.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCSTPIS.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFCSTPIS.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCSTPIS.Get_pesquisa,Self.ObjCSTPIS.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCSTPIS.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCSTPIS.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjCSTPIS.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFCSTPIS.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCSTPIS.FormShow(Sender: TObject);
begin

limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCSTPIS:=TObjCSTPIS.create(Self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
     retira_fundo_labels(self);
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.

