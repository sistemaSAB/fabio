unit UobjSUBMENURELPERSONALIZADO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal
,UOBJMENURELPERSONALIZADO
;
//USES_INTERFACE



Type
   TObjSUBMENURELPERSONALIZADO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               Menu:TOBJMENURELPERSONALIZADO;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                procedure EdtMenuExit(Sender: TObject;LABELNOME:TLABEL);
procedure EdtMenuKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Nome:string;
//CODIFICA VARIAVEIS PRIVADAS




               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UMENURELPERSONALIZADO;





Function  TObjSUBMENURELPERSONALIZADO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('Menu').asstring<>'')
        Then Begin
                 If (Self.Menu.LocalizaCodigo(FieldByName('Menu').asstring)=False)
                 Then Begin
                          Messagedlg('Menu N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Menu.TabelaparaObjeto;
        End;
        Self.Nome:=fieldbyname('Nome').asstring;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjSUBMENURELPERSONALIZADO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Menu').asstring:=Self.Menu.GET_CODIGO;
        ParamByName('Nome').asstring:=Self.Nome;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjSUBMENURELPERSONALIZADO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjSUBMENURELPERSONALIZADO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Menu.ZerarTabela;
        Nome:='';
//CODIFICA ZERARTABELA



     End;
end;

Function TObjSUBMENURELPERSONALIZADO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Menu.get_Codigo='')
      Then Mensagem:=mensagem+'/Menu';
      If (Nome='')
      Then Mensagem:=mensagem+'/Nome';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjSUBMENURELPERSONALIZADO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Menu.LocalizaCodigo(Self.Menu.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Menu n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjSUBMENURELPERSONALIZADO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Menu.Get_Codigo<>'')
        Then Strtoint(Self.Menu.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Menu';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjSUBMENURELPERSONALIZADO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjSUBMENURELPERSONALIZADO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjSUBMENURELPERSONALIZADO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro SUBMENURELPERSONALIZADO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Menu,Nome');
           SQL.ADD(' from  TabSubMenuRelPersonalizado');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjSUBMENURELPERSONALIZADO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjSUBMENURELPERSONALIZADO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjSUBMENURELPERSONALIZADO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Menu:=TOBJMENURELPERSONALIZADO.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabSubMenuRelPersonalizado(CODIGO,Menu,Nome');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:Menu,:Nome)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabSubMenuRelPersonalizado set CODIGO=:CODIGO');
                ModifySQL.add(',Menu=:Menu,Nome=:Nome');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabSubMenuRelPersonalizado where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjSUBMENURELPERSONALIZADO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjSUBMENURELPERSONALIZADO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabSUBMENURELPERSONALIZADO');
     Result:=Self.ParametroPesquisa;
end;

function TObjSUBMENURELPERSONALIZADO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de SUBMENURELPERSONALIZADO ';
end;


function TObjSUBMENURELPERSONALIZADO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENSUBMENURELPERSONALIZADO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENSUBMENURELPERSONALIZADO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjSUBMENURELPERSONALIZADO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Menu.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjSUBMENURELPERSONALIZADO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjSUBMENURELPERSONALIZADO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjSubMenuRelPersonalizado.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjSubMenuRelPersonalizado.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjSubMenuRelPersonalizado.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjSubMenuRelPersonalizado.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
//CODIFICA GETSESUBMITS


procedure TObjSUBMENURELPERSONALIZADO.EdtMenuExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Menu.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Menu.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Menu.GET_NOME;
End;
procedure TObjSUBMENURELPERSONALIZADO.EdtMenuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FMENURELPERSONALIZADO:TFMENURELPERSONALIZADO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FMENURELPERSONALIZADO:=TFMENURELPERSONALIZADO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Menu.Get_Pesquisa,Self.Menu.Get_TituloPesquisa,FMENURELPERSONALIZADO)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Menu.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Menu.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Menu.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FMENURELPERSONALIZADO);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjSUBMENURELPERSONALIZADO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJSUBMENURELPERSONALIZADO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



end.



