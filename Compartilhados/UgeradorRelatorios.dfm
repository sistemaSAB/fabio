object FgeradorRelatorios: TFgeradorRelatorios
  Left = 86
  Top = 93
  Width = 664
  Height = 430
  Caption = 'Gerador de Relat'#243'rios - Exclaim Tecnologia'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid_Selecao: TDBGrid
    Left = 8
    Top = 192
    Width = 641
    Height = 201
    DataSource = DS_Selecao
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object MemoSql: TMemo
    Left = 8
    Top = 52
    Width = 521
    Height = 133
    Lines.Strings = (
      'MemoSql')
    TabOrder = 1
  end
  object BtExecutaComando: TButton
    Left = 536
    Top = 96
    Width = 113
    Height = 44
    Caption = 'Executa Comando'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = BtExecutaComandoClick
  end
  object BTAbrirComando: TButton
    Left = 536
    Top = 52
    Width = 113
    Height = 44
    Caption = 'Abrir Comando'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 3
    OnClick = BTAbrirComandoClick
  end
  object BtDesenhaRelatorio: TButton
    Left = 536
    Top = 140
    Width = 113
    Height = 45
    Caption = 'Desenha Relat'#243'rio'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    OnClick = BtDesenhaRelatorioClick
  end
  object DS_Selecao: TDataSource
    DataSet = Query_Selecao
    Left = 552
    Top = 16
  end
  object Query_Selecao: TIBQuery
    Database = FDataModulo.IBDatabase
    Transaction = FDataModulo.IBTransaction
    Left = 584
    Top = 16
  end
  object Query_Relatorio: TIBQuery
    Database = FDataModulo.IBDatabase
    Transaction = FDataModulo.IBTransaction
    Left = 616
    Top = 16
  end
  object OpenDialog: TOpenDialog
    Filter = 'Arquivos Sql|*.sql|Arquivos Texto|*.txt'
    Left = 520
    Top = 16
  end
end
