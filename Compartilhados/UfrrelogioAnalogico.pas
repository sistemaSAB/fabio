unit UfrrelogioAnalogico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, StdCtrls, jpeg, ExtCtrls;

type
  TFrrelogioanalogico = class(TFrame)
    lbrelogioanalogico: TLabel;
    ImageRelogio: TImage;
    Hmemox: TMemo;
    Hmemoy: TMemo;
    Mmemox: TMemo;
    Mmemoy: TMemo;
    Timerrelogio: TTimer;
    procedure TimerrelogioTimer(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure Inicializarelogio;
  end;

implementation

uses UessencialGlobal;

{$R *.dfm}

{ TFrrelogioanalogico }

procedure TFrrelogioanalogico.Inicializarelogio;
begin

     TimerRelogio.enabled:=false;

     lbrelogioanalogico.caption:='';
     lbrelogioanalogico.AutoSize:=False;
     lbrelogioanalogico.Left:=ImageRelogio.Left;
     lbrelogioanalogico.Top:=ImageRelogio.Top;
     lbrelogioanalogico.Width:=ImageRelogio.Width;
     lbrelogioanalogico.Height:=ImageRelogio.Height;
     lbrelogioanalogico.repaint;
     TimerrelogioTimer(nil);
     TimerRelogio.enabled:=true;


end;

procedure TFrrelogioanalogico.TimerrelogioTimer(Sender: TObject);
var
phora:Ttime;
hora,min,seg,mseg:word;
X,Y:integer;
begin
      lbrelogioanalogico.Repaint;
      phora:=RetornaDataHoraServidor;

      DecodeTime(phora,hora,min,seg,mseg);

      lbrelogioanalogico.Canvas.Pen.Color:=clblack;
      lbrelogioanalogico.Canvas.Pen.Width:=1;
      lbrelogioanalogico.Canvas.MoveTo(62,60);
      lbrelogioanalogico.Canvas.LineTo(strtoint(Mmemox.Lines[min]),strtoint(Mmemoy.lines[min]));

      lbrelogioanalogico.Canvas.Pen.Color:=clblue;
      lbrelogioanalogico.Canvas.Pen.Width:=1;
      lbrelogioanalogico.Canvas.MoveTo(62,60);

      if (hora>=12)
      then hora:=hora-12;//sistema de 12 horas e nao 24

      lbrelogioanalogico.Canvas.LineTo(strtoint(Hmemox.Lines[Hora*2]),strtoint(Hmemoy.lines[hora*2]));

End;

end.
