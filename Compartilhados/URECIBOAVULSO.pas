unit URECIBOAVULSO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjRECIBOAVULSO,
  jpeg, ppCtrls, ppPrnabl, ppClass, ppBands, ppCache, ppComm, ppRelatv,
  ppProd, ppReport, ppModule, raCodMod, UDataModulo;

type
  TFRECIBOAVULSO = class(TForm)
    Panel2: TPanel;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    LbData: TLabel;
    LbHora: TLabel;
    LbValor: TLabel;
    LbCredor: TLabel;
    LbRecebedor: TLabel;
    LbReferente: TLabel;
    EdtCODIGO: TEdit;
    EdtData: TMaskEdit;
    EdtHora: TMaskEdit;
    EdtValor: TEdit;
    EdtCredor: TEdit;
    EdtRecebedor: TEdit;
    EdtReferente: TEdit;
    ImagemFundo: TImage;
    ppRecibo: TppReport;
    ppHeaderBand1: TppHeaderBand;
    ppDetailBand1: TppDetailBand;
    ppFooterBand1: TppFooterBand;
    ppShape1: TppShape;
    ppLabel1: TppLabel;
    ppLabel2: TppLabel;
    ppLabel3: TppLabel;
    ppLabel4: TppLabel;
    ppShape2: TppShape;
    ppShape3: TppShape;
    pplbpagador: TppLabel;
    pplbrecebedor: TppLabel;
    pplbreferente: TppLabel;
    pplbvalor: TppLabel;
    ppShape4: TppShape;
    ppShape5: TppShape;
    ppShape6: TppShape;
    pplbdata: TppLabel;
    raCodeModule1: TraCodeModule;
    ppShape7: TppShape;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
  private
         ObjRECIBOAVULSO:TObjRECIBOAVULSO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         Function  AtualizaQuantidade: string;
         procedure ImprimePersonalizado;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FRECIBOAVULSO: TFRECIBOAVULSO;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UMenuRelatorios;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFRECIBOAVULSO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjRECIBOAVULSO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Data(edtData.text);
        Submit_Hora(edtHora.text);
        Submit_Valor(edtValor.text);
        Submit_Credor(edtCredor.text);
        Submit_Recebedor(edtRecebedor.text);
        Submit_Referente(edtReferente.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFRECIBOAVULSO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjRECIBOAVULSO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtData.text:=Get_Data;
        EdtHora.text:=Get_Hora;
        EdtValor.text:=Get_Valor;
        EdtCredor.text:=Get_Credor;
        EdtRecebedor.text:=Get_Recebedor;
        EdtReferente.text:=Get_Referente;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFRECIBOAVULSO.TabelaParaControles: Boolean;
begin
     If (Self.ObjRECIBOAVULSO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFRECIBOAVULSO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjRECIBOAVULSO=Nil)
     Then exit;

     If (Self.ObjRECIBOAVULSO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

     Self.ObjRECIBOAVULSO.free;
end;

procedure TFRECIBOAVULSO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFRECIBOAVULSO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjRECIBOAVULSO.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     Self.ObjRECIBOAVULSO.status:=dsInsert;
     edtData.setfocus;
     edtData.text:=datetostr(RetornaDataServidor);
     edthora.text:=timetostr(RetornaDataHoraServidor);


end;


procedure TFRECIBOAVULSO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjRECIBOAVULSO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjRECIBOAVULSO.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
                edtData.setfocus;
                
          End;


end;

procedure TFRECIBOAVULSO.btgravarClick(Sender: TObject);
begin

     If Self.ObjRECIBOAVULSO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjRECIBOAVULSO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjRECIBOAVULSO.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFRECIBOAVULSO.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjRECIBOAVULSO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjRECIBOAVULSO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjRECIBOAVULSO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFRECIBOAVULSO.btcancelarClick(Sender: TObject);
begin
     Self.ObjRECIBOAVULSO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFRECIBOAVULSO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFRECIBOAVULSO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjRECIBOAVULSO.Get_pesquisa,Self.ObjRECIBOAVULSO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjRECIBOAVULSO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjRECIBOAVULSO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjRECIBOAVULSO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFRECIBOAVULSO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFRECIBOAVULSO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjRECIBOAVULSO:=TObjRECIBOAVULSO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;
     lbquantidade.Caption:=AtualizaQuantidade;
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFRECIBOAVULSO.btrelatoriosClick(Sender: TObject);
begin

     With FmenuRelatorios do
     Begin
            With RgOpcoes do
            Begin
                  items.clear;
                  items.add('Recibo Personalizado');//0
                  items.add('Recibo Padr�o');//1
            end;

            showmodal;

            If (Tag=0)//indica botao cancel ou fechar
            Then exit;
            Case RgOpcoes.ItemIndex of
                  00:Self.ObjRECIBOAVULSO.ImprimeRecibo(edtcodigo.text);
                  01:imprimePersonalizado;
            End
     end;

end;

function TFRECIBOAVULSO.AtualizaQuantidade: string;
begin
     result:='Existem '+ ContaRegistros('TABRECIBOAVULSO','codigo') + ' Recibos Avulsos Cadastrados';
end;

procedure TFRECIBOAVULSO.ImprimePersonalizado;
begin
     pplbpagador.Caption:=EdtCredor.Text;
     pplbrecebedor.Caption:=EdtRecebedor.Text;
     pplbreferente.Caption:='                        '+EdtReferente.Text;
     pplbvalor.Caption:=formata_valor(EdtValor.Text)+' ('+valorextenso(StrToCurr(EdtValor.Text))+')' ;
     if (Objparametroglobal.validaparametro('CIDADE A SER IMPRESSA NO CHEQUE')=False)
     Then Exit;
     pplbdata.Caption:=ObjparametroGlobal.get_valor+', '+FormatDateTime('dd "de" mmmm "de" yyyy',now);
     ppRecibo.Print;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjRECIBOAVULSO.OBJETO.Get_Pesquisa,Self.ObjRECIBOAVULSO.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjRECIBOAVULSO.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjRECIBOAVULSO.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjRECIBOAVULSO.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
