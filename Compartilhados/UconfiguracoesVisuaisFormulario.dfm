object FconfiguracoesVisuaisFormulario: TFconfiguracoesVisuaisFormulario
  Left = 486
  Top = 179
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Configura'#231#245'es do Formul'#225'rio'
  ClientHeight = 85
  ClientWidth = 254
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 5
    Top = 3
    Width = 78
    Height = 14
    Caption = 'Componente: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbnomeComponente: TLabel
    Left = 87
    Top = 3
    Width = 78
    Height = 14
    Caption = 'Componente: '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object BtGravaConfiguracoes: TButton
    Left = 3
    Top = 55
    Width = 244
    Height = 25
    Caption = 'Grava Configura'#231#245'es'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = BtGravaConfiguracoesClick
  end
  object BtOcultaCampo: TButton
    Left = 3
    Top = 32
    Width = 244
    Height = 23
    Caption = 'Oculta/Mostra Campo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BtOcultaCampoClick
  end
end
