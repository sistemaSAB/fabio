object Fusuarios: TFusuarios
  Left = 526
  Top = 315
  Width = 633
  Height = 301
  Caption = 'Cadastro de Usu'#225'rios - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 617
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 516
      Top = 0
      Width = 101
      Height = 51
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Usu'#225'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -21
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    object btsair: TBitBtn
      Left = 402
      Top = -1
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object BtOpcoes: TBitBtn
      Left = 352
      Top = -1
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      OnClick = BtOpcoesClick
    end
    object btcancelar: TBitBtn
      Left = 152
      Top = -1
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btgravar: TBitBtn
      Left = 102
      Top = -1
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btexcluir: TBitBtn
      Left = 202
      Top = -1
      Width = 50
      Height = 52
      Caption = '&E'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btrelatorios: TBitBtn
      Left = 302
      Top = -1
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object btalterar: TBitBtn
      Left = 52
      Top = -1
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object Btnovo: TBitBtn
      Left = 2
      Top = -1
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btpesquisar: TBitBtn
      Left = 252
      Top = -1
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 51
    Width = 617
    Height = 162
    Align = alClient
    Color = clSilver
    TabOrder = 1
    DesignSize = (
      617
      162)
    object ImagemFundo: TImage
      Left = 1
      Top = 1
      Width = 615
      Height = 160
      Align = alClient
      Stretch = True
    end
    object Label1: TLabel
      Left = 30
      Top = 23
      Width = 39
      Height = 14
      Caption = 'C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel
      Left = 30
      Top = 46
      Width = 94
      Height = 14
      Caption = 'Nome do Usu'#225'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label3: TLabel
      Left = 30
      Top = 116
      Width = 26
      Height = 14
      HelpType = htKeyword
      Caption = 'N'#237'vel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object LbNivel: TLabel
      Left = 226
      Top = 116
      Width = 40
      Height = 14
      HelpType = htKeyword
      Caption = 'LbNivel'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Senha: TLabel
      Left = 30
      Top = 70
      Width = 34
      Height = 14
      Caption = 'Senha'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label4: TLabel
      Left = 30
      Top = 93
      Width = 80
      Height = 14
      Caption = 'Repita a Senha'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 463
      Top = 16
      Width = 115
      Height = 14
      Anchors = [akRight]
      Caption = 'Somente no Sistema'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object EdtCodigo: TEdit
      Left = 131
      Top = 21
      Width = 90
      Height = 19
      MaxLength = 9
      TabOrder = 1
    end
    object EdtNome: TEdit
      Left = 131
      Top = 44
      Width = 298
      Height = 19
      CharCase = ecUpperCase
      MaxLength = 30
      TabOrder = 2
      OnKeyPress = EdtNomeKeyPress
    end
    object EdtSenha: TEdit
      Left = 131
      Top = 67
      Width = 298
      Height = 19
      MaxLength = 50
      PasswordChar = '#'
      TabOrder = 3
    end
    object Edtnivel: TEdit
      Left = 131
      Top = 113
      Width = 90
      Height = 19
      Color = 6073854
      TabOrder = 5
      OnKeyDown = EdtnivelKeyDown
    end
    object EdtSenha2: TEdit
      Left = 131
      Top = 90
      Width = 298
      Height = 19
      MaxLength = 50
      PasswordChar = '#'
      TabOrder = 4
    end
    object CheckSomenteSistema: TCheckBox
      Left = 593
      Top = 16
      Width = 13
      Height = 13
      Alignment = taLeftJustify
      Anchors = [akRight]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 213
    Width = 617
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 2
    DesignSize = (
      617
      50)
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 617
      Height = 50
      Align = alClient
      Stretch = True
    end
    object lbquantidade: TLabel
      Left = 319
      Top = 17
      Width = 290
      Height = 18
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = 'Existem X Tabelas A cadastradas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object BitBtn9: TBitBtn
      Left = 788
      Top = 6
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abr&ir pelo  C'#243'digo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
    object BitBtn10: TBitBtn
      Left = 788
      Top = 46
      Width = 148
      Height = 38
      Anchors = [akTop, akRight]
      Caption = 'Abrir pelo C'#243'digo de &Barras'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -9
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
end
