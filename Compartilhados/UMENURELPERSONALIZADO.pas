unit UMENURELPERSONALIZADO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjMENURELPERSONALIZADO;

type
  TFMENURELPERSONALIZADO = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbNome: TLabel;
    EdtNome: TEdit;
    LbOrdem: TLabel;
    EdtOrdem: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjMENURELPERSONALIZADO:TObjMENURELPERSONALIZADO;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FMENURELPERSONALIZADO: TFMENURELPERSONALIZADO;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFMENURELPERSONALIZADO.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjMENURELPERSONALIZADO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
        Submit_Ordem(edtOrdem.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFMENURELPERSONALIZADO.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjMENURELPERSONALIZADO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtOrdem.text:=Get_Ordem;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFMENURELPERSONALIZADO.TabelaParaControles: Boolean;
begin
     If (Self.ObjMENURELPERSONALIZADO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFMENURELPERSONALIZADO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjMENURELPERSONALIZADO=Nil)
     Then exit;

    If (Self.ObjMENURELPERSONALIZADO.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
    End;

    Self.ObjMENURELPERSONALIZADO.free;
end;

procedure TFMENURELPERSONALIZADO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFMENURELPERSONALIZADO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjMENURELPERSONALIZADO.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjMENURELPERSONALIZADO.status:=dsInsert;
     Guia.pageindex:=0;
     edtNome.setfocus;

end;


procedure TFMENURELPERSONALIZADO.btalterarClick(Sender: TObject);
begin
    If (Self.ObjMENURELPERSONALIZADO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjMENURELPERSONALIZADO.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNome.setfocus;
                
          End;


end;

procedure TFMENURELPERSONALIZADO.btgravarClick(Sender: TObject);
begin

     If Self.ObjMENURELPERSONALIZADO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjMENURELPERSONALIZADO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjMENURELPERSONALIZADO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFMENURELPERSONALIZADO.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjMENURELPERSONALIZADO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjMENURELPERSONALIZADO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjMENURELPERSONALIZADO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFMENURELPERSONALIZADO.btcancelarClick(Sender: TObject);
begin
     Self.ObjMENURELPERSONALIZADO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFMENURELPERSONALIZADO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFMENURELPERSONALIZADO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjMENURELPERSONALIZADO.Get_pesquisa,Self.ObjMENURELPERSONALIZADO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjMENURELPERSONALIZADO.status<>dsinactive
                                  then exit;

                                  If (Self.ObjMENURELPERSONALIZADO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjMENURELPERSONALIZADO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFMENURELPERSONALIZADO.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFMENURELPERSONALIZADO.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjMENURELPERSONALIZADO:=TObjMENURELPERSONALIZADO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjMENURELPERSONALIZADO.OBJETO.Get_Pesquisa,Self.ObjMENURELPERSONALIZADO.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjMENURELPERSONALIZADO.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjMENURELPERSONALIZADO.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjMENURELPERSONALIZADO.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
