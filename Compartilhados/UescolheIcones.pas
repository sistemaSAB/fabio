unit UescolheIcones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ComCtrls, Menus;

type
  TFescolheIcones = class(TForm)
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    IMGPADRAO: TImage;
    I11: TImage;
    I19: TImage;
    I27: TImage;
    I35: TImage;
    I08: TImage;
    I16: TImage;
    I24: TImage;
    I32: TImage;
    I01: TImage;
    I02: TImage;
    I10: TImage;
    I18: TImage;
    I26: TImage;
    I34: TImage;
    I09: TImage;
    I17: TImage;
    I25: TImage;
    I33: TImage;
    I07: TImage;
    I23: TImage;
    I31: TImage;
    I39: TImage;
    I38: TImage;
    I30: TImage;
    I22: TImage;
    I14: TImage;
    I21: TImage;
    I29: TImage;
    I06: TImage;
    I05: TImage;
    I13: TImage;
    I37: TImage;
    I04: TImage;
    I12: TImage;
    I20: TImage;
    I28: TImage;
    I36: TImage;
    I03: TImage;
    TabSheet5: TTabSheet;
    I15: TImage;
    I40: TImage;
    I42: TImage;
    I43: TImage;
    I44: TImage;
    I45: TImage;
    I41: TImage;
    PopupMenuIcone: TPopupMenu;
    MenuExcluirIcone: TMenuItem;
    AAAAA: TTabSheet;
    IMGPADRAO_3D: TImage;
    I18_3D: TImage;
    I11_3D: TImage;
    I19_3D: TImage;
    I27_3D: TImage;
    I35_3D: TImage;
    I01_3D: TImage;
    I02_3D: TImage;
    I10_3D: TImage;
    I26_3D: TImage;
    I34_3D: TImage;
    I09_3D: TImage;
    I17_3D: TImage;
    I25_3D: TImage;
    I33_3D: TImage;
    I07_3D: TImage;
    I23_3D: TImage;
    I31_3D: TImage;
    I39_3D: TImage;
    I38_3D: TImage;
    I30_3D: TImage;
    I22_3D: TImage;
    I14_3D: TImage;
    I21_3D: TImage;
    I29_3D: TImage;
    I06_3D: TImage;
    I05_3D: TImage;
    I13_3D: TImage;
    I37_3D: TImage;
    I04_3D: TImage;
    I12_3D: TImage;
    I20_3D: TImage;
    I28_3D: TImage;
    I36_3D: TImage;
    I03_3D: TImage;
    I15_3D: TImage;
    I32_3D: TImage;
    I24_3D: TImage;
    I16_3D: TImage;
    I08_3D: TImage;
    TabSheet1: TTabSheet;
    I40_3D: TImage;
    I42_3D: TImage;
    I43_3D: TImage;
    I44_3D: TImage;
    I45_3D: TImage;
    I41_3D: TImage;
    I46: TImage;
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
  private

    procedure DuploClick(Sender: TObject);
    procedure PassaEvento;
    { Private declarations }
  public
    { Public declarations }
    NOMEIMAGEM:String;
    procedure PegaFigura(Parametro: TPicture; Pnome:String);
  end;

var
  FescolheIcones: TFescolheIcones;

implementation

{$R *.dfm}

procedure TFescolheIcones.PegaFigura(Parametro: TPicture; Pnome:String);
var
  Cont:integer;
begin
     for cont:=0 to Self.ComponentCount -1
     do begin
          if (uppercase(Self.Components [cont].ClassName) = 'TIMAGE')
          then Begin
                    if (uppercase(TImage(Self.Components [cont]).name)=uppercase(Pnome))
                    Then Begin
                              Parametro.Bitmap:=TImage(Self.Components [cont]).Picture.Bitmap;
                              exit;
                    End;
          End;
     End;

     

     Pnome:='IMGPADRAO';
     Self.PegaFigura(Parametro,pnome);

     
end;


procedure TFescolheIcones.PassaEvento;
var
  Cont:integer;
begin
     for cont:=0 to Self.ComponentCount -1
     do begin
          if (uppercase(Self.Components [cont].ClassName) = 'TIMAGE')
          then Begin
                    TImage(Self.Components [cont]).OnDblClick:=Self.DuploClick;
          End;
     End;
end;


procedure TFescolheIcones.DuploClick(Sender: TObject);
begin
     Self.NOMEIMAGEM:=uppercase(Timage(sender).name);
     Self.Close;
end;

procedure TFescolheIcones.FormShow(Sender: TObject);
begin
     Self.NOMEIMAGEM:='';
     PageControl2.ActivePageIndex:=0;
end;

procedure TFescolheIcones.FormCreate(Sender: TObject);
begin
     Self.PassaEvento;
end;

procedure TFescolheIcones.PageControl2Change(Sender: TObject);
begin
     if  PageControl2.ActivePageIndex>1
     Then PageControl2.ActivePageIndex:=1;
end;

end.
