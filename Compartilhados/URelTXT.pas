unit URelTXT;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, ExtCtrls, Qrctrls,UDataModulo;

type
  TFrelTXT = class(TForm)
    QR: TQuickRep;
    SB: TQRStringsBand;
    BANDATITULO: TQRBand;
    QRBand4: TQRBand;
    lbnomesuperior: TQRLabel;
    LbNumPag: TQRSysData;
    LBTITULO: TQRLabel;
    bandatotaliza: TQRBand;
    LBTOTALIZA1: TQRLabel;
    LBTOTALIZA2: TQRLabel;
    LBTOTALIZA3: TQRLabel;
    LBTOTALIZA4: TQRLabel;
    LINHA2: TQRShape;
    LBSUBTITULO1: TQRLabel;
    LBSUBTITULO2: TQRLabel;
    LINHA1: TQRShape;
    QRBand1: TQRBand;
    linharodape: TQRShape;
    LBDADOS: TQRLabel;
    procedure LBDADOSPrint(sender: TObject; var Value: String);
    procedure bandatotalizaBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure LimpaCampos;
  end;

var
  FrelTXT: TFrelTXT;

implementation




{$R *.DFM}

procedure TFrelTXT.LimpaCampos;
begin

     LBTITULO.caption:='';
     LBTOTALIZA1.caption:='';
     LBTOTALIZA2.caption:='';
     LBTOTALIZA3.caption:='';
     LBTOTALIZA4.caption:='';
     LBTITULO.Enabled:=fALSE;
     LBSUBTITULO1.Enabled:=fALSE;
     LBSUBTITULO2.Enabled:=fALSE;
     LBTOTALIZA1.Enabled:=fALSE;
     LBTOTALIZA2.Enabled:=fALSE;
     LBTOTALIZA3.Enabled:=fALSE;
     LBTOTALIZA4.Enabled:=fALSE;


end;

procedure TFrelTXT.LBDADOSPrint(sender: TObject; var Value: String);
var
apoio:string;
posicao:Integer;
begin
     lbdados.font.Color:=clBLACK;
     LBDADOS.Font.Style:=[];
     LBDADOS.Font.size:=9;

     case sb.item[1] of
     '?':Begin//negritpo
              LBDADOS.Font.Style:=[fsbold];
              value:=copy(sb.Item,2,length(sb.item));
         end;
     '�':Begin//cor

              apoio:=copy(sb.Item,2,length(sb.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=sb.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN lbdados.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN lbdados.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN lbdados.font.Color:=clBlue
                                        ELSE lbdados.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(sb.Item,posicao+2,length(sb.item));
              End;
         End;
     Else Begin
                value:=Sb.item;

     End;

     end;



end;

procedure TFrelTXT.bandatotalizaBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
     If ((LBTOTALIZA1.enabled=False)and
     (LBTOTALIZA2.enabled=False)and
     (LBTOTALIZA3.enabled=False)and
     (LBTOTALIZA4.enabled=False))
     Then Begin
                //imprimo s� a linha
                PrintBand:=True;
                bandatotaliza.height:=10;
     End
     Else BEgin
                PrintBand:=True;
                bandatotaliza.height:=91;
     End;
end;

procedure TFrelTXT.QRBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
     lbnomesuperior.WIDTH:=BANDATITULO.WIDTH;
     LBTITULO.WIDTH:=BANDATITULO.WIDTH;
     LBDADOS.WIDTH:=BANDATITULO.WIDTH;
     LINHA1.WIDTH:=BANDATITULO.WIDTH;
     LINHA2.WIDTH:=BANDATITULO.WIDTH;
     linharodape.WIDTH:=BANDATITULO.WIDTH;
     LbNumPag.left:=BANDATITULO.WIDTH-20;
     lbnomesuperior.width:=BANDATITULO.width-30;


     {if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then LbNomeSuperior.caption:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else LbNomeSuperior.caption:=ObjParametroGlobal.get_valor;}

     lbnomesuperior.caption:=lbnomesuperior.caption+'  - '+datetostr(now)+'-'+timetostr(now);

end;

end.
