object FOpcaorel: TFOpcaorel
  Left = 45
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Op'#231#245'es - EXCLAIM TECNOLOGIA'
  ClientHeight = 537
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RgOpcoes: TRadioGroup
    Left = 2
    Top = 1
    Width = 580
    Height = 528
    TabOrder = 0
  end
  object Btrelatorios: TBitBtn
    Left = 585
    Top = 3
    Width = 95
    Height = 38
    Caption = '&Ok'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    TabOrder = 1
    OnClick = BtrelatoriosClick
  end
  object BtCancelar: TBitBtn
    Left = 585
    Top = 42
    Width = 95
    Height = 38
    Caption = '&Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Times New Roman'
    Font.Style = [fsBold, fsItalic]
    ParentFont = False
    TabOrder = 2
    OnClick = BtCancelarClick
  end
end
