object FparametrosAvulso: TFparametrosAvulso
  Left = 465
  Top = 180
  Width = 772
  Height = 551
  Caption = 'Cadastro de Par'#226'metros - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 756
    Height = 283
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object Label1: TLabel
        Left = 3
        Top = 10
        Width = 39
        Height = 14
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 3
        Top = 59
        Width = 110
        Height = 14
        Caption = 'Nome do Par'#226'metro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 3
        Top = 105
        Width = 91
        Height = 14
        Caption = 'Valor / Descri'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 3
        Top = 144
        Width = 39
        Height = 14
        Caption = 'Fun'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCodigo: TEdit
        Left = 3
        Top = 27
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object edtnome: TEdit
        Left = 3
        Top = 74
        Width = 751
        Height = 19
        MaxLength = 150
        TabOrder = 1
      end
      object edtvalor: TEdit
        Left = 3
        Top = 119
        Width = 751
        Height = 19
        MaxLength = 1500
        TabOrder = 2
      end
      object memofuncao: TMemo
        Left = 0
        Top = 165
        Width = 748
        Height = 90
        Align = alBottom
        Lines.Strings = (
          'memofuncao')
        TabOrder = 3
        OnKeyPress = memofuncaoKeyPress
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Todos'
      object StrGrid: TStringGrid
        Left = 0
        Top = 0
        Width = 826
        Height = 351
        ColCount = 3
        RowCount = 2
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
        ParentFont = False
        ScrollBars = ssNone
        TabOrder = 0
        ColWidths = (
          65
          359
          398)
      end
      object btCarregar: TBitBtn
        Left = 0
        Top = 356
        Width = 156
        Height = 39
        Caption = 'Carregar Parametros'
        TabOrder = 1
        OnClick = btCarregarClick
      end
      object btAtualizar: TBitBtn
        Left = 163
        Top = 356
        Width = 156
        Height = 39
        Caption = 'Atualizar Parametros'
        TabOrder = 2
        OnClick = btAtualizarClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 419
    Width = 756
    Height = 94
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object btcancelar: TBitBtn
      Left = 329
      Top = 8
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btsair: TBitBtn
      Left = 113
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btsairClick
    end
    object btgravar: TBitBtn
      Left = 221
      Top = 8
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btalterar: TBitBtn
      Left = 113
      Top = 8
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btexcluir: TBitBtn
      Left = 5
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object Btnovo: TBitBtn
      Left = 5
      Top = 8
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
  end
  object DBGrid: TDBGrid
    Left = 0
    Top = 283
    Width = 756
    Height = 136
    Align = alClient
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Verdana'
    TitleFont.Style = []
    OnDblClick = DBGridDblClick
    OnKeyPress = DBGridKeyPress
  end
end
