unit URELPERSREPORTBUILDER;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjRELPERSREPORTBUILDER,
  jpeg,UessencialGlobal, UessencialLocal,IbDatabase;

type
  TFRELPERSREPORTBUILDER = class(TForm)
    OpenDialog: TOpenDialog;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    btsair: TBitBtn;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    Panel1: TPanel;
    LbCodigo: TLabel;
    LbNome: TLabel;
    LbArquivo: TLabel;
    EdtCodigo: TEdit;
    EdtNome: TEdit;
    EdtArquivo: TEdit;
    btarquivo: TBitBtn;
    ImagemFundo: TImage;
    BitBtn1: TBitBtn;
    Guia: TTabbedNotebook;
    MemoSQLCabecalho: TMemo;
    MemoSQLRepeticao: TMemo;
    MemoSQLRepeticao_2: TMemo;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btConfigurarClick(Sender: TObject);
    procedure btarquivoClick(Sender: TObject);
    procedure btOpcoesClick(Sender: TObject);
    procedure CarregaDatabaseExterno(pDatabase:TIBDatabase);
    procedure BitBtn1Click(Sender: TObject);
  private
         ObjRELPERSREPORTBUILDER:TObjRELPERSREPORTBUILDER;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         function  atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
    pDatabaseExterna : TIBDatabase;//utilizado somente para alterar o database dentro do objeto quando for chamar o relatorio
  end;

var
  FRELPERSREPORTBUILDER: TFRELPERSREPORTBUILDER;


implementation

uses Upesquisa, UrelpersonalizadoReportBuilder, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFRELPERSREPORTBUILDER.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjRELPERSREPORTBUILDER do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_Nome(UpperCase(edtNome.text));
        Submit_SQLCabecalho(UpperCase(MemoSQLCabecalho.text));
        Submit_SQLRepeticao(UpperCase(MemoSQLRepeticao.text));
        Submit_SQLRepeticao_2(UpperCase(MemoSQLRepeticao_2.text));
        Submit_Arquivo(edtArquivo.text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFRELPERSREPORTBUILDER.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjRELPERSREPORTBUILDER do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtNome.text:=Get_Nome;
        MemoSQLCabecalho.text:=Get_SQLCabecalho;
        MemoSQLRepeticao.text:=Get_SQLRepeticao;
        MemoSQLRepeticao_2.text:=Get_SQLRepeticao_2;
        EdtArquivo.text:=Get_Arquivo;      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFRELPERSREPORTBUILDER.TabelaParaControles: Boolean;
begin
     If (ObjRELPERSREPORTBUILDER.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFRELPERSREPORTBUILDER.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     ObjRELPERSREPORTBUILDER.pDatabaseExterna := self.pDatabaseExterna;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
end;

procedure TFRELPERSREPORTBUILDER.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjRELPERSREPORTBUILDER=Nil)
     Then exit;

     If (ObjRELPERSREPORTBUILDER.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

     ObjRELPERSREPORTBUILDER.free;
end;

procedure TFRELPERSREPORTBUILDER.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFRELPERSREPORTBUILDER.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjRELPERSREPORTBUILDER.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;
     btarquivo.visible:=True;

     ObjRELPERSREPORTBUILDER.status:=dsInsert;
     EdtNOme.setfocus;

end;


procedure TFRELPERSREPORTBUILDER.btalterarClick(Sender: TObject);
begin
    If (ObjRELPERSREPORTBUILDER.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjRELPERSREPORTBUILDER.Status:=dsEdit;
                edtnoME.setfocus;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                btarquivo.visible:=True;
          End;


end;

procedure TFRELPERSREPORTBUILDER.btgravarClick(Sender: TObject);
begin

     If ObjRELPERSREPORTBUILDER.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjRELPERSREPORTBUILDER.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjRELPERSREPORTBUILDER.Get_codigo;
     mostra_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFRELPERSREPORTBUILDER.btexcluirClick(Sender: TObject);
begin
     If (ObjRELPERSREPORTBUILDER.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjRELPERSREPORTBUILDER.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjRELPERSREPORTBUILDER.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFRELPERSREPORTBUILDER.btcancelarClick(Sender: TObject);
begin
     ObjRELPERSREPORTBUILDER.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFRELPERSREPORTBUILDER.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFRELPERSREPORTBUILDER.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjRELPERSREPORTBUILDER.Get_pesquisa,ObjRELPERSREPORTBUILDER.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjRELPERSREPORTBUILDER.status<>dsinactive
                                  then exit;

                                  If (ObjRELPERSREPORTBUILDER.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjRELPERSREPORTBUILDER.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFRELPERSREPORTBUILDER.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFRELPERSREPORTBUILDER.FormShow(Sender: TObject);
begin
     Try
        ObjRELPERSREPORTBUILDER:=TObjRELPERSREPORTBUILDER.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     PegaCorForm(Self);
     Guia.PageIndex:=0;
end;

procedure TFRELPERSREPORTBUILDER.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;
procedure TFRELPERSREPORTBUILDER.btConfigurarClick(Sender: TObject);
begin
     FrelpersonalizadoReportBuilder.PassaObjeto(Self.ObjRELPERSREPORTBUILDER);
     FrelpersonalizadoReportBuilder.ShowModal;
end;

procedure TFRELPERSREPORTBUILDER.btarquivoClick(Sender: TObject);
begin
     if (Self.ObjRELPERSREPORTBUILDER.Status=dsinactive)
     then exit;

     if (OpenDialog.Execute=False)
     Then exit;

     EdtArquivo.Text:=ExtractFileName(OpenDialog.FileName);
end;

function TFRELPERSREPORTBUILDER.atualizaQuantidade: string;
begin
//     result:='Existe(m) '+ContaRegistros('tabrelpersreportbuilder','codigo')+' Relat�rio(s) Cadastrados';
end;

procedure TFRELPERSREPORTBUILDER.btOpcoesClick(Sender: TObject);
begin
    If(EdtCodigo.Text = '')
    Then Begin
          MensagemErro('Escolha um relat�rio para Configurar');
          Exit;
    End;
    ObjRELPERSREPORTBUILDER.Opcoes(EdtCodigo.Text);
end;

procedure TFRELPERSREPORTBUILDER.CarregaDatabaseExterno(
  pDatabase: TIBDatabase);
begin
      pDatabaseExterna := pDatabase;
end;

procedure TFRELPERSREPORTBUILDER.BitBtn1Click(Sender: TObject);
begin
    if (EdtCodigo.Text = '')
    then exit;

    ObjRELPERSREPORTBUILDER.Configurar(EdtCodigo.Text);
end;

end.

