unit UfrScriptSql_MYSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, IBScript, DB, IBCustomDataSet, StdCtrls, Grids, DBGrids, Buttons,
  Menus, ExtCtrls, ComCtrls, IBDatabase, FileCtrl, SynEdit, SynMemo,
  SynEditHighlighter, SynHighlighterSQL, SynEditMiscClasses, SynEditSearch,
  SynEditRegexSearch, ZConnection, ZAbstractRODataset, ZAbstractDataset,
  ZDataset;
type
  TFrScriptSql_MYSQL = class(TFrame)
    DBGridScript: TDBGrid;
    DsScript: TDataSource;
    SaveDialogScript: TSaveDialog;
    PanelBotoes: TPanel;
    BtExecutaScript: TBitBtn;
    LbScript: TListBox;
    FileListBox: TFileListBox;
    DirectoryListBox: TDirectoryListBox;
    DriveComboBox: TDriveComboBox;
    btexecutasqllote: TBitBtn;
    Memoerro: TMemo;
    Label1: TLabel;
    btexecutasqlpasta: TBitBtn;

    Button1: TButton;
    Button2: TButton;
    Splitter1: TSplitter;
    SynSQLSyn1: TSynSQLSyn;
    SynEditSearch: TSynEditSearch;
    SynEditRegexSearch: TSynEditRegexSearch;
    memo_script: TSynEdit;
    lblocalizar: TLabel;
    lblocalizaresubstituir: TLabel;
    Label2: TLabel;
    memoanotacoes: TMemo;
    StatusBar1: TStatusBar;
    ZConnectionScript: TZConnection;
    ZQueryScript: TZQuery;
    procedure BtExecutaScriptClick(Sender: TObject);
    procedure MemoScriptKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RichEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LbScriptDblClick(Sender: TObject);
    procedure btexecutasqlloteClick(Sender: TObject);
    procedure FileListBoxDblClick(Sender: TObject);
    procedure DirectoryListBoxChange(Sender: TObject);
    procedure btexecutasqlpastaClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure lblocalizarClick(Sender: TObject);
    procedure lblocalizaresubstituirClick(Sender: TObject);
    procedure MemoerroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure memo_scriptReplaceText(Sender: TObject; const ASearch,
      AReplace: String; Line, Column: Integer;
      var Action: TSynReplaceAction);
  private
    { Private declarations }
    fSearchFromCaret: boolean;
    Function ExecutaCOmando:boolean;
    Function ExecutaSqlPastaAtual(primeiroarquivo:integer):boolean;
    procedure ShowSearchReplaceDialog(AReplace: boolean);
    procedure DoSearchReplaceText(AReplace, ABackwards: boolean);


  public
    { Public declarations }
    Function InicializaFrame(phost,Pbanco,Pusuario,Psenha:String;var Pmensagem:String):boolean;
  end;

implementation

uses dlgSearchText, dlgConfirmReplace, dlgReplaceText,SynEditTypes, SynEditMiscProcs;
{$R *.dfm}

var
  gbSearchBackwards: boolean;
  gbSearchCaseSensitive: boolean;
  gbSearchFromCaret: boolean;
  gbSearchSelectionOnly: boolean;
  gbSearchTextAtCaret: boolean;
  gbSearchWholeWords: boolean;
  gbSearchRegex: boolean;

  gsSearchText: string;
  gsSearchTextHistory: string;
  gsReplaceText: string;
  gsReplaceTextHistory: string;

resourcestring
  STextNotFound = 'Texto n�o encontrado';
  SNoSelectionAvailable = 'Sele��o inv�lida, procure outro texto?';


procedure TFrScriptSql_MYSQL.BtExecutaScriptClick(Sender: TObject);
begin
     Self.ExecutaCOmando;
End;



Function TFrScriptSql_MYSQL.InicializaFrame(phost,Pbanco,Pusuario,Psenha:String;var Pmensagem:String):boolean;
begin
     result:=False;

     try
       pmensagem:='';

       Self.ZConnectionScript.Disconnect;
       Self.ZConnectionScript.User:=pusuario;
       Self.ZConnectionScript.Password:=psenha;
       Self.ZConnectionScript.Database:=pbanco;
       Self.ZConnectionScript.HostName:=phost;
       Self.ZConnectionScript.Connect;
       Self.ZQueryScript.Connection:=Self.ZConnectionScript;
       pmensagem:='Conex�o Local com Sucesso';
     Except
          on e:exception do
          begin
               pmensagem:=E.message;
               exit;
          End;
     End;

     (*ibDATASETScript.database:=Self.Ibdatabase;
     IBScript.Dataset:=IBDataSetScript;
     IBScript.Database:=Self.Ibdatabase;
     IBScript.Transaction:=Self.IBTransaction;*)

     Memo_Script.lines.clear;
     LbScript.Items.clear;
     memoerro.lines.clear;
     memoanotacoes.lines.clear;
     result:=True;
end;

procedure TFrScriptSql_MYSQL.MemoScriptKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (ssctrl in shift) and (key=ord('P'))//Ctrl P(prior=anterior)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex>0)
                       Then LbScript.ItemIndex:=LbScript.ItemIndex-1
                       Else Begin
                                 if (LbScript.ItemIndex=-1)
                                 Then LbScript.ItemIndex:=LbScript.items.count-1;
                       End;

                       Memo_Script.Lines.Clear;
                       Memo_Script.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=ord('N'))//Ctrl N  (next=proximo)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex<(LbScript.Items.count-1))
                       Then LbScript.ItemIndex:=LbScript.ItemIndex+1;

                       Memo_Script.Lines.Clear;
                       Memo_Script.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=13)//Ctrl ENTER(executa)
     Then Begin
               BtExecutaScriptClick(sender);
     end;

     if  (ssctrl in shift)
     then Begin
               if (key=ord('F')) or (key=ord('f'))
               Then lblocalizarClick(sender);

               if (key=ord('r')) or (key=ord('R'))
               Then lblocalizaresubstituirClick(sender);

               if (key=ord('s')) or (key=ord('S'))
               Then Begin
                         if (SaveDialogScript.Execute=True)
                         then Memo_Script.Lines.SaveToFile(SaveDialogScript.FileName+'.sql');
               End;
     End;


     if (key=vk_f3)//procura o proximo
     Then DoSearchReplaceText(FALSE, FALSE);

     if (key=13)
     Then memo_script.setfocus;

end;


procedure TFrScriptSql_MYSQL.RichEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (ssctrl in shift) and (key=ord('P'))//Ctrl P(prior=anterior)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex>0)
                       Then LbScript.ItemIndex:=LbScript.ItemIndex-1
                       Else Begin
                                 if (LbScript.ItemIndex=-1)
                                 Then LbScript.ItemIndex:=LbScript.items.count-1;
                       End;

                       Memo_Script.Lines.Clear;
                       Memo_Script.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=ord('N'))//Ctrl N  (next=proximo)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex<(LbScript.Items.count-1))
                       Then LbScript.ItemIndex:=LbScript.ItemIndex+1;

                       Memo_Script.Lines.Clear;
                       Memo_Script.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=13)//Ctrl ENTER(executa)
     Then Begin
               BtExecutaScriptClick(sender);
     end;
end;

procedure TFrScriptSql_MYSQL.LbScriptDblClick(Sender: TObject);
begin
     if (LbScript.Items.Count>0)
     Then Memo_Script.Lines.text:=LbScript.Items[LbScript.Itemindex];
end;

procedure TFrScriptSql_MYSQL.btexecutasqlloteClick(Sender: TObject);
var
praiz:string;
arquivos,cont:integer;
diretorioatual:string;
begin

     //preciso percorrer todas as subpastas da pasta atual
     //e para pasta executar os SQLS

     Praiz:=DirectoryListBox.Directory;

     for cont:=DirectoryListBox.Itemindex to DirectoryListBox.Items.count -1 do
     Begin
          Application.ProcessMessages;
          DirectoryListBox.ItemIndex:=cont;
          FileListBox.Drive:=DriveComboBox.drive;
          diretorioatual:=Praiz+'\'+DirectoryListBox.items[cont];
          try
            FileListBox.Directory:=diretorioatual;
          Except
          End;
          Application.ProcessMessages;
          //executa os sqls da pasta atual a partir do primeiro arquivo
          if (Self.ExecutaSqlPastaAtual(0)=False)
          then exit;
     End;

     Showmessage('Conclu�do');
end;

function TFrScriptSql_MYSQL.ExecutaCOmando: boolean;
var
Pultimocomando:string;
Begin
     result:=False;

     //IBScript.Terminator:=';';
     //IBScript.Script:=Memo_Script.Lines;

     try
        //IBScript.ExecuteScript;
        Self.ZQueryScript.close;
        Self.ZQueryScript.SQL.clear;
        Self.ZQueryScript.SQL:=memo_script.Lines;
        Self.ZQueryScript.Open;
        result:=True;

        PultimoComando:='';
        if (LbScript.Items.count>0)
        then PultimoComando:=LbScript.Items[LbScript.items.count-1];

        if (uppercase(Pultimocomando)<>uppercase(Memo_Script.Lines.text))
        Then Begin
                  LbScript.Items.add(Memo_Script.Lines.text);
                  LbScript.Itemindex:=-1;
        End;
        Memo_Script.Lines.clear;
     Except
           on e:exception do
           begin
                Messagedlg(E.message,mterror,[mbok],0);
                memoerro.lines.add(e.message);
           End;
     End;
end;

procedure TFrScriptSql_MYSQL.FileListBoxDblClick(Sender: TObject);
begin
     Memo_Script.Lines.clear;
     Memo_Script.Lines.LoadFromFile(FileListBox.FileName);
end;

procedure TFrScriptSql_MYSQL.DirectoryListBoxChange(Sender: TObject);
begin
   FileListBox.Directory:=DirectoryListBox.Directory;
end;

procedure TFrScriptSql_MYSQL.btexecutasqlpastaClick(Sender: TObject);
begin
      //executa os sqls da pasta atual a partir do arquivo que estiver selecionado
      Self.ExecutaSqlPastaAtual(FileListBox.itemindex);
end;

function TFrScriptSql_MYSQL.ExecutaSqlPastaAtual(primeiroarquivo:integer): boolean;
var
arquivos:integer;
begin
     result:=False;
     for arquivos:=primeiroarquivo to FileListBox.items.count-1 do
     Begin
          Application.ProcessMessages;
          FileListBox.ItemIndex:=arquivos;
          Memo_Script.Lines.clear;
          Memo_Script.Lines.LoadFromFile(FileListBox.FileName);
          Try
              if (self.ExecutaCOmando=False)
              then exit;
          Except
          End;
     End;
     result:=true;
end;

procedure TFrScriptSql_MYSQL.Button1Click(Sender: TObject);
begin
     //if (Self.IBTransaction.Active=true)
     //then Self.IBTransaction.Commit;
end;

procedure TFrScriptSql_MYSQL.Button2Click(Sender: TObject);
begin
     //if (Self.IBTransaction.Active=true)
     //then Self.IBTransaction.Rollback;
end;

procedure TFrScriptSql_MYSQL.ShowSearchReplaceDialog(AReplace: boolean);
var
  dlg: TTextSearchDialog;
begin
  //Statusbar.SimpleText := '';
  if AReplace then
    dlg := TTextReplaceDialog.Create(Self)
  else
    dlg := TTextSearchDialog.Create(Self);
  with dlg do try
    // assign search options
    SearchBackwards := gbSearchBackwards;
    SearchCaseSensitive := gbSearchCaseSensitive;
    SearchFromCursor := gbSearchFromCaret;
    SearchInSelectionOnly := gbSearchSelectionOnly;
    // start with last search text
    SearchText := gsSearchText;
    if gbSearchTextAtCaret then begin
      // if something is selected search for that text
      if memo_script.SelAvail and (memo_script.BlockBegin.Line = memo_script.BlockEnd.Line)
      then
        SearchText := Memo_Script.SelText
      else
        SearchText := Memo_Script.GetWordAtRowCol(Memo_Script.CaretXY);
    end;
    SearchTextHistory := gsSearchTextHistory;
    if AReplace then with dlg as TTextReplaceDialog do begin
      ReplaceText := gsReplaceText;
      ReplaceTextHistory := gsReplaceTextHistory;
    end;
    SearchWholeWords := gbSearchWholeWords;
    if ShowModal = mrOK then begin
      gbSearchSelectionOnly := SearchInSelectionOnly;
      gbSearchBackwards := SearchBackwards;
      gbSearchCaseSensitive := SearchCaseSensitive;
      gbSearchFromCaret := SearchFromCursor;
      gbSearchWholeWords := SearchWholeWords;
      gbSearchRegex := SearchRegularExpression;
      gsSearchText := SearchText;
      gsSearchTextHistory := SearchTextHistory;
      if AReplace then with dlg as TTextReplaceDialog do begin
        gsReplaceText := ReplaceText;
        gsReplaceTextHistory := ReplaceTextHistory;
      end;
      fSearchFromCaret := gbSearchFromCaret;
      if gsSearchText <> '' then begin
        DoSearchReplaceText(AReplace, gbSearchBackwards);
        fSearchFromCaret := TRUE;
      end;
    end;
  finally
    dlg.Free;
  end;
end;

procedure TFrScriptSql_MYSQL.DoSearchReplaceText(AReplace: boolean;
  ABackwards: boolean);
var
  Options: TSynSearchOptions;
begin
  //Statusbar.SimpleText := '';
  if AReplace then
    Options := [ssoPrompt, ssoReplace, ssoReplaceAll]
  else
    Options := [];
  if ABackwards then
    Include(Options, ssoBackwards);
  if gbSearchCaseSensitive then
    Include(Options, ssoMatchCase);
  if not fSearchFromCaret then
    Include(Options, ssoEntireScope);
  if gbSearchSelectionOnly then
  begin
    if (not memo_script.SelAvail) or SameText(memo_script.SelText, gsSearchText) then
    begin
      if MessageDlg(SNoSelectionAvailable, mtWarning, [mbYes, mbNo], 0) = mrYes then
        gbSearchSelectionOnly := False
      else
        Exit;
    end
    else
      Include(Options, ssoSelectedOnly);
  end;
  if gbSearchWholeWords
  then Include(Options, ssoWholeWord);

  if gbSearchRegex
  then
    memo_script.SearchEngine := SynEditRegexSearch
  else memo_script.SearchEngine := SynEditSearch;

  
  if memo_script.SearchReplace(gsSearchText, gsReplaceText, Options) = 0 then
  begin
    MessageBeep(MB_ICONASTERISK);

    //memo_script.SimpleText := STextNotFound;

    
    if ssoBackwards in Options then
      memo_script.BlockEnd := memo_script.BlockBegin
    else
      memo_script.BlockBegin := memo_script.BlockEnd;
    memo_script.CaretXY := memo_script.BlockBegin;
  end;

  if ConfirmReplaceDialog <> nil then
    ConfirmReplaceDialog.Free;
end;
procedure TFrScriptSql_MYSQL.lblocalizarClick(Sender: TObject);
begin
    ShowSearchReplaceDialog(FALSE);
end;

procedure TFrScriptSql_MYSQL.lblocalizaresubstituirClick(Sender: TObject);
begin
   ShowSearchReplaceDialog(TRUE);
end;

procedure TFrScriptSql_MYSQL.MemoerroKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     //key:=ord(uppercase(key));
     
     if (ssctrl  in shift)
     Then Begin
               if (key=ord('s')) or (key=ord('S'))
               Then Begin
                          if (SaveDialogScript.execute)
                          Then Tmemo(sender).lines.savetofile(SaveDialogScript.filename);
               End;
     End;
end;

procedure TFrScriptSql_MYSQL.memo_scriptReplaceText(Sender: TObject;
  const ASearch, AReplace: String; Line, Column: Integer;
  var Action: TSynReplaceAction);

var
  APos: TPoint;
  EditRect: TRect;
begin
  if ASearch = AReplace then
    Action := raSkip
  else begin
    APos := memo_script.ClientToScreen(
      memo_script.RowColumnToPixels(
      memo_script.BufferToDisplayPos(
        BufferCoord(Column, Line) ) ) );
    EditRect := ClientRect;
    EditRect.TopLeft := ClientToScreen(EditRect.TopLeft);
    EditRect.BottomRight := ClientToScreen(EditRect.BottomRight);

    if ConfirmReplaceDialog = nil then
      ConfirmReplaceDialog := TConfirmReplaceDialog.Create(Application);
    ConfirmReplaceDialog.PrepareShow(EditRect, APos.X, APos.Y,
      APos.Y + memo_script.LineHeight, ASearch);
    case ConfirmReplaceDialog.ShowModal of
      mrYes: Action := raReplace;
      mrYesToAll: Action := raReplaceAll;
      mrNo: Action := raSkip;
      else Action := raCancel;
    end;
  end;
end;


end.
