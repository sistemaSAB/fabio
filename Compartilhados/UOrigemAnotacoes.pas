unit UOrigemAnotacoes;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjOrigemAnotacoes;

type
  TFOrigemAnotacoes = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    EdtCodigo: TEdit;
    edtnome: TEdit;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    BtNovo: TBitBtn;
    btalterar: TBitBtn;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         Function  AtualizaQuantidade: string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  FOrigemAnotacoes: TFOrigemAnotacoes;
  ObjOrigemAnotacoes:TObjOrigemAnotacoes;

implementation

uses Uessencialglobal, Upesquisa, UescolheImagemBotao;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFOrigemAnotacoes.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjOrigemAnotacoes do
    Begin
         Submit_CODIGO          ( edtCODIGO.text);
         Submit_Nome            ( edtNome.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFOrigemAnotacoes.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjOrigemAnotacoes do
     Begin
        edtCODIGO.text          :=Get_CODIGO          ;
        edtNome.text            :=Get_Nome            ;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFOrigemAnotacoes.TabelaParaControles: Boolean;
begin
     ObjOrigemAnotacoes.TabelaparaObjeto;
     ObjetoParaControles;
end;



//****************************************
//****************************************



procedure TFOrigemAnotacoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjOrigemAnotacoes=Nil)
     Then exit;

    If (ObjOrigemAnotacoes.status<>dsinactive)
    Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjOrigemAnotacoes.free;
end;

procedure TFOrigemAnotacoes.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFOrigemAnotacoes.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;


procedure TFOrigemAnotacoes.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjOrigemAnotacoes.Get_novocodigo;
     edtcodigo.enabled:=False;
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     ObjOrigemAnotacoes.status:=dsInsert;
     Edtnome.setfocus;
end;

procedure TFOrigemAnotacoes.BtCancelarClick(Sender: TObject);
begin
     ObjOrigemAnotacoes.cancelar;
     
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFOrigemAnotacoes.BtgravarClick(Sender: TObject);
begin

     If ObjOrigemAnotacoes.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjOrigemAnotacoes.salvar(true)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0); 

end;

procedure TFOrigemAnotacoes.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     EdtCodigo.enabled:=False;
     ObjOrigemAnotacoes.Status:=dsEdit;
     edtNome.setfocus;
end;

procedure TFOrigemAnotacoes.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjOrigemAnotacoes.Get_pesquisa,ObjOrigemAnotacoes.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjOrigemAnotacoes.status<>dsinactive
                                  then exit;

                                  If (ObjOrigemAnotacoes.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFOrigemAnotacoes.btalterarClick(Sender: TObject);
begin
    If (ObjOrigemAnotacoes.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFOrigemAnotacoes.btexcluirClick(Sender: TObject);
begin
     If (ObjOrigemAnotacoes.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjOrigemAnotacoes.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjOrigemAnotacoes.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;


procedure TFOrigemAnotacoes.FormShow(Sender: TObject);
begin
     UessencialGlobal.PegaCorForm(Self);

     limpaedit(Self);
     desabilita_campos(Self);

     Try
        ObjOrigemAnotacoes:=TObjOrigemAnotacoes.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;
     lbquantidade.Caption:=AtualizaQuantidade;
end;

function TFOrigemAnotacoes.AtualizaQuantidade: string;
begin
     result:='Existem '+ ContaRegistros('TABORIGEMANOTACOES','codigo') + ' Origens Cadastradas';
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Obj.Get_PesquisaOrigemAnotacoes,Obj.Get_TituloPesquisaOrigemAnotacoes,FOrigemAnotacoes)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If Self.edtTipoLancto.text<>''
                                 Then Self.LbTipoLanctoNome.caption:=OBjLancamento.PegaNometipolancto(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;
}
