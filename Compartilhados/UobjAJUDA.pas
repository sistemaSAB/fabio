unit UobjAJUDA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc , ComCtrls
;
//USES_INTERFACE


Type
   TObjAJUDA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);
                procedure BotaoOpcoes(pcodigo: string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_NOMEMENU(parametro: string);
                Function Get_NOMEMENU: string;
                Procedure Submit_LIGADOMENU(parametro: string);
                Function Get_LIGADOMENU: string;
                Procedure Submit_AJUDA(mmoAjuda: TRichEdit);
                Function Get_AJUDA(var mmoAjuda: TRichEdit):Boolean;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               MS:TMemoryStream;

               CODIGO:string;
               NOMEMENU:string;
               LIGADOMENU:string;
               AJUDA:string;
//CODIFICA VARIAVEIS PRIVADAS










               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                procedure AtualizarAjuda;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjAJUDA.TabelaparaObjeto:Boolean;//ok
Var
   Temp:TStream;
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.NOMEMENU:=fieldbyname('NOMEMENU').asstring;
        Self.LIGADOMENU:=fieldbyname('LIGADOMENU').asstring;
        //Self.AJUDA:=fieldbyname('AJUDA').asstring;
        try
           try
              Temp:=CreateBlobStream(FieldByName('AJUDA'),bmRead);
              self.MS.LoadFromStream(Temp);
           Except
                 exit;
           End;
        Finally
               freeandnil(temp);
        End;

        result:=True;
     End;
end;


Procedure TObjAJUDA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NOMEMENU').asstring:=Self.NOMEMENU;
        ParamByName('LIGADOMENU').asstring:=Self.LIGADOMENU;
        ParamByName('AJUDA').LoadFromStream(MS,ftBlob);
  End;
End;

//***********************************************************************

function TObjAJUDA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

 { if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;
                 }

   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjAJUDA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NOMEMENU:='';
        LIGADOMENU:='';
        //AJUDA:='';
        MS.Clear;
//CODIFICA ZERARTABELA

     End;
end;

Function TObjAJUDA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjAJUDA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjAJUDA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjAJUDA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';

//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjAJUDA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjAJUDA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro AJUDA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NOMEMENU,LIGADOMENU,USERM,USERC,DATAC,DATAM,AJUDA');
           SQL.ADD(' ');
           SQL.ADD(' from  TABAJUDA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjAJUDA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjAJUDA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjAJUDA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjAJUDA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        MS:=TMemoryStream.Create;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABAJUDA(CODIGO,NOMEMENU,LIGADOMENU,USERM');
                InsertSQL.add(' ,USERC,DATAC,DATAM,AJUDA)');
                InsertSQL.add('values (:CODIGO,:NOMEMENU,:LIGADOMENU,:USERM,:USERC');
                InsertSQL.add(' ,:DATAC,:DATAM,:AJUDA)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABAJUDA set CODIGO=:CODIGO,NOMEMENU=:NOMEMENU');
                ModifySQL.add(',LIGADOMENU=:LIGADOMENU,USERM=:USERM,USERC=:USERC,DATAC=:DATAC');
                ModifySQL.add(',DATAM=:DATAM,AJUDA=:AJUDA');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABAJUDA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjAJUDA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjAJUDA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabAJUDA');
     Result:=Self.ParametroPesquisa;
end;

function TObjAJUDA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de AJUDA ';
end;


function TObjAJUDA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENAJUDA,1) CODIGO FROM RDB$DATABASE');
           //CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

           //IbqueryGen.sql.add('SELECT GEN_ID(GENAJUDA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjAJUDA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    MS.Free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjAJUDA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjAJUDA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjAJUDA.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjAJUDA.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjAJUDA.Submit_NOMEMENU(parametro: string);
begin
        Self.NOMEMENU:=Parametro;
end;
function TObjAJUDA.Get_NOMEMENU: string;
begin
        Result:=Self.NOMEMENU;
end;
procedure TObjAJUDA.Submit_LIGADOMENU(parametro: string);
begin
        Self.LIGADOMENU:=Parametro;
end;
function TObjAJUDA.Get_LIGADOMENU: string;
begin
        Result:=Self.LIGADOMENU;
end;
procedure TObjAJUDA.Submit_AJUDA(mmoAjuda: TRichEdit);
begin
       MS.Clear;
       mmoAjuda.Lines.SaveToStream(MS);
end;
function TObjAJUDA.Get_AJUDA(var mmoAjuda: TRichEdit):Boolean;
begin
       Result:=True;
       mmoAjuda.Lines.Clear;
       mmoAjuda.Lines.LoadFromStream(MS);

end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjAJUDA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJAJUDA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;

procedure TObjAJUDA.BotaoOpcoes(pcodigo: string);
begin
       With FOpcaorel do
       Begin
            RgOpcoes.Items.clear;
            RgOpcoes.Items.add('Atualizar Ajuda');


            showmodal;
            if (tag=0)
            Then exit;
            Case RgOpcoes.ItemIndex of
                 0:Self.AtualizarAjuda;
            End;

       end;
end;

procedure TObjAJUDA.AtualizarAjuda;
var
   Query:TIBQuery;
begin
{C:\Programme\Firebird2\bin>isql -q -i c:\Scripts\CreateScript.sql

WinExec()     }

//WinExec(PChar('cmd dir C:\Program Files (x86)\Firebird\bin>isql -q -i D:\Atualiza��es\Ajuda SAB\Vers�o 1.0.0.0\Ajuda Vers�o 1.0.0.0.txt'),sw_normal);
//WinExec(PChar('cmd dir C:\Program Files (x86)\Firebird\bin>isql -q -i'),sw_normal);

end;


end.



