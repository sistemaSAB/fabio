object FRpesquisaGrid: TFRpesquisaGrid
  Left = 0
  Top = 0
  Width = 695
  Height = 321
  TabOrder = 0
  object DBGridPesquisa: TDBGrid
    Left = 0
    Top = 0
    Width = 695
    Height = 292
    Align = alClient
    DataSource = DsPesquisa
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyDown = DBGridPesquisaKeyDown
    OnKeyPress = DBGridPesquisaKeyPress
  end
  object Panel1: TPanel
    Left = 0
    Top = 292
    Width = 695
    Height = 29
    Align = alBottom
    TabOrder = 1
    object edtbusca: TMaskEdit
      Left = 4
      Top = 6
      Width = 241
      Height = 19
      CharCase = ecUpperCase
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 0
      Text = 'EDTBUSCA'
      OnExit = edtbuscaExit
      OnKeyPress = edtbuscaKeyPress
    end
  end
  object QueryPesquisa: TIBQuery
    Left = 632
    Top = 16
  end
  object DsPesquisa: TDataSource
    DataSet = QueryPesquisa
    Left = 600
    Top = 16
  end
end
