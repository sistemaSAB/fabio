unit UObjacessos;
Interface
Uses windows,stdctrls,Classes,Db,UessencialGlobal,Ibcustomdataset,IBStoredProc
//USES
;

Type
   TObjacessos=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;


                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_DataHora(parametro: string);
                Function Get_DataHora: string;
                //CODIFICA DECLARA GETSESUBMITS
                function VerificaUltimoAcesso: Boolean;

                
         Private
               ObjDataset:Tibdataset;
               CODIGO:string;
               DataHora:string;
//CODIFICA VARIAVEIS PRIVADAS


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
    function EncriptaDecripta(original: string): string;

   End;


implementation
uses SysUtils,Dialogs,Udatamodulo,Ibquery,Controls
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjacessos.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.DataHora:=fieldbyname('DataHora').asstring;
//CODIFICA TABELAPARAOBJETO


        result:=True;
     End;
end;


Procedure TObjacessos.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('DataHora').asstring:=Self.DataHora;
//CODIFICA OBJETOPARATABELA


  End;
End;

//***********************************************************************

function TObjacessos.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




 if Self.status=dsinsert
 then  Self.ObjDataset.Insert//libera para insercao
 Else  Self.ObjDataset.edit;//se for edicao libera para tal
 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjacessos.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        DataHora:='';
//CODIFICA ZERARTABELA


     End;
end;

Function TObjacessos.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      If (DataHora='')
      Then Mensagem:=mensagem+'/DataHora';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjacessos.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjacessos.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjacessos.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjacessos.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjacessos.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,DataHora');
           SelectSQL.ADD(' from  TABACESSOS');
           SelectSQL.ADD(' WHERE CODIGO='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjacessos.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjacessos.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjacessos.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

//CODIFICA CRIACAO DE OBJETOS

        ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select CODIGO,DataHora');
                SelectSQL.ADD(' from  TABACESSOS');
                SelectSQL.ADD(' WHERE CODIGO=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TABACESSOS(CODIGO,DataHora)');
                InsertSQL.add('values (:CODIGO,:DataHora)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABACESSOS set CODIGO=:CODIGO,DataHora=:DataHora');
                ModifySQL.add('');
                ModifySQL.add('where CODIGO=:CODIGO');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABACESSOS where CODIGO=:CODIGO ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,DataHora');
                RefreshSQL.ADD(' from  TABACESSOS');
                RefreshSQL.ADD(' WHERE CODIGO=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjacessos.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjacessos.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from Tabacessos');
     Result:=Self.ParametroPesquisa;
end;

function TObjacessos.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de acessos ';
end;




destructor TObjacessos.Free;
begin
Freeandnil(Self.ObjDataset);
Freeandnil(Self.ParametroPesquisa);
//CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjacessos.RetornaCampoCodigo: string;
begin
      result:='CODIGO';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjacessos.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TOBJACESSOS.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TOBJACESSOS.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TOBJACESSOS.Submit_DataHora(parametro: string);
begin
        Self.DataHora:=Parametro;
end;
function TOBJACESSOS.Get_DataHora: string;
begin
        Result:=Self.DataHora;
end;
//CODIFICA GETSESUBMITS

//CODIFICA EXITONKEYDOWN
Function TobjAcessos.VerificaUltimoAcesso:Boolean;
var
ultimo_valor:string;
Begin
     result:=True;//ok pode passar
     
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select max(codigo) as codigo from TabAcessos');
          open;
          if (recordcount=0)
          Then exit;
          self.LocalizaCodigo(fieldbyname('codigo').asstring);
          Self.TabelaparaObjeto;//tenho o ultimo acesso
          //aqui vai a desenc.
          ultimo_valor:=EncriptaDecripta(ultimo_valor);
          //********************************
          //posso ter no maximo 10 minutos de atraso em relacao a ultima maquina que acessou

          If (strtodate(datetostr(now))<strtodate(ultimo_valor))//se a data for menor naum deixa passar
          Then Begin
                    result:=False;
                    exit;
          End
          Else Begin
                    If (strtodate(datetostr(now))=strtodate(ultimo_valor))//se for o mesmo dia
                    Then Begin
                              result:=False;
                              exit;
                              //continuar depois

                    End
                    Else Begin
                              //dia maior, deixa passar
                              result:=True;
                              exit;
                    End;

          End;


          if ((Date+(time+10))<=strtodatetime(Ultimo_valor))//verifico se � menor a ultima data
          Then result:=false
          Else result:=true;

     End;

End;

Function TobjAcessos.EncriptaDecripta(original:string):string;
var
final:string;
cont:integer;
palavra:string;
begin
     palavra:='';
     palavra:=original;


     final:='';
     for cont:=1 to length(palavra) do
     begin
	final:=final+chr(256-ord(copy(palavra,cont,1)[1]));
     end;
     result:=final;

end;



end.


