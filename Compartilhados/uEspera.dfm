object fEspera: TfEspera
  Left = 521
  Top = 373
  BorderStyle = bsNone
  Caption = 'Aguarde'
  ClientHeight = 87
  ClientWidth = 377
  Color = 14024703
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lbMensagem: TLabel
    Left = 0
    Top = 24
    Width = 377
    Height = 30
    Alignment = taCenter
    AutoSize = False
    Caption = 'Aguarde...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -21
    Font.Name = 'Arial Black'
    Font.Style = [fsBold]
    ParentFont = False
  end
end
