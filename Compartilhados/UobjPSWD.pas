unit UObjPSWD;
Interface
Uses windows,stdctrls,Classes,Db,Uessencialglobal,Ibcustomdataset,IBStoredProc
//USES
;

Type
   TObjPSWD=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nivel1(parametro: string);
                Function Get_Nivel1: string;
                Procedure Submit_Nivel2(parametro: string);
                Function Get_Nivel2: string;
                Procedure Submit_Nivel3(parametro: string);
                Function Get_Nivel3: string;
                //CODIFICA DECLARA GETSESUBMITS
                Function verificacompatibilidade:boolean;

                
         Private
               ObjDataset:Tibdataset;
               CODIGO:string;
               Nivel1:string;
               Nivel2:string;
               Nivel3:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function EncriptaDecripta(original: string): string;
                function TrocaLetra(Original: string): string;
                function GeraSenha(DataAtual: String): String;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Ureltxt,Upesquisa,SysUtils,Dialogs,UDatamodulo,Ibquery,Controls
//USES IMPLEMENTATION
;


{ TTabTitulo }


Function  TObjPSWD.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Nivel1:=fieldbyname('Nivel1').asstring;
        Self.Nivel2:=fieldbyname('Nivel2').asstring;
        Self.Nivel3:=Self.EncriptaDecripta(fieldbyname('Nivel3').asstring);
        result:=True;
     End;
end;


Procedure TObjPSWD.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('Nivel1').asstring:=Self.Nivel1;
        fieldbyname('Nivel2').asstring:=Self.Nivel2;
        fieldbyname('Nivel3').asstring:=Self.EncriptaDecripta(Self.Nivel3);
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjPSWD.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




 if Self.status=dsinsert
 then  Self.ObjDataset.Insert//libera para insercao
 Else  Self.ObjDataset.edit;//se for edicao libera para tal
 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjPSWD.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Nivel1:='';
        Nivel2:='';
        Nivel3:='';
//CODIFICA ZERARTABELA



     End;
end;

Function TObjPSWD.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjPSWD.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjPSWD.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjPSWD.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
//CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjPSWD.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjPSWD.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Nivel1,Nivel3');
           SelectSQL.ADD(' from  TabPSWD');
           SelectSQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPSWD.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPSWD.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPSWD.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

//CODIFICA CRIACAO DE OBJETOS

        ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select CODIGO,Nivel1,Nivel');
                SelectSQL.ADD(' from  TabPSWD');
                SelectSQL.ADD(' WHERE codigo=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TabPSWD(CODIGO,Nivel1,Nivel2,NIvel3)');
                InsertSQL.add('values (:CODIGO,:Nivel1,:Nivel2,:NIvel3)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabPSWD set CODIGO=:CODIGO,Nivel1=:Nivel1,Nivel2=:Nivel2,NIvel3=:NIvel3');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabPSWD where codigo=:codigo ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,Nivel1,Nivel2,NIvel3');
                RefreshSQL.ADD(' from  TabPSWD');
                RefreshSQL.ADD(' WHERE codigo=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjPSWD.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPSWD.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPSWD');
     Result:=Self.ParametroPesquisa;
end;

function TObjPSWD.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de PSWD ';
end;


function TObjPSWD.Get_NovoCodigo: string;
var
StrTemp:TIBStoredProc;
begin
     Try
        Try
           StrTemp:=TIBStoredProc.create(nil);
           StrTemp.database:=ObjDataset.Database;
      StrTemp.StoredProcName:='PROC_GERA_PSWD';
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

           StrTemp.ExecProc;
           Result:=StrTemp.ParamByname('CODIGO').ASSTRING;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o PSWD',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(StrTemp);
     End;


end;


destructor TObjPSWD.Free;
begin
Freeandnil(Self.ObjDataset);
Freeandnil(Self.ParametroPesquisa);
//CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPSWD.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPSWD.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjPswd.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjPswd.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjPswd.Submit_Nivel1(parametro: string);
begin
        Self.Nivel1:=Parametro;
end;
function TObjPswd.Get_Nivel1: string;
begin
        Result:=Self.Nivel1;
end;
procedure TObjPswd.Submit_Nivel2(parametro: string);
begin
        Self.Nivel2:=Parametro;
end;
function TObjPswd.Get_Nivel2: string;
begin
        Result:=Self.Nivel2;
end;

Function TObjPswd.TrocaLetra(Original:string):string;
var
cont:integer;
palavra:string;
Begin
     palavra:=''; 
 palavra:=original; 
 
 for cont:=1 to length(palavra) do 
 Begin 
        case palavra[cont] of

        '1': palavra[cont]:='6';
        '6': palavra[cont]:='1';

        '2': palavra[cont]:='7';
        '7': palavra[cont]:='2';

        '3': palavra[cont]:='5';
        '5': palavra[cont]:='3';

        '4': palavra[cont]:='8';
        '8': palavra[cont]:='4';

        '9': palavra[cont]:='0';
        '0': palavra[cont]:='9';

        'a': palavra[cont]:='Z';
        'Z': palavra[cont]:='a';

        'e': palavra[cont]:='X';
        'X': palavra[cont]:='e';

        'i': palavra[cont]:='R';
        'R': palavra[cont]:='i';

        'o': palavra[cont]:='K';
        'K': palavra[cont]:='o';

        'u': palavra[cont]:='H';
        'H': palavra[cont]:='u';


        End;
 End;
 result:=palavra;
End;

//De acordo com a data passada de parametro gera-se uma senha
Function TObjPSWD.GeraSenha(DataAtual:String):String;
var
Cont:Integer;
Senha:String;
Data:Tdate;
ano,dia,mes:word;
anos,dias,mess:String;
vetorMeses:Array[1..12] of string;
numerofinal:longint;
Begin

     Result:='';

     VetorMeses[01]:='jane';
     VetorMeses[02]:='feve';
     VetorMeses[03]:='mar�';
     VetorMeses[04]:='abri';
     VetorMeses[05]:='maio';
     VetorMeses[06]:='junh';
     VetorMeses[07]:='julh';
     VetorMeses[08]:='agos';
     VetorMeses[09]:='sete';
     VetorMeses[10]:='outu';
     VetorMeses[11]:='nove';
     VetorMeses[12]:='deze';

     Try
        data:=Strtodate(dataatual);
     except
        exit;
     End;

     DecodeDate(data,ano,mes,dia);
     anos:='';
     mess:='';
     dias:='';

     If ((ano*5)<10)
     Then anos:='0'+Inttostr(Ano*5)
     Else anos:=Inttostr(Ano*5);

     If ((mes*10)<10)
     Then mess:='0'+inttostr(mes*10)
     Else mess:=inttostr(mes*10);

     If ((dia*8)<10)
     Then dias:='0'+inttostr(dia*8)
     Else dias:=inttostr(dia*8);

     senha:='';
     Senha:=VetorMeses[mes][4]+'@'+VetorMeses[mes][2]+'%'+ANOS+'#'+VetorMeses[mes][1]+'='+DIAS+'$'+MESS;
     Senha:=senha+')'+VetorMeses[mes][3];

     numerofinal:=0;
     for cont:=1 to length(senha) do
     begin
          numerofinal:=numerofinal+ord(senha[cont]);
     End;

     for cont:=1 to length(Self.Nivel3) do
     begin
          numerofinal:=numerofinal+ord(Self.Nivel3[cont]);
     End;
     Result:=inttostr(numerofinal);
End;


Function TObjPSWD.EncriptaDecripta(original:string):string;
var
final:string;
cont:integer;
palavra:string;
begin
     palavra:='';
     palavra:=original;

     final:='';
     for cont:=1 to length(palavra) do
     begin
      	final:=final+chr(256-ord(copy(palavra,cont,1)[1]));
     end;
     final:=Self.TrocaLetra(Final);
     result:=final;
end;



function TObjPSWD.verificacompatibilidade: boolean;
var
temp,senhausuario,tmpsenha,tmpnivel1,tmpnivel2:string;
posicao:integer;
begin
     Result:=False;
     tmpnivel1:=Self.Nivel1;
     tmpnivel2:=Self.Nivel2;
     tmpnivel1:=Self.EncriptaDecripta(tmpnivel1);
     tmpnivel2:=Self.EncriptaDecripta(tmpnivel2);
     tmpsenha:=Self.GeraSenha(Tmpnivel1);

     if (Tmpsenha<>tmpnivel2)
     Then Begin
               senhausuario:='';
               senhausuario:=InputBox('Senha de Ativa��o','Vers�o Vencida! Digite a senha','');
               //o cara vai digitar a senha +'-'+'novo vencimento'
               posicao:=pos('/',senhausuario);
               if (posicao=0)
               Then begin
                         Messagedlg('Senha Incompleta!',mterror,[mbok],0);
                         exit;
               End;
               temp:=Copy(senhausuario,1,posicao-1);
               if (temp<>tmpsenha)
               Then Begin
                         Messagedlg('Senha Inv�lida!',mterror,[mbok],0);
                         exit;
               End;
               //pego o novo vencimento e gero a nova senha
               temp:=Copy(senhausuario,posicao+1,(length(senhausuario)-posicao));
               //decodificando a data digitada
               
               
               
     End;








end;

function TObjPSWD.Get_Nivel3: string;
begin
     Result:=Self.Nivel3;
end;

procedure TObjPSWD.Submit_Nivel3(parametro: string);
begin
     Self.Nivel3:=Parametro;
end;

end.


