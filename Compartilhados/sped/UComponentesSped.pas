unit UComponentesSped;

interface

uses
  SysUtils, Classes, ACBrSpedFiscal, ACBrSpedPisCofins;

type
  TFCOmponentesSped = class(TDataModule)
    ACBrSPEDFiscal1: TACBrSPEDFiscal;
    ACBrSPEDPisCofins1: TACBrSPEDPisCofins;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOmponentesSped: TFCOmponentesSped;

implementation

{$R *.dfm}

end.
