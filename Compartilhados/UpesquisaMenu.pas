{jonas}

unit UpesquisaMenu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, StdCtrls, Buttons, ExtCtrls, ComCtrls, IBQuery,
  DB, IBCustomDataSet;

type

  TfPesquisaMenu = class(TForm)
    Acao: TBitBtn;
    queryExec: TIBQuery;
    panelPesquisa: TPanel;
    Panel1: TPanel;
    edtPesquisa: TEdit;
    ListBox1: TListBox;
    pnl1: TPanel;
    label2: TLabel;
    lbTotalEncontrados: TLabel;
    StatusBar1: TStatusBar;
    procedure ListBox1DblClick(Sender: TObject);
    procedure edtPesquisaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ListBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtPesquisaChange(Sender: TObject);
  private

    listaAction: array[0..200] of TNotifyEvent;
    totalAcoes:Integer;
    formulario:Tform;
    mainMenu:TMainMenu;

    procedure LimpaAcoes();

  public


    procedure pesquisa(menuProcura: string);
    procedure pesquisaMainMenu(menuProcura: string);
    procedure submit_formulario(formulario:tform);
    procedure submit_MainMenu(mainMenu:TMainMenu);
    function tiraSimbolo(var info:string;simbolo:Char):string;

  end;

var
  fPesquisaMenu: TfPesquisaMenu;

implementation

uses UDataModulo;


{$R *.dfm}

{ TfPesquisaMenu }



procedure TfPesquisaMenu.pesquisa(menuProcura: string);
var
  i:integer;
  subStr,str:string;
begin

  try

    self.LimpaAcoes();
    self.totalAcoes:=0;
    self.ListBox1.Items.Text:='';
    self.lbTotalEncontrados.Caption:='0';


    for i:=0 to self.formulario.ComponentCount-1 do
    begin

      if (self.formulario.Components[i].ClassNameIs('TMenuItem')) then
      begin

        subStr:= UpperCase(menuProcura);
        str   := UpperCase((TMenuItem(self.formulario.Components[i]).Caption));
        str:=self.tiraSimbolo(str,'&');

        if Pos(subStr,str) <> 0 then
        begin

            str:=TMenuItem(self.formulario.Components[i]).caption;
            str:=self.tiraSimbolo(str,'&');

            if Assigned(TMenuItem(self.formulario.Components[i]).OnClick) then
            begin
              ListBox1.Items.add(str);
              listaAction[totalAcoes]:= TMenuItem(self.formulario.Components[i]).OnClick;
              totalAcoes:=totalAcoes+1;
            end

        end;

      end;

    end;


    if (totalAcoes > 0) then
    begin
      self.lbTotalEncontrados.Caption:=IntToStr(totalAcoes);
      self.ListBox1.Selected[0]:=True;
    end;

  except

    on e:Exception do
    begin

      ShowMessage('Erro: '+e.Message);

    end;

  end;


end;

procedure TfPesquisaMenu.pesquisaMainMenu(menuProcura: string);
var
  i:integer;
  subStr,str:string;
begin

  try

    self.LimpaAcoes();
    self.totalAcoes:=0;
    self.ListBox1.Items.Text:='';
    self.lbTotalEncontrados.Caption:='0';


    for i:=0 to self.mainMenu.ComponentCount-1 do
    begin

      if (self.mainMenu.Components[i].ClassNameIs('TMenuItem')) then
      begin

        subStr:= UpperCase(menuProcura);
        str   := UpperCase((TMenuItem(self.mainMenu.Components[i]).Caption));
        str:=self.tiraSimbolo(str,'&');

        if Pos(subStr,str) <> 0 then
        begin

          ListBox1.Items.add(TMenuItem(self.mainMenu.Components[i]).caption);
          listaAction[totalAcoes]:= TMenuItem(self.mainMenu.Components[i]).OnClick;
          totalAcoes:=totalAcoes+1;

        end;

      end;

    end;

    self.lbTotalEncontrados.Caption:=IntToStr(totalAcoes);

  except

    on e:Exception do
    begin

      ShowMessage('Erro: '+e.Message);

    end;

  end;


end;

procedure TfPesquisaMenu.submit_formulario(formulario:TForm);
begin
  self.formulario:=formulario;
end;

procedure TfPesquisaMenu.ListBox1DblClick(Sender: TObject);
begin

  Acao.OnClick:=self.listaAction[self.ListBox1.ItemIndex];

  try
    Acao.OnClick(Sender);
  except
  end;

end;

function TfPesquisaMenu.tiraSimbolo(var info: string;simbolo:Char):string;
var
  i:Integer;
begin

   result:='';

   for i:=1 to Length(info) do
   begin

    if (info[i] <> simbolo) then
      result:=result+info[i];

   end;

end;

procedure TfPesquisaMenu.edtPesquisaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  {if (self.edtPesquisa.Text = '') then
    Exit;

   if (Key = VK_RETURN) then
      //self.pesquisaMainMenu(self.edtPesquisa.Text);
     self.pesquisa(self.edtPesquisa.Text); }

  if (Key = VK_RETURN) then
  begin

    self.ListBox1DblClick(self.ListBox1);
    //self.ListBox1.SetFocus;

  end
  else if (Key = 40) then {seta para baixo}
  begin

    self.ListBox1.ItemIndex:=self.ListBox1.ItemIndex+1;
    
  end
  else if (Key = 38) then {seta para cima}
  begin

      self.ListBox1.ItemIndex:=self.ListBox1.ItemIndex-1;

  end;

   self.edtPesquisa.SelStart := Length(edtPesquisa.Text)+1;


end;

procedure TfPesquisaMenu.LimpaAcoes;
var
  i:integer;
begin

  for i := 0  to 200 do
    self.listaAction[i]:=nil;

end;

procedure TfPesquisaMenu.FormShow(Sender: TObject);
begin


   self.LimpaAcoes();
   self.totalAcoes:=0;
   self.ListBox1.Items.Text:='';
   self.edtPesquisa.SetFocus;

end;


procedure TfPesquisaMenu.submit_MainMenu(mainMenu: TMainMenu);
begin

  self.mainMenu:=mainMenu;

end;

procedure TfPesquisaMenu.ListBox1KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (Key = VK_RETURN) then
    self.ListBox1DblClick(sender)
  else if (Key = VK_SPACE) then
  begin
    self.edtPesquisa.Text:='';
    self.edtPesquisa.SetFocus;
  end;

end;

procedure TfPesquisaMenu.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

  if (Key = 27) then  {ESC}
  begin

    self.Close;
      
  end




  (*if (Key = 113) then {F2}

    begin

      self.panelPesquisa.Visible:=False;

      self.panelExecSQL.Align:=alClient;
      self.panelExecSQL.Visible:=True;

    end

  else if (Key = 112) then {F1}
  begin

    self.panelExecSQL.Visible:=False;

    self.panelPesquisa.Align:=alClient;
    self.panelPesquisa.Visible:=True;

  end; *)

end;

procedure TfPesquisaMenu.edtPesquisaChange(Sender: TObject);
begin

  self.pesquisa(edtPesquisa.Text) ;

  {lokura}

end;

end.
