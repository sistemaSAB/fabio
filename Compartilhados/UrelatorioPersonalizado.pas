unit UrelatorioPersonalizado;

interface

uses
  ibquery,db,UessencialGlobal,Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  RDprint, StdCtrls, Buttons,inifiles, Menus, ExtCtrls, Uobjarquivoini,
  Grids, Printers;

type
  TFrelatorioPersonalizado = class(TForm)
    Rdprint: TRDprint;
    MainMenu1: TMainMenu;
    Opes1: TMenuItem;
    IMPRIMEFOLHADEGUIA1: TMenuItem;
    SalvaPosies1: TMenuItem;
    modelo: TLabel;
    DestriLabels1: TMenuItem;
    PanelPOsicoes: TPanel;
    LbColuna: TLabel;
    lblinha: TLabel;
    lbposicao: TLabel;
    edtleft: TEdit;
    edttop: TEdit;
    STRGCampos: TStringGrid;
    DestriCampoAtual1: TMenuItem;
    AbreTeladeConfiguraesGerais1: TMenuItem;
    Panel1: TPanel;
    procedure PanelPOsicoesDockOver(Sender: TObject;
      Source: TDragDockObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean);
    procedure FormDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure modeloClick(Sender: TObject);
    procedure edtleftKeyPress(Sender: TObject; var Key: Char);
    procedure edttopKeyPress(Sender: TObject; var Key: Char);
    procedure modeloMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure FormShow(Sender: TObject);
    procedure SalvaPosies1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure IMPRIMEFOLHADEGUIA1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DestriLabels1Click(Sender: TObject);
    procedure DestriCampoAtual1Click(Sender: TObject);
    procedure STRGCamposDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure AbreTeladeConfiguraesGerais1Click(Sender: TObject);
    procedure RdprintNewPage(Sender: TObject; Pagina: Integer);
    procedure STRGCamposDblClick(Sender: TObject);



  private


    function  carregaconfiguracoes: boolean;

    Procedure CriaNovaLabel(nome:String;quantidade:integer;Plinha,Pcoluna:integer;Pcaption:String);overload;
    Procedure CriaNovaLabel;overload;

    Procedure DestroiLabels;overload;
    Procedure DestroiLabels(Pnome:String);overload;
    Procedure apagalinhagrid(posicao:integer);
    procedure TrocaCorLabels;
    Procedure ConfiguraComponenteRdprint;
    Procedure GravaConfiguracoesfolha;


    { Private declarations }
  public
    { Public declarations }
    ObjArquivoIni:tobjarquivoini;
    CampoNome : integer;
    Campolinha: integer;
    campocoluna: integer;
    campoquantidade: integer;
    campotipo: integer;
    campovalor: integer;
    camporepeticao:integer;
    campoatributos:integer;
    campoauditora:integer;

    DIVISOR:integer;
    Nomerelatorio:String;
    cabecalho:string;
    QtdeRepeticaoFolha:integer;
    Imprime_Sem_Repeticao_todas_Folhas:boolean;
    ImprimeCabecalho:boolean;


    TabelaCampos:String;
    TabelaCamposRepeticao:String;

    PLinhaPrimeiroProdutoRepeticao:integer;


    Function Inicializa:boolean;
    Function Encerra:boolean;
    Function RetornaPrimeiroProdutorepeticao:integer;

    Procedure RetornaCamposporTipo(PTIPO:String;PcamposExtras:TStringList);overload;
    Procedure RetornaCamposporTipo(PTIPO:String;PcamposExtras:TStringList;PcampoAuditora:TStringList);overload;
    Function RetornaQuantidade(PTIPO:string;PVALORCAMPO:string):integer;
    function RetornaLinha(PTIPO, PVALORCAMPO: string): integer;


    procedure ImprimeCamposPreImpresso(Querylocal: Tibquery;pCampoRepeticao: boolean; out Plinha: integer;PcamposImpressaoExtra:TStringList;PvalorCamposImpressaoExtra:TStringList);overload;
    procedure ImprimeCamposPreImpresso(Querylocal: Tibquery;pCampoRepeticao: boolean; out Plinha: integer);overload;
    procedure ImprimeCamposPreImpresso(Querylocal: Tibquery;pCampoRepeticao: boolean; out Plinha: integer; PcamposImpressaoExtra,PvalorCamposImpressaoExtra: TStringList; SOMENTELABELX: STring);overload;
    procedure ImprimeCamposPreImpresso(Querylocal: Tibquery;pCampoRepeticao: boolean; out Plinha: integer; PcamposImpressaoExtra,PvalorCamposImpressaoExtra: TStringList; SOMENTELABELX: STring;Pignoravazio:boolean;SOMENTETIPOX:String);overload;


  end;

var

  FrelatorioPersonalizado: TFrelatorioPersonalizado;

implementation

uses UescolheTipoCamporelatorio,
  UopcoesListCheckBox, 
  UConfiguracoesgeraisRelatorioPersonalizado,
  UvisualizaCamposRelatorioPersonalizado, UDataModulo;

{$R *.DFM}


procedure TFrelatorioPersonalizado.PanelPOsicoesDockOver(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
begin
     Accept:=True;
     PanelPOsicoes.Left:=X;
     PanelPOsicoes.top:=y;

end;

procedure TFrelatorioPersonalizado.FormDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
     accept:=true;
end;

procedure TFrelatorioPersonalizado.FormDragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
     PanelPOsicoes.Left:=x;
     PanelPOsicoes.top:=y;
end;

procedure TFrelatorioPersonalizado.modeloClick(Sender: TObject);
var
posicao:integer;
begin

     if (uppercase(Tlabel(sender).name)='MODELO')
     Then exit;

     Self.TrocaCorLabels;

     
     TLabel(Sender).Font.Color:=clBlue;
     
     posicao:=Self.STRGCampos.Cols[0].IndexOf(uppercase(Tlabel(sender).name));

     Self.STRGCampos.Row:=posicao;

     Self.STRGCampos.cells[Self.Campolinha,posicao]:=floattostr(int((TLabel(Sender).Top+self.VertScrollBar.Position)/Self.divisor));
     Self.STRGCampos.cells[Self.campocoluna,posicao]:=floattostr(int(TLabel(Sender).left/Self.DIVISOR));


     PanelPOsicoes.Caption:=TLabel(Sender).Name;
     lbposicao.caption:=inttostr(posicao);

     edtleft.text:=floattostr(int(TLabel(Sender).left/Self.DIVISOR));
     edttop.text:=floattostr(int((TLabel(Sender).Top+self.VertScrollBar.Position)/Self.DIVISOR));
     edtleft.setfocus;
     VertScrollBar.Position:=0;
end;

procedure TFrelatorioPersonalizado.TrocaCorLabels;
var
  cont:integer;
  temp:Tlabel;
begin
   for cont:=2 to Self.STRGCampos.RowCount-1 do
   Begin
        temp:=nil;
        temp:=Self.findcomponent(Self.STRGCampos.Cells[Self.camponome,cont]) as Tlabel;

        if (temp<>nil)
        Then Begin
                  if (Self.STRGCampos.Cells[Self.camporepeticao,cont]='R')
                  Then temp.font.color:=clred
                  else Temp.font.color:=clblack;

                  //escrevendo o valor porem com com (.) pontos para marcar as colunas usadas 
                  temp.Caption:=CompletaPalavra(Self.STRGCampos.Cells[Self.campovalor,cont],strtoint(Self.STRGCampos.Cells[Self.campoquantidade,cont])-1,'.');
                  temp.Caption:=temp.Caption+'|';

                  if (Self.STRGCampos.cells[Self.campotipo,cont]='SUBTOTAL') or (Self.STRGCampos.cells[Self.campotipo,cont]='SUBTOTAL_MEDIA')
                  Then temp.font.color:=clPurple;

                  if (Self.STRGCampos.cells[Self.campotipo,cont]='TOTALFINAL') or (Self.STRGCampos.cells[Self.campotipo,cont]='TOTALFINAL_MEDIA')
                  Then temp.font.color:=clLime;

        End;
   End;

end;


procedure TFrelatorioPersonalizado.edtleftKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
                  if (Self.Panelposicoes.caption='')
                  Then exit;

                  if (edtleft.Text='0')
                   or (edttop.text='0')
                  Then Begin
                           MessageDlg('Nem a linha ou coluna pode ter valor Zero',mterror,[mbok],0);
                           exit;
                   End;

                   TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=Self.DIVISOR*strtoint(edtleft.text);
                   TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=Self.DIVISOR*(strtoint(edttop.text));

                   Self.STRGCampos.Cells[Self.Campolinha,strtoint(lbposicao.caption)]:=edttop.Text;
                   Self.STRGCampos.Cells[Self.campocoluna,strtoint(lbposicao.caption)]:=edtleft.Text;

                   edttop.setfocus;
        End
        Else key:=#0;
end;

procedure TFrelatorioPersonalizado.edttopKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then
        if key=#13
        Then Begin
               if (Self.Panelposicoes.caption='')
               Then exit;
               
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).left:=Self.DIVISOR*strtoint(edtleft.text);
               TLabel(Self.FindComponent(PanelPOsicoes.caption)).top:=(Self.DIVISOR*strtoint(edttop.text))-VertScrollBar.position;
               Self.STRGCampos.Cells[Self.Campolinha,strtoint(lbposicao.caption)]:=edttop.Text;
               Self.STRGCampos.Cells[Self.campocoluna,strtoint(lbposicao.caption)]:=edtleft.Text;
               edtleft.SetFocus;
        End
        Else key:=#0;
end;

procedure TFrelatorioPersonalizado.modeloMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
     Tlabel(Sender).hint:=Tlabel(Sender).name;
End;

procedure TFrelatorioPersonalizado.FormShow(Sender: TObject);
var
cont:integer;
begin
     Self.caption:='Configura��o de Relat�rio Personalizado  - '+Self.Nomerelatorio;

     Self.PanelPOsicoes.caption:='';
     edtleft.text:='1';
     edttop.text:='1';
     Self.DIVISOR:=7;

     if (Self.Inicializa=False)
     then Self.PanelPOsicoes.visible:=False
     Else Begin
               //criando as labels
               for cont:=2 to Self.STRGCampos.RowCount-1 do
               begin
                    //passo o nome, a qtde, linha, coluna
                    Self.CriaNovaLabel(Self.STRGCampos.Cells[Self.camponome,cont],strtoint(Self.strgcampos.cells[Self.campolinha,1]),strtoint(Self.STRGCampos.Cells[Self.campolinha,cont]),strtoint(Self.STRGCampos.Cells[Self.campocoluna,cont]),Self.STRGCampos.Cells[Self.campovalor,cont]);
               End;
     End;
end;

procedure TFrelatorioPersonalizado.SalvaPosies1Click(Sender: TObject);
var
int_habilita:integer;
Temp:string;
arquivo_ini:Tinifile;
cont:integer;
begin

     self.ObjArquivoIni.ZerarTabela;
     if (self.ObjArquivoIni.Localizanome(Self.Nomerelatorio)=False)
     then Begin
               Self.ObjArquivoIni.status:=dsinsert;
               Self.ObjArquivoIni.Submit_CODIGO('0');
               Self.ObjArquivoIni.Submit_nome(Self.Nomerelatorio);
               Self.ObjArquivoIni.Submit_arquivo('');
               if (Self.ObjArquivoIni.Salvar(true)=False)
               Then Begin
                         mensagemerro('N�o foi poss�vel salvar o registro na tabela de arquivos ini');
                         exit;
               End;
               if (self.ObjArquivoIni.Localizanome(Self.Nomerelatorio)=False)
               then Begin
                         mensagemerro('N�o foi poss�vel encontrar o registro '+Self.Nomerelatorio+' na tanela de arquivos ini');
                         exit;
               End;
     End;
     Self.ObjArquivoIni.TabelaparaObjeto;


     Try
        Self.ObjArquivoIni.Status:=dsedit;
        Self.ObjArquivoIni.Arquivo.clear;
        //Self.ObjArquivoIni.Arquivo.Text:=Self.STRGCampos.Cols[0].text;

        for cont:=0 to Self.STRGCampos.RowCount-1 do
        Begin
             implode(TstringList(Self.strgcampos.rows[cont]),temp);
             Self.ObjArquivoIni.Arquivo.Add(temp);
        End;



        if (self.ObjArquivoIni.Salvar(true)=False)
        Then Begin
                  mensagemerro('N�o foi poss�vel salvar as altera��es');
                  exit;
        end;

        Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);
     Except
     End;
end;







Function TFrelatorioPersonalizado.carregaconfiguracoes:boolean;
var
Temp,nome,valor:string;
cont,posicao:integer;
begin
    result:=False;
    modeloclick(modelo);

    FConfiguracoesgeraisRelatorioPersonalizado.ConfiguraPadrao;
    Self.ConfiguraComponenteRdprint;

    //*******************carregando primeiro as configuracoes da folha****************

    if (self.ObjArquivoIni.Localizanome('CONF_PAGINA_'+Self.nomeRelatorio)=False)
    then MensagemAviso('N�o foi encontrado nenhuma configura��o de pagina para '+'CONF_PAGINA_'+self.Nomerelatorio+' ser� usado configura��o padr�o')
    Else Begin
              Self.ObjArquivoIni.TabelaparaObjeto;

              Self.STRGCampos.rowcount:=Self.ObjArquivoIni.Arquivo.Count;
              Self.STRGCampos.Cols[0]:=Self.ObjArquivoIni.Arquivo;

              //na primeira coluna tenho todas as outras separadas por ; preciso percorrer e separar
              for cont:=0 to Self.STRGCampos.RowCount-1 do
              Begin
                      temp:=Self.strgcampos.Cells[0,cont];

                      Self.ObjArquivoIni.Arquivo.Clear;

                      if (ExplodeStr(temp,self.objarquivoini.arquivo,';','string')=false)
                      then exit;

                      nome:=Self.ObjArquivoIni.Arquivo[0];
                      valor:=Self.ObjArquivoIni.Arquivo[1];


                      if (nome='MOSTRAPREVIEW')
                      then Begin
                                if (valor='SIM')
                                then FConfiguracoesgeraisRelatorioPersonalizado.CbMostraPreview.Checked:=true
                                Else FConfiguracoesgeraisRelatorioPersonalizado.CbMostraPreview.Checked:=False;
                      End
                      else
                      if (nome='CAPTIONFORMULARIO')
                      then Begin
                                FConfiguracoesgeraisRelatorioPersonalizado.edtcaptionformulario.text:=valor
                      End
                      else
                      if (nome='QUANTIDADELINHAS')
                      then Begin
                                Try
                                  strtoint(valor);
                                  FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadelinhas.text:=valor;
                                Except
                                      FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadelinhas.text:='30';
                                End;
                      End
                      else
                      if (nome='QUANTIDADECOLUNAS')
                      then Begin
                                Try
                                  strtoint(valor);
                                  FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadecolunas.text:=valor;
                                Except
                                      FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadecolunas.text:='90';
                                End;
                      End
                      else
                      if (nome='FONTETAMANHOPADRAO')
                      then Begin
                                FConfiguracoesgeraisRelatorioPersonalizado.ComboFontetamanhopadrao.text:=valor;
                      End
                      else
                      if (nome='ENTRELINHAS')
                      then Begin
                                if valor='OITO'
                                then FConfiguracoesgeraisRelatorioPersonalizado.ComboEntrelinhas.itemindex:=0
                                Else  FConfiguracoesgeraisRelatorioPersonalizado.ComboEntrelinhas.itemindex:=1;
                      End
                      else
                      if (nome='QUANTIDADEITENSREPETICAO')
                      then Begin
                                Try
                                  strtoint(valor);
                                  FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadeitemsrepeticao.text:=valor;
                                Except
                                      FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadeitemsrepeticao.text:='10';
                                End;
                      End
                      else
                      if (nome='DUPLICAITENSREPETICAO')
                      then Begin
                                Try
                                  if (Valor='SIM')
                                  Then FConfiguracoesgeraisRelatorioPersonalizado.cbduplicaitensrepetica.Checked:=True
                                  Else FConfiguracoesgeraisRelatorioPersonalizado.cbduplicaitensrepetica.Checked:=False;
                                Except
                                      FConfiguracoesgeraisRelatorioPersonalizado.cbduplicaitensrepetica.Checked:=False;
                                End;
                      End
                      else
                      if (nome='QTDEABAIXODUPLICAR')
                      then Begin
                                Try
                                  strtoint(valor);
                                  FConfiguracoesgeraisRelatorioPersonalizado.edtqtdelinhasabaixoduplica.text:=valor;
                                Except
                                      FConfiguracoesgeraisRelatorioPersonalizado.edtqtdelinhasabaixoduplica.text:='0';
                                End;
                      End
                      else
                      if (nome='IMPRIME_SEM_REPETICAO_EM_TODAS_FOLHAS')
                      then Begin
                                Try
                                  if (valor='SIM') or (valor='')
                                  Then FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhoemtodasasfolhas.ItemIndex:=1
                                  Else  FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhoemtodasasfolhas.ItemIndex:=0;
                                Except
                                End;
                      End
                      else
                      if (nome='IMPRIME_CABECALHO_PADRAO')
                      then Begin
                                Try
                                  if (valor='SIM') or (valor='')
                                  Then FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhopadrao.ItemIndex:=1
                                  Else FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhopadrao.ItemIndex:=0;
                                Except
                                End;
                      End
                      else
                      if (nome='ORIENTACAO_FOLHA')
                      then Begin
                                Try
                                  if (valor='RETRATO') or (valor='')
                                  Then FConfiguracoesgeraisRelatorioPersonalizado.comboorientacao.ItemIndex:=0
                                  Else FConfiguracoesgeraisRelatorioPersonalizado.comboorientacao.ItemIndex:=1
                                Except
                                     FConfiguracoesgeraisRelatorioPersonalizado.comboorientacao.ItemIndex:=0;
                                End;
                      End;


              End;
              Self.ConfiguraComponenteRdprint;
    End;


    //nome,linha,coluna,quantidade,tipo,valor,campoauditora
    Self.STRGCampos.ColCount:=9;
    Self.STRGCampos.rowcount:=2;

    Self.STRGCampos.cells[Self.CampoNome,0]:='Nome';
    Self.STRGCampos.cells[Self.Campolinha,0]:='linha';
    Self.STRGCampos.cells[Self.campocoluna,0]:='coluna';
    Self.STRGCampos.cells[Self.campoquantidade,0]:='qtde';
    Self.STRGCampos.cells[Self.campotipo,0]:='tipo';
    Self.STRGCampos.cells[Self.campovalor,0]:='valor';
    Self.STRGCampos.cells[Self.camporepeticao,0]:='repeticao';
    Self.STRGCampos.cells[Self.campoatributos,0]:='atributos';
    Self.STRGCampos.cells[Self.campoauditora,0]:='Sub-Total';


    Self.STRGCampos.cells[Self.camponome,1]:='Quantidade';
    Self.STRGCampos.cells[Self.Campolinha,1]:='0';
    Self.STRGCampos.cells[Self.campocoluna,1]:='0';
    Self.STRGCampos.cells[Self.campoquantidade,1]:='0';
    Self.STRGCampos.cells[Self.campotipo,1]:='0';
    Self.STRGCampos.cells[Self.campovalor,1]:='0';
    Self.STRGCampos.cells[Self.camporepeticao,1]:='0';
    Self.STRGCampos.cells[Self.campoatributos,1]:='0';
    Self.STRGCampos.cells[Self.campoauditora,1]:='0';



    if (self.ObjArquivoIni.Localizanome(Self.nomeRelatorio)=False)
    then begin
              MensagemAviso('N�o foi encontrado nenhuma configura��o para '+self.Nomerelatorio);
              exit;
    End;
    Self.ObjArquivoIni.TabelaparaObjeto;


    Self.STRGCampos.rowcount:=Self.ObjArquivoIni.Arquivo.Count;
    Self.STRGCampos.Cols[0]:=Self.ObjArquivoIni.Arquivo;
    //na primeira coluna tenho todas as outras separadas por ; preciso percorrer e separar


    for cont:=0 to Self.STRGCampos.RowCount-1 do
    Begin
            temp:=Self.strgcampos.Cells[0,cont];

            Self.ObjArquivoIni.Arquivo.Clear;
            if (ExplodeStr(temp,self.objarquivoini.arquivo,';','string')=false)
            then exit;

            Self.STRGCampos.Rows[cont]:=Self.ObjArquivoIni.Arquivo;
    End;

     AjustaLArguraColunaGrid(Self.STRGCampos);



end;

procedure TFrelatorioPersonalizado.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
keyc:char;
temp:string;
cont:integer;
Ctemp:Tlabel;
begin

keyc:=#13;

case key of

     vk_left:begin
                if (self.PanelPOsicoes.Caption='')
                then exit;

                if (strtoint(edtleft.text)>1)
                Then  edtleft.text:=inttostr(strtoint(edtleft.text)-1);

                self.edtleftKeyPress(Sender,keyc);
     End;
     vk_right:Begin
                  if (self.PanelPOsicoes.Caption='')
                  then exit;

                 edtleft.text:=inttostr(strtoint(edtleft.text)+1);
                 self.edtleftKeyPress(Sender,keyc);
     End;
     vk_up:Begin
                if (self.PanelPOsicoes.Caption='')
                then exit;

                if (strtoint(edttop.text)>1)
                Then edttop.text:=inttostr(strtoint(edttop.text)-1);
                self.edtleftKeyPress(Sender,keyc);
     End;
     vk_down:Begin
                    if (self.PanelPOsicoes.Caption='')
                    then exit;

                    edttop.text:=inttostr(strtoint(edttop.text)+1);
                    self.edtleftKeyPress(Sender,keyc);
     End;
     vk_insert:Self.CriaNovaLabel;
     vk_delete:begin
                    if (Self.PanelPOsicoes.Caption<>'')
                    then Begin
                              Self.DestroiLabels(Self.PanelPOsicoes.Caption);
                    End;
      end;
      vk_f5:begin
                 //altera quantidade
                 if (PanelPOsicoes.Caption='')
                 then exit;

                 try
                    cont:=strtoint(Self.STRGCampos.cells[Self.campoquantidade,strtoint(Self.lbposicao.caption)]);
                 Except
                      cont:=0;
                 End;

                 temp:=inttostr(cont);

                 if (inputquery('QUANTIDADE','DIGITE A NOVA QUANTIDADE',temp)=false)
                 then exit;

                 try
                       cont:=strtoint(temp);
                 Except
                       cont:=0;
                 End;

                 Self.STRGCampos.cells[Self.campoquantidade,strtoint(Self.lbposicao.caption)]:=inttostr(cont);
                 Self.TrocaCorLabels;
      End;
      vk_f6:begin
                 //altera tipo
                 if (PanelPOsicoes.Caption='')
                 then exit;

                 FescolheTipoCamporelatorio.RGTIPO.Items.clear;
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Label');//0
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Campo do Banco de Dados');//1
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Sub-Total (Soma)');//2
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Label Sub-Total (Soma)');//3

                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Sub-Total (M�dia)');//4
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Label Sub-Total (M�dia)');//5

                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Total Final (Soma)');//6
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Label Total Final (Soma)');//7

                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Total Final (M�dia)');//8
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Label Total Final (M�dia)');//9






                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABEL')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=0
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='BD')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=1
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='SUBTOTAL')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=2
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABELSUBTOTAL')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=3
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='SUBTOTAL_MEDIA')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=4
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABELSUBTOTAL_MEDIA')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=5
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='TOTALFINAL')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=6
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABELTOTALFINAL')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=7
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='TOTALFINAL_MEDIA')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=8
                 else
                 if (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABELTOTALFINAL_MEDIA')
                 Then FescolhetipoCamporelatorio.RGTIPO.ItemIndex:=9;


                 FescolhetipoCamporelatorio.showmodal;

                 case FescolhetipoCamporelatorio.RGTIPO.ItemIndex of
                  0:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='LABEL';
                  1:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='BD';
                  2:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='SUBTOTAL';
                  3:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='LABELSUBTOTAL';
                  4:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='SUBTOTAL_MEDIA';
                  5:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='LABELSUBTOTAL_MEDIA';
                  6:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='TOTALFINAL';
                  7:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='LABELTOTALFINAL';
                  8:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='TOTALFINAL_MEDIA';
                  9:Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]:='LABELTOTALFINAL_MEDIA';
                 End;

                 Self.TrocaCorLabels;
      End;
      vk_f7:begin
                 //altera valor
                 if (PanelPOsicoes.Caption='')
                 then exit;

                 if (
                    (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABEL')
                 or
                    (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABELTOTALFINAL')
                 or
                    (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABELSUBTOTAL')
                     )
                 Then Begin
                         temp:=Self.STRGCampos.cells[Self.campovalor,strtoint(Self.lbposicao.caption)];

                         if (inputquery('VALOR','DIGITE O NOVO VALOR',temp)=false)
                         then exit;

                         Self.STRGCampos.cells[Self.campoquantidade,strtoint(Self.lbposicao.caption)]:=inttostr(length(temp));
                 End
                 Else
                 if ((Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='BD')
                 or
                     (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='TOTALFINAL')
                 or
                     (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='SUBTOTAL'))

                 Then Begin
                           FvisualizaCamposRelatorioPersonalizado.TabelaCampos:=Self.TabelaCampos;
                           FvisualizaCamposRelatorioPersonalizado.TabelaCamposRepeticao:=Self.TabelaCamposRepeticao;


                           FvisualizaCamposRelatorioPersonalizado.pvalor:=Self.STRGCampos.cells[Self.campovalor,strtoint(Self.lbposicao.caption)];
                           FvisualizaCamposRelatorioPersonalizado.ptipo:=Self.STRGCampos.cells[Self.camporepeticao,strtoint(Self.lbposicao.caption)];

                           FvisualizaCamposRelatorioPersonalizado.showModal;
                           temp:=FvisualizaCamposRelatorioPersonalizado.campoescolhido;

                           //Self.STRGCampos.cells[Self.campoquantidade,strtoint(Self.lbposicao.caption)]:=inttostr(length(temp));
                 End
                 else exit;

                 Self.STRGCampos.cells[Self.campovalor,strtoint(Self.lbposicao.caption)]:=temp;



                 Ctemp:=(Self.FindComponent(self.panelposicoes.caption) as Tlabel);
                 
                 if (Ctemp<>nil)
                 Then Ctemp.caption:=temp;

                 Self.TrocaCorLabels;
      End;
      vk_f8:begin//repeticao

                 //altera repeticao
                 if (PanelPosicoes.Caption='')
                 then exit;

                 FescolheTipoCamporelatorio.RGTIPO.Items.clear;
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Sem Repeti��o');
                 FescolheTipoCamporelatorio.RGTIPO.Items.add('Com Repeti��o');
                 


                 
                 if (Self.STRGCampos.cells[Self.camporepeticao,strtoint(Self.lbposicao.caption)]='R')
                 Then Fescolhetipocamporelatorio.RGTIPO.ItemIndex:=1
                 else Fescolhetipocamporelatorio.RGTIPO.ItemIndex:=0;

                 Fescolhetipocamporelatorio.showmodal;

                 if (Fescolhetipocamporelatorio.RGTIPO.ItemIndex=0)
                 Then Self.STRGCampos.cells[Self.camporepeticao,strtoint(Self.lbposicao.caption)]:='N'
                 Else Self.STRGCampos.cells[Self.camporepeticao,strtoint(Self.lbposicao.caption)]:='R';

                 Self.TrocaCorLabels;
      End;

      vk_f9:begin//atributos

                 if (PanelPosicoes.Caption='')
                 then exit;

                 FopcoesListCheckBox.CHLBOpcoes.Items.clear;
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Negrito');//0
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('It�lico');//1
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Alinhado a esquerda');//2
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Formata��o de Valor');//3
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Centraliza na Folha');//4
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Quebrar em V�rias Linhas');//5
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Quebrar em V�rias Linhas Iniciando na Coluna 1 a partir da segunda linha');//6
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Fonte Expandida (s05cpp)');//7
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Fonte Normal (s10cpp)');//8
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Fonte s12cpp');//9
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Fonte s17cpp');//10
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Fonte s20cpp');//11
                 FopcoesListCheckBox.CHLBOpcoes.Items.add('Fonte Normal');//12
   

                 temp:=Self.STRGCampos.cells[Self.campoatributos,strtoint(Self.lbposicao.caption)];

                 if (pos('NEGRITO',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[0]:=true;

                 if (pos('ITALICO',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[1]:=true;

                 if (pos('ALINHAMENTOESQUERDA',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[2]:=true;

                 if (pos('FORMATAVALOR',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[3]:=true;

                 if (pos('CENTRALIZANAFOLHA',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[4]:=true;

                 if (pos('QUEBRARLINHAS',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[5]:=true;

                 if (pos('QUEBRARLINHAS_INICIA_COLUNA_1',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[6]:=true;


                 if (pos('QUEBRARLINHAS_INICIA_COLUNA_1',temp)<>0)
                 then FopcoesListCheckBox.CHLBOpcoes.Checked[6]:=true;

                 if (pos('FONTE05CPP',temp)<>0)
                 then  FopcoesListCheckBox.CHLBOpcoes.Checked[7]:=true;

                 if (pos('FONTE10CPP',temp)<>0)
                 then  FopcoesListCheckBox.CHLBOpcoes.Checked[8]:=true;

                 if (pos('FONTE12CPP',temp)<>0)
                 then  FopcoesListCheckBox.CHLBOpcoes.Checked[9]:=true;

                 if (pos('FONTE17CPP',temp)<>0)
                 then  FopcoesListCheckBox.CHLBOpcoes.Checked[10]:=true;

                 if (pos('FONTE20CPP',temp)<>0)
                 then  FopcoesListCheckBox.CHLBOpcoes.Checked[11]:=true;

                 if (pos('FONTENORMAL',temp)<>0)
                 then  FopcoesListCheckBox.CHLBOpcoes.Checked[12]:=true;

                 FopcoesListCheckBox.showmodal;

                 temp:='|';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[0]=true)
                 then temp:=temp+'|NEGRITO';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[1]=true)
                 then temp:=temp+'|ITALICO';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[2]=true)
                 then temp:=temp+'|ALINHAMENTOESQUERDA';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[3]=true)
                 then temp:=temp+'|FORMATAVALOR';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[4]=true)
                 then temp:=temp+'|CENTRALIZANAFOLHA';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[5]=true)
                 then temp:=temp+'|QUEBRARLINHAS';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[6]=true)
                 then temp:=temp+'|QUEBRARLINHAS_INICIA_COLUNA_1';




                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[7]=true)
                 then temp:=temp+'|'+'FONTE05CPP';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[8]=true)
                 then  temp:=temp+'|'+'FONTE10CPP';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[9]=true)
                 then  temp:=temp+'|'+'FONTE12CPP';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[10]=true)
                 then  temp:=temp+'|'+'FONTE17CPP';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[11]=true)
                 then  temp:=temp+'|'+'FONTE20CPP';

                 if (FopcoesListCheckBox.CHLBOpcoes.Checked[12]=true)
                 then  temp:=temp+'|'+'FONTENORMAL';

                 Self.STRGCampos.cells[Self.campoatributos,strtoint(Self.lbposicao.caption)]:=temp;

                 Self.TrocaCorLabels;
      End;

      vk_f11:
             Begin
                  //Campo Auditoria no Subtotal
                 if (PanelPOsicoes.Caption='')
                 then exit;

                 if ((Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='SUBTOTAL')
                 or
                    (Self.STRGCampos.cells[Self.campotipo,strtoint(Self.lbposicao.caption)]='LABELSUBTOTAL'))
                 Then Begin
                           FvisualizaCamposRelatorioPersonalizado.TabelaCampos:='';
                           FvisualizaCamposRelatorioPersonalizado.TabelaCamposRepeticao:=Self.TabelaCamposRepeticao;
                           FvisualizaCamposRelatorioPersonalizado.showModal;
                           temp:=FvisualizaCamposRelatorioPersonalizado.campoescolhido;
                 End
                 else exit;

                 Self.STRGCampos.cells[Self.campoauditora,strtoint(Self.lbposicao.caption)]:=temp;

                 Ctemp:=(Self.FindComponent(self.panelposicoes.caption) as Tlabel);
                 
                 if (Ctemp<>nil)
                 Then Ctemp.caption:=temp;

                 Self.TrocaCorLabels;
             End;

      End;
      AjustaLArguraColunaGrid(Self.STRGCampos);


End;


procedure TFrelatorioPersonalizado.IMPRIMEFOLHADEGUIA1Click(Sender: TObject);
var
cont:integer;
cont2:integer;
begin
     RdPrint.Abrir;
     if (RdPrint.setup=false)
     Then Begin
              RdPrint.fechar;
              exit;
     End;
     for cont:=1 to RdPrint.TamanhoQteColunas do
     Begin
         RdPrint.Imp(1,cont,inttostr(cont)[length(inttostr(cont))]);
     End;

     for cont:=2 to RdPrint.TamanhoQteLinhas do
     Begin
         RdPrint.Imp(cont,1,inttostr(cont));
     End;
     RdPrint.Novapagina;
     RdPrint.Imp(1,1,'NOVA FOLHA');
     RdPrint.fechar;
end;

procedure TFrelatorioPersonalizado.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     Self.Encerra;
     Self.Nomerelatorio:='';
     Self.TabelaCampos:='';
     Self.TabelaCamposRepeticao:='';
     Self.DestroiLabels;
end;

function TFrelatorioPersonalizado.Inicializa: boolean;
begin
     result:=False;

     Self.Rdprint.OpcoesPreview.Preview:=True;

     Self.CampoNome :=0;
     Self.Campolinha:=1;
     Self.campocoluna:=2;
     Self.campoquantidade:=3;
     Self.campotipo:=4;
     Self.campovalor:=5;
     Self.camporepeticao:=6;
     Self.campoatributos:=7;
     Self.campoauditora:=8;
     
     Self.QtdeRepeticaoFolha:=10;






     if (self.Nomerelatorio='')
     then begin
               mensagemerro('Configure primeiro o nome do relat�rio');
               exit;
     End; 

     try
        Self.ObjArquivoIni:=TObjarquivoini.Create;
     Except
           mensagemerro('Erro na tentativa de criar o objeto de arquivos ini');
           exit;
     End;
     Self.DestroiLabels;
     Strgcampos.ColCount:=1;
     Strgcampos.RowCount:=2;
     Strgcampos.rows[0].clear;
     Strgcampos.cells[0,0]:='0';

     Self.cabecalho:='';

     if (ObjParametroGlobal.ValidaParametro('CABECALHO NOS RELATORIOS')=FALSE)
     Then Self.cabecalho:='EXCLAIM TECNOLOGIA - BY EXCLAIM TECNOLOGIA'
     Else Begin
               Self.cabecalho:=ObjParametroGlobal.get_valor;
     End;
     Self.cabecalho:=Self.cabecalho+'  - '+datetostr(now)+'-'+timetostr(now);

     Self.carregaconfiguracoes;
     result:=true;

end;

function TFrelatorioPersonalizado.Encerra: boolean;
begin
     if (Self.ObjArquivoIni<>nil)
     Then Self.ObjArquivoIni.Free;
end;

procedure TFrelatorioPersonalizado.CriaNovaLabel;
begin
     Self.CriaNovaLabel('',0,0,0,'');
End;

procedure TFrelatorioPersonalizado.CriaNovaLabel(nome:String;quantidade:integer;Plinha,Pcoluna:integer;Pcaption:String);
var
Teste:Tlabel;
QuantCampos,temp2:integer;
begin
    with TLabel.Create(Self)  do
    begin

         
         if (quantidade=0)
         then Begin
                 QuantCampos:=strtoint(Self.STRGCampos.cells[1,1]);
                 quantcampos:=quantcampos+1;
                 //alterando a quantidade
                 Self.STRGCampos.cells[Self.CampoNome,1]:='Quantidade';
                 Self.STRGCampos.cells[Self.Campolinha,1]:=inttostr(quantcampos);

                 temp2:=1;

                 if (nome='')
                 then nome:='CAMPO'+inttostr(temp2);

                 teste:=nil;
                 teste:=(Self.FindComponent(nome) as TLabel);

                 while (teste<>nil) do
                 begin
                      nome:='';
                      inc(temp2,1);

                      if (nome='')
                      then nome:='CAMPO'+inttostr(temp2);

                      
                      teste:=nil;
                      teste:=(Self.FindComponent(nome) as TLabel);
                 end;
         End
         Else begin
                   nome:=nome;
         End;

         Parent:=Self;
         Height:=modelo.Height;
         Width:=modelo.width;
         tag:=0;

         if (pcoluna=0)
         then Left:=Self.Divisor//coluna 1
         Else Left:=(Pcoluna*Self.Divisor);

         if (pcoluna=0)
         then Top:=Self.Divisor//linha 1
         Else Top:=(plinha*Self.DIVISOR);


         Name:=nome;
         if (pcaption='')
         then caption:=nome
         else caption:=pcaption;

         Font:=modelo.Font;
         OnClick:=modeloClick;
         OnMouseMove:=modeloMouseMove;
         Transparent:=True;
    End;

    //adicionando a nova label no grid
    if (quantidade=0)
    then begin
              Self.STRGCampos.RowCount:=Self.STRGCampos.RowCount+1;
              Self.STRGCampos.cells[Self.camponome,Self.STRGCampos.RowCount-1]:=nome;
              Self.STRGCampos.cells[Self.Campolinha,Self.STRGCampos.RowCount-1]:=floattostr(int(top/Self.divisor));
              Self.STRGCampos.cells[Self.CampoColuna,Self.STRGCampos.RowCount-1]:=floattostr(int(left/Self.divisor));
              Self.STRGCampos.cells[Self.campoquantidade,Self.STRGCampos.RowCount-1]:='0';
              Self.STRGCampos.cells[Self.campotipo,Self.STRGCampos.RowCount-1]:='LABEL';
              Self.STRGCampos.cells[Self.campovalor,Self.STRGCampos.RowCount-1]:=nome;
              Self.STRGCampos.cells[Self.camporepeticao,Self.STRGCampos.RowCount-1]:='N';
              Self.STRGCampos.cells[Self.campoatributos,Self.STRGCampos.RowCount-1]:='|';
    End
    Else Begin


    End;


    //clicando no novo label
    teste:=nil;
    teste:=(Self.FindComponent(nome) as TLabel);

    if (teste<>nil)
    then modeloClick(teste);

    AjustaLArguraColunaGrid(Self.STRGCampos);
end;

procedure TFrelatorioPersonalizado.DestroiLabels;
Begin
     Self.DestroiLabels('');
End;

procedure TFrelatorioPersonalizado.DestroiLabels(Pnome:string);
var
teste:Tlabel;
cont:integer;
begin
     if (Pnome<>'')
     Then Begin
               cont:=Self.STRGCampos.Cols[0].IndexOf(pnome);
               Self.STRGCampos.row:=cont;
               Self.apagalinhagrid(cont);
               
              //quantidade
               cont:=strtoint(Self.strgcampos.cells[1,1]);
               cont:=cont-1;
               Self.STRGCampos.cells[1,1]:=inttostr(cont);

               teste:=(FindComponent(Pnome) as TLabel);
               FreeAndNil(teste);
     End
     Else Begin
           (*For Cont := 0 to Self.ComponentCount-1 do
           Begin


                if Self.Components [cont].ClassName = 'TLabel'
                Then begin
                          //as labels que nao pode destruir esta com um tag 666
                          Try
                            if (Self.Components[cont].Tag<>666)
                            Then Begin
                                      showmessage(Self.Components[cont].Name);
                                      teste:=(FindComponent(Self.Components[cont].Name) as TLabel);
                                      FreeAndNil(teste);
                            end;
                          except
                                MensagemErro(IntToStr(cont));
                          end;
                end;

           end;*)

           //destroi as labels que estao listadas no Grid
           for cont:=2 to Self.STRGCampos.RowCount-1 do
           Begin
                //ShowMessage(Self.STRGCampos.Cells[camponome,cont]);
                teste:=(FindComponent(Self.STRGCampos.Cells[camponome,cont]) as TLabel);
                FreeAndNil(teste);
           end;

           Self.STRGCampos.rowcount:=2;
           Self.STRGCampos.cells[1,1]:='0';
     End;
     Self.PanelPOsicoes.caption:='';
end;

procedure TFrelatorioPersonalizado.DestriLabels1Click(Sender: TObject);
begin
    if (MEssagedlg('Certeza que deseja destruir todos os campos?',mtConfirmation,[mbyes,mbno],0)=mrno)
    Then exit;
    Self.DestroiLabels;
end;

procedure TFrelatorioPersonalizado.DestriCampoAtual1Click(Sender: TObject);
begin
    if (MEssagedlg('Certeza que deseja destruir o campo atual?',mtConfirmation,[mbyes,mbno],0)=mrno)
    Then exit;

     if (Self.PanelPOsicoes.Caption<>'')
     then Self.DestroiLabels(Self.panelposicoes.caption);
end;

procedure TFrelatorioPersonalizado.apagalinhagrid(posicao: integer);
var
cont:integer;
begin
     if posicao<>(Self.STRGCampos.RowCount-1)
     Then Begin
               for cont:=posicao+1 to Self.STRGCampos.RowCount-1 do
               begin
                    Self.STRGCampos.Rows[cont-1]:=STRGCampos.Rows[cont];
               End;
     End;

     Self.STRGCampos.RowCount:=Self.STRGCampos.RowCount-1
end;

procedure TFrelatorioPersonalizado.STRGCamposDrawCell(Sender: TObject;
  ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
    if (gdselected in State)//selecionado
    then begin
          (Sender as TStringGrid).canvas.brush.color :=clblue;
          (Sender as TStringGrid).canvas.Font.Color:=clblack;

          (Sender as TStringGrid).canvas.FillRect(rect);
          (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
          exit;
    End;


    If (Sender as TStringGrid).Cells[Self.camporepeticao,Arow]='R'
    then begin
          (Sender as TStringGrid).canvas.brush.color :=RGB(255,255,153);
          (Sender as TStringGrid).canvas.Font.Color:=clblack;

          (Sender as TStringGrid).canvas.FillRect(rect);
          (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
    end
    else Begin
              if (Arow>0)
              Then Begin
                        if not(gdselected in State)
                        then begin
                              (Sender as TStringGrid).canvas.brush.color :=clwhite;
                              (Sender as TStringGrid).canvas.Font.Color:=clblack;

                              (Sender as TStringGrid).canvas.FillRect(rect);
                              (Sender as TStringGrid).canvas.TextOut(Rect.Left,Rect.Top,(Sender as TStringGrid).Cells[ACol,ARow]);
                        End;
              End;
    End;

end;

function TFrelatorioPersonalizado.RetornaPrimeiroProdutorepeticao: integer;
var
    cont,temp:integer;
begin
     result:=1;
     temp:=1000;

     for Cont:=2 to Self.STRGCampos.rowcount-1 do
     begin
          if (self.STRGCampos.Cells[Self.camporepeticao,cont]='R')
          Then Begin
                    Try
                       if (strtoint(Self.STRGCampos.Cells[Self.Campolinha,cont])<temp)
                       then temp:=strtoint(Self.STRGCampos.Cells[Self.Campolinha,cont]);
                    Except

                    End;
          End;
     End;

     if (temp<1000)
     Then result:=temp;

     PLinhaPrimeiroProdutoRepeticao:=result;
End;




procedure TFrelatorioPersonalizado.AbreTeladeConfiguraesGerais1Click(
  Sender: TObject);
begin
     FConfiguracoesgeraisRelatorioPersonalizado.Showmodal;

     if (Messagedlg('Deseja salvar as altera��es?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;
     
     Self.ConfiguraComponenteRdprint;
     Self.GravaConfiguracoesfolha;
     
end;

procedure TFrelatorioPersonalizado.ConfiguraComponenteRdprint;
var
valor:string;
begin
     With Self.rdprint do
     begin
          OpcoesPreview.Preview:=True;
          UsaGerenciadorImpr:=true;
          OpcoesPreview.CaptionPreview:=FConfiguracoesgeraisRelatorioPersonalizado.edtcaptionformulario.text;

          // F�BIO
          //If (FConfiguracoesgeraisRelatorioPersonalizado.ComboOrientacao.Text = 'RETRATO')
          //then Orientacao:=poPortrait
          //else Orientacao:=poLandscape;

          valor:=FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadelinhas.text;

          self.ImprimeCabecalho:=false;

          Try
              strtoint(valor);
              TamanhoQteLinhas:=strtoint(valor);
          Except
              TamanhoQteLinhas:=30;
          End;

          valor:=FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadecolunas.text;

          Try
              strtoint(valor);
              TamanhoQteColunas:=strtoint(valor);
          Except
              TamanhoQteColunas:=90;
          End;

          valor:=FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadeitemsrepeticao.text;

          Try
            strtoint(valor);
            Self.QtdeRepeticaoFolha:=strtoint(valor);

          Except
            Self.QtdeRepeticaoFolha:=10;
          End;

          Rdprint.FonteTamanhoPadrao:=S17cpp;

          if (uppercase(FConfiguracoesgeraisRelatorioPersonalizado.ComboFontetamanhopadrao.text)='S20CPP')
          then Rdprint.FonteTamanhoPadrao:=S20cpp;

          if (uppercase(FConfiguracoesgeraisRelatorioPersonalizado.ComboFontetamanhopadrao.text)='S17CPP')
          then Rdprint.FonteTamanhoPadrao:=S17cpp;

          if (uppercase(FConfiguracoesgeraisRelatorioPersonalizado.ComboFontetamanhopadrao.text)='S12CPP')
          then Rdprint.FonteTamanhoPadrao:=S12cpp;

          if (uppercase(FConfiguracoesgeraisRelatorioPersonalizado.ComboFontetamanhopadrao.text)='S10CPP')
          then Rdprint.FonteTamanhoPadrao:=S10cpp;

          if (uppercase(FConfiguracoesgeraisRelatorioPersonalizado.ComboFontetamanhopadrao.text)='S05CPP')
          then Rdprint.FonteTamanhoPadrao:=S05cpp;

          Rdprint.TamanhoQteLPP:=Oito;

          if (uppercase(FConfiguracoesgeraisRelatorioPersonalizado.ComboEntrelinhas.text)= 'SEIS')
          then Rdprint.TamanhoQteLPP:=seis;


          if (FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhoemtodasasfolhas.ItemIndex=1)
          Then Self.Imprime_Sem_Repeticao_todas_Folhas:=True
          Else Self.Imprime_Sem_Repeticao_todas_Folhas:=False;

          if (FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhopadrao.ItemIndex=1)
          Then Self.ImprimeCabecalho:=True
          Else Self.ImprimeCabecalho:=False;


          if (FConfiguracoesgeraisRelatorioPersonalizado.comboorientacao.ItemIndex=0)
          Then Rdprint.Orientacao:=poPortrait
          Else Rdprint.Orientacao:=poLandscape;
          
     End;
end;

procedure TFrelatorioPersonalizado.GravaConfiguracoesfolha;
var
ptemp:string;
begin
    self.ObjArquivoIni.ZerarTabela;
    if (self.ObjArquivoIni.Localizanome('CONF_PAGINA_'+Self.nomeRelatorio)=False)
    then begin
              self.ObjArquivoIni.Submit_CODIGO('0');
              self.ObjArquivoIni.Status:=dsinsert;
              Self.ObjArquivoIni.Submit_nome('CONF_PAGINA_'+Self.nomeRelatorio);
    End
    Else Begin
              Self.ObjArquivoIni.TabelaparaObjeto;
              Self.ObjArquivoIni.Status:=dsEdit;
    End;

    Self.ObjArquivoIni.Arquivo.Clear;

    With Self.Rdprint do
    begin

        if (OpcoesPreview.Preview=true)
        then ptemp:='MOSTRAPREVIEW;SIM;'
        Else ptemp:='MOSTRAPREVIEW;NAO;';
        
        Self.ObjArquivoIni.Arquivo.add(ptemp);
    
        ptemp:='CAPTIONFORMULARIO;'+OpcoesPreview.CaptionPreview+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);
    
        ptemp:='QUANTIDADELINHAS;'+inttostr(TamanhoQteLinhas)+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);
    
        ptemp:='QUANTIDADECOLUNAS;'+inttostr(TamanhoQteColunas)+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);
    
    
        if (FonteTamanhoPadrao=S05cpp)
        then ptemp:='FONTETAMANHOPADRAO;S05CPP;';

        if (FonteTamanhoPadrao=S10cpp)
        then ptemp:='FONTETAMANHOPADRAO;S10CPP;';

        if (FonteTamanhoPadrao=S12cpp)
        then ptemp:='FONTETAMANHOPADRAO;S12CPP;';

        if (FonteTamanhoPadrao=S17cpp)
        then ptemp:='FONTETAMANHOPADRAO;S17CPP;';

        if (FonteTamanhoPadrao=S20cpp)
        then ptemp:='FONTETAMANHOPADRAO;S20CPP;';

        Self.ObjArquivoIni.Arquivo.add(ptemp);

        if (TamanhoQteLPP=Oito)
        Then ptemp:='ENTRELINHAS;OITO;'
        Else ptemp:='ENTRELINHAS;SEIS;';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        ptemp:='QUANTIDADEITENSREPETICAO;'+FConfiguracoesgeraisRelatorioPersonalizado.edtquantidadeitemsrepeticao.text+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        if (FConfiguracoesgeraisRelatorioPersonalizado.cbduplicaitensrepetica.Checked=True)
        Then ptemp:='DUPLICAITENSREPETICAO;SIM;'
        else ptemp:='DUPLICAITENSREPETICAO;NAO;';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        ptemp:='QTDEABAIXODUPLICAR;'+FConfiguracoesgeraisRelatorioPersonalizado.edtqtdelinhasabaixoduplica.text;
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        if (FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhoemtodasasfolhas.ItemIndex=1)
        Then ptemp:='IMPRIME_SEM_REPETICAO_EM_TODAS_FOLHAS;SIM;'
        else ptemp:='IMPRIME_SEM_REPETICAO_EM_TODAS_FOLHAS;NAO;';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        if (FConfiguracoesgeraisRelatorioPersonalizado.comboimprimecabecalhopadrao.ItemIndex=1)
        Then ptemp:='IMPRIME_CABECALHO_PADRAO;SIM;'
        else ptemp:='IMPRIME_CABECALHO_PADRAO;NAO;';
        Self.ObjArquivoIni.Arquivo.add(ptemp);


        if (FConfiguracoesgeraisRelatorioPersonalizado.comboorientacao.ItemIndex=0)
        Then ptemp:='ORIENTACAO_FOLHA;RETRATO;'
        else ptemp:='ORIENTACAO_FOLHA;PAISAGEM;';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        if (Self.ObjArquivoIni.Salvar(true)=false)
        Then Begin
                  mensagemerro('Erro na tentativa de salvar as configura��es de p�gina');
                  exit;
        End;
        MensagemAviso('Configura��es de P�gina salvas com sucesso');
    End;
End;

Procedure TfRelatorioPersonalizado.ImprimeCamposPreImpresso(Querylocal:Tibquery;pCampoRepeticao:boolean;out Plinha:integer);
Begin
     //nesse caso a impressao eh feita somente das labels e dos campos da query
     Self.ImprimeCamposPreImpresso(Querylocal,pCampoRepeticao,Plinha,nil,nil,'',false,'');
End;

Procedure TfRelatorioPersonalizado.ImprimeCamposPreImpresso(Querylocal:Tibquery;pCampoRepeticao:boolean;out Plinha:integer;PcamposImpressaoExtra:TStringList;PvalorCamposImpressaoExtra:TStringList);
begin
     Self.ImprimeCamposPreImpresso(Querylocal,pCampoRepeticao,Plinha,PcamposImpressaoExtra,PvalorCamposImpressaoExtra,'',false,'');
End;

Procedure TfRelatorioPersonalizado.ImprimeCamposPreImpresso(Querylocal:Tibquery;pCampoRepeticao:boolean;out Plinha:integer;PcamposImpressaoExtra:TStringList;PvalorCamposImpressaoExtra:TStringList;SOMENTELABELX:STring);
Begin
     Self.ImprimeCamposPreImpresso(Querylocal,pCampoRepeticao,Plinha,PcamposImpressaoExtra,PvalorCamposImpressaoExtra,SOMENTELABELX,false,'');
End;

Procedure TfRelatorioPersonalizado.ImprimeCamposPreImpresso(Querylocal:Tibquery;pCampoRepeticao:boolean;out Plinha:integer;PcamposImpressaoExtra:TStringList;PvalorCamposImpressaoExtra:TStringList;SOMENTELABELX:STring;Pignoravazio:boolean;SOMENTETIPOX:String);
var
contlinha,pposicaocampoextra,plinhalocal,pcoluna,pquantidade,cont:integer;
temp1,temp2,temp,pimprimir:String;
continua,QuebrarVariasLinhas,Pformatacaovalor,Comnegrito,Comitalico,ComAlinhamentoEsquerda,ComFormatacaoValor,ComCentralizaFolha:boolean;
QuebrarVariasLinhasIniciando_coluna_1:boolean;
tamanhofonte:ttipofonte;
PlinhaAdicionalTemp,QuantLInhasAdicionais:integer;
begin


     With Self do
     begin


         plinhalocal:=plinha;//pegando a linha que veio
         QuantLInhasAdicionais:=0;

         for cont:=2 to STRGCampos.RowCount-1 do//comeco do 2 porque a 0 � o cabecalho e a 1 � qtde
         begin
              Comnegrito:=false;
              Comitalico:=false;
              ComAlinhamentoEsquerda:=false;
              ComFormatacaoValor:=false;
              ComCentralizaFolha:=False;
              QuebrarVariasLinhas:=False;
              QuebrarVariasLinhasIniciando_coluna_1:=False;


              if (Self.rdprint.fontetamanhopadrao=S05cpp)
              Then tamanhofonte:=expandido;
              if (Self.rdprint.fontetamanhopadrao=S10cpp)
              Then tamanhofonte:=normal;
              if (Self.rdprint.fontetamanhopadrao=S12cpp)
              Then tamanhofonte:=comp12;
              if (Self.rdprint.fontetamanhopadrao=S17cpp)
              Then tamanhofonte:=comp17;
              if (Self.rdprint.fontetamanhopadrao=S20cpp)
              Then tamanhofonte:=comp20;

              continua:=True;

              if (SOMENTELABELX<>'')
              Then Begin
                        //no caso de ter vindo com esse campo preenchido � para
                        //imprimir somente essa label, seja ela de banco ou n�o
                        if (Uppercase(SOMENTELABELX)<>uppercase(STRGCampos.Cells[campovalor,cont]))
                        Then Continua:=False;
              End;

              if (SOMENTETIPOX<>'')
              Then Begin
                        if (Uppercase(SOMENTETIPOX)<>uppercase(STRGCampos.Cells[campotipo,cont]))
                        Then Continua:=False;
              End;


              if(continua=true)
              Then Begin
                      if (
                      ((STRGCampos.cells[camporepeticao,cont]='R') and (pcampoRepeticao=True))
                      or
                      ((STRGCampos.cells[camporepeticao,cont]<>'R') and (PcampoRepeticao=False))
                         )
                      Then begin
                                if (plinha=0)//se veio zero � porque � pra usar a linha que foi configurada
                                then plinhalocal:=strtoint(STRGCampos.Cells[campolinha,cont]);


                                
                                (*Else Begin
                                          //quando tenho situacoes de repeticao
                                          //antes de chamar a funcao � passado
                                          //a linha que deve ser impresso
                                          plinhalocal:=plinha;
                                End;*)

                                pcoluna:=strtoint(STRGCampos.Cells[campocoluna,cont]);
                                pquantidade:=strtoint(STRGCampos.Cells[campoquantidade,cont]);

                                Pformatacaovalor:=True;

                                if
                                (
                                  (STRGCampos.Cells[campotipo,cont]='LABEL')
                                  or
                                    ((STRGCampos.Cells[campotipo,cont]='LABELTOTALFINAL') and (SOMENTETIPOX='LABELTOTALFINAL'))
                                  or ((STRGCampos.Cells[campotipo,cont]='LABELSUBTOTAL') and (SOMENTETIPOX='LABELSUBTOTAL'))

                                )
                                Then pimprimir:=STRGCampos.Cells[campovalor,cont]
                                Else Begin


                                          Try
                                              //campo do banco de dados
                                              if (STRGCampos.Cells[campotipo,cont]='BD')

                                              Then pimprimir:=Querylocal.fieldbyname(STRGCampos.Cells[campovalor,cont]).asstring
                                              Else pimprimir:='';

                                              if (PcamposImpressaoExtra<>nil)
                                              Then Begin
                                                      pposicaocampoextra:=PcamposImpressaoExtra.IndexOf(uppercase(STRGCampos.Cells[campovalor,cont]));
                                                      if (pposicaocampoextra<>-1)
                                                      Then Begin
                                                                //TeNHo valor extra para o campo a ser impresso
                                                                Pimprimir:=PvalorCamposImpressaoExtra[pposicaocampoextra];
                                                                //tenho que evitar formatacao de valor em casos de impressao extra
                                                                //pois pode acontecer de um campo de valor total ser preenchido com ----
                                                                //exemplo: um pedido sera impresso em 3 vias, nas duas primeiras
                                                                //vias nao mostra o valor final mostro ---- somente na ultima via mostro
                                                                //o valor

                                                                Pformatacaovalor:=False;
                                                      End;

                                              End;
        
                                          Except
                                
                                          End;
                                End;





                                if (Pimprimir<>'') or (Pignoravazio=false)
                                Then Begin
                                          //configuracao (negrito,italico,completaesquerda,formatavalor....)
                                          temp:=STRGCampos.cells[campoatributos,cont];

                                          if (pos('NEGRITO',temp)<>0)
                                          then  ComNegrito:=true;

                                          if (pos('FONTE05CPP',temp)<>0)
                                          then  tamanhofonte:=expandido;

                                          if (pos('FONTE10CPP',temp)<>0)
                                          then  tamanhofonte:=normal;

                                          if (pos('FONTE12CPP',temp)<>0)
                                          then  tamanhofonte:=comp12;

                                          if (pos('FONTE17CPP',temp)<>0)
                                          then  tamanhofonte:=comp17;
                                          
                                          if (pos('FONTE20CPP',temp)<>0)
                                          then  tamanhofonte:=comp20;
                                          
                                          if (pos('FONTENORMAL',temp)<>0)
                                          then  tamanhofonte:=normal;
                                          
                                          if (pos('ITALICO',temp)<>0)
                                          then Comitalico:=true;

                                          if (pos('ALINHAMENTOESQUERDA',temp)<>0)
                                          then ComAlinhamentoEsquerda:=true;
                                          
                                          if ((pos('FORMATAVALOR',temp)<>0) and (Pformatacaovalor=True))
                                          then ComFormatacaoValor:=true;
                                          
                                          if (pos('CENTRALIZANAFOLHA',temp)<>0)
                                          Then ComCentralizaFolha:=true;
                                          
                                          if (pos('QUEBRARLINHAS',temp)<>0)
                                          Then QuebrarVariasLinhas:=True;
                                          
                                          if (pos('QUEBRARLINHAS_INICIA_COLUNA_1',temp)<>0)
                                          Then QuebrarVariasLinhasIniciando_coluna_1:=True;

                                          if (pos('QUEBRARLINHAS_INICIA_COLUNA_1',temp)<>0)
                                          Then QuebrarVariasLinhasIniciando_coluna_1:=True;

                                          if (ComFormatacaoValor=true)
                                          then begin
                                                    try
                                                          StrToCurr(pimprimir);
                                                          Try
                                                             pimprimir:=formata_valor(pimprimir);
                                                          Except
                                                             pimprimir:='';
                                                          End;
                                                    except

                                                    end;

                                          End;



                                          (*calculo quantas linhas adicionais tera e imprimo
                                          Exemplo
                                          Em caso de Repeticao
                                            Codigo: XXX
                                            Nome: YYYYY
                                            
                                            Codigo: ZZZZ
                                            Nome:JJJJ

                                          Neste exemplo os produtos de repeti��o est�o divididos em
                                          mais de uma linhas, sendo assim
                                          preciso calcular quantas linhas adicionais precisarei
                                          alem da atual (que veio na chamada do procedimento)
                                          no final aumento em X linhas a variavel da chamada
                                          pra voltar  alterada 
                                          *)



                                          PlinhaAdicionalTemp:=0;

                                          if (QuebrarVariasLinhas=False)
                                          Then Begin//Nao imprime com quebra de linha

                                                    if ((STRGCampos.cells[camporepeticao,cont]='R')
                                                    and (STRGCampos.Cells[campotipo,cont]='BD'))
                                                    Then Begin
                                                              temp:=STRGCampos.cells[campolinha,cont];//pegando a linha que foi configurada esse campo de repeticao
                                                              //se esse campo de repeticao foi configurado abaixo da primeira linha
                                                              //e ele � do tipo BD (porque tem subtotais)

                                                              if (strtoint(temp)>Self.PLinhaPrimeiroProdutoRepeticao)
                                                              Then Begin
                                                                        PlinhaAdicionalTemp:=strtoint(temp)-Self.PLinhaPrimeiroProdutoRepeticao;

                                                                        if (QuantLInhasAdicionais< plinhaadicionaltemp)
                                                                        then QuantLInhasAdicionais:=plinhaadicionaltemp;
                                                              End;
                                                    End;


                                                    if (ComCentralizaFolha=True)
                                                    then Begin
                                                              temp:=pimprimir;
                                                              pimprimir:=Centraliza_String(temp,Rdprint.TamanhoQteColunas);
                                                    End
                                                    Else Begin
                                                            if (ComAlinhamentoEsquerda=true)
                                                            then pimprimir:=CompletaPalavra_a_Esquerda(pimprimir,pquantidade,' ')
                                                            Else pimprimir:=completapalavra(pimprimir,pquantidade,' ');
                                                    End;
                                                    
                                                    if (Comnegrito=false) and (Comitalico=false)
                                                    then Rdprint.impf(plinhalocal+plinhaadicionaltemp,pcoluna,pimprimir,[tamanhofonte])
                                                    Else Begin
                                                              if (Comnegrito=true) and (Comitalico=false)
                                                              Then Rdprint.impf(plinhalocal+plinhaadicionaltemp,pcoluna,pimprimir,[negrito,tamanhofonte])
                                                              else
                                                                   if (Comnegrito=false) and (Comitalico=true)
                                                                   Then Rdprint.impf(plinhalocal+plinhaadicionaltemp,pcoluna,pimprimir,[italico,tamanhofonte])
                                                                   else Rdprint.impf(plinhalocal+plinhaadicionaltemp,pcoluna,pimprimir,[italico,negrito,tamanhofonte]);
                                                    End;

                                          End
                                          Else Begin
                                                    //***********************************************************************
                                                    //opcao de imprimir em varias linhas por enquanto nao � possivel formatar
                                                    //tenho o texto a ser impresso e a quantidade que devo imprimir
                                                    temp1:='';
                                                    temp2:='';
                                                    DividirValorCOMSEGUNDASEMTAMANHO(pimprimir,pquantidade,temp1,temp2);
                                                    contlinha:=plinhalocal;

                                                    While (temp1<>'') do
                                                    Begin

                                                           if (Comnegrito=false) and (Comitalico=false)
                                                           then Rdprint.impf(contlinha,pcoluna,temp1,[tamanhofonte])
                                                           Else Begin
                                                                     if (Comnegrito=true) and (Comitalico=false)
                                                                     Then Rdprint.impf(contlinha,pcoluna,temp1,[negrito,tamanhofonte])
                                                                     else
                                                                          if (Comnegrito=false) and (Comitalico=true)
                                                                          Then Rdprint.impf(contlinha,pcoluna,temp1,[italico,tamanhofonte])
                                                                          else Rdprint.impf(contlinha,pcoluna,temp1,[italico,negrito,tamanhofonte]);
                                                           End;

                                          
                                                           IF (QuebrarVariasLinhasIniciando_coluna_1)
                                                           Then Begin
                                                                      //como comeca na 1 o tamanho sera o tamanho anterior +1 at� a coluna
                                                                      //aumentando a qtde e jogo a coluna para 1
                                                                       if (Pcoluna>1)
                                                                      then Begin
                                                                                pquantidade:=pquantidade+pcoluna;
                                                                                pcoluna:=1;
                                          
                                                                                if (pquantidade>Self.Rdprint.TamanhoQteColunas)
                                                                                Then pquantidade:=Self.Rdprint.TamanhoQteColunas; 

                                                                      End;
                                                           End;


                                                           temp1:='';
                                                           pimprimir:=temp2;
                                                           DividirValorCOMSEGUNDASEMTAMANHO(pimprimir,pquantidade,temp1,temp2);

                                                           inc(contlinha,1);

                                                    End;//while de impressao de varias linhas
                                                    
                                                    if (contlinha>plinhalocal)
                                                    Then QuantLInhasAdicionais:=contlinha-plinhalocal;

                                          End;//else quebrar linhas
                                End;//<VAZIO>
                      End;//then begin
              End;//continua
         End;//for

         //teve alteracoes de linha ou seja, mais de uma linha
         (*If(contlinha-plinha>1)
         Then plinha:=contlinha;*)

         Plinha:=plinhalocal+QuantLInhasAdicionais;


     End;//with
End;

    
procedure TFrelatorioPersonalizado.RdprintNewPage(Sender: TObject;
  Pagina: Integer);
begin
     if (Self.ImprimeCabecalho=True)
     Then Begin
           // Cabe�alho...
           rdprint.imp (01,01,cabecalho);

           if (RDprint.FonteTamanhoPadrao=s20cpp)//160 colunas
           Then rdprint.impD(02,160,'P�gina: ' + formatfloat('000',pagina),[])
           Else
               if (RDprint.FonteTamanhoPadrao=s17cpp)//130 colunas
               Then rdprint.impD(02,130,'P�gina: ' + formatfloat('000',pagina),[])
               Else rdprint.impf(02,82,'P�gina: ' + formatfloat('000',pagina),[]);
     End;
     
     //Linha  := 8;
end;


procedure TFrelatorioPersonalizado.RetornaCamposporTipo(PTIPO: String;
  PcamposExtras: TStringList);
begin
     Self.RetornaCamposporTipo(ptipo,pcamposextras,nil);
end;



procedure TFrelatorioPersonalizado.RetornaCamposporTipo(PTIPO: String;
  PcamposExtras: TStringList;PcampoAuditora:TStringList);
var
cont:integer;
temp:Tlabel;  
begin
     pcamposextras.clear;

     if (PcampoAuditora<>nil)
     Then PcampoAuditora.clear;


    for cont:=2 to Self.STRGCampos.RowCount-1 do
    Begin
         if (Self.STRGCampos.cells[Self.campotipo,cont]=uppercase(ptipo))
         Then begin
                  PcamposExtras.add(Self.STRGCampos.cells[Self.campovalor,cont]);

                  if (PcampoAuditora<>nil)
                  Then PcampoAuditora.add(Self.STRGCampos.cells[Self.campoauditora,cont]);
         End;
    End;

end;

function TFrelatorioPersonalizado.RetornaQuantidade(PTIPO,PVALORCAMPO: string): integer;
var
cont:integer;
begin
    //percorre todos os campos em busca de um campo do tipo informado e com
     //o valor informado

     result:=0;

     for cont:=2 to STRGCampos.RowCount-1 do//comeco do 2 porque a 0 � o cabecalho e a 1 � qtde
     begin
          if ((STRGCampos.Cells[campotipo,cont]=PTIPO) AND (STRGCampos.Cells[campovalor,cont]=PVALORCAMPO))
          Then Begin
                  Try
                      result:=strtoint(STRGCampos.Cells[campoquantidade,cont]);
                  Except
                        result:=0;
                  End;
                  exit;
          End;
     End;
end;


function TFrelatorioPersonalizado.RetornaLinha(PTIPO,PVALORCAMPO: string): integer;
var
cont:integer;
begin
    //percorre todos os campos em busca de um campo do tipo informado e com
     //o valor informado

     result:=0;

     for cont:=2 to STRGCampos.RowCount-1 do//comeco do 2 porque a 0 � o cabecalho e a 1 � qtde
     begin
          if ((STRGCampos.Cells[campotipo,cont]=PTIPO) AND (STRGCampos.Cells[campovalor,cont]=PVALORCAMPO))
          Then Begin
                  Try
                      result:=strtoint(STRGCampos.Cells[campolinha,cont]);
                  Except
                        result:=0;
                  End;
                  exit;
          End;
     End;
end;

procedure TFrelatorioPersonalizado.STRGCamposDblClick(Sender: TObject);
var
temp:Tlabel;
begin
     if (Self.STRGCampos.row>2)
     then Begin
               temp:=nil;
               temp:=Self.findcomponent(Self.STRGCampos.Cells[Self.camponome,Self.strgcampos.row]) as TLabel;

               if (temp<>nil)
               then  Self.modeloClick(temp);


     End;
end;

end.

