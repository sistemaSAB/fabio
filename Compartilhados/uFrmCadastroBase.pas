unit uFrmCadastroBase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uFrmBase, ComCtrls, ToolWin, uClassBase, StdCtrls;

type
  TFrmCadastroBase = class(TFrmBase)
    ToolBar1: TToolBar;
    btNovo: TToolButton;
    ToolButton2: TToolButton;
    btAlterar: TToolButton;
    ToolButton4: TToolButton;
    btGravar: TToolButton;
    ToolButton6: TToolButton;
    btPesquisar: TToolButton;
    ToolButton8: TToolButton;
    btExcluir: TToolButton;
    btSair: TToolButton;
    ToolButton11: TToolButton;
    btCancelar: TToolButton;
    ToolButton1: TToolButton;
    btRelatorios: TToolButton;
    ToolButton3: TToolButton;
    btOpcoes: TToolButton;
    ToolButton5: TToolButton;
    procedure btSairClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btNovoClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btOpcoesClick(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaEstadoBotoes;
  protected
    objeto: TClassBase;
    FCodigoBusca: string;
  public
    { Public declarations }
    property CodigoBusca: string read FCodigoBusca write FCodigoBusca;
  end;

var
  FrmCadastroBase: TFrmCadastroBase;

implementation

uses uGlobal, UDataModulo;

{$R *.dfm}

procedure TFrmCadastroBase.btSairClick(Sender: TObject);
begin
  inherited;
  Self.Close;
end;

procedure TFrmCadastroBase.btPesquisarClick(Sender: TObject);
begin
  inherited;
  objeto.Pesquisa(CodigoBusca,true);
  CodigoBusca := '';
  AtualizaEstadoBotoes;
end;

procedure TFrmCadastroBase.FormCreate(Sender: TObject);
begin
  inherited;
  objeto.Setadatabase(FDataModulo.IBDatabase);
end;

procedure TFrmCadastroBase.FormDestroy(Sender: TObject);
begin
  inherited;
  {Por precau��o, como o objeto ser� instanciado no create dos
      formul�rios filhos, caso n�o ocorra o show do mesmo, n�o haver�
      a destrui��o pelo close.
   Ent�o aqui finaliza se ainda estiver instanciado.
  }
  if Assigned(objeto) then
    FreeAndNil(objeto);
end;

procedure TFrmCadastroBase.btGravarClick(Sender: TObject);
begin
  try
    inherited;
    habilita_campos(Self,False);
    AtualizaEstadoBotoes;
  except
    
  end;
end;

procedure TFrmCadastroBase.btExcluirClick(Sender: TObject);
begin
  inherited;
  if(objeto.Deletar(CodigoBusca,True)) then
    ShowMessage('Registro excluido');
  CodigoBusca := '';
  AtualizaEstadoBotoes;
end;

procedure TFrmCadastroBase.btNovoClick(Sender: TObject);
begin
  inherited;
  objeto.Status := dsInsert;
  limpaedit(self);
  habilita_campos(Self,true);
  AtualizaEstadoBotoes;
end;

procedure TFrmCadastroBase.btCancelarClick(Sender: TObject);
begin
  inherited;
  objeto.Cancelar;
  limpaedit(self);
  habilita_campos(Self,false);
  AtualizaEstadoBotoes;
end;

procedure TFrmCadastroBase.FormShow(Sender: TObject);
begin
  inherited;
  habilita_campos(Self,False);
  AtualizaEstadoBotoes;
end;

procedure TFrmCadastroBase.btAlterarClick(Sender: TObject);
begin
  inherited;
  objeto.Status := dsEdit;
  habilita_campos(Self,true);
  AtualizaEstadoBotoes;
end;

procedure TFrmCadastroBase.AtualizaEstadoBotoes;
begin
{� necess�rio atualizar o Status do Objeto antes de atualizar os botoes.
}
  btNovo.Enabled := objeto.Status in [dsInactive];
  btAlterar.Enabled := objeto.Status in [dsInactive];
  btGravar.Enabled := objeto.Status in [dsInsert,dsEdit];
  btExcluir.Enabled := objeto.Status in [dsInactive];
  btCancelar.Enabled := objeto.Status in [dsInsert,dsEdit];
end;

procedure TFrmCadastroBase.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if Assigned(objeto) then
    FreeAndNil(objeto);
end;

procedure TFrmCadastroBase.btOpcoesClick(Sender: TObject);
begin
  inherited;
  objeto.MostraCamposTabela;
end;

end.
