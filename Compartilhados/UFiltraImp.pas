unit UFiltraImp;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, Mask, ExtCtrls, ComCtrls;

type
  TFfiltroImp = class(TForm)
    pnl2: TPanel;
    Grupo01: TPanel;
    LbGrupo01: TLabel;
    btgrupo01: TSpeedButton;
    edtgrupo01: TMaskEdit;
    Grupo02: TPanel;
    LbGrupo02: TLabel;
    btgrupo02: TSpeedButton;
    edtgrupo02: TMaskEdit;
    Grupo03: TPanel;
    lbgrupo03: TLabel;
    btgrupo03: TSpeedButton;
    edtgrupo03: TMaskEdit;
    Grupo04: TPanel;
    LbGrupo04: TLabel;
    btgrupo04: TSpeedButton;
    edtgrupo04: TMaskEdit;
    Grupo05: TPanel;
    lbgrupo05: TLabel;
    btgrupo05: TSpeedButton;
    edtgrupo05: TMaskEdit;
    Grupo06: TPanel;
    LbGrupo06: TLabel;
    ComboGrupo06: TComboBox;
    Combo2Grupo06: TComboBox;
    Grupo07: TPanel;
    LbGrupo07: TLabel;
    Combo2Grupo07: TComboBox;
    edtgrupo07: TMaskEdit;
    ComboGrupo07: TComboBox;
    Grupo08: TPanel;
    lbgrupo08: TLabel;
    btgrupo08: TSpeedButton;
    edtgrupo08: TMaskEdit;
    grupo09: TPanel;
    lbgrupo09: TLabel;
    btgrupo09: TSpeedButton;
    edtgrupo09: TMaskEdit;
    grupo10: TPanel;
    lbgrupo10: TLabel;
    btgrupo10: TSpeedButton;
    edtgrupo10: TMaskEdit;
    grupo11: TPanel;
    lbgrupo11: TLabel;
    btgrupo11: TSpeedButton;
    edtgrupo11: TMaskEdit;
    grupo12: TPanel;
    lbgrupo12: TLabel;
    edtgrupo12: TMaskEdit;
    Grupo13: TPanel;
    lbgrupo13: TLabel;
    Combo2Grupo13: TComboBox;
    edtgrupo13: TMaskEdit;
    ComboGrupo13: TComboBox;
    Grupo14: TPanel;
    lbgrupo14: TLabel;
    Memogrupo14: TMemo;
    Grupo15: TPanel;
    lbgrupo15: TLabel;
    ComboGrupo15: TComboBox;
    pnl3: TPanel;
    btCancelar: TSpeedButton;
    Grupo16: TPanel;
    chkOP1: TCheckBox;
    chkop2: TCheckBox;
    chkop3: TCheckBox;
    chkop4: TCheckBox;
    pnlRodape: TPanel;
    Grupo17: TPanel;
    btGravar: TBitBtn;
    Img1: TImage;
    bt2: TSpeedButton;
    lb2grupo01: TLabel;
    lb2grupo03: TLabel;
    lb2grupo02: TLabel;
    lb2grupo04: TLabel;
    lb2grupo05: TLabel;
    lb2grupo08: TLabel;
    lb2grupo09: TLabel;
    lb2grupo10: TLabel;
    lb2grupo11: TLabel;
    lb2grupo12: TLabel;
    chkGrupo15: TCheckBox;
    procedure FormActivate(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure ComboGrupo07Change(Sender: TObject);
    procedure Combo2Grupo07Change(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ComboGrupo13Change(Sender: TObject);
    procedure Memogrupo14KeyPress(Sender: TObject; var Key: Char);
    procedure btgrupo01Click(Sender: TObject);
    procedure btgrupo02Click(Sender: TObject);
    procedure btgrupo03Click(Sender: TObject);
    procedure btgrupo04Click(Sender: TObject);
    procedure btgrupo05Click(Sender: TObject);
    procedure btgrupo08Click(Sender: TObject);
    procedure btgrupo09Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btgrupo10Click(Sender: TObject);
    procedure btgrupo11Click(Sender: TObject);
    procedure edtgrupo01KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btGravarClick(Sender: TObject);
  private
    procedure AbreCalendario(editenviou: TObject);
    procedure LimpaOnExit(Sender: TObject);

    { Private declarations }
  Public
    { Public declarations }
    BaseMysql:boolean;
    Procedure DesativaGrupos;
    function  MascaraData: String;
    function  MascaraHora: String;
    Function  Validadata(PGrupo:Integer;var Pdata:String;Pdesconsideraembranco:boolean):boolean;overload;
    Function  Validadata(PGrupo:Integer;var Pdata:Tdate;Pdesconsideraembranco:boolean):boolean;overload;
    procedure EdtSomenteNumerosKeyPress(Sender: TObject; var Key: Char);
  End;

var
  FfiltroImp: TFfiltroImp;


implementation

uses UessencialGlobal, Ucalendario, UescolheImagemBotao;

{$R *.DFM}

procedure TFfiltroImp.FormActivate(Sender: TObject);
var
poptop:integer;
Conta:integer;
primeiro:Boolean;
verificatamanho:boolean;
begin
     verificatamanho:=False;
   //  FescolheImagemBotao.PegaFiguraBotoes(nil,nil,nil,btgravar,nil,nil,nil,nil);
     primeiro:=false;
     Tag:=0;
     conta:=0;
     poptop:=0;//seria o primeiro grupo
     //aqui eu verifico grupo a grupo quais estao enabled=true
     //os que nao estiverem eu dou um visible=false
     //os que estiverem eu alinho-os de acordo com propriedade top
     //entre um e outro � 42
     
     If (Grupo01.Enabled=True)
     Then Begin
               Grupo01.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo01.Visible:=True;
               edtgrupo01.setfocus;
               primeiro:=True;

               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}

               if ((edtgrupo01.EditMask='!99/99/9999;1;_') or (edtgrupo01.EditMask='!99/99/9999;')or(edtgrupo01.EditMask='99/99/9999')) Then
               begin
                    btgrupo01.Visible:=True;

               end
               Else
               begin
                    btgrupo01.Visible:=False;

               end;

          End
     Else Grupo01.Visible:=False;

     If (Grupo02.Enabled=True)
     Then Begin
               Grupo02.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo02.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        edtgrupo02.setfocus;
               End;
               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}
               if ((edtgrupo02.EditMask='!99/99/9999;1;_') or (edtgrupo02.EditMask='!99/99/9999;') or(edtgrupo02.EditMask='99/99/9999'))Then
               begin
                    btgrupo02.Visible:=True;

               end
               Else
               begin
                    btgrupo02.Visible:=False;

               end;
          End
     Else Grupo02.Visible:=False;

     If (Grupo03.Enabled=True)
     Then Begin
               Grupo03.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo03.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        edtgrupo03.setfocus;
               End;
               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}
               if ((edtgrupo03.EditMask='!99/99/9999;1;_') or (edtgrupo03.EditMask='!99/99/9999;')or(edtgrupo03.EditMask='99/99/9999'))   Then
               begin
                    btgrupo03.Visible:=True;

               end
               Else
               begin
                    btgrupo03.Visible:=False;

               end
          End
     Else Grupo03.Visible:=False;

     If (Grupo04.Enabled=True) Then
     Begin
               Grupo04.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo04.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        edtgrupo04.setfocus;
               End;
               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}
               if ((edtgrupo04.EditMask='!99/99/9999;1;_') or (edtgrupo04.EditMask='!99/99/9999;')or(edtgrupo04.EditMask='99/99/9999'))
               Then
               begin
                   btgrupo04.Visible:=True;
               end
               Else
               begin
                  btgrupo04.Visible:=False;
               end;
     End
     Else Grupo04.Visible:=False;

     If (Grupo05.Enabled=True) Then
     Begin
               Grupo05.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo05.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        edtgrupo05.setfocus;
               End;
               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}
               if ((edtgrupo05.EditMask='!99/99/9999;1;_') or (edtgrupo05.EditMask='!99/99/9999;')or(edtgrupo05.EditMask='99/99/9999'))
               Then
               begin
                    btgrupo05.Visible:=True;

               end
               Else
               begin
                    btgrupo05.Visible:=False;

               end;
     End
     Else Grupo05.Visible:=False;

     If (Grupo06.Enabled=True)Then
     Begin
               Grupo06.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo06.Visible:=True;
               If (primeiro=False)
               Then Begin
                        primeiro:=True;
                        combogrupo06.setfocus;
               End;
     End
     Else Grupo06.Visible:=False;

     If (Grupo07.Enabled=True)Then
     Begin
               Grupo07.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo07.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                        edtgrupo07.setfocus;
               End;

     End
     Else Grupo07.Visible:=False;

     If (Grupo08.Enabled=True)Then
     Begin
               Grupo08.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo08.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         edtgrupo08.setfocus;
               End;
               if ((edtgrupo08.EditMask='!99/99/9999;1;_') or (edtgrupo08.EditMask='!99/99/9999;')or(edtgrupo08.EditMask='99/99/9999'))
               Then
               begin
                    btgrupo08.Visible:=True;

               end
               Else
               begin
                    btgrupo08.Visible:=False;

               end;

     End
     Else Grupo08.Visible:=False;

     If (Grupo09.Enabled=True)Then
     Begin
               Grupo09.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo09.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         edtgrupo09.setfocus;
               End;
               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}
               if ((edtgrupo09.EditMask='!99/99/9999;1;_') or (edtgrupo09.EditMask='!99/99/9999;')or(edtgrupo09.EditMask='99/99/9999'))
               Then
               begin
                    btgrupo09.Visible:=True;

               end
               Else
               begin
                   btgrupo09.Visible:=False;

               end;

     End
     Else Grupo09.Visible:=False;

     If (Grupo10.Enabled=True)Then
     Begin
               Grupo10.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo10.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         edtgrupo10.setfocus;
               End;
               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}
              if ((edtgrupo10.EditMask='!99/99/9999;1;_') or (edtgrupo10.EditMask='!99/99/9999;')or(edtgrupo10.EditMask='99/99/9999'))
              Then
              begin
                  btgrupo10.Visible:=True;

              end
              Else
              begin
                  btgrupo10.Visible:=False;

              end;


     End
     Else Grupo10.Visible:=False;

     If (Grupo11.Enabled=True)Then
     Begin
               Grupo11.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo11.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         edtgrupo11.setfocus;
               End;
               {Caso queira mudar a cor do edit na pesquisa, passe a cor direto pelo edit}
               if ((edtgrupo11.EditMask='!99/99/9999;1;_') or (edtgrupo11.EditMask='!99/99/9999;')or(edtgrupo11.EditMask='99/99/9999'))
               Then
               begin
                    btgrupo11.Visible:=True;

               end
               Else
               begin
                    btgrupo11.Visible:=False;

               end;

          End
     Else Grupo11.Visible:=False;

     If (Grupo12.Enabled=True)
     Then Begin
               Grupo12.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo12.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         edtgrupo12.setfocus;
               End;



     End
     Else Grupo12.Visible:=False;

     If (Grupo13.Enabled=True)
     Then Begin
               Grupo13.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo13.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         edtgrupo13.setfocus;
               End;

          End
     Else Grupo13.Visible:=False;

     If (Grupo14.Enabled=True)
     Then Begin
               Grupo14.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+120;
               Grupo14.Visible:=True;
               If (primeiro=False)
               Then Begin
                         primeiro:=True;
                         memogrupo14.setfocus;
               End;

          End
     Else Grupo14.Visible:=False;


     If (Grupo15.Enabled=True)
     Then Begin
       Grupo15.Top:=poptop;
       inc(conta,1);
       poptop:=poptop+42;
       Grupo15.Visible:=True;

       If (primeiro=False)
       Then Begin
                 primeiro:=True;
                ComboGrupo15.setfocus;
       End;

     End
     Else Grupo15.Visible:=False;

    { If (Grupo16.Enabled=True)
     Then Begin
               Grupo16.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo16.Visible:=True;

     End
     Else Grupo16.Visible:=False; }

     If (Grupo17.Enabled=True)
     Then Begin
               Grupo17.Top:=poptop;
               inc(conta,1);
               poptop:=poptop+42;
               Grupo17.Visible:=True;
               //btGravarClick(sender);

     End
     Else Grupo17.Visible:=False;



     FfiltroImp.Height:=conta*41;

     If (Grupo14.enabled=True)//por causa do memo
     Then FfiltroImp.Height:=FfiltroImp.Height+50;

     
     If (conta<=1)
     Then FfiltroImp.Height:=100;
     FfiltroImp.Height:=FfiltroImp.Height+50;

     {if FfiltroImp.Height<189
     Then FfiltroImp.Height:=189;}

end;

procedure TFfiltroImp.BtCancelarClick(Sender: TObject);
begin
     Tag:=0;
     close;
end;

procedure TFfiltroImp.DesativaGrupos;
begin
     limpaedit(self);
     Grupo16.Visible:=false;

     Grupo01.Enabled:=False;
     lbgrupo01.Caption:='';
     lb2grupo01.Caption:=''; //rodolfo
     edtgrupo01.EditMask:='';
     edtGrupo01.onkeydown:=nil;
     edtGrupo01.OnKeyPress:=nil;
     edtGrupo01.OnChange:=nil;
     edtGrupo01.PasswordChar:=#0;
     edtGrupo01.Color:=clWindow;

     Grupo02.Enabled:=False;
     lbgrupo02.Caption:='';
     lb2grupo02.Caption:=''; //rodolfo
     edtgrupo02.EditMask:='';
     edtGrupo02.OnKeyPress:=nil;
     edtGrupo02.onkeydown:=nil;
     edtGrupo03.OnChange:=nil;
     edtGrupo02.PasswordChar:=#0;
     edtGrupo02.Color:=clWindow;

     Grupo03.Enabled:=False;
     lbgrupo03.Caption:='';
     lb2grupo03.Caption:=''; //rodolfo
     edtgrupo03.EditMask:='';
     edtGrupo03.OnKeyPress:=nil;
     edtGrupo03.onkeydown:=nil;
     edtGrupo03.OnChange:=nil;
     edtGrupo03.PasswordChar:=#0;
     edtGrupo03.Color:=clWindow;

     Grupo04.Enabled:=False;
     lbgrupo04.Caption:='';
     lb2grupo04.Caption:=''; //rodolfo
     edtgrupo04.EditMask:='';
     edtGrupo04.OnKeyPress:=nil;
     edtGrupo04.onkeydown:=nil;
     edtGrupo04.OnChange:=nil;
     edtGrupo04.PasswordChar:=#0;
     edtGrupo04.Color:=clWindow;

     Grupo05.Enabled:=False;
     lbgrupo05.Caption:='';
     lb2grupo05.Caption:=''; //rodolfo
     edtgrupo05.EditMask:='';
     edtGrupo05.OnKeyPress:=nil;
     edtGrupo05.onkeydown:=nil;
     edtGrupo05.OnChange:=nil;
     edtGrupo05.PasswordChar:=#0;
     edtGrupo05.Color:=clWindow;

     Grupo06.Enabled:=False;
     lbgrupo06.Caption:='';

     Grupo07.Enabled:=False;
     lbgrupo07.Caption:='';
     edtgrupo07.EditMask:='';
     edtGrupo07.OnKeyPress:=nil;
     edtGrupo07.onkeydown:=nil;
     edtGrupo07.OnChange:=nil;
     edtGrupo07.PasswordChar:=#0;
     edtGrupo07.Color:=clWindow;

     Grupo08.Enabled:=False;
     lbgrupo08.Caption:='';
     lb2grupo08.Caption:=''; //rodolfo
     edtgrupo08.EditMask:='';
     edtGrupo08.OnKeyPress:=nil;
     edtGrupo08.onkeydown:=nil;
     edtGrupo08.OnChange:=nil;
     edtGrupo08.PasswordChar:=#0;
     edtGrupo08.Color:=clWindow;

     Grupo09.Enabled:=False;
     lbgrupo09.Caption:='';
     lb2grupo09.Caption:=''; //rodolfo
     edtgrupo09.EditMask:='';
     edtGrupo09.OnKeyPress:=nil;
     edtGrupo09.onkeydown:=nil;
     edtGrupo09.OnChange:=nil;
     edtGrupo09.PasswordChar:=#0;
     edtGrupo09.Color:=clWindow;

     Grupo10.Enabled:=False;
     lbgrupo10.Caption:='';
     lb2grupo10.Caption:=''; //rodolfo
     edtgrupo10.EditMask:='';
     edtGrupo10.OnKeyPress:=nil;
     edtGrupo10.onkeydown:=nil;
     edtGrupo10.OnChange:=nil;
     edtGrupo10.PasswordChar:=#0;
     edtGrupo10.Color:=clWindow;

     Grupo11.Enabled:=False;
     lbgrupo11.Caption:='';
     lb2grupo11.Caption:=''; //rodolfo
     edtgrupo11.EditMask:='';
     edtGrupo11.OnKeyPress:=nil;
     edtGrupo11.onkeydown:=nil;
     edtGrupo11.OnChange:=nil;
     edtGrupo11.PasswordChar:=#0;
     edtGrupo11.Color:=clWindow;

     Grupo12.Enabled:=False;
     lbgrupo12.Caption:='';
     lb2grupo12.Caption:=''; //rodolfo
     edtgrupo12.EditMask:='';
     edtGrupo12.OnKeyPress:=nil;
     edtGrupo12.onkeydown:=nil;
     edtGrupo12.OnChange:=nil;
     edtGrupo12.PasswordChar:=#0;
     edtGrupo12.Color:=clWindow;

     Grupo13.Enabled:=False;
     lbgrupo13.Caption:='';
     Grupo14.Enabled:=False;
     lbgrupo14.Caption:='';

     Grupo15.Enabled:=False;
     lbgrupo15.Caption:='';
     ComboGrupo15.Items.Clear;
     ComboGrupo15.ItemIndex:=-1;
     chkGrupo15.Visible := False; //Rodolfo 07/08/12


     edtgrupo01.OnExit:=Self.LimpaOnExit;
     edtgrupo02.OnExit:=Self.LimpaOnExit;
     edtgrupo03.OnExit:=Self.LimpaOnExit;
     edtgrupo04.OnExit:=Self.LimpaOnExit;
     edtgrupo05.OnExit:=Self.LimpaOnExit;
     edtgrupo07.OnExit:=Self.LimpaOnExit;
     edtgrupo08.OnExit:=Self.LimpaOnExit;
     edtgrupo09.OnExit:=Self.LimpaOnExit;
     edtgrupo10.OnExit:=Self.LimpaOnExit;
     edtgrupo11.OnExit:=Self.LimpaOnExit;
     edtgrupo12.OnExit:=Self.LimpaOnExit;
    // limpalabel(Self);
End;

procedure TFfiltroImp.ComboGrupo07Change(Sender: TObject);
begin
     Combo2Grupo07.itemindex:=ComboGrupo07.itemindex;
end;

procedure TFfiltroImp.Combo2Grupo07Change(Sender: TObject);
begin
     ComboGrupo07.itemindex:=Combo2Grupo07.itemindex;
end;

procedure TFfiltroImp.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
           Perform(Wm_NextDlgCtl,0,0);
      end
      Else
         if (key=#27)
         Then Begin
                  Self.Tag:=0;
                  Self.close;
         End;
end;

procedure TFfiltroImp.ComboGrupo13Change(Sender: TObject);
begin
     Combo2Grupo13.itemindex:=ComboGrupo13.itemindex;
end;

procedure TFfiltroImp.Memogrupo14KeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Memogrupo14.setfocus;        

end;

procedure TFfiltroImp.AbreCalendario(editenviou: TObject);
begin
     Fcalendario.EditEnviouGlobal:=editenviou;
     Fcalendario.Left:=Self.Left+Tedit(editenviou).left;
     Fcalendario.top:=Self.Top+Tedit(editenviou).top;
     Fcalendario.Calendario.Date:=now;
     Fcalendario.showmodal;
end;



procedure TFfiltroImp.btgrupo01Click(Sender: TObject);
begin
     AbreCalendario(edtgrupo01);
end;

procedure TFfiltroImp.btgrupo02Click(Sender: TObject);
begin
AbreCalendario(edtgrupo02);
end;

procedure TFfiltroImp.btgrupo03Click(Sender: TObject);
begin
AbreCalendario(edtgrupo03);
end;

procedure TFfiltroImp.btgrupo04Click(Sender: TObject);
begin
AbreCalendario(edtgrupo04);
end;

procedure TFfiltroImp.btgrupo05Click(Sender: TObject);
begin
AbreCalendario(edtgrupo05);
end;

procedure TFfiltroImp.btgrupo08Click(Sender: TObject);
begin
AbreCalendario(edtgrupo08);
end;

procedure TFfiltroImp.btgrupo09Click(Sender: TObject);
begin
AbreCalendario(edtgrupo09);
end;

procedure TFfiltroImp.FormShow(Sender: TObject);
var
  f:THandle;
begin
     //PegaCorForm(Self);
     FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');

     f:=Application.Handle;
     ForceForegroundWindow(f);
     
end;

Function TFFiltroImp.MascaraData:String;
Begin
   //usado em editmask do FFiltroImp
   result:='!99/99/9999;1;_';
End;
Function TFFiltroImp.MascaraHora:String;
Begin
   result:='!99:99;1;_';
End;



function TFfiltroImp.Validadata(PGrupo: Integer; var Pdata:String;Pdesconsideraembranco:boolean): boolean;
var
pdatatemp:Tdate;
begin
     pdata:='';
     result:=Validadata(PGrupo,Pdatatemp,Pdesconsideraembranco);
     if (result=true)
     then pdata:=datetostr(Pdatatemp);
End;

function TFfiltroImp.Validadata(PGrupo: Integer; var Pdata:Tdate;Pdesconsideraembranco:boolean): boolean;
var
Tempedit,templabel:String;
begin
     result:=False;
     with Self do
     Begin
       //o pgrupo indica o Grupo que devo validar e o pdata � o retorno

       Case PGrupo of
         1:Begin
                Tempedit:=edtgrupo01.Text;
                templabel:=LbGrupo01.caption;
         End;
         2:Begin
                Tempedit:=edtgrupo02.Text;
                templabel:=LbGrupo02.caption;
         End;
         3:Begin
                Tempedit:=edtgrupo03.Text;
                templabel:=LbGrupo03.caption;
         End;
         4:Begin
                Tempedit:=edtgrupo04.Text;
                templabel:=LbGrupo04.caption;
         End;
         5:Begin
                Tempedit:=edtgrupo05.Text;
                templabel:=LbGrupo05.caption;
         End;
         7:Begin
                Tempedit:=edtgrupo07.Text;
                templabel:=LbGrupo07.caption;
         End;
         8:Begin
                Tempedit:=edtgrupo08.Text;
                templabel:=LbGrupo08.caption;
         End;
         9:Begin
                Tempedit:=edtgrupo09.Text;
                templabel:=LbGrupo09.caption;
         End;
         10:Begin
                Tempedit:=edtgrupo10.Text;
                templabel:=LbGrupo10.caption;
         End;
       end;

       try
          if (Pdesconsideraembranco=True)
          Then Begin
                    if (comebarra(tempedit)<>'')
                    Then Pdata:=strtodate(Tempedit);
          End
          Else Pdata:=strtodate(Tempedit);

          result:=true;
       except
             Messagedlg(templabel+' Inv�lida',mterror,[mbok],0);
             exit;
       End;


     End;
end;

procedure TFfiltroImp.EdtSomenteNumerosKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not(key in['0'..'9',#8])
     Then key:=#0;
end;

procedure TFfiltroImp.btgrupo10Click(Sender: TObject);
begin
AbreCalendario(edtgrupo10);
end;

procedure TFfiltroImp.btgrupo11Click(Sender: TObject);
begin
AbreCalendario(edtgrupo11);
end;

procedure TFfiltroImp.LimpaOnExit(Sender: TObject);
begin
     // Esse procedimento fica vazio mesmo
end;
procedure TFfiltroImp.edtgrupo01KeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    if (Key = VK_Space)
    then if (TEdit(Sender).Text = '__/__/____')
         then Begin
                    if (self.BaseMysql=False)
                    then TEdit(Sender).Text:=DateToStr(RetornaDataServidor)//Firebird
                    Else TEdit(Sender).Text:=datetostr(now);//se for mysql pega a data local no PC  
         End;
end;

procedure TFfiltroImp.FormCreate(Sender: TObject);
begin
     Self.BaseMysql:=False;
     chkGrupo15.Visible := False; //Deixo escondido para nao afetar outros codigos que ja est�o funfando - Rodolfo 07/08/12
end;

procedure TFfiltroImp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     Self.BaseMysql:=False;
end;

procedure TFfiltroImp.btGravarClick(Sender: TObject);
begin
     Tag:=1;
     close;
end;

End.
