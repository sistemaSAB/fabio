unit uEspera;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, gifimage, ExtCtrls;

type
  TfEspera = class(TForm)
    lbMensagem: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
  private
    FMensagem: string;
    { Private declarations }
  public
    { Public declarations }
    property Mensagem: string read FMensagem write FMensagem;
  end;


var
  fEspera: TfEspera;

implementation

{$R *.dfm}

procedure TfEspera.FormShow(Sender: TObject);
begin
  if Self.Mensagem = '' then
    lbMensagem.Caption := 'AGUARDE...'
  else
    lbMensagem.Caption := Self.Mensagem;
  Application.ProcessMessages;
  Self.Update;
end;

procedure TfEspera.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Self.Mensagem := '';
end;

procedure TfEspera.Timer1Timer(Sender: TObject);
begin
  Application.ProcessMessages;
end;



end.
