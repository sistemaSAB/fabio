object FConfiguracoesgeraisRelatorioPersonalizado: TFConfiguracoesgeraisRelatorioPersonalizado
  Left = 152
  Top = 205
  Width = 549
  Height = 401
  Caption = 'Configura'#231#245'es Gerais - Relat'#243'rio Personalizado'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 14
    Width = 108
    Height = 14
    Caption = 'Mostra Preview? '
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 16
    Top = 44
    Width = 200
    Height = 14
    Caption = 'Caption do Formul'#225'rio de Setup'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 16
    Top = 80
    Width = 145
    Height = 14
    Caption = 'Fonte tamanho padr'#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 16
    Top = 111
    Width = 149
    Height = 14
    Caption = 'Quantidade de Colunas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label7: TLabel
    Left = 16
    Top = 141
    Width = 135
    Height = 14
    Caption = 'Quantidade de linhas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 16
    Top = 171
    Width = 79
    Height = 14
    Caption = 'Entre Linhas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 16
    Top = 238
    Width = 148
    Height = 14
    Caption = 'Duplica Itens Repeti'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label10: TLabel
    Left = 16
    Top = 208
    Width = 220
    Height = 14
    Caption = 'Quantidade de Items de Repeti'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 264
    Width = 216
    Height = 14
    Caption = 'Qtde de Linhas Abaixo ao Duplicar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 16
    Top = 291
    Width = 352
    Height = 14
    Caption = 'Imprimir Cabe'#231'alho (Sem Repeti'#231#227'o) em todas as folhas'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 17
    Top = 316
    Width = 4
    Height = 14
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label12: TLabel
    Left = 17
    Top = 314
    Width = 170
    Height = 14
    Caption = 'Imprime Cabe'#231'alho Padr'#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 17
    Top = 346
    Width = 69
    Height = 14
    Caption = 'Orienta'#231#227'o'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
  end
  object CbMostraPreview: TCheckBox
    Left = 240
    Top = 13
    Width = 33
    Height = 17
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object edtcaptionformulario: TEdit
    Left = 240
    Top = 41
    Width = 297
    Height = 21
    TabOrder = 1
    Text = 'Relat'#243'rio Personalizado'
  end
  object ComboFontetamanhopadrao: TComboBox
    Left = 240
    Top = 77
    Width = 145
    Height = 21
    ItemHeight = 13
    ItemIndex = 1
    TabOrder = 2
    Text = 'S17CPP'
    Items.Strings = (
      'S20CPP'
      'S17CPP'
      'S12CPP'
      'S10CPP'
      'S05CPP')
  end
  object edtquantidadecolunas: TEdit
    Left = 240
    Top = 108
    Width = 121
    Height = 21
    TabOrder = 3
    Text = '96'
  end
  object edtquantidadelinhas: TEdit
    Left = 240
    Top = 138
    Width = 121
    Height = 21
    TabOrder = 4
    Text = '33'
  end
  object ComboEntrelinhas: TComboBox
    Left = 240
    Top = 168
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 5
    Text = 'oito'
    Items.Strings = (
      'oito'
      'seis')
  end
  object cbduplicaitensrepetica: TCheckBox
    Left = 240
    Top = 237
    Width = 33
    Height = 17
    TabOrder = 6
  end
  object edtquantidadeitemsrepeticao: TEdit
    Left = 240
    Top = 205
    Width = 57
    Height = 21
    TabOrder = 7
    Text = '10'
  end
  object edtqtdelinhasabaixoduplica: TEdit
    Left = 240
    Top = 261
    Width = 57
    Height = 21
    TabOrder = 8
    Text = '0'
  end
  object comboimprimecabecalhoemtodasasfolhas: TComboBox
    Left = 380
    Top = 288
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 9
    Text = '   '
    Items.Strings = (
      'N'#227'o'
      'Sim')
  end
  object comboimprimecabecalhopadrao: TComboBox
    Left = 205
    Top = 313
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 10
    Text = '   '
    Items.Strings = (
      'N'#227'o'
      'Sim')
  end
  object comboorientacao: TComboBox
    Left = 101
    Top = 345
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 11
    Text = 'Retrato'
    Items.Strings = (
      'RETRATO'
      'PAISAGEM')
  end
end
