object FCODIGORECEITAESTADUAL: TFCODIGORECEITAESTADUAL
  Left = 511
  Top = 292
  Width = 786
  Height = 329
  Caption = 'Cadastro de CODIGORECEITAESTADUAL - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ImagemFundo: TImage
    Left = 0
    Top = 50
    Width = 770
    Height = 191
    Align = alClient
    Stretch = True
  end
  object LbCodigo: TLabel
    Left = 29
    Top = 76
    Width = 44
    Height = 13
    Caption = 'Codigo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object LbDescricao: TLabel
    Left = 141
    Top = 74
    Width = 64
    Height = 13
    Caption = 'Descricao'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object panelrodape: TPanel
    Left = 0
    Top = 241
    Width = 770
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object ImagemRodape: TImage
      Left = 0
      Top = 0
      Width = 770
      Height = 50
      Align = alClient
      Stretch = True
    end
  end
  object EdtCodigo: TEdit
    Left = 29
    Top = 90
    Width = 100
    Height = 19
    MaxLength = 9
    TabOrder = 1
  end
  object EdtDescricao: TEdit
    Left = 141
    Top = 90
    Width = 364
    Height = 19
    MaxLength = 500
    TabOrder = 2
  end
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 770
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 3
    DesignSize = (
      770
      50)
    object lbnomeformulario: TLabel
      Left = 464
      Top = 24
      Width = 294
      Height = 23
      Alignment = taRightJustify
      Anchors = []
      Caption = 'C'#243'digo de Receita Estadual'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Btnovo: TBitBtn
      Left = 3
      Top = -3
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btpesquisar: TBitBtn
      Left = 253
      Top = -3
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 303
      Top = -3
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
    end
    object btalterar: TBitBtn
      Left = 53
      Top = -3
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btexcluir: TBitBtn
      Left = 203
      Top = -3
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
    end
    object btgravar: TBitBtn
      Left = 103
      Top = -3
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 153
      Top = -3
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btsair: TBitBtn
      Left = 403
      Top = -3
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
    end
    object btopcoes: TBitBtn
      Left = 353
      Top = -3
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
    end
  end
end
