unit UobjORDERBYRELPERSONALIZADOextendido;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal,uobjorderbyrelpersonalizado;
//USES_INTERFACE



Type
   TObjORDERBYRELPERSONALIZADOextendido=class(TobjORderByRelPersonalizado)

          Public
                ObjDataSource:TdataSource;
                Constructor Create;override;
                Destructor  Free;override;
                Procedure PesquisaComandos(Prelatorio:string);
         Private
                ObjQueryPEsquisa:Tibquery;

   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls;

{ TObjORDERBYRELPERSONALIZADOextendido }

constructor TObjORDERBYRELPERSONALIZADOextendido.Create;
begin
  inherited;
  Self.ObjQueryPEsquisa:=TIBQuery.Create(nil);
  Self.ObjQueryPEsquisa.Database:=FDataModulo.IBDatabase;
  Self.ObjDataSource:=TDataSource.create(nil);
  Self.ObjDataSource.DataSet:=Self.ObjQueryPEsquisa;
end;

destructor TObjORDERBYRELPERSONALIZADOextendido.Free;
begin
  inherited;
     freeandnil(Self.ObjDataSource);
     freeandnil(Self.ObjQueryPEsquisa);
end;

procedure TObjORDERBYRELPERSONALIZADOextendido.PesquisaComandos(
  Prelatorio: string);
begin
     With Self.ObjQueryPEsquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select ordem,ordemdefault,nome,comando,codigo from taborderbyrelpersonalizado where relatorio='+Prelatorio);
          sql.add('order by ordemdefault');
          open;
     End;
end;

end.



