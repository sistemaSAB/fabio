unit UFRImportaDadosCaixasPAF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, ExtCtrls, StdCtrls, Gauges, UobjECF,UobjCLIENTE,UobjOPERADOR, UObjUsuarios
  ,UObjniveis,UobjCFOP,UobjVENDA,UobjPRODUTO,UobjORCAMENTO,UobjGRUPOProduto,UobjSERVICOS,UobjPREVENDA,
  UobjFORMASPAGAMENTO,UobjOPERADORACARTAO,UobjPRODNFE,UobjNFE,UobjFECHAMENTO,UobjCSOSN,UobjDETALHEREDUCAOZ
  ,UobjSUBGRUPO,UobjRECEBIMENTOVENDA,UobjBEMCLIENTE,UobjRECEBIMENTOAVULSO,UobjTOTALIZADORNAOFISCAL,UobjPRODORCAMENTO
  ,UobjTransportadora,UobjRECEBIMENTOORCAMENTO,UobjCAIXA,UobjREDUCAOZ,UessencialGlobal,UobjRECEBIMENTOPREVENDA,UobjPRODUTOSVENDA,
  UobjPRODPREVENDA,UobjESTOQUEMOVIMENTO,UobjMOVIMENTODIA,UobjParametros,Mask,UobjINDICEPRODUCAO,UobjDOCUMENTOSECF
  ,UobjALTERACAOCAIXA,UobjESTOQUEPRODUCAO,UobjMOVIMENTOECF,UobjUNIDADEMEDIDA,IBQuery,UobjSAIDARETAGUARDA;

type
  TFRImportaDadosCaixasPAF = class(TFrame)
    pnl1: TPanel;
    pnl2: TPanel;
    STRG1: TStringGrid;
    pnl3: TPanel;
    gBarradeProgresso: TGauge;
    btImportacao: TButton;
    edtDataInicial: TMaskEdit;
    edtDataFinal: TMaskEdit;
    lb1: TLabel;
    lb2: TLabel;
    btAtualizarCaixas: TButton;
    edtpath: TEdit;
    bt1: TButton;
    dlgOpen1: TOpenDialog;
    panelpafamanda: TPanel;
    btExportarVendas: TButton;
    Button2: TButton;

     Function Criar : Boolean;
     function Destruir : Boolean;
    procedure STRG1DblClick(Sender: TObject);
    procedure btImportacaoClick(Sender: TObject);
    procedure edtDataInicialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDataFinalKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btAtualizarCaixasClick(Sender: TObject);
    procedure bt1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button2Click(Sender: TObject);


  private
       //Usado para importa��o de vendas dos caixas para o caixa central
       Objvenda:TObjVENDA;
       Objorcamento:TObjORCAMENTO;
       ObjPREVENDA:TObjPREVENDA;
       ObjFECHAMENTO:TObjFECHAMENTO;
       ObjDETALHEREDUCAOZ :TObjDETALHEREDUCAOZ;
       ObjREDUCAOZ :TObjREDUCAOZ;
       ObjRECEBIMENTOVENDA:TObjRECEBIMENTOVENDA;
       ObjRECEBIMENTOAVULSO:TObjRECEBIMENTOAVULSO;
       ObjPRODORCAMENTO:TObjPRODORCAMENTO;
       ObjRECEBIMENTOORCAMENTO:TObjRECEBIMENTOORCAMENTO;
       ObjRECEBIMENTOPREVENDA:TObjRECEBIMENTOPREVENDA ;
       ObjProdutosVenda:TObjPRODUTOSVENDA;
       ObjProdPreVenda:TObjPRODPREVENDA;
       ObjMovimentoDia:TObjMOVIMENTODIA;
       Objestoquemovimento:TObjESTOQUEMOVIMENTO;
       ObjDocumentosECF:TObjDOCUMENTOSECF;
       ObjAlteracaoCaixa:TObjALTERACAOCAIXA;
       ObjEstoqueProducao:TObjESTOQUEPRODUCAO;
       ObjMovimentoECF:TObjMOVIMENTOECF;
       objSaidaRetaguarda:TObjSAIDARETAGUARDA;

       //Usado para a atualiza��o de caixas, antes era feita um a um
       //Agora todos s�o atualizados direto pelo concentrador

       ObjProduto:TObjPRODUTO;
       ObjCliente:TObjCLIENTE;
       ObjUsuario:TObjUsuarios;
       ObjNiveis:TObjniveis;
       ObjECF:TObjECF;
       ObjOperador:TObjOPERADOR;
       ObjTransportadora:TObjTRANSPORTADORA;
       ObjCFOP:TObjCFOP;
       ObjServico:TObjSERVICOS;
       ObjGrupoProduto:TObjGRUPOPRODUTO;
       ObjSubgrupo:TObjSUBGRUPO;
       ObjFormasPagamento:TObjFORMASPAGAMENTO;
       ObjOperadoracartao:TObjOPERADORACARTAO;
       ObjTotalizadorNaofiscal:TObjTOTALIZADORNAOFISCAL; {tipo de recebimento}
       ObjCsosn:TObjCSOSN;
       ObjCaixa:TObjCAIXA;
       Objbemcliente:TObjBEMCLIENTE;
       Objparametros:TObjParametros;
       ObjIndiceProducao:TObjINDICEPRODUCAO;
       ObjUnidadeMedida:TObjUNIDADEMEDIDA;




       procedure Carrega_CaminhoCaixas;
       function ConectaBanco(Path : string; Silencioso : boolean): boolean;
       function ImportaDados:Boolean;
       function AtualizaDadosPeloServidor:Boolean;
       function AtualizaDadosPeloArquivo(SomenteCaixaCentral:Boolean):Boolean;

       function ImportaVenda:Boolean;
       function Importaorcamento:Boolean;
       function ImportaPreVenda:Boolean;
       function ImportaFechamento:Boolean;
       function ImportaDetalheReducaoZ:Boolean;
       function ImportaReducaoZ:Boolean;
       function ImportaRecebimentoVenda:Boolean;
       Function ImportaRecebimentoPreVenda:Boolean;
       function ImportaRecebimentoAvulso:Boolean;
       function ImportaProdOrcamento:Boolean;
       function ImportaRecebimentoOrcamento:Boolean;
       function ImportaProdutosVenda:Boolean;
       function ImportaProPreVenda:Boolean;
       function ImportaMovimentoDia:Boolean;
       function ImportaEstoqueMovimento:Boolean;
       function ImportaDocumentosECF:Boolean;
       function ImportaAlteracaoCaixa:Boolean;
       function ImportaEstoqueProducao:Boolean;
       function ImportaMovimentoECF: Boolean;

       function AtualizaTotalizadorNaoFiscal:Boolean;
       function AtualizaTransportadora:Boolean;
       Function AtualizaUsuarios:Boolean;
       Function AtualizaClientes:Boolean;
       Function AtualizaFormasPagamento:Boolean;
       Function AtualizaServicos:Boolean;
       function AtualizaParametros:Boolean;
       Function AtualizaProdutos:Boolean;
       function AtualizaIndiceProducao:Boolean;
       function AtualizaUnidadeMedida:Boolean;

       function AtualizaEstoque(pArquivo:string):Boolean;
       function VerificaEntrada(pNF,pCnpj:string;pquery:tibquery):Boolean;

  public
    { Public declarations }
  end;

implementation

uses UDataModuloAmanda,UDataModulo,DB;

{$R *.dfm}

function TFRImportaDadosCaixasPAF.Criar:Boolean;
begin
     Carrega_CaminhoCaixas;
     gBarradeProgresso.Progress := 0;
end;


function TFRImportaDadosCaixasPAF.Destruir:Boolean;
begin
    //Destruir Objetos
end;


procedure TFRImportaDadosCaixasPAF.Carrega_CaminhoCaixas;
var
  Query:TIBQuery;
begin
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;

      try
           STRG1.RowCount:= 2;
           STRG1.ColCount:= 3;
           STRG1.FixedRows:=1;
           STRG1.FixedCols:=0;
           STRG1.ColWidths[0] := 50;
           STRG1.ColWidths[1] := 350;
           STRG1.ColWidths[2] := 350;
           STRG1.Cells[0,0] := ' X ';
           STRG1.Cells[1,0] := 'CAIXA';
           STRG1.Cells[2,0] := 'PATH';

           with Query do
           begin
                Close;
                SQL.Clear;
                SQL.Add('select * from tabcaixa');
                Open;

                while not eof do
                begin
                     STRG1.Cells[0,STRG1.RowCount-1] := '';
                     STRG1.Cells[1,STRG1.RowCount-1] := fieldbyname('observacao').AsString;
                     STRG1.Cells[2,STRG1.RowCount-1] := fieldbyname('path').AsString;
                     STRG1.RowCount:=STRG1.RowCount+1;

                     Next;
                end;
                STRG1.RowCount:=STRG1.RowCount-1;
           end;
      finally
           FreeAndNil(query);
      end;
end;



procedure TFRImportaDadosCaixasPAF.STRG1DblClick(Sender: TObject);
begin
        if(STRG1.Cells[0,STRG1.Row]='X') then
        begin
           STRG1.Cells[0,STRG1.Row]:='';
        end
        else
        begin
           STRG1.Cells[0,STRG1.Row]:='X'
        end;

end;

procedure TFRImportaDadosCaixasPAF.btImportacaoClick(Sender: TObject);
var
  i:Integer;
begin
      //inicia importa��o, tem que verificar os caixas marcados,
      //conectar um a um e fazer a importa��o

      i:=1;
      gBarradeProgresso.MaxValue:=STRG1.RowCount;
      gBarradeProgresso.MinValue:=0;
      gBarradeProgresso.Progress:=0;
      while i<STRG1.RowCount do
      begin
          gBarradeProgresso.Progress:=gBarradeProgresso.Progress+1;
          Application.ProcessMessages;

          //Verifica no stringgrid quais caixas foram marcados
          if(STRG1.Cells[0,i]='X') then
          begin
             //Antes de se conectar no caixa, verifica se o path do caixa n�o esta vazio
             if(STRG1.Cells[2,i]<>'') then
             begin
                 //Conecta ao banco de dados do caixa
                 if(self.ConectaBanco(STRG1.Cells[2,i],True)=False) then
                 begin
                      MensagemErro('Erro ao tentar conectar o banco de dados do caixa '+STRG1.Cells[1,i]);
                      gBarradeProgresso.Progress := 0;

                      Exit;
                 end;

                 //importa os dados do caixa
                 if(ImportaDados=False) then
                 begin
                     MensagemErro('Erro na importa��o de dados do caixa '+STRG1.Cells[1,i]);
                     FDataModuloAmanda.IBTransactionAmanda.RollbackRetaining;
                     FDataModulo.IBTransaction.RollbackRetaining;
                     //FDataModulo.IBTransactionDAV.RollbackRetaining;
                     gBarradeProgresso.Progress := 0;
                     edtDataFinal.Text:='';
                     edtDataInicial.Text:='';
                     Exit;
                 end;

                 FDataModuloAmanda.IBTransactionAmanda.CommitRetaining;
                 FDataModulo.IBTransaction.CommitRetaining;
                 //FDataModulo.IBTransactionDAV.CommitRetaining;

             end;
             STRG1.Cells[0,i]:='';
          end;
          Inc(i,1);
      end;
      gBarradeProgresso.Progress:=gBarradeProgresso.Progress+1;
      Application.ProcessMessages;
      MensagemSucesso('Importa��o Conclu�da!');
      gBarradeProgresso.Progress := 0;
      edtDataFinal.Text:='';
      edtDataInicial.Text:='';


end;

function TFRImportaDadosCaixasPAF.ConectaBanco(Path : string; Silencioso : boolean): boolean;
var
  Nomelogin,senhalogin:string;
begin
      Result := False;

      Nomelogin:='PROTECAO';
      senhalogin:='PROTECAO';
      FDataModuloAmanda.IbDatabaseAmanda.Close;
      FDataModuloAmanda.IbDatabaseAmanda.Databasename:=Path;
      FDataModuloAmanda.IbDatabaseAmanda.Params.clear;
      FDataModuloAmanda.IbDatabaseAmanda.Params.Add('User_name='+nomeLogin);
      FDataModuloAmanda.IbDatabaseAmanda.Params.Add('password='+senhaLogin);
      Try
         FDataModuloAmanda.IbDatabaseamanda.Open ;
         If(not Silencioso)
         Then Messagedlg('Conex�o Efetuada com Sucesso!',mtInformation,[mbok],0);
      Except
         on E: Exception do
         Begin
              Messagedlg('Erro na tentativa de Conectar-se a base! Erro : '+#13+E.Message,mterror,[mbok],0);
              exit;
         End;
      End;
      Result := True;
end;

function TFRImportaDadosCaixasPAF.ImportaDados:Boolean;
begin
     //Importa dados
     //*** ORDEM DE IMPORTA��O
     {

       - Objorcamento
       - Objprevenda
       - Objvenda
       - Objprodnfe
       - Objnfe
       - Objfechamento
       - ObjdetalhereducaoZ
       - ObjreducaoZ
       - Objrecebimentovenda
       - Objrecebimentoprevenda
       - Objrecebimentoavulso
       - Objprodorcamento
       - Objrecebimentoorcamento

     }

     Result:=False;

     //Importa dados tabela a tabela

     if(Importaorcamento=False) then
     begin
         MensagemErro('Erro ao tentar importar os or�amentos');
         Exit;
     end;

     if(ImportaPreVenda=False) then
     begin
          MensagemErro('Erro ao tentar importar as Pr� Vendas');
          Exit;
     end;

     if(ImportaFechamento=False) then
     begin
        MensagemErro('Erro ao tentar importar dados da TabFechamento');
        Exit;
     end;


     if(ImportaVenda=False) then
     begin
         MensagemErro('Erro ao tentar importar as vendas');
         Exit;
     end;

     if(ImportaReducaoZ=False) then
     begin
         MensagemErro('Erro ao tentar importar dados da TabReducaoZ');
         Exit;
     end;

     if(ImportaDetalheReducaoZ=False) then
     begin
         MensagemErro('Erro ao tentar importar dados da TabdetalheReducaoZ');
         Exit;
     end;

     if(ImportaRecebimentoAvulso=False) then
     begin
        MensagemErro('Erro ao tentar importar os recebimentos avulsos');
        Exit;
     end;

     if(ImportaRecebimentoVenda=False) then
     begin
         MensagemErro('Erro ao tentar importar os Recebimentos da Venda');
         Exit;
     end;

     if(ImportaRecebimentoPreVenda=False) then
     begin
        MensagemErro('Erro ao tentar importar os recebimentos da Pre Venda');
        Exit;
     end;


     if(ImportaProdOrcamento=False) then
     begin
        MensagemErro('Erro ao tentar importar os Produtos do Orcamento');
        Exit;
     end;

     if(ImportaRecebimentoOrcamento=False) then
     begin
        MensagemErro('Erro ao tentar importar os recebimentos do Orcamento');
        Exit;
     end;

     if(ImportaProdutosVenda=false) then
     begin
         MensagemErro('Erro ao tentar importar os produtos da venda');
         Exit;
     end;

     if(ImportaProPreVenda=False)then
     begin
         MensagemErro('Erro ao tentar importar os produtos da prevenda');
         Exit;
     end;

     if(ImportaMovimentoDia=False)then
     begin
         MensagemErro('Erro ao tentar importar os dados do Movimento Dia');
         Exit;
     end;

     if(ImportaEstoqueMovimento=False)then
     begin
         MensagemErro('Erro ao tentar importar dados do Estoque por Movimento');
         Exit;
     end;

     if(ImportaDocumentosECF=False)then
     begin
         MensagemErro('Erro ao tentar importar os Documentos ECF');
         Exit;
     end;

     if(ImportaAlteracaoCaixa=False)then
     begin
         MensagemErro('Erro ao tentar importar os dados de Altera��o de Caixa');
         Exit;
     end;

     if(ImportaEstoqueProducao=False)then
     begin
         MensagemErro('Erro ao tentar importar os dados de Produ��o de Estoque');
         Exit;
     end;

     if(ImportaMovimentoECF=False)then
     begin
         MensagemErro('Erro ao tentar importar os dados de Movimento do ECF');
         Exit;
     end;

     Result:=True;


end;

function TFRImportaDadosCaixasPAF.ImportaVenda:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     Objvenda:=TObjVENDA.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     Result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;

              sql.Add('select * from tabvenda where exportado=''N'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              while not eof do
              begin
                  Objvenda.ZerarTabela;
                  if not(Objvenda.LocalizaCodigo(QueryLocal.fieldbyname('codigo').AsString)) then
                  begin
                    Objvenda.Status:=dsInsert;
                  end
                  else begin
                    Objvenda.Status:=dsEdit;
                  end;

                  Objvenda.Submit_CODIGO(fieldbyname('codigo').AsString);
                  Objvenda.Submit_CUPOM(fieldbyname('cupom').AsString);
                  Objvenda.submit_exportado('S');
                  Objvenda.submit_exporta(fieldbyname('exporta').AsString);
                  Objvenda.Submit_NF(fieldbyname('nf').AsString);
                  Objvenda.Submit_DATA(fieldbyname('data').AsString);
                  Objvenda.Submit_VALORTOTAL(fieldbyname('valortotal').AsString);
                  Objvenda.Submit_DESCONTO(QueryLocal.fieldbyname('desconto').AsString);
                  Objvenda.Submit_JUROS(fieldbyname('juros').AsString);
                  Objvenda.Submit_EMISSAONF(fieldbyname('emissaonf').AsString);
                  Objvenda.Submit_BASECALCULOICMS(fieldbyname('basecalculoicms').AsString);
                  Objvenda.Submit_VALORICMS(fieldbyname('valoricms').AsString);
                  Objvenda.Submit_BASECALCULOICMS_SUBSTITUICAO(fieldbyname('BASECALCULOICMS_SUBSTITUICAO').AsString);
                  Objvenda.Submit_VALORICMS_SUBSTITUICAO(fieldbyname('valoricms_substituicao').AsString);
                  Objvenda.Submit_VALORFRETE(fieldbyname('valorfrete').AsString);
                  Objvenda.Submit_VALORSEGURO(fieldbyname('valorseguro').AsString);
                  Objvenda.Submit_OUTRASDESPESAS(fieldbyname('outrasdespesas').AsString);
                  Objvenda.Submit_VALORTOTALIPI(fieldbyname('valortotalipi').AsString);
                  Objvenda.Submit_NomeTransportadora(fieldbyname('nometransportadora').AsString);
                  Objvenda.Submit_FreteporContaTransportadora(fieldbyname('freteporcontatransportadora').AsString);
                  Objvenda.Submit_PlacaVeiculoTransportadora(fieldbyname('placaveiculotransportadora').AsString);
                  Objvenda.Submit_UFVeiculoTransportadora(fieldbyname('UFveiculotransportadora').AsString);
                  objvenda.Submit_CNPJTransportadora(fieldbyname('cnpjtransportadora').AsString);
                  Objvenda.Submit_EnderecoTransportadora(fieldbyname('enderecotransportadora').AsString);
                  Objvenda.Submit_MunicipioTransportadora(fieldbyname('municipiotransportadora').AsString);
                  Objvenda.Submit_UFTransportadora(fieldbyname('uftransportadora').AsString);
                  Objvenda.Submit_IETransportadora(fieldbyname('ietransportadora').AsString);
                  Objvenda.Submit_Quantidade(fieldbyname('quantidade').AsString);
                  Objvenda.Submit_Especie(fieldbyname('especie').AsString);
                  Objvenda.Submit_Marca(fieldbyname('marca').AsString);
                  Objvenda.Submit_Numero(fieldbyname('numero').AsString);
                  Objvenda.Submit_PesoBruto(fieldbyname('pesobruto').AsString);
                  Objvenda.Submit_PesoLiquido(fieldbyname('pesoliquido').AsString);
                  Objvenda.Submit_DadosAdicionais(fieldbyname('dadosadicionais').AsString);
                  objvenda.Submit_OBSERVACOES(fieldbyname('observacoes').AsString);
                  objvenda.Submit_DAV(fieldbyname('dav').AsString);
                  Objvenda.Submit_CONTADORCUPOMFISCAL(fieldbyname('contadorcupomfiscal').AsString);
                  objvenda.Submit_DATAECF(fieldbyname('dataecf').AsString);
                  Objvenda.Submit_TIPODESCONTO(fieldbyname('tipodesconto').AsString);
                  objvenda.Submit_TIPOACRESCIMO(fieldbyname('tipoacrescimo').AsString);
                  objvenda.Submit_CANCELADO(fieldbyname('cancelado').AsString);
                  objvenda.Submit_CANCELAMENTOACRESCIMO(fieldbyname('cancelamentoacrescimo').AsString);
                  objvenda.Submit_ORDEMDESCONTOACRESCIMO(fieldbyname('ordemdescontoacrescimo').AsString);
                  objvenda.Submit_DataHoraCancelamento(fieldbyname('datahoracancelamento').AsString);
                  objvenda.Submit_UsuarioCancelamento(fieldbyname('usuariocancelamento').AsString);
                  Objvenda.Submit_MODELONF(fieldbyname('modelonf').AsString);
                  Objvenda.Submit_SERIENF(fieldbyname('serienf').AsString);
                  Objvenda.Submit_SUBSERIENF(fieldbyname('subserienf').AsString);
                  Objvenda.Submit_CANCELAMENTOEXPORTADO(fieldbyname('CANCELAMENTOEXPORTADO').AsString);
                  Objvenda.Submit_NUMEROPREVENDA(fieldbyname('NUMEROPREVENDA').AsString);
                  Objvenda.CLIENTE.Submit_CODIGO(fieldbyname('cliente').AsString);
                  Objvenda.Fechamento.Submit_CODIGO(fieldbyname('fechamento').AsString);
                  Objvenda.CFOP.Submit_CODIGO(fieldbyname('cfop').AsString);
                  objvenda.Transportadora.Submit_CODIGO(fieldbyname('transportadora').AsString);
                  Objvenda.OPERADORAVULSO.Submit_CODIGO(fieldbyname('operadoravulso').AsString);
                  Objvenda.ECF.Submit_Codigo(fieldbyname('ecf').AsString);
                  Objvenda.ORCAMENTO.Submit_CODIGO(fieldbyname('orcamento').AsString);
                  Objvenda.PREVENDA.Submit_CODIGO(fieldbyname('prevenda').AsString);
                  Objvenda.Submit_HORAECF(fieldbyname('HORAECF').AsString);

                  Objvenda.Submit_NUMEROSERIE(fieldbyname('NUMEROSERIE').AsString);
                  Objvenda.Submit_NUMEROSERIECRIPTO(fieldbyname('NUMEROSERIECRIPTO').AsString);
                  Objvenda.Submit_CONTADORCUPOMFISCALCRIPTO(fieldbyname('CONTADORCUPOMFISCALCRIPTO').AsString);
                  Objvenda.Submit_CUPOMCRIPTO(fieldbyname('CUPOMCRIPTO').AsString);
                  Objvenda.Submit_VALORFINALCRIPTO(fieldbyname('VALORFINALCRIPTO').AsString);
                  Objvenda.Submit_CANCELADOCRIPTO(fieldbyname('CANCELADOCRIPTO').AsString);

                  if(Objvenda.Salvar(false)=False)then
                  begin
                      Result:=False;
                      Exit;
                  end;

                  QueryAux.Close;
                  QueryAux.SQL.Clear;
                  QueryAux.SQL.Add('update tabvenda set exportado=''S'' ');
                  QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                  QueryAux.ExecSQL;

                  next;
              end;

              Result:=True;
         end;

     finally
         Objvenda.Free;
         FreeAndNil(QueryLocal);
         FreeAndNil(QueryAux);
     end;

end;

function TFRImportaDadosCaixasPAF.Importaorcamento:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     Objorcamento:=TObjORCAMENTO.Create;
     
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select * from taborcamento where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;
              SQL.Add('ORDER BY DAVMESCLADO,CODIGO');
              
              Open;

              while not Eof do
              begin
                   if not(Objorcamento.LocalizaCodigo(fieldbyname('codigo').AsString)) then
                   begin
                     Objorcamento.Status:=dsInsert;
                   end
                   else begin
                     Objorcamento.Status:=dsEdit;
                   end;

                   Objorcamento.Submit_CODIGO(fieldbyname('codigo').AsString);
                   Objorcamento.Submit_NOME(fieldbyname('nome').AsString);
                   Objorcamento.Submit_DATACADASTRO(fieldbyname('datacadastro').AsString);
                   Objorcamento.Submit_CONCLUIDO(fieldbyname('concluido').AsString);
                   Objorcamento.Submit_VALORTOTAL(fieldbyname('valortotal').AsString);
                   Objorcamento.Submit_DESCONTO(fieldbyname('desconto').AsString);
                   Objorcamento.Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                   Objorcamento.Submit_HISTORICO(fieldbyname('historico').AsString);
                   Objorcamento.Submit_COO(fieldbyname('coo').AsString);
                   Objorcamento.Submit_DAV(fieldbyname('dav').AsString);
                   Objorcamento.Submit_CANCELADO(fieldbyname('cancelado').AsString);
                   Objorcamento.Submit_REFERENCIA(fieldbyname('referencia').AsString);
                   Objorcamento.Submit_DADOSADICIONAIS(fieldbyname('dadosadicionais').AsString);
                   Objorcamento.Submit_exportado('S');
                   Objorcamento.Submit_COOGERENCIAL(fieldbyname('coogerencial').AsString);
                   Objorcamento.Submit_ACRESCIMO(fieldbyname('acrescimo').AsString);
                   Objorcamento.Submit_coocripto(fieldbyname('coocripto').AsString);
                   Objorcamento.Submit_Davcripto(fieldbyname('davcripto').AsString);
                   Objorcamento.Submit_datacadastrocripto(fieldbyname('datacadastrocripto').AsString);
                   Objorcamento.Submit_valorfinalcripto(fieldbyname('valorfinalcripto').AsString);
                   Objorcamento.Submit_nomecripto(fieldbyname('nomecripto').AsString);
                   Objorcamento.Submit_TITULOFISCAL(fieldbyname('titulofiscal').AsString);
                   Objorcamento.Submit_DAVMESCLADO(fieldbyname('davmesclado').AsString);
                   Objorcamento.Submit_CLIENTE_NOME(fieldbyname('cliente_nome').AsString);
                   Objorcamento.Submit_CLIENTE_NOMECRIPTO(fieldbyname('nomecripto').AsString);
                   Objorcamento.Submit_CLIENTE_CNPJ(fieldbyname('cliente_cnpj').AsString);
                   Objorcamento.Submit_CLIENTE_CNPJCRIPTO(fieldbyname('CLIENTE_CNPJCRIPTO').AsString);
                   Objorcamento.Submit_ECF_NUMEROSEQUENCIAL(fieldbyname('ecf_numerosequencial').AsString);
                   Objorcamento.Submit_ECF_NUMEROSEQUENCIALCRIPTO(fieldbyname('ecf_numerosequencialcripto').AsString);
                   Objorcamento.Submit_CODIGOCRIPTO(fieldbyname('codigocripto').AsString);
                   Objorcamento.Submit_COOGERENCIALCRIPTO(fieldbyname('coogerencialcripto').AsString);
                   Objorcamento.Submit_CCF(fieldbyname('ccf').AsString);
                   Objorcamento.CODIGO_OPERADOR.Submit_CODIGO(fieldbyname('codigo_operador').AsString);
                   objorcamento.CODIGO_CLIENTE.Submit_CODIGO(fieldbyname('codigo_cliente').AsString);
                   Objorcamento.ECF.Submit_Codigo(fieldbyname('ecf').AsString);
                   Objorcamento.BEMCLIENTE.Submit_CODIGO(fieldbyname('bemcliente').AsString);
                   Objorcamento.Submit_NUMEROMESA(fieldbyname('NUMEROMESA').AsString);
                   Objorcamento.Submit_CONFERENCIACOOCRG(QueryLocal.fieldbyname('CONFERENCIACOOCRG').AsString);
                   Objorcamento.Submit_CONFERENCIACOO(fieldbyname('CONFERENCIACOO').AsString);
                   Objorcamento.Submit_CONFERENCIASEQUENCIAECF(fieldbyname('CONFERENCIASEQUENCIAECF').AsString);
                   Objorcamento.Submit_HORACADASTRO(fieldbyname('HORACADASTRO').AsString);

                   if(Objorcamento.Salvar(false)=false)then
                   begin
                       Result:=False;
                       Exit;
                   end;

                   QueryAux.Close;
                   QueryAux.SQL.Clear;
                   QueryAux.SQL.Add('update taborcamento set exportado=''S'' ');
                   QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                   QueryAux.ExecSQL;

                   Next;
              end;
              result:=True;
         end;

     finally
        Objorcamento.Free;
        FreeAndNil(QueryLocal);
        FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaPreVenda:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjPREVENDA:=TObjPREVENDA.Create;
     
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     Result:=False;
     try
         with QueryLocal do
         begin
             Close;
             SQL.Clear;
             SQL.Add('select * from tabprevenda where exportado<>''S'' ') ;
             if(edtDataInicial.Text<>'  /  /    ')
             then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
             if(edtDataFinal.Text<>'  /  /    ')
             then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;
             SQL.Add('ORDER BY PREVENDAMESCLADO,CODIGO');
             Open;

             while not eof do
             begin
                 if(ObjPREVENDA.LocalizaCodigo(fieldbyname('codigo').AsString)=False)
                 then ObjPREVENDA.Status:=dsInsert
                 else ObjPREVENDA.Status:=dsEdit;

                  ObjPREVENDA.Submit_CODIGO(fieldbyname('codigo').AsString);
                  ObjPREVENDA.Submit_NOME(fieldbyname('nome').AsString);
                  ObjPREVENDA.Submit_DATACADASTRO(fieldbyname('datacadastro').AsString);
                  ObjPREVENDA.Submit_CONCLUIDO(fieldbyname('concluido').AsString);
                  ObjPREVENDA.Submit_VALORTOTAL(fieldbyname('valortotal').AsString);
                  ObjPREVENDA.Submit_DESCONTO(fieldbyname('desconto').AsString);
                  ObjPREVENDA.Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                  ObjPREVENDA.Submit_HISTORICO(fieldbyname('historico').AsString);
                  ObjPREVENDA.Submit_COO(fieldbyname('coo').AsString);
                  ObjPREVENDA.Submit_PREVENDA(fieldbyname('prevenda').AsString);
                  ObjPREVENDA.Submit_CANCELADO(fieldbyname('cancelado').AsString);
                  ObjPREVENDA.Submit_REFERENCIA(fieldbyname('referencia').AsString);
                  ObjPREVENDA.Submit_DADOSADICIONAIS(fieldbyname('dadosadicionais').AsString);
                  ObjPREVENDA.Submit_exportado('S');
                  ObjPREVENDA.Submit_ACRESCIMO(fieldbyname('acrescimo').AsString);
                  ObjPREVENDA.Submit_PREVENDAMESCLADO(fieldbyname('prevendamesclado').AsString);
                  ObjPREVENDA.CODIGO_OPERADOR.Submit_CODIGO(fieldbyname('codigo_operador').AsString);
                  ObjPREVENDA.CODIGO_CLIENTE.Submit_CODIGO(fieldbyname('codigo_cliente').AsString);
                  ObjPREVENDA.ECF.Submit_Codigo(fieldbyname('ecf').AsString);

                  if(ObjPREVENDA.Salvar(False)=False)then
                  begin
                       Result:=False;
                       Exit;
                  end;

                  QueryAux.Close;
                  QueryAux.SQL.Clear;
                  QueryAux.SQL.Add('update tabprevenda set exportado=''S'' ');
                  QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                  QueryAux.ExecSQL;

                 Next ;
             end;
             result:=True;
         end;

     finally
         ObjPREVENDA.Free;
         FreeAndNil(QueryLocal);
         FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaFechamento:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjFECHAMENTO:=TObjFECHAMENTO.Create;
     
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     result:=False;
     try
          with QueryLocal do
          begin
              Close;
              SQL.Clear;
              SQL.Add('select  * from tabfechamento where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;


              Open;

              while not Eof do
              begin
                    if(ObjFECHAMENTO.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                    begin
                          ObjFECHAMENTO.Status:=dsInsert;
                          ObjFECHAMENTO.Submit_CODIGO(fieldbyname('codigo').AsString);
                          ObjFECHAMENTO.Submit_DataAbertura(fieldbyname('dataabertura').AsString);
                          ObjFECHAMENTO.Submit_HoraAbertura(fieldbyname('horaabertura').AsString);
                          ObjFECHAMENTO.Submit_Statusfechamento(fieldbyname('statusfechamento').AsString);
                          ObjFECHAMENTO.Submit_DataFechamento(fieldbyname('datafechamento').AsString);
                          ObjFECHAMENTO.Submit_HoraFechamento(fieldbyname('horafechamento').AsString);
                          ObjFECHAMENTO.Submit_TotalVendido(fieldbyname('totalvendido').AsString);
                          ObjFECHAMENTO.Submit_TotalFisico(fieldbyname('totalfisico').AsString);
                          ObjFECHAMENTO.Submit_TotalSangria(fieldbyname('totalsangria').AsString);
                          ObjFECHAMENTO.Submit_TotalSuprimento(fieldbyname('totalsuprimento').AsString);
                          ObjFECHAMENTO.submit_exportado('S');
                          ObjFECHAMENTO.Operador.Submit_CODIGO(fieldbyname('operador').AsString);
                          ObjFECHAMENTO.Caixa.Submit_CODIGO(fieldbyname('caixa').AsString);

                          if(ObjFECHAMENTO.Salvar(False)=False) then
                          begin
                              Result:=False;
                              Exit;
                          end;
                                                    
                          QueryAux.Close;
                          QueryAux.SQL.Clear;
                          QueryAux.SQL.Add('update tabfechamento set exportado=''S'' ');
                          QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                          QueryAux.ExecSQL;

                    end;
                    Next;
              end;
              Result:=True;
          end;
     finally
          ObjFECHAMENTO.Free;
          FreeAndNil(QueryLocal);
          FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaDetalheReducaoZ:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjDETALHEREDUCAOZ:=TObjDETALHEREDUCAOZ.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     result:=False;
     try
        with QueryLocal do
        begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabdetalhereducaoz where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              while not eof do
              begin
                   if(ObjDETALHEREDUCAOZ.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                   begin
                           ObjDETALHEREDUCAOZ.Status:=dsInsert;
                           ObjDETALHEREDUCAOZ.Submit_CODIGO(fieldbyname('codigo').AsString);
                           ObjDETALHEREDUCAOZ.Submit_TOTALIZADORPARCIAL(fieldbyname('totalizadorparcial').AsString);
                           ObjDETALHEREDUCAOZ.Submit_exportado('S');
                           ObjDETALHEREDUCAOZ.Submit_VALORACUMULADO(fieldbyname('valoracumulado').AsString);
                           ObjDETALHEREDUCAOZ.REDUCAOZ.Submit_CODIGO(fieldbyname('reducaoz').AsString);
                           ObjDETALHEREDUCAOZ.Submit_NUMEROSERIE(fieldbyname('NUMEROSERIE').AsString);
                           ObjDETALHEREDUCAOZ.Submit_NUMEROSERIECRIPTO(fieldbyname('NUMEROSERIECRIPTO').AsString);
                           ObjDETALHEREDUCAOZ.Submit_CRZ(fieldbyname('CRZ').AsString);
                           ObjDETALHEREDUCAOZ.Submit_CRZCRIPTO(fieldbyname('CRZCRIPTO').AsString);
                           ObjDETALHEREDUCAOZ.Submit_TOTALIZADORPARCIALCRIPTO(fieldbyname('TOTALIZADORPARCIALCRIPTO').AsString);
                           if(ObjDETALHEREDUCAOZ.Salvar(False)=False) then
                           begin
                                Result:=False;
                                Exit;
                           end;

                           QueryAux.Close;
                           QueryAux.SQL.Clear;
                           QueryAux.SQL.Add('update tabdetalhereducaoz set exportado=''S'' ');
                           QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                           QueryAux.ExecSQL;

                   end;

                   Next;
              end;
              Result:=True;
        end;
     finally
        FreeAndNil(QueryLocal);
        FreeAndNil(QueryAux);
        ObjDETALHEREDUCAOZ.Free;
     end;

end;

function TFRImportaDadosCaixasPAF.ImportaReducaoZ:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjREDUCAOZ:=TObjREDUCAOZ.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     result:=false;
     try
          with QueryLocal do
          begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabreducaoz where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;
              while not Eof do
              begin
                  if(ObjREDUCAOZ.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                  begin
                          ObjREDUCAOZ.Status:=dsInsert;
                          ObjREDUCAOZ.Submit_CODIGO(fieldbyname('codigo').AsString);
                          ObjREDUCAOZ.Submit_CONTADORREDUCAOZ(fieldbyname('contadorreducaoz').AsString);
                          ObjREDUCAOZ.Submit_CONTADORORDEMOPERACAO(fieldbyname('contadorordemoperacao').AsString);
                          ObjREDUCAOZ.Submit_CONTADORREINICIOOPERACAO(fieldbyname('contadorreiniciooperacao').AsString);
                          ObjREDUCAOZ.Submit_DATAMOVIMENTO(fieldbyname('datamovimento').AsString);
                          ObjREDUCAOZ.Submit_DATAEMISSAO(fieldbyname('dataemissao').AsString);
                          ObjREDUCAOZ.Submit_HORAEMISSAO(fieldbyname('horaemissao').AsString);
                          ObjREDUCAOZ.Submit_VENDABRUTADIARIA(fieldbyname('vendabrutadiaria').AsString);
                          ObjREDUCAOZ.Submit_INCIDENCIADESCONTOISSQN(fieldbyname('incidenciadescontoissqn').AsString);
                          ObjREDUCAOZ.Submit_Contadorreducazcripto(fieldbyname('contreducaozcripto').AsString);
                          ObjREDUCAOZ.Submit_exportado('S');
                          ObjREDUCAOZ.Submit_GRANDETOTAL(fieldbyname('grandetotal').AsString);
                          ObjREDUCAOZ.ECF.Submit_Codigo(fieldbyname('ecf').AsString);
                          ObjREDUCAOZ.Submit_NUMEROSERIE(fieldbyname('NUMEROSERIE').AsString);

                          if(ObjREDUCAOZ.Salvar(False)=False) then
                          begin
                              Result:=False;
                              Exit;
                          end;

                          QueryAux.Close;
                          QueryAux.SQL.Clear;
                          QueryAux.SQL.Add('update tabreducaoz set exportado=''S'' ');
                          QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                          QueryAux.ExecSQL;
                  end;

                  Next;
              end;
              result:=True;
          end;
     finally
         ObjREDUCAOZ.Free;
         FreeAndNil(QueryLocal);
         FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaRecebimentoVenda:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjRECEBIMENTOVENDA:=TObjRECEBIMENTOVENDA.Create;
     
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     result:=False;
     try
          with QueryLocal do
          begin
               Close;
               SQL.Clear;
               SQL.Add('select * from tabrecebimentovenda where exportado<>''S'' ');
               if(edtDataInicial.Text<>'  /  /    ')
               then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
               if(edtDataFinal.Text<>'  /  /    ')
               then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

               Open;

               while not Eof do
               begin
                     if(ObjRECEBIMENTOVENDA.LocalizaCodigo(fieldbyname('codigo').AsString)=False)then
                     begin
                           ObjRECEBIMENTOVENDA.Status:=dsInsert;
                           ObjRECEBIMENTOVENDA.Submit_CODIGO(fieldbyname('codigo').AsString);
                           ObjRECEBIMENTOVENDA.Submit_Valor(fieldbyname('valor').AsString);
                           ObjRECEBIMENTOVENDA.Submit_VENCIMENTO(fieldbyname('vencimento').AsString);
                           ObjRECEBIMENTOVENDA.Submit_COMP(fieldbyname('comp').AsString);
                           ObjRECEBIMENTOVENDA.Submit_BANCO(fieldbyname('banco').AsString);
                           ObjRECEBIMENTOVENDA.Submit_AGENCIA(fieldbyname('agencia').AsString);
                           ObjRECEBIMENTOVENDA.Submit_C1(fieldbyname('c1').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CONTA(fieldbyname('conta').AsString);
                           ObjRECEBIMENTOVENDA.Submit_C2(fieldbyname('c2').AsString);
                           ObjRECEBIMENTOVENDA.Submit_SERIE(fieldbyname('serie').AsString);
                           ObjRECEBIMENTOVENDA.Submit_NUMCHEQUE(fieldbyname('numcheque').AsString);
                           ObjRECEBIMENTOVENDA.Submit_C3(fieldbyname('c3').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CLIENTE1(fieldbyname('cliente1').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CPFCLIENTE1(fieldbyname('cpfcliente1').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CLIENTE2(fieldbyname('cliente2').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CPFCLIENTE2(fieldbyname('cpfcliente2').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CODIGODEBARRAS(fieldbyname('codigodebarras').AsString);
                           ObjRECEBIMENTOVENDA.submit_exportado('S');
                           ObjRECEBIMENTOVENDA.Submit_UsuarioAutorizou(fieldbyname('usuarioautorizou').AsString);
                           ObjRECEBIMENTOVENDA.Submit_Cancelado(fieldbyname('cancelado').AsString);
                           ObjRECEBIMENTOVENDA.Submit_DataHoraCancelamento(fieldbyname('datahoracancelamento').AsString);
                           ObjRECEBIMENTOVENDA.Submit_UsuarioCancelamento(fieldbyname('usuariocancelamento').AsString);
                           ObjRECEBIMENTOVENDA.Submit_IdTransacao(fieldbyname('idtransacao').AsString);
                           ObjRECEBIMENTOVENDA.Submit_ValorTransacao(fieldbyname('valortransacao').AsString);
                           ObjRECEBIMENTOVENDA.Submit_NomeRede(fieldbyname('nomerede').AsString);
                           ObjRECEBIMENTOVENDA.Submit_NSU(fieldbyname('nsu').AsString);
                           ObjRECEBIMENTOVENDA.Submit_Autorizacao(fieldbyname('autorizacao').AsString);
                           ObjRECEBIMENTOVENDA.Submit_Data(fieldbyname('data').AsString);
                           ObjRECEBIMENTOVENDA.Submit_Hora(fieldbyname('hora').AsString);
                           ObjRECEBIMENTOVENDA.Submit_TipoTransacao(fieldbyname('tipotransacao').AsString);
                           ObjRECEBIMENTOVENDA.Submit_TipoParcelamento(fieldbyname('tipoparcelamento').AsString);
                           ObjRECEBIMENTOVENDA.Submit_QuantidadeParcelas(fieldbyname('quantidadeparcelas').AsString);
                           ObjRECEBIMENTOVENDA.Submit_DataVencimentoParcelas(fieldbyname('datavencimentoparcelas').AsString);
                           ObjRECEBIMENTOVENDA.Submit_ValorParcelas(fieldbyname('valorparcelas').AsString);
                           ObjRECEBIMENTOVENDA.Venda.Submit_CODIGO(fieldbyname('venda').AsString);
                           ObjRECEBIMENTOVENDA.FormaPagamentoECF.FormaPagamento.Submit_CODIGO(fieldbyname('formapagamento').AsString);
                           ObjRECEBIMENTOVENDA.RecebimentoAvulso.Submit_CODIGO(fieldbyname('recebimentoavulso').AsString);

                           ObjRECEBIMENTOVENDA.Submit_NUMEROSERIE(fieldbyname('NUMEROSERIE').AsString);
                           //ObjRECEBIMENTOVENDA.Submit_NUMEROSERIECRIPTO(fieldbyname('NUMEROSERIECRIPTO').AsString);
                           ObjRECEBIMENTOVENDA.Submit_COO(fieldbyname('COO').AsString);
                           //ObjRECEBIMENTOVENDA.Submit_COOCRIPTO(fieldbyname('COOCRIPTO').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CCF(fieldbyname('CCF').AsString);
                           //ObjRECEBIMENTOVENDA.Submit_CCFCRIPTO(fieldbyname('CCFCRIPTO').AsString);
                           ObjRECEBIMENTOVENDA.Submit_CNF(fieldbyname('CNF').AsString);
                           //ObjRECEBIMENTOVENDA.Submit_CNFCRIPTO(fieldbyname('CNFCRIPTO').AsString);

                           if(ObjRECEBIMENTOVENDA.Salvar(False)=False) then
                           begin
                                Result:=False;
                                Exit;
                           end;

                           QueryAux.Close;
                           QueryAux.SQL.Clear;
                           QueryAux.SQL.Add('update tabrecebimentovenda set exportado=''S'' ');
                           QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                           QueryAux.ExecSQL;

                     end;
                     Next;
               end;
               result:=True;
          end;
     finally
         ObjRECEBIMENTOVENDA.Free;
         FreeAndNil(QueryLocal);
         FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaRecebimentoPreVenda:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux : TIBQuery;
begin
     ObjRECEBIMENTOPREVENDA:=TObjRECEBIMENTOPREVENDA.Create;
     
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabrecebimentoprevenda where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              while not eof do
              begin
                   if(ObjRECEBIMENTOPREVENDA.LocalizaCodigo(fieldbyname('codigo').AsString)=False)then
                   begin
                         ObjRECEBIMENTOPREVENDA.Status:=dsInsert;
                         ObjRECEBIMENTOPREVENDA.Submit_CODIGO(fieldbyname('codigo').AsString);
                         ObjRECEBIMENTOPREVENDA.Submit_VALOR(fieldbyname('valor').AsString);
                         ObjRECEBIMENTOPREVENDA.Submit_vencimento(fieldbyname('vencimento').AsString);
                         ObjRECEBIMENTOPREVENDA.Submit_saldo(fieldbyname('saldo').AsString);
                         ObjRECEBIMENTOPREVENDA.Submit_referencia_parcela(fieldbyname('referencia_parcela').AsString);
                         ObjRECEBIMENTOPREVENDA.Submit_Exportado(fieldbyname('S').AsString);
                         ObjRECEBIMENTOPREVENDA.CODIGOPREVENDA.Submit_CODIGO(fieldbyname('codigoprevenda').AsString);
                         ObjRECEBIMENTOPREVENDA.CODIGOFORMAPAGAMENTO.Submit_CODIGO(fieldbyname('codigoformapagamento').AsString);

                         if(ObjRECEBIMENTOPREVENDA.Salvar(False)=False) then
                         begin
                              result:=False;
                              Exit;
                         end;

                         QueryAux.Close;
                         QueryAux.SQL.Clear;
                         QueryAux.SQL.Add('update tabrecebimentoprevenda set exportado=''S'' ');
                         QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                         QueryAux.ExecSQL;

                   end;
                   next;
              end;

              Result:=True;
         end;
     finally
         ObjRECEBIMENTOPREVENDA.Free;
         FreeAndNil(QueryLocal);
         FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaRecebimentoAvulso:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjRECEBIMENTOAVULSO:=TObjRECEBIMENTOAVULSO.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;
     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     Result:=False;
     try
           with QueryLocal do
           begin
                Close;
                SQL.Clear;
                SQL.Add('select * from tabrecebimentoavulso where exportado<>''S'' ');
                if(edtDataInicial.Text<>'  /  /    ')
                then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
                if(edtDataFinal.Text<>'  /  /    ')
                then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

                Open;

                while not Eof do
                begin
                     if(ObjRECEBIMENTOAVULSO.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                     begin
                           ObjRECEBIMENTOAVULSO.Status:=dsInsert;
                           ObjRECEBIMENTOAVULSO.Submit_CODIGO(fieldbyname('codigo').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_VALOR(fieldbyname('valor').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_CUPOM(fieldbyname('cupom').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_IDENTIFICADOR(fieldbyname('identificador').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_CONTADORGERALNAOFISCAL(fieldbyname('contadorgeralnaofiscal').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_Cancelado(fieldbyname('cancelado').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_exportado('S');
                           ObjRECEBIMENTOAVULSO.Submit_DataHoraCancelamento(fieldbyname('datahoracancelamento').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_UsuarioCancelamento(fieldbyname('usuariocancelamento').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_DATAECF(fieldbyname('dataecf').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_HORAECF(fieldbyname('horaecf').AsString);
                           ObjRECEBIMENTOAVULSO.CLIENTE.Submit_CODIGO(fieldbyname('cliente').AsString);
                           ObjRECEBIMENTOAVULSO.TIPORECEBIMENTO.Submit_CODIGO(fieldbyname('tiporecebimento').AsString);
                           ObjRECEBIMENTOAVULSO.ECF.Submit_Codigo(fieldbyname('ecf').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_NUMEROSERIE(fieldbyname('NUMEROSERIE').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_NUMEROSERIECRIPTO(fieldbyname('NUMEROSERIECRIPTO').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_CUPOMCRIPTO(fieldbyname('CUPOMCRIPTO').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_CONTADORGERALNAOFISCALCRIPTO(fieldbyname('CONTADORGERALNAOFISCALCRIPTO').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_SIGLA(fieldbyname('SIGLA').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_SIGLACRIPTO(fieldbyname('SIGLACRIPTO').AsString);
                           ObjRECEBIMENTOAVULSO.Submit_DATAECFCRIPTO(fieldbyname('DATAECFCRIPTO').AsString);

                           if(ObjRECEBIMENTOAVULSO.Salvar(False)=False) then
                           begin
                                 Result:=False;
                                 Exit;
                           end;

                           QueryAux.Close;
                           QueryAux.SQL.Clear;
                           QueryAux.SQL.Add('update tabrecebimentoavulso set exportado=''S'' ');
                           QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                           QueryAux.ExecSQL;

                     end;

                     Next;
                end;

                result:=True;
           end;
     finally
           ObjRECEBIMENTOAVULSO.Free;
           FreeAndNil(QueryLocal);
           FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaProdOrcamento:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
    ObjPRODORCAMENTO:=TObjPRODORCAMENTO.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;


     Result:=False;
     try
          with QueryLocal do
          begin
               Close;
               SQL.Clear;
               SQL.Add('SELECT * FROM TABPRODORCAMENTO WHERE EXPORTADO<>''S'' ');
               if(edtDataInicial.Text<>'  /  /    ')
               then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
               if(edtDataFinal.Text<>'  /  /    ')
               then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

               Open;

               while not Eof do
               begin
                     //DEVIDO O DAV-OS, ONDE � POSS�VEL ALTERAR O OR�AMENTO AP�S A CONCLUS�O, ENT�O TEMOS QUE
                     //ATUALIZAR NO BANCO DE DADOS CENTRAL TAMB�M. ANTES S� INSERIA SE N�O EXISTISSE NO BD CENTRAL
                     
                     ObjPRODORCAMENTO.ZerarTabela;

                     if(ObjPRODORCAMENTO.LocalizaCodigo(fieldbyname('codigo').AsString)=False)then
                     begin
                          ObjPRODORCAMENTO.Status:=dsInsert;
                     end
                     else begin
                          ObjPRODORCAMENTO.Status:=dsEdit;
                     end;

                    ObjPRODORCAMENTO.Submit_CODIGO(fieldbyname('codigo').AsString);
                    ObjPRODORCAMENTO.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                    ObjPRODORCAMENTO.Submit_DESCONTO(fieldbyname('desconto').AsString);
                    ObjPRODORCAMENTO.Submit_VALOR(fieldbyname('valor').AsString);
                    ObjPRODORCAMENTO.Submit_EMBALAGEMUNIDADE(fieldbyname('embalagemunidade').AsString);
                    ObjPRODORCAMENTO.Submit_UNIDADE(fieldbyname('unidade').AsString);
                    ObjPRODORCAMENTO.Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                    ObjPRODORCAMENTO.Submit_NUMEROSERIE(fieldbyname('numeroserie').AsString);
                    ObjPRODORCAMENTO.Submit_valorcripto(fieldbyname('valorcripto').AsString);
                    ObjPRODORCAMENTO.Submit_quantidadecripto(fieldbyname('quantidadecripto').AsString);
                    ObjPRODORCAMENTO.Submit_produtocripto(fieldbyname('produtocripto').AsString);
                    ObjPRODORCAMENTO.Submit_descontocripto(fieldbyname('descontocripto').AsString);
                    ObjPRODORCAMENTO.Submit_datainclusao(fieldbyname('datainclusao').AsString);
                    ObjPRODORCAMENTO.Submit_exportado('S');
                    ObjPRODORCAMENTO.Submit_servicocripto(fieldbyname('servicocripto').AsString);
                    ObjPRODORCAMENTO.Submit_DATAINCLUSAOCRIPTO(fieldbyname('datainclusaocripto').AsString);
                    ObjPRODORCAMENTO.Submit_CANCELADO(fieldbyname('cancelado').AsString);
                    ObjPRODORCAMENTO.Submit_CODIGOORCAMENTOCRIPTO(fieldbyname('codigoorcamentocripto').AsString);
                    ObjPRODORCAMENTO.Submit_CANCELADO(fieldbyname('cancelado').AsString);
                    ObjPRODORCAMENTO.Submit_DESCRICAO(fieldbyname('descricao').AsString);
                    ObjPRODORCAMENTO.Submit_DESCRICAOCRIPTO(fieldbyname('descricaocripto').AsString);
                    ObjPRODORCAMENTO.Submit_UNIDADECRIPTO(fieldbyname('unidadecrpto').AsString);
                    ObjPRODORCAMENTO.Submit_ACRESCIMO(fieldbyname('acrescimo').AsString);
                    ObjPRODORCAMENTO.Submit_ACRESCIMOCRIPTO(fieldbyname('acrescimocripto').AsString);
                    ObjPRODORCAMENTO.Submit_CANCELADOCRIPTO(fieldbyname('canceladocripto').AsString);
                    ObjPRODORCAMENTO.Submit_VALORFINALCRIPTO(fieldbyname('valorfinalcripto').AsString);
                    ObjPRODORCAMENTO.Submit_SEQUENCIAECF(fieldbyname('sequenciaecf').AsString);
                    ObjPRODORCAMENTO.Submit_SEQUENCIAECFCRIPTO(fieldbyname('sequenciaecfcripto').AsString);
                    ObjPRODORCAMENTO.Submit_TOTALIZADORPARCIAL(fieldbyname('totalizadorparcial').AsString);
                    ObjPRODORCAMENTO.Submit_TOTALIZADORPARCIALCRIPTO(fieldbyname('totalizadorparcialcripto').AsString);
                    ObjPRODORCAMENTO.Submit_MANIPULADO(fieldbyname('manipulado').AsString);
                    ObjPRODORCAMENTO.CODIGOORCAMENTO.Submit_CODIGO(fieldbyname('codigoorcamento').AsString);
                    ObjPRODORCAMENTO.CODIGOPRODUTO.Submit_CODIGO(fieldbyname('codigoproduto').AsString);
                    ObjPRODORCAMENTO.CODIGOSERVICO.Submit_CODIGO(fieldbyname('codigoservico').AsString);
                    ObjPRODORCAMENTO.Submit_DAVORIGEM(fieldbyname('DAVORIGEM').AsString);
                    ObjPRODORCAMENTO.Submit_DAVDESTINO(fieldbyname('DAVDESTINO').AsString);
                    ObjPRODORCAMENTO.Submit_QTDORIGINAL(fieldbyname('QTDORIGINAL').AsString);
                    ObjPRODORCAMENTO.Submit_QTDTRANSFERIDA(fieldbyname('QTDTRANSFERIDA').AsString);
                    //o envio deste campo � somente para uma gambi de verifica��o da quantidade
                    ObjPRODORCAMENTO.Submit_DATAM(fieldbyname('DATAM').AsString);

                    if(ObjPRODORCAMENTO.Salvar(False)=False) then
                    begin
                        Result:=False;
                        Exit;
                    end;

                    QueryAux.Close;
                    QueryAux.SQL.Clear;
                    QueryAux.SQL.Add('update TABPRODORCAMENTO set exportado=''S'' ');
                    QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                    QueryAux.ExecSQL;
                    
                     Next;
               end;

               Result:=True;
          end;
     finally
         ObjPRODORCAMENTO.Free;
         FreeAndNil(QueryLocal);
         FreeAndNil(QueryAux);
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaRecebimentoOrcamento:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
    ObjRECEBIMENTOORCAMENTO:=TObjRECEBIMENTOORCAMENTO.Create;
    
    QueryLocal:=TIBQuery.Create(nil);
    QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

    QueryAux:=TIBQuery.Create(nil);
    QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

    Result:=False;
    try
          with QueryLocal do
          begin
               Close;
               SQL.Clear;
               SQL.Add('select * from tabrecebimentoorcamento where exportado<>''S'' ');
               if(edtDataInicial.Text<>'  /  /    ')
               then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
               if(edtDataFinal.Text<>'  /  /    ')
               then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

               Open;

               while not Eof do
               begin
                    if(ObjRECEBIMENTOORCAMENTO.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                    begin
                            ObjRECEBIMENTOORCAMENTO.Status:=dsInsert;
                            ObjRECEBIMENTOORCAMENTO.Submit_CODIGO(fieldbyname('codigo').AsString);
                            ObjRECEBIMENTOORCAMENTO.Submit_VALOR(fieldbyname('valor').AsString);
                            ObjRECEBIMENTOORCAMENTO.Submit_vencimento(fieldbyname('vencimento').AsString);
                            ObjRECEBIMENTOORCAMENTO.Submit_saldo(fieldbyname('saldo').AsString);
                            ObjRECEBIMENTOORCAMENTO.Submit_referencia_parcela(fieldbyname('referencia_parcela').AsString);
                            ObjRECEBIMENTOORCAMENTO.Submit_Exportado('S');
                            ObjRECEBIMENTOORCAMENTO.CODIGOORCAMENTO.Submit_CODIGO(fieldbyname('codigoorcamento').AsString);
                            ObjRECEBIMENTOORCAMENTO.CODIGOFORMAPAGAMENTO.Submit_CODIGO(fieldbyname('codigoformapagamento').AsString);

                            if(ObjRECEBIMENTOORCAMENTO.Salvar(False)=False) then
                            begin
                                 Result:=False;
                                 Exit;
                            end;

                            QueryAux.Close;
                            QueryAux.SQL.Clear;
                            QueryAux.SQL.Add('update tabrecebimentoorcamento set exportado=''S'' ');
                            QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                            QueryAux.ExecSQL;
                    end;

                    Next;
               end;

               Result:=True;
          end;
    finally
          ObjRECEBIMENTOORCAMENTO.Free;
          FreeAndNil(QueryLocal);
          FreeAndNil(QueryAux);
    end;
end;

procedure TFRImportaDadosCaixasPAF.edtDataInicialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if(Key=VK_SPACE)
     then edtDataInicial.Text:=FormatDateTime('dd/mm/yyyy',Now);
end;

procedure TFRImportaDadosCaixasPAF.edtDataFinalKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     if(Key=VK_SPACE)
     then edtDataFinal.Text:=FormatDateTime('dd/mm/yyyy',Now);
end;

procedure TFRImportaDadosCaixasPAF.btAtualizarCaixasClick(Sender: TObject);
var
  i:Integer;
begin
      //Aqui � feita a atualiza��o de dados nos caixas selecionados

      i:=1;
      gBarradeProgresso.MaxValue:=STRG1.RowCount;
      gBarradeProgresso.MinValue:=0;
      gBarradeProgresso.Progress:=0;
      while i<STRG1.RowCount do
      begin
          gBarradeProgresso.Progress:=gBarradeProgresso.Progress+1;
          Application.ProcessMessages;

          //Verifica no stringgrid quais caixas foram marcados
          if(STRG1.Cells[0,i]='X') then
          begin
             //Antes de se conectar no caixa, verifica se o path do caixa n�o esta vazio
             if(STRG1.Cells[2,i]<>'') then
             begin
                 //Conecta ao banco de dados do caixa
                 if(self.ConectaBanco(STRG1.Cells[2,i],True)=False) then
                 begin
                      MensagemErro('Erro ao tentar conectar o banco de dados do caixa '+STRG1.Cells[1,i]);
                      gBarradeProgresso.Progress := 0;
                      Exit;
                 end;

                 if(edtpath.Text<>'') then
                 begin
                       if(AtualizaDadosPeloArquivo(False)=False) then
                       begin
                           MensagemErro('Erro na atualiza��o de dados do caixa '+STRG1.Cells[1,i]);
                           FDataModuloAmanda.IBTransactionAmanda.RollbackRetaining;
                           FDataModulo.IBTransaction.RollbackRetaining;
                           gBarradeProgresso.Progress := 0;
                           Exit;
                       end;
                 end
                 else
                 begin
                       if(AtualizaDadosPeloServidor=False) then
                       begin
                           MensagemErro('Erro na atualiza��o de dados do caixa '+STRG1.Cells[1,i]);
                           FDataModuloAmanda.IBTransactionAmanda.RollbackRetaining;
                           FDataModulo.IBTransaction.RollbackRetaining;
                           gBarradeProgresso.Progress := 0;
                           Exit;
                       end;
                 end;

                 FDataModuloAmanda.IBTransactionAmanda.CommitRetaining;
                 FDataModulo.IBTransaction.CommitRetaining;

             end;
             STRG1.Cells[0,i]:=''
          end;
          Inc(i,1);
      end;
      //Fazer a atualiza��o do banco atual
       if(edtpath.Text<>'') then
       begin
          if(AtualizaDadosPeloArquivo(True)=False) then
          begin
               MensagemErro('Erro na atualiza��o de dados do caixa central');
               FDataModuloAmanda.IBTransactionAmanda.RollbackRetaining;
               FDataModulo.IBTransaction.RollbackRetaining;
               gBarradeProgresso.Progress := 0;
               Exit;
          end;
       end  ;

      gBarradeProgresso.Progress:=gBarradeProgresso.Progress+1;
      Application.ProcessMessages;
      MensagemSucesso('Atualiza��o Conclu�da!');
      gBarradeProgresso.Progress := 0;
      edtDataFinal.Text:='';
      edtDataInicial.Text:='';

end;

procedure TFRImportaDadosCaixasPAF.bt1Click(Sender: TObject);
begin
     if (dlgOpen1.Execute)
     then edtpath.text:=dlgOpen1.filename;
end;

function TFRImportaDadosCaixasPAF.AtualizaDadosPeloArquivo(SomenteCaixaCentral:Boolean):Boolean;
var
  linha,virgula,temp,path:string;
  STRArquivo,StrCampos,StrValores:TStringList;
  pcampocodigoretaguarda,pcampocodigo,cont,cont2,indice:integer;
  encontrou:Boolean;
  ModoImportacao:integer;
  Cancelar:boolean;
begin
     //Montar atualiza��o de dados
     {
          OP��ES PARA FAZER ATUALIZA��O
          Usando apenas um datamodulo
             - O datamodulo ja vai estar conectado ao banco central, a ideia �
             atualizar o primeiro caixa, depois conectar no proximo caixa usando
             o mesmo data modulo. Assim aproveita os objetos, sem a necessidade de
             atualizar usando query's.

          Usando dois datamodulos
             - No banco do caixa central, atualiza usando objetos, enquanto nos outros
             caixas atualiza usando comandos dentro de query's.

          Testar e ver se o primeiro da certo, se der, � o melhor a se utilizar.

          Criar um create dinamico nos objetos, onde eu possa passar a TRANSACTION e o DATABASE
     }

    {
        **************************************
        * Tabelas para fazer a importa��o
        **************************************
        cliente
        produto

     }

     //Self.SetaNil;//setando nil para os objetos

     {SE O PARAMETRO ABAIXO ESTIVER SETADO PARA O MODO DE IMPORTA��O DE CARGA POR HTTP
     EU SIMPLESMENTE FA�O O DOWNLOAD DO ARQUIVO DA WEB E SALVO NO MESMO PATH DO PARAMETRO
     "ARQUIVO DE IMPORTA��O DO RETAGUARDA", E ENT�O PROSSEGUE NA IMPORTA��O POR TXT}



    { if (edtpath.Text='')Then
     Begin
             if (dlgOpen1.Execute=False)
             Then exit;

             path:=dlgOpen1.filename;
     End
     Else path:=edtpath.Text; }

     Result:=False;
     Try
        STRArquivo:=TStringList.Create;
        StrCampos:=TStringList.Create;
        StrValores:=TStringList.Create;

        STRArquivo.clear;
        StrCampos.clear;
        StrValores.clear;

     Except
           MensagemErro('Erro na tentativa de criar as StringLists');
           exit;
     End;

     Try

       Try
           STRArquivo.LoadFromFile(path);
       Except
           on e:exception do
           Begin
               Mensagemerro('Erro na tentativa de abrir o arquivo '+path+#13+e.message);
               exit;
           End;
       End;

       if (STRArquivo.Count<2)
       then Begin
                 MensagemErro('Arquivo sem informa��o');
                 exit;
       End;


       for cont:=0 to STRArquivo.Count-1 do
       Begin

            temp:=STRArquivo[cont];
            StrValores.clear;
            ExplodeStr_COMVAZIOS(temp,StrValores,';','string');
            RetiraAspasInicio_e_Fim(StrValores);
            if ((StrValores[0]<>'T')and (StrValores[0]<>'D'))//titulo ou dados
            Then Begin
                      Mensagemerro('Linha com formato inv�lido '+inttostr(cont));
                      exit;
            End;

            if (StrValores[0]='T')//titulo
            Then Begin

                      //transferindo o nome dos campos para a StringList de Campos
                      StrCampos.text:=StrValores.text;

                      //verificando se o campo c�digo existe
                      pcampocodigo:=StrCampos.IndexOf('CODIGO');

                      if (pcampocodigo<0)
                      Then Begin
                                 //se o c�digo n�o estiver presente procuro pelo c�digo do retaguarda

                                  pcampocodigoretaguarda:=StrCampos.IndexOf('CODIGORETAGUARDA');

                                  if (pcampocodigoretaguarda<0)
                                  Then Begin
                                            Mensagemerro('O Campo CODIGO ou CODIGORETAGUARDA n�o foram encontrados na linha '+inttostr(cont)+' Conte�do da Linha: '+ strcampos.Text);
                                            exit;
                                  End;
                      End;

            End
            Else Begin

                    if (StrCampos[1]='TABPRODUTO')Then
                    Begin
                          //Se for atualizar somente os caixas, ent�o uso o FdatamoduloAmanda
                          //Sen�o, se for pra atualizar somente o caixa central, uso o DataModulo
                          if(SomenteCaixaCentral=False)
                          then ObjProduto:=TObjPRODUTO.Create('TABPRODUTO',FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda)
                          else ObjProduto:=TObjPRODUTO.Create;
                          try
                                ObjPRoduto.ZerarTabela;

                                if (pcampocodigo>0)
                                Then encontrou:=ObjPRoduto.LocalizaCodigo(strvalores[pcampocodigo])
                                Else encontrou:=ObjPRoduto.LocalizaporCampo('CODIGORETAGUARDA',strvalores[pcampocodigoretaguarda]);

                                if (encontrou=False)//Quando n�o encontra insere um novo
                                Then begin
                                      ObjPRoduto.Status:=dsinsert;
                                      ObjProduto.Submit_CODIGO(ObjProduto.Get_NovoCodigo);
                                end
                                Else Begin//encontrou entao edita o que j� tem hoje
                                          ObjPRoduto.TabelaparaObjeto;
                                          ObjPRoduto.Status:=dsEdit;
                                End;

                                for cont2:=2 to StrCampos.Count-1 do//come�a do 01 porque o primeiro � t�tulo da Linha (T/D)
                                BEgin
                                     ObjPRoduto.fieldbyname(StrCampos[cont2],StrValores[cont2]);
                                End;

                                //Depois de preencher nos atributos os valores enviados
                                //preenche com valores default campos que se encontram VAZIOS
                                //As vezes na importa��o � enviado apenas o codigo,nome,estoque
                                //e os impostos ficam em branco, neste caso este procedimento preencheria
                                //os valores default dos demais campos
                                //Lembrar que isto s� tem efeito se for inser��o
                                //Pois na edi��o � usado um tabela para objeto, preenchendo os campos
                                //Com os valores que estavam antes

                                if (OBJPRODUTO.LocalizaCodigo('0','TABPRODUTO_DEFAULT')=True)
                                then OBJPRODUTO.TabelaparaObjeto(False);//preenche s� os em branco

                                if (ObjProduto.Salvar(True)=false)
                                Then Begin
                                          MEnsagemErro('Erro na tentativa de salvar o produto '+ObjPRoduto.Get_CODIGO+' Linha '+inttostr(cont)+#13+' Valores da linha: '+Strvalores.text);
                                          exit;
                                End;
                          finally
                                ObjProduto.Free;
                          end;
                    End
                    Else
                    if (StrCampos[1]='TABCLIENTE')Then
                    Begin
                          //Se for atualizar somente os caixas, ent�o uso o FdatamoduloAmanda
                          //Sen�o, se for pra atualizar somente o caixa central, uso o DataModulo
                          if(SomenteCaixaCentral=False)
                          then ObjCliente:=TObjCLIENTE.Create('TABCLIENTE',FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda)
                          else ObjCliente:=TObjCLIENTE.Create;

                          try
                              { Estavamos utilizando o campo c�digo do cliente como o c�digo do cliente no frente de caixa, ambos considerando ser inteiro,
                              mas ent�o surgiu a necessidade da utiliza��o de c�digos alfanum�ricos ou negativos, ent�o foi adicionado o campo
                              codigoretaguarda. Com isso o campo c�digo tornou-se exclusivo do frente de caixa

                              a pedido da cormack, vou realizar a busca pela campo cpf_cnpj para ter a certeza de que n�o teremos
                              cadastros duplicados
                              }
                              ObjCLIENTE.ZerarTabela;
                              if (ObjCLIENTE.LocalizaporCampo('CODIGORETAGUARDA',''''+strvalores[pcampocodigoretaguarda]+'''','TABCLIENTE')=False)
                              Then begin
                                    {n�o encontrei o cliente pelo codigoretaguarda, pode ser que tenha acontecido:
                                       o operador cadastrou o cliente direto no frente de caixa, realizou a venda,
                                       o frente de caixa gerou o arquivo de exporta��o, com os dados do cliente, mas campo codigoretaguarda vazio
                                       o retaguarda do parceiro importou, viu que o campo codigoretaguarda esta vazio, ent�o cadastrou o cliente.
                                     Em outra ocasi�o o or�amento foi gerado no retaguarda, com o cliente que fora cadastrado no frente de caixa,
                                       ent�o o arquivo vir� com o o campo codigoretaguarda preenchido, MAS SE EU SOMENTE LOCALIZASSE PELO CAMPO
                                       CODIGORETAGUARDA, o cliente n�o seria encontrado, pois no frente de caixa este campo n�o foi preenchido.
                                     Ent�o realizo uma busca pelo cpf/cnpj, se encontrar, atualizo, sen�o cria.
                                    }
                                    indice := StrCampos.IndexOf('CPF_CGC');
                                    if(indice>0)then
                                    begin
                                          if(Length(RetornaSoNumeros(StrValores[indice]))<=0)//se o campo vier vazio insere
                                          then ObjCLIENTE.Status:=dsinsert
                                          else if not(Objcliente.LocalizaporCampo('CPF_CGC',''''+StrValores[indice]+'''','TABCLIENTE'))
                                                then ObjCLIENTE.Status:=dsinsert
                                                else begin
                                                      ObjCLIENTE.TabelaparaObjeto;
                                                      ObjCLIENTE.Status:=dsEdit;
                                                end
                                    end
                                    else ObjCLIENTE.Status:=dsinsert;
                              end
                              Else Begin
                                        ObjCLIENTE.TabelaparaObjeto;
                                        ObjCLIENTE.Status:=dsEdit;
                              End;

                              for cont2:=2 to StrCampos.Count-1 do
                              Begin
                                   ObjCLIENTE.fieldbyname(StrCampos[cont2],StrValores[cont2]);
                              End;

                              if (ObjCLIENTE.LocalizaCodigo('0','TABCLIENTE_DEFAULT')=True)
                              then ObjCLIENTE.TabelaparaObjeto(False);//preenche s� os em branco

                              if (ObjCLIENTE.Salvar(True)=false)Then
                              Begin
                                        MEnsagemErro('Erro na tentativa de salvar o CLIENTE '+ObjCLIENTE.Get_CODIGO+' LInha '+inttostr(cont));
                                        exit;
                              End;
                          finally
                              ObjCliente.Free;
                          end;

                    End
                    else
                    begin
                              MEnsagemErro('Tabela Inv�lida. Linha'+inttostr(cont));
                              exit;
                    end;
            End;
       End;
       Result:=True;
     Finally

     End;
end;

function TFRImportaDadosCaixasPAF.ImportaProdutosVenda:Boolean;
  function RetornaVendaCancelada(pvenda:string):boolean;
  var
    query:TIBQuery;
  begin
    Result := false;
    query := TIBQuery.Create(nil);
    query.Database:=FDataModulo.IBDatabase;
    try
      query.Close;
      query.sql.clear;
      query.sql.add('SELECT CODIGO FROM TABVENDA WHERE CANCELADO=''S'' AND CODIGO='+pvenda);
      query.Open;
      if(query.RecordCount>0)
      then Result := True;
    finally
      FreeAndNil(query);
    end;
  end;

var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
  VendaCancelada,Edicao:Boolean;
begin
     ObjProdutosVenda:=TObjPRODUTOSVENDA.Create ;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     VendaCancelada := false;
     Result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabprodutosvenda where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              if(QueryLocal.RecordCount=0)
              then begin
                Result:=True;
                exit;
              end;

              VendaCancelada:=RetornaVendaCancelada(fieldbyname('venda').AsString);

              while not eof do
              begin
                    ObjProdutosVenda.ZerarTabela;
                    Edicao:=False;
                    if not(ObjProdutosVenda.LocalizaCodigo(fieldbyname('codigo').AsString)) then
                    begin
                      ObjProdutosVenda.Status:=dsInsert;
                    end
                    else begin
                      ObjProdutosVenda.Status:=dsEdit;
                      Edicao:=True;
                    end;

                    ObjProdutosVenda.Submit_CODIGO(fieldbyname('codigo').AsString);
                    ObjProdutosVenda.Submit_EMB_UNID(fieldbyname('emb_unid').AsString);
                    ObjProdutosVenda.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                    ObjProdutosVenda.Submit_VALOR(formata_valor(fieldbyname('valor').AsString));
                    ObjProdutosVenda.Submit_DESCONTO(fieldbyname('desconto').AsString);
                    ObjProdutosVenda.Submit_juros(fieldbyname('juros').AsString);
                    ObjProdutosVenda.Submit_sequenciaecf(fieldbyname('sequenciaecf').AsString);
                    ObjProdutosVenda.Submit_Unidade(fieldbyname('unidade').AsString);
                    ObjProdutosVenda.Submit_INFORMACAOCOMPLEMENTAR(fieldbyname('informacaocomplementar').AsString);
                    ObjProdutosVenda.Submit_Aliquota(fieldbyname('aliquota').AsString);
                    ObjProdutosVenda.Submit_AliquotaCupom(fieldbyname('aliquotacupom').AsString);
                    ObjProdutosVenda.Submit_ReducaoBaseCalculo(fieldbyname('reducaobasecalculo').AsString);
                    ObjProdutosVenda.Submit_Isento(fieldbyname('isento').AsString);
                    ObjProdutosVenda.Submit_SubstituicaoTributaria(fieldbyname('substituicaotributaria').AsString);
                    ObjProdutosVenda.Submit_ValorPauta(fieldbyname('valorpauta').AsString);
                    ObjProdutosVenda.submit_exportado('S');
                    ObjProdutosVenda.Submit_exporta('S');
                    ObjProdutosVenda.Submit_IPI(fieldbyname('ipi').AsString);
                    ObjProdutosVenda.Submit_CANCELADO(fieldbyname('cancelado').AsString);
                    ObjProdutosVenda.Submit_QTDECANCELADA(fieldbyname('qtdecancelada').AsString);
                    ObjProdutosVenda.Submit_VALORCANCELAMENTO(fieldbyname('valorcancelamento').AsString);
                    ObjProdutosVenda.Submit_CANCELAMENTODEACRESCIMO(fieldbyname('cancelamentodeacrescimo').AsString);
                    ObjProdutosVenda.Submit_ARREDONDAMENTOTRUNCAMENTO(fieldbyname('ARREDONDAMENTOTRUNCAMENTO').AsString);
                    ObjProdutosVenda.Submit_PRODUTOMERCADORIA(fieldbyname('produtomercadoria').AsString);
                    ObjProdutosVenda.Submit_DataHoraCancelamento(fieldbyname('datahoracancelamento').AsString);
                    ObjProdutosVenda.Submit_UsuarioCancelamento(fieldbyname('USUARIOCANCELAMENTO').AsString);
                    ObjProdutosVenda.Submit_TotalizadorParcial(fieldbyname('totalizadorparcial').AsString);
                    ObjProdutosVenda.Submit_NAOTRIBUTADO(fieldbyname('naotributado').AsString);
                    ObjProdutosVenda.Submit_MANIPULADO(fieldbyname('manipulado').AsString);
                    ObjProdutosVenda.PRODUTO.Submit_CODIGO(fieldbyname('produto').AsString);
                    ObjProdutosVenda.VENDA.Submit_CODIGO(fieldbyname('venda').AsString);
                    ObjProdutosVenda.SERVICO.Submit_CODIGO(fieldbyname('servico').AsString);
                    ObjProdutosVenda.Submit_BAIXAESTOQUEPRODUCAO(fieldbyname('BAIXAESTOQUEPRODUCAO').AsString);
                    ObjProdutosVenda.Submit_MANIPULADO(fieldbyname('MANIPULADO').AsString);
                    ObjProdutosVenda.Submit_STA(fieldbyname('ORIGEM').AsString);
                    ObjProdutosVenda.Submit_STB(fieldbyname('CST').AsString);
                    ObjProdutosVenda.Submit_CFOP(fieldbyname('CFOP').AsString);
                    ObjProdutosVenda.Submit_CSOSN(fieldbyname('CSOSN').AsString);
                    ObjProdutosVenda.Submit_PERCENTUALPIS(fieldbyname('PERCENTUALPIS').AsString);
                    ObjProdutosVenda.Submit_PERCENTUALCOFINS(fieldbyname('PERCENTUALCOFINS').AsString);
                    ObjProdutosVenda.Submit_VALORPIS(fieldbyname('VALORPIS').AsString);
                    ObjProdutosVenda.Submit_VALORCOFINS(fieldbyname('VALORCOFINS').AsString);

                    ObjProdutosVenda.Submit_NUMEROSERIE(fieldbyname('NUMEROSERIE').AsString);
                    //ObjProdutosVenda.Submit_NUMEROSERIECRIPTO(fieldbyname('NUMEROSERIECRIPTO').AsString);
                    ObjProdutosVenda.Submit_COO(fieldbyname('COO').AsString);
                    //ObjProdutosVenda.Submit_COOCRIPTO(fieldbyname('COOCRIPTO').AsString);
                    ObjProdutosVenda.Submit_CCF(fieldbyname('CCF').AsString);
                    //ObjProdutosVenda.Submit_CCFCRIPTO(fieldbyname('CCFCRIPTO').AsString);
                    //ObjProdutosVenda.Submit_PRODUTOCRIPTO(fieldbyname('PRODUTOCRIPTO').AsString);
                    //ObjProdutosVenda.Submit_SERVICOCRIPTO(fieldbyname('SERVICOCRIPTO').AsString);
                    //ObjProdutosVenda.Submit_QUANTIDADECRIPTO(fieldbyname('QUANTIDADECRIPTO').AsString);
                    //ObjProdutosVenda.Submit_VALORCRIPTO(fieldbyname('VALORCRIPTO').AsString);
                    //ObjProdutosVenda.Submit_VALORFINALCRIPTO(fieldbyname('VALORFINALCRIPTO').AsString);
                    //ObjProdutosVenda.Submit_CANCELADOCRIPTO(fieldbyname('CANCELADOCRIPTO').AsString);

                    if(ObjProdutosVenda.Salvar(False)=False) then
                    begin
                         Result:=False;
                         Exit;
                    end;

                    QueryAux.Close;
                    QueryAux.SQL.Clear;
                    QueryAux.SQL.Add('update tabprodutosvenda set exportado=''S'' ');
                    QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                    QueryAux.ExecSQL;

                    if((fieldbyname('cancelado').AsString)='N')
                    then begin
                      if not(VendaCancelada)
                      then begin
                        if not(ObjProdutosVenda.BaixaEstoqueProduto(fieldbyname('codigo').AsString))
                        then Exit;
                      end
                      else begin
                        if(Edicao) //quando cancela no FC e por acaso j� foi exportado, ent�o eu preciso aumentar o estoque
                        then if not(ObjProdutosVenda.AumentaEstoqueProduto(fieldbyname('codigo').AsString))
                              then Exit;
                      end;
                    end;
                    Next;
              end;
              result:=True;
         end;
     finally
          FreeAndNil(QueryAux);
          FreeAndNil(QueryLocal);
          ObjProdutosVenda.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaProPreVenda:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjProdPreVenda:=TObjPRODPREVENDA.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     Result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabprodprevenda where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              while not eof do
              begin
                    if(ObjProdPreVenda.LocalizaCodigo(fieldbyname('codigo').AsString)=False)
                    then ObjProdPreVenda.Status:=dsInsert
                    else ObjProdPreVenda.Status:=dsEdit;

                    ObjProdPreVenda.Submit_CODIGO(fieldbyname('codigo').AsString);
                    ObjProdPreVenda.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                    ObjProdPreVenda.Submit_DESCONTO(fieldbyname('desconto').AsString);
                    ObjProdPreVenda.Submit_VALOR(fieldbyname('valor').AsString);
                    ObjProdPreVenda.Submit_EMBALAGEMUNIDADE(fieldbyname('embalagemunidade').AsString);
                    ObjProdPreVenda.Submit_UNIDADE(fieldbyname('unidade').AsString);
                    ObjProdPreVenda.Submit_VALORFINAL(fieldbyname('valorfinal').AsString);
                    ObjProdPreVenda.Submit_NUMEROSERIE(fieldbyname('numeroserie').AsString);
                    ObjProdPreVenda.Submit_datainclusao(fieldbyname('datainclusao').AsString);
                    ObjProdPreVenda.Submit_exportado('S');
                    ObjProdPreVenda.Submit_CANCELADO(fieldbyname('cancelado').AsString);
                    ObjProdPreVenda.Submit_DESCRICAO(fieldbyname('descricao').AsString);
                    ObjProdPreVenda.Submit_ACRESCIMO(fieldbyname('acrescimo').AsString);
                    ObjProdPreVenda.CODIGOPREVENDA.Submit_CODIGO(fieldbyname('codigoprevenda').AsString);
                    ObjProdPreVenda.CODIGOPRODUTO.Submit_CODIGO(fieldbyname('codigoproduto').AsString);
                    ObjProdPreVenda.CODIGOSERVICO.Submit_CODIGO(fieldbyname('codigoservico').AsString);

                    if(ObjProdPreVenda.Salvar(False)=False) then
                    begin
                         Result:=False;
                         Exit;
                    end;

                    QueryAux.Close;
                    QueryAux.SQL.Clear;
                    QueryAux.SQL.Add('update tabprodprevenda set exportado=''S'' ');
                    QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                    QueryAux.ExecSQL;

                    Next;
              end;
              result:=True;
         end;
     finally
          FreeAndNil(QueryAux);
          FreeAndNil(QueryLocal);
          ObjProdPreVenda.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaMovimentoDia:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjMovimentoDia:=TObjMOVIMENTODIA.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     Result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabmovimentodia where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              while not eof do
              begin
                      if(ObjMovimentoDia.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                            ObjMovimentoDia.Status:=dsInsert;
                            ObjMovimentoDia.Submit_CODIGO(fieldbyname('codigo').AsString);
                            ObjMovimentoDia.Submit_numeroserie(fieldbyname('numeroserie').AsString);
                            ObjMovimentoDia.Submit_mfadicional(fieldbyname('mfadicional').AsString);
                            ObjMovimentoDia.Submit_tipoecf(fieldbyname('tipoecf').AsString);
                            ObjMovimentoDia.Submit_marca(fieldbyname('marca').AsString);
                            ObjMovimentoDia.Submit_modelo(fieldbyname('modelo').AsString);
                            ObjMovimentoDia.Submit_data(fieldbyname('data').AsString);
                            ObjMovimentoDia.Submit_hora(fieldbyname('hora').AsString);
                            ObjMovimentoDia.ecf.Submit_Codigo(fieldbyname('ecf').AsString);

                            if(ObjMovimentoDia.Salvar(False)=False) then
                            begin
                                 Result:=False;
                                 Exit;
                            end;

                            QueryAux.Close;
                            QueryAux.SQL.Clear;
                            QueryAux.SQL.Add('update tabmovimentodia set exportado=''S'' ');
                            QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                            QueryAux.ExecSQL;
                      end;

                      Next;
              end;
              Result:=True;
         end;
     finally
          FreeAndNil(QueryAux);
          FreeAndNil(QueryLocal);
          ObjMovimentoDia.Free
     end;
end;

function TFRImportaDadosCaixasPAF.ImportaEstoqueMovimento:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     Objestoquemovimento:=TObjESTOQUEMOVIMENTO.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     Result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabestoquemovimento where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              while not eof do
              begin
                      if(Objestoquemovimento.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                            Objestoquemovimento.Status:=dsInsert;
                            Objestoquemovimento.Submit_CODIGO(fieldbyname('codigo').AsString);
                            Objestoquemovimento.Submit_estoque(fieldbyname('estoque').AsString);
                            Objestoquemovimento.Submit_ESTOQUECRIPTO(fieldbyname('estoquecripto').AsString);
                            Objestoquemovimento.Movimento.Submit_CODIGO(fieldbyname('movimento').AsString);
                            Objestoquemovimento.Produto.Submit_CODIGO(fieldbyname('produto').AsString);
                            Objestoquemovimento.Submit_DESCRICAO(fieldbyname('DESCRICAO').AsString);

                            if(Objestoquemovimento.Salvar(False)=False) then
                            begin
                                 Result:=False;
                                 Exit;
                            end;

                            QueryAux.Close;
                            QueryAux.SQL.Clear;
                            QueryAux.SQL.Add('update tabestoquemovimento set exportado=''S'' ');
                            QueryAux.SQL.Add('where codigo='+fieldbyname('codigo').AsString);
                            QueryAux.ExecSQL;
                      end;

                      Next;
              end;
              Result:=True;
         end;
     finally
          FreeAndNil(QueryAux);
          FreeAndNil(QueryLocal);
          Objestoquemovimento.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.AtualizaDadosPeloServidor:Boolean;
begin
    //Pega os dados que est�o no servidor e atualiza nos caixas
    {
TABTOTALIZADORNAOFISCAL - cadastrar no servidor
TABTRANSPORTADORA - cadastrar no servidor
TABUSUARIOS - cadastrar no servidor
TABCLIENTE
TABFORMASPAGAMENTO - cadastrar no servidor
TABPARAMETROS - cadastrar no servidor
TABSERVICOS
    }
    Result:=False;

    if(AtualizaTotalizadorNaoFiscal=False) then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da TabtotalizadorFiscal');
         Exit;
    end;

    if(AtualizaTransportadora=False)then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabtransportadora');
         Exit;
    end;

    if(AtualizaUsuarios=False) then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabusuarios');
         Exit;
    end;

    if(AtualizaClientes=False) then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabcliente');
         Exit;
    end;

    if(AtualizaFormasPagamento=False) then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabformaspagamento');
         Exit;
    end;

    if(AtualizaParametros=False) then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabparametros');
         Exit;
    end;

    if(AtualizaServicos=False)then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabservicos');
         Exit;
    end;

    if(AtualizaProdutos=False)then
    begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabprodutos');
         Exit;
    end;

    if not(AtualizaIndiceProducao)
    then begin
         MensagemErro('Erro ao tentar atualizar os dados da Tabindiceproducao');
         Exit;
    end;

    if not(AtualizaUnidadeMedida)
    then begin
         MensagemErro('Erro ao tentar atualizar os dados da TabunidadeMedida');
         Exit;
    end;
    Result:=True;
end;

function TFRImportaDadosCaixasPAF.AtualizaTotalizadorNaoFiscal:Boolean;
var
  QueryLocal:TIBQuery;
begin
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     ObjTotalizadorNaofiscal:=TObjTOTALIZADORNAOFISCAL.Create(FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);

     Result:=False;
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select * from tabtotalizadornaofiscal where codigo<>-1');
                 if(edtDataInicial.Text<>'  /  /    ')  and (edtDataFinal.Text<>'  /  /    ')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      if(ObjTotalizadorNaofiscal.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             ObjTotalizadorNaofiscal.Status:=dsInsert;
                      end
                      else
                      begin
                             ObjTotalizadorNaofiscal.Status:=dsEdit;
                      end;

                      ObjTotalizadorNaofiscal.Submit_CODIGO(fieldbyname('codigo').AsString);
                      ObjTotalizadorNaofiscal.Submit_Nome(fieldbyname('nome').AsString);

                      if(ObjTotalizadorNaofiscal.Salvar(False)=False)then
                      begin
                           Result:=False;
                           Exit;
                      end;

                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            ObjTotalizadorNaofiscal.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.AtualizaTransportadora:Boolean;
var
  QueryLocal:TIBQuery;
begin
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     ObjTransportadora:=TObjTRANSPORTADORA.Create(FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
     Result:=False;
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select * from tabtransportadora where codigo<>-1');
                 if(edtDataInicial.Text<>'  /  /    ')  and (edtDataFinal.Text<>'  /  /    ')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      if(ObjTransportadora.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             ObjTransportadora.Status:=dsInsert;
                      end
                      else
                      begin
                             ObjTransportadora.Status:=dsEdit;
                      end;

                      ObjTransportadora.Submit_CODIGO(fieldbyname('codigo').AsString);
                      ObjTransportadora.Submit_NOME(fieldbyname('nome').AsString);
                      ObjTransportadora.Submit_PLACA(fieldbyname('placa').AsString);
                      ObjTransportadora.Submit_ESPECIFICACOES(fieldbyname('especificacoes').AsString);
                      ObjTransportadora.Submit_PESOMAXIMO(fieldbyname('pesomaximo').AsString);
                      ObjTransportadora.Submit_UfPlaca(fieldbyname('ufplaca').AsString);
                      ObjTransportadora.Submit_CPFCNPJ(fieldbyname('cpfcnpj').AsString);
                      ObjTransportadora.Submit_Endereco(fieldbyname('endereco').AsString);
                      ObjTransportadora.Submit_Municipio(fieldbyname('municipio').AsString);
                      ObjTransportadora.Submit_Uf(fieldbyname('uf').AsString);
                      ObjTransportadora.Submit_IE(fieldbyname('ie').AsString);
                      ObjTransportadora.Submit_Peso(fieldbyname('peso').AsString);

                      if(ObjTransportadora.Salvar(False)=False)then
                      begin
                            Result:=False;
                            Exit;
                      end;

                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            ObjTransportadora.Free;
     end;

end;

function TFRImportaDadosCaixasPAF.AtualizaUsuarios:Boolean;
var
  QueryLocal:TIBQuery;
begin
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     ObjUsuario:=TObjUsuarios.Create(False,FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
     Result:=False;
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select * from tabusuarios where codigo<>-1');
                 if(edtDataInicial.Text<>'  /  /    ')  and (edtDataFinal.Text<>'  /  /    ')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      if(ObjUsuario.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             ObjUsuario.Status:=dsInsert;
                      end
                      else
                      begin
                             ObjUsuario.Status:=dsEdit;
                      end;

                      ObjUsuario.Submit_CODIGO(fieldbyname('codigo').AsString);
                      ObjUsuario.Submit_nome(fieldbyname('nome').AsString);
                      ObjUsuario.Submit_senha(fieldbyname('senha').AsString);
                      ObjUsuario.Submit_Nivel(fieldbyname('nivel').AsString);

                      if(ObjUsuario.Salvar(False,True)=False)then
                      begin
                           Result:=False;
                           Exit;
                      end;

                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            ObjUsuario.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.AtualizaClientes:Boolean;
var
  QueryLocal:TIBQuery;
begin

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     ObjCliente:=TObjCLIENTE.Create('TABCLIENTE',FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
     Result:=False;
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select * from tabcliente where codigo<>-1');
                 if(edtDataInicial.Text<>'  /  /    ')  and (edtDataFinal.Text<>'  /  /    ')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      if(ObjCliente.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             ObjCliente.Status:=dsInsert;
                      end
                      else
                      begin
                             ObjCliente.Status:=dsEdit;
                      end;

                      ObjCliente.Submit_CODIGO(fieldbyname('codigo').AsString);
                      ObjCliente.Submit_NOME(fieldbyname('nome').AsString);
                      ObjCliente.Submit_FANTASIA(fieldbyname('fantasia').AsString);
                      ObjCliente.Submit_FISICA_JURIDICA(fieldbyname('fisica_juridica').AsString);
                      ObjCliente.Submit_CPF_CGC(fieldbyname('cpf_cgc').AsString);
                      ObjCliente.Submit_RG_IE(fieldbyname('rg_ie').AsString);
                      ObjCliente.Submit_FONE(fieldbyname('fone').AsString);
                      ObjCliente.Submit_CELULAR(fieldbyname('fone').AsString);
                      ObjCliente.Submit_ENDERECOFATURA(fieldbyname('enderecofatura').AsString);
                      ObjCliente.Submit_BAIRROFATURA(fieldbyname('bairrofatura').AsString);
                      ObjCliente.Submit_CIDADEFATURA(fieldbyname('cidadefatura').AsString);
                      ObjCliente.Submit_CEPFATURA(fieldbyname('cepfatura').AsString);
                      ObjCliente.Submit_ATIVO(fieldbyname('ativo').AsString);
                      ObjCliente.Submit_LimiteCredito(fieldbyname('limitecredito').AsString);
                      ObjCliente.Submit_ValorComprometido(fieldbyname('valorcomprometido').AsString);
                      ObjCliente.Submit_ValorAtrasado(fieldbyname('valoratrasado').AsString);
                      ObjCliente.Submit_EstadoFatura(fieldbyname('estadofatura').AsString);
                      ObjCliente.Submit_SEXO(fieldbyname('sexo').AsString);
                      ObjCliente.Submit_DATANASCIMENTO(fieldbyname('datanascimento').AsString);
                      ObjCliente.Submit_CODIGORETAGUARDA(fieldbyname('codigoretaguarda').AsString);
                      ObjCliente.Submit_nomecripto(fieldbyname('nomecripto').AsString);
                      ObjCliente.Submit_CPF_CGCcripto(fieldbyname('cpf_cgccripto').AsString);
                      ObjCliente.Submit_codigocidade(fieldbyname('codigocidade').AsString);
                      ObjCliente.Submit_codigopais(fieldbyname('codigopais').AsString);
                      ObjCliente.Submit_IEprodutorRural(fieldbyname('ieprodutorrural').AsString);
                      ObjCliente.Submit_Numero(fieldbyname('numero').AsString);

                      if(ObjCliente.Salvar(False)=False) then
                      begin
                              Result:=false;
                              Exit;
                      end;

                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            ObjCliente.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.AtualizaFormasPagamento:Boolean;
var
  QueryLocal:TIBQuery;
begin

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     ObjFormasPagamento:=TObjFORMASPAGAMENTO.Create(FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
     Result:=False;
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select * from tabformaspagamento where codigo<>-1');
                 if(edtDataInicial.Text<>'  /  /    ')  and (edtDataFinal.Text<>'  /  /    ')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      if(ObjFormasPagamento.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             ObjFormasPagamento.Status:=dsInsert;
                      end
                      else
                      begin
                             ObjFormasPagamento.Status:=dsEdit;
                      end;

                      ObjFormasPagamento.Submit_CODIGO(fieldbyname('codigo').asstring);
                      ObjFormasPagamento.Submit_Nome(fieldbyname('nome').asstring);
                      ObjFormasPagamento.Submit_Dinheiro_Cheque(fieldbyname('dinheiro_cheque').asstring);
                      ObjFormasPagamento.Submit_PermiteEscolherSangria(fieldbyname('permiteescolhersangria').asstring);
                      ObjFormasPagamento.Submit_ValidaCheque(fieldbyname('validacheque').asstring);
                      ObjFormasPagamento.ObjOperadoraCartao.Submit_CODIGO(fieldbyname('operadoracartao').asstring);

                      if(ObjFormasPagamento.Salvar(False)=False)then
                      begin
                            Result:=False;
                            Exit;
                      end;

                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            ObjFormasPagamento.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.AtualizaServicos:Boolean;
var
  QueryLocal:TIBQuery;
begin
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     ObjServico:=TObjSERVICOS.Create(FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
     Result:=False;
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select * from tabservicos where codigo<>-1');
                 if(edtDataInicial.Text<>'  /  /    ')  and (edtDataFinal.Text<>'  /  /    ')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      if(ObjServico.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             ObjServico.Status:=dsInsert;
                      end
                      else
                      begin
                             ObjServico.Status:=dsEdit;
                      end;

                      ObjServico.Submit_CODIGO(QueryLocal.fieldbyname('codigo').AsString);
                      ObjServico.Submit_NOME(fieldbyname('nome').AsString);
                      ObjServico.Submit_CODIGODEBARRA(fieldbyname('codigodebarra').AsString);
                      ObjServico.Submit_VALOR(fieldbyname('valor').AsString);
                      ObjServico.Submit_SUBSTITUICAO_ISS(fieldbyname('substituicao_iss').AsString);
                      ObjServico.Submit_NAOTRIBUTADO_ISS(fieldbyname('naotributado_iss').AsString);
                      ObjServico.Submit_ISENTO_ISS(fieldbyname('ISENTO_ISS').AsString);
                      ObjServico.Submit_ALIQUOTA_ISS(fieldbyname('aliquota_iss').AsString);
                      ObjServico.Submit_CODIGORETAGUARDA(fieldbyname('codigoretaguarda').AsString);
                      ObjServico.Submit_UNIDADE(fieldbyname('unidade').AsString);
                      ObjServico.Submit_NCM(fieldbyname('ncm').AsString);

                      ObjServico.CFOP.Submit_CODIGO(fieldbyname('CFOP').AsString);
                      ObjServico.SituacaoTributaria_TabelaB.Submit_CODIGO(fieldbyname('SituacaoTributaria_TabelaB').AsString);

                      if(ObjServico.Salvar(False)=False)then
                      begin
                            Result:=False;
                            Exit;
                      end;


                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            ObjServico.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.AtualizaParametros:Boolean;
var
  QueryLocal:TIBQuery;
begin
     result := false;


     MensagemErro('Habilitar atualiza��o de par�metros');
     result := True;
     exit;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     Objparametros:=TObjParametros.Create(FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
     Result:=False;     
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('select * from tabparametros where codigo<>-1');
                 if(edtDataInicial.Text<>'  /  /    ')  and (edtDataFinal.Text<>'  /  /    ')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      if(Objparametros.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             Objparametros.Status:=dsInsert;
                      end
                      else
                      begin
                             Objparametros.Status:=dsEdit;
                      end;

                      Objparametros.Submit_PCODIGO(fieldbyname('codigo').AsString);
                      Objparametros.Submit_PNOME(fieldbyname('nome').AsString);
                      Objparametros.Submit_PVALOR(fieldbyname('valor').AsString);
                      Objparametros.Submit_PFuncao(fieldbyname('funcao').AsString);
                      Objparametros.Submit_Popcoes(fieldbyname('opcoes').AsString);

                      if(Objparametros.Salvar(False)=False)then
                      begin
                           Result:=False;
                           Exit;
                      end;

                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            Objparametros.Free;
     end;
end;

function TFRImportaDadosCaixasPAF.AtualizaProdutos: Boolean;
var
  QueryLocal:TIBQuery;
begin
     result := false;
     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModulo.IBDatabase;

     ObjProduto:=TObjPRODUTO.Create('TABPRODUTO',FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
     Result:=False;
     try
           with QueryLocal do
           begin
                 Close;
                 SQL.Clear;
                 SQL.Add('SELECT * FROM TABPRODUTO WHERE CODIGO <>-1');
                 if(Trim(comebarra(edtDataInicial.Text))<>'')  and (Trim(comebarra(edtDataFinal.Text))<>'')
                 then  sql.Add('and( (datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+'and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+') or  (datam>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39+' and datam<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39+')  )')  ;

                 Open;

                 while not eof do
                 begin
                      ObjProduto.ZerarTabela;
                      if(ObjProduto.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                             ObjProduto.Status:=dsInsert;
                      end
                      else
                      begin
                             ObjProduto.Status:=dsEdit;
                      end;

                      ObjProduto.Submit_CODIGO(fieldbyname('codigo').AsString);
                      ObjProduto.Submit_CODIGODEBARRA(fieldbyname('CODIGODEBARRA').AsString);
                      ObjProduto.Submit_NOME(fieldbyname('NOME').AsString);
                      ObjProduto.Submit_UNIDADE(fieldbyname('UNIDADE').AsString);
                      ObjProduto.Submit_EMBALAGEM(fieldbyname('EMBALAGEM').AsString);
                      ObjProduto.Submit_QUANTIDADEEMBALAGEM(fieldbyname('QUANTIDADEEMBALAGEM').AsString);
                      ObjProduto.Submit_PESOUNITARIO(fieldbyname('PESOUNITARIO').AsString);
                      ObjProduto.Submit_CUSTO(fieldbyname('CUSTO').AsString);
                      ObjProduto.Submit_MARGEm(fieldbyname('MARGEM').AsString);
                      ObjProduto.Submit_MARGEMEMBALAGEM(fieldbyname('MARGEMEMBALAGEM').AsString);
                      //ObjProduto.Submit_MARGEMREVENDA(fieldbyname('MARGEMREVENDA').AsString);
                      //ObjProduto.Submit_MARGEMEMBALAGEMREVENDA(fieldbyname('MARGEMEMBALAGEMREVENDA').AsString);
                      //ObjProduto.Submit_PRECOSUGERIDO(fieldbyname('PRECOSUGERIDO').AsString);
                      ObjProduto.Submit_PRECOVENDA(fieldbyname('PRECOVENDA').AsString);
                      //ObjProduto.Submit_PRECOEMBALAGEM(fieldbyname('PRECOEMBALAGEM').AsString);
                      //ObjProduto.Submit_PRECOREVENDA(fieldbyname('PRECOREVENDA').AsString);
                      //ObjProduto.Submit_PRECOEMBALAGEMREVENDA(fieldbyname('PRECOEMBALAGEMREVENDA').AsString);
                      ObjProduto.Submit_DESCONTOMAXIMO(fieldbyname('DESCONTOMAXIMO').AsString);
                      ObjProduto.Submit_IPI(fieldbyname('IPI').AsString);
                      ObjProduto.Submit_ESTOQUE(fieldbyname('ESTOQUE').AsString);
                      ObjProduto.Submit_ISENTOICMS_ESTADO(fieldbyname('ISENTOICMS_ESTADO').AsString);
                      ObjProduto.Submit_ISENTOICMS_FORAESTADO(fieldbyname('ISENTOICMS_FORAESTADO').AsString);
                      ObjProduto.Submit_Aliquota_ICMS_Estado(fieldbyname('Aliquota_ICMS_Estado').AsString);
                      ObjProduto.Submit_Reducao_BC_ICMS_Estado(fieldbyname('Reducao_BC_ICMS_Estado').AsString);
                      ObjProduto.Submit_Aliquota_ICMS_Cupom_Estado(fieldbyname('Aliquota_ICMS_Cupom_Estado').AsString);
                      ObjProduto.Submit_Aliquota_ICMS_ForaEstado(fieldbyname('Aliquota_ICMS_ForaEstado').AsString);
                      ObjProduto.Submit_Reducao_BC_ICMS_ForaEstado(fieldbyname('Reducao_BC_ICMS_ForaEstado').AsString);
                      ObjProduto.Submit_Aliquota_ICMS_Cupom_ForaEstado(fieldbyname('Aliquota_ICMS_Cupom_ForaEstado').AsString);
                      ObjProduto.Submit_classificacaofiscal(fieldbyname('classificacaofiscal').AsString);
                      ObjProduto.Submit_SUBSTITUICAOICMS_ESTADO(fieldbyname('SUBSTITUICAOICMS_ESTADO').AsString);
                      ObjProduto.Submit_VALORPAUTA_SUB_TRIB_ESTADO(fieldbyname('VALORPAUTA_SUB_TRIB_ESTADO').AsString);
                      ObjProduto.Submit_SUBSTITUICAOICMS_FORAESTADO(fieldbyname('SUBSTITUICAOICMS_FORAESTADO').AsString);
                      ObjProduto.Submit_VALORPAUTA_SUB_TRIB_FORAESTADO(fieldbyname('VALORPAUTA_SUB_TRIB_FORAESTADO').AsString);
                      ObjProduto.Submit_GTIN(fieldbyname('GTIN').AsString);
                      ObjProduto.Submit_ArredondamentoTruncamento(fieldbyname('ArredondamentoTruncamento').AsString);
                      ObjProduto.Submit_ProdutoMercadoria(fieldbyname('ProdutoMercadoria').AsString);
                      ObjProduto.Submit_SolicitaInfoComplementar(fieldbyname('SolicitaInfoComplementar').AsString);
                      ObjProduto.Submit_codigoretaguarda(fieldbyname('codigoretaguarda').AsString);
                      ObjProduto.Submit_NaoTributadoICMS_Estado(fieldbyname('NaoTributadoICMS_Estado').AsString);
                      ObjProduto.Submit_NaoTributadoICMS_ForaEstado(fieldbyname('NaoTributadoICMS_ForaEstado').AsString);
                      ObjProduto.Submit_cfopforaestado(fieldbyname('cfopforaestado').AsString);
                      ObjProduto.Submit_NCM(fieldbyname('NCM').AsString);

                      ObjProduto.SituacaoTributaria_TabelaA.Submit_CODIGO(fieldbyname('SituacaoTributaria_TabelaA').AsString);
                      ObjProduto.SituacaoTributaria_TabelaB.Submit_CODIGO(fieldbyname('SituacaoTributaria_TabelaB').AsString);
                      ObjProduto.CSOSN.Submit_codigo(fieldbyname('CSOSN').AsString);
                      ObjProduto.CFOP.Submit_CODIGO(fieldbyname('CFOP').AsString);
                      ObjProduto.Submit_ATIVO(fieldbyname('ATIVO').AsString);

                      if not(ObjProduto.Salvar(False))then
                      begin
                            Exit;
                      end;

                      Next;
                 end;
                 result:=True;
           end;
     finally
            FreeAndNil(QueryLocal);
            ObjProduto.Free;
     end;
end;


function TFRImportaDadosCaixasPAF.AtualizaIndiceProducao: Boolean;
var
  QueryLocal:TIBQuery;
begin
  result := false;
  QueryLocal:=TIBQuery.Create(nil);
  QueryLocal.Database:=FDataModulo.IBDatabase;

  ObjIndiceProducao:=TObjINDICEPRODUCAO.Create(FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
  Result:=False;
  try
    with QueryLocal do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM TABINDICEPRODUCAO WHERE CODIGO <>-1');
      if(Trim(comebarra(edtDataInicial.Text))<>'')  and (Trim(comebarra(edtDataFinal.Text))<>'')
      then  sql.Add('AND( (DATAC>='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAINICIAL.TEXT))+' 00:00'+#39+'AND DATAC<='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAFINAL.TEXT))+' 23:59'+#39+') OR  (DATAM>='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAINICIAL.TEXT))+' 00:00'+#39+' AND DATAM<='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAFINAL.TEXT))+' 23:59'+#39+')  )')  ;

      Open;

      while not eof do
      begin
        ObjIndiceProducao.ZerarTabela;
        if not(ObjIndiceProducao.LocalizaCodigo(fieldbyname('codigo').AsString))
        then ObjIndiceProducao.Status:=dsInsert
        else ObjIndiceProducao.Status:=dsEdit;

        ObjIndiceProducao.Submit_CODIGO(fieldbyname('CODIGO').AsString);
        ObjIndiceProducao.Submit_Quantidade(fieldbyname('QUANTIDADE').AsString);
        ObjIndiceProducao.PRODUTOORIGEM.Submit_CODIGO(fieldbyname('PRODUTOORIGEM').AsString);
        ObjIndiceProducao.PRODUTODESTINO.Submit_CODIGO(fieldbyname('PRODUTODESTINO').AsString);

        if not(ObjIndiceProducao.Salvar(False))then
        begin
          Exit;
        end;

        Next;
      end;
      result:=True;
    end;
  finally
    FreeAndNil(QueryLocal);
    ObjIndiceProducao.Free;
  end;
end;

function TFRImportaDadosCaixasPAF.AtualizaUnidadeMedida: Boolean;
var
  QueryLocal:TIBQuery;
begin
  result := false;
  QueryLocal:=TIBQuery.Create(nil);
  QueryLocal.Database:=FDataModulo.IBDatabase;

  ObjUnidadeMedida:=TObjUNIDADEMEDIDA.Create(FDataModuloAmanda.IbDatabaseAmanda,FDataModuloAmanda.IBTransactionAmanda);
  Result:=False;
  try
    with QueryLocal do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM TABUNIDADEMEDIDA WHERE CODIGO <>-1');
      if(Trim(comebarra(edtDataInicial.Text))<>'')  and (Trim(comebarra(edtDataFinal.Text))<>'')
      then  sql.Add('AND( (DATAC>='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAINICIAL.TEXT))+' 00:00'+#39+'AND DATAC<='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAFINAL.TEXT))+' 23:59'+#39+') OR  (DATAM>='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAINICIAL.TEXT))+' 00:00'+#39+' AND DATAM<='+#39+FORMATDATETIME('MM/DD/YYYY',STRTODATE(EDTDATAFINAL.TEXT))+' 23:59'+#39+')  )')  ;

      Open;

      while not eof do
      begin
        ObjUnidadeMedida.ZerarTabela;
        if not(ObjUnidadeMedida.LocalizaCodigo(fieldbyname('codigo').AsString))
        then ObjUnidadeMedida.Status:=dsInsert
        else ObjUnidadeMedida.Status:=dsEdit;

        ObjUnidadeMedida.Submit_CODIGO(fieldbyname('CODIGO').AsString);
        ObjUnidadeMedida.Submit_SIGLA(fieldbyname('SIGLA').AsString);
        ObjUnidadeMedida.Submit_DESCRICAO(fieldbyname('DESCRICAO').AsString);

        if not(ObjUnidadeMedida.Salvar(False))then
        begin
          Exit;
        end;

        Next;
      end;
      result:=True;
    end;
  finally
    FreeAndNil(QueryLocal);
    ObjUnidadeMedida.Free;
  end;
end;


function TFRImportaDadosCaixasPAF.ImportaDocumentosECF: Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
  ObjDocumentosECF:=TObjDOCUMENTOSECF.Create;

  QueryLocal:=TIBQuery.Create(nil);
  QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

  QueryAux:=TIBQuery.Create(nil);
  QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

  Result:=False;
  try
    with QueryLocal do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM TABDOCUMENTOSECF WHERE EXPORTADO<>''S'' ');
      if(Trim(comebarra(edtDataInicial.Text))<>'')
      then  sql.Add('AND DATAC>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
      if(Trim(comebarra(edtDataFinal.Text))<>'')
      then  sql.Add('AND DATAC<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

      Open;

      while not eof do
      begin
        if not(ObjDocumentosECF.LocalizaCodigo(fieldbyname('codigo').AsString)) then
        begin
          ObjDocumentosECF.ZerarTabela;
          ObjDocumentosECF.Status:=dsInsert;
          ObjDocumentosECF.Submit_CODIGO(fieldbyname('CODIGO').AsString);
          ObjDocumentosECF.ECF.Submit_Codigo(fieldbyname('ECF').AsString);
          ObjDocumentosECF.Submit_CONTADORORDEMOPERACAO(fieldbyname('CONTADORORDEMOPERACAO').AsString);
          ObjDocumentosECF.Submit_CONTADORGERALNAOFISCAL(fieldbyname('CONTADORGERALNAOFISCAL').AsString);
          ObjDocumentosECF.Submit_CONTADORGERALRELGERENCIAL(fieldbyname('CONTADORGERALRELGERENCIAL').AsString);
          ObjDocumentosECF.Submit_CONTADORCREDITODEBITO(fieldbyname('CONTADORCREDITODEBITO').AsString);
          ObjDocumentosECF.Submit_SIGLA(fieldbyname('SIGLA').AsString);
          ObjDocumentosECF.Submit_DATA(fieldbyname('DATA').AsString);
          ObjDocumentosECF.Submit_HORA(fieldbyname('HORA').AsString);
          ObjDocumentosECF.Submit_Cancelado(fieldbyname('Cancelado').AsString);
          ObjDocumentosECF.Submit_DataHoraCancelamento(fieldbyname('DataHoraCancelamento').AsString);
          ObjDocumentosECF.Submit_UsuarioCancelamento(fieldbyname('UsuarioCancelamento').AsString);
          ObjDocumentosECF.Submit_CodigoRecebimentoVenda(fieldbyname('CodigoRecebimentoVenda').AsString);
          ObjDocumentosECF.Submit_EXPORTADO(fieldbyname('EXPORTADO').AsString);

          ObjDocumentosECF.Submit_NUMEROSERIE(fieldbyname('NUMEROSERIE').AsString);
          ObjDocumentosECF.Submit_NUMEROSERIECRIPTO(fieldbyname('NUMEROSERIECRIPTO').AsString);
          ObjDocumentosECF.Submit_CONTADORORDEMOPERACAOCRIPTO(fieldbyname('CONTADORORDEMOPERACAOCRIPTO').AsString);
          ObjDocumentosECF.Submit_CONTADORCREDITODEBITOCRIPTO(fieldbyname('CONTADORCREDITODEBITOCRIPTO').AsString);
          ObjDocumentosECF.Submit_SIGLACRIPTO(fieldbyname('SIGLACRIPTO').AsString);
          ObjDocumentosECF.Submit_DATACRIPTO(fieldbyname('DATACRIPTO').AsString);
          ObjDocumentosECF.Submit_CONTADORGERALNAOFISCALCRIPTO(fieldbyname('CONTADORGERALNAOFISCALCRIPTO').AsString);

          if not(ObjDocumentosECF.Salvar(False)) then
          begin
            Result:=False;
            Exit;
          end;

          QueryAux.Close;
          QueryAux.SQL.Clear;
          QUERYAUX.SQL.ADD('UPDATE TABDOCUMENTOSECF SET EXPORTADO=''S'' ');
          QueryAux.SQL.Add('WHERE CODIGO='+fieldbyname('codigo').AsString);
          try
            QueryAux.ExecSQL;
          except
            on e:exception do
            begin
              MensagemErro('Erro ao atualizar registro de origem para exportado.'+#13+'Erro: '+e.Message);
              Exit;
            end;
          end;
        end;

        Next;
      end;
      Result:=True;
    end;
  finally
    FreeAndNil(QueryAux);
    FreeAndNil(QueryLocal);
    ObjDocumentosECF.Free;
  end;
end;

function TFRImportaDadosCaixasPAF.ImportaAlteracaoCaixa: Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
  ObjAlteracaoCaixa:=TObjALTERACAOCAIXA.Create;

  QueryLocal:=TIBQuery.Create(nil);
  QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

  QueryAux:=TIBQuery.Create(nil);
  QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

  Result:=False;
  try
    with QueryLocal do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM TABALTERACAOCAIXA WHERE EXPORTADO<>''S'' ');
      if(Trim(comebarra(edtDataInicial.Text))<>'')
      then  sql.Add('AND DATAC>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
      if(Trim(comebarra(edtDataFinal.Text))<>'')
      then  sql.Add('AND DATAC<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

      Open;

      while not eof do
      begin
        if not(ObjAlteracaoCaixa.LocalizaCodigo(fieldbyname('codigo').AsString)) then
        begin
          ObjAlteracaoCaixa.ZerarTabela;
          ObjAlteracaoCaixa.Status:=dsInsert;
          ObjAlteracaoCaixa.Submit_CODIGO(fieldbyname('CODIGO').AsString);
          ObjAlteracaoCaixa.Fechamento.Submit_CODIGO(fieldbyname('FECHAMENTO').AsString);
          ObjAlteracaoCaixa.Submit_Data(fieldbyname('DATA').AsString);
          ObjAlteracaoCaixa.Submit_Hora(fieldbyname('HORA').AsString);
          ObjAlteracaoCaixa.Submit_NUmCupom(fieldbyname('NUMCUPOM').AsString);
          ObjAlteracaoCaixa.Submit_Valor(fieldbyname('VALOR').AsString);
          ObjAlteracaoCaixa.Submit_SangriaSuprimento(fieldbyname('SANGRIASUPRIMENTO').AsString);

          if not(ObjAlteracaoCaixa.Salvar(False)) then
          begin
            Result:=False;
            Exit;
          end;

          QueryAux.Close;
          QueryAux.SQL.Clear;
          QUERYAUX.SQL.ADD('UPDATE TABALTERACAOCAIXA SET EXPORTADO=''S'' ');
          QueryAux.SQL.Add('WHERE CODIGO='+fieldbyname('CODIGO').AsString);
          try
            QueryAux.ExecSQL;
          except
            on e:exception do
            begin
              MensagemErro('Erro ao atualizar registro de origem para exportado na tabela de altera��o de caixa.'+#13+'Erro: '+e.Message);
              Exit;
            end;
          end;
        end;

        Next;
      end;
      Result:=True;
    end;
  finally
    FreeAndNil(QueryAux);
    FreeAndNil(QueryLocal);
    ObjAlteracaoCaixa.Free;
  end;
end;

function TFRImportaDadosCaixasPAF.ImportaEstoqueProducao: Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
  ObjEstoqueProducao:=TObjESTOQUEPRODUCAO.Create;

  QueryLocal:=TIBQuery.Create(nil);
  QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

  QueryAux:=TIBQuery.Create(nil);
  QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

  Result:=False;
  try
    with QueryLocal do
    begin
      Close;
      SQL.Clear;
      SQL.Add('SELECT * FROM TABESTOQUEPRODUCAO WHERE EXPORTADO<>''S'' ');
      if(Trim(comebarra(edtDataInicial.Text))<>'')
      then  sql.Add('AND DATAC>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
      if(Trim(comebarra(edtDataFinal.Text))<>'')
      then  sql.Add('AND DATAC<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

      Open;

      while not eof do
      begin
        if not(ObjEstoqueProducao.LocalizaCodigo(fieldbyname('codigo').AsString)) then
        begin
          ObjEstoqueProducao.ZerarTabela;
          ObjEstoqueProducao.Status:=dsInsert;
          ObjEstoqueProducao.Submit_CODIGO(fieldbyname('CODIGO').AsString);
          ObjEstoqueProducao.Submit_QUANTIDADE(fieldbyname('QUANTIDADE').AsString);
          ObjEstoqueProducao.Submit_DATA(fieldbyname('DATA').AsString);
          ObjEstoqueProducao.Submit_BAIXAESTOQUE(fieldbyname('BAIXAESTOQUE').AsString);
          ObjEstoqueProducao.Submit_EXPORTADO(fieldbyname('EXPORTADO').AsString);
          ObjEstoqueProducao.USUARIO.Submit_CODIGO(fieldbyname('USUARIO').AsString);
          ObjEstoqueProducao.PRODUTO.Submit_CODIGO(fieldbyname('PRODUTO').AsString);

          if not(ObjEstoqueProducao.Salvar(False)) then
          begin
            Result:=False;
            Exit;
          end;

          QueryAux.Close;
          QueryAux.SQL.Clear;
          QUERYAUX.SQL.ADD('UPDATE TABESTOQUEPRODUCAO SET EXPORTADO=''S'' ');
          QueryAux.SQL.Add('WHERE CODIGO='+fieldbyname('CODIGO').AsString);
          try
            QueryAux.ExecSQL;
          except
            on e:exception do
            begin
              MensagemErro('Erro ao atualizar registro de origem para exportado na tabela de Produ��o de Estoque.'+#13+'Erro: '+e.Message);
              Exit;
            end;
          end;
        end;

        Next;
      end;
      Result:=True;
    end;
  finally
    FreeAndNil(QueryAux);
    FreeAndNil(QueryLocal);
    ObjEstoqueProducao.Free;
  end;
end;

function TFRImportaDadosCaixasPAF.ImportaMovimentoECF:Boolean;
var
  QueryLocal:TIBQuery;
  QueryAux:TIBQuery;
begin
     ObjMovimentoECF:=TObjMOVIMENTOECF.Create;

     QueryLocal:=TIBQuery.Create(nil);
     QueryLocal.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     QueryAux:=TIBQuery.Create(nil);
     QueryAux.Database:=FDataModuloAmanda.IbDatabaseAmanda;

     Result:=False;
     try
         with QueryLocal do
         begin
              Close;
              SQL.Clear;
              SQL.Add('select * from tabmovimentoecf where exportado<>''S'' ');
              if(edtDataInicial.Text<>'  /  /    ')
              then  sql.Add('and datac>='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataInicial.Text))+' 00:00'+#39)  ;
              if(edtDataFinal.Text<>'  /  /    ')
              then  sql.Add('and datac<='+#39+FormatDateTime('mm/dd/yyyy',StrToDate(edtDataFinal.Text))+' 23:59'+#39) ;

              Open;

              while not eof do
              begin
                      if(ObjMovimentoECF.LocalizaCodigo(fieldbyname('codigo').AsString)=False) then
                      begin
                            ObjMovimentoECF.Status:=dsInsert;
                            ObjMovimentoECF.Submit_CODIGO(fieldbyname('CODIGO').AsString);
                            ObjMovimentoECF.Submit_DATA(fieldbyname('DATA').AsString);
                            ObjMovimentoECF.Submit_HORA(fieldbyname('HORA').AsString);
                            ObjMovimentoECF.Submit_COOINICIAL(fieldbyname('COOINICIAL').AsString);
                            ObjMovimentoECF.ecf.Submit_Codigo(fieldbyname('ECF').AsString);
                            ObjMovimentoECF.CAIXA.Submit_CODIGO(fieldbyname('CAIXA').AsString);
                            
                            if(ObjMovimentoECF.Salvar(False)=False) then
                            begin
                                 Result:=False;
                                 Exit;
                            end;

                            QueryAux.Close;
                            QueryAux.SQL.Clear;
                            QueryAux.SQL.Add('UPDATE TABMOVIMENTODIA SET EXPORTADO=''S'' ');
                            QueryAux.SQL.Add('WHERE CODIGO='+fieldbyname('CODIGO').AsString);
                            QueryAux.ExecSQL;
                      end;

                      Next;
              end;
              Result:=True;
         end;
     finally
          FreeAndNil(QueryAux);
          FreeAndNil(QueryLocal);
          ObjMovimentoECF.Free
     end;
end;



procedure TFRImportaDadosCaixasPAF.Timer1Timer(Sender: TObject);
begin
  btImportacaoClick(sender);
end;

{este procedimento ser� utilizado para resgatar o movimento de entrada/saida
a partir de um arquivo e ent�o salvar no bd para movimentar o estoque
leiaute do arquivo

E;nf;cgc;produto;quantidade;st_a_consumidorfinal;st_b_consumidorfinal;cfop_consumidor;csosnconsumidorfinal;ncm;descricao;unidade;custo_consumidor;preco_consumidor;ativo
0  1   2       3          4                    5                    6               7                    8   9        10      11               12               13    14

E;5540;03743085000196;;4266;1;0;0;;;90178090;TRENA 50 MT FIBRA DE VIDRO FG 5008;PC;37,8868;0;S
E;1111;43.214.055/0071-10;;16;1;0;0;5949;500;84219999;FILTRO DE AR EFA 414;UN;1;10;S

S;NF;Produto;qtde
S;19850;16;1;
S;19850;17;1;}

function TFRImportaDadosCaixasPAF.AtualizaEstoque(pArquivo:string): Boolean;
var
  querylocal:TIBQuery;
  arquivo,Linha:TStringList;
  cont,colunaproduto,colunaquantidade:Integer;
begin

  Result:= false;

  querylocal := TIBQuery.Create(nil);
  querylocal.Database := FDataModulo.IBDatabase;

  arquivo := TStringList.Create;
  linha   := TStringList.Create;

  objSaidaRetaguarda   := TObjSAIDARETAGUARDA.Create ();
  ObjProduto           := TObjPRODUTO.Create ('TABPRODUTO',FDataModulo.IBDatabase,FDataModulo.IBTransaction);

  try

    arquivo.LoadFromFile (pArquivo);

    if (arquivo.Count = 0) then
    begin

      MensagemAviso ('N�o h� informa��es no arquivo');
      Exit;

    end;

    for cont := 0 to arquivo.Count -1 do
    begin

      if not (ExplodeStr_COMVAZIOS (arquivo[Cont],Linha,';','string')) then
      begin

        MensagemErro ('Erro ao separar dados na linha ' + inttostr (cont));
        Exit;

      end;

      if (Linha[0] = 'E') then
      begin
        colunaproduto := 3;
        colunaquantidade := 4;

        if (ObjProduto.LocalizaCodigo (Linha[colunaproduto])) then
        begin

          ObjProduto.TabelaparaObjeto ();
          ObjProduto.Status := dsEdit;

        end
        else
        begin

          ObjProduto.ZerarTabela;
          //ObjProduto.preencherValorPadrao ();

          ObjProduto.Status:=dsInsert;
          ObjProduto.Submit_CODIGO (Linha[colunaproduto]);

        end;

        {Bloco de instru��es em comum nas duas rotinas do "if" superior}
        //ObjProduto.Submit_QUANTIDADEEMBALAGEM               (Linha[4]);
        ObjProduto.SituacaoTributaria_TabelaA.Submit_CODIGO (Linha[5]);
        ObjProduto.SituacaoTributaria_TabelaB.Submit_CODIGO (Linha[6]);
        ObjProduto.CFOP.Submit_CODIGO                       (Linha[7]);
        ObjProduto.Submit_cfopforaestado                    (Linha[7]);
        ObjProduto.CSOSN.Submit_codigo                      (Linha[8]);
        ObjProduto.Submit_NCM                               (Linha[9]);
        ObjProduto.Submit_NOME                              (Linha[10]);
        ObjProduto.Submit_UNIDADE                           (Linha[11]); {ver unidadeCripto}
        ObjProduto.Submit_PRECOVENDA                        (Linha[12]); {ver aqui: pre�o venda?}
        ObjProduto.Submit_PRECOSUGERIDO                     (Linha[13]); {ver aqui: precosugerido?}
        ObjProduto.Submit_ATIVO                             (Linha[14]);

        ObjProduto.LocalizaCodigo('0','TABPRODUTO_DEFAULT');
        ObjProduto.TabelaparaObjeto(False);

        if not (ObjProduto.Salvar (false)) then
        begin

          MensagemErro ('Erro ao salvar produto de c�digo: ' + Linha[3]);
          Exit;

        end;
      end
      else
      begin
        colunaproduto := 2;
        colunaquantidade := 3;
      end;

      if (Linha[0] = 'E') then
      begin

        if (Linha.Count <> 15) then
        begin

          MensagemErro ('Quantidade de colunas diferentes de 15: '+IntToStr (Linha.Count));
          Exit;

        end;

        {primeiro verifico se j� existe movimento para essa nf e cnpj}
        if not (VerificaEntrada (Linha[1],RetornaSoNumeros(Linha[2]),querylocal)) then
        begin

          querylocal.Close;
          querylocal.SQL.Clear;

          querylocal.SQL.Add('INSERT INTO TABENTRADARETAGUARDA (CODIGO,nf,cgc,produto,quantidade) values');
          querylocal.SQL.Add('(:CODIGO,:nf,:cgc,:produto,:quantidade)');

          querylocal.ParamByName ('CODIGO').AsString               := '0';
          querylocal.ParamByName ('nf').AsString                   := Linha[1];
          querylocal.ParamByName ('cgc').AsString                  := RetornaSoNumeros(Linha[2]);
          querylocal.ParamByName ('produto').AsString              := Linha[colunaproduto];
          querylocal.ParamByName ('quantidade').AsString           := Linha[colunaquantidade];

        end;

      end
      else if (Linha [0] = 'S') then
      begin

        querylocal.Close;
        querylocal.SQL.Clear;

        querylocal.SQL.Text:=objSaidaRetaguarda.InsertSql.Text;

        querylocal.ParamByName ('CODIGO').AsString     := '0';
        querylocal.ParamByName ('NF').AsString         := Linha[1];
        querylocal.ParamByName ('PRODUTO').AsString    := Linha[colunaproduto];
        querylocal.ParamByName ('QUANTIDADE').AsString := Linha[colunaquantidade];

      end;

      {Bloco de instru��es em comum nas duas rotinas do "if" superior}
      try

        querylocal.ExecSQL;

      except

        on e:Exception do
        begin

          MensagemErro (e.Message);
          Exit;

        end;

      end;

      if (Linha[0] = 'E') then
        {aumentaEstoque}
        ObjProduto.AumentaEstoqueProdutoConcentrador(Linha[colunaproduto],Linha[colunaquantidade],False)
      else
        {BaixaEstoque}
        ObjProduto.BaixaEstoqueProdutoConcentrador(Linha[colunaproduto],Linha[colunaquantidade],False);

    end;

  finally

    if (querylocal <> nil) then FreeAndNil (Querylocal);
    if (arquivo    <> nil) then FreeAndNil (Arquivo);
    if (Linha      <> nil) then FreeAndNil (Linha);

    if (objSaidaRetaguarda   <> nil) then objSaidaRetaguarda.Free;
    if (ObjProduto           <> nil) then ObjProduto.Free;

  end;

  result := True;

end;

procedure TFRImportaDadosCaixasPAF.Button2Click(Sender: TObject);
begin

  if (AtualizaEstoque (edtpath.Text)) then
    FDataModulo.IBTransaction.CommitRetaining
  else
    FDataModulo.IBTransaction.RollbackRetaining;

end;

function TFRImportaDadosCaixasPAF.VerificaEntrada(pNF, pCnpj: string;
  pquery: tibquery): Boolean;
begin
  result:=False;
  pquery.Close;
  pquery.SQL.Clear;
  pquery.SQL.Add('SELECT CODIGO FROM TABENTRADARETAGUARDA');
  pquery.SQL. Add('WHERE NF='+QuotedStr(pNF)+' AND CGC='+QuotedStr(pCnpj));

  pquery.Open;
  if(pquery.RecordCount>0)
  then begin
    result := True;
    Exit;
  end;

end;

end.
