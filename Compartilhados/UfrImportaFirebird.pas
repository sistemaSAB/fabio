  unit UfrImportaFirebird;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, IBDatabase, StdCtrls, Buttons,
  ComCtrls, Grids, DBGrids, ExtCtrls, IBTable,uobjimporta;

type
  TFrImportaFirebird = class(TFrame)
    IBTransaction1: TIBTransaction;
    IBQuery1: TIBQuery;
    OpenDialog: TOpenDialog;
    DataSource: TDataSource;
    IBTable1: TIBTable;
    GuiaFirebird: TPageControl;
    TabSheet1: TTabSheet;
    MemoSQL: TMemo;
    PanelCaminhotabela: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    ComboTabela: TComboBox;
    BitBtn2: TBitBtn;
    edtcaminhobanco: TEdit;
    btcaminhobanco: TBitBtn;
    DbGrid: TDBGrid;
    TabSheet2: TTabSheet;
    LbCampos: TListBox;
    LbValor: TListBox;
    Panel1: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    PanelNovoValor: TPanel;
    Button1: TButton;
    Rg_Opcoes_valor: TRadioGroup;
    BarradeProgresso: TProgressBar;
    IBDatabase1: TIBDatabase;
    procedure MemoSQLKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btcaminhobancoClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure LbCamposKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LbCamposClick(Sender: TObject);
    procedure LbValorClick(Sender: TObject);
    procedure LbValorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BitBtn4Click(Sender: TObject);
    procedure LbValorKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
    procedure ComboTabelaKeyPress(Sender: TObject; var Key: Char);
  private
         Objimporta:tobjImporta;
    { Private declarations }
    procedure Importa;
  public
    { Public declarations }
    Procedure InicializaFrame;
    Procedure DestroiFrame;
  end;

implementation

uses UessencialGlobal, UDataModulo, UFiltraImp;

{$R *.dfm}

procedure TFrImportaFirebird.MemoSQLKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (ssctrl in Shift) and (Key=13)
     Then Begin
               IBQuery1.close;
               IBQuery1.sql.clear;
               IBQuery1.sql.Text:=MemoSql.text;
               IBQuery1.open;
     End;
end;

procedure TFrImportaFirebird.BitBtn2Click(Sender: TObject);
begin
     if (IBQuery1.Active=False)
     then begin
               MensagemAviso('Execute o comando que ir� trazer os dados');
               exit;
     End;

     Self.Importa;
end;

procedure TFrImportaFirebird.Importa;
var
cont:integer;
pnometabela,Pvalor,pvalorbanco:string;
begin

    if (LbCampos.items.count=0)
    Then Begin
              Mensagemaviso('Configure os campos que ser�o importados');
              exit;
    End;

    if (IBQuery1.Active=False)
    Then Begin
              mensagemaviso('Execute o Comando SQL');
              exit;
    End;

    Pnometabela:=ComboTabela.text;
try
    if (Self.ObjImporta.InstanciaObjeto(pnometabela)=False)
    then Begin
              Mensagemerro('Erro na tentativa de instanciar o objeto da '+pnometabela);
              exit;
    End;

    Try
        IBQuery1.last;
        BarradeProgresso.Position:=0;
        BarradeProgresso.Max:=IBQuery1.RecordCount;
        IBQuery1.First;
        While not(IBQuery1.Eof) do
        Begin
             Application.ProcessMessages;
             BarradeProgresso.Position:=BarradeProgresso.Position+1;
             Objimporta.ZerarTabela;
             for cont:=0 to LbCampos.Items.count-1 do
             Begin
                  Pvalorbanco:=IBQuery1.FieldByname(uppercase(LbCampos.Items[cont])).AsString;
                  Pvalor:=lbvalor.items[cont];
                  Pvalor:=StrReplace(Pvalor,'[VALORBANCO]',pvalorbanco);
                  Pvalor:=StrReplace(Pvalor,'[VAZIO]','');

                  Objimporta.fieldbyname(uppercase(LbCampos.Items[cont]),Pvalor);
             End;

             if (Objimporta.Salvar(False)=False)
             Then Begin
                       if (messagedlg('Deseja Continuar?',mtconfirmation,[mbyes,mbno],0)=mrno)
                       Then Begin
                                 if (messagedlg('Deseja Commitar at� o momento?',mtconfirmation,[mbyes,mbno],0)=mryes)
                                 Then FDataModulo.IBTransaction.CommitRetaining;
                                 exit;
                       End;
             End;

             IBQuery1.next;
        End;
        FDataModulo.IBTransaction.CommitRetaining;
        MensagemAviso('Exporta�ao Realizada com Sucesso!');

    except
        FDataModulo.IBTransaction.RollbackRetaining;
    end;

finally
    Self.ObjImporta.destroiobjeto;
end;


end;

procedure TFrImportaFirebird.BitBtn1Click(Sender: TObject);
begin
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.enabled:=True;
          Grupo02.enabled:=True;
          edtgrupo01.EditMask:='';
          edtgrupo02.EditMask:='';
          edtgrupo02.PasswordChar:='#';
          edtgrupo01.OnKeyDown:=nil;
          edtgrupo02.OnKeyDown:=nil;
          edtgrupo01.CharCase:=ecUpperCase;
          LbGrupo01.caption:='Usu�rio';
          LbGrupo02.caption:='Senha';
          showmodal;

          if (tag=0)
          then exit;

          IBDatabase1.close;
          IBDatabase1.DatabaseName:=edtcaminhobanco.Text;
          IBDatabase1.Params.clear;
          IBDatabase1.Params.Add('User_name='+ edtgrupo01.Text );
          IBDatabase1.Params.Add('password='+edtgrupo02.Text);
          IBDatabase1.Open;
     End;
end;

procedure TFrImportaFirebird.btcaminhobancoClick(Sender: TObject);
begin
     if (OpenDialog.Execute=False)
     then exit;
     edtcaminhobanco.Text:=OpenDialog.FileName;
end;

procedure TFrImportaFirebird.BitBtn3Click(Sender: TObject);
var
cont:integer;
begin
     LbCampos.Items.clear;
     LbValor.items.clear;
     for cont:=0 to IBQuery1.Fields.count-1 do
     Begin
          LbCampos.Items.add(uppercase(IBQuery1.Fields[cont].FieldName));
          LbValor.items.add('[VALORBANCO]');
     End;
end;

procedure TFrImportaFirebird.LbCamposKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
Pselecionado:integer;
pnovocampo:String;
begin
     LbValor.ItemIndex:=LbCampos.itemindex;

     if (key=VK_DELETE)
     Then begin
               Pselecionado:=LbCampos.ItemIndex;
               LbCampos.DeleteSelected;
               LbValor.itemindex:=Pselecionado;
               LbValor.DeleteSelected;
     End;

     if (key=VK_INSERT)
     Then Begin
               if (InputQuery('Novo Campo','Digite o nome do campo',pnovocampo)=False)
               Then exit;
               if (Pnovocampo<>'')
               then Begin
                         LbCampos.Items.add(uppercase(pnovocampo));
                         LbValor.items.add('[VALORBANCO]');
               End;
     end;
end;

procedure TFrImportaFirebird.LbCamposClick(Sender: TObject);
begin
    LbValor.ItemIndex:=LbCampos.itemindex;
end;

procedure TFrImportaFirebird.LbValorClick(Sender: TObject);
begin
    LbCampos.ItemIndex:=LbValor.itemindex;
end;

procedure TFrImportaFirebird.LbValorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     LbCampos.ItemIndex:=Lbvalor.itemindex;
end;

procedure TFrImportaFirebird.BitBtn4Click(Sender: TObject);
begin
     LbCampos.Items.clear;
     lbvalor.items.clear;
end;

procedure TFrImportaFirebird.LbValorKeyPress(Sender: TObject; var Key: Char);
begin
     if (Key=#13)//altera valor do campo
     Then Begin


     End;
end;

procedure TFrImportaFirebird.Button1Click(Sender: TObject);
var
PvalorPadrao:String;
begin
     case Rg_Opcoes_valor.ItemIndex of

     0:LbValor.Items[LbCampos.ItemIndex]:='[VALORBANCO]';
     1:LbValor.Items[LbCampos.ItemIndex]:='[VAZIO]';
     2:begin
            PvalorPadrao:='';
            if (inputquery('Valor Padr�o','Digite o valor padr�o',pvalorpadrao)=False)
            Then exit;

            LbValor.Items[LbCampos.ItemIndex]:='[VAZIO]'+Pvalorpadrao;
     end;
     3:begin
            PvalorPadrao:='';
            if (inputquery('Valor Concatenado','Digite o valor a ser concatenado',pvalorpadrao)=False)
            Then exit;

            LbValor.Items[LbCampos.ItemIndex]:='[VALORBANCO]'+Pvalorpadrao;
     end;
     End;
end;

procedure TFrImportaFirebird.ComboTabelaKeyPress(Sender: TObject;
  var Key: Char);
var
cont:integer;
pcampos:String;

begin
     if (key=#13)
     Then Begin
               if (Messagedlg('Deseja gerar o Sql?',mtconfirmation,[mbyes,mbno],0)=mrno)
               Then exit;

               IBTable1.close;
               IBTable1.TableName:=ComboTabela.Text;
               IBTable1.Open;

               PCampos:='Select ';
               for cont:=0 to IBTable1.Fields.count-1 do
               Begin
                    if (Cont>0)
                    Then Pcampos:=Pcampos+',';
                    Pcampos:=Pcampos+IBTable1.Fields[cont].FieldName;
               end;
               Pcampos:=PCampos+' from '+Combotabela.Text;

               MemoSQL.Lines.clear;
               MemoSQL.Lines.text:=Pcampos;
     End;
end;

procedure TFrImportaFirebird.DestroiFrame;
begin
    IBDatabase1.Close;
    Objimporta.free;
end;

procedure TFrImportaFirebird.InicializaFrame;
begin
     IBDatabase1.Close;
     GuiaFirebird.ActivePageIndex:=0;
     Self.Objimporta:=tobjimporta.create(self);
     Self.ObjImporta.RetornaTabelasDisponiveis(ComboTabela.Items);
end;

end.
