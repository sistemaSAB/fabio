{jonas 25/05/2011 10:19}

unit mdGraphicText;

interface

uses
  SysUtils, Graphics, Types;

type
  TGraphicText = class
  private
    fCanvas: TCanvas;
    fRect: TRect;
    fLine: Integer;
    fColumn: Integer;
    fLastText: string;
    function GetFont: TFont;
    function GetBrush: TBrush;
    function GetFontWeight: Integer;
    function GetFontWidth: Integer;
    procedure SetColumn(const aValue: Integer);
    procedure SetLine(const aValue: Integer);
  public
    procedure SetCanvas(aValue: TCanvas);
    procedure SetRect(aValue: TRect);
    procedure Write(const aText: string);
    procedure NewLine;
    procedure WriteLine(const aText: string);
    procedure FillRect(rect: TRect);
    property Font: TFont read GetFont;
    property Brush: TBrush read GetBrush;
    property Line: Integer read fLine write SetLine;
    property Column: Integer read fColumn write SetColumn;
    property Canvas: TCanvas read fCanvas;
  end;

implementation

{ TGraphicText }

function TGraphicText.GetFont: TFont;
begin
  Result := fCanvas.Font;
end;

function TGraphicText.GetBrush: TBrush;
begin
  Result := fCanvas.Brush;
end;

function TGraphicText.GetFontWeight: Integer;
begin
  Result := fCanvas.TextHeight(fLastText);
end;

function TGraphicText.GetFontWidth: Integer;
begin
  if Length(fLastText) = 0 then
    Result := 0
  else
    Result := fCanvas.TextWidth(fLastText);
end;

procedure TGraphicText.SetColumn(const aValue: Integer);
begin
  fColumn := aValue;
end;

procedure TGraphicText.SetLine(const aValue: Integer);
begin
  fLine := aValue;
end;

procedure TGraphicText.SetCanvas(aValue: TCanvas);
begin
  fCanvas := aValue;
end;

procedure TGraphicText.SetRect(aValue: TRect);
begin
  fRect := aValue;
  SetColumn(fRect.Left + 2);
  SetLine(fRect.Top + 2);
end;

procedure TGraphicText.Write(const aText: string);
begin
  fLastText := aText;
  fCanvas.TextOut(fColumn, fLine, fLastText);
  SetColumn(fColumn + GetFontWidth);
end;

procedure TGraphicText.NewLine;
begin
  SetLine(fLine + GetFontWeight);
  SetColumn(fRect.Left + 2);
end;

procedure TGraphicText.WriteLine(const aText: string);
begin
  Write(aText);
  NewLine;
end;

procedure TGraphicText.FillRect(rect: TRect);
begin
  fCanvas.FillRect(rect);
end;

end.

{-}
