unit uobjencerrasistema;
interface

uses forms,sysutils,extctrls,uessencialglobal,ibquery,ibdatabase;

type
      Tobjencerrasistema=class
      public
            AtivarDesligamento:boolean;
            function desligatimer:boolean;
            constructor create(ParametroFormPrincipal:Tform);
            destructor free;

      private
            Query:tibquery;
            TimerVerificaEncerrar: TTimer;
            timeravisaencerramento: TTimer;
            NumeroTentativas:integer;
            PFormPrincipal:Tform;
            TempoRestanteEncerrarGlobal:integer;

            Contaverificacoes:integer;

            procedure timeravisaencerramentoTimer(Sender: TObject);
            procedure TimerVerificaEncerrarTimer(Sender: TObject);

      end;




implementation

uses udatamodulo,UmostraMEnsagem;

{ Tobjencerrasistema }

constructor Tobjencerrasistema.create(ParametroFormPrincipal:Tform);
begin
     Self.AtivarDesligamento:=False;
     self.Contaverificacoes:=0;

     Self.TimerVerificaEncerrar:=TTimer.create(nil);
     //Self.TimerVerificaEncerrar.Interval:=60000; //60 e 60 seg

     Self.TimerVerificaEncerrar.Interval:=60000;


     Self.TimerVerificaEncerrar.OnTimer:=Self.TimerVerificaEncerrarTimer;
     Self.TimerVerificaEncerrar.Enabled:=True;

     Self.timeravisaencerramento:=TTimer.create(nil);
     Self.timeravisaencerramento.Interval:=10000;
     Self.timeravisaencerramento.enabled:=False;
     Self.timeravisaencerramento.OnTimer:=Self.timeravisaencerramentoTimer;

     Self.PFormPrincipal:=ParametroFormPrincipal;

     self.NumeroTentativas:=0;
     self.TempoRestanteEncerrarGlobal:=70;

     Self.Query:=TIBQuery.Create(nil);
     Self.Query.Database:=FDataModulo.IBDatabase;



end;

procedure TObjEncerraSistema.TimerVerificaEncerrarTimer(Sender: TObject);
var
   pnaoativa:boolean;
begin
     With Self.Query do
     Begin
          pnaoativa:=False;
          Self.TimerVerificaEncerrar.Enabled:=False;
          
          inc(Contaverificacoes,1);
          
          Try
          
             Try
          
                   close;
                   sql.clear;
                   sql.add('Select valor from tabparametros where nome=''DESLIGAR O SISTEMA AGORA''');
                   open;
          
                   //o Ativar Desligamento � quando por algum motivo fora o parametro no BD
                   //que � necess�rio o desligamento

                   //Exemplo: O Sistema verifica que a senha ta vencida, para evitar que o cliente
                   //deixe a maquina ligada para n�o validar a senha

                   //O Sistema verifica a cada X horas, se a senha estiver vencida ele ativa a variavel
                   //AtivarDesligamento 


                   if (FieldByName('valor').asstring='SIM') or (Self.AtivarDesligamento=True)
                   Then Begin
                             pnaoativa:=True;//para nao ativar esse timer verificador de novo no finally
                             TempoRestanteEncerrarGlobal:=60;//60 segundos para fechar
                             timeravisaencerramento.Enabled:=true;
                   End


             Except
          
             End;
          
          
          Finally
                 close;
          
                 if pnaoativa=false
                 then TimerVerificaEncerrar.Enabled:=true;
          End;
     End;
end;

procedure TObjEncerraSistema.timeravisaencerramentoTimer(Sender: TObject);
begin
     if (TempoRestanteEncerrarGlobal>0)
     Then TempoRestanteEncerrarGlobal:=TempoRestanteEncerrarGlobal-10;

     if (TempoRestanteEncerrarGlobal<=0)
     Then Begin
                PfechasistemaGlobal:=True;//para nao perguntar se tem certeza que deseja fechar o sistema
                
                inc(Self.numerotentativas,1);
                if (Self.numerotentativas<5)
                Then Self.PFormPrincipal.close//fechando o principal ele fecha as conexoes com o BD
                Else Application.Terminate;
     End
     Else Begin

               Fmostramensagem.BorderStyle:=bsDialog;
               Fmostramensagem.Lbmensagemprincipal.caption:='O Sistema ser� encerrado. Voc� tem '+inttostr(temporestanteencerrarglobal)+' seg';
               Fmostramensagem.Lbmensagemsecundaria.Caption:=' para terminar a atividade atual';
               Fmostramensagem.lboriginal_demo.caption:='';
               Fmostramensagem.Show;
               Application.ProcessMessages;
     End;
end;


destructor Tobjencerrasistema.free;
begin
     Try
         Self.TimerVerificaEncerrar.Enabled:=False;
     Except

     End;

     freeandnil(Self.query);
     FreeAndNil(TimerVerificaEncerrar);
     FreeAndNil(timeravisaencerramento);

    
     //jonas
      //FreeAndNil(timerVerificaEncerrar);
      //FreeAndNil(timeravisaencerramento);
     //end-jonas

end;


function Tobjencerrasistema.desligatimer: boolean;
begin
     result:=False;
     Try
         Self.TimerVerificaEncerrar.Enabled:=False;
         result:=True;
     Except

     End;

end;

end.
