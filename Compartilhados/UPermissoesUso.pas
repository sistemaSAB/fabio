unit UPermissoesUso;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, StdCtrls, Buttons, ExtCtrls,db, UObjPermissoesUso,
  Grids, DBGrids, UObjUsuarios;

type
  TFPermissoesUso = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    btcancelar: TBitBtn;
    btgravar: TBitBtn;
    btexcluir: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    LbNivel: TLabel;
    Label4: TLabel;
    EdtCodigo: TEdit;
    EdtNome: TEdit;
    Edtnivel: TEdit;
    combocontrolapornivel: TComboBox;
    dbgridusuarios: TDBGrid;
    dbgridusados: TDBGrid;
    Panel2: TPanel;
    ImagemFundo: TImage;
    Label5: TLabel;
    lbl1: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure EdtnivelKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btopcoesClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure dbgridusuariosDblClick(Sender: TObject);
    procedure dbgridusadosDblClick(Sender: TObject);
    procedure dbgridusuariosKeyPress(Sender: TObject; var Key: Char);
  private
         ObjPermissoesUso:TObjPermissoesUso;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         PRocedure LimpaLabels;
         Procedure RetornaUsuarios;
         function atualizaQuantidade:string;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FPermissoesUso: TFPermissoesUso;


implementation

uses Uessencialglobal, Upesquisa, UNiveis, UescolheImagemBotao,
  UobjPERMISSOESEXTRAS, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFPermissoesUso.ControlesParaObjeto: Boolean;
Begin

        Try
        With self.objpermissoesuso do
        Begin
             Submit_nivel(edtnivel.text);
             Submit_CODIGO(edtCODIGO.text);
             Submit_nome(edtnome.text);
             Submit_ControlaPorNivel(Submit_ComboBox(combocontrolapornivel));
             result:=true;
        End;
        Except
          result:=False;
        End;

End;

function TFPermissoesUso.ObjetoParaControles: Boolean;
Begin
        Try
        With self.objpermissoesuso do
        Begin
             edtnivel.text:=get_nivel;
             lbnivel.caption:=Get_nomenivel;
             edtCODIGO.text:=Get_CODIGO;
             edtnome.text:=Get_nome;
             if (Get_ControlaPorNivel='N')
             Then combocontrolapornivel.itemindex:=0
             Else combocontrolapornivel.itemindex:=1;
             Self.Retornausuarios;
             result:=true;
        End;
        Except
              result:=False;
        End;

End;

function TFPermissoesUso.TabelaParaControles: Boolean;
begin
     If (self.objpermissoesuso.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFPermissoesUso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  If (self.objpermissoesuso=Nil) Then
    exit;

  If (self.objpermissoesuso.status<>dsinactive) Then
  Begin
    Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
    abort;
    exit;
  End;

  self.objpermissoesuso.free;
  if(Self.Owner = nil) then
    FDataModulo.IBTransaction.Commit;

end;

procedure TFPermissoesUso.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
      end;
end;



procedure TFPermissoesUso.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.LimpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     //edtcodigo.text:=self.objpermissoesuso.Get_novocodigo;
     edtcodigo.text:='0';
     edtcodigo.enabled:=False;


     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;

     self.objpermissoesuso.status:=dsInsert;
     combocontrolapornivel.setfocus;
     Self.Retornausuarios;
     
end;


procedure TFPermissoesUso.btalterarClick(Sender: TObject);
begin
    If (self.objpermissoesuso.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                self.objpermissoesuso.Status:=dsEdit;

                esconde_botoes(Self);
                Btgravar.Visible:=True;
                BtCancelar.Visible:=True;
                btpesquisar.Visible:=True;
                EdtNome.enabled:=false;
                combocontrolapornivel.setfocus;
          End;


end;

procedure TFPermissoesUso.btgravarClick(Sender: TObject);
begin

     If self.objpermissoesuso.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (self.objpermissoesuso.salvar(true)=False)
     Then exit;

     mostra_botoes(Self);
     EdtCodigo.Text:=self.objpermissoesuso.get_codigo;
     //limpaedit(Self);
     //Self.LimpaLabels;
     desabilita_campos(Self);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFPermissoesUso.btexcluirClick(Sender: TObject);
begin
     if (ObjUsuarioGlobal.Get_nome<>'SYSDBA')
     then Exit;

     If (self.objpermissoesuso.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (self.objpermissoesuso.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (self.objpermissoesuso.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     Self.btcancelarClick(nil);
     lbquantidade.caption:=atualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFPermissoesUso.btcancelarClick(Sender: TObject);
begin
     self.objpermissoesuso.cancelar;

     limpaedit(Self);
     Self.LimpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);
     dbgridusados.DataSource.DataSet.Close;
     dbgridusuarios.DataSource.DataSet.Close;

end;

procedure TFPermissoesUso.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFPermissoesUso.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(self.objpermissoesuso.Get_pesquisa,self.objpermissoesuso.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If self.objpermissoesuso.status<>dsinactive
                                  then exit;

                                  If (self.objpermissoesuso.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  self.objpermissoesuso.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.LimpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFPermissoesUso.EdtnivelKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FNiveisX:TFNiveis;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FNiveisX:=TFNiveis.create(self);
            If (FpesquisaLocal.PreparaPesquisa(self.objpermissoesuso.Get_nivelPesquisa,self.objpermissoesuso.Get_nivelTituloPesquisa,FNiveisX)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If TEdit(Sender).text<>''
                                 Then Self.LbNivel.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           freeandnil(FNiveisX);
     End;
end;

procedure TFPermissoesUso.btopcoesClick(Sender: TObject);
begin
     self.objpermissoesuso.opcoes;
end;

procedure TFPermissoesUso.FormShow(Sender: TObject);
begin
     UessencialGlobal.PegaCorForm(Self);

     limpaedit(Self);
     Self.LimpaLabels;
     desabilita_campos(Self);

     Try
        self.objpermissoesuso:=TObjPermissoesUso.create;
        Self.dbgridusuarios.datasource:=self.objpermissoesuso.ObjpermissoesExtras.ObjDatasourceusuarios;
        Self.dbgridusados.datasource:=self.objpermissoesuso.ObjpermissoesExtras.ObjDatasource;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=clwhite;
     lbquantidade.caption:=atualizaQuantidade;
     
     If (self.objpermissoesuso.VerificaPermissao=False)
     Then esconde_botoes(Self)
     Else mostra_botoes(Self);
end;

procedure TFPermissoesUso.LimpaLabels;
begin
     Self.lbnivel.caption:='';
end;

procedure TFPermissoesUso.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (Newtab=1)
     Then Begin
               if (edtcodigo.text='') or (self.objpermissoesuso.status=dsinsert)
               Then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.Retornausuarios;
     End;
end;

procedure TFPermissoesUso.RetornaUsuarios;
begin
     self.objpermissoesuso.ObjpermissoesExtras.RetornaUsuariosDisponiveis(EdtCodigo.text);
     self.objpermissoesuso.ObjpermissoesExtras.RetornaUsuariosUsados(EdtCodigo.text);
     formatadbgrid(Self.dbgridusuarios);
     formatadbgrid(Self.dbgridusados);
end;

procedure TFPermissoesUso.dbgridusuariosDblClick(Sender: TObject);
var
Pusuario:string;
begin
     if self.objpermissoesuso.status=dsinsert
     then Exit;
     
     if (Self.dbgridusuarios.DataSource.DataSet.Active=False)
     then exit;

     if (Self.dbgridusuarios.DataSource.DataSet.recordcount=0)
     then exit;

     if (mensagempergunta('Certeza que deseja inserir o usu�rio?')=mrno)
     Then exit;

     pusuario:=Self.dbgridusuarios.datasource.dataset.fieldbyname('usuario').asstring;
     Self.ObjPermissoesUso.ObjpermissoesExtras.ZerarTabela;
     Self.ObjPermissoesUso.ObjpermissoesExtras.submit_codigo('0');
     Self.ObjPermissoesUso.ObjpermissoesExtras.Submit_usuario(pusuario);
     Self.ObjPermissoesUso.ObjpermissoesExtras.Submit_permissao(edtcodigo.text);
     Self.ObjPermissoesUso.ObjpermissoesExtras.status:=dsinsert;
     Self.ObjPermissoesUso.ObjpermissoesExtras.salvar(true);
     Self.RetornaUsuarios;
end;

procedure TFPermissoesUso.dbgridusadosDblClick(Sender: TObject);
var
Pcodigo:string;
begin
     if self.objpermissoesuso.status=dsinsert
     then Exit;
     
     if (Self.dbgridusados.DataSource.DataSet.Active=False)
     then exit;

     if (Self.dbgridusados.DataSource.DataSet.recordcount=0)
     then exit;

     if (mensagempergunta('Certeza que deseja Excluir o usu�rio?')=mrno)
     Then exit;

     pcodigo:=Self.dbgridusados.datasource.dataset.fieldbyname('codigo').asstring;

     Self.ObjPermissoesUso.ObjpermissoesExtras.Exclui(pcodigo,true);
     Self.RetornaUsuarios;
end;

procedure TFPermissoesUso.dbgridusuariosKeyPress(Sender: TObject;
  var Key: Char);
begin
     if (key=#13)
     Then Self.dbgridusuariosDblClick(sender);
end;

function TFPermissoesUso.atualizaQuantidade: string;
begin
      result:='Existem '+ContaRegistros('tabpermissoesuso','codigo')+' Permiss�es Cadastradas';
end;

end.

