object FrScriptSql_MYSQL: TFrScriptSql_MYSQL
  Left = 0
  Top = 0
  Width = 1270
  Height = 741
  TabOrder = 0
  object Splitter1: TSplitter
    Left = 0
    Top = 260
    Width = 1270
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object DBGridScript: TDBGrid
    Left = 0
    Top = 564
    Width = 1270
    Height = 158
    Align = alBottom
    DataSource = DsScript
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object PanelBotoes: TPanel
    Left = 0
    Top = 0
    Width = 1270
    Height = 219
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 456
      Top = 17
      Width = 24
      Height = 13
      Caption = 'Erros'
    end
    object lblocalizar: TLabel
      Left = 9
      Top = 196
      Width = 56
      Height = 13
      Hint = 'Localizar (CTRL+F)'
      Caption = 'Localizar  |  '
      ParentShowHint = False
      ShowHint = True
      OnClick = lblocalizarClick
    end
    object lblocalizaresubstituir: TLabel
      Left = 86
      Top = 196
      Width = 97
      Height = 13
      Hint = 'Localizar e Substituir (CTRL+R)'
      Caption = 'Localizar e Substituir'
      ParentShowHint = False
      ShowHint = True
      OnClick = lblocalizaresubstituirClick
    end
    object Label2: TLabel
      Left = 716
      Top = 17
      Width = 51
      Height = 13
      Caption = 'Anota'#231#245'es'
    end
    object BtExecutaScript: TBitBtn
      Left = 2
      Top = 4
      Width = 106
      Height = 25
      Caption = '&Executa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtExecutaScriptClick
    end
    object FileListBox: TFileListBox
      Left = 272
      Top = 31
      Width = 181
      Height = 163
      ItemHeight = 13
      Mask = '*.sql'
      TabOrder = 1
      OnDblClick = FileListBoxDblClick
    end
    object DirectoryListBox: TDirectoryListBox
      Left = 8
      Top = 55
      Width = 262
      Height = 139
      ItemHeight = 16
      TabOrder = 2
      OnChange = DirectoryListBoxChange
    end
    object DriveComboBox: TDriveComboBox
      Left = 8
      Top = 31
      Width = 265
      Height = 19
      DirList = DirectoryListBox
      TabOrder = 3
    end
    object btexecutasqllote: TBitBtn
      Left = 110
      Top = 4
      Width = 136
      Height = 25
      Caption = 'Executa Todos Sqls'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexecutasqlloteClick
    end
    object Memoerro: TMemo
      Left = 456
      Top = 31
      Width = 257
      Height = 162
      Lines.Strings = (
        'Memoerro')
      TabOrder = 5
      WordWrap = False
      OnKeyDown = MemoerroKeyDown
    end
    object btexecutasqlpasta: TBitBtn
      Left = 248
      Top = 4
      Width = 136
      Height = 25
      Caption = 'Executa Sqls Pasta'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexecutasqlpastaClick
    end
    object Button1: TButton
      Left = 460
      Top = 196
      Width = 75
      Height = 20
      Caption = 'Commit'
      Enabled = False
      TabOrder = 7
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 540
      Top = 196
      Width = 75
      Height = 20
      Caption = 'Rollback'
      Enabled = False
      TabOrder = 8
      OnClick = Button2Click
    end
    object memoanotacoes: TMemo
      Left = 716
      Top = 31
      Width = 257
      Height = 162
      Lines.Strings = (
        'Memoerro')
      TabOrder = 9
      WordWrap = False
      OnKeyDown = MemoerroKeyDown
    end
  end
  object LbScript: TListBox
    Left = 0
    Top = 219
    Width = 1270
    Height = 41
    Align = alTop
    ItemHeight = 13
    TabOrder = 1
    OnDblClick = LbScriptDblClick
  end
  object memo_script: TSynEdit
    Left = 0
    Top = 263
    Width = 1270
    Height = 301
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    TabOrder = 3
    OnKeyDown = MemoScriptKeyDown
    Gutter.Font.Charset = DEFAULT_CHARSET
    Gutter.Font.Color = clWindowText
    Gutter.Font.Height = -11
    Gutter.Font.Name = 'Courier New'
    Gutter.Font.Style = []
    Highlighter = SynSQLSyn1
    Lines.Strings = (
      'memo_script')
    WantReturns = False
    WantTabs = True
    OnReplaceText = memo_scriptReplaceText
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 722
    Width = 1270
    Height = 19
    Panels = <
      item
        Text = 
          'Ctrl+S = Salvar    |   Ctrl+F = Procurar  |  F3 = Localiza Pr'#243'xi' +
          'ma | Ctrl+R = Substituir | Ctrl + Enter = Executa SQL'
        Width = 50
      end>
  end
  object DsScript: TDataSource
    DataSet = ZQueryScript
    Left = 578
    Top = 3
  end
  object SaveDialogScript: TSaveDialog
    Left = 547
    Top = 1
  end
  object SynSQLSyn1: TSynSQLSyn
    Left = 360
    Top = 328
  end
  object SynEditSearch: TSynEditSearch
    Left = 472
    Top = 320
  end
  object SynEditRegexSearch: TSynEditRegexSearch
    Left = 444
    Top = 352
  end
  object ZConnectionScript: TZConnection
    Protocol = 'mysql-4.1'
    HostName = 'localhost'
    Left = 608
    Top = 2
  end
  object ZQueryScript: TZQuery
    Params = <>
    Left = 639
    Top = 3
  end
end
