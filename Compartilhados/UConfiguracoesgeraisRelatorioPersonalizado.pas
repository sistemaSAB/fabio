unit UConfiguracoesgeraisRelatorioPersonalizado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TFConfiguracoesgeraisRelatorioPersonalizado = class(TForm)
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    CbMostraPreview: TCheckBox;
    edtcaptionformulario: TEdit;
    ComboFontetamanhopadrao: TComboBox;
    edtquantidadecolunas: TEdit;
    edtquantidadelinhas: TEdit;
    ComboEntrelinhas: TComboBox;
    Label1: TLabel;
    cbduplicaitensrepetica: TCheckBox;
    Label10: TLabel;
    edtquantidadeitemsrepeticao: TEdit;
    Label4: TLabel;
    edtqtdelinhasabaixoduplica: TEdit;
    Label9: TLabel;
    comboimprimecabecalhoemtodasasfolhas: TComboBox;
    Label11: TLabel;
    comboimprimecabecalhopadrao: TComboBox;
    Label12: TLabel;
    Label13: TLabel;
    comboorientacao: TComboBox;
  private
    { Private declarations }
  public
    { Public declarations }
    Procedure ConfiguraPadrao;
  end;

var
  FConfiguracoesgeraisRelatorioPersonalizado: TFConfiguracoesgeraisRelatorioPersonalizado;

implementation

{$R *.dfm}

{ TFConfiguracoesgeraisRelatorioPersonalizado }

procedure TFConfiguracoesgeraisRelatorioPersonalizado.ConfiguraPadrao;
begin
     CbMostraPreview.Checked:=true;
     edtcaptionformulario.text:='Relatório Personalizado';
     ComboFontetamanhopadrao.ItemIndex:=1;
     edtquantidadecolunas.Text:='96';
     edtquantidadelinhas.Text:='33';
     ComboEntrelinhas.ItemIndex:=0;
     edtquantidadeitemsrepeticao.text:='10';
     cbduplicaitensrepetica.Checked:=False;
     edtqtdelinhasabaixoduplica.text:='0';
     comboimprimecabecalhoemtodasasfolhas.ItemIndex:=1;
     comboorientacao.itemindex:=0;

end;

end.
