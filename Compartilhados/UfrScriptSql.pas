unit UfrScriptSql;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, 
  Dialogs, IBScript, DB, IBCustomDataSet, StdCtrls, Grids, DBGrids, Buttons,
  Menus, ExtCtrls, ComCtrls,IBQuery;

type
  TFrScriptSql = class(TFrame)
    DBGridScript: TDBGrid;
    IBDataSetScript: TIBDataSet;
    OpenDialogScript: TOpenDialog;
    DsScript: TDataSource;
    IBScript: TIBScript;
    SaveDialogScript: TSaveDialog;
    PanelBotoes: TPanel;
    BtExecutaScript: TBitBtn;
    BitBtn1: TBitBtn;
    BtAbrirScript: TBitBtn;
    LbScript: TListBox;
    MemoScript: TRichEdit;
    procedure BtExecutaScriptClick(Sender: TObject);
    procedure BtAbrirScriptClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure MemoScriptKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RichEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure LbScriptDblClick(Sender: TObject);
  private
    { Private declarations }


  public
    { Public declarations }
    Procedure InicializaFrame;
  end;

implementation

{$R *.dfm}

uses UessencialGlobal, UDataModulo;


procedure TFrScriptSql.BtAbrirScriptClick(Sender: TObject);
begin
     if (OpenDialogScript.execute=false)
     then exit;

     MemoScript.Lines.clear;
     MemoScript.Lines.LoadFromFile(OpenDialogScript.filename);

end;

procedure TFrscriptSQL.BtExecutaScriptClick(Sender: TObject);
var
Pultimocomando:string;
begin
     IBScript.Script:=MemoScript.Lines;

     try
        IBScript.ExecuteScript;

        PultimoComando:='';
        if (LbScript.Items.count>0)
        then PultimoComando:=LbScript.Items[LbScript.items.count-1];

        if (uppercase(Pultimocomando)<>uppercase(MemoScript.Lines.text))
        Then Begin
                  LbScript.Items.add(MemoScript.Lines.text);
                  LbScript.Itemindex:=-1;
        End;
        MemoScript.Lines.clear;
     Except
           on e:exception do
           begin
                mensagemerro(E.message);
           End;
     End;
end;



procedure TFrScriptSql.InicializaFrame;
begin
     ibDATASETScript.database:=FdataModulo.Ibdatabase;
     IBScript.Dataset:=IBDataSetScript;
     IBScript.Database:=FdataModulo.Ibdatabase;
     IBScript.Transaction:=FdataModulo.IBTransaction;
     MemoScript.lines.clear;
     LbScript.Items.clear;
     PegaCorForm(Self);

end;

procedure TFrScriptSql.BitBtn1Click(Sender: TObject);
begin
      if (SaveDialogScript.Execute=false)
      then exit;

      MemoScript.Lines.SaveToFile(SaveDialogScript.FileName+'.sql');
end;

procedure TFrScriptSql.MemoScriptKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (ssctrl in shift) and (key=ord('P'))//Ctrl P(prior=anterior)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex>0)
                       Then LbScript.ItemIndex:=LbScript.ItemIndex-1
                       Else Begin
                                 if (LbScript.ItemIndex=-1)
                                 Then LbScript.ItemIndex:=LbScript.items.count-1;
                       End;

                       MemoScript.Lines.Clear;
                       MemoScript.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=ord('N'))//Ctrl N  (next=proximo)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex<(LbScript.Items.count-1))
                       Then LbScript.ItemIndex:=LbScript.ItemIndex+1;

                       MemoScript.Lines.Clear;
                       MemoScript.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=13)//Ctrl ENTER(executa)
     Then Begin
               BtExecutaScriptClick(sender);
     end;
end;


procedure TFrScriptSql.RichEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if (ssctrl in shift) and (key=ord('P'))//Ctrl P(prior=anterior)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex>0)
                       Then LbScript.ItemIndex:=LbScript.ItemIndex-1
                       Else Begin
                                 if (LbScript.ItemIndex=-1)
                                 Then LbScript.ItemIndex:=LbScript.items.count-1;
                       End;

                       MemoScript.Lines.Clear;
                       MemoScript.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=ord('N'))//Ctrl N  (next=proximo)
     Then Begin
               if (LbScript.Items.Count>0)
               Then Begin
                       if (LbScript.ItemIndex<(LbScript.Items.count-1))
                       Then LbScript.ItemIndex:=LbScript.ItemIndex+1;

                       MemoScript.Lines.Clear;
                       MemoScript.Lines.Text:=LbScript.Items[LbScript.ItemIndex];
               End;
     end;

     if (ssctrl in shift) and (key=13)//Ctrl ENTER(executa)
     Then Begin
               BtExecutaScriptClick(sender);
     end;
end;

procedure TFrScriptSql.LbScriptDblClick(Sender: TObject);
begin
     if (LbScript.Items.Count>0)
     Then MemoScript.Lines.text:=LbScript.Items[LbScript.Itemindex];
end;

end.
