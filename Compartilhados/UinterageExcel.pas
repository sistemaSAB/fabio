unit UinterageExcel;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, OleServer, ExcelXP, Excel2000;

type
  TFInterageExcel = class(TForm)
    ExcelApplication1: TExcelApplication;
    ExcelWorksheet1: TExcelWorksheet;
  private
     { Private declarations }
  public
    { Public declarations }
    lcid : Integer;
    wkbk : _Workbook;
    Function AbreArquivoExcel(Caminho: String):boolean;overload;
    Function AbreArquivoExcel(Caminho, Ordem:String):boolean;overload;
    Function Escreve(coluna_e_linha:String;Texto:String):boolean;
    Function Imprime:boolean;
    Function  Salva(nome:String):boolean;
    procedure Fecha;
    Procedure TestaHyperlink;
    Function RetornaColuna(indice:integer):string;

  end;

var
     FInterageExcel: TFInterageExcel;
implementation

{$R *.dfm}

Function TFInterageExcel.AbreArquivoExcel(Caminho, Ordem:String):boolean;
begin
     If Trim(Ordem)=''
     Then Ordem:='1';
     Try
         lcid := GetUserDefaultLCID;
         ExcelApplication1.Visible[lcid] := true;
         ExcelApplication1.DisplayAlerts[lcid] := false;
         WkBk := ExcelApplication1.Workbooks.Open(caminho, EmptyParam,EmptyParam, EmptyParam, EmptyParam,EmptyParam, EmptyParam, EmptyParam,EmptyParam, EmptyParam, EmptyParam,EmptyParam, EmptyParam, LCID);
         ExcelWorksheet1.ConnectTo(WkBk.Worksheets[strtoint(Ordem)]as _Worksheet);
         ExcelWorksheet1.Activate(LCID);
         ExcelApplication1.ScreenUpdating[lcid] := true;

         //ExcelWorksheet1.Name := 'Relatórios via Excel';  Nome da Planilha
         result:=True;

     Except
           result:=False;
           exit;
     End;
end;
Function TFInterageExcel.AbreArquivoExcel(Caminho:String):boolean;
begin
     Try
         lcid := GetUserDefaultLCID;
         ExcelApplication1.Visible[lcid] := true;
         ExcelApplication1.DisplayAlerts[lcid] := false;
         WkBk := ExcelApplication1.Workbooks.Open(caminho, EmptyParam,EmptyParam, EmptyParam, EmptyParam,EmptyParam, EmptyParam, EmptyParam,EmptyParam, EmptyParam, EmptyParam,EmptyParam, EmptyParam, LCID);
         ExcelWorksheet1.ConnectTo(WkBk.Worksheets[1]as _Worksheet);
         ExcelWorksheet1.Activate(LCID);
         ExcelApplication1.ScreenUpdating[lcid] := true;
         result:=True;
     Except
           result:=False;
           exit;
     End;
end;





procedure TFInterageExcel.Fecha;
begin
     ExcelApplication1.disconnect;
end;


function TFInterageExcel.Escreve(coluna_e_linha:string;Texto: String): boolean;
begin
     Try
       ExcelApplication1.Range[coluna_e_linha,coluna_e_linha].Value2:=Texto;
       result:=True;
     Except
           result:=False;
     End;
end;

function TFInterageExcel.Imprime: boolean;
begin
     Try
       ExcelWorksheet1.PrintOut;
       result:=True
     Except
           result:=False;
     End;

end;

function TFInterageExcel.Salva(nome: String): boolean;
begin
    Try
       ExcelWorksheet1. SaveAs(nome);
       result:=True
     Except
           result:=False;
     End;

end;

procedure TFInterageExcel.TestaHyperlink;
begin
    ExcelWorksheet1.Hyperlinks.add(ExcelApplication1.Range['R7','R7'],'Quadro.xls%20-%20''18''!B101', EmptyParam,EmptyParam,'teste');
end;

function TFInterageExcel.RetornaColuna(indice: integer): string;
var
colunas:string;
cont,quant:integer;
begin
    COLUNAS:='ABCDEFGHIJKLMNOPQRSTUVWXYZ';

    if (indice<=26)
    Then Begin
              result:=colunas[indice];
              exit;
    End;

    case indice of
    27: result:='AA';
    28: result:='AB';
    29: result:='AC';
    30: result:='AD';
    31: result:='AE';
    32: result:='AF';
    33: result:='AG';
    34: result:='AH';
    35: result:='AI';
    36: result:='AJ';
    37: result:='AK';
    38: result:='AL';
    39: result:='AM';
    40: result:='AN';
    end;
    
    if (indice>40)
    Then Exception.Create('Valor Inválido para retornar a coluna '+inttostr(indice));

end;

end.
