unit UobjIntegracao;

(*Objeto Vazio usado apenas para interface das fun��es que ser�o usadas em outros aplicativos,
Exemplo

no Amanda usaremos este que n�o tem nada e n�o instaciaremos a variavel global,
por�m no safira instanciaremos e a unit n�o ser� essa ser� uma unit local ao
fonte do Safira por�m com o mesmo nome, usarei em casos onde preciso
integrar o financeiro a m�dulos especificos de cada softwares
*)

interface

type

   TObjIntegracao=class

          Public

                constructor create;
                Function ExcluiporChequeDevolvido(Pcodigo:String):boolean;
                function LancaComissaoNegativa(Pcodigo: string): boolean;

          Private


   End;


implementation

{ TObjIntegracao }

constructor TObjIntegracao.create;
begin

end;

function TObjIntegracao.ExcluiporChequeDevolvido(Pcodigo: String): boolean;
begin
     //fun��o vazia
     Result:=True;
end;

function TObjIntegracao.LancaComissaoNegativa(Pcodigo: string): boolean;
begin
     result:=False;

     result:=True;
end;


end.
