unit UobjSpedPisCofins_Novo;

interface

uses Classes,StdCtrls,ComCtrls,IBQuery, db, Dialogs, ibdatabase,
     ACBrSpedPisCofins,Forms,Useg,SysUtils,UessencialGlobal,ACBrEPCBlocos,
     ACBrEPCBloco_0,ACBrEPCBloco_0_Class,ACBrEPCBloco_A_Class,
     ACBrEPCBloco_C_Class,ACBrEPCBloco_D_Class,ACBrEPCBloco_F_Class,
     ACBrEPCBloco_M_Class,ACBrEPCBloco_1_Class;

type
  TFonteDados = Class(TComponent)
  private
    FQuery :TIBQuery;
    FSQL: String;
    FRegistro: String;
    FDatabase: TIBDatabase;
    FDataFIN: TDateTime;
    FDataINI: TDateTime;
    Fstatus: TStatusBar;
    FgeraRegistroH: Boolean;
    FDataFinEstoque: TDateTime;
    FDataIniEstoque: TDateTime;
    FindPerfil: string;
    FcodSitEspecial: string;
    procedure SetSQL(const Value: String);
    function AbreQuery:Boolean;Virtual;
    procedure SetRegistro(const Value: String);
    procedure SetDatabase(const Value: TIBDatabase);
    procedure SetDataFIN(const Value: TDateTime);
    procedure SetDataINI(const Value: TDateTime);
    procedure GetFieldSoNumeros(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldDesencripta(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldDesencriptaSoNumeros(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldTrim(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure Setstatus(const Value: TStatusBar);
    procedure SetgeraRegistroH(const Value: Boolean);
    procedure SetDataFinEstoque(const Value: TDateTime);
    procedure SetDataIniEstoque(const Value: TDateTime);
    function teveErros: Boolean;
    procedure SetindPerfil(const Value: string);
    procedure SetcodSitEspecial(const Value: string);
  protected
    function ValidaBrancosNulos:Boolean;Virtual;
    function ValidaRegistros:Boolean; Virtual;Abstract;
    procedure SetParametros;Virtual;Abstract;
    procedure SetCampos;Virtual;
  public
    strErros: TStringList;
    strAdvertencia: TStringList;
    strItem: TStringList;
    strServico: TStringList;
    strUnidade: TStringList;
    strNatOp: TStringList;
    strCodPart: TStringList;
    valor_total_debitos:Currency;
    VL_TOT_CREDITOS_E110:Currency;
    software:string;
    property SQL :String read FSQL write SetSQL;
    property Registro :String read FRegistro write SetRegistro;
    property Database :TIBDatabase read FDatabase write SetDatabase;
    property DataINI :TDateTime read FDataINI write SetDataINI;
    property DataFIN :TDateTime read FDataFIN write SetDataFIN;
    property codSitEspecial:string read FcodSitEspecial write SetcodSitEspecial;
    property indPerfil:string read FindPerfil write SetindPerfil;
    property DataIniEstoque :TDateTime read FDataIniEstoque write SetDataIniEstoque;
    property DataFinEstoque :TDateTime read FDataFinEstoque write SetDataFinEstoque;
    property status :TStatusBar read Fstatus write Setstatus;
    property geraRegistroH: Boolean read FgeraRegistroH write SetgeraRegistroH;
    procedure preenche;virtual;abstract;
    procedure addStr(str: TStringList; valor: string);
    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
  end;


type
  TFonteDadosParamData = Class(TFonteDados)
  protected
    procedure SetParametros;override;
  public
  end;

type
  TFonteDadosBloco0 = Class(TFonteDados)
  protected
      procedure SetParametros;override;
  public
    FAcbrEPBloco0: TBloco_0;
end;

type
  TFonteDados0000 = Class(TFonteDadosBloco0)
  private
    FIndAtiv: string;
    FCodFin: string;
    FcodTipoEscrituracao: string;

    numeroReciboAnterior: string;{em caso de escritura��o retificada}
    indicadorNaturezaPJ : string;{Indicador da natureza da pessoa jur�dica}
    FindApropCredito: string;

    function getCodTipoEscrituracao :Tacbrtipoescrituracao;
    function getCodVer              :TACBrVersaoLeiaute;
    function getIndAtiv             :TACBrIndicadorAtividade;
    //function getIndPerfil           :TACBrPerfil;
    function getIndicadorNaturezaPJ :TACBrIndicadorNaturezaPJ;
    //function getCodFin              :TACBrCodFinalidade;
    function getCodSitEsp(cod_esp : string):TACBrIndicadorSituacaoEspecial;

    procedure SetCodFin(const Value: string);
    procedure SetIndAtiv(const Value: string);
    procedure SetcodTipoEscrituracao(const Value: string);
    procedure SetindApropCredito(const Value: string);

  protected

    property codTipoEscrituracao: string read FcodTipoEscrituracao write SetcodTipoEscrituracao;
    property IndAtiv :string read FIndAtiv write SetIndAtiv;
    property indApropCredito :string read FindApropCredito write SetindApropCredito;
    function ValidaRegistros:Boolean; override;

    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;

  public
end;

type
  TFonteDados0100 = class(TFonteDadosBloco0)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0110 = class(TFonteDadosBloco0)
    private

      indIncidTrib: string; {C�digo indicador da incid�ncia tribut�ria no per�odo: }
      indApropCred: string; {C�digo indicador de m�todo de apropria��o de cr�ditos comuns, no caso de incid�ncia no regime n�o-cumulativo (COD_INC_TRIB = 1 ou 3)}
      indContrib: string;   {C�digo indicador do Tipo de Contribui��o Apurada no Per�odo}
      indRegCum: string;    {C�digo indicador do crit�rio de escritura��o e apura��o adotado}

      function getIndIncidTrib():TACBrCodIndIncTributaria;
      function getIndApropCred():TACBrIndAproCred;
      function getIndContrib  ():TACBrCodIndTipoCon;
      function getIndRegCum   ():TACBrCodIndCritEscrit;

  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0140 = class(TFonteDadosBloco0)
    private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0150 = class(TFonteDadosBloco0)
    private
      codigoParticipante: Integer;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0150Cliente = class(TFonteDados0150)
    private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0150Fornecedor = class(TFonteDados0150)
    private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0190 = class(TFonteDadosBloco0)
    private
      pSigla:string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0200 = Class(TFonteDadosBloco0)
  private
    pCodigoItem: string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
public
end;

type
  TFonteDados0200Produto = class(TFonteDados0200)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0200Servico = class(TFonteDados0200)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDados0400= Class(TFonteDadosBloco0)
  private
    pCodigoNatOp: string;
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
    procedure inicializa;
public
end;

type
  TFonteDadosBlocoA = Class(TFonteDados)
  private
  protected
    procedure SetParametros;override;
  public
    FAcbrEPBlocoA: TBloco_A;
end;

type
  TFonteDadosA001 = class(TFonteDadosBlocoA)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBlocoC = Class(TFonteDadosParamData)
  private

    FpCOFINS: Currency;
    FpPIS: Currency;
    FindIncidTributaria: string;
    procedure SetpCOFINS(const Value: Currency);
    procedure SetpPIS(const Value: Currency);
    procedure SetindIncidTributaria(const Value: string);

  protected

      procedure SetParametros;override;

      function getValorPis(BCPIS:Currency):Currency;
      function getValorCofins(BCCOFINS:Currency):Currency;
      function getSituacaoNF(pSituacao:string):TACBrCodSit;
      function getTipoFrete(pTipo: string): TACBrTipoFrete;
      function get_CSTICMS   (cst_icms: string) :TACBrSituacaoTribICMS;
      function get_CSTIPI    (cst_ipi: string) :TACBrSituacaoTribIPI;
      function get_CSTPIS    (cst_pis: string) :TACBrSituacaoTribPIS;
      function get_CSTCOFINS (cst_cofins: string) :TACBrSituacaoTribCOFINS;
  public

    FAcbrEPBlocoC: TBloco_C;
    property pPIS:Currency read FpPIS write SetpPIS;
    property pCOFINS:Currency read FpCOFINS write SetpCOFINS;
    property indIncidTributaria :string read FindIncidTributaria write SetindIncidTributaria;

end;

type
  TFonteDadosC001 = class(TFonteDadosBlocoC)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC010 = class(TFonteDadosBlocoC)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC100 = class(TFonteDadosBlocoC)
  private
  protected
    function ValidaRegistros:Boolean; override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC170 = class(TFonteDadosBlocoC)
  private
    codigoNF:Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    function getVL_BC_ICMS(pReducao,baseCalculo: Currency): Currency;
    function getVL_ICMS(bc_icms, aliquota:currency): Currency;
end;

type
  TFonteDadosC170Saida = class(TFonteDadosC170)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;


type
  TFonteDadosC100Saida = class(TFonteDadosC100)
  private
    FRegC170 :TFonteDadosC170Saida;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;


type
  TFonteDadosC181 = class(TFonteDadosBlocoC)
  private
    codigoProduto: Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC185 = class(TFonteDadosBlocoC)
  private
    codigoProduto: integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC180 = class(TFonteDadosBlocoC)
  private
    FRegC181 :TFonteDadosC181;
    FRegC185 :TFonteDadosC185;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosC191 = class(TFonteDadosBlocoC)
  private
    codigoProduto:integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC195 = class(TFonteDadosBlocoC)
  private
    codigoProduto: Integer;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosC190 = class(TFonteDadosBlocoC)
  private
    FRegC191 :TFonteDadosC191;
    FRegC195 :TFonteDadosC195;
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TFonteDadosBlocoD = Class(TFonteDados)
  private
  protected
    procedure SetParametros;override;
  public
    FAcbrEPBlocoD: TBloco_D;
end;

type
  TFonteDadosD001 = class(TFonteDadosBlocoD)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBlocoF = Class(TFonteDados)
  private
  protected
    procedure SetParametros;override;
  public
    FAcbrEPBlocoF: TBloco_F;
end;

type
  TFonteDadosF001 = class(TFonteDadosBlocoF)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosF010 = class(TFonteDadosBlocoF)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBlocoM = Class(TFonteDados)
  private
  protected
    procedure SetParametros;override;
  public
    indIncidTrib: string;
    FAcbrEPBlocoM: TBloco_M;
end;

type
  TFonteDadosM001 = class(TFonteDadosBlocoM)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosM200 = class(TFonteDadosBlocoM)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosM210 = class(TFonteDadosBlocoM)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosM600 = class(TFonteDadosBlocoM)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TFonteDadosBloco1 = Class(TFonteDados)
  private
  protected
    procedure SetParametros;override;
  public
    FAcbrEPBloco1: TBloco_1;
end;

type
  TFonteDados1001 = class(TFonteDadosBloco1)
  private
  protected
    function ValidaRegistros:Boolean;override;
    procedure SetParametros;override;
    procedure SetCampos;override;
    procedure preenche;override;
  public
end;

type
  TObjSpedPisCofins_Novo = class(TFonteDados)
  private

    ACBrSpedPISCOFINS1: TACBrSPEDPisCofins;
    FpPIS: Currency;
    queryTemp:TIBQuery;
    FpCOFINS: Currency;
    FnotasBuffer: Integer;
    FlinhasBuffer: Integer;
    FCOD_RECEITAESTADUAL: string;
    FcodTipoEscrituracao: string;
    FPathArquivo: string;
    Fsoftware: string;
    FIndAtiv: string;
    FmemoErros: TMemo;
    FnumeroReciboAnterior: string;
    FindicadorNaturezaPJ: string;
    FCodSitEspecial: string;
    FindContrib: string;
    FindIncidTrib: string;
    FindApropCred: string;
    FindRegCum: string;

    procedure LimpaRegistros;
    procedure inicializa;

    {setter's}
    procedure SetCOD_RECEITAESTADUAL(const Value: string);
    procedure SetcodTipoEscrituracao(const Value: string);
    procedure SetIndAtiv(const Value: string);
    procedure SetlinhasBuffer(const Value: Integer);
    procedure SetmemoErros(const Value: TMemo);
    procedure SetnotasBuffer(const Value: Integer);
    procedure SetPathArquivo(const Value: string);
    procedure SetpCOFINS(const Value: Currency);
    procedure SetpPIS(const Value: Currency);
    procedure Setsoftware(const Value: string);
    procedure SetnumeroReciboAnterior(const Value: string);
    procedure SetindicadorNaturezaPJ(const Value: string);
    procedure SetCodSitEspecial(const Value: string);
    procedure SetindApropCred(const Value: string);
    procedure SetindContrib(const Value: string);
    procedure SetindIncidTrib(const Value: string);
    procedure SetindRegCum(const Value: string);

  protected
  public

    FReg0000  :TFonteDados0000;
    FReg0100  :TFonteDados0100;
    FReg0110  :TFonteDados0110;
    FReg0140  :TFonteDados0140;
    FReg0150C :TFonteDados0150Cliente;
    FReg0150F :TFonteDados0150Fornecedor;
    FReg0190  :TFonteDados0190;
    FReg0200P :TFonteDados0200Produto;
    FReg0200S :TFonteDados0200Servico;
    FReg0400  :TFonteDados0400;
    FRegA001  :TFonteDadosA001;
    FRegC001  :TFonteDadosC001;
    FRegC010  :TFonteDadosC010;
    FRegC100S :TFonteDadosC100Saida;
    FRegC180  :TFonteDadosC180;
    FRegC190  :TFonteDadosC190;
    FRegD001  :TFonteDadosD001;
    FRegF001  :TFonteDadosF001;
    FRegF010  :TFonteDadosF010;
    FRegM001  :TFonteDadosM001;
    FRegM200  :TFonteDadosM200;
    FRegM210  :TFonteDadosM210;
    FRegM600  :TFonteDadosM600;
    FReg1001  :TFonteDados1001;

    strErros: TStringList;
    strAdvertencia: TStringList;
    strItem: TStringList;
    strServico :TStringList;
    strUnidade: TStringList;
    strNatOp: TStringList;
    strCodPart: TStringList;

    property DataIni: TDateTime read FDataIni write SetDataIni;
    property DataFin: TDateTime read FDataFin write SetDataFin;
    property DataIniEstoque: TDateTime read FDataIni write SetDataIni;
    property DataFinEstoque: TDateTime read FDataFinEstoque write SetDataFinEstoque;
    property IndAtiv: string read FIndAtiv write SetIndAtiv;
    property CodSitEspecial :string read FCodSitEspecial write SetCodSitEspecial;
    property codTipoEscrituracao: string read FcodTipoEscrituracao write SetcodTipoEscrituracao;
    property DataBase :TIBDatabase read FDataBase write SetDataBase;
    property PathArquivo: string read FPathArquivo write SetPathArquivo;
    property pPIS: Currency read FpPIS write SetpPIS;
    property pCOFINS: Currency read FpCOFINS write SetpCOFINS;
    property status:TStatusBar read Fstatus write Setstatus;
    property COD_RECEITAESTADUAL: string read FCOD_RECEITAESTADUAL write SetCOD_RECEITAESTADUAL;
    property geraRegistroH: Boolean read FgeraRegistroH write SetgeraRegistroH;
    property software: string read Fsoftware write Setsoftware;
    property linhasBuffer:Integer read FlinhasBuffer write SetlinhasBuffer;
    property notasBuffer:Integer read FnotasBuffer write SetnotasBuffer;
    property memoErros :TMemo read FmemoErros write SetmemoErros;

    property numeroReciboAnterior :string read FnumeroReciboAnterior write SetnumeroReciboAnterior;
    property indicadorNaturezaPJ : string read FindicadorNaturezaPJ write SetindicadorNaturezaPJ;
    property indIncidTrib : string read FindIncidTrib write SetindIncidTrib;
    property indApropCred : string read FindApropCred write SetindApropCred;
    property indContrib : string read FindContrib write SetindContrib;
    property indRegCum :string read FindRegCum write SetindRegCum;

    procedure gravaTXT;
    procedure preencheComponente;
    function  geraSped:Boolean;
    procedure preencheSql;
    function  teveErros:Boolean;
    procedure carregaErros(str: TStringList);
    procedure carregaErrosMemo();
    function pegaModeloTreeView(tv: TTreeView): string;
    function pegaTipoNFTreeView(tv: TTreeView): string;


    constructor Create(AOwner: TComponent;ibdatabase: TIBDatabase);overload;
    destructor destroy;override;
end;

implementation

uses ACBrEPCBloco_F, ACBrEPCBloco_M;


{ TFonteDados }

function TFonteDados.AbreQuery: Boolean;
begin
  result := False;
  if self.FSQL = 'vazio;' then
    Exit;

  Self.status.Panels[1].Text:='Gerando registro: '+Registro;
  Application.ProcessMessages;

  if FQuery = nil then
    FQuery := TIBQuery.Create(Self);

  FQuery.Close;

  FQuery.DisableControls;
  FQuery.Database := FDatabase;
  FQuery.SQL.Text := Self.FSQL;

  if FQuery.Params.Count > 0 then
    Self.SetParametros;

  FQuery.Open;
  if not (FQuery.IsEmpty) then
  begin
    Self.SetCampos;
    FQuery.First;
    while not FQuery.Eof do
    begin
      self.ValidaRegistros;
      self.ValidaBrancosNulos;
      FQuery.Next;
    end;
    FQuery.First;
  end
  else
    Result := False;
    
  result := True;
end;

procedure TFonteDados.addStr(str: TStringList; valor: string);
var
  _i:integer;
begin
  for _i := 0  to str.Count-1  do
    if (str[_i] = valor) then Exit;
  str.Add (valor);
end;

constructor TFonteDados.Create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDados.Destroy;
begin
  FreeAndNil(fquery);
  inherited;
end;

procedure TFonteDados.GetFieldDesencripta(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
  begin
    Text := DesincriptaSenha(Sender.AsString);
    text := Trim(text);
  end;
end;

procedure TFonteDados.GetFieldDesencriptaSoNumeros(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
  begin
    text := DesincriptaSenha(Sender.AsString);
    Text := RetornaSoNumeros(text);
  end
end;

procedure TFonteDados.GetFieldSoNumeros(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := RetornaSoNumeros(Sender.AsString);
end;

procedure TFonteDados.GetFieldTrim(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if sender.AsString <> '' then
    Text := Trim(sender.AsString);
end;

procedure TFonteDados.SetCampos;
var
  i: integer;
begin
  for i := 0  to FQuery.Fields.Count-1  do
    FQuery.Fields[i].Required := True;
end;

procedure TFonteDados.SetcodSitEspecial(const Value: string);
begin
  FcodSitEspecial := Value;
end;

procedure TFonteDados.SetDatabase(const Value: TIBDatabase);
begin
  FDatabase := Value;
end;

procedure TFonteDados.SetDataFIN(const Value: TDateTime);
begin
  FDataFIN := Value;
end;

procedure TFonteDados.SetDataFinEstoque(const Value: TDateTime);
begin
  FDataFinEstoque := Value;
end;

procedure TFonteDados.SetDataINI(const Value: TDateTime);
begin
  FDataINI := Value;
end;

procedure TFonteDados.SetDataIniEstoque(const Value: TDateTime);
begin
  FDataIniEstoque := Value;
end;

procedure TFonteDados.SetgeraRegistroH(const Value: Boolean);
begin
  FgeraRegistroH := Value;
end;


procedure TFonteDados.SetindPerfil(const Value: string);
begin
  FindPerfil := Value;
end;

procedure TFonteDados.SetRegistro(const Value: String);
begin
  FRegistro := Value;
end;

procedure TFonteDados.SetSQL(const Value: String);
begin
  FSQL := Value;
end;

procedure TFonteDados.Setstatus(const Value: TStatusBar);
begin
  Fstatus := Value;
end;

function TFonteDados.teveErros: Boolean;
begin
  result := (strErros.Count > 0);
end;

function TFonteDados.ValidaBrancosNulos: Boolean;
var
  i :Integer;
begin
  Self.status.Panels[1].Text:='Validando registro: '+Registro+'...';
  for i := 0  to FQuery.FieldCount - 1 do
  begin
    Application.ProcessMessages;
    if FQuery.Fields[i].Required then
    begin
      if FQuery.Fields[i].AsString = ''  then
        strErros.Add('Erro no registro' + FRegistro + ' campo: "'+FQuery.Fields[i].DisplayLabel+'" com valor em branco ou nulo');
    end;
  end;
  Result := (strErros.Count <= 0);
end;

{ TFonteDadosParamData }

procedure TFonteDadosParamData.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsDateTime := FDataINI;
  FQuery.Params[1].AsDateTime := FDataFIN;
end;

{ TFonteDadosBloco0 }

procedure TFonteDadosBloco0.SetParametros;
begin
  inherited;
end;

{ TFonteDados0000 }

{function TFonteDados0000.getCodFin: TACBrCodFinalidade;
begin
  if (FCodFin = '0') then
    result := raOriginal
  else
    result := raSubstituto;
end;}

function TFonteDados0000.getCodVer: TACBrVersaoLeiaute;
begin
  if (FDataIni >= StrToDate ('14/03/2012')) then
    result := vlVersao201
  else
    result := vlVersao200;
end;

function TFonteDados0000.getIndAtiv: TACBrIndicadorAtividade;
begin

  case StrToIntDef (IndAtiv,-1) of

    0: result := indAtivIndustrial;
    1: result := indAtivPrestadorServico;
    2: result := indAtivComercio;
    3: result := indAtivoFincanceira;
    4: result := indAtivoImobiliaria;
    9: Result := indAtivoOutros;

    -1:
    begin
      Result := indAtivoOutros;
      strErros.Add ('Indicador de atividade inv�lido, indicador: '+IndAtiv);
    end;

  end;
  
end;

{function TFonteDados0000.getIndPerfil: TACBrPerfil;
begin
  if (FIndPerfil = 'A') then
    result := pfPerfilA
  else
  if (FIndPerfil = 'B') then
    result := pfPerfilB
  else
    result := pfPerfilC;
end;}

function TFonteDados0000.getCodTipoEscrituracao: Tacbrtipoescrituracao;
begin

  if (self.codTipoEscrituracao = '0') then
    Result := tpEscrOriginal
  else
  if (self.codTipoEscrituracao = '1') then
    result := tpEscrRetificadora
  else
  begin
    strErros.Add('Tipo da escritura��o inv�lido. Tipo: '+self.codTipoEscrituracao);
    result:=tpEscrOriginal;
  end;

end;

procedure TFonteDados0000.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  with (FAcbrEPBloco0.Registro0000New) do
  begin
   COD_VER          := getCodVer;
   TIPO_ESCRIT      := getCodTipoEscrituracao;

   if TIPO_ESCRIT = tpEscrRetificadora then
    NUM_REC_ANTERIOR := numeroReciboAnterior;

   IND_SIT_ESP      := getCodSitEsp(self.FcodSitEspecial);
   DT_INI           := DataINI;
   DT_FIN           := DataFIN;
   NOME             := FQuery.Fields[0].Text;
   CNPJ             := FQuery.Fields[1].Text;
   UF               := FQuery.Fields[2].Text;
   COD_MUN          := FQuery.Fields[3].AsInteger;
   SUFRAMA          := '';
   IND_NAT_PJ       := getIndicadorNaturezaPJ;
   IND_ATIV         := getIndAtiv;
  end;
  FAcbrEPBloco0.Registro0001New.IND_MOV := imComDados;
  
end;

procedure TFonteDados0000.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Raz�o Social';
  FQuery.Fields[1].DisplayLabel := 'CNPJ';
  FQuery.Fields[2].DisplayLabel := 'UF';
  FQuery.Fields[3].DisplayLabel := 'C�digo cidade';

  if software = 'PAF' then
  begin
    FQuery.Fields[0].OnGetText := GetFieldDesencripta;
    FQuery.Fields[1].OnGetText := GetFieldDesencriptaSoNumeros;
    FQuery.Fields[2].OnGetText := GetFieldDesencripta;
    FQuery.Fields[3].OnGetText := GetFieldDesencriptaSoNumeros;
  end
  else
  begin
    FQuery.Fields[0].OnGetText := GetFieldTrim;
    FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
    FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  end;
end;

procedure TFonteDados0000.SetCodFin(const Value: string);
begin
  FCodFin := Value;
end;

procedure TFonteDados0000.SetcodTipoEscrituracao(const Value: string);
begin
  FcodTipoEscrituracao := Value;
end;

procedure TFonteDados0000.SetIndAtiv(const Value: string);
begin
  FIndAtiv := Value;
end;

procedure TFonteDados0000.SetParametros;
begin
  inherited;
end;

function TFonteDados0000.ValidaRegistros: Boolean;
var
  dtini,dtfim:string;
begin
  if self.IndPerfil = '' then
    strErros.Add('Indicador de perfil n�o informado');

  if self.codTipoEscrituracao = '' then
    strErros.Add('C�digo do tipo de escritura��o do arquivo n�o informado');

  if self.codTipoEscrituracao = '1' then
    if self.numeroReciboAnterior = '' then
      strErros.Add('Para escritura��o retificadora � necessario informar o numero anterior do recibo');

  if self.IndAtiv = '' then
    strErros.Add('Indicador de atividade n�o informado');

  if self.codSitEspecial = '0' then
  begin
    primeiroUltimoDia(FormatDateTime('mm',DataINI),FormatDateTime('yyyy',DataFIN),dtini,dtFim);
    if DataFIN <> StrToDate(dtfim) then
      strErros.Add('Se o indicador de situa��o especial = 0 a data final deve ser igual ao �ltimo dia do m�s')
  end;

end;

function TFonteDados0000.getIndicadorNaturezaPJ: TACBrIndicadorNaturezaPJ;
begin

  if (indicadorNaturezaPJ = '0') then
    result := indNatPJSocEmpresariaGeral
  else
  if (indicadorNaturezaPJ = '1') then
    result := indNatPJSocCooperativa
  else
  if (indicadorNaturezaPJ = '2') then
    result := indNatPJEntExclusivaFolhaSal
  else
  begin
    Result:=indNatPJSocEmpresariaGeral;
    MensagemErro ('Indicador de natureza inv�lido. Indicador: '+indicadorNaturezaPJ);
  end;
  {indNatPJSocEmpresariaGeral, // 0 - Sociedade empres�ria geral
  indNatPJSocCooperativa,     // 1 - Sociedade Cooperativa
  indNatPJEntExclusivaFolhaSal// 2 - Entidade sujeita ao PIS/Pasep exclusivamente com base  na folha de sal�rios}

end;

function TFonteDados0000.getCodSitEsp(cod_esp : string): TACBrIndicadorSituacaoEspecial;
begin

  case StrToIntDef(cod_esp,0) of
    0:result := indSitAbertura;
    1:result := indSitCisao;
    2:result := indSitFusao;
    3:Result := indSitIncorporacao;
    4:result := indSitEncerramento;
  end;
  
end;

procedure TFonteDados0000.SetindApropCredito(const Value: string);
begin
  FindApropCredito := Value;
end;

{ TObjSpedPisCofins_Novo }

procedure TObjSpedPisCofins_Novo.SetCOD_RECEITAESTADUAL(
  const Value: string);
begin
  FCOD_RECEITAESTADUAL := Value;
end;

procedure TObjSpedPisCofins_Novo.SetcodTipoEscrituracao(
  const Value: string);
begin
  FcodTipoEscrituracao := Value;
end;

procedure TObjSpedPisCofins_Novo.SetIndAtiv(const Value: string);
begin
  FIndAtiv := Value;
end;

procedure TObjSpedPisCofins_Novo.SetlinhasBuffer(const Value: Integer);
begin
  FlinhasBuffer := Value;
end;

procedure TObjSpedPisCofins_Novo.SetmemoErros(const Value: TMemo);
begin
  FmemoErros := Value;
end;

procedure TObjSpedPisCofins_Novo.SetnotasBuffer(const Value: Integer);
begin
  FnotasBuffer := Value;
end;

procedure TObjSpedPisCofins_Novo.SetPathArquivo(const Value: string);
begin
  FPathArquivo := Value;
end;

procedure TObjSpedPisCofins_Novo.SetpCOFINS(const Value: Currency);
begin
  FpCOFINS := Value;
end;

procedure TObjSpedPisCofins_Novo.SetpPIS(const Value: Currency);
begin
  FpPIS := Value;
end;

procedure TObjSpedPisCofins_Novo.Setsoftware(const Value: string);
begin
  Fsoftware := Value;
end;

procedure TObjSpedPisCofins_Novo.inicializa;
var
  i: integer;
begin

  ACBrSpedPISCOFINS1.LinhasBuffer := self.FlinhasBuffer;
  ACBrSpedPISCOFINS1.Arquivo := ExtractFileName(self.FPathArquivo);
  ACBrSpedPISCOFINS1.Path    := ExtractFileDir(self.FPathArquivo)+'\';
  self.limpaRegistros;
  ACBrSpedPISCOFINS1.DT_INI := DataIni;
  ACBrSpedPISCOFINS1.DT_FIN := DataFin;

  {zera campos}
  strErros.Clear;
  strAdvertencia.Clear;
  strItem.Clear;
  strServico.Clear;
  strUnidade.Clear;
  strNatOp.Clear;
  strCodPart.Clear;
  ACBrSpedPISCOFINS1.IniciaGeracao;

  FReg0000 := TFonteDados0000.Create(Self);
  FReg0000.Registro := '0000';

  FReg0100 := TFonteDados0100.Create(self);
  FReg0100.Registro := '0100';

  FReg0110 := TFonteDados0110.Create(self);
  FReg0110.Registro := '0110';

  FReg0140 := TFonteDados0140.Create(self);
  FReg0140.Registro := '0140';

  FReg0150C := TFonteDados0150Cliente.Create(self);
  FReg0150C.Registro := '0150 CLIENTE';

  FReg0150F := TFonteDados0150Fornecedor.Create(self);
  Freg0150F.Registro := '0150 FORNECEDOR';

  FReg0190 := TFonteDados0190.Create(self);
  FReg0190.Registro := '0190 UNIDADE DE MEDIDAS';

  FReg0200P := TFonteDados0200Produto.Create(self);
  FReg0200P.Registro := '0200P';

  FReg0200S := TFonteDados0200Servico.Create(self);
  FReg0200S.Registro := '0200S';

  FReg0400 := TFonteDados0400.Create(self);
  FReg0400.Registro := '0400';

  FRegA001 := TFonteDadosA001.Create(self);
  FRegA001.Registro := 'A001';

  FRegC001 := TFonteDadosC001.Create(self);
  FRegC001.Registro := 'C001';

  FRegC010 := TFonteDadosC010.Create(self);
  FRegC010.Registro := 'C010';

  FRegC100S := TFonteDadosC100Saida.Create(self);
  FRegC100S.Registro := 'C100S';

  FRegC180 := TFonteDadosC180.create(self);
  FRegC180.Registro := 'C180';

  FRegC190 := TFonteDadosC190.create(self);
  FRegC190.Registro := 'C190';

  FRegD001 := TFonteDadosD001.create(self);
  FRegD001.Registro := 'D001';

  FRegF001 := TFonteDadosF001.Create(self);
  FRegF001.Registro := 'F001';

  FRegF010 := TFonteDadosF010.Create(self);
  FRegF010.Registro := 'F010';

  FRegM001 := TFonteDadosM001.Create(self);
  FRegM001.Registro := 'M001';

  FReg1001 := TFonteDados1001.Create(self);
  FReg1001.Registro := '1001';

  FRegM200 := TFonteDadosM200.Create(self);
  FRegM200.Registro := 'M200';

  FRegM210 := TFonteDadosM210.Create(self);
  FRegM210.Registro := 'M210';

  FRegM600 := TFonteDadosM600.Create(self);
  FRegM600.Registro := 'M600';


  for i := 0  to self.ComponentCount-1  do
  begin

    if self.Components[i] is TFonteDados then
    begin
      TFonteDados(self.Components[i]).Database := self.DataBase;
      TFonteDados(self.Components[i]).DataINI := self.FDataIni;
      TFonteDados(self.Components[i]).DataFIN := self.FDataFin;
      TFonteDados(self.Components[i]).DataIniEstoque := self.FDataIniEstoque;
      TFonteDados(self.Components[i]).DataFinEstoque := self.FDataFinEstoque;
      TFonteDados(self.Components[i]).strUnidade := self.strUnidade;
      TFonteDados(self.Components[i]).strItem := self.strItem;
      TFonteDados(self.Components[i]).strServico := self.strServico;
      TFonteDados(self.Components[i]).strNatOp := self.strNatOp;
      TFonteDados(self.Components[i]).strCodPart := self.strCodPart;
      TFonteDados(self.Components[i]).strErros := self.strErros;
      TFonteDados(self.Components[i]).strAdvertencia := self.strAdvertencia;
      TFonteDados(self.Components[i]).valor_total_debitos  := 0;
      TFonteDados(self.Components[i]).VL_TOT_CREDITOS_E110 := 0;
      TFonteDados(self.Components[i]).status := self.Fstatus;
      TFonteDados(self.components[i]).geraRegistroH := self.geraRegistroH;
      TFonteDados(self.Components[i]).software := self.software;
      TFonteDados(self.Components[i]).indPerfil := self.IndPerfil;
      TFonteDados(self.Components[i]).codSitEspecial := self.FCodSitEspecial;
    end;

    if self.Components[i] is TFonteDadosBloco0 then
      TFonteDadosBloco0(self.Components[i]).FAcbrEPBloco0 := ACBrSpedPISCOFINS1.Bloco_0;

    if self.Components[i] is TFonteDadosBlocoA then
      TFonteDadosBlocoA(self.Components[i]).FAcbrEPBlocoA := ACBrSpedPISCOFINS1.Bloco_A;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrEPBlocoC := ACBrSpedPISCOFINS1.Bloco_C;
      TFonteDadosBlocoC(self.Components[i]).pPIS := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
      TFonteDadosBlocoC(self.Components[i]).indIncidTributaria := self.indIncidTrib;
    end;

    if Self.Components[i] is TFonteDadosC100Saida then
      TFonteDadosC100Saida(self.Components[i]).inicializa;

    if Self.Components[i] is TFonteDadosC180 then
      TFonteDadosC180(self.Components[i]).inicializa;

    if Self.Components[i] is TFonteDadosC190 then
      TFonteDadosC190(self.Components[i]).inicializa;

    if self.Components[i] is TFonteDadosBlocoD then
      TFonteDadosBlocoD(self.Components[i]).FAcbrEPBlocoD := ACBrSpedPISCOFINS1.Bloco_D;

    if self.Components[i] is TFonteDadosBlocoF then
      TFonteDadosBlocoF(self.Components[i]).FAcbrEPBlocoF := ACBrSpedPISCOFINS1.Bloco_F;

    if self.Components[i] is TFonteDadosBlocoM then
    begin
      TFonteDadosBlocoM(self.Components[i]).FAcbrEPBlocoM := ACBrSpedPISCOFINS1.Bloco_M;
      TFonteDadosBlocoM(self.Components[i]).indIncidTrib := self.FindIncidTrib;
    end;

    if self.Components[i] is TFonteDadosBloco1 then
      TFonteDadosBloco1(self.Components[i]).FAcbrEPBloco1 := ACBrSpedPISCOFINS1.Bloco_1;

    if self.Components[i] is TFonteDados0000 then
    begin
      TFonteDados0000(self.Components[i]).numeroReciboAnterior := self.numeroReciboAnterior;
      TFonteDados0000(self.Components[i]).indicadorNaturezaPJ  := self.indicadorNaturezaPJ;
      TFonteDados0000(self.Components[i]).IndAtiv := self.IndAtiv;
      TFonteDados0000(self.Components[i]).codTipoEscrituracao := self.codTipoEscrituracao;
      TFonteDados0000(self.Components[i]).codSitEspecial := self.FCodSitEspecial;
      TFonteDados0000(self.Components[i]).indApropCredito := self.indApropCred;
    end;

    if Self.Components[i] is TFonteDados0110 then
    begin
      TFonteDados0110(self.Components[i]).indIncidTrib := self.FindIncidTrib;
      TFonteDados0110(self.Components[i]).indApropCred := self.FindApropCred;
      TFonteDados0110(self.Components[i]).indContrib := self.FindContrib;
      TFonteDados0110(self.Components[i]).indRegCum := self.indRegCum;
    end;

   // if Self.Components[i] is TFonteDadosBlocoM then
     // TFonteDadosBlocoM(self.Components[i]).indIncidTrib := self.FindIncidTrib;

    //if self.Components[i] is TFonteDadosC180 then
      //TFonteDadosC180(Self.Components[i]).indIncidTributaria := self.indIncidTrib;

  end;

end;

procedure TObjSpedPisCofins_Novo.LimpaRegistros;
begin
  with (ACBrSpedPISCOFINS1) do
  begin
    Bloco_0.LimpaRegistros;
    Bloco_1.LimpaRegistros;
    Bloco_9.LimpaRegistros;
    Bloco_A.LimpaRegistros;
    Bloco_C.LimpaRegistros;
    Bloco_D.LimpaRegistros;
    Bloco_F.LimpaRegistros;
    Bloco_M.LimpaRegistros;

    Bloco_0.Gravado:=false;
    Bloco_1.Gravado:=false;
    Bloco_9.Gravado:=false;
    Bloco_A.Gravado:=false;
    Bloco_C.Gravado:=false;
    Bloco_D.Gravado:=false;
    Bloco_F.Gravado:=false;
    Bloco_M.Gravado:=false;
  end;
end;

procedure TObjSpedPisCofins_Novo.carregaErros(str: TStringList);
var
  i:integer;
begin
  for i := 0  to str.Count-1  do
    memoErros.Lines.Add(str[i]);
end;

procedure TObjSpedPisCofins_Novo.carregaErrosMemo;
begin
  self.carregaErros(strERROS);
  self.carregaErros(strAdvertencia);
end;

function TObjSpedPisCofins_Novo.pegaModeloTreeView(tv: TTreeView): string;
  var NoRaiz:TTreeNode;
begin

  if tv.Selected = nil then
    Exit;

  NoRaiz := tv.Selected;

  While (Pos('MODELO',UpperCase(NoRaiz.Text)) = 0) do
  begin
    NoRaiz := NoRaiz.Parent;
    if NoRaiz = nil then Exit;
  end;

  result:=RetornaSoNumeros(NoRaiz.Text);
  
end;

constructor TObjSpedPisCofins_Novo.Create(AOwner: TComponent;
  ibdatabase: TIBDatabase);
begin
  inherited Create(AOwner);
  self.ACBrSpedPISCOFINS1 := TACBrSPEDPisCofins.Create(AOwner);
  self.SetDataBase(ibdatabase);
  strErros := TStringList.Create;
  strAdvertencia := TStringList.Create;
  strItem := TStringList.Create;
  strServico := TStringList.Create;
  strUnidade := TStringList.Create;
  strNatOp := TStringList.Create;
  strCodPart := TStringList.Create;
  queryTemp := TIBQuery.Create(nil);
  queryTemp.Database := self.FDatabase;
end;

destructor TObjSpedPisCofins_Novo.destroy;
begin

  FreeAndNil (self.ACBrSpedPISCOFINS1);

  FreeAndNil(FReg0000);
  FreeAndNil(FReg0100);
  FreeAndNil(FReg0110);
  FreeAndNil(FReg0140);
  FreeAndNil(FReg0150C);
  FreeAndNil(Freg0150F);
  FreeAndNil(FReg0190);
  FreeAndNil(FReg0200P);
  FreeAndNil(FReg0200S);
  FreeAndNil(FReg0400);
  FreeAndNil(FRegA001);
  FreeAndNil(FRegC001);
  FreeAndNil(FRegC010);
  FreeAndNil(FRegC100S);
  FreeAndNil(FRegC180);
  FreeAndNil(FRegC190);
  FreeAndNil(FRegD001);
  FreeAndNil(FRegF001);
  FreeAndNil(FRegF010);
  FreeAndNil(FRegM001);
  FreeAndNil(FRegM200);
  FreeAndNil(FRegM210);
  FreeAndNil(FRegM600);
  FreeAndNil(FReg1001);

  FreeAndNil (self.strErros);
  FreeAndNil (self.strAdvertencia);
  FreeAndNil (self.strNatOp);
  FreeAndNil (self.strItem);
  FreeAndNil (self.strServico);
  FreeAndNil (self.strUnidade);
  FreeAndNil (self.strCodPart);
  FreeAndNil (self.queryTemp);
  inherited;
end;

function TObjSpedPisCofins_Novo.geraSped: Boolean;
begin
  preencheSql;
  preencheComponente;
  Result := not (teveErros);
end;

procedure TObjSpedPisCofins_Novo.gravaTXT;
begin
  ACBrSpedPISCOFINS1.SaveFileTXT;
end;

procedure TObjSpedPisCofins_Novo.preencheComponente;
begin

  {BLOCO 0}
  FReg0000.preenche;
  FReg0100.preenche;
  FReg0110.preenche;
  FReg0140.preenche;

  {BLOCO A}
  FRegA001.preenche;

  {BLOCO C}
  FRegC010.preenche;
  FRegC100S.preenche;
  FRegC180.preenche;
  FRegC190.preenche;
  FRegC001.preenche;

  {BLOCO F}
  FRegF010.preenche;
  FRegF001.preenche;

  {BLOCO D}
  FRegD001.preenche;

  {BLOCO M}
  FRegM200.preenche;
  FRegM210.preenche;
  FRegM600.preenche;
  FRegM001.preenche;

  {BLOCO 1}
  FReg1001.preenche;


  FReg0150C.preenche;
  FReg0150F.preenche;
  FReg0200P.preenche;  {registro 0200 deve vir primeiro porque utiliza unidade}
  FReg0200S.preenche;  {de medida e o registro 0190 s�o as unidades de medida}
  FReg0190.preenche;
  FReg0400.preenche;

  if not (teveErros) then
  begin
    gravaTXT;
    Self.status.Panels[1].Text:='Arquivo sped gerado com sucesso: "'+self.PathArquivo+'"';
  end
  else
    Self.status.Panels[1].Text:='Erro ao gerar arquivo: "'+self.PathArquivo+'"';

  Application.ProcessMessages;

end;

procedure TObjSpedPisCofins_Novo.preencheSql;
var
  sqlSped:TobjSqlTxt;
begin

  self.inicializa;
  
  sqlSped := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+'\sqlSped_PISCOFINS.txt');

  try

    FReg0000.SQL  := sqlSped.ReadString('REG0000');
    FReg0100.SQL  := sqlSped.readString('REG0100');
    FReg0100.SQL  := sqlSped.readString('REG0110');
    FReg0140.SQL  := sqlSped.readString('REG0140');
    FReg0150C.SQL := sqlSped.readString('REG0150C');
    FReg0150F.SQL := sqlSped.readString('REG0150F');
    FReg0190.SQL  := sqlSped.readString('REG0190');
    FReg0200P.SQL := sqlSped.readString('REG0200P');
    FReg0400.SQL  := sqlSped.readString('REG0400');

    FRegC010.SQL           := sqlSped.readString('REGC010');
    FRegC100S.SQL          := sqlSped.readString('REGC100S');
    FRegC100S.FRegC170.SQL := sqlSped.readString('REGC170S');

    FRegC180.SQL          := sqlSped.readString('REGC180');
    FRegC180.FRegC181.SQL := sqlSped.readString('REGC181');
    FRegC180.FRegC185.SQL := sqlSped.readString('REGC185');

    FRegC190.SQL := sqlSped.readString('REGC190');
    FRegC190.FRegC191.SQL := sqlSped.readString('REGC191');
    FRegC190.FRegC195.SQL := sqlSped.readString('REGC195');

    FRegF010.SQL := sqlSped.readString('REGF010');
    FRegM200.SQL := sqlSped.readString('REGM200');
    FRegM210.SQL := sqlSped.readString('REGM210')

  finally
    FreeAndNil(sqlSped);
  end;

end;

function TObjSpedPisCofins_Novo.teveErros: Boolean;
begin
  result := (strErros.Count > 0);
end;

procedure TObjSpedPisCofins_Novo.SetnumeroReciboAnterior(const Value: string);
begin
  FnumeroReciboAnterior := Value;
end;

procedure TObjSpedPisCofins_Novo.SetindicadorNaturezaPJ(
  const Value: string);
begin
  FindicadorNaturezaPJ := Value;
end;

procedure TObjSpedPisCofins_Novo.SetCodSitEspecial(const Value: string);
begin
  FCodSitEspecial := Value;
end;

procedure TObjSpedPisCofins_Novo.SetindApropCred(const Value: string);
begin
  FindApropCred := Value;
end;

procedure TObjSpedPisCofins_Novo.SetindContrib(const Value: string);
begin
  FindContrib := Value;
end;

procedure TObjSpedPisCofins_Novo.SetindIncidTrib(const Value: string);
begin
  FindIncidTrib := Value;
end;

procedure TObjSpedPisCofins_Novo.SetindRegCum(const Value: string);
begin
  FindRegCum := Value;
end;

function TObjSpedPisCofins_Novo.pegaTipoNFTreeView(tv: TTreeView): string;
var
  noRaiz:TTreeNode;
begin

   if tv.Selected = nil then
    Exit;

  NoRaiz := tv.Selected;

  While NoRaiz.Parent <> nil do
    NoRaiz := NoRaiz.Parent;

  if (Pos('Pr�pria',noRaiz.Text) <> 0) then
    result := 'S' {saida}
  else
  if (Pos('Terceiros',noRaiz.Text) <> 0) then
    result := 'E' {entrada}
  else
    Result := '';

end;


{ TFonteDados0100 }

procedure TFonteDados0100.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  with (FAcbrEPBloco0.Registro0100New) do
  begin
    NOME     :=  FQuery.Fields[0].Text;
    CPF      :=  FQuery.Fields[1].Text;
    CRC      :=  FQuery.Fields[2].Text;
    CNPJ     :=  FQuery.Fields[3].Text;
    CEP      :=  FQuery.Fields[4].Text;
    ENDERECO :=  FQuery.Fields[5].Text;
    NUM      :=  FQuery.Fields[6].Text;
    COMPL    :=  FQuery.Fields[7].Text;
    BAIRRO   :=  FQuery.Fields[8].Text;
    FONE     :=  FQuery.Fields[9].Text;
    FAX      :=  FQuery.Fields[10].Text;
    EMAIL    :=  FQuery.Fields[11].Text;
    COD_MUN  :=  FQuery.Fields[12].AsInteger;
  end;

end;

procedure TFonteDados0100.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Nome';
  FQuery.Fields[1].DisplayLabel := 'CPF';
  FQuery.Fields[2].DisplayLabel := 'CRC';
  FQuery.Fields[3].DisplayLabel := 'CNPJ';
  FQuery.Fields[4].DisplayLabel := 'CEP';
  FQuery.Fields[5].DisplayLabel := 'Endere�o';
  FQuery.Fields[6].DisplayLabel := 'N�mero';
  FQuery.Fields[7].DisplayLabel := 'Complemento';
  FQuery.Fields[8].DisplayLabel := 'Bairro';
  FQuery.Fields[9].DisplayLabel := 'Fone';
  FQuery.Fields[10].DisplayLabel := 'Fax';
  FQuery.Fields[11].DisplayLabel := 'Email';
  FQuery.Fields[12].DisplayLabel := 'C�digo da cidade';

  FQuery.Fields[3].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[10].Required := False;

  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[4].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[5].OnGetText := GetFieldTrim;
  FQuery.Fields[6].OnGetText := GetFieldTrim;
  FQuery.Fields[7].OnGetText := GetFieldTrim;
  FQuery.Fields[8].OnGetText := GetFieldTrim;
  FQuery.Fields[9].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[10].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[11].OnGetText := GetFieldTrim;
end;

procedure TFonteDados0100.SetParametros;
begin
  inherited;
end;

function TFonteDados0100.ValidaRegistros: Boolean;
begin

  if not ValidaCPF(FQuery.Fields[1].Text) then
    strErros.Add('CPF do contador inv�lido. Registro: '+self.Registro);

  if FQuery.Fields[3].Text <> '' then
    if not ValidaCNPJ(FQuery.Fields[3].Text) then
      strErros.Add('CNPJ inv�lido. Registro: '+self.Registro);

end;

{ TFonteDados0110 }

function TFonteDados0110.getIndApropCred(): TACBrIndAproCred;
begin
  case StrToInt(self.indApropCred)-1 of
    0:result := indMetodoApropriacaoDireta;
    1:Result := indMetodoDeRateioProporcional;
  end
end;

function TFonteDados0110.getIndContrib(): TACBrCodIndTipoCon;
begin
  case StrToInt(self.indContrib)-1 of
    0:result := codIndTipoConExclAliqBasica;
    1:Result := codIndTipoAliqEspecificas;
  end
end;

function TFonteDados0110.getIndIncidTrib(): TACBrCodIndIncTributaria;
begin
  case StrToIntDef(self.indIncidTrib,-1)-1 of
    0:result := codEscrOpIncNaoCumulativo;
    1:Result := codEscrOpIncCumulativo;
    2:Result := codEscrOpIncAmbos;
  end
end;

function TFonteDados0110.getIndRegCum: TACBrCodIndCritEscrit;
begin
  case StrToIntDef(self.indRegCum,-1) of
    1:result := codRegimeCaixa;
    2:Result := codRegimeCompetEscritConsolidada;
    9:Result := codRegimeCompetEscritDetalhada;
  end
end;

procedure TFonteDados0110.preenche;
begin
  inherited;

  if not self.ValidaRegistros then Exit;

  with (FAcbrEPBloco0.Registro0110New) do
  begin

    COD_INC_TRIB := self.getIndIncidTrib;

    if self.indIncidTrib <> '2' then
      IND_APRO_CRED  := self.getIndApropCred;

    COD_TIPO_CONT  := getIndContrib;

    IND_REG_CUM    := Self.getIndRegCum;

  end;
end;

procedure TFonteDados0110.SetCampos;
begin
  inherited;
end;

procedure TFonteDados0110.SetParametros;
begin
  inherited;

end;

function TFonteDados0110.ValidaRegistros: Boolean;
begin

  if indIncidTrib = '2' then
    if indRegCum = '' then
      strErros.Add('�ndice de regime cumulativo obrigat�rio para o tipo de incid�ncia tribut�ria');

  if indIncidTrib <> '2' then
    if indApropCred = '' then
      strErros.Add('Indicador de apropria��o de cr�dito inv�lido para o tipo de incid�ncia tribut�ria');

  result := not(teveErros);

end;

{ TFonteDados0140 }

procedure TFonteDados0140.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;
  with (FAcbrEPBloco0.Registro0140New) do
  begin
    COD_EST := '1';
    NOME    := FQuery.Fields[0].Text;
    CNPJ    := FQuery.Fields[1].Text;
    UF      := FQuery.Fields[2].Text;
    IE      := FQuery.Fields[3].Text;
    COD_MUN := FQuery.Fields[4].AsInteger;
    IM      := '';
    SUFRAMA := '';
  end;
end;

procedure TFonteDados0140.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Raz�o social';
  FQuery.Fields[1].DisplayLabel := 'CNPJ';
  FQuery.Fields[2].DisplayLabel := 'UF';
  FQuery.Fields[3].DisplayLabel := 'IE';
  FQuery.Fields[4].DisplayLabel := 'C�digo da cidade';

  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDados0140.SetParametros;
begin
  inherited;
end;

function TFonteDados0140.ValidaRegistros: Boolean;
begin
  if not ValidaCNPJ(FQuery.Fields[1].Text) then
    strErros.Add('CNPJ da empresa inv�lido. Registro: '+self.Registro);
end;

{ TFonteDados0150 }

procedure TFonteDados0150.preenche;
begin
  inherited;
end;

procedure TFonteDados0150.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo do cliente';
  FQuery.Fields[1].DisplayLabel := 'Nome';
  FQuery.Fields[2].DisplayLabel := 'C�digo do pa�s';
  FQuery.Fields[3].DisplayLabel := 'CPF CGC';
  FQuery.Fields[4].DisplayLabel := 'RG_IE';
  FQuery.Fields[5].DisplayLabel := 'C�digo da cidade';
  FQuery.Fields[6].DisplayLabel := 'Endere�o';
  FQuery.Fields[7].DisplayLabel := 'N�mero';
  FQuery.Fields[8].DisplayLabel := 'Bairro';
  FQuery.Fields[9].DisplayLabel := 'UF';

  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[1].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[4].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[6].OnGetText := GetFieldTrim;
  FQuery.Fields[7].OnGetText := GetFieldTrim;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[8].OnGetText := GetFieldTrim;
  FQuery.Fields[8].Required := False;
end;

procedure TFonteDados0150.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.codigoParticipante;
end;

function TFonteDados0150.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados0150Cliente }

procedure TFonteDados0150Cliente.preenche;
var
  i:Integer;
begin
  inherited;

  for i := 0 to strCodPart.Count-1 do
  begin

    if ((Pos('C',strCodPart[i])) <> 0) then
    begin

      self.codigoParticipante := StrToInt(RetornaSoNumeros(strCodPart[i]));
      if not AbreQuery then Exit;
      if teveErros then Exit;

      with (FAcbrEPBloco0.Registro0150New) do
      begin

        COD_PART := strCodPart[i];
        NOME     := FQuery.Fields[1].Text;
        COD_PAIS := FQuery.Fields[2].Text;

        if ValidaCPF(FQuery.Fields[3].Text) then
          CPF  := FQuery.Fields[3].Text
        else
          CNPJ := FQuery.Fields[3].Text;

        if validaInscricaoEstadual(FQuery.Fields[4].Text,FQuery.Fields[9].Text) then
          IE := FQuery.Fields[4].Text;

        COD_MUN  := FQuery.Fields[5].AsInteger;
        SUFRAMA  := '';
        ENDERECO := FQuery.Fields[6].Text;
        NUM      := FQuery.Fields[7].Text;
        COMPL    := '';
        BAIRRO   := FQuery.Fields[8].Text;

      end;

    end;

  end;

end;

procedure TFonteDados0150Cliente.SetCampos;
begin
  inherited;
end;

procedure TFonteDados0150Cliente.SetParametros;
begin
  inherited;
end;

function TFonteDados0150Cliente.ValidaRegistros: Boolean;
begin
  if not (ValidaCPF(FQuery.Fields[3].Text)) and not (ValidaCNPJ(FQuery.Fields[3].Text)) then
    strErros.Add('Campo inv�lido: '+fquery.Fields[3].DisplayLabel+'. Registro: '+self.Registro);
end;

{ TFonteDados0150Fornecedor }

procedure TFonteDados0150Fornecedor.preenche;
var
  i:Integer;
begin
  inherited;

  for i := 0 to strCodPart.Count-1 do
  begin

    if ((Pos('F',strCodPart[i])) <> 0) then
    begin

      self.codigoParticipante := StrToInt(RetornaSoNumeros(strCodPart[i]));
      if not AbreQuery then Exit;
      if teveErros then Exit;

      with (FAcbrEPBloco0.Registro0150New) do
      begin

        COD_PART := strCodPart[i];
        NOME     := FQuery.Fields[1].Text;
        COD_PAIS := FQuery.Fields[2].Text;

        if ValidaCPF(FQuery.Fields[3].Text) then
          CPF  := FQuery.Fields[3].Text
        else
          CNPJ := FQuery.Fields[3].Text;

        IE := FQuery.Fields[4].Text;
        COD_MUN  := FQuery.Fields[5].AsInteger;
        SUFRAMA  := '';
        ENDERECO := FQuery.Fields[6].Text;
        NUM      := FQuery.Fields[7].Text;
        COMPL    := '';
        BAIRRO   := FQuery.Fields[8].Text;

      end;

    end;

  end;

end;

procedure TFonteDados0150Fornecedor.SetCampos;
begin
  inherited;
end;

procedure TFonteDados0150Fornecedor.SetParametros;
begin
  inherited;
end;

function TFonteDados0150Fornecedor.ValidaRegistros: Boolean;
begin

  if not (ValidaCPF(FQuery.Fields[3].Text)) and not (ValidaCNPJ(FQuery.Fields[3].Text)) then
    strErros.Add('Campo inv�lido: '+fquery.Fields[3].DisplayLabel+'. Registro: '+self.Registro);

  if validaInscricaoEstadual(FQuery.Fields[4].Text,FQuery.Fields[9].Text) then
    strErros.Add('Campo inv�lido: '+fquery.Fields[4].DisplayLabel+'. Registro: '+self.Registro);
    
end;

{ TFonteDados0190 }

procedure TFonteDados0190.preenche;
var
  _i:Integer;
begin
  inherited;

  for _i:=0  to strUnidade.Count-1  do
  begin

    self.pSigla := strUnidade[_i];

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrEPBloco0.Registro0190New) do
    begin
      UNID  := strUnidade[_i];
      DESCR := FQuery.Fields[0].Text;

      if strUnidade[_i] = '' then
        strErros.Add('Unidade de medida com valor em branco');

      if FQuery.Fields[0].Text = '' then
        strErros.Add('Descri��o da unidade de medida n�o infomada. Sigla: '+fquery.Params[0].AsString);
    end;

  end;

end;

procedure TFonteDados0190.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Descri��o';
  FQuery.Fields[0].OnGetText := GetFieldTrim;
end;

procedure TFonteDados0190.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsString := pSigla;
end;

function TFonteDados0190.ValidaRegistros: Boolean;
begin
end;

{ TFonteDados0200 }

procedure TFonteDados0200.preenche;
begin
  inherited;
end;

procedure TFonteDados0200.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Descri��o';
  FQuery.Fields[1].DisplayLabel := 'C�digo de barras';
  FQuery.Fields[2].DisplayLabel := 'Unidade';
  FQuery.Fields[3].DisplayLabel := 'NCM';

  FQuery.Fields[1].Required := False;
  FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;
end;

procedure TFonteDados0200.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsString := self.pCodigoItem;
end;

function TFonteDados0200.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados0200Produto }

procedure TFonteDados0200Produto.preenche;
var
  i:Integer;
begin
  inherited;

  for i:=0  to strItem.Count-1  do
  begin

    self.pCodigoItem := strItem[i];

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrEPBloco0.Registro0200New) do
    begin
      COD_ITEM   := strItem[i];
      DESCR_ITEM := FQuery.Fields[0].Text;
      COD_BARRA  := FQuery.Fields[1].Text;
      UNID_INV   := FQuery.Fields[2].Text;
      COD_NCM    := FQuery.Fields[3].Text;
      TIPO_ITEM  := tiMercadoriaRevenda;
      addStr (strUnidade,UNID_INV);
    end;

  end;

end;

procedure TFonteDados0200Produto.SetCampos;
begin
  inherited;
end;

procedure TFonteDados0200Produto.SetParametros;
begin
  inherited;
end;

function TFonteDados0200Produto.ValidaRegistros: Boolean;
begin

  if (Length(FQuery.Fields[3].Text) <> 2) and (Length(FQuery.Fields[3].text) <> 8) then
    strErros.Add('Tamanho de campo(NCM) inv�lido. Produto: '+fquery.Params[0].AsString+' com NCM '+fquery.Fields[3].Text);

end;

{ TFonteDados0200Servico }

procedure TFonteDados0200Servico.preenche;
var
  i:Integer;
begin
  inherited;

  for i:=0  to strServico.Count-1  do
  begin

    self.pCodigoItem := strServico[i];

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrEPBloco0.Registro0200New) do
    begin
      COD_ITEM   := strServico[i];
      DESCR_ITEM := FQuery.Fields[0].Text;
      COD_BARRA  := FQuery.Fields[1].Text;
      UNID_INV   := FQuery.Fields[2].Text;
      COD_NCM    := FQuery.Fields[3].Text;
      TIPO_ITEM  := tiServicos;
      addStr (strUnidade,UNID_INV);
    end;

  end;

end;

procedure TFonteDados0200Servico.SetCampos;
begin
  inherited;
  FQuery.Fields[3].Required := False;
end;

procedure TFonteDados0200Servico.SetParametros;
begin
  inherited;
end;

function TFonteDados0200Servico.ValidaRegistros: Boolean;
begin

end;

{ TFonteDados0400 }

procedure TFonteDados0400.inicializa;
begin

end;

procedure TFonteDados0400.preenche;
var
  i:Integer;
begin
  inherited;

  for i:=0  to strNatOp.Count-1  do
  begin

    self.pCodigoNatOp := strNatOp[i];

    if not AbreQuery then Exit;
    if teveErros then Exit;

    with (FAcbrEPBloco0.Registro0400New) do
    begin
      COD_NAT   := strNatOp[i];
      DESCR_NAT := FQuery.Fields[0].Text;
    end;
    
  end;
  
end;

procedure TFonteDados0400.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'Descri��o';
  FQuery.Fields[0].OnGetText := GetFieldTrim;
end;

procedure TFonteDados0400.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsString := self.pCodigoNatOp;
end;

function TFonteDados0400.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosBlocoA }

procedure TFonteDadosBlocoA.SetParametros;
begin
  inherited;
end;

{ TFonteDadosA001 }

procedure TFonteDadosA001.preenche;
begin
  inherited;
  if (FAcbrEPBlocoA.RegistroA001New.RegistroA010.Count <= 0) then
    FAcbrEPBlocoA.RegistroA001New.IND_MOV := imSemDados
  else
    FAcbrEPBlocoA.RegistroA001New.IND_MOV := imComDados;
end;

procedure TFonteDadosA001.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosA001.SetParametros;
begin
  inherited;
end;

function TFonteDadosA001.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosBlocoC }

function TFonteDadosBlocoC.getSituacaoNF(pSituacao: string): TACBrCodSit;
begin
  pSituacao:=UpperCase(pSituacao);
  if (pSituacao = 'I') or (pSituacao = 'G') or (pSituacao = 'N') then {situa��o "N" de nao cancelada}
    result := sdfRegular
  else
  if (pSituacao = 'C') or (pSituacao = 'S') then {situa��o "S" de cancelada}
    result := sdfCancelado
  else if (pSituacao = 'Z') then
    result := sdfInutilizado;
end;

function TFonteDadosBlocoC.getTipoFrete(pTipo: string): TACBrTipoFrete;
begin
  if (pTipo = '1') then
    result := tfPorContaEmitente
  else
  if (pTipo = '2') then
    result := tfPorContaDestinatario
  else
    result := tfSemCobrancaFrete;
end;

function TFonteDadosBlocoC.getValorCofins(BCCOFINS: Currency): Currency;
begin
  result := (BCCOFINS * pCOFINS / 100);
end;

function TFonteDadosBlocoC.getValorPis(BCPIS: Currency): Currency;
begin
  result := (bcPIS * pPIS / 100);
end;

function TFonteDadosBlocoC.get_CSTCOFINS(cst_cofins: string): TACBrSituacaoTribCOFINS;
begin

  case StrToIntDef (cst_cofins,99) of

      1:result := stcofinsValorAliquotaNormal;
      2:Result := stcofinsValorAliquotaDiferenciada;
      3:result := stcofinsQtdeAliquotaUnidade;
      4:Result := stcofinsMonofaticaAliquotaZero;
      5:result := stcofinsValorAliquotaPorST;
      6:Result := stcofinsAliquotaZero;
      7:result := stcofinsIsentaContribuicao;
      8:Result := stcofinsSemIncidenciaContribuicao;
      9:result := stcofinsSuspensaoContribuicao;

      49:Result := stcofinsOutrasOperacoesSaida;

      50:Result := stcofinsOperCredExcRecTribMercInt;
      51:Result := stcofinsOperCredExcRecNaoTribMercInt;
      52:Result := stcofinsOperCredExcRecExportacao;
      53:Result := stcofinsOperCredRecTribNaoTribMercInt;
      54:Result := stcofinsOperCredRecTribMercIntEExportacao;
      55:Result := stcofinsOperCredRecNaoTribMercIntEExportacao;
      56:Result := stcofinsOperCredRecTribENaoTribMercIntEExportacao;

      60:result := stcofinsCredPresAquiExcRecTribMercInt;
      61:result := stcofinsCredPresAquiExcRecNaoTribMercInt;
      62:result := stcofinsCredPresAquiExcExcRecExportacao;
      63:result := stcofinsCredPresAquiRecTribNaoTribMercInt;
      64:result := stcofinsCredPresAquiRecTribMercIntEExportacao;
      65:result := stcofinsCredPresAquiRecNaoTribMercIntEExportacao;
      66:result := stcofinsCredPresAquiRecTribENaoTribMercIntEExportacao;
      67:result := stcofinsOutrasOperacoes_CredPresumido;

      70:result := stcofinsOperAquiSemDirCredito;
      71:result := stcofinsOperAquiComIsensao;
      72:result := stcofinsOperAquiComSuspensao;
      73:result := stcofinsOperAquiAliquotaZero;
      74:result := stcofinsOperAqui_SemIncidenciaContribuicao;
      75:result := stcofinsOperAquiPorST;

      98:result := stcofinsOutrasOperacoesEntrada;
      99:result := stcofinsOutrasOperacoes;

  else
    Result := stcofinsOutrasOperacoes;
  end;

end;


function TFonteDadosBlocoC.get_CSTICMS(cst_icms: string): TACBrSituacaoTribICMS;
begin

  case StrToInt(cst_icms) of
    0: result := sticmsTributadaIntegralmente;
    10:Result := sticmsTributadaComCobracaPorST;
    20:result := sticmsComReducao;
    30:result := sticmsIsentaComCobracaPorST;
    40:result := sticmsIsenta;
    41:result := sticmsNaoTributada;
    50:result := sticmsSuspensao;
    51:result := sticmsDiferimento;
    60:result := sticmsCobradoAnteriormentePorST;
    70:result := sticmsComReducaoPorST;
    90:result := sticmsOutros;
    100:result := sticmsEstrangeiraImportacaoDiretaTributadaIntegralmente;
    110:result := sticmsEstrangeiraImportacaoDiretaTributadaComCobracaPorST;
    120:result := sticmsEstrangeiraImportacaoDiretaComReducao;
    130:result := sticmsEstrangeiraImportacaoDiretaIsentaComCobracaPorST;
    140:result := sticmsEstrangeiraImportacaoDiretaIsenta;
    141:result := sticmsEstrangeiraImportacaoDiretaNaoTributada;
    150:result := sticmsEstrangeiraImportacaoDiretaSuspensao;
    151:result := sticmsEstrangeiraImportacaoDiretaDiferimento;
    160:result := sticmsEstrangeiraImportacaoDiretaCobradoAnteriormentePorST;
    170:result := sticmsEstrangeiraImportacaoDiretaComReducaoPorST;
    190:result := sticmsEstrangeiraImportacaoDiretaOutros;
    200:result := sticmsEstrangeiraAdqMercIntTributadaIntegralmente;
    210:result := sticmsEstrangeiraAdqMercIntTributadaComCobracaPorST;
    220:result := sticmsEstrangeiraAdqMercIntComReducao;
    230:result := sticmsEstrangeiraAdqMercIntIsentaComCobracaPorST;
    240:result := sticmsEstrangeiraAdqMercIntIsenta;
    241:result := sticmsEstrangeiraAdqMercIntNaoTributada;
    250:result := sticmsEstrangeiraAdqMercIntSuspensao;
    251:result := sticmsEstrangeiraAdqMercIntDiferimento;
    260:result := sticmsEstrangeiraAdqMercIntCobradoAnteriormentePorST;
    270:result := sticmsEstrangeiraAdqMercIntComReducaoPorST;
    290:result := sticmsEstrangeiraAdqMercIntOutros;

    101:result := sticmsSimplesNacionalTributadaComPermissaoCredito;
    102:result := sticmsSimplesNacionalTributadaSemPermissaoCredito;
    103:result := sticmsSimplesNacionalIsencaoPorFaixaReceitaBruta;
    201:result := sticmsSimplesNacionalTributadaComPermissaoCreditoComST;
    202:result := sticmsSimplesNacionalTributadaSemPermissaoCreditoComST;
    203:result := sticmsSimplesNacionalIsencaoPorFaixaReceitaBrutaComST;
    300:result := sticmsSimplesNacionalImune;
    400:result := sticmsSimplesNacionalNaoTributada;
    500:result := sticmsSimplesNacionalCobradoAnteriormentePorST;
    900:result := sticmsSimplesNacionalOutros;

  else
    Result := sticmsOutros;
  end;

end;

function TFonteDadosBlocoC.get_CSTIPI(cst_ipi: string): TACBrSituacaoTribIPI;
begin

  case StrToInt(cst_ipi) of

    0:result  := stipiEntradaRecuperacaoCredito;
    1:result  := stipiEntradaTributradaZero;
    2:result  := stipiEntradaIsenta;
    3:result  := stipiEntradaNaoTributada;
    4:result  := stipiEntradaImune;
    5:result  := stipiEntradaComSuspensao;
    49:result := stipiOutrasEntradas;
    50:result := stipiSaidaTributada;
    51:result := stipiSaidaTributadaZero;
    52:result := stipiSaidaIsenta;
    53:result := stipiSaidaNaoTributada;
    54:result := stipiSaidaImune;
    55:result := stipiSaidaComSuspensao;
    99:result := stipiOutrasSaidas;
    
  else result := stipiOutrasSaidas;

  end;

end;

function TFonteDadosBlocoC.get_CSTPIS(cst_pis: string): TACBrSituacaoTribPIS;
begin

  case StrToIntdef (cst_pis,99) of

      1:result := stpisValorAliquotaNormal;
      2:Result := stpisValorAliquotaDiferenciada;
      3:result := stpisQtdeAliquotaUnidade;
      4:Result := stpisMonofaticaAliquotaZero;
      5:result := stpisValorAliquotaPorST;
      6:Result := stpisAliquotaZero;
      7:result := stpisIsentaContribuicao;
      8:Result := stpisSemIncidenciaContribuicao;
      9:result := stpisSuspensaoContribuicao;

      49:Result := stpisOutrasOperacoesSaida;

      50:Result := stpisOperCredExcRecTribMercInt;
      51:Result := stpisOperCredExcRecNaoTribMercInt;
      52:Result := stpisOperCredExcRecExportacao;
      53:Result := stpisOperCredRecTribNaoTribMercInt;
      54:Result := stpisOperCredRecTribMercIntEExportacao;
      55:Result := stpisOperCredRecNaoTribMercIntEExportacao;
      56:Result := stpisOperCredRecTribENaoTribMercIntEExportacao;

      60:result := stpisCredPresAquiExcRecTribMercInt;
      61:result := stpisCredPresAquiExcRecNaoTribMercInt;
      62:result := stpisCredPresAquiExcExcRecExportacao;
      63:result := stpisCredPresAquiRecTribNaoTribMercInt;
      64:result := stpisCredPresAquiRecTribMercIntEExportacao;
      65:result := stpisCredPresAquiRecNaoTribMercIntEExportacao;
      66:result := stpisCredPresAquiRecTribENaoTribMercIntEExportacao;
      67:result := stpisOutrasOperacoes_CredPresumido;

      70:result := stpisOperAquiSemDirCredito;
      71:result := stpisOperAquiComIsensao;
      72:result := stpisOperAquiComSuspensao;
      73:result := stpisOperAquiAliquotaZero;
      74:result := stpisOperAqui_SemIncidenciaContribuicao;
      75:result := stpisOperAquiPorST;

      98:result := stpisOutrasOperacoesEntrada;
      99:result := stpisOutrasOperacoes;

  else ;

      Result := stpisOutrasOperacoes;

  end;

end;

procedure TFonteDadosBlocoC.SetindIncidTributaria(const Value: string);
begin
  FindIncidTributaria := Value;
end;

procedure TFonteDadosBlocoC.SetParametros;
begin
  inherited;
end;

procedure TFonteDadosBlocoC.SetpCOFINS(const Value: Currency);
begin
  FpCOFINS := Value;
end;

procedure TFonteDadosBlocoC.SetpPIS(const Value: Currency);
begin
  FpPIS := Value;
end;

{ TFonteDadosC001 }

procedure TFonteDadosC001.preenche;
begin
  inherited;
  if (FAcbrEPBlocoC.RegistroC001.RegistroC010.Count <= 0) then
    FAcbrEPBlocoC.RegistroC001New.IND_MOV := imSemDados
  else
    FAcbrEPBlocoC.RegistroC001New.IND_MOV := imComDados;
end;

procedure TFonteDadosC001.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC001.SetParametros;
begin
  inherited;
end;

function TFonteDadosC001.ValidaRegistros: Boolean;
begin
end;

{ TFonteDadosC010 }

procedure TFonteDadosC010.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  with (FAcbrEPBlocoC.RegistroC010New) do
  begin
    CNPJ      := FQuery.Fields[0].Text;
    IND_ESCRI := IndEscriIndividualizado;
  end;
end;

procedure TFonteDadosC010.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CNPJ';
  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDadosC010.SetParametros;
begin
end;

function TFonteDadosC010.ValidaRegistros: Boolean;
begin
end;

{ TFonteDadosC100 }

procedure TFonteDadosC100.preenche;
begin
  inherited;  
end;

procedure TFonteDadosC100.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC100.SetParametros;
begin
  inherited;
end;

function TFonteDadosC100.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC170 }

function TFonteDadosC170.getVL_BC_ICMS(pReducao,
  baseCalculo: Currency): Currency;
begin
  Result := baseCalculo - (pReducao * baseCalculo/100);
  Result := StrToCurrDef(FormatFloat('0.00',Result),0);
end;

function TFonteDadosC170.getVL_ICMS(bc_icms, aliquota: currency): Currency;
begin
  result := bc_icms * aliquota / 100;
  result := StrToCurrDef(FormatFloat('0.00',result),0);
end;

procedure TFonteDadosC170.preenche;
begin
  inherited;
end;

procedure TFonteDadosC170.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosC170.SetParametros;
begin
  FQuery.Params[0].AsInteger := self.codigoNF;
end;

function TFonteDadosC170.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC170Saida }

procedure TFonteDadosC170Saida.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not (FQuery.Eof) do
  begin

    with (FAcbrEPBlocoC.RegistroC170New) do
    begin

      NUM_ITEM    :=  IntToStr(FQuery.RecNo);
      COD_ITEM    :=  FQuery.Fields[1].Text;
      DESCR_COMPL :=  FQuery.Fields[2].Text;
      QTD         :=  FQuery.Fields[3].AsCurrency;
      UNID        :=  FQuery.Fields[4].Text;
      VL_ITEM     :=  FQuery.Fields[5].AsCurrency;
      VL_DESC     :=  FQuery.Fields[6].AsCurrency;
      IND_MOV     :=  mfSim;
      CFOP        :=  FQuery.Fields[10].Text;
      COD_NAT     :=  FQuery.Fields[10].Text;
      IND_APUR    :=  iaMensal;

      addStr (strUnidade,UNID);
      addStr (strItem,COD_ITEM);
      addStr (strNatOp,CFOP);

      {ICMS}
      if (FQuery.Fields[8].Text <> '') then
        CST_ICMS := self.get_CSTICMS (FQuery.Fields[7].Text + FQuery.Fields[8].Text)
      else
        CST_ICMS := self.get_CSTICMS (FQuery.Fields[9].Text);

      ALIQ_ICMS :=  FQuery.Fields[13].AsCurrency;

      if (ALIQ_ICMS >= 0) then
      begin
        VL_BC_ICMS :=  self.getVL_BC_ICMS(FQuery.Fields[11].AsCurrency,FQuery.Fields[12].AsCurrency);
        VL_ICMS    :=  self.getVL_ICMS(VL_BC_ICMS,ALIQ_ICMS);
      end
      else
      begin
        VL_BC_ICMS := 0;
        VL_ICMS    := 0;
      end;

      {ICMS ST}
      VL_BC_ICMS_ST :=  0;
      ALIQ_ST       :=  0;
      VL_ICMS_ST    :=  0;

      {IPI}
      if FQuery.Fields[15].Text <> '' then
        CST_IPI       :=  get_CSTIPI (FQuery.Fields[15].Text);
      COD_ENQ       :=  '';
      {VL_BC_IPI     :=  '';}
      {ALIQ_IPI      :=  '';}
      {VL_IPI        := '';}


      {PIS}
      CST_PIS       :=  self.get_CSTPIS (FQuery.Fields[16].Text);
      VL_BC_PIS     :=  FQuery.Fields[12].AsCurrency;
      ALIQ_PIS_PERC :=  FQuery.Fields[20].AsCurrency;
      {QUANT_BC_PIS  :=  '';}
      {ALIQ_PIS_R    :=  '';}
      VL_PIS        :=  FQuery.Fields[18].AsCurrency;

      {COFINS}
      CST_COFINS        :=  self.get_cstcofins(FQuery.Fields[17].text);
      VL_BC_COFINS      :=  FQuery.Fields[12].AsCurrency;
      ALIQ_COFINS_PERC  :=  FQuery.Fields[21].AsCurrency;
      {QUANT_BC_COFINS  := '';}
      {ALIQ_COFINS_R    := '';}
      VL_COFINS         := FQuery.Fields[19].AsCurrency;

      COD_CTA   := '';

    end;

    FQuery.Next;

  end;
end;

procedure TFonteDadosC170Saida.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo NF';
  FQuery.Fields[1].DisplayLabel := 'C�digo produto';
  FQuery.Fields[2].DisplayLabel := 'Descricao produto';
  FQuery.Fields[3].DisplayLabel := 'QTD';
  FQuery.Fields[4].DisplayLabel := 'UN';
  FQuery.Fields[5].DisplayLabel := 'Valor';
  FQuery.Fields[6].DisplayLabel := 'Desconto';
  FQuery.Fields[7].DisplayLabel := 'Origem da mercadoria';
  FQuery.Fields[8].DisplayLabel := 'CST';
  FQuery.Fields[9].DisplayLabel := 'CSOSN';
  FQuery.Fields[10].DisplayLabel := 'CFOP';
  FQuery.Fields[11].DisplayLabel := 'Redu��o BC ICMS';
  FQuery.Fields[12].DisplayLabel := 'Valor final';
  FQuery.Fields[13].DisplayLabel := 'Al�quota';
  FQuery.Fields[14].DisplayLabel := 'NCM';
  FQuery.Fields[15].DisplayLabel := 'CST IPI';
  FQuery.Fields[16].DisplayLabel := 'CST PIS';
  FQuery.Fields[17].DisplayLabel := 'CST COFINS';
  FQuery.Fields[18].DisplayLabel := 'Valor PIS';
  FQuery.Fields[19].DisplayLabel := 'Valor COFINS';
  FQuery.Fields[20].DisplayLabel := 'Percentual PIS';
  FQuery.Fields[21].DisplayLabel := 'Percentual COFINS';

  FQuery.Fields[6].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[9].Required := False;
  FQuery.Fields[11].Required := False;
  FQuery.Fields[13].Required := False;
  FQuery.Fields[15].Required := False;
  FQuery.Fields[16].Required := False;
  FQuery.Fields[17].Required := False;
  FQuery.Fields[2].OnGetText  := GetFieldTrim;
  FQuery.Fields[4].OnGetText  := GetFieldTrim;
  FQuery.Fields[14].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDadosC170Saida.SetParametros;
begin
  inherited;
end;

function TFonteDadosC170Saida.ValidaRegistros: Boolean;
begin

  {PIS}
  if FQuery.Fields[16].Text = '1' then
    if (FQuery.Fields[20].AsCurrency <> 0.65) and (FQuery.Fields[20].AsCurrency <> 1.65) then
      strErros.Add('Devem ser informadas al�quotas b�sicas (0,65 ou 1,65) para opera��es com CST do PIS = 01');

  {COFINS}
  if FQuery.Fields[17].Text = '1' then
    if (FQuery.Fields[21].AsCurrency <> 3) and (FQuery.Fields[21].AsCurrency <> 7.6) then
      strErros.Add('Devem ser informadas al�quotas b�sicas (3 ou 7.6) para opera��es com CST da COFINS = 01');

  {PIS}
  if self.indIncidTributaria = '1' then
    if FQuery.Fields[16].Text = '1' then
      if FQuery.Fields[20].AsCurrency <> 1.65 then
        strErros.Add('Devem ser informadas al�quotas b�sicas (PIS 1.65) para opera��es com CST = 1, de acordo com o indicador de incid�ncia tribut�ria no per�odo');

  {COFINS}
  if self.indIncidTributaria = '1' then
    if FQuery.Fields[17].Text = '1' then
      if FQuery.Fields[21].AsCurrency <> 7.6 then
        strErros.Add('Devem ser informadas al�quotas b�sicas (COFINS 7.6) para opera��es com CST = 1, de acordo com o indicador de incid�ncia tribut�ria no per�odo');

end;

{ TFonteDadosC100Saida }

constructor TFonteDadosC100Saida.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC100Saida.destroy;
begin
  FreeAndNil(FRegC170);
  inherited;
end;

procedure TFonteDadosC100Saida.inicializa;
var
  i:Integer;
begin

  FRegC170 := TFonteDadosC170Saida.Create(self);
  FRegC170.Registro := 'C170S';

  //FRegC190CST := TFonteDadosC190SaidaCST.Create(self);
  //FRegC190CST.Registro := 'C190 SAIDACST';

  //FRegC190CSOSN := TFonteDadosC190SaidaCSOSN.Create(self);
  //FRegC190CSOSN.Registro := 'C190 SAIDACSOSN';


  for i := 0  to self.ComponentCount-1  do
  begin
  
    TFonteDados(self.Components[i]).Database := self.DataBase;
    TFonteDados(self.Components[i]).DataINI := self.FDataIni;
    TFonteDados(self.Components[i]).DataFIN := self.FDataFin;
    TFonteDados(self.Components[i]).DataIniEstoque := self.FDataIniEstoque;
    TFonteDados(self.Components[i]).DataFinEstoque := self.FDataFinEstoque;
    TFonteDados(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDados(self.Components[i]).strItem := self.strItem;
    TFonteDados(self.Components[i]).strServico := self.strServico;
    TFonteDados(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDados(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDados(self.Components[i]).strErros := self.strErros;
    TFonteDados(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDados(self.Components[i]).valor_total_debitos  := 0;
    TFonteDados(self.Components[i]).VL_TOT_CREDITOS_E110 := 0;
    TFonteDados(self.Components[i]).status := self.Fstatus;
    TFonteDados(self.components[i]).geraRegistroH := self.geraRegistroH;
    TFonteDados(self.Components[i]).software := self.software;
    TFonteDados(self.Components[i]).indPerfil := self.IndPerfil;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrEPBlocoC := Self.FAcbrEPBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
      TFonteDadosBlocoC(self.Components[i]).indIncidTributaria := self.indIncidTributaria;
    end;

  end;


end;

procedure TFonteDadosC100Saida.preenche;
var
  situacaoNF:TACBrCodSit;
begin

  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    {PIS COFINS}
    if (FQuery.Fields[22].AsCurrency > 0) or (FQuery.Fields[23].AsCurrency > 0) then
    begin

      with (FAcbrEPBlocoC.RegistroC100New) do
      begin

        IND_OPER   := tpSaidaPrestacao;
        IND_EMIT   := edEmissaoPropria;

        COD_MOD := CompletaPalavra_a_Esquerda(FQuery.Fields[3].Text,2,'0');

        if (FQuery.Fields[5].Text = 'S') then
          COD_SIT := sdfComplementar
        else
          COD_SIT  := Self.getSituacaoNF(FQuery.Fields[4].Text);

        {usado no registro C190}
        situacaoNF := COD_SIT;

        if (FQuery.Fields[3].Text = '55') then
          SER := '1'
        else
          SER := '';

        NUM_DOC  := FQuery.Fields[6].Text;
        CHV_NFE  := FQuery.Fields[7].Text;

        {o componente estava assumindo valores, e neste caso o campo deve conter vazio, sen�o o validador reclama}
        IND_PGTO := tpNenhum;
        IND_FRT  := tfNenhum;

        if (FQuery.Fields[1].Text <> '') then
          COD_PART := FQuery.Fields[1].Text+'C'
        else
          COD_PART := FQuery.Fields[2].Text+'F';

        addStr(strCodPart,COD_PART);

        NUM_DOC       := FQuery.Fields[6].Text;
        CHV_NFE       := FQuery.Fields[7].Text;
        DT_DOC        := FQuery.Fields[8].AsDateTime;
        DT_E_S        := FQuery.Fields[9].AsDateTime;
        VL_DOC        := FQuery.Fields[10].AsCurrency;
        IND_PGTO      := tpVista;
        VL_DESC       := FQuery.Fields[12].AsCurrency;
        VL_MERC       := FQuery.Fields[11].AsCurrency;
        IND_FRT       := self.getTipoFrete(FQuery.Fields[13].Text);
        VL_FRT        := FQuery.Fields[16].AsCurrency;
        VL_SEG        := FQuery.Fields[14].AsCurrency;
        VL_OUT_DA     := FQuery.Fields[15].AsCurrency;
        VL_BC_ICMS    := FQuery.Fields[17].AsCurrency;
        VL_ICMS       := FQuery.Fields[18].AsCurrency;
        VL_BC_ICMS_ST := FQuery.Fields[19].AsCurrency;
        VL_ICMS_ST    := FQuery.Fields[20].AsCurrency;
        VL_IPI        := FQuery.Fields[21].AsCurrency;
        VL_PIS        := FQuery.Fields[22].AsCurrency;
        VL_COFINS     := FQuery.Fields[23].AsCurrency;
        VL_COFINS_ST  := StrToCurr('0,00');
        VL_PIS_ST     := StrToCurr('0,00');

      end;

    end;

    FRegC170.codigoNF := FQuery.Fields[0].AsInteger;
    FRegC170.preenche;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC100Saida.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel  := 'C�digo NF';
  FQuery.Fields[1].DisplayLabel  := 'C�digo cliente';
  FQuery.Fields[2].DisplayLabel  := 'C�digo fornecedor';
  FQuery.Fields[3].DisplayLabel  := 'C�digo fiscal';
  FQuery.Fields[4].DisplayLabel  := 'Situa��o NF';
  FQuery.Fields[5].DisplayLabel  := 'NF complementar';
  FQuery.Fields[6].DisplayLabel  := 'N�mero NF';
  FQuery.Fields[7].DisplayLabel  := 'Chave acesso';
  FQuery.Fields[8].DisplayLabel  := 'Data emiss�o';
  FQuery.Fields[9].DisplayLabel  := 'Data saida';
  FQuery.Fields[10].DisplayLabel := 'Valor final';
  FQuery.Fields[11].DisplayLabel := 'Valor total';
  FQuery.Fields[12].DisplayLabel := 'Desconto';
  FQuery.Fields[13].DisplayLabel := 'FretePorContaTransportadora';
  FQuery.Fields[14].DisplayLabel := 'Valor seguro';
  FQuery.Fields[15].DisplayLabel := 'Outras despesas';
  FQuery.Fields[16].DisplayLabel := 'Valor frete';
  FQuery.Fields[17].DisplayLabel := 'BC ICMS';
  FQuery.Fields[18].DisplayLabel := 'VL ICMS';
  FQuery.Fields[19].DisplayLabel := 'BC ICMS ST';
  FQuery.Fields[20].DisplayLabel := 'VL ICMS ST';
  FQuery.Fields[21].DisplayLabel := 'Valor IPI';
  FQuery.Fields[22].DisplayLabel := 'Valor PIS';
  FQuery.Fields[23].DisplayLabel := 'Valor COFINS';

  FQuery.Fields[6].OnGetText := GetFieldTrim;
  FQuery.Fields[7].OnGetText := GetFieldTrim;

  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[5].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[12].Required := False;
  FQuery.Fields[13].Required := False;
  FQuery.Fields[14].Required := False;
  FQuery.Fields[15].Required := False;
  FQuery.Fields[16].Required := False;
  FQuery.Fields[17].Required := False;
  FQuery.Fields[18].Required := False;
  FQuery.Fields[19].Required := False;
  FQuery.Fields[20].Required := False;
  FQuery.Fields[21].Required := False;
  FQuery.Fields[22].Required := False;
  FQuery.Fields[23].Required := False;
end;

procedure TFonteDadosC100Saida.SetParametros;
begin
  inherited;
end;

function TFonteDadosC100Saida.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC180 }

constructor TFonteDadosC180.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC180.destroy;
begin
  FreeAndNil(FRegC181);
  FreeAndNil(FRegC185);
  inherited;
end;

procedure TFonteDadosC180.inicializa;
var
  i:Integer;
begin

  FRegC181 := TFonteDadosC181.Create(self);
  FRegC181.Registro := 'C181';

  FRegC185 := TFonteDadosC185.Create(self);
  FRegC185.Registro := 'C185';

  for i := 0  to self.ComponentCount-1  do
  begin
  
    TFonteDados(self.Components[i]).Database := self.DataBase;
    TFonteDados(self.Components[i]).DataINI := self.FDataIni;
    TFonteDados(self.Components[i]).DataFIN := self.FDataFin;
    TFonteDados(self.Components[i]).DataIniEstoque := self.FDataIniEstoque;
    TFonteDados(self.Components[i]).DataFinEstoque := self.FDataFinEstoque;
    TFonteDados(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDados(self.Components[i]).strItem := self.strItem;
    TFonteDados(self.Components[i]).strServico := self.strServico;
    TFonteDados(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDados(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDados(self.Components[i]).strErros := self.strErros;
    TFonteDados(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDados(self.Components[i]).valor_total_debitos  := 0;
    TFonteDados(self.Components[i]).VL_TOT_CREDITOS_E110 := 0;
    TFonteDados(self.Components[i]).status := self.Fstatus;
    TFonteDados(self.components[i]).geraRegistroH := self.geraRegistroH;
    TFonteDados(self.Components[i]).software := self.software;
    TFonteDados(self.Components[i]).indPerfil := self.IndPerfil;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrEPBlocoC := Self.FAcbrEPBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;

    if self.Components[i] is TFonteDadosC181 then
      TFonteDadosC181(self.Components[i]).indIncidTributaria := self.indIncidTributaria;

    if self.Components[i] is TFonteDadosC185 then
      TFonteDadosC185(self.Components[i]).indIncidTributaria := self.indIncidTributaria;

  end;

end;

procedure TFonteDadosC180.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  FQuery.First;
  while not FQuery.Eof do
  begin

    with (FAcbrEPBlocoC.RegistroC180New) do
    begin
      COD_MOD     := FQuery.Fields[0].Text;
      DT_DOC_INI  := DataINI;
      DT_DOC_FIN  := DataFIN;

      COD_ITEM    := FQuery.Fields[1].Text;
      addStr (strItem,COD_ITEM);

      COD_NCM     := FQuery.Fields[2].Text;
      VL_TOT_ITEM := FQuery.Fields[3].AsCurrency;
    end;

    FRegC181.codigoProduto := FQuery.Fields[1].AsInteger;
    FRegC181.preenche;

    FRegC185.codigoProduto := FQuery.Fields[1].AsInteger;
    FRegC185.preenche;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC180.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo fiscal';
  FQuery.Fields[1].DisplayLabel := 'C�digo produto';
  FQuery.Fields[2].DisplayLabel := 'NCM';
  FQuery.Fields[3].DisplayLabel := 'Valor final Itens';
  FQuery.Fields[2].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDadosC180.SetParametros;
begin
  inherited;
end;

function TFonteDadosC180.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC181 }

procedure TFonteDadosC181.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrEPBlocoC.RegistroC181New) do
    begin
      CST_PIS   := get_CSTPIS(FQuery.Fields[0].text);
      CFOP      := FQuery.Fields[1].text;
      VL_ITEM   := FQuery.Fields[6].AsCurrency;
      VL_DESC   := FQuery.Fields[3].AsCurrency;
      VL_BC_PIS := FQuery.Fields[4].AsCurrency;
      ALIQ_PIS  := FQuery.Fields[2].AsCurrency;
      VL_PIS    := FQuery.Fields[5].AsCurrency;

      addStr(strNatOp,CFOP);
    end;

    FQuery.Next;

  end;
  
end;

procedure TFonteDadosC181.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CST PIS';
  FQuery.Fields[1].DisplayLabel := 'CFOP';
  FQuery.Fields[2].DisplayLabel := 'Percentual PIS';
  FQuery.Fields[3].DisplayLabel := 'Desconto';
  FQuery.Fields[4].DisplayLabel := 'Valor BC PIS';
  FQuery.Fields[5].DisplayLabel := 'Valor PIS';
  FQuery.Fields[6].DisplayLabel := 'Valor Item';

  FQuery.Fields[3].Required := False;
end;

procedure TFonteDadosC181.SetParametros;
begin
  inherited;
  FQuery.Params[2].AsInteger := self.codigoProduto;
end;

function TFonteDadosC181.ValidaRegistros: Boolean;
begin
  if FQuery.Fields[0].Text = '1' then
    if (FQuery.Fields[2].AsCurrency <> 0.65) and (FQuery.Fields[2].AsCurrency <> 1.65) then
      strErros.Add('Devem ser informadas al�quotas b�sicas (0,65 ou 1,65) para opera��es com CST do PIS = 01');

  if self.indIncidTributaria = '1' then
    if FQuery.Fields[0].Text = '1' then
      if FQuery.Fields[2].AsCurrency <> 1.65 then
        strErros.Add('Devem ser informadas al�quotas b�sicas (PIS 1.65) para opera��es com CST = 1, de acordo com o indicador de incid�ncia tribut�ria no per�odo');
end;

{ TFonteDadosC185 }

procedure TFonteDadosC185.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin
                                                          
    with (FAcbrEPBlocoC.RegistroC185New) do
    begin
      CST_COFINS   := get_CSTCOFINS(FQuery.Fields[0].text);
      CFOP         := FQuery.Fields[1].text;
      VL_ITEM      := FQuery.Fields[6].AsCurrency;
      VL_DESC      := FQuery.Fields[3].AsCurrency;
      VL_BC_COFINS := FQuery.Fields[4].AsCurrency;
      ALIQ_COFINS  := FQuery.Fields[2].AsCurrency;
      VL_COFINS    := FQuery.Fields[5].AsCurrency;
      addStr(strNatOp,CFOP);
    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC185.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CST COFINS';
  FQuery.Fields[1].DisplayLabel := 'CFOP';
  FQuery.Fields[2].DisplayLabel := 'Percentual COFINS';
  FQuery.Fields[3].DisplayLabel := 'Desconto';
  FQuery.Fields[4].DisplayLabel := 'Valor BC COFINS';
  FQuery.Fields[5].DisplayLabel := 'Valor COFINS';
  FQuery.Fields[6].DisplayLabel := 'Valor item';

  FQuery.Fields[3].Required := False;
  
end;

procedure TFonteDadosC185.SetParametros;
begin
  inherited;
  FQuery.Params[2].AsInteger := self.codigoProduto;
end;

function TFonteDadosC185.ValidaRegistros: Boolean;
begin
  if FQuery.Fields[0].Text = '1' then
    if (FQuery.Fields[2].AsCurrency <> 3) and (FQuery.Fields[2].AsCurrency <> 7.6) then
      strErros.Add('Devem ser informadas al�quotas b�sicas (3 ou 7,6) para opera��es com CST da COFINS = 01');

  if self.indIncidTributaria = '1' then
    if FQuery.Fields[0].Text = '1' then
      if FQuery.Fields[2].AsCurrency <> 7.6 then
        strErros.Add('Devem ser informadas al�quotas b�sicas (COFINS 7.6) para opera��es com CST = 1, de acordo com o indicador de incid�ncia tribut�ria no per�odo');
end;

{ TFonteDadosC190 }

constructor TFonteDadosC190.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TFonteDadosC190.destroy;
begin
  FreeAndNil(FRegC191);
  FreeAndNil(FRegC195);
  inherited;
end;

procedure TFonteDadosC190.inicializa;
var
  i:Integer;
begin

  FRegC191 := TFonteDadosC191.Create(self);
  FRegC191.Registro := 'C191';

  FRegC195 := TFonteDadosC195.Create(self);
  FRegC195.Registro := 'C195';

  for i := 0  to self.ComponentCount-1  do
  begin
  
    TFonteDados(self.Components[i]).Database := self.DataBase;
    TFonteDados(self.Components[i]).DataINI := self.FDataIni;
    TFonteDados(self.Components[i]).DataFIN := self.FDataFin;
    TFonteDados(self.Components[i]).DataIniEstoque := self.FDataIniEstoque;
    TFonteDados(self.Components[i]).DataFinEstoque := self.FDataFinEstoque;
    TFonteDados(self.Components[i]).strUnidade := self.strUnidade;
    TFonteDados(self.Components[i]).strItem := self.strItem;
    TFonteDados(self.Components[i]).strServico := self.strServico;
    TFonteDados(self.Components[i]).strNatOp := self.strNatOp;
    TFonteDados(self.Components[i]).strCodPart := self.strCodPart;
    TFonteDados(self.Components[i]).strErros := self.strErros;
    TFonteDados(self.Components[i]).strAdvertencia := self.strAdvertencia;
    TFonteDados(self.Components[i]).valor_total_debitos  := 0;
    TFonteDados(self.Components[i]).VL_TOT_CREDITOS_E110 := 0;
    TFonteDados(self.Components[i]).status := self.Fstatus;
    TFonteDados(self.components[i]).geraRegistroH := self.geraRegistroH;
    TFonteDados(self.Components[i]).software := self.software;
    TFonteDados(self.Components[i]).indPerfil := self.IndPerfil;

    if self.Components[i] is TFonteDadosBlocoC then
    begin
      TFonteDadosBlocoC(self.Components[i]).FAcbrEPBlocoC := Self.FAcbrEPBlocoC;
      TFonteDadosBlocoC(self.Components[i]).pPIS    := self.pPIS;
      TFonteDadosBlocoC(self.Components[i]).pCOFINS := self.pCOFINS;
    end;

  end;

end;

procedure TFonteDadosC190.preenche;
begin
  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not fquery.Eof do
  begin

    with (FAcbrEPBlocoC.RegistroC190New) do
    begin
      COD_MOD     := FQuery.Fields[0].Text;
      DT_REF_INI  := DataINI;
      DT_REF_FIN  := DataFIN;

      COD_ITEM    := FQuery.Fields[1].Text;
      addStr (strItem,COD_ITEM);

      COD_NCM     := FQuery.Fields[2].Text;
      VL_TOT_ITEM := FQuery.Fields[3].AsCurrency;
    end;

    FRegC191.codigoProduto := FQuery.Fields[1].AsInteger;
    FRegC191.preenche;

    FRegC195.codigoProduto := FQuery.Fields[1].AsInteger;
    FRegC195.preenche;

    FQuery.Next;

  end;

  {N�o devem ser inclu�dos na consolida��o do Registro C190 e registros filhos (C191 e C195) os documentos fiscais
  que n�o correspondam a aquisi��es com direito a cr�dito ou a devolu��es (devolu��es de vendas), bem como as notas
  fiscais eletr�nicas canceladas, as notas fiscais eletr�nicas denegadas ou de numera��o inutilizada e as notas fiscais
  referentes a transfer�ncia de mercadorias e produtos entre estabelecimentos da pessoa jur�dica, etc.}

end;

procedure TFonteDadosC190.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'C�digo fiscal';
  FQuery.Fields[1].DisplayLabel := 'C�digo produto';
  FQuery.Fields[2].DisplayLabel := 'NCM';
  FQuery.Fields[3].DisplayLabel := 'Total NF';

  FQuery.Fields[2].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDadosC190.SetParametros;
begin
  inherited;
end;

function TFonteDadosC190.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosC191 }

procedure TFonteDadosC191.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrEPBlocoC.RegistroC191New) do
    begin
      CST_PIS        := get_CSTPIS(FQuery.Fields[0].text);
      CFOP           := FQuery.Fields[1].AsInteger;
      VL_ITEM        := FQuery.Fields[6].AsCurrency;
      VL_DESC        := FQuery.Fields[3].AsCurrency;
      VL_BC_PIS      := FQuery.Fields[4].AsCurrency;
      ALIQ_PIS       := FQuery.Fields[2].AsCurrency;
      VL_PIS         := FQuery.Fields[5].AsCurrency;
      CNPJ_CPF_PART  := FQuery.Fields[7].Text;
      addStr(strNatOp,IntToStr(CFOP));
    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC191.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CST PIS';
  FQuery.Fields[1].DisplayLabel := 'CFOP';
  FQuery.Fields[2].DisplayLabel := 'Percentual PIS';
  FQuery.Fields[3].DisplayLabel := 'Desconto';
  FQuery.Fields[4].DisplayLabel := 'Valor BC PIS';
  FQuery.Fields[5].DisplayLabel := 'Valor PIS';
  FQuery.Fields[6].DisplayLabel := 'Valor item';
  FQuery.Fields[7].DisplayLabel := 'CNPJ';

  FQuery.Fields[3].Required := False;
  FQuery.Fields[7].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDadosC191.SetParametros;
begin
  inherited;
  FQuery.Params[2].AsInteger := self.codigoProduto;
end;

function TFonteDadosC191.ValidaRegistros: Boolean;
begin
  if not ValidaCNPJ(FQuery.Fields[7].Text) then
    if not ValidaCPF(FQuery.Fields[7].Text) then
      strErros.Add('CNPJ_CPF inv�lido no castro de fornecedores. CNPJ_CPF: '+fquery.Fields[7].Text);

  if not (FQuery.Fields[0].AsInteger in [50..66]) then
    if not (FQuery.Fields[0].AsInteger in [70..75]) then
      if not (FQuery.Fields[0].AsInteger in [98..99]) then
        strErros.Add('Para opera��es de entrada, o CST PIS deve ser preenchido com os valores de 50 a 66, 70 a 75, 98 ou 99');

end;

{ TFonteDadosC195 }

procedure TFonteDadosC195.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with (FAcbrEPBlocoC.RegistroC195New) do
    begin
      CST_COFINS     := get_CSTCOFINS(FQuery.Fields[0].Text);
      CFOP           := FQuery.Fields[1].AsInteger;
      VL_ITEM        := FQuery.Fields[6].AsCurrency;
      VL_DESC        := FQuery.Fields[3].AsCurrency;
      VL_BC_COFINS   := FQuery.Fields[4].AsCurrency;
      ALIQ_COFINS    := FQuery.Fields[2].AsCurrency;
      VL_COFINS      := FQuery.Fields[5].AsCurrency;
      CNPJ_CPF_PART  := FQuery.Fields[7].Text;
      addStr(strNatOp,IntToStr(CFOP));
    end;

    FQuery.Next;

  end;

end;

procedure TFonteDadosC195.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CST COFINS';
  FQuery.Fields[1].DisplayLabel := 'CFOP';
  FQuery.Fields[2].DisplayLabel := 'Percentual COFINS';
  FQuery.Fields[3].DisplayLabel := 'Desconto';
  FQuery.Fields[4].DisplayLabel := 'Valor BC COFINS';
  FQuery.Fields[5].DisplayLabel := 'Valor COFINS';
  FQuery.Fields[6].DisplayLabel := 'Valor item';
  FQuery.Fields[7].DisplayLabel := 'CNPJ';

  FQuery.Fields[3].Required := False;
  FQuery.Fields[7].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDadosC195.SetParametros;
begin
  inherited;
  FQuery.Params[2].AsInteger := self.codigoProduto;
end;

function TFonteDadosC195.ValidaRegistros: Boolean;
begin
  if not ValidaCNPJ(FQuery.Fields[7].Text) then
    if not ValidaCPF(FQuery.Fields[7].Text) then
      strErros.Add('CNPJ_CPF inv�lido no castro de fornecedores. CNPJ_CPF: '+fquery.Fields[7].Text);

  if not (FQuery.Fields[0].AsInteger in [50..66]) then
    if not (FQuery.Fields[0].AsInteger in [70..75]) then
      if not (FQuery.Fields[0].AsInteger in [98..99]) then
        strErros.Add('Para opera��es de entrada, o CST COFINS deve ser preenchido com os valores de 50 a 66, 70 a 75, 98 ou 99');

end;

{ TFonteDadosBlocoD }

procedure TFonteDadosBlocoD.SetParametros;
begin
  inherited;
end;

{ TFonteDadosD001 }

procedure TFonteDadosD001.preenche;
begin
  inherited;
  if (FAcbrEPBlocoD.RegistroD001.RegistroD010.Count <= 0) then
    FAcbrEPBlocoD.RegistroD001New.IND_MOV := imSemDados
  else
    FAcbrEPBlocoD.RegistroD001New.IND_MOV := imComDados;
end;

procedure TFonteDadosD001.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosD001.SetParametros;
begin
  inherited;
end;

function TFonteDadosD001.ValidaRegistros: Boolean;
begin
end;

{ TFonteDadosBlocoF }

procedure TFonteDadosBlocoF.SetParametros;
begin
  inherited;
end;

{ TFonteDadosF001 }

procedure TFonteDadosF001.preenche;
begin
  inherited;
  if (FAcbrEPBlocoF.RegistroF001.RegistroF010.Count <= 0) then
    FAcbrEPBlocoF.RegistroF001New.IND_MOV := imSemDados
  else
    FAcbrEPBlocoF.RegistroF001New.IND_MOV := imComDados;
end;

procedure TFonteDadosF001.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosF001.SetParametros;
begin
  inherited;
end;

function TFonteDadosF001.ValidaRegistros: Boolean;
begin
end;

{ TFonteDadosBlocoM }

procedure TFonteDadosBlocoM.SetParametros;
begin
  inherited;
end;

{ TFonteDadosM001 }

procedure TFonteDadosM001.preenche;
begin
  inherited;
  {if (FAcbrEPBlocoM.RegistroM001.RegistroM100.Count <= 0) then
    FAcbrEPBlocoM.RegistroM001New.IND_MOV := imSemDados
  else
    FAcbrEPBlocoM.RegistroM001New.IND_MOV := imComDados;}

  FAcbrEPBlocoM.RegistroM001New.IND_MOV := imComDados;
end;

procedure TFonteDadosM001.SetCampos;
begin
  inherited;
end;

procedure TFonteDadosM001.SetParametros;
begin
  inherited;
end;

function TFonteDadosM001.ValidaRegistros: Boolean;
begin
end;

{ TFonteDadosBloco1 }

procedure TFonteDadosBloco1.SetParametros;
begin
  inherited;
end;

{ TFonteDados1001 }

procedure TFonteDados1001.preenche;
begin
  inherited;
  if (FAcbrEPBloco1.Registro1001.Registro1010.Count <= 0) then
    FAcbrEPBloco1.Registro1001New.IND_MOV := imSemDados
  else
    FAcbrEPBloco1.Registro1001New.IND_MOV := imComDados;
end;

procedure TFonteDados1001.SetCampos;
begin
  inherited;
end;

procedure TFonteDados1001.SetParametros;
begin
  inherited;
end;

function TFonteDados1001.ValidaRegistros: Boolean;
begin
end;

{ TFonteDadosF010 }

procedure TFonteDadosF010.preenche;
begin
  inherited;
  
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    with FAcbrEPBlocoF.RegistroF010New do
      CNPJ := FQuery.Fields[0].Text;
      
    FQuery.Next;
  end;

end;

procedure TFonteDadosF010.SetCampos;
begin
  inherited;
  FQuery.Fields[0].DisplayLabel := 'CNPJ';
  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
end;

procedure TFonteDadosF010.SetParametros;
begin
end;

function TFonteDadosF010.ValidaRegistros: Boolean;
begin
  if not ValidaCNPJ(FQuery.Fields[0].text) then
    strErros.Add('CNPJ inv�lido no cadastro da empresa');
end;

{ TFonteDadosM200 }

procedure TFonteDadosM200.preenche;
begin

  inherited;
  //if not AbreQuery then Exit;
  //if teveErros then Exit;

  with (FAcbrEPBlocoM.RegistroM200New) do
  begin
    VL_TOT_CONT_NC_PER := 0;
    VL_TOT_CRED_DESC := 0;
    VL_TOT_CRED_DESC_ANT := 0;
    VL_TOT_CONT_NC_DEV := 0;
    VL_RET_NC := 0;
    VL_OUT_DED_NC := 0;
    VL_CONT_NC_REC := 0;
    VL_TOT_CONT_CUM_PER := 0;
    VL_RET_CUM := 0;
    VL_OUT_DED_CUM := 0;
    VL_CONT_CUM_REC := 0;
    VL_TOT_CONT_REC := 0;
  end;

end;

procedure TFonteDadosM200.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosM200.SetParametros;
begin
  inherited;

end;

function TFonteDadosM200.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosM600 }

procedure TFonteDadosM600.preenche;
begin

  inherited;
  //if not AbreQuery then Exit;
  //if teveErros then Exit;

  with (FAcbrEPBlocoM.RegistroM600New) do
  begin
    VL_TOT_CONT_NC_PER := 0;
    VL_TOT_CRED_DESC := 0;
    VL_TOT_CRED_DESC_ANT := 0;
    VL_TOT_CONT_NC_DEV := 0;
    VL_RET_NC := 0;
    VL_OUT_DED_NC := 0;
    VL_CONT_NC_REC := 0;
    VL_TOT_CONT_CUM_PER := 0;
    VL_RET_CUM := 0;
    VL_OUT_DED_CUM := 0;
    VL_CONT_CUM_REC := 0;
    VL_TOT_CONT_REC := 0;
  end;

end;

procedure TFonteDadosM600.SetCampos;
begin
  inherited;

end;

procedure TFonteDadosM600.SetParametros;
begin
  inherited;

end;

function TFonteDadosM600.ValidaRegistros: Boolean;
begin

end;

{ TFonteDadosM210 }

procedure TFonteDadosM210.preenche;
var
  cod_cont : string;
begin
  inherited;

  //if not AbreQuery then Exit;
  //if teveErros then Exit;

  (*while not FQuery.Eof do
  begin

    with FAcbrEPBlocoM.RegistroM210New do
    begin

      if (FQuery.Fields[0].Text = '1') then {CST_PIS}
      begin

        if (FQuery.Fields[1].AsCurrency = 1.65) then {ALIQ_PIS}
        begin

          if (indIncidTrib = '1') or (indIncidTrib = '3') then
            COD_CONT := ccNaoAcumAliqBasica; {01}

        end
        else if FQuery.Fields[1].AsCurrency = 0.65 then {ALIQ_PIS}
        begin

          if (indIncidTrib = '2') or (indIncidTrib = '3') then
            COD_CONT := ccAcumAliqBasica; {51}

        end;

      end
      else if FQuery.Fields[0].Text = '2' then {CST_PIS}
      begin

        if (indIncidTrib = '1') or (indIncidTrib = '3') then
          COD_CONT := ccNaoAcumAliqDiferenciada {2}
        else
          COD_CONT := ccAcumAliqDiferenciada; {52}


      end;

    end;
    
  end;*)

end;

procedure TFonteDadosM210.SetCampos;
begin
  inherited;

  FQuery.Fields[0].DisplayLabel := 'CST PIS';
  FQuery.Fields[1].DisplayLabel := 'PERCENTUAL PIS';
  FQuery.Fields[2].DisplayLabel := 'BC_PIS';

end;

procedure TFonteDadosM210.SetParametros;
begin
  inherited;

end;

function TFonteDadosM210.ValidaRegistros: Boolean;
begin

end;

end.

