object FMostraBarraProgresso: TFMostraBarraProgresso
  Left = 506
  Top = 320
  Width = 571
  Height = 183
  BorderIcons = []
  Caption = 'Progresso'
  Color = 10643006
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object BarradeProgresso: TGauge
    Left = 3
    Top = 27
    Width = 546
    Height = 27
    BackColor = clActiveCaption
    BorderStyle = bsNone
    ForeColor = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Progress = 0
  end
  object Lbmensagem: TLabel
    Left = 0
    Top = 2
    Width = 550
    Height = 20
    AutoSize = False
    Caption = '(    ) Gerando Venda dos Produtos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object BarradeProgresso2: TGauge
    Left = 3
    Top = 83
    Width = 546
    Height = 27
    BackColor = clActiveCaption
    BorderStyle = bsNone
    ForeColor = 4963365
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Progress = 0
    Visible = False
  end
  object Lbmensagem2: TLabel
    Left = 0
    Top = 58
    Width = 550
    Height = 20
    AutoSize = False
    Caption = '(    ) Gerando Venda dos Produtos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    Visible = False
  end
  object btcancelar: TButton
    Left = 473
    Top = 117
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 0
    OnClick = btcancelarClick
  end
end
