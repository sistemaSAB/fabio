unit UCATEGORIACONTATOCEL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCATEGORIACONTATOCEL,
  jpeg;

type
  TFCATEGORIACONTATOCEL = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbNome: TLabel;
    EdtNome: TEdit;
//DECLARA COMPONENTES

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjCATEGORIACONTATOCEL:TObjCATEGORIACONTATOCEL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCATEGORIACONTATOCEL: TFCATEGORIACONTATOCEL;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCATEGORIACONTATOCEL.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjCATEGORIACONTATOCEL do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCATEGORIACONTATOCEL.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCATEGORIACONTATOCEL do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCATEGORIACONTATOCEL.TabelaParaControles: Boolean;
begin
     If (Self.ObjCATEGORIACONTATOCEL.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCATEGORIACONTATOCEL.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjCATEGORIACONTATOCEL:=TObjCATEGORIACONTATOCEL.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFCATEGORIACONTATOCEL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCATEGORIACONTATOCEL=Nil)
     Then exit;

If (Self.ObjCATEGORIACONTATOCEL.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjCATEGORIACONTATOCEL.free;
end;

procedure TFCATEGORIACONTATOCEL.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCATEGORIACONTATOCEL.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCATEGORIACONTATOCEL.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjCATEGORIACONTATOCEL.status:=dsInsert;
     Guia.pageindex:=0;
     edtNome.setfocus;

end;


procedure TFCATEGORIACONTATOCEL.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCATEGORIACONTATOCEL.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjCATEGORIACONTATOCEL.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNome.setfocus;
                
          End;


end;

procedure TFCATEGORIACONTATOCEL.btgravarClick(Sender: TObject);
begin

     If Self.ObjCATEGORIACONTATOCEL.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCATEGORIACONTATOCEL.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCATEGORIACONTATOCEL.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCATEGORIACONTATOCEL.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjCATEGORIACONTATOCEL.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCATEGORIACONTATOCEL.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCATEGORIACONTATOCEL.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCATEGORIACONTATOCEL.btcancelarClick(Sender: TObject);
begin
     Self.ObjCATEGORIACONTATOCEL.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCATEGORIACONTATOCEL.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCATEGORIACONTATOCEL.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCATEGORIACONTATOCEL.Get_pesquisa,Self.ObjCATEGORIACONTATOCEL.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCATEGORIACONTATOCEL.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCATEGORIACONTATOCEL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjCATEGORIACONTATOCEL.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCATEGORIACONTATOCEL.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCATEGORIACONTATOCEL.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjCATEGORIACONTATOCEL.OBJETO.Get_Pesquisa,Self.ObjCATEGORIACONTATOCEL.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjCATEGORIACONTATOCEL.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjCATEGORIACONTATOCEL.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname
(Self.ObjCATEGORIACONTATOCEL.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
