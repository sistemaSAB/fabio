unit UobjLIGACOESFATURACEL;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJFATURACEL
,UOBJACESSOCONTRATOCEL
,UOBJTELEFONECONTATOCEL
;
//USES_INTERFACE





Type
   TObjLIGACOESFATURACEL=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               Fatura:TOBJFATURACEL;
               Acesso:TOBJACESSOCONTRATOCEL;
               TelefoneContato:TOBJTELEFONECONTATOCEL;
//CODIFICA VARIAVEIS PUBLICAS




                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaDados:Boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Telefone(parametro: string);
                Function Get_Telefone: string;
                Procedure Submit_Data(parametro: string);
                Function Get_Data: string;
                Procedure Submit_hora(parametro: string);
                Function Get_hora: string;
                Procedure Submit_duracao(parametro: string);
                Function Get_duracao: string;
                Procedure Submit_valor(parametro: string);
                Function Get_valor: string;

                Procedure Submit_tipo(parametro: string);
                Function Get_tipo: string;

                procedure EdtFaturaExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFaturaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtAcessoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtAcessoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtTelefoneContatoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTelefoneContatoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure RetornaClientesNaoCadastrados(Pfatura,PAcesso:string);
                Procedure CadastraTelefone(Ptelefone:string;PtelefoneContato:string);
                procedure AcertaDependencias;

//CODIFICA DECLARA GETSESUBMITS




         Private
               Objquery:Tibquery;
               Objquerypesquisa:tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Telefone:string;
               Data:string;
               hora:string;
               duracao:string;
               valor:string;
               tipo:string;
//CODIFICA VARIAVEIS PRIVADAS








               ParametroPesquisa:TStringList;


                Procedure ObjetoparaTabela;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UFATURACEL, UACESSOCONTRATOCEL, UTELEFONECONTATOCEL;





Function  TObjLIGACOESFATURACEL.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('Fatura').asstring<>'')
        Then Begin
                 If (Self.Fatura.LocalizaCodigo(FieldByName('Fatura').asstring)=False)
                 Then Begin
                          Messagedlg('Fatura N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Fatura.TabelaparaObjeto;
        End;
        If(FieldByName('Acesso').asstring<>'')
        Then Begin
                 If (Self.Acesso.LocalizaCodigo(FieldByName('Acesso').asstring)=False)
                 Then Begin
                          Messagedlg('Acesso N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Acesso.TabelaparaObjeto;
        End;
        If(FieldByName('TelefoneContato').asstring<>'')
        Then Begin
                 If (Self.TelefoneContato.LocalizaCodigo(FieldByName('TelefoneContato').asstring)=False)
                 Then Begin
                          Messagedlg('Contato-Telefone N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TelefoneContato.TabelaparaObjeto;
        End;
        Self.Telefone:=fieldbyname('Telefone').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.hora:=fieldbyname('hora').asstring;
        Self.duracao:=fieldbyname('duracao').asstring;
        Self.valor:=fieldbyname('valor').asstring;
        Self.tipo:=fieldbyname('tipo').asstring;
//CODIFICA TABELAPARAOBJETO









        result:=True;
     End;
end;


Procedure TObjLIGACOESFATURACEL.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Fatura').asstring:=Self.Fatura.GET_CODIGO;
        ParamByName('Acesso').asstring:=Self.Acesso.GET_CODIGO;
        ParamByName('TelefoneContato').asstring:=Self.TelefoneContato.GET_CODIGO;
        ParamByName('Telefone').asstring:=Self.Telefone;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('hora').asstring:=Self.hora;
        ParamByName('duracao').asstring:=Self.duracao;
        ParamByName('valor').asstring:=virgulaparaponto(Self.valor);
        ParamByName('tipo').asstring:=Self.tipo;
//CODIFICA OBJETOPARATABELA









  End;
End;

//***********************************************************************

function TObjLIGACOESFATURACEL.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjLIGACOESFATURACEL.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Fatura.ZerarTabela;
        Acesso.ZerarTabela;
        TelefoneContato.ZerarTabela;
        Telefone:='';
        Data:='';
        hora:='';
        duracao:='';
        valor:='';
        tipo:='';
//CODIFICA ZERARTABELA









     End;
end;

Function TObjLIGACOESFATURACEL.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Fatura.get_Codigo='')
      Then Mensagem:=mensagem+'/Fatura';
      If (Acesso.get_Codigo='')
      Then Mensagem:=mensagem+'/Acesso';
      If (Telefone='')
      Then Mensagem:=mensagem+'/Telefone';

      If (Data='')
      Then Mensagem:=mensagem+'/Data';

      If (hora='')
      Then Mensagem:=mensagem+'/Hora';

      If (duracao='')
      Then Mensagem:=mensagem+'/Dura��o';

      If (valor='')
      Then Self.Valor:='0';

      If (valor='')
      Then Self.Valor:='L';


      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjLIGACOESFATURACEL.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Fatura.LocalizaCodigo(Self.Fatura.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Fatura n�o Encontrado!';
      If (Self.Acesso.LocalizaCodigo(Self.Acesso.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Acesso n�o Encontrado!';

      if (Self.TelefoneContato.Get_CODIGO<>'')
      then begin
                If (Self.TelefoneContato.LocalizaCodigo(Self.TelefoneContato.Get_CODIGO)=False)
                Then Mensagem:=mensagem+'/ Contato-Telefone n�o Encontrado!';
      End;
      
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjLIGACOESFATURACEL.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Fatura.Get_Codigo<>'')
        Then Strtoint(Self.Fatura.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Fatura';
     End;
     try
        If (Self.Acesso.Get_Codigo<>'')
        Then Strtoint(Self.Acesso.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Acesso';
     End;
     try
        If (Self.TelefoneContato.Get_Codigo<>'')
        Then Strtoint(Self.TelefoneContato.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Contato-Telefone';
     End;
     try
        Strtoint(Self.duracao);
     Except
           Mensagem:=mensagem+'/Dura��o';
     End;
     try
        Strtofloat(Self.valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjLIGACOESFATURACEL.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;
     try
        Strtotime(Self.hora);
     Except
           Mensagem:=mensagem+'/Hora';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjLIGACOESFATURACEL.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';

        if (Self.tipo<>'L') and (Self.tipo<>'T')
        Then Self.tipo:='L';

//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;


function TObjLIGACOESFATURACEL.LocalizaDados: Boolean;
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Fatura,Acesso,TelefoneContato,Telefone,Data,hora');
           SQL.ADD(' ,duracao,valor,tipo');
           SQL.ADD(' from  TabLigacoesFaturaCel');
           SQL.ADD(' WHERE Telefone='#39+Self.Telefone+#39);
           SQL.ADD(' and  Data='+#39+formatdatetime('mm/dd/yyyy',strtodate(Self.Data))+#39);
           SQL.ADD(' and hora='+#39+formatdatetime('hh:mm:ss',strtotime(Self.hora))+#39);
           SQL.ADD(' and duracao='+self.duracao);
           SQL.ADD(' and valor='+VIRGULAPARAPONTO(self.valor));
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;


function TObjLIGACOESFATURACEL.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro LIGACOESFATURACEL vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Fatura,Acesso,TelefoneContato,Telefone,Data,hora');
           SQL.ADD(' ,duracao,valor,tipo');
           SQL.ADD(' from  TabLigacoesFaturaCel');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjLIGACOESFATURACEL.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjLIGACOESFATURACEL.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjLIGACOESFATURACEL.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.Objquerypesquisa;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Fatura:=TOBJFATURACEL.create;
        Self.Acesso:=TOBJACESSOCONTRATOCEL.create;
        Self.TelefoneContato:=TOBJTELEFONECONTATOCEL.create;
//CODIFICA CRIACAO DE OBJETOS




        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabLigacoesFaturaCel(CODIGO,Fatura,Acesso');
                InsertSQL.add(' ,TelefoneContato,Telefone,Data,hora,duracao,valor,tipo)');
                InsertSQL.add('values (:CODIGO,:Fatura,:Acesso,:TelefoneContato,:Telefone');
                InsertSQL.add(' ,:Data,:hora,:duracao,:valor,:tipo)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabLigacoesFaturaCel set CODIGO=:CODIGO,Fatura=:Fatura');
                ModifySQL.add(',Acesso=:Acesso,TelefoneContato=:TelefoneContato,Telefone=:Telefone');
                ModifySQL.add(',Data=:Data,hora=:hora,duracao=:duracao,valor=:valor,tipo=:tipo');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabLigacoesFaturaCel where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjLIGACOESFATURACEL.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjLIGACOESFATURACEL.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabLIGACOESFATURACEL');
     Result:=Self.ParametroPesquisa;
end;

function TObjLIGACOESFATURACEL.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de LIGACOESFATURACEL ';
end;


function TObjLIGACOESFATURACEL.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENLIGACOESFATURACEL,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENLIGACOESFATURACEL,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjLIGACOESFATURACEL.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Fatura.FREE;
Self.Acesso.FREE;
Self.TelefoneContato.FREE;
//CODIFICA DESTRUICAO DE OBJETOS




end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjLIGACOESFATURACEL.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjLIGACOESFATURACEL.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjLigacoesFaturaCel.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjLigacoesFaturaCel.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjLigacoesFaturaCel.Submit_Telefone(parametro: string);
begin
        Self.Telefone:=Parametro;
end;
function TObjLigacoesFaturaCel.Get_Telefone: string;
begin
        Result:=Self.Telefone;
end;
procedure TObjLigacoesFaturaCel.Submit_Data(parametro: string);
begin
        Self.Data:=Parametro;
end;
function TObjLigacoesFaturaCel.Get_Data: string;
begin
        Result:=Self.Data;
end;
procedure TObjLigacoesFaturaCel.Submit_hora(parametro: string);
begin
        Self.hora:=Parametro;
end;
function TObjLigacoesFaturaCel.Get_hora: string;
begin
        Result:=Self.hora;
end;
procedure TObjLigacoesFaturaCel.Submit_duracao(parametro: string);
begin
        Self.duracao:=Parametro;
end;
function TObjLigacoesFaturaCel.Get_duracao: string;
begin
        Result:=Self.duracao;
end;
procedure TObjLigacoesFaturaCel.Submit_valor(parametro: string);
begin
        Self.valor:=Parametro;
end;
function TObjLigacoesFaturaCel.Get_valor: string;
begin
        Result:=Self.valor;
end;
//CODIFICA GETSESUBMITS
procedure TObjLigacoesFaturaCel.Submit_tipo(parametro: string);
begin
        Self.tipo:=Parametro;
end;
function TObjLigacoesFaturaCel.Get_tipo: string;
begin
        Result:=Self.tipo;
end;


procedure TObjLIGACOESFATURACEL.EdtFaturaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Fatura.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Fatura.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Fatura.Contrato.Get_Nome+'/'+Self.Fatura.Get_vencimento;
End;
procedure TObjLIGACOESFATURACEL.EdtFaturaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFATURACEL:TFFATURACEL;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFATURACEL:=TFFATURACEL.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Fatura.Get_Pesquisa,Self.Fatura.Get_TituloPesquisa,FFaturaCel)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fatura.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Fatura.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Fatura.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFATURACEL);
     End;
end;
procedure TObjLIGACOESFATURACEL.EdtAcessoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Acesso.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Acesso.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Acesso.Get_Telefone;
End;
procedure TObjLIGACOESFATURACEL.EdtAcessoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FACESSOCONTRATOCEL:TFACESSOCONTRATOCEL;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FACESSOCONTRATOCEL:=TFACESSOCONTRATOCEL.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Acesso.Get_Pesquisa,Self.Acesso.Get_TituloPesquisa,FACESSOCONTRATOCEL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Acesso.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Acesso.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Acesso.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FACESSOCONTRATOCEL);
     End;
end;
procedure TObjLIGACOESFATURACEL.EdtTelefoneContatoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TelefoneContato.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TelefoneContato.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.TelefoneContato.GET_NOME;
End;
procedure TObjLIGACOESFATURACEL.EdtTelefoneContatoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FTELEFONECONTATOCEL:TFTELEFONECONTATOCEL;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FTELEFONECONTATOCEL:=TFTELEFONECONTATOCEL.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TelefoneContato.Get_Pesquisa,Self.TelefoneContato.Get_TituloPesquisa,FTELEFONECONTATOCEL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TelefoneContato.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.TelefoneContato.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TelefoneContato.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTELEFONECONTATOCEL);
     End;
end;
//CODIFICA EXITONKEYDOWN



procedure TObjLIGACOESFATURACEL.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJLIGACOESFATURACEL';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;




procedure TObjLIGACOESFATURACEL.RetornaClientesNaoCadastrados(Pfatura,
  PAcesso: string);
begin
     if (Pfatura='')
     Then Begin
               Mensagemerro('Escolha uma fatura');
               exit;
     end;

     if (PAcesso='')
     Then Begin
               Mensagemerro('Escolha um Acesso');
               exit;
     end;

     With Self.Objquerypesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select distinct(telefone) from tabligacoesfaturacel where fatura='+pfatura+' and acesso='+pacesso+' and TelefoneContato is null');
          open;
     End;
end;

procedure TObjLIGACOESFATURACEL.CadastraTelefone(Ptelefone: string;
  PtelefoneContato: string);
begin
     With Self.Objquery do
     begin
          close;
          sql.clear;
          sql.add('update tabligacoesfaturacel set telefonecontato='+Ptelefonecontato+' where telefone='+#39+ptelefone+#39);
          sql.add('and telefonecontato is null');
          Try
              ExecSQL;
              FDataModulo.IBTransaction.CommitRetaining;
          Except
                Mensagemerro('N�o foi poss�vel cadastrar o telefone '+Ptelefone+' nas liga��es de fatura');
                exit;
          End;

     End;
end;

procedure TObjLIGACOESFATURACEL.AcertaDependencias;
begin
     With Self.Objquerypesquisa do
     begin
          close;
          sql.clear;
          sql.add('Select distinct(telefone) from  tabligacoesfaturacel  where telefonecontato is null');
          open;

          While not(eof) do
          Begin

               if (Self.TelefoneContato.Localiza_Telefone(fieldbyname('telefone').asstring)=true)
               Then Begin
                         Self.TelefoneContato.ZerarTabela;
                         Self.TelefoneContato.TabelaparaObjeto;

                         self.Objquery.close;
                         self.Objquery.sql.clear;
                         self.Objquery.sql.add('update tabligacoesfaturacel set telefonecontato='+Self.telefonecontato.get_codigo);
                         self.Objquery.sql.add('where telefone='+#39+fieldbyname('telefone').asstring+#39+'  and telefonecontato is null');
                         Try
                            self.Objquery.ExecSQL;
                            FDataModulo.IBTransaction.CommitRetaining;

                         Except
                            Mensagemerro('Erro na tentativa de atualizar as liga��es para o telefone '+Fieldbyname('telefone').asstring);
                            exit;
                         End;
               End;
              next;
          End;
          close;
     End;
end;

end.



