unit UTELEFONECONTATOCEL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjTELEFONECONTATOCEL,
  jpeg;

type
  TFTELEFONECONTATOCEL = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbNome: TLabel;
    EdtNome: TEdit;
    LbContato: TLabel;
    EdtContato: TEdit;
    LbNomeContato: TLabel;
    LbTelefone: TLabel;
    EdtTelefone: TEdit;
    LbObservacao: TLabel;
    memoobservacao: TMemo;
//DECLARA COMPONENTES
    procedure edtContatoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtContatoExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure memoobservacaoKeyPress(Sender: TObject; var Key: Char);
  private
         ObjTELEFONECONTATOCEL:TObjTELEFONECONTATOCEL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FTELEFONECONTATOCEL: TFTELEFONECONTATOCEL;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFTELEFONECONTATOCEL.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjTELEFONECONTATOCEL do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
        Contato.Submit_codigo(edtContato.text);
        Submit_Telefone(edtTelefone.text);
        Submit_Observacao(memoObservacao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFTELEFONECONTATOCEL.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjTELEFONECONTATOCEL do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtContato.text:=Contato.Get_codigo;
        EdtTelefone.text:=Get_Telefone;
        memoObservacao.text:=Get_Observacao;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFTELEFONECONTATOCEL.TabelaParaControles: Boolean;
begin
     If (Self.ObjTELEFONECONTATOCEL.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFTELEFONECONTATOCEL.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjTELEFONECONTATOCEL:=TObjTELEFONECONTATOCEL.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFTELEFONECONTATOCEL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjTELEFONECONTATOCEL=Nil)
     Then exit;

If (Self.ObjTELEFONECONTATOCEL.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjTELEFONECONTATOCEL.free;
end;

procedure TFTELEFONECONTATOCEL.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFTELEFONECONTATOCEL.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjTELEFONECONTATOCEL.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjTELEFONECONTATOCEL.status:=dsInsert;
     Guia.pageindex:=0;
     edtNome.setfocus;

end;


procedure TFTELEFONECONTATOCEL.btalterarClick(Sender: TObject);
begin
    If (Self.ObjTELEFONECONTATOCEL.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjTELEFONECONTATOCEL.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNome.setfocus;
                
          End;


end;

procedure TFTELEFONECONTATOCEL.btgravarClick(Sender: TObject);
begin

     If Self.ObjTELEFONECONTATOCEL.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjTELEFONECONTATOCEL.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjTELEFONECONTATOCEL.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFTELEFONECONTATOCEL.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjTELEFONECONTATOCEL.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjTELEFONECONTATOCEL.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjTELEFONECONTATOCEL.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFTELEFONECONTATOCEL.btcancelarClick(Sender: TObject);
begin
     Self.ObjTELEFONECONTATOCEL.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFTELEFONECONTATOCEL.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFTELEFONECONTATOCEL.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjTELEFONECONTATOCEL.Get_pesquisa,Self.ObjTELEFONECONTATOCEL.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjTELEFONECONTATOCEL.status<>dsinactive
                                  then exit;

                                  If (Self.ObjTELEFONECONTATOCEL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjTELEFONECONTATOCEL.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFTELEFONECONTATOCEL.LimpaLabels;
begin
//LIMPA LABELS
    lbnomecontato.caption:='';
end;

procedure TFTELEFONECONTATOCEL.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
procedure TFTELEFONECONTATOCEL.edtContatoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjTELEFONECONTATOCEL.edtContatokeydown(sender,key,shift,lbnomeContato);
end;
 
procedure TFTELEFONECONTATOCEL.edtContatoExit(Sender: TObject);
begin
    ObjTELEFONECONTATOCEL.edtContatoExit(sender,lbnomeContato);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFTELEFONECONTATOCEL.memoobservacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     then Memoobservacao.SetFocus;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjTELEFONECONTATOCEL.OBJETO.Get_Pesquisa,Self.ObjTELEFONECONTATOCEL.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjTELEFONECONTATOCEL.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjTELEFONECONTATOCEL.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjTELEFONECONTATOCEL.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
