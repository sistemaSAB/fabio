unit UCONTATOCEL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjTelefoneCONTATOCEL,
  jpeg, Grids, DBGrids;

type
  TFCONTATOCEL = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbNome: TLabel;
    EdtNome: TEdit;
    LbCategoria: TLabel;
    EdtCategoria: TEdit;
    LbNomeCategoria: TLabel;
    DBGrid: TDBGrid;
    PanelTelefones: TPanel;
    btgravar_cel: TBitBtn;
    btexcluir_cel: TBitBtn;
    edtnome_cel: TEdit;
    edttelefone: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtcodigo_cel: TEdit;
    btcancelar_cel: TBitBtn;

//DECLARA COMPONENTES
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtCategoriaExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btgravar_celClick(Sender: TObject);
    procedure btexcluir_celClick(Sender: TObject);
    procedure DBGridDblClick(Sender: TObject);
    procedure btcancelar_celClick(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
  private
         ObjTelefoneCONTATOCEL:TObjTelefoneCONTATOCEL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         Procedure ResgataTelefones;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCONTATOCEL: TFCONTATOCEL;


implementation

uses UessencialGlobal, Upesquisa;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCONTATOCEL.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjTelefoneCONTATOCEL.Contato do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Nome(edtNome.text);
        Categoria.Submit_codigo(edtCategoria.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFCONTATOCEL.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjTelefoneCONTATOCEL.Contato do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtNome.text:=Get_Nome;
        EdtCategoria.text:=Categoria.Get_codigo;
        LbNomeCategoria.caption:=Categoria.Get_nome;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCONTATOCEL.TabelaParaControles: Boolean;
begin
     If (Self.ObjTelefoneCONTATOCEL.Contato.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFCONTATOCEL.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjTelefoneCONTATOCEL:=TObjTelefoneCONTATOCEL.create;
        DBGrid.DataSource:=Self.ObjTelefoneCONTATOCEL.ObjDatasource;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);
end;

procedure TFCONTATOCEL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjTelefoneCONTATOCEL=Nil)
     Then exit;

If (Self.ObjTelefoneCONTATOCEL.Contato.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjTelefoneCONTATOCEL.free;
end;

procedure TFCONTATOCEL.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFCONTATOCEL.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjCONTATOCEL.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjTelefoneCONTATOCEL.Contato.status:=dsInsert;
     Guia.pageindex:=0;
     edtNome.setfocus;

end;


procedure TFCONTATOCEL.btalterarClick(Sender: TObject);
begin
    If (Self.ObjTelefoneCONTATOCEL.Contato.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjTelefoneCONTATOCEL.Contato.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtNome.setfocus;
                
          End;


end;

procedure TFCONTATOCEL.btgravarClick(Sender: TObject);
begin

     If Self.ObjTelefoneCONTATOCEL.Contato.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjTelefoneCONTATOCEL.Contato.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjTelefoneCONTATOCEL.Contato.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCONTATOCEL.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjTelefoneCONTATOCEL.Contato.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjTelefoneCONTATOCEL.Contato.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjTelefoneCONTATOCEL.Contato.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFCONTATOCEL.btcancelarClick(Sender: TObject);
begin
     Self.ObjTelefoneCONTATOCEL.Contato.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCONTATOCEL.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFCONTATOCEL.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjTelefoneCONTATOCEL.Contato.Get_pesquisa,Self.ObjTelefoneCONTATOCEL.Contato.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjTelefoneCONTATOCEL.Contato.status<>dsinactive
                                  then exit;

                                  If (Self.ObjTelefoneCONTATOCEL.Contato.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjTelefoneCONTATOCEL.Contato.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFCONTATOCEL.LimpaLabels;
begin
//LIMPA LABELS
   lbnomecategoria.caption:='';
end;

procedure TFCONTATOCEL.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
procedure TFCONTATOCEL.edtCategoriaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjTelefoneCONTATOCEL.Contato.edtCategoriakeydown(sender,key,shift,lbnomeCategoria);
end;
 
procedure TFCONTATOCEL.edtCategoriaExit(Sender: TObject);
begin
    ObjTelefoneCONTATOCEL.Contato.edtCategoriaExit(sender,lbnomeCategoria);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFCONTATOCEL.btgravar_celClick(Sender: TObject);
begin
     With Self.ObjTelefoneCONTATOCEL do
     begin
          ZerarTabela;
          if (EdtCODIGO_cel.text='') or (edtcodigo_cel.text='0')
          then Begin
                    EdtCODIGO_Cel.text:='0';
                    status:=dsinsert;
          End
          else BEgin
                    if (LocalizaCodigo(edtcodigo_cel.Text)=False)
                    Then Begin
                              Mensagemerro('Registro n�o encontrado para ser editado');
                              exit;
                    End;
                    TabelaparaObjeto;
                    status:=dsedit;
          End;
          Submit_CODIGO(edtcodigo_cel.text);
          Submit_Nome(edtnome_cel.Text);
          Submit_Telefone(edttelefone.Text);
          Contato.Submit_CODIGO(EdtCODIGO.Text);

          if (salvar(true)=False)
          Then exit;

          limpaedit(PanelTelefones);
          edtnome_cel.SetFocus;

          Self.ResgataTelefones;
     End;
end;

procedure TFCONTATOCEL.ResgataTelefones;
begin
     Self.ObjTelefoneCONTATOCEL.REsgataTelefones(edtcodigo.text);


end;

procedure TFCONTATOCEL.btexcluir_celClick(Sender: TObject);
begin
     if (DBGrid.DataSource.DataSet.Active=False)
     then exit;

     if (DBGrid.DataSource.DataSet.RecordCount=0)
     then exit;

     if (MensagemPergunta('Certeza que deseja excluir esse telefone?')=mrno)
     Then exit;

     ObjTelefoneCONTATOCEL.Exclui(DBGrid.DataSource.DataSet.fieldbyname('codigo').asstring,true);

     Self.ResgataTelefones;
     
end;

procedure TFCONTATOCEL.DBGridDblClick(Sender: TObject);
begin
     if (DBGrid.DataSource.DataSet.Active=False)
     then exit;

     if (DBGrid.DataSource.DataSet.RecordCount=0)
     then exit;

     if (Self.ObjTelefoneCONTATOCEL.LocalizaCodigo(DBGrid.DataSource.DataSet.fieldbyname('codigo').AsString)=false)
     then exit;

     Self.ObjTelefoneCONTATOCEL.TabelaparaObjeto;

     edtcodigo_cel.Text:=Self.ObjTelefoneCONTATOCEL.get_codigo;
     edtnome_cel.Text:=Self.ObjTelefoneCONTATOCEL.Get_Nome;
     edttelefone.Text:=Self.ObjTelefoneCONTATOCEL.Get_Telefone;
     edtnome_cel.SetFocus;
     

end;

procedure TFCONTATOCEL.btcancelar_celClick(Sender: TObject);
begin
     limpaedit(PanelTelefones);
end;

procedure TFCONTATOCEL.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if NewTab=1
     Then Begin
               if (edtcodigo.Text='')
               then begin
                        AllowChange:=False;
                        exit;
               End;

               if (self.ObjTelefoneCONTATOCEL.Contato.Status=dsinsert)
               then Begin
                         AllowChange:=False;
                         exit;
               End;

               Self.ResgataTelefones;

               limpaedit(PanelTelefones);
               habilita_campos(PanelTelefones);
               edtcodigo_cel.Enabled:=false;
     End;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjTelefoneCONTATOCEL.Contato.OBJETO.Get_Pesquisa,Self.ObjTelefoneCONTATOCEL.Contato.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjTelefoneCONTATOCEL.Contato.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjTelefoneCONTATOCEL.Contato.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjTelefoneCONTATOCEL.Contato.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
