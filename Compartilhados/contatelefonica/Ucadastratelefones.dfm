object FcadastraTelefones: TFcadastraTelefones
  Left = 195
  Top = 208
  Width = 870
  Height = 500
  Caption = 'Cadastramento de Telefones de Fatura'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid: TDBGrid
    Left = 0
    Top = 86
    Width = 862
    Height = 387
    Align = alClient
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyPress = DBGridKeyPress
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 86
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 5
      Top = 0
      Width = 37
      Height = 13
      Caption = 'Fatura'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbFatura: TLabel
      Left = 133
      Top = 24
      Width = 30
      Height = 13
      Caption = 'Fatura'
    end
    object lbacesso: TLabel
      Left = 133
      Top = 56
      Width = 35
      Height = 13
      Caption = 'Acesso'
    end
    object Label2: TLabel
      Left = 5
      Top = 39
      Width = 42
      Height = 13
      Caption = 'Acesso'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object edtfatura: TEdit
      Left = 5
      Top = 16
      Width = 121
      Height = 21
      Color = clSkyBlue
      TabOrder = 0
      Text = 'edtfatura'
      OnExit = edtfaturaExit
      OnKeyDown = edtfaturaKeyDown
    end
    object edtacesso: TEdit
      Left = 5
      Top = 53
      Width = 121
      Height = 21
      Color = clSkyBlue
      TabOrder = 1
      Text = 'edtacesso'
      OnExit = edtacessoExit
      OnKeyDown = edtacessoKeyDown
    end
    object btlistaligacoes: TButton
      Left = 757
      Top = 56
      Width = 102
      Height = 25
      Caption = 'Lista Liga'#231#245'es'
      TabOrder = 2
      OnClick = btlistaligacoesClick
    end
    object Button1: TButton
      Left = 624
      Top = 56
      Width = 131
      Height = 25
      Caption = 'Acerta Depend'#234'ncias'
      TabOrder = 3
      OnClick = Button1Click
    end
  end
end
