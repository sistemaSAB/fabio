unit Uimportafatura;

interface

uses
  db,udatamodulo,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Grids, DBGrids, Buttons, ComCtrls,UobjLigacoesFaturaCel,
  TabNotBk;

type
  TFimportaFatura = class(TForm)
    Panel1: TPanel;
    OpenDialog: TOpenDialog;
    StrgDados: TStringGrid;
    MemoArquivo: TMemo;
    Pagina: TTabbedNotebook;
    btimportar: TBitBtn;
    btarquivo: TBitBtn;
    lbacesso: TLabel;
    edtacesso: TEdit;
    Label2: TLabel;
    LbFatura: TLabel;
    edtfatura: TEdit;
    Label1: TLabel;
    chk_torpedo: TCheckBox;
    comboextrai: TBitBtn;
    procedure btarquivoClick(Sender: TObject);
    procedure btimportarClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtfaturaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtacessoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtfaturaExit(Sender: TObject);
    procedure edtacessoExit(Sender: TObject);
    procedure comboextraiClick(Sender: TObject);

  private
    { Private declarations }
    Strlist:TStringList;
    ObjLigacoesFaturaCel:tobjLigacoesFaturaCel;
    Procedure ReprocessaMemo;
  public
    { Public declarations }
  end;

var
  FimportaFatura: TFimportaFatura;

implementation

uses UessencialGlobal, DateUtils;

{$R *.dfm}

procedure TFimportaFatura.btarquivoClick(Sender: TObject);
begin
     if (OpenDialog.execute=False)
     Then exit;

     MemoArquivo.Lines.clear;
     MemoArquivo.Lines.LoadFromFile(OpenDialog.FileName);
     Self.reprocessaMemo;
end;

procedure TFimportaFatura.ReprocessaMemo;
var
coluna,cont:integer;
linha:string;

begin
     StrgDados.rowcount:=1;


     //percorrendo o memo
     linha:=MemoArquivo.Lines[0];
     if (ExplodeStr(linha,Strlist,';','')=False)
     Then Begin
               mensagemerro('N�o foi poss�vel resgatar os dados da primeira linha do arquivo');
               exit;
     End;

     StrgDados.FixedCols:=0;
     //StrgDados.FixedRows:=1;
     
     StrgDados.ColCount:=Strlist.Count;

     for cont:=0 to MemoArquivo.Lines.Count-1 do
     Begin
          Strlist.Clear;
          linha:=memoarquivo.lines[cont];
          if (ExplodeStr(linha,Strlist,';','')=False)
          Then Begin
                    mensagemerro('N�o foi poss�vel resgatar os dados da linha '+inttostr(cont)+' do arquivo'+#13+LINHA);
                    exit;
          End;

          if (Strlist.count<>StrgDados.ColCount)
          then Begin
                    Mensagemerro('Erro na linha '+inttostr(cont)+', a quantidade de colunas n�o � igual a primeira linha do arquivo'+#13+linha);
                    exit;
          End;

          for coluna:=0 to strgdados.colcount-1 do
          Begin
                StrgDados.Cells[coluna,StrgDados.RowCount-1]:=Strlist[coluna];
          End;
          StrgDados.RowCount:=StrgDados.RowCount+1;
     End;//for
     StrgDados.RowCount:=StrgDados.RowCount-1;

end;

procedure TFimportaFatura.btimportarClick(Sender: TObject);
var
cont:integer;
temp:ttime;
pduracao:string;
pminuto,phora,psegundo,pmec:word;
begin
     if (edtfatura.Text='')
     then Begin
               mensagemerro('Escolha a Fatura');
               exit;
     End;

     if (edtacesso.Text='')
     then Begin
               mensagemerro('Escolha o Acesso');
               exit;
     End;


     if (ObjLigacoesFaturaCel.Fatura.LocalizaCodigo(edtfatura.Text)=False)
     then Begin
               Mensagemerro('Fatura n�o encontrada');
               exit;
     End;

     if (ObjLigacoesFaturaCel.Acesso.LocalizaCodigo(edtacesso.Text)=False)
     then Begin
               Mensagemerro('Acesso n�o encontrado');
               exit;
     End;



     //a primeira linha � o nome dos campos
     //DATA;HORA;DURACAO;TELEFONE;VALOR
Try

     for cont:=1 to StrgDados.RowCount-1 do
     Begin
          StrgDados.Row:=cont;

          Self.ObjLigacoesFaturaCel.ZerarTabela;
          Self.ObjLigacoesFaturaCel.Fatura.submit_codigo(edtfatura.Text);
          Self.ObjLigacoesFaturaCel.Acesso.Submit_CODIGO(edtacesso.text);
          Self.ObjLigacoesFaturaCel.Submit_CODIGO('0');
          Self.ObjLigacoesFaturaCel.Submit_Data(StrgDados.Cells[0,cont]);
          Self.ObjLigacoesFaturaCel.Submit_hora(StrgDados.Cells[1,cont]);


          if (chk_torpedo.checked=true)
          Then Self.ObjLigacoesFaturaCel.Submit_tipo('T')
          Else Self.ObjLigacoesFaturaCel.Submit_tipo('L');


          pduracao:=StrgDados.Cells[2,cont];

          //hora:minuto:segundo
          //tenho q separar os 3 e transformar em segundo
          DecodeTime(strtodatetime(pduracao),phora,pminuto,psegundo,pmec);
          Self.ObjLigacoesFaturaCel.Submit_duracao(inttostr(((phora*60)*60)+(pminuto*60)+psegundo));



          Self.ObjLigacoesFaturaCel.Submit_Telefone(StrgDados.Cells[3,cont]);
          Self.ObjLigacoesFaturaCel.Submit_valor(StrgDados.Cells[4,cont]);
          Self.ObjLigacoesFaturaCel.Status:=dsinsert;


          if (Self.ObjLigacoesFaturaCel.VerificaBrancos=True)
          Then exit;

          if (Self.ObjLigacoesFaturaCel.VerificaNumericos=False)
          Then Exit;

          if (Self.ObjLigacoesFaturaCel.VerificaData=False)
          Then Exit;

          if (Self.ObjLigacoesFaturaCel.VerificaFaixa=False)
          Then Exit;

          if (Self.ObjLigacoesFaturaCel.VerificaRelacionamentos=False)
          Then Exit;



          if (Self.ObjLigacoesFaturaCel.LocalizaDados=True)
          then Begin
                    if (MensagemPergunta('Esse registro j� foi importado anteriormente, deseja continuar?')=mrno)
                    then exit;
          End;

          if (self.ObjLigacoesFaturaCel.Salvar(False)=False)
          then Begin
                    Mensagemerro('Erro na tentativa de gravar');
                    exit;
          End;
     End;
     StrgDados.Rowcount:=1;
     StrgDados.rows[0].clear;
     FDataModulo.IBTransaction.CommitRetaining;
     MensagemAviso('Conclu�do');

Finally
       FDataModulo.IBTransaction.rollbackretaining;
End;



end;

procedure TFimportaFatura.FormActivate(Sender: TObject);
begin
     pagina.PageIndex:=0;
     desab_botoes(self);
     limpaedit(self);
     LbFatura.caption:='';
     lbacesso.caption:='';

     Try
        Strlist:=TStringList.Create;
     Except
           Mensagemerro('Erro na tentativa de criar a String List');
           exit;
     End;

     Try
        ObjLigacoesFaturaCel:=tobjLigacoesFaturaCel.create;
     Except
           Mensagemerro('Erro na tentativa de criar o objeto de liga��es fatura');
           exit;
     End;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR CADASTRO DE IMPORTACAO DE FATURA')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(self);

     edtfatura.SetFocus;

end;

procedure TFimportaFatura.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     freeandnil(Strlist);
end;

procedure TFimportaFatura.edtfaturaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjLigacoesFaturaCel.EdtFaturaKeyDown(sender,key,shift,LbFatura);
end;

procedure TFimportaFatura.edtacessoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjLigacoesFaturaCel.EdtAcessoKeyDown(sender,key,shift,lbacesso);
end;

procedure TFimportaFatura.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);

end;

procedure TFimportaFatura.edtfaturaExit(Sender: TObject);
begin
     Self.ObjLigacoesFaturaCel.EdtFaturaExit(sender,LbFatura);
end;

procedure TFimportaFatura.edtacessoExit(Sender: TObject);
begin
    Self.ObjLigacoesFaturaCel.EdtAcessoExit(sender,lbacesso);
end;

procedure TFimportaFatura.comboextraiClick(Sender: TObject);
var
cont:integer;
temp:string;
Strtemp:tstringlist;
begin
     Try
        Strtemp:=Tstringlist.create;
     Except
           mensagemerro('Erro na cria��o da StringList');
           exit;
     End;

     Try
         StrgDados.RowCount:=1;
         StrgDados.FixedCols:=0;
         StrgDados.fixedrows:=0;
         StrgDados.Rows[0].clear;

         for cont:=0 to memoarquivo.lines.count-1 do
         Begin
              temp:=MemoArquivo.lines[cont];
              //'08/01/2008	18:01:33	00:00:08		6784059705	0,17'
              ExplodeStr(temp,strtemp,'	','');
              if (strtemp.count<>5)
              Then Begin
                        Mensagemerro(' Erro na Linha '+temp);
                        exit;
              End;
              StrgDados.cells[0,cont]:=Strtemp[0];
              StrgDados.cells[1,cont]:=Strtemp[1];
              StrgDados.cells[2,cont]:=Strtemp[2];
              StrgDados.cells[3,cont]:=Strtemp[3];
              StrgDados.cells[4,cont]:=Strtemp[4];
              StrgDados.rowcount:=StrgDados.rowcount+1;
         End;
         StrgDados.rowcount:=StrgDados.rowcount-1;
     Finally
            freeandnil(Strtemp);
     End;
end;

end.


