object FimportaFatura: TFimportaFatura
  Left = 234
  Top = 142
  Width = 870
  Height = 500
  Caption = 'Importa'#231#227'o de Fatura'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 862
    Height = 149
    Align = alTop
    TabOrder = 0
    object Pagina: TTabbedNotebook
      Left = 1
      Top = 1
      Width = 860
      Height = 147
      Align = alClient
      TabFont.Charset = DEFAULT_CHARSET
      TabFont.Color = clBtnText
      TabFont.Height = -11
      TabFont.Name = 'MS Sans Serif'
      TabFont.Style = []
      TabOrder = 0
      object TTabPage
        Left = 4
        Top = 24
        Caption = '&1-Principal'
        object lbacesso: TLabel
          Left = 133
          Top = 56
          Width = 35
          Height = 13
          Caption = 'Acesso'
        end
        object Label2: TLabel
          Left = 5
          Top = 39
          Width = 42
          Height = 13
          Caption = 'Acesso'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbFatura: TLabel
          Left = 133
          Top = 24
          Width = 30
          Height = 13
          Caption = 'Fatura'
        end
        object Label1: TLabel
          Left = 5
          Top = 0
          Width = 37
          Height = 13
          Caption = 'Fatura'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object btimportar: TBitBtn
          Left = 739
          Top = 33
          Width = 114
          Height = 25
          Caption = 'Importar'
          TabOrder = 3
          OnClick = btimportarClick
        end
        object btarquivo: TBitBtn
          Left = 739
          Top = 57
          Width = 114
          Height = 25
          Caption = 'Abrir Arquivo'
          TabOrder = 4
          OnClick = btarquivoClick
        end
        object edtacesso: TEdit
          Left = 5
          Top = 53
          Width = 121
          Height = 21
          Color = clSkyBlue
          TabOrder = 1
          Text = 'edtacesso'
          OnExit = edtacessoExit
          OnKeyDown = edtacessoKeyDown
        end
        object edtfatura: TEdit
          Left = 5
          Top = 16
          Width = 121
          Height = 21
          Color = clSkyBlue
          TabOrder = 0
          Text = 'edtfatura'
          OnExit = edtfaturaExit
          OnKeyDown = edtfaturaKeyDown
        end
        object chk_torpedo: TCheckBox
          Left = 7
          Top = 80
          Width = 97
          Height = 17
          Caption = 'Torpedo'
          TabOrder = 2
        end
        object comboextrai: TBitBtn
          Left = 739
          Top = 81
          Width = 114
          Height = 25
          Caption = 'Extrai para o Grid'
          TabOrder = 5
          OnClick = comboextraiClick
        end
      end
      object TTabPage
        Left = 4
        Top = 24
        Caption = '&2-Configura'#231#227'o'
      end
    end
  end
  object StrgDados: TStringGrid
    Left = 0
    Top = 149
    Width = 862
    Height = 101
    Align = alClient
    TabOrder = 1
  end
  object MemoArquivo: TMemo
    Left = 0
    Top = 250
    Width = 862
    Height = 223
    Align = alBottom
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    Lines.Strings = (
      'Memo1')
    ParentFont = False
    TabOrder = 2
  end
  object OpenDialog: TOpenDialog
    Left = 664
    Top = 72
  end
end
