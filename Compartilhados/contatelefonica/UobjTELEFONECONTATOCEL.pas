unit UobjTELEFONECONTATOCEL;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJCONTATOCEL
;
//USES_INTERFACE



Type
   TObjTELEFONECONTATOCEL=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Contato:TOBJCONTATOCEL;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                function    Localiza_Telefone(ptelefone: string): boolean;


                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                Procedure Submit_Telefone(parametro: string);
                Function Get_Telefone: string;
                Procedure Submit_Observacao(parametro: String);
                Function Get_Observacao: string;
                procedure EdtContatoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtContatoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Procedure REsgataTelefones(Pcodigo:string);
//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Nome:string;
               Telefone:string;
               Observacao:String;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UCONTATOCEL;





Function  TObjTELEFONECONTATOCEL.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        If(FieldByName('Contato').asstring<>'')
        Then Begin
                 If (Self.Contato.LocalizaCodigo(FieldByName('Contato').asstring)=False)
                 Then Begin
                          Messagedlg('Contato N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Contato.TabelaparaObjeto;
        End;
        Self.Telefone:=fieldbyname('Telefone').asstring;
        Self.Observacao:=fieldbyname('Observacao').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjTELEFONECONTATOCEL.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('Contato').asstring:=Self.Contato.GET_CODIGO;
        ParamByName('Telefone').asstring:=Self.Telefone;
        ParamByName('Observacao').asstring:=Self.Observacao;
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjTELEFONECONTATOCEL.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

   if (Self.Localiza_Telefone(Self.Telefone)=True)
   then Begin
             if (Self.Status=dsInsert)
             Then Begin
                       Mensagemerro('Esse telefone j� esta cadastrado para o contato '+Self.Objquery.fieldbyname('nomecontato').asstring);
                       exit;
             End
             Else begin
                       if (Self.Objquery.FieldByName('codigo').asstring<>Self.codigo)
                       then Begin
                                 Mensagemerro('Esse telefone j� esta cadastrado para o contato '+Self.Objquery.fieldbyname('nomecontato').asstring);
                                 exit;
                       End;
             End;
   End;



    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjTELEFONECONTATOCEL.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Nome:='';
        Contato.ZerarTabela;
        Telefone:='';
        Observacao:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjTELEFONECONTATOCEL.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Telefone='')
      Then Mensagem:=mensagem+'/Telefone';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjTELEFONECONTATOCEL.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
     If (Self.Contato.LocalizaCodigo(Self.Contato.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Contato n�o Encontrado!';

//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End;

     result:=true;
End;

function TObjTELEFONECONTATOCEL.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Contato.Get_Codigo<>'')
        Then Strtoint(Self.Contato.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Contato';
     End;
//CODIFICA VERIFICANUMERICOS

     if (VerificaSoNumeros(Self.Telefone)=False)
     Then Begin
               if (Mensagempergunta('O telefone n�o cont�m somente n�meros. Deseja continuar?')=mrno)
               Then exit;
     End;



     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjTELEFONECONTATOCEL.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjTELEFONECONTATOCEL.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjTELEFONECONTATOCEL.LocalizaCodigo(parametro: string): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro TELEFONECONTATOCEL vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Nome,Contato,Telefone,Observacao');
           SQL.ADD(' from  TabTelefoneContatoCel');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjTELEFONECONTATOCEL.Localiza_Telefone(ptelefone:string): boolean;//ok
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select TCC.CODIGO,TCC.Nome,TCC.Contato,TCC.Telefone,TCC.Observacao,');
           sql.add('TabContatocel.nome as NOMECONTATO');
           SQL.ADD(' from  TabTelefoneContatoCel TCC');
           SQL.ADD(' join tabcontatocel on TCC.contato=tabcontatocel.codigo');
           SQL.ADD(' WHERE TCC.telefone='+#39+ptelefone+#39);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjTELEFONECONTATOCEL.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjTELEFONECONTATOCEL.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           on e:exception do
           Begin
                 mensagemerro(e.message);
                 result:=false;
           End;
     End;
end;


constructor TObjTELEFONECONTATOCEL.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ObjDatasource:=TDataSource. create(nil);
        Self.ObjDatasource.dataset:=Self.ObjqueryPesquisa;


        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Contato:=TOBJCONTATOCEL.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabTelefoneContatoCel(CODIGO,Nome,Contato');
                InsertSQL.add(' ,Telefone,Observacao)');
                InsertSQL.add('values (:CODIGO,:Nome,:Contato,:Telefone,:Observacao');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabTelefoneContatoCel set CODIGO=:CODIGO,Nome=:Nome');
                ModifySQL.add(',Contato=:Contato,Telefone=:Telefone,Observacao=:Observacao');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabTelefoneContatoCel where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjTELEFONECONTATOCEL.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjTELEFONECONTATOCEL.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabTELEFONECONTATOCEL');
     Result:=Self.ParametroPesquisa;
end;

function TObjTELEFONECONTATOCEL.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de TELEFONECONTATOCEL ';
end;


function TObjTELEFONECONTATOCEL.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENTELEFONECONTATOCEL,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENTELEFONECONTATOCEL,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjTELEFONECONTATOCEL.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjDataSource);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Contato.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjTELEFONECONTATOCEL.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjTELEFONECONTATOCEL.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjTelefoneContatoCel.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjTelefoneContatoCel.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjTelefoneContatoCel.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjTelefoneContatoCel.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
procedure TObjTelefoneContatoCel.Submit_Telefone(parametro: string);
begin
        Self.Telefone:=Parametro;
end;
function TObjTelefoneContatoCel.Get_Telefone: string;
begin
        Result:=Self.Telefone;
end;
procedure TObjTelefoneContatoCel.Submit_Observacao(parametro: String);
begin
        Self.Observacao:=Parametro;
end;
function TObjTelefoneContatoCel.Get_Observacao: String;
begin
        Result:=Self.Observacao;
end;
//CODIFICA GETSESUBMITS


procedure TObjTELEFONECONTATOCEL.EdtContatoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Contato.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Contato.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Contato.GET_NOME;
End;
procedure TObjTELEFONECONTATOCEL.EdtContatoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCONTATOCEL:TFCONTATOCEL;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCONTATOCEL:=TFCONTATOCEL.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Contato.Get_Pesquisa,Self.Contato.Get_TituloPesquisa,FContatoCel)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Contato.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Contato.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Contato.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCONTATOCEL);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjTELEFONECONTATOCEL.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJTELEFONECONTATOCEL';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TObjTELEFONECONTATOCEL.REsgataTelefones(Pcodigo: string);
begin
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select telefone,nome,codigo  from tabtelefonecontatocel where contato='+Pcodigo);
          open;
     End;
end;




end.



