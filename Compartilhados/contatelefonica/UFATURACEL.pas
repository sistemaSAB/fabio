unit UFATURACEL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjFATURACEL,
  jpeg;

type
  TFFATURACEL = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbContrato: TLabel;
    EdtContrato: TEdit;
    LbNomeContrato: TLabel;
    Memoobservacao: TMemo;
    Lbvencimento: TLabel;
    Edtvencimento: TMaskEdit;
    Lbvalor: TLabel;
    Edtvalor: TEdit;
    Lbobservacao: TLabel;
//DECLARA COMPONENTES
    procedure edtContratoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtContratoExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MemoobservacaoKeyPress(Sender: TObject; var Key: Char);
  private
         ObjFATURACEL:TObjFATURACEL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFATURACEL: TFFATURACEL;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFFATURACEL.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjFATURACEL do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Contrato.Submit_codigo(edtContrato.text);
        Submit_vencimento(edtvencimento.text);
        Submit_valor(edtvalor.text);
        Submit_observacao(memoobservacao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFFATURACEL.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjFATURACEL do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtContrato.text:=Contrato.Get_codigo;
        Edtvencimento.text:=Get_vencimento;
        Edtvalor.text:=Get_valor;
        memoobservacao.text:=Get_observacao;
        lbnomecontrato.caption:=Contrato.get_nome;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFFATURACEL.TabelaParaControles: Boolean;
begin
     If (Self.ObjFATURACEL.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFFATURACEL.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjFATURACEL:=TObjFATURACEL.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE FATURA')=False)
     then desab_botoes(Self)
     Else habilita_botoes(self);

end;

procedure TFFATURACEL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjFATURACEL=Nil)
     Then exit;

If (Self.ObjFATURACEL.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjFATURACEL.free;
end;

procedure TFFATURACEL.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFFATURACEL.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjFATURACEL.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjFATURACEL.status:=dsInsert;
     Guia.pageindex:=0;
     edtContrato.setfocus;

end;


procedure TFFATURACEL.btalterarClick(Sender: TObject);
begin
    If (Self.ObjFATURACEL.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjFATURACEL.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtContrato.setfocus;
                
          End;


end;

procedure TFFATURACEL.btgravarClick(Sender: TObject);
begin

     If Self.ObjFATURACEL.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjFATURACEL.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjFATURACEL.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFATURACEL.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjFATURACEL.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjFATURACEL.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjFATURACEL.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFFATURACEL.btcancelarClick(Sender: TObject);
begin
     Self.ObjFATURACEL.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFFATURACEL.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFFATURACEL.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjFATURACEL.Get_pesquisa,Self.ObjFATURACEL.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjFATURACEL.status<>dsinactive
                                  then exit;

                                  If (Self.ObjFATURACEL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjFATURACEL.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFFATURACEL.LimpaLabels;
begin
//LIMPA LABELS
    LbNomeContrato.caption:='';
end;

procedure TFFATURACEL.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
procedure TFFATURACEL.edtContratoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjFATURACEL.edtContratokeydown(sender,key,shift,lbnomeContrato);
end;
 
procedure TFFATURACEL.edtContratoExit(Sender: TObject);
begin
    ObjFATURACEL.edtContratoExit(sender,lbnomeContrato);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFFATURACEL.MemoobservacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
     IF KEY=#13
     Then Memoobservacao.SetFocus;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjFATURACEL.OBJETO.Get_Pesquisa,Self.ObjFATURACEL.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjFATURACEL.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjFATURACEL.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjFATURACEL.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
