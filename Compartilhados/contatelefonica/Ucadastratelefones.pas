unit Ucadastratelefones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Grids, DBGrids,uobjligacoesfaturacel, StdCtrls;

type
  TFcadastraTelefones = class(TForm)
    DBGrid: TDBGrid;
    Panel1: TPanel;
    Label1: TLabel;
    edtfatura: TEdit;
    LbFatura: TLabel;
    lbacesso: TLabel;
    edtacesso: TEdit;
    Label2: TLabel;
    btlistaligacoes: TButton;
    Button1: TButton;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edtfaturaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtfaturaExit(Sender: TObject);
    procedure edtacessoExit(Sender: TObject);
    procedure edtacessoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btlistaligacoesClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    ObjLigacoesFatura:TObjLIGACOESFATURACEL;
  public
    { Public declarations }
  end;

var
  FcadastraTelefones: TFcadastraTelefones;

implementation

uses UDataModulo, UessencialGlobal, UcadastraTelefone;

{$R *.dfm}

procedure TFcadastraTelefones.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     LbFatura.caption:='';
     lbacesso.caption:='';

     Try
        ObjLigacoesFatura:=TObjLIGACOESFATURACEL.create;
        DBGrid.DataSource:=Self.ObjLigacoesFatura.ObjDatasource;
     Except
           desab_botoes(self);
           Mensagemerro('Erro na tentativa de criar o objeto de liga��es');
           exit;
     End;
     habilita_botoes(self);
     edtfatura.SetFocus;

end;

procedure TFcadastraTelefones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     ObjLigacoesFatura.Free;
end;

procedure TFcadastraTelefones.edtfaturaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjLigacoesFatura.EdtFaturaKeyDown(sender,key,shift,LbFatura);
end;

procedure TFcadastraTelefones.edtfaturaExit(Sender: TObject);
begin
     Self.ObjLigacoesFatura.EdtFaturaExit(sender,LbFatura);
end;

procedure TFcadastraTelefones.edtacessoExit(Sender: TObject);
begin
     Self.ObjLigacoesFatura.EdtAcessoExit(sender,lbacesso);
end;

procedure TFcadastraTelefones.edtacessoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjLigacoesFatura.EdtAcessoKeyDown(sender,key,shift,lbacesso);
end;

procedure TFcadastraTelefones.btlistaligacoesClick(Sender: TObject);
begin
     Self.ObjLigacoesFatura.RetornaClientesNaoCadastrados(edtfatura.text,edtacesso.text);
     Self.DBGrid.SetFocus;
end;

procedure TFcadastraTelefones.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;

procedure TFcadastraTelefones.DBGridKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key<>#13
     Then exit;

     if (DBGrid.DataSource.DataSet.Active=false)
     then exit;

     if (DBGrid.DataSource.DataSet.recordcount=0)
     Then exit;


     if (Self.ObjLigacoesFatura.TelefoneContato.Localiza_Telefone(DBGrid.DataSource.DataSet.fieldbyname('telefone').asstring)=true)
     Then Begin
               mensagemaviso('Telefone j� cadastrado');
               exit;
     End;


     Fcadastratelefone.PTelefone:=DBGrid.DataSource.DataSet.fieldbyname('telefone').asstring;
     Fcadastratelefone.PObjLIgacoes:=Self.ObjLigacoesFatura;

     Fcadastratelefone.showmodal;

     Self.btlistaligacoesClick(sender);


end;

procedure TFcadastraTelefones.Button1Click(Sender: TObject);
begin
     Self.ObjLigacoesFatura.AcertaDependencias;
end;

end.
