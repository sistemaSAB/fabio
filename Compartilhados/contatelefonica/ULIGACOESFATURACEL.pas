unit ULIGACOESFATURACEL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjLIGACOESFATURACEL,
  jpeg;

type
  TFLIGACOESFATURACEL = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbFatura: TLabel;
    EdtFatura: TEdit;
    LbAcesso: TLabel;
    EdtAcesso: TEdit;
    LbNomeAcesso: TLabel;
    LbTelefoneContato: TLabel;
    EdtTelefoneContato: TEdit;
    LbNomeTelefoneContato: TLabel;
    LbTelefone: TLabel;
    EdtTelefone: TEdit;
    LbData: TLabel;
    EdtData: TMaskEdit;
    Lbhora: TLabel;
    Edthora: TMaskEdit;
    Lbduracao: TLabel;
    Edtduracao: TEdit;
    Lbvalor: TLabel;
    Edtvalor: TEdit;
    combotipo: TComboBox;
    Label1: TLabel;
//DECLARA COMPONENTES
    procedure edtFaturaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtAcessoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtAcessoExit(Sender: TObject);
    procedure edtTelefoneContatoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtTelefoneContatoExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjLIGACOESFATURACEL:TObjLIGACOESFATURACEL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FLIGACOESFATURACEL: TFLIGACOESFATURACEL;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFLIGACOESFATURACEL.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjLIGACOESFATURACEL do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Fatura.Submit_codigo(edtFatura.text);
        Acesso.Submit_codigo(edtAcesso.text);
        TelefoneContato.Submit_codigo(edtTelefoneContato.text);
        Submit_Telefone(edtTelefone.text);
        Submit_Data(edtData.text);
        Submit_hora(edthora.text);
        Submit_duracao(edtduracao.text);
        Submit_valor(edtvalor.text);
        Submit_tipo(Submit_ComboBox(combotipo));
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFLIGACOESFATURACEL.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjLIGACOESFATURACEL do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtFatura.text:=Fatura.Get_codigo;
        EdtAcesso.text:=Acesso.Get_codigo;
        EdtTelefoneContato.text:=TelefoneContato.Get_codigo;
        EdtTelefone.text:=Get_Telefone;
        EdtData.text:=Get_Data;
        Edthora.text:=Get_hora;
        Edtduracao.text:=Get_duracao;
        Edtvalor.text:=Get_valor;

        if (get_tipo='T')
        Then combotipo.itemindex:=0
        Else combotipo.itemindex:=1;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFLIGACOESFATURACEL.TabelaParaControles: Boolean;
begin
     If (Self.ObjLIGACOESFATURACEL.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFLIGACOESFATURACEL.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjLIGACOESFATURACEL:=TObjLIGACOESFATURACEL.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE LIGACOES DA FATURA')=False)
     then desab_botoes(Self)
     Else habilita_botoes(self);

end;

procedure TFLIGACOESFATURACEL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjLIGACOESFATURACEL=Nil)
     Then exit;

If (Self.ObjLIGACOESFATURACEL.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjLIGACOESFATURACEL.free;
end;

procedure TFLIGACOESFATURACEL.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFLIGACOESFATURACEL.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjLIGACOESFATURACEL.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjLIGACOESFATURACEL.status:=dsInsert;
     Guia.pageindex:=0;
     edtFatura.setfocus;

end;


procedure TFLIGACOESFATURACEL.btalterarClick(Sender: TObject);
begin
    If (Self.ObjLIGACOESFATURACEL.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjLIGACOESFATURACEL.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtFatura.setfocus;
                
          End;


end;

procedure TFLIGACOESFATURACEL.btgravarClick(Sender: TObject);
begin

     If Self.ObjLIGACOESFATURACEL.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjLIGACOESFATURACEL.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjLIGACOESFATURACEL.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFLIGACOESFATURACEL.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjLIGACOESFATURACEL.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjLIGACOESFATURACEL.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjLIGACOESFATURACEL.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFLIGACOESFATURACEL.btcancelarClick(Sender: TObject);
begin
     Self.ObjLIGACOESFATURACEL.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFLIGACOESFATURACEL.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFLIGACOESFATURACEL.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjLIGACOESFATURACEL.Get_pesquisa,Self.ObjLIGACOESFATURACEL.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjLIGACOESFATURACEL.status<>dsinactive
                                  then exit;

                                  If (Self.ObjLIGACOESFATURACEL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjLIGACOESFATURACEL.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFLIGACOESFATURACEL.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeTelefoneContato.caption:='';
     LbNomeAcesso.caption:='';
end;

procedure TFLIGACOESFATURACEL.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
procedure TFLIGACOESFATURACEL.edtFaturaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjLIGACOESFATURACEL.edtFaturakeydown(sender,key,shift,nil);
end;
 
procedure TFLIGACOESFATURACEL.edtAcessoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjLIGACOESFATURACEL.edtAcessokeydown(sender,key,shift,lbnomeAcesso);
end;
 
procedure TFLIGACOESFATURACEL.edtAcessoExit(Sender: TObject);
begin
    ObjLIGACOESFATURACEL.edtAcessoExit(sender,lbnomeAcesso);
end;
procedure TFLIGACOESFATURACEL.edtTelefoneContatoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjLIGACOESFATURACEL.edtTelefoneContatokeydown(sender,key,shift,lbnomeTelefoneContato);
end;
 
procedure TFLIGACOESFATURACEL.edtTelefoneContatoExit(Sender: TObject);
begin
    ObjLIGACOESFATURACEL.edtTelefoneContatoExit(sender,lbnomeTelefoneContato);
end;
//CODIFICA ONKEYDOWN E ONEXIT


end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjLIGACOESFATURACEL.OBJETO.Get_Pesquisa,Self.ObjLIGACOESFATURACEL.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjLIGACOESFATURACEL.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjLIGACOESFATURACEL.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjLIGACOESFATURACEL.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
