unit UACESSOCONTRATOCEL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjACESSOCONTRATOCEL,
  jpeg;

type
  TFACESSOCONTRATOCEL = class(TForm)
    Guia: TTabbedNotebook;
    Panel1: TPanel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    LbCODIGO: TLabel;
    EdtCODIGO: TEdit;
    LbContrato: TLabel;
    EdtContrato: TEdit;
    LbNomeContrato: TLabel;
    Memoobservacao: TMemo;
    LbTelefone: TLabel;
    EdtTelefone: TEdit;
    LbObservacao: TLabel;
//DECLARA COMPONENTES
    procedure edtContratoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure edtContratoExit(Sender: TObject);
    procedure MemoobservacaoKeyPress(Sender: TObject; var Key: Char);

    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjACESSOCONTRATOCEL:TObjACESSOCONTRATOCEL;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FACESSOCONTRATOCEL: TFACESSOCONTRATOCEL;


implementation

uses UessencialGlobal, Upesquisa, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFACESSOCONTRATOCEL.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjACESSOCONTRATOCEL do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Contrato.Submit_codigo(edtContrato.text);
        Submit_Telefone(edtTelefone.text);
        Submit_Observacao(memoObservacao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFACESSOCONTRATOCEL.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjACESSOCONTRATOCEL do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtContrato.text:=Contrato.Get_codigo;
        EdtTelefone.text:=Get_Telefone;
        memoObservacao.text:=Get_Observacao;
        LbNomeContrato.caption:=Contrato.Get_Nome;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFACESSOCONTRATOCEL.TabelaParaControles: Boolean;
begin
     If (Self.ObjACESSOCONTRATOCEL.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFACESSOCONTRATOCEL.FormActivate(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
        Self.ObjACESSOCONTRATOCEL:=TObjACESSOCONTRATOCEL.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     PegaFiguraBotoes(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair);

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE ACESSOS DE CONTRATO DE TELEFONE')=False)
     then desab_botoes(Self)
     Else habilita_botoes(self);

end;

procedure TFACESSOCONTRATOCEL.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjACESSOCONTRATOCEL=Nil)
     Then exit;

If (Self.ObjACESSOCONTRATOCEL.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Self.ObjACESSOCONTRATOCEL.free;
end;

procedure TFACESSOCONTRATOCEL.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFACESSOCONTRATOCEL.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=Self.ObjACESSOCONTRATOCEL.Get_novocodigo;
     edtcodigo.enabled:=False;

     
     Btgravar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Self.ObjACESSOCONTRATOCEL.status:=dsInsert;
     Guia.pageindex:=0;
     edtContrato.setfocus;

end;


procedure TFACESSOCONTRATOCEL.btalterarClick(Sender: TObject);
begin
    If (Self.ObjACESSOCONTRATOCEL.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjACESSOCONTRATOCEL.Status:=dsEdit;
                guia.pageindex:=0;
                desab_botoes(Self);
                Btgravar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                edtContrato.setfocus;
                
          End;


end;

procedure TFACESSOCONTRATOCEL.btgravarClick(Sender: TObject);
begin

     If Self.ObjACESSOCONTRATOCEL.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjACESSOCONTRATOCEL.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjACESSOCONTRATOCEL.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFACESSOCONTRATOCEL.btexcluirClick(Sender: TObject);
begin
     If (Self.ObjACESSOCONTRATOCEL.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjACESSOCONTRATOCEL.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjACESSOCONTRATOCEL.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFACESSOCONTRATOCEL.btcancelarClick(Sender: TObject);
begin
     Self.ObjACESSOCONTRATOCEL.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFACESSOCONTRATOCEL.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFACESSOCONTRATOCEL.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjACESSOCONTRATOCEL.Get_pesquisa,Self.ObjACESSOCONTRATOCEL.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjACESSOCONTRATOCEL.status<>dsinactive
                                  then exit;

                                  If (Self.ObjACESSOCONTRATOCEL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjACESSOCONTRATOCEL.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFACESSOCONTRATOCEL.LimpaLabels;
begin
//LIMPA LABELS
     LbNomeContrato.caption:='';
end;

procedure TFACESSOCONTRATOCEL.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
end;
procedure TFACESSOCONTRATOCEL.edtContratoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjACESSOCONTRATOCEL.edtContratokeydown(sender,key,shift,lbnomeContrato);
end;
 
procedure TFACESSOCONTRATOCEL.edtContratoExit(Sender: TObject);
begin
    ObjACESSOCONTRATOCEL.edtContratoExit(sender,lbnomeContrato);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFACESSOCONTRATOCEL.MemoobservacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     then memoobservacao.setfocus;
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa
(Self.ObjACESSOCONTRATOCEL.OBJETO.Get_Pesquisa,Self.ObjACESSOCONTRATOCEL.OBJETO.Get_TituloPesquisa,NOMEDOFORM_OU_NIL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjACESSOCONTRATOCEL.objeto.RETORNACAMPOCODIGO).asstring;
                                 If TEdit(Sender).text<>''
                                 Then Begin
                                        If Self.ObjACESSOCONTRATOCEL.objeto.RETORNACAMPONOME<>''
                                        Then Self.Lb.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ObjACESSOCONTRATOCEL.objeto.RETORNACAMPONOME).asstring
                                        Else Self.Lb.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;




}
