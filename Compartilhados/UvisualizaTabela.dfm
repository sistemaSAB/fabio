object Fvisualizatabela: TFvisualizatabela
  Left = 441
  Top = 249
  Width = 960
  Height = 603
  Caption = 'Visualiza'#231#227'o de Tabelas'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TPageControl
    Left = 0
    Top = 0
    Width = 944
    Height = 565
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet1: TTabSheet
      Caption = 'Tabelas'
      object DBNavigatorTabela: TDBNavigator
        Left = 0
        Top = 34
        Width = 936
        Height = 25
        DataSource = DsTabela
        Align = alTop
        TabOrder = 0
      end
      object DBGridTabela: TDBGrid
        Left = 0
        Top = 59
        Width = 936
        Height = 478
        Align = alClient
        DataSource = DsTabela
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 936
        Height = 34
        Align = alTop
        TabOrder = 2
        DesignSize = (
          936
          34)
        object BtCommit_tabela: TButton
          Left = 784
          Top = 6
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '&Commit'
          TabOrder = 0
          OnClick = BtCommit_tabelaClick
        end
        object btRollback_Tabela: TButton
          Left = 864
          Top = 6
          Width = 75
          Height = 25
          Anchors = [akTop, akRight]
          Caption = '&Rollback'
          TabOrder = 1
          OnClick = btRollback_TabelaClick
        end
        object Combotabela: TComboBox
          Left = 0
          Top = 8
          Width = 145
          Height = 21
          CharCase = ecUpperCase
          ItemHeight = 13
          TabOrder = 2
          Text = 'COMBOTABELA'
          OnKeyPress = CombotabelaKeyPress
        end
        object Button1: TButton
          Left = 151
          Top = 7
          Width = 75
          Height = 25
          Caption = 'Op'#231#245'es'
          TabOrder = 3
          OnClick = Button1Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Scripts'
      ImageIndex = 1
      inline FrScriptSql1: TFrScriptSql
        Left = 0
        Top = 0
        Width = 936
        Height = 537
        Align = alClient
        TabOrder = 0
        inherited DBGridScript: TDBGrid
          Width = 936
          Height = 369
        end
        inherited PanelBotoes: TPanel
          Width = 936
          inherited BtExecutaScript: TBitBtn
            OnClick = FrScriptSql1BtExecutaScriptClick
          end
        end
        inherited LbScript: TListBox
          Width = 936
        end
        inherited MemoScript: TRichEdit
          Width = 936
        end
      end
    end
  end
  object Ibtabela: TIBTable
    Left = 896
    Top = 56
  end
  object DsTabela: TDataSource
    DataSet = Ibtabela
    Left = 928
    Top = 56
  end
end
