object FCadastraPROTECAOTELA: TFCadastraPROTECAOTELA
  Left = 194
  Top = 244
  Width = 712
  Height = 341
  Caption = 'Cadastro de Prote'#231#227'o de Tela - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 704
    Height = 314
    Align = alClient
    PageIndex = 1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object LbCODIGO: TLabel
        Left = 16
        Top = 20
        Width = 44
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbMensagem: TLabel
        Left = 16
        Top = 70
        Width = 69
        Height = 13
        Caption = 'Mensagem'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Lbdatahorainicial: TLabel
        Left = 16
        Top = 144
        Width = 122
        Height = 13
        Caption = 'Data e Hora Inicial'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Lbdatahorafinal: TLabel
        Left = 192
        Top = 144
        Width = 113
        Height = 13
        Caption = 'Data e Hora Final'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 16
        Top = 36
        Width = 100
        Height = 19
        MaxLength = 9
        TabOrder = 2
      end
      object Edtdatahorainicial: TMaskEdit
        Left = 16
        Top = 160
        Width = 114
        Height = 19
        EditMask = '!99/99/9999 99:99;'
        MaxLength = 16
        TabOrder = 0
        Text = '  /  /       :  '
      end
      object Edtdatahorafinal: TMaskEdit
        Left = 192
        Top = 160
        Width = 115
        Height = 19
        EditMask = '!99/99/9999 99:99;'
        MaxLength = 16
        TabOrder = 1
        Text = '  /  /       :  '
      end
      object Panel1: TPanel
        Left = 0
        Top = 196
        Width = 696
        Height = 90
        Align = alBottom
        Color = 3355443
        TabOrder = 3
        object Btnovo: TBitBtn
          Left = 3
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Novo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          OnClick = BtnovoClick
        end
        object btalterar: TBitBtn
          Left = 115
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Alterar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          OnClick = btalterarClick
        end
        object btgravar: TBitBtn
          Left = 227
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Gravar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          OnClick = btgravarClick
        end
        object btcancelar: TBitBtn
          Left = 339
          Top = 6
          Width = 108
          Height = 38
          Caption = '&Cancelar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          OnClick = btcancelarClick
        end
        object btpesquisar: TBitBtn
          Left = 3
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Pesquisar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          OnClick = btpesquisarClick
        end
        object btrelatorios: TBitBtn
          Left = 115
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Relat'#243'rios'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 5
        end
        object btexcluir: TBitBtn
          Left = 227
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Excluir'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          OnClick = btexcluirClick
        end
        object btsair: TBitBtn
          Left = 339
          Top = 46
          Width = 108
          Height = 38
          Caption = '&Sair'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          OnClick = btsairClick
        end
      end
      object edtmensagem: TMemo
        Left = 16
        Top = 88
        Width = 673
        Height = 45
        Lines.Strings = (
          'edtmensagem')
        TabOrder = 4
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Configura'#231#227'o da Prote'#231#227'o de Tela'
      object panelconfiguracao: TPanel
        Left = 0
        Top = 0
        Width = 696
        Height = 286
        Align = alClient
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 24
          Width = 189
          Height = 13
          Caption = 'Tempo para ativa'#231#227'o (segundos)'
        end
        object Label2: TLabel
          Left = 216
          Top = 24
          Width = 224
          Height = 13
          Caption = 'Velocidade da anima'#231#227'o (ms por pixel)'
        end
        object Label3: TLabel
          Left = 8
          Top = 72
          Width = 190
          Height = 13
          Caption = 'Posi'#231#227'o Superior (Top em Pixels)'
        end
        object edttempo: TEdit
          Left = 8
          Top = 40
          Width = 121
          Height = 19
          TabOrder = 0
          Text = '0'
          OnKeyPress = edttempoKeyPress
        end
        object edtvelocidade: TEdit
          Left = 216
          Top = 40
          Width = 121
          Height = 19
          TabOrder = 1
          Text = '0'
        end
        object btgravarINI: TButton
          Left = 8
          Top = 176
          Width = 75
          Height = 25
          Caption = '&Gravar'
          TabOrder = 6
          OnClick = btgravarINIClick
        end
        object btvisualiza: TButton
          Left = 88
          Top = 176
          Width = 75
          Height = 25
          Caption = '&Visualizar'
          TabOrder = 7
          OnClick = btvisualizaClick
        end
        object btfonte: TButton
          Left = 168
          Top = 128
          Width = 75
          Height = 25
          Caption = '&Fonte'
          TabOrder = 5
          OnClick = btfonteClick
        end
        object edttop: TEdit
          Left = 8
          Top = 88
          Width = 121
          Height = 19
          TabOrder = 2
          Text = '0'
          OnKeyPress = edttempoKeyPress
        end
        object btcorfundo: TBitBtn
          Left = 40
          Top = 128
          Width = 121
          Height = 25
          Caption = '&Cor de Fundo'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          OnClick = btcorfundoClick
        end
        object PanelCor: TPanel
          Left = 8
          Top = 128
          Width = 25
          Height = 25
          TabOrder = 3
        end
      end
    end
  end
  object FontDialog: TFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Left = 232
    Top = 192
  end
  object ColorDialog: TColorDialog
    Left = 188
    Top = 192
  end
end
