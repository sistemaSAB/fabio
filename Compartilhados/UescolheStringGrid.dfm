object FEscolheStringGrid: TFEscolheStringGrid
  Left = 146
  Top = 103
  Width = 654
  Height = 308
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object STRGConvenio: TStringGrid
    Left = 0
    Top = 8
    Width = 545
    Height = 265
    TabOrder = 0
    OnKeyPress = STRGConvenioKeyPress
  end
  object BtOk: TBitBtn
    Left = 552
    Top = 7
    Width = 89
    Height = 39
    Caption = '&OK'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BtOkClick
  end
  object BtCancelar: TBitBtn
    Left = 552
    Top = 47
    Width = 89
    Height = 39
    Caption = '&CANCELAR'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = BtCancelarClick
  end
end
