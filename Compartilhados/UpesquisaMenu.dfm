object fPesquisaMenu: TfPesquisaMenu
  Left = 819
  Top = 311
  Width = 274
  Height = 332
  BorderStyle = bsSizeToolWin
  Caption = 'Pesquisa de menu'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Acao: TBitBtn
    Left = 50
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Acao'
    TabOrder = 0
  end
  object panelPesquisa: TPanel
    Left = 0
    Top = 0
    Width = 258
    Height = 275
    Align = alClient
    TabOrder = 1
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 256
      Height = 21
      Align = alTop
      TabOrder = 0
      DesignSize = (
        256
        21)
      object edtPesquisa: TEdit
        Left = 0
        Top = 0
        Width = 256
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnChange = edtPesquisaChange
        OnKeyDown = edtPesquisaKeyDown
      end
    end
    object ListBox1: TListBox
      Left = 1
      Top = 22
      Width = 256
      Height = 224
      Align = alClient
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Narrow'
      Font.Style = []
      ItemHeight = 16
      ParentFont = False
      TabOrder = 1
      OnDblClick = ListBox1DblClick
      OnKeyDown = ListBox1KeyDown
    end
    object pnl1: TPanel
      Left = 1
      Top = 246
      Width = 256
      Height = 28
      Align = alBottom
      Color = 10643006
      Ctl3D = False
      ParentCtl3D = False
      TabOrder = 2
      object label2: TLabel
        Left = 1
        Top = 1
        Width = 99
        Height = 26
        Align = alLeft
        Caption = '  encontrado(s)  :'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWhite
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
      object lbTotalEncontrados: TLabel
        Left = 100
        Top = 1
        Width = 11
        Height = 26
        Align = alLeft
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clLime
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Layout = tlCenter
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 275
    Width = 258
    Height = 19
    Panels = <>
  end
  object queryExec: TIBQuery
    Left = 280
    Top = 264
  end
end
