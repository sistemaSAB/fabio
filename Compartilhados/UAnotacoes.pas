unit UAnotacoes;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db,UObjAnotacoes;

type
  TFanotacoes = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    lbquantidade: TLabel;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    ImagemFundo: TImage;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    btOpcoes: TBitBtn;
    btalterar: TBitBtn;
    BtNovo: TBitBtn;
    btpesquisar: TBitBtn;
    Btgravar: TBitBtn;
    BtCancelar: TBitBtn;
    btrelatorios: TBitBtn;
    btexcluir: TBitBtn;
    btsair: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Assunto: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdtCodigo: TEdit;
    edtcontato: TEdit;
    edtfone: TEdit;
    edtassunto: TEdit;
    edtdata: TMaskEdit;
    edtdataretorno: TMaskEdit;
    comboconcluido: TComboBox;
    edtorigem: TEdit;
    edtendereco: TEdit;
    edttecnicoresponsavel: TEdit;
    memoobservacoes: TMemo;
    Label10: TLabel;
    lbOrigem: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsairClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtNovoClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
    procedure BtgravarClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure memoobservacoesKeyPress(Sender: TObject; var Key: Char);
    procedure btrelatoriosClick(Sender: TObject);
    procedure edtorigemKeyPress(Sender: TObject; var Key: Char);
    procedure edtorigemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
//    procedure btconcluidasClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtorigemExit(Sender: TObject);
  private
         Function ControlesParaObjeto:Boolean;
         Function ObjetoParaControles:Boolean;
         Function TabelaParaControles:Boolean;
         Procedure PreparaAlteracao;
         Function  AtualizaQuantidade: string;

    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fanotacoes: TFanotacoes;
  ObjAnotacoes:TObjAnotacoes;
                        //e passa em StringList e o inverso tmbm

implementation

uses Uessencialglobal, Upesquisa, UOrigemAnotacoes, UescolheImagemBotao,UObjOrigemAnotacoes;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFanotacoes.ControlesParaObjeto: Boolean;
Begin

  Try
    With ObjAnotacoes do
    Begin
         Submit_CODIGO        (edtCODIGO.text);
         Submit_Contato       (edtContato.text);
         Submit_Fone          (edtFone.text);
         Submit_Assunto       (edtAssunto.text);
         Submit_Data          (edtData.text);
         Submit_DataRetorno   (edtDataRetorno.text);
         Submit_Observacoes   (memoobservacoes.Text);
         Submit_Concluido     (comboConcluido.text[1]);
         Submit_Origem        (edtorigem.text);
         Submit_Endereco(edtendereco.text);
         Submit_TecnicoResponsavel(edttecnicoresponsavel.text);
         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFanotacoes.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjAnotacoes do
     Begin
        edtCODIGO.text         := Get_CODIGO              ;
        edtContato.text        := Get_Contato             ;
        edtFone.text           := Get_Fone                ;
        edtAssunto.text        := Get_Assunto             ;
        edtData.text           := Get_Data                ;
        edtDataRetorno.text    := Get_DataRetorno         ;
        edtOrigem.text         := Origem.Get_CODIGO       ;
        edttecnicoresponsavel.text:=Get_TecnicoResponsavel;
        edtendereco.text:=Get_Endereco;
        memoObservacoes.lines.text  :=Get_Observacoes;
        lbOrigem.Caption:=Origem.Get_Nome;
        If (Get_concluido='S')
        Then ComboConcluido.itemindex:=0
        Else ComboConcluido.itemindex:=1;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFanotacoes.TabelaParaControles: Boolean;
begin
     ObjAnotacoes.TabelaparaObjeto;
     ObjetoParaControles;
end;




//****************************************
//****************************************



procedure TFanotacoes.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjAnotacoes=Nil)
     Then exit;

   If (ObjAnotacoes.status<>dsinactive)
   Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;
     
   //FreeAndNil(ObjAnotacoes);
   ObjAnotacoes.free;
end;

procedure TFanotacoes.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFanotacoes.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then begin
            Perform(Wm_NextDlgCtl,0,0);
      end;
end;


procedure TFanotacoes.BtNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     habilita_campos(Self);
     esconde_botoes(Self);
     lbOrigem.Caption:='';

     //edtcodigo.text:='0';
     edtcodigo.text:=ObjAnotacoes.Get_novocodigo;
     edtcodigo.enabled:=False;
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     edtdata.text:=datetostr(now);
     ObjAnotacoes.status:=dsInsert;
     edtorigem.setfocus;
     edtorigem.text:=ObjAnotacoes.PegaOrigemPadrao;
     comboconcluido.itemindex:=1;
     
end;

procedure TFanotacoes.BtCancelarClick(Sender: TObject);
begin
     ObjAnotacoes.cancelar;
     lbOrigem.Caption:='';
     limpaedit(Self);
     desabilita_campos(Self);
     mostra_botoes(Self);


end;

procedure TFanotacoes.BtgravarClick(Sender: TObject);
begin

     If ObjAnotacoes.Status=dsInactive
     Then exit;

     If Self.ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjAnotacoes.salvar(True)=False)
     Then exit;

     mostra_botoes(Self);
     limpaedit(Self);
     lbOrigem.Caption:='';
     desabilita_campos(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Self.TabelaParaControles;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFanotacoes.PreparaAlteracao;
begin
     habilita_campos(Self);
     esconde_botoes(Self);
     Btgravar.Visible:=True;
     BtCancelar.Visible:=True;
     btpesquisar.Visible:=True;
     EdtCodigo.enabled:=False;
     ObjAnotacoes.Status:=dsEdit;
     edtcontato.setfocus;
end;

procedure TFanotacoes.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjAnotacoes.Get_pesquisa,ObjAnotacoes.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjAnotacoes.status<>dsinactive
                                  then exit;

                                  If (ObjAnotacoes.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFanotacoes.btalterarClick(Sender: TObject);
begin
    If (ObjAnotacoes.Status=dsinactive) and (EdtCodigo.text<>'')
    Then PreparaAlteracao;
end;

procedure TFanotacoes.btexcluirClick(Sender: TObject);
begin
     If (ObjAnotacoes.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjAnotacoes.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (ObjAnotacoes.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     lbquantidade.Caption:=AtualizaQuantidade;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;


procedure TFanotacoes.memoobservacoesKeyPress(Sender: TObject;
  var Key: Char);
begin
     If key=#13
     Then memoobservacoes.setfocus;
end;

procedure TFanotacoes.btrelatoriosClick(Sender: TObject);
begin
     ObjAnotacoes.Imprime(EdtCodigo.text);
end;

procedure TFanotacoes.edtorigemKeyPress(Sender: TObject; var Key: Char);
begin
     If not(Key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFanotacoes.edtorigemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FOrigemAnotacoes:TFOrigemAnotacoes;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FOrigemAnotacoes:=TFOrigemAnotacoes.create(nil);
            If (FpesquisaLocal.PreparaPesquisa(ObjAnotacoes.Get_PesquisaOrigem,ObjAnotacoes.Get_TituloPesquisaOrigem,FOrigemAnotacoes)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtorigem.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FOrigemAnotacoes);
     End;

end;
   {
procedure TFanotacoes.btconcluidasClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjAnotacoes.Get_pesquisaConcluida,ObjAnotacoes.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjAnotacoes.status<>dsinactive
                                  then exit;

                                  If (ObjAnotacoes.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;

                                  TabelaParaControles;
                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;
   }
procedure TFanotacoes.FormShow(Sender: TObject);
begin
     UessencialGlobal.PegaCorForm(Self);

      limpaedit(Self);
     desabilita_campos(Self);
     lbOrigem.Caption:='';
     Try
        ObjAnotacoes:=TObjAnotacoes.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     retira_fundo_labels(self);
     Self.Color:=clwhite;
     lbquantidade.Caption:=AtualizaQuantidade;
     
end;

function TFanotacoes.AtualizaQuantidade: string;
begin
    result:='Existem '+ ContaRegistros('TABANOTACOES','codigo') + ' Anota��es Cadastradas';
end;

procedure TFanotacoes.edtorigemExit(Sender: TObject);
begin
      ObjAnotacoes.edtorigemExit(Sender,lborigem);
end;

end.
{var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            If (FpesquisaLocal.PreparaPesquisa(Obj.Get_PesquisaAnotacoes,Obj.Get_TituloPesquisaAnotacoes,Self)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 Self.edtTipoLancto.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 If Self.edtTipoLancto.text<>''
                                 Then Self.LbTipoLanctoNome.caption:=OBjLancamento.PegaNometipolancto(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring);
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;

end;
}
