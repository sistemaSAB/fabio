unit UobjLancamentosFC;

interface

Uses
    IBquery,UDataModulo,Classes,StdCtrls,DB,UObjValores,Dialogs,Windows,Controls;

type
    TobjLancamentosFC = class
      public
            ObjDatasource:TDataSource;
            ObjQuery:TIBQuery; //n�o pode ser utilizado. As vendas s�o carregadas nesta query
            objQueryAuxiliar:TIBQuery;
            ObjQueryTEMP:TIBQuery;

            Constructor Create;
            Destructor  Destroy;
            function CarregaParametros:Boolean;virtual;
            Procedure CarregaLOG(var pReferencia:tmemo);virtual;

            function VerificaDados(pVenda:string):Boolean;virtual;abstract;
            function VerificaRelacionamentoFormasPagamento(pVenda:string):Boolean;virtual;abstract;
            function VerificaLancamentoPendencias(pVenda,pPendencia:String; SomenteAVista:boolean):integer;virtual;abstract;
            function VerificaCorrespondenciaValores(pVenda,pPendencia:string; SomenteAVista:boolean):Integer;virtual;abstract;



            function Executa(pComando:string):Boolean;virtual;
            procedure ProcessaRecebimentos(pVenda,pData1,pData2:string);virtual;abstract;
            function EfetuaLancamentos(pVenda,pPendencia:string; SomenteAVista:Boolean):Boolean;virtual;abstract;


            procedure EdtVENDAKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);virtual;abstract;
            procedure LogAdd(parametro:string);

      protected
            Memo:TMemo;
            objValores:TObjValores;
            PCodigoquitacao : string;
    end;


implementation

uses SysUtils, UessencialGlobal,ULancamentosFC, Upesquisa;

var
  Lancamentos:TFLancamentosFrentedeCaixa;

{ TobjLancamentosFC }

procedure TobjLancamentosFC.CarregaLOG(var pReferencia: TMemo);
begin
      Self.Memo := pReferencia;
      Self.Memo.Clear;

end;

constructor TobjLancamentosFC.Create;
begin
      Lancamentos := TFLancamentosFrentedeCaixa.Create(nil);
      self.ObjQuery := TIBQuery.Create(nil);
      self.ObjQuery.Database := FDataModulo.IBDatabase;
      ObjDatasource := TDataSource.Create(nil);
      ObjDatasource.DataSet := self.ObjQuery;
      self.objQueryAuxiliar := TIBQuery.Create(nil);
      self.objQueryAuxiliar.Database := FDataModulo.IBDatabase;
      self.ObjQueryTEMP := TIBQuery.Create(nil);
      self.ObjQueryTEMP.Database := FDataModulo.IBDatabase;
      objValores := TObjValores.Create;
      Self.CarregaParametros;
      Lancamentos.PassaObjeto(Self);
      Lancamentos.ShowModal;

end;

destructor TobjLancamentosFC.Destroy;
begin
      FreeAndNil(Lancamentos);
      FreeAndNil(Self.Objquery);
      FreeAndNil(Self.Objqueryauxiliar);
      FreeAndNil(Self.Objquerytemp);
      FreeAndNil(Self.Objdatasource);
      objValores.Free;

end;

function TobjLancamentosFC.Executa(pComando: string): Boolean;
begin
      Result := False;
      with ObjQuery do
      begin
            Close;
            SQL.Clear;
            SQL.Text := pComando;
            try
                  Open;
            except
                  on e:Exception do
                  begin
                        LogAdd(e.Message);
                        Result := false;
                  end;
            end;

      end;

end;

procedure TobjLancamentosFC.LogAdd(parametro: string);
begin
      Memo.Lines.Add(parametro);
end;

function TobjLancamentosFC.CarregaParametros: Boolean;
begin
     result := false;
     If (ObjLanctoPortadorGlobal.Lancamento.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','QUITA��O')=False)
     Then Begin
               MensagemErro('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="QUITA��O", n�o foi encontrado!');
               exit;
     End;
     ObjLanctoPortadorGlobal.Lancamento.ObjGeraLancamento.TabelaparaObjeto;
     PCodigoquitacao:=ObjLanctoPortadorGlobal.Lancamento.ObjGeraLancamento.TipoLAncto.Get_CODIGO;
     result := True;
end;


end.
