object FCADASTRARELATORIOPERSONALIZADO: TFCADASTRARELATORIOPERSONALIZADO
  Left = 315
  Top = 217
  Width = 1005
  Height = 688
  Caption = 'Cadastro de Relat'#243'rios Personalizados'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnMouseMove = FormMouseMove
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Guia: TTabbedNotebook
    Left = 0
    Top = 57
    Width = 989
    Height = 545
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = 10643006
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    OnChange = GuiaChange
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object LbCODIGO: TLabel
        Left = 16
        Top = 12
        Width = 39
        Height = 14
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbSubMenu: TLabel
        Left = 125
        Top = 12
        Width = 56
        Height = 14
        Caption = 'Sub-Menu'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbNomeSubMenu: TLabel
        Left = 232
        Top = 31
        Width = 56
        Height = 14
        Caption = 'Sub-Menu'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbNome: TLabel
        Left = 16
        Top = 56
        Width = 37
        Height = 13
        Caption = 'Nome'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbSqlPrincipal: TLabel
        Left = 16
        Top = 208
        Width = 161
        Height = 14
        Caption = 'Sql Principal ( "SQL + WHERE")'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object LbSqlRepeticao: TLabel
        Left = 16
        Top = 312
        Width = 165
        Height = 14
        Caption = 'Sql Repeti'#231#227'o ("SQL + WHERE")'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label1: TLabel
        Left = 16
        Top = 96
        Width = 329
        Height = 14
        Caption = 'Tabela ou Sql Principal "sem filtros" para configurar o layout'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label2: TLabel
        Left = 16
        Top = 150
        Width = 336
        Height = 14
        Caption = 'Tabela ou Sql Repeti'#231#227'o "sem filtros" para configurar o layout'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 642
        Top = 12
        Width = 60
        Height = 14
        Caption = 'Permiss'#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbnomepermissao: TLabel
        Left = 749
        Top = 31
        Width = 56
        Height = 14
        Caption = 'Sub-Menu'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 401
        Top = 11
        Width = 136
        Height = 14
        Caption = 'Order by Personalizado?'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 16
        Top = 413
        Width = 127
        Height = 14
        Caption = 'Sql Group By Repeti'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbVisualizar: TLabel
        Left = 822
        Top = 76
        Width = 75
        Height = 19
        Cursor = crHandPoint
        Caption = 'Visualizar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btrelatoriosClick
        OnMouseMove = lbSqlUltimaExecucaoMouseMove
        OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
      end
      object lbImportarExportar: TLabel
        Left = 800
        Top = 102
        Width = 134
        Height = 19
        Cursor = crHandPoint
        Caption = '&Importar/Exportar'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btBTImportarExportarClick
        OnMouseMove = lbSqlUltimaExecucaoMouseMove
        OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
      end
      object lbSqlUltimaExecucao: TLabel
        Left = 793
        Top = 153
        Width = 157
        Height = 19
        Cursor = crHandPoint
        Caption = 'Sql '#218'ltima Exec&u'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        OnMouseMove = lbSqlUltimaExecucaoMouseMove
        OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
      end
      object lbConfigura: TLabel
        Left = 821
        Top = 128
        Width = 76
        Height = 19
        Cursor = crHandPoint
        Caption = 'Config&ura'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = btconfiguraClick
        OnMouseMove = lbSqlUltimaExecucaoMouseMove
        OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
      end
      object EdtCODIGO: TEdit
        Left = 16
        Top = 28
        Width = 100
        Height = 19
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 9
        ParentFont = False
        TabOrder = 0
      end
      object EdtSubMenu: TEdit
        Left = 125
        Top = 28
        Width = 100
        Height = 19
        Color = 6073854
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 9
        ParentFont = False
        TabOrder = 1
        OnExit = edtSubMenuExit
        OnKeyDown = edtSubMenuKeyDown
      end
      object EdtNome: TEdit
        Left = 16
        Top = 72
        Width = 728
        Height = 19
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 100
        ParentFont = False
        TabOrder = 4
      end
      object memoSqlPrincipal: TMemo
        Left = 16
        Top = 227
        Width = 724
        Height = 81
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        Lines.Strings = (
          'memoSqlPrincipal')
        ParentFont = False
        TabOrder = 7
        OnKeyPress = memoSqlPrincipalKeyPress
      end
      object memoSqlRepeticao: TMemo
        Left = 16
        Top = 330
        Width = 724
        Height = 77
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        Lines.Strings = (
          'memoSqlRepeticao')
        ParentFont = False
        TabOrder = 8
        OnKeyPress = memoSqlRepeticaoKeyPress
      end
      object edtpermissao: TEdit
        Left = 642
        Top = 28
        Width = 100
        Height = 19
        Color = 6073854
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        MaxLength = 9
        ParentFont = False
        TabOrder = 2
        OnExit = edtpermissaoExit
        OnKeyDown = edtpermissaoKeyDown
      end
      object comboorderbypersonalizado: TComboBox
        Left = 401
        Top = 28
        Width = 145
        Height = 21
        BevelKind = bkSoft
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        TabOrder = 3
        Items.Strings = (
          'N'#227'o'
          'Sim')
      end
      object memosqlgroupby: TMemo
        Left = 16
        Top = 431
        Width = 724
        Height = 70
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        OnKeyPress = memosqlgroupbyKeyPress
      end
      object btextrai13memorepeticao: TButton
        Left = 756
        Top = 334
        Width = 101
        Height = 25
        Caption = 'Extrair sem #13'
        TabOrder = 10
        OnClick = btextrai13memorepeticaoClick
      end
      object btextrai13memoprincipal: TButton
        Left = 756
        Top = 232
        Width = 101
        Height = 25
        Caption = 'Extrair sem #13'
        TabOrder = 11
        OnClick = btextrai13memoprincipalClick
      end
      object memotabelaprincipal: TMemo
        Left = 16
        Top = 112
        Width = 725
        Height = 38
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        Lines.Strings = (
          'memoSqlPrincipal')
        ParentFont = False
        TabOrder = 5
        OnKeyPress = memoSqlPrincipalKeyPress
      end
      object memotabelarepeticao: TMemo
        Left = 16
        Top = 168
        Width = 725
        Height = 38
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        Lines.Strings = (
          'memoSqlPrincipal')
        ParentFont = False
        TabOrder = 6
        OnKeyPress = memotabelarepeticaoKeyPress
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Comandos Order By'
      object panelorderby: TPanel
        Left = 0
        Top = 0
        Width = 981
        Height = 190
        Align = alTop
        Color = 10643006
        TabOrder = 0
        object Lbordemdefault: TLabel
          Left = 133
          Top = 104
          Width = 94
          Height = 13
          Caption = 'Ordem Default'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbordem: TLabel
          Left = 9
          Top = 104
          Width = 43
          Height = 13
          Caption = 'Ordem'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbcomando: TLabel
          Left = 9
          Top = 59
          Width = 60
          Height = 13
          Caption = 'Comando'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label8: TLabel
          Left = 9
          Top = 16
          Width = 37
          Height = 13
          Caption = 'Nome'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object LbCodigoOrderBy: TLabel
          Left = 709
          Top = 12
          Width = 40
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = []
          ParentFont = False
        end
        object Edtordemdefaultorderby: TEdit
          Left = 133
          Top = 120
          Width = 100
          Height = 19
          MaxLength = 9
          TabOrder = 3
        end
        object Edtordemorderby: TEdit
          Left = 9
          Top = 120
          Width = 100
          Height = 19
          MaxLength = 9
          TabOrder = 2
        end
        object edtcomandoorderby: TEdit
          Left = 9
          Top = 75
          Width = 736
          Height = 19
          TabOrder = 1
        end
        object edtnomeorderby: TEdit
          Left = 9
          Top = 32
          Width = 560
          Height = 19
          MaxLength = 100
          TabOrder = 0
        end
        object pnl2: TPanel
          Left = 1
          Top = 149
          Width = 979
          Height = 40
          Align = alBottom
          BevelOuter = bvNone
          Color = 14024703
          TabOrder = 4
          object btBtgravarOrderBy: TBitBtn
            Left = 852
            Top = 1
            Width = 39
            Height = 39
            TabOrder = 0
            OnClick = btBtgravarOrderByClick
          end
          object btcancelarnovoorderby: TBitBtn
            Left = 891
            Top = 1
            Width = 39
            Height = 39
            TabOrder = 1
            OnClick = btcancelarnovoorderbyClick
          end
          object btexcluirorderby: TBitBtn
            Left = 930
            Top = 1
            Width = 39
            Height = 39
            TabOrder = 2
            OnClick = btexcluirorderbyClick
          end
        end
      end
      object Dbgridorderby: TDBGrid
        Left = 0
        Top = 190
        Width = 981
        Height = 95
        Align = alClient
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'Verdana'
        TitleFont.Style = []
        OnDblClick = DbgridorderbyDblClick
        OnKeyPress = DbgridorderbyKeyPress
      end
      object Panel: TPanel
        Left = 0
        Top = 285
        Width = 981
        Height = 232
        Align = alBottom
        TabOrder = 2
        object botaosetacima: TSpeedButton
          Left = 220
          Top = 64
          Width = 26
          Height = 22
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333000333
            3333333333777F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333FF7F7FFFF333333000090000
            3333333777737777F333333099999990333333373F3333373333333309999903
            333333337F33337F33333333099999033333333373F333733333333330999033
            3333333337F337F3333333333099903333333333373F37333333333333090333
            33333333337F7F33333333333309033333333333337373333333333333303333
            333333333337F333333333333330333333333333333733333333}
          NumGlyphs = 2
          OnClick = botaosetacimaClick
        end
        object botaosetabaixo: TSpeedButton
          Left = 220
          Top = 88
          Width = 26
          Height = 24
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333303333
            333333333337F33333333333333033333333333333373F333333333333090333
            33333333337F7F33333333333309033333333333337373F33333333330999033
            3333333337F337F33333333330999033333333333733373F3333333309999903
            333333337F33337F33333333099999033333333373333373F333333099999990
            33333337FFFF3FF7F33333300009000033333337777F77773333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333309033333333333337F7F333333333333090333
            33333333337F7F33333333333300033333333333337773333333}
          NumGlyphs = 2
          OnClick = botaosetabaixoClick
        end
        object ListBoxOrderBy: TListBox
          Left = 8
          Top = 8
          Width = 201
          Height = 217
          DragMode = dmAutomatic
          ItemHeight = 13
          TabOrder = 0
          OnDragDrop = ListBoxOrderByDragDrop
          OnDragOver = ListBoxOrderByDragOver
          OnMouseDown = ListBoxOrderByMouseDown
        end
        object BtGravarOrdem: TButton
          Left = 216
          Top = 176
          Width = 97
          Height = 25
          Caption = 'Gravar Ordem'
          TabOrder = 1
          OnClick = BtGravarOrdemClick
        end
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&3 - Filtros'
      object panelfiltro: TPanel
        Left = 0
        Top = 0
        Width = 981
        Height = 433
        Align = alTop
        Color = 10643006
        TabOrder = 0
        object lbcodigofiltro: TLabel
          Left = 712
          Top = 12
          Width = 44
          Height = 13
          Caption = 'C'#243'digo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbtipo: TLabel
          Left = 520
          Top = 10
          Width = 28
          Height = 13
          Caption = 'Tipo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 5
          Top = 10
          Width = 200
          Height = 13
          Caption = 'Nome (Vari'#225'vel do Par'#226'metro)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lblabel: TLabel
          Left = 127
          Top = 58
          Width = 88
          Height = 13
          Caption = 'Caption Label'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbmascara: TLabel
          Left = 5
          Top = 58
          Width = 54
          Height = 13
          Caption = 'M'#225'scara'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbrequerido: TLabel
          Left = 408
          Top = 57
          Width = 66
          Height = 13
          Caption = 'Requerido'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbtabelachaveestrangeira: TLabel
          Left = 4
          Top = 170
          Width = 184
          Height = 13
          Caption = 'Comando Chave Estrangeira'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbcampochavestrangeira: TLabel
          Left = 4
          Top = 234
          Width = 168
          Height = 13
          Caption = 'Campo Chave Estrangeira'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbcomandoandprincipal: TLabel
          Left = 4
          Top = 281
          Width = 207
          Height = 13
          Caption = 'Comando "and" do Sql Principal'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Lbcomandoandsecundario: TLabel
          Left = 3
          Top = 345
          Width = 759
          Height = 26
          Caption = 
            'Comando "and" do Sql Repeti'#231#227'o  ( Em caso de m'#250'ltipla escolha di' +
            'gite apenas o nome do par'#226'metro Ex:   :pcodigo)  Usar :nome no c' +
            'omando da vari'#225'vel'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        object Label4: TLabel
          Left = 486
          Top = 57
          Width = 151
          Height = 13
          Caption = 'Usa em branco(vazia)?'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label9: TLabel
          Left = 5
          Top = 116
          Width = 290
          Height = 13
          Caption = 'M'#250'lt.Escolha? (Somente para Tipos Inteiros)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label10: TLabel
          Left = 302
          Top = 116
          Width = 329
          Height = 13
          Caption = 'Tabela.Campo usado no comando m'#250'ltipla Escolha'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Verdana'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbGravar: TLabel
          Left = 841
          Top = 301
          Width = 64
          Height = 22
          Cursor = crHandPoint
          Caption = 'Gravar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btgravarfiltrosClick
          OnMouseMove = lbSqlUltimaExecucaoMouseMove
          OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
        end
        object lbCancelarNovo: TLabel
          Left = 806
          Top = 330
          Width = 135
          Height = 22
          Cursor = crHandPoint
          Caption = 'Cancelar\novo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btcancelarfiltrosClick
          OnMouseMove = lbSqlUltimaExecucaoMouseMove
          OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
        end
        object lb4: TLabel
          Left = 841
          Top = 389
          Width = 65
          Height = 22
          Cursor = crHandPoint
          Caption = 'Excluir'
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btexcluirfiltrosClick
          OnMouseMove = lbSqlUltimaExecucaoMouseMove
          OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
        end
        object lb5: TLabel
          Left = 835
          Top = 359
          Width = 76
          Height = 22
          Cursor = crHandPoint
          Caption = 'Replicar'
          Font.Charset = ANSI_CHARSET
          Font.Color = clLime
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = btreplicarfiltroClick
          OnMouseMove = lbSqlUltimaExecucaoMouseMove
          OnMouseLeave = lbSqlUltimaExecucaoMouseLeave
        end
        object edtnomefiltro: TEdit
          Left = 5
          Top = 26
          Width = 497
          Height = 19
          MaxLength = 50
          TabOrder = 0
        end
        object Edtcaptionlabelfiltro: TEdit
          Left = 130
          Top = 74
          Width = 273
          Height = 19
          MaxLength = 50
          TabOrder = 3
        end
        object Edtmascarafiltro: TEdit
          Left = 5
          Top = 74
          Width = 121
          Height = 19
          MaxLength = 50
          TabOrder = 2
        end
        object Edtcampochavestrangeirafiltro: TEdit
          Left = 4
          Top = 250
          Width = 161
          Height = 19
          MaxLength = 100
          TabOrder = 9
        end
        object combotipofiltro: TComboBox
          Left = 520
          Top = 25
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Text = '   '
          Items.Strings = (
            'INTEIRO'
            'DECIMAL'
            'DATA'
            'HORA'
            'DATA E HORA'
            'STRING')
        end
        object comborequeridofiltro: TComboBox
          Left = 408
          Top = 72
          Width = 73
          Height = 21
          ItemHeight = 13
          TabOrder = 4
          Text = '  '
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object memocomandosqlchaveestrangeirafiltro: TMemo
          Left = 4
          Top = 185
          Width = 649
          Height = 43
          Lines.Strings = (
            'memocomandosqlchaveestrangeira')
          MaxLength = 500
          TabOrder = 8
        end
        object memocomandoandprincipalfiltro: TMemo
          Left = 4
          Top = 297
          Width = 649
          Height = 43
          Lines.Strings = (
            'memocomandoandprincipal')
          TabOrder = 10
        end
        object memocomandoandrepeticaofiltro: TMemo
          Left = 3
          Top = 375
          Width = 649
          Height = 48
          Lines.Strings = (
            'memocomandoandprincipal')
          TabOrder = 11
        end
        object combousamesmoembrancofiltro: TComboBox
          Left = 486
          Top = 72
          Width = 97
          Height = 21
          ItemHeight = 13
          TabOrder = 5
          Text = '  '
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object combomultiplaescolhafiltro: TComboBox
          Left = 5
          Top = 131
          Width = 82
          Height = 21
          ItemHeight = 13
          TabOrder = 6
          Text = '  '
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object edtcampomultiplaescolhafiltro: TEdit
          Left = 301
          Top = 131
          Width = 103
          Height = 19
          TabOrder = 7
        end
      end
      object Dbgridfiltro: TDBGrid
        Left = 0
        Top = 433
        Width = 981
        Height = 84
        Align = alClient
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clNavy
        TitleFont.Height = -11
        TitleFont.Name = 'Verdana'
        TitleFont.Style = []
        OnDblClick = DbgridfiltroDblClick
        OnKeyPress = DbgridfiltroKeyPress
      end
    end
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 989
    Height = 57
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    TabOrder = 1
    DesignSize = (
      989
      57)
    object lbnomeformulario: TLabel
      Left = 591
      Top = 3
      Width = 195
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigoPedido: TLabel
      Left = 826
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 631
      Top = 25
      Width = 130
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Personalizados'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btPesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btExcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btCancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btAlterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btBtnovoClick
      Spacing = 0
    end
    object btSair: TBitBtn
      Left = 300
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 602
    Width = 989
    Height = 48
    Align = alBottom
    Color = clMedGray
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    DesignSize = (
      989
      48)
    object imgrodape: TImage
      Left = 1
      Top = 0
      Width = 987
      Height = 47
      Align = alBottom
      Stretch = True
    end
    object lb9: TLabel
      Left = 628
      Top = 11
      Width = 275
      Height = 20
      Anchors = [akTop, akRight]
      Caption = 'Existem X Relat'#243'rios Cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object SaveDialog1: TSaveDialog
    Left = 380
    Top = 128
  end
end
