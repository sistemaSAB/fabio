unit UrelModelo;

interface
uses classes,sysutils,ibquery;
Type
   TObjRelModelo=class
          Public
                SQL:TSTringList;
                Procedure RelModelo;
                constructor create;
                destructor free;
          Private
                Objquery:Tibquery;

   End;




implementation

uses UessencialGlobal, UMostraBarraProgresso, UReltxtRDPRINT,rdprint,forms;

{ TObjRelModelo }

constructor TObjRelModelo.create;
begin
     Try
        Self.Objquery:=Tibquery.Create(nil);
        Self.SQL:=TSTringList.create;
        Self.SQL.clear;
     Except
           MensagemErro('Erro na tentativa de Criar a Query');
           exit;
     End;

end;

destructor TObjRelModelo.free;
begin
     Freeandnil(Self.Objquery);
     Freeandnil(Self.sql);
end;

procedure TObjRelModelo.RelModelo;
begin

     Try
       With Objquery do
       Begin
            close;
            sql.clear;
            sql.text:=Self.SQL.Text;
            open;
            if (Recordcount=0)
            Then Begin
                      MensagemAviso('Nenhuma Informa��o foi selecionada');
                      exit;
            End;
            last;
            FMostraBarraProgresso.ConfiguracoesIniciais(recordcount,0);
            FMostraBarraProgresso.Lbmensagem.caption:='Gerando Relat�rio';
            first;
       
            With Freltxtrdprint do
            Begin
                 ConfiguraImpressao;
                 RDprint.TamanhoQteColunas:=130;
                 RDprint.FonteTamanhoPadrao:=S17cpp;
                 LinhaLocal:=3;
                 if (rdprint.setup=False)
                 Then Begin
                           rdprint.fechar;
                           exit;
                 End;
                 
                 RDprint.ImpC(linhalocal,65,'',[negrito]);
                 IncrementaLinha(2);
       
                 While (Objquery.Eof) do
                 Begin
                      FMostraBarraProgresso.IncrementaBarra1(1);
                      FMostraBarraProgresso.show;
                      Application.ProcessMessages;
       
                      Objquery.next;
                 End;
       
                 rdprint.fechar;
            End;
       End;
     Finally
            FMostraBarraProgresso.close;
     End;
end;

end.
