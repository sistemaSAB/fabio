inherited FrmCadastroBase: TFrmCadastroBase
  Left = 493
  Top = 250
  Caption = 'FrmCadastroBase'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 784
    Height = 68
    ButtonHeight = 21
    ButtonWidth = 60
    Caption = 'ToolBar1'
    Ctl3D = False
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 0
    object btNovo: TToolButton
      Left = 0
      Top = 2
      Hint = 'Novo'
      Caption = '&Novo'
      ImageIndex = 0
      OnClick = btNovoClick
    end
    object ToolButton2: TToolButton
      Left = 60
      Top = 2
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object btAlterar: TToolButton
      Left = 68
      Top = 2
      Hint = 'Alterar'
      Caption = '&Alterar'
      ImageIndex = 1
      OnClick = btAlterarClick
    end
    object ToolButton4: TToolButton
      Left = 128
      Top = 2
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object btGravar: TToolButton
      Left = 136
      Top = 2
      Hint = 'Gravar'
      Caption = '&Gravar'
      ImageIndex = 2
      OnClick = btGravarClick
    end
    object ToolButton6: TToolButton
      Left = 196
      Top = 2
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 3
      Style = tbsSeparator
    end
    object btCancelar: TToolButton
      Left = 204
      Top = 2
      Caption = '&Cancelar'
      ImageIndex = 6
      OnClick = btCancelarClick
    end
    object ToolButton1: TToolButton
      Left = 264
      Top = 2
      Width = 8
      Caption = 'ToolButton1'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btExcluir: TToolButton
      Left = 272
      Top = 2
      Hint = 'Excluir'
      Caption = '&Excluir'
      ImageIndex = 4
      OnClick = btExcluirClick
    end
    object ToolButton8: TToolButton
      Left = 332
      Top = 2
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object btPesquisar: TToolButton
      Left = 340
      Top = 2
      Hint = 'Pesquisar'
      Caption = '&Pesquisar'
      ImageIndex = 3
      OnClick = btPesquisarClick
    end
    object ToolButton3: TToolButton
      Left = 400
      Top = 2
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btRelatorios: TToolButton
      Left = 408
      Top = 2
      Caption = '&Relatorios'
      ImageIndex = 6
    end
    object ToolButton11: TToolButton
      Left = 468
      Top = 2
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btOpcoes: TToolButton
      Left = 476
      Top = 2
      Caption = '&Opcoes'
      ImageIndex = 6
      OnClick = btOpcoesClick
    end
    object ToolButton5: TToolButton
      Left = 536
      Top = 2
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object btSair: TToolButton
      Left = 544
      Top = 2
      Hint = 'Sair'
      Caption = '&Sair'
      ImageIndex = 5
      OnClick = btSairClick
    end
  end
end
