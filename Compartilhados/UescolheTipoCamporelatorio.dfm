object FescolheTipoCamporelatorio: TFescolheTipoCamporelatorio
  Left = 192
  Top = 107
  Width = 250
  Height = 280
  Caption = 'Escolha o tipo do Campo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RGTIPO: TRadioGroup
    Left = 0
    Top = 0
    Width = 242
    Height = 246
    Align = alClient
    ItemIndex = 0
    Items.Strings = (
      'Label'
      'Campo do Banco de Dados')
    TabOrder = 0
  end
end
