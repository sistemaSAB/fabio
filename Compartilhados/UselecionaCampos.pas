unit UselecionaCampos;

interface

uses
  Ulabelrel,ibquery,db,Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, CheckLst, Buttons, ExtCtrls, ComCtrls, UobjCAMPOSRELATORIO,UessencialGlobal;

type
  TFselecionaCampos = class(TForm)
    Abas: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CheckCampos: TCheckListBox;
    btselecionatodos: TButton;
    painelextra: TPanel;
    Check01: TCheckBox;
    LabeledEdit1: TLabeledEdit;
    btcancel: TBitBtn;
    btok: TBitBtn;
    Label1: TLabel;
    ListaNomeOriginal: TListBox;
    ListaNovoNome: TListBox;
    TabSheet3: TTabSheet;
    ComboRelSalvos: TComboBox;
    Label2: TLabel;
    abrirrelsalvo: TBitBtn;
    edtnovorel: TEdit;
    Label3: TLabel;
    btsalvarnovorel: TBitBtn;
    BitBtn1: TBitBtn;
    procedure btselecionatodosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btokClick(Sender: TObject);
    procedure btcancelClick(Sender: TObject);
    procedure ListaNovoNomeKeyPress(Sender: TObject; var Key: Char);
    procedure ListaNovoNomeClick(Sender: TObject);
    procedure ListaNomeOriginalClick(Sender: TObject);
    procedure ListaNovoNomeDblClick(Sender: TObject);
    procedure ListaNomeOriginalKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btsalvarnovorelClick(Sender: TObject);
    procedure abrirrelsalvoClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    NomeRelatorio:String;
    Procedure RecuperaNomes;

  public
    { Public declarations }
    Procedure Submit_NomeRelatorio(parametro:Str100);
    Procedure ResgataCampos(PObjquery:tibquery);
  end;

var
  FselecionaCampos: TFselecionaCampos;
  ObjCamposRelatorio:TobjCamposRelatorio;
implementation

uses UdesenhaRelatorio, Uregua;



{$R *.dfm}

procedure TFselecionaCampos.btselecionatodosClick(Sender: TObject);
var
cont:integer;
apoio:boolean;
begin
     apoio:=not(CheckCampos.Checked[0]);

     for cont:=0 to CheckCampos.Items.count-1 do
     Begin
          CheckCampos.Checked[cont]:=apoio;
     End;
end;

procedure TFselecionaCampos.FormShow(Sender: TObject);
begin

     PegaFiguraBotao(btok,'BOTAOOK.BMP');
     PegaFiguraBotao(btcancel,'BOTAOCANCELAR.BMP');
     Abas.ActivePageIndex:=0;
     Self.RecuperaNomes;
     tag:=0;
     Abas.Enabled:=True;
     ComboRelSalvos.Items.clear;
     ComboRelSalvos.Text:='';

     Try
        ObjCamposRelatorio:=nil;
        ObjCamposRelatorio:=TObjCAMPOSRELATORIO.Create;
        ObjCamposRelatorio.ResgataRelSalvos(Self.NomeRelatorio,ComboRelSalvos.Items);

     Except
           Messagedlg('Erro na Tentativa de Criar o Objeto de Configura��o de Campos de Relat�rios!',mterror,[mbok],0);
           Abas.Enabled:=False;
           exit;
     End;

end;

procedure TFselecionaCampos.btokClick(Sender: TObject);
begin
     tag:=1;
     Self.Close;
end;

procedure TFselecionaCampos.btcancelClick(Sender: TObject);
begin
     Self.Tag:=0;
     Self.close;
end;

procedure TFselecionaCampos.RecuperaNomes;
var
cont:Integer;
begin
     //esse procecimento utiliza o checkcampos
     //como padrao e preencha os dpois cheques de
     //nomes com os mesmos nomes

     Self.ListaNovoNome.Items.clear;
     Self.ListaNomeOriginal.Items.clear;
     for cont:=0 to CheckCampos.Items.count-1 do
     Begin
          Self.ListaNovoNome.Items.add(CheckCampos.Items[cont]);
          Self.ListaNomeOriginal.Items.add(CheckCampos.Items[cont]);
     End;


end;

procedure TFselecionaCampos.ListaNovoNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Begin
               Self.ListaNovoNomeDblClick(sender);
     End
     Else ListaNomeOriginal.ItemIndex:=ListaNovoNome.ItemIndex;
end;

procedure TFselecionaCampos.ListaNovoNomeClick(Sender: TObject);
begin
     ListaNomeOriginal.ItemIndex:=ListaNovoNome.ItemIndex;
end;

procedure TFselecionaCampos.ListaNomeOriginalClick(Sender: TObject);
begin
ListaNovoNome.ItemIndex:=ListaNomeOriginal.ItemIndex;
end;

procedure TFselecionaCampos.ListaNovoNomeDblClick(Sender: TObject);
var
novonome:String;
begin
    novonome:=Self.ListaNomeOriginal.Items[Self.ListaNomeOriginal.itemindex];
    if (inputquery('Troca nome do campo','Digite o novo nome',novonome)=false)
    Then exit;
    Self.ListaNovoNome.Items[Self.ListaNomeOriginal.ItemIndex]:=novonome;
end;

procedure TFselecionaCampos.ListaNomeOriginalKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     Then Begin
               Self.ListaNovoNomeDblClick(sender);
     End
     Else ListaNovoNome.ItemIndex:=ListaNomeOriginal.ItemIndex;
end;

procedure TFselecionaCampos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (Abas.Enabled=false)
     Then Self.Tag:=0; //mesma coisa que clicar no cancelar

     if (ObjCamposRelatorio<>nil)
     Then ObjCamposRelatorio.Free;
end;

procedure TFselecionaCampos.Submit_NomeRelatorio(parametro: Str100);
begin
     Self.nomerelatorio:=parametro;
end;

procedure TFselecionaCampos.btsalvarnovorelClick(Sender: TObject);
var
cont:Integer;
begin
     //primeiro processo
     //pegar cada campo que sera usado
     //abrir um novo registro com o numero de indice dele e o novo nome

     ObjCamposRelatorio.excluipornome(Self.NomeRelatorio,edtnovorel.Text);
     for cont:=0 to Self.CheckCampos.Items.count-1 do
     Begin
         if Self.CheckCampos.checked[cont]=True
         Then Begin
                   ObjCamposRelatorio.ZerarTabela;
                   ObjCamposRelatorio.Status:=dsinsert;
                   ObjCamposRelatorio.Submit_CODIGO('0');
                   ObjCamposRelatorio.Submit_NomeRelSistema(uppercase(self.NomeRelatorio));
                   ObjCamposRelatorio.Submit_NomeRelatorio(edtnovorel.text);
                   ObjCamposRelatorio.Submit_IndiceCampo(inttostr(cont));
                   ObjCamposRelatorio.Submit_NomeCampo(ListaNovoNome.Items[cont]);
                   if (ObjCamposRelatorio.Salvar(true)=False)
                   Then Begin
                             Messagedlg('Erro na tentativa de Salvar',mterror,[mbok],0);
                             exit;
                   End;
         End;
     End;

     ObjCamposRelatorio.ResgataRelSalvos(Self.NomeRelatorio,ComboRelSalvos.Items);
     Messagedlg('Salvo com Sucesso',mtInformation,[mbok],0);
end;
                   
procedure TFselecionaCampos.abrirrelsalvoClick(Sender: TObject);
begin
     Self.CheckCampos.Checked[0]:=True;
     //tirando a sele��o de todos
     Self.btselecionatodosClick(sender);
     //restaurando o nome original
     Self.RecuperaNomes;
     
     ObjCamposRelatorio.LocalizaRelatorio(Self.NomeRelatorio,ComboRelSalvos.Text,CheckCampos,ListaNovoNome);
     Messagedlg('Abertura conclu�da',mtInformation,[mbok],0);
     
end;

procedure TFselecionaCampos.ResgataCampos(PObjquery: tibquery);
var
cont:integer;
begin
     //pegando o nome dos campos para o checkbox
     Self.CheckCampos.Items.clear;
     for cont:=0 to PObjquery.FieldCount-1 do
     Begin
          Self.CheckCampos.Items.add(PObjquery.Fields[cont].DisplayName);
     End;
end;

procedure TFselecionaCampos.BitBtn1Click(Sender: TObject);
var
Cont:integer;
linhaatual:integer;
begin


     With FdesenhaRelatorio do
     Begin
          Conta_label_usada_gb:=0;
          linhaatual:=1;
          Desenha_regua(imagerel);
          //criando as labels para o rotulo
          for cont:=0 to CheckCampos.Items.count-1 do
          Begin
               if (CheckCampos.Checked[cont])
               Then Begin
                         //rotulo
                         VetorLabel[Conta_label_usada_gb]:=Tlabelrel.create(nil);
                         VetorLabel[Conta_label_usada_gb].Parent := FdesenhaRelatorio;
                         VetorLabel[Conta_label_usada_gb].height := 30;
                         VetorLabel[Conta_label_usada_gb].width := 120;
                         VetorLabel[Conta_label_usada_gb].caption := ListaNovoNome.Items[cont];
                         VetorLabel[Conta_label_usada_gb].rotulo:=VetorLabel[Conta_label_usada_gb].caption; 
                         VetorLabel[Conta_label_usada_gb].Reposiciona(1,LinhaAtual);
                         VetorLabel[Conta_label_usada_gb].Font.Name:='Courier New';
                         VetorLabel[Conta_label_usada_gb].Font.Size:=8;

                         VetorLabel[Conta_label_usada_gb].Transparent:=True;
                         VetorLabel[Conta_label_usada_gb].tipo:='R';//r�tulo
                         VetorLabel[Conta_label_usada_gb].nomecampoBD:='';
                         VetorLabel[Conta_label_usada_gb].QuantidadeCaracteres:=length(VetorLabel[Conta_label_usada_gb].Caption);
                         VetorLabel[Conta_label_usada_gb].posicaovetor:=Conta_label_usada_gb;
                         VetorLabel[Conta_label_usada_gb].OnClick:=ClicaLabel;
                         
                         //Conta quantas labels foram usadas
                         inc(Conta_label_usada_gb,1);

                         //Label de Dados
                         VetorLabel[Conta_label_usada_gb]:=Tlabelrel.create(nil);
                         VetorLabel[Conta_label_usada_gb].Parent := FdesenhaRelatorio;
                         VetorLabel[Conta_label_usada_gb].height := 30;
                         VetorLabel[Conta_label_usada_gb].width := 120;
                         VetorLabel[Conta_label_usada_gb].caption := checkcampos.Items[cont];
                         VetorLabel[Conta_label_usada_gb].rotulo:=VetorLabel[Conta_label_usada_gb].caption;
                         VetorLabel[Conta_label_usada_gb].Reposiciona(30,linhaatual);
                         VetorLabel[Conta_label_usada_gb].QuantidadeCaracteres:=length(VetorLabel[Conta_label_usada_gb].rotulo);
                         VetorLabel[Conta_label_usada_gb].Font.Name:='Courier New';
                         VetorLabel[Conta_label_usada_gb].Font.Size:=8;
                         VetorLabel[Conta_label_usada_gb].Font.Color:=clRed;//para diferenciar
                         VetorLabel[Conta_label_usada_gb].Transparent:=True;
                         VetorLabel[Conta_label_usada_gb].nomecampoBD:=checkcampos.Items[cont];
                         VetorLabel[Conta_label_usada_gb].tipo:='D';//dados
                         VetorLabel[Conta_label_usada_gb].posicaovetor:=Conta_label_usada_gb;
                         VetorLabel[Conta_label_usada_gb].OnClick:=ClicaLabel;

                         //incrementando a linha
                         inc(linhaatual,1);
                         //Conta os Botoes
                         inc(Conta_label_usada_gb,1);
               End;
          End;

          ShowModal;
          //Anotando o Nome de Cada Label, esquerda, superior,
          //quantidade de caracteres, campo que sera usado 
          //desalocando a memoria das labels usadas
          for cont:=Conta_label_usada_gb-1 downto 0 do
          Begin
              Freeandnil(VetorLabel[cont]);
          End;
     End;



end;

end.
