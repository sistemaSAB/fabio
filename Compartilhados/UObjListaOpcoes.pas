{jonas}

unit UObjListaOpcoes;

interface

uses Forms,StdCtrls,Buttons,Classes,Graphics,Windows,ExtCtrls,IBQuery,Dialogs;

type TObjListaOpcoes = class

  private

    formulario     :TForm;
    painelPesquisa :TPanel;
    editPesquisa   :TEdit;
    titulo         :string;
    lista          :TListBox;
    objetoGerador  :TObject;
    itens          :TStringList;
    itensPesquisado:TStringList;
    //itens2          :TIBQuery;
    orientacao     :string;

    posX           :integer;
    PosY           :Integer;
    posicao        :TPoint;
    incrementoX    :Integer;
    incrementoY    :integer;
    ind            :Integer;

    procedure adicionaItens ();
    procedure adicionaItensPesquisado ();
    procedure get_posicaoMouse ();

    {eventos}
    procedure pressionaEnter          (Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure habilitaPesquisa        (Sender: TObject; var Key: Word;Shift: TShiftState);
    procedure redimensionaFormulario  (Sender: TObject);
    procedure carregaItensPesquisado  (parametro:string);

  public

    constructor create(objetoGerador:TObject);

    destructor  free();

    {submit}
    procedure submit_ObjetoGerador (objetoGerador:TObject);
    procedure submit_titulo        (titulo:string);
    procedure submit_itens          (itens:TStringList);
    procedure submit_font_estilo    (estilo:TFontStyle);
    procedure submit_font_tamanho   (tamanho:Integer);
    procedure submit_orientacao     (orientacao:string);
    procedure submit_incrementoX    (parametro:integer);
    procedure submit_incrementoY    (parametro:integer);
    procedure submit_altura         (parametro:integer);
    procedure submit_largura        (parametro:integer);

    {get}
    function get_formulario_posX    ():integer;
    function get_formulario_posY    ():Integer;
    function get_formulario_largura ():Integer;
    function get_formulario_altura  ():Integer;

    procedure showOpen (modal:boolean = true);
    function larguraProporcional ()  :integer;

end;

implementation

uses Controls, SysUtils, Types;

{ TObjListaOpcoes }

procedure TObjListaOpcoes.adicionaItens;
var
  i:integer;
begin

    lista.Items.Clear;
    
    for i :=0  to itens.Count-1  do
    begin

      lista.Items.Append (itens[i]);

    end;

    lista.ItemIndex := 0;


end;

constructor TObjListaOpcoes.create(objetoGerador:TObject);
begin

  {formulario (form)}
  formulario:=TForm.Create (nil);
  formulario.BorderStyle:=bsSizeToolWin;
  formulario.Height:=150;
  formulario.Width:=300;
  formulario.KeyPreview := True;
  formulario.OnKeyDown := habilitaPesquisa;
  formulario.OnResize := redimensionaFormulario;

  {painel pesquisa}
  painelPesquisa:=TPanel.Create (nil);
  painelPesquisa.Height := 22;
  painelPesquisa.Parent:=formulario;
  painelPesquisa.Align := alBottom;
  painelPesquisa.Caption :='';
  painelPesquisa.Visible := False;

  {editPesquisa}
  editPesquisa:=TEdit.Create (nil);
  editPesquisa.Height := painelPesquisa.Height;
  editPesquisa.Width  := formulario.Width-15;
  editPesquisa.Left   := painelPesquisa.Left;
  editPesquisa.Top    := painelPesquisa.Top;
  editPesquisa.Parent := painelPesquisa;
  editPesquisa.Text   := '';
  editPesquisa.OnKeyDown := pressionaEnter;


  {lista (listBox)}
  lista:=TListBox.Create(nil);
  lista.Parent:=formulario;
  lista.Align:=alClient;

  self.titulo     := 'Op��es';
  Self.orientacao := 'DIREITA';

  self.submit_ObjetoGerador (objetoGerador);
  self.incrementoX := 0;
  self.incrementoY := 0;

  ind:= 0;

  itensPesquisado := TStringList.Create;

end;


destructor TObjListaOpcoes.free;
begin

  FreeAndNil (self.formulario);
  FreeAndNil (itenspesquisado);

  //FreeAndNil (self.itens); {sera destruido pelo fun��o chamadora}
  //FreeAndNil (Self.lista); {aqui nao precisa destruir porque o parent de lista � o formulario, ao destruir o formulario ele destroi tamb�m seus filhos}

end;

procedure TObjListaOpcoes.showOpen (modal:boolean);
begin

  formulario.Caption := Self.titulo;

  self.get_posicaoMouse ();

  formulario.Left := posX;
  formulario.Top := PosY;

  self.adicionaItens();

  if not (modal) then
    formulario.Show
  else
    formulario.ShowModal;

end;

procedure TObjListaOpcoes.submit_ObjetoGerador(objetoGerador: TObject);
begin

  self.objetoGerador := objetoGerador;

end;

procedure TObjListaOpcoes.submit_titulo(titulo: string);
begin

  self.titulo := titulo;

end;

procedure TObjListaOpcoes.submit_font_estilo(estilo: TFontStyle);
begin

  self.lista.Font.Style := [estilo];

end;

procedure TObjListaOpcoes.submit_font_tamanho(tamanho:Integer);
begin

  self.lista.Font.Size := tamanho;

end;

procedure TObjListaOpcoes.submit_itens(itens: TStringList);
begin

  self.itens := itens;

end;

procedure TObjListaOpcoes.submit_orientacao(orientacao: string);
begin

  self.orientacao := UpperCase (orientacao);

end;

procedure TObjListaOpcoes.get_posicaoMouse();
begin

  GetCursorPos (self.posicao);

  posX := posicao.X;
  PosY := posicao.Y;

  posX := posX + incrementoX;
  PosY := PosY + incrementoY;


end;

procedure TObjListaOpcoes.submit_incrementoX(parametro: integer);
begin

  self.incrementoX := parametro;

end;

procedure TObjListaOpcoes.submit_incrementoY(parametro: integer);
begin

  self.incrementoY := parametro;

end;

function TObjListaOpcoes.get_formulario_posX: integer;
begin

  result := formulario.Left;

end;

function TObjListaOpcoes.get_formulario_posY: Integer;
begin

  result := formulario.Top;

end;

function TObjListaOpcoes.get_formulario_altura: Integer;
begin

  result := formulario.Height;

end;

function TObjListaOpcoes.get_formulario_largura: Integer;
begin

  result := formulario.Width;

end;

procedure TObjListaOpcoes.submit_altura(parametro: integer);
begin

  formulario.Height := parametro;

end;

procedure TObjListaOpcoes.submit_largura(parametro: integer);
begin

  formulario.Width := parametro;

end;

function TObjListaOpcoes.larguraProporcional: integer;
var
  i:Integer;
  maior:Integer;
begin

  maior := 0;
  //concluir


end;

procedure TObjListaOpcoes.pressionaEnter (Sender: TObject; var Key: Word;Shift: TShiftState);
var
  i:Integer;
begin

  if (Key = VK_RETURN) then
  begin

    for i := 0  to lista.Items.Count-1  do
    begin

       carregaItensPesquisado (editPesquisa.Text);

    end;

  end;

end;

procedure TObjListaOpcoes.habilitaPesquisa(Sender: TObject; var Key: Word;Shift: TShiftState);
begin

   if (Key = VK_SPACE) then
   begin

      painelPesquisa.Visible := True;
      editPesquisa.SetFocus;


   end else if (Key = VK_ESCAPE) then
   begin

      painelPesquisa.Visible := False;
      adicionaItens ();

   end;

end;

procedure TObjListaOpcoes.redimensionaFormulario(Sender: TObject);
begin

  editPesquisa.Width := formulario.Width - 15;

end;

procedure TObjListaOpcoes.carregaItensPesquisado(parametro: string);
var
  i:integer;
begin

  itensPesquisado.Clear;

  for i := 0  to itens.Count-1  do
  begin

    if (Pos (UpperCase (parametro),UpperCase (itens[i])) <> 0) then
      itensPesquisado.Add (itens[i]);

  end;

  lista.Items.Clear;
  adicionaItensPesquisado;


end;

procedure TObjListaOpcoes.adicionaItensPesquisado;
var
  i:integer;
begin

    lista.Items.Clear;
    for i :=0  to itensPesquisado.Count-1  do
      lista.Items.Append (itensPesquisado[i]);

    lista.ItemIndex := 0;



end;

end.
