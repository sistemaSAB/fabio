unit UConfordemcamposPesquisa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons;

type
  TFConfordemcamposPesquisa = class(TForm)
    LbCamposDisponiveis: TListBox;
    LbCamposSelecionados: TListBox;
    Label1: TLabel;
    Label2: TLabel;
    btinsere: TSpeedButton;
    btinseretodos: TSpeedButton;
    botaosetabaixo: TSpeedButton;
    botaosetacima: TSpeedButton;
    btconcluido: TBitBtn;
    btdelete: TSpeedButton;
    btcancelar: TBitBtn;
    procedure btinsereClick(Sender: TObject);
    procedure btinseretodosClick(Sender: TObject);
    procedure LbCamposSelecionadosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure botaosetacimaClick(Sender: TObject);
    procedure botaosetabaixoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btconcluidoClick(Sender: TObject);
    procedure LbCamposDisponiveisDblClick(Sender: TObject);
    procedure btdeleteClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FConfordemcamposPesquisa: TFConfordemcamposPesquisa;

implementation

{$R *.dfm}

procedure TFConfordemcamposPesquisa.btinsereClick(Sender: TObject);
begin
     if (Self.LbCamposDisponiveis.ItemIndex<0)
     Then exit;

     if (self.LbCamposSelecionados.Items.IndexOf(Self.LbCamposDisponiveis.Items[Self.LbCamposDisponiveis.itemindex])>=0)
     Then exit;

     self.LbCamposSelecionados.Items.add(Self.LbCamposDisponiveis.Items[Self.LbCamposDisponiveis.itemindex]);
end;

procedure TFConfordemcamposPesquisa.btinseretodosClick(Sender: TObject);
var
cont:integer;
begin
     for cont:=0 to self.LbCamposDisponiveis.Items.count-1 do
     Begin
          Self.LbCamposDisponiveis.ItemIndex:=cont;
          btinsereClick(sender);

     End;

end;

procedure TFConfordemcamposPesquisa.LbCamposSelecionadosKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);

begin
     if key=VK_DELETE
     Then Begin
               BtDeleteClick(sender);

     End;
end;



procedure TFConfordemcamposPesquisa.botaosetacimaClick(Sender: TObject);
var
temp:string;
begin
     if (Self.lbcamposselecionados.ItemIndex>0)//primeiro nao
     and (Self.lbcamposselecionados.ItemIndex<=(Self.lbcamposselecionados.Items.count-1))//ultimo pode
     Then Begin
               temp:=Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex];
               Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex]:=Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex-1];
               Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex-1]:=temp;
               Self.lbcamposselecionados.ItemIndex:=Self.lbcamposselecionados.itemindex-1;
               Self.lbcamposselecionados.SetFocus;

     End;


end;

procedure TFConfordemcamposPesquisa.botaosetabaixoClick(Sender: TObject);
var
temp:string;
begin
     if (Self.lbcamposselecionados.ItemIndex>=0)//do primeiro pra cima
     and (Self.lbcamposselecionados.ItemIndex<(Self.lbcamposselecionados.Items.count-1))//ultimo nao da
     Then Begin
               temp:=Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex];
               Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex]:=Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex+1];
               Self.lbcamposselecionados.items[Self.lbcamposselecionados.itemindex+1]:=temp;
               Self.lbcamposselecionados.ItemIndex:=Self.lbcamposselecionados.itemindex+1;
               Self.lbcamposselecionados.SetFocus;
     End;

end;

procedure TFConfordemcamposPesquisa.FormShow(Sender: TObject);
begin
     Self.Tag:=0;
end;

procedure TFConfordemcamposPesquisa.btconcluidoClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;
end;

procedure TFConfordemcamposPesquisa.LbCamposDisponiveisDblClick(
  Sender: TObject);
begin
     btinsereClick(sender);
end;

procedure TFConfordemcamposPesquisa.btdeleteClick(Sender: TObject);
var
posicao:integer;
begin
     if (Self.LbCamposSelecionados.ItemIndex<0)
     Then exit;


     if (self.LbCamposSelecionados.Items[Self.LbCamposSelecionados.ItemIndex]='CODIGO')
     Then Begin
              Messagedlg('N�o � permitido retirar o campo "CODIGO"',mterror,[mbok],0);
              exit;
     End;

     posicao:=Self.LbCamposSelecionados.ItemIndex;
     Self.LbCamposSelecionados.Items.Delete(posicao);

     if (posicao<=Self.LbCamposSelecionados.Items.count-1)
     Then Self.LbCamposSelecionados.ItemIndex:=posicao;
end;

procedure TFConfordemcamposPesquisa.btcancelarClick(Sender: TObject);
begin
     if (Messagedlg('Tem certeza que deseja cancelar?',mtconfirmation,[mbyes,mbno],0)=mrno)
     then exit;
     
     Self.Tag:=0;
     Self.Close;
end;

end.
