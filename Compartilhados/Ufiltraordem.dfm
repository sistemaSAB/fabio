object FfiltraOrdem: TFfiltraOrdem
  Left = 195
  Top = 82
  Width = 471
  Height = 464
  Caption = 'Ordem - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Btgravar: TBitBtn
    Left = 350
    Top = 2
    Width = 97
    Height = 38
    Caption = '&Ok'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = BtgravarClick
  end
  object BtCancelar: TBitBtn
    Left = 350
    Top = 40
    Width = 97
    Height = 38
    Caption = '&Cancelar'
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BtCancelarClick
  end
  object Grupo01: TPanel
    Left = 1
    Top = 1
    Width = 347
    Height = 41
    TabOrder = 2
    object LbGrupo01: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo01: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo02: TPanel
    Left = 1
    Top = 42
    Width = 347
    Height = 41
    TabOrder = 3
    object LbGrupo02: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo02: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo03: TPanel
    Left = 1
    Top = 83
    Width = 347
    Height = 41
    TabOrder = 4
    object LbGrupo03: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo03: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo04: TPanel
    Left = 1
    Top = 124
    Width = 347
    Height = 41
    TabOrder = 5
    object LbGrupo04: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo04: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo05: TPanel
    Left = 1
    Top = 165
    Width = 347
    Height = 41
    TabOrder = 6
    object LbGrupo05: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo05: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo06: TPanel
    Left = 1
    Top = 206
    Width = 347
    Height = 41
    TabOrder = 7
    object LbGrupo06: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo06: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo07: TPanel
    Left = 1
    Top = 247
    Width = 347
    Height = 41
    TabOrder = 8
    object LbGrupo07: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo07: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo08: TPanel
    Left = 1
    Top = 288
    Width = 347
    Height = 41
    TabOrder = 9
    object LbGrupo08: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo08: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo09: TPanel
    Left = 1
    Top = 329
    Width = 347
    Height = 41
    TabOrder = 10
    object LbGrupo09: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo09: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
  object Grupo10: TPanel
    Left = 1
    Top = 370
    Width = 347
    Height = 41
    TabOrder = 11
    object LbGrupo10: TLabel
      Left = 7
      Top = 9
      Width = 58
      Height = 19
      Caption = 'Grupo06'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -16
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object ComboGrupo10: TComboBox
      Left = 151
      Top = 8
      Width = 161
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Items.Strings = (
        'Matricula de Alunos'
        'Nota Fiscal de Entrada'
        'Nota Fiscal de Saida'
        'Entrada'
        'Saida')
    end
  end
end
