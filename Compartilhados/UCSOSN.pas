unit UCSOSN;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCSOSN,
  jpeg;

type
  TFCSOSN = class(TForm)
    panelrodape: TPanel;
    ImagemRodape: TImage;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    ImagemFundo: TImage;
    Lbcodigo: TLabel;
    Edtcodigo: TEdit;
    LbDescricao: TLabel;
    EdtDescricao: TEdit;
    Label1: TLabel;
    memoDescricao2: TMemo;
    lbMenuFiscal: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbMenuFiscalClick(Sender: TObject);
  private
         ObjCSOSN:TObjCSOSN;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;


    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCSOSN: TFCSOSN;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFCSOSN.ControlesParaObjeto: Boolean;
Begin

  Try
    With Self.ObjCSOSN do
    Begin
        Submit_codigo(edtcodigo.text);
        Submit_Descricao(edtDescricao.text);
        Submit_Descricao2(self.memoDescricao2.Text);


         result:=true;
    End;
    
  Except
        result:=False;
  End;
  
End;

function TFCSOSN.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjCSOSN do
     Begin
        Edtcodigo.text:=Get_codigo;
        EdtDescricao.text:=Get_Descricao;
        self.memoDescricao2.Text:=get_descricao2;

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFCSOSN.TabelaParaControles: Boolean;
begin
     If (Self.ObjCSOSN.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFCSOSN.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjCSOSN=Nil)
     Then exit;

     If (Self.ObjCSOSN.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjCSOSN.free;
end;

procedure TFCSOSN.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFCSOSN.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);


     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjCSOSN.status:=dsInsert;
     edtDescricao.setfocus;

end;


procedure TFCSOSN.btalterarClick(Sender: TObject);
begin
    If (Self.ObjCSOSN.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                Self.ObjCSOSN.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtDescricao.setfocus;
                
    End;


end;

procedure TFCSOSN.btgravarClick(Sender: TObject);
begin

     If Self.ObjCSOSN.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjCSOSN.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjCSOSN.Get_codigo;
     Self.ObjCSOSN.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCSOSN.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjCSOSN.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjCSOSN.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjCSOSN.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFCSOSN.btcancelarClick(Sender: TObject);
begin
     Self.ObjCSOSN.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFCSOSN.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFCSOSN.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjCSOSN.Get_pesquisa,Self.ObjCSOSN.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjCSOSN.status<>dsinactive
                                  then exit;

                                  If (Self.ObjCSOSN.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjCSOSN.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFCSOSN.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFCSOSN.FormShow(Sender: TObject);
begin


     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjCSOSN:=TObjCSOSN.create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
     retira_fundo_labels(self);

  PegaCorForm(Self);

end;



procedure TFCSOSN.lbMenuFiscalClick(Sender: TObject);
begin
  //FDataModulo.MenuFiscal;
end;

end.

