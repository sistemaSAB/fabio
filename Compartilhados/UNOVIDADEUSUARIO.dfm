object FNOVIDADEUSUARIO: TFNOVIDADEUSUARIO
  Left = 88
  Top = 82
  Width = 580
  Height = 419
  Caption = 'Cadastro NOVIDADEUSUARIO - EXCLAIM TECNOLOGIA'
  Color = 15458499
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ImageRodapeInterno: TImage
    Left = 1
    Top = 324
    Width = 571
    Height = 66
  end
  object Btnovo: TSpeedButton
    Left = 6
    Top = 324
    Width = 70
    Height = 65
    Cursor = crHandPoint
    Flat = True
    OnClick = BtnovoClick
  end
  object btgravar: TSpeedButton
    Left = 76
    Top = 322
    Width = 70
    Height = 69
    Cursor = crHandPoint
    Flat = True
    OnClick = btgravarClick
  end
  object btalterar: TSpeedButton
    Left = 146
    Top = 323
    Width = 70
    Height = 68
    Cursor = crHandPoint
    Flat = True
    OnClick = btalterarClick
  end
  object btcancelar: TSpeedButton
    Left = 216
    Top = 324
    Width = 70
    Height = 65
    Cursor = crHandPoint
    Flat = True
    OnClick = btcancelarClick
  end
  object btexcluir: TSpeedButton
    Left = 286
    Top = 322
    Width = 70
    Height = 68
    Cursor = crHandPoint
    Flat = True
    OnClick = btexcluirClick
  end
  object btpesquisar: TSpeedButton
    Left = 356
    Top = 325
    Width = 70
    Height = 63
    Cursor = crHandPoint
    Flat = True
    OnClick = btpesquisarClick
  end
  object btrelatorios: TSpeedButton
    Left = 426
    Top = 324
    Width = 70
    Height = 62
    Cursor = crHandPoint
    Flat = True
    OnClick = btrelatoriosClick
  end
  object btsair: TSpeedButton
    Left = 496
    Top = 323
    Width = 70
    Height = 67
    Cursor = crHandPoint
    Flat = True
    OnClick = btsairClick
  end
  object Guia: TTabbedNotebook
    Left = 1
    Top = 2
    Width = 569
    Height = 320
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = 'Principal'
            object LbCodigo: TLabel                  
              Left = 13                       
              Top = 100             
              Width = 40                     
              Height = 13                    
              Caption = 'Codigo'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object EdtCodigo: TEdit
        Left = 48
        Top = 100             
        Width = 100           
        Height = 21          
        MaxLength = 9        
        TabOrder = 0
      end                    
      object LbNovidade: TLabel                  
              Left = 13                       
              Top = 122             
              Width = 40                     
              Height = 13                    
              Caption = 'Novidade'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object EdtNovidade: TEdit
        Left = 64
        Top = 122             
        Width = 100           
        Height = 21          
        MaxLength = 9        
        TabOrder = 1
        Color = clInfoBk
        OnExit = edtNovidadeExit
        OnKeyDown = edtNovidadeKeyDown
      end                    
      object LbNomeNovidade: TLabel                  
              Left = 128
              Top = 122             
              Width = 40                     
              Height = 13                    
              Caption = 'Novidade'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object LbUsuario: TLabel                  
              Left = 13                       
              Top = 144             
              Width = 40                     
              Height = 13                    
              Caption = 'Usuario'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
      object EdtUsuario: TEdit
        Left = 56
        Top = 144             
        Width = 100           
        Height = 21          
        MaxLength = 9        
        TabOrder = 2
        Color = clInfoBk
        OnExit = edtUsuarioExit
        OnKeyDown = edtUsuarioKeyDown
      end                    
      object LbNomeUsuario: TLabel                  
              Left = 112
              Top = 144             
              Width = 40                     
              Height = 13                    
              Caption = 'Usuario'             
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'Verdana'
              Font.Style = [fsBold]
              ParentFont = False             
      end                                    
object Label1: TLabel

        Left = 264
        Top = 0
        Width = 37
        Height = 13
        Caption = 'Label1'
      end
      object Edit1: TEdit
        Left = 64
        Top = 64
        Width = 121
        Height = 19
        TabOrder = 0
        Text = 'Edit1'
      end
    end
  end
end
