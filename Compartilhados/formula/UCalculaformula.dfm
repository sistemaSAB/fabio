object Fcalculaformula: TFcalculaformula
  Left = -4
  Top = -4
  Width = 808
  Height = 578
  Caption = 'Fcalculaformula'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 32
    Width = 61
    Height = 19
    Caption = 'Variavel'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label2: TLabel
    Left = 456
    Top = 33
    Width = 127
    Height = 19
    Caption = 'F'#243'rmula Largura'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label3: TLabel
    Left = 140
    Top = 33
    Width = 112
    Height = 19
    Caption = 'F'#243'rmula Altura'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label4: TLabel
    Left = 16
    Top = 448
    Width = 36
    Height = 13
    Caption = 'Largura'
  end
  object Label5: TLabel
    Left = 146
    Top = 448
    Width = 27
    Height = 13
    Caption = 'Altura'
  end
  object LbAreaTotal: TLabel
    Left = 16
    Top = 496
    Width = 61
    Height = 19
    Caption = 'Variavel'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lblargura: TListBox
    Left = 452
    Top = 56
    Width = 310
    Height = 186
    ItemHeight = 13
    TabOrder = 0
  end
  object lbaltura: TListBox
    Left = 136
    Top = 56
    Width = 310
    Height = 186
    ItemHeight = 13
    TabOrder = 1
  end
  object lbvariavel: TListBox
    Left = 8
    Top = 56
    Width = 117
    Height = 186
    ItemHeight = 13
    TabOrder = 2
  end
  object Calcular: TButton
    Left = 576
    Top = 440
    Width = 185
    Height = 57
    Caption = 'Calcular'
    TabOrder = 3
    OnClick = CalcularClick
  end
  object edtlargura: TEdit
    Left = 16
    Top = 467
    Width = 121
    Height = 21
    TabOrder = 4
    Text = 'edtlargura'
  end
  object edtaltura: TEdit
    Left = 144
    Top = 467
    Width = 121
    Height = 21
    TabOrder = 5
    Text = 'edtaltura'
  end
  object edtvariavel: TEdit
    Left = 16
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 6
    Text = 'edtvariavel'
  end
  object edtformulalargura: TEdit
    Left = 280
    Top = 8
    Width = 297
    Height = 21
    TabOrder = 8
    Text = 'edtformulalargura'
  end
  object edtformulaaltura: TEdit
    Left = 144
    Top = 8
    Width = 121
    Height = 21
    TabOrder = 7
    Text = 'edtformulaaltura'
  end
  object BitBtn1: TBitBtn
    Left = 584
    Top = 8
    Width = 75
    Height = 25
    Caption = 'OK'
    TabOrder = 9
    OnClick = BitBtn1Click
  end
  object BitBtn2: TBitBtn
    Left = 664
    Top = 8
    Width = 75
    Height = 25
    Caption = 'limpar'
    TabOrder = 10
    OnClick = BitBtn2Click
  end
  object LBVARIAVEL2: TListBox
    Left = 8
    Top = 248
    Width = 117
    Height = 186
    ItemHeight = 13
    TabOrder = 11
    OnClick = BitBtn1Click
  end
  object LBLARGURA2: TListBox
    Left = 452
    Top = 248
    Width = 310
    Height = 186
    ItemHeight = 13
    TabOrder = 12
  end
  object LBALTURA2: TListBox
    Left = 136
    Top = 248
    Width = 310
    Height = 186
    ItemHeight = 13
    TabOrder = 13
  end
  object RDFormula: TRDFormula
    About = 'RDFormula 1.0 - Registrado'
    Left = 728
    Top = 472
  end
end
