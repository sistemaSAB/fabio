object frmMain: TfrmMain
  Left = 27
  Top = 101
  Width = 530
  Height = 349
  Caption = 'RDFormula Demostra'#231#227'o:'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'ms sans serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 296
    Top = 7
    Width = 209
    Height = 81
  end
  object TITULO: TLabel
    Left = 29
    Top = 53
    Width = 222
    Height = 16
    Caption = 'F'#243'rmulas e Express'#245'es Matem'#225'ticas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label11: TLabel
    Left = 27
    Top = 16
    Width = 230
    Height = 37
    Caption = 'RDFormula 1.0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -32
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label12: TLabel
    Left = 308
    Top = 12
    Width = 185
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Deltress Inform'#225'tica Ltda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label15: TLabel
    Left = 310
    Top = 31
    Width = 185
    Height = 13
    Alignment = taCenter
    AutoSize = False
    Caption = 'Fone/Fax (14) 427-5322'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label16: TLabel
    Left = 310
    Top = 48
    Width = 185
    Height = 13
    Cursor = crHandPoint
    Alignment = taCenter
    AutoSize = False
    Caption = 'deltress@deltress.com.br'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsUnderline]
    ParentFont = False
    OnClick = Label16Click
  end
  object Label10: TLabel
    Left = 344
    Top = 64
    Width = 122
    Height = 16
    Caption = 'www.deltress.com.br'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clMaroon
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 34
    Top = 111
    Width = 52
    Height = 13
    Caption = 'Express'#227'o:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label13: TLabel
    Left = 100
    Top = 112
    Width = 275
    Height = 13
    Caption = '(Utilize as variav'#233'is pr'#233'-definidas para montar a express'#227'o)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clTeal
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label6: TLabel
    Left = 258
    Top = 160
    Width = 58
    Height = 13
    Caption = 'Quantidade:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label8: TLabel
    Left = 309
    Top = 180
    Width = 80
    Height = 13
    Caption = '=> "Quantidade"'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label9: TLabel
    Left = 309
    Top = 236
    Width = 46
    Height = 13
    Caption = '=> "Dias"'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object LabelDias: TLabel
    Left = 258
    Top = 216
    Width = 21
    Height = 13
    Caption = 'Dias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 257
    Top = 258
    Width = 48
    Height = 13
    Caption = 'Resultado'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 34
    Top = 160
    Width = 110
    Height = 13
    Caption = 'Exemplo de Variaveis : '
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Bevel2: TBevel
    Left = 16
    Top = 100
    Width = 489
    Height = 209
  end
  object ComboBox1: TComboBox
    Left = 32
    Top = 128
    Width = 457
    Height = 21
    ItemHeight = 13
    TabOrder = 0
    Text = ' (10 / 3) + (8.55 * 2) + (8^8)  - 100.78'
    Items.Strings = (
      ' (10 / 3) + (8.55 * 2) + (8^8)  - 100.78'
      '10 MOD 3'
      'Trunc  (100 / 3)'
      'Round (100 / 3)'
      'Round (100/3 * 100) / 100'
      '(Banana * Laranja / Uva) * 0.75'
      'Salario / DiaMes * Dias'
      'Banana * Quantidade')
  end
  object QUANTIDADE: TEdit
    Left = 256
    Top = 176
    Width = 50
    Height = 21
    TabOrder = 1
    Text = '12'
  end
  object DIAS: TEdit
    Left = 256
    Top = 232
    Width = 50
    Height = 21
    TabOrder = 2
    Text = '15'
  end
  object RESPOSTA: TEdit
    Left = 255
    Top = 273
    Width = 230
    Height = 24
    Color = clInfoBk
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    ReadOnly = True
    TabOrder = 3
  end
  object Memo3: TMemo
    Left = 31
    Top = 176
    Width = 205
    Height = 121
    TabStop = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Lines.Strings = (
      ' Salario     =  181,00'
      ' DiaMes      =  30'
      ' DiaSemana   =   7'
      ' Banana      =  1,85'
      ' Laranja     =  2,50'
      ' Uva         =  5,00'
      '')
    ParentFont = False
    ReadOnly = True
    TabOrder = 4
  end
  object BitBtn2: TBitBtn
    Left = 420
    Top = 194
    Width = 62
    Height = 58
    Caption = '&Calcular'
    TabOrder = 5
    OnClick = BitBtn2Click
    Glyph.Data = {
      76020000424D7602000000000000760000002800000020000000200000000100
      0400000000000002000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00777888888888
      8888888888888888777777000000000000000000000000087777770FFFFFFF8F
      FFFFFF8FFFFFFF087777770FFFFFFF8FFFFFFF8FFFFFFF087777770FFFFFFF8F
      FFFFFF8FFFFFFF08777777088888888888808888888888087777770FFFFFFF8F
      FFFF008FFFFFFF087777770FFFFFFF8FFFFF0B007FFFFF087777770FFFFFFF8F
      FFFFF08807FFFF087777770888888888888880BB808888087777770FFFFFFF8F
      FFFFFF0FB807FF0877777707777777877777FF80FB807F087777770000000000
      00077F8F0FB8070077777066666666666660888880FB8008077770E006006006
      00607F8FFF0F0708707770EE06E06E06E0607F8FFFF0FB80770770E666666666
      66607F8FFFFF0FB8070770E00600600600608888888880FB800770EE06E06E06
      E0607F8FFFFFFF0FB80770E66666666666607F8FFFFFFF00F00770E006006006
      00607F8FFFFFFF0800F070EE06E06E06E060888888888808770070E666666666
      66607F8FFFFFFF08777770E00600600600607F8FFFFFFF08777770EE06E06E06
      E0607F8FFFFFFF08777770E6666666666660000000000007777770E666666666
      6660877777777777777770E07FFFFFFFF060877777777777777770E077777777
      7060877777777777777770E0000000000060877777777777777770EEEEEEEEEE
      EE60777777777777777777000000000000077777777777777777}
    Layout = blGlyphTop
    Spacing = -1
  end
  object RDFormula1: TRDFormula
    About = 'RDFormula 1.0 - Registrado'
    OnVariavel = RDFormula1Variavel
    OnErro = RDFormula1Erro
    Left = 416
    Top = 112
  end
end
