unit Main;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, RDFormula,shellapi, ExtCtrls;

type
  TfrmMain = class(TForm)
    RDFormula1: TRDFormula;
    TITULO: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label10: TLabel;
    Bevel1: TBevel;
    Label3: TLabel;
    Label13: TLabel;
    ComboBox1: TComboBox;
    Label6: TLabel;
    QUANTIDADE: TEdit;
    Label8: TLabel;
    DIAS: TEdit;
    Label9: TLabel;
    LabelDias: TLabel;
    RESPOSTA: TEdit;
    Label5: TLabel;
    Memo3: TMemo;
    Label4: TLabel;
    BitBtn2: TBitBtn;
    Bevel2: TBevel;
    procedure BitBtn2Click(Sender: TObject);
    procedure RDFormula1Erro(Sender: TObject; MsgErro: String);
    procedure RDFormula1Variavel(Sender: TObject; VarName: String;
      var Value: Extended; var Found: Boolean);
    procedure Label16Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.DFM}

procedure TfrmMain.BitBtn2Click(Sender: TObject);
begin
     RDFormula1.Expressao := Combobox1.Text;   // Express�o digitada ou lida de um arquivo
     RDFormula1.Execute;                       // Executa calculo...
     if RDFormula1.Falhou then                 // � TRUE se houve falha na execu��o...
        Resposta.Text := 'Erro'
     else
        Resposta.Text := FloatToStrF(RDFormula1.Resultado, ffGeneral,  18, 2);
// ou       Resposta.Text := floattostr(RDFormula1.Resultado);
// ou       Resposta.Test := formatfloat('###,###,##0.00',RDFormula1.Resultado);
end;

procedure TfrmMain.RDFormula1Erro(Sender: TObject; MsgErro: String);
begin
     // Quando ocorre ERRO na valida��o da Express�o, este evento � disparado
     // MSGerro cont�m uma string com o tipo de erro encontrado...
     MessageDlg(MsgErro, mtError, [mbOk], 0);
end;

procedure TfrmMain.RDFormula1Variavel(Sender: TObject; VarName: String;
  var Value: Extended; var Found: Boolean);
begin
// 1) Este evento procura pelo valor de uma VARIAVEL encontrada na f�rmula
// 2) Todas as variaveis possiveis de serem utilizadas na express�o, devem estar
//    relacionadas aqui e com seus devidos valores fixos ou variaveis,
//    campo de arquivo etc.
// 3) O VALUE representa o valor que deve ser v�lido
// 4) VARNAME � o nome da variavel encontrada na express�o,
//    Para evitar erros use UPPERCASE ou LOWERCASE para converter o VARNAME
// 5) FOUND � uma variavel do tipo boolean, voce deve validar se o valor de VARNAME
//    foi encontrado (TRUE) e n�o (FALSE)

     Found   := True;

     // Transforme tudo pra Maiusculo para evitar case sensitive...
     VarName := Uppercase(VarName);

     // Valores Fixos...
     if (VarName = 'SALARIO')    then  Value := 181     else
     if (VarName = 'DIAMES')     then  Value := 30      else
     if (VarName = 'DIASEMANA')  then  Value := 7       else
     if (VarName = 'BANANA')     then  Value := 1.85    else
     if (VarName = 'LARANJA')    then  Value := 2.50    else
     if (VarName = 'UVA')        then  Value := 5.00    else
     // Valores lidos do campo EDIT
     if (VarName = 'DIAS')       then  Value := STRTOINT(DIAS.TEXT)         else
     if (VarName = 'QUANTIDADE') then  Value := STRTOINT(QUANTIDADE.TEXT)   else
        Found := False;  // Se nao achou o valor mode FALSE para FOUND.
end;

procedure TfrmMain.Label16Click(Sender: TObject);
begin
     ShellExecute(0,nil,PChar('mailto: deltress@deltress.com.br ?Subject=RDFormula'), nil, nil, SW_SHOW);
end;

end.
