program TesteFormula;

uses
  Forms,
  Uprincipal in 'Uprincipal.pas' {Fprincipal},
  UCalculaformula in 'UCalculaformula.pas' {Fcalculaformula};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFprincipal, Fprincipal);
  Application.CreateForm(TFcalculaformula, Fcalculaformula);
  Application.Run;
end.
