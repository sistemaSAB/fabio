unit UCalculaformula;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RDFormula, StdCtrls, Buttons;

type
  TFcalculaformula = class(TForm)
    lblargura: TListBox;
    lbaltura: TListBox;
    lbvariavel: TListBox;
    RDFormula: TRDFormula;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Calcular: TButton;
    edtlargura: TEdit;
    edtaltura: TEdit;
    Label4: TLabel;
    Label5: TLabel;
    edtvariavel: TEdit;
    edtformulalargura: TEdit;
    edtformulaaltura: TEdit;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    LBVARIAVEL2: TListBox;
    LBLARGURA2: TListBox;
    LBALTURA2: TListBox;
    LbAreaTotal: TLabel;
    procedure CalcularClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
  private
    { Private declarations }
    Function VerificaOperadores_valores(PFormula:String):Boolean;
    Function Substitui_variaveis_valor(Pformula:String;Pvariavel,Pvalor:String):String;
    Function ApagaEspaco(Pformula:String):string;
    Procedure Substitui_Formula_por_Valor_Largura(Pvariavel,Pvalor:String);
    Procedure Substitui_Formula_por_Valor_altura(Pvariavel,Pvalor:String);
  public
    { Public declarations }
     Function Calcula_Area:boolean;
  end;

var
  Fcalculaformula: TFcalculaformula;

implementation

{$R *.dfm}

function TFcalculaformula.ApagaEspaco(Pformula: String): string;
var
cont:integer;
ptemp:String;
begin
     Result:='';
     ptemp:='';

     for cont:=1 to length(pformula) do
     Begin
          if (pformula[cont]<>' ')
          Then Ptemp:=ptemp+pformula[cont];
     End;
     result:=ptemp;
end;

procedure TFcalculaformula.CalcularClick(Sender: TObject);
begin
     Self.Calcula_Area;
     
end;



function TFcalculaformula.Calcula_Area: boolean;
var
cont:integer;
begin
     result:=False;


     Substitui_Formula_por_Valor_Largura('L',edtlargura.Text);
     Substitui_Formula_por_Valor_Largura('A',edtaltura.Text);

     Substitui_Formula_por_Valor_Altura('L',edtlargura.Text);
     Substitui_Formula_por_Valor_Altura('A',edtaltura.Text);


     //percorrendo todos os items
     for cont:=0 to lblargura.Items.count-1 do
     Begin

          if (Self.VerificaOperadores_valores(lblargura.Items[cont])=True)
          Then Begin//soh tem operadores e valores
                    //calcula com o rdformula

                    RDFormula.Expressao:=lblargura.Items[cont];
                    RDFormula.Execute;
                    if (RDFormula.Falhou=True)
                    Then Begin
                              Messagedlg('N�o � poss�vel calcular a f�rmula '+lblargura.Items[cont]+#13+'Express�o Inv�lida',mterror,[mbok],0);
                              exit;
                    End;
                    lblargura.Items[cont]:=floattostr(RDFormula.Resultado);
                    Self.Substitui_Formula_por_Valor_Largura(lbvariavel.Items[cont],lblargura.Items[cont]);
          End;


          if (Self.VerificaOperadores_valores(lbaltura.Items[cont])=True)
          Then Begin//soh tem operadores e valores
                    //calcula com o rdformula

                    RDFormula.Expressao:=lbaltura.Items[cont];
                    RDFormula.Execute;
                    if (RDFormula.Falhou=True)
                    Then Begin
                              Messagedlg('N�o � poss�vel calcular a f�rmula '+lbaltura.Items[cont]+#13+'Express�o Inv�lida',mterror,[mbok],0);
                              exit;
                    End;
                    lbaltura.Items[cont]:=floattostr(RDFormula.Resultado);
                    Self.Substitui_Formula_por_Valor_altura(lbvariavel.Items[cont],lbaltura.Items[cont]);
          End;
     End;
end;



procedure TFcalculaformula.Substitui_Formula_por_Valor_altura(Pvariavel,
  Pvalor: String);
var
cont:integer;
begin
     for cont:=0 to lblargura.Items.count-1 do
     Begin
          lbaltura.Items[cont]:=Self.Substitui_variaveis_valor(lbaltura.Items[cont],Pvariavel,Pvalor);
     End;
end;

procedure TFcalculaformula.Substitui_Formula_por_Valor_Largura(Pvariavel,
  Pvalor: String);
var
cont:integer;
begin
     for cont:=0 to lblargura.Items.count-1 do
     Begin
          lblargura.Items[cont]:=Self.Substitui_variaveis_valor(lblargura.Items[cont],Pvariavel,Pvalor);
     End;
end;

function TFcalculaformula.Substitui_variaveis_valor(Pformula: String;
  Pvariavel, Pvalor: String): String;
var
cont:integer;
ptemp:String;
pformulafinal:String;
Begin
     result:='';
     pformulafinal:='';
     //tenho uma string pra trabalhar
     //primeiro preciso localizar a substring dentro da string, se for ela preciso
     //substituir
     pvariavel:=uppercase(pvariavel);
     pformula:=Self.apagaespaco(uppercase(pformula));
     ptemp:='';


     //2+3+BD*3
     for cont:=1 to  length(pformula) do
     Begin
          if (pformula[cont] in ['*','/','+','-','(',')'])
          Then BEgin
                    if (ptemp=pvariavel)//encontrou
                    Then Begin
                              //substitui
                               pformulafinal:=copy(pformulafinal,1,length(pformulafinal)-length(pvariavel));
                               pformulafinal:=pformulafinal+pvalor;
                    End;
                    pformulafinal:=pformulafinal+pformula[cont];
                    ptemp:='';
          End
          Else BEgin
                    pformulafinal:=pformulafinal+pformula[cont];
                    ptemp:=ptemp+pformula[cont];
          End;
     End;

     if (ptemp=pvariavel)//encontrou
     Then Begin
               //substitui
               pformulafinal:=copy(pformulafinal,1,length(pformulafinal)-length(pvariavel));
               pformulafinal:=pformulafinal+pvalor;
     End;
     result:=pformulafinal;
end;


function TFcalculaformula.VerificaOperadores_valores(PFormula: String): Boolean;
var
cont:integer;
begin
     result:=False;
     Pformula:=ApagaEspaco(pformula);
     for cont:=1 to length(pformula) do
     Begin
          if not(pformula[cont] in ['0'..'9','.','(',')','+','-','*','/'])
          Then exit;//tem outro alem de numeros e operadores
     End;
     result:=True;
end;

procedure TFcalculaformula.BitBtn1Click(Sender: TObject);
begin
     lbvariavel.Items.ADD(EDTVARIAVEL.TEXT);
     lblargura.Items.add(edtformulalargura.Text);
     lbaltura.items.add(edtformulaaltura.Text);

     lbvariavel2.Items.ADD(EDTVARIAVEL.TEXT);
     lblargura2.Items.add(edtformulalargura.Text);
     lbaltura2.items.add(edtformulaaltura.Text);
     edtvariavel.setfocus;
end;

procedure TFcalculaformula.BitBtn2Click(Sender: TObject);
begin
     lbvariavel.Items.clear;
     lbaltura.Items.clear;
     lblargura.Items.clear;

     lbvariavel2.Items.clear;
     lbaltura2.Items.clear;
     lblargura2.Items.clear;

end;

end.
