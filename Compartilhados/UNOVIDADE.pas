unit UNOVIDADE;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db,
  jpeg,UessencialGlobal, OleCtrls, SHDocVw,uobjnovidade;

type
  TFNOVIDADE = class(TForm)
    Panel1: TPanel;
    EdtCodigo: TEdit;
    btVarsaoAnterior: TSpeedButton;
    lbVersao: TLabel;
    btVersaoProxima: TSpeedButton;
    Panel2: TPanel;
    CheckVisualizado: TCheckBox;
    RE_texto: TRichEdit;
    StaticText1: TStaticText;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btVersaoProximaClick(Sender: TObject);
    procedure btVarsaoAnteriorClick(Sender: TObject);
    procedure CheckVisualizadoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
         IndexStringList:Integer;
         Procedure LimpaLabels;
    { Private declarations }
  public
      ObjNOVIDADE:TObjNOVIDADE;
      SomenteNovasUsuario:Boolean;
      Function  VerificaNovidade:Boolean;
      Procedure PreencheMemo(PCodigo:string);
      Procedure PreencheTodas;
      Procedure Formata;
  end;

var
  FNOVIDADE: TFNOVIDADE;
implementation

uses Uessenciallocal, Upesquisa;

{$R *.DFM}
procedure TFNOVIDADE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    // If (Self.ObjNOVIDADE=Nil)
    // Then exit;

     If (Self.ObjNOVIDADE.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

     Self.ObjNOVIDADE.free;
     Self.ObjNOVIDADE:=nil;
     Self.SomenteNovasUsuario:=False;
end;

procedure TFNOVIDADE.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFNOVIDADE.LimpaLabels;
begin
//LIMPA LABELS
end;
procedure TFNOVIDADE.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     
     IndexStringList:=0;
     PegaCorForm(Self);
     Self.PreencheTodas;



end;

procedure TFNOVIDADE.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
end;

function TFNOVIDADE.VerificaNovidade: Boolean;
begin
   Result:=Self.ObjNOVIDADE.VerificaNovidade;
end;

procedure TFNOVIDADE.PreencheMemo(PCodigo:string);
Var
   Cont:Integer;
begin
    if(Self.ObjNOVIDADE.LocalizaCodigo(PCodigo)=false)
    then Begin
            MensagemErro('Novidade n�o encontrada');
            exit;
    end;
    Self.ObjNOVIDADE.TabelaparaObjeto;

    Self.EdtCodigo.Text:=Self.ObjNOVIDADE.Get_Codigo;
    Self.lbVersao.Caption:=Self.ObjNOVIDADE.Get_Versao;
    Self.RE_texto.Lines.clear;
    Self.RE_texto.lines.Text:=Self.ObjNOVIDADE.get_texto;

    self.Formata;

    


    
    CheckVisualizado.Checked:=Self.ObjNOVIDADE.CheckNaoMostrar(pcodigo);
    
end;

procedure TFNOVIDADE.btVersaoProximaClick(Sender: TObject);
begin

     if (self.ObjNOVIDADE.StrListNovidades.Count>0)
     Then Begin
               if (IndexStringList < self.ObjNOVIDADE.StrListNovidades.Count-1)
               then Begin
                        Inc(IndexStringList,1);
                        Self.PreencheMemo(self.ObjNOVIDADE.StrListNovidades[IndexStringList]);
               End;
     End;

end;

procedure TFNOVIDADE.btVarsaoAnteriorClick(Sender: TObject);
begin

     if (Self.ObjNOVIDADE.StrListNovidades.Count>0)
     Then Begin
               if (IndexStringList > 0)
               then Begin
                          Inc(IndexStringList,-1);
                          Self.PreencheMemo(Self.ObjNOVIDADE.StrListNovidades[IndexStringList]);
               End;
     End;
end;

procedure TFNOVIDADE.PreencheTodas;
begin
     Self.ObjNOVIDADE.PreencheStrlNovidade(SomenteNOvasusuario);   
     
     if (Self.ObjNOVIDADE.StrListNovidades.count>0)
     Then Begin
              IndexStringList:=Self.ObjNOVIDADE.StrListNovidades.Count-1;
              Self.PreencheMemo(self.ObjNOVIDADE.StrListNovidades[IndexStringList]);
     End;
end;

procedure TFNOVIDADE.CheckVisualizadoClick(Sender: TObject);
begin
    if (CheckVisualizado.Checked = true)
    then ObjNOVIDADE.Visualizado(EdtCodigo.Text)
    Else ObjNOVIDADE.DesVisualizar(EdtCodigo.Text);
end;

procedure TFNOVIDADE.Formata;
begin
   (* re_texto.SelStart:=1;
    re_texto.SelLength:=20;
    re_texto.SelAttributes.Style:=[fsbold];*)
end;





procedure TFNOVIDADE.FormCreate(Sender: TObject);
begin

     Try
             Self.ObjNOVIDADE:=TObjNOVIDADE.create;
     Except
             Messagedlg('Erro na Inicializa��o do Objeto (Unovidade "FormCreate") !',mterror,[mbok],0);
             Self.close;
     End;

end;

procedure TFNOVIDADE.FormDestroy(Sender: TObject);
begin

  if (self.ObjNOVIDADE <> nil) then
    Self.ObjNOVIDADE.free;


end;

end.

