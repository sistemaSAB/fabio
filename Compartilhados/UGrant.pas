unit UGrant;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, IBServices, IBDatabaseInfo, Db, IBDatabase, ExtCtrls,
  IBCustomDataSet, IBTable, CheckLst, IBQuery, IBStoredProc;

type
  TFgrant = class(TForm)
    checktabela: TCheckListBox;
    Checkprocedure: TCheckListBox;
    Panel1: TPanel;
    Label8: TLabel;
    combouser: TComboBox;
    btexecuta: TBitBtn;
    BitBtn1: TBitBtn;
    Permitir: TRadioButton;
    RetiraPermissao: TRadioButton;
    procedure btexecutaClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Fgrant: TFgrant;

implementation

uses Udatamodulo, Uprincipal,UessencialGlobal;

{$R *.DFM}

Procedure Pega_Usuarios;//ok
var
cont:integer;
Begin
     With FGrant do
     Begin
        try
          if (preparauser=false)
          then exit;
          Fdatamodulo.IbUser.DisplayUsers;
          for cont:=1 to Fdatamodulo.IbUser.UserInfoCount-1 do
          Begin
             combouser.Items.add(Fdatamodulo.IbUser.UserInfo[cont].UserName);
          End;
         Finally
                Fdatamodulo.IbUser.Active:=False;
         End;

     End;
End;

procedure invertetabela;//ok
var
   cont:integer;
begin
        With Fgrant do
        Begin
           for cont:=0 to checktabela.Items.Count -1 do
           Begin
                if (CheckTabela.checked[cont]=false)
                Then checktabela.Checked[cont]:=true
                else checktabela.Checked[cont]:=false;
           end;
        End;
End;

procedure inverteprocedure;//ok
var
   cont:integer;
begin
        With Fgrant do
        Begin
           for cont:=0 to checkprocedure.Items.Count -1 do
           Begin
                if (checkprocedure.checked[cont]=false)
                Then checkprocedure.Checked[cont]:=true
                else checkprocedure.Checked[cont]:=false;
           end;
        End;

End;


procedure TFgrant.btexecutaClick(Sender: TObject);//ok
var
cont,cont2:integer;
comando,final:string;
Qlocal:Tibquery;
begin

        Try
                Qlocal:=Tibquery.create(nil);
                Qlocal.database:=FdataModulo.ibdatabase;
        Except
              Messagedlg('Erro na tentativa de cria��o da Query!!',mterror,[mbok],0);
              exit;
        End;

Try


     If (Permitir.Checked=True)
     Then comando:='Grant All'
     Else comando:='Revoke ALL';

     for cont:=0 to checktabela.items.count-1 do
     Begin
           final:='';
           if (checktabela.checked[cont]=True)
           Then Begin      
                     If (Permitir.Checked=True)
                     Then final:=comando+'  on '+checktabela.Items.Strings[cont]+' to '+combouser.text
                     Else final:=comando+'  on '+checktabela.Items.Strings[cont]+' from '+combouser.text;

                     Try
                        Qlocal.close;
                        Qlocal.sql.clear;
                        Qlocal.sql.Add(final);
                        Qlocal.execsql;
                     Except
                           Messagedlg('Erro durante a atualiza��o dos dados!',mterror,[mbok],0);
                           exit;
                     End;
                End;
     End;

     for cont:=0 to checkprocedure.items.count-1 do
     Begin
           final:='';
           if (checkprocedure.checked[cont]=True)
           Then Begin
                     If (Permitir.Checked=True)
                     Then final:='Grant execute on procedure '+checkprocedure.Items.Strings[cont]+' to '+combouser.text
                     Else final:='Revoke execute on procedure '+checkprocedure.Items.Strings[cont]+' from '+combouser.text;
                     
                     Try
                        Qlocal.close;
                        Qlocal.sql.clear;
                        Qlocal.sql.Add(final);
                        Qlocal.execsql;
                     Except
                           Messagedlg('Erro durante a atualiza��o dos dados!',mterror,[mbok],0);
                           exit;
                     End;

                End;
     End;

     Messagedlg('Processamento Conclu�do!',mtinformation,[mbok],0);
Finally
       freeandnil(qlocal);
End;
End;


procedure TFgrant.BitBtn1Click(Sender: TObject);
begin
Fgrant.close;
end;

procedure TFgrant.FormActivate(Sender: TObject);
var
TabelaLocal:Tibtable;
StrLocal:TIBStoredProc;
cont:integer;
begin
     Try
        TabelaLocal:=TIBTable.create(NIL);
        TabelaLocal.database:=Fdatamodulo.ibdatabase;
        StrLocal:=TIBStoredProc.create(nil);
        StrLocal.database:=FDataModulo.IBDatabase;

     Except

     End;

Try
        Try
                btexecuta.enabled:=False;
                checktabela.items.clear;
                checkprocedure.items.clear;

                With TabelaLocal do
                Begin
                     for cont:=0 to (TabelaLocal.TableNames.Count-1) do
                     Begin
                       checktabela.items.add(TabelaLocal.TableNames[cont]);
                     end;
                End;
                for cont:=0 to (StrLocal.StoredProcedureNames.Count -1) do
                Begin
                     checkprocedure.items.add(StrLocal.StoredProcedureNames[cont]);
                end;
                //passando os usuarios para o combo
                pega_usuarios;
                //SELECIONANDO TODAS AS TABELAS E PROCEDURES
                invertetabela;
                inverteprocedure;
                //**************************

                btexecuta.Enabled:=True;

        Except
              Messagedlg('Erro na listagem das tabelas e dos usuarios!',mterror,[mbok],0);
              exit;
        End;
Finally
       FreeAndNil(TabelaLocal);
       FreeAndNil(StrLocal);
End;
end;



end.
