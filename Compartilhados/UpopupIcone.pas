unit UpopupIcone;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TFpopupIcone = class(TForm)
    Lbexcluir: TLabel;
    lbrenomear: TLabel;
    Label1: TLabel;
    procedure LbexcluirMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Label1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure LbexcluirClick(Sender: TObject);
    procedure lbrenomearClick(Sender: TObject);
  private
    { Private declarations }
    Procedure TiraCor;
  public
    { Public declarations }
  end;

var
  FpopupIcone: TFpopupIcone;

implementation

{$R *.dfm}

procedure TFpopupIcone.LbexcluirMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin

     Self.tiraCor;
     Tlabel(Sender).color:=rgb(198,211,239);
     Tlabel(Sender).font.color:=clWhite;
end;

procedure TFpopupIcone.TiraCor;
begin
     Lbexcluir.Color:=clWhite;
     Lbexcluir.font.color:=clBlack;
     lbrenomear.Color:=clWhite;
     lbrenomear.font.color:=clBlack;

end;

procedure TFpopupIcone.FormMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
     Self.TiraCor;
end;

procedure TFpopupIcone.Label1MouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
     Self.TiraCor;
end;

procedure TFpopupIcone.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if Key=#27
     Then Self.Close;

end;

procedure TFpopupIcone.FormShow(Sender: TObject);
begin
     Self.Tag:=0;
     Self.TiraCor;
end;

procedure TFpopupIcone.LbexcluirClick(Sender: TObject);
begin
     Self.Tag:=1;
     Self.Close;

end;

procedure TFpopupIcone.lbrenomearClick(Sender: TObject);
begin
     Self.Tag:=2;
     Self.Close;
end;

end.
