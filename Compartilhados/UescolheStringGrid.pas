unit UescolheStringGrid;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids;

type
  TFEscolheStringGrid = class(TForm)
    STRGConvenio: TStringGrid;
    BtOk: TBitBtn;
    BtCancelar: TBitBtn;
    procedure STRGConvenioKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure BtOkClick(Sender: TObject);
    procedure BtCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FEscolheStringGrid: TFEscolheStringGrid;

implementation

{$R *.dfm}

procedure TFEscolheStringGrid.STRGConvenioKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then bTOKCLICK(SENDER);
end;

procedure TFEscolheStringGrid.FormShow(Sender: TObject);
begin
     Self.Tag:=-1;
end;

procedure TFEscolheStringGrid.BtOkClick(Sender: TObject);
begin
     Self.Tag:=STRGConvenio.Row;
     Self.Close;
end;

procedure TFEscolheStringGrid.BtCancelarClick(Sender: TObject);
begin
     Self.Tag:=-1;
     Self.Close;
end;

end.
