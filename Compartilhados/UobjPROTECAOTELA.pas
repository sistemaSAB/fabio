unit UobjPROTECAOTELA;
Interface
Uses graphics,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc,UobjarquivoIni;

//USES_INTERFACE


Type
   TObjPROTECAOTELA=class

          Public
                //ObjDatasource                               :TDataSource;

                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];



                //*****CONFIGURACOES******
                VelocidadeAnimacao:String;
                TempoAtivacao:String;
                TopMensagem:String;
                Fonte:Tfont;
                CorFundo:String;
                //************************


//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:Str09) :boolean;
                Function    Exclui(Pcodigo:str09;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :Str100;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:Str09;
                Function  RetornaCampoCodigo:Str100;
                Function  RetornaCampoNome:Str100;
                Procedure Imprime(Pcodigo:Str09);

                Procedure Submit_CODIGO(parametro: STR09);
                Function Get_CODIGO: STR09;
                Procedure Submit_Mensagem(parametro: string);
                Function Get_Mensagem: string;
                Procedure Submit_datahorainicial(parametro: STR16);
                Function Get_datahorainicial: STR16;
                Procedure Submit_datahorafinal(parametro: STR16);
                Function Get_datahorafinal: STR16;
                function CarregaINI: boolean;
                function GravaINI: boolean;

                Function Retornamensagem(PdataHora:TdateTime):string;

                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               ObjArquivoIni:TobjarquivoIni;
               
               CODIGO:STR09;
               MensagemX:string;
               datahorainicial:STR16;
               datahorafinal:STR16;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                function  ConfiguraPadrao: boolean;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios;





Function  TObjPROTECAOTELA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.MensagemX:=fieldbyname('Mensagem').asstring;
        Self.datahorainicial:=fieldbyname('datahorainicial').asstring;
        Self.datahorafinal:=fieldbyname('datahorafinal').asstring;
//CODIFICA TABELAPARAOBJETO




        result:=True;
     End;
end;


Procedure TObjPROTECAOTELA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Mensagem').asstring:=Self.MensagemX;
        ParamByName('datahorainicial').asstring:=Self.datahorainicial;
        ParamByName('datahorafinal').asstring:=Self.datahorafinal;
//CODIFICA OBJETOPARATABELA




  End;
End;

//***********************************************************************

function TObjPROTECAOTELA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=False)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPROTECAOTELA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        MensagemX:='';
        datahorainicial:='';
        datahorafinal:='';
//CODIFICA ZERARTABELA




     End;
end;

Function TObjPROTECAOTELA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=False;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=true;
end;


function TObjPROTECAOTELA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjPROTECAOTELA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPROTECAOTELA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodatetime(Self.datahorainicial);
     Except
           Mensagem:=mensagem+'/Data e Hora Inicial';
     End;
     try
        Strtodatetime(Self.datahorafinal);
     Except
           Mensagem:=mensagem+'/Data e Hora Final';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPROTECAOTELA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPROTECAOTELA.LocalizaCodigo(parametro: Str09): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro PROTECAOTELA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Mensagem,datahorainicial,datahorafinal');
           SQL.ADD(' from  TabProtecaoTela');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPROTECAOTELA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPROTECAOTELA.Exclui(Pcodigo: str09;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPROTECAOTELA.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        Self.ObjArquivoIni:=TobjarquivoIni.Create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Fonte:=TFont.Create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabProtecaoTela(CODIGO,Mensagem,datahorainicial');
                InsertSQL.add(' ,datahorafinal)');
                InsertSQL.add('values (:CODIGO,:Mensagem,:datahorainicial,:datahorafinal');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabProtecaoTela set CODIGO=:CODIGO,Mensagem=:Mensagem');
                ModifySQL.add(',datahorainicial=:datahorainicial,datahorafinal=:datahorafinal');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabProtecaoTela where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjPROTECAOTELA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPROTECAOTELA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPROTECAOTELA');
     Result:=Self.ParametroPesquisa;
end;

function TObjPROTECAOTELA.Get_TituloPesquisa: Str100;
begin
     Result:=' Pesquisa de PROTECAOTELA ';
end;


function TObjPROTECAOTELA.Get_NovoCodigo: Str09;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENPROTECAOTELA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENPROTECAOTELA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPROTECAOTELA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.ObjArquivoIni.free;
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPROTECAOTELA.RetornaCampoCodigo: Str100;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPROTECAOTELA.RetornaCampoNome: Str100;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjProtecaoTela.Submit_CODIGO(parametro: STR09);
begin
        Self.CODIGO:=Parametro;
end;
function TObjProtecaoTela.Get_CODIGO: STR09;
begin
        Result:=Self.CODIGO;
end;
procedure TObjProtecaoTela.Submit_Mensagem(parametro: string);
begin
        Self.MensagemX:=Parametro;
end;

function TObjProtecaoTela.Get_Mensagem: string;
begin
        Result:=Self.MensagemX;
end;

procedure TObjProtecaoTela.Submit_datahorainicial(parametro: STR16);
begin
        Self.datahorainicial:=Parametro;
end;
function TObjProtecaoTela.Get_datahorainicial: STR16;
begin
        Result:=Self.datahorainicial;
end;
procedure TObjProtecaoTela.Submit_datahorafinal(parametro: STR16);
begin
        Self.datahorafinal:=Parametro;
end;
function TObjProtecaoTela.Get_datahorafinal: STR16;
begin
        Result:=Self.datahorafinal;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjPROTECAOTELA.Imprime(Pcodigo: Str09);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJPROTECAOTELA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;


Function TObjProtecaotela.ConfiguraPadrao:boolean;
Begin
     Self.VelocidadeAnimacao:='500';//ms
     Self.TempoAtivacao:='5';//s
     Self.TopMensagem:='200';
     Self.Fonte.Name:='Arial';
     Self.Fonte.Size:=20;
     Self.Fonte.Color:=clblack;
     Self.CorFundo:=inttostr(clwhite);
End;


Function TObjProtecaotela.CarregaINI:boolean;
var
Temp,nome,valor:string;
StrTemp:TStringlist;
QuantConf,cont,posicao:integer;
begin
    result:=False;

    Self.ConfiguraPadrao;

    if (self.ObjArquivoIni.Localizanome('CONFIGURACAO_PROTECAO_TELA')=true)
    then Begin
              Self.ObjArquivoIni.TabelaparaObjeto;


              try
                 Strtemp:=TStringList.create;
              Except
                    on e:exception do
                    begin
                         mensagemerro(e.message);
                         exit;
                    end;
              End;

              Try

                QuantConf:=Self.ObjArquivoIni.Arquivo.Count;

                for cont:=0 to quantconf-1 do
                Begin
                      temp:=self.objarquivoini.arquivo[cont];
                      strtemp.clear;
                      
                      if (ExplodeStr(temp,strtemp,';','string')=false)
                      then exit;
                
                      nome:=strtemp[0];
                      valor:=strtemp[1];


                      if (nome='TEMPO_ATIVACAO_SEG')//em segunds
                      then Begin
                                Try
                                   strtoint(valor);
                                   Self.TempoAtivacao:=valor;
                                Except
                                    mensagemerro('Par�metro '+nome+' inv�lido. Ser� usado o padr�o de 5s');
                                    Self.TempoAtivacao:='5';
                                End;
                      End;

                      if (nome='VELOCIDADE_ANIMACAO')//em ms
                      then Begin
                                Try
                                   strtoint(valor);
                                   Self.VelocidadeAnimacao:=valor;
                                Except
                                    mensagemerro('Par�metro '+nome+' inv�lido. Ser� usado o padr�o de 500ms');
                                    Self.VelocidadeAnimacao:='500';
                                End;
                      End;


                      if (nome='FONTE_NOME')
                      then Begin
                                Try
                                   Self.Fonte.Name:=valor;
                                Except
                                End;
                      End;

                      if (nome='FONTE_TAMANHO')
                      then Begin
                                Try
                                   Self.Fonte.Size:=strtoint(valor);
                                Except
                                   mensagemerro('Tamanho '+valor+' inv�lido para a fonte. Ser� usado 12');
                                   Self.Fonte.Size:=12;
                                End;
                      End;

                      if (nome='FONTE_COR')
                      then Begin
                                Try
                                   Self.Fonte.color:=strtoint(valor);
                                Except
                                   mensagemerro('Cor'+valor+' inv�lida para a fonte. Ser� usado a cor Preta');
                                   Self.Fonte.Color:=clBlack;
                                End;
                      End;

                      if (nome='TOP_MENSAGEM')
                      then Begin
                                Try
                                   strtoint(valor);
                                   Self.TopMensagem:=valor
                                Except
                                   mensagemerro('Valor Inv�lido para a Posi��o Superior (Top):'+valor+'Ser� usado 200');
                                   Self.TopMensagem:='200';
                                End;
                      End;

                      if (nome='COR_FUNDO')
                      then Begin
                                Try
                                   strtoint(valor);
                                   Self.CorFundo:=valor;
                                Except
                                   mensagemerro('Valor Inv�lido para a Cor de Fundo! Ser� utilizado a cor branca');
                                   Self.CorFundo:=inttostr(clwhite);
                                End;
                      End;


                End;//for
                
              Finally
                     freeandnil(strtemp);
              End;
    End;//then

end;


Function TObjProtecaotela.GravaINI:boolean;
var
ptemp:string;
begin
    self.ObjArquivoIni.ZerarTabela;
    
    if (self.ObjArquivoIni.Localizanome('CONFIGURACAO_PROTECAO_TELA')=False)
    then begin
              self.ObjArquivoIni.Submit_CODIGO('0');
              self.ObjArquivoIni.Status:=dsinsert;
              Self.ObjArquivoIni.Submit_nome('CONFIGURACAO_PROTECAO_TELA');
    End
    Else Begin
              Self.ObjArquivoIni.TabelaparaObjeto;
              Self.ObjArquivoIni.Status:=dsEdit;
    End;

    Self.ObjArquivoIni.Arquivo.Clear;

    With Self do
    begin

         Try
            strtoint(Self.TempoAtivacao);
         Except
               MensagemErro('Tempo Ativa��o inv�lido. Ser� usado o padr�o de 5s');
               Self.TempoAtivacao:='5';
         End;

        ptemp:='TEMPO_ATIVACAO_SEG;'+Self.TempoAtivacao+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        Try
           strtoint(Self.VelocidadeAnimacao)
        Except
              MensagemErro('Velocidade de Anima��o inv�lido. Ser� usado o padr�o de 500ms');
              Self.VelocidadeAnimacao:='500';
        End;
        
        ptemp:='VELOCIDADE_ANIMACAO;'+Self.VelocidadeAnimacao+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        ptemp:='FONTE_NOME;'+Self.Fonte.Name+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        ptemp:='FONTE_TAMANHO;'+inttostr(Self.Fonte.Size)+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        ptemp:='FONTE_COR;'+inttostr(Self.Fonte.Color)+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        ptemp:='TOP_MENSAGEM;'+Self.topmensagem+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

         ptemp:='COR_FUNDO;'+Self.corfundo+';';
        Self.ObjArquivoIni.Arquivo.add(ptemp);

        if (Self.ObjArquivoIni.Salvar(true)=false)
        Then Begin
                  mensagemerro('Erro na tentativa de salvar as configura��es');
                  exit;
        End;
        MensagemAviso('Configura��es salvas com sucesso');
    End;
    
End;







function TObjPROTECAOTELA.Retornamensagem(PdataHora: TdateTime): string;
begin
     result:='';


     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select mensagem from tabprotecaotela where');
          sql.add('datahorainicial<='+#39+formatdatetime('mm/dd/yyyy hh:mm',pdatahora)+#39);
          sql.add('and datahorafinal>='+#39+formatdatetime('mm/dd/yyyy hh:mm',pdatahora)+#39);
          open;

          while not(eof) do
          Begin
               IF (Result<>'')
               Then result:=result+'      |     '+fieldbyname('mensagem').asstring
               Else result:=fieldbyname('mensagem').asstring;
               next;
          End;
     End;

end;

end.



