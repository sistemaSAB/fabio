
unit URelAnotacoes;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QuickRpt, ExtCtrls, Qrctrls, Db;

type
  TFrelanotacoes = class(TForm)
    QR: TQuickRep;
    QRBand1: TQRBand;
    BandaDetalhes: TQRBand;
    titulo: TQRLabel;
    CampoContato: TQRDBText;
    CampoFone: TQRDBText;
    QRShape3: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    CampoAssunto: TQRDBText;
    QRLabel5: TQRLabel;
    CampoDataRetorno: TQRDBText;
    QRLabel6: TQRLabel;
    CampoData: TQRDBText;
    CampoObservacao: TQRMemo;
    QRLabel7: TQRLabel;
    CampoOrigem: TQRDBText;
    QRShape1: TQRShape;
    QRBand2: TQRBand;
    QRShape2: TQRShape;
    campoorigemnome: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel9: TQRLabel;
    CampoEndereco: TQRDBText;
    QRLabel10: TQRLabel;
    campotecnico: TQRDBText;
    QRLabel11: TQRLabel;
    campousuario: TQRDBText;
    QRImage1: TQRImage;
    procedure QRBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public

    { Public declarations }
  end;

var
  Frelanotacoes: TFrelanotacoes;

implementation

uses UessencialGlobal;

{$R *.DFM}

{ TFrelPortador }


procedure TFrelanotacoes.QRBeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
     PegaFigura(qrimage1,'Logo200x52x24b.jpg')
end;

end.
