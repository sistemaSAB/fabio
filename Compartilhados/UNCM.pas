unit UNCM;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjNCM,
  jpeg;

type
  TFNCM = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    panelrodape: TPanel;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbDESCRICAO: TLabel;
    EdtDESCRICAO: TEdit;
    LbOBSERVACAO: TLabel;
    EdtOBSERVACAO: TEdit;
    ImagemFundo: TImage;
    ImagemRodape: TImage;
    Label1: TLabel;
    edtCodigoNCM: TEdit;
    Label2: TLabel;
    edtPercentualTributo: TEdit;
    Label3: TLabel;
    edtPercentualTributoImp: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure edtCodigoNCMExit(Sender: TObject);
    procedure edtPercentualTributoKeyPress(Sender: TObject; var Key: Char);
  private
         ObjNCM:TObjNCM;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FNCM: TFNCM;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFNCM.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjNCM do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_DESCRICAO(edtDESCRICAO.text);
        Submit_OBSERVACAO(edtOBSERVACAO.text);
        submit_CODIGONCM(edtCodigoNCM.Text);
        submit_percentualTributo(edtPercentualTributo.Text);
        submit_percentualTributoimp(edtPercentualTributoImp.Text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFNCM.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjNCM do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtDESCRICAO.text:=Get_DESCRICAO;
        EdtOBSERVACAO.text:=Get_OBSERVACAO;
        edtCodigoNCM.Text:=get_CODIGONCM;
        edtPercentualTributo.Text:=get_percentualTributo;
        edtPercentualTributoImp.Text := get_percentualTributoimp;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFNCM.TabelaParaControles: Boolean;
begin
     If (Self.ObjNCM.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFNCM.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjNCM=Nil)
     Then exit;

     If (Self.ObjNCM.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjNCM.free;
     if(Self.Owner = nil) then
       FDataModulo.IBTransaction.Commit;
end;

procedure TFNCM.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFNCM.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjNCM.status:=dsInsert;
     edtCodigoNCM.setfocus;

end;


procedure TFNCM.btalterarClick(Sender: TObject);
begin
    If (Self.ObjNCM.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                Self.ObjNCM.Status:=dsEdit;
                esconde_botoes(Self);
                Btgravar.visible:=True;
                BtCancelar.visible:=True;
                btpesquisar.visible:=True;
                edtDESCRICAO.setfocus;
                
    End;


end;

procedure TFNCM.btgravarClick(Sender: TObject);
begin

     If Self.ObjNCM.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Self.ObjNCM.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjNCM.Get_codigo;
     Self.ObjNCM.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFNCM.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjNCM.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjNCM.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjNCM.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFNCM.btcancelarClick(Sender: TObject);
begin
     Self.ObjNCM.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFNCM.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFNCM.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);
           FpesquisaLocal.NomeCadastroPersonalizacao:='FNCM.PESQUISA';

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjNCM.Get_pesquisa,Self.ObjNCM.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjNCM.status<>dsinactive
                                  then exit;

                                  If (Self.ObjNCM.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjNCM.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFNCM.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFNCM.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjNCM:=TObjNCM.create(self.Owner);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
     retira_fundo_labels(self);
end;
//CODIFICA ONKEYDOWN E ONEXIT


procedure TFNCM.btopcoesClick(Sender: TObject);
begin

  if (lbCodigo.Caption = '') then
    Exit;

  ObjNCM.opcoes(LbCodigo.Caption);


  //LocalizaCodigo(lbcodigo.Caption);
  //tabelaparaObjeto;
  //ObjetoParaControles;
end;

procedure TFNCM.edtCodigoNCMExit(Sender: TObject);
begin

  if edtCodigoNCM.Text <> '' then
  begin
    if (Length(edtCodigoNCM.Text) <> 2) and (Length(edtCodigoNCM.Text) <> 8) then
    begin
      MensagemAviso('C�digo NCM inv�lido');
      edtCodigoNCM.SetFocus;
      Exit;
    end;
  end;

end;

procedure TFNCM.edtPercentualTributoKeyPress(Sender: TObject;var Key: Char);
begin
  ValidaNumeros(Tedit(Sender),Key,'FLOAT');
end;

end.

