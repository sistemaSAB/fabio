object fPesquisaPersonalizada: TfPesquisaPersonalizada
  Left = 490
  Top = 245
  Width = 860
  Height = 538
  Caption = 'Pesquisa'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PanelCabecalho: TPanel
    Left = 0
    Top = 0
    Width = 844
    Height = 46
    Align = alTop
    BevelOuter = bvNone
    Color = clCaptionText
    TabOrder = 0
    object Label1: TLabel
      Left = 2
      Top = 3
      Width = 140
      Height = 37
      Caption = 'Pesquisa'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -32
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object PanelEscolheResultado: TPanel
    Left = 0
    Top = 46
    Width = 844
    Height = 32
    Align = alTop
    BevelOuter = bvNone
    Color = 14024703
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    object Label3: TLabel
      Left = 0
      Top = 0
      Width = 217
      Height = 32
      Align = alLeft
      AutoSize = False
      Caption = 'Voc'#234' est'#225' procurando por: '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -16
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
    end
    object lbNomeCampo: TLabel
      Left = 217
      Top = 0
      Width = 5
      Height = 32
      Align = alLeft
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
    end
  end
  object DBGrid: TDBGrid
    Left = 0
    Top = 78
    Width = 844
    Height = 277
    Align = alClient
    BorderStyle = bsNone
    Color = clWhite
    DataSource = DataSource1
    DragCursor = crArrow
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ImeMode = imOpen
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    TabOrder = 2
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = clBlack
    TitleFont.Height = -11
    TitleFont.Name = 'Verdana'
    TitleFont.Style = [fsBold]
    OnCellClick = DBGridCellClick
    OnDrawColumnCell = DBGridDrawColumnCell
    OnDblClick = DBGridDblClick
    OnKeyDown = DBGridKeyDown
    OnKeyPress = DBGridKeyPress
    OnKeyUp = DBGridKeyUp
  end
  object Panel1: TPanel
    Left = 0
    Top = 355
    Width = 844
    Height = 65
    Align = alBottom
    Color = clWhite
    TabOrder = 3
    DesignSize = (
      844
      65)
    object edtbusca: TMaskEdit
      Left = 1
      Top = 31
      Width = 827
      Height = 30
      Anchors = [akLeft]
      CharCase = ecUpperCase
      Color = clWhite
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Text = ','
      Visible = False
      OnKeyDown = edtbuscaKeyDown
      OnKeyPress = edtbuscaKeyPress
    end
    object rbExata: TRadioButton
      Left = 326
      Top = 12
      Width = 131
      Height = 17
      Anchors = [akLeft]
      Caption = 'exatamente igual'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Visible = False
    end
    object rbInciciaCom: TRadioButton
      Left = 194
      Top = 11
      Width = 113
      Height = 17
      Anchors = [akLeft]
      Caption = 'inicia com'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      Visible = False
    end
    object rbTenhaemQualquerParte: TRadioButton
      Left = 0
      Top = 11
      Width = 181
      Height = 17
      Anchors = [akLeft]
      Caption = 'tenha em qualquer parte'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      TabStop = True
      Visible = False
    end
  end
  object PanelInformacoes: TPanel
    Left = 0
    Top = 420
    Width = 844
    Height = 80
    Align = alBottom
    BevelOuter = bvNone
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clSilver
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    DesignSize = (
      844
      80)
    object ImageRodape: TImage
      Left = 0
      Top = 0
      Width = 844
      Height = 80
      Align = alClient
      Stretch = True
    end
    object Label4: TLabel
      Left = 2
      Top = 6
      Width = 184
      Height = 16
      Caption = 'Voc'#234' pesquisou por....:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbNomeCampoInformacao: TLabel
      Left = 191
      Top = 6
      Width = 462
      Height = 16
      AutoSize = False
      Caption = 'Nome'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label5: TLabel
      Left = 2
      Top = 23
      Width = 184
      Height = 16
      Caption = 'Voc'#234' digitou..........:'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbValorDigitado: TLabel
      Left = 191
      Top = 23
      Width = 463
      Height = 16
      AutoSize = False
      Caption = 'lata de tomate '
      Font.Charset = ANSI_CHARSET
      Font.Color = clYellow
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object lbQtdeRegistrosEncontrados: TLabel
      Left = 3
      Top = 41
      Width = 627
      Height = 16
      AutoSize = False
      Caption = '35'
      Color = 4227327
      Font.Charset = ANSI_CHARSET
      Font.Color = clMaroon
      Font.Height = -16
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lbCadastrar: TLabel
      Left = 711
      Top = 3
      Width = 128
      Height = 32
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = 'Cadastrar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbCadastrarClick
      OnMouseMove = lbCadastrarMouseMove
      OnMouseLeave = lbCadastrarMouseLeave
    end
    object lbSair: TLabel
      Left = 787
      Top = 29
      Width = 52
      Height = 32
      Cursor = crHandPoint
      Anchors = [akTop, akRight]
      Caption = 'Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clLime
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      OnClick = lbSairClick
    end
    object labelrodape: TLabel
      Left = 1
      Top = 62
      Width = 700
      Height = 14
      Caption = 
        'Espa'#231'o=Busca/F12=Ordena,CTRL m'#250'ltiplas colunas/Del=apagar coluna' +
        '/ENTER=seleciona/F11=Retorna Colunas'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
  end
  object QueryColunas: TIBQuery
    Left = 655
    Top = 138
  end
  object querypesq: TIBQuery
    Left = 694
    Top = 142
  end
  object DataSource1: TDataSource
    DataSet = querypesq
    Left = 732
    Top = 144
  end
end
