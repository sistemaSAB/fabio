unit Upesquisa;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DB, DBGrids, ComCtrls, StdCtrls,IBCustomDataSet, IBTable, IBQuery, IBStoredProc, IBDatabase,
  IBEvents, Mask, Buttons, UessencialGlobal, ExtCtrls;

type
  TFpesquisa = class(TForm)
    DataSource1: TDataSource;
    querypesq: TIBQuery;
    PanelInformacoes: TPanel;
    QueryColunas: TIBQuery;
    ImageRodape: TImage;
    Label4: TLabel;
    lbNomeCampoInformacao: TLabel;
    Label5: TLabel;
    lbValorDigitado: TLabel;
    lbQtdeRegistrosEncontrados: TLabel;
    lbCadastrar: TLabel;
    lbSair: TLabel;
    labelrodape: TLabel;
    Panel1: TPanel;
    edtbusca: TMaskEdit;
    rbExata: TRadioButton;
    rbInciciaCom: TRadioButton;
    rbTenhaemQualquerParte: TRadioButton;
    PanelCabecalho: TPanel;
    Label1: TLabel;
    DBGrid: TDBGrid;
    PanelEscolheResultado: TPanel;
    Label3: TLabel;
    lbNomeCampo: TLabel;
    procedure DBGridKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure edtbuscaKeyPress(Sender: TObject; var Key: Char);
    procedure DBGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridDblClick(Sender: TObject);
    procedure DBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure edtbuscaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure lbCadastrarClick(Sender: TObject);
    procedure lbSairClick(Sender: TObject);
    procedure DBGridKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbCadastrarMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbSairMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbCadastrarMouseLeave(Sender: TObject);
    procedure lbSairMouseLeave(Sender: TObject);
    procedure DBGridCellClick(Column: TColumn);
    procedure rbTenhaemQualquerParteClick(Sender: TObject);
    procedure rbInciciaComClick(Sender: TObject);
    procedure rbExataClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
      FormCadastro:Tform;
      comandosql:string;
      ComplementoPorcentagem:string;//usado para por % na pesquisa de nome automaticamente de acordo com um parametro
      Procedure PersonalizaColunas;
      Procedure PersonalizaGrid;
      Procedure LimpaLabel;

    { Private declarations }
  public
        PesquisaEmEstoque:Boolean;
        str_pegacampo,str_valorpesquisado:string;
        str_campopesquisainicial,str_valorcampopesquisainicial:String;
        
        NomeCadastroPersonalizacao:String;
        Invisiveis:TStringList;
        Function PreparaPesquisa(comando:TStringList;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:TStringList;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase):Boolean;overload;
        
        Function PreparaPesquisaULTIMAPESQUISA(comando:TStringList;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:string;TitulodoForm:String;FormCad:Tform):Boolean;overload;
        Function PreparaPesquisa(comando:string;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase):Boolean;overload;
        
        Function PreparaPesquisaN(comando:string;TitulodoForm:String;NomeForm:string):Boolean;

  end;

var
     Fpesquisa: TFpesquisa;



implementation

uses UDataModulo, UescolheImagemBotao;



{$R *.DFM}

procedure TFpesquisa.DBGridKeyPress(Sender: TObject; var Key: Char);
begin
// Alterado por F�bio por Ordem do Sadol 04/08/2009
// A Ideia � n�o precisar dar "Espa�o" para aparecer o Edit
// obs.: porem, para qm ja tem o custume de dar o Espa�o, esse espa�o preenche o edit, vou precisar tirar (Mauricio)
         //     if Key=#32
         //    then
     rbInciciaCom.Visible:=false;
     rbExata.Visible:=false;
     rbTenhaemQualquerParte.Visible:=false;

     Case Key of
          #32:Begin
                 edtbusca.text:='';
                 str_pegacampo:=dbgrid.SelectedField.FieldName;
                 EdtBusca.Visible :=true;
                 EdtBusca.SetFocus;      

                 Case dbgrid.SelectedField.datatype of

                   ftdatetime :  Begin
                                   edtBusca.EditMask:='00/00/0000 00:00:00';
                                   EdtBusca.width:=300;
                                End;
                   ftdate    :  Begin
                                   edtBusca.EditMask:='00/00/0000;1;_';
                                   EdtBusca.width:=130;
                                End;
                   ftTime    :  Begin
                                   edtBusca.EditMask:='00:00';
                                   EdtBusca.width:=130;
                                End;
                   ftInteger  :Begin
                                  edtBusca.EditMask:='';
                                  EdtBusca.width:=300;
                                  EdtBusca.maxlength:=15
                               End;

                   ftLargeint  :Begin
                                  edtBusca.EditMask:='';
                                  EdtBusca.width:=300;
                                  EdtBusca.maxlength:=15
                               End;

                   ftbcd      :Begin
                                  edtBusca.EditMask:='';
                                  EdtBusca.width:=300;
                                  EdtBusca.maxlength:=15
                               End;
                   ftfloat    :Begin
                                  edtBusca.EditMask:='';
                                  EdtBusca.width:=300;
                                  EdtBusca.maxlength:=15
                               End;
                   ftString   :Begin
                                  edtBusca.EditMask:='';
                                  EdtBusca.width:=700;
                                  EdtBusca.maxlength:=255;
                                  rbTenhaemQualquerParte.Visible:=true;
                                  rbInciciaCom.Visible:=true;
                                  rbExata.Visible:=true;
                                  //rbTenhaemQualquerParte.Checked:=true;
                               End;
                 Else
                               Begin
                                         Messagedlg('Este tipo de dados n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
                                         DBGrid.setfocus;
                                         exit;
                               End;

                 End;
                 //EdtBusca.Text :='';

                 //EdtBusca.Text:=Key;
                 //EdtBusca.SelStart := 1; // Deixa o cursor apos o 1� digito



          End;
          #13: Begin
                    if  Self.QueryPesq.recordcount<>0
                    Then Self.modalresult:=mrok
                    Else Self.modalresult:=mrCancel;
               End;
          #27:Begin
                   Self.modalresult:=mrcancel;
              End;
     end;
end;

procedure TFpesquisa.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
cont:integer;
begin
    //O Tag � usado para indicar a formatacao do Dbgrid, zero ele
    //para nao influenciar em outras pesquisas
    Self.tag:=0;
    
    if edtbusca.Visible = true
    then  Begin
              edtbusca.Visible:= false;
              lbQtdeRegistrosEncontrados.Caption:='';
    end;


    if (Invisiveis<>nil)
    Then Begin
              Invisiveis.clear;
              for cont:=0 to dbgrid.Columns.Count-1 do
              Begin
                  if (dbgrid.Columns.Items[cont].Visible=False)
                  Then Invisiveis.add(inttostr(cont));
              End;
    End;

    Self.NomeCadastroPersonalizacao:='';

end;

procedure TFpesquisa.FormActivate(Sender: TObject);
var
cont:integer;
enter:char;
begin
     UessencialGlobal.PegaCorForm(Self);
     Self.Tag:=0;
     
     Case Self.QueryPesq.state of

        dsInactive: Begin
                        Messagedlg('A tabela n�o foi preparada antes de chamar o form!', mterror,[mbok],0);
                        abort;
                    End;
        Else Begin
                ComplementoPorcentagem:='';
                comandosql:='';
                comandosql:= Self.QueryPesq.SQL.Text;
                if (Self.Tag=3)
                Then Formatadbgrid_3_casas(dbgrid)
                Else Formatadbgrid(dbgrid);

                dbgrid.setfocus;
                If (ObjParametroGlobal.ValidaParametro('% PADRAO NA PESQUISA EM CAMPOS NOME')=True)
                Then Begin
                          
                          If (ObjParametroGlobal.Get_Valor='SIM')
                          Then ComplementoPorcentagem:='%'
                          Else ComplementoPorcentagem:='';
                End;

                if (Invisiveis<>nil)
                Then Begin
                          for cont:=0 to Invisiveis.Count-1 do
                          Begin
                               try
                                  strtoint(Invisiveis[cont]);
                                  if (DBGrid.Columns.Count>Cont)
                                  Then dbgrid.Columns.Items[strtoint(Invisiveis[cont])].Visible :=False;
                               except
                               end;

                          End;
                End;

             End;
     End;

     if (Self.str_campopesquisainicial<>'') and (Self.str_valorcampopesquisainicial<>'')
     Then Begin
               if (DBGrid.DataSource.DataSet.Fields.FindField(str_campopesquisainicial)<>nil)
               Then Begin
                         dbgrid.SelectedField:=DBGrid.DataSource.DataSet.Fields.FindField(str_campopesquisainicial);
                         self.str_pegacampo:=Self.str_campopesquisainicial;
                         edtbusca.text:=Self.str_valorcampopesquisainicial;
                         Enter:=#13;
                         edtbuscaKeyPress(edtbusca,enter);
               End;
     End;

     if (Self.NomeCadastroPersonalizacao<>'')
     Then Self.labelrodape.Caption:=Self.labelrodape.Caption+'/F3 - Personaliza Colunas';

     Self.PersonalizaGrid;

     //edtteste.SetFocus;
    //AdjustColumnWidths(DBGrid);
    
end;

procedure TFpesquisa.edtbuscaKeyPress(Sender: TObject; var Key: Char);
var
 str_busca,OrderBy:string;
 indice_grid:integer;
begin

   if key =#27//ESC
   then Begin
           Self.str_valorpesquisado:='';
           EdtBusca.Visible := false;
           lbQtdeRegistrosEncontrados.Caption:='';
           dbgrid.SetFocus;
           exit;
   End;

   If Key=#13//procura
   Then Begin
          if (edtbusca.Text='')
          then exit;

          Self.str_valorpesquisado:='';
          str_busca:=edtbusca.Text;
          OrderBy:='';

          Case dbgrid.SelectedField.DataType of

             ftstring   :Begin
                             if (rbTenhaemQualquerParte.Checked=true)
                             then str_busca:='%'+str_busca+'%';

                             if (rbInciciaCom.Checked=true)
                             then str_busca:=str_busca+'%';

                             str_busca:=' UPPER('+str_pegacampo+') like '+#39+str_busca+#39;
                             OrderBy:= ' order by '+str_pegacampo;
                         End;

             ftinteger  :Begin
                             str_busca := str_pegacampo+' = '+trim(str_busca);
                         End;

             ftLargeint :Begin
                             str_busca := str_pegacampo+' = '+trim(str_busca)
                         end;

             ftfloat    :Begin
                             str_busca := tira_ponto(str_busca);
                             str_busca := virgulaparaponto(str_busca);
                             str_busca := str_pegacampo+' = '+trim(str_busca)
                         End;

             ftBcd      :Begin
                             str_busca := tira_ponto(str_busca);
                             str_busca := virgulaparaponto(str_busca);
                             str_busca := str_pegacampo+' = '+trim(str_busca)
                         End;

             ftDate     :Begin
                             str_busca := #39+FormatDateTime('mm/dd/yyyy', StrToDate(str_busca))+#39;
                             str_busca := str_pegacampo+' = '+trim(str_busca)
                         End;

             fttime     :Begin
                             str_busca := str_pegacampo+' = '+trim(str_busca)
                         End;

             ftDateTime :Begin
                             str_busca := str_pegacampo+' = '+trim(str_busca)
                         End;
          Else Begin
                       Messagedlg('Este dado n�o est� configurado para ser pesquisado!',mterror,[mbok],0);
          End;
          end;
          indice_grid:=self.DBGrid.SelectedIndex;
          Self.QueryPesq.close;
          Self.QueryPesq.sql.clear;

          If(Pos('WHERE',UpperCase(comandosql))<>0)
          Then Self.QueryPesq.sql.add(comandosql+' and '+str_busca)
          Else Self.QueryPesq.sql.add(comandosql+' where '+str_busca);
          Self.QueryPesq.sql.add(OrderBy);
          try
              Self.QueryPesq.open;
          except
              MensagemErro('Erro na pesquisa !');
              exit;
          end;


          lbQtdeRegistrosEncontrados.Caption:='';
          if (Self.querypesq.RecordCount=0)
          then lbQtdeRegistrosEncontrados.Caption:='Nenhum registro ecncontrado, tecle F10 para Pesquisa Completa';
          {
          else Begin
                  Self.querypesq.Last;      // Isso aki deixa a pesquisa mais lenta ainda
                  lbQtdeRegistrosEncontrados.Caption:=InttoStr(Self.querypesq.RecordCount);
                  Self.querypesq.First;

          end;
          }
        

          lbNomeCampoInformacao.Caption:=str_pegacampo;
          lbValorDigitado.Caption:=edtbusca.Text;

          Self.str_valorpesquisado:=edtbusca.text;
          edtbusca.Text:='';


          Formatadbgrid(dbgrid);
          Formatadbgrid(dbgrid,querypesq);
          Self.Personalizagrid;

          //self.DBGrid.SelectedIndex:=indice_grid;
          edtbusca.Visible:=False;
          rbInciciaCom.Visible:=false;
          rbExata.Visible:=false;
          rbTenhaemQualquerParte.Visible:=false;
          Dbgrid.SetFocus;
          exit;

        End;

//Aqui defino as regras que podem ser digitadas
//como estou utilizando maskEdit Para Datas e Horas
//N�o preciso pois defini uma mascara, mas para
//float e Integer preciso

        Case dbgrid.SelectedField.DataType OF

                ftInteger:  Begin
                                 if not (Key in ['0'..'9',#8])
                                 Then key:=#0;
                            End;
                ftFloat  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftCurrency  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;
                ftBCD  :  Begin
                                 If Not(Key in ['0'..'9',#8,'.',','])
                                 Then key:=#0
                                 Else Begin
                                           If Key='.'
                                           Then Key:=',';
                                      End;
                            End;


        End;
End;


procedure TFpesquisa.DBGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
indice_grid:integer;
cont,marca_registro:integer ;
comandoordena:String;
ColunaAtual:integer;
begin
     if (key=VK_Delete)
     then begin
               dbgrid.Columns.Items[dbgrid.SelectedIndex].Visible :=false;
               dbgrid.SelectedIndex :=dbgrid.SelectedIndex +1;
     end;

     If (key=VK_F10)
     then Begin
               Self.QueryPesq.close;
               Self.QueryPesq.sql.clear;
               Self.QueryPesq.sql.add(comandosql);
               //InputBox('','',comandosql);
               Self.QueryPesq.open;
               DbGrid.SelectedIndex:=ColunaAtual;
               Formatadbgrid(dbgrid,querypesq);
               Self.Personalizagrid;
     end;

     if (key=VK_f12)
     Then Begin


                     try
                         ColunaAtual:=DbGrid.SelectedIndex;
                         str_pegacampo:=DbGrid.SelectedField.FieldName;
                         if (Length(str_pegacampo)>4)
                         Then Begin
                                   if (uppercase(copy(str_pegacampo,1,2))='XX')
                                   and (uppercase(copy(str_pegacampo,length(str_pegacampo)-1,2))='XX')
                                   Then exit;
                         End;

                         indice_grid:=self.DBGrid.SelectedIndex;
                         marca_registro:= self.DBGrid.DataSource.DataSet.fieldbyname('codigo').AsInteger;
                         ComandoOrdena:=comandosql+' order by ';

                         If (ssCtrl in Shift)
                         Then Begin  //Control pressionado, sinal de ordem por mais de uma coluna
                                   //procuro a palavra order se encontrar significa que posso ordenar pelo proximo campo
                                   If(Pos('order ',Self.QueryPesq.sql.text)<>0)
                                   Then ComandoOrdena:=Self.QueryPesq.sql.text+','
                                   Else ComandoOrdena:=comandosql+' order by ';
                         End;

                         Self.QueryPesq.close;
                         Self.QueryPesq.sql.clear;
                         Self.QueryPesq.sql.add(Comandoordena+str_pegacampo);
                         Self.QueryPesq.open;
                         DbGrid.SelectedIndex:=ColunaAtual;
                         Formatadbgrid(dbgrid,querypesq);
                         Self.Personalizagrid;

                        //self.DBGrid.DataSource.DataSet.Locate( 'codigo',marca_registro,[loCaseInsensitive]);
                        //self.DBGrid.SelectedIndex:=indice_grid;
                        //self.DBGrid.DataSource.DataSet.GotoBookmark(marca_registro);

                     except

                     end;
                //End
                //Else Begin
                 //       If (Self.QueryPesq.recordcount=0)
                 //       Then messagedlg('N�o Existem Registros para Serem Ordenados!',mterror,[mbok],0)
                  //      Else messagedlg('Selecione um Campo para Ordena��o e Pressione Ordenar!',mtwarning,[mbok],0);
                   //  End;
                DbGrid.setfocus;
          End
     Else Begin
               if key=VK_F11
               Then Begin
                         for cont:=0 to dbgrid.Columns.Count-1 do
                         dbgrid.Columns.Items[cont].Visible :=true;
               End
               Else Begin
                         if ((key=VK_f3) and (Self.NomeCadastroPersonalizacao<>''))
                         Then Begin
                                   Self.personalizacolunas;
                         End;
               End;
     End;


end;

function TFpesquisa.PreparaPesquisa(comando: string;TitulodoForm:String;FormCad:Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando); 
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;


function TFpesquisa.PreparaPesquisa(comando: string;TitulodoForm:String;FormCad:Tform;BasedeDados:TIBDatabase): Boolean;
begin
    querypesq.Database:=basededados;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando);
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TFpesquisa.PreparaPesquisa(comando: TStringList;
  TitulodoForm: String; FormCad: Tform): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;


end;


function TFpesquisa.PreparaPesquisa(comando: TStringList;
  TitulodoForm: String; FormCad: Tform;BasedeDados:TIBDatabase): Boolean;
begin
    querypesq.Database:=BasedeDados;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;
end;



procedure TFpesquisa.DBGridDblClick(Sender: TObject);
begin
    if  Self.QueryPesq.recordcount<>0
    Then Self.modalresult:=mrok
    Else Self.modalresult:=mrCancel;
        
end;

function TFpesquisa.PreparaPesquisaN(comando: string; TitulodoForm:String;NomeForm: string): Boolean;
begin
    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql.add(comando);
            open;
            If (NomeForm<>'')
            Then Begin
                      //verificar se � possicel chamar um form pelo nome
                      FormCadastro:=Tform(Application.FindComponent(Nomeform));
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;
end;

function TFpesquisa.PreparaPesquisaULTIMAPESQUISA(comando: TStringList;
  TitulodoForm: String; FormCad: Tform): Boolean;
begin

    querypesq.Database:=FDataModulo.ibdatabase;
    Self.Caption:=TitulodoForm;
     Try
        With Self.QueryPesq do
        Begin
            close;
            sql.clear;
            sql:=comando;
            open;
            If (FormCad<>Nil)
            Then Begin
                      Self.FormCadastro:=FormCad;
                      Self.lbCadastrar.visible:=True;
                 End
            Else Self.lbCadastrar.visible:=False;
        End;
     Except
           Messagedlg('Erro na Tentativa de Pesquisa!',mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;
end;


procedure TFpesquisa.DBGridDrawColumnCell(Sender: TObject;const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
    if (Self.PesquisaEmEstoque = false)  // Pesquisa comum
    then Begin
            (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

            If not Odd(Self.querypesq.RecNo) then
            If not (gdselected in state)
            Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

            (Sender as TdbGrid).canvas.FillRect(rect);
            (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);
    end else
    Begin   // Pesquisa com cores diferente comformeo estoque
            (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL1;

            If not Odd(Self.querypesq.RecNo) then
            If not (gdselected in state)
            Then (Sender as Tdbgrid).canvas.brush.color :=CORGRIDZEBRADOGLOBAL2;

            (Sender as TdbGrid).canvas.FillRect(rect);
            (Sender as TdbGrid).DefaultDrawDataCell(rect, column.field,state);

             if (DBGrid.DataSource.DataSet.FieldList.IndexOf('estoque')<>-1)
             Then Begin
                      if (DBGrid.DataSource.DataSet.FieldByName('estoque').asfloat<0)
                      Then Begin
                                dbgrid.canvas.Font.color:=rgb(153,0,0);
                                dbgrid.DefaultDrawDataCell(rect,dbgrid.Columns[datacol].Field,state);
                      End
                      Else Begin
                               if (DBGrid.DataSource.DataSet.FieldList.IndexOf('estoqueminimo')=-1)
                               Then exit;

                               if (DBGrid.DataSource.DataSet.FieldByName('estoqueminimo').asfloat = 0) then
                                 exit;
                               if (DBGrid.DataSource.DataSet.FieldByName('estoque').asfloat<=DBGrid.DataSource.DataSet.FieldByName('estoqueminimo').asfloat)
                               Then Begin
                                         dbgrid.canvas.Font.color:=RGB(0,40,174);
                                         dbgrid.DefaultDrawDataCell(rect,dbgrid.Columns[datacol].Field,state);
                               End;
                      End;
             End;
        end;
end;

procedure TFpesquisa.FormCreate(Sender: TObject);
begin
     Self.NomeCadastroPersonalizacao:='';
     Self.str_valorcampopesquisainicial:='';
     Self.str_campopesquisainicial:='';
     Self.str_valorpesquisado:='';
     Self.PesquisaEmEstoque:=false;


end;

procedure TFpesquisa.PersonalizaColunas;
begin
     ObjCamposPesquisa_Global.ConfiguraColunas(Self.DBGrid,Self.NomeCadastroPersonalizacao);
     Self.PersonalizaGrid;
end;

procedure TFpesquisa.PersonalizaGrid;
begin
    ObjCamposPesquisa_Global.Personalizagrid(Self.DBGrid,self.NomeCadastroPersonalizacao);
end;



procedure TFpesquisa.edtbuscaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

     If (key=VK_F10)
     then Begin
               Self.QueryPesq.close;
               Self.QueryPesq.sql.clear;
               Self.QueryPesq.sql.add(comandosql);
               Self.QueryPesq.open;
               Formatadbgrid(dbgrid,querypesq);
               Self.Personalizagrid;
     end;

end;

procedure TFpesquisa.FormShow(Sender: TObject);
//var
  //i : Integer;
begin
     FescolheImagemBotao.PegaFiguraImagem(ImageRodape,'RODAPE');
     lbNomeCampo.Caption:=dbgrid.SelectedField.FieldName;
     Self.LimpaLabel;
    {
    For I := 0 to DBGrid.FieldCount - 1 do
        DBGrid.Fields[i].Tag := 30;
    AutoSizeDBGrid(DBGrid);
    }
end;

procedure TFpesquisa.LimpaLabel;
begin
      lbNomeCampo.Caption:='';
      lbNomeCampoInformacao.Caption:='';
      lbValorDigitado.Caption:='';
      lbQtdeRegistrosEncontrados.Caption:='';
end;

procedure TFpesquisa.lbCadastrarClick(Sender: TObject);
begin
     If (Self.FormCadastro<>nil)
     Then Begin

               IF Self.FormCadastro.Visible=False
               Then Self.FormCadastro.showmodal
               Else Self.FormCadastro.show;

               Self.QueryPesq.close;
               Self.QueryPesq.open;
               Self.DBGrid.setfocus;
          End;

end;

procedure TFpesquisa.lbSairClick(Sender: TObject);
begin
     Self.Close;
end;

procedure TFpesquisa.DBGridKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     lbNomeCampo.Caption:=dbgrid.SelectedField.FieldName;
end;

procedure TFpesquisa.lbCadastrarMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
    TLabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFpesquisa.lbSairMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: Integer);
begin
        TLabel(Sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFpesquisa.lbCadastrarMouseLeave(Sender: TObject);
begin
     TLabel(Sender).Font.Style:=[fsbold];
end;

procedure TFpesquisa.lbSairMouseLeave(Sender: TObject);
begin
     TLabel(Sender).Font.Style:=[fsbold];
end;

procedure TFpesquisa.DBGridCellClick(Column: TColumn);
begin
     lbNomeCampo.Caption:=dbgrid.SelectedField.FieldName;
end;

procedure TFpesquisa.rbTenhaemQualquerParteClick(Sender: TObject);
begin
     if (edtbusca.Enabled=true)
     then edtbusca.SetFocus;
end;

procedure TFpesquisa.rbInciciaComClick(Sender: TObject);
begin
     if (edtbusca.Enabled=true)
     then edtbusca.SetFocus;

end;

procedure TFpesquisa.rbExataClick(Sender: TObject);
begin
     if (edtbusca.Enabled=true)
     then edtbusca.SetFocus;

end;

procedure TFpesquisa.FormKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
begin
     If(ssalt in Shift) and (key=67)
     Then lbCadastrarClick(sender);

     If(ssalt in Shift) and (key=83)
     Then lbSairClick(sender);

end;

end.
