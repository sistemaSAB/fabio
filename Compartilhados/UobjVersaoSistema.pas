unit UobjVersaoSistema;

Interface

Uses windows,types,sysutils,Dialogs,Classes;

Type

   TObjVersaosistema=class

          Public
                tamanho,Companhia,Descricao,VersaodoArquivo,Nomeinterno,CopyRight,MarcaRegistrada,NomeOriginal,NomeProduto,
                VersaoProduto,Comentarios,Autor:String;
                Constructor Create;
                Destructor  Free;
          Private
                 Procedure ZeraCampos;

          End;

implementation


{ TobjVersaoSistema}
constructor TobjVersaoSistema.Create;
const
    //Parametro de versao
    InfoStr:array[1..11] of string=('CompanyName','FileDescription','FileVersion','InternalName','LegalCopyright','LegalTradeMarks','OriginalFileName','ProductName','ProductVersion','Comments','Author');
var
    TamVer,i:integer;
    Dummy:Dword;
    Tam:Uint;
    Verinfo:Pchar;
    Valor:Pointer;
    Translation:Pointer;
    VerBegin:string;
begin
     Self.ZeraCampos;
     //******************Resgatando Informa��es de Vers�o******************************

     //obt�m o tamanho da informa��o de vers�o
     TamVer:=GetFileVersionInfoSize(Pchar(ParamStr(0)),Dummy);
     if (Tamver>0)
     Then Begin
              //aloca a mem�ria para armazenar a vers�o
              GetMem(VerInfo,TamVer);
              Self.tamanho:=Inttostr(TamVer);
              //obt�m informa��o da vers�o
              GetFileVersionInfo(pchar(paramstr(0)),0,tamver,VerInfo);
              //obt�m informa��o de l�ngua
              VerQueryValue(VerInfo,'\\VarFileInfo\\Translation',Translation,Tam);
              //inicializa variavel para obter informacoes
              VerBegin:='\\StringFileInfo\\'+
                         InttoHex(loWord(Longint(Translation^)),4)+
                         InttoHex(HiWord(Longint(Translation^)),4)+'\\';
              //obt�m as informa��es
              for i:=1 to 11 do
              Begin

                   if (VerQueryValue(VerInfo,pchar(VerBegin+InfoStr[i]),Valor,Tam))
                   Then Begin
                             //se tam>0 entao tem a informacao
                             if (Tam>0)
                             Then Begin
                                      Case i of
                                          1:Self.Companhia        :=String(Pchar(Valor));
                                          2:Self.Descricao        :=String(Pchar(Valor));
                                          3:Self.VersaodoArquivo  :=String(Pchar(Valor));
                                          4:Self.Nomeinterno      :=String(Pchar(Valor));
                                          5:Self.CopyRight        :=String(Pchar(Valor));
                                          6:Self.MarcaRegistrada  :=String(Pchar(Valor));
                                          7:Self.NomeOriginal     :=String(Pchar(Valor));
                                          8:Self.NomeProduto      :=String(Pchar(Valor));
                                          9:Self.VersaoProduto    :=String(Pchar(Valor));
                                          10:Self.Comentarios     :=String(Pchar(Valor));
                                          11:Self.Autor           :=String(Pchar(Valor));
                                      End;
                             end;
                   End;
              end;
              FreeMem(VerInfo,Tamver);
     end;
     
end;


destructor TObjVersaosistema.Free;
begin

end;

procedure TObjVersaosistema.ZeraCampos;
begin
     With Self do
     Begin
          tamanho        :='';
          Companhia      :='';
          Descricao      :='';
          VersaodoArquivo:='';
          Nomeinterno    :='';
          CopyRight      :='';
          MarcaRegistrada:='';
          NomeOriginal   :='';
          NomeProduto    :='';
          VersaoProduto  :='';
          Comentarios    :='';
          Autor          :='';
     End;
end;

end.
