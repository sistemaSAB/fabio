unit UAjuda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, Menus,ComCtrls,IBQuery,UDataModulo,UessencialGlobal;

type
  TFAjuda = class(TForm)
    pnl1: TPanel;
    lb1: TLabel;
    pnl2: TPanel;
    pnlItensAjuda: TPanel;
    tv1: TTreeView;
    mmoAjuda: TRichEdit;
    procedure FormShow(Sender: TObject);
    procedure tv1Click(Sender: TObject);
    procedure tv1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
       AjudaGlobal:string;

       procedure BloqueiaAjudas;
       procedure LiberaAjuda;
       procedure CarregaAjuda;
  public
      Procedure PassaAjuda(Ajuda:string);
  end;

var
  FAjuda: TFAjuda;

implementation

uses DateUtils, DB;

{$R *.dfm}

procedure TFAjuda.FormShow(Sender: TObject);
begin
     BloqueiaAjudas;
     CarregaAjuda;
     mmoAjuda.Lines.Clear;

end;

procedure TFAjuda.BloqueiaAjudas;
begin
    ///

end;

procedure TFAjuda.PassaAjuda(ajuda:string);
begin
     AjudaGlobal:=Ajuda;
end;

procedure TFAjuda.LiberaAjuda;
begin
   //
end;

procedure TFAjuda.CarregaAjuda;
var
  raiz,ultimo,pai:TTreeNode;
  Query:TIBQuery;
  QueryFilhos:TIBQuery;
begin
      Query:=TIBQuery.Create(nil);
      Query.Database:= FDataModulo.IBDatabase;
      QueryFilhos:=TIBQuery.Create(nil);
      QueryFilhos.Database:= FDataModulo.IBDatabase;

      tv1.Items.Clear;

      try
          with Query do
          begin
               Close;
               sql.Clear;
               sql.Add('select * from tabajuda where ligadomenu is null');
               Open;
               self.tv1.Items.AddChild(nil,fieldbyname('nomemenu').AsString);
               raiz:=self.tv1.Items.GetFirstNode;
               pai:=raiz;
               ultimo:=pai;

               while not Eof do
               begin
                    if(RecNo>1) then
                    begin
                       self.tv1.Items.AddChild(nil,fieldbyname('nomemenu').AsString);
                       ultimo:=ultimo.GetNext;
                       pai:=ultimo;

                    end;

                    QueryFilhos.Close;
                    QueryFilhos.sql.Clear;
                    QueryFilhos.SQL.Add('select * from tabajuda where ligadomenu='+fieldbyname('codigo').AsString);
                    QueryFilhos.Open;
                    while not QueryFilhos.Eof do
                    begin
                         self.tv1.Items.AddChild(pai,QueryFilhos.fieldbyname('nomemenu').AsString);
                         ultimo:=ultimo.GetNext;
                         QueryFilhos.Next;
                    end;

                    Next;
               end;

          end;
      finally
            FreeAndNil(Query);
            FreeAndNil(QueryFilhos);

      end;

end;


procedure TFAjuda.tv1Click(Sender: TObject);
Var
  NoRaiz:TTreeNode;
  Query:TIBQuery;
  Codigo:string;
  Ajuda:string;
begin

      Query:=TIBQuery.Create(nil);
      query.Database:=FDataModulo.IBDatabase;
      try
             if(Self.tv1.Selected = nil)
             then Exit;

             NoRaiz:=tv1.Selected;

             while NoRaiz.Parent <> nil
             do NoRaiz:=NoRaiz.Parent;

             mmoAjuda.Lines.Clear;

             with Query do
             begin
                    Close;
                    SQL.Clear;
                    SQL.Add('select codigo from tabajuda where nomemenu='+#39+NoRaiz.Text+#39);
                    Open;
                    codigo:=fieldbyname('codigo').AsString;
                    if(NoRaiz.Text=tv1.Selected.Text)then
                    begin
                         { Close;
                          SQL.Clear;
                          SQL.Add('select ajuda from tabajuda');
                          SQL.Add('where ligadomenu='+codigo);
                          Open;
                          while not Eof do
                          begin
                              mmoAjuda.text:=mmoAjuda.text + fieldbyname('ajuda').AsString;
                              mmoAjuda.Lines.Add('');
                              mmoAjuda.Lines.Add('');
                              mmoAjuda.Lines.Add('');
                              next;
                          end;  }
                    end
                    else
                    begin
                          Close;
                          SQL.Clear;
                          SQL.Add('select ajuda from tabajuda where nomemenu='+#39+tv1.Selected.Text+#39);
                          SQL.Add('and ligadomenu='+codigo);
                          Open;
                          mmoAjuda.text:=fieldbyname('ajuda').AsString;
                    end;



             end;
      finally
             FreeAndNil(Query);
      end;

end;

procedure TFAjuda.tv1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      //ShowMessage(tv1.Selected.Text);
end;

end.
