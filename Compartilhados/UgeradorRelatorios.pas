unit UgeradorRelatorios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, IBCustomDataSet, IBQuery, Grids, DBGrids,UDataModulo,
  StdCtrls, Buttons;

type
  TFgeradorRelatorios = class(TForm)
    DS_Selecao: TDataSource;
    Grid_Selecao: TDBGrid;
    Query_Selecao: TIBQuery;
    Query_Relatorio: TIBQuery;
    MemoSql: TMemo;
    BtExecutaComando: TButton;
    BTAbrirComando: TButton;
    OpenDialog: TOpenDialog;
    BtDesenhaRelatorio: TButton;
    procedure FormShow(Sender: TObject);
    procedure BtExecutaComandoClick(Sender: TObject);
    procedure BTAbrirComandoClick(Sender: TObject);
    procedure BtDesenhaRelatorioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;


var
  FgeradorRelatorios: TFgeradorRelatorios;


implementation

uses UdesenhaRelatorio, ULabelRel, Uregua;



{$R *.dfm}

procedure TFgeradorRelatorios.FormShow(Sender: TObject);
begin
     Self.Query_Selecao.Close;
     Self.Query_Relatorio.close;
     Self.MemoSql.Lines.clear;
end;

procedure TFgeradorRelatorios.BtExecutaComandoClick(Sender: TObject);
var
Comando,primeirapalavra:String;
cont,PosicaoEspaco:Integer;
begin
     //******Validando se o  SQL � um SQl de Sele��o*********************
     //Esse for para no momento que encontra algum caractere diferente de ' '
     //para evitar espacos no inicio do SQL
     for cont:=1 to length(Self.MemoSql.text) do
     Begin
          if (Self.MemoSql.text[cont]<>' ')
          Then break;
     End;
     //copiando o comando SQL para uma Variavel String
     comando:=copy(Self.MemoSql.text,cont,length(Self.MemoSql.text)-cont+1);
     //procurando o primeiro espaco apos o comando inicial
     PosicaoEspaco:=pos(' ',comando);
     PrimeiraPalavra:=copy(Comando,1,PosicaoEspaco-1);
     if (UpperCase(primeirapalavra)<>'SELECT')
     Then Begin
               Messagedlg('O Comando SQL deve come�ar pela palavra SELECT',mterror,[mbok],0);
               exit;
     End;
     //**********Tentando Executar o SQl****************
     Try
        Self.Query_Selecao.close;
        Self.Query_Selecao.SQL.clear;
        Self.Query_Selecao.SQL.text:=comando;
        Self.Query_Selecao.open;
     Except
           Messagedlg('Erro na Tentativa de Executar o Comando',mterror,[mbok],0);
           exit;
     End;

end;

procedure TFgeradorRelatorios.BTAbrirComandoClick(Sender: TObject);
begin
     //filtrando arquivos SQL e texto somente
     OpenDialog.Filter:='Arquivos Sql|*.sql|Arquivos Texto|*.txt';
     if (OpenDialog.Execute)
     Then Begin
               MemoSql.lines.clear;
               Try
                  MemoSql.lines.LoadFromFile(OpenDialog.FileName);
               Except
                     Messagedlg('Erro na Tentativa de Abrir o Comando do Arquivo '+#13+OpenDialog.FileName,mterror,[mbok],0);
                     exit;
               End;
     End;
end;

procedure TFgeradorRelatorios.BtDesenhaRelatorioClick(Sender: TObject);
var
Cont:integer;
linhaatual:integer;
begin
     //verificando se a query est� aberta 
     if (Self.Query_Selecao.Active=False)
     Then Begin
               Messagedlg('Execute um Comando antes de Desenhar o Relat�rio',mterror,[mbok],0);
               exit; 
     End;

     With FdesenhaRelatorio do
     Begin
          //Zerando a Vari�vel que conta quantas labels foram alocadas
          //no vetor
          Conta_label_usada_gb:=0;

          //Primeira LInha do relat�rio
          linhaatual:=1;
          //Desenha a R�gua
          Desenha_regua(imagerel);

          //Este for percorre todos os campos da query
          //que foram selecionados na execu��o do SQL
          for cont:=0 to Self.Query_Selecao.FieldCount-1 do
          Begin
               //criando a label para o rotulo
               VetorLabel[Conta_label_usada_gb]:=Tlabelrel.create(nil);
               VetorLabel[Conta_label_usada_gb].Parent := FdesenhaRelatorio;
               VetorLabel[Conta_label_usada_gb].height := 30;
               VetorLabel[Conta_label_usada_gb].width := 120;
               VetorLabel[Conta_label_usada_gb].caption := Self.Query_Selecao.Fields[cont].DisplayName;
               VetorLabel[Conta_label_usada_gb].rotulo:=VetorLabel[Conta_label_usada_gb].caption; 
               VetorLabel[Conta_label_usada_gb].Reposiciona(1,LinhaAtual);
               VetorLabel[Conta_label_usada_gb].Font.Name:='Courier New';
               VetorLabel[Conta_label_usada_gb].Font.Size:=8;
               VetorLabel[Conta_label_usada_gb].Transparent:=True;
               VetorLabel[Conta_label_usada_gb].tipo:='R';//r�tulo
               VetorLabel[Conta_label_usada_gb].nomecampoBD:='';
               VetorLabel[Conta_label_usada_gb].QuantidadeCaracteres:=length(VetorLabel[Conta_label_usada_gb].Caption);
               VetorLabel[Conta_label_usada_gb].posicaovetor:=Conta_label_usada_gb;
               //Atribuindo o evento onclick ao um evento generico que estara no form
               VetorLabel[Conta_label_usada_gb].OnClick:=ClicaLabel;
               //Conta quantas labels foram usadas
               inc(Conta_label_usada_gb,1);

               //Criando a Label de Dados
               VetorLabel[Conta_label_usada_gb]:=Tlabelrel.create(nil);
               VetorLabel[Conta_label_usada_gb].Parent := FdesenhaRelatorio;
               VetorLabel[Conta_label_usada_gb].height := 30;
               VetorLabel[Conta_label_usada_gb].width := 120;
               VetorLabel[Conta_label_usada_gb].caption :='BD_'+Self.Query_Selecao.Fields[cont].DisplayName;
               VetorLabel[Conta_label_usada_gb].rotulo:=VetorLabel[Conta_label_usada_gb].caption;
               VetorLabel[Conta_label_usada_gb].Reposiciona(30,linhaatual);
               VetorLabel[Conta_label_usada_gb].QuantidadeCaracteres:=length(VetorLabel[Conta_label_usada_gb].rotulo);
               VetorLabel[Conta_label_usada_gb].Font.Name:='Courier New';
               VetorLabel[Conta_label_usada_gb].Font.Size:=8;
               VetorLabel[Conta_label_usada_gb].Font.Color:=clRed;//para diferenciar
               VetorLabel[Conta_label_usada_gb].Transparent:=True;
               VetorLabel[Conta_label_usada_gb].nomecampoBD:=Self.Query_Selecao.Fields[cont].Text;
               VetorLabel[Conta_label_usada_gb].tipo:='D';//dados
               VetorLabel[Conta_label_usada_gb].posicaovetor:=Conta_label_usada_gb;
               //Atribuindo o evento onclick ao um evento generico que estara no form
               VetorLabel[Conta_label_usada_gb].OnClick:=ClicaLabel;
               //incrementando a linha
               inc(linhaatual,1);
               //Conta quantas labels foram usadas
               inc(Conta_label_usada_gb,1);
          End;

          //Visualizando o Relatorio
          ShowModal;

          //Ao Retornar desalocando as labels que foram
          //alocadas no vetor
          for cont:=Conta_label_usada_gb-1 downto 0 do
          Begin
              Freeandnil(VetorLabel[cont]);
          End;
     End;
end;

end.
