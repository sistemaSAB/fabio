unit uRecuperaDuplicidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,ClipBrd;

type
  TformRecuperaDuplicidade = class(TForm)
    Label25: TLabel;
    edtNumeroNFe: TEdit;
    listXML: TListBox;
    lbCountArquivos: TLabel;
    btnRecuperar: TSpeedButton;
    OpenDialog1: TOpenDialog;
    procedure edtNumeroNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnRecuperarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtNumeroNFeExit(Sender: TObject);
    procedure listXMLKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormResize(Sender: TObject);
  private
    procedure SetHorizontalScrollBar(list: TListBox);
    procedure findFiles(currentDir,filter,numeroBusca:string; Itens:TStrings);
    procedure createCornners(Control: TWinControl;cornnerSize:integer);
    { Private declarations }
  public
    pathXML:string;
    numeroNFe:string;
  end;

var
  formRecuperaDuplicidade: TformRecuperaDuplicidade;

implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TformRecuperaDuplicidade.edtNumeroNFeKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
var
  currentDir,filter:string;
  confNFe:TobjSqlTxt;
begin

  if (Key = vk_f9) then
  begin

    if OpenDialog1.Execute then
    begin

      listXML.Items.Add( OpenDialog1.FileName );
      listXML.Selected[0] := true;
      edtNumeroNFe.Text := tirazeroesquerda( Copy(RetornaSoNumeros( listXML.Items[0] ),26,9) );
      SetHorizontalScrollBar( listXML );

    end;

  end
  else if ( Key = vk_return ) then
  begin

    try

      Screen.Cursor := crHourGlass;

      {pegando o diretorio de backup}
      try
        confNFe := TobjSqlTxt.Create(nil,ExtractFilePath(Application.ExeName)+'sqlNFE.txt');
        currentDir := confNFe.readString('NFE DIRETORIO BACKUP',true);
      finally
        FreeAndNil(confNFe);
      end;

      filter := '*.xml';
      findFiles(currentDir,filter,edtNumeroNFe.Text,listXML.Items);
      SetHorizontalScrollBar( listXML );

    finally
      Screen.Cursor := crDefault;
    end;
    

  end;


end;

procedure TformRecuperaDuplicidade.SetHorizontalScrollBar(list : TListBox) ;
var
  j, MaxWidth: integer;
begin

  MaxWidth := 0;

  for j := 0 to list.Items.Count - 1 do
    if MaxWidth < list.Canvas.TextWidth(list.Items[j]) then
        MaxWidth := list.Canvas.TextWidth(list.Items[j]) ;

  SendMessage(list.Handle, LB_SETHORIZONTALEXTENT,MaxWidth + 5, 0) ;

end;

procedure TformRecuperaDuplicidade.findFiles(currentDir,filter,numeroBusca:string; Itens:TStrings);
var
  sr: TSearchRec;
  chave:string;
  numero:string;
  index_:integer;
begin

  itens.Clear;
  if FindFirst(CurrentDir + Filter, faAnyFile, sr) = 0 then
  repeat

    index_ := Pos('_',sr.Name);

    if index_ > 0 then
      chave := Copy(sr.Name,1,(index_-1))
    else
      chave := sr.Name;

    numero := tirazeroesquerda( Copy(RetornaSoNumeros( chave ),26,9) );

    if numero = numeroBusca then
      Itens.Add( CurrentDir + sr.Name );

  until FindNext(sr) <> 0;

  if itens.Count = 0 then
    lbCountArquivos.Caption := ' nenhum arquivo encontrado'
  else
  if Itens.Count = 1 then
    lbCountArquivos.Caption := ' 1 arquivo encontrado'
  else
    lbCountArquivos.Caption := IntToStr( Itens.Count ) + ' arquivos encontrados';

  SysUtils.FindClose(sr);

end;

procedure TformRecuperaDuplicidade.btnRecuperarClick(Sender: TObject);
begin

  self.Tag := 0;

  if listXML.Items.Count > 0 then
  begin

    if (listXML.ItemIndex >= 0) then
    begin

      pathXML := listXML.Items [ listXML.ItemIndex ];
      self.Tag := 1;
      self.Close;

    end
    else
      ShowMessage( 'selecione um arquivo' );

  end
  else
    ShowMessage( 'Nenhum arquivo selecionado para recupera��o' );


end;

procedure TformRecuperaDuplicidade.FormShow(Sender: TObject);
begin
  self.tag := 0;
end;

procedure TformRecuperaDuplicidade.edtNumeroNFeExit(Sender: TObject);
begin
  numeroNFe := edtNumeroNFe.Text;
end;

procedure TformRecuperaDuplicidade.listXMLKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  {pressionou Control + C e a lista n�o esta vazia? copia para area de transferencia}
  if (Shift = [ssCtrl]) and (Key = 67) then
    if listXML.ItemIndex >= 0 then
      Clipboard.AsText := listXML.Items[listXML.itemIndex];

end;

procedure TformRecuperaDuplicidade.createCornners(Control: TWinControl;cornnerSize:integer);
var
  R: TRect;
  Rgn: HRGN;
begin

  with Control do
  begin
     R := ClientRect;
     rgn := CreateRoundRectRgn(R.Left, R.Top, R.Right, R.Bottom, cornnerSize, cornnerSize);
     Perform(EM_GETRECT, 0, lParam(@r));
     InflateRect(r, - 5, - 5);
     Perform(EM_SETRECTNP, 0, lParam(@r));
     SetWindowRgn(Handle, rgn, True);
     Invalidate;
  end;

end;

procedure TformRecuperaDuplicidade.FormResize(Sender: TObject);
begin
  createCornners(listXML,5);
  createCornners(edtNumeroNFe,5);
end;

end.
