unit UalteraFaturas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons;

type
  TfAlteraFaturas = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    edtNumeroParcelas: TEdit;
    Label2: TLabel;
    edtValorNF: TEdit;
    Panel2: TPanel;
    Label3: TLabel;
    lbValorNF: TLabel;
    Panel3: TPanel;
    Panel4: TPanel;
    Splitter1: TSplitter;
    Panel5: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    listDatas: TListBox;
    ListValores: TListBox;
    procedure FormShow(Sender: TObject);
    procedure edtNumeroParcelasExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure listDatasDblClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure ListValoresDblClick(Sender: TObject);
    procedure edtValorNFKeyPress(Sender: TObject; var Key: Char);
  private
    procedure ajustaData;
    procedure ajustaValor;
  public
      data:TDateTime;
  end;

var
  fAlteraFaturas: TfAlteraFaturas;

implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TfAlteraFaturas.ajustaData;
var
  i:integer;
  mes:string;
  dataAux:TDateTime;
begin

  if (edtNumeroParcelas.Text <> '') then
  begin

    listDatas.Clear;
    dataAux := self.data;

    for i := 1  to StrToIntDef(edtNumeroParcelas.Text,1)  do
    begin

      listDatas.Items.Add(DateToStr(dataAux));
      dataAux := IncMonth(dataAux);

    end;

    ajustaValor;

  end;

end;

procedure TfAlteraFaturas.ajustaValor;
var
  i:integer;
  valor:Currency;
  diferenca:Currency;
begin

  valor := StrToCurr(edtValorNF.Text)/StrToInt(edtNumeroParcelas.Text);
  valor := trunca(valor);

  ListValores.Clear;
  for i := 1  to StrToInt(edtNumeroParcelas.Text)  do
    ListValores.Items.Add(CurrToStr(valor));

  diferenca := StrToInt(edtNumeroParcelas.Text) * valor;

  if diferenca <> StrToCurr(edtValorNF.Text) then
  begin
    valor := valor +  StrToCurr(edtValorNF.Text) - diferenca;
    ListValores.Items[StrToInt(edtNumeroParcelas.Text)-1] := CurrToStr(valor);
  end


end;

procedure TfAlteraFaturas.FormShow(Sender: TObject);
begin

  lbValorNF.Caption :=  FormatCurr('0.00 R$',StrToCurrDef(edtValorNF.text,0));
  ListValores.Items.Add(edtValorNF.Text);
  edtNumeroParcelasExit(edtNumeroParcelas);

end;

procedure TfAlteraFaturas.edtNumeroParcelasExit(Sender: TObject);
begin
  ajustaData;
end;

procedure TfAlteraFaturas.FormKeyPress(Sender: TObject; var Key: Char);
begin

  if (key = #13) then
    Perform(Wm_NextDlgCtl,0,0);


end;

procedure TfAlteraFaturas.listDatasDblClick(Sender: TObject);
var
  newDate:string;
begin

  newDate := InputBox('Altera data','Nova data: ',listDatas.Items[listDatas.ItemIndex]);

  if validaData(newDate) then
    listDatas.Items[listDatas.ItemIndex] := newDate;

end;

procedure TfAlteraFaturas.BitBtn2Click(Sender: TObject);
begin
  self.Tag := 0;
  self.Close;
end;

procedure TfAlteraFaturas.BitBtn1Click(Sender: TObject);
begin
  self.Tag := 1;
  self.Close;
end;

procedure TfAlteraFaturas.ListValoresDblClick(Sender: TObject);
begin
     EdtValornf.text:=ListValores.Items[ListValores.itemindex];
     edtvalornf.setfocus
end;

procedure TfAlteraFaturas.edtValorNFKeyPress(Sender: TObject;
  var Key: Char);
begin
     If key=#13
     Then Begin
              try
                 strtofloat(tira_Ponto(edtvalornf.text));
                 if (ListValores.itemindex=-1)
                 or (listDatas.itemindex=-1)
                 Then Begin
                           ListValores.itemindex:=0;
                           listDatas.itemindex:=0;
                 End;

                 ListValores.items[ListValores.itemindex]:=Formata_Valor(edtvalornf.text);
                 //lbValorNF.caption:=Formata_valor(Floattostr(SomaParcelas));
                 edtvalornf.text:='';

              Except

              End;
     End;
end;

end.
