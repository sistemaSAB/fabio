unit UobjTransmiteNFE;

interface

uses Classes,pcnNFe,pcnConversao,IBQuery,IBDatabase,UessencialGlobal,SysUtils,
     forms,Dialogs,ACBrNFe,DB,Controls,UComponentesNfe,pcnLeitor, //ACBrNFeDANFERave
     DateUtils,ACBrUtil,ACBrDFeSSL;

type
  TFonteDados = class(TComponent)
  private
	//teste
    FQuery :TIBQuery;
    FqueryTemp: TIBQuery;
    FSQL: String;
    FDatabase: TIBDatabase;
    FRegistro: String;
    FpCodigoNF: Integer;
    FtipoNF: string;
    FnumeroNFE: string;
    FrefNFe_ref: string;
    FchaveRef: string;
    FcupomRef: Boolean;
    FinfComplementares: string;
    FCRT: string;
    FarquivoSQL: string;
    FarquivoSQlManual: boolean;
    FnfeProcessando: Boolean;
    procedure SetDatabase(const Value: TIBDatabase);
    function AbreQuery:Boolean;Virtual;
    function teveErros: Boolean;
    function getCampoSQL(const sectionname:string):string;
    function getCampoTable(const sectionName:string):TIBQuery;
    function getValueTable(const sectionName:string):string;
    procedure SetRegistro(const Value: String);
    procedure GetFieldTrim(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldSoNumeros(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldSoNumerosUpperCase(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldUpperCase(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure GetFieldTrunca(Sender: TField; var Text: String; DisplayText:Boolean);
    procedure SetpCodigoNF(const Value: Integer);
    procedure SettipoNF(const Value: string);
    procedure SetnumeroNFE(const Value: string);
    procedure SetchaveRef(const Value: string);
    procedure SetinfComplementares(const Value: string);
    procedure SetCRT(const Value: string);
    procedure setArquivoSQL(const Value: string);
    procedure setArquivoSQLManual(const Value: boolean);
    procedure preparaFComponente();

  protected
    procedure preenche;virtual;abstract;
    procedure SetParametros;Virtual;Abstract;
    function ValidaRegistros:Boolean; Virtual;Abstract;
    function ValidaBrancosNulos:Boolean;Virtual;
    procedure SetCampos;Virtual;
  public
    strErros :TStringList;
    denegada :Boolean;
    property Database :TIBDatabase read FDatabase write SetDatabase;
    property Registro :String read FRegistro write SetRegistro;
    property pCodigoNF:Integer read FpCodigoNF write SetpCodigoNF;
    property tipoNF:string read FtipoNF write SettipoNF;
    property numeroNFE :string read FnumeroNFE write SetnumeroNFE;
    property chaveRef:string read FchaveRef write SetchaveRef;
    property infComplementares:string read FinfComplementares write SetinfComplementares;
    property CRT:string read FCRT write SetCRT;
    property arquivoSQL:string read FarquivoSQL write setArquivoSQL;
    property arquivoSQLManual:boolean read FarquivoSQlManual write setArquivoSQLManual;
    property nfeProcessamento: Boolean read FnfeProcessando write FnfeProcessando default false;

    function operacaoCombustivel(pCFOP: string): Boolean;

    constructor Create(AOwner: TComponent);override;
    destructor Destroy;override;
end;

type
  TFonteDadosParamData = Class(TFonteDados)
  protected
    procedure SetParametros;override;
  public
end;

type
  TComIDE = class (TFonteDadosParamData)
  private
    function getProximoNumero:Integer;
    function getIndPag(indpag:string):TpcnIndicadorPagamento;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    IDE:TIde;
end;

type
  TComComplementar = class(TComIDE)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComCupomRef = class(TComIDE)
  private
    FnCOO: string;
    FnECF: string;
    Fmodelo: string;
    FstrRefCupom: string;
    procedure SetnCOO(const Value: string);
    procedure SetnECF(const Value: string);
    procedure Setmodelo(const Value: string);
    function getModeloECF(pModelo:string=''):TpcnECFModRef;
    procedure SetstrRefCupom(const Value: string);
  protected
    property nECF:string read FnECF write SetnECF;
    property nCOO:string read FnCOO write SetnCOO;
    property modelo:string read Fmodelo write Setmodelo;
    property strRefCupom:string read FstrRefCupom write SetstrRefCupom;
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComDevolucao = class(TComIDE)
  private
    FmodeloRef: Integer;
    FCNPJ_ref: string;
    FcUF_ref: string;
    Fserie_ref: string;
    FnNF_ref: string;
    FAAMM_ref: string;
    Fmodelo_ref: string;
    procedure SetmodeloRef(const Value: Integer);
    procedure SetAAMM_ref(const Value: string);
    procedure SetCNPJ_ref(const Value: string);
    procedure SetcUF_ref(const Value: string);
    procedure Setmodelo_ref(const Value: string);
    procedure SetnNF_ref(const Value: string);
    procedure Setserie_ref(const Value: string);
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
    property modeloRef:Integer read FmodeloRef write SetmodeloRef;
    property cUF_ref    :string read FcUF_ref write SetcUF_ref;
    property AAMM_ref   :string read FAAMM_ref write SetAAMM_ref;
    property CNPJ_ref   :string read FCNPJ_ref write SetCNPJ_ref;
    property modelo_ref :string read Fmodelo_ref write Setmodelo_ref;
    property serie_ref  :string read Fserie_ref write Setserie_ref;
    property nNF_ref    :string read FnNF_ref write SetnNF_ref;
  public
end;

type
  TComInfAdic = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    INFADIC: TInfAdic;
end;

type
  TComEmit = class(TFonteDados)
  private
    function getCRT(crt:string):TpcnCRT;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    EMIT:TEmit;
end;

type
  TComEnderEmit = class(TFonteDados)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    ENDEREMIT:TenderEmit;
end;

type
  TComDestC = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    DEST: TDest;
end;

type
  TComDestCNFCe = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    DEST: TDest;
end;

type
  TComDestF = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    DEST: TDest;
end;

type
  TComEnderDest = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    ENDERDEST: TenderDest;
end;

type
  TComEnderDestC = class(TComEnderDest)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComEnderDestCNFCe = class(TComEnderDest)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComEnderDestF = class(TComEnderDest)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComTransp = class(TFonteDadosParamData)
  private
    function getModFrete():TpcnModalidadeFrete;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    Transp:TTransp;
end;

type
  TComTransporta = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    Transporta:TTransporta;
end;

type
  TComveicTransp = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    veicTransp:TveicTransp;
end;

type
  TComVol = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    vol:TVolCollection;
end;

type
  TComDet = class(TFonteDadosParamData)
  private
    ind:Integer;
  protected
  public
    Det:TDetCollection;
end;

type
  TComImposto = class(TComDet)
  private
    FTot:Currency;
    ind:integer;
    pCodigoProdnf:string;
    pCodigoProduto:string;
  protected
    procedure setParametros;override;
    procedure inicializa();
  public
end;

type
  TComICMS = class(TComImposto)
  private
    FTotvBC :Currency;
    FTotVtotTrib:Currency;
    function getOrigICMS():TpcnOrigemMercadoria;
    function getCSTICMS():TpcnCSTIcms;
    function operacaoCEST():Boolean;
    function getCSOSN():TpcnCSOSNIcms;
    function getModBCICMS():TpcnDeterminacaoBaseIcms;
    function zeraImpostoNoSimples:Boolean;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    procedure inicializa();
end;

type
  TComICMSST = class(TComImposto)
  private
    FTotvBC :Currency;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    procedure inicializa();
end;

type
  TComPIS = class(TComImposto)
  private
    function getCSTPIS():TpcnCstPis;
    function zeraImpostoSimples:Boolean;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComCOFINS = class(TComImposto)
  private
    function getCSTCOFINS():TpcnCstCofins;
    function zeraImpostoSimples:Boolean;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComIPI = class(TComImposto)
  private
    function getCSTIPI():TpcnCstIpi;
    function zeraImpostoSimples:Boolean;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComDIFAL = class(TComImposto)
  private
    FTotvFCPUFDest  : Currency;
    FTotvICMSUFDest : Currency;
    FTotvICMSUFRemet: Currency;
    function zeraImpostoSimples:Boolean;
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComCobranca = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    Cobr:TCobr;
end;

type
  TComFat = class(TComCobranca)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComDup = class(TComCobranca)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;


type
  TComProdutos = class(TComDet)
  private
    FTotSeg   :Currency;
    FTotFret  :Currency;
    FTotVprod :Currency;
    FTotVNF   :Currency;
    FTotVDesc :Currency;
    FTotVoutr :Currency;
    FICMS    :TComICMS;
    FICMSST  :TComICMSST;
    FPIS     :TComPIS;
    FCOFINS  :TComCOFINS;
    FIPI     :TComIPI;
    procedure preencheTot();
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    total   :TTotal;
    procedure inicializa();
    Constructor create(AOwner:TComponent);
    destructor destroy;override;
end;

type
  TComCartaCorrecao = class(TFonteDados)
  private
    FnSeqEvento: Integer;
    FidLote: integer;
    FxCorrecao: string;
    FcondUso: string;
    Fchaveacesso: string;
    FcOrgao: integer;
    FCNPJCPF: string;
    FPathXML: string;
    procedure SetcondUso(const Value: string);
    procedure SetidLote(const Value: integer);
    procedure SetnSeqEvento(const Value: Integer);
    procedure SetxCorrecao(const Value: string);
    function getnSeqEvento(chAcesso:string) : Integer;
    procedure Setchaveacesso(const Value: string);
    procedure SetcOrgao(const Value: integer);
    procedure SetCNPJCPF(const Value: string);
  protected
    procedure preenche;override;
    function enviarCarta:Boolean;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
    carta:TCartaCorrecao;
    property condUso:string read FcondUso write SetcondUso;
    property idLote:integer read FidLote write SetidLote;
    property nSeqEvento:Integer read FnSeqEvento write SetnSeqEvento;
    property xCorrecao:string read FxCorrecao write SetxCorrecao;
    property chaveacesso:string read Fchaveacesso write Setchaveacesso;
    property cOrgao:integer read FcOrgao write SetcOrgao;
    property CNPJCPF:string read FCNPJCPF write SetCNPJCPF;
    property PathXML : string read FPathXML write FPathXML;
    procedure inicializa();
    function getRetWs:string;
    function getRetornoWS:string;
    function getCaminhoXML:string;
end;

type
  TComFormaPag = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;

type
  TComAutorizacao = class(TFonteDadosParamData)
  private
  protected
    procedure preenche;override;
    procedure setParametros;override;
    procedure setCampos;override;
    function validaRegistros:Boolean;override;
  public
end;


type
  TObjTransmiteNFE = class(TFonteDados)
  private

    path_cancelamento:string;
    {ide}
    FIde: TComIDE;
    FInfAdic: TComInfAdic;
    FComplementar :TComComplementar;
    FDevolucao :TComDevolucao;
    FCupomRef:TComCupomRef;
    FCobrFat:TComFat;
    FCobrDup:TComDup;

    {emitende}
    FEmit: TComEmit;
    FEnderEmit: TComEnderEmit;

    {destinatario}
    FDestC :TComDestC;
    FDestF :TComDestF;
    FEnderDestC :TComEnderDestC;
    FEnderDestF :TComEnderDestF;

    {transporte}
    FTransp :TComTransp;
    FTransporta :TComTransporta;
    FVeicTransp :TComveicTransp;
    FVol :TComVol;

    {produtos}
    FProdutos :TComProdutos;

    {NFC-e forma de pagamento}
    FFormasPagamento: TComFormaPag;

    FdataBase: TIBDatabase;                          
    FpCodigoNF: Integer;                             
    FTipoNF: string;                                 
    Fcontingencia: Boolean;                          
    FDirBackup:string;
    FfLp: Boolean;                                   
    FnumeroNFE: string;                              
    FnfeComplementar: Boolean;                       
    FnfeDevolucao: Boolean;                          
    Fref55: Boolean;                                 
    Fref01: Boolean;                                 
    FmodeloRef: Integer;                             
    FchaveRef: string;                               
    FCNPJ_ref: string;                               
    FcUF_ref: string;                                
    Fserie_ref: string;                              
    FnNF_ref: string;                                
    FAAMM_ref: string;                               
    Fmodelo_ref: string;                             
    FnCOO: string;
    FnECF: string;
    Fmodeloecf: string;
    FcartaCorrecao: Boolean;
    Fend_Rua: string;
    Fend_Bairro: string;
    Fend_Complemento: string;
    Fend_Municipio: string;
    Fend_Numero: string;
    Fend_Cpf_Cnpj: string;
    Fend_UF: string;
    Fend_Cod_Municipio: string;
    FstrRefCupom: string;
    FConsumidorFinal: Boolean;
    procedure SetdataBase(const Value: TIBDatabase);
    procedure inicializa();
    procedure iniciaGeracao();
    function configuraComponentes():Boolean;
    function configuraComponentesNFCE():boolean;
    function  teveErros:Boolean;
    function getCampoTable(const sectionName:string): TIBQuery;
    procedure SetpCodigoNF(const Value: Integer);
    procedure SetTipoNF(const Value: string);
    procedure Setcontingencia(const Value: Boolean);
    procedure SetfLp(const Value: Boolean);
    procedure SetnumeroNFE(const Value: string);
    procedure SetnfeComplementar(const Value: Boolean);
    procedure SetnfeDevolucao(const Value: Boolean);
    procedure Setref01(const Value: Boolean);
    procedure Setref55(const Value: Boolean);
    procedure SetmodeloRef(const Value: Integer);
    procedure SetchaveRef(const Value: string);
    procedure SetAAMM_ref(const Value: string);
    procedure SetCNPJ_ref(const Value: string);
    procedure SetcUF_ref(const Value: string);
    procedure Setmodelo_ref(const Value: string);
    procedure SetnNF_ref(const Value: string);
    procedure Setserie_ref(const Value: string);
    procedure SetnCOO(const Value: string);
    procedure SetnECF(const Value: string);
    procedure Setmodeloecf(const Value: string);
    procedure limpaCampos;
    procedure SetcartaCorrecao(const Value: Boolean);
    procedure setend_Bairro(const Value: string);
    procedure setend_Complemento(const Value: string);
    procedure setend_Cpf_Cnpj(const Value: string);
    procedure setend_Municipio(const Value: string);
    procedure setend_Numero(const Value: string);
    procedure setend_Rua(const Value: string);
    procedure setend_UF(const Value: string);
    procedure setend_Cod_Municipio(const Value: string);
    procedure SetstrRefCupom(const Value: string);
    procedure setFConsumidorFInal(const Value: Boolean);
  protected
  public
    strErros: TStringList;
    FCCe:TComCartaCorrecao;
    property dataBase: TIBDatabase read FdataBase write SetdataBase;
    property pCodigoNF: Integer read FpCodigoNF write SetpCodigoNF;
    property TipoNF :string read FTipoNF write SetTipoNF;
    property contingencia :Boolean read Fcontingencia write Setcontingencia default false;
    property fLp : Boolean read FfLp write SetfLp default false; {consulta o retorno}
    property numeroNFE :string read FnumeroNFE write SetnumeroNFE;
    property nfeComplementar:Boolean read FnfeComplementar write SetnfeComplementar default False;
    property nfeDevolucao:Boolean read FnfeDevolucao write SetnfeDevolucao default false;
    property cartaCorrecao : Boolean read FcartaCorrecao write SetcartaCorrecao default false;
    property modeloRef:Integer read FmodeloRef write SetmodeloRef;
    property chaveRef:string read FchaveRef write SetchaveRef;
    property cUF_ref    :string read FcUF_ref write SetcUF_ref;
    property AAMM_ref   :string read FAAMM_ref write SetAAMM_ref;
    property CNPJ_ref   :string read FCNPJ_ref write SetCNPJ_ref;
    property modelo_ref :string read Fmodelo_ref write Setmodelo_ref;
    property serie_ref  :string read Fserie_ref write Setserie_ref;
    property nNF_ref    :string read FnNF_ref write SetnNF_ref;
    property nECF:string read FnECF write SetnECF;
    property nCOO:string read FnCOO write SetnCOO;
    property strRefCupom:string read FstrRefCupom write SetstrRefCupom;
    property modeloecf:string read Fmodeloecf write Setmodeloecf;
    property CRT:string read FCRT write SetCRT;
    property end_Cpf_Cnpj: string read Fend_Cpf_Cnpj write setend_Cpf_Cnpj;
    property end_Rua: string read Fend_Rua write setend_Rua;
    property end_Numero: string read Fend_Numero write setend_Numero;
    property end_Complemento: string read Fend_Complemento write setend_Complemento;
    property end_Bairro: string read Fend_Bairro write setend_Bairro;
    property end_Municipio: string read Fend_Municipio write setend_Municipio;
    property end_Cod_Municipio: string read Fend_Cod_Municipio write setend_Cod_Municipio;
    property end_UF: string read Fend_UF write setend_UF;
    property consumidorFinal: Boolean read FConsumidorFinal write setFConsumidorFInal;

    procedure preencheSql;
    procedure preencheSqlNFCE;
    procedure preencheComponente;
    procedure preencheComponenteEvento;
    procedure preencheComponenteEventoNFCE;
    procedure transmiteSefaz;
    procedure transmiteSefazEvento;
    procedure transmiteSefazEventoNFCE;
    function Transmite():Boolean;
    function TransmiteNFCE: Boolean;
    function consultaServico(visualizar:Boolean = true):string;
    function consultaServicoNFCE(visualizar: Boolean = true): string;
    procedure consultaXML;
    procedure consultaXMLNFCE;
    procedure retornaProtocolo();
    procedure retornaProtocoloNFCe;
    procedure downloadXML(chaveAcesso:string);
    function  manifestacaoDestinatario(tpEvento:SmallInt;chaveAcesso,xJust:string):boolean;
    procedure imprimeDanfe(pathXML:string = '');
    procedure imprimeDanfeNFCe(pathXML: string);
    function transmiteXMLContingente(var pRecibo:string;pathXML:string = ''):Boolean;
    function consultaPelaChave(pChave,pUF:string;visualizarMSG:Boolean) : Integer;
    function consultaPelaChaveNFCe(pChave, pUF: string;
      visualizarMSG: Boolean): integer;
    function cancelaNFe(pathXML:string;motivoCanc:string):Boolean;
    function cancelaNFCe(pathXML, motivoCanc: string): Boolean;
    function cancelaPelaChave(chave,protocoloAutorizacao,justificativa:string;var arqCanc:string):Boolean;
    function cancelaPelaChaveNFCe(chave, protocoloAutorizacao,
      justificativa: string; var arqCanc: string): Boolean;
    function consultaSituacao(pathXML:string = ''):Boolean;
    function consultaSituacaoNFCe(pathXML: string): Boolean;
    function inutilizaFaixa(var pprotocolo:string;cnpj,Justificativa,ano,modelo,serie,numeroInicial,numeroFinal:string):Boolean;
    function inutilizaFaixaNFCe(var pprotocolo: string; cnpj,
      Justificativa, ano, modelo, serie, numeroInicial,
      numeroFinal: string): Boolean;

    function enviarEmail(CaminhoXML:string;const sSmtpHost, sSmtpPort, sSmtpUser, sSmtpPasswd,
                         sFrom, sTo, sAssunto: String; sMensagem : TStrings;
                         SSL : Boolean; EnviaPDF: Boolean = true;
                         sCC: TStrings=nil; Anexos:TStrings=nil;
                         PedeConfirma: Boolean = False; AguardarEnvio: Boolean = False;
                         NomeRemetente: String = '';TLS : Boolean = True):Boolean;

    function retornaChaveAcesso:string;
    function retornaArquivoXML:string;
    function retornaRecibo:string;
    function selecionaCertificado:string;
    function getCampoSQL2(const sectionname:string):string;
    function retornaCertificado:string;
    function consultaNotaProcessada(pXml: String): integer;

    constructor Create(AOwner: TComponent;ibdatabase: TIBDatabase);overload;
    destructor destroy;override;

end;

implementation

uses ufrmStatus, pcnEventoNFe, pcnEnvEventoNFe, ACBrNFeWebServices, pcnConversaoNFe,
  ACBrNFeConfiguracoes, ACBrDFe, Math;


{ TComIDE }

function TComIDE.getIndPag(indpag:string): TpcnIndicadorPagamento;
begin
  if indpag = '0' then
    Result := ipVista
  else
  if indpag = '1' then
    Result := ipPrazo
  else
    Result := ipOutras;
end;

function TComIDE.getProximoNumero: Integer;
begin
  Result := getCampoTable('PROXIMO NUMERO').Fields[1].AsInteger;
end;

procedure TComIDE.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  ide.tpImp   := FComponentesNfe.ACBrNFe.DANFE.TipoDANFE;
  IDE.natOp   := FQuery.Fields[0].Text;
  IDE.dEmi    := FQuery.Fields[1].AsDateTime;
  IDE.dSaiEnt := FQuery.Fields[2].AsDateTime;
  IDE.serie   := FQuery.Fields[3].AsInteger;
  IDE.hSaiEnt := FQuery.Fields[4].AsDateTime;
  IDE.cUF     := getCampoTable('CODIGO UF EMITENTE').Fields[0].AsInteger;
  IDE.cMunFG  := getCampoTable('CODIGO CIDADE EMITENTE').Fields[0].AsInteger;
  IDE.cNF     := StrToInt(self.numeroNFE); {getProximoNumero;}
  IDE.nNF     := ide.cNF;
  IDE.verProc   := '1.0.0.0'; //Vers�o do seu sistema
  IDE.finNFe  := fnNormal;

  if self.tipoNF = 'SAIDA' then
    IDE.tpNF    := tnSaida
  else
    IDE.tpNF    := tnEntrada;

  IDE.indPag := self.getIndPag(FQuery.Fields[5].Text);

end;

procedure TComIDE.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Natureza de opera��o';
  FQuery.Fields[1].disPlayLabel := 'N�mero de Serie';
  FQuery.Fields[2].DisplayLabel := 'Indicador de pagamento';
  FQuery.Fields[3].disPlayLabel := 'Data de emiss�o';
  FQuery.Fields[4].disPlayLabel := 'Data de saida';
  FQuery.Fields[5].disPlayLabel := 'Hora de saida';

  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;

end;

procedure TComIDE.setParametros;
begin
  inherited;
end;

function TComIDE.validaRegistros:Boolean;
begin
  inherited;
  if FQuery.Fields[3].AsDateTime > FQuery.Fields[4].AsDateTime then
    strErros.Add('Data de emiss�o da nota deve ser menor ou igual a data de saida');

  if self.numeroNFE = '' then
    strErros.Add('N�mero NFe vazio');

  if not (IsNumeric(self.numeroNFE)) then
    strErros.Add('N�mero NFe inv�lido');
end;

{ TFonteDados }

function TFonteDados.AbreQuery: Boolean;
begin
  result := False;
  if self.FSQL = 'vazio;' then
    Exit;

  Application.ProcessMessages;

  if FQuery = nil then
    FQuery := TIBQuery.Create(Self);

  FQuery.Close;

  FQuery.DisableControls;
  FQuery.Database := FDatabase;
  FQuery.SQL.Text := Self.FSQL;

  if FQuery.Params.Count > 0 then
    Self.SetParametros;

  FQuery.Open;
  if not (FQuery.IsEmpty) then
  begin
    Self.SetCampos;
    FQuery.First;
    while not FQuery.Eof do
    begin
      self.ValidaRegistros;
      self.ValidaBrancosNulos;
      FQuery.Next;
    end;
    FQuery.First;
  end
  else
  begin
    Result := False;
    Exit;
  end;

  result := True;
end;

constructor TFonteDados.Create(AOwner: TComponent);
begin
  inherited;
  self.arquivoSQL := '\sqlNFE.txt';
end;

destructor TFonteDados.Destroy;
begin

  inherited;
end;


function TFonteDados.getCampoSQL(const sectionname: string): string;
var
  objsql:TobjSqlTxt;
begin

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;

  objsql := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try
    result := objsql.readString(sectionName);
  finally
    FreeAndNil(objsql);
  end;

end;


function TFonteDados.getCampoTable(const sectionName:string): TIBQuery;
var
  objsql:TobjSqlTxt;
begin

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;

  objsql := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try

    if FqueryTemp = nil then
      FqueryTemp := TIBQuery.Create(Self);

    FqueryTemp.Active := False;

    FqueryTemp.DisableControls;
    FqueryTemp.Database := FDatabase;
    FqueryTemp.SQL.Text := objsql.readString(sectionName);
    FqueryTemp.Active := True;
    result := FqueryTemp;

  finally
    FreeAndNil(objsql);
  end;

end;

procedure TFonteDados.GetFieldSoNumeros(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := RetornaSoNumeros(Sender.AsString);
end;

procedure TFonteDados.GetFieldSoNumerosUpperCase(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
    if Sender.AsString <> '' then
    Text := UpperCase(RetornaSoNumeros(Sender.AsString));
end;

procedure TFonteDados.GetFieldTrim(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if sender.AsString <> '' then
    Text := Trim(sender.AsString);
end;

procedure TFonteDados.GetFieldTrunca(Sender: TField; var Text: String;DisplayText: Boolean);
begin
  if Sender.AsString <> '' then
    Text := formata_valor(Sender.AsString,2,false);
end;

procedure TFonteDados.GetFieldUpperCase(Sender: TField; var Text: String;DisplayText: Boolean);
begin
  if sender.AsString <> '' then
    Text := Trim(UpperCase(sender.AsString));
end;

function TFonteDados.getValueTable(const sectionName: string): string;
var
  objsql:TobjSqlTxt;
begin

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;

  objsql := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try
    Result := objsql.readString(sectionName,true);
  finally
    FreeAndNil(objsql);
  end;

end;

function TFonteDados.operacaoCombustivel(pCFOP: string): Boolean;
begin

  Result := (pCFOP = '1651') or (pCFOP = '1652') or (pCFOP = '1653') or
            (pCFOP = '1658') or (pCFOP = '1659') or (pCFOP = '1660') or
            (pCFOP = '1661') or (pCFOP = '1662') or (pCFOP = '1663') or
            (pCFOP = '1664') or (pCFOP = '2651') or (pCFOP = '2652') or
            (pCFOP = '2653') or (pCFOP = '2658') or (pCFOP = '2659') or
            (pCFOP = '2660') or (pCFOP = '2661') or (pCFOP = '2662') or
            (pCFOP = '2663') or (pCFOP = '2664') or (pCFOP = '3651') or
            (pCFOP = '3652') or (pCFOP = '3653') or (pCFOP = '5651') or
            (pCFOP = '5652') or (pCFOP = '5653') or (pCFOP = '5654') or
            (pCFOP = '5655') or (pCFOP = '5656') or (pCFOP = '5657') or

            (pCFOP = '5658') or (pCFOP = '5659') or (pCFOP = '5660') or
            (pCFOP = '5661') or (pCFOP = '5662') or (pCFOP = '5663') or
            (pCFOP = '5664') or (pCFOP = '5665') or (pCFOP = '5666') or
            (pCFOP = '5667') or (pCFOP = '6651') or (pCFOP = '6652') or
            (pCFOP = '6653') or (pCFOP = '6654') or (pCFOP = '6655') or

            (pCFOP = '6656') or (pCFOP = '6657') or (pCFOP = '6658') or
            (pCFOP = '6659') or (pCFOP = '6660') or (pCFOP = '6661') or
            (pCFOP = '6662') or (pCFOP = '6663') or (pCFOP = '6664') or
            (pCFOP = '6665') or (pCFOP = '6666') or (pCFOP = '6667') or
            (pCFOP = '7651') or (pCFOP = '7654') or (pCFOP = '7667');

end;

procedure TFonteDados.preparaFComponente;
begin

  try
    if Assigned(FComponentesNfe) then
      FreeAndNil(FComponentesNfe);
  except
  end;

  FComponentesNfe := TFComponentesNfe.Create(self);

end;

procedure TFonteDados.setArquivoSQL(const Value: string);
begin
  FarquivoSQL := Value;
end;

procedure TFonteDados.setArquivoSQLManual(const Value: boolean);
begin
  FarquivoSQlManual := Value;
end;

procedure TFonteDados.SetCampos;
var
  i: integer;
begin
  for i := 0  to FQuery.Fields.Count-1  do
    FQuery.Fields[i].Required := True;
end;

procedure TFonteDados.SetchaveRef(const Value: string);
begin
  FchaveRef := Value;
end;

procedure TFonteDados.SetCRT(const Value: string);
begin
  FCRT := Value;
end;

procedure TFonteDados.SetDatabase(const Value: TIBDatabase);
begin
  FDatabase := Value;
end;

procedure TFonteDados.SetinfComplementares(const Value: string);
begin
  FinfComplementares := Value;
end;

procedure TFonteDados.SetnumeroNFE(const Value: string);
begin
  FnumeroNFE := Value;
end;

procedure TFonteDados.SetpCodigoNF(const Value: Integer);
begin
  FpCodigoNF := Value;
end;

procedure TFonteDados.SetRegistro(const Value: String);
begin
  FRegistro := Value;
end;

procedure TFonteDados.SettipoNF(const Value: string);
begin
  FtipoNF := Value;
end;

function TFonteDados.teveErros: Boolean;
begin
  result := (strErros.Count > 0);
end;

function TFonteDados.ValidaBrancosNulos: Boolean;
var
  i :Integer;
begin

  for i := 0  to FQuery.FieldCount - 1 do
  begin
    Application.ProcessMessages;
    if FQuery.Fields[i].Required then
    begin
      if FQuery.Fields[i].AsString = ''  then
        strErros.Add('Erro no registro ' + FRegistro + ' campo: "'+FQuery.Fields[i].DisplayLabel+'" com valor em branco ou nulo');
    end;
  end;
  Result := (strErros.Count <= 0);

end;

{ TObjTransmiteNFE }


function TObjTransmiteNFE.cancelaNFe(pathXML:string;motivoCanc:string):Boolean;
var
  pCodigo:string;
  idLote:string;
  dhEventoaux:string;
begin

  result := False;

  preparaFComponente();

  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;
    
  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

  if not (FileExists (pathXML)) then
  begin
    MensagemAviso ('Arquivo XML n�o encontrado');
    Exit;
  end;

  FcomponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(pathXML);
  Pcodigo := inttostr(FcomponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.nNF);

  if (Pcodigo = '') then
  Begin
   mensagemErro('Erro! C�digo n�o encontrado');
   exit;
  End;

  if (MensagemPergunta('N�mero da NFe: '+pcodigo+#13+'Deseja Continuar?') = mrno) Then
   exit;


  idLote := '1';

  dhEventoaux := DateTimeToStr(now);
  dhEventoaux := InputBox('Hora','Hora do cancelamento: ',dhEventoaux);

  FComponentesNfe.ACBrNFe.EventoNFe.Evento.Clear;
  FComponentesNfe.ACBrNFe.EventoNFe.idLote := StrToInt(idLote) ;
  with FComponentesNfe.ACBrNFe.EventoNFe.Evento.Add do
  begin
    infEvento.dhEvento := StrToDateTime(dhEventoaux);
    infEvento.tpEvento := teCancelamento;
    infEvento.detEvento.xJust := motivoCanc;
  end;

  try
    Screen.Cursor := crHourGlass;
    FComponentesNfe.ACBrNFe.EnviarEvento(StrToInt(idLote));
  finally
    Screen.Cursor := crDefault;
  end;

  if (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 101) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 151) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 135) then
  begin
    strErros.Add('Erro ao cancelar a NF-e. C�digo do status: '+IntToStr(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat)+'. Motivo: '+FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo);
    Exit;
  end;

  FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
  FComponentesNfe.MemoResp.lines.SaveToFile(path_cancelamento+StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml');
  result := True;


end;

function TObjTransmiteNFE.cancelaNFCe(pathXML:string;motivoCanc:string):Boolean;
var
  pCodigo:string;
  idLote:string;
  dhEventoaux:string;
begin

  result := False;

  preparaFComponente;

  Self.configuraComponentesNFCE;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;

  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

  if not (FileExists (pathXML)) then
  begin
    MensagemAviso ('Arquivo XML n�o encontrado');
    Exit;
  end;

  FcomponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(pathXML);
  Pcodigo := inttostr(FcomponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide.nNF);

  if (Pcodigo = '') then
  Begin
   mensagemErro('Erro! C�digo n�o encontrado');
   exit;
  End;

  if (MensagemPergunta('N�mero da NFe: '+pcodigo+#13+'Deseja Continuar?') = mrno) Then
   exit;


  idLote := '1';

  dhEventoaux := DateTimeToStr(now);
  dhEventoaux := InputBox('Hora','Hora do cancelamento: ',dhEventoaux);

  FComponentesNfe.ACBrNFe.EventoNFe.Evento.Clear;
  FComponentesNfe.ACBrNFe.EventoNFe.idLote := StrToInt(idLote) ;
  with FComponentesNfe.ACBrNFe.EventoNFe.Evento.Add do
  begin
    infEvento.dhEvento := StrToDateTime(dhEventoaux);
    infEvento.tpEvento := teCancelamento;
    infEvento.detEvento.xJust := motivoCanc;
  end;

  try
    Screen.Cursor := crHourGlass;
    FComponentesNfe.ACBrNFe.EnviarEvento(StrToInt(idLote));
  finally
    Screen.Cursor := crDefault;
  end;

  //if (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 101) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 151) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 135) then
    //Exit;

  if (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 101) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 151) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 135) then
  begin
    strErros.Add('Erro ao cancelar a NF-e. C�digo do status: '+IntToStr(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat)+'. Motivo: '+FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo);
    Exit;
  end;

  FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
  FComponentesNfe.MemoResp.lines.SaveToFile(path_cancelamento+StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml');

  result := True;


end;

function TObjTransmiteNFE.configuraComponentes:Boolean;
var
  confNFe:TobjSqlTxt;
  gtDados:string;
  mesAno:string;
begin

  Result := False;

  self.strErros.Clear;

  if not arquivoSQLManual then
    self.arquivoSQL := 'SQLNFE.TXT';

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    strerros.add('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;

  confNFe := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try

    gtDados := '';
    gtDados := confNFe.readString('NFE ORIENTACAO DANFE',true);

    if (gtDados = 'RETRATO') Then
      FComponentesNfe.ACBrNFe.DANFE.TipoDANFE := tiRetrato
    else
      FComponentesNfe.ACBrNFe.DANFE.TipoDANFE := tiPaisagem;

    FComponentesNfe.ACBrNFeDANFERave1.TipoDANFE := FComponentesNfe.ACBrNFe.DANFE.TipoDANFE;

    gtDados := '';
    gtDados := UpperCase(confNFe.readString('NFE FORMA DE EMISAO DO DANFE',true));

    if (gtDados = 'NORMAL ON-LINE') Then
      FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao := teNormal
    else
    if (gtDados = 'CONTINGENCIA OFF-LINE') Then
      FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao := teContingencia
    else
    if (gtDados = 'SCAN') Then
      FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao :=  teSCAN
    else
    if (gtDados = 'DPEC') Then
      FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao :=  teDPEC
    else
    if (gtDados = 'FSDA') Then
      FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao :=  teFSDA;

    if contingencia then
      FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao := teFSDA;

    FComponentesNfe.ACBrNFe.Configuracoes.Geral.SSLLib := libCapicom;
    FComponentesNfe.ACBrNFe.Configuracoes.Geral.ModeloDF := moNFe;
    FComponentesNfe.ACBrNFe.Configuracoes.Geral.VersaoDF := ve310;

    gtDados := '';
    gtDados := confNFe.readString('NFE AMBIENTE',true);
    if ( gtDados = 'PRODUCAO') Then
      FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Ambiente := taproducao
    else
      FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Ambiente := taHomologacao;

    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSchemas := ExtractFilePath(Application.ExeName)+'\Schemas';

    gtDados := '';
    gtDados := confNFe.readString('NFE CAMINHO LOGO MARCA DANFE',true);
    FComponentesNfe.ACBrNFe.DANFE.Logo := gtDados;

    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.SalvarApenasNFeProcessadas := True;

    FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar          := StrToBool(confNFe.readString('NFE SALVAR ARQUIVOS DE ENVIO E RESPOSTA',true));
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Salvar    := FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar;
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.SalvarEvento := FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar;

    if FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar then
    begin
      FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar := confNFe.readString('NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA',true);
      FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar;
    end;

    path_cancelamento                                      := confNFe.readString('NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO',true);
    FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe := confNFe.readString('NFE CAMINHO DOS ARQUIVOS XML',true);
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu := confNFe.readString('NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO',true);

    mesAno := FormatDateTime('mm.yyyy',now);

    //arquivos xml
    if (not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe+mesAno)) then
      ForceDirectories(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe+mesAno);
    FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe := FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe+mesAno+'\';

    FComponentesNfe.ACBrNFe.DANFE.PathPDF := ExtractFilePath( Application.ExeName );

    //cancelamento
    if (not DirectoryExists(path_cancelamento+mesAno)) then
      ForceDirectories(path_cancelamento+mesAno);

    path_cancelamento := path_cancelamento + mesAno+'\';

    //inutilizacao
    if (not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+mesAno)) then
      ForceDirectories(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+mesAno);
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+mesAno+'\';

    //envio e resposta
    if (not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar+mesAno)) then
      ForceDirectories(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar+mesAno);

    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar+mesAno+'\';


    gtDados := '';
    gtDados := confNFe.readString('NFE UF WEBSERVICE',true);
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.UF := gtDados;

    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := StrToBool(confNFe.readString('NFE VISUALIZAR MENSAGEM DE TRANSMISSAO',true));
    //FComponentesNfe.ACBrNFeDANFERave1.RavFile := confNFe.readString('NFE ARQUIVO RAVE',true); celio0712
    FDirBackup := confNFe.readString('NFE DIRETORIO BACKUP',true);
    FComponentesNfe.ACBrNFeDANFERave1.Sistema := confNFe.readString('NFE DESENVOLVEDOR DO SISTEMA',true);
    FComponentesNfe.ACBrNFeDANFERave1.ExpandirLogoMarca := StrToBool(confNFe.readString('EXPANDIR LOGO MARCA',true));

    FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie := confNFe.readString('CERTIFICADO',true);

    FComponentesNfe.ACBrNFeDANFERave2.Sistema            := FComponentesNfe.ACBrNFeDANFERave1.Sistema;
    FComponentesNfe.ACBrNFeDANFERave2.ExpandirLogoMarca  := FComponentesNfe.ACBrNFeDANFERave1.ExpandirLogoMarca;


    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.TimeOut                  := 15000;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.AguardarConsultaRet      := 5000;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.IntervaloTentativas      := 3000;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Tentativas               := 10;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.AjustaAguardaConsultaRet := True;

  finally
    FreeAndNil(confNFe);
  end;

  try

    {verificando os diretorios}
    if FComponentesNfe.ACBrNFe.DANFE.Logo <> '' then
      if not FileExists(FComponentesNfe.ACBrNFe.DANFE.Logo) then
      begin
        strErros.Add('Diret�rio NFE CAMINHO LOGO MARCA DANFE inv�lido');
        Exit;
      end;
    (*
    if FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathDPEC <> '' then
      if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathDPEC) then
      begin
        //MensagemErro('Diret�rio "NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA" inv�lido');
        strerros.add('Diret�rio "NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA" inv�lido');
        Exit;
      end;
      celio0712
    *)
    if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe) then
    begin
      strErros.add('Diret�rio "NFE CAMINHO DOS ARQUIVOS XML" inv�lido: '+FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe);
      Exit;
    end;

    if not DirectoryExists(path_cancelamento) then
    begin
      strErros.add('Diret�rio "CAMINHO DOS ARQUIVOS DE CANCELAMENTO" inv�lido: ' + path_cancelamento);
      Exit;
    end;

    if FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar then
    begin
      //if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathCan) then celio0712
      if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento) then
      begin
        strerros.Add('Diret�rio "NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO" inv�lido: '+FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento);
        Exit;
      end;
    end;

    if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu) then
    begin
      strerros.Add('Diret�rio "NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO" inv�lido: '+FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu);
      Exit;
    end;
    (*
    if not FileExists(FComponentesNfe.ACBrNFeDANFERave1.RavFile) then
    begin
      //MensagemErro('Arquivo RAVE para impress�o do DANFE n�o encontrado');
      strErros.Add('Arquivo RAVE para impress�o do DANFE n�o encontrado');
      Exit;
    end;
    celio0712
    *)

    if not DirectoryExists(FDirBackup) then
    begin
      strerros.Add('Diret�rio "NFE DIRETORIO BACKUP" inv�lido: '+FDirBackup);
      Exit;
    end;

    if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSchemas) then
    begin
      strErros.add('Diret�rio "arquivos schemas n�o encontrado" ' + FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSchemas);
      Exit;
    end;

    Result := True;

  except
  
    on e:Exception do
    begin
      strErros.Add('Verifique os diret�rios XML. '+e.Message);
      Exit;
    end;
    
  end;

end;

function TObjTransmiteNFE.configuraComponentesNFCE:boolean;
var
  confNFe:TobjSqlTxt;
  gtDados:string;
  mesAno:string;
begin
  Result := false;
  self.arquivoSQL := 'sqlNFCE.txt';


  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    strErros.Add('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;

  confNFe := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try
    gtDados := '';
    gtDados := confNFe.readString('ORIENTACAO DANFE',true);

    if (gtDados = 'RETRATO') Then
      FComponentesNfe.ACBrNFe.DANFE.TipoDANFE := tiRetrato
    else
      FComponentesNfe.ACBrNFe.DANFE.TipoDANFE := tiPaisagem;

    FComponentesNfe.ACBrNFe.Configuracoes.Geral.ModeloDF := moNFCe;
    FComponentesNfe.ACBrNFe.Configuracoes.Geral.VersaoDF :=  ve310;

    //FComponentesNfe.ACBrNFe.Configuracoes.Geral.IdToken := '000001';
    //FComponentesNfe.ACBrNFe.Configuracoes.Geral.Token := '0123456789';

    //FComponentesNfe.ACBrNFe.Configuracoes.Geral.IdToken := confNFe.readString('ID NFCE',true); celio0712
    FComponentesNfe.ACBrNFe.Configuracoes.Geral.idCSC := confNFe.readString('ID NFCE',true);
    //FComponentesNfe.ACBrNFe.Configuracoes.Geral.Token := confNFe.readString('TOKEN NFCE',true); celio0712
    FComponentesNfe.ACBrNFe.Configuracoes.Geral.CSC := confNFe.readString('TOKEN NFCE',true);

    FComponentesNfe.ACBrNFe.Configuracoes.Geral.ExibirErroSchema := true;
    FComponentesNfe.ACBrNFe.Configuracoes.Geral.FormaEmissao := teNormal;

    FComponentesNfe.ACBrNFeDANFERave1.TipoDANFE := tiNFCe;

    gtDados := confNFe.readString('AMBIENTE',true);
    if ( gtDados = 'PRODUCAO') Then
      FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Ambiente := taproducao
    else
      FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Ambiente := taHomologacao;

    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSchemas := ExtractFilePath(Application.ExeName)+'\Schemas';

    gtDados := '';
    gtDados := confNFe.readString('CAMINHO LOGO MARCA DANFE',true);
    FComponentesNfe.ACBrNFe.DANFE.Logo := gtDados;

    FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar          := StrToBool(confNFe.readString('SALVAR ARQUIVOS DE ENVIO E RESPOSTA',true));
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Salvar    := FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar;
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.SalvarEvento := FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar;

    if FComponentesNfe.ACBrNFe.Configuracoes.Geral.Salvar then
    begin
      FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar := confNFe.readString('CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA',true);
      FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar;
    end;

    path_cancelamento                                      := confNFe.readString('CAMINHO DOS ARQUIVOS DE CANCELAMENTO',true);
    FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe := confNFe.readString('CAMINHO DOS ARQUIVOS XML',true);
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento := confNFe.readString('CAMINHO DOS ARQUIVOS DE CANCELAMENTO',true);
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu := confNFe.readString('CAMINHO DOS ARQUIVOS DE INUTILIZACAO',true);

    mesAno := FormatDateTime('mm.yyyy',now);

    //arquivos xml
    if (not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe+mesAno)) then
      ForceDirectories(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe+mesAno);
    FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe := FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe+mesAno+'\';

    //cancelamento
    if (not DirectoryExists(path_cancelamento+mesAno)) then
      ForceDirectories(path_cancelamento+mesAno);

    path_cancelamento := path_cancelamento + mesAno+'\';


    //inutilizacao
    if (not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+mesAno)) then
      ForceDirectories(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+mesAno);
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+mesAno+'\';

    //envio e resposta
    if (not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar+mesAno)) then
      ForceDirectories(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar+mesAno);
    FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSalvar+mesAno+'\';



    gtDados := '';
    gtDados := confNFe.readString('UF WEBSERVICE',true);
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.UF := gtDados;

    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := StrToBool(confNFe.readString('VISUALIZAR MENSAGEM DE TRANSMISSAO',true));
    //FComponentesNfe.ACBrNFeDANFERave1.RavFile := confNFe.readString('ARQUIVO RAVE',true); celio0712
    FDirBackup := confNFe.readString('DIRETORIO BACKUP',true);
    FComponentesNfe.ACBrNFeDANFERave1.Sistema := confNFe.readString('DESENVOLVEDOR DO SISTEMA',true);
    FComponentesNfe.ACBrNFeDANFERave1.ExpandirLogoMarca := StrToBool(confNFe.readString('EXPANDIR LOGO MARCA',true));
    FComponentesNfe.ACBrNFe.Configuracoes.Certificados.NumeroSerie := confNFe.readString('CERTIFICADO',true);
    FComponentesNfe.ACBrNFe.Configuracoes.Geral.IncluirQRCodeXMLNFCe := True;

    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.TimeOut                  := 15000;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.AguardarConsultaRet      := 5000;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.IntervaloTentativas      := 3000;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Tentativas               := 10;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.AjustaAguardaConsultaRet := True;    
  finally
    FreeAndNil(confNFe);
  end;


  try

    {verificando os diretorios}
    if FComponentesNfe.ACBrNFe.DANFE.Logo <> '' then
      if not FileExists(FComponentesNfe.ACBrNFe.DANFE.Logo) then
      begin
        strErros.add('Diret�rio CAMINHO LOGO MARCA DANFE inv�lido');
        Exit;
      end;
    (*
    if FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathDPEC <> '' then
      if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathDPEC) then
      begin
        //MensagemErro('Diret�rio "CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA" inv�lido');
        strErros.Add('Diret�rio "CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA" inv�lido');
        Exit;
      end;
    celio0712
    *)

    if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe) then
    begin
      strErros.Add('Diret�rio "CAMINHO DOS ARQUIVOS XML" inv�lido');
      Exit;
    end;

    if not DirectoryExists(path_cancelamento) then
    begin
      strErros.add('Diret�rio "CAMINHO DOS ARQUIVOS DE CANCELAMENTO" inv�lido');
      Exit;
    end;

    if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu) then
    begin
      strErros.Add('Diret�rio "CAMINHO DOS ARQUIVOS DE INUTILIZACAO" inv�lido');
      Exit;
    end;

    (*
    if not FileExists(FComponentesNfe.ACBrNFeDANFERave1.RavFile) then
    begin
      //MensagemErro('Arquivo RAVE para impress�o do DANFE n�o encontrado');
      strErros.Add('Arquivo RAVE para impress�o do DANFE n�o encontrado');
      Exit;
    end;
    celio0712
    *)

    if not DirectoryExists(FDirBackup) then
    begin
      strErros.Add('Diret�rio "DIRETORIO BACKUP" inv�lido');
      Exit;
    end;

    if not DirectoryExists(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathSchemas) then
    begin
      strErros.Add('Diret�rio "arquivos schemas n�o encontrado"');
      Exit;
    end;
    Result := true;
  except
    on e:Exception do
    begin
      strErros.Add('Verifique os diret�rios XML. '+e.Message);
      Exit;
    end;
  end;


end;

function TObjTransmiteNFE.consultaPelaChave(pChave,pUF: string;visualizarMSG:Boolean):integer;
begin

  result := -1;

  preparaFComponente;

  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;
    
  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := visualizarMSG;

  if (pUF <> '') then
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.UF := pUF;

  if (Trim (pChave) = '') then
  begin

    if not(InputQuery('WebServices Consultar', 'Chave da NF-e:', pChave)) then
      exit;

     if (Trim (pChave) = '') then
     begin
       MensagemAviso ('Informe a chave de acesso');
       Exit;
     end;

  end;

  FComponentesNfe.ACBrNFe.WebServices.Consulta.NFeChave := pChave;

  try
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar:=visualizarMSG;
    Screen.Cursor:=crHourGlass;
    FComponentesNfe.ACBrNFe.WebServices.Consulta.Executar ();
    Result := FComponentesNfe.ACBrNFe.WebServices.Consulta.cStat;
  finally
    Screen.Cursor:=crDefault;
  end;

  {cStat = 100 significa que a nfe existe e esta autorizada}
end;

function TObjTransmiteNFE.consultaPelaChaveNFCe(pChave,pUF: string;visualizarMSG:Boolean):integer;
begin

  result := -1;

  preparaFComponente;

  Self.configuraComponentesNFCE;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;

  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := visualizarMSG;

  if (pUF <> '') then
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.UF := pUF;

  if (Trim (pChave) = '') then
  begin

    if not(InputQuery('WebServices Consultar', 'Chave da NF-e:', pChave)) then
      exit;

     if (Trim (pChave) = '') then
     begin
       MensagemAviso ('Informe a chave de acesso');
       Exit;
     end;

  end;

  FComponentesNfe.ACBrNFe.WebServices.Consulta.NFeChave := pChave;

  try
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar:=visualizarMSG;
    Screen.Cursor:=crHourGlass;
    FComponentesNfe.ACBrNFe.WebServices.Consulta.Executar ();
    Result := FComponentesNfe.ACBrNFe.WebServices.Consulta.cStat;
  finally
    Screen.Cursor:=crDefault;
  end;

  {cStat = 100 significa que a nfe existe e esta autorizada}
end;

function TObjTransmiteNFE.consultaServico(visualizar:Boolean=true):string;
begin

  Result := '';
  
  try
    try
      Screen.Cursor := crHourGlass;
      Application.ProcessMessages;

      preparaFComponente;
      Self.configuraComponentes;
      if self.strErros.Count > 0 then
      begin
        ShowMessage(self.strErros.Text);
        Exit;
      end;
      FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := visualizar;

      if not (FComponentesNfe.ACBrNFe.WebServices.StatusServico.Executar) then
        ShowMessage(FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg);

      if (FcomponentesNfe.ACBrNFe.WebServices.StatusServico.cStat <> 107) then
        result := FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg;
    except
      on e:exception do
      begin
        Result := e.Message;
      end;
    end;


  finally
    Screen.Cursor:=crDefault;
  end;

end;

function TObjTransmiteNFE.consultaServicoNFCE(visualizar:Boolean=true):string;
begin

  Result := '';

  try
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;

    preparaFComponente;
    Self.configuraComponentesNFCE;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := visualizar;

    if not (FComponentesNfe.ACBrNFe.WebServices.StatusServico.Executar) then
      ShowMessage(FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg);

    if (FcomponentesNfe.ACBrNFe.WebServices.StatusServico.cStat <> 107) then
      result := FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg;

  finally
    Screen.Cursor:=crDefault;
  end;

end;


function TObjTransmiteNFE.consultaSituacao(pathXML: string): Boolean;
begin

  result := False;

  try

    Screen.Cursor := crHourGlass;

    if not (FileExists (pathXML)) then
    begin
      MensagemAviso ('Arquivo XML n�o encontrado');
      Exit;
    end;

    preparaFComponente;
    Self.configuraComponentes;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

    try
      FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(pathXML);
      FComponentesNfe.ACBrNFe.Consultar;
      FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Consulta.RetWS);
      FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
    except
      on e:Exception do
      begin
        MensagemErro('Erro ao consultar situa��o. '+e.Message);
        Exit;
      end;
    end;

  finally
    Screen.Cursor := crDefault;
  end;

  result := True;

end;

function TObjTransmiteNFE.consultaSituacaoNFCe(pathXML: string): Boolean;
begin

  result := False;

  try

    Screen.Cursor := crHourGlass;

    if not (FileExists (pathXML)) then
    begin
      MensagemAviso ('Arquivo XML n�o encontrado');
      Exit;
    end;

    preparaFComponente;
    Self.configuraComponentesNFCE;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

    try
      FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(pathXML);
      FComponentesNfe.ACBrNFe.Consultar;
      FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Consulta.RetWS);
      FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
    except
      on e:Exception do
      begin
        MensagemErro('Erro ao consultar situa��o. '+e.Message);
        Exit;
      end;
    end;

  finally
    Screen.Cursor := crDefault;
  end;

  result := True;

end;

procedure TObjTransmiteNFE.consultaXML;
begin

  preparaFComponente;
  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;

  FComponentesNfe.OpenDialog1.Title := 'Selecione a NFE';
  FComponentesNfe.OpenDialog1.DefaultExt := '*-nfe.XML';
  FComponentesNfe.OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';
  FComponentesNfe.OpenDialog1.InitialDir := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe;
  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;
  
  if FComponentesNfe.OpenDialog1.Execute then
  begin
    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(FComponentesNfe.OpenDialog1.FileName);
    FComponentesNfe.ACBrNFe.Consultar;
    FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Consulta.RetWS);
    FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
  end;

end;

procedure TObjTransmiteNFE.consultaXMLNFCE;
begin

  preparaFComponente;
  Self.configuraComponentesNFCE;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;

  FComponentesNfe.OpenDialog1.Title := 'Selecione a NFC-E';
  FComponentesNfe.OpenDialog1.DefaultExt := '*-nfe.XML';
  FComponentesNfe.OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';
  FComponentesNfe.OpenDialog1.InitialDir := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe;
  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

  if FComponentesNfe.OpenDialog1.Execute then
  begin
    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(FComponentesNfe.OpenDialog1.FileName);
    FComponentesNfe.ACBrNFe.Consultar;
    FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Consulta.RetWS);
    FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
  end;

end;

constructor TObjTransmiteNFE.Create(AOwner: TComponent;ibdatabase: TIBDatabase);
begin
  inherited create(Aowner);
  self.strErros := TStringList.Create;
  self.SetdataBase(ibdatabase);

  if not Assigned(FCCe) then
  begin
    FCCe := TComCartaCorrecao.Create(self);
    FCCe.Registro := 'Carta de corre��o eletr�nica';
    //FCCe.carta := FComponentesNfe.ACBrNFe.CartaCorrecao;
  end;

  self.consumidorFinal := True;
end;

destructor TObjTransmiteNFE.destroy;
begin
  FreeAndNil(FIde);
  FreeAndNil(FInfAdic);
  FreeAndNil(FComplementar);
  FreeAndNil(FDevolucao);
  FreeAndNil(FCupomRef);
  FreeAndNil(FCobrFat);
  FreeAndNil(FCobrDup);
  FreeAndNil(FEmit);
  FreeAndNil(FEnderEmit);
  FreeAndNil(FDestC);
  FreeAndNil(FDestF);
  FreeAndNil(FEnderDestC);
  FreeAndNil(FEnderDestF);
  FreeAndNil(FTransp);
  FreeAndNil(FTransporta);
  FreeAndNil(FVeicTransp);
  FreeAndNil(FVol);
  FreeAndNil(FProdutos);
  FreeAndNil(FCCe);
  FreeAndNil(strErros);
  inherited;
end;

function TObjTransmiteNFE.enviarEmail(CaminhoXML:string;const sSmtpHost, sSmtpPort,
  sSmtpUser, sSmtpPasswd, sFrom, sTo, sAssunto: String;
  sMensagem: TStrings; SSL, EnviaPDF: Boolean; sCC, Anexos: TStrings;
  PedeConfirma, AguardarEnvio: Boolean; NomeRemetente: String;
  TLS: Boolean): Boolean;
begin

  result := false;

  try

    preparaFComponente;
    Self.configuraComponentes;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

    if not (FileExists(caminhoXML)) then
    begin
      MensagemErro('Arquivo xml n�o encontrado. Arquivo: '+caminhoXML);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(caminhoXML);

    try
      Screen.Cursor := crHourGlass;
      FComponentesNfe.ACBrNFe.MAIL.Host     := sSmtpHost;
      FComponentesNfe.ACBrNFe.MAIL.Port     := sSmtpPort;
      FComponentesNfe.ACBrNFe.MAIL.Username := sSmtpUser;
      FComponentesNfe.ACBrNFe.MAIL.Password := sSmtpPasswd;
      FComponentesNfe.ACBrNFe.MAIL.From     := sFrom;
      FComponentesNfe.ACBrNFe.MAIL.SetSSL := SSL;
      FComponentesNfe.ACBrNFe.MAIL.SetTLS := TLS;

      (*
      FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].EnviarEmail (sSmtpHost
                                                  ,sSmtpPort
                                                  ,sSmtpUser
                                                  ,sSmtpPasswd
                                                  ,sFrom
                                                  ,sTo
                                                  ,sAssunto,sMensagem
                                                  ,SSL // SSL - Conex�o Segura
                                                  ,True //Enviar PDF junto
                                                  ,nil //Lista com emails que ser�o enviado c�pias - TStrings
                                                  ,nil // Lista de anexos - TStrings
                                                  ,true  //Pede confirma��o de leitura do email
                                                  ,true  //Aguarda Envio do Email(n�o usa thread)
                                                  ,NomeRemetente // Nome do Rementente
                                                  ,TLS); // Auto TLS
                                                  celio0712
                                                  *)
      FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].EnviarEmail (sTo,sAssunto,sMensagem,True,nil,nil);
    finally
      Screen.Cursor := crdefault;
    end;

  except
    on e:Exception do
    begin
      ShowMessage(e.message);
      Exit;
    end;
  end;

  result := True;

end;

procedure TObjTransmiteNFE.imprimeDanfe(pathXML:string);
var
  wnProt: TLeitor;
begin

  preparaFComponente;

  if pathXML = '' then
  begin

    Try
      if (FComponentesNfe.ACBrNFe.DANFE <> nil) then
      begin

        if FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Confirmada then
        begin
          FComponentesNfe.ACBrNFe.DANFE.ProtocoloNFe :=  FComponentesNfe.ACBrNFe.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
          FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Imprimir;
        end;

      end;

    Except
      MensagemErro('Erro na tentativa de Imprimir, tente imprimir direto do arquivo');
    End;

  end
  else
  begin

    if not FileExists(pathXML) then
    begin
      MensagemAviso('Arquivo XML n�o encontrado para impress�o');
      Exit;
    end;

    try

      Self.configuraComponentes;
      if self.strErros.Count > 0 then
      begin
        ShowMessage(self.strErros.Text);
        Exit;
      end;
        
      FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

      FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(pathXML);

      wnProt:=TLeitor.Create;
      wnProt.CarregarArquivo(pathXML);
      wnProt.Grupo:=wnProt.Arquivo;
      FcomponentesNfe.ACBrNFe.DANFE.ProtocoloNFe:=wnProt.rCampo(tcStr,'nProt');
      FcomponentesNfe.ACBrNFe.NotasFiscais.Imprimir;

    finally
      wnProt.Free;
    end;

  end;


end;

procedure TObjTransmiteNFE.imprimeDanfeNFCe(pathXML:string);
var
  wnProt: TLeitor;
begin

  preparaFComponente;

  if pathXML = '' then
  begin

    Try
      if (FComponentesNfe.ACBrNFe.DANFE <> nil) then
      begin


        FComponentesNfe.ACBrNFe.DANFE := FComponentesNfe.ACBrNFeDANFCeFortes1;

        try
          if FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Confirmada then
          begin
            FComponentesNfe.ACBrNFe.DANFE.ProtocoloNFe :=  FComponentesNfe.ACBrNFe.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
            FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].Imprimir;
          end;
        finally
          FComponentesNfe.ACBrNFe.DANFE := FComponentesNfe.ACBrNFeDANFERave1;
        end;

      end;

    Except
      MensagemErro('Erro na tentativa de Imprimir, tente imprimir direto do arquivo');
    End;

  end
  else
  begin

    if not FileExists(pathXML) then
    begin
      MensagemAviso('Arquivo XML n�o encontrado para impress�o');
      Exit;
    end;

    try

      Self.configuraComponentesNFCE;
      if self.strErros.Count > 0 then
      begin
        ShowMessage(self.strErros.Text);
        Exit;
      end;

      FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

      FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(pathXML);

      wnProt:=TLeitor.Create;
      wnProt.CarregarArquivo(pathXML);
      wnProt.Grupo:=wnProt.Arquivo;
      FcomponentesNfe.ACBrNFe.DANFE.ProtocoloNFe:=wnProt.rCampo(tcStr,'nProt');
      FComponentesNfe.ACBrNFe.DANFE := FComponentesNfe.ACBrNFeDANFCeFortes1;
      try
        FcomponentesNfe.ACBrNFe.NotasFiscais.Imprimir;
      finally
        FComponentesNfe.ACBrNFe.DANFE := FComponentesNfe.ACBrNFeDANFERave1;
      end;

    finally
      wnProt.Free;
    end;

  end;


end;

procedure TObjTransmiteNFE.iniciaGeracao;
begin
  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  FComponentesNfe.ACBrNFe.NotasFiscais.Add;
end;

procedure TObjTransmiteNFE.inicializa;
var
  i:Integer;
begin

  //self.strErros := TStringList.Create;
  Self.denegada := False;
  self.iniciaGeracao;

  FIde := TComIDE.Create(self);
  FIde.Registro := 'IDE';                                                                   
                                                                                            
  FComplementar := TComComplementar.Create(self);                                           
  FComplementar.Registro := 'NF-e Complementar';                                            
                                                                                            
  FDevolucao := TComDevolucao.Create(self);                                                 
  FDevolucao.Registro := 'NF-e devolu��o';                                                  
                                                                                            
  FCupomRef := TComCupomRef.Create(self);                                                   
  FCupomRef.Registro := 'NF-e referenciado por cupom fiscal';                               
                                                                                            
  FInfAdic := TComInfAdic.Create(self);                                                     
  FInfAdic.Registro := 'Informa��es adicionais';                                            
                                                                                            
  FEmit := TComEmit.Create(self);                                                           
  FEmit.Registro := 'Emitente';                                                             
                                                                                            
  FCobrFat := TComFat.Create(self);                                                         
  FCobrFat.Registro := 'Fatura';                                                            

  FCobrDup := TComDup.Create(self);
  FCobrDup.Registro := 'Duplicatas';

  FEnderEmit := TComEnderEmit.Create(self);
  FEnderEmit.Registro := 'Endere�o emitente';

  FDestC := TComDestC.Create(self);
  FDestC.Registro := 'Destinat�rio cliente';

  FDestF := TComDestF.Create(self);
  FDestF.Registro := 'Destinat�rio fornecedor';

  FEnderDestC := TComEnderDestC.Create(self);
  FEnderDestC.Registro := 'Endere�o destinat�rio cliente';

  FEnderDestF := TComEnderDestF.Create(self);
  FEnderDestF.Registro := 'Endere�o destinat�rio fornecedor';

  FTransp := TComTransp.Create(self);
  FTransp.Registro := 'Transporte modalidade de frete';

  FTransporta := TComTransporta.Create(self);
  FTransporta.Registro := 'Transportadora';

  FVeicTransp := TComveicTransp.Create(self);
  FVeicTransp.Registro := 'Veiculo transportadora';

  FVol := TComVol.Create(self);
  FVol.Registro := 'Volumes transportados';

  FProdutos := TComProdutos.Create(self);
  FProdutos.Registro := 'Produtos';

  if not Assigned(FCCe) then
  begin
    FCCe := TComCartaCorrecao.Create(self);
    FCCe.carta := FComponentesNfe.ACBrNFe.CartaCorrecao;
    FCCe.Registro := 'Carta de corre��o eltr�nica';
  end;

  for i :=0  to self.ComponentCount-1  do
  begin

    if self.Components[i] is TFonteDados then
    begin
      TFonteDados(self.Components[i]).FDatabase  := self.FdataBase;
      TFonteDados(self.Components[i]).strErros   := self.strErros;
      TFonteDados(self.Components[i]).denegada   := self.denegada;
      TFonteDados(self.Components[i]).pCodigoNF  := self.pCodigoNF;
      TFonteDados(self.Components[i]).tipoNF     := self.TipoNF;
      TFonteDados(self.Components[i]).numeroNFE  := self.numeroNFE;
      TFonteDados(self.Components[i]).chaveRef   := self.chaveRef;
      TFonteDados(self.Components[i]).CRT        := self.CRT;
    end;


    if Self.Components[i] is TComIDE then
      TComIDE(self.Components[i]).IDE := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Ide;

    if self.Components[i] is TComCobranca then
      TComCobranca(self.Components[i]).Cobr := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Cobr;

    if self.Components[i] is TComDevolucao then
    begin
      TComDevolucao(self.Components[i]).modeloRef  := self.modeloRef;
      TComDevolucao(self.Components[i]).CNPJ_ref   := self.CNPJ_ref;
      TComDevolucao(self.Components[i]).cUF_ref    := self.cUF_ref;
      TComDevolucao(self.Components[i]).AAMM_ref   := self.AAMM_ref;
      TComDevolucao(self.Components[i]).modelo_ref := Self.modelo_ref;
      TComDevolucao(self.Components[i]).serie_ref  := Self.serie_ref;
      TComDevolucao(self.Components[i]).nNF_ref    := self.nNF_ref;
    end;

    if self.Components[i] is TComCupomRef then
    begin
      TComCupomRef(self.Components[i]).modelo := self.modeloecf;
      TComCupomRef(self.Components[i]).nCOO   := self.nCOO;
      TComCupomRef(self.Components[i]).nECF   := self.nECF;
      TComCupomRef(self.Components[i]).strRefCupom := self.strRefCupom;
    end;

    if self.Components[i] is TComInfAdic then
      TComInfAdic(self.Components[i]).INFADIC := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.InfAdic;

    if self.Components[i] is TComEmit then
      TComEmit(self.Components[i]).EMIT := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit;

    if self.Components[i] is TComEnderEmit then
      TComEnderEmit(self.Components[i]).ENDEREMIT := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Emit.EnderEmit;

    if self.Components[i] is TComDestC then
      TComDestC(self.Components[i]).Dest := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest;

    if self.Components[i] is TComDestF then
      TComDestF(self.Components[i]).Dest := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest;

    if self.Components[i] is TComEnderDestC then
      TComEnderDestC(self.Components[i]).ENDERDEST := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest;

    if self.Components[i] is TComEnderDestF then
      TComEnderDestF(self.Components[i]).ENDERDEST := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Dest.EnderDest;

    if self.Components[i] is TComTransp then
      TComTransp(self.Components[i]).Transp := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp;

    if self.Components[i] is TComTransporta then
      TComTransporta(self.Components[i]).Transporta := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Transporta;

    if self.Components[i] is TComveicTransp then
      TComveicTransp(self.Components[i]).veicTransp := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.veicTransp;

    if Self.Components[i] is TComVol then
      TComVol(self.Components[i]).vol := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Transp.Vol;

    if self.Components[i] is TComDet then
      TComDet(self.Components[i]).Det := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Det;

    if Self.Components[i] is TComProdutos then
    begin
      TComProdutos(self.Components[i]).inicializa;
      TComProdutos(self.Components[i]).total := FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.Total;
    end;

  end;

  FFormasPagamento := TComFormaPag.Create(self);
  FFormasPagamento.Registro := 'Formas de pagamento';

end;

function TObjTransmiteNFE.inutilizaFaixa(var pprotocolo:string;cnpj,Justificativa,ano,modelo,serie,numeroInicial,numeroFinal:string): Boolean;
begin

  result := False;

  cnpj := RetornaSoNumeros(cnpj);

  if cnpj = '' then
  begin
    MensagemAviso('CNPJ n�o informado');
    Exit;
  end;

  if Justificativa = '' then
  begin
    MensagemAviso('Justificativa n�o informada');
    Exit;
  end;

  if ano = '' then
  begin
    MensagemAviso('Ano n�o informado');
    Exit;
  end;

  Try

    preparaFComponente;
    Self.configuraComponentes;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;
      
    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

    try
      Screen.Cursor := crHourGlass;
      FComponentesNfe.ACBrNFe.WebServices.Inutiliza(cnpj,Justificativa, StrToInt(Ano), StrToInt(Modelo), StrToInt(Serie), StrToInt(NumeroInicial), StrToInt(NumeroFinal));
    finally
      Screen.Cursor := crDefault;
    end;

    pprotocolo := FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.Protocolo;
    FcomponentesNfe.MemoResp.Text :=  UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.RetWS);
    FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);

  Except
    on e:exception do
    Begin
      Mensagemerro('Erro na tentativa de inutilizar a sequ�ncia '+#13+E.message);
      exit;
    End;
  End;

  {FComponentesNfe.MemoResp.Lines.clear;
  FComponentesNfe.MemoResp.Lines.text:=FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.DadosMsg;
  FComponentesNfe.MemoResp.Lines.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+numeroInicial+' - '+numeroFinal+' ARQUIVOINU_E.XML');}

  FComponentesNfe.MemoResp.Lines.clear;
  FComponentesNfe.MemoResp.Lines.text:=FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.RetWS;

  try
    FComponentesNfe.MemoResp.Lines.SaveToFile (FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+numeroInicial+' - '+numeroFinal+' ARQUIVOINU_E.XML');
  except
    on e:Exception do
    begin
      MensagemErro ('N�o foi possivel salvar arquivo de inutiliza��o. '+e.Message);
      Exit;
    end;
  end;
  
  result := True;

end;

function TObjTransmiteNFE.inutilizaFaixaNFCe(var pprotocolo:string;cnpj,Justificativa,ano,modelo,serie,numeroInicial,numeroFinal:string): Boolean;
begin

  result := False;

  cnpj := RetornaSoNumeros(cnpj);

  if cnpj = '' then
  begin
    MensagemAviso('CNPJ n�o informado');
    Exit;
  end;

  if Justificativa = '' then
  begin
    MensagemAviso('Justificativa n�o informada');
    Exit;
  end;

  if ano = '' then
  begin
    MensagemAviso('Ano n�o informado');
    Exit;
  end;

  Try

    preparaFComponente;
    Self.configuraComponentesNFCE;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;

    try
      Screen.Cursor := crHourGlass;
      FComponentesNfe.ACBrNFe.WebServices.Inutiliza(cnpj,Justificativa, StrToInt(Ano), StrToInt(Modelo), StrToInt(Serie), StrToInt(NumeroInicial), StrToInt(NumeroFinal));
    finally
      Screen.Cursor := crDefault;
    end;

    pprotocolo := FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.Protocolo;
    FcomponentesNfe.MemoResp.Text :=  UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.RetWS);
    FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);

  Except
    on e:exception do
    Begin
      Mensagemerro('Erro na tentativa de inutilizar a sequ�ncia '+#13+E.message);
      exit;
    End;
  End;

  {FComponentesNfe.MemoResp.Lines.clear;
  FComponentesNfe.MemoResp.Lines.text:=FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.DadosMsg;
  FComponentesNfe.MemoResp.Lines.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+numeroInicial+' - '+numeroFinal+' ARQUIVOINU_E.XML');}

  FComponentesNfe.MemoResp.Lines.clear;
  FComponentesNfe.MemoResp.Lines.text:=FcomponentesNfe.ACBrNFe.WebServices.Inutilizacao.RetWS;

  try
    FComponentesNfe.MemoResp.Lines.SaveToFile (FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathInu+numeroInicial+' - '+numeroFinal+' ARQUIVOINU_E.XML');
  except
    on e:Exception do
    begin
      MensagemErro ('N�o foi possivel salvar arquivo de inutiliza��o. '+e.Message);
      Exit;
    end;
  end;
  
  result := True;

end;


procedure TObjTransmiteNFE.limpaCampos;
begin

  FpCodigoNF := 0;
  FTipoNF    := '';
  FfLp := False;
  FnumeroNFE := '';
  FnfeComplementar := False;
  FcartaCorrecao := False;
  Fcontingencia := false;
  FnfeDevolucao    := False;
  Fref55           := False;
  Fref01           := False;
  FmodeloRef       := 0;
  FchaveRef        := '';
  FCNPJ_ref        := '';
  FcUF_ref         := '';
  Fserie_ref       := '';
  FnNF_ref         := '';
  FAAMM_ref        := '';
  Fmodelo_ref      := '';
  FnCOO            := '';
  FstrRefCupom          := '';
  FnECF            := '';
  Fmodeloecf       := '';

end;

procedure TObjTransmiteNFE.preencheComponente;
begin

  if strErros.Count > 0 then Exit;

  if nfeComplementar then
    FComplementar.preenche;

  if nfeDevolucao then
    FDevolucao.preenche;

  FCupomRef.preenche;

  FIde.preenche;
  FInfAdic.infComplementares := FCupomRef.infComplementares + FComplementar.infComplementares + FDevolucao.infComplementares;
  FInfAdic.preenche;
  FCobrFat.preenche;
  FCobrDup.preenche;
  FEmit.preenche;
  FEnderEmit.preenche;
  FDestC.preenche;
  FEnderDestC.preenche;
  FDestF.preenche;
  FEnderDestF.preenche;
  FTransp.preenche;
  FTransporta.preenche;
  FVeicTransp.preenche;
  FVol.preenche;
  FProdutos.preenche;

  Application.ProcessMessages;
  
end;

procedure TObjTransmiteNFE.preencheSql;
var
  sqlNFE:TobjSqlTxt;
begin

  self.inicializa;

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    //ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    strerros.add('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;
  
  sqlNFE := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);


  try
    FIde.FSQL              := sqlNFE.readString('IDE');
    FInfAdic.FSQL          := sqlNFE.readString('INFADIC');
    FEmit.FSQL             := sqlNFE.readString('EMITENTE');
    FEnderEmit.FSQL        := sqlNFE.readString('ENDERECO EMITENTE');
    FDestC.FSQL            := sqlNFE.readString('DESTINATARIO CLIENTE');
    FDestF.FSQL            := sqlNFE.readString('DESTINATARIO FORNECEDOR');
    FEnderDestC.FSQL       := sqlNFE.readString('ENDERECO DESTINATARIO CLIENTE');
    FEnderDestF.FSQL       := sqlNFE.readString('ENDERECO DESTINATARIO FORNECEDOR');
    FTransp.FSQL           := sqlNFE.readString('TRANSPORTE');
    FTransporta.FSQL       := sqlNFE.readString('TRANSPORTA');
    FVeicTransp.FSQL       := sqlNFE.readString('VEICULO TRANSPORTADORA');
    FVol.FSQL              := sqlNFE.readString('VOLUMES TRANSPORTADOS');
    FProdutos.FSQL         := sqlNFE.readString('PRODUTOS');
    FProdutos.FICMS.FSQL   := sqlNFE.readString('ICMS NORMAL');
    FProdutos.FICMSST.FSQL := sqlNFE.readString('ICMS ST');
    FProdutos.FPIS.FSQL    := sqlNFE.readString('PIS');
    FProdutos.FCOFINS.FSQL := sqlNFE.readString('COFINS');
    FProdutos.FIPI.FSQL    := sqlNFE.readString('IPI');
    FCobrFat.FSQL          := sqlNFE.readString('COBRANCA FATURA');
    FCobrDup.FSQL          := sqlNFE.readString('COBRANCA DUPLICATA');
  finally
    FreeAndNil(sqlNFE);
  end;

end;

procedure TObjTransmiteNFE.preencheSqlNFCE;
var
  sqlNFE:TobjSqlTxt;
begin

  self.inicializa;

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    //ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    strErros.Add('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;
  
  sqlNFE := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);


  try
    FIde.FSQL              := sqlNFE.readString('IDE');
    FInfAdic.FSQL          := sqlNFE.readString('INFADIC');
    FEmit.FSQL             := sqlNFE.readString('EMITENTE');
    FEnderEmit.FSQL        := sqlNFE.readString('ENDERECO EMITENTE');
    FDestC.FSQL            := sqlNFE.readString('DESTINATARIO CLIENTE');
    FDestF.FSQL            := sqlNFE.readString('DESTINATARIO FORNECEDOR');
    FEnderDestC.FSQL       := sqlNFE.readString('ENDERECO DESTINATARIO CLIENTE');
    FEnderDestF.FSQL       := sqlNFE.readString('ENDERECO DESTINATARIO FORNECEDOR');
    //FTransp.FSQL           := sqlNFE.readString('TRANSPORTE');
    //FTransporta.FSQL       := sqlNFE.readString('TRANSPORTA');
    //FVeicTransp.FSQL       := sqlNFE.readString('VEICULO TRANSPORTADORA');
    //FVol.FSQL              := sqlNFE.readString('VOLUMES TRANSPORTADOS');
    FProdutos.FSQL         := sqlNFE.readString('PRODUTOS');
    FProdutos.FICMS.FSQL   := sqlNFE.readString('ICMS NORMAL');
    FProdutos.FICMSST.FSQL := sqlNFE.readString('ICMS ST');
    FProdutos.FPIS.FSQL    := sqlNFE.readString('PIS');
    FProdutos.FCOFINS.FSQL := sqlNFE.readString('COFINS');
    FProdutos.FIPI.FSQL    := sqlNFE.readString('IPI');
    FCobrFat.FSQL          := sqlNFE.readString('COBRANCA FATURA');
    FCobrDup.FSQL          := sqlNFE.readString('COBRANCA DUPLICATA');
    FFormasPagamento.FSQL  := sqlNFE.readString('FORMAS PAGAMENTO');

  finally
    FreeAndNil(sqlNFE);
  end;

end;

function TObjTransmiteNFE.retornaArquivoXML: string;
begin
  result := FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe + StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase])+'-NFe.xml';
end;

function TObjTransmiteNFE.retornaChaveAcesso: string;
begin
  result := StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase]);
end;

function TObjTransmiteNFE.retornaRecibo: string;
begin
  result := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo; 
end;

procedure TObjTransmiteNFE.SetAAMM_ref(const Value: string);
begin
  FAAMM_ref := Value;
end;

procedure TObjTransmiteNFE.SetcartaCorrecao(const Value: Boolean);
begin
  FcartaCorrecao := Value;
end;

procedure TObjTransmiteNFE.SetchaveRef(const Value: string);
begin
  FchaveRef := Value;
end;

procedure TObjTransmiteNFE.SetCNPJ_ref(const Value: string);
begin
  FCNPJ_ref := Value;
end;

procedure TObjTransmiteNFE.Setcontingencia(const Value: Boolean);
begin
  Fcontingencia := Value;
end;

procedure TObjTransmiteNFE.SetcUF_ref(const Value: string);
begin
  FcUF_ref := Value;
end;

procedure TObjTransmiteNFE.SetdataBase(const Value: TIBDatabase);
begin
  FdataBase := Value;
end;

procedure TObjTransmiteNFE.SetfLp(const Value: Boolean);
begin
  FfLp := Value;
end;

procedure TObjTransmiteNFE.Setmodeloecf(const Value: string);
begin
  Fmodeloecf := Value;
end;

procedure TObjTransmiteNFE.SetmodeloRef(const Value: Integer);
begin
  FmodeloRef := Value;
end;

procedure TObjTransmiteNFE.Setmodelo_ref(const Value: string);
begin
  Fmodelo_ref := Value;
end;

procedure TObjTransmiteNFE.SetnCOO(const Value: string);
begin
  FnCOO := Value;
end;

procedure TObjTransmiteNFE.SetnECF(const Value: string);
begin
  FnECF := Value;
end;

procedure TObjTransmiteNFE.SetnfeComplementar(const Value: Boolean);
begin
  FnfeComplementar := Value;
end;

procedure TObjTransmiteNFE.SetnfeDevolucao(const Value: Boolean);
begin
  FnfeDevolucao := Value;
end;

procedure TObjTransmiteNFE.SetnNF_ref(const Value: string);
begin
  FnNF_ref := Value;
end;

procedure TObjTransmiteNFE.SetnumeroNFE(const Value: string);
begin
  FnumeroNFE := Value;
end;

procedure TObjTransmiteNFE.SetpCodigoNF(const Value: Integer);
begin
  FpCodigoNF := Value;
end;

procedure TObjTransmiteNFE.Setref01(const Value: Boolean);
begin
  Fref01 := Value;
end;

procedure TObjTransmiteNFE.Setref55(const Value: Boolean);
begin
  Fref55 := Value;
end;


procedure TObjTransmiteNFE.Setserie_ref(const Value: string);
begin
  Fserie_ref := Value;
end;

procedure TObjTransmiteNFE.SetTipoNF(const Value: string);
begin
  FTipoNF := Value;
end;

function TObjTransmiteNFE.teveErros: Boolean;
begin
  result := (strErros.Count > 0);
end;

function TObjTransmiteNFE.getCampoTable(const sectionName:string): TIBQuery;
var
  objsql:TobjSqlTxt;
begin

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;
  
  objsql := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try

    if FqueryTemp = nil then
      FqueryTemp := TIBQuery.Create(Self);

    FqueryTemp.Active := False;

    FqueryTemp.DisableControls;
    FqueryTemp.Database := FDatabase;
    FqueryTemp.SQL.Text := objsql.readString(sectionName);
    FqueryTemp.Active := True;
    result := FqueryTemp;

  finally
    FreeAndNil(objsql);
  end;

end;

{ TODO : transmite }
function TObjTransmiteNFE.Transmite: Boolean;
begin

  result := false;

  preparaFComponente;

  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;

  self.preencheSql;

  if self.cartaCorrecao then
  begin
    FCCe.carta := FComponentesNfe.ACBrNFe.CartaCorrecao;
    self.FCCe.enviarCarta;
  end
  else
  begin

    self.preencheComponenteEvento;
    self.transmiteSefazEvento;

  end;

  self.limpaCampos;
  result := self.teveErros;
  Result := True;
  
end;

function TObjTransmiteNFE.TransmiteNFCE: Boolean;
begin
  preparaFComponente;
  Self.configuraComponentesNFCE;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;
    
  self.preencheSqlNFCE;

  self.preencheComponenteEventoNFCE;
  self.transmiteSefazEventoNFCE;

  self.limpaCampos;
  result := self.teveErros;
  
end;



{ TInfAdic }

procedure TComInfAdic.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  if self.infComplementares <> '' then
    INFADIC.infCpl := FQuery.Fields[0].Text+#13#10+self.infComplementares
  else
    INFADIC.infCpl := FQuery.Fields[0].Text;

end;

procedure TComInfAdic.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Informa��es complementares';
  FQuery.Fields[0].OnGetText := GetFieldTrim;
  FQuery.Fields[0].Required := False;
end;

procedure TComInfAdic.setParametros;
begin
  inherited;
end;

function TComInfAdic.validaRegistros: Boolean;
begin

end;

{ TODO : transmite sefaz }
procedure TObjTransmiteNFE.transmiteSefaz;
var
  pRecibo :string;
begin

  if teveErros then
    Exit;

  try

    try

      try
        //FComponentesNfe.ACBrNFe.NotasFiscais.Valida; celio0712
        FComponentesNfe.ACBrNFe.NotasFiscais.Validar;
      except
        on e:exception do
        Begin
          strErros.Add('Erro na tentativa de Validar '+#13+E.message);
          exit;
        End;
      end;

      try
        FComponentesNfe.ACBrNFe.NotasFiscais.Assinar;
      except
        on e:exception do
        Begin
          strErros.Add('Erro na tentativa de Assinar '+#13+E.message);
          exit;
        End;
      end;

      if not contingencia then
      begin

        try
          Screen.Cursor := crHourGlass;
          Application.ProcessMessages;

          frmStatus.Show;
          frmStatus.setStatus('Consultando servi�o...');
          if not (FComponentesNfe.ACBrNFe.WebServices.StatusServico.Executar) then
            strErros.Add(FComponentesNfe.ACBrNFe.WebServices.StatusServico.Msg);

        finally
          Screen.Cursor:=crDefault;
        end;

        FComponentesNfe.ACBrNFe.WebServices.Enviar.Lote := '0';

        try

          Screen.Cursor := crHourGlass;
          Application.ProcessMessages;

          frmStatus.setStatus('Transmitindo NF-e...');
          if not (FComponentesNfe.ACBrNFe.WebServices.Enviar.Executar()) then
            strErros.Add(FComponentesNfe.ACBrNFe.WebServices.Enviar.Msg);

        finally
          Screen.Cursor := crDefault;
        end;

        //precibo := FACBRNfe.WebServices.Enviar.Recibo;
        //FACBRNfe.WebServices.Retorno.Recibo := precibo;

        FComponentesNfe.ACBrNFe.WebServices.Retorno.Recibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;

        try

          if (fLp = False) then
          begin

            FComponentesNfe.ACBrNFe.WebServices.Retorno.Recibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;

            try
              Screen.Cursor:=crHourGlass;
              try
                if not (FComponentesNfe.ACBrNFe.WebServices.Retorno.Executar) then
                begin
                  strerros.Add(FComponentesNfe.ACBrNFe.WebServices.Retorno.Msg);
                  Exit;
                end;
              except
                strErros.Add(FComponentesNfe.ACBrNFe.WebServices.Retorno.Msg);
                exit;
              end;

            finally
              Screen.Cursor:=crDefault;
            end;

          end;

        except

          on e:Exception do
          begin

            {O Stat 105 � lote em processamento algum mano loko leu no forum que teve casos de lote em processamento com status 0 por isso verifiquei a frase lote em processamento}
            if ( (FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat = 105) or (fLp) or (pos ('LOTE EM PROCESSAMENTO',uppercase (e.message))>0)) then
              strErros.Add('O Lote foi enviado mas continua em processamento, tente novamente mais tarde apenas para confirmar esta NF')
            else
            begin
              strErros.add(e.Message);
              exit;
            end;

          end;

        end;

        frmStatus.setStatus('NF-e transmitida com sucesso!');
        Sleep(1000);
        frmStatus.Close;
        imprimeDanfe;

        //if not (FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe)) then celio0712
        if not (FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe)) then
          strErros.Add('N�o foi possivel salvar arquivo XML');

      end
      else
      begin
        FcomponentesNfe.MemoResp.Text:= UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);
        FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
        //FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe); celio0712
        FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.arquivos.PathNFe);
      end;

    except
      on e:Exception do
      begin
        strErros.Add(e.Message);
        frmStatus.Close;
        exit;
      end;
    end;

  finally
    frmStatus.Close;
  end;

end;

{ TODO : transmiteSefazEvento }
procedure TObjTransmiteNFE.transmiteSefazEvento;
var
  vNumLote:Integer;
  msgerro,msgDenegada,temp:string;
  i,tempo:integer;
  cStat:integer;
begin

  if teveErros then
    Exit;

  vNumLote := StrToInt(OnlyNumber('1'));

  try

    Screen.Cursor := crHourGlass;
    msgerro := '';
    msgDenegada := '';
    self.denegada := False;

    {frmStatus.lblStatus.Caption := 'Consultando servi�o...';
    frmStatus.Show;
    Application.ProcessMessages;
    msgerro := consultaServico(false);
    if (msgerro <> '') then
    begin
      frmStatus.Close;
      strErros.Add(msgerro);
      Exit;
    end;}

    frmStatus.Show;
    frmStatus.lblStatus.Caption := 'Transmitindo NF-e...';
    Application.ProcessMessages;

    try
      FComponentesNfe.ACBrNFe.Enviar(vNumLote,self.FDirBackup,false,false);
     // FComponentesNfe.ACBrNFe.Enviar(vNumLote,false,false);    a linha de cima tava comentada - fabio jr
    except
      on e:Exception do
      begin
        msgerro := e.Message;
      end;
    end;

    frmStatus.Close;

    cStat := FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat;

    {cStat=105 Lote em processamento o aplicativo do contribuinte dever� fazer umanova consulta;}
    if ( cStat = 105 ) or (Pos('lote em processamento',LowerCase(msgerro)) > 0) then
    begin

      try

        for i:= 1 to 5 do
        begin

          try
            FComponentesNfe.ACBrNFe.Consultar;
          except
            on e:Exception do
            begin
              temp := e.Message;
            end;
          end;

          cStat := FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat;

          {100-sucesso 205 e 302 denegada-denegado uso ou execao}
          if (cStat = 100 ) or (cStat = 205) or (cStat = 302) or (temp <> '') then
          begin
            Break;
          end
          else
          begin

            frmStatus.Show;
            for tempo := i * 5 downto 1 do
            begin
              frmStatus.lblStatus.Caption := 'Consultando Status da NF-e tentativa '+ IntToStr( i ) +'/5 aguarde...' + IntToStr( tempo ) + ' seg' ;
              Application.ProcessMessages;
              Sleep(1000);
            end;
            
          end;

        end;

      finally
        frmStatus.Close;
      end;

      //tentou 5 vezes e n�o deu certo, marca nota como em processamento
      if (i > 5)  and ( (cStat <> 100 ) and (cStat <> 205) and (cStat <> 302) ) or (cStat=105) or (Pos('lote em processamento',LowerCase(temp)) > 0) then
      begin

        mensagemerro('N�o foi poss�vel confirmar a gera��o da NF-e, consulte manualmente mais tarde');
        self.nfeProcessamento := true;
        FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);

        //FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe); celio0712
        FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe);

      end;

    end;

    if cStat = 100 then //100 gerada
    begin

      FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);
      //FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe);  celio0712
      FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe);
      //FComponentesNfe.ACBrNFe.DANFE.ImprimirDANFE();

    end
    else if (cStat = 205) or (cStat = 302) or (msgerro <> '') then // 205 denegada 302 denegado o uso
    begin

      {ja teve caso que o cstat veio 0 e a msgerro veio como denegado o uso, ent�o alem de testar o cstat verifico
      se na mensagem de retorno contem a string "denegada" na sefaz ou "denegado" o uso }

      if ((Pos('denegada',LowerCase(msgerro)) > 0) or (Pos('denegado',LowerCase(msgerro)) > 0)) then
      begin
        //FdataBase.DefaultTransaction.CommitRetaining;
        FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);
        //FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe); celio0712
        FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe);
        self.denegada := True;
      end
      else
      begin

        //ja vi casos onde a msgerro retornou vazia e o cstat do componente veio 0
        //n� vers�o 2.00 da nf-e isso ocorrria, na 3.10 n�o ocorreu mais esta situa��o
        if (msgerro = '') and (cStat = 0) then
          strErros.Add('Erro desconhecido ao gerar NF-e')
        else
          strErros.Add(msgerro);

      end;

    end;

  finally

    frmStatus.Close;
    Screen.Cursor := crDefault;

  end;

end;

procedure TObjTransmiteNFE.transmiteSefazEventoNFCE;
var
  vNumLote:Integer;
  msgerro,msgDenegada:string;
  i:integer;
begin

  if teveErros then
    Exit;

  vNumLote := StrToInt(OnlyNumber('1'));

  try

    Screen.Cursor := crHourGlass;
    msgerro := '';
    msgDenegada := '';
    self.denegada := False;

    {frmStatus.lblStatus.Caption := 'Consultando servi�o...';
    frmStatus.Show;
    Application.ProcessMessages;
    msgerro := consultaServico(false);
    if (msgerro <> '') then
    begin
      frmStatus.Close;
      strErros.Add(msgerro);
      Exit;
    end;}

    frmStatus.lblStatus.Caption := 'Transmitindo NFC-e...';
    frmStatus.Show;
    Application.ProcessMessages;

    //10/02/15 - sincrono estava true, mas nao retornava que havia gerado, entao
    //voltei para false

    try
      FComponentesNfe.ACBrNFe.Enviar(vNumLote,self.FDirBackup,false,false);
      //FComponentesNfe.ACBrNFe.Enviar(vNumLote,false,false); a linha de cima tava comentada - fabio jr
    except
      on e:Exception do
      begin
        msgerro := e.Message;
      end;
    end;

    frmStatus.Close;

    //showmessage('retorno '+FComponentesNfe.ACBrNFe.WebServices.Enviar.RetornoWS);

    if ((FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat <> 100) and (FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat <> 205) and (FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat <> 302)) or (msgerro <> '')  then //100 gerada 205 denegada 302 denegado o uso
    begin

        if ((Pos('denegada',LowerCase(msgerro)) > 0) or (Pos('denegado',LowerCase(msgerro)) > 0)) then
        begin
          FdataBase.DefaultTransaction.CommitRetaining;
          FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);
          //FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe); celio0712
          FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe);
          ShowMessage('NFC-e foi gerada mais esta denegada o uso, n�o podendo ser mais utilizada');
          self.denegada := True;
        end
        else
        begin

          if (msgerro <> '') or (FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat = 0) then
            strErros.Add(msgerro)
          else
            strErros.Add('Erro na transmiss�o. cStat: '+IntToStr(FComponentesNfe.ACBrNFe.WebServices.Retorno.cStat) + #13 + 'Motivo: '+FComponentesNfe.ACBrNFe.WebServices.Retorno.xMotivo);

        end;

    end
    else
    begin

      FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Retorno.RetWS);
      //FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe); 0712
      FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe);
      FdataBase.DefaultTransaction.CommitRetaining;

      FComponentesNfe.ACBrNFe.DANFE := FComponentesNfe.ACBrNFeDANFCeFortes1;

      FComponentesNfe.ACBrNFe.DANFE.ImprimirDANFE();

      FComponentesNfe.ACBrNFe.DANFE := FComponentesNfe.ACBrNFeDANFERave1;

    end;

  finally

    frmStatus.Close;
    Screen.Cursor := crDefault;

  end;

end;


function TObjTransmiteNFE.transmiteXMLContingente(var pRecibo:string;pathXML:string):Boolean;
begin

  result := False;

  preparaFComponente;
  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;

  if pathXML <> '' then
  begin
    if not FileExists(pathXML) then
    begin
      MensagemAviso ('Caminho do arquivo xml inv�lido');
      Exit;
    end;
  end
  else
  begin
    FComponentesNfe.OpenDialog1.Title := 'Selecione o XML';
    FComponentesNfe.OpenDialog1.DefaultExt := '*.XML';
    FComponentesNfe.OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Todos os Arquivos (*.*)|*.*';
    FComponentesNfe.OpenDialog1.InitialDir := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe;

    if FComponentesNfe.OpenDialog1.Execute then
      pathXML := FComponentesNfe.OpenDialog1.FileName
    else
      Exit;
  end;

  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile (pathXML);

  try
    if not (FComponentesNfe.ACBrNFe.WebServices.StatusServico.Executar) then
    begin
      //ShowMessage(FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg);
      strErros.Add(FcomponentesNfe.ACBrNFe.WebServices.StatusServico.Msg);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.WebServices.Enviar.Lote := '0';

    if not (FComponentesNfe.ACBrNFe.WebServices.Enviar.Executar()) then
    begin
       //ShowMessage(FcomponentesNfe.ACBrNFe.WebServices.Enviar.Msg);
       strErros.Add(FcomponentesNfe.ACBrNFe.WebServices.Enviar.Msg);
       Exit;
    end

  except
    on e:Exception do
    Begin
      mensagemerro('Erro na tentativa de enviar a NFE'+#13+E.message);
      exit;
    end;
  end;

  precibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;

  FComponentesNfe.ACBrNFe.WebServices.Retorno.Recibo := FComponentesNfe.ACBrNFe.WebServices.Enviar.Recibo;
  if not (FComponentesNfe.ACBrNFe.WebServices.Retorno.Executar) then
  begin
    //ShowMessage(FComponentesNfe.ACBrNFe.WebServices.Retorno.Msg);
    strerros.Add(FComponentesNfe.ACBrNFe.WebServices.Retorno.Msg);
    Exit;
  end;

  imprimeDanfe;
  result := True;

end;

function TObjTransmiteNFE.cancelaPelaChave(chave,protocoloAutorizacao,justificativa:string;var arqCanc:string): Boolean;
var
 xmlCancelamento:TStringList;
 idLote:string;
 CNPJ:string;
 dhEventoAux:string;
begin

  result := False;

  preparaFComponente;
  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;
    
  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

  if chave = '' then
    if not(InputQuery('WebServices Eventos: Cancelamento', 'Chave da NF-e', Chave)) then
      exit;

  Chave := Trim((Chave));

  if justificativa = '' then
    if not(InputQuery('WebServices Eventos: Cancelamento', 'Justificativa do Cancelamento', Justificativa)) then
      exit;

  if protocoloAutorizacao = '' then
   if not(InputQuery('WebServices Eventos: Cancelamento', 'Protocolo de Autoriza��o', protocoloAutorizacao)) then
    exit;

  idLote := '1';
  //if not(InputQuery('WebServices Eventos: Cancelamento', 'Identificador de controle do Lote de envio do Evento', idLote)) then
     //exit;

  CNPJ := copy(Chave,7,14);
  //if not(InputQuery('WebServices Eventos: Cancelamento', 'CNPJ ou o CPF do autor do Evento', CNPJ)) then
     //exit;

  //dhEventoaux := DateTimeToStr(IncHour(now,1));
  dhEventoaux := DateTimeToStr(now);
  if FComponentesNfe.ACBrNFe.Configuracoes.WebServices.UF <> 'MS' then
    dhEventoaux := InputBox('Hora','Hora do cancelamento: ',dhEventoaux);


  try

    FComponentesNfe.ACBrNFe.EventoNFe.Evento.Clear;
    with FComponentesNfe.ACBrNFe.EventoNFe.Evento.Add do
     begin
       infEvento.chNFe := Chave;
       infEvento.CNPJ   := CNPJ;
       infEvento.dhEvento := StrToDateTime(dhEventoAux);
       infEvento.tpEvento := teCancelamento;
       infEvento.detEvento.xJust := Justificativa;
       infEvento.detEvento.nProt := protocoloAutorizacao;
     end;

    //FComponentesNfe.ACBrNFe.EnviarEventoNFe(StrToInt(idLote)); celio0712
    FComponentesNfe.ACBrNFe.EnviarEvento(StrToInt(idLote));

    
    if (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 101) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 151) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 135) then
      Exit;

    //arqCanc := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathCan + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml'; celio0712
    //arqCanc := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml';
    arqCanc := path_cancelamento + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml';

    FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
    FComponentesNfe.MemoResp.Lines.SaveToFile(arqCanc);

  except
    on e:Exception do
    begin
      MensagemAviso(e.Message);
      Exit;
    end;
    
  end;

  result := True;


end;

function TObjTransmiteNFE.cancelaPelaChaveNFCe(chave,protocoloAutorizacao,justificativa:string;var arqCanc:string): Boolean;
var
 xmlCancelamento:TStringList;
 idLote:string;
 CNPJ:string;
 dhEventoAux:string;
begin

  result := False;

  preparaFComponente;
  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;

  FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
  FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

  if chave = '' then
    if not(InputQuery('WebServices Eventos: Cancelamento', 'Chave da NF-e', Chave)) then
      exit;

  Chave := Trim((Chave));

  if justificativa = '' then
    if not(InputQuery('WebServices Eventos: Cancelamento', 'Justificativa do Cancelamento', Justificativa)) then
      exit;

  if protocoloAutorizacao = '' then
   if not(InputQuery('WebServices Eventos: Cancelamento', 'Protocolo de Autoriza��o', protocoloAutorizacao)) then
    exit;

  idLote := '1';
  //if not(InputQuery('WebServices Eventos: Cancelamento', 'Identificador de controle do Lote de envio do Evento', idLote)) then
     //exit;

  CNPJ := copy(Chave,7,14);
  //if not(InputQuery('WebServices Eventos: Cancelamento', 'CNPJ ou o CPF do autor do Evento', CNPJ)) then
     //exit;

  //dhEventoaux := DateTimeToStr(IncHour(now,1));
  dhEventoaux := DateTimeToStr(now);
  if FComponentesNfe.ACBrNFe.Configuracoes.WebServices.UF <> 'MS' then
    dhEventoaux := InputBox('Hora','Hora do cancelamento: ',dhEventoaux);


  try

    FComponentesNfe.ACBrNFe.EventoNFe.Evento.Clear;
    with FComponentesNfe.ACBrNFe.EventoNFe.Evento.Add do
     begin
       infEvento.chNFe := Chave;
       infEvento.CNPJ   := CNPJ;
       infEvento.dhEvento := StrToDateTime(dhEventoAux);
       infEvento.tpEvento := teCancelamento;
       infEvento.detEvento.xJust := Justificativa;
       infEvento.detEvento.nProt := protocoloAutorizacao;
     end;

    //FComponentesNfe.ACBrNFe.EnviarEventoNFe(StrToInt(idLote)); celio0712
    FComponentesNfe.ACBrNFe.EnviarEvento(StrToInt(idLote));

    
    if (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 101) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 151) and (FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat <> 135) then
      Exit;

    //arqCanc := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathCan + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml'; celio0712
    //arqCanc := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathEvento + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml';
    arqCanc := path_cancelamento + StringReplace(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe,'Nfe','',[rfIgnoreCase])+'-can.xml';

    FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FcomponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
    FComponentesNfe.MemoResp.Lines.SaveToFile(arqCanc);

  except
    on e:Exception do
    begin
      MensagemAviso(e.Message);
      Exit;
    end;

  end;

  result := True;


end;

{ TODO : preencheComponente_evento }
procedure TObjTransmiteNFE.preencheComponenteEvento;
var
  objsql:TobjSqlTxt;
  FTotSeg, FTotFret, FTotVoutr, FTotVprod, FTotVDesc:Currency;
  F_IDE:TComIDE;F_INFADIC:TComInfAdic;
  F_EMIT:TComEmit;F_ENDEREMIT:TComEnderEmit;
  F_DESTC:TComDestC;F_DESTF:TComDestF;
  F_ENDERDESTC:TComEnderDestC;F_ENDERDESTF:TComEnderDestF;
  F_TRANSPORTE:TComTransp;F_TRANSPORTA:TComTransporta;
  F_VEICTRANSP:TComveicTransp;F_VOL:TComVol;
  F_PROD:TComProdutos; F_ICMS:TComICMS; F_ICMSST:TComICMSST;
  F_PIS:TComPIS;F_COFINS:TComCOFINS;F_IPI:TComIPI;
  F_COMPLEMENTAR:TComComplementar;F_DEVOLUCAO:TComDevolucao;
  F_CUPOMREFERENCIADO:TComCupomRef;F_COBRDUP:TComDup;F_COBRFAT:TComFat;
  F_AutXML:TComAutorizacao;
  F_DIFAL: TComDIFAL;
  i:Integer;
  strValues,strSubValues:TStringList;
  auxInfo, UFEmit, UFDest:string;
begin

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    //ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    strErros.Add('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;

  objsql := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try

    //----------------------------------------------------------------------------
    F_IDE := TComIDE.Create(nil);
    F_IDE.FDatabase := self.FdataBase;
    F_IDE.FSQL := objsql.readString('IDE');
    F_IDE.Registro := 'Ide';

    F_INFADIC := TComInfAdic.Create(nil);
    F_INFADIC.FDatabase := self.FdataBase;
    F_INFADIC.FSQL := objsql.readString('INFADIC');
    F_INFADIC.Registro := 'Informa��es adicionais';

    F_EMIT := TComEmit.Create(nil);
    F_EMIT.FDatabase := self.FdataBase;
    F_EMIT.FSQL := objsql.readString('EMITENTE');
    F_EMIT.Registro := 'Emitente';

    F_ENDEREMIT := TComEnderEmit.Create(nil);
    F_ENDEREMIT.FDatabase := self.FdataBase;
    F_ENDEREMIT.FSQL := objsql.readString('ENDERECO EMITENTE');
    F_ENDEREMIT.Registro := 'Endere�o do emitente';

    F_DESTC := TComDestC.Create(nil);
    F_DESTC.FDatabase := self.FdataBase;
    F_DESTC.FSQL := objsql.readString('DESTINATARIO CLIENTE');
    F_DESTC.Registro := 'Destinat�rio cliente';

    F_DESTF := TComDestF.Create(nil);
    F_DESTF.FDatabase := self.FdataBase;
    F_DESTF.FSQL := objsql.readString('DESTINATARIO FORNECEDOR');
    F_DESTF.Registro := 'Destinat�rio fornecedor';

    F_ENDERDESTC := TComEnderDestC.Create(nil);
    F_ENDERDESTC.FDatabase := self.FdataBase;
    F_ENDERDESTC.FSQL := objsql.readString('ENDERECO DESTINATARIO CLIENTE');
    F_ENDERDESTC.Registro := 'Endere�o destinat�rio cliente';

    F_ENDERDESTF := TComEnderDestF.Create(nil);
    F_ENDERDESTF.FDatabase := self.FdataBase;
    F_ENDERDESTF.FSQL := objsql.readString('ENDERECO DESTINATARIO FORNECEDOR');
    F_ENDERDESTF.Registro := 'Endere�o destinat�rio fornecedor';

    F_TRANSPORTE := TComTransp.Create(nil);
    F_TRANSPORTE.FDatabase := self.FdataBase;
    F_TRANSPORTE.FSQL := objsql.readString('TRANSPORTE');
    F_TRANSPORTE.Registro := 'Transporte';

    F_TRANSPORTA := TComTransporta.Create(nil);
    F_TRANSPORTA.FDatabase := self.FdataBase;
    F_TRANSPORTA.FSQL := objsql.readString('TRANSPORTA');
    F_TRANSPORTA.Registro := 'Transporta';

    F_VEICTRANSP := TComveicTransp.Create(nil);
    F_VEICTRANSP.FDatabase := self.FdataBase;
    F_VEICTRANSP.FSQL := objsql.readString('VEICULO TRANSPORTADORA');
    F_VEICTRANSP.Registro := 'Ve�culo transportadora';

    F_VOL := TComVol.Create(nil);
    F_VOL.FDatabase := self.FdataBase;
    F_VOL.FSQL := objsql.readString('VOLUMES TRANSPORTADOS');
    F_VOL.Registro := 'volumes transportados';

    F_PROD := TComProdutos.Create(nil);
    F_PROD.FDatabase := self.FdataBase;
    F_PROD.FSQL := objsql.readString('PRODUTOS');
    F_PROD.Registro := 'Produtos';

    F_ICMS := TComICMS.Create(nil);
    F_ICMS.FDatabase := self.FdataBase;
    F_ICMS.FSQL := objsql.readString('ICMS NORMAL');
    F_ICMS.Registro := 'Icms normal';

    F_ICMSST := TComICMSST.Create(nil);
    F_ICMSST.FDatabase := self.FdataBase;
    F_ICMSST.FSQL := objsql.readString('ICMS ST');

    F_PIS := TComPIS.Create(nil);
    F_PIS.FDatabase := self.FdataBase;
    F_PIS.FSQL := objsql.readString('PIS');

    F_COFINS := TComCOFINS.Create(nil);
    F_COFINS.FDatabase := self.FdataBase;
    F_COFINS.FSQL := objsql.readString('COFINS');

    F_IPI := TComIPI.Create(nil);
    F_IPI.FDatabase := self.FdataBase;
    F_IPI.FSQL := objsql.readString('IPI');

    F_COBRFAT := TComFat.Create(nil);
    F_COBRFAT.FDatabase := self.FdataBase;
    F_COBRFAT.FSQL := objsql.readString('COBRANCA FATURA');

    F_COBRDUP := TComDup.Create(nil);
    F_COBRDUP.FDatabase := self.FdataBase;
    F_COBRDUP.FSQL := objsql.readString('COBRANCA DUPLICATA');

    F_COMPLEMENTAR := TComComplementar.Create(nil);
    F_COMPLEMENTAR.FDatabase := self.FdataBase;
    F_COMPLEMENTAR.chaveRef := self.chaveRef;

    F_DEVOLUCAO := TComDevolucao.Create(nil);
    F_DEVOLUCAO.FDatabase := self.FdataBase;
    F_DEVOLUCAO.cUF_ref := self.cUF_ref;
    F_DEVOLUCAO.AAMM_ref := self.AAMM_ref;
    F_DEVOLUCAO.CNPJ_ref:=self.CNPJ_ref;
    F_DEVOLUCAO.modeloRef:=self.modeloRef;
    F_DEVOLUCAO.serie_ref:=self.serie_ref;
    F_DEVOLUCAO.nNF_ref := self.nNF_ref;
    F_DEVOLUCAO.chaveRef := self.chaveRef;
    F_DEVOLUCAO.modeloRef := self.modeloRef;

    F_CUPOMREFERENCIADO := TComCupomRef.Create(nil);
    F_CUPOMREFERENCIADO.FDatabase := Self.FdataBase;
    F_CUPOMREFERENCIADO.nCOO      := self.nCOO;
    F_CUPOMREFERENCIADO.strRefCupom    := self.strRefCupom;
    F_CUPOMREFERENCIADO.nECF      := self.nECF;
    F_CUPOMREFERENCIADO.modelo    := self.modeloecf;

    F_AutXML := TComAutorizacao.Create(nil);
    F_AutXML.FDatabase := Self.FdataBase;
    F_AutXML.FSQL := objsql.readString('AUTORIZACAO XML');

    F_DIFAL := TComDIFAL.Create(nil);
    F_DIFAL.Database := self.FdataBase;
    F_DIFAL.FSQL := objsql.readString('DIFAL');
    
    //----------------------------------------------------------------------------

    F_IDE.pCodigoNF := self.pCodigoNF;
    F_IDE.strErros := self.strErros;
    F_IDE.denegada := self.denegada;
    F_IDE.numeroNFE := self.numeroNFE;

    F_IDE.AbreQuery;
    if teveErros then Exit;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    with FComponentesNfe.ACBrNFe.NotasFiscais.Add.NFe do
    begin


      {IDE}

      try

        Ide.tpImp   := FComponentesNfe.ACBrNFe.DANFE.TipoDANFE;
        Ide.natOp   := F_IDE.FQuery.Fields[0].Text;
        Ide.serie   := F_IDE.FQuery.Fields[1].AsInteger;
        IDE.indPag  := F_IDE.getIndPag(F_IDE.FQuery.fields[2].text);
        Ide.dEmi    := F_IDE.FQuery.Fields[3].AsDateTime;
        ide.dSaiEnt := F_IDE.FQuery.Fields[4].AsDateTime;
        Ide.hSaiEnt := F_IDE.FQuery.Fields[5].AsDateTime;
        Ide.cUF     := getCampoTable('CODIGO UF EMITENTE').Fields[0].AsInteger;
        Ide.cMunFG  := getCampoTable('CODIGO CIDADE EMITENTE').Fields[0].AsInteger;
        Ide.cNF     := StrToInt(self.numeroNFE);
        Ide.nNF     := ide.cNF;


        Ide.verProc   := '1.0.0.0'; //Vers�o do seu sistema

        if (F_CUPOMREFERENCIADO.strRefCupom <> '') then
        begin
          //modelo;numeroECF;numeroCUPOM
          //ex: 2D;001;009842|2D;001;009848|
          try
            try
              auxInfo := '';
              strValues    := TStringList.Create;
              strSubValues := TStringList.Create;

              strValues.Clear;
              ExtractStrings(['|'],[' '],PChar(F_CUPOMREFERENCIADO.strRefCupom),strValues);
              for i:=0 to strValues.Count-1 do
              begin
                strSubValues.Clear;
                ExtractStrings([';'],[' '],PChar(strValues[i]),strSubValues);
                with ide.NFref.Add do
                begin
                  RefECF.modelo  := F_CUPOMREFERENCIADO.getModeloECF(strSubValues[0]);
                  RefECF.nECF    := strSubValues[1];
                  RefECF.nCOO    := strSubValues[2];
                  auxInfo := auxInfo + RefECF.nCOO+', ';
                end
              end;
              auxInfo := Copy(auxInfo,1,Length(auxInfo)-2);
              auxInfo := auxInfo+'.';
              F_CUPOMREFERENCIADO.infComplementares := #13+'NF-e referenciada por cupom fiscal. Numero cupom: '+auxInfo;
            finally
              FreeAndNil(strValues);
              FreeAndNil(strSubValues);
            end;
          except
            on e:Exception do
            begin
              strErros.Add('Erro ao referenciar CUPOM. Msg: '+e.Message);
              raise;
            end;
          end;

        end
        else if (F_CUPOMREFERENCIADO.nCOO <> '') and (F_CUPOMREFERENCIADO.nECF <> '') then
        begin

          if teveErros then Exit;
          with ide.NFref.Add do
          begin
            RefECF.modelo := F_CUPOMREFERENCIADO.getModeloECF;
            RefECF.nECF   := F_CUPOMREFERENCIADO.nECF;
            RefECF.nCOO   := F_CUPOMREFERENCIADO.nCOO;
            F_CUPOMREFERENCIADO.infComplementares := #13+'NF-e referenciada por cupom fiscal. Numero cupom: '+F_CUPOMREFERENCIADO.nCOO;
          end;
        end;

        if self.nfeDevolucao then
        begin

           {3.10}
           Ide.finNFe := fnDevolucao;

          if teveErros then Exit;
          if F_DEVOLUCAO.modeloRef = 1 then
            begin

              with Ide.NFref.add do
              begin
                RefNF.cUF    := StrToInt(self.cUF_ref);
                RefNF.AAMM   := FormatDateTime('yymm',StrToDate (self.AAMM_ref));
                RefNF.CNPJ   := RetornaSoNumeros(self.CNPJ_ref);
                RefNF.modelo := modeloRef;
                RefNF.serie  := StrToInt(Self.serie_ref);
                RefNF.nNF    := StrToInt(self.nNF_ref);
              end;

            end
          else if F_DEVOLUCAO.modeloRef = 55 then
          begin
            if teveErros then Exit;
            with ide.NFref.Add do
            begin
              refNFe := Self.chaveRef;
            end
          end;

        end
        else
        if self.nfeComplementar then
        begin

          if teveErros then Exit;
          Ide.finNFe  := fnComplementar;
          with Ide.NFref.Add do
            refNFe := F_COMPLEMENTAR.chaveRef;

        end
        else
          ide.finNFe  := fnNormal;


        //aqui existe uma op��o: devolu��o de entrada (cfop:entrada) ex: cfop 2201
        // Ide.finNFe := fnDevolucao; e IDE.tpNF := tnEntrada;

        if self.tipoNF = 'SAIDA' then
          IDE.tpNF    := tnSaida
        else
          IDE.tpNF    := tnEntrada;

      Except
        on e:exception do
        begin
          strErros.Add('Erro ao preencher IDE. '+e.Message);
        end;
      end;


      {INFORMACOES ADICIONAIS}
      F_INFADIC.pCodigoNF := self.pCodigoNF;
      F_INFADIC.strErros := self.strErros;
      F_INFADIC.denegada := self.denegada;
      F_INFADIC.AbreQuery;

      try

        if teveErros then Exit;
        InfAdic.infCpl := F_INFADIC.FQuery.Fields[0].Text + F_CUPOMREFERENCIADO.infComplementares;

        {FATURAS E DUPLICATAS}
        F_COBRFAT.pCodigoNF := self.pCodigoNF;
        F_COBRFAT.strErros := self.strErros;
        F_COBRFAT.denegada := self.denegada;
        F_COBRFAT.AbreQuery;
        if teveErros then Exit;

        if not F_COBRFAT.FQuery.IsEmpty then
        begin
          Cobr.Fat.nFat  := F_COBRFAT.FQuery.Fields[0].AsString;
          Cobr.Fat.vOrig := F_COBRFAT.FQuery.Fields[1].AsCurrency;
          Cobr.Fat.vDesc := F_COBRFAT.FQuery.Fields[2].AsCurrency;
          Cobr.Fat.vLiq  := F_COBRFAT.FQuery.Fields[3].AsCurrency;
        end;

        F_COBRDUP.pCodigoNF := self.pCodigoNF;
        F_COBRDUP.strErros := self.strErros;
        F_COBRDUP.denegada := self.denegada;
        F_COBRDUP.AbreQuery;
        if teveErros then Exit;

        F_COBRDUP.FQuery.First;
        while not (F_COBRDUP.FQuery.Eof) do
        begin
          with Cobr.Dup.Add do
          begin
            nDup  := F_COBRDUP.FQuery.Fields[0].AsString;
            dVenc := F_COBRDUP.FQuery.Fields[1].AsDateTime;;
            vDup  := F_COBRDUP.FQuery.Fields[2].AsCurrency;
          end;
          F_COBRDUP.FQuery.Next;
        end;

      Except
        on e:exception do
        begin
          strErros.Add('Erro ao preencher INFORMACOES ADICIONAIS. '+e.Message);
        end;
      end;


      {EMIT}
      F_EMIT.pCodigoNF := self.pCodigoNF;
      F_EMIT.strErros := self.strErros;
      F_EMIT.denegada := self.denegada;
      F_EMIT.AbreQuery;
      if teveErros then Exit;

      try
      
        Emit.CNPJCPF := F_EMIT.FQuery.Fields[0].Text;
        Emit.IE      := F_EMIT.FQuery.Fields[2].Text;
        Emit.xNome   := F_EMIT.FQuery.Fields[1].Text;
        Emit.xFant   := F_EMIT.FQuery.Fields[4].Text;

        if self.CRT <> '' then
          emit.CRT := F_EMIT.getCRT(Self.CRT)
        else
          Emit.CRT := F_EMIT.getCRT(F_EMIT.FQuery.Fields[6].Text);

        {ENDER EMIT}
        F_ENDEREMIT.pCodigoNF := self.pCodigoNF;
        F_ENDEREMIT.strErros := self.strErros;
        F_ENDEREMIT.denegada := self.denegada;
        F_ENDEREMIT.AbreQuery;
        if teveErros then Exit;

        Emit.EnderEmit.fone    := F_ENDEREMIT.FQuery.Fields[0].Text;

        if (F_ENDEREMIT.FQuery.Fields[1].Text <> '') then
          Emit.EnderEmit.CEP     := StrToInt(F_ENDEREMIT.FQuery.Fields[1].Text);

        Emit.EnderEmit.xLgr    := F_ENDEREMIT.FQuery.Fields[2].Text;
        Emit.EnderEmit.nro     := F_ENDEREMIT.FQuery.Fields[3].Text;
        Emit.EnderEmit.xCpl    := F_ENDEREMIT.FQuery.Fields[4].Text;
        Emit.EnderEmit.xBairro := F_ENDEREMIT.FQuery.Fields[5].Text;
        Emit.EnderEmit.cMun    := StrToInt(F_ENDEREMIT.FQuery.Fields[6].Text);
        Emit.EnderEmit.xMun    := F_ENDEREMIT.FQuery.Fields[7].Text;
        Emit.EnderEmit.UF      := F_ENDEREMIT.FQuery.Fields[8].Text;
        Emit.EnderEmit.cPais   := StrToInt(F_ENDEREMIT.FQuery.Fields[9].Text);
        Emit.EnderEmit.xPais   := F_ENDEREMIT.FQuery.Fields[10].Text;
        UFEmit := Emit.EnderEmit.UF ;
      Except
        on e:exception do
        begin
          strErros.Add('Erro ao preencher INFORMACOES EMITENTE. '+e.Message);
        end;
      end;

      {DEST}
      F_DESTC.pCodigoNF := self.pCodigoNF;
      F_DESTC.strErros := self.strErros;
      F_DESTC.denegada := self.denegada;

      if F_DESTC.AbreQuery then
      begin

        if teveErros then Exit;

        Dest.CNPJCPF := F_DESTC.FQuery.Fields[0].Text;
        dest.IE      := F_DESTC.FQuery.Fields[1].Text;
        dest.xNome   := F_DESTC.FQuery.Fields[2].Text;
        dest.Email   := F_DESTC.FQuery.Fields[6].Text;

        if (F_DESTC.FQuery.Fields[1].Text) <> '' then
          DEST.IE := F_DESTC.FQuery.Fields[1].Text
        else
        begin
          if F_DESTC.FQuery.Fields[5].Text = 'J' then
          begin
            if F_DESTC.FQuery.Fields[4].Text <> '' then
              dest.IE := F_DESTC.FQuery.Fields[4].Text
            else
            dest.IE := '';
          end
          else
            DEST.IE := '';
        end;

        {3.10}                          
        if (Dest.IE = '') then
        begin
          Dest.indIEDest := inNaoContribuinte;
          ide.indFinal   := cfConsumidorFinal;
        end
        else
          Dest.indIEDest := inContribuinte;

        {ENDERECO DESTINATARIO CLIENTE}
        F_ENDERDESTC.pCodigoNF := self.pCodigoNF;
        F_ENDERDESTC.strErros := self.strErros;
        F_ENDERDESTC.denegada := self.denegada;
        F_ENDERDESTC.AbreQuery;
        if teveErros then Exit;

        if (F_ENDERDESTC.FQuery.Fields[0].Text <> '') then
          dest.EnderDest.CEP     := StrToInt(F_ENDERDESTC.FQuery.Fields[0].Text);

        dest.EnderDest.cMun    := StrToInt(F_ENDERDESTC.FQuery.Fields[1].Text);
        dest.EnderDest.UF      := F_ENDERDESTC.FQuery.Fields[2].Text;
        dest.EnderDest.fone    := F_ENDERDESTC.FQuery.Fields[3].Text;
        dest.EnderDest.xBairro := F_ENDERDESTC.FQuery.Fields[4].Text;
        dest.EnderDest.cPais   := StrToInt(F_ENDERDESTC.FQuery.Fields[5].Text);
        dest.EnderDest.xPais   := F_ENDERDESTC.FQuery.Fields[6].Text;
        dest.EnderDest.xLgr    := F_ENDERDESTC.FQuery.Fields[7].Text;
        dest.EnderDest.xMun    := F_ENDERDESTC.FQuery.Fields[8].Text;
        UFDest := dest.EnderDest.UF;

        if F_ENDERDESTC.FQuery.Fields[9].Text <> '' then
          dest.EnderDest.nro := F_ENDERDESTC.FQuery.Fields[9].Text
        else
          Dest.EnderDest.nro := 'S/N';

      end
      else
      begin

        F_DESTF.pCodigoNF := self.pCodigoNF;
        F_DESTF.strErros := self.strErros;
        F_DESTF.denegada := self.denegada;

        if F_DESTF.AbreQuery then
        begin
          if teveErros then Exit;
          Dest.CNPJCPF := F_DESTF.FQuery.Fields[0].Text;
          dest.IE      := F_DESTF.FQuery.Fields[1].Text;
          Dest.xNome   := F_DESTF.FQuery.Fields[2].Text;
          DEST.Email   := F_DESTF.FQuery.Fields[4].Text;
        end;

        {ENDERECO DESTINATARIO FORNECEDOR}
        F_ENDERDESTF.pCodigoNF := self.pCodigoNF;
        F_ENDERDESTF.strErros := self.strErros;
        F_ENDERDESTF.denegada := self.denegada;
        F_ENDERDESTF.AbreQuery;
        if teveErros then Exit;

        if (F_ENDERDESTF.FQuery.Fields[0].Text <> '') then
          dest.EnderDest.CEP     := StrToInt(F_ENDERDESTF.FQuery.Fields[0].Text);

        dest.EnderDest.cMun    := StrToInt(F_ENDERDESTF.FQuery.Fields[1].Text);
        dest.EnderDest.UF      := F_ENDERDESTF.FQuery.Fields[2].Text;
        dest.EnderDest.fone    := F_ENDERDESTF.FQuery.Fields[3].Text;
        dest.EnderDest.xBairro := F_ENDERDESTF.FQuery.Fields[4].Text;
        dest.EnderDest.cPais   := StrToInt(F_ENDERDESTF.FQuery.Fields[5].Text);
        dest.EnderDest.xPais   := F_ENDERDESTF.FQuery.Fields[6].Text;
        dest.EnderDest.xLgr    := F_ENDERDESTF.FQuery.Fields[7].Text;
        dest.EnderDest.xMun    := F_ENDERDESTF.FQuery.Fields[8].Text;
        UFDest := dest.EnderDest.UF;

        if F_ENDERDESTF.FQuery.Fields[9].Text <> '' then
          dest.EnderDest.nro := F_ENDERDESTF.FQuery.Fields[9].Text
        else
          Dest.EnderDest.nro := 'S/N';

      end;

      {3.10}
      if (Emit.EnderEmit.UF = Dest.EnderDest.UF) then
        Ide.idDest := doInterna
      else
      if (Dest.EnderDest.UF = 'EX') then
        Ide.idDest := doExterior
      else
        Ide.idDest := doInterestadual;

      if trim(self.end_Cpf_Cnpj)<>'' then
      begin

        Entrega.CNPJCPF := self.end_Cpf_Cnpj;
        Entrega.xLgr := self.end_Rua;
        Entrega.nro := self.end_Numero;
        Entrega.xCpl := self.end_Complemento;
        Entrega.xBairro := self.end_Bairro;
        Entrega.cMun := StrToInt(Self.end_Cod_Municipio);
        Entrega.xMun := Self.end_Municipio;
        Entrega.UF := Self.end_UF;

      end;

      {

      //identifica��o do estrangeiro em caso de opera��o com exterior
      Dest.idEstrangeiro := '00154';

      //EXPORTA, informa��es do local de embarque em caso de opera��o com o exterior
      exporta.UFembarq     := 'MS';
      exporta.xLocEmbarq   := 'Dourados';
      exporta.UFSaidaPais  := 'MS';
      exporta.xLocExporta  := ''; //local onde ocorrer� o embarque dos produtos
      exporta.xLocDespacho := '';

      }

      {AUTORIZACAO PARA DOWNLOAD XML}
      {O Estado da Bahia ir� exigir o preenchimento da dados de identifica��o do
      Escrit�rio de Contabilidade, CNPJ ou CPF, nas NF-e emitidas por seus
      contribuintes, dados estes conforme informa��o no cadastro da SEFAZ.
      Caso a empresa n�o possua um contrato com esse profissional ou Escrit�rio
      dever� informar o CNPJ da SEFAZ (13.937.073/0001-56) no Grupo de Autoriza��o.

      fonte:http://www.sefaz.ba.gov.br/especiais/aviso_emissores_nfe.html}

      if(UpperCase(Emit.EnderEmit.UF) = 'BA') then
      begin
        //F_AutXML.pCodigoNF := Self.pCodigoNF;
        F_AutXML.strErros := self.strErros;
        if F_AutXML.AbreQuery then
        begin
          if teveErros then Exit;
          with autXML.Add do
          begin
            CNPJCPF := F_AutXML.FQuery.Fields[0].Text;
          end;
        end
        else
        begin
          //caso n�o tenha usar cnpj da sefaz BA
          with autXML.add do
          begin
            CNPJCPF := '13937073000156';
          end;
        end;

      end;

      {TRANSPORTADORA modalidade}
      F_TRANSPORTE.pCodigoNF := self.pCodigoNF;
      F_TRANSPORTE.strErros := self.strErros;
      F_TRANSPORTE.denegada := self.denegada;

      if F_TRANSPORTE.AbreQuery then
      begin
        if teveErros then Exit;
        Transp.modFrete := F_TRANSPORTE.getModFrete;

        {TRANSPORTA dados}
        F_TRANSPORTA.pCodigoNF := self.pCodigoNF;
        F_TRANSPORTA.strErros := self.strErros;
        F_TRANSPORTA.denegada := self.denegada;


        if F_TRANSPORTA.AbreQuery then
        begin

          if teveErros then Exit;
          Transp.Transporta.CNPJCPF := F_TRANSPORTA.FQuery.Fields[0].Text;
          Transp.Transporta.IE      := F_TRANSPORTA.FQuery.Fields[1].Text;
          Transp.Transporta.xMun    := F_TRANSPORTA.FQuery.Fields[2].Text;
          Transp.Transporta.UF      := F_TRANSPORTA.FQuery.Fields[3].Text;
          Transp.Transporta.xEnder  := F_TRANSPORTA.FQuery.Fields[4].Text;
          Transp.Transporta.xNome   := F_TRANSPORTA.FQuery.Fields[5].Text;

          {VEICULO TRANSPORTADORA}
          F_VEICTRANSP.pCodigoNF := self.pCodigoNF;
          F_VEICTRANSP.strErros := self.strErros;
          F_VEICTRANSP.denegada := self.denegada;

          if F_VEICTRANSP.AbreQuery then
          begin
            if teveErros then Exit;
            Transp.veicTransp.placa := tira_caracter(F_VEICTRANSP.FQuery.Fields[0].Text,'-');
            Transp.veicTransp.UF    := F_VEICTRANSP.FQuery.Fields[1].Text;
          end;

          {VALUMES TRANSPORTADOS}
          F_VOL.pCodigoNF := self.pCodigoNF;
          F_VOL.strErros := self.strErros;
          F_VOL.denegada := self.denegada;

          if F_VOL.AbreQuery then
          begin
            if teveErros then Exit;
            with Transp.Vol.Add do
            begin
              if F_VOL.FQuery.Fields[0].Text <> '' then
                qVol := F_VOL.FQuery.Fields[0].AsInteger;

              esp    := F_VOL.FQuery.Fields[1].Text;
              marca  := F_VOL.FQuery.Fields[2].Text;
              nVol   := F_VOL.FQuery.Fields[3].Text;
              pesoB  := F_VOL.FQuery.Fields[4].AsCurrency;
              pesoL  := F_VOL.FQuery.Fields[5].AsCurrency;
            end;

          end;

        end;

      end;


      {PRODUTOS}

      F_PROD.pCodigoNF := self.pCodigoNF;
      F_PROD.strErros := self.strErros;
      F_PROD.denegada := self.denegada;

      FTotSeg:=0;
      FTotFret:=0;
      FTotVoutr:=0;
      FTotVprod:=0;
      FTotVDesc:=0;

      if F_PROD.AbreQuery then
      begin

        if teveErros then Exit;

        while not (F_PROD.FQuery.Eof) do
        begin

          with Det.Add do
          begin

            try

              Prod.nItem   := F_PROD.FQuery.RecNo;
              Prod.CFOP    := F_PROD.FQuery.Fields[0].Text;
              Prod.cProd   := F_PROD.FQuery.Fields[1].Text;
              Prod.xProd   := F_PROD.FQuery.Fields[2].Text;
              Prod.qCom    := F_PROD.FQuery.Fields[3].AsCurrency;
              Prod.qTrib   := F_PROD.FQuery.Fields[4].AsCurrency;
              Prod.uCom    := F_PROD.FQuery.Fields[5].Text;
              Prod.uTrib   := F_PROD.FQuery.Fields[6].Text;
              Prod.vUnCom  := F_PROD.FQuery.Fields[7].AsCurrency;
              Prod.vUnTrib := F_PROD.FQuery.Fields[8].AsCurrency;
              Prod.vProd   := F_PROD.FQuery.Fields[9].AsCurrency;
              Prod.vDesc   := F_PROD.FQuery.Fields[10].AsCurrency;
              Prod.NCM     := F_PROD.FQuery.Fields[11].Text;
              Prod.vFrete  := F_PROD.FQuery.Fields[12].AsCurrency;
              Prod.vSeg    := F_PROD.FQuery.Fields[13].AsCurrency;
              Prod.vOutro  := F_PROD.FQuery.Fields[14].AsCurrency;

              //esta sendo preenchido junto com o ICMS_ST
              //MensagemErro('desabilitar CEST producao');
              //Prod.CEST    := F_PROD.FQuery.Fields[17].Text;


              //validando aqui porq dentro da classe TCOmProdutos nao tenho acesso aos fields

              //opera��es internas onde a uf do emitente � igual a uf do destinatario
              if Emit.EnderEmit.UF = Dest.EnderDest.UF then
              begin

                if Ide.tpNF = tnSaida then
                begin

                  if Prod.CFOP[1] = '6' then
                    strErros.Add('CFOP de saida inv�lido para opera��o interna. Produto: '+Prod.xProd+' - '+Prod.cProd)

                end
                else if ide.tpNF = tnEntrada then
                begin

                  if Prod.CFOP[1] = '2' then
                    strErros.Add('CFOP de entrada inv�lido para opera��o interna. Produto: '+Prod.xProd+' - '+Prod.cProd)

                end;

              end
              else
              begin

                //opera��es externas onde a uf do emitente � diferente da uf do destinatario

                if Ide.tpNF = tnSaida then
                begin

                  if Prod.CFOP[1] = '5' then
                    strErros.Add('CFOP de saida inv�lido para opera��o externa. Produto: '+Prod.xProd+' - '+Prod.cProd)

                end
                else if ide.tpNF = tnEntrada then
                begin

                  if Prod.CFOP[1] = '1' then
                    strErros.Add('CFOP de entrada inv�lido para opera��o externa. Produto: '+Prod.xProd+' - '+Prod.cProd)

                end;

              end;

              if self.operacaoCombustivel(prod.CFOP) then
              begin
                {Comb - Grupo de informa��es especificas para combust�veis}
                Prod.comb.cProdANP := F_PROD.FQuery.Fields[16].AsInteger;
                prod.comb.CODIF := '';
                prod.comb.qTemp := 0;
                prod.comb.UFcons := dest.EnderDest.UF;
              end;

              {totais prod}
              FTotSeg   := FTotSeg   + Prod.vSeg;
              FTotFret  := FTotFret  + Prod.vFrete;
              FTotVoutr := FTotVoutr + Prod.vOutro;
              FTotVprod := FTotVprod + Prod.vProd;
              FTotVDesc := FTotVDesc + Prod.vDesc;

            Except
              on e:exception do
              begin
                strErros.Add('Erro ao preencher PRODUTOS. '+e.Message);
              end;
            end;


            with Imposto do
            begin
            
              {ICMS NORMAL}
              F_ICMS.pCodigoNF := self.pCodigoNF;
              F_ICMS.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_ICMS.strErros := self.strErros;
              F_ICMS.denegada := self.denegada;
              F_ICMS.pCodigoProduto := Prod.cProd;

              if F_ICMS.AbreQuery then
              begin


                try
                
                  if teveErros then exit;
                  ICMS.orig   := F_ICMS.getOrigICMS;
                  ICMS.CST    := F_ICMS.getCSTICMS;
                  ICMS.CSOSN  := F_ICMS.getCSOSN;
                  ICMS.modBC  := F_ICMS.getModBCICMS;

                  if Emit.CRT = crtSimplesNacional then
                  begin

                    if ICMS.CSOSN = csosnvazio then
                    begin
                      strErros.Add('CSOSN vazio para o produto: '+prod.cProd);
                      Exit;
                    end;

                  end
                  else
                  begin

                    if ICMS.CST = cstVazio then
                    begin
                      strErros.Add('CST vazio para o produto: '+prod.cProd);
                      Exit;
                    end;

                  end;

                  vTotTrib := StrToCurrDef(F_ICMS.FQuery.Fields[7].Text,0);


                  if F_ICMS.FQuery.Fields[5].AsCurrency > 0 then {aliquota}
                  begin
                    ICMS.vBC    := F_ICMS.FQuery.Fields[3].AsCurrency;
                    ICMS.vICMS  := F_ICMS.FQuery.Fields[4].AsCurrency;
                  end
                  else
                  begin
                    ICMS.vBC   := 0;
                    ICMS.vICMS := 0;
                  end;

                  ICMS.pICMS  := F_ICMS.FQuery.Fields[5].AsCurrency;
                  ICMS.pRedBC := F_ICMS.FQuery.Fields[6].AsCurrency;

                  if ICMS.CST = cst51 then
                    ICMS.vICMSOp := ICMS.vICMS;


                  if (Emit.CRT = crtSimplesNacional) then
                  begin

                    if F_ICMS.zeraImpostoNoSimples then
                    begin
                      ICMS.vBC    := 0;
                      ICMS.vICMS  := 0;
                      ICMS.pICMS  := 0;
                      ICMS.pRedBC := 0;
                    end;

                  end;

                  F_ICMS.FTotvBC := F_ICMS.FTotvBC + ICMS.vBC;
                  F_ICMS.FTot    := F_ICMS.FTot + ICMS.vICMS;
                  F_ICMS.FTotVtotTrib := F_ICMS.FTotVtotTrib + vTotTrib;

                Except
                  on e:exception do
                  begin
                    strErros.Add('Erro ao preencher ICMS NORMAL. '+e.Message);
                  end;
                end

              end;


              {ICMS ST}
              F_ICMSST.pCodigoNF      := self.pCodigoNF;
              F_ICMSST.pCodigoProdnf  := F_PROD.FQuery.Fields[15].AsString;
              F_ICMSST.strErros       := self.strErros;
              F_ICMSST.denegada       := self.denegada;
              F_ICMSST.pCodigoProduto := Prod.cProd;

              if F_ICMSST.AbreQuery then
              begin

                if teveErros then Exit;

                try

                  //para sair com icmsST destacado deve ser utilizando cst 10,30 ou 70 e csosn: 201,202 ou 203

                  //� uma opera��o com imposto ST? ent�o vou fazer a valida��o para preenchimento ST
                  if F_ICMSST.FQuery.Fields[3].AsCurrency > 0 then
                  begin

                    //para destacar imposto ST no danfe as seguintes regras devem ser satisfeitas
                    //4-CST 2-CSOSN
                    if (Emit.CRT = crtSimplesNacional) then
                    begin

                      if (F_ICMS.FQuery.Fields[2].Text <> '201')  and (F_ICMS.FQuery.Fields[2].Text <> '202') and (F_ICMS.FQuery.Fields[2].Text <> '203') then
                        strErros.Add('Para destacar imposto ST com CSOSN deve ser usado as seguintes: 201, 202 ou 203');

                    end
                    else
                    begin

                      if (F_ICMSST.FQuery.Fields[4].Text <> '') and not (F_ICMSST.FQuery.Fields[4].Text = '10')  or (F_ICMSST.FQuery.Fields[4].Text = '30') or (F_ICMSST.FQuery.Fields[4].Text = '70') then
                        strErros.Add('Para destacar imposto ST com CST deve ser usado as seguintes: 10,30 ou 70')

                    end;

                  end;

                  if (F_ICMSST.FQuery.Fields[3].AsCurrency > 0) then
                  begin
                    ICMS.vICMSST  := F_ICMSST.FQuery.Fields[3].AsCurrency;
                    ICMS.vBCST    := F_ICMSST.FQuery.Fields[1].AsCurrency;
                    ICMS.pRedBCST := F_ICMSST.FQuery.Fields[0].AsCurrency;
                    ICMS.pICMSST  := F_ICMSST.FQuery.Fields[2].AsCurrency;
                    //icms.vBCSTRet   := 130.0;
                    //icms.vICMSSTRet := 10.0;
                  end;

                  {CEST}
                  //operacao com ST pode ser com destaque no danfe ou nao (zero)
                  {if (F_ICMS.operacaoCEST) or (ICMS.vICMSST > 0) then
                  begin

                    //pega no banco (cadastro de produtos)
                    Prod.CEST := F_PROD.FQuery.Fields[17].Text;

                    //caso nao tenha no banco vai procurar na tabela
                    if prod.CEST = '' then
                      prod.CEST := get_campoTabela('cest','ncm','tabcest',Prod.NCM);


                    //se o cadastro do produto estiver vazio e a tabela tamb�m nao tiver � gerado uma rejei��o informando o codigo do produto que esta sem o cest
                    if (Prod.CEST = '') then
                      strErros.Add('Informa��o CEST deve ser informada para opera��es ST. Produto: '+F_PROD.FQuery.Fields[1].Text+' - '+F_PROD.FQuery.Fields[2].Text);

                  end;}


                  F_ICMSST.FTot  := F_ICMSST.FTot + ICMS.vICMSST;
                  F_ICMSST.FTotvBC := F_ICMSST.FTotvBC + ICMS.vBCST;


                except
                  on e:exception do
                  begin
                    strErros.Add('Erro ao preencher ICMS ST. '+e.Message);
                  end;
                end

              end;

              {PIS}
              F_PIS.pCodigoNF := self.pCodigoNF;
              F_PIS.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_PIS.strErros := self.strErros;
              F_PIS.denegada := self.denegada;
              F_PIS.pCodigoProduto := Prod.cProd;

              if F_PIS.AbreQuery then
              begin
                if teveErros then Exit;

                if F_PIS.FQuery.Fields[1].AsCurrency > 0 then
                begin
                  PIS.vBC  := F_PIS.FQuery.Fields[0].AsCurrency;
                  PIS.vPIS := F_PIS.FQuery.Fields[2].AsCurrency;
                end
                else
                begin
                  PIS.vBC  := 0;
                  PIS.vPIS := 0;
                end;

                PIS.pPIS      := F_PIS.FQuery.Fields[1].AsCurrency;
                PIS.vAliqProd := F_PIS.FQuery.Fields[3].AsCurrency;
                PIS.qBCProd   := F_PIS.FQuery.Fields[4].AsCurrency;
                PIS.CST       := F_PIS.getCSTPIS;

                if Emit.CRT = crtSimplesNacional then
                begin

                  if F_PIS.zeraImpostoSimples then
                  begin
                    PIS.vBC       :=0;
                    PIS.vPIS      :=0;
                    PIS.pPIS      :=0;
                    PIS.vAliqProd :=0;
                    PIS.qBCProd   :=0;
                    PIS.CST       := pis99;
                  end;
                  
                end;

                F_PIS.FTot := F_PIS.FTot + PIS.vPIS;



              end;

              {COFINS}
              F_COFINS.pCodigoNF := self.pCodigoNF;
              F_COFINS.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_COFINS.strErros := self.strErros;
              F_COFINS.denegada := self.denegada;
              F_COFINS.pCodigoProduto := Prod.cProd;

              if F_COFINS.AbreQuery then
              begin

                if teveErros then Exit;

                if F_COFINS.FQuery.Fields[1].AsCurrency > 0 then
                begin
                  COFINS.vBC     := F_COFINS.FQuery.Fields[0].AsCurrency;
                  COFINS.vCOFINS := F_COFINS.FQuery.Fields[2].AsCurrency;
                end
                else
                begin
                  COFINS.vBC     := 0;
                  COFINS.vCOFINS := 0;
                end;

                COFINS.pCOFINS   := F_COFINS.FQuery.Fields[1].AsCurrency;
                COFINS.vAliqProd := F_COFINS.FQuery.Fields[3].AsCurrency;
                COFINS.qBCProd   := F_COFINS.FQuery.Fields[4].AsCurrency;
                COFINS.CST       := F_COFINS.getCSTCOFINS;

                if Emit.CRT = crtSimplesNacional then
                begin
                  if F_COFINS.zeraImpostoSimples then
                  begin
                    COFINS.vBC       :=0;
                    COFINS.vCOFINS   :=0;
                    COFINS.pCOFINS   :=0;
                    COFINS.vAliqProd :=0;
                    COFINS.qBCProd   :=0;
                    COFINS.CST       :=cof99;
                  end;
                end;

                F_COFINS.FTot := F_COFINS.FTot + COFINS.vCOFINS;

              end;


              {IPI}
              F_IPI.pCodigoNF := self.pCodigoNF;
              F_IPI.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_IPI.strErros := self.strErros;
              F_IPI.denegada := self.denegada;
              F_IPI.pCodigoProduto := Prod.cProd;

              if F_IPI.AbreQuery then
              begin

                if teveErros then Exit;

                if F_IPI.FQuery.Fields[2].AsCurrency > 0 then
                begin
                  IPI.CST  := F_IPI.getCSTIPI;
                  IPI.vBC  := F_IPI.FQuery.Fields[1].AsCurrency;
                  IPI.pIPI := F_IPI.FQuery.Fields[2].AsCurrency;
                  IPI.vIPI := F_IPI.FQuery.Fields[3].AsCurrency;
                end;

                if Emit.CRT = crtSimplesNacional then
                begin

                  if F_IPI.zeraImpostoSimples then
                  begin
                    IPI.vBC  := 0;
                    IPI.pIPI := 0;
                    IPI.vIPI := 0;
                  end;
                  
                end;

                F_IPI.FTot := F_IPI.FTot + IPI.vIPI;

              end;

              if not(self.nfeDevolucao) and ( UpperCase(UFEmit) <> UpperCase( UFDest ) ) then
              begin
                {Difal - Diferenca de Aliquota}
                F_DIFAL.pCodigoNF := self.pCodigoNF;
                F_DIFAL.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
                F_DIFAL.strErros := self.strErros;
                F_DIFAL.denegada := self.denegada;

                if F_DIFAL.AbreQuery then
                begin
                  ICMSUFDest.vBCUFDest := F_DIFAL.FQuery.Fields[0].AsCurrency;
                  ICMSUFDest.pICMSUFDest := F_DIFAL.FQuery.Fields[1].AsCurrency;
                  ICMSUFDest.vICMSUFDest := F_DIFAL.FQuery.Fields[2].AsCurrency;
                  ICMSUFDest.vICMSUFRemet := F_DIFAL.FQuery.Fields[3].AsCurrency;
                  ICMSUFDest.pICMSInter := F_DIFAL.FQuery.Fields[4].AsCurrency;
                  ICMSUFDest.pICMSInterPart := F_DIFAL.FQuery.Fields[5].AsCurrency;
                  ICMSUFDest.pFCPUFDest := F_DIFAL.FQuery.Fields[6].AsCurrency;
                  ICMSUFDest.vFCPUFDest := F_DIFAL.FQuery.Fields[7].AsCurrency;


                  F_DIFAL.FTotvFCPUFDest := F_DIFAL.FTotvFCPUFDest + ICMSUFDest.vFCPUFDest;
                  F_DIFAL.FTotvICMSUFDest := F_DIFAL.FTotvICMSUFDest + ICMSUFDest.vICMSUFDest;
                  F_DIFAL.FTotvICMSUFRemet := F_DIFAL.FTotvICMSUFRemet + ICMSUFDest.vICMSUFRemet;
                end;

              end;


            end;

          end;

          F_PROD.FQuery.Next;

        end;

        {TOTAIS}

        {ICMS}
        Total.ICMSTot.vICMS := F_ICMS.FTot;
        Total.ICMSTot.vBC   := F_ICMS.FTotvBC;

        {ICMS ST}
        Total.ICMSTot.vST   := F_ICMSST.FTot;
        total.ICMSTot.vBCST := F_ICMSST.FTotvBC;

        {PIS}
        Total.ICMSTot.vPIS := F_PIS.FTot;

        {COFINS}
        total.ICMSTot.vCOFINS := F_COFINS.FTot;

        {IPI}
        total.ICMSTot.vIPI := F_IPI.FTot;

        {por itens}
        total.ICMSTot.vSeg   := FTotSeg;
        total.ICMSTot.vFrete := FTotFret;
        total.ICMSTot.vOutro := FTotVoutr;
        total.ICMSTot.vProd  := FTotVprod;
        total.ICMSTot.vDesc  := FTotVDesc;
        total.ICMSTot.vII    := 0;

        if not(self.nfeDevolucao) and ( UpperCase(UFEmit) <> UpperCase( UFDest ) ) then
        begin
          Total.ICMSTot.vFCPUFDest   := F_DIFAL.FTotvFCPUFDest;
          Total.ICMSTot.vICMSUFDest  := F_DIFAL.FTotvICMSUFDest;
          Total.ICMSTot.vICMSUFRemet := F_DIFAL.FTotvICMSUFRemet;
        end;

        Total.ICMSTot.vTotTrib := F_ICMS.FTotVtotTrib;
        total.ICMSTot.vNF := total.ICMSTot.vProd - total.ICMSTot.vDesc + total.ICMSTot.vFrete + total.ICMSTot.vSeg + total.ICMSTot.vOutro + total.ICMSTot.vIPI + total.ICMSTot.vST;
      
      end;

    end;

  finally

    if Assigned(F_ICMS) then FreeAndNil(F_ICMS);
    if Assigned(F_ICMSST) then FreeAndNil(F_ICMSST);
    if Assigned(F_IPI) then FreeAndNil(F_IPI);
    if Assigned(F_PIS) then FreeAndNil(F_PIS);
    if Assigned(F_COFINS) then FreeAndNil(F_COFINS);
    if Assigned(F_PROD) then FreeAndNil(F_PROD);
    if Assigned(F_VEICTRANSP) then FreeAndNil(F_VEICTRANSP);
    if Assigned(F_TRANSPORTA) then FreeAndNil(F_TRANSPORTA);
    if Assigned(F_TRANSPORTE) then FreeAndNil(F_TRANSPORTE);
    if Assigned(F_ENDERDESTF) then FreeAndNil(F_ENDERDESTF);
    if Assigned(F_ENDERDESTC) then FreeAndNil(F_ENDERDESTC);
    if Assigned(F_DESTC) then FreeAndNil(F_DESTC);
    if Assigned(F_DESTF) then FreeAndNil(F_DESTF);
    if Assigned(F_ENDEREMIT) then FreeAndNil(F_ENDEREMIT);
    if Assigned(F_EMIT) then FreeAndNil(F_EMIT);
    if Assigned(F_INFADIC) then FreeAndNil(F_INFADIC);
    if Assigned(objsql) then FreeAndNil(objsql);
    if Assigned(F_VOL) then FreeAndNil(F_VOL);
    if Assigned(F_IDE) then FreeAndNil(F_IDE);
    if Assigned(F_COMPLEMENTAR) then FreeAndNil(F_COMPLEMENTAR);
    if Assigned(F_DEVOLUCAO) then FreeAndNil(F_DEVOLUCAO);
    if Assigned(F_CUPOMREFERENCIADO) then FreeAndNil(F_CUPOMREFERENCIADO);
    if Assigned(F_COBRFAT) then FreeAndNil(F_COBRFAT);
    if Assigned(F_COBRDUP) then FreeAndNil(F_COBRDUP);
    if Assigned(F_AutXML) then FreeAndNil(F_AutXML);
    if Assigned(F_DIFAL) then FreeAndNil(F_DIFAL);

  end;

end;

procedure TObjTransmiteNFE.preencheComponenteEventoNFCE;
var
  objsql:TobjSqlTxt;
  FTotSeg, FTotFret, FTotVoutr, FTotVprod, FTotVDesc:Currency;
  F_IDE:TComIDE;F_INFADIC:TComInfAdic;
  F_EMIT:TComEmit;F_ENDEREMIT:TComEnderEmit;
  F_DESTC:TComDestCNFCe;F_DESTF:TComDestF;
  F_ENDERDESTC:TComEnderDestCNFCe;F_ENDERDESTF:TComEnderDestF;
  F_PROD:TComProdutos; F_ICMS:TComICMS; F_ICMSST:TComICMSST;
  F_PIS:TComPIS;F_COFINS:TComCOFINS;F_IPI:TComIPI;
  F_COMPLEMENTAR:TComComplementar;F_DEVOLUCAO:TComDevolucao;
  F_CUPOMREFERENCIADO:TComCupomRef;F_COBRDUP:TComDup;F_COBRFAT:TComFat;
  F_FormasPagamento:TComFormaPag;
  i:Integer;
begin

  if not FileExists(ExtractFilePath(Application.ExeName)+self.arquivoSQL) then
  begin
    //ShowMessage('Verifique o arquivo: '+self.arquivoSQL);
    strErros.Add('Verifique o arquivo: '+self.arquivoSQL);
    Exit;
  end;

  objsql := TobjSqlTxt.Create(self,ExtractFilePath(Application.ExeName)+self.arquivoSQL);

  try

    //----------------------------------------------------------------------------
    F_IDE := TComIDE.Create(nil);
    F_IDE.FDatabase := self.FdataBase;
    F_IDE.FSQL := objsql.readString('IDE');
    F_IDE.Registro := 'Ide';

    F_INFADIC := TComInfAdic.Create(nil);
    F_INFADIC.FDatabase := self.FdataBase;
    F_INFADIC.FSQL := objsql.readString('INFADIC');
    F_INFADIC.Registro := 'Informa��es adicionais';

    F_EMIT := TComEmit.Create(nil);
    F_EMIT.FDatabase := self.FdataBase;
    F_EMIT.FSQL := objsql.readString('EMITENTE');
    F_EMIT.Registro := 'Emitente';

    F_ENDEREMIT := TComEnderEmit.Create(nil);
    F_ENDEREMIT.FDatabase := self.FdataBase;
    F_ENDEREMIT.FSQL := objsql.readString('ENDERECO EMITENTE');
    F_ENDEREMIT.Registro := 'Endere�o do emitente';

    F_DESTC := TComDestCNFCe.Create(nil);
    F_DESTC.FDatabase := self.FdataBase;
    F_DESTC.FSQL := objsql.readString('DESTINATARIO CLIENTE');
    F_DESTC.Registro := 'Destinat�rio cliente';

    F_DESTF := TComDestF.Create(nil);
    F_DESTF.FDatabase := self.FdataBase;
    F_DESTF.FSQL := objsql.readString('DESTINATARIO FORNECEDOR');
    F_DESTF.Registro := 'Destinat�rio fornecedor';

    F_ENDERDESTC := TComEnderDestCNFCe.Create(nil);  
    F_ENDERDESTC.FDatabase := self.FdataBase;
    F_ENDERDESTC.FSQL := objsql.readString('ENDERECO DESTINATARIO CLIENTE');
    F_ENDERDESTC.Registro := 'Endere�o destinat�rio cliente';

    F_ENDERDESTF := TComEnderDestF.Create(nil);
    F_ENDERDESTF.FDatabase := self.FdataBase;
    F_ENDERDESTF.FSQL := objsql.readString('ENDERECO DESTINATARIO FORNECEDOR');
    F_ENDERDESTF.Registro := 'Endere�o destinat�rio fornecedor';

    F_PROD := TComProdutos.Create(nil);
    F_PROD.FDatabase := self.FdataBase;
    F_PROD.FSQL := objsql.readString('PRODUTOS');
    F_PROD.Registro := 'Produtos';

    F_ICMS := TComICMS.Create(nil);
    F_ICMS.FDatabase := self.FdataBase;
    F_ICMS.FSQL := objsql.readString('ICMS NORMAL');
    F_ICMS.Registro := 'Icms normal';
    F_ICMS.arquivoSQL := self.arquivoSQL;

    F_ICMSST := TComICMSST.Create(nil);
    F_ICMSST.FDatabase := self.FdataBase;
    F_ICMSST.FSQL := objsql.readString('ICMS ST');
    F_ICMSST.arquivoSQL := self.arquivoSQL;

    F_PIS := TComPIS.Create(nil);
    F_PIS.FDatabase := self.FdataBase;
    F_PIS.FSQL := objsql.readString('PIS');
    F_PIS.arquivoSQL := self.arquivoSQL;

    F_COFINS := TComCOFINS.Create(nil);
    F_COFINS.FDatabase := self.FdataBase;
    F_COFINS.FSQL := objsql.readString('COFINS');
    F_COFINS.arquivoSQL := self.arquivoSQL;

    F_IPI := TComIPI.Create(nil);
    F_IPI.FDatabase := self.FdataBase;
    F_IPI.FSQL := objsql.readString('IPI');
    F_IPI.arquivoSQL := self.arquivoSQL;

    F_COBRFAT := TComFat.Create(nil);
    F_COBRFAT.FDatabase := self.FdataBase;
    F_COBRFAT.FSQL := objsql.readString('COBRANCA FATURA');

    F_COBRDUP := TComDup.Create(nil);
    F_COBRDUP.FDatabase := self.FdataBase;
    F_COBRDUP.FSQL := objsql.readString('COBRANCA DUPLICATA');

    F_COMPLEMENTAR := TComComplementar.Create(nil);
    F_COMPLEMENTAR.FDatabase := self.FdataBase;
    F_COMPLEMENTAR.chaveRef := self.chaveRef;

    F_DEVOLUCAO := TComDevolucao.Create(nil);
    F_DEVOLUCAO.FDatabase := self.FdataBase;
    F_DEVOLUCAO.cUF_ref := self.cUF_ref;
    F_DEVOLUCAO.AAMM_ref := self.AAMM_ref;
    F_DEVOLUCAO.CNPJ_ref:=self.CNPJ_ref;
    F_DEVOLUCAO.modeloRef:=self.modeloRef;
    F_DEVOLUCAO.serie_ref:=self.serie_ref;
    F_DEVOLUCAO.nNF_ref := self.nNF_ref;
    F_DEVOLUCAO.chaveRef := self.chaveRef;
    F_DEVOLUCAO.modeloRef := self.modeloRef;

    F_CUPOMREFERENCIADO := TComCupomRef.Create(nil);
    F_CUPOMREFERENCIADO.FDatabase := Self.FdataBase;
    F_CUPOMREFERENCIADO.nCOO := self.nCOO;
    F_CUPOMREFERENCIADO.nECF := self.nECF;
    F_CUPOMREFERENCIADO.modelo := self.modeloecf;

    F_FormasPagamento := TComFormaPag.Create(nil);
    F_FormasPagamento.FDatabase := self.FdataBase;
    F_FormasPagamento.FSQL := objsql.readString('FORMAS PAGAMENTO');


    //----------------------------------------------------------------------------

    F_IDE.pCodigoNF := self.pCodigoNF;
    F_IDE.strErros := self.strErros;
    F_IDE.denegada := self.denegada;
    F_IDE.numeroNFE := self.numeroNFE;

    F_IDE.AbreQuery;
    if teveErros then Exit;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    with FComponentesNfe.ACBrNFe.NotasFiscais.Add.NFe do
    begin


      {IDE}

      Ide.natOp   := F_IDE.FQuery.Fields[0].Text;
      Ide.serie   := F_IDE.FQuery.Fields[1].AsInteger;
      IDE.indPag  := F_IDE.getIndPag(F_IDE.FQuery.fields[2].text);
      Ide.dEmi    := F_IDE.FQuery.Fields[3].AsDateTime;
      ide.dSaiEnt := F_IDE.FQuery.Fields[4].AsDateTime;
      Ide.hSaiEnt := F_IDE.FQuery.Fields[5].AsDateTime;
      Ide.cUF     := getCampoTable('CODIGO UF EMITENTE').Fields[0].AsInteger;
      Ide.cMunFG  := getCampoTable('CODIGO CIDADE EMITENTE').Fields[0].AsInteger;
      Ide.cNF     := StrToInt(self.numeroNFE);
      Ide.nNF     := ide.cNF;
      ide.finNFe  := fnNormal;
      IDE.tpNF    := tnSaida;
      Ide.verProc   := '1.0.0.0'; //Vers�o do seu sistema
      Ide.tpImp     := tiNFCe;
      Ide.indFinal  := cfConsumidorFinal;
      Ide.indPres   := pcPresencial;
      Ide.tpAmb := taHomologacao;
      Ide.modelo := 65;
      Ide.serie := F_IDE.FQuery.Fields[1].AsInteger;



      {INFORMACOES ADICIONAIS}
      F_INFADIC.pCodigoNF := self.pCodigoNF;
      F_INFADIC.strErros := self.strErros;
      F_INFADIC.denegada := self.denegada;
      F_INFADIC.AbreQuery;
      if teveErros then Exit;
      InfAdic.infCpl := F_INFADIC.FQuery.Fields[0].Text + F_CUPOMREFERENCIADO.infComplementares;
      InfAdic.infAdFisco :=  '';


      {EMIT}
      F_EMIT.pCodigoNF := self.pCodigoNF;
      F_EMIT.strErros := self.strErros;
      F_EMIT.denegada := self.denegada;
      F_EMIT.AbreQuery;
      if teveErros then Exit;

      Emit.CNPJCPF := F_EMIT.FQuery.Fields[0].Text;
      Emit.IE      := F_EMIT.FQuery.Fields[2].Text;
      Emit.xNome   := F_EMIT.FQuery.Fields[1].Text;
      Emit.xFant   := F_EMIT.FQuery.Fields[4].Text;
      Emit.IEST := '';

      if self.CRT <> '' then
        emit.CRT := F_EMIT.getCRT(Self.CRT)
      else
        Emit.CRT := F_EMIT.getCRT(F_EMIT.FQuery.Fields[6].Text);

      {ENDER EMIT}
      F_ENDEREMIT.pCodigoNF := self.pCodigoNF;
      F_ENDEREMIT.strErros := self.strErros;
      F_ENDEREMIT.denegada := self.denegada;
      F_ENDEREMIT.AbreQuery;
      if teveErros then Exit;

      Emit.EnderEmit.fone    := F_ENDEREMIT.FQuery.Fields[0].Text;

      if (F_ENDEREMIT.FQuery.Fields[1].Text <> '') then
        Emit.EnderEmit.CEP     := StrToInt(F_ENDEREMIT.FQuery.Fields[1].Text);

      Emit.EnderEmit.xLgr    := F_ENDEREMIT.FQuery.Fields[2].Text;
      Emit.EnderEmit.nro     := F_ENDEREMIT.FQuery.Fields[3].Text;
      Emit.EnderEmit.xCpl    := F_ENDEREMIT.FQuery.Fields[4].Text;
      Emit.EnderEmit.xBairro := F_ENDEREMIT.FQuery.Fields[5].Text;
      Emit.EnderEmit.cMun    := StrToInt(F_ENDEREMIT.FQuery.Fields[6].Text);
      Emit.EnderEmit.xMun    := F_ENDEREMIT.FQuery.Fields[7].Text;
      Emit.EnderEmit.UF      := F_ENDEREMIT.FQuery.Fields[8].Text;
      Emit.EnderEmit.cPais   := StrToInt(F_ENDEREMIT.FQuery.Fields[9].Text);
      Emit.EnderEmit.xPais   := F_ENDEREMIT.FQuery.Fields[10].Text;

      {DEST}
      F_DESTC.pCodigoNF := self.pCodigoNF;
      F_DESTC.strErros := self.strErros;
      F_DESTC.denegada := self.denegada;

      if F_DESTC.AbreQuery then
      begin

        if teveErros then Exit;

        Dest.CNPJCPF := F_DESTC.FQuery.Fields[0].Text;
        dest.xNome   := F_DESTC.FQuery.Fields[2].Text;
        dest.Email   := F_DESTC.FQuery.Fields[6].Text;
        dest.ISUF := '';

        if Trim( F_DESTC.FQuery.Fields[1].Text ) <> '' then
        begin
          Dest.IE := F_DESTC.FQuery.Fields[1].Text;
          Dest.indIEDest := inContribuinte;
        end
        else
          Dest.indIEDest := inNaoContribuinte;


        {ENDERECO DESTINATARIO CLIENTE}
        F_ENDERDESTC.pCodigoNF := self.pCodigoNF;
        F_ENDERDESTC.strErros := self.strErros;
        F_ENDERDESTC.denegada := self.denegada;
        F_ENDERDESTC.AbreQuery;
        if teveErros then Exit;

        if (F_ENDERDESTC.FQuery.Fields[0].Text <> '') then
          dest.EnderDest.CEP     := StrToInt(F_ENDERDESTC.FQuery.Fields[0].Text);

        dest.EnderDest.cMun    := StrToInt(F_ENDERDESTC.FQuery.Fields[1].Text);
        dest.EnderDest.UF      := F_ENDERDESTC.FQuery.Fields[2].Text;
        dest.EnderDest.fone    := F_ENDERDESTC.FQuery.Fields[3].Text;
        dest.EnderDest.xBairro := F_ENDERDESTC.FQuery.Fields[4].Text;
        dest.EnderDest.cPais   := StrToInt(F_ENDERDESTC.FQuery.Fields[5].Text);
        dest.EnderDest.xPais   := F_ENDERDESTC.FQuery.Fields[6].Text;
        dest.EnderDest.xLgr    := F_ENDERDESTC.FQuery.Fields[7].Text;
        dest.EnderDest.xMun    := F_ENDERDESTC.FQuery.Fields[8].Text;

        if F_ENDERDESTC.FQuery.Fields[9].Text <> '' then
          dest.EnderDest.nro := F_ENDERDESTC.FQuery.Fields[9].Text;

      end
      else
      begin

        F_DESTF.pCodigoNF := self.pCodigoNF;
        F_DESTF.strErros := self.strErros;
        F_DESTF.denegada := self.denegada;

        if F_DESTF.AbreQuery then
        begin
          if teveErros then Exit;
          Dest.CNPJCPF := F_DESTF.FQuery.Fields[0].Text;
          dest.IE      := F_DESTF.FQuery.Fields[1].Text;
          Dest.xNome   := F_DESTF.FQuery.Fields[2].Text;
          DEST.Email   := F_DESTF.FQuery.Fields[4].Text;
        end;

        {ENDERECO DESTINATARIO FORNECEDOR}
        F_ENDERDESTF.pCodigoNF := self.pCodigoNF;
        F_ENDERDESTF.strErros := self.strErros;
        F_ENDERDESTF.denegada := self.denegada;
        F_ENDERDESTF.AbreQuery;
        if teveErros then Exit;

        if (F_ENDERDESTF.FQuery.Fields[0].Text <> '') then
          dest.EnderDest.CEP     := StrToInt(F_ENDERDESTF.FQuery.Fields[0].Text);

        dest.EnderDest.cMun    := StrToInt(F_ENDERDESTF.FQuery.Fields[1].Text);
        dest.EnderDest.UF      := F_ENDERDESTF.FQuery.Fields[2].Text;
        dest.EnderDest.fone    := F_ENDERDESTF.FQuery.Fields[3].Text;
        dest.EnderDest.xBairro := F_ENDERDESTF.FQuery.Fields[4].Text;
        dest.EnderDest.cPais   := StrToInt(F_ENDERDESTF.FQuery.Fields[5].Text);
        dest.EnderDest.xPais   := F_ENDERDESTF.FQuery.Fields[6].Text;
        dest.EnderDest.xLgr    := F_ENDERDESTF.FQuery.Fields[7].Text;
        dest.EnderDest.xMun    := F_ENDERDESTF.FQuery.Fields[8].Text;

        if F_ENDERDESTF.FQuery.Fields[9].Text <> '' then
          dest.EnderDest.nro := F_ENDERDESTF.FQuery.Fields[9].Text;
      
      end;

      {3.10}
      if (Emit.EnderEmit.UF = Dest.EnderDest.UF) then
        Ide.idDest := doInterna
      else
      if (Dest.EnderDest.UF = 'EX') then
        Ide.idDest := doExterior
      else
        Ide.idDest := doInterestadual;


      transp.modFrete := mfSemFrete;

      {
      infNFeSupl.qrCode := FComponentesNfe.ACBrNFe.GetURLQRCode( UFtoCUF( Emit.EnderEmit.UF ),
                                                                Ide.tpAmb,
                                                                StringReplace (FComponentesNfe.ACBrNFe.NotasFiscais.Items[0].NFe.infNFe.ID,'Nfe','',[rfIgnoreCase]),
                                                                Emit.xNome,
                                                                ide.dEmi,

                                                                )
      }

      {PRODUTOS}

      F_PROD.pCodigoNF := self.pCodigoNF;
      F_PROD.strErros := self.strErros;
      F_PROD.denegada := self.denegada;

      FTotSeg:=0;
      FTotFret:=0;
      FTotVoutr:=0;
      FTotVprod:=0;
      FTotVDesc:=0;

      if F_PROD.AbreQuery then
      begin

        if teveErros then Exit;

        while not (F_PROD.FQuery.Eof) do
        begin

          with Det.Add do
          begin

            Prod.nItem   := F_PROD.FQuery.RecNo;
            Prod.CFOP    := F_PROD.FQuery.Fields[0].Text;
            Prod.cProd   := F_PROD.FQuery.Fields[1].Text;
            Prod.xProd   := F_PROD.FQuery.Fields[2].Text;
            Prod.qCom    := F_PROD.FQuery.Fields[3].AsCurrency;
            Prod.qTrib   := F_PROD.FQuery.Fields[4].AsCurrency;
            Prod.uCom    := F_PROD.FQuery.Fields[5].Text;
            Prod.uTrib   := F_PROD.FQuery.Fields[6].Text;
            Prod.vUnCom  := F_PROD.FQuery.Fields[7].AsCurrency;
            Prod.vUnTrib := F_PROD.FQuery.Fields[8].AsCurrency;
            Prod.vProd   := F_PROD.FQuery.Fields[9].AsCurrency;
            Prod.vDesc   := F_PROD.FQuery.Fields[10].AsCurrency;
            Prod.NCM     := F_PROD.FQuery.Fields[11].Text;
            Prod.vFrete  := F_PROD.FQuery.Fields[12].AsCurrency;
            Prod.vSeg    := F_PROD.FQuery.Fields[13].AsCurrency;
            Prod.vOutro  := F_PROD.FQuery.Fields[14].AsCurrency;

            if self.operacaoCombustivel(prod.CFOP) then
            begin
              {Comb - Grupo de informa��es especificas para combust�veis}
              Prod.comb.cProdANP := F_PROD.FQuery.Fields[16].AsInteger;
              prod.comb.CODIF := '';
              prod.comb.qTemp := 0;
              prod.comb.UFcons := dest.EnderDest.UF;
            end;

            {totais prod}
            FTotSeg   := FTotSeg   + Prod.vSeg;
            FTotFret  := FTotFret  + Prod.vFrete;
            FTotVoutr := FTotVoutr + Prod.vOutro;
            FTotVprod := FTotVprod + Prod.vProd;
            FTotVDesc := FTotVDesc + Prod.vDesc;


            with Imposto do
            begin
            
              {ICMS NORMAL}
              F_ICMS.pCodigoNF := self.pCodigoNF;
              F_ICMS.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_ICMS.strErros := self.strErros;
              F_ICMS.denegada := self.denegada;
              F_ICMS.pCodigoProduto := Prod.cProd;
              
              if F_ICMS.AbreQuery then
              begin

                if teveErros then exit;
                ICMS.orig   := F_ICMS.getOrigICMS;
                ICMS.CST    := F_ICMS.getCSTICMS;
                ICMS.CSOSN  := F_ICMS.getCSOSN;
                ICMS.modBC  := F_ICMS.getModBCICMS;

                if Emit.CRT = crtSimplesNacional then
                begin

                  if ICMS.CSOSN = csosnvazio then
                  begin
                    strErros.Add('CSOSN vazio para o produto: '+prod.cProd);
                    Exit;
                  end;

                end
                else
                begin

                  if ICMS.CST = cstVazio then
                  begin
                    strErros.Add('CST vazio para o produto: '+prod.cProd);
                    Exit;
                  end;

                end;

                vTotTrib := StrToCurrDef(F_ICMS.FQuery.Fields[7].Text,0);
                

                if F_ICMS.FQuery.Fields[5].AsCurrency > 0 then {aliquota}
                begin
                  ICMS.vBC    := F_ICMS.FQuery.Fields[3].AsCurrency;
                  ICMS.vICMS  := F_ICMS.FQuery.Fields[4].AsCurrency;
                end
                else
                begin
                  ICMS.vBC   := 0;
                  ICMS.vICMS := 0;
                end;

                ICMS.pICMS  := F_ICMS.FQuery.Fields[5].AsCurrency;
                ICMS.pRedBC := F_ICMS.FQuery.Fields[6].AsCurrency;

                if (Emit.CRT = crtSimplesNacional) then
                begin

                  if F_ICMS.zeraImpostoNoSimples then
                  begin
                    ICMS.vBC    := 0;
                    ICMS.vICMS  := 0;
                    ICMS.pICMS  := 0;
                    ICMS.pRedBC := 0;
                  end;
                  
                end;

                F_ICMS.FTotvBC := F_ICMS.FTotvBC + ICMS.vBC;
                F_ICMS.FTot    := F_ICMS.FTot + ICMS.vICMS;
                F_ICMS.FTotVtotTrib := F_ICMS.FTotVtotTrib + vTotTrib;

              end;

              {ICMS ST}
              F_ICMSST.pCodigoNF := self.pCodigoNF;
              F_ICMSST.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_ICMSST.strErros := self.strErros;
              F_ICMSST.denegada := self.denegada;
              F_ICMSST.pCodigoProduto := Prod.cProd;

              if F_ICMSST.AbreQuery then
              begin

                if teveErros then Exit;

                if (F_ICMSST.FQuery.Fields[4].Text = '10')  or (F_ICMSST.FQuery.Fields[4].Text = '30') or (F_ICMSST.FQuery.Fields[4].Text = '70') then
                begin
                  ICMS.vICMSST  := F_ICMSST.FQuery.Fields[3].AsCurrency;
                  ICMS.vBCST    := F_ICMSST.FQuery.Fields[1].AsCurrency;
                  ICMS.pRedBCST := F_ICMSST.FQuery.Fields[0].AsCurrency;
                  ICMS.pICMSST  := F_ICMSST.FQuery.Fields[2].AsCurrency;
                end
                else
                begin
                  ICMS.vICMSST  := 0;
                  ICMS.vBCST    := 0;
                  ICMS.pRedBCST := 0;
                  ICMS.pICMSST  := 0;
                end;

                F_ICMSST.FTot  := F_ICMSST.FTot + ICMS.vICMSST;
                F_ICMSST.FTotvBC := F_ICMSST.FTotvBC + ICMS.vBCST;

              end;

              {PIS}
              F_PIS.pCodigoNF := self.pCodigoNF;
              F_PIS.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_PIS.strErros := self.strErros;
              F_PIS.denegada := self.denegada;
              F_PIS.pCodigoProduto := Prod.cProd;

              if F_PIS.AbreQuery then
              begin
                if teveErros then Exit;

                if F_PIS.FQuery.Fields[1].AsCurrency > 0 then
                begin
                  PIS.vBC  := F_PIS.FQuery.Fields[0].AsCurrency;
                  PIS.vPIS := F_PIS.FQuery.Fields[2].AsCurrency;
                end
                else
                begin
                  PIS.vBC  := 0;
                  PIS.vPIS := 0;
                end;

                PIS.pPIS      := F_PIS.FQuery.Fields[1].AsCurrency;
                PIS.vAliqProd := F_PIS.FQuery.Fields[3].AsCurrency;
                PIS.qBCProd   := F_PIS.FQuery.Fields[4].AsCurrency;
                PIS.CST       := F_PIS.getCSTPIS;

                if Emit.CRT = crtSimplesNacional then
                begin

                  if F_PIS.zeraImpostoSimples then
                  begin
                    PIS.vBC       :=0;
                    PIS.vPIS      :=0;
                    PIS.pPIS      :=0;
                    PIS.vAliqProd :=0;
                    PIS.qBCProd   :=0;
                    PIS.CST       := pis99;
                  end;
                  
                end;

                F_PIS.FTot := F_PIS.FTot + PIS.vPIS;



              end;

              {COFINS}
              F_COFINS.pCodigoNF := self.pCodigoNF;
              F_COFINS.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_COFINS.strErros := self.strErros;
              F_COFINS.denegada := self.denegada;
              F_COFINS.pCodigoProduto := Prod.cProd;

              if F_COFINS.AbreQuery then
              begin

                if teveErros then Exit;

                if F_COFINS.FQuery.Fields[1].AsCurrency > 0 then
                begin
                  COFINS.vBC     := F_COFINS.FQuery.Fields[0].AsCurrency;
                  COFINS.vCOFINS := F_COFINS.FQuery.Fields[2].AsCurrency;
                end
                else
                begin
                  COFINS.vBC     := 0;
                  COFINS.vCOFINS := 0;
                end;

                COFINS.pCOFINS   := F_COFINS.FQuery.Fields[1].AsCurrency;
                COFINS.vAliqProd := F_COFINS.FQuery.Fields[3].AsCurrency;
                COFINS.qBCProd   := F_COFINS.FQuery.Fields[4].AsCurrency;
                COFINS.CST       := F_COFINS.getCSTCOFINS;

                if Emit.CRT = crtSimplesNacional then
                begin
                  if F_COFINS.zeraImpostoSimples then
                  begin
                    COFINS.vBC       :=0;
                    COFINS.vCOFINS   :=0;
                    COFINS.pCOFINS   :=0;
                    COFINS.vAliqProd :=0;
                    COFINS.qBCProd   :=0;
                    COFINS.CST       :=cof99;
                  end;
                end;

                F_COFINS.FTot := F_COFINS.FTot + COFINS.vCOFINS;

              end;


              {IPI}
              F_IPI.pCodigoNF := self.pCodigoNF;
              F_IPI.pCodigoProdnf := F_PROD.FQuery.Fields[15].AsString;
              F_IPI.strErros := self.strErros;
              F_IPI.denegada := self.denegada;
              F_IPI.pCodigoProduto := Prod.cProd;

              if F_IPI.AbreQuery then
              begin

                if teveErros then Exit;

                if F_IPI.FQuery.Fields[2].AsCurrency > 0 then
                begin
                  IPI.CST  := F_IPI.getCSTIPI;
                  IPI.vBC  := F_IPI.FQuery.Fields[1].AsCurrency;
                  IPI.pIPI := F_IPI.FQuery.Fields[2].AsCurrency;
                  IPI.vIPI := F_IPI.FQuery.Fields[3].AsCurrency;
                end;

                if Emit.CRT = crtSimplesNacional then
                begin

                  if F_IPI.zeraImpostoSimples then
                  begin
                    IPI.vBC  := 0;
                    IPI.pIPI := 0;
                    IPI.vIPI := 0;
                  end;
                  
                end;

                F_IPI.FTot := F_IPI.FTot + IPI.vIPI;

              end;

            end;

          end;

          F_PROD.FQuery.Next;

        end;

        {TOTAIS}

        {ICMS}
        Total.ICMSTot.vICMS := F_ICMS.FTot;
        Total.ICMSTot.vBC   := F_ICMS.FTotvBC;

        {ICMS ST}
        Total.ICMSTot.vST   := F_ICMSST.FTot;
        total.ICMSTot.vBCST := F_ICMSST.FTotvBC;

        {PIS}
        Total.ICMSTot.vPIS := F_PIS.FTot;

        {COFINS}
        total.ICMSTot.vCOFINS := F_COFINS.FTot;

        {IPI}
        total.ICMSTot.vIPI := F_IPI.FTot;

        {por itens}
        total.ICMSTot.vSeg   := FTotSeg;
        total.ICMSTot.vFrete := FTotFret;
        total.ICMSTot.vOutro := FTotVoutr;
        total.ICMSTot.vProd  := FTotVprod;
        total.ICMSTot.vDesc  := FTotVDesc;
        total.ICMSTot.vII    := 0;

        //PAGAMENTOS apenas para NFC-e
        F_FormasPagamento.pCodigoNF := self.pCodigoNF;
        F_FormasPagamento.strErros := self.strErros;

        if F_FormasPagamento.AbreQuery then
        begin
          if teveErros then
            exit;
            
          while not (F_FormasPagamento.FQuery.Eof) do
          begin
            with pag.Add do
            begin
              case F_FormasPagamento.FQuery.Fields[1].AsInteger of
                1: tPag := fpDinheiro;
                2: tPag := fpCheque;
                3: tPag := fpCartaoCredito;
                4: tPag := fpCartaoDebito;
                5: tPag := fpCreditoLoja;
                10:tPag := fpValeAlimentacao;
                11:tPag := fpValeRefeicao;
                12:tPag := fpValePresente;
                13:tPag := fpValeCombustivel;
                99:tPag := fpOutro;
              end;

              vPag := F_FormasPagamento.FQuery.Fields[0].AsCurrency;
              
            end;

            F_FormasPagamento.FQuery.Next;
          end;

        end;

        Total.ICMSTot.vTotTrib := F_ICMS.FTotVtotTrib;
        total.ICMSTot.vNF := total.ICMSTot.vProd - total.ICMSTot.vDesc + total.ICMSTot.vFrete + total.ICMSTot.vSeg + total.ICMSTot.vOutro + total.ICMSTot.vIPI + total.ICMSTot.vST;
      
      end;

    end;

  finally

    if Assigned(F_ICMS) then FreeAndNil(F_ICMS);
    if Assigned(F_ICMSST) then FreeAndNil(F_ICMSST);
    if Assigned(F_IPI) then FreeAndNil(F_IPI);
    if Assigned(F_PIS) then FreeAndNil(F_PIS);
    if Assigned(F_COFINS) then FreeAndNil(F_COFINS);
    if Assigned(F_PROD) then FreeAndNil(F_PROD);
    if Assigned(F_ENDERDESTF) then FreeAndNil(F_ENDERDESTF);
    if Assigned(F_ENDERDESTC) then FreeAndNil(F_ENDERDESTC);
    if Assigned(F_DESTC) then FreeAndNil(F_DESTC);
    if Assigned(F_DESTF) then FreeAndNil(F_DESTF);
    if Assigned(F_ENDEREMIT) then FreeAndNil(F_ENDEREMIT);
    if Assigned(F_EMIT) then FreeAndNil(F_EMIT);
    if Assigned(F_INFADIC) then FreeAndNil(F_INFADIC);
    if Assigned(objsql) then FreeAndNil(objsql);
    if Assigned(F_IDE) then FreeAndNil(F_IDE);
    if Assigned(F_COMPLEMENTAR) then FreeAndNil(F_COMPLEMENTAR);
    if Assigned(F_DEVOLUCAO) then FreeAndNil(F_DEVOLUCAO);
    if Assigned(F_CUPOMREFERENCIADO) then FreeAndNil(F_CUPOMREFERENCIADO);
    if Assigned(F_COBRFAT) then FreeAndNil(F_COBRFAT);
    if Assigned(F_COBRDUP) then FreeAndNil(F_COBRDUP);
    if Assigned(F_FormasPagamento) then FreeAndNil(F_FormasPagamento);

  end;

end;

procedure TObjTransmiteNFE.retornaProtocolo;
var
  vChave,vProtocolo:string;
begin

  vChave := InputBox('Chave de acesso da NF-e','Chave de acesso: ','');

  if vChave <> '' then
  begin

    preparaFComponente;
    Self.configuraComponentes;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;
      
    FComponentesNfe.ACBrNFe.WebServices.Consulta.NFeChave := vChave;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;
    FComponentesNfe.ACBrNFe.WebServices.Consulta.Executar;

    InputBox('Retorno da consulta','Protocolo: ',UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Consulta.Protocolo));

  end;

end;

procedure TObjTransmiteNFE.retornaProtocoloNFCe;
var
  vChave,vProtocolo:string;
begin

  vChave := InputBox('Chave de acesso da NF-e','Chave de acesso: ','');

  if vChave <> '' then
  begin

    preparaFComponente;
    Self.configuraComponentes;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.WebServices.Consulta.NFeChave := vChave;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;
    FComponentesNfe.ACBrNFe.WebServices.Consulta.Executar;

    InputBox('Retorno da consulta','Protocolo: ',UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Consulta.Protocolo));

  end;

end;

function TObjTransmiteNFE.selecionaCertificado: string;
begin

  preparaFComponente;

  try
   //result := FComponentesNfe.ACBrNFe.Configuracoes.Certificados.SelecionarCertificado; celio0712
   result := FComponentesNfe.ACBrNFe.SSL.SelecionarCertificado;
  except
    Result := '';
  end;
  
end;

function TObjTransmiteNFE.getCampoSQL2(const sectionname: string): string;
begin
  result := Self.getCampoSQL(sectionname);
end;

procedure TObjTransmiteNFE.downloadXML(chaveAcesso:string);
begin

  preparaFComponente;
  Self.configuraComponentes;
  if self.strErros.Count > 0 then
  begin
    ShowMessage(self.strErros.Text);
    Exit;
  end;
    
  FComponentesNfe.ACBrNFe.DownloadNFe.Download.Chaves.Add.chNFe:=chaveAcesso;
  FComponentesNfe.ACBrNFe.DownloadNFe.Download.CNPJ := Copy(FComponentesNfe.ACBrNFe.DownloadNFe.Download.Chaves.Items[0].chNFe,7,14);
  FComponentesNfe.ACBrNFe.Download;

end;

function TObjTransmiteNFE.manifestacaoDestinatario(tpEvento:SmallInt;chaveAcesso,xJust:string):boolean;
var
  lMsg,idLote,dhEventoaux,path,mesAno:string;
  tipoEvento:TpcnTpEvento;
  str:TStringList;
begin

  Result := False;

  preparaFComponente;

  try

   if (chaveAcesso='') then
    chaveAcesso := InputBox('chNFE','Chave da NF-e: ','');

   if Trim(chaveAcesso) = '' then
    Exit;


        (*
      0 - confirma��o da operacao
      1 - Ci�ncia da operacao
      2 - Desconhecimento da operacao
      3 - Opera��o n�o realizada
    *)
    
    if (tpEvento = 3) then
    begin

      if (xJust = '') then
      begin
        ShowMessage('Para este evento � necessario informar a justificativa');
        Exit;
      end;

    end;

    case tpEvento of
      0:tipoEvento := teManifDestConfirmacao;
      1:tipoEvento := teManifDestCiencia;
      2:tipoEvento := teManifDestDesconhecimento;
      3:tipoEvento := teManifDestOperNaoRealizada;
    else ;
      ShowMessage('Tipo do evento inv�lido');
      Exit;
    end;

   dhEventoaux := DateTimeToStr(now);
   dhEventoaux := InputBox('Hora','Hora do cancelamento: ',dhEventoaux);

   FComponentesNfe.ACBrNFe.EventoNFe.Evento.Clear;

   Self.configuraComponentes;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

   with FComponentesNfe.ACBrNFe.EventoNFe.Evento.Add do
   begin

    infEvento.chNFe  := chaveAcesso;
    infEvento.CNPJ   := Copy(infEvento.chNFe,7,14);
    InfEvento.cOrgao := 91;
    infEvento.dhEvento := StrToDateTime(dhEventoaux);
    infEvento.tpEvento := tipoEvento;

    if (infEvento.tpEvento = teManifDestOperNaoRealizada) then
      InfEvento.detEvento.xJust := xJust;

   end;

   idLote := '1';

    mesAno := FormatDateTime('mm.yyyy',now);
    path := FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe+'..\..\MANIFESTO\'+mesAno+'\';

    if (not DirectoryExists(path)) then
      ForceDirectories(path);


    //FComponentesNfe.ACBrNFe.EnviarEventoNFe(StrToInt(IDLote)); celio0712
    FComponentesNfe.ACBrNFe.EnviarEvento(StrToInt(IDLote));
    with FComponentesNfe.AcbrNFe.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento do
    begin

      if cStat <> 135 then
      begin
        ShowMessage('cStat: '+IntToStr(cStat)+'. '+xMotivo);
        Exit;
      end
      else
      begin
        FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS);
        FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.EnvEvento.RetornoWS);
        FComponentesNfe.LoadXML(FComponentesNfe.WBResposta);
        FComponentesNfe.MemoResp.Lines.SaveToFile(path+'manifest - '+chaveAcesso+'.xml');
        ShowMessage(xMotivo);
      end;

    end;


  except

    on e:Exception do
    begin
      ShowMessage(e.Message);
      Exit;
    end;

  end;

  Result := True;
  
end;



function TObjTransmiteNFE.retornaCertificado: string;
begin
  result := getValueTable('CERTIFICADO');
end;

function TObjTransmiteNFE.consultaNotaProcessada(pXml: String): Integer;
var
  cstat: Integer;
begin

  result := 0;

  preparaFComponente;

  try

    Screen.Cursor := crHourGlass;

    if Trim( pXml ) = '' then
    begin
      MensagemAviso ('Arquivo XML vazio');
      Exit;
    end;

    Self.configuraComponentes;
    if self.strErros.Count > 0 then
    begin
      ShowMessage(self.strErros.Text);
      Exit;
    end;

    FComponentesNfe.ACBrNFe.NotasFiscais.Clear;
    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := True;

    { TODO -oCelio : Aqui usa os retornos do fcomponentesnfe }

    try
      //FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(pathXML);
      FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromString( pXml );
      FComponentesNfe.ACBrNFe.Consultar;

      cStat := FComponentesNfe.ACBrNFe.WebServices.consulta.cStat;    
      //cstat da consulta ou da nota??? post do user anfm do acbr http://www.forumweb.com.br/foruns/topic/79459-receber-o-numero-do-protocolo-pela-consulta/ 

      //verifica se o cstat � 100 gerado ou continua 105 em processamento
      //se for
      //100 - gerada
      //ou 205,302 - denegada
      //deve salvar o xml
      if (cstat=100) or (cstat=205) or (cstat=302) then
      begin

        //FComponentesNfe.ACBrNFe.NotasFiscais.SaveToFile(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe); celio0712
        FComponentesNfe.ACBrNFe.NotasFiscais.GravarXML(FComponentesNfe.ACBrNFe.Configuracoes.Arquivos.PathNFe);
        result := cstat;
        
      end;

      FComponentesNfe.MemoResp.Lines.Text := UTF8Encode(FComponentesNfe.ACBrNFe.WebServices.Consulta.RetWS);
      FcomponentesNfe.LoadXML(FcomponentesNfe.WBResposta);
      
    except
      on e:Exception do
      begin
        MensagemErro('Erro ao consultar situa��o. '+e.Message);
        Exit;
      end;
    end;

  finally
    Screen.Cursor := crDefault;
  end;

end;


procedure TObjTransmiteNFE.setend_Bairro(const Value: string);
begin
  Fend_Bairro := Value;
end;

procedure TObjTransmiteNFE.setend_Complemento(const Value: string);
begin
  Fend_Complemento := Value;
end;

procedure TObjTransmiteNFE.setend_Cpf_Cnpj(const Value: string);
begin
  Fend_Cpf_Cnpj := Value;
end;

procedure TObjTransmiteNFE.setend_Municipio(const Value: string);
begin
  Fend_Municipio := Value;
end;

procedure TObjTransmiteNFE.setend_Numero(const Value: string);
begin
  Fend_Numero := Value;
end;

procedure TObjTransmiteNFE.setend_Rua(const Value: string);
begin
  Fend_Rua := Value;
end;

procedure TObjTransmiteNFE.setend_UF(const Value: string);
begin
  Fend_UF := Value;
end;

procedure TObjTransmiteNFE.setend_Cod_Municipio(const Value: string);
begin
  Fend_Cod_Municipio := Value;
end;

procedure TObjTransmiteNFE.SetstrRefCupom(const Value: string);
begin
  FstrRefCupom := Value;
end;

procedure TObjTransmiteNFE.setFConsumidorFInal(const Value: Boolean);
begin
  FConsumidorFinal := Value;
end;

{ TComEmit }

function TComEmit.getCRT(crt:string): TpcnCRT;
begin

  if crt = '1' then
    result := crtSimplesNacional
  else
  if crt = '2' then
    result := crtSimplesExcessoReceita
  else
    result := crtRegimeNormal;
    
end;

procedure TComEmit.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  EMIT.CNPJCPF := FQuery.Fields[0].Text;
  EMIT.xNome   := FQuery.Fields[1].Text;
  EMIT.IE      := FQuery.Fields[2].Text;
  EMIT.IEST    := FQuery.Fields[3].Text;
  EMIT.xFant   := FQuery.Fields[4].Text;

  if self.CRT <> '' then
    EMIT.CRT := getCRT(Self.crt)
  else
    emit.CRT := getCRT(FQuery.Fields[6].text);
  
end;

procedure TComEmit.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CNPJ';
  FQuery.Fields[1].disPlayLabel := 'RAZAO SOCIAL';
  FQuery.Fields[2].disPlayLabel := 'IE';
  FQuery.Fields[3].disPlayLabel := 'IEST';
  FQuery.Fields[4].disPlayLabel := 'FANTASIA';
  FQuery.Fields[5].DisplayLabel := 'UF';
  FQuery.Fields[6].disPlayLabel := 'CRT';

  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[1].OnGetText := GetFieldTrim;
  FQuery.Fields[2].OnGetText := GetFieldSoNumeros;
  Fquery.Fields[3].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[4].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[5].OnGetText := GetFieldUpperCase;

  FQuery.Fields[3].Required := False;

end;

procedure TComEmit.setParametros;
begin
end;

function TComEmit.validaRegistros: Boolean;
begin
  if not ValidaCNPJ(FQuery.Fields[0].text) then
    strErros.Add('Campo: '+FQuery.Fields[0].DisplayLabel+' invalido');

  //if not validaInscricaoEstadual(FQuery.Fields[2].text,FQuery.Fields[5].Text) then
    //strErros.Add('Campo: '+FQuery.Fields[2].DisplayLabel+' invalido');


end;

{ TComEnderEmit }

procedure TComEnderEmit.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  ENDEREMIT.fone    := FQuery.Fields[0].Text;
  ENDEREMIT.CEP     := StrToInt(FQuery.Fields[1].Text);
  ENDEREMIT.xLgr    := FQuery.Fields[2].Text;
  ENDEREMIT.nro     := FQuery.Fields[3].Text;
  ENDEREMIT.xCpl    := FQuery.Fields[4].Text;
  ENDEREMIT.xBairro := FQuery.Fields[5].Text;
  ENDEREMIT.cMun    := FQuery.Fields[6].AsInteger;
  ENDEREMIT.xMun    := FQuery.Fields[7].Text;
  ENDEREMIT.UF      := FQuery.Fields[8].Text;
  ENDEREMIT.cPais   := FQuery.Fields[9].AsInteger;
  ENDEREMIT.xPais   := FQuery.Fields[10].Text;

end;

procedure TComEnderEmit.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Fone';
  FQuery.Fields[1].disPlayLabel := 'Cep';
  FQuery.Fields[2].disPlayLabel := 'Endere�o';
  FQuery.Fields[3].disPlayLabel := 'N�mero';
  FQuery.Fields[4].disPlayLabel := 'Complemento';
  FQuery.Fields[5].disPlayLabel := 'Bairro';
  FQuery.Fields[6].disPlayLabel := 'Codigo cidade';
  FQuery.Fields[7].disPlayLabel := 'Cidade';
  FQuery.Fields[8].disPlayLabel := 'Estado';
  FQuery.Fields[9].disPlayLabel := 'Codigo pais';
  FQuery.Fields[10].disPlayLabel := 'Pais';

  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldTrim;
  FQuery.Fields[4].OnGetText := GetFieldTrim;
  FQuery.Fields[5].OnGetText := GetFieldTrim;

  FQuery.Fields[0].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
end;

procedure TComEnderEmit.setParametros;
begin
  inherited;

end;

function TComEnderEmit.validaRegistros: Boolean;
begin

end;

{ TComDest }

procedure TComDestC.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  Dest.CNPJCPF := FQuery.Fields[0].Text;
  dest.IE      := FQuery.Fields[1].Text;
  dest.xNome   := FQuery.Fields[2].Text;

  if FQuery.Fields[1].Text <> '' then
    DEST.IE := FQuery.Fields[1].Text
  else
  begin
    if FQuery.Fields[5].Text = 'J' then
    begin
      if FQuery.Fields[4].Text <> '' then
        dest.IE := FQuery.Fields[4].Text
      else
        dest.IE := 'ISENTO';
    end
    else
      DEST.IE := 'ISENTO'

  end;

   dest.Email   := FQuery.Fields[6].Text;

end;

procedure TComDestC.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CPF CGC';
  FQuery.Fields[1].disPlayLabel := 'IE Produtor rural';
  FQuery.Fields[2].disPlayLabel := 'Nome';
  FQuery.Fields[3].disPlayLabel := 'UF';
  FQuery.Fields[4].disPlayLabel := 'RG IE';
  FQuery.Fields[5].disPlayLabel := 'Fisica/Juridica';
  FQuery.Fields[6].DisplayLabel := 'Email';

  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldUpperCase;
  FQuery.Fields[4].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[5].OnGetText := GetFieldUpperCase;
  FQuery.Fields[6].OnGetText := GetFieldTrim;

  //nfe para o exterior nao deve valida cpf/cnpj
  FQuery.Fields[0].Required := False;
  
  FQuery.Fields[1].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[6].Required := False;
end;

procedure TComDestC.setParametros;
begin
  inherited;
end;

function TComDestC.validaRegistros: Boolean;
begin

  if (FQuery.Fields[3].Text <> 'EX') then
  begin

    if not ValidaCPF(FQuery.Fields[0].Text) then
      if not ValidaCNPJ(FQuery.Fields[0].Text) then
        strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido');

  end;

  if( Trim( FQuery.Fields[4].Text ) <> '' ) and ( Length( FQuery.Fields[0].Text ) = 14 ) then
    if ( ValidaIE( FQuery.Fields[4].Text, FQuery.Fields[3].Text ) <> '' )then
      strErros.Add('Campo: '+fquery.Fields[4].DisplayLabel+' inv�lido');

  //validando tamanho maximo para campo Nome
  if Length(FQuery.Fields[2].AsString) > 60 then
    strErros.Add( 'Campo: '+FQuery.Fields[2].DisplayLabel+' ultrapassa o limite reservado (60)' );

end;

{ TComEnderDest }

procedure TComEnderDest.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  ENDERDEST.CEP     := StrToInt(FQuery.Fields[0].text);
  ENDERDEST.cMun    := FQuery.Fields[1].AsInteger;
  ENDERDEST.UF      := FQuery.Fields[2].Text;
  ENDERDEST.fone    := FQuery.Fields[3].Text;
  ENDERDEST.xBairro := FQuery.Fields[4].Text;
  ENDERDEST.cPais   := FQuery.Fields[5].AsInteger;
  ENDERDEST.xPais   := FQuery.Fields[6].Text;
  ENDERDEST.xLgr    := FQuery.Fields[7].Text;
  ENDERDEST.xMun    := FQuery.Fields[8].Text;

  if FQuery.Fields[9].Text <> '' then
    ENDERDEST.nro     := FQuery.Fields[9].Text;


end;

procedure TComEnderDest.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CEP';
  FQuery.Fields[1].disPlayLabel := 'C�digo cidade';
  FQuery.Fields[2].disPlayLabel := 'UF';
  FQuery.Fields[3].disPlayLabel := 'Fone';
  FQuery.Fields[4].disPlayLabel := 'Bairro';
  FQuery.Fields[5].disPlayLabel := 'C�digo pais';
  FQuery.Fields[6].disPlayLabel := 'Pais';
  FQuery.Fields[7].disPlayLabel := 'Endere�o';
  FQuery.Fields[8].DisplayLabel := 'Cidade';
  FQuery.Fields[9].DisplayLabel := 'N�mero';

  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[3].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldUpperCase;
  FQuery.Fields[4].OnGetText := GetFieldTrim;
  FQuery.Fields[6].OnGetText := GetFieldTrim;
  FQuery.Fields[7].OnGetText := GetFieldTrim;
  FQuery.Fields[8].OnGetText := GetFieldTrim;
  FQuery.Fields[9].OnGetText := GetFieldTrim;

  //cep pode estar vazio quando uf for exterior
  FQuery.Fields[0].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[9].Required := False;
end;

procedure TComEnderDest.setParametros;
begin
  inherited;
end;

function TComEnderDest.validaRegistros: Boolean;
begin
  inherited;
  //ShowMessage('passou');


end;

{ TComDestF }

procedure TComDestF.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  Dest.CNPJCPF := FQuery.Fields[0].Text;
  dest.IE      := FQuery.Fields[1].Text;
  Dest.xNome   := FQuery.Fields[2].Text;
  DEST.Email   := FQuery.Fields[4].Text;
  
end;

procedure TComDestF.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CGC';
  FQuery.Fields[1].disPlayLabel := 'IE';
  FQuery.Fields[2].disPlayLabel := 'Raz�o social';
  FQuery.Fields[3].disPlayLabel := 'UF';
  FQuery.Fields[4].disPlayLabel := 'Email';

  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldUpperCase;
  FQuery.Fields[4].OnGetText := GetFieldTrim;

  //CGC pode estar vazio em caso de opera��o com EX
  FQuery.Fields[0].Required := False;
  FQuery.Fields[1].Required := False;
  FQuery.Fields[4].Required := False;

end;

procedure TComDestF.setParametros;
begin
  inherited;
end;

function TComDestF.validaRegistros: Boolean;
begin

  if (FQuery.Fields[3].Text <> 'EX') then
  begin

    if not ValidaCPF(FQuery.Fields[0].Text) then
      if not ValidaCNPJ(FQuery.Fields[0].Text) then
        strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido');
        
  end;

  if( Trim( FQuery.Fields[1].Text ) <> '' ) and ( Length( FQuery.Fields[0].Text ) = 14 ) then
    if ( ValidaIE( FQuery.Fields[1].Text, FQuery.Fields[3].Text ) <> '' ) then
      strErros.Add('Campo: '+fquery.Fields[1].DisplayLabel+' inv�lido');

  //validando tamanho maximo para campo Nome
  if Length(FQuery.Fields[2].AsString) > 60 then
    strErros.Add( 'Campo: '+FQuery.Fields[2].DisplayLabel+' ultrapassa o limite reservado (60)' );
end;

{ TFonteDadosParamData }

procedure TFonteDadosParamData.SetParametros;
begin
  inherited;
  FQuery.Params[0].AsInteger := pCodigoNF;
end;

{ TComEnderDestC }

procedure TComEnderDestC.preenche;
begin
  inherited;
end;

procedure TComEnderDestC.setCampos;
begin
  inherited;
end;

procedure TComEnderDestC.setParametros;
begin
  inherited;
end;

function TComEnderDestC.validaRegistros: Boolean;
begin

   //uf
  if (FQuery.Fields[2].Text <> 'EX') then
    if not IsNumeric(FQuery.Fields[0].Text) then
      strErros.Add('Campo: '+FQuery.Fields[0].DisplayLabel+' com valor invalido. Registro: '+self.FRegistro);

  //xLogr
  if Length(FQuery.Fields[7].AsString) > 60 then
    strErros.Add( 'Campo: '+FQuery.Fields[7].DisplayLabel+' ultrapassa o limite reservado' );

  if Length(FQuery.Fields[3].AsString) > 14 then
    strErros.Add( 'Campo: '+FQuery.Fields[3].DisplayLabel+' ultrapassa o limite reservado' );



end;

{ TComEnderDestF }

procedure TComEnderDestF.preenche;
begin
  inherited;
end;

procedure TComEnderDestF.setCampos;
begin
  inherited;
end;

procedure TComEnderDestF.setParametros;
begin
  inherited;
end;

function TComEnderDestF.validaRegistros: Boolean;
begin

   //uf
  if (FQuery.Fields[2].Text <> 'EX') then
    if not IsNumeric(FQuery.Fields[0].Text) then
      strErros.Add('Campo: '+FQuery.Fields[0].DisplayLabel+' com valor inv�lido. Registro: '+self.FRegistro);

  //xLogr
  if Length(FQuery.Fields[7].AsString) > 60 then
    strErros.Add( 'Campo: '+FQuery.Fields[7].DisplayLabel+' ultrapassa o limite reservado' );

  if Length(FQuery.Fields[3].AsString) > 14 then
    strErros.Add( 'Campo: '+FQuery.Fields[3].DisplayLabel+' ultrapassa o limite reservado' );

end;

{ TComTransp }

function TComTransp.getModFrete: TpcnModalidadeFrete;
begin
  if FQuery.Fields[0].Text = '1' then
    result := mfContaEmitente
  else
  if FQuery.Fields[0].Text = '2' then
    Result := mfContaDestinatario
  else
    Result := mfSemFrete;
end;

procedure TComTransp.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  Transp.modFrete := getModFrete();

end;

procedure TComTransp.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Modalidade de frete';
  FQuery.Fields[0].Required := false;
end;

procedure TComTransp.setParametros;
begin
  inherited;
end;

function TComTransp.validaRegistros: Boolean;
begin
  if FQuery.Fields[0].Text <> '' then
    if (FQuery.Fields[0].Text <> '0') and (FQuery.Fields[0].Text <> '1') and (FQuery.Fields[0].Text <> '2') and (FQuery.Fields[0].Text <> '9') then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' com valor inv�lido. Valor do campo: '+fquery.Fields[0].Text);
end;

{ TComTransporta }

procedure TComTransporta.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  if FQuery.Fields[5].Text <> '' then
  begin
    Transporta.CNPJCPF := FQuery.Fields[0].Text;
    Transporta.IE      := FQuery.Fields[1].Text;
    Transporta.xMun    := FQuery.Fields[2].Text;
    Transporta.UF      := FQuery.Fields[3].Text;
    Transporta.xEnder  := FQuery.Fields[4].Text;
    Transporta.xNome   := FQuery.Fields[5].Text;
  end;

end;

procedure TComTransporta.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CPF CNPJ';
  FQuery.Fields[1].disPlayLabel := 'IE';
  FQuery.Fields[2].disPlayLabel := 'Municipio';
  FQuery.Fields[3].disPlayLabel := 'UF';
  FQuery.Fields[4].disPlayLabel := 'Endere�o';
  FQuery.Fields[5].disPlayLabel := 'Nome';

  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldUpperCase;
  FQuery.Fields[4].OnGetText := GetFieldTrim;
  FQuery.Fields[5].OnGetText := GetFieldTrim;

  FQuery.Fields[0].Required := False;
  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;

end;

procedure TComTransporta.setParametros;
begin
  inherited;
end;

function TComTransporta.validaRegistros: Boolean;
begin
  if not ValidaCNPJ(FQuery.Fields[0].text) then
    if not ValidaCPF(FQuery.Fields[0].text) then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido. Registro: '+self.Registro);
end;

{ TComveicTransp }

procedure TComveicTransp.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  veicTransp.placa := FQuery.Fields[0].Text;
  veicTransp.UF    := FQuery.Fields[1].Text;
end;

procedure TComveicTransp.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Placa';
  FQuery.Fields[1].disPlayLabel := 'UF';

  FQuery.Fields[0].OnGetText := GetFieldUpperCase;
  FQuery.Fields[1].OnGetText := GetFieldUpperCase;

  FQuery.Fields[0].Required := False;
  FQuery.Fields[1].Required := False;
end;

procedure TComveicTransp.setParametros;
begin
  inherited;
end;

function TComveicTransp.validaRegistros: Boolean;
begin
  if tira_caracter(FQuery.Fields[0].Text,'-') <> '' then
  begin
    if Length(tira_caracter(FQuery.Fields[0].Text,'-')) <> 7 then
      strErros.Add('Campo: '+FQuery.Fields[0].DisplayLabel+' requer 7 caracteres. Valor do campo: '+FQuery.Fields[0].Text);
  end;
end;

{ TComVol }

procedure TComVol.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  if (FQuery.Fields[0].Text <> '') and (FQuery.Fields[0].Text <> '0') then
  begin

    vol.Add;

    if FQuery.Fields[0].Text <> '' then
      vol.Items[0].qVol := FQuery.Fields[0].AsInteger;

    vol.Items[0].esp    := FQuery.Fields[1].Text;
    vol.Items[0].marca  := FQuery.Fields[2].Text;
    vol.Items[0].nVol   := FQuery.Fields[3].Text;
    vol.Items[0].pesoB  := FQuery.Fields[4].AsCurrency;
    vol.Items[0].pesoL  := FQuery.Fields[5].AsCurrency;

  end;

end;

procedure TComVol.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Quantidade volume';
  FQuery.Fields[1].disPlayLabel := 'Especie';
  FQuery.Fields[2].disPlayLabel := 'Marca';
  FQuery.Fields[3].disPlayLabel := 'Numera��o';
  FQuery.Fields[4].disPlayLabel := 'Peso bruto';
  FQuery.Fields[5].disPlayLabel := 'Peso liquido';

  FQuery.Fields[0].Required := false;
  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[5].Required := False;
end;

procedure TComVol.setParametros;
begin
  inherited;
end;

function TComVol.validaRegistros: Boolean;
begin

end;

{ TComProdutos }

procedure TComProdutos.preenche;
var
  i:integer;
begin

  inherited;

  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin

    Det.Add;
    Det.Items[FQuery.RecNo-1].Prod.nItem   := FQuery.RecNo;
    Det.Items[FQuery.RecNo-1].Prod.CFOP    := FQuery.Fields[0].Text;
    Det.Items[FQuery.RecNo-1].Prod.cProd   := FQuery.Fields[1].Text;
    Det.Items[FQuery.RecNo-1].Prod.xProd   := FQuery.Fields[2].Text;
    Det.Items[FQuery.RecNo-1].Prod.qCom    := FQuery.Fields[3].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.qTrib   := FQuery.Fields[4].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.uCom    := FQuery.Fields[5].Text;
    Det.Items[FQuery.RecNo-1].Prod.uTrib   := FQuery.Fields[6].Text;
    Det.Items[FQuery.RecNo-1].Prod.vUnCom  := FQuery.Fields[7].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.vUnTrib := FQuery.Fields[8].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.vProd   := FQuery.Fields[9].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.vDesc   := FQuery.Fields[10].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.NCM     := FQuery.Fields[11].Text;
    Det.Items[FQuery.RecNo-1].Prod.vFrete  := FQuery.Fields[12].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.vSeg    := FQuery.Fields[13].AsCurrency;
    Det.Items[FQuery.RecNo-1].Prod.vOutro  := FQuery.Fields[14].AsCurrency;

    if self.operacaoCombustivel(FQuery.Fields[0].Text) then
    begin
      det.Items[FQuery.RecNo-1].Prod.comb.cProdANP := FQuery.Fields[16].AsInteger;
      det.Items[FQuery.RecNo-1].Prod.comb.CODIF := '';
      det.Items[FQuery.RecNo-1].Prod.comb.qTemp := 0;
      det.Items[FQuery.RecNo-1].Prod.comb.UFcons := 'MS';
    end;

    FTotSeg   := FTotSeg   + Det.Items[FQuery.RecNo-1].Prod.vSeg;
    FTotFret  := FTotFret  + Det.Items[FQuery.RecNo-1].Prod.vFrete;
    FTotVoutr := FTotVoutr + Det.Items[FQuery.RecNo-1].Prod.vOutro;
    FTotVprod := FTotVprod + Det.Items[FQuery.RecNo-1].Prod.vProd;
    FTotVDesc := FTotVDesc + Det.Items[FQuery.RecNo-1].Prod.vDesc;

    for i:=0 to self.ComponentCount-1 do
    begin
      if self.Components[i] is TComImposto then
      begin
        TComImposto(self.Components[i]).ind := FQuery.RecNo-1;
        TComImposto(self.Components[i]).pCodigoProdnf  := FQuery.Fields[15].AsString;
        TComImposto(self.Components[i]).preenche;
      end
    end;

    FQuery.Next;
    
  end;

  self.preencheTot;

end;


procedure TComProdutos.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CFOP';
  FQuery.Fields[1].disPlayLabel := 'C�digo Produto';
  FQuery.Fields[2].disPlayLabel := 'Descri��o';
  FQuery.Fields[3].disPlayLabel := 'Quantidade';
  FQuery.Fields[4].disPlayLabel := 'Quantidade tributada';
  FQuery.Fields[5].disPlayLabel := 'Unidade';
  FQuery.Fields[6].disPlayLabel := 'Unidade tributa��o';
  FQuery.Fields[7].disPlayLabel := 'Valor';
  FQuery.Fields[8].disPlayLabel := 'Valor unitario de tributa��o';
  FQuery.Fields[9].disPlayLabel := 'Valor final';
  FQuery.Fields[10].disPlayLabel := 'Desconto';
  FQuery.Fields[11].disPlayLabel := 'NCM';
  FQuery.Fields[12].disPlayLabel := 'Valor Frete';
  FQuery.Fields[13].disPlayLabel := 'Valor Seguro';
  FQuery.Fields[14].disPlayLabel := 'Valor Outros';
  FQuery.Fields[15].DisplayLabel := 'Codigo prodNF';
  FQuery.Fields[16].DisplayLabel := 'Codigo ANP';
  FQuery.Fields[17].DisplayLabel := 'Cest';

  FQuery.Fields[5].OnGetText := GetFieldUpperCase;
  FQuery.Fields[6].OnGetText := GetFieldUpperCase;
  FQuery.Fields[11].OnGetText := GetFieldSoNumeros;

  FQuery.Fields[4].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[10].Required := False;
  FQuery.Fields[11].Required := False; //esta validando no validaRegistros
  FQuery.Fields[12].Required := False;
  FQuery.Fields[13].Required := False;
  FQuery.Fields[14].Required := False;
  FQuery.Fields[15].Required := False;
  FQuery.Fields[16].Required := False;
  FQuery.Fields[17].Required := False;

end;

function TComProdutos.validaRegistros: Boolean;
begin

  if tipoNF = 'SAIDA' then
  begin

    if (FQuery.Fields[0].Text[1] <> '5') and (FQuery.Fields[0].Text[1] <> '6') and (FQuery.Fields[0].Text[1] <> '7')  then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido para nfe de saida. Valor do campo: '+FQuery.Fields[0].Text);

  end
  else if tipoNF = 'ENTRADA' then
  begin

    if (FQuery.Fields[0].Text[1] <> '1') and (FQuery.Fields[0].Text[1] <> '2') and (FQuery.Fields[0].Text[1] <> '3')  then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido para nf-e de entrada. Valor do campo: '+FQuery.Fields[0].Text);

  end;

  if (Length(FQuery.Fields[11].Text) <> 8) then
    strErros.Add('Campo: '+FQuery.fields[11].DisplayLabel+' deve conter 8 digitos. Produto: '+fquery.Fields[1].Text);

  if self.operacaoCombustivel(FQuery.Fields[0].Text) then
    if FQuery.Fields[16].Text = '' then
      strErros.Add('Para opera��o com combust�vel ou lubrificante � necessario preencher o campo: C�digo ANP. Produto: '+fquery.Fields[1].Text);

  //if ( (Fquery.Fields[3].AsCurrency * FQuery.Fields[7].AsCurrency) <> FQuery.Fields[9].AsCurrency ) then
  //  strErros.Add('Valor unitario * quantidade difere do valor total. Produto: '+FQuery.Fields[1].Text);

end;

procedure TComProdutos.setParametros;
begin
  inherited;
end;

procedure TComProdutos.inicializa;
var
  i:integer;
begin

  FTotSeg   := 0;
  FTotFret  := 0;
  FTotVoutr := 0;
  FTotVprod := 0;
  FTotVNF   := 0;
  FTotVDesc := 0;

  FICMS := TComICMS.Create(self);
  FICMS.Registro := 'Imposto ICMS';

  FICMSST := TComICMSST.Create(self);
  FICMS.Registro := 'Imposto ICMS ST';

  FPIS := TComPIS.Create(self);
  FPIS.Registro := 'Imposto PIS';

  FCOFINS := TComCOFINS.Create(self);
  FCOFINS.Registro := 'Imposto COFINS';

  FIPI := TComIPI.Create(self);
  FIPI.Registro := 'Imposto IPI';

  for i :=0  to self.ComponentCount-1  do
  begin

    if self.Components[i] is TFonteDados then
    begin
      TFonteDados(self.Components[i]).FDatabase := self.FdataBase;
      TFonteDados(self.Components[i]).strErros  := self.strErros;
      TFonteDados(self.Components[i]).denegada  := self.denegada;
      TFonteDados(self.Components[i]).pCodigoNF := self.pCodigoNF;
      TFonteDados(self.Components[i]).tipoNF    := self.TipoNF;
      TFonteDados(self.Components[i]).CRT       := self.CRT;
    end;

    if self.Components[i] is TComDet then
      TComDet(self.Components[i]).Det := self.Det;

    if self.Components[i] is TComImposto then
      TComImposto(self.Components[i]).inicializa;

    if self.Components[i] is TComICMS then
      TComICMS(self.Components[i]).inicializa;

    if self.Components[i] is TComICMSST then
      TComICMSST(self.Components[i]).inicializa;

  end

end;

constructor TComProdutos.create(AOwner: TComponent);
begin
  inherited;
end;

destructor TComProdutos.destroy;
begin
  FreeAndNil(FICMS);
  FreeAndNil(FICMSST);
  FreeAndNil(FPIS);
  FreeAndNil(FCOFINS);
  FreeAndNil(FIPI);
  inherited;
end;

procedure TComProdutos.preencheTot;
var
  i:Integer;
begin

  for i := 0  to self.ComponentCount-1  do
  begin
    if self.Components[i] is TComICMS then
    begin
      total.ICMSTot.vICMS := TComICMS(self.Components[i]).FTot;
      total.ICMSTot.vBC   := TComICMS(self.Components[i]).FTotvBC;
    end
    else
    if self.Components[i] is TComICMSST then
    begin
      total.ICMSTot.vST   := TComICMSST(self.Components[i]).FTot;
      total.ICMSTot.vBCST := TComICMSST(self.Components[i]).FTotvBC;
    end
    else
    if self.Components[i] is TComPIS then
      total.ICMSTot.vPIS := TComPIS(self.Components[i]).FTot
    else
    if self.Components[i] is TComCOFINS then
      total.ICMSTot.vCOFINS := TComCOFINS(self.Components[i]).FTot
    else
    if self.Components[i] is TComIPI then
      total.ICMSTot.vIPI := TComIPI(self.Components[i]).FTot;
  end;

  total.ICMSTot.vSeg   := FTotSeg;
  total.ICMSTot.vFrete := FTotFret;
  total.ICMSTot.vOutro := FTotVoutr;
  total.ICMSTot.vProd  := FTotVprod;
  total.ICMSTot.vDesc  := FTotVDesc;
  total.ICMSTot.vII    := 0;

  total.ICMSTot.vNF := total.ICMSTot.vProd - total.ICMSTot.vDesc + total.ICMSTot.vFrete + total.ICMSTot.vSeg + total.ICMSTot.vOutro + total.ICMSTot.vIPI + total.ICMSTot.vST;

end;

{ TComICMS }

function TComICMS.getCSOSN: TpcnCSOSNIcms;
begin

  if FQuery.Fields[2].Text = '' then
  begin
    result := csosnVazio;
    Exit;
  end;

  case StrToIntDef(FQuery.Fields[2].Text,-1) of
    101:result := csosn101;
    102:result := csosn102;
    103:Result := csosn103;
    201:Result := csosn201;
    202:Result := csosn202;
    203:Result := csosn203;
    300:Result := csosn300;
    400:Result := csosn400;
    500:Result := csosn500;
    900:Result := csosn900;
  else
    strErros.Add('Campo: '+FQuery.Fields[2].DisplayLabel+' inv�lido. Valor do campo: '+fquery.Fields[2].Text);
  end;

end;

function TComICMS.getCSTICMS: TpcnCSTIcms;
begin

  if fquery.Fields[1].Text = '' then
  begin
    Result := cstVazio;
    Exit;
  end;

  case StrToIntDef (FQuery.Fields[1].Text,-1) of

    0:  result := cst00;
    10: result := cst10;
    20: result := cst20;
    30: result := cst30;
    40: result := cst40;
    41: result := cst41;
    50: result := cst50;
    51: result := cst51;
    60: result := cst60;
    70: result := cst70;
    90: result := cst90;

  else
    begin
      strErros.Add('Campo: '+FQuery.Fields[1].DisplayLabel+' invalido. Valor do campo: '+fquery.Fields[1].text);
      exit;
    end
  end;
end;

function TComICMS.operacaoCEST: Boolean;
var
  cst,csosn:integer;
begin

  cst   := StrToIntDef (FQuery.Fields[1].Text,-1);
  csosn := StrToIntDef (FQuery.Fields[2].Text,-1);

  result := (cst=10)    or (cst=30)    or (cst=60) or (cst=70) or (cst=90) or
            (csosn=201) or (csosn=202) or (csosn=203) or (csosn=500);

  //se 900 - Outros, e conter valor de ICMSST tamb�m precisa do cest mais nao estou validando aqui
  //valido de fora porque precisa da informa��o do valor do icmsST que esta em outro ponto do codigo


end;

function TComICMS.getModBCICMS: TpcnDeterminacaoBaseIcms;
begin
  result := dbiValorOperacao;
end;

function TComICMS.getOrigICMS: TpcnOrigemMercadoria;
var
  org:Integer;
begin

  (*if (FQuery.Fields[0].Text = '0') then
    Result := oeNacional
  else
  if (FQuery.Fields[0].Text = '1') then
    Result := oeEstrangeiraImportacaoDireta
  else
  if (FQuery.Fields[0].Text = '2') then
    Result := oeEstrangeiraAdquiridaBrasil
  else
    strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido. Valor do campo: '+fquery.Fields[0].text);*)


  org := StrToIntDef(FQuery.Fields[0].Text,-1);

  case (org) of

    0:Result := oeNacional;
    1:Result := oeEstrangeiraImportacaoDireta;
    2:Result := oeEstrangeiraAdquiridaBrasil;
    3:Result := oeNacionalConteudoImportacaoSuperior40;
    4:Result := oeNacionalProcessosBasicos;
    5:Result := oeNacionalConteudoImportacaoInferiorIgual40;
    6:Result := oeEstrangeiraImportacaoDiretaSemSimilar;
    7:Result := oeEstrangeiraAdquiridaBrasilSemSimilar;
    8:Result := oeNacionalConteudoImportacaoSuperior70;

  else;
    strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido. Valor do campo: '+fquery.Fields[0].text);
  end;


end;

procedure TComICMS.inicializa;
begin
  FTotvBC := 0;
  FTotVtotTrib := 0;
end;



procedure TComICMS.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  Det.Items[ind].Imposto.ICMS.orig   := getOrigICMS;
  Det.Items[ind].Imposto.ICMS.CST    := getCSTICMS;
  Det.Items[ind].Imposto.ICMS.CSOSN  := getCSOSN;
  Det.Items[ind].Imposto.ICMS.modBC  := getModBCICMS;

  if FQuery.Fields[5].AsCurrency > 0 then {aliquota}
  begin
    Det.Items[ind].Imposto.ICMS.vBC    := fquery.Fields[3].AsCurrency;
    Det.Items[ind].Imposto.ICMS.vICMS  := FQuery.Fields[4].AsCurrency;
  end
  else
  begin
    Det.Items[ind].Imposto.ICMS.vBC   := 0;
    Det.Items[ind].Imposto.ICMS.vICMS := 0;
  end;

  Det.Items[ind].Imposto.ICMS.pICMS  := FQuery.Fields[5].AsCurrency;
  Det.Items[ind].Imposto.ICMS.pRedBC := FQuery.Fields[6].AsCurrency;

  if self.zeraImpostoNoSimples then
  begin
    Det.Items[ind].Imposto.ICMS.vBC    := 0;
    Det.Items[ind].Imposto.ICMS.vICMS  := 0;
    Det.Items[ind].Imposto.ICMS.pICMS  := 0;
    Det.Items[ind].Imposto.ICMS.pRedBC := 0;
  end;

  FTot    := FTot + Det.Items[ind].Imposto.ICMS.vICMS;
  FTotvBC := FTotvBC + Det.Items[ind].Imposto.ICMS.vBC;

end;

procedure TComICMS.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Origem';
  FQuery.Fields[1].disPlayLabel := 'CST';
  FQuery.Fields[2].disPlayLabel := 'CSOSN';
  FQuery.Fields[3].disPlayLabel := 'Base de calculo ICMS';
  FQuery.Fields[4].disPlayLabel := 'Valor ICMS';
  FQuery.Fields[5].disPlayLabel := 'Aliquota';
  FQuery.Fields[6].disPlayLabel := 'Redu��o BC ICMS';
  FQuery.Fields[7].disPlayLabel := 'Valor Total Tributos';

  FQuery.Fields[7].OnGetText  := GetFieldTrunca;

  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[5].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;
end;

procedure TComICMS.setParametros;
begin
  inherited;
end;

function TComICMS.validaRegistros: Boolean;
begin

  if FQuery.Fields[5].AsCurrency > 0 then
  begin

    if FQuery.Fields[3].AsCurrency <= 0 then
      strErros.Add('Campo: '+fquery.Fields[3].DisplayLabel+' deve ser maior que zero no produto '+self.pCodigoProduto);

    if FQuery.Fields[4].AsCurrency <= 0 then
      strErros.Add('Campo: '+fquery.Fields[4].DisplayLabel+' deve ser maior que zero no produto '+self.pCodigoProduto);

  end;

  if (FQuery.Fields[1].Text = '') and (FQuery.Fields[2].Text = '') then
    strErros.Add('Empresa no simples cadastra CSOSN e empresas fora do simples utilizam CST. Produto '+self.pCodigoProduto);

end;

function TComICMS.zeraImpostoNoSimples: Boolean;
begin
  result := StrToBool(getValueTable('ZERA IMPOSTO NO SIMPLES'));
end;

{ TComICMSST }

procedure TComICMSST.inicializa;
begin
  self.FTotvBC := 0;
end;

procedure TComICMSST.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  if (FQuery.Fields[4].Text = '10')  or (FQuery.Fields[4].Text = '30') or (FQuery.Fields[4].Text = '70') then
  begin
    Det.Items[ind].Imposto.ICMS.vICMSST  := FQuery.Fields[3].AsCurrency;
    Det.Items[ind].Imposto.ICMS.vBCST    := FQuery.Fields[1].AsCurrency;
    Det.Items[ind].Imposto.ICMS.pRedBCST := FQuery.Fields[0].AsCurrency;
    Det.Items[ind].Imposto.ICMS.pICMSST  := FQuery.Fields[2].AsCurrency;
  end
  else
  begin
    Det.Items[ind].Imposto.ICMS.vICMSST  := 0;
    Det.Items[ind].Imposto.ICMS.vBCST    := 0;
    Det.Items[ind].Imposto.ICMS.pRedBCST := 0;
    Det.Items[ind].Imposto.ICMS.pICMSST  := 0;
  end;

  FTot    := FTot    + Det.Items[ind].Imposto.ICMS.vICMSST;
  FTotvBC := FTotvBC + Det.Items[ind].Imposto.ICMS.vBCST;

end;

procedure TComICMSST.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Redu��o ST';
  FQuery.Fields[1].disPlayLabel := 'BC ICMS ST';
  FQuery.Fields[2].disPlayLabel := 'Aliquota ST';
  FQuery.Fields[3].disPlayLabel := 'Valor ICMS ST';
  FQuery.Fields[4].disPlayLabel := 'CST';

  FQuery.fields[0].Required := false;
  FQuery.fields[1].Required := false;
  FQuery.fields[2].Required := false;
  FQuery.fields[3].Required := false;
  FQuery.fields[4].Required := false;
end;

procedure TComICMSST.setParametros;
begin
  inherited;
end;

function TComICMSST.validaRegistros: Boolean;
begin



end;

{ TComImposto }

procedure TComImposto.inicializa;
begin
  self.FTot := 0;
end;

procedure TComImposto.setParametros;
begin
  inherited;
  FQuery.Params[1].AsString := pCodigoProdnf;
end;

{ TComPIS }

function TComPIS.getCSTPIS: TpcnCstPis;
begin

  if FQuery.Fields[5].Text = '' then
  begin
    result := pis99;
    Exit;
  end;

  case (FQuery.Fields[5].AsInteger) of

    1:result := pis01;
    2:Result := pis02;
    3:result := pis03;
    4:Result := pis04;
    5:result := pis05;
    6:Result := pis06;
    7:result := pis07;
    8:Result := pis08;
    9:result := pis09;

    49:Result := pis49;

    50:Result := pis50;
    51:Result := pis51;
    52:Result := pis52;
    53:Result := pis53;
    54:Result := pis54;
    55:Result := pis55;
    56:Result := pis56;

    60:result := pis60;
    61:result := pis61;
    62:result := pis62;
    63:result := pis63;
    64:result := pis64;
    65:result := pis65;
    66:result := pis66;
    67:result := pis67;

    70:result := pis70;
    71:result := pis71;
    72:result := pis72;
    73:result := pis73;
    74:result := pis74;
    75:result := pis75;

    98:result := pis98;
    99:result := pis99;

  else ;
    Result := pis99; {outras saidas}
  end;

end;

procedure TComPIS.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  if FQuery.Fields[1].AsCurrency > 0 then
  begin
    Det.Items[ind].Imposto.PIS.vBC := FQuery.Fields[0].AsCurrency;
    Det.Items[ind].Imposto.PIS.vPIS      := FQuery.Fields[2].AsCurrency;
  end
  else
  begin
    Det.Items[ind].Imposto.PIS.vBC  := 0;
    Det.Items[ind].Imposto.PIS.vPIS := 0;
  end;


  Det.Items[ind].Imposto.PIS.pPIS      := FQuery.Fields[1].AsCurrency;
  Det.Items[ind].Imposto.PIS.vAliqProd := FQuery.Fields[3].AsCurrency;
  Det.Items[ind].Imposto.PIS.qBCProd   := FQuery.Fields[4].AsCurrency;
  Det.Items[ind].Imposto.PIS.CST       := getCSTPIS;

  if self.zeraImpostoSimples then
  begin
    Det.Items[ind].Imposto.PIS.vBC       :=0;
    Det.Items[ind].Imposto.PIS.vPIS      :=0;
    Det.Items[ind].Imposto.PIS.pPIS      :=0;
    Det.Items[ind].Imposto.PIS.vAliqProd :=0;
    Det.Items[ind].Imposto.PIS.qBCProd   :=0;
    Det.Items[ind].Imposto.PIS.CST       := pis99;
  end;

  FTot := FTot + Det.Items[ind].Imposto.PIS.vPIS;

end;

procedure TComPIS.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'BC PIS';
  FQuery.Fields[1].disPlayLabel := 'Percentual PIS';
  FQuery.Fields[2].disPlayLabel := 'Valor PIS';
  FQuery.Fields[3].disPlayLabel := 'Valor aliquota produto';
  FQuery.Fields[4].disPlayLabel := 'Quantidade BC produto';
  FQuery.Fields[5].disPlayLabel := 'CST PIS';

  FQuery.Fields[0].Required := false;
  FQuery.Fields[1].Required := false;
  FQuery.Fields[2].Required := false;
  FQuery.Fields[3].Required := false;
  FQuery.Fields[4].Required := false;
  FQuery.Fields[5].Required := false;

end;

procedure TComPIS.setParametros;
begin
  inherited;
end;

function TComPIS.validaRegistros: Boolean;
begin
  if FQuery.Fields[1].AsCurrency > 0 then
  begin

    if FQuery.Fields[0].AsCurrency <= 0 then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' deve ser maior que zero no produto '+self.pCodigoProduto);

    //if FQuery.Fields[2].AsCurrency <= 0 then
      //strErros.Add('Campo: '+fquery.Fields[2].DisplayLabel+' deve ser maior que zero');

  end;
end;

function TComPIS.zeraImpostoSimples: Boolean;
begin
  result := StrToBool(getValueTable('ZERA IMPOSTO NO SIMPLES'));
end;

{ TComCOFINS }

function TComCOFINS.getCSTCOFINS: TpcnCstCofins;
begin

  if FQuery.Fields[5].Text = '' then
  begin
    result := cof99;
    Exit;
  end;

  case (FQuery.Fields[5].AsInteger) of

    1:result := cof01;
    2:Result := cof02;
    3:result := cof03;
    4:Result := cof04;
    5:result := cof05;
    6:Result := cof06;
    7:result := cof07;
    8:Result := cof08;
    9:result := cof09;

    49:Result := cof49;

    50:Result := cof50;
    51:Result := cof51;
    52:Result := cof52;
    53:Result := cof53;
    54:Result := cof54;
    55:Result := cof55;
    56:Result := cof56;

    60:result := cof60;
    61:result := cof61;
    62:result := cof62;
    63:result := cof63;
    64:result := cof64;
    65:result := cof65;
    66:result := cof66;
    67:result := cof67;
    70: result := cof70;
    71: result := cof71;
    72: result := cof72;
    73: result := cof73;
    74: result := cof74;
    75: result := cof75;
    98: result := cof98;
    99: result := cof99;

  else ;
    Result := cof99;{outras saidas}
  end;
end;

procedure TComCOFINS.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  if FQuery.Fields[1].AsCurrency > 0 then
  begin
    Det.Items[ind].Imposto.COFINS.vBC     := FQuery.Fields[0].AsCurrency;
    Det.Items[ind].Imposto.COFINS.vCOFINS := FQuery.Fields[2].AsCurrency;
  end
  else
  begin
    Det.Items[ind].Imposto.COFINS.vBC     := 0;
    Det.Items[ind].Imposto.COFINS.vCOFINS := 0;
  end;

  Det.Items[ind].Imposto.COFINS.pCOFINS   := FQuery.Fields[1].AsCurrency;
  Det.Items[ind].Imposto.COFINS.vAliqProd := FQuery.Fields[3].AsCurrency;
  Det.Items[ind].Imposto.COFINS.qBCProd   := FQuery.Fields[4].AsCurrency;
  Det.Items[ind].Imposto.COFINS.CST       := getCSTCOFINS;

  if Self.zeraImpostoSimples then
  begin
    Det.Items[ind].Imposto.COFINS.vBC       :=0;
    Det.Items[ind].Imposto.COFINS.vCOFINS   :=0;
    Det.Items[ind].Imposto.COFINS.pCOFINS   :=0;
    Det.Items[ind].Imposto.COFINS.vAliqProd :=0;
    Det.Items[ind].Imposto.COFINS.qBCProd   :=0;
    Det.Items[ind].Imposto.COFINS.CST       :=cof99;
  end;

  FTot := FTot + Det.Items[ind].Imposto.COFINS.vCOFINS;

end;

procedure TComCOFINS.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'BC COFINS';
  FQuery.Fields[1].disPlayLabel := 'Percentual COFINS';
  FQuery.Fields[2].disPlayLabel := 'Valor COFINS';
  FQuery.Fields[3].disPlayLabel := 'Valor aliquota produto';
  FQuery.Fields[4].disPlayLabel := 'Quantidade BC produto';
  FQuery.Fields[5].disPlayLabel := 'CST COFINS';

  FQuery.Fields[0].Required := False;
  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[5].Required := False;
end;

procedure TComCOFINS.setParametros;
begin
  inherited;
end;

function TComCOFINS.validaRegistros: Boolean;
begin
  if FQuery.Fields[1].AsCurrency > 0 then
  begin

    if FQuery.Fields[0].AsCurrency <= 0 then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' deve ser maior que zero no produto '+self.pCodigoProduto);

    //if FQuery.Fields[2].AsCurrency <= 0 then
      //strErros.Add('Campo: '+fquery.Fields[2].DisplayLabel+' deve ser maior que zero');

  end;
end;

function TComCOFINS.zeraImpostoSimples: Boolean;
begin
  result := StrToBool(getValueTable('ZERA IMPOSTO NO SIMPLES'));
end;

{ TComIPI }

function TComIPI.getCSTIPI: TpcnCstIpi;
begin

  case FQuery.Fields[0].AsInteger of
    0  :result := ipi00;
    1  :result := ipi01;
    2  :result := ipi02;
    3  :result := ipi03;
    4  :result := ipi04;
    5  :result := ipi05;
    51 :result := ipi51;
    52 :result := ipi52;
    53 :Result := ipi53;
    54 :result := ipi54;
    55 :Result := ipi55;
    49 :result := ipi49;
    50 :result := ipi50;
    99 :Result := ipi99;
  else
    result := ipi99
  end;

end;

procedure TComIPI.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  if FQuery.Fields[2].AsCurrency > 0 then
  begin
    Det.Items[ind].Imposto.IPI.CST  := getCSTIPI;
    Det.Items[ind].Imposto.IPI.vBC  := FQuery.Fields[1].AsCurrency;
    Det.Items[ind].Imposto.IPI.pIPI := FQuery.Fields[2].AsCurrency;
    Det.Items[ind].Imposto.IPI.vIPI := FQuery.Fields[3].AsCurrency;
  end;

  if zeraImpostoSimples then
  begin
    Det.Items[ind].Imposto.IPI.vBC  := 0;
    Det.Items[ind].Imposto.IPI.pIPI := 0;
    Det.Items[ind].Imposto.IPI.vIPI := 0;
  end;

  FTot := FTot + Det.Items[ind].Imposto.IPI.vIPI;
  
end;

procedure TComIPI.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CST IPI';
  FQuery.Fields[1].disPlayLabel := 'BC IPI';
  FQuery.Fields[2].disPlayLabel := 'Percentual IPI';
  FQuery.Fields[3].disPlayLabel := 'Valor IPI';

  FQuery.Fields[0].Required := False;
  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[3].Required := False;
end;

procedure TComIPI.setParametros;
begin
  inherited;
end;

function TComIPI.validaRegistros: Boolean;
begin

  if FQuery.Fields[2].AsCurrency > 0 then
  begin

    if FQuery.Fields[0].Text = '' then
      strErros.Add('Campo: '+FQuery.Fields[0].DisplayLabel+' vazio ou nulo no produto '+self.pCodigoProduto);

    if FQuery.Fields[3].AsCurrency <= 0 then
      strErros.Add('Campo: '+fquery.Fields[3].DisplayLabel+' n�o pode ser menor que 0(zero) no produto '+self.pCodigoProduto);
      
  end;

end;

function TComIPI.zeraImpostoSimples: Boolean;
begin
  result := StrToBool(getValueTable('ZERA IMPOSTO NO SIMPLES'));
end;

{ TComComplementar }

procedure TComComplementar.preenche;
begin

  self.validaRegistros;

  Ide.NFref.Add;
  Ide.NFref.Items[0].refNFe := Self.chaveRef;
  Ide.finNFe := fnComplementar;
  
end;

procedure TComComplementar.setCampos;
begin
  inherited;

end;


procedure TComComplementar.setParametros;
begin
  inherited;

end;

function TComComplementar.validaRegistros: Boolean;
begin

  if chaveRef = '' then
    strErros.Add('Erro no registro: '+self.Registro+' .Necessario informar a chave de referencia da NF-e');

end;

{ TComDevolucao }

procedure TComDevolucao.preenche;
begin

  self.validaRegistros;
  if teveErros then Exit;
  
  if modeloRef = 1 then                                                                     
  begin                                                                                     
                                                                                            
    Ide.NFref.Add;                                                                          
                                                                                            
    with Ide.NFref.Items[0].RefNF do                                                        
    begin
      cUF    := StrToInt(self.cUF_ref);
      AAMM   := FormatDateTime('yymm',StrToDate (self.AAMM_ref));
      CNPJ   := RetornaSoNumeros(self.CNPJ_ref);
      modelo := modeloRef;
      serie  := StrToInt(Self.serie_ref);
      nNF    := StrToInt(self.nNF_ref);                          
    end;

  end
  else if modeloRef = 55 then
  begin
    Ide.NFref.Add;
    Ide.NFref.Items[0].refNFe := Self.chaveRef;
  end;

end;

procedure TComDevolucao.SetAAMM_ref(const Value: string);
begin
  FAAMM_ref := Value;
end;

procedure TComDevolucao.setCampos;
begin
  inherited;

end;

procedure TComDevolucao.SetCNPJ_ref(const Value: string);
begin
  FCNPJ_ref := Value;
end;

procedure TComDevolucao.SetcUF_ref(const Value: string);
begin
  FcUF_ref := Value;
end;

procedure TComDevolucao.SetmodeloRef(const Value: Integer);
begin
  FmodeloRef := Value;
end;

procedure TComDevolucao.Setmodelo_ref(const Value: string);
begin
  Fmodelo_ref := Value;
end;

procedure TComDevolucao.SetnNF_ref(const Value: string);
begin
  FnNF_ref := Value;
end;

procedure TComDevolucao.setParametros;
begin
  inherited;

end;

procedure TComDevolucao.Setserie_ref(const Value: string);
begin
  Fserie_ref := Value;
end;

function TComDevolucao.validaRegistros: Boolean;
begin

  if modeloRef = 55 then
  begin
    if chaveRef = '' then
      strErros.Add('Erro no registro: '+self.Registro+' .Chave de referencia inv�lida');
  end
  else if modeloRef = 1 then
  begin
  
  end;

end;

{ TComCupomRef }

function TComCupomRef.getModeloECF(pModelo:string=''): TpcnECFModRef;
var
  l_modelo:string;
begin

  if pModelo<>''then
    l_modelo := pModelo
  else
    l_modelo := self.modelo;

  if l_modelo = '2D' then
    Result := ECFModRef2D
  else
  if l_modelo = '2C' then
    result := ECFModRef2C
  else
  if l_modelo = '2B' then
    result := ECFModRef2B
  else
    result := ECFModRefVazio;
end;

procedure TComCupomRef.preenche;
begin

  if (self.FnCOO <> '') and (self.FnECF <> '') then
  begin
    Ide.NFref.Add;
    ide.NFref.Items[0].RefECF.modelo := self.getModeloECF;
    ide.NFref.Items[0].RefECF.nECF   := self.FnECF;
    ide.NFref.Items[0].RefECF.nCOO   := self.FnCOO;
    infComplementares := 'NF-e referenciada por cupom fiscal. Numero cupom: '+self.FnCOO+#13;
  end;

end;

procedure TComCupomRef.setCampos;
begin
  inherited;

end;

procedure TComCupomRef.Setmodelo(const Value: string);
begin
  Fmodelo := Value;
end;

procedure TComCupomRef.SetnCOO(const Value: string);
begin
  FnCOO := Value;
end;

procedure TComCupomRef.SetnECF(const Value: string);
begin
  FnECF := Value;
end;

procedure TComCupomRef.setParametros;
begin
  inherited;

end;

procedure TComCupomRef.SetstrRefCupom(const Value: string);
begin
  FstrRefCupom := Value;
end;

function TComCupomRef.validaRegistros: Boolean;
begin
end;

{ TComCobranca }

procedure TComCobranca.preenche;
begin
  inherited;

end;

procedure TComCobranca.setCampos;
begin
  inherited;

end;

procedure TComCobranca.setParametros;
begin
  inherited;

end;

function TComCobranca.validaRegistros: Boolean;
begin

end;

{ TComFat }

procedure TComFat.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;
  
  Cobr.Fat.nFat  := FQuery.Fields[0].AsString;
  Cobr.Fat.vOrig := FQuery.Fields[1].AsCurrency;
  Cobr.Fat.vDesc := FQuery.Fields[2].AsCurrency;
  Cobr.Fat.vLiq  := FQuery.Fields[3].AsCurrency;
end;

procedure TComFat.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'N�mero fatura';
  FQuery.Fields[1].disPlayLabel := 'Valor original';
  FQuery.Fields[2].disPlayLabel := 'Valor desconto';
  FQuery.Fields[3].disPlayLabel := 'Valor liquido';

  FQuery.Fields[2].Required := False;

end;

procedure TComFat.setParametros;
begin
  inherited;
end;

function TComFat.validaRegistros: Boolean;
begin

end;

{ TComDup }

procedure TComDup.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  while not FQuery.Eof do
  begin
    Cobr.Dup.Add;
    Cobr.Dup.Items[FQuery.RecNo-1].nDup  := FQuery.Fields[0].AsString;
    Cobr.Dup.Items[FQuery.RecNo-1].dVenc := FQuery.Fields[1].AsDateTime;
    Cobr.Dup.Items[FQuery.RecNo-1].vDup  := FQuery.Fields[2].AsCurrency;
    FQuery.Next;
  end;
end;

procedure TComDup.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'N�mero duplicata';
  FQuery.Fields[1].disPlayLabel := 'Data vencimento';
  FQuery.Fields[2].disPlayLabel := 'Valor duplicata';
end;

procedure TComDup.setParametros;
begin
  inherited;
end;

function TComDup.validaRegistros: Boolean;
begin

end;

{ TComCartaCorrecao }

function TComCartaCorrecao.enviarCarta: Boolean;
begin
  result := False;

  self.inicializa;
  self.validaRegistros;
  if teveErros then Exit;
                                                                                            
  try

    FComponentesNfe.ACBrNFe.Configuracoes.WebServices.Visualizar := true;
    FComponentesNfe.ACBrNFe.EventoNFe.Evento.Clear;
    FComponentesNfe.ACBrNFe.EventoNFe.idLote := self.idLote;

    if not FileExists(PathXML) then
    begin
      strErros.Add('Nenhuma NF-e carregada');
      Exit;
    end;
    
    FComponentesNfe.ACBrNFe.NotasFiscais.LoadFromFile(PathXML);

    with FComponentesNfe.ACBrNFe.EventoNFe.Evento.Add do
    begin
     infEvento.chNFe := self.CHAVEACESSO;
     infEvento.CNPJ   := self.CNPJCPF;
     infEvento.dhEvento := now;
     infEvento.tpEvento := teCCe;
     infEvento.nSeqEvento := self.nSeqEvento;
     infEvento.detEvento.xCorrecao := self.xCorrecao;
    end;

    //if not FComponentesNfe.ACBrNFe.EnviarEventoNFe(self.idLote) then celio0712
    if not FComponentesNfe.ACBrNFe.EnviarEvento(self.idLote) then
    begin
      strErros.Add('Erro ao transmitir, verifique o retorno');
      Exit;
    end;

    {if FComponentesNfe.ACBrNFe.WebServices.CartaCorrecao.CCeRetorno.cStat <> 135 then
    begin
      strErros.Add(FComponentesNfe.ACBrNFe.WebServices.CartaCorrecao.CCeRetorno.retEvento.Items[0].RetInfEvento.xMotivo);
      Exit;
    end;}

  except
    on e:Exception do
    begin
      strErros.Add(e.Message);
      Exit;
    end;
  end;

  result := True;

end;

function TComCartaCorrecao.getCaminhoXML: string;
var
  mesAno:string;
begin

  mesAno := FormatDateTime('mm.yyyy',now);
  result := getValueTable('CAMINHO XML DA CARTA DE CORRECAO');

  if (not DirectoryExists(result+mesAno)) then
    ForceDirectories(result+mesAno);
  Result := Result+mesAno+'\';

end;

function TComCartaCorrecao.getnSeqEvento(chAcesso: string): Integer;
begin
  fquery.Close;
  fquery.SQL.Text := getCampoSQL('EVENTO CCE');
  if FQuery.Params.Count < 1 then
  begin
    strErros.Add('SQL EVENTO CCE sem parametro');
    Exit;
  end
  else
  begin
    FQuery.Params[0].AsString := chAcesso;
  end;
  fquery.Open;
  result := fquery.fieldbyname('max').AsInteger;
  {para cada carta vinculada a uma NF-e (chave de acesso) so podem ser emitidos nseqEvento de 1-20}
end;

function TComCartaCorrecao.getRetornoWS: string;
begin
  { TODO : altera��o acbr 08/01/2015 }
  result := FComponentesNfe.ACBrNFe.WebServices.EnvEvento.RetornoWS;
end;

function TComCartaCorrecao.getRetWs: string;
begin
  { TODO : altera��o acbr 08/01/2015 }
  result := FComponentesNfe.ACBrNFe.WebServices.EnvEvento.RetWS;
end;

procedure TComCartaCorrecao.inicializa;
begin
  if not Assigned(FQuery) then
  begin
    FQuery := TIBQuery.Create(self);
    FQuery.Database := FDatabase;
  end;
end;

procedure TComCartaCorrecao.preenche;
begin
  //inherited;

end;

procedure TComCartaCorrecao.setCampos;
begin
  inherited;

end;

procedure TComCartaCorrecao.Setchaveacesso(const Value: string);
begin
  Fchaveacesso := Value;
end;

procedure TComCartaCorrecao.SetCNPJCPF(const Value: string);
begin
  FCNPJCPF := Value;
end;

procedure TComCartaCorrecao.SetcondUso(const Value: string);
begin
  FcondUso := Value;
end;

procedure TComCartaCorrecao.SetcOrgao(const Value: integer);
begin
  FcOrgao := Value;
end;

procedure TComCartaCorrecao.SetidLote(const Value: integer);
begin
  FidLote := Value;
end;

procedure TComCartaCorrecao.SetnSeqEvento(const Value: Integer);
begin
  FnSeqEvento := Value;
end;

procedure TComCartaCorrecao.setParametros;
begin
  inherited;

end;

procedure TComCartaCorrecao.SetxCorrecao(const Value: string);
begin
  FxCorrecao := Value;
end;

function TComCartaCorrecao.validaRegistros: Boolean;
begin

  if self.idLote <= 0 then
    strErros.Add('idLote n�o informado');

  if self.chaveacesso = '' then
    strErros.Add('Chave acesso n�o informada');

  if self.CNPJCPF = '' then
    strErros.Add('CNPJ/CPF n�o informado')
  else
  begin

    if not ValidaCNPJ(self.CNPJCPF) then
      if not ValidaCPF(self.CNPJCPF) then
        strErros.Add('CNPJ/CPF inv�lido');

  end;

  if self.xCorrecao = '' then
    strErros.Add('Corre��o n�o informada');

  if self.nSeqEvento > 20 then
  begin
    strErros.Add('N�o � possivel emitir mais de 20 cartas de corre��o para uma mesma chave de acesso');
    Exit;
  end;

end;


{ TComDestCNFCe }

procedure TComDestCNFCe.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;

  Dest.CNPJCPF := FQuery.Fields[0].Text;
  dest.IE      := FQuery.Fields[1].Text;
  dest.xNome   := FQuery.Fields[2].Text;

  if FQuery.Fields[1].Text <> '' then
    DEST.IE := FQuery.Fields[1].Text
  else
  begin
    if FQuery.Fields[5].Text = 'J' then
    begin
      if FQuery.Fields[4].Text <> '' then
        dest.IE := FQuery.Fields[4].Text
      else
        dest.IE := 'ISENTO';
    end
    else
      DEST.IE := 'ISENTO'

  end;

   dest.Email   := FQuery.Fields[6].Text;

end;

procedure TComDestCNFCe.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'CPF CGC';
  FQuery.Fields[1].disPlayLabel := 'IE Produtor rural';
  FQuery.Fields[2].disPlayLabel := 'Nome';
  FQuery.Fields[3].disPlayLabel := 'UF';
  FQuery.Fields[4].disPlayLabel := 'RG IE';
  FQuery.Fields[5].disPlayLabel := 'Fisica/Juridica';
  FQuery.Fields[6].DisplayLabel := 'Email';

  FQuery.Fields[0].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[1].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[2].OnGetText := GetFieldTrim;
  FQuery.Fields[3].OnGetText := GetFieldUpperCase;
  FQuery.Fields[4].OnGetText := GetFieldSoNumeros;
  FQuery.Fields[5].OnGetText := GetFieldUpperCase;
  FQuery.Fields[6].OnGetText := GetFieldTrim;

  //nfe para o exterior nao deve valida cpf/cnpj
  FQuery.Fields[0].Required := False;
  
  FQuery.Fields[1].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[6].Required := False;
end;

procedure TComDestCNFCe.setParametros;
begin
  inherited;

end;

function TComDestCNFCe.validaRegistros: Boolean;
begin

end;

{ TComEnderDestCNFCe }

procedure TComEnderDestCNFCe.preenche;
begin
  inherited;

end;

procedure TComEnderDestCNFCe.setCampos;
begin
  inherited;
  {
  FQuery.Fields[0].disPlayLabel := 'CEP';
  FQuery.Fields[1].disPlayLabel := 'C�digo cidade';
  FQuery.Fields[2].disPlayLabel := 'UF';
  FQuery.Fields[3].disPlayLabel := 'Fone';
  FQuery.Fields[4].disPlayLabel := 'Bairro';
  FQuery.Fields[5].disPlayLabel := 'C�digo pais';
  FQuery.Fields[6].disPlayLabel := 'Pais';
  FQuery.Fields[7].disPlayLabel := 'Endere�o';
  FQuery.Fields[8].DisplayLabel := 'Cidade';
  FQuery.Fields[9].DisplayLabel := 'N�mero';

  cep pode estar vazio quando uf for exterior
  }
  FQuery.Fields[0].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[7].Required := False;
  FQuery.Fields[8].Required := False;
  FQuery.Fields[9].Required := False;

end;

procedure TComEnderDestCNFCe.setParametros;
begin
  inherited;

end;

function TComEnderDestCNFCe.validaRegistros: Boolean;
begin

end;

{ TComFormaPag }

procedure TComFormaPag.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;
end;

procedure TComFormaPag.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Valor';
  FQuery.Fields[1].disPlayLabel := 'Grupo NFC-e';

  FQuery.Fields[0].Required := false;
  FQuery.Fields[1].Required := false;
end;

procedure TComFormaPag.setParametros;
begin
  inherited;

end;

function TComFormaPag.validaRegistros: Boolean;
begin

end;

{ TComAutorizacao }

procedure TComAutorizacao.preenche;
begin
  inherited;
  if not AbreQuery then Exit;
  if teveErros then Exit;
end;

procedure TComAutorizacao.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'Cpf/Cnpj';

  FQuery.Fields[0].Required := false;
end;

procedure TComAutorizacao.setParametros;
begin
  inherited;

end;

function TComAutorizacao.validaRegistros: Boolean;
begin
  (*
  if tipoNF = 'SAIDA' then
  begin

    if (FQuery.Fields[0].Text[1] <> '5') and (FQuery.Fields[0].Text[1] <> '6') and (FQuery.Fields[0].Text[1] <> '7')  then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido para nfe de saida. Valor do campo: '+FQuery.Fields[0].Text);

  end
  else if tipoNF = 'ENTRADA' then
  begin

    if (FQuery.Fields[0].Text[1] <> '1') and (FQuery.Fields[0].Text[1] <> '2') and (FQuery.Fields[0].Text[1] <> '3')  then
      strErros.Add('Campo: '+fquery.Fields[0].DisplayLabel+' inv�lido para nf-e de entrada. Valor do campo: '+FQuery.Fields[0].Text);

  end;
  *)
end;

{ TComDIFAL }

procedure TComDIFAL.preenche;
begin
  inherited;
end;

procedure TComDIFAL.setCampos;
begin
  inherited;
  FQuery.Fields[0].disPlayLabel := 'BC ICMS UFdestino';
  FQuery.Fields[1].disPlayLabel := 'Percentual ICMS UF Destino';
  FQuery.Fields[2].disPlayLabel := 'Valor ICMS UF Destino';
  FQuery.Fields[3].disPlayLabel := 'Valor ICMS UF Origem';
  FQuery.Fields[4].disPlayLabel := 'Percentual ICMS Interestadual';
  FQuery.Fields[5].disPlayLabel := 'Percentual ICMS Inter Participacao';
  FQuery.Fields[6].disPlayLabel := 'Percentual FCP UF Destino';
  FQuery.Fields[7].disPlayLabel := 'Valor FCP UF Destino';

  FQuery.Fields[0].Required := False;
  FQuery.Fields[1].Required := False;
  FQuery.Fields[2].Required := False;
  FQuery.Fields[3].Required := False;
  FQuery.Fields[4].Required := False;
  FQuery.Fields[5].Required := False;
  FQuery.Fields[6].Required := False;
  FQuery.Fields[7].Required := False;

end;

procedure TComDIFAL.setParametros;
begin
  inherited;

end;

function TComDIFAL.validaRegistros: Boolean;
begin
//  if FQuery.Fields[2].AsCurrency > 0 then
//  begin
//
//    if FQuery.Fields[0].Text = '' then
//      strErros.Add('Campo: '+FQuery.Fields[0].DisplayLabel+' vazio ou nulo no produto '+self.pCodigoProduto);
//
//    if FQuery.Fields[3].AsCurrency <= 0 then
//      strErros.Add('Campo: '+fquery.Fields[3].DisplayLabel+' n�o pode ser menor que 0(zero) no produto '+self.pCodigoProduto);
//      
//  end;
end;

function TComDIFAL.zeraImpostoSimples: Boolean;
begin
  result := StrToBool(getValueTable('ZERA IMPOSTO NO SIMPLES'));
end;

end.

