unit uEnderecoEntrega;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, Buttons, UobjCLIENTE;

type
  TFEnderecoEntrega = class(TForm)
    GroupBox1: TGroupBox;
    lbLbCPF_CGC: TLabel;
    edtCPF_CGC: TMaskEdit;
    lbLbEnderecoCobranca: TLabel;
    edtendereco: TEdit;
    lbNumero: TLabel;
    edtNumero: TEdit;
    lb5: TLabel;
    edtComplemento: TEdit;
    lbLbBairroCobranca: TLabel;
    edtbairro: TEdit;
    lbLbCidadeCobranca: TLabel;
    Combocbbcidade: TComboBox;
    lbLbEstadoCobranca: TLabel;
    edtestado: TEdit;
    Panel1: TPanel;
    btOK: TBitBtn;
    btCancel: TBitBtn;
    Label1: TLabel;
    edtCodigoCidade: TEdit;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CombocbbcidadeExit(Sender: TObject);
    procedure edtestadoExit(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btOKClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    function validaDados:boolean;
    { Private declarations }
  public
    { Public declarations }
    objCliente : TObjCLIENTE;
  end;

var
  FEnderecoEntrega: TFEnderecoEntrega;

implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TFEnderecoEntrega.FormShow(Sender: TObject);
begin
  objCliente := TObjCLIENTE.Create;
  objCliente.carregaCidades( Combocbbcidade );
end;

procedure TFEnderecoEntrega.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FreeAndNil( objcliente );
end;

procedure TFEnderecoEntrega.CombocbbcidadeExit(Sender: TObject);
begin
  if (Combocbbcidade.Text <> '') then
  begin
    edtestado.Text := ObjCliente.retornaUF(Combocbbcidade.Text);

    if (edtestado.Text <> '') then
    begin
      edtestadoExit(edtestado);
      //ComboCodigoPais.SetFocus;
    end

  end
end;

procedure TFEnderecoEntrega.edtestadoExit(Sender: TObject);
var
   cidade:string;
begin
  cidade := ObjCliente.retornaCidade(combocbbcidade.Text);
  edtCodigoCidade.Text := ObjCliente.retornaCodigoCidade (cidade,edtestado.Text);
end;

procedure TFEnderecoEntrega.btCancelClick(Sender: TObject);
begin
  self.Tag := 0;
  self.Close;
end;

procedure TFEnderecoEntrega.btOKClick(Sender: TObject);
begin
  if not validaDados then
    Exit;
  Self.Tag := 1;
  self.Close;
end;

procedure TFEnderecoEntrega.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function tfenderecoentrega.validaDados:boolean;
begin
  result := false;
  if(Trim(RetornaSoNumeros(edtCPF_CGC.Text)) = '') then
  begin
    if((Trim(RetornaSoNumeros(edtendereco.Text)) <> '') or
      (Trim(RetornaSoNumeros(Combocbbcidade.Text)) <> '')) then
    begin
      MensagemErro('CPF ou CNPJ de informação obrigatória');
      edtCPF_CGC.SetFocus;
      exit;
    end;
  end;
  result := True;
end;

end.
