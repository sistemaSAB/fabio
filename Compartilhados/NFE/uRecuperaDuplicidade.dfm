object formRecuperaDuplicidade: TformRecuperaDuplicidade
  Left = 570
  Top = 270
  Width = 529
  Height = 253
  Caption = 'Recuperar XML duplicidade'
  Color = clBtnFace
  Constraints.MinHeight = 207
  Constraints.MinWidth = 384
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnResize = FormResize
  OnShow = FormShow
  DesignSize = (
    513
    215)
  PixelsPerInch = 96
  TextHeight = 13
  object Label25: TLabel
    Left = 18
    Top = 6
    Width = 47
    Height = 13
    Caption = 'N'#250'mero'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGray
    Font.Height = -11
    Font.Name = 'Segoe UI Symbol'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object lbCountArquivos: TLabel
    Left = 19
    Top = 187
    Width = 111
    Height = 13
    Anchors = [akLeft, akBottom]
    Caption = '0 arquivos encontrados'
    Color = clBtnFace
    ParentColor = False
  end
  object btnRecuperar: TSpeedButton
    Left = 405
    Top = 182
    Width = 97
    Height = 22
    Anchors = [akRight, akBottom]
    Caption = 'Recuperar'
    Flat = True
    OnClick = btnRecuperarClick
  end
  object edtNumeroNFe: TEdit
    Left = 18
    Top = 22
    Width = 143
    Height = 23
    BorderStyle = bsNone
    Ctl3D = False
    Font.Charset = ANSI_CHARSET
    Font.Color = 6073854
    Font.Height = -13
    Font.Name = 'Segoe UI Symbol'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    OnExit = edtNumeroNFeExit
    OnKeyDown = edtNumeroNFeKeyDown
  end
  object listXML: TListBox
    Left = 18
    Top = 60
    Width = 483
    Height = 112
    Anchors = [akLeft, akTop, akRight, akBottom]
    BorderStyle = bsNone
    Color = clBtnHighlight
    Ctl3D = True
    ItemHeight = 13
    ParentCtl3D = False
    TabOrder = 1
    OnKeyDown = listXMLKeyDown
  end
  object OpenDialog1: TOpenDialog
    Filter = 'Arquivos de NF-e (*.XML)|*.XML'
    Left = 356
    Top = 23
  end
end
