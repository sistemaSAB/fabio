unit UacertaImposto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, DB, IBCustomDataSet, IBQuery, StdCtrls, Mask, DBCtrls,
  Buttons, ExtCtrls, ImgList, Grids, DBGrids,mdGraphicText,UDataModulo,
  Menus;

type
  TfAcertaImposto = class(TForm)
    PageControl1: TPageControl;
    TabSheet2: TTabSheet;
    TabSheet1: TTabSheet;
    query1: TIBQuery;
    Panel1: TPanel;
    ds1: TDataSource;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    btAnt: TSpeedButton;
    btprox: TSpeedButton;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    btOk: TBitBtn;
    btCancel: TBitBtn;
    ImageList1: TImageList;
    TabSheet8: TTabSheet;
    DBGrid1: TDBGrid;
    queryAux: TIBQuery;
    edt_ICMS_BC: TEdit;
    edt_ICMS_ALIQUOTA: TEdit;
    edt_ICMS_VALOR: TEdit;
    edt_ICMS_REDUCAO: TEdit;
    edt_ICMSST_BC: TEdit;
    edt_ICMSST_ALIQUOTA: TEdit;
    edt_ICMSST_VALOR: TEdit;
    edt_ICMSST_REDUCAO: TEdit;
    edt_PIS_CST: TEdit;
    edt_PIS_ALIQUOTA: TEdit;
    edt_PIS_VALOR: TEdit;
    edt_PIS_BC: TEdit;
    edt_COFINS_CST: TEdit;
    edt_COFINS_ALIQUOTA: TEdit;
    edt_COFINS_VALOR: TEdit;
    edt_COFINS_BC: TEdit;
    edt_IPI_CST: TEdit;
    edt_IPI_ALIQUOTA: TEdit;
    edt_IPI_VALOR: TEdit;
    BC: TLabel;
    edt_IPI_BC: TEdit;
    btOk_ICMS: TSpeedButton;
    btOK_ICMS_ST: TSpeedButton;
    btOk_PIS: TSpeedButton;
    btOK_COFINS: TSpeedButton;
    btOK_IPI: TSpeedButton;
    pageControlTotais: TPageControl;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label24: TLabel;
    tot_produto: TEdit;
    lbTotalItens: TLabel;
    tot_nf: TEdit;
    tot_desconto: TEdit;
    tot_ICMS_BC: TEdit;
    tot_ICMS_VALOR: TEdit;
    Label25: TLabel;
    Label26: TLabel;
    tot_ICMSST_BC: TEdit;
    tot_ICMSST_VALOR: TEdit;
    Label27: TLabel;
    Label28: TLabel;
    tot_PIS_BC: TEdit;
    tot_PIS_VALOR: TEdit;
    Label29: TLabel;
    Label30: TLabel;
    tot_COFINS_VALOR: TEdit;
    tot_COFINS_BC: TEdit;
    Label31: TLabel;
    Label32: TLabel;
    tot_IPI_BC: TEdit;
    tot_IPI_VALOR: TEdit;
    Label33: TLabel;
    edt_ICMS_ValorFrete: TEdit;
    Label34: TLabel;
    tot_ICMS_ValorFrete: TEdit;
    Label35: TLabel;
    CSOSN: TLabel;
    edt_ICMS_CSOSN: TEdit;
    TabSheet14: TTabSheet;
    edt_outros_CRT: TLabel;
    outros_CRT: TEdit;
    SpeedButton1: TSpeedButton;
    Label36: TLabel;
    edt_ICMS_ValorOutros: TEdit;
    Label37: TLabel;
    tot_ICMS_ValorOutros: TEdit;
    TabSheet15: TTabSheet;
    Label38: TLabel;
    edt_outros_valorTributos: TEdit;
    SpeedButton2: TSpeedButton;
    Label39: TLabel;
    edt_outros_percentualTributo: TEdit;
    pReplica: TPopupMenu;
    Replicar1: TMenuItem;
    edt_ICMS_CST: TEdit;
    procedure query1BeforeOpen(DataSet: TDataSet);
    procedure query1AfterOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btAntClick(Sender: TObject);
    procedure btproxClick(Sender: TObject);
    procedure dbEdtValorChange(Sender: TObject);
    procedure dbEdtDescontoChange(Sender: TObject);
    procedure dbEdtQuantChange(Sender: TObject);
    procedure dbEdt_BCICMSChange(Sender: TObject);
    procedure dbEdt_ALIQUOTAICMSChange(Sender: TObject);
    procedure dbEdt_VALORICMSChange(Sender: TObject);
    procedure dbEdt_REDUCAOICMSChange(Sender: TObject);
    procedure dbEdt_BCICMSSTChange(Sender: TObject);
    procedure dbEdt_ALIQUOTASTChange(Sender: TObject);
    procedure dbEdt_VALORICMSSTChange(Sender: TObject);
    procedure dbEdt_REDUCAOICMSSTChange(Sender: TObject);
    procedure dbEdt_PERCENTUALPISChange(Sender: TObject);
    procedure dbEdt_VALORPISChange(Sender: TObject);
    procedure dbEdt_BCPISChange(Sender: TObject);
    procedure dbEdt_PERCENTUALCOFINSChange(Sender: TObject);
    procedure dbEdt_VALORCOFINSChange(Sender: TObject);
    procedure dbEdt_BCCOFINSChange(Sender: TObject);
    procedure dbEdt_PERCENTUALIPIChange(Sender: TObject);
    procedure dbEdt_VALORIPIChange(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure queryAuxAfterOpen(DataSet: TDataSet);
    procedure queryAuxBeforeOpen(DataSet: TDataSet);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure edt_ICMS_BCExit(Sender: TObject);
    procedure edt_ICMS_ALIQUOTAExit(Sender: TObject);
    procedure edt_ICMS_VALORExit(Sender: TObject);
    procedure edt_ICMS_REDUCAOExit(Sender: TObject);
    procedure edt_ICMSST_BCExit(Sender: TObject);
    procedure edt_ICMSST_ALIQUOTAExit(Sender: TObject);
    procedure edt_ICMSST_VALORExit(Sender: TObject);
    procedure edt_ICMSST_REDUCAOExit(Sender: TObject);
    procedure edt_PIS_ALIQUOTAExit(Sender: TObject);
    procedure edt_PIS_VALORExit(Sender: TObject);
    procedure edt_PIS_BCExit(Sender: TObject);
    procedure edt_COFINS_ALIQUOTAExit(Sender: TObject);
    procedure edt_COFINS_VALORExit(Sender: TObject);
    procedure edt_COFINS_BCExit(Sender: TObject);
    procedure edt_IPI_ALIQUOTAExit(Sender: TObject);
    procedure edt_IPI_VALORExit(Sender: TObject);
    procedure edt_IPI_BCExit(Sender: TObject);
    procedure tot_produtoExit(Sender: TObject);
    procedure tot_nfExit(Sender: TObject);
    procedure tot_descontoExit(Sender: TObject);
    procedure tot_ICMS_BCExit(Sender: TObject);
    procedure tot_ICMS_VALORExit(Sender: TObject);
    procedure tot_ICMSST_BCExit(Sender: TObject);
    procedure tot_ICMSST_VALORExit(Sender: TObject);
    procedure tot_PIS_BCExit(Sender: TObject);
    procedure tot_PIS_VALORExit(Sender: TObject);
    procedure tot_COFINS_BCExit(Sender: TObject);
    procedure tot_COFINS_VALORExit(Sender: TObject);
    procedure tot_IPI_BCExit(Sender: TObject);
    procedure tot_IPI_VALORExit(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edt_ICMS_VALOREnter(Sender: TObject);
    procedure edt_ICMSST_VALOREnter(Sender: TObject);
    procedure edt_PIS_VALOREnter(Sender: TObject);
    procedure edt_COFINS_VALOREnter(Sender: TObject);
    procedure edt_IPI_VALOREnter(Sender: TObject);
    procedure edt_ICMS_ValorFreteExit(Sender: TObject);
    procedure tot_ICMS_ValorFreteExit(Sender: TObject);
    procedure edt_ICMS_CSTExit(Sender: TObject);
    procedure edt_ICMS_ValorOutrosExit(Sender: TObject);
    procedure tot_ICMS_ValorOutrosExit(Sender: TObject);
    procedure edt_outros_valorTributosExit(Sender: TObject);
    procedure edt_outros_percentualTributoExit(Sender: TObject);
    procedure btCancelClick(Sender: TObject);
    procedure btOkClick(Sender: TObject);
    procedure pReplicaPopup(Sender: TObject);
    procedure outros_CRTExit(Sender: TObject);
  private
    fGraphicText: TGraphicText;
  public
    procedure chamaOnExit();

  end;

var
  fAcertaImposto: TfAcertaImposto;

implementation

uses UessencialGlobal;


{$R *.dfm}

const
  cGridSelectedColor = $00FFBFBF;

type
  TGridHack = class(TCustomGrid);

procedure TfAcertaImposto.query1BeforeOpen(DataSet: TDataSet);
begin
  query1.Active := False;
end;

procedure TfAcertaImposto.query1AfterOpen(DataSet: TDataSet);
begin
  query1.First;
end;

procedure TfAcertaImposto.FormShow(Sender: TObject);
begin
  PageControl1.TabIndex := 0;
  PageControl2.TabIndex := 0;
  query1.Database   := FDataModulo.IBDatabase;
  queryAux.Database := FDataModulo.IBDatabase;
  self.chamaOnExit;
end;

procedure TfAcertaImposto.FormClose(Sender: TObject;var Action: TCloseAction);
begin
  query1.Active := False;
end;

procedure TfAcertaImposto.btAntClick(Sender: TObject);
begin
  ds1.DataSet.Prior;
end;

procedure TfAcertaImposto.btproxClick(Sender: TObject);
begin
  ds1.DataSet.Next;
end;

procedure TfAcertaImposto.dbEdtValorChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdtDescontoChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdtQuantChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_BCICMSChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_ALIQUOTAICMSChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_VALORICMSChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_REDUCAOICMSChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_BCICMSSTChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_ALIQUOTASTChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_VALORICMSSTChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_REDUCAOICMSSTChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_PERCENTUALPISChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_VALORPISChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_BCPISChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_PERCENTUALCOFINSChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_VALORCOFINSChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_BCCOFINSChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_PERCENTUALIPIChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.dbEdt_VALORIPIChange(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).text,0));
end;

procedure TfAcertaImposto.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  notes: string;
begin
  // Se n�o existe dados, n�o faz nada
  if DBGrid1.DataSource.DataSet.IsEmpty then
    Exit;

  // Mant�m a coluna com o mesmo tamanho do grid
  if DBGrid1.Columns[0].Width <> DBGrid1.ClientWidth then
  begin
    DBGrid1.Columns[0].Width := DBGrid1.ClientWidth;
    // Mant�m o DefaultRowHeigth com uma altura para caber tudo
    TGridHack(DBGrid1).DefaultRowHeight := 50;
  end;

  // Configura o objeto que far� todo o desenho
  fGraphicText.SetCanvas(DBGrid1.Canvas);
  fGraphicText.SetRect(Rect);

  // A linha selecionada deve aparecer com uma cor diferente
  if gdSelected in State then
  begin
    fGraphicText.Brush.Color := cGridSelectedColor;
    fGraphicText.FillRect(Rect);
  end;

  fGraphicText.Font.Style := fGraphicText.Font.Style + [fsBold];                                                  
  fGraphicText.Font.Color := clBlue;
  fGraphicText.Font.Size := 10;
  fGraphicText.Write(query1.fieldbyname('NOME_PRODUTO').asstring);

  fGraphicText.Font.Style := fGraphicText.Font.Style - [fsBold];
  fGraphicText.Font.Size := 9;

  // Nova linha, no Canvas, � posicionar pixels...
  // mas a classe GraphicText j� tem isso encapsulado no m�todo NewLine.
  fGraphicText.NewLine;

  fGraphicText.Font.Color := clBlack;
  notes := Copy(query1.fieldbyname('PRODUTO').asstring, 1, 100) + ' ';
  fGraphicText.Write(notes);

  fGraphicText.NewLine;

  fGraphicText.Font.Color := clGreen;
  fGraphicText.Write('Quantidade: ' + query1.fieldbyname('QUANTIDADE').asstring + ' - ');
  fGraphicText.Write('Valor: ' + FormatCurr('0.00 R$',query1.fieldbyname('VALOR').AsCurrency) + ' - ');
  fGraphicText.Write('Desconto: ' + FormatCurr('0.00 R$',query1.fieldbyname('DESCONTO').AsCurrency));

end;

procedure TfAcertaImposto.FormCreate(Sender: TObject);
begin
  fGraphicText := TGraphicText.Create;
  TGridHack(DBGrid1).DefaultRowHeight := 50;
end;

procedure TfAcertaImposto.FormDestroy(Sender: TObject);
begin
  fGraphicText.Free;
end;


procedure TfAcertaImposto.queryAuxAfterOpen(DataSet: TDataSet);
begin
  queryAux.First;
end;

procedure TfAcertaImposto.queryAuxBeforeOpen(DataSet: TDataSet);
begin
 queryAux.Active := False;
end;

procedure TfAcertaImposto.DBGrid1DblClick(Sender: TObject);
begin
  PageControl2.TabIndex := 1;
end;

procedure TfAcertaImposto.edt_ICMS_BCExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_ICMS_ALIQUOTAExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_ICMS_VALORExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_ICMS_REDUCAOExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_ICMSST_BCExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_ICMSST_ALIQUOTAExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_ICMSST_VALORExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_ICMSST_REDUCAOExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_PIS_ALIQUOTAExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_PIS_VALORExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_PIS_BCExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_COFINS_ALIQUOTAExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_COFINS_VALORExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_COFINS_BCExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_IPI_ALIQUOTAExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_IPI_VALORExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.edt_IPI_BCExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.chamaOnExit;
var
  i:integer;
begin
  for i := 0 to self.ComponentCount-1 do
    if self.Components[i] is TEdit then
      if Assigned(TEdit(self.Components[i]).onExit) then
        TEdit(self.Components[i]).OnExit(TEdit(self.Components[i]));
end;

procedure TfAcertaImposto.tot_produtoExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_nfExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_descontoExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_ICMS_BCExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_ICMS_VALORExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_ICMSST_BCExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_ICMSST_VALORExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_PIS_BCExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_PIS_VALORExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_COFINS_BCExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_COFINS_VALORExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_IPI_BCExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_IPI_VALORExit(Sender: TObject);
begin
    TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.PageControl1Change(Sender: TObject);
begin

  if PageControl1.TabIndex = 1 then
    pageControlTotais.TabIndex := 0;

end;

procedure TfAcertaImposto.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if (key = #13) then
    Perform(Wm_NextDlgCtl,0,0)
end;

procedure TfAcertaImposto.edt_ICMS_VALOREnter(Sender: TObject);
var
  bc,reducao,aliquota,vICMS:Currency;
begin

  reducao := 0;
  vICMS   := 0;
  
  try
    reducao  := StrToCurrDef(edt_ICMS_REDUCAO.Text,0);
    bc       := StrToCurrDef(edt_ICMS_BC.Text,0);
    bc       := bc - (reducao/100.00*bc);
    aliquota := StrToCurrDef(edt_ICMS_ALIQUOTA.Text,0);
    vICMS := (aliquota/100.00*bc);

    edt_ICMS_VALOR.Text := CurrToStr(aliquota/100.00*bc);
  except
    edt_ICMS_VALOR.Text := '0,00';
  end

end;

procedure TfAcertaImposto.edt_ICMSST_VALOREnter(Sender: TObject);
var
  bc,reducao,aliquota,vICMS:Currency;
begin

  reducao := 0;
  vICMS   := 0;
  
  {try
    reducao  := StrToCurrDef(edt_ICMSST_REDUCAO.Text,0);
    bc       := StrToCurrDef(edt_ICMSST_BC.Text,0);
    bc       := bc - (reducao/100.00*bc);
    aliquota := StrToCurrDef(edt_ICMSST_ALIQUOTA.Text,0);
    vICMS := (aliquota/100.00*bc);

    edt_ICMSST_VALOR.Text := CurrToStr(vICMS);
  except
    edt_ICMSST_VALOR.Text := '0,00';
  end}

  //de acordo com marcelo mysaki o calculo do st nao � como descrito acima, e sim como descrito abaixo (25/02/2014)

  try
    reducao  := StrToCurrDef(edt_ICMSST_REDUCAO.Text,0);
    bc       := StrToCurrDef(edt_ICMSST_BC.Text,0);
    aliquota := StrToCurrDef(edt_ICMSST_ALIQUOTA.Text,0);
    vICMS := (aliquota/100.00*bc);
    vICMS := (reducao/100.00*vICMS);

    edt_ICMSST_VALOR.Text := CurrToStr(vICMS);
  except
    edt_ICMSST_VALOR.Text := '0,00';
  end


end;

procedure TfAcertaImposto.edt_PIS_VALOREnter(Sender: TObject);
var
  bc,aliquota,vICMS:Currency;
begin

  vICMS   := 0;
  
  try
    bc       := StrToCurrDef(edt_PIS_BC.Text,0);
    aliquota := StrToCurrDef(edt_PIS_ALIQUOTA.Text,0);
    vICMS := (aliquota/100.00*bc);

    edt_PIS_VALOR.Text := CurrToStr(aliquota/100.00*bc);
  except
    edt_PIS_VALOR.Text := '0,00';
  end

end;

procedure TfAcertaImposto.edt_COFINS_VALOREnter(Sender: TObject);
var
  bc,aliquota,vICMS:Currency;
begin

  vICMS   := 0;
  
  try
    bc       := StrToCurrDef(edt_COFINS_BC.Text,0);
    aliquota := StrToCurrDef(edt_COFINS_ALIQUOTA.Text,0);
    vICMS := (aliquota/100.00*bc);

    edt_COFINS_VALOR.Text := CurrToStr(aliquota/100.00*bc);
  except
    edt_COFINS_VALOR.Text := '0,00';
  end

end;

procedure TfAcertaImposto.edt_IPI_VALOREnter(Sender: TObject);
var
  bc,aliquota,vICMS:Currency;
begin

  vICMS   := 0;
  
  try
    bc       := StrToCurrDef(edt_IPI_BC.Text,0);
    aliquota := StrToCurrDef(edt_IPI_ALIQUOTA.Text,0);
    vICMS := (aliquota/100.00*bc);

    edt_IPI_VALOR.Text := CurrToStr(aliquota/100.00*bc);
  except
    edt_IPI_VALOR.Text := '0,00';
  end

end;

procedure TfAcertaImposto.edt_ICMS_ValorFreteExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_ICMS_ValorFreteExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0));
end;

procedure TfAcertaImposto.edt_ICMS_CSTExit(Sender: TObject);
begin
  if TEdit(sender).Text = '' then
    TEdit(Sender).Text := 'NULL';
end;

procedure TfAcertaImposto.edt_ICMS_ValorOutrosExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0))
end;

procedure TfAcertaImposto.tot_ICMS_ValorOutrosExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0));
end;

procedure TfAcertaImposto.edt_outros_valorTributosExit(Sender: TObject);
begin
  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0));
end;

procedure TfAcertaImposto.edt_outros_percentualTributoExit(Sender: TObject);
var
  percentualtributo:Currency;
  valorFinalProduto:Currency;
begin

  TEdit(sender).Text := FormatCurr('0.00',StrToCurrDef(TEdit(sender).Text,0));


  if not (ds1.DataSet.IsEmpty) then
  begin
    valorFinalProduto  := ds1.DataSet.fieldbyname('valorfinal').AsCurrency;
    percentualtributo := StrToCurrDef(edt_outros_percentualTributo.text,0);
    edt_outros_valorTributos.Text := CurrToStr( (percentualtributo/100)*valorFinalProduto );
    edt_outros_valorTributosExit(edt_outros_valorTributos);
  end;                         

end;

procedure TfAcertaImposto.btCancelClick(Sender: TObject);
begin
  self.Tag := 0;
end;

procedure TfAcertaImposto.btOkClick(Sender: TObject);
begin


  self.Tag := 1;

end;

procedure TfAcertaImposto.pReplicaPopup(Sender: TObject);
begin

  ShowMessage(self.Components[pReplica.PopupComponent.ComponentIndex].ClassName);

  Replicar1.Enabled := (self.Components[pReplica.PopupComponent.ComponentIndex].ClassNameIs('TEdit'));

  if Replicar1.Enabled then
    if not TEdit(self.Components[pReplica.PopupComponent.ComponentIndex]).ShowHint then
      if Trim (TEdit(self.Components[pReplica.PopupComponent.ComponentIndex]).Hint) <> '' then
        Replicar1.Caption := 'Replicar campo: '+TEdit(self.Components[pReplica.PopupComponent.ComponentIndex]).Hint;
end;

procedure TfAcertaImposto.outros_CRTExit(Sender: TObject);
begin
  if TEdit(sender).Text = '' then
    TEdit(Sender).Text := 'NULL';
end;

end.



