unit UstatusNFE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls, pngimage;

type
  TFstatusNFE = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    Image2: TImage;
    Image4: TImage;
    Image5: TImage;
    Image1: TImage;
    Animate1: TAnimate;
    Image3: TImage;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public

    procedure INICIALIZA();
    procedure setFormSize(size:integer);

  end;

var
  FstatusNFE: TFstatusNFE;

implementation

{$R *.dfm}

procedure TFstatusNFE.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  //if (Key = vk_f1) THEN
    //INICIALIZA();

end;

procedure TFstatusNFE.INICIALIZA;
begin

  Sleep(1000);
  Application.ProcessMessages;
  Animate1.Visible:=True;
  Animate1.Play(Animate1.StartFrame,Animate1.StopFrame,0);
  Image1.Visible:=True;
  Application.ProcessMessages;
  Sleep(1500);
  Application.ProcessMessages;
  setFormSize(56);
  Image2.Visible:=True;
  Application.ProcessMessages;
  Sleep(1500);
  setFormSize(56);
  Application.ProcessMessages;
  Image3.Visible:=True;
  Application.ProcessMessages;
  Sleep(1500);
  Application.ProcessMessages;
  setFormSize(56);
  Image4.Visible:=True;
  Application.ProcessMessages;
  Sleep(1500);
  Application.ProcessMessages;
  setFormSize(56);
  Image5.Visible:=True;
  Application.ProcessMessages;

  Sleep(2000);
  Animate1.Visible:=False;
  Application.ProcessMessages;
  setFormSize(-60);
  Sleep(1000);
  self.Close;


end;

procedure TFstatusNFE.setFormSize(size: integer);
begin

  self.Height:=self.Height + size;

end;

procedure TFstatusNFE.FormActivate(Sender: TObject);
begin
  self.INICIALIZA();
end;

end.
