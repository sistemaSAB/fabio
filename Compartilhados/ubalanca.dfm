object Fbalanca: TFbalanca
  Left = 480
  Top = 283
  Width = 539
  Height = 382
  Caption = 'Fbalanca'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 16
  object pnlConf: TPanel
    Left = 0
    Top = 0
    Width = 273
    Height = 344
    Align = alLeft
    BevelInner = bvRaised
    TabOrder = 0
    object Label16: TLabel
      Left = 8
      Top = 7
      Width = 225
      Height = 20
      Caption = 'Configura'#231#245'es (balanca.ini) '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Panel1: TPanel
      Left = 8
      Top = 31
      Width = 257
      Height = 260
      BevelOuter = bvLowered
      TabOrder = 0
      object Label1: TLabel
        Left = 7
        Top = 14
        Width = 58
        Height = 16
        Caption = 'Balan'#231'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 7
        Top = 41
        Width = 83
        Height = 16
        Caption = 'Porta Serial'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 7
        Top = 68
        Width = 74
        Height = 16
        Caption = 'Baud Rate'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label6: TLabel
        Left = 7
        Top = 95
        Width = 64
        Height = 16
        Caption = 'Data Bits'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label7: TLabel
        Left = 7
        Top = 122
        Width = 41
        Height = 16
        Caption = 'Parity'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 7
        Top = 176
        Width = 92
        Height = 16
        Caption = 'Handshaking'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 7
        Top = 149
        Width = 63
        Height = 16
        Caption = 'Stop Bits'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label12: TLabel
        Left = 7
        Top = 204
        Width = 95
        Height = 16
        Caption = 'Usar Balan'#231'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label9: TLabel
        Left = 7
        Top = 233
        Width = 59
        Height = 16
        Caption = 'TimeOut'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object cmbBalanca: TComboBox
        Left = 106
        Top = 6
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        ItemIndex = 2
        TabOrder = 0
        Text = 'Toledo'
        Items.Strings = (
          'Nenhuma'
          'Filizola'
          'Toledo')
      end
      object cmbPortaSerial: TComboBox
        Left = 106
        Top = 34
        Width = 145
        Height = 24
        ItemHeight = 16
        ItemIndex = 0
        TabOrder = 1
        Text = 'COM1'
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'COM5'
          'COM6'
          'COM7'
          'COM8')
      end
      object cmbBaudRate: TComboBox
        Left = 106
        Top = 62
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        ItemIndex = 6
        TabOrder = 2
        Text = '9600'
        Items.Strings = (
          '110'
          '300'
          '600'
          '1200'
          '2400'
          '4800'
          '9600'
          '14400'
          '19200'
          '38400'
          '56000'
          '57600')
      end
      object cmbDataBits: TComboBox
        Left = 106
        Top = 90
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        ItemIndex = 3
        TabOrder = 3
        Text = '8'
        Items.Strings = (
          '5'
          '6'
          '7'
          '8')
      end
      object cmbHandShaking: TComboBox
        Left = 106
        Top = 174
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        ItemIndex = 0
        TabOrder = 6
        Text = 'Nenhum'
        Items.Strings = (
          'Nenhum'
          'XON/XOFF'
          'RTS/CTS'
          'DTR/DSR')
      end
      object cmbParity: TComboBox
        Left = 106
        Top = 118
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        ItemIndex = 0
        TabOrder = 4
        Text = 'none'
        Items.Strings = (
          'none'
          'odd'
          'even'
          'mark'
          'space')
      end
      object cmbStopBits: TComboBox
        Left = 106
        Top = 146
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        ItemIndex = 0
        TabOrder = 5
        Text = 's1'
        Items.Strings = (
          's1'
          's1,5'
          's2'
          '')
      end
      object CmbUsaBalanca: TComboBox
        Left = 106
        Top = 202
        Width = 145
        Height = 24
        Style = csDropDownList
        ItemHeight = 16
        ItemIndex = 0
        TabOrder = 7
        Text = 'SIM'
        Items.Strings = (
          'SIM'
          'N'#195'O')
      end
      object edtTimeOut: TEdit
        Left = 106
        Top = 230
        Width = 145
        Height = 24
        TabOrder = 8
        Text = '2000'
        OnKeyPress = edtTimeOutKeyPress
      end
    end
    object btnCarregarINI: TButton
      Left = 16
      Top = 299
      Width = 112
      Height = 33
      Caption = 'Carregar INI'
      TabOrder = 1
      OnClick = btnCarregarINIClick
    end
    object btnSalvarINI: TButton
      Left = 145
      Top = 299
      Width = 112
      Height = 33
      Caption = 'Salvar INI'
      TabOrder = 2
      OnClick = btnSalvarINIClick
    end
  end
  object pnlGeral: TPanel
    Left = 273
    Top = 0
    Width = 250
    Height = 344
    Align = alClient
    BevelInner = bvRaised
    TabOrder = 1
    object Label10: TLabel
      Left = 8
      Top = 220
      Width = 68
      Height = 16
      Caption = 'Mensagem'
    end
    object Label2: TLabel
      Left = 8
      Top = 160
      Width = 172
      Height = 16
      Caption = 'Ultima Resposta da Balan'#231'a'
    end
    object Label3: TLabel
      Left = 8
      Top = 109
      Width = 105
      Height = 16
      Caption = 'Ultimo Peso Lido:'
    end
    object lblTESTE: TLabel
      Left = 8
      Top = 7
      Width = 141
      Height = 20
      Caption = 'Teste da Balan'#231'a'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btnSair: TButton
      Left = 8
      Top = 312
      Width = 234
      Height = 27
      Caption = 'Sair'
      TabOrder = 0
      OnClick = btnSairClick
    end
    object chbMonitorar: TCheckBox
      Left = 8
      Top = 286
      Width = 233
      Height = 17
      Caption = 'Monitorar a Balan'#231'a'
      TabOrder = 1
      OnClick = chbMonitorarClick
    end
    object Memo1: TMemo
      Left = 8
      Top = 238
      Width = 233
      Height = 41
      TabOrder = 2
    end
    object sttResposta: TStaticText
      Left = 8
      Top = 178
      Width = 233
      Height = 36
      AutoSize = False
      BevelKind = bkTile
      Caption = 'Resposta da Balan'#231'a'
      TabOrder = 3
    end
    object sttPeso: TStaticText
      Left = 8
      Top = 127
      Width = 233
      Height = 24
      AutoSize = False
      BevelKind = bkTile
      Caption = '0,000'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object btnLerPeso: TButton
      Left = 32
      Top = 63
      Width = 193
      Height = 38
      Caption = 'Ler Peso'
      Enabled = False
      TabOrder = 5
      OnClick = btnLerPesoClick
    end
    object btnConectar: TButton
      Left = 16
      Top = 31
      Width = 105
      Height = 25
      Caption = 'Ativar'
      TabOrder = 6
      OnClick = btnConectarClick
    end
    object btnDesconectar: TButton
      Left = 134
      Top = 31
      Width = 105
      Height = 25
      Caption = 'Desativar'
      Enabled = False
      TabOrder = 7
      OnClick = btnDesconectarClick
    end
  end
  object ACBrBAL1: TACBrBAL
    Porta = 'COM1'
    OnLePeso = ACBrBAL1LePeso
    Left = 232
  end
end
