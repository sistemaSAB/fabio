object Ffornecedor2: TFfornecedor2
  Left = 154
  Top = 163
  Width = 565
  Height = 457
  Caption = 'Cadastro de Fornecedor - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Verdana'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 329
    Width = 549
    Height = 90
    Align = alBottom
    Color = 3355443
    TabOrder = 1
    object Btnovo: TBitBtn
      Left = 3
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Novo'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
    end
    object btalterar: TBitBtn
      Left = 115
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Alterar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 227
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Gravar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 339
      Top = 6
      Width = 108
      Height = 38
      Caption = '&Cancelar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
    end
    object btpesquisar: TBitBtn
      Left = 3
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Pesquisar'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btpesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 115
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Relat'#243'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
    end
    object btexcluir: TBitBtn
      Left = 227
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Excluir'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btexcluirClick
    end
    object btsair: TBitBtn
      Left = 339
      Top = 46
      Width = 108
      Height = 38
      Caption = '&Sair'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btsairClick
    end
  end
  object Guia: TTabbedNotebook
    Left = 0
    Top = 0
    Width = 549
    Height = 329
    Align = alClient
    PageIndex = 1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    TabFont.Charset = DEFAULT_CHARSET
    TabFont.Color = clBtnText
    TabFont.Height = -11
    TabFont.Name = 'Verdana'
    TabFont.Style = []
    TabOrder = 0
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&1 - Principal'
      object LbCodigo: TLabel
        Left = 3
        Top = 24
        Width = 40
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbRazaoSocial: TLabel
        Left = 3
        Top = 48
        Width = 73
        Height = 13
        Caption = 'Raz'#227'o Social'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFantasia: TLabel
        Left = 3
        Top = 72
        Width = 47
        Height = 13
        Caption = 'Fantasia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCGC: TLabel
        Left = 3
        Top = 96
        Width = 27
        Height = 13
        Caption = 'CGC'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbIE: TLabel
        Left = 311
        Top = 99
        Width = 12
        Height = 13
        Caption = 'IE'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEstado: TLabel
        Left = 403
        Top = 144
        Width = 38
        Height = 13
        Caption = 'Estado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEndereco: TLabel
        Left = 3
        Top = 120
        Width = 53
        Height = 13
        Caption = 'Endere'#231'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCidade: TLabel
        Left = 3
        Top = 143
        Width = 40
        Height = 13
        Caption = 'Cidade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 3
        Top = 168
        Width = 35
        Height = 13
        Caption = 'Bairro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFone: TLabel
        Left = 3
        Top = 198
        Width = 27
        Height = 13
        Caption = 'Fone'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbContato: TLabel
        Left = 3
        Top = 224
        Width = 45
        Height = 13
        Caption = 'Contato'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbEmail: TLabel
        Left = 3
        Top = 248
        Width = 36
        Height = 13
        Caption = 'e-mail'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbDataCadastro: TLabel
        Left = 3
        Top = 272
        Width = 83
        Height = 13
        Caption = 'Data Cadastro'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFax: TLabel
        Left = 306
        Top = 202
        Width = 20
        Height = 13
        Caption = 'Fax'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtFantasia: TEdit
        Left = 89
        Top = 72
        Width = 397
        Height = 19
        MaxLength = 50
        TabOrder = 2
      end
      object EdtCodigo: TEdit
        Left = 89
        Top = 24
        Width = 69
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtCGC: TEdit
        Left = 89
        Top = 96
        Width = 157
        Height = 19
        MaxLength = 20
        TabOrder = 3
      end
      object EdtRazaoSocial: TEdit
        Left = 89
        Top = 48
        Width = 397
        Height = 19
        MaxLength = 50
        TabOrder = 1
      end
      object EdtIE: TEdit
        Left = 329
        Top = 96
        Width = 157
        Height = 19
        MaxLength = 20
        TabOrder = 4
      end
      object ComboEstado: TComboBox
        Left = 448
        Top = 142
        Width = 39
        Height = 21
        ItemHeight = 13
        TabOrder = 7
        Items.Strings = (
          'AC'
          'AL'
          'AM'
          'AP'
          'BA'
          'CE'
          'DF'
          'ES'
          'GO'
          'MA'
          'MG'
          'MS'
          'MT'
          'PA'
          'PB'
          'PE'
          'PI'
          'PR'
          'RJ'
          'RN'
          'RO'
          'RR'
          'RS'
          'SC'
          'SE'
          'SP'
          'TO')
      end
      object EdtEndereco: TEdit
        Left = 89
        Top = 120
        Width = 397
        Height = 19
        MaxLength = 50
        TabOrder = 5
      end
      object EdtCidade: TEdit
        Left = 89
        Top = 143
        Width = 294
        Height = 19
        MaxLength = 20
        TabOrder = 6
      end
      object EdtDataCadastro: TMaskEdit
        Left = 89
        Top = 272
        Width = 68
        Height = 19
        EditMask = '!99/99/9999;1;_'
        MaxLength = 10
        TabOrder = 13
        Text = '  /  /    '
      end
      object EdtEmail: TEdit
        Left = 89
        Top = 248
        Width = 399
        Height = 19
        MaxLength = 50
        TabOrder = 12
      end
      object EdtContato: TEdit
        Left = 89
        Top = 224
        Width = 399
        Height = 19
        MaxLength = 50
        TabOrder = 11
      end
      object EdtFone: TEdit
        Left = 89
        Top = 198
        Width = 159
        Height = 19
        MaxLength = 20
        TabOrder = 9
      end
      object EdtFax: TEdit
        Left = 329
        Top = 200
        Width = 159
        Height = 19
        MaxLength = 20
        TabOrder = 10
      end
      object edtBairro: TEdit
        Left = 88
        Top = 168
        Width = 401
        Height = 19
        TabOrder = 8
      end
    end
    object TTabPage
      Left = 4
      Top = 24
      Caption = '&2 - Financeiro'
      object LbNomePrazoPagamento: TLabel
        Left = 218
        Top = 181
        Width = 100
        Height = 13
        Caption = 'Prazo Pagamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 11
        Top = 211
        Width = 68
        Height = 13
        Caption = 'Observa'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPrazoPagamento: TLabel
        Left = 11
        Top = 180
        Width = 100
        Height = 13
        Caption = 'Prazo Pagamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCEP: TLabel
        Left = 11
        Top = 155
        Width = 23
        Height = 13
        Caption = 'CEP'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbContaCorrente: TLabel
        Left = 11
        Top = 129
        Width = 89
        Height = 13
        Caption = 'Conta Corrente'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeRamoAtividade: TLabel
        Left = 227
        Top = 36
        Width = 90
        Height = 13
        Caption = 'Ramo Atividade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbRamoAtividade: TLabel
        Left = 11
        Top = 33
        Width = 90
        Height = 13
        Caption = 'Ramo Atividade'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbCodigoPlanoContas: TLabel
        Left = 11
        Top = 57
        Width = 119
        Height = 13
        Caption = 'Codigo Plano Contas'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbBanco: TLabel
        Left = 11
        Top = 81
        Width = 35
        Height = 13
        Caption = 'Banco'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbAgencia: TLabel
        Left = 11
        Top = 105
        Width = 45
        Height = 13
        Caption = 'Ag'#234'ncia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object MemoObservacoes: TMemo
        Left = 141
        Top = 204
        Width = 404
        Height = 103
        TabOrder = 7
      end
      object EdtPrazoPagamento: TEdit
        Left = 141
        Top = 179
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 6
        OnKeyDown = EdtPrazoPagamentoKeyDown
      end
      object EdtCEP: TEdit
        Left = 141
        Top = 154
        Width = 160
        Height = 19
        MaxLength = 20
        TabOrder = 5
      end
      object EdtContaCorrente: TEdit
        Left = 141
        Top = 128
        Width = 240
        Height = 19
        MaxLength = 30
        TabOrder = 4
      end
      object EdtAgencia: TEdit
        Left = 141
        Top = 104
        Width = 240
        Height = 19
        MaxLength = 30
        TabOrder = 3
      end
      object EdtBanco: TEdit
        Left = 141
        Top = 80
        Width = 240
        Height = 19
        MaxLength = 30
        TabOrder = 2
      end
      object EdtCodigoPlanoContas: TEdit
        Left = 141
        Top = 56
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        OnKeyDown = EdtCodigoPlanoContasKeyDown
      end
      object edtRamoAtividade: TEdit
        Left = 140
        Top = 32
        Width = 79
        Height = 19
        Color = clSkyBlue
        TabOrder = 0
        OnExit = edtRamoAtividadeExit
        OnKeyDown = edtRamoAtividadeKeyDown
      end
    end
  end
end
