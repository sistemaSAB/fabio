unit UtrocaMaterialpedido;

{
        /*Jonatan Medina  14/07/2011  */

        Modulo de troca de materiais para pedidos conclu�dos
        Alterado por Jonatan Medina...
        Foi Adicionado:
        Troca de cores
        Troca de Materiais

        J� tinha
        Exlus�o de material
        Exclus�o de projetos

        Tem como objetivo : Quando se faz um pedido, conclui e vende... ae depois de um certo tempo, ap�s a venda o cliente quer trocar ou devolver
        algum produto por algum motivo...
        As modifica��es s�o salvas quando o usuario clicar em concluir

        Em rela��o a parte financeira
        Quando a troca e a devolu��o resultar em um valor positivo, ou seja, sobrou dinheiro do montante que o cliente pagou,
        � ent�o gerado um credito para o respectivo cliente...
        Quando a troca resultar em um valor negativo, ou seja, faltou dinheiro do montante que o cliente pagou, � gerado um titulo
        a receber do cliente...





}
interface

uses
UessencialGlobal, UobjPedidoObjetos,  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Grids, DBGrids, DB, IBCustomDataSet, IBQuery,
  Buttons,UescolheImagemBotao,UobjMATERIAISTROCA,UobjLANCAMENTOCREDITO,UobjMATERIAISDEVOLUCAO,UFiltraImp,
  ComCtrls;

type
  TFtrocaMaterialpedido = class(TForm)
    PanelPedido: TPanel;
    PanelMateriais: TPanel;
    edtPedido: TEdit;
    MemoPedido: TMemo;
    Dspesquisa: TDataSource;
    QueryPesquisa: TIBQuery;
    MemoNovosValores: TMemo;
    QueryCalculos: TIBQuery;
    edtNovaCor: TEdit;
    pnl1: TPanel;
    Image1: TImage;
    edtCodigoMaterial: TEdit;
    lb1: TLabel;
    pnl3: TPanel;
    lb2: TLabel;
    lbConcluir: TLabel;
    lbCancelar: TLabel;
    lbnomeformulario: TLabel;
    pgc1: TPageControl;
    ts1: TTabSheet;
    pnl4: TPanel;
    btBtExcluirProjeto: TButton;
    btexcluir: TButton;
    btAlterar: TButton;
    btTrocarMaterial: TButton;
    dbgrid: TDBGrid;
    pnl2: TPanel;
    lbInformacaoProcura: TLabel;
    edtPesquisa: TEdit;
    ts2: TTabSheet;
    ts3: TTabSheet;
    StrGridMateriaisTrocados: TStringGrid;
    StrGridMateriaisDevolvidos: TStringGrid;
    lbDescricaoPedido: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btAbrirpedidoClick(Sender: TObject);
    procedure edtPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btcancelarClick(Sender: TObject);
    procedure edtPedidoKeyPress(Sender: TObject; var Key: Char);
    procedure btexcluirClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure btgravarClick(Sender: TObject);
    procedure btBtExcluirProjetoClick(Sender: TObject);
    procedure dbgridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure btAlterarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btTrocarMaterialClick(Sender: TObject);
    procedure dbgridKeyPress(Sender: TObject; var Key: Char);
    procedure edtPesquisaExit(Sender: TObject);
    procedure edtPesquisaKeyPress(Sender: TObject; var Key: Char);
    procedure edtPedidoExit(Sender: TObject);
    procedure lbConcluirMouseLeave(Sender: TObject);
    procedure lbConcluirMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure MemoPedidoClick(Sender: TObject);
    procedure pgc1Change(Sender: TObject);
    procedure edtPedidoChange(Sender: TObject);
    procedure lbDescricaoPedidoClick(Sender: TObject);
    procedure lbDescricaoPedidoMouseLeave(Sender: TObject);
    procedure lbDescricaoPedidoMouseMove(Sender: TObject;
      Shift: TShiftState; X, Y: Integer);
    procedure edtPedidoDblClick(Sender: TObject);
  private
    { Private declarations }
    ObjpedidoObjetos:TObjPedidoObjetos;
    Usouf9napesquisa:boolean;
    PporcentagemDescontoAplicadoPedido:Currency;
    //Guarda o valor do pedido antes de concluir as trocas/devolu��es
    ValorTotalAntesConcluir:Currency;
    //Guarda o valor em andamento
    ValorTotalAlteracoesEmAndamento:Currency;
    procedure AtualizaGrid;
    Function  AtualizaValorProjeto(PpedidoProjeto:string{;Concluido:Str01}):Boolean;
    Function  AtualizaNovosValores:Currency;overload;
    Function  AtualizaNovosValores(var PNovoValorTotal:Currency):Currency;overload;
    Function  RetornavalorTotalAtualizado:Currency;
    function  ResgataMateriais(pedido:string;NOME:string):Boolean;
    function OrganizaMateriaisTrocados():Boolean;
    procedure GravamateriaisDevolvidosparaDevolucaoProjetos;
  public
    { Public declarations }
  end;

var
  FtrocaMaterialpedido: TFtrocaMaterialpedido;
  CampoPesquisaGrid:string;


implementation

uses UDataModulo,
  UobjFERRAGEM_PP_PEDIDO, UobjDIVERSO_PP_PEDIDO, Uprincipal, DateUtils,
  UObjGeraTitulo, UObjPendencia, UPEDIDO;



{$R *.dfm}

procedure TFtrocaMaterialpedido.FormClose(Sender: TObject;
var Action: TCloseAction);
begin
     if (MensagemPergunta('Tem certeza que deseja sair? Se as trocas n�o foram conclu�das o sistema ir� desfazer as altera��es!')=mrno)
     Then Begin
               abort;
               exit;
     End;

     FdataModulo.IBTransaction.RollbackRetaining;

     ObjpedidoObjetos.Free;
end;

procedure TFtrocaMaterialpedido.btAbrirpedidoClick(Sender: TObject);
var
Pvalortitulo,PDesconto,PDescontoBonificacao:Currency;
begin
     if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(edtPedido.Text)=False)
     then Begin
               MensagemErro('Pedido n�o encontrado');
               exit;
     End;

     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;
     if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Concluido='N')
     Then Begin
               MensagemErro('O pedido tem que estar conclu�do!');
               exit;
     End;

     Try
        PvalorTitulo:=strtofloat(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_valortitulo);
     except
        MensagemErro('Valor Inv�lido para o T�tulo');
        exit;
     End;

     Try
        Pdesconto:=strtofloat(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto);
     except
        MensagemErro('Valor Inv�lido para o Desconto');
        exit;
     End;

     Try
        PDescontoBonificacao:=strtofloat(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorBonificacaoCliente);
     except
        MensagemErro('Valor Inv�lido para o valor de desconto da bonifica��o (bonifica��o usada)');
        exit;
     End;


     //tenho um campo ValorTitulo que indica quanto que foi pago pelo cliente
     //isso independente de ter havido troca depois
     //Tenho o Desconto Aplicado e o desconto de  bonificacao usada
     //entao se eu somar valorTitulo+desconto+Descontobonificacao=ValorProdutos
     //Dessa forma tenho como saber quantos % o desconto aplicado representa
     //sobre os produtos vendidos a primeira vez


     Try
        //ValorProdutos    100%
        //Desconto          X
        //X=(Desconto*100)/valorprodutos;

        PporcentagemDescontoAplicadoPedido:=0;
        
        if (PDesconto>0)
        Then PporcentagemDescontoAplicadoPedido:=(  (pdesconto*100)/(Pvalortitulo+PDesconto+PDescontoBonificacao)  );

     Except
           PporcentagemDescontoAplicadoPedido:=0;
     End;
     lbDescricaoPedido.Caption:=ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Descricao;

     PanelMateriais.Enabled:=true;
     memopedido.Lines.Clear;
     memopedido.Lines.Add('Cliente                : '{+ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.get_codigo+'-'}+ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome);
     MemoPedido.Lines.add('Data da Venda          : '+ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_Data);
     MemoPedido.Lines.add('Valor Total            : '+formata_valor(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal));
     MemoPedido.Lines.add('Valor Desconto         : '+formata_valor(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorDesconto));
     MemoPedido.Lines.add('Valor Acr�scimo        : '+formata_valor(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorAcrescimo));
     MemoPedido.Lines.add('Desconto Bonif. Cliente: '+formata_valor(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorBonificacaoCliente));
     MemoPedido.Lines.add('Valor Final            : '+formata_valor(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorFinal));
     MemoPedido.Lines.add('Valor pago pelo Cliente: '+formata_valor(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_valortitulo));
     MemoPedido.Lines.add('Desconto Aplicado      : '+formata_valor(PporcentagemDescontoAplicadoPedido)+'%');
     MemoPedido.Lines.add('Bonifica��o Gerada     : '+formata_valor(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_BonificacaoGerada));

    // PanelPedido.Enabled:=False;
     //btgravar.Enabled:=true;
     //btcancelar.Enabled:=true;
     lbConcluir.Visible:=true;
     lbCancelar.Visible:=true;


     Self.AtualizaGrid;

end;

procedure TFtrocaMaterialpedido.edtPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtPedido_Concluido_KeyDown(sender,key,shift,nil);
end;

procedure TFtrocaMaterialpedido.AtualizaGrid;
begin
     With QueryPesquisa do
     Begin
          close;

          if (edtPedido.Text='')
          Then exit;

          close;
          sql.clear;
          sql.add('Select * from proc_materiais_pp_pedido('+edtPedido.Text+')');
          open;
          formatadbgrid(Dbgrid);
          Self.AtualizaNovosValores;
     End;
end;

procedure TFtrocaMaterialpedido.btcancelarClick(Sender: TObject);
begin
     if (MensagemPergunta('Tem certeza que deseja cancelar as altera��es?')=mrno)
     Then exit;

     FDataModulo.IBTransaction.RollbackRetaining;
     PanelMateriais.Enabled:=False;
     limpaedit(PanelPedido);
     PanelPedido.Enabled:=true;
     Self.AtualizaGrid;
     //btgravar.Enabled:=false;
     //btcancelar.Enabled:=false;
     lbConcluir.Visible:=False;
     lbCancelar.Visible:=False;

     edtPedido.setfocus;

end;

procedure TFtrocaMaterialpedido.edtPedidoKeyPress(Sender: TObject;
  var Key: Char);
begin
     if key=#13
     then btAbrirpedidoClick(sender);
     if not (Key in['0'..'9',Chr(8)]) then
     begin
          Key:= #0;
     end;
end;

procedure TFtrocaMaterialpedido.btexcluirClick(Sender: TObject);
var
PpedidoProjeto:string;
QueryDeleta:TIBQuery;
Objmateriaisdevolucao:TObjMATERIAISDEVOLUCAO;
ProdutoTrocadoNaoConcluido:string;
NovaQuantidade:Integer;

{
          Usado pra devolver materiais de pedidos concluidos...
          Funciona da seguinte maneira, faz a venda a um cliente e por algum motivo
          ele quer devolver algum produto...
          Com o valor do produto devolvido � gerado um cr�dito para o cliente
          ou um titulo a pagar para o cliente

          Se o material que escolher para devolver tiver mais de um, por exemplo tiver uma quantidade de 4
          e quiser devolver apenas 1,2 ou 3, vai aparecer pro cara escolher a parada...

}
begin
     if (Dbgrid.DataSource.DataSet.Active=False)
     then exit;

     if (Dbgrid.DataSource.DataSet.RecordCount=0)
     then exit;

     if (Messagedlg('Tem Certeza que deseja excluir o(a)'+QueryPesquisa.fieldbyname('material').asstring+' '+QueryPesquisa.fieldbyname('nomematerial').AsString +' Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     QueryDeleta:=TIBQuery.Create(nil);
     QueryDeleta.Database:=FDataModulo.IBDatabase;
     Objmateriaisdevolucao:=TObjMATERIAISDEVOLUCAO.Create;
     

     PpedidoProjeto:=QueryPesquisa.fieldbyname('pedidoprojeto').asstring;
     // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
     if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(QueryPesquisa.fieldbyname('pedidoprojeto').asstring)=true)
     then Begin
          MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto ele n�o pode ser alterado');
          exit;
     end;

     // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
     ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
     ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     If (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')
     then Begin
              If (MessageDlg('Este � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                             'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                             'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo)
               Then exit
               Else Begin
                        ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                        ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                        ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False);
               End;
     End;

     try

           //Vejo que tipo de materia que �
           if (QueryPesquisa.Fieldbyname('material').asstring='FERRAGEM') then
           begin
                //se houve alguma troca neste ferragem_PP ent�o exlcluo todo o historico de troca neste ferragem_PP
                //a unica informa��o q sei � esta ferragem foi devolvido, ver se isso � o melhor...
                with QueryDeleta do
                begin
                      //Se tem mais de um material desse tipo, ent�o pergunto quanto vai querer excluir
                      if(QueryPesquisa.fieldbyname('quantidade').AsInteger>1)then
                      begin
                                FfiltroImp.DesativaGrupos;
                                FfiltroImp.Grupo01.Enabled:=True;
                                FfiltroImp.LbGrupo01.Caption:='Quantidade � Devolver';
                                FfiltroImp.edtgrupo01.Text:=QueryPesquisa.fieldbyname('quantidade').AsString;
                                FfiltroImp.ShowModal;
                                if(FfiltroImp.Tag=0)
                                then Exit;

                                //Guardando quantos sobraram
                                NovaQuantidade:=QueryPesquisa.Fieldbyname('quantidade').AsInteger - StrToInt(FfiltroImp.edtgrupo01.Text);

                                //no caso da pessoa digitar mais do que tem
                                //Novaquantidade zera e quantidade devolvida recebe quantos tem...
                                if(NovaQuantidade<0) then
                                begin
                                      NovaQuantidade:=0;
                                      FfiltroImp.edtgrupo01.Text:=QueryPesquisa.Fieldbyname('quantidade').AsString ;
                                end;

                                //Vejo se este material_pp ja sofreu alguma troca que n�o foi conclu�da
                                Close;
                                SQL.Clear;
                                SQL.Add('select * from tabmateriaistroca');
                                SQL.Add('where pedido='+edtPedido.text);
                                SQL.Add('and ferragem_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                                sql.Add('and concluido=''N''');
                                SQL.Add('order by codigo');
                                Open;
                                Last;

                                //guardando qual material foi devolvido
                                Objmateriaisdevolucao.Status:=dsInsert;
                                Objmateriaisdevolucao.Submit_CODIGO('0');
                                Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                //se ja sofreu alguma troca que ainda n�o foi concluida
                                if(recordcount>0) then
                                begin
                                      First;
                                      Objmateriaisdevolucao.Submit_FERRAGEM(fieldbyname('ferragem').asstring);
                                      Objmateriaisdevolucao.Submit_FERRAGEMCOR(fieldbyname('ferragemcor').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end
                                else
                                begin
                                      Objmateriaisdevolucao.Submit_FERRAGEM(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                      Objmateriaisdevolucao.Submit_FERRAGEMCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end;

                                Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                Objmateriaisdevolucao.Submit_MATERIAL('1');
                                Objmateriaisdevolucao.Salvar(false);

                                if(QueryPesquisa.fieldbyname('quantidade').AsString=FfiltroImp.edtgrupo01.text) then
                                begin
                                        Close;
                                        SQL.Clear;
                                        sql.Add('Delete from tabmateriaistroca where ferragem_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ExecSQL;

                                        if(ObjPedidoObjetos.ObjFerragem_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                                        then Exit;
                                end
                                else
                                begin
                                        ObjpedidoObjetos.ObjFerragem_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ObjpedidoObjetos.ObjFerragem_PP.TabelaparaObjeto;
                                        ObjpedidoObjetos.ObjFerragem_PP.Status:=dsEdit;
                                        ObjpedidoObjetos.ObjFerragem_PP.Submit_Quantidade(IntToStr(NovaQuantidade));
                                        ObjpedidoObjetos.ObjFerragem_PP.Salvar(False);
                                end;



                      end
                      //se tiver apenas um material desse tipo, ent�o so excluir
                      else
                      begin
                              //As informa��es de trocas s�o apagadas, � gerado apenas um informa��o de devolu��o

                              Close;
                              SQL.Clear;
                              SQL.Add('select * from tabmateriaistroca');
                              SQL.Add('where pedido='+edtPedido.text);
                              SQL.Add('and ferragem_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                              sql.Add('and concluido=''N''');
                              SQL.Add('order by codigo');
                              Open;
                              Last;

                              //guardando qual material foi devolvido
                              Objmateriaisdevolucao.Status:=dsInsert;
                              Objmateriaisdevolucao.Submit_CODIGO('0');
                              Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                              if(recordcount>0) then
                              begin
                                    First;
                                    Objmateriaisdevolucao.Submit_FERRAGEM(fieldbyname('ferragem').asstring);
                                    Objmateriaisdevolucao.Submit_FERRAGEMCOR(fieldbyname('ferragemcor').asstring);
                                    Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                              end
                              else
                              begin
                                    Objmateriaisdevolucao.Submit_FERRAGEM(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                    Objmateriaisdevolucao.Submit_FERRAGEMCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                    Objmateriaisdevolucao.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('quantidade').AsString);
                              end;

                              Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                              Objmateriaisdevolucao.Submit_MATERIAL('1');
                              Objmateriaisdevolucao.Salvar(false);


                              Close;
                              SQL.Clear;
                              sql.Add('Delete from tabmateriaistroca where ferragem_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                              ExecSQL;

                              if(ObjPedidoObjetos.ObjFerragem_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                              then Exit;
                      end;
                end;
           end;

           if (QueryPesquisa.Fieldbyname('material').asstring='PERFILADO') then
           begin
                with QueryDeleta do
                begin
                      //Se tiver mais de um desse mesmo perfilado
                      if(QueryPesquisa.fieldbyname('quantidade').AsInteger>1)then
                      begin
                                FfiltroImp.DesativaGrupos;
                                FfiltroImp.Grupo01.Enabled:=True;
                                FfiltroImp.LbGrupo01.Caption:='Quantidade � Devolver';
                                FfiltroImp.edtgrupo01.Text:=QueryPesquisa.fieldbyname('quantidade').AsString;
                                FfiltroImp.ShowModal;
                                if(FfiltroImp.Tag=0)
                                then Exit;

                                //Guardando quantos sobraram
                                NovaQuantidade:=QueryPesquisa.Fieldbyname('quantidade').AsInteger - StrToInt(FfiltroImp.edtgrupo01.Text);

                                //no caso da pessoa digitar mais do que tem
                                //Novaquantidade zera e quantidade devolvida recebe quantos tem...
                                if(NovaQuantidade<0) then
                                begin
                                      NovaQuantidade:=0;
                                      FfiltroImp.edtgrupo01.Text:=QueryPesquisa.Fieldbyname('quantidade').AsString ;
                                end;
                                Close;
                                SQL.Clear;
                                SQL.Add('select * from tabmateriaistroca');
                                SQL.Add('where pedido='+edtPedido.text);
                                SQL.Add('and Perfilado_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                                sql.Add('and concluido=''N''');
                                SQL.Add('order by codigo');
                                Open;
                                Last;

                                //guardando qual material foi devolvido
                                Objmateriaisdevolucao.Status:=dsInsert;
                                Objmateriaisdevolucao.Submit_CODIGO('0');
                                Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                if(recordcount>0) then
                                begin
                                      First;
                                      Objmateriaisdevolucao.Submit_Perfilado(fieldbyname('Perfilado').asstring);
                                      Objmateriaisdevolucao.Submit_PerfiladoCOR(fieldbyname('Perfiladocor').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end
                                else
                                begin
                                      Objmateriaisdevolucao.Submit_Perfilado(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                      Objmateriaisdevolucao.Submit_PerfiladoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end;

                                Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                Objmateriaisdevolucao.Submit_MATERIAL('2');
                                Objmateriaisdevolucao.Salvar(false);

                                //se a quantidade digitada for igual, ent�o exclui tudo
                                if(QueryPesquisa.fieldbyname('quantidade').AsString=FfiltroImp.edtgrupo01.text) then
                                begin
                                        Close;
                                        SQL.Clear;
                                        sql.Add('Delete from tabmateriaistroca where Perfilado_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ExecSQL;

                                        if(ObjPedidoObjetos.ObjPerfilado_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                                        then Exit;
                                end
                                else
                                begin
                                        ObjpedidoObjetos.ObjPerfilado_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ObjpedidoObjetos.ObjPerfilado_PP.TabelaparaObjeto;
                                        ObjpedidoObjetos.ObjPerfilado_PP.Status:=dsEdit;
                                        ObjpedidoObjetos.ObjPerfilado_PP.Submit_Quantidade(IntToStr(NovaQuantidade));
                                        ObjpedidoObjetos.ObjPerfilado_PP.Salvar(False);
                                end;


                      end
                      else
                      begin
                                Close;
                                SQL.Clear;
                                SQL.Add('select * from tabmateriaistroca');
                                SQL.Add('where pedido='+edtPedido.text);
                                SQL.Add('and Perfilado_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                                sql.Add('and concluido=''N''');
                                SQL.Add('order by codigo');
                                Open;
                                Last;

                                //guardando qual material foi devolvido
                                Objmateriaisdevolucao.Status:=dsInsert;
                                Objmateriaisdevolucao.Submit_CODIGO('0');
                                Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                if(recordcount>0) then
                                begin
                                      First;
                                      Objmateriaisdevolucao.Submit_Perfilado(fieldbyname('Perfilado').asstring);
                                      Objmateriaisdevolucao.Submit_PerfiladoCOR(fieldbyname('Perfiladocor').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end
                                else
                                begin
                                      Objmateriaisdevolucao.Submit_Perfilado(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                      Objmateriaisdevolucao.Submit_PerfiladoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('quantidade').AsString);
                                end;

                                Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                Objmateriaisdevolucao.Submit_MATERIAL('2');
                                Objmateriaisdevolucao.Salvar(false);

                                Close;
                                SQL.Clear;
                                sql.Add('Delete from tabmateriaistroca where Perfilado_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                                ExecSQL;

                                if(ObjPedidoObjetos.ObjPerfilado_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                                then Exit;

                      end;
                end;
           end;

           if (QueryPesquisa.Fieldbyname('material').asstring='VIDRO') Then
           begin
                with QueryDeleta do
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select * from tabmateriaistroca');
                      SQL.Add('where pedido='+edtPedido.text);
                      SQL.Add('and Vidro_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                      sql.Add('and concluido=''N''');
                      SQL.Add('order by codigo');
                      Open;
                      Last;

                      //guardando qual material foi devolvido
                      Objmateriaisdevolucao.Status:=dsInsert;
                      Objmateriaisdevolucao.Submit_CODIGO('0');
                      Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                      if(recordcount>0) then
                      begin
                            First;
                            Objmateriaisdevolucao.Submit_Vidro(fieldbyname('Vidro').asstring);
                            Objmateriaisdevolucao.Submit_VidroCOR(fieldbyname('Vidrocor').asstring);
                            Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                      end
                      else
                      begin
                            Objmateriaisdevolucao.Submit_Vidro(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                            Objmateriaisdevolucao.Submit_VidroCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                            Objmateriaisdevolucao.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('quantidade').AsString);
                      end;

                      Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                      Objmateriaisdevolucao.Submit_MATERIAL('3');
                      Objmateriaisdevolucao.Salvar(false);

                      Close;
                      SQL.Clear;
                      sql.Add('Delete from tabmateriaistroca where Vidro_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ExecSQL;

                      if(ObjPedidoObjetos.ObjVidro_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                      then Exit;
                end;

           end;

           if (QueryPesquisa.Fieldbyname('material').asstring='KITBOX') Then
           begin
                with QueryDeleta do
                begin
                      if(QueryPesquisa.fieldbyname('quantidade').AsInteger>1)then
                      begin
                                FfiltroImp.DesativaGrupos;
                                FfiltroImp.Grupo01.Enabled:=True;
                                FfiltroImp.LbGrupo01.Caption:='Quantidade � Devolver';
                                FfiltroImp.edtgrupo01.Text:=QueryPesquisa.fieldbyname('quantidade').AsString;
                                FfiltroImp.ShowModal;
                                if(FfiltroImp.Tag=0)
                                then Exit;

                                //Guardando quantos sobraram
                                NovaQuantidade:=QueryPesquisa.Fieldbyname('quantidade').AsInteger - StrToInt(FfiltroImp.edtgrupo01.Text);

                                //no caso da pessoa digitar mais do que tem
                                //Novaquantidade zera e quantidade devolvida recebe quantos tem...
                                if(NovaQuantidade<0) then
                                begin
                                      NovaQuantidade:=0;
                                      FfiltroImp.edtgrupo01.Text:=QueryPesquisa.Fieldbyname('quantidade').AsString ;
                                end;
                                Close;
                                SQL.Clear;
                                SQL.Add('select * from tabmateriaistroca');
                                SQL.Add('where pedido='+edtPedido.text);
                                SQL.Add('and KitBox_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                                sql.Add('and concluido=''N''');
                                SQL.Add('order by codigo');
                                Open;
                                Last;

                                //guardando qual material foi devolvido
                                Objmateriaisdevolucao.Status:=dsInsert;
                                Objmateriaisdevolucao.Submit_CODIGO('0');
                                Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                if(recordcount>0) then
                                begin
                                      First;
                                      Objmateriaisdevolucao.Submit_KitBox(fieldbyname('KitBox').asstring);
                                      Objmateriaisdevolucao.Submit_KitBoxCOR(fieldbyname('KitBoxcor').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end
                                else
                                begin
                                      Objmateriaisdevolucao.Submit_KitBox(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                      Objmateriaisdevolucao.Submit_KitBoxCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end;

                                Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                Objmateriaisdevolucao.Submit_MATERIAL('4');
                                Objmateriaisdevolucao.Salvar(false);

                                //se a quantidade digitada for igual, ent�o exclui tudo
                                if(QueryPesquisa.fieldbyname('quantidade').AsString=FfiltroImp.edtgrupo01.text) then
                                begin
                                        Close;
                                        SQL.Clear;
                                        sql.Add('Delete from tabmateriaistroca where KitBox_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ExecSQL;

                                        if(ObjPedidoObjetos.ObjKitBox_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                                        then Exit;
                                end
                                else
                                begin
                                        ObjpedidoObjetos.ObjKitBox_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ObjpedidoObjetos.ObjKitBox_PP.TabelaparaObjeto;
                                        ObjpedidoObjetos.ObjKitBox_PP.Status:=dsEdit;
                                        ObjpedidoObjetos.ObjKitBox_PP.Submit_Quantidade(IntToStr(NovaQuantidade));
                                        ObjpedidoObjetos.ObjKitBox_PP.Salvar(False);
                                end;


                      end
                      else
                      begin
                                Close;
                                SQL.Clear;
                                SQL.Add('select * from tabmateriaistroca');
                                SQL.Add('where pedido='+edtPedido.text);
                                SQL.Add('and KitBox_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                                sql.Add('and concluido=''N''');
                                SQL.Add('order by codigo');
                                Open;
                                Last;

                                //guardando qual material foi devolvido
                                Objmateriaisdevolucao.Status:=dsInsert;
                                Objmateriaisdevolucao.Submit_CODIGO('0');
                                Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                if(recordcount>0) then
                                begin
                                      First;
                                      Objmateriaisdevolucao.Submit_KitBox(fieldbyname('KitBox').asstring);
                                      Objmateriaisdevolucao.Submit_KitBoxCOR(fieldbyname('KitBoxcor').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                                end
                                else
                                begin
                                      Objmateriaisdevolucao.Submit_KitBox(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                      Objmateriaisdevolucao.Submit_KitBoxCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('quantidade').AsString);
                                end;

                                Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                Objmateriaisdevolucao.Submit_MATERIAL('4');
                                Objmateriaisdevolucao.Salvar(false);

                                Close;
                                SQL.Clear;
                                sql.Add('Delete from tabmateriaistroca where KitBox_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                                ExecSQL;

                                if(ObjPedidoObjetos.ObjKitBox_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                                then Exit;

                      end;

                end;

           end;

           if (QueryPesquisa.Fieldbyname('material').asstring='PERSIANA') then
           begin
                with QueryDeleta do
                begin
                      Close;
                      SQL.Clear;
                      SQL.Add('select * from tabmateriaistroca');
                      SQL.Add('where pedido='+edtPedido.text);
                      SQL.Add('and Persiana_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                      sql.Add('and concluido=''N''');
                      SQL.Add('order by codigo');
                      Open;
                      Last;

                      //guardando qual material foi devolvido
                      Objmateriaisdevolucao.Status:=dsInsert;
                      Objmateriaisdevolucao.Submit_CODIGO('0');
                      Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                      if(recordcount>0) then
                      begin
                            First;
                            Objmateriaisdevolucao.Submit_Persiana(fieldbyname('Persiana').asstring);
                            Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                      end
                      else
                      begin
                            Objmateriaisdevolucao.Submit_Persiana(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                            Objmateriaisdevolucao.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('quantidade').AsString);
                      end;

                      Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                      Objmateriaisdevolucao.Submit_MATERIAL('6');
                      Objmateriaisdevolucao.Salvar(false);

                      Close;
                      SQL.Clear;
                      sql.Add('Delete from tabmateriaistroca where Persiana_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ExecSQL;

                      if(ObjPedidoObjetos.ObjPersiana_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                      then Exit;
                end;

           end;

           if (QueryPesquisa.Fieldbyname('material').asstring='DIVERSO') then
           begin
                with QueryDeleta do
                begin
                      if(QueryPesquisa.fieldbyname('quantidade').AsInteger>1)then
                      begin
                                FfiltroImp.DesativaGrupos;
                                FfiltroImp.Grupo01.Enabled:=True;
                                FfiltroImp.LbGrupo01.Caption:='Quantidade � Devolver';
                                FfiltroImp.edtgrupo01.Text:=QueryPesquisa.fieldbyname('quantidade').AsString;
                                FfiltroImp.ShowModal;
                                if(FfiltroImp.Tag=0)
                                then Exit;

                                //Guardando quantos sobraram
                                NovaQuantidade:=QueryPesquisa.Fieldbyname('quantidade').AsInteger - StrToInt(FfiltroImp.edtgrupo01.Text);

                                //no caso da pessoa digitar mais do que tem
                                //Novaquantidade zera e quantidade devolvida recebe quantos tem...
                                if(NovaQuantidade<0) then
                                begin
                                      NovaQuantidade:=0;
                                      FfiltroImp.edtgrupo01.Text:=QueryPesquisa.Fieldbyname('quantidade').AsString ;
                                end;
                                Close;
                                SQL.Clear;
                                SQL.Add('select * from tabmateriaistroca');
                                SQL.Add('where pedido='+edtPedido.text);
                                SQL.Add('and Diverso_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                                sql.Add('and concluido=''N''');
                                SQL.Add('order by codigo');
                                Open;
                                Last;

                                //guardando qual material foi devolvido
                                Objmateriaisdevolucao.Status:=dsInsert;
                                Objmateriaisdevolucao.Submit_CODIGO('0');
                                Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                if(recordcount>0) then
                                begin
                                      First;
                                      Objmateriaisdevolucao.Submit_Diverso(fieldbyname('Diverso').asstring);
                                      Objmateriaisdevolucao.Submit_DiversoCOR(fieldbyname('Diversocor').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                end
                                else
                                begin
                                      Objmateriaisdevolucao.Submit_Diverso(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                      Objmateriaisdevolucao.Submit_DiversoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('quantidade').AsString);
                                end;

                                Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                Objmateriaisdevolucao.Submit_MATERIAL('5');
                                Objmateriaisdevolucao.Salvar(false);

                                //se a quantidade digitada for igual, ent�o exclui tudo
                                if(QueryPesquisa.fieldbyname('quantidade').AsString=FfiltroImp.edtgrupo01.text) then
                                begin
                                        Close;
                                        SQL.Clear;
                                        sql.Add('Delete from tabmateriaistroca where Diverso_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ExecSQL;

                                        if(ObjPedidoObjetos.ObjDiverso_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                                        then Exit;
                                end
                                else
                                begin
                                        ObjpedidoObjetos.ObjDiverso_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                                        ObjpedidoObjetos.ObjDiverso_PP.TabelaparaObjeto;
                                        ObjpedidoObjetos.ObjDiverso_PP.Status:=dsEdit;
                                        ObjpedidoObjetos.ObjDiverso_PP.Submit_Quantidade(IntToStr(NovaQuantidade));
                                        ObjpedidoObjetos.ObjDiverso_PP.Salvar(False);
                                end;


                      end
                      else
                      begin
                                Close;
                                SQL.Clear;
                                SQL.Add('select * from tabmateriaistroca');
                                SQL.Add('where pedido='+edtPedido.text);
                                SQL.Add('and Diverso_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                                sql.Add('and concluido=''N''');
                                SQL.Add('order by codigo');
                                Open;
                                Last;

                                //guardando qual material foi devolvido
                                Objmateriaisdevolucao.Status:=dsInsert;
                                Objmateriaisdevolucao.Submit_CODIGO('0');
                                Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                if(recordcount>0) then
                                begin
                                      First;
                                      Objmateriaisdevolucao.Submit_Diverso(fieldbyname('Diverso').asstring);
                                      Objmateriaisdevolucao.Submit_DiversoCOR(fieldbyname('Diversocor').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                                end
                                else
                                begin
                                      Objmateriaisdevolucao.Submit_Diverso(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                                      Objmateriaisdevolucao.Submit_DiversoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                                      Objmateriaisdevolucao.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('quantidade').AsString);
                                end;

                                Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                Objmateriaisdevolucao.Submit_MATERIAL('5');
                                Objmateriaisdevolucao.Salvar(false);

                                Close;
                                SQL.Clear;
                                sql.Add('Delete from tabmateriaistroca where Diverso_pp='+QueryPesquisa.Fieldbyname('codigopp').asstring);
                                ExecSQL;

                                if(ObjPedidoObjetos.ObjDiverso_PP.Exclui(QueryPesquisa.Fieldbyname('codigopp').asstring,False)=False)
                                then Exit;

                      end;




                end;

           end;

     finally
           Objmateriaisdevolucao.Free;
           FreeAndNil(QueryDeleta);
     end;
     Self.AtualizaGrid;

    Self.AtualizavalorProjeto(PpedidoProjeto{,'N'});
end;

procedure TFtrocaMaterialpedido.FormKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);

   end;

procedure TFtrocaMaterialpedido.btgravarClick(Sender: TObject);
var
PnovoValorTotal,PBonificacaoGerada,PBonificacaoInicial:Currency;
PsaldoAtualCliente:Currency;
ObjlancamentoCredito:TObjLANCAMENTOCREDITO;
QueryCredito:TIBQuery;
Objgeratitulo:TObjGeraTitulo;
ObjPendencia:TobjPendencia;
codigotitulo:string;
SaldoAnterior:Currency;
GeraBonificacao:string;
GeraTituloReceber:string;
begin
     {
          Ap�s terminar as altera��es necess�rias, ao concluir
          Aqui � commitado as altera��es, gerado bonifica��es e titulos a receber dependendo do caso
          Funciona da seguinte maneira...
          Apos concluir, as trocas s�o efetuadas...
          verifica os parametros para gera��o de bonifica��es ou titulos a receber...
          caso esteja habilitado faz =P .....


     }


     if(ObjParametroGlobal.ValidaParametro('GERAR BONIFICA��O AO CLIENTE NA TROCA DE MATERIAIS EM PEDIDOS CONCLU�DOS')=False)then
     begin
           MensagemErro('O par�metro "GERAR BONIFICA��O AO CLIENTE NA TROCA DE MATERIAIS EM PEDIDOS CONCLU�DOS" n�o foi encontrado.');
           exit;
     end;
     GeraBonificacao:=ObjParametroGlobal.Get_Valor;

     
     if(ObjParametroGlobal.ValidaParametro('GERAR TITULO A RECEBER DO CLIENTE NA TROCA DE MATERIAIS EM PEDIDOS CONCLU�DOS')=False) then
     begin
           MensagemErro('O par�metro "GERAR TITULO A RECEBER DO CLIENTE NA TROCA DE MATERIAIS EM PEDIDOS CONCLU�DOS" n�o foi encontrado.');
           exit;
     end;
     GeraTituloReceber:=ObjParametroGlobal.Get_Valor;



     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.LocalizaCodigo(edtPedido.Text);
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.TabelaparaObjeto;
     ObjlancamentoCredito:=TObjLANCAMENTOCREDITO.Create;
     QueryCredito:=TIBQuery.Create(nil);
     QueryCredito.Database:=FDataModulo.IBDatabase;
     
     Try
          PBonificacaoInicial:=Strtofloat(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.Get_BonificacaoGerada);
     Except
           PBonificacaoInicial:=0;
     End;
     //Busca o credito que o cliente possui
     Try
          with QueryCredito do
          begin
                Close;
                sql.Clear;
                sql.Add('select sum(valor) from tablancamentocredito where cliente='+ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.cliente.Get_Codigo);
                Open;
                PsaldoAtualCliente:=fieldbyname('sum').AsCurrency;
          end;
     finally
                FreeAndNil(QueryCredito);
     End;

     //Organizando os materiais trocado na tabmateriaistroca
     if(OrganizaMateriaisTrocados=False)
     then Exit; 
     
     //retorna a bonificacao e o novo valor total
     PBonificacaoGerada:=Self.AtualizaNovosValores(PnovoValorTotal);
     try
              //Lan�ando um credito pro cliente no caso de Saldo Positivo
              if(PBonificacaoGerada>=0)  then
              begin
                    //Se foi gerado uma bonifica��o positiva, ou seja, sobrou dinheiro do que o cliente pagou
                    //ent�o gera uma cr�dito para o cliente
                    //se o parametro para gera��o de bonifica��o estiver como sim, ent�o aumenta o cr�dito do cliente
                    if(GeraBonificacao='SIM') then
                    begin
                          ObjlancamentoCredito.ZerarTabela;
                          ObjlancamentoCredito.Status:=dsInsert;
                          ObjlancamentoCredito.Submit_Codigo('0');
                          ObjlancamentoCredito.Submit_Data(DateToStr(Now));
                          ObjlancamentoCredito.Submit_Cliente(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Codigo);
                          ObjlancamentoCredito.Submit_Valor(Floattostr(PBonificacaoGerada));
                          ObjlancamentoCredito.Submit_Pedido(edtPedido.Text);
                          ObjlancamentoCredito.Submit_Historico('Bonifica��o gerada em troca de materiais ou Devolu��o em Pedidos Conclu�dos');
                          if(ObjlancamentoCredito.Salvar(False)=False)
                          then Exit;
                    end;
              end
              else
              begin
                    //Se a bonifica��o for negativa, ou seja faltou dinheiro do que o cliente pagou
                    //ent�o guarda o cr�dito do cliente at� o momento
                    SaldoAnterior:=PsaldoAtualCliente;
                    //subtrai do cr�dito do cliente o valor da bonifica��o
                    PsaldoAtualCliente:=PsaldoAtualCliente+PBonificacaoGerada;
                    //se a bonifica��o for negativa, ou seja, o cliente n�o tem cr�dito suficiente pra cobrir os novos gastos
                    if (PSaldoAtualCliente<0)
                    then begin
                            //Verifica se o parametro para gerar um titulo a receber do cliente esta como sim, se sim
                            //gera um titulo a pagar, sendo que o valor � (Valor do credito do cliente anterior - Bonifica��o)
                            if(GeraTituloReceber='SIM') then
                            begin
                                  if (MessageDlg('Sera gerado um titulo a receber do Cliente em : R$ '+formata_valor(Psaldoatualcliente*(-1))+#13+'Tem certeza que deseja concluir?',mtconfirmation,[mbyes,mbno],0)=mrno)
                                  Then exit;
                                  //Lan�ando um titulo a receber deste cliente..
                                  Objgeratitulo:=TObjGeraTitulo.create;
                                  ObjPendencia:=TobjPendencia.Create;
                                  try
                                          If (Objgeratitulo.LocalizaHistorico('TITULO GERANDO PELO MODULO DE TROCA EM PEDIDOS CONCLUIDOS')=False)
                                          Then Begin
                                                    Messagedlg('O GERATITULO de hist�rico="TITULO GERANDO PELO MODULO DE TROCA EM PEDIDOS CONCLUIDOS" n�o foi encontrado!',mterror,[mbok],0);
                                                    exit;
                                          End;
                                          Objgeratitulo.TabelaparaObjeto;
                                          ObjPendencia.Titulo.ZerarTabela;
                                          ObjPendencia.Titulo.Status:=dsinsert;
                                          codigotitulo:=ObjPendencia.Titulo.get_novocodigo;
                                          ObjPendencia.Titulo.Submit_CODIGO(codigotitulo);
                                          ObjPendencia.Titulo.Submit_HISTORICO('TROCA PARA '+ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Nome+' - N� '+ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_codigo);
                                          ObjPendencia.Titulo.Submit_GERADOR(Objgeratitulo.Get_Gerador);
                                          ObjPendencia.Titulo.Submit_CODIGOGERADOR(Objgeratitulo.Get_CodigoGerador);
                                          ObjPendencia.Titulo.Submit_CREDORDEVEDOR(Objgeratitulo.Get_CredorDevedor);
                                          ObjPendencia.Titulo.Submit_CODIGOCREDORDEVEDOR(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.get_codigo);
                                          ObjPendencia.Titulo.Submit_EMISSAO(Datetostr(Now));
                                          ObjPendencia.Titulo.Submit_PRAZO(Objgeratitulo.Get_Prazo);
                                          ObjPendencia.Titulo.Submit_PORTADOR(Objgeratitulo.get_portador);
                                          ObjPendencia.Titulo.Submit_VALOR(CurrToStr(PSaldoAtualCliente*(-1)));
                                          ObjPendencia.Titulo.Submit_CONTAGERENCIAL(Objgeratitulo.Get_ContaGerencial);
                                          ObjPendencia.Titulo.Submit_NUMDCTO(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_codigo);//vai o numero do pedido
                                          ObjPendencia.Titulo.Submit_GeradoPeloSistema('N');
                                          ObjPendencia.Titulo.Submit_ParcelasIguais(True);
                                          if (ObjPendencia.Titulo.salvar(false,true)=False)//nao exporta a contabilidade pois vamos exportar com estoque por esse modulo
                                          Then Begin
                                                    Messagedlg('Erro na tentativa de Gerar o T�tulo',mterror,[mbok],0);
                                                    exit;
                                          End;
                                          //Descontando do cr�dito do cliente...
                                          //ou seja, zera o cr�dito do cliente, tendo em vista que o valor do cr�dito ja foi utilizado
                                          ObjlancamentoCredito.ZerarTabela;
                                          ObjlancamentoCredito.Status:=dsInsert;
                                          ObjlancamentoCredito.Submit_Codigo('0');
                                          ObjlancamentoCredito.Submit_Data(DateToStr(Now));
                                          ObjlancamentoCredito.Submit_Cliente(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Codigo);
                                          ObjlancamentoCredito.Submit_Valor(Floattostr(SaldoAnterior*(-1)));
                                          ObjlancamentoCredito.Submit_Pedido(edtPedido.Text);
                                          ObjlancamentoCredito.Submit_Historico('Bonifica��o gerada em troca de materiais ou Devolu��o em Pedidos Conclu�dos');
                                          if(ObjlancamentoCredito.Salvar(False)=False)
                                          then Exit;
                                  finally
                                         Objgeratitulo.free;
                                         ObjPendencia.free;
                                  end;
                            end;

                    end
                    else
                    begin
                            if(GeraBonificacao='SIM')then
                            begin
                                  //se o cliente ja tem credito, ent�o debita do credito do cliente o valor da bonifica��o
                                  ObjlancamentoCredito.ZerarTabela;
                                  ObjlancamentoCredito.Status:=dsInsert;
                                  ObjlancamentoCredito.Submit_Codigo('0');
                                  ObjlancamentoCredito.Submit_Data(DateToStr(Now));
                                  ObjlancamentoCredito.Submit_Cliente(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Cliente.Get_Codigo);
                                  ObjlancamentoCredito.Submit_Valor(Floattostr(PBonificacaoGerada));
                                  ObjlancamentoCredito.Submit_Pedido(edtPedido.Text);
                                  ObjlancamentoCredito.Submit_Historico('Bonifica��o gerada em troca de materiais ou Devolu��o em Pedidos Conclu�dos');
                                  if(ObjlancamentoCredito.Salvar(False)=False)
                                  then Exit;
                            end;
                    end;
              end;
     finally
              ObjlancamentoCredito.Free;
     end;
     //Excluindo os pedidos projetos que ficaram sem produtos
     if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.excluir_sem_produtos(edtpedido.Text)=False)
     Then Begin
               MensagemErro('N�o foi poss�vel excluir os pedidos projetos em branco');
               FdataModulo.IBTransaction.RollbackRetaining;
               exit;
     End;
     if(GeraBonificacao='SIM')then
     begin
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.Status:=dsedit;
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.Submit_ValorTotal(Floattostr(PNovoValorTotal));
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.Submit_BonificacaoGerada(FloatToStr(PBonificacaoInicial+PBonificacaoGerada));
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.Submit_AlteradoporTroca('S');
             //Salvo sem verificar o desconto
             if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.pedido.Salvar(false,false)=False)
             Then Begin
                       MensagemErro('N�o foi poss�vel salvar as altera��es do pedido');
                       FdataModulo.IBTransaction.RollbackRetaining;
                       exit;
             End;
             //exportando contabilidade
             if (ObjpedidoObjetos.ExportaContabilidade_TROCA(edtPedido.Text,PBonificacaoGerada)=False)
             then begin
                       mensagemErro('Erro na tentativa de lan�ar a contabilidade da TROCA');
                       FdataModulo.IBTransaction.RollbackRetaining;
                       exit;
             End;
             //Atualizar as comiss�es do pedido que sofreu as altera��es
             ObjpedidoObjetos.AtualizaComissaoTrocaMateriaisPedConcluido(edtPedido.Text,CurrToStr(PBonificacaoGerada));
     End;

     //Concluindo
     FdataModulo.IBTransaction.CommitRetaining;
     MensagemAviso('Conclu�do');
     btAbrirpedidoClick(sender);
end;

function TFtrocaMaterialpedido.AtualizaValorProjeto(
  PpedidoProjeto: string{;Concluido:Str01}): Boolean;
var
Pvalor:Currency;
Ppedido:string;
PvalorAnterior:Currency;
begin
     result:=False;

     if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(PpedidoProjeto)=False)
     Then begin
               MensagemErro('Projeto n�o localizado para ser exclu�do');
               exit;
     End;
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

     pvalor:=0;
     Pvalor:=pvalor+ObjpedidoObjetos.ObjFerragem_PP .Soma_por_PP(PpedidoProjeto);
     Pvalor:=pvalor+ObjpedidoObjetos.ObjPerfilado_PP.Soma_por_PP(PpedidoProjeto);
     Pvalor:=pvalor+ObjpedidoObjetos.ObjVidro_PP    .Soma_por_PP(PpedidoProjeto);
     Pvalor:=pvalor+ObjpedidoObjetos.ObjKitBox_PP   .Soma_por_PP(PpedidoProjeto);
     Pvalor:=pvalor+ObjpedidoObjetos.ObjServico_PP  .Soma_por_PP(PpedidoProjeto);
     Pvalor:=pvalor+ObjpedidoObjetos.ObjPersiana_PP .Soma_por_PP(PpedidoProjeto);
     Pvalor:=pvalor+ObjpedidoObjetos.ObjDiverso_PP  .Soma_por_PP(PpedidoProjeto);

     //apos gravar em todas as tabelas, preciso acertar o valor total
     //na tabela de pedidoprojeto, que seria a soma de todas as tabelas

      //PARA TESTES
      //AKI JA GRAVAVA DIRETO NA TABPEDIDO_PROJ
      //A IDEIA � SO ATUALIZAR DEPOIS QUE CONCLUIR
      //VO USAR UMA VARIAVEL PRA CONTROLAR ESSE VALOR AO INVES DE USAR DO BANCO

     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(ppedidoprojeto);
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
     PvalorAnterior:=StrToCurr(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_ValorTotal);
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_ValorTotal(floattostr(Pvalor));
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.status:=dsedit;

     if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False)=False)
     Then Begin
               Messagedlg('Erro na Tentativa de Gravar o Valor Total nesse Projeto',mterror,[mbok],0);
               exit;
     End;

     Self.AtualizaGrid;

     {if(Concluido='N') then
     begin
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(ppedidoprojeto);
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_ValorTotal(floattostr(PvalorAnterior));
             ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.status:=dsedit;

             if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False)=False)
             Then Begin
                       Messagedlg('Erro na Tentativa de Gravar o Valor Total nesse Projeto',mterror,[mbok],0);
                       exit;
             End;
     end;     }


end;

procedure TFtrocaMaterialpedido.btBtExcluirProjetoClick(Sender: TObject);
var
PpedidoProjeto:string;
QueryDeleta:TIBQuery;
begin

     {

         Devolvendo um projeto, ou seja, devolve todos os materias do respectivo projeto
         por exemplo
                    Porta de correr {Ferragens,vidro, diversos), todos os materiais s�o devolvidos
     }

     QueryDeleta:=TIBQuery.Create(nil);
     QueryDeleta.Database:=FDataModulo.IBDatabase;

     try
             if (Dbgrid.DataSource.DataSet.Active=False)
             then exit;

             if (Dbgrid.DataSource.DataSet.RecordCount=0)
             then exit;

             if (Messagedlg('Tem Certeza que deseja excluir o Pedido Projeto N�mero '+QueryPesquisa.fieldbyname('pedidoprojeto').asstring,mtconfirmation,[mbyes,mbno],0)=mrno)
             Then exit;

             PpedidoProjeto:=QueryPesquisa.fieldbyname('pedidoprojeto').asstring;

             //Se este � o projeto principal naum pode ser alterado em hip�tese alguma
             if (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(ppedidoprojeto)=true)
             then Begin
                  MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto ele n�o pode ser exclu�do');
                  exit;
             end;

             //Se houve alguma troca neste pedido projeto, ent�o tenho que remover todos essa dependencias
             //Quando se faz a troca grava na tabmateriaistroca, mas quando devolve, guarda na tabmateriaisdevolu��o
             //quando vou excluir, excluo todos os materias...
             //tenho que achar uma maneira mais correta de fazer isso, por ora, serve
              try
                   with QueryDeleta do
                   begin
                         Close;
                         SQL.Clear;
                         sql.Add('Delete from tabmateriaistroca where pedidoprojeto='+QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
                         sql.Add('and pedido='+edtPedido.Text);
                         ExecSQL;

                         GravamateriaisDevolvidosparaDevolucaoProjetos;
                   end;
              finally
                   FreeAndNil(QueryDeleta);
              end;

             //Este pedidoprojeto foi excluido, ent�o preciso retornar a comiss�o do mesmo
             if (ObjPedidoObjetos.RetornaComissaoColocadorPedidoProjeto(ppedidoprojeto)=False)
             then begin
                       FDataModulo.IBTransaction.RollbackRetaining;
                       MensagemErro('Erro na tentativa de Apagar a Comiss�o do Colocador do Pedido projeto');
                       exit;
             End;

             if (ObjPedidoObjetos.ExcluiPedidoProjeto_sem_atualizar_valor_total(ppedidoprojeto)=False)
             Then FDataModulo.IBTransaction.RollbackRetaining;

             Self.AtualizaGrid;
     finally
             FreeAndNil(QueryDeleta);
     end;



end;

function TFtrocaMaterialpedido.AtualizaNovosValores:Currency;
var
temp:Currency;
begin
     Result:=Self.AtualizaNovosValores(temp);
end;

Function TFtrocaMaterialpedido.AtualizaNovosValores(var PNovoValorTotal: Currency): Currency;
var
  PValortotal:Currency;
  Pdiferenca,PBonificacaoGerada:Currency;
begin
     result:=0;

     MemoNovosValores.Lines.clear;
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.LocalizaCodigo(edtPedido.text);
     ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.TabelaparaObjeto;

     Pnovovalortotal:=Self.RetornavalorTotalAtualizado;

     Try
        PValorTotal:=Strtofloat(ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Pedido.Get_ValorTotal);
     Except
        PValorTotal:=0;
     End;
     
     Pdiferenca:=PvalorTotal-PnovoValorTotal;

     MemoNovosValores.Lines.add(' ');
     MemoNovosValores.Lines.add('Valor dos Produtos At. : '+formata_valor(PNovoValorTotal));
     MemoNovosValores.Lines.add('Diferen�a              : '+formata_valor(Pdiferenca));
     //A bonificacao � o total devido desconto a % de desconto do pedido
     //exemplo se o pedido teve 10% de desconto
     //Se eu devolver um produto de R$ 500,00 somente R$ 450,00 � bonificado ao cliente
     PBonificacaoGerada:=pdiferenca-((Pdiferenca*PporcentagemDescontoAplicadoPedido)/100);
     MemoNovosValores.Lines.add('Bonifica��o Gerada     : '+formata_valor(PBonificacaoGerada));
     result:=PBonificacaoGerada;
     
end;


function TFtrocaMaterialpedido.RetornavalorTotalAtualizado: Currency;
begin
     With QueryCalculos do
     Begin
          close;

          if (edtPedido.Text='')
          Then exit;

          sql.clear;
          sql.add('Select sum(tabPedido_Proj.valorfinal) as SOMA from TabPedido_Proj');
          sql.add('join tabpedido on tabPedido_Proj.Pedido=tabPedido.codigo');
          sql.add('Where tabPedido.Codigo='+edtPedido.Text);
          open;
          result:=fieldbyname('soma').asfloat;
     End;
end;



procedure TFtrocaMaterialpedido.dbgridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
          With Dbgrid.Canvas do
          begin
                If Brush.Color = clhighlight then
                Begin
                    Brush.Color := CORGRIDZEBRADOGLOBAL1;
                    Font.Color:=clBlack;
                end
                else
                Begin
                    If Odd(Dbgrid.DataSource.DataSet.RecNo) Then
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL2;
                          Font.Color:=clBlack;
                    End
                    else
                    Begin
                          Brush.Color:= CORGRIDZEBRADOGLOBAL1;
                           Font.Color:=clBlack;
                    End;
                End;
                Dbgrid.DefaultDrawDataCell(Rect, Dbgrid.Columns[DataCol].Field, State);
          End;
end;

procedure TFtrocaMaterialpedido.btAlterarClick(Sender: TObject);
var
PpedidoProjeto:string;
Key:Word;
Shift: TShiftState;
QueryProjeto:TIBQuery;
Projeto:string;
MaterialCor:string;
TipoPedido:string;
ObjMateriasTroca:TObjMATERIAISTROCA;
begin
       {
            Usado pra trocar as cores do materias...
            Exemplo de  uso: Segundo a vidrex acontece o seguinte: O cliente pode fazer a compra de um projeto, pagar, mas s�
            marca p intalar alguns meses depois (exemplo cl�ssico � quando a constru��o da obra n�o esta acabada ainda), ae na
            hora da instala��o o cliente ou arquiteto sugerir trocar a cor de algum material, por exemplo o vidro...
            At� ent�o acontecia o seguinte problema, era preciso estornar a venda e fazer novamente, mas com um problema, quando
            se fazia isso tudo se perdia, comiss�es, historicos dessa venda... logo isso torna complicado o controle...
            Com este modulo, ao ter um pedido conclu�do  e se quiser fazer alguma modifica��o sem retornar a venda basta usar esse
            modulo...


       }


       QueryProjeto:=TIBQuery.Create(nil);
       QueryProjeto.Database:=FDataModulo.IBDatabase;
       ObjMateriasTroca:=TObjMATERIAISTROCA.Create;
       try
           if (Dbgrid.DataSource.DataSet.Active=False)
           then exit;

           if (Dbgrid.DataSource.DataSet.RecordCount=0)
           then exit;

           if (Messagedlg('Tem Certeza que deseja alterar a cor do(da)'+QueryPesquisa.fieldbyname('material').asstring+' '+QueryPesquisa.fieldbyname('nomematerial').AsString +' Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
           Then exit;

           PpedidoProjeto:=QueryPesquisa.fieldbyname('pedidoprojeto').asstring;
           // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
           if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(QueryPesquisa.fieldbyname('pedidoprojeto').asstring)=true)
           then Begin
                MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto ele n�o pode ser alterado');
                exit;
           end;

           // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
           ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
           ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;


           If (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')
           then Begin
                    If (MessageDlg('Este � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                                   'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                                   'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo)
                     Then exit
                     Else Begin
                              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False);
                     End;
           End;
           QueryProjeto.Close;
           QueryProjeto.SQL.Clear;
           QueryProjeto.SQL.Add('select projeto,tipopedido from tabpedido_proj where codigo='+QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
           QueryProjeto.open;
           edtNovaCor.Text:='';
           Projeto:=QueryProjeto.Fieldbyname('projeto').asstring;
           TipoPedido:=QueryProjeto.fieldbyname('tipopedido').AsString;

           if (QueryPesquisa.Fieldbyname('material').asstring='FERRAGEM') Then
           begin
                 Key:=VK_F9;
                 ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorFerragemPraTrocaKeyDown(Sender,edtnovacor,key,Shift,QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                 btAlterar.Caption:='Alterar Cor';
                 if(edtNovaCor.Text<>'') then
                 begin
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabFerragemCor.Codigo as FerragemCor, TabFerragem_Proj.Quantidade');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendaInstalado+(TabFerragem.PrecoVendaInstalado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendaretirado+(TabFerragem.PrecoVendaretirado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendafornecido+(TabFerragem.PrecoVendafornecido*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      //se esta esta ferragem esta ligada ao projeto
                      QueryProjeto.SQL.Add('from TabFerragem_Proj');
                      QueryProjeto.SQL.Add('Join TabFerragemCor on TabFerragemCOr.Ferragem = TabFerragem_Proj.Ferragem');
                      QueryProjeto.SQL.Add('join TabFerragem on TabFerragem.Codigo = TabFerragemCor.Ferragem');
                      QueryProjeto.SQL.Add('Where TabFerragemCor.Cor = '+edtNovaCor.Text);
                      QueryProjeto.SQL.Add('and TabFerragem_Proj.Projeto = '+Projeto);
                      QueryProjeto.SQL.Add('and   tabferragemcor.ferragem='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      QueryProjeto.Open;

                      //Se a ferragem n�o esta no projeto, se ele foi inserida por conta
                      //ent�o n�o faz liga��o com o projeto
                      if(QueryProjeto.RecordCount=0)
                      then begin
                            QueryProjeto.Close;
                            QueryProjeto.SQL.Clear;
                            QueryProjeto.SQL.Add('Select  TabFerragemCor.Codigo as FerragemCor, TabFerragem_Proj.Quantidade');
                            if (TipoPedido='INSTALADO')
                            Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendaInstalado+(TabFerragem.PrecoVendaInstalado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='RETIRADO')
                            Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendaretirado+(TabFerragem.PrecoVendaretirado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='FORNECIDO')
                            Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendafornecido+(TabFerragem.PrecoVendafornecido*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');
                            QueryProjeto.SQL.Add('from TabFerragemCor');
                            QueryProjeto.SQL.Add('left Join TabFerragem_Proj on TabFerragemCOr.Ferragem = TabFerragem_Proj.Ferragem');
                            QueryProjeto.SQL.Add('left join TabFerragem on TabFerragem.Codigo = TabFerragemCor.Ferragem');
                            QueryProjeto.SQL.Add('Where TabFerragemCor.Cor = '+edtNovaCor.Text);
                            QueryProjeto.SQL.Add('and   tabferragemcor.ferragem='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                            QueryProjeto.Open;
                      end;
                      MaterialCor:=QueryProjeto.fieldbyname('FerragemCor').asstring;
                      ObjpedidoObjetos.ObjFerragem_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjFerragem_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjFerragem_PP.TabelaparaObjeto;
                      ObjMateriasTroca.Status:=dsInsert;


                      //Gravando um historico de materiais trocados
                      //Ou seja Grava o material que foi trocado
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_FERRAGEM_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_FERRAGEM(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_FERRAGEMCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('1');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);


                      //Gravando o novo material na tabferragem_pp
                      ObjpedidoObjetos.ObjFerragem_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjFerragem_PP.FerragemCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjFerragem_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);
                      ObjpedidoObjetos.ObjFerragem_PP.Salvar(False);
                 end;

           end;


           if (QueryPesquisa.Fieldbyname('material').asstring='PERFILADO') then
           begin
                 Key:=VK_F9;
                 ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorPerfiladoPraTrocaKeyDown(Sender,edtnovacor,key,Shift,QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                 btAlterar.Caption:='Alterar Cor';
                 if(edtNovaCor.Text<>'') then
                 begin
                      ///se tivesse que mudar a cor do pedidoprojeto tambem, mas a principio n�o seria o certo...
                      {ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.CorFerragem.Submit_Codigo(edtNovaCor.Text);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False); }
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabPerfiladoCor.Codigo as PerfiladoCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabperfilado.PRecoVendaInstalado+(Tabperfilado.PrecoVendaInstalado*TabperfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabperfilado.PRecoVendaretirado+(Tabperfilado.PrecoVendaretirado*TabperfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabperfilado.PRecoVendafornecido+(Tabperfilado.PrecoVendafornecido*TabperfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      //se esta ferragem esta ligada ao projeto
                      QueryProjeto.SQL.Add('from TabPerfilado_Proj');
                      QueryProjeto.SQL.Add('Join TabPerfiladoCor on TabPerfiladoCOr.Perfilado = TabPerfilado_Proj.Perfilado');
                      QueryProjeto.SQL.Add('join TabPerfilado on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
                      QueryProjeto.SQL.Add('Where TabPerfilado_Proj.Projeto = '+Projeto);
                      QueryProjeto.SQL.Add('and   TabPerfiladoCor.Cor = '+edtNovaCor.Text);
                      QueryProjeto.SQL.Add('and   tabPerfiladocor.Perfilado='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);

                      QueryProjeto.Open;
                      if(QueryProjeto.RecordCount=0)
                      then begin
                            QueryProjeto.Close;
                            QueryProjeto.SQL.Clear;
                            QueryProjeto.SQL.Add('Select  TabPerfiladoCor.Codigo as PerfiladoCor');
                            if (TipoPedido='INSTALADO')
                            Then QueryProjeto.SQL.Add(',(tabPerfilado.PRecoVendaInstalado+(TabPerfilado.PrecoVendaInstalado*TabPerfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='RETIRADO')
                            Then QueryProjeto.SQL.Add(',(tabPerfilado.PRecoVendaretirado+(TabPerfilado.PrecoVendaretirado*TabPerfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='FORNECIDO')
                            Then QueryProjeto.SQL.Add(',(tabPerfilado.PRecoVendafornecido+(TabPerfilado.PrecoVendafornecido*TabPerfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            //se esta esta ferragem esta ligada ao projeto
                            QueryProjeto.SQL.Add('from TabPerfiladoCor');
                            QueryProjeto.SQL.Add('left Join TabPerfilado_Proj on TabPerfiladoCOr.Perfilado = TabPerfilado_Proj.Perfilado');
                            QueryProjeto.SQL.Add('left join TabPerfilado on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
                            QueryProjeto.SQL.Add('Where TabPerfiladoCor.Cor = '+edtNovaCor.Text);
                            QueryProjeto.SQL.Add('and   tabPerfiladocor.Perfilado='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                            
                            QueryProjeto.Open;
                      end;
                      MaterialCor:=QueryProjeto.fieldbyname('PerfiladoCor').asstring;
                      ObjpedidoObjetos.ObjPerfilado_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjPerfilado_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjPerfilado_PP.TabelaparaObjeto;
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_Perfilado_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_Perfilado(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_PerfiladoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('2');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);
                      ObjpedidoObjetos.ObjPerfilado_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjPerfilado_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);
                      ObjpedidoObjetos.ObjPerfilado_PP.Salvar(false);
                 end;

           end;


           if (QueryPesquisa.Fieldbyname('material').asstring='VIDRO') then
           begin
                 Key:=VK_F9;
                 ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorVidroPraTrocaKeyDown(Sender,edtnovacor,key,Shift,QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                 btAlterar.Caption:='Alterar Cor';
                 if(edtNovaCor.Text<>'') then
                 begin
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabVidro.descricao,TabVidroCor.Codigo as VidroCor,TabVidro.Referencia');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(TabVidro.PRecoVendaInstalado+(TabVidro.PrecoVendaInstalado*TabVidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(TabVidro.PRecoVendaretirado+(TabVidro.PrecoVendaretirado*TabVidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(TabVidro.PRecoVendafornecido+(TabVidro.PrecoVendafornecido*TabVidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');
                      QueryProjeto.SQL.Add('from  Tabvidro');
                      QueryProjeto.SQL.Add('Join  TabvidroCor on TabvidroCor.vidro = Tabvidro.codigo');
                      QueryProjeto.SQL.Add('where TabvidroCor.Cor = '+edtNovaCor.Text);
                      QueryProjeto.SQL.Add('and   tabvidrocor.vidro='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);

                      QueryProjeto.Open;

                      MaterialCor:=QueryProjeto.fieldbyname('VidroCor').asstring;
                      ObjpedidoObjetos.ObjVidro_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjVidro_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjVidro_PP.TabelaparaObjeto;
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_Vidro_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_Vidro(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_VidroCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('3');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);
                      ObjpedidoObjetos.ObjVidro_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjVidro_PP.VidroCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjVidro_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);
                      ObjpedidoObjetos.ObjVidro_PP.Salvar(false);
                 end;
           end;


           if (QueryPesquisa.Fieldbyname('material').asstring='KITBOX')then
           begin
                 Key:=VK_F9;
                 ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorKitBoxPraTrocaKeyDown(Sender,edtnovacor,key,Shift,QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                 btAlterar.Caption:='Alterar Cor';
                 if(edtNovaCor.Text<>'') then
                 begin
                      ///se tivesse que mudar a cor do pedidoprojeto tambem, mas a principio n�o seria o certo...
                      {ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.CorFerragem.Submit_Codigo(edtNovaCor.Text);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False); }
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabKitBoxCor.Codigo as KitBoxCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendaInstalado+(TabKitBox.PrecoVendaInstalado*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendaretirado+(TabKitBox.PrecoVendaretirado*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendafornecido+(TabKitBox.PrecoVendafornecido*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      //se esta ferragem esta ligada ao projeto
                      QueryProjeto.SQL.Add('from TabKitBox_Proj');
                      QueryProjeto.SQL.Add('Join TabKitBoxCor on TabKitBoxCOr.KitBox = TabKitBox_Proj.KitBox');
                      QueryProjeto.SQL.Add('join TabKitBox on TabKitBox.Codigo = TabKitBoxCor.KitBox');
                      QueryProjeto.SQL.Add('Where TabKitBox_Proj.Projeto = '+Projeto);
                      QueryProjeto.SQL.Add('and   TabKitBoxCor.Cor = '+edtNovaCor.Text);
                      QueryProjeto.SQL.Add('and   tabKitBoxcor.KitBox='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);

                      QueryProjeto.Open;
                      if(QueryProjeto.RecordCount=0)
                      then begin
                            QueryProjeto.Close;
                            QueryProjeto.SQL.Clear;
                            QueryProjeto.SQL.Add('Select  TabKitBoxCor.Codigo as KitBoxCor, TabKitBox_Proj.Quantidade');
                            if (TipoPedido='INSTALADO')
                            Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendaInstalado+(TabKitBox.PrecoVendaInstalado*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='RETIRADO')
                            Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendaretirado+(TabKitBox.PrecoVendaretirado*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='FORNECIDO')
                            Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendafornecido+(TabKitBox.PrecoVendafornecido*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            QueryProjeto.SQL.Add('from TabKitBoxCor');
                            QueryProjeto.SQL.Add('left Join TabKitBox_Proj on TabKitBoxCOr.KitBox = TabKitBox_Proj.KitBox');
                            QueryProjeto.SQL.Add('left join TabKitBox on TabKitBox.Codigo = TabKitBoxCor.KitBox');
                            QueryProjeto.SQL.Add('Where TabKitBoxCor.Cor = '+edtNovaCor.Text);
                            QueryProjeto.SQL.Add('and   tabKitBoxcor.KitBox='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                            QueryProjeto.Open;
                      end;
                      MaterialCor:=QueryProjeto.fieldbyname('KitBoxCor').asstring;
                      ObjpedidoObjetos.ObjKitBox_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjKitBox_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjKitBox_PP.TabelaparaObjeto;
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_KitBox_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_KitBox(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_KitBoxCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('4');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);
                      ObjpedidoObjetos.ObjKitBox_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjKitBox_PP.KitBoxCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjKitBox_PP.Submit_Valor(QueryProjeto.fieldbyname('preco').AsString);
                      ObjpedidoObjetos.ObjKitBox_PP.Salvar(false);
                 end;

           end;
            //Ver o problema em alterar a cor da persiana
           if (QueryPesquisa.Fieldbyname('material').asstring='PERSIANA')then
           begin
                { Key:=VK_F9;
                 ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto. (Sender,edtnovacor,key,Shift,nil,Projeto);
                 btAlterar.Caption:='Alterar Cor';
                 if(edtNovaCor.Text<>'') then
                 begin
                      ///se tivesse que mudar a cor do pedidoprojeto tambem, mas a principio n�o seria o certo...
                      {ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.CorFerragem.Submit_Codigo(edtNovaCor.Text);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False); }
                      {QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabPerfiladoCor.Codigo as PerfiladoCor');
                      QueryProjeto.SQL.Add('from TabPerfilado_Proj');
                      QueryProjeto.SQL.Add('Join TabPerfiladoCor on TabPerfiladoCOr.Perfilado = TabPerfilado_Proj.Perfilado');
                      QueryProjeto.SQL.Add('join TabPerfilado on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
                      QueryProjeto.SQL.Add('Where TabPerfilado_Proj.Projeto = '+Projeto);
                      QueryProjeto.SQL.Add('and   TabPerfiladoCor.Cor = '+edtNovaCor.Text);
                      QueryProjeto.Open;
                      MaterialCor:=QueryProjeto.fieldbyname('PerfiladoCor').asstring;
                      ObjpedidoObjetos.ObjPerfilado_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjPerfilado_PP.TabelaparaObjeto;
                      ObjpedidoObjetos.ObjPerfilado_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjPerfilado_PP.Salvar(True);
                 end;}

           end;

           if (QueryPesquisa.Fieldbyname('material').asstring='DIVERSO')
           then
           begin
                 Key:=VK_F9;
                 ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtCorDiversoPraTrocaKeyDown (Sender,edtnovacor,key,Shift,QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                 btAlterar.Caption:='Alterar Cor';
                 if(edtNovaCor.Text<>'') then
                 begin
                      ///se tivesse que mudar a cor do pedidoprojeto tambem, mas a principio n�o seria o certo...
                      {ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.ZerarTabela;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.CorFerragem.Submit_Codigo(edtNovaCor.Text);
                      ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False); }
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabDiversoCor.Codigo as DiversoCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendaInstalado+(TabDiverso.PrecoVendaInstalado*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendaretirado+(TabDiverso.PrecoVendaretirado*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendafornecido+(TabDiverso.PrecoVendafornecido*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');
                      QueryProjeto.SQL.Add('from TabDiverso_Proj');
                      QueryProjeto.SQL.Add('Join TabDiversoCor on TabDiversoCOr.Diverso = TabDiverso_Proj.Diverso');
                      QueryProjeto.SQL.Add('join TabDiverso on TabDiverso.Codigo = TabDiversoCor.Diverso');
                      QueryProjeto.SQL.Add('Where TabDiverso_Proj.Projeto = '+Projeto);
                      QueryProjeto.SQL.Add('and   TabDiversoCor.Cor = '+edtNovaCor.Text);
                      QueryProjeto.SQL.Add('and   tabDiversocor.Diverso='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);

                      QueryProjeto.Open;
                      if(QueryProjeto.RecordCount=0)
                      then begin
                            QueryProjeto.Close;
                            QueryProjeto.SQL.Clear;
                            QueryProjeto.SQL.Add('Select  TabDiversoCor.Codigo as DiversoCor, TabDiverso_Proj.Quantidade');
                            if (TipoPedido='INSTALADO')
                            Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendaInstalado+(TabDiverso.PrecoVendaInstalado*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='RETIRADO')
                            Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendaretirado+(TabDiverso.PrecoVendaretirado*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            if (TipoPedido='FORNECIDO')
                            Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendafornecido+(TabDiverso.PrecoVendafornecido*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                            QueryProjeto.SQL.Add('from TabDiversoCor');
                            QueryProjeto.SQL.Add('left Join TabDiverso_Proj on TabDiversoCOr.Diverso = TabDiverso_Proj.Diverso');
                            QueryProjeto.SQL.Add('left join TabDiverso on TabDiverso.Codigo = TabDiversoCor.Diverso');
                            QueryProjeto.SQL.Add('Where TabDiversoCor.Cor = '+edtNovaCor.Text);
                            QueryProjeto.SQL.Add('and   tabDiversocor.Diverso='+QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                            QueryProjeto.Open;
                      end;
                      MaterialCor:=QueryProjeto.fieldbyname('DiversoCor').asstring;
                      ObjpedidoObjetos.ObjDiverso_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjDiverso_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjDiverso_PP.TabelaparaObjeto;
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_Diverso_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_Diverso(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_DiversoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('5');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);
                      ObjpedidoObjetos.ObjDiverso_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjDiverso_PP.DiversoCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjDiverso_PP.Submit_Valor(QueryProjeto.fieldbyname('preco').AsString);
                      ObjpedidoObjetos.ObjDiverso_PP.Salvar(false);
                 end;

           end;
           Self.AtualizaGrid;

           Self.AtualizavalorProjeto(PpedidoProjeto{,'N'});

       finally
            FreeAndNil(QueryProjeto);
            ObjMateriasTroca.Free;
       end;


end;


procedure TFtrocaMaterialpedido.FormShow(Sender: TObject);
begin
     limpaedit(Self);
     QueryPesquisa.close;
     PanelPedido.Enabled:=true;
     PanelMateriais.Enabled:=False;
     //btgravar.Enabled:=False;
     //btcancelar.Enabled:=False;
     lbConcluir.Visible:=False;
     lbCancelar.Visible:=false;
     MemoNovosValores.Lines.clear;
     Usouf9napesquisa:=False;
     edtPedido.SetFocus;

     try
        ObjpedidoObjetos:=TObjPedidoObjetos.Create(Self);
        QueryPesquisa.Database:=FDataModulo.IBDatabase;
        QueryCalculos.Database:=FDataModulo.IBDatabase;
     except
           MensagemErro('Erro na tentativa de Criar o Objeto de Pedido');
           exit;
     End;
     FescolheImagemBotao.PegaFiguraImagem(Image1,'RODAPE');
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSAR M�DULO DE TROCA EM PEDIDO CONCLU�DO')=False)
     Then PanelPedido.Enabled:=False;
     lbDescricaoPedido.Caption:='';
     pgc1.TabIndex:=0;
end;

procedure TFtrocaMaterialpedido.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      if (Key = VK_F1) then
      Fprincipal.chamaPesquisaMenu();
end;

procedure TFtrocaMaterialpedido.btTrocarMaterialClick(Sender: TObject);
var
PpedidoProjeto:string;
Key:Word;
Shift: TShiftState;
QueryProjeto:TIBQuery;
Projeto:string;
MaterialCor:string;
TipoPedido:string;
ObjMateriasTroca:TObjMATERIAISTROCA;
Quantidade,paltura,plargura,plarguraarredondamento,palturaarredondamento:Currency;
begin
       {

          Como funciona a troca de materias...Jonatan Medina 11/07/2011
          Funciona para quando um cliente faz uma compra por�m por algum motivo
          ele quer trocar o material que ele comprou...
          Logo o material � trocado e � gerado uma diferen�a para o cliente
          Cr�dito se for diferen�a a pagar pro cliente
          e um titulo a receber do cliente caso falte dinheiro para adicionar um novo material


       }

       QueryProjeto:=TIBQuery.Create(nil);
       QueryProjeto.Database:=FDataModulo.IBDatabase;
       ObjMateriasTroca:=TObjMATERIAISTROCA.Create;
       try
           if (Dbgrid.DataSource.DataSet.Active=False)
           then exit;

           if (Dbgrid.DataSource.DataSet.RecordCount=0)
           then exit;

           if (Messagedlg('Tem Certeza que deseja trocar o (a)'+QueryPesquisa.fieldbyname('material').asstring+' '+QueryPesquisa.fieldbyname('nomematerial').AsString +' Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
           Then exit;
           PpedidoProjeto:=QueryPesquisa.fieldbyname('pedidoprojeto').asstring;
           // Se este � o projeto principal naum pode ser alterado em hip�tese alguma
           if (ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.VerificaSePedidoProjetoPrincipal(QueryPesquisa.fieldbyname('pedidoprojeto').asstring)=true)
           then Begin
                MensagemErro('Existe(m) projeto(s) replicado(s) derivado(s) desse projeto. Portanto ele n�o pode ser alterado');
                exit;
           end;

           // Projeto replicado naum pode ser alterado se, for deixa de ser replicado.
           ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.LocalizaCodigo(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
           ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.TabelaparaObjeto;

           If (ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Get_PedidoProjetoPrincipal <> '')
           then Begin
                    If (MessageDlg('Este � um Projeto REPLICADO.  A altera��o do mesmo far� com que '+#13+
                                   'deixe de ser REPLICADO, e passe a ser um projeto INDIVIDUAL.'+#13+
                                   'Tem certeza que deseja alterar este projeto?', mtConfirmation, [mbYes, mbNo], 0) = mrNo)
                     Then exit
                     Else Begin
                              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Status:=dsEdit;
                              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Submit_PedidoProjetoPrincipal('');
                              ObjPedidoObjetos.ObjMedidas_Proj.PedidoProjeto.Salvar(False);
                     End;
           End;
           QueryProjeto.Close;
           QueryProjeto.SQL.Clear;
           QueryProjeto.SQL.Add('select projeto,tipopedido from tabpedido_proj where codigo='+QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
           QueryProjeto.open;
           edtNovaCor.Text:='';
           Projeto:=QueryProjeto.Fieldbyname('projeto').asstring;
           TipoPedido:=QueryProjeto.fieldbyname('tipopedido').AsString;
           QueryProjeto.Close;
           QueryProjeto.SQL.Clear;
           QueryProjeto.SQL.Add('select projeto,tipopedido from tabpedido_proj where codigo='+QueryPesquisa.Fieldbyname('pedidoprojeto').asstring);
           QueryProjeto.open;
           Projeto:=QueryProjeto.Fieldbyname('projeto').asstring;
           TipoPedido:=QueryProjeto.fieldbyname('tipopedido').AsString;
           edtCodigoMaterial.Text:='';
           if (QueryPesquisa.Fieldbyname('material').asstring='FERRAGEM') Then
           begin
                      Key:=VK_F9;
                      ObjPedidoObjetos.ObjFerragem_PP.EdtFerragemCorViewKeyDown(Sender,edtCodigoMaterial,key,Shift,lb1);
                      btTrocarMaterial.Caption:='Trocar &Material' ;
                      if(edtCodigoMaterial.Text='')
                      then Exit;

                      //Buscando informa��es sobre o material
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabFerragemCor.Codigo as FerragemCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendaInstalado+(TabFerragem.PrecoVendaInstalado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendaretirado+(TabFerragem.PrecoVendaretirado*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabFerragem.PRecoVendafornecido+(TabFerragem.PrecoVendafornecido*TabFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      QueryProjeto.SQL.Add('from TabFerragemCor');
                      QueryProjeto.SQL.Add('join TabFerragem on TabFerragem.Codigo = TabFerragemCor.Ferragem');
                      QueryProjeto.SQL.Add('Where   TabFerragemCor.Codigo = '+edtCodigoMaterial.Text);
                      QueryProjeto.Open;

                      MaterialCor:=QueryProjeto.fieldbyname('FerragemCor').asstring;
                      //Localizando o material do pedido (TabFerragem_PP)
                      ObjpedidoObjetos.ObjFerragem_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjFerragem_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjFerragem_PP.TabelaparaObjeto;

                      //Gravando na Tabmateriaistroca qual material foi trocado
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_Ferragem_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_Ferragem(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_FerragemCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('1');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);

                      //Alterando o Ferragem_PP com o novo material
                      ObjpedidoObjetos.ObjFerragem_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjFerragem_PP.FerragemCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjFerragem_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);

                      FfiltroImp.DesativaGrupos;
                      FfiltroImp.Grupo01.Enabled:=True;
                      FfiltroImp.LbGrupo01.Caption:='Digite a quantidade';
                      FfiltroImp.edtgrupo01.Text:='1';
                      FfiltroImp.ShowModal;

                      ObjpedidoObjetos.ObjFerragem_PP.Submit_Quantidade(FfiltroImp.edtgrupo01.Text);
                      ObjpedidoObjetos.ObjFerragem_PP.Salvar(false);
           end;
           if (QueryPesquisa.Fieldbyname('material').asstring='PERFILADO') Then
           begin
                      key:=VK_F9;
                      ObjPedidoObjetos.ObjPerfilado_PP.EdtPerfiladoCorKeyDown(Sender,edtCodigoMaterial,key,Shift,lb1);
                      btTrocarMaterial.Caption:='Trocar &Material' ;
                      if(edtCodigoMaterial.Text='')
                      then Exit;
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabPerfiladoCor.Codigo as PerfiladoCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabPerfilado.PRecoVendaInstalado+(TabPerfilado.PrecoVendaInstalado*TabPerfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabPerfilado.PRecoVendaretirado+(TabPerfilado.PrecoVendaretirado*TabPerfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabPerfilado.PRecoVendafornecido+(TabPerfilado.PrecoVendafornecido*TabPerfiladoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      QueryProjeto.SQL.Add('from TabPerfiladoCor');
                      QueryProjeto.SQL.Add('join TabPerfilado on TabPerfilado.Codigo = TabPerfiladoCor.Perfilado');
                      QueryProjeto.SQL.Add('Where   TabPerfiladoCor.Codigo = '+edtCodigoMaterial.Text);
                      QueryProjeto.Open;
                      MaterialCor:=QueryProjeto.fieldbyname('PerfiladoCor').asstring;
                      ObjpedidoObjetos.ObjPerfilado_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjPerfilado_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjPerfilado_PP.TabelaparaObjeto;

                      //gravando material que foi trocado
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_Perfilado_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_Perfilado(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_PerfiladoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('2');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);

                      ObjpedidoObjetos.ObjPerfilado_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjPerfilado_PP.PerfiladoCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjPerfilado_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);
                      FfiltroImp.DesativaGrupos;
                      FfiltroImp.Grupo01.Enabled:=True;
                      FfiltroImp.LbGrupo01.Caption:='Digite a quantidade';
                      FfiltroImp.edtgrupo01.Text:='1';
                      FfiltroImp.ShowModal;
                      ObjpedidoObjetos.ObjPerfilado_PP.Submit_Quantidade(FfiltroImp.edtgrupo01.Text);
                      ObjpedidoObjetos.ObjPerfilado_PP.Salvar(false);
           end;
           if (QueryPesquisa.Fieldbyname('material').asstring='VIDRO') then
           begin
           
                      Key:=VK_F9;
                      ObjpedidoObjetos.ObjVidro_PP.EdtVidroCorKeyDown(Sender,edtCodigoMaterial,key,Shift,lb1);
                      btTrocarMaterial.Caption:='Trocar &Material' ;
                      if(edtCodigoMaterial.Text='')
                      then Exit;
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabVidroCor.Codigo as VidroCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabVidro.PRecoVendaInstalado+(TabVidro.PrecoVendaInstalado*TabVidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabVidro.PRecoVendaretirado+(TabVidro.PrecoVendaretirado*TabVidroFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabVidro.PRecoVendafornecido+(TabVidro.PrecoVendafornecido*TabVidroCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      QueryProjeto.SQL.Add('from TabVidroCor');
                      QueryProjeto.SQL.Add('join TabVidro on TabVidro.Codigo = TabVidroCor.Vidro');
                      QueryProjeto.SQL.Add('Where   TabVidroCor.Codigo = '+edtCodigoMaterial.Text);
                      QueryProjeto.Open;
                      MaterialCor:=QueryProjeto.fieldbyname('VidroCor').asstring;
                      ObjpedidoObjetos.ObjVidro_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjVidro_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjVidro_PP.TabelaparaObjeto;

                      //gravando material que foi trocado
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_Vidro_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_Vidro(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_VidroCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('3');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);

                      ObjpedidoObjetos.ObjVidro_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjVidro_PP.VidroCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjVidro_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);

                      FfiltroImp.DesativaGrupos;
                      FfiltroImp.Grupo01.Enabled:=True;
                      FfiltroImp.Grupo02.Enabled:=True;
                      FfiltroImp.Grupo03.Enabled:=True;
                      FfiltroImp.LbGrupo01.Caption:='Altura';
                      FfiltroImp.edtgrupo01.Text:='';
                      FfiltroImp.LbGrupo02.Caption:='Largura';
                      FfiltroImp.edtgrupo02.Text:='';
                      FfiltroImp.LbGrupo03.Caption:='Quantidade em M�';
                      FfiltroImp.edtgrupo03.Text:='0';
                      FfiltroImp.ShowModal;
                      //Calculando em M�
                      if (FfiltroImp.edtgrupo01.Text <> '') and (FfiltroImp.edtgrupo02.Text <> '') then
                      Begin
                              Try
                                 paltura:=strtocurr(tira_ponto(FfiltroImp.edtgrupo01.Text));
                              Except
                                    paltura:=0;
                              End;

                              Try
                                 plargura:=strtocurr(tira_ponto(FfiltroImp.edtgrupo02.Text));
                              Except
                                  plargura:=0;
                              End;

                              FdataModulo.Arredonda5CM(paltura,palturaarredondamento);
                              FdataModulo.Arredonda5CM(plargura,plarguraarredondamento);

                              quantidade:=((plargura+plarguraarredondamento)*(paltura+palturaarredondamento))/1000000;
                              FfiltroImp.edtgrupo03.Text:=formata_valor(CurrToStr(quantidade));
                      end;
                      ObjpedidoObjetos.ObjVidro_PP.Submit_Quantidade(FfiltroImp.edtgrupo03.Text);
                      ObjpedidoObjetos.ObjVidro_PP.Salvar(false);
           end;
           if (QueryPesquisa.Fieldbyname('material').asstring='KITBOX') then
           begin
                      Key:=VK_F9;
                      ObjpedidoObjetos.ObjKitBox_PP.EdtKitBoxCorKeyDown(Sender,edtCodigoMaterial,key,Shift,lb1);
                      btTrocarMaterial.Caption:='Trocar &Material' ;
                      if(edtCodigoMaterial.Text='')
                      then Exit;
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabKitBoxCor.Codigo as KitBoxCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendaInstalado+(TabKitBox.PrecoVendaInstalado*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendaretirado+(TabKitBox.PrecoVendaretirado*TabKitBoxFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabKitBox.PRecoVendafornecido+(TabKitBox.PrecoVendafornecido*TabKitBoxCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      QueryProjeto.SQL.Add('from TabKitBoxCor');
                      QueryProjeto.SQL.Add('join TabKitBox on TabKitBox.Codigo = TabKitBoxCor.KitBox');
                      QueryProjeto.SQL.Add('Where   TabKitBoxCor.Codigo = '+edtCodigoMaterial.Text);
                      QueryProjeto.Open;
                      MaterialCor:=QueryProjeto.fieldbyname('KitBoxCor').asstring;
                      ObjpedidoObjetos.ObjKitBox_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjKitBox_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjKitBox_PP.TabelaparaObjeto;

                       //gravando material que foi trocado
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_KitBox_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_KitBox(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_KitBoxCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('4');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);

                      ObjpedidoObjetos.ObjKitBox_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjKitBox_PP.KitBoxCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjKitBox_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);
                      FfiltroImp.DesativaGrupos;
                      FfiltroImp.Grupo01.Enabled:=True;
                      FfiltroImp.LbGrupo01.Caption:='Digite a quantidade';
                      FfiltroImp.edtgrupo01.Text:='1';
                      FfiltroImp.ShowModal;
                      ObjpedidoObjetos.ObjKitBox_PP.Submit_Quantidade(FfiltroImp.edtgrupo01.Text);
                      ObjpedidoObjetos.ObjKitBox_PP.Salvar(false);
           end;
           if (QueryPesquisa.Fieldbyname('material').asstring='PERSIANA') then
           begin
                  Key:=VK_F9;
                  ObjpedidoObjetos.ObjPersiana_PP.EdtPersianaGrupoDiametroCorKeyDown(Sender,edtCodigoMaterial,key,Shift,lb1);

           end;
           if (QueryPesquisa.Fieldbyname('material').asstring='DIVERSO') then
           begin
                      Key:=VK_F9;
                      ObjpedidoObjetos.ObjDiverso_PP.EdtDiversoCorKeyDown(Sender,edtCodigoMaterial,key,Shift,lb1);
                      btTrocarMaterial.Caption:='Trocar &Material' ;
                      if(edtCodigoMaterial.Text='')
                      then Exit;
                      QueryProjeto.Close;
                      QueryProjeto.SQL.Clear;
                      QueryProjeto.SQL.Add('Select  TabDiversoCor.Codigo as DiversoCor');
                      if (TipoPedido='INSTALADO')
                      Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendaInstalado+(TabDiverso.PrecoVendaInstalado*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='RETIRADO')
                      Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendaretirado+(TabDiverso.PrecoVendaretirado*TabDiversoFerragemCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      if (TipoPedido='FORNECIDO')
                      Then QueryProjeto.SQL.Add(',(tabDiverso.PRecoVendafornecido+(TabDiverso.PrecoVendafornecido*TabDiversoCor.PorcentagemAcrescimoFinal)/100) as PRECO');

                      QueryProjeto.SQL.Add('from TabDiversoCor');
                      QueryProjeto.SQL.Add('join TabDiverso on TabDiverso.Codigo = TabDiversoCor.Diverso');
                      QueryProjeto.SQL.Add('Where   TabDiversoCor.Codigo = '+edtCodigoMaterial.Text);
                      QueryProjeto.Open;
                      MaterialCor:=QueryProjeto.fieldbyname('DiversoCor').asstring;
                      ObjpedidoObjetos.ObjDiverso_PP.ZerarTabela;
                      ObjpedidoObjetos.ObjDiverso_PP.LocalizaCodigo(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjpedidoObjetos.ObjDiverso_PP.TabelaparaObjeto;

                      //gravando material que foi trocado
                      ObjMateriasTroca.Status:=dsInsert;
                      ObjMateriasTroca.Submit_CODIGO('0');
                      ObjMateriasTroca.Submit_Diverso_PP(QueryPesquisa.Fieldbyname('codigopp').asstring);
                      ObjMateriasTroca.Submit_Diverso(QueryPesquisa.fieldbyname('CodigoMaterial').asstring);
                      ObjMateriasTroca.Submit_DiversoCOR(QueryPesquisa.fieldbyname('MaterialCOR').asstring);
                      ObjMateriasTroca.Submit_MATERIAL('5');
                      ObjMateriasTroca.Submit_QUANTIDADE(QueryPesquisa.fieldbyname('Quantidade').asstring);
                      ObjMateriasTroca.Submit_PEDIDO(edtPedido.Text);
                      ObjMateriasTroca.Submit_DATATROCA(Datetostr(now));
                      ObjMateriasTroca.Submit_Concluido('N');
                      ObjMateriasTroca.Submit_PEDIDOPROJETO(QueryPesquisa.fieldbyname('pedidoprojeto').asstring);
                      ObjMateriasTroca.Salvar(False);

                      ObjpedidoObjetos.ObjDiverso_PP.Status:=dsEdit;
                      ObjpedidoObjetos.ObjDiverso_PP.DiversoCor.Submit_Codigo(MaterialCor);
                      ObjpedidoObjetos.ObjDiverso_PP.Submit_Valor(QueryProjeto.fieldbyname('Preco').AsString);
                      FfiltroImp.DesativaGrupos;
                      FfiltroImp.Grupo01.Enabled:=True;
                      FfiltroImp.LbGrupo01.Caption:='Digite a quantidade';
                      FfiltroImp.edtgrupo01.Text:='1';
                      FfiltroImp.ShowModal;
                      ObjpedidoObjetos.ObjDiverso_PP.Submit_Quantidade(FfiltroImp.edtgrupo01.Text);
                      ObjpedidoObjetos.ObjDiverso_PP.Salvar(false);
           end;

           Self.AtualizaGrid;
           Self.AtualizavalorProjeto(PpedidoProjeto{,'N'});

       finally
            FreeAndNil(QueryProjeto);
            ObjMateriasTroca.free;
       end;
end;

procedure TFtrocaMaterialpedido.dbgridKeyPress(Sender: TObject;
  var Key: Char);
begin
        EdtPesquisa.Enabled:=True;
        EdtPesquisa.Visible:=True;
        pnl2.Visible:=True;
        EdtPesquisa.Text:=edtpesquisa.Text+ Key;
        EdtPesquisa.SetFocus;
        if(edtPesquisa.Text<>' ') then
        edtPesquisa.SelStart:=Length(edtPesquisa.text);
        CampoPesquisagrid:=Dbgrid.SelectedField.FieldName;
        lbInformacaoProcura.Caption:='Voc� esta procurando por '+CampoPesquisaGrid;
end;

procedure TFtrocaMaterialpedido.edtPesquisaExit(Sender: TObject);
begin
          edtpesquisa.Visible:=False;
          edtpesquisa.Text:='';
          pnl2.Visible:=False;
end;

function TFtrocaMaterialpedido.ResgataMateriais(pedido:string;NOME:string):Boolean;
var
where:string;
Key:Char;
begin
        if(NOME<>'')
        then where:='where UPPER('+CampoPesquisaGrid+')like'+#39+'%'+NOME+'%'+#39 ;
        {if(Length(edtPesquisa.Text)=1)
        then where:='';  }
        with QueryPesquisa do
        begin
              close;
              sql.clear;
              sql.add('Select * from proc_materiais_pp_pedido('+edtPedido.Text+')');
              if(where<>'')
              then SQL.Add(where);
              open;
              formatadbgrid(Dbgrid);
              Self.AtualizaNovosValores;

        end;
end;

procedure TFtrocaMaterialpedido.edtPesquisaKeyPress(Sender: TObject;
  var Key: Char);
var
  Nome:string;
begin
      Nome:='';
      if(Key=#13)then
      begin
          Dbgrid.SetFocus;
          Exit;
      end;
      if(Key<>#8)then
      begin
           if not (Key in['0'..'9',Chr(8)]) then
           begin
              Nome:=edtPesquisa.text+UpperCase(Key);
           end
           else Nome:=edtPesquisa.text+Key;
      end
      else  Nome:=edtPesquisa.text;
      ResgataMateriais(edtPedido.Text,nome);
end;

procedure TFtrocaMaterialpedido.edtPedidoExit(Sender: TObject);
begin
        FDataModulo.IBTransaction.RollbackRetaining;
        btAbrirpedidoClick(sender);
        Dbgrid.SetFocus;
end;

procedure TFtrocaMaterialpedido.lbConcluirMouseLeave(Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFtrocaMaterialpedido.lbConcluirMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
    TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFtrocaMaterialpedido.MemoPedidoClick(Sender: TObject);
begin
      // Dbgrid.SetFocus;
end;

{procedure TFtrocaMaterialpedido.edtlargura(Sender: TObject);
begin
        
end;   }

procedure TFtrocaMaterialpedido.pgc1Change(Sender: TObject);
var
  Query:TIBQuery;
  queryPesquisaMaterial:TIBQuery;
  QueryPesquisaMaterialTrocado:TIBQuery;
begin
       {
            Mostra os materias trocados e devolvidos num StringGrid


       }

       with pgc1 do
       begin
              try
                    query:=TIBQuery.Create(nil);
                    Query.Database:=FDataModulo.IBDatabase;
                    queryPesquisaMaterial:=TIBQuery.Create(nil);
                    queryPesquisaMaterial.Database:=FDataModulo.IBDatabase;
                    QueryPesquisaMaterialTrocado:=TIBQuery.Create(nil);
                    QueryPesquisaMaterialTrocado.database:=FDataModulo.IBDatabase;
                    if(TabIndex=1) then
                    begin
                          StrGridMateriaisTrocados.RowCount:=2;
                          StrGridMateriaisTrocados.ColCount:=10;
                          StrGridMateriaisTrocados.FixedCols:=0;
                          StrGridMateriaisTrocados.FixedRows:=1;
                          StrGridMateriaisTrocados.ColWidths[0] := 50;
                          StrGridMateriaisTrocados.ColWidths[1] := 200;
                          StrGridMateriaisTrocados.ColWidths[2] := 350;
                          StrGridMateriaisTrocados.ColWidths[3] := 100;
                          StrGridMateriaisTrocados.ColWidths[4] := 100;
                          StrGridMateriaisTrocados.ColWidths[5] := 50;
                          StrGridMateriaisTrocados.ColWidths[6] := 350;
                          StrGridMateriaisTrocados.ColWidths[7] := 100;
                          StrGridMateriaisTrocados.ColWidths[8] := 100;
                          StrGridMateriaisTrocados.ColWidths[9] := 100;
                          StrGridMateriaisTrocados.Cells[0,0] := 'CODIGO';
                          StrGridMateriaisTrocados.Cells[1,0] := 'MATERIAL';
                          StrGridMateriaisTrocados.Cells[2,0] := 'NOME DO MATERIAL TROCADO';
                          StrGridMateriaisTrocados.Cells[3,0] := 'COR';
                          StrGridMateriaisTrocados.Cells[4,0] := 'QUANTIDADE';
                          StrGridMateriaisTrocados.Cells[5,0] := '-->';
                          StrGridMateriaisTrocados.Cells[6,0] := 'NOME DO MATERIAL';
                          StrGridMateriaisTrocados.Cells[7,0] := 'COR';
                          StrGridMateriaisTrocados.Cells[8,0] := 'QUANTIDADE';
                          StrGridMateriaisTrocados.Cells[9,0] := 'DATA DA TROCA';
                          Query.Close;
                          Query.SQL.Clear;
                          Query.SQL.Add('select * from tabmateriaistroca where pedido='+edtPedido.Text);
                          Query.SQL.Add('order by codigo');
                          Query.Open;
                          while not Query.Eof do
                          begin
                                  StrGridMateriaisTrocados.Cells[0,StrGridMateriaisTrocados.RowCount-1] := Query.fieldbyname('codigo').AsString;
                                  if(Query.FieldByName('material').AsString='1')then
                                  begin
                                        //Seleciona a ferragem que foi trocada
                                        StrGridMateriaisTrocados.Cells[1,StrGridMateriaisTrocados.RowCount-1] := 'FERRAGEM';
                                        queryPesquisaMaterial.Close;
                                        queryPesquisaMaterial.SQL.Clear;
                                        queryPesquisaMaterial.SQL.Add('select tabferragem.descricao,tabcor.descricao as nomecor');
                                        queryPesquisaMaterial.SQL.Add('from tabferragem join tabferragemcor on tabferragemcor.ferragem=tabferragem.codigo');
                                        queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabferragemcor.cor');
                                        queryPesquisaMaterial.SQL.Add('where tabferragem.codigo='+query.fieldbyname('ferragem').AsString);
                                        queryPesquisaMaterial.SQL.Add('and tabferragemcor.codigo='+query.fieldbyname('ferragemcor').AsString);
                                        queryPesquisaMaterial.Open;

                                        //Busca a ferragem atual pela qual a anterior foi trocada
                                        //Primeiro verifica se este produto ja foi trocado
                                        QueryPesquisaMaterialTrocado.Close;
                                        QueryPesquisaMaterialTrocado.SQL.Clear;
                                        QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tmt.quantidade');
                                        QueryPesquisaMaterialTrocado.SQL.Add('from tabmateriaistroca tmt');
                                        QueryPesquisaMaterialTrocado.SQL.Add('join tabferragem fer on fer.codigo=tmt.ferragem');
                                        QueryPesquisaMaterialTrocado.SQL.Add('join tabferragemcor fercor on fercor.codigo=tmt.ferragemcor');
                                        QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=fercor.cor');
                                        QueryPesquisaMaterialTrocado.SQL.Add('where tmt.ferragem_pp='+query.fieldbyname('ferragem_pp').AsString);
                                        QueryPesquisaMaterialTrocado.SQL.Add('and tmt.codigo>'+query.fieldbyname('codigo').AsString) ;
                                        QueryPesquisaMaterialTrocado.Open;
                                        QueryPesquisaMaterialTrocado.Last;
                                        //se n�o foi, ent�o busca direto na tabferragem_PP
                                        if(QueryPesquisaMaterialTrocado.RecordCount=0) then
                                        begin
                                              QueryPesquisaMaterialTrocado.Close;
                                              QueryPesquisaMaterialTrocado.SQL.Clear;
                                              QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tfpp.quantidade');
                                              QueryPesquisaMaterialTrocado.SQL.Add('from tabferragem_pp tfpp');
                                              QueryPesquisaMaterialTrocado.SQL.Add('join tabferragemcor tfc on tfc.codigo= tfpp.ferragemcor');
                                              QueryPesquisaMaterialTrocado.SQL.Add('join tabferragem fer on fer.codigo=tfc.ferragem');
                                              QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=tfc.cor');
                                              QueryPesquisaMaterialTrocado.SQL.Add('where tfpp.codigo='+query.fieldbyname('ferragem_pp').AsString);
                                              QueryPesquisaMaterialTrocado.Open;
                                        end;
                                        QueryPesquisaMaterialTrocado.First;
                                  end;
                                  if(Query.FieldByName('material').AsString='2') then
                                  begin
                                         StrGridMateriaisTrocados.Cells[1,StrGridMateriaisTrocados.RowCount-1] := 'PERFILADO';
                                         queryPesquisaMaterial.Close;
                                         queryPesquisaMaterial.SQL.Clear;
                                         queryPesquisaMaterial.SQL.Add('select tabperfilado.descricao,tabcor.descricao as nomecor');
                                         queryPesquisaMaterial.SQL.Add('from tabperfilado join tabperfiladocor on tabperfiladocor.perfilado=tabperfilado.codigo');
                                         queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabperfiladocor.cor');
                                         queryPesquisaMaterial.SQL.Add('where tabperfilado.codigo='+query.fieldbyname('perfilado').AsString);
                                         queryPesquisaMaterial.SQL.Add('and tabperfiladocor.codigo='+query.fieldbyname('perfiladocor').AsString);
                                         queryPesquisaMaterial.Open;

                                         //Busca a perfilado atual pela qual a anterior foi trocada
                                        //Primeiro verifica se este produto ja foi trocado
                                        QueryPesquisaMaterialTrocado.Close;
                                        QueryPesquisaMaterialTrocado.SQL.Clear;
                                        QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tmt.quantidade');
                                        QueryPesquisaMaterialTrocado.SQL.Add('from tabmateriaistroca tmt');
                                        QueryPesquisaMaterialTrocado.SQL.Add('join tabperfilado fer on fer.codigo=tmt.perfilado');
                                        QueryPesquisaMaterialTrocado.SQL.Add('join tabperfiladocor fercor on fercor.codigo=tmt.perfiladocor');
                                        QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=fercor.cor');
                                        QueryPesquisaMaterialTrocado.SQL.Add('where tmt.perfilado_pp='+query.fieldbyname('perfilado_pp').AsString);
                                        QueryPesquisaMaterialTrocado.SQL.Add('and tmt.codigo>'+query.fieldbyname('codigo').AsString) ;
                                        QueryPesquisaMaterialTrocado.Open;
                                        QueryPesquisaMaterialTrocado.Last;
                                        //se n�o foi, ent�o busca direto na perfilado_PP
                                        if(QueryPesquisaMaterialTrocado.RecordCount=0) then
                                        begin
                                              QueryPesquisaMaterialTrocado.Close;
                                              QueryPesquisaMaterialTrocado.SQL.Clear;
                                              QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tfpp.quantidade');
                                              QueryPesquisaMaterialTrocado.SQL.Add('from tabperfilado_pp tfpp');
                                              QueryPesquisaMaterialTrocado.SQL.Add('join tabperfiladocor tfc on tfc.codigo= tfpp.perfiladocor');
                                              QueryPesquisaMaterialTrocado.SQL.Add('join tabperfilado fer on fer.codigo=tfc.perfilado');
                                              QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=tfc.cor');
                                              QueryPesquisaMaterialTrocado.SQL.Add('where tfpp.codigo='+query.fieldbyname('perfilado_pp').AsString);
                                              QueryPesquisaMaterialTrocado.Open;
                                        end;
                                        QueryPesquisaMaterialTrocado.First;
                                  end;
                                  if(Query.FieldByName('material').AsString='3') then
                                  begin
                                         StrGridMateriaisTrocados.Cells[1,StrGridMateriaisTrocados.RowCount-1] := 'VIDRO';
                                         queryPesquisaMaterial.Close;
                                         queryPesquisaMaterial.SQL.Clear;
                                         queryPesquisaMaterial.SQL.Add('select tabvidro.descricao,tabcor.descricao as nomecor');
                                         queryPesquisaMaterial.SQL.Add('from tabvidro join tabvidrocor on tabvidrocor.vidro=tabvidro.codigo');
                                         queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabvidrocor.cor');
                                         queryPesquisaMaterial.SQL.Add('where tabvidro.codigo='+query.fieldbyname('vidro').AsString);
                                         queryPesquisaMaterial.SQL.Add('and tabvidrocor.codigo='+query.fieldbyname('vidrocor').AsString);
                                         queryPesquisaMaterial.Open;

                                          //Busca a vidro atual pela qual a anterior foi trocada
                                          //Primeiro verifica se este produto ja foi trocado
                                          QueryPesquisaMaterialTrocado.Close;
                                          QueryPesquisaMaterialTrocado.SQL.Clear;
                                          QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tmt.quantidade');
                                          QueryPesquisaMaterialTrocado.SQL.Add('from tabmateriaistroca tmt');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabvidro fer on fer.codigo=tmt.vidro');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabvidrocor fercor on fercor.codigo=tmt.vidrocor');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=fercor.cor');
                                          QueryPesquisaMaterialTrocado.SQL.Add('where tmt.vidro_pp='+query.fieldbyname('vidro_pp').AsString);
                                          QueryPesquisaMaterialTrocado.SQL.Add('and tmt.codigo>'+query.fieldbyname('codigo').AsString) ;
                                          QueryPesquisaMaterialTrocado.Open;
                                          QueryPesquisaMaterialTrocado.Last;
                                          //se n�o foi, ent�o busca direto na vidro_PP
                                          if(QueryPesquisaMaterialTrocado.RecordCount=0) then
                                          begin
                                                QueryPesquisaMaterialTrocado.Close;
                                                QueryPesquisaMaterialTrocado.SQL.Clear;
                                                QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tfpp.quantidade');
                                                QueryPesquisaMaterialTrocado.SQL.Add('from tabvidro_pp tfpp');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabvidrocor tfc on tfc.codigo= tfpp.vidrocor');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabvidro fer on fer.codigo=tfc.vidro');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=tfc.cor');
                                                QueryPesquisaMaterialTrocado.SQL.Add('where tfpp.codigo='+query.fieldbyname('vidro_pp').AsString);
                                                QueryPesquisaMaterialTrocado.Open;
                                          end;
                                          QueryPesquisaMaterialTrocado.First;
                                         
                                  end;
                                  if(Query.FieldByName('material').AsString='4') then
                                  begin
                                         StrGridMateriaisTrocados.Cells[1,StrGridMateriaisTrocados.RowCount-1] := 'KITBOX';
                                         queryPesquisaMaterial.Close;
                                         queryPesquisaMaterial.SQL.Clear;
                                         queryPesquisaMaterial.SQL.Add('select tabkitbox.descricao,tabcor.descricao as nomecor');
                                         queryPesquisaMaterial.SQL.Add('from tabkitbox join tabkitboxcor on tabkitboxcor.kitbox=tabkitbox.codigo');
                                         queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabkitboxcor.cor');
                                         queryPesquisaMaterial.SQL.Add('where tabkitbox.codigo='+query.fieldbyname('kitbox').AsString);
                                         queryPesquisaMaterial.SQL.Add('and tabkitboxcor.codigo='+query.fieldbyname('kitboxcor').AsString);
                                         queryPesquisaMaterial.Open;

                                         //Busca o kitbox atual pela qual a anterior foi trocada
                                          //Primeiro verifica se este produto ja foi trocado
                                          QueryPesquisaMaterialTrocado.Close;
                                          QueryPesquisaMaterialTrocado.SQL.Clear;
                                          QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tmt.quantidade');
                                          QueryPesquisaMaterialTrocado.SQL.Add('from tabmateriaistroca tmt');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabkitbox fer on fer.codigo=tmt.vidro');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabkitboxcor fercor on fercor.codigo=tmt.kitboxcor');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=fercor.cor');
                                          QueryPesquisaMaterialTrocado.SQL.Add('where tmt.kitbox_pp='+query.fieldbyname('kitbox_pp').AsString);
                                          QueryPesquisaMaterialTrocado.SQL.Add('and tmt.codigo>'+query.fieldbyname('codigo').AsString) ;
                                          QueryPesquisaMaterialTrocado.Open;
                                          QueryPesquisaMaterialTrocado.Last;
                                          //se n�o foi, ent�o busca direto na kitbox_PP
                                          if(QueryPesquisaMaterialTrocado.RecordCount=0) then
                                          begin
                                                QueryPesquisaMaterialTrocado.Close;
                                                QueryPesquisaMaterialTrocado.SQL.Clear;
                                                QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tfpp.quantidade');
                                                QueryPesquisaMaterialTrocado.SQL.Add('from tabkitbox_pp tfpp');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabkitboxcor tfc on tfc.codigo= tfpp.kitboxcor');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabkitbox fer on fer.codigo=tfc.kitbox');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=tfc.cor');
                                                QueryPesquisaMaterialTrocado.SQL.Add('where tfpp.codigo='+query.fieldbyname('kitbox_pp').AsString);
                                                QueryPesquisaMaterialTrocado.Open;
                                          end;
                                          QueryPesquisaMaterialTrocado.First;
                                  end;
                                  if(Query.FieldByName('material').AsString='5') then
                                  begin
                                         StrGridMateriaisTrocados.Cells[1,StrGridMateriaisTrocados.RowCount-1] := 'DIVERSO';
                                         queryPesquisaMaterial.Close;
                                         queryPesquisaMaterial.SQL.Clear;
                                         queryPesquisaMaterial.SQL.Add('select tabdiverso.descricao,tabcor.descricao as nomecor');
                                         queryPesquisaMaterial.SQL.Add('from tabdiverso join tabdiversocor on tabdiversocor.diverso=tabdiverso.codigo');
                                         queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabdiversocor.cor');
                                         queryPesquisaMaterial.SQL.Add('where tabdiverso.codigo='+query.fieldbyname('diverso').AsString);
                                         queryPesquisaMaterial.SQL.Add('and tabdiversocor.codigo='+query.fieldbyname('diversocor').AsString);
                                         queryPesquisaMaterial.Open;

                                         QueryPesquisaMaterialTrocado.Close;
                                          QueryPesquisaMaterialTrocado.SQL.Clear;
                                          QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tmt.quantidade');
                                          QueryPesquisaMaterialTrocado.SQL.Add('from tabmateriaistroca tmt');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabdiverso fer on fer.codigo=tmt.vidro');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabdiversocor fercor on fercor.codigo=tmt.diversocor');
                                          QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=fercor.cor');
                                          QueryPesquisaMaterialTrocado.SQL.Add('where tmt.diverso_pp='+query.fieldbyname('diverso_pp').AsString);
                                          QueryPesquisaMaterialTrocado.SQL.Add('and tmt.codigo>'+query.fieldbyname('codigo').AsString) ;
                                          QueryPesquisaMaterialTrocado.Open;
                                          QueryPesquisaMaterialTrocado.Last;
                                          //se n�o foi, ent�o busca direto na diverso_PP
                                          if(QueryPesquisaMaterialTrocado.RecordCount=0) then
                                          begin
                                                QueryPesquisaMaterialTrocado.Close;
                                                QueryPesquisaMaterialTrocado.SQL.Clear;
                                                QueryPesquisaMaterialTrocado.SQL.Add('select fer.descricao, tabcor.descricao as cor,tfpp.quantidade');
                                                QueryPesquisaMaterialTrocado.SQL.Add('from tabdiverso_pp tfpp');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabdiversocor tfc on tfc.codigo= tfpp.diversocor');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabdiverso fer on fer.codigo=tfc.diverso');
                                                QueryPesquisaMaterialTrocado.SQL.Add('join tabcor on tabcor.codigo=tfc.cor');
                                                QueryPesquisaMaterialTrocado.SQL.Add('where tfpp.codigo='+query.fieldbyname('diverso_pp').AsString);
                                                QueryPesquisaMaterialTrocado.Open;
                                          end;
                                          QueryPesquisaMaterialTrocado.First;
                                  end;
                                  StrGridMateriaisTrocados.Cells[2,StrGridMateriaisTrocados.RowCount-1] := queryPesquisaMaterial.fieldbyname('descricao').AsString;
                                  StrGridMateriaisTrocados.Cells[3,StrGridMateriaisTrocados.RowCount-1] := queryPesquisaMaterial.fieldbyname('nomecor').AsString;
                                  StrGridMateriaisTrocados.Cells[4,StrGridMateriaisTrocados.RowCount-1] := Query.fieldbyname('quantidade').AsString;
                                  StrGridMateriaisTrocados.Cells[5,StrGridMateriaisTrocados.RowCount-1] := '-->';
                                  StrGridMateriaisTrocados.Cells[6,StrGridMateriaisTrocados.RowCount-1] := QueryPesquisaMaterialTrocado.fieldbyname('descricao').AsString;
                                  StrGridMateriaisTrocados.Cells[7,StrGridMateriaisTrocados.RowCount-1] := QueryPesquisaMaterialTrocado.fieldbyname('cor').AsString;
                                  StrGridMateriaisTrocados.Cells[8,StrGridMateriaisTrocados.RowCount-1] := QueryPesquisaMaterialTrocado.fieldbyname('quantidade').AsString;
                                  strGridMateriaisTrocados.Cells[9,StrGridMateriaisTrocados.RowCount-1] := Query.fieldbyname('DATATROCA').AsString;
                                  StrGridMateriaisTrocados.RowCount:=StrGridMateriaisTrocados.RowCount+1;
                                  Query.next;
                          end;
                          StrGridMateriaisTrocados.RowCount:=StrGridMateriaisTrocados.RowCount-1;
                    end;
                     if(TabIndex=2) then
                    begin
                          StrGridMateriaisDevolvidos.RowCount:=2;
                          StrGridMateriaisDevolvidos.ColCount:=6;
                          StrGridMateriaisDevolvidos.FixedCols:=0;
                          StrGridMateriaisDevolvidos.FixedRows:=1;
                          StrGridMateriaisDevolvidos.ColWidths[0] := 50;
                          StrGridMateriaisDevolvidos.ColWidths[1] := 200;
                          StrGridMateriaisDevolvidos.ColWidths[2] := 350;
                          StrGridMateriaisDevolvidos.ColWidths[3] := 100;
                          StrGridMateriaisDevolvidos.ColWidths[4] := 100;
                          StrGridMateriaisDevolvidos.ColWidths[5] := 150;
                          StrGridMateriaisDevolvidos.Cells[0,0] := 'CODIGO';
                          StrGridMateriaisDevolvidos.Cells[1,0] := 'MATERIAL';
                          StrGridMateriaisDevolvidos.Cells[2,0] := 'NOME DO MATERIAL';
                          StrGridMateriaisDevolvidos.Cells[3,0] := 'COR';
                          StrGridMateriaisDevolvidos.Cells[4,0] := 'QUANTIDADE';
                          StrGridMateriaisDevolvidos.Cells[5,0] := 'DATA DA DEVOLU��O';
                          Query.Close;
                          Query.SQL.Clear;
                          Query.SQL.Add('select * from tabmateriaisdevolucao where pedido='+edtPedido.Text);
                          Query.Open;
                          while not Query.Eof do
                          begin
                                  StrGridMateriaisDevolvidos.Cells[0,StrGridMateriaisDevolvidos.RowCount-1] := Query.fieldbyname('codigo').AsString;
                                  if(Query.FieldByName('material').AsString='1')then
                                  begin
                                        StrGridMateriaisDevolvidos.Cells[1,StrGridMateriaisDevolvidos.RowCount-1] := 'FERRAGEM';
                                        queryPesquisaMaterial.Close;
                                        queryPesquisaMaterial.SQL.Clear;
                                        queryPesquisaMaterial.SQL.Add('select tabferragem.descricao,tabcor.descricao as nomecor');
                                        queryPesquisaMaterial.SQL.Add('from tabferragem join tabferragemcor on tabferragemcor.ferragem=tabferragem.codigo');
                                        queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabferragemcor.cor');
                                        queryPesquisaMaterial.SQL.Add('where tabferragem.codigo='+query.fieldbyname('ferragem').AsString);
                                        queryPesquisaMaterial.SQL.Add('and tabferragemcor.codigo='+query.fieldbyname('ferragemcor').AsString);
                                        queryPesquisaMaterial.Open;
                                        StrGridMateriaisDevolvidos.Cells[2,StrGridMateriaisDevolvidos.RowCount-1] := queryPesquisaMaterial.fieldbyname('descricao').AsString;
                                  end;
                                  if(Query.FieldByName('material').AsString='2') then
                                  begin
                                        StrGridMateriaisDevolvidos.Cells[1,StrGridMateriaisDevolvidos.RowCount-1] := 'PERFILADO';
                                        queryPesquisaMaterial.Close;
                                        queryPesquisaMaterial.SQL.Clear;
                                        queryPesquisaMaterial.SQL.Add('select tabperfilado.descricao,tabcor.descricao as nomecor');
                                        queryPesquisaMaterial.SQL.Add('from tabperfilado join tabperfiladocor on tabperfiladocor.perfilado=tabperfilado.codigo');
                                        queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabperfiladocor.cor');
                                        queryPesquisaMaterial.SQL.Add('where tabperfilado.codigo='+query.fieldbyname('perfilado').AsString);
                                        queryPesquisaMaterial.SQL.Add('and tabperfiladocor.codigo='+query.fieldbyname('perfiladocor').AsString);
                                        queryPesquisaMaterial.Open;
                                        StrGridMateriaisDevolvidos.Cells[2,StrGridMateriaisDevolvidos.RowCount-1] := queryPesquisaMaterial.fieldbyname('descricao').AsString;
                                  end;
                                  if(Query.FieldByName('material').AsString='3') then
                                  begin
                                        StrGridMateriaisDevolvidos.Cells[1,StrGridMateriaisDevolvidos.RowCount-1] := 'VIDRO';
                                        queryPesquisaMaterial.Close;
                                        queryPesquisaMaterial.SQL.Clear;
                                        queryPesquisaMaterial.SQL.Add('select tabvidro.descricao,tabcor.descricao as nomecor');
                                        queryPesquisaMaterial.SQL.Add('from tabvidro join tabvidrocor on tabvidrocor.vidro=tabvidro.codigo');
                                        queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabvidrocor.cor');
                                        queryPesquisaMaterial.SQL.Add('where tabvidro.codigo='+query.fieldbyname('vidro').AsString);
                                        queryPesquisaMaterial.SQL.Add('and tabvidrocor.codigo='+query.fieldbyname('vidrocor').AsString);
                                        queryPesquisaMaterial.Open;
                                        StrGridMateriaisDevolvidos.Cells[2,StrGridMateriaisDevolvidos.RowCount-1] := queryPesquisaMaterial.fieldbyname('descricao').AsString;
                                  end;
                                  if(Query.FieldByName('material').AsString='4') then
                                  begin
                                        StrGridMateriaisDevolvidos.Cells[1,StrGridMateriaisDevolvidos.RowCount-1] := 'KITBOX';
                                        queryPesquisaMaterial.Close;
                                        queryPesquisaMaterial.SQL.Clear;
                                        queryPesquisaMaterial.SQL.Add('select tabkitbox.descricao,tabcor.descricao as nomecor');
                                        queryPesquisaMaterial.SQL.Add('from tabkitbox join tabkitboxcor on tabkitboxcor.kitbox=tabkitbox.codigo');
                                        queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabkitboxcor.cor');
                                        queryPesquisaMaterial.SQL.Add('where tabkitbox.codigo='+query.fieldbyname('kitbox').AsString);
                                        queryPesquisaMaterial.SQL.Add('and tabkitboxcor.codigo='+query.fieldbyname('kitboxcor').AsString);
                                        queryPesquisaMaterial.Open;
                                        StrGridMateriaisDevolvidos.Cells[2,StrGridMateriaisDevolvidos.RowCount-1] := queryPesquisaMaterial.fieldbyname('descricao').AsString;
                                  end;
                                  if(Query.FieldByName('material').AsString='5') then
                                  begin
                                        StrGridMateriaisDevolvidos.Cells[1,StrGridMateriaisDevolvidos.RowCount-1] := 'DIVERSO';
                                        queryPesquisaMaterial.Close;
                                        queryPesquisaMaterial.SQL.Clear;
                                        queryPesquisaMaterial.SQL.Add('select tabdiverso.descricao,tabcor.descricao as nomecor');
                                        queryPesquisaMaterial.SQL.Add('from tabdiverso join tabdiversocor on tabdiversocor.diverso=tabdiverso.codigo');
                                        queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabdiversocor.cor');
                                        queryPesquisaMaterial.SQL.Add('where tabdiverso.codigo='+query.fieldbyname('diverso').AsString);
                                        queryPesquisaMaterial.SQL.Add('and tabdiversocor.codigo='+query.fieldbyname('diversocor').AsString);
                                        queryPesquisaMaterial.Open;
                                        StrGridMateriaisDevolvidos.Cells[2,StrGridMateriaisDevolvidos.RowCount-1] := queryPesquisaMaterial.fieldbyname('descricao').AsString;
                                  end;
                                  if(Query.FieldByName('material').AsString='6') then
                                  begin
                                        StrGridMateriaisDevolvidos.Cells[1,StrGridMateriaisDevolvidos.RowCount-1] := 'PERSIANA';
                                        queryPesquisaMaterial.Close;
                                        queryPesquisaMaterial.SQL.Clear;
                                        queryPesquisaMaterial.SQL.Add('select tabpersiana.nome,tabcor.descricao as nomecor');
                                        queryPesquisaMaterial.SQL.Add('from tabpersiana join tabpersianagrupodiametrocor on tabpersianagrupodiametrocor.persiana=tabpersiana.codigo');
                                        queryPesquisaMaterial.SQL.Add('join tabcor on tabcor.codigo=tabpersianagrupodiametrocor.cor');
                                        queryPesquisaMaterial.SQL.Add('where tabpersiana.codigo='+query.fieldbyname('persiana').AsString);
                                       // queryPesquisaMaterial.SQL.Add('and tabpersianagrupodiametrocor.codigo='+query.fieldbyname('diversocor').AsString);
                                        queryPesquisaMaterial.Open;
                                        StrGridMateriaisDevolvidos.Cells[2,StrGridMateriaisDevolvidos.RowCount-1] := queryPesquisaMaterial.fieldbyname('nome').AsString;
                                  end;

                                  StrGridMateriaisDevolvidos.Cells[3,StrGridMateriaisDevolvidos.RowCount-1] := queryPesquisaMaterial.fieldbyname('nomecor').AsString;
                                  StrGridMateriaisDevolvidos.Cells[4,StrGridMateriaisDevolvidos.RowCount-1] := Query.fieldbyname('quantidade').AsString;
                                  StrGridMateriaisDevolvidos.Cells[5,StrGridMateriaisDevolvidos.RowCount-1] := Query.fieldbyname('DATADEVOLUCAO').AsString;
                                  StrGridMateriaisDevolvidos.RowCount:=StrGridMateriaisDevolvidos.RowCount+1;
                                  Query.next;
                          end;
                          StrGridMateriaisDevolvidos.RowCount:=StrGridMateriaisDevolvidos.RowCount-1;
                    end;
              finally
                     FreeAndNil(Query);
                     FreeAndNil(QueryPesquisaMaterialTrocado);
                     FreeAndNil(queryPesquisaMaterial);
              end;



       end;
end;

function TFtrocaMaterialpedido.OrganizaMateriaisTrocados:Boolean;
Var
    Query:TIBQuery;
    ObjMateriasTroca:TObjMATERIAISTROCA;
    pedidoprojeto:string;
begin
         {

              Organiza os materiais trocados, por exemplo
              Tenho um material A...
              Ae troco o material A pelo material B
              Depois, sem concluir as trocas, troco o material B pelo Material C
              Ae concluo....
              Na verdade, eu n�o tive duas trocas e sim um... Troquei o material A pelo material C
              Sem essa fun��o terei que foram efetuadas duas trocas...
              A por B e B por C...


         }

         result:=True;
         Query:=TIBQuery.Create(nil);
         Query.Database:=FDataModulo.IBDatabase;
         ObjMateriasTroca:=TObjMATERIAISTROCA.Create;
         try
               QueryPesquisa.First;
               while not QueryPesquisa.Eof
               do
               begin
                     with Query do
                     begin
                           Close;
                           SQL.Clear;
                           SQL.Add('select codigo,pedidoprojeto from tabmateriaistroca');
                           SQL.Add('where pedido='+edtPedido.text);
                           if(QueryPesquisa.fieldbyname('Material').AsString='FERRAGEM') then
                           begin
                                SQL.Add('and ferragem_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                           end;
                           if(QueryPesquisa.fieldbyname('Material').AsString='PERFILDO') then
                           begin
                                SQL.Add('and perfilado_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                           end;
                           if(QueryPesquisa.fieldbyname('Material').AsString='VIDRO') then
                           begin
                                SQL.Add('and vidro_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                           end;
                           if(QueryPesquisa.fieldbyname('Material').AsString='KITBOX') then
                           begin
                                SQL.Add('and kitbox_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                           end;
                           if(QueryPesquisa.fieldbyname('Material').AsString='DIVERSO') then
                           begin
                                SQL.Add('and diverso_pp='+QueryPesquisa.fieldbyname('codigopp').AsString);
                           end;
                           sql.Add('and concluido=''N''');
                           SQL.Add('order by codigo');
                           Open;
                           Last;
                           First;
                           pedidoprojeto:=fieldbyname('pedidoprojeto').AsString;
                           //no caso de haver mais de uma troca para um material sem que tenha concluido a anterior
                           //nesse caso, seleciona todos os materias nesta situa��o
                           //atualiza o primeiro trocado pra concluido
                           //deleta os outros n�o concluidos
                           if(FieldByName('codigo').AsString<>'') then
                           begin
                                   //O primeiro fica, os demais s�o excluidos
                                   if(ObjMateriasTroca.LocalizaCodigo(fieldbyname('codigo').AsString)=True) then
                                   begin
                                          ObjMateriasTroca.TabelaparaObjeto;
                                          ObjMateriasTroca.Status:=dsEdit;
                                          ObjMateriasTroca.Submit_Concluido('S');
                                          ObjMateriasTroca.Salvar(False);
                                   end ;
                                   Next;
                                   while not Eof do
                                   begin
                                          ObjMateriasTroca.Exclui(fieldbyname('codigo').asstring,False);
                                          Next;
                                   end;
                           end;
                     end;
                     QueryPesquisa.Next;
               end;


         finally
              FreeAndNil(Query);
              ObjMateriasTroca.Free;
         end;

end;



procedure TFtrocaMaterialpedido.edtPedidoChange(Sender: TObject);
begin
     pgc1.TabIndex:=0;
end;

procedure TFtrocaMaterialpedido.lbDescricaoPedidoClick(Sender: TObject);
var
Fpedido:TFpedido;
begin

     Try
        Fpedido:=TFpedido.Create(nil);

     Except
           Messagedlg('Erro na tentativa de Criar o Cadastro de Pedido',mterror,[mbok],0);
           exit;
     End;

     try
          if(edtPedido.Text='')
          then Exit;
          Fpedido.Tag:=StrToInt(edtPedido.Text);
          Fpedido.ShowModal;
     finally
          FreeAndNil(Fpedido);
     end;
End;


procedure TFtrocaMaterialpedido.lbDescricaoPedidoMouseLeave(
  Sender: TObject);
begin
      TEdit(sender).Font.Style:=[fsBold];
end;

procedure TFtrocaMaterialpedido.lbDescricaoPedidoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
      TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFtrocaMaterialpedido.GravamateriaisDevolvidosparaDevolucaoProjetos;
var
     query:TIBQuery;
     QueryDeleta:TIBQuery;
     Objmateriaisdevolucao:TObjMATERIAISDEVOLUCAO;
begin
     query:=TIBQuery.Create(nil);
     query.Database:=FDataModulo.IBDatabase;
     QueryDeleta:=TIBQuery.Create(nil);
     QueryDeleta.Database:=FDataModulo.IBDatabase;
     Objmateriaisdevolucao:=TObjMATERIAISDEVOLUCAO.Create;

     try
          with query do
          begin
                Close;
                Sql.Clear;
                SQL.Add('select * from proc_materiais_pp_pedido ('+edtPedido.Text+')');
                SQL.Add('where pedidoprojeto='+self.QueryPesquisa.FieldByName('pedidoprojeto').AsString);

                Open;

                while not Eof do
                begin
                         if (Query.Fieldbyname('material').asstring='FERRAGEM') then
                         begin
                              //se houve alguma troca neste ferragem_PP ent�o exlcluo todo o historico de troca neste ferragem_PP
                              //a unica informa��o q sei � esta ferragem foi devolvido, ver se isso � o melhor...
                              with QueryDeleta do
                              begin
                                    //Se tem mais de um material desse tipo, ent�o pergunto quanto vai querer excluir
                                    //As informa��es de trocas s�o apagadas, � gerado apenas um informa��o de devolu��o

                                    Close;
                                    SQL.Clear;
                                    SQL.Add('select * from tabmateriaistroca');
                                    SQL.Add('where pedido='+edtPedido.text);
                                    SQL.Add('and ferragem_pp='+Query.fieldbyname('codigopp').AsString);
                                    sql.Add('and concluido=''N''');
                                    SQL.Add('order by codigo');
                                    Open;
                                    Last;

                                    //guardando qual material foi devolvido
                                    Objmateriaisdevolucao.Status:=dsInsert;
                                    Objmateriaisdevolucao.ZerarTabela;
                                    Objmateriaisdevolucao.Submit_CODIGO('0');
                                    Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                    if(recordcount>0) then
                                    begin
                                            First;
                                            Objmateriaisdevolucao.Submit_FERRAGEM(fieldbyname('ferragem').asstring);
                                            Objmateriaisdevolucao.Submit_FERRAGEMCOR(fieldbyname('ferragemcor').asstring);
                                            Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                                    end
                                    else
                                    begin
                                            Objmateriaisdevolucao.Submit_FERRAGEM(Query.fieldbyname('CodigoMaterial').asstring);
                                            Objmateriaisdevolucao.Submit_FERRAGEMCOR(Query.fieldbyname('MaterialCOR').asstring);
                                            Objmateriaisdevolucao.Submit_QUANTIDADE(Query.fieldbyname('quantidade').AsString);
                                    end;

                                    Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                    Objmateriaisdevolucao.Submit_MATERIAL('1');
                                    Objmateriaisdevolucao.Salvar(false);

                              end;
                         end;

                         if (Query.Fieldbyname('material').asstring='PERFILADO') then
                         begin
                              with QueryDeleta do
                              begin
                                      Close;
                                      SQL.Clear;
                                      SQL.Add('select * from tabmateriaistroca');
                                      SQL.Add('where pedido='+edtPedido.text);
                                      SQL.Add('and Perfilado_pp='+Query.fieldbyname('codigopp').AsString);
                                      sql.Add('and concluido=''N''');
                                      SQL.Add('order by codigo');
                                      Open;
                                      Last;

                                      //guardando qual material foi devolvido
                                      Objmateriaisdevolucao.Status:=dsInsert;
                                      Objmateriaisdevolucao.ZerarTabela;

                                      Objmateriaisdevolucao.Submit_CODIGO('0');
                                      Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                      if(recordcount>0) then
                                      begin
                                                    First;
                                                    Objmateriaisdevolucao.Submit_Perfilado(fieldbyname('Perfilado').asstring);
                                                    Objmateriaisdevolucao.Submit_PerfiladoCOR(fieldbyname('Perfiladocor').asstring);
                                                    Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                      end
                                      else
                                      begin
                                                    Objmateriaisdevolucao.Submit_Perfilado(Query.fieldbyname('CodigoMaterial').asstring);
                                                    Objmateriaisdevolucao.Submit_PerfiladoCOR(Query.fieldbyname('MaterialCOR').asstring);
                                                    Objmateriaisdevolucao.Submit_QUANTIDADE(Query.fieldbyname('quantidade').AsString);
                                      end;

                                      Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                      Objmateriaisdevolucao.Submit_MATERIAL('2');
                                      Objmateriaisdevolucao.Salvar(false);
                              end;
                         end;

                         if (Query.Fieldbyname('material').asstring='VIDRO') Then
                         begin
                              with QueryDeleta do
                              begin
                                    Close;
                                    SQL.Clear;
                                    SQL.Add('select * from tabmateriaistroca');
                                    SQL.Add('where pedido='+edtPedido.text);
                                    SQL.Add('and Vidro_pp='+Query.fieldbyname('codigopp').AsString);
                                    sql.Add('and concluido=''N''');
                                    SQL.Add('order by codigo');
                                    Open;
                                    Last;

                                    //guardando qual material foi devolvido
                                    Objmateriaisdevolucao.Status:=dsInsert;
                                    Objmateriaisdevolucao.ZerarTabela;
                                    Objmateriaisdevolucao.Submit_CODIGO('0');

                                    Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                    if(recordcount>0) then
                                    begin
                                          First;
                                          Objmateriaisdevolucao.Submit_Vidro(fieldbyname('Vidro').asstring);
                                          Objmateriaisdevolucao.Submit_VidroCOR(fieldbyname('Vidrocor').asstring);
                                          Objmateriaisdevolucao.Submit_QUANTIDADE(FfiltroImp.edtgrupo01.Text);
                                    end
                                    else
                                    begin
                                          Objmateriaisdevolucao.Submit_Vidro(Query.fieldbyname('CodigoMaterial').asstring);
                                          Objmateriaisdevolucao.Submit_VidroCOR(Query.fieldbyname('MaterialCOR').asstring);
                                          Objmateriaisdevolucao.Submit_QUANTIDADE(Query.fieldbyname('quantidade').AsString);
                                    end;

                                    Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                    Objmateriaisdevolucao.Submit_MATERIAL('3');
                                    Objmateriaisdevolucao.Salvar(false);

                              end;

                         end;

                         if (Query.Fieldbyname('material').asstring='KITBOX') Then
                         begin
                              with QueryDeleta do
                              begin
                                    Close;
                                    SQL.Clear;
                                    SQL.Add('select * from tabmateriaistroca');
                                    SQL.Add('where pedido='+edtPedido.text);
                                    SQL.Add('and KitBox_pp='+Query.fieldbyname('codigopp').AsString);
                                    sql.Add('and concluido=''N''');
                                    SQL.Add('order by codigo');
                                    Open;
                                    Last;

                                    //guardando qual material foi devolvido
                                    Objmateriaisdevolucao.Status:=dsInsert;
                                    Objmateriaisdevolucao.ZerarTabela;
                                    Objmateriaisdevolucao.Submit_CODIGO('0');
                                    Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                    if(recordcount>0) then
                                    begin
                                                    First;
                                                    Objmateriaisdevolucao.Submit_KitBox(fieldbyname('KitBox').asstring);
                                                    Objmateriaisdevolucao.Submit_KitBoxCOR(fieldbyname('KitBoxcor').asstring);
                                                    Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                                    end
                                    else
                                    begin
                                                    Objmateriaisdevolucao.Submit_KitBox(Query.fieldbyname('CodigoMaterial').asstring);
                                                    Objmateriaisdevolucao.Submit_KitBoxCOR(Query.fieldbyname('MaterialCOR').asstring);
                                                    Objmateriaisdevolucao.Submit_QUANTIDADE(Query.fieldbyname('quantidade').AsString);
                                    end;

                                    Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                    Objmateriaisdevolucao.Submit_MATERIAL('4');
                                    Objmateriaisdevolucao.Salvar(false);

                              end;

                         end;

                         if (Query.Fieldbyname('material').asstring='PERSIANA') then
                         begin
                              with QueryDeleta do
                              begin
                                    Close;
                                    SQL.Clear;
                                    SQL.Add('select * from tabmateriaistroca');
                                    SQL.Add('where pedido='+edtPedido.text);
                                    SQL.Add('and Persiana_pp='+Query.fieldbyname('codigopp').AsString);
                                    sql.Add('and concluido=''N''');
                                    SQL.Add('order by codigo');
                                    Open;
                                    Last;

                                    //guardando qual material foi devolvido
                                    Objmateriaisdevolucao.Status:=dsInsert;
                                    Objmateriaisdevolucao.ZerarTabela;
                                    Objmateriaisdevolucao.Submit_CODIGO('0');
                                    Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                    if(recordcount>0) then
                                    begin
                                          First;
                                          Objmateriaisdevolucao.Submit_Persiana(fieldbyname('Persiana').asstring);
                                          Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                                    end
                                    else
                                    begin
                                          Objmateriaisdevolucao.Submit_Persiana(Query.fieldbyname('CodigoMaterial').asstring);
                                          Objmateriaisdevolucao.Submit_QUANTIDADE(Query.fieldbyname('quantidade').AsString);
                                    end;

                                    Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                    Objmateriaisdevolucao.Submit_MATERIAL('6');
                                    Objmateriaisdevolucao.Salvar(false);

                              end;

                         end;

                         if (Query.Fieldbyname('material').asstring='DIVERSO') then
                         begin
                              with QueryDeleta do
                              begin
                                    Close;
                                    SQL.Clear;
                                    SQL.Add('select * from tabmateriaistroca');
                                    SQL.Add('where pedido='+edtPedido.text);
                                    SQL.Add('and Diverso_pp='+Query.fieldbyname('codigopp').AsString);
                                    sql.Add('and concluido=''N''');
                                    SQL.Add('order by codigo');
                                    Open;
                                    Last;

                                    //guardando qual material foi devolvido
                                    Objmateriaisdevolucao.Status:=dsInsert;
                                    Objmateriaisdevolucao.ZerarTabela;
                                    Objmateriaisdevolucao.Submit_CODIGO('0');
                                    Objmateriaisdevolucao.Submit_PEDIDO(edtPedido.Text);
                                    if(recordcount>0) then
                                    begin
                                                    First;
                                                    Objmateriaisdevolucao.Submit_Diverso(fieldbyname('Diverso').asstring);
                                                    Objmateriaisdevolucao.Submit_DiversoCOR(fieldbyname('Diversocor').asstring);
                                                    Objmateriaisdevolucao.Submit_QUANTIDADE(fieldbyname('quantidade').AsString);
                                    end
                                    else
                                    begin
                                                    Objmateriaisdevolucao.Submit_Diverso(Query.fieldbyname('CodigoMaterial').asstring);
                                                    Objmateriaisdevolucao.Submit_DiversoCOR(Query.fieldbyname('MaterialCOR').asstring);
                                                    Objmateriaisdevolucao.Submit_QUANTIDADE(Query.fieldbyname('quantidade').AsString);
                                    end;

                                    Objmateriaisdevolucao.Submit_DATADEVOLUCAO(DateToStr(Now));
                                    Objmateriaisdevolucao.Submit_MATERIAL('5');
                                    Objmateriaisdevolucao.Salvar(false);

                              end;

                         end;
                         query.Next;
                end;

          end;
     finally
            FreeAndNil(query);
            FreeAndNil(QueryDeleta);
            Objmateriaisdevolucao.Free;
     end;


end;

procedure TFtrocaMaterialpedido.edtPedidoDblClick(Sender: TObject);
var
Key: Word;
Shift: TShiftState;
begin
       key:=VK_F9;
       ObjpedidoObjetos.ObjMedidas_Proj.PedidoProjeto.EdtPedido_Concluido_KeyDown(sender,key,shift,nil);
end;

End.
