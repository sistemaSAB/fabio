NFE ORIENTACAO DANFE
RETRATO;

NFE FORMA DE EMISAO DO DANFE
NORMAL ON-LINE;

NFE AMBIENTE
HOMOLOGACAO;

NFE CAMINHO LOGO MARCA DANFE
\\192.168.25.90\public\NF-E\logo.bmp;

NFE SALVAR ARQUIVOS DE ENVIO E RESPOSTA
-1;

NFE CAMINHO DOS ARQUIVOS DE ENVIO E RESPOSTA
\\192.168.25.90\public\NF-E\ENVIO E RESPOSTA\;

NFE CAMINHO DOS ARQUIVOS XML
\\192.168.25.90\public\NF-E\XML\;

CAMINHO XML DA CARTA DE CORRECAO
\\192.168.25.90\public\NF-E\CARTACORRECAO\;

NFE CAMINHO DOS ARQUIVOS DE CANCELAMENTO
\\192.168.25.90\public\NF-E\CANCELAMENTO\;

NFE CAMINHO DOS ARQUIVOS DE INUTILIZACAO
\\192.168.25.90\public\NF-E\INUTILIZACAO\;

NFE UF WEBSERVICE
MS;

NFE VISUALIZAR MENSAGEM DE TRANSMISSAO
0;

NFE ARQUIVO RAVE
\\192.168.25.90\public\NF-E\NotaFiscalEletronica.rav;

NFE DIRETORIO BACKUP
\\192.168.25.90\public\NF-E\NFE XML DE BACKUP\;

NFE DESENVOLVEDOR DO SISTEMA
Exclaim Tecnologia;

EXPANDIR LOGO MARCA
0;

CERTIFICADO
280115092257EF04;

ZERA IMPOSTO NO SIMPLES
0;

IDE
select nf.naturezaoperacao,'1' as serienf,nf.indpag,CAST (nf.dataemissao||' '||nf.horaemissao AS timestamp) as dhEmi,
CAST(nf.datasaida||' '||nf.horasaida AS timestamp) as dhSaiEnt,nf.horasaida
from tabnotafiscal nf
where nf.codigo = :pcodigoNF;

INFADIC
select nf.dadosadicionais
from tabnotafiscal nf 
where nf.codigo = :pcodigoNF;

EMITENTE
select e.cnpj,e.razaosocial,e.ie, '' as IEST,e.fantasia,e.estado,e.crt
from tabempresa e
where e.codigo = 1;

ENDERECO EMITENTE
select e.fone,e.cep,e.endereco,e.numero,e.complemento,e.bairro,e.codigocidade,
e.cidade,e.estado,'1058' as CODIGOPAIS,'BRASIL' as PAIS
from tabempresa e
where e.codigo = 1;

DESTINATARIO CLIENTE
select c.cpf_cgc,'' AS ieprodutorrural,c.nome,c.estado,c.rg_ie,c.fisica_juridica,
c.email
from tabcliente c
inner join tabnotafiscal nf on nf.cliente = c.codigo
where nf.codigo = :pCodigoNF;

DESTINATARIO FORNECEDOR
select f.cgc,f.ie,f.razaosocial,f.estado,f.email
from tabfornecedor f
inner join tabnotafiscal nf on nf.fornecedor = f.codigo
where nf.codigo = :pCodigoNF;

ENDERECO DESTINATARIO CLIENTE
select c.cep,c.codigocidade,c.estado,c.fone,
c.bairro,c.codigopais,'BRASIL' as pais,c.endereco,
c.cidade,c.numero
from tabcliente c
inner join tabnotafiscal nf on nf.cliente = c.codigo
where nf.codigo = :pCodigoNF;

ENDERECO DESTINATARIO FORNECEDOR
select f.cep,f.codigocidade,f.estado,f.fone,f.bairro,
f.codigopais,'BRASIL' as pais,f.endereco,f.cidade,f.numero
from tabfornecedor f
inner join tabnotafiscal nf on nf.fornecedor = f.codigo
where nf.codigo = :pCodigoNF;

TRANSPORTE
select nf.freteporcontatransportadora
from tabnotafiscal nf
where nf.codigo = :pCodigoNF;

TRANSPORTA
select t.cpfcnpj,t.ie,t.municipio,t.uf,t.endereco,t.nome
from tabtransportadora t
inner join tabnotafiscal nf on nf.transportadora = t.codigo
where nf.codigo = :pCodigoNF;

VEICULO TRANSPORTADORA
select t.placa,t.ufplaca
from tabtransportadora t
inner join tabnotafiscal nf on nf.transportadora = t.codigo
where nf.codigo = :pCodigoNF;

VOLUMES TRANSPORTADOS
select nf.quantidade,nf.especie,nf.marca,nf.numerovolumes,nf.pesobruto,nf.pesoliquido
from tabnotafiscal nf
where nf.codigo = :pCodigoNF;

PRODUTOS 
select pnf.cfop,
case when(pnf.projeto is not null) then pnf.projeto
else pnf.codigoproduto end as produto,pnf.descricao,pnf.quantidade,pnf.quantidade as qTrib,
pnf.unidade,pnf.unidade as uTrib,pnf.valorunitario,pnf.valorunitario as vUnTrib,
pnf.valorfinal,pnf.desconto,pnf.ncm,pnf.valorfrete,pnf.valorseguro,
pnf.valoroutros,pnf.codigo,null as codigoanp,pnf.cest
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNF;

ICMS NORMAL
select pnf.situacaotributaria_tabelaa,pnf.situacaotributaria_tabelab,pnf.csosn,
pnf.bc_icms,pnf.valor_icms,pnf.percentualicms,pnf.reducaobasecalculo,pnf.vtottrib
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

ICMS ST 
select pnf.reducaobasecalculo_st,pnf.bc_icms_st,pnf.percentualicmsst,
pnf.valor_icms_st,pnf.situacaotributaria_tabelab
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

PIS
select pnf.bc_pis,pnf.percentual_pis,pnf.valor_pis,'0' as vAliqProd,
'0' as qBCprod,pnf.cst_pis
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

COFINS
select pnf.bc_cofins,pnf.percentual_cofins,pnf.valor_cofins,'0' as vAliqProd,
'0' as qBCprod,pnf.cst_cofins
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

IPI
select pnf.cst_ipi,pnf.bc_ipi,pnf.percentual_ipi,pnf.valor_ipi
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;

COBRANCA FATURA
select f.nfat,f.vorig,f.vdesc,f.vliq
from tabcobrfatura f
where f.notafiscal = :pCodigoNF;

COBRANCA DUPLICATA
select d.ndup,d.dvenc,d.vdup
from tabcobrduplicata d
inner join tabcobrfatura f on f.codigo = d.cobrfatura
where f.notafiscal = :pCodigoNF;

PROXIMO NUMERO
select first 1 codigo,numero
from tabnotafiscal
where upper (situacao) = 'N' and modelo_nf = 2
order by codigo;

CODIGO UF EMITENTE
select e.codigoestado
from tabempresa e
where e.codigo = 1;

CODIGO CIDADE EMITENTE
select e.codigocidade
from tabempresa e
where e.codigo = 1;

EVENTO CCE
select MAX(cc.NSEQEVENTO)  from TABCARTACORRECAO cc  
where cc.chaveacesso = :chaveAcesso;

AUTORIZACAO XML
select case when (c.cnpj is null or (c.cnpj='')) then c.cpf else c.cnpj end
from tabcontador c;

DIFAL
select pnf.BC_ICMS_UF_DEST, pnf.PERCENTUAL_ICMS_UF_DEST, pnf.VALOR_ICMS_UF_DEST
,pnf.VALOR_ICMS_UF_REMET, pnf.PERCENTUAL_ICMS_INTER, pnf.PERCENTUAL_ICMS_INTER_PART
,pnf.PERCENTUAL_FCP_UF_DEST, pnf.VALOR_FCP_UF_DEST
from viewprodutosvenda_sped pnf
where pnf.notafiscal = :pCodigoNF and pnf.codigo = :pCodigoProdNF;
