unit UobjNOTAFISCALCFOP;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,uobjnotafiscal,uobjcfop;

Type
   TObjNOTAFISCALCFOP=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                NOTAFISCAL:TOBJNOTAFISCAL;
                CFOP:TOBJCFOP;
//CODIFICA VARIAVEIS PUBLICAS



                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaNota(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    exclui_Cfop(Pnota:string;PCfop:string):boolean;

                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;
                Function    RetornaCFOP_por_Nota(Pnota:string;var PCfop:String;var PNaturezaOperacao:String):Boolean;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                procedure EdtNOTAFISCALExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;Psituacao:String);overload;
                procedure EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                procedure EdtNOTAFISCAL_naousada_KeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);


                procedure EdtCFOPExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                //CODIFICA DECLARA GETSESUBMITS




                
         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UNotaFiscal, UCFOP;


{ TTabTitulo }


Function  TObjNOTAFISCALCFOP.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('NOTAFISCAL').asstring<>'')
        Then Begin
                 If (Self.NOTAFISCAL.LocalizaCodigo(FieldByName('NOTAFISCAL').asstring)=False)
                 Then Begin
                          Messagedlg('Nota Fiscal N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.NOTAFISCAL.TabelaparaObjeto;
        End;
        If(FieldByName('CFOP').asstring<>'')
        Then Begin
                 If (Self.CFOP.LocalizaCodigo(FieldByName('CFOP').asstring)=False)
                 Then Begin
                          Messagedlg('Cfop N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CFOP.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO



        result:=True;
     End;
end;


Procedure TObjNOTAFISCALCFOP.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('NOTAFISCAL').asstring:=Self.NOTAFISCAL.GET_CODIGO;
        ParamByName('CFOP').asstring:=Self.CFOP.GET_CODIGO;
//CODIFICA OBJETOPARATABELA



  End;
End;

//***********************************************************************

function TObjNOTAFISCALCFOP.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjNOTAFISCALCFOP.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        NOTAFISCAL.ZerarTabela;
        CFOP.ZerarTabela;
//CODIFICA ZERARTABELA



     End;
end;

Function TObjNOTAFISCALCFOP.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (NOTAFISCAL.get_codigo='')
      Then Mensagem:=mensagem+'/Nota Fiscal';
      If (CFOP.get_codigo='')
      Then Mensagem:=mensagem+'/Cfop';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjNOTAFISCALCFOP.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.NOTAFISCAL.LocalizaCodigo(Self.NOTAFISCAL.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Nota Fiscal n�o Encontrado!';

      If (Self.CFOP.LocalizaCodigo(Self.CFOP.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Cfop '+Self.CFOP.Get_CODIGO+' n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjNOTAFISCALCFOP.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.NOTAFISCAL.Get_Codigo<>'')
        Then Strtoint(Self.NOTAFISCAL.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Nota Fiscal';
     End;
     try
        If (Self.CFOP.Get_Codigo<>'')
        Then Strtoint(Self.CFOP.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cfop';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjNOTAFISCALCFOP.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjNOTAFISCALCFOP.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjNOTAFISCALCFOP.LocalizaNota(Parametro: string): boolean;
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NOTAFISCAL,CFOP');
           SQL.ADD(' from  TABNOTAFISCALCFOP');
           SQL.ADD(' WHERE Notafiscal='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjNOTAFISCALCFOP.LocalizaCodigo(parametro: string): boolean;//ok
begin
       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,NOTAFISCAL,CFOP');
           SQL.ADD(' from  TABNOTAFISCALCFOP');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjNOTAFISCALCFOP.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjNOTAFISCALCFOP.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;

function TObjNOTAFISCALCFOP.exclui_Cfop(Pnota, PCfop: string): boolean;
begin
     result:=False;
     With Self.Objquery do
     begin
          close;
          sql.clear;
          sql.add('Delete from tabnotafiscalcfop');
          sql.add('where TabNotaFiscalCFOP.notafiscal='+Pnota);
          sql.add('and TabNotaFiscalCFOP.CFOP='+Pcfop);
          Try
            execsql;
            FdataModulo.IBTransaction.CommitRetaining;
            result:=True;
          Except
                on e:exception do
                Begin
                    FdataModulo.IBTransaction.RollbackRetaining;
                    mensagemerro('Erro: '+e.message);
                    exit;
                End;
          End;
     End;
end;

constructor TObjNOTAFISCALCFOP.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.NOTAFISCAL:=TOBJNOTAFISCAL.create;
        Self.CFOP:=TOBJCFOP.create;
//CODIFICA CRIACAO DE OBJETOS



        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABNOTAFISCALCFOP(CODIGO,NOTAFISCAL,CFOP');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:NOTAFISCAL,:CFOP)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABNOTAFISCALCFOP set CODIGO=:CODIGO,NOTAFISCAL=:NOTAFISCAL');
                ModifySQL.add(',CFOP=:CFOP');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABNOTAFISCALCFOP where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjNOTAFISCALCFOP.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjNOTAFISCALCFOP.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabNOTAFISCALCFOP');
     Result:=Self.ParametroPesquisa;
end;

function TObjNOTAFISCALCFOP.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de NOTAFISCALCFOP ';
end;


function TObjNOTAFISCALCFOP.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENNOTAFISCALCFOP,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENNOTAFISCALCFOP,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjNOTAFISCALCFOP.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.NOTAFISCAL.FREE;
Self.CFOP.FREE;
//CODIFICA DESTRUICAO DE OBJETOS



end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjNOTAFISCALCFOP.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjNOTAFISCALCFOP.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjNOTAFISCALCFOP.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjNOTAFISCALCFOP.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
//CODIFICA GETSESUBMITS


procedure TObjNOTAFISCALCFOP.EdtNOTAFISCALExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.NOTAFISCAL.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.NOTAFISCAL.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.NOTAFISCAL.GET_NOME;
End;


procedure TObjNOTAFISCALCFOP.EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
begin
     Self.EdtNOTAFISCALKeyDown(sender,key,shift,labelnome,'');

End;

procedure TObjNOTAFISCALCFOP.EdtNOTAFISCALKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel;Psituacao:String);
var
   FpesquisaLocal:Tfpesquisa;
   FNOTAFISCAL:TFNOTAFISCAL;
   tempSql:String;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FNOTAFISCAL:=TFNOTAFISCAL.create(nil);
            tempsql:='';

            if (psituacao='')
            Then tempsql:=Self.NOTAFISCAL.Get_Pesquisa.Text
            Else tempsql:=Self.NOTAFISCAL.Get_Pesquisa(psituacao).Text;

            If (FpesquisaLocal.PreparaPesquisa(tempsql,Self.NOTAFISCAL.Get_TituloPesquisa,FNOTAFISCAL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NOTAFISCAL.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.NOTAFISCAL.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NOTAFISCAL.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FNOTAFISCAL);
     End;
end;

procedure TObjNOTAFISCALCFOP.EdtCFOPExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CFOP.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CFOP.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CFOP.GET_NOME;
End;
procedure TObjNOTAFISCALCFOP.EdtCFOPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCFOP:TFCFOP;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCFOP:=TFCFOP.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.CFOP.Get_Pesquisa,Self.CFOP.Get_TituloPesquisa,FCFOP)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CFOP.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CFOP.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CFOP.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCFOP);
     End;
end;
//******************
procedure TObjNOTAFISCALCFOP.EdtCFOPKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FCFOP:TFCFOP;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCFOP:=TFCFOP.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.CFOP.Get_Pesquisa,Self.CFOP.Get_TituloPesquisa,FCFOP)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CFOP.RETORNACAMPOCODIGO).asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
            End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCFOP);
     End;
end;



//CODIFICA EXITONKEYDOWN


function TObjNOTAFISCALCFOP.RetornaCFOP_por_Nota(Pnota: string;var PCfop:String;var PNaturezaOperacao:String): Boolean;
begin
     result:=False;
     
     PCfop:='';
     PNaturezaOperacao:='';

     if (Self.LocalizaNOta(pnota)=False)
     Then exit;

     Self.Objquery.first;

     Self.CFOP.LocalizaCodigo(Self.Objquery.fieldbyname('cfop').asstring);
     Self.CFOP.TabelaparaObjeto;

     PCfop:=Self.CFOP.Get_CODIGO;
     PNaturezaOperacao:=Self.CFOP.Get_NOME;

     Self.Objquery.Next;

     While not(Self.Objquery.eof) do
     Begin
          Self.CFOP.LocalizaCodigo(Self.Objquery.fieldbyname('cfop').asstring);
          Self.CFOP.TabelaparaObjeto;

          PCfop:=PCfop+'/'+Self.CFOP.Get_CODIGO;
          PNaturezaOperacao:=PNaturezaOperacao+'/'+Self.CFOP.Get_NOME;

          Self.Objquery.next;
     End;
     result:=true;
end;





procedure TObjNOTAFISCALCFOP.EdtNOTAFISCALKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.EdtNOTAFISCALKeyDown(sender,key,shift,nil);
end;

procedure TObjNOTAFISCALCFOP.EdtNOTAFISCAL_naousada_KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     Self.EdtNOTAFISCALKeyDown(sender,key,shift,nil,'N');
end;

end.



