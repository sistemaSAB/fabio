unit UobjCLIENTE;
Interface
Uses rdprint,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJRAMOATIVIDADE,
UOBJtipocliente,UobjPlanodeContas,uobjcidade, UPLanodeContas;

Type
   TObjCLIENTE=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                RamoAtividade:TOBJRAMOATIVIDADE;
                tipocliente:TOBJtipocliente;
                CodigoPlanoContas:TObjPlanodeContas;
                ObjCidade :TObjCidade;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaCodigo2(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_Pesquisa2                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Codigo2(parametro: string);
                Function Get_Codigo2: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                Procedure Submit_Sexo(parametro: string);
                Function Get_Sexo: string;
                Procedure Submit_Fantasia(parametro: string);
                Function Get_Fantasia: string;
                Procedure Submit_Fisica_Juridica(parametro: string);
                Function Get_Fisica_Juridica: string;



                Procedure Submit_CPF_CGC(parametro: string);
                Function Get_CPF_CGC: string;
                Procedure Submit_RG_IE(parametro: string);
                Function Get_RG_IE: string;
                Procedure Submit_email(parametro: string);
                Function Get_email: string;
                Procedure Submit_Fone(parametro: string);
                Function Get_Fone: string;
                Procedure Submit_Celular(parametro: string);
                Function Get_Celular: string;
                Procedure Submit_Fax(parametro: string);
                Function Get_Fax: string;
                Procedure Submit_Endereco(parametro: string);
                Function Get_Endereco: string;
                Procedure Submit_Bairro(parametro: string);
                Function Get_Bairro: string;
                Procedure Submit_Cidade(parametro: string);
                Function Get_Cidade: string;
                Procedure Submit_CEP(parametro: string);
                Function Get_CEP: string;
                Procedure Submit_Estado(parametro: string);
                Function Get_Estado: string;
                Procedure Submit_DataCadastro(parametro: string);
                Function Get_DataCadastro: string;
                Procedure Submit_DataUltimaCompra(parametro: string);
                Function Get_DataUltimaCompra: string;
                Procedure Submit_ValorUltimaCompra(parametro: string);
                Function Get_ValorUltimaCompra: string;
                Procedure Submit_DataMaiorFatura(parametro: string);
                Function Get_DataMaiorFatura: string;
                Procedure Submit_ValorMaiorFatura(parametro: string);
                Function Get_ValorMaiorFatura: string;
                Procedure Submit_SituacaoSerasa(parametro: string);
                Function Get_SituacaoSerasa: string;
                Procedure Submit_LimiteCredito(parametro: string);
                Function Get_LimiteCredito: string;
                Procedure Submit_Banco(parametro: string);
                Function Get_Banco: string;
                Procedure Submit_Agencia(parametro: string);
                Function Get_Agencia: string;
                Procedure Submit_ContaCorrente(parametro: string);
                Function Get_ContaCorrente: string;
                Procedure Submit_DataNascimento(parametro: string);
                Function Get_DataNascimento: string;
                Procedure Submit_PermiteVendaAPrazo(parametro: string);
                Function Get_PermiteVendaAPrazo: string;
                Procedure Submit_Ativo(parametro: string);
                Function Get_Ativo: string;
                Procedure Submit_Observacao(parametro: string);
                Function Get_Observacao: string;
                Procedure Submit_Contato(parametro: string);
                Function Get_Contato: string;
                Procedure Submit_Numero(parametro: string);
                Function Get_Numero: string;
                Procedure Submit_Complemento(parametro: string);
                Function Get_Complemento: string;

                Procedure Submit_codigocidade(parametro: string);
                Function Get_codigocidade: string;


                Function Get_ValorCredito:string;
                Procedure Submit_ValorCredito(parametro:string);

                function get_codigoPais: string;
                procedure submit_codigoPais(parametro: string);



                procedure EdtRamoAtividadeExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdttipoclienteExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCodigoPlanoContasExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdttipoclienteKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtCodigoPlanoContasKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                Function fieldbyname(PCampo:String):String;overload;
                Procedure fieldbyname(PCampo:String;PValor:String);overload;

                function PrimeiroRegistro: Boolean;
                function ProximoRegisto(PCodigo: Integer): Boolean;
                function RegistoAnterior(PCodigo: Integer): Boolean;
                function UltimoRegistro: Boolean;

                //function RetornaCredito(StrCodigo: String) :String;
                function   carregaCidades(comboBoxCidade:TComboBox): boolean;
                function   retornaUF(str: string): string;
                function   retornaCidade(str: string): string;
                function   retornaCodigoCidade(cidade, uf: string): string;
                function   carregaCodigoPaises(comboBoxPais: TComboBox): boolean;
                function   retornaPais(parametro: string): string;
                function   retornaCodigoPais(parametro: string): string;



                

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               Codigo:string;
               Codigo2:string;

               Nome:string;
               Sexo:string;
               Fantasia:string;
               Fisica_Juridica:string;
               CPF_CGC:string;
               RG_IE:string;
               email:string;
               Fone:string;
               Celular:string;
               Fax:string;
               Endereco:string;
               Bairro:string;
               Cidade:string;
               CEP:string;
               Estado:string;
               DataCadastro:string;
               DataUltimaCompra:string;
               ValorUltimaCompra:string;
               DataMaiorFatura:string;
               ValorMaiorFatura:string;
               SituacaoSerasa:string;
               LimiteCredito:string;
               Banco:string;
               Agencia:string;
               ContaCorrente:string;
               DataNascimento:string;
               PermiteVendaAPrazo:string;
               Ativo:string;
               Observacao:string;
               Contato:string;
               Numero :string;
               Complemento:string;
               codigocidade:string;
               codigoPais:string;
               ValorCredito:string;


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ImprimeClienteCredito;
                Procedure ImprimeClienteDados(parametro: string);
                function RetornaCredito(Codigo: String) :String;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,UMenuRelatorios,
  URAMOATIVIDADE,UtipoCliente, UReltxtRDPRINT;


{ TTabTitulo }


Function  TObjCLIENTE.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        Self.Codigo2:=fieldbyname('Codigo2').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.Sexo:=fieldbyname('Sexo').asstring;
        Self.Fantasia:=fieldbyname('Fantasia').asstring;
        Self.Fisica_Juridica:=fieldbyname('Fisica_Juridica').asstring;
        Self.CPF_CGC:=fieldbyname('CPF_CGC').asstring;
        Self.RG_IE:=fieldbyname('RG_IE').asstring;
        self.codigoPais := Fieldbyname ('codigoPais').AsString;

        Self.ValorCredito:=Fieldbyname('ValorCredito').asstring;
        

        If(FieldByName('RamoAtividade').asstring<>'')
        Then Begin
                 If (Self.RamoAtividade.LocalizaCodigo(FieldByName('RamoAtividade').asstring)=False)
                 Then Begin
                          Messagedlg('Ramo Atividade N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.RamoAtividade.TabelaparaObjeto;
        End;

        If(FieldByName('tipocliente').asstring<>'')
        Then Begin
                 If (Self.tipocliente.LocalizaCodigo(FieldByName('tipocliente').asstring)=False)
                 Then Begin
                          Messagedlg('Tipo de Cliente N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.tipocliente.TabelaparaObjeto;
        End;

        If(FieldByName('CodigoPlanoContas').asstring<>'')
        Then Begin
                 If (Self.CodigoPlanoContas.LocalizaCodigo(FieldByName('CodigoPlanoContas').asstring)=False)
                 Then Begin
                          Messagedlg('Codigo Plano de Contas N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CodigoPlanoContas.TabelaparaObjeto;
        End;

        Self.email:=fieldbyname('email').asstring;
        Self.Fone:=fieldbyname('Fone').asstring;
        Self.Celular:=fieldbyname('Celular').asstring;
        Self.Fax:=fieldbyname('Fax').asstring;
        Self.Endereco:=fieldbyname('Endereco').asstring;
        Self.Bairro:=fieldbyname('Bairro').asstring;
        Self.Cidade:=fieldbyname('Cidade').asstring;
        Self.CEP:=fieldbyname('CEP').asstring;
        Self.Estado:=fieldbyname('Estado').asstring;
        Self.DataCadastro:=fieldbyname('DataCadastro').asstring;
        Self.DataUltimaCompra:=fieldbyname('DataUltimaCompra').asstring;
        Self.ValorUltimaCompra:=fieldbyname('ValorUltimaCompra').asstring;
        Self.DataMaiorFatura:=fieldbyname('DataMaiorFatura').asstring;
        Self.ValorMaiorFatura:=fieldbyname('ValorMaiorFatura').asstring;
        Self.SituacaoSerasa:=fieldbyname('SituacaoSerasa').asstring;
        Self.LimiteCredito:=fieldbyname('LimiteCredito').asstring;
        Self.Banco:=fieldbyname('Banco').asstring;
        Self.Agencia:=fieldbyname('Agencia').asstring;
        Self.ContaCorrente:=fieldbyname('ContaCorrente').asstring;
        Self.DataNascimento:=fieldbyname('DataNascimento').asstring;
        Self.PermiteVendaAPrazo:=fieldbyname('PermiteVendaAPrazo').asstring;
        Self.Ativo:=fieldbyname('Ativo').asstring;
        Self.Observacao:=fieldbyname('Observacao').asstring;
        Self.Contato:=fieldbyname('Contato').AsString;
        Self.Numero:=fieldbyname('Numero').AsString;
        Self.Complemento:=fieldbyname('Complemento').AsString;
        Self.codigocidade:=fieldbyname('codigocidade').AsString;
        result:=True;
     End;
end;


Procedure TObjCLIENTE.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('Codigo').asstring:=Self.Codigo;
        ParamByName('Codigo2').asstring:=Self.Codigo2;
        ParamByName('Nome').asstring:=Self.Nome;
        ParamByName('Sexo').asstring:=Self.Sexo;
        ParamByName('Fantasia').asstring:=Self.Fantasia;
        ParamByName('Fisica_Juridica').asstring:=Self.Fisica_Juridica;
        ParamByName('CPF_CGC').asstring:=Self.CPF_CGC;
        ParamByName('RG_IE').asstring:=Self.RG_IE;
        ParamByName('RamoAtividade').asstring:=Self.RamoAtividade.GET_CODIGO;
        ParamByName('tipocliente').asstring:=Self.tipocliente.GET_CODIGO;
        ParamByName('email').asstring:=Self.email;
        ParamByName('Fone').asstring:=Self.Fone;
        ParamByName('Celular').asstring:=Self.Celular;
        ParamByName('Fax').asstring:=Self.Fax;
        ParamByName('Endereco').asstring:=Self.Endereco;
        ParamByName('Bairro').asstring:=Self.Bairro;
        ParamByName('Cidade').asstring:=Self.Cidade;
        ParamByName('CEP').asstring:=Self.CEP;
        ParamByName('Estado').asstring:=Self.Estado;
        ParamByName('DataCadastro').asstring:=Self.DataCadastro;
        ParamByName('DataUltimaCompra').asstring:=Self.DataUltimaCompra;
        ParamByName('ValorUltimaCompra').asstring:=virgulaparaponto(Self.ValorUltimaCompra);
        ParamByName('DataMaiorFatura').asstring:=Self.DataMaiorFatura;
        ParamByName('ValorMaiorFatura').asstring:=virgulaparaponto(Self.ValorMaiorFatura);
        ParamByName('SituacaoSerasa').asstring:=Self.SituacaoSerasa;
        ParamByName('LimiteCredito').asstring:=virgulaparaponto(Self.LimiteCredito);
        ParamByName('CodigoPlanoContas').asstring:=Self.CodigoPlanoContas.Get_CODIGO;
        ParamByName('Banco').asstring:=Self.Banco;
        ParamByName('Agencia').asstring:=Self.Agencia;
        ParamByName('ContaCorrente').asstring:=Self.ContaCorrente;
        ParamByName('DataNascimento').asstring:=Self.DataNascimento;
        ParamByName('PermiteVendaAPrazo').asstring:=Self.PermiteVendaAPrazo;
        ParamByName('Ativo').asstring:=Self.Ativo;
        ParamByName('Observacao').asstring:=Self.Observacao;
        ParamByName('Contato').AsString:=Self.Contato;
        Parambyname('Numero').AsString:=Self.Numero;
        ParamByName('Complemento').AsString:=Self.Complemento;
        ParamByName('codigocidade').AsString:=Self.codigocidade;
        Parambyname('ValorCredito').asstring:=virgulaparaponto(Self.ValorCredito);
        ParamByName('codigoPais').asstring:=self.codigoPais;
  End;
End;
//*********************************************************************
Procedure TObjCLIENTE.fieldbyname(PCampo:String;PValor:String);
Begin
     PCampo:=Uppercase(pcampo);

     If (Pcampo='CODIGO')
     Then Begin
              Self.Submit_CODIGO(pVALOR);
              exit;
     End;
     If (Pcampo='NOME')
     Then Begin
              Self.Submit_NOME(pVALOR);
              exit;
     End;
     If (Pcampo='FANTASIA')
     Then Begin
              Self.Submit_FANTASIA(pVALOR);
              exit;
     End;
     If (Pcampo='SEXO')
     Then Begin
              Self.Submit_SEXO(pVALOR);
              exit;
     End;
     If (Pcampo='FISICA_JURIDICA')
     Then Begin
              Self.Submit_FISICA_JURIDICA(pVALOR);
              exit;
     End;

     If (Pcampo='CPF_CGC')
     Then Begin
              Self.Submit_CPF_CGC(pVALOR);
              exit;
     End;
     If (Pcampo='RG_IE')
     Then Begin
              Self.Submit_RG_IE(pVALOR);
              exit;
     End;

     If (Pcampo='RAMOATIVIDADE')
     Then Begin
              Self.RAMOATIVIDADE.Submit_codigo(pVALOR);
              exit;
     End;

     If (Pcampo='TIPOCLIENTE')
     Then Begin
              Self.tipocliente.Submit_codigo(pVALOR);
              exit;
     End;

     If (Pcampo='EMAIL')
     Then Begin
              Self.Submit_EMAIL(pVALOR);
              exit;
     End;
     If (Pcampo='FONE')
     Then Begin
              Self.Submit_FONE(pVALOR);
              exit;
     End;
     If (Pcampo='CELULAR')
     Then Begin
              Self.Submit_CELULAR(pVALOR);
              exit;
     End;
     If (Pcampo='FAX')
     Then Begin
              Self.Submit_FAX(pVALOR);
              exit;
     End;
     If (Pcampo='DATACADASTRO')
     Then Begin
              Self.Submit_DATACADASTRO(pVALOR);
              exit;
     End;
     If (Pcampo='DATAULTIMACOMPRA')
     Then Begin
              Self.Submit_DATAULTIMACOMPRA(pVALOR);
              exit;
     End;
     If (Pcampo='VALORULTIMACOMPRA')
     Then Begin
              Self.Submit_VALORULTIMACOMPRA(pVALOR);
              exit;
     End;
     If (Pcampo='DATAMAIORFATURA')
     Then Begin
              Self.Submit_DATAMAIORFATURA(pVALOR);
              exit;
     End;
     If (Pcampo='VALORMAIORFATURA')
     Then Begin
              Self.Submit_VALORMAIORFATURA(pVALOR);
              exit;
     End;
     If (Pcampo='SITUACAOSERASA')
     Then Begin
              Self.Submit_SITUACAOSERASA(pVALOR);
              exit;
     End;
     If (Pcampo='LIMITECREDITO')
     Then Begin
              Self.Submit_LIMITECREDITO(pVALOR);
              exit;
     End;
     If (Pcampo='BANCO')
     Then Begin
              Self.Submit_BANCO(pVALOR);
              exit;
     End;
     If (Pcampo='AGENCIA')
     Then Begin
              Self.Submit_AGENCIA(pVALOR);
              exit;
     End;
     If (Pcampo='CONTACORRENTE')
     Then Begin
              Self.Submit_CONTACORRENTE(pVALOR);
              exit;
     End;
     If (Pcampo='DATANASCIMENTO')
     Then Begin
              Self.Submit_DATANASCIMENTO(pVALOR);
              exit;
     End;
     If (Pcampo='PERMITEVENDAAPRAZO')
     Then Begin
              Self.Submit_PERMITEVENDAAPRAZO(pVALOR);
              exit;
     End;
     If (Pcampo='ATIVO')
     Then Begin
              Self.Submit_ATIVO(pVALOR);
              exit;
     End;
     If (Pcampo='OBSERVACAO')
     Then Begin
              Self.Submit_OBSERVACAO(pVALOR);
              exit;
     End;
     If (Pcampo='CONTATO')
     Then Begin
              Self.Submit_CONTATO(pVALOR);
              exit;
     End;
     If (Pcampo='ENDERECO')
     Then Begin
              Self.Submit_ENDERECO(pVALOR);
              exit;
     End;
     If (Pcampo='BAIRRO')
     Then Begin
              Self.Submit_BAIRRO(pVALOR);
              exit;
     End;
     If (Pcampo='CIDADE')
     Then Begin
              Self.Submit_CIDADE(pVALOR);
              exit;
     End;
     If (Pcampo='CEP')
     Then Begin
              Self.Submit_CEP(pVALOR);
              exit;
     End;
     If (Pcampo='ESTADO')
     Then Begin
              Self.Submit_ESTADO(pVALOR);
              exit;
     End;
     If (Pcampo='NUMERO')
     Then Begin
              Self.Submit_NUMERO(pVALOR);
              exit;
     End;
     If (Pcampo='COMPLEMENTO')
     Then Begin
              Self.Submit_COMPLEMENTO(pVALOR);
              exit;
     End;

     If (Pcampo='CODIGOCIDADE')
     Then Begin
              Self.Submit_codigocidade(pVALOR);
              exit;
     End;


     If (Pcampo='CODIGOPLANOCONTAS')
     Then Begin
              Self.CODIGOPLANOCONTAS.Submit_codigo(pVALOR);
              exit;
     End;
     If (Pcampo='VALORCREDITO')
     Then Begin
              Self.Submit_VALORCREDITO(pVALOR);
              exit;
     End;
     If (Pcampo='CODIGO2')
     Then Begin
              Self.Submit_CODIGO2(pVALOR);
              exit;
     End;
end;

Function TObjCLIENTE.fieldbyname(PCampo:String):String;
Begin                                                          
     PCampo:=Uppercase(pcampo);                                
     Result:=''; 
     If (Pcampo='CODIGO')
     Then Begin
              Result:=Self.Get_CODIGO;
              exit;
     End;
     If (Pcampo='NOME')
     Then Begin
              Result:=Self.Get_NOME;
              exit;
     End;
     If (Pcampo='FANTASIA')
     Then Begin
              Result:=Self.Get_FANTASIA;
              exit;
     End;
     If (Pcampo='SEXO')
     Then Begin
              Result:=Self.Get_SEXO;
              exit;
     End;
     If (Pcampo='FISICA_JURIDICA')
     Then Begin
              Result:=Self.Get_FISICA_JURIDICA;
              exit;
     End;


     If (Pcampo='CPF_CGC')
     Then Begin
              Result:=Self.Get_CPF_CGC;
              exit;
     End;
     If (Pcampo='RG_IE')
     Then Begin
              Result:=Self.Get_RG_IE;
              exit;
     End;
     If (Pcampo='RAMOATIVIDADE')
     Then Begin
              Result:=Self.RAMOATIVIDADE.Get_codigo;
              exit;
     End;
     If (Pcampo='TIPOCLIENTE')
     Then Begin
              Result:=Self.tipocliente.Get_codigo;
              exit;
     End;
     If (Pcampo='EMAIL')
     Then Begin
              Result:=Self.Get_EMAIL;
              exit;
     End;
     If (Pcampo='FONE')
     Then Begin
              Result:=Self.Get_FONE;
              exit;
     End;
     If (Pcampo='CELULAR')
     Then Begin
              Result:=Self.Get_CELULAR;
              exit;
     End;
     If (Pcampo='FAX')
     Then Begin
              Result:=Self.Get_FAX;
              exit;
     End;
     If (Pcampo='DATACADASTRO')
     Then Begin
              Result:=Self.Get_DATACADASTRO;
              exit;
     End;
     If (Pcampo='DATAULTIMACOMPRA')
     Then Begin
              Result:=Self.Get_DATAULTIMACOMPRA;
              exit;
     End;
     If (Pcampo='VALORULTIMACOMPRA')
     Then Begin
              Result:=Self.Get_VALORULTIMACOMPRA;
              exit;
     End;
     If (Pcampo='DATAMAIORFATURA')
     Then Begin
              Result:=Self.Get_DATAMAIORFATURA;
              exit;
     End;
     If (Pcampo='VALORMAIORFATURA')
     Then Begin
              Result:=Self.Get_VALORMAIORFATURA;
              exit;
     End;
     If (Pcampo='SITUACAOSERASA')
     Then Begin
              Result:=Self.Get_SITUACAOSERASA;
              exit;
     End;
     If (Pcampo='LIMITECREDITO')
     Then Begin
              Result:=Self.Get_LIMITECREDITO;
              exit;
     End;
     If (Pcampo='BANCO')
     Then Begin
              Result:=Self.Get_BANCO;
              exit;
     End;
     If (Pcampo='AGENCIA')
     Then Begin
              Result:=Self.Get_AGENCIA;
              exit;
     End;
     If (Pcampo='CONTACORRENTE')
     Then Begin
              Result:=Self.Get_CONTACORRENTE;
              exit;
     End;
     If (Pcampo='DATANASCIMENTO')
     Then Begin
              Result:=Self.Get_DATANASCIMENTO;
              exit;
     End;
     If (Pcampo='PERMITEVENDAAPRAZO')
     Then Begin
              Result:=Self.Get_PERMITEVENDAAPRAZO;
              exit;
     End;
     If (Pcampo='ATIVO')
     Then Begin
              Result:=Self.Get_ATIVO;
              exit;
     End;
     If (Pcampo='OBSERVACAO')
     Then Begin
              Result:=Self.Get_OBSERVACAO;
              exit;
     End;
     If (Pcampo='CONTATO')
     Then Begin
              Result:=Self.Get_CONTATO;
              exit;
     End;
     If (Pcampo='ENDERECO')
     Then Begin
              Result:=Self.Get_ENDERECO;
              exit;
     End;
     If (Pcampo='BAIRRO')
     Then Begin
              Result:=Self.Get_BAIRRO;
              exit;
     End;
     If (Pcampo='CIDADE')
     Then Begin
              Result:=Self.Get_CIDADE;
              exit;
     End;
     If (Pcampo='CEP')
     Then Begin
              Result:=Self.Get_CEP;
              exit;
     End;
     If (Pcampo='ESTADO')
     Then Begin
              Result:=Self.Get_ESTADO;
              exit;
     End;
     If (Pcampo='NUMERO')
     Then Begin
              Result:=Self.Get_NUMERO;
              exit;
     End;
     If (Pcampo='COMPLEMENTO')
     Then Begin
              Result:=Self.Get_COMPLEMENTO;
              exit;
     End;

     If (Pcampo='CODIGOCIDADE')
     Then Begin
              Result:=Self.Get_codigocidade;
              exit;
     End;
     If (Pcampo='CODIGOPLANOCONTAS')
     Then Begin
              Result:=Self.CODIGOPLANOCONTAS.Get_codigo;
              exit;
     End;
     If (Pcampo='VALORCREDITO')
     Then Begin
              Result:=Self.Get_VALORCREDITO;
              exit;
     End;
     If (Pcampo='CODIGO2')
     Then Begin
              Result:=Self.Get_CODIGO2;
              exit;
     End;
end;





//***********************************************************************

function TObjCLIENTE.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       on e:exception do
       Begin
         if (Self.Status=dsInsert)
         Then Messagedlg('Erro na  tentativa de Inserir '+#13+e.Message,mterror,[mbok],0)
         Else Messagedlg('Erro na  tentativa de Editar '+#13+e.Message,mterror,[mbok],0);

         exit;
       End;

 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCLIENTE.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Codigo2:='';
        Nome:='';
        Sexo:='';
        Fantasia:='';
        Fisica_Juridica:='';
        CPF_CGC:='';
        RG_IE:='';
        RamoAtividade.ZerarTabela;
        tipocliente.ZerarTabela;
        email:='';
        Fone:='';
        Celular:='';
        Fax:='';
        Endereco:='';
        Bairro:='';
        Cidade:='';
        CEP:='';
        Estado:='';
        DataCadastro:='';
        DataUltimaCompra:='';
        ValorUltimaCompra:='';
        DataMaiorFatura:='';
        ValorMaiorFatura:='';
        SituacaoSerasa:='';
        LimiteCredito:='';
        Banco:='';
        Agencia:='';
        ContaCorrente:='';
        DataNascimento:='';
        PermiteVendaAPrazo:='';
        Ativo:='';
        Observacao:='';
        Contato:='';
        Numero:='';
        Complemento:='';
        codigocidade:='';
        CodigoPlanoContas.ZerarTabela;
        ValorCredito:='';
        codigoPais:='';
     End;
end;

Function TObjCLIENTE.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';

      If (Nome='')
      Then Mensagem:=mensagem+'/Nome';

      If (Sexo='')
      Then Mensagem:=mensagem+'/Sexo';

      If (Cidade='')
      Then Mensagem:=mensagem+'/Cidade';

      If (Fisica_Juridica='')
      Then Self.Fisica_Juridica:='F';

      if (ValorCredito='')
      Then Self.ValorCredito:='0';
      
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCLIENTE.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
      Result:=False;
      mensagem:='';
      
     { If (Self.RamoAtividade.LocalizaCodigo(Self.RamoAtividade.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Ramo Atividade n�o Encontrado!';    }

      If (Self.tipocliente.LocalizaCodigo(Self.tipocliente.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Tipo de Cliente n�o Encontrado!';

     { If (Self.CodigoPlanoContas.LocalizaCodigo(Self.CodigoPlanoContas.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Codigo Plano Contas n�o Encontrado!'; }


     { if (Self.ObjCidade.LocalizaNome(self.Cidade,Self.Estado)=False)
      Then Mensagem:=mensagem+'/A cidade '+Self.cidade+' n�o foi encontrada na UF '+self.Estado; }

//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCLIENTE.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     try
        If (Self.RamoAtividade.Get_Codigo<>'')
        Then Strtoint(Self.RamoAtividade.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Ramo Atividade';
     End;

     try
        If (Self.tipocliente.Get_Codigo<>'')
        Then Strtoint(Self.tipocliente.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Tipo de Cliente';
     End;
     {try
        Strtofloat(Self.ValorUltimaCompra);
     Except
           Mensagem:=mensagem+'/Valor Ultima Compra';
     End;
     try
        Strtofloat(Self.ValorMaiorFatura);
     Except
           Mensagem:=mensagem+'/Valor Maior Fatura';
     End;
     try
        Strtofloat(Self.LimiteCredito);
     Except
           Mensagem:=mensagem+'/Limite Cr�dito';
     End;    }

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCLIENTE.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
    { try
        Strtodate(Self.DataCadastro);
     Except
           Mensagem:=mensagem+'/Data Cadastro';
     End;

     if (Self.DataUltimaCompra<>'NULL')then
     try
        Strtodate(Self.DataUltimaCompra);
     Except
           Mensagem:=mensagem+'/Data Ultima Compra';
     End;
     try
        Strtodate(Self.DataMaiorFatura);
     Except
           Mensagem:=mensagem+'/Data Maior Fatura';
     End;   }
     try
        Strtodate(Self.DataNascimento);
     Except
           Mensagem:=mensagem+'/Data Nascimento';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCLIENTE.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
   temp:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';

        if (RetornaSoNUmeros(Self.CPF_CGC)='')
        then Self.CPF_CGC:='';
                             
        if ((self.CPF_CGC<>'')  and (FdataModulo.ValidaCPFCNPJ_cadastramento_Cliente=true))
        then Begin
                  temp:=RetornaSoNUmeros(Self.CPF_CGC);

                  if ((come(temp,'0')='')or (come(temp,'1')=''))//o cpf era somente zero ou1 pra passar no calculo
                  Then mensagem:=mensagem+'\Campo CPF_CNPJ Inv�lido'
                  Else Begin
                            if (length(temp)=11)//cpf
                            then Begin
                                      if (ValidaCPF(temp)=False)
                                      then mensagem:=mensagem+'\CPF Inv�lido';
                            End
                            Else Begin
                                      if (length(temp)=14)//cnpj
                                      then Begin
                                                if (ValidaCNPJ(temp)=False)
                                                then mensagem:=mensagem+'\CNPJ Inv�lido';
                                      End
                                      Else Mensagem:=mensagem+'\Campo CPF_CNPJ Inv�lido';
                            End;
                  End;
        End;

//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;


function TObjCLIENTE.LocalizaCodigo2(Parametro: string): boolean;
begin

       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CLIENTE vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,codigo2,TipoCliente,Nome,Sexo,Fantasia,Fisica_Juridica,CPF_CGC,RG_IE');
           SQL.ADD(' ,RamoAtividade,email,Fone,Celular,Fax,Endereco,Bairro');
           SQL.ADD(' ,Cidade,CEP,Estado');
           SQL.ADD(' ,DataCadastro,DataUltimaCompra');
           SQL.ADD(' ,ValorUltimaCompra,DataMaiorFatura,ValorMaiorFatura,SituacaoSerasa');
           SQL.ADD(' ,LimiteCredito,CodigoPlanoContas,Banco,Agencia,ContaCorrente,DataNascimento');
           SQL.ADD(' ,PermiteVendaAPrazo,Ativo,Observacao,COntato,Numero, Complemento,ValorCredito,codigocidade,codigoPais');
           SQL.ADD(' from  TabCliente');
           SQL.ADD(' WHERE Codigo2='+#39+parametro+#39);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;

end;

function TObjCLIENTE.LocalizaCodigo(parametro: string): boolean;//ok
begin
       Result:=false;

       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro CLIENTE vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           
           close;
           Sql.Clear;
           SQL.ADD('Select Codigo,codigo2,TipoCliente,Nome,Sexo,Fantasia,Fisica_Juridica,CPF_CGC,RG_IE');
           SQL.ADD(' ,RamoAtividade,email,Fone,Celular,Fax,Endereco,Bairro');
           SQL.ADD(' ,Cidade,CEP,Estado');
           SQL.ADD(' ,DataCadastro,DataUltimaCompra');
           SQL.ADD(' ,ValorUltimaCompra,DataMaiorFatura,ValorMaiorFatura,SituacaoSerasa');
           SQL.ADD(' ,LimiteCredito,CodigoPlanoContas,Banco,Agencia,ContaCorrente,DataNascimento');
           SQL.ADD(' ,PermiteVendaAPrazo,Ativo,Observacao,COntato,Numero, Complemento,ValorCredito,codigocidade,codigoPais');
           SQL.ADD(' from  TabCliente');
           SQL.ADD(' WHERE Codigo='+parametro);
           
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCLIENTE.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCLIENTE.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCLIENTE.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.RamoAtividade:=TOBJRAMOATIVIDADE.create;
        Self.tipocliente:=TOBJtipocliente.create;
        Self.CodigoPlanoContas:=TObjPlanodeContas.Create;
        Self.ObjCidade :=TObjCidade.create;
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabCliente(Codigo,codigo2,TipoCliente,Nome,Sexo,Fantasia,Fisica_Juridica');
                InsertSQL.add(' ,CPF_CGC,RG_IE,RamoAtividade,email,Fone,Celular,Fax');
                InsertSQL.add(' ,Endereco,Bairro,Cidade,CEP');
                InsertSQL.add(' ,Estado');
                InsertSQL.add(' ,DataCadastro,DataUltimaCompra');
                InsertSQL.add(' ,ValorUltimaCompra,DataMaiorFatura,ValorMaiorFatura');
                InsertSQL.add(' ,SituacaoSerasa,LimiteCredito,CodigoPlanoContas,Banco');
                InsertSQL.add(' ,Agencia,ContaCorrente,DataNascimento,PermiteVendaAPrazo');
                InsertSQL.add(' ,Ativo,Observacao, COntato, Numero, Complemento,ValorCredito,codigocidade,codigoPais)');
                InsertSQL.add('values (:Codigo,:codigo2,:TipoCliente,:Nome,:Sexo,:Fantasia,:Fisica_Juridica');
                InsertSQL.add(' ,:CPF_CGC,:RG_IE,:RamoAtividade,:email,:Fone,:Celular');
                InsertSQL.add(' ,:Fax,:Endereco,:Bairro,:Cidade');
                InsertSQL.add(' ,:CEP,:Estado');
                InsertSQL.add(' ,:DataCadastro');
                InsertSQL.add(' ,:DataUltimaCompra,:ValorUltimaCompra,:DataMaiorFatura');
                InsertSQL.add(' ,:ValorMaiorFatura,:SituacaoSerasa,:LimiteCredito,:CodigoPlanoContas');
                InsertSQL.add(' ,:Banco,:Agencia,:ContaCorrente,:DataNascimento,:PermiteVendaAPrazo');
                InsertSQL.add(' ,:Ativo,:Observacao,:COntato,:Numero,:Complemento,:ValorCredito,:codigocidade,:codigoPais)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabCliente set Codigo=:Codigo,codigo2=:codigo2,TipoCliente=:TipoCliente,Nome=:Nome,Sexo=:Sexo');
                ModifySQL.add(',Fantasia=:Fantasia,Fisica_Juridica=:Fisica_Juridica');
                ModifySQL.add(',CPF_CGC=:CPF_CGC,RG_IE=:RG_IE,RamoAtividade=:RamoAtividade');
                ModifySQL.add(',email=:email,Fone=:Fone,Celular=:Celular,Fax=:Fax,Endereco=:Endereco');
                ModifySQL.add(',Bairro=:Bairro,Cidade=:Cidade');
                ModifySQL.add(',CEP=:CEP,Estado=:Estado');
                ModifySQL.add(',DataCadastro=:DataCadastro');
                ModifySQL.add(',DataUltimaCompra=:DataUltimaCompra,ValorUltimaCompra=:ValorUltimaCompra');
                ModifySQL.add(',DataMaiorFatura=:DataMaiorFatura,ValorMaiorFatura=:ValorMaiorFatura');
                ModifySQL.add(',SituacaoSerasa=:SituacaoSerasa,LimiteCredito=:LimiteCredito');
                ModifySQL.add(',CodigoPlanoContas=:CodigoPlanoContas,Banco=:Banco,Agencia=:Agencia');
                ModifySQL.add(',ContaCorrente=:ContaCorrente,DataNascimento=:DataNascimento');
                ModifySQL.add(',PermiteVendaAPrazo=:PermiteVendaAPrazo,Ativo=:Ativo');
                ModifySQL.add(',Observacao=:Observacao,COntato=:COntato,Numero=:Numero,Complemento=:Complemento,ValorCredito=:ValorCredito');
                ModifySQL.add(',codigocidade=:codigocidade,codigoPais=:codigoPais where Codigo=:Codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabCliente where Codigo=:Codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCLIENTE.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCLIENTE.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.ADD('Select Codigo,codigo2,TipoCliente,Nome,Fantasia,Sexo,Fisica_Juridica,CPF_CGC,RG_IE');
     Self.ParametroPesquisa.ADD(' ,RamoAtividade,email,Fone,Celular,Fax,Endereco,Bairro');
     Self.ParametroPesquisa.ADD(' ,Cidade,CEP,Estado');
     Self.ParametroPesquisa.ADD(' ,DataCadastro,DataUltimaCompra');
     Self.ParametroPesquisa.ADD(' ,ValorUltimaCompra,DataMaiorFatura,ValorMaiorFatura,SituacaoSerasa');
     Self.ParametroPesquisa.ADD(' ,LimiteCredito,CodigoPlanoContas,Banco,Agencia,ContaCorrente,DataNascimento');
     Self.ParametroPesquisa.ADD(' ,PermiteVendaAPrazo,Ativo,Observacao,COntato,Numero,codigoPais');
     Self.ParametroPesquisa.Add('from TabCliente');                                               

     Result:=Self.ParametroPesquisa;
end;

function TObjCLIENTE.Get_Pesquisa2: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.ADD('Select Codigo,codigo2,TipoCliente,Nome,Fantasia,Sexo,Fisica_Juridica,CPF_CGC,RG_IE');
     Self.ParametroPesquisa.ADD(' ,RamoAtividade,email,Fone,Celular,Fax,Endereco,Bairro');
     Self.ParametroPesquisa.ADD(' ,Cidade,CEP,Estado');
     Self.ParametroPesquisa.ADD(' ,DataCadastro,DataUltimaCompra');
     Self.ParametroPesquisa.ADD(' ,ValorUltimaCompra,DataMaiorFatura,ValorMaiorFatura,SituacaoSerasa');
     Self.ParametroPesquisa.ADD(' ,LimiteCredito,CodigoPlanoContas,Banco,Agencia,ContaCorrente,DataNascimento');
     Self.ParametroPesquisa.ADD(' ,PermiteVendaAPrazo,Ativo,Observacao,COntato,Numero,codigoPais');
     Self.ParametroPesquisa.Add('from TabCliente');
     self.ParametroPesquisa.Add('where ativo=''S'' ');                                             

     Result:=Self.ParametroPesquisa;
end;

function TObjCLIENTE.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de CLIENTE ';
end;


function TObjCLIENTE.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCLIENTE,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCLIENTE,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCLIENTE.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.RamoAtividade.FREE;
    Self.tipocliente.FREE;
    Self.CodigoPlanoContas.Free;
    Self.ObjCidade.free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCLIENTE.RetornaCampoCodigo: string;
begin
      result:='Codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCLIENTE.RetornaCampoNome: string;
begin
      result:='nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjCliente.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjCliente.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjCliente.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TObjCliente.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
procedure TObjCliente.Submit_Sexo(parametro: string);
begin
        Self.Sexo:=Parametro;
end;
function TObjCliente.Get_Sexo: string;
begin
        Result:=Self.Sexo;
end;
procedure TObjCliente.Submit_Fantasia(parametro: string);
begin
        Self.Fantasia:=Parametro;
end;
function TObjCliente.Get_Fantasia: string;
begin
        Result:=Self.Fantasia;
end;
procedure TObjCliente.Submit_Fisica_Juridica(parametro: string);
begin
        Self.Fisica_Juridica:=Parametro;
end;
function TObjCliente.Get_Fisica_Juridica: string;
begin
        Result:=Self.Fisica_Juridica;
end;

procedure TObjCliente.Submit_CPF_CGC(parametro: string);
begin
        Self.CPF_CGC:=Parametro;
end;
function TObjCliente.Get_CPF_CGC: string;
begin
        Result:=Self.CPF_CGC;
end;
procedure TObjCliente.Submit_RG_IE(parametro: string);
begin
        Self.RG_IE:=Parametro;
end;
function TObjCliente.Get_RG_IE: string;
begin
        Result:=Self.RG_IE;
end;
procedure TObjCliente.Submit_email(parametro: string);
begin
        Self.email:=Parametro;
end;
function TObjCliente.Get_email: string;
begin
        Result:=Self.email;
end;
procedure TObjCliente.Submit_Fone(parametro: string);
begin
        Self.Fone:=Parametro;
end;
function TObjCliente.Get_Fone: string;
begin
        Result:=Self.Fone;
end;
procedure TObjCliente.Submit_Celular(parametro: string);
begin
        Self.Celular:=Parametro;
end;
function TObjCliente.Get_Celular: string;
begin
        Result:=Self.Celular;
end;
procedure TObjCliente.Submit_Fax(parametro: string);
begin
        Self.Fax:=Parametro;
end;
function TObjCliente.Get_Fax: string;
begin
        Result:=Self.Fax;
end;
procedure TObjCliente.Submit_Endereco(parametro: string);
begin
        Self.Endereco:=Parametro;
end;
function TObjCliente.Get_Endereco: string;
begin
        Result:=Self.Endereco;
end;
procedure TObjCliente.Submit_Bairro(parametro: string);
begin
        Self.Bairro:=Parametro;
end;
function TObjCliente.Get_Bairro: string;
begin
        Result:=Self.Bairro;
end;
procedure TObjCliente.Submit_Cidade(parametro: string);
begin
        Self.Cidade:=Parametro;
end;
function TObjCliente.Get_Cidade: string;
begin
        Result:=Self.Cidade;
end;
procedure TObjCliente.Submit_CEP(parametro: string);
begin
        Self.CEP:=Parametro;
end;
function TObjCliente.Get_CEP: string;
begin
        Result:=Self.CEP;
end;
procedure TObjCliente.Submit_Estado(parametro: string);
begin
        Self.Estado:=Parametro;
end;
function TObjCliente.Get_Estado: string;
begin
        Result:=Self.Estado;
end;
procedure TObjCliente.Submit_DataCadastro(parametro: string);
begin
        Self.DataCadastro:=Parametro;
end;
function TObjCliente.Get_DataCadastro: string;
begin
        Result:=Self.DataCadastro;
end;
procedure TObjCliente.Submit_DataUltimaCompra(parametro: string);
begin
        Self.DataUltimaCompra:=Parametro;
end;
function TObjCliente.Get_DataUltimaCompra: string;
begin
        Result:=Self.DataUltimaCompra;
end;
procedure TObjCliente.Submit_ValorUltimaCompra(parametro: string);
begin
        Self.ValorUltimaCompra:=Parametro;
end;
function TObjCliente.Get_ValorUltimaCompra: string;
begin
        Result:=Self.ValorUltimaCompra;
end;
procedure TObjCliente.Submit_DataMaiorFatura(parametro: string);
begin
        Self.DataMaiorFatura:=Parametro;
end;
function TObjCliente.Get_DataMaiorFatura: string;
begin
        Result:=Self.DataMaiorFatura;
end;
procedure TObjCliente.Submit_ValorMaiorFatura(parametro: string);
begin
        Self.ValorMaiorFatura:=Parametro;
end;
function TObjCliente.Get_ValorMaiorFatura: string;
begin
        Result:=Self.ValorMaiorFatura;
end;
procedure TObjCliente.Submit_SituacaoSerasa(parametro: string);
begin
        Self.SituacaoSerasa:=Parametro;
end;
function TObjCliente.Get_SituacaoSerasa: string;
begin
        Result:=Self.SituacaoSerasa;
end;
procedure TObjCliente.Submit_LimiteCredito(parametro: string);
begin
        Self.LimiteCredito:=Parametro;
end;
function TObjCliente.Get_LimiteCredito: string;
begin
        Result:=Self.LimiteCredito;
end;
procedure TObjCliente.Submit_Banco(parametro: string);
begin
        Self.Banco:=Parametro;
end;
function TObjCliente.Get_Banco: string;
begin
        Result:=Self.Banco;
end;
procedure TObjCliente.Submit_Agencia(parametro: string);
begin
        Self.Agencia:=Parametro;
end;
function TObjCliente.Get_Agencia: string;
begin
        Result:=Self.Agencia;
end;
procedure TObjCliente.Submit_ContaCorrente(parametro: string);
begin
        Self.ContaCorrente:=Parametro;
end;
function TObjCliente.Get_ContaCorrente: string;
begin
        Result:=Self.ContaCorrente;
end;
procedure TObjCliente.Submit_DataNascimento(parametro: string);
begin
        Self.DataNascimento:=Parametro;
end;
function TObjCliente.Get_DataNascimento: string;
begin
        Result:=Self.DataNascimento;
end;
procedure TObjCliente.Submit_PermiteVendaAPrazo(parametro: string);
begin
        Self.PermiteVendaAPrazo:=Parametro;
end;
function TObjCliente.Get_PermiteVendaAPrazo: string;
begin
        Result:=Self.PermiteVendaAPrazo;
end;
procedure TObjCliente.Submit_Ativo(parametro: string);
begin
        Self.Ativo:=Parametro;
end;
function TObjCliente.Get_Ativo: string;
begin
        Result:=Self.Ativo;
end;
procedure TObjCliente.Submit_Observacao(parametro: string);
begin
        Self.Observacao:=Parametro;
end;
function TObjCliente.Get_Observacao: string;
begin
        Result:=Self.Observacao;
end;
//CODIFICA GETSESUBMITS


procedure TObjCLIENTE.EdtRamoAtividadeExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.RamoAtividade.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.RamoAtividade.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.RamoAtividade.GET_NOME;
End;
procedure TObjCLIENTE.EdtRamoAtividadeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FRamoAtividade:TFRAMOATIVIDADE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FRAMOATIVIDADE:=TFRAMOATIVIDADE.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.RamoAtividade.Get_Pesquisa,Self.RamoAtividade.Get_TituloPesquisa,FRamoAtividade)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RamoAtividade.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.RamoAtividade.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.RamoAtividade.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FRamoAtividade);

     End;
end;
//CODIFICA EXITONKEYDOWN
procedure TObjCLIENTE.EdttipoclienteExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.tipocliente.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.tipocliente.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.tipocliente.GET_NOME;
End;
procedure TObjCLIENTE.EdttipoclienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FtipoCliente:TFTIPOCLIENTE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);
            FtipoCliente:=TFTIPOCLIENTE.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.tipocliente.Get_Pesquisa,Self.tipocliente.Get_TituloPesquisa,FtipoCliente)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.tipocliente.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.tipocliente.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.tipocliente.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           FreeAndNil(FtipoCliente);
     End;
end;

procedure TObjCLIENTE.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCLIENTE';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Clientes com saldo de Cr�dito');
                Items.add('Dados do Cliente');  {Rodolfo}
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
               0  : Self.ImprimeClienteCredito;
               1  : Self.ImprimeClienteDados(pcodigo);   {Rodolfo}
              -1  : exit;
          End;
     end;

end;


{Rodolfo}
procedure TObjCLIENTE.ImprimeClienteDados(parametro: string);
var
teste : string;
begin
     if (parametro='')
     Then Begin
          MensagemAviso('Nenhuma cliente foi selecionado');
          exit;
     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select * from tabcliente where codigo='+#39+parametro+#39);
          open;

          //teste := Fieldbyname('cliente').asstring;
          //Messagedlg(teste,mterror,[mbok],0);

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               Then Begin
                         RDprint.Fechar;
                         Exit;
               End;

               RDprint.ImpF(linhalocal,33,'RELAT�RIO DE CADASTRO PESSOAL',[negrito]);
               IncrementaLinha(2);
               DesenhaLinha;
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Nome:',[negrito]);
               RDprint.Imp(linhalocal,7,Fieldbyname('nome').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Fantasia:',[negrito]);
               RDprint.Imp(linhalocal,11,Fieldbyname('fantasia').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'CPF/CNPJ:',[negrito]);
               RDprint.Imp(linhalocal,11,Fieldbyname('cpf_cgc').asstring);

               RDprint.ImpF(linhalocal,35,'RG/IE:',[negrito]);
               RDprint.Imp(linhalocal,42,Fieldbyname('rg_ie').asstring);

               RDprint.ImpF(linhalocal,70,'Sexo:',[negrito]);
               RDprint.Imp(linhalocal,76,Fieldbyname('sexo').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'e-mail:',[negrito]);
               RDprint.Imp(linhalocal,9,Fieldbyname('email').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Fone:',[negrito]);
               RDprint.Imp(linhalocal,7,Fieldbyname('fone').asstring);

               RDprint.ImpF(linhalocal,35,'Fax:',[negrito]);
               RDprint.Imp(linhalocal,40,Fieldbyname('fax').asstring);

               RDprint.ImpF(linhalocal,70,'Celular:',[negrito]);
               RDprint.Imp(linhalocal,79,Fieldbyname('celular').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Ramo Atividade:',[negrito]);
               RDprint.Imp(linhalocal,17,Fieldbyname('ramoatividade').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Tipo de Cliente:',[negrito]);
               RDprint.Imp(linhalocal,18,Fieldbyname('tipocliente').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Contato:',[negrito]);
               RDprint.Imp(linhalocal,10,Fieldbyname('contato').asstring);
               IncrementaLinha(2);

               RDprint.ImpF(linhalocal,1,'Endere�o:',[negrito]);
               RDprint.Imp(linhalocal,11,Fieldbyname('endereco').asstring);

               RDprint.ImpF(linhalocal,70,'N�:',[negrito]);
               RDprint.Imp(linhalocal,74,Fieldbyname('numero').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Complemento:',[negrito]);
               RDprint.Imp(linhalocal,14,Fieldbyname('complemento').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Bairro:',[negrito]);
               RDprint.Imp(linhalocal,9,Fieldbyname('bairro').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'Cidade:',[negrito]);
               RDprint.Imp(linhalocal,9,retornaPalavrasAntesSimbolo(Fieldbyname('cidade').asstring,'|'));

               RDprint.ImpF(linhalocal,70,'Estado:',[negrito]);
               RDprint.Imp(linhalocal,78,Fieldbyname('estado').asstring);
               IncrementaLinha(1);

               RDprint.ImpF(linhalocal,1,'CEP:',[negrito]);
               RDprint.Imp(linhalocal,6,Fieldbyname('cep').asstring);
               IncrementaLinha(2);

               DesenhaLinha;
               DesenhaLinha;
               IncrementaLinha(1);


               RDprint.Fechar;

          End;


     End;
end;



function TObjCLIENTE.Get_Contato: string;
begin
     Result:=Self.Contato;
end;

procedure TObjCLIENTE.Submit_Contato(parametro: string);
begin
    Self.Contato:=parametro;
end;

function TObjCLIENTE.Get_Numero: string;
begin
   Result:=Self.Numero;
end;

procedure TObjCLIENTE.Submit_Numero(parametro: string);
begin
    Self.Numero:=parametro;
end;


function TObjCLIENTE.Get_Complemento: string;
begin
    Result:=Self.Complemento;
end;

procedure TObjCLIENTE.Submit_Complemento(parametro: string);
begin
    Self.Complemento:=parametro;
end;


function TObjCLIENTE.Get_codigocidade: string;
begin
    Result:=Self.codigocidade;
end;

procedure TObjCLIENTE.Submit_codigocidade(parametro: string);
begin
    Self.codigocidade:=parametro;
end;


procedure TObjCLIENTE.EdtCodigoPlanoContasExit(Sender: TObject;
  LABELNOME: TLABEL);
begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CodigoPlanoContas.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CodigoPlanoContas.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CodigoPlanoContas.GET_NOME;

end;

procedure TObjCLIENTE.EdtCodigoPlanoContasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState; LABELNOME: Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FplanoContas:TFplanodeContas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FplanoContas:=TFplanodeContas.Create(nil);
            If (FpesquisaLocal.PreparaPesquisa(Self.CodigoPlanoContas.Get_Pesquisa,Self.CodigoPlanoContas.Get_TituloPesquisa,FplanoContas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CodigoPlanoContas.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.CodigoPlanoContas.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CodigoPlanoContas.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);

     End;
end;

function TObjCLIENTE.Get_ValorCredito: string;
var
    QueryRetornaValorCredito:TIBQuery;
begin
     QueryRetornaValorCredito:=TIBQuery.Create(nil);
     QueryRetornaValorCredito.Database:=FDataModulo.IBDatabase;
     try
            with QueryRetornaValorCredito do
            begin
                Close;
                SQL.Clear;
                SQL.Add('select sum(valor) from tablancamentocredito where cliente='+Self.Codigo);
                Open;
                Result:=fieldbyname('sum').AsString;
            end;
     finally
            FreeAndNil(QueryRetornaValorCredito);
     end;



end;

procedure TObjCLIENTE.Submit_ValorCredito(parametro: string);
begin
     Self.ValorCredito:=Parametro;
end;

function TObjCLIENTE.Get_Codigo2: string;
begin
     Result:=Self.Codigo2;
end;

procedure TObjCLIENTE.Submit_Codigo2(parametro: string);
begin
     Self.Codigo2:=Parametro;
end;


procedure TObjCLIENTE.ImprimeClienteCredito;
var
psomacredito:currency;
begin
     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.text:=  //add('Select codigo,fantasia,valorcredito from tabcliente where valorcredito>0');
          'select tabcliente.codigo, tabcliente.nome,sum(valor) as credito '+
          'from tabcliente '+
          'join tablancamentocredito credito on credito.cliente=tabcliente.codigo '+
          'group by tabcliente.codigo, tabcliente.nome ';

          open;

          if (recordcount=0)
          Then Begin
                    MensagemAviso('Nenhuma informa��o foi selecionada');
                    exit;
          End;

          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               LinhaLocal:=3;
               RDprint.Abrir;

               if (RDprint.Setup=False)
               Then Begin
                         RDprint.Fechar;
                         Exit;
               End;

               RDprint.ImpF(linhalocal,45,'CLIENTE COM SALDO DE CR�DITO',[negrito]);
               IncrementaLinha(2);

               RDprint.ImpF(linhalocal,1,CompletaPalavra('CODIGO',6,' ')+' '+
                                         CompletaPalavra('FANTASIA',60,' ')+' '+
                                         CompletaPalavra_a_Esquerda('SALDO',20,' '),[negrito]);
               IncrementaLinha(1);
               DesenhaLinha;

               psomacredito:=0;

               While not(Self.Objquery.eof) do
               Begin
                     VerificaLinha;
                     RDprint.Imp(linhalocal,1,CompletaPalavra(fieldbyname('CODIGO').asstring,6,' ')+' '+
                                         CompletaPalavra(fieldbyname('NOME').asstring,60,' ')+' '+
                                         CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Credito').asstring),20,' '));
                    IncrementaLinha(1);

                    psomacredito:=psomacredito+fieldbyname('credito').ascurrency;
                    Self.Objquery.next;
               End;
               DesenhaLinha;
               VerificaLinha;
               RDprint.Impf(linhalocal,1,CompletaPalavra('TOTAL',6,' ')+' '+
                                        CompletaPalavra('',60,' ')+' '+
                                        CompletaPalavra_a_Esquerda(formata_valor(psomacredito),20,' '),[negrito]);
               IncrementaLinha(1);

               RDprint.Fechar;

          End;
     End;



end;



function TObjcliente.PrimeiroRegistro: Boolean;
Var MenorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from Tabcliente') ;
           Open;

           if (FieldByName('MenorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MenorCodigo:=fieldbyname('MenorCodigo').AsString;
           Self.LocalizaCodigo(MenorCodigo);

           Result:=true;
     end;
end;

function TObjcliente.UltimoRegistro: Boolean;
Var MaiorCodigo:string;
begin
     Result:=false;
     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from Tabcliente') ;
           Open;

           if (FieldByName('MaiorCodigo').AsInteger<0)then
           Begin
               Result:=False;
               exit;
           end;

           MaiorCodigo:=fieldbyname('MaiorCodigo').AsString;
           Self.LocalizaCodigo(MaiorCodigo);

           Result:=true;
     end;
end;

function TObjcliente.ProximoRegisto(PCodigo:Integer): Boolean;
Var MaiorValor:Integer;
begin
     Result:=false;

     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MAX(Codigo) as MaiorCodigo from Tabcliente') ;
           Open;

           MaiorValor:=fieldbyname('MaiorCodigo').AsInteger;
     end;

     if (MaiorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Inc(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MaiorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Inc(PCodigo,1);
     end;

     Result:=true;
end;

function TObjcliente.RegistoAnterior(PCodigo: Integer): Boolean;
Var MenorValor:Integer;
begin
     Result:=false;
     if (PCodigo=0)then
     Begin
         if (Self.PrimeiroRegistro=true) then
         Begin
              Result:=true;
              exit;
         end;
     end;

     With Self.Objquery do
     Begin
           Close;
           Sql.Clear;
           Sql.Add('Select MIN(Codigo) as MenorCodigo from Tabcliente') ;
           Open;

           MenorValor:=fieldbyname('MenorCodigo').AsInteger;
     end;

     if (MenorValor = PCodigo)then
     Begin
         Result:=false;
         exit;
     end;

     Dec(PCodigo,1);

     While (Self.LocalizaCodigo(IntToStr(PCodigo))<>true)do
     Begin
           if (MenorValor = PCodigo)then
           Begin
               Result:=false;
               exit;
           end;

           Dec(PCodigo,1);
     end;

     Result:=true;
end;


function TObjcliente.RetornaCredito(Codigo: String):String;

begin
      result:='';
      Self.Objquery:= TIBQuery.Create(nil);
      Self.Objquery.Database:= FDataModulo.IBDatabase;
      Self.Objquery.Close();
      Self.Objquery.SQL.Clear();
      Self.Objquery.SQL.Add('select valorcredito');
      Self.Objquery.SQL.Add('from tabcliente');
      Self.Objquery.SQL.add('where codigo=:2');
      Result:= Self.Objquery.Fieldbyname('ValorCredito').asstring;

end;

function TObjCliente.carregaCidades(comboBoxCidade:TComboBox): boolean;
var
  cidade,uf:string;
  query:TIBQuery;
begin

  comboBoxCidade.Items.Append ('');//para limpa labels
  try
    query:=TIBQuery.Create(nil);
    query.Database:=FDataModulo.IBDatabase;
  except

  end;

  try
        try

          with Query do
          begin

            Close;
            sql.Clear;
            SQL.Add('select codigo,nome,estado,codigocidade');
            SQL.Add('from tabcidade');
            SQL.Add('order by nome');


            try

              Open;
              first;

              while not (eof) do
              begin

                cidade := Fieldbyname('nome').AsString;
                uf     := Fieldbyname('estado').AsString;

                comboBoxCidade.Items.Append (AlinhaTexto (cidade,'|'+uf,36));

                Next;

              end;
        
              result:=True;

            except

               result:=False;

            end;


          end;

        except

          result:=False;

        end;
  finally

  end;


end;

function TObjCliente.retornaUF(str: string): string;
var
  i:Integer;
begin

  //ver para utilizar o copy

  result:='';

  i:=1;

  while ((str[i] <> '|') and (i <= Length (str))) do i:=i+1;

  i:=i+1;
  while ( i <= Length(str) ) do
  begin

    result:=result+str[i];
    i:=i+1;

  end;

end;

function TObjCliente.retornaCidade(str: string): string;
var
  i,j:Integer;
  aux:string;
begin

   result:='';

  if (str = '') then
    Exit;

  aux:='';
  
  i:=1;
  while ((str[i] <> '|') and (i <= Length (str))) do
  begin

    aux:=aux+str[i];
    i:=i+1;

  end;

  i:=Length(aux);
  result:='';

  while ( (aux[i] = ' ') and (i >= 1 ) ) do I:=I-1;


  for j := 1 to i  do
    result:=result+aux[j];


end;

function TObjCliente.retornaCodigoCidade(cidade, uf: string): string;
var
  query:TIBQuery;
begin
   try
     query:=TIBQuery.Create(nil);
     query.Database:=FDataModulo.IBDatabase;
   except

   end;

   try
        with Query do
        begin

          close;
          sql.Clear;

          sql.Add('select codigocidade');
          sql.Add('from tabcidade');
          sql.Add('where nome = '+#39+cidade+#39+'and upper(estado) = '+#39+uppercase(uf)+#39);

          //InputBox('','',SQL.Text);

          try

            Open;

            if (recordcount > 1) then
            begin

              MensagemAviso('Aten��o, existe registro em duplicidade no cadastro de cidades');
              Exit;

            end;

            result:=Fieldbyname('codigocidade').AsString;

          except

            MensagemAviso('N�o foi possivel executar a fun��o retornaCodigoCidade');
            Exit;

          end;

        end;
   finally
        FreeAndNil(query);
   end;


end;

function TObjCliente.get_codigoPais: string;
begin
  result := self.codigoPais;
end;

procedure TObjCliente.submit_codigoPais(parametro: string);
begin
  self.codigoPais:=parametro;
end;

function Tobjcliente.retornaCodigoPais(parametro: string): string;
var
  i:Integer;
begin

  Result := '';

  for i:=Pos('|',parametro)+1 to Length (parametro)  do
    Result := Result + parametro[i];

end;


function Tobjcliente.retornaPais(parametro: string): string;
begin

  Result :='';

  if (parametro = '') then
    Exit;

  with (ObjQuery)  do
  begin

    Close;
    sql.Clear;
    sql.Add ('select pais, codigopais');
    sql.Add ('from tabcodigopais');
    SQL.Add ('where codigopais = '+parametro);

    Open;
    First;

    Result := AlinhaTexto (FieldByName('pais').asString,'|'+FieldByName('codigopais').asString,34);


  end;

end;

function Tobjcliente.carregaCodigoPaises(comboBoxPais: TComboBox): boolean;
var
  pais,codigopais:string;
begin

  comboBoxPais.Items.Append ('');// para o limpa labels.

  try

    with ObjQuery do
    begin

      Close;
      sql.Clear;
      SQL.Add('select pais,codigopais');
      SQL.Add('from tabcodigopais');
      SQL.Add('order by pais');


      try

        Open;
        first;

        while not (eof) do
        begin

          pais       := Fieldbyname('pais').AsString;
          codigopais := Fieldbyname('codigopais').AsString;

          comboBoxpais.Items.Append (AlinhaTexto (pais,'|'+codigopais,34));

          Next;

        end;
        
        result:=True;

      except

         result:=False;

      end;


    end;

  except

    result:=False;

  end;

end;

end.



