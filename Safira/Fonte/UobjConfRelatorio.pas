unit UobjConfRelatorio;
interface
uses Forms,UessencialGlobal,controls;
Type
   TObjConfRelatorio=Class
          Public
                Function RecuperaArquivoRelatorio(Falvo:TForm;NomeArquivo:string):Boolean;
                Function NovoArquivoRelatorio(Falvo:TForm;NomeArquivo:string):Boolean;

          Private
                 Function GeraArquivoRelatorio(FAlvo:Tform;NomeArquivo:string):Boolean;
                 Function AbreArquivo(nomeArquivo:string):boolean;
                 Function CriaArquivo(NomeArquivo:string):Boolean;
                 Procedure FechaArquivo;
                 Function RecuperaDados(Falvo:Tform):boolean;
                 Function RecuperaTipoLinha(Linha:string):String;
                 Function RecuperaValorLinha(Linha:string):String;

   End;
type TMyControl = class(TControl);
type TMyControlClass = class of TMyControl;

Implementation
uses dialogs,QRCTRLS,sysutils,classes,graphics,quickrpt,printers;

var
Arq:Text;


{ TObjConfRelatorio }


function TObjConfRelatorio.AbreArquivo(nomeArquivo: string): boolean;
var
Temppath:String;
begin
     TempPath:='';

     TempPath:=extractfilepath(Application.ExeName);

     If TempPath[length(TempPath)]<>'\'
     Then TempPath:=TempPath+'\';

     Try
             AssignFile(arq,TempPath+'CONFRELS\'+NomeArquivo+'.CFR');
             Reset(Arq);
             Result:=True;
     Except
             Result:=False;
     End;

end;

function TObjConfRelatorio.CriaArquivo(NomeArquivo: string): Boolean;
var
Temppath:String;
begin
     TempPath:='';

     TempPath:=extractfilepath(Application.ExeName);

     If TempPath[length(TempPath)]<>'\'
     Then TempPath:=TempPath+'\';

     Try
             AssignFile(arq,TempPath+'CONFRELS\'+NomeArquivo+'.CFR');
             Rewrite(Arq);
             Result:=True;
     Except
             Result:=False;
     End;

end;

procedure TObjConfRelatorio.FechaArquivo;
begin
     CloseFile(Arq);
end;

function TObjConfRelatorio.GeraArquivoRelatorio(FAlvo: Tform;NomeArquivo:string): Boolean;
var
  int_habilita:integer;
  ClasseComponente:String;
  QualquerCoisa:TMyControlClass;
begin
     If (Self.CriaArquivo(NomeArquivo)=False)
     Then Begin
               Messagedlg('Erro na Criação do Arquivo de Configuração de Relatório!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
   //Recebo o form e vasculho por componentes de relatorios
   //mapeio cada um guardando seu nome e seus atributos
   //uma especie de dfm do delphi

Try

   for int_habilita:=0 to FAlvo.ComponentCount -1 do
   Begin
        ClasseComponente:='';
        ClasseComponente:=uppercase(Falvo.Components [int_habilita].ClassName);

        if (ClasseComponente='TQRLABEL')
        Then qualquercoisa:=TmyControlClass(TQRLabel);
        if (ClasseComponente='TQRDBTEXT')
        then qualquercoisa:=TmyControlClass(TQRDBText);
        if (ClasseComponente='TQRIMAGE')
        then qualquercoisa:=TmyControlClass(TQRImage);
        if (ClasseComponente='TQRRICHTEXT')
        then qualquercoisa:=TmyControlClass(TQRRICHTEXT);
        if (ClasseComponente='TQUICKREP')
        then qualquercoisa:=TmyControlClass(TQUICKREP);
        if (ClasseComponente='TQRMEMO')
        then qualquercoisa:=TmyControlClass(TQRMEMO);


        If ((ClasseComponente='TQRLABEL') or (ClasseComponente='TQRDBTEXT')
           or (ClasseComponente='TQRIMAGE') or (ClasseComponente='TQRRICHTEXT')
           or (ClasseComponente='TQUICKREP')or (ClasseComponente='TQRMEMO'))
        Then Begin
                  Writeln(Arq,'*/*');//Indica novo componente
                  //Tipo
                  Writeln(Arq,'TYPE='+Uppercase(Falvo.components[int_habilita].classname));
                  //Ordem de Criacao
                  Writeln(Arq,'ORDEM='+inttostr(int_habilita));
                  //Nome
                  Writeln(Arq,'COMPONENTE='+(Falvo.components[int_habilita] as Qualquercoisa).name);
                  //Left
                  Writeln(Arq,'LEFT='+inttostr((Falvo.components[int_habilita]as qualquercoisa).left));
                  //Top
                  Writeln(Arq,'TOP='+inttostr((Falvo.components[int_habilita]as Qualquercoisa).Top));
                  //Height
                  Writeln(Arq,'HEIGHT='+inttostr((Falvo.components[int_habilita]as Qualquercoisa).Height));
                  //Width
                  Writeln(Arq,'WIDTH='+inttostr((Falvo.components[int_habilita]as Qualquercoisa).Width));
                  //Enabled
                  If (Falvo.components[int_habilita]as Qualquercoisa).enabled=true
                  Then Writeln(Arq,'ENABLED=TRUE')
                  Else Writeln(Arq,'ENABLED=FALSE');

                  If (ClasseComponente='TQRIMAGE')
                  Then Begin
                          IF Tqrimage((Falvo.components[int_habilita]as Qualquercoisa)).Stretch=True
                          Then Writeln(Arq,'STRETCH=TRUE')
                          Else Writeln(Arq,'STRETCH=FALSE');
                     End;

                //propriedas comuns a TQRLABEL e TQRDBTEXT sem o TQRImage
                If (ClasseComponente='TQRLABEL') or (ClasseComponente='TQRDBTEXT')
                or (ClasseComponente='TQRRICHTEXT') or (ClasseComponente='TQRMEMO') 
                then Begin
                          If (ClasseComponente='TQRRICHTEXT')//Color
                          Then Writeln(Arq,'COLOR='+Inttostr((Falvo.components[int_habilita] as qualquercoisa).color));
                          //Font Name
                          Writeln(Arq,'FONTNAME='+(Falvo.components[int_habilita]as Qualquercoisa).font.name);
                          //FontSize
                          Writeln(Arq,'FONTSIZE='+inttostr(TQrlabel(Falvo.components[int_habilita]).font.size));
                     End;
                If (classeComponente='TQRLABEL')//caption
                Then Writeln(Arq,'CAPTION='+TQRLABEL(Falvo.components[int_habilita]).caption);

                If (ClasseComponente='TQUICKREP')
                Then Begin
                          Writeln(Arq,'PAGE.BOTTOMMARGIN='+Floattostr(TQUICKREP(Falvo.components[int_habilita]).page.bottommargin));
                          Writeln(Arq,'PAGE.COLUMNS='+INTTOSTR(TQUICKREP(Falvo.components[int_habilita]).page.columns));
                          Writeln(Arq,'PAGE.COLUMNSPACE='+FLOATTOSTR(TQUICKREP(Falvo.components[int_habilita]).page.COLUMNSPACE));
                          Writeln(Arq,'PAGE.LEFTMARGIN='+FLOATTOSTR(TQUICKREP(Falvo.components[int_habilita]).page.LEFTMARGIN));
                          Writeln(Arq,'PAGE.LENGTH='+FLOATTOSTR(TQUICKREP(Falvo.components[int_habilita]).page.Length));
                          If TQUICKREP(Falvo.components[int_habilita]).page.ORIENTATION=poPortrait
                          Then Writeln(Arq,'PAGE.ORIENTATION=POPORTRAIT')
                          Else Writeln(Arq,'PAGE.ORIENTATION=POLANDSCAPE');
                          Writeln(Arq,'PAGE.RIGHTMARGIN='+FLOATTOSTR(TQUICKREP(Falvo.components[int_habilita]).page.RightMargin));
                          Writeln(Arq,'PAGE.TOPMARGIN='+FLOATTOSTR(TQUICKREP(Falvo.components[int_habilita]).page.TopMargin));
                          Writeln(Arq,'PAGE.WIDTH='+FLOATTOSTR(TQUICKREP(Falvo.components[int_habilita]).page.Width));
                     End;


                //Fim de Componente
                Writeln(Arq,'*@*');
        End;

   End;

Finally
   Self.FechaArquivo;
End;
   result:=True;


End;

function TObjConfRelatorio.RecuperaArquivoRelatorio(Falvo:Tform;NomeArquivo:string): Boolean;
var
Linha:String;
begin

     If (AbreArquivo(NomeArquivo)=False)
     Then Begin
               If (Messagedlg('Erro na Abertura do Arquivo de Configuração! Deseja Criar um Arquivo com as Definições Originais?',mtconfirmation,[mbyes,mbno],0)=Mrno)
               Then Begin
                        result:=False;
                        exit;
                    End;
               If (Self.GeraArquivoRelatorio(Falvo,NomeArquivo)=False)
               Then Begin
                        Messagedlg('Erro na Tentativa de Criar Arquivo de Configuração de Relatório!',mterror,[mbok],0);
                        result:=False;
                        exit;
                    End;
               result:=True;
               exit;
           End;

Try

     While not eof(arq) do
     Begin
          linha:='';
          Readln(Arq,linha);
          If linha='*/*'
          Then Begin
                    //Novo Componente
                    If Self.RecuperaDados(FAlvo)=False
                    Then exit;
               End;

     End;
Finally
       Self.FechaArquivo;
End;

end;

function TObjConfRelatorio.NovoArquivoRelatorio(Falvo: TForm;
  NomeArquivo: string): Boolean;
begin
     self.GeraArquivoRelatorio(falvo,nomearquivo);
end;

Function TObjConfRelatorio.RecuperaDados(Falvo: Tform):boolean;
var
Linha,Tipo,Valor,NomeComponente,ClasseComponente:string;
Ordem:Integer;
QualquerCoisa:TMyControlClass;

begin
     linha:='';

     //Pegando o Tipo do Componente(classe)
     readln(arq,linha);
     Tipo:=Self.RecuperaTipoLinha(Linha);
     Valor:=Self.RecuperaValorLinha(Linha);
     If tipo<>'TYPE'
     Then Begin
               Messagedlg('Erro de Configuração no Arquivo Texto!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
     ClasseComponente:='';
     ClasseComponente:=Valor;

     //Pegando a ordem de criacao no formulario
     readln(arq,linha);
     Tipo:=Self.RecuperaTipoLinha(Linha);
     Valor:=Self.RecuperaValorLinha(Linha);
     if Tipo<>'ORDEM'
     Then Begin
               Messagedlg('Erro de Configuração no Arquivo Texto!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
     ordem:=strtoint(valor);


     //Recuperando o Nome do Componente
     readln(arq,linha);
     Tipo:=Self.RecuperaTipoLinha(Linha);
     Valor:=Self.RecuperaValorLinha(Linha);
     if Tipo<>'COMPONENTE'
     Then Begin
               Messagedlg('Erro de Configuração no Arquivo Texto!',mterror,[mbok],0);
               result:=False;
               exit;
          End;
     NomeComponente:=valor;

     //********************************************************
     //Definicao da Classe do Componente a ser configurado
     If ClasseComponente='TQRLABEL'
     Then QualquerCoisa:=TMyControlClass(Tqrlabel);
     if (ClasseComponente='TQRDBTEXT')
     then qualquercoisa:=TmyControlClass(TQRDBText);
     if (ClasseComponente='TQRIMAGE')
     then qualquercoisa:=TmyControlClass(TQRImage);
     if (ClasseComponente='TQRRICHTEXT')
     then qualquercoisa:=TmyControlClass(TQRRICHTEXT);
     if (ClasseComponente='TQUICKREP')
     then qualquercoisa:=TmyControlClass(TQUICKREP);
     if (ClasseComponente='TQRMEMO')
     then qualquercoisa:=TmyControlClass(TQRMEMO);

     //**************************************************
     //Antes de qualquer coisa verifico se o componente existe
     Try
        IF(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).Enabled
        Then ;
     Except
         Messagedlg('O Componente '+NomeComponente+' não foi encontrado neste relatório, verifique a existência do mesmo e remova-o',mterror,[mbok],0);
         result:=True;
         exit;
     End;


     While (linha<>'*@*') do//ate que aparece o fim da definicao do componente
     Begin
          Try
                  readln(arq,linha);
                  Tipo:=Self.RecuperaTipoLinha(Linha);
                  Valor:=Self.RecuperaValorLinha(Linha);

                  //******************************************************
                  //Verificando as propriedades

                  If Tipo='LEFT'
                  Then (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).left:=Strtoint(Valor);

                  //Then  (Falvo.components[ordem] as Qualquercoisa).left:=Strtoint(Valor);

                  If Tipo='TOP'
                  Then  (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).top:=Strtoint(Valor);

                  If Tipo='HEIGHT'
                  Then  (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).Height:=Strtoint(Valor);

                  If Tipo='WIDTH'
                  Then  (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).Width:=Strtoint(Valor);

                  If Tipo='COLOR'
                  Then  (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).color:=Strtoint(Valor);

                  If Tipo='FONTNAME'
                  Then  (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).font.name:=Valor;

                  If Tipo='FONTSIZE'
                  Then  (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).font.size:=strtoint(Valor);

                  If Tipo='STRETCH'//especifico do TqrImage
                  Then  BEgin
                            If Valor='TRUE'
                            Then TQRIMAGE((FAlvo.FindComponent(NomeComponente) as QualquerCoisa)).stretch:=true
                            Else TQRIMAGE((FAlvo.FindComponent(NomeComponente) as QualquerCoisa)).stretch:=False;
                        End;

                  If Tipo='TEXT'
                  Then (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).text:=valor;

                  If Tipo='CAPTION'
                  Then  TQRLABEL(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).caption:=valor;

                  If Tipo='ENABLED'
                  Then  BEgin
                            If Valor='TRUE'
                            Then (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).enabled:=true
                            Else (FAlvo.FindComponent(NomeComponente) as QualquerCoisa).enabled:=False;
                        End;

                  If Tipo='PAGE.BOTTOMMARGIN'//especifico do TquickRep                  aa
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.bottommargin:=strtofloat(valor);

                  If Tipo='PAGE.COLUMNS'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.Columns:=strtoint(valor);

                  If Tipo='PAGE.COLUMNSPACE'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.ColumnSpace:=strtofloat(valor);

                  If Tipo='PAGE.LEFTMARGIN'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.LeftMargin:=strtofloat(valor);

                  If Tipo='PAGE.LENGTH'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.Length:=strtofloat(valor);

                  If Tipo='PAGE.LEFTMARGIN'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.LeftMargin:=strtofloat(valor);

                  If Tipo='PAGE.ORIENTATION'//especifico do TquickRep
                  Then  Begin
                             If valor='POPORTRAIT'
                             Then TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.Orientation:=poPortrait
                             Else TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.Orientation:=poLandscape;
                        End;
                  If Tipo='PAGE.RIGHTMARGIN'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.RightMargin:=strtofloat(valor);

                  If Tipo='PAGE.TOPMARGIN'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.TopMargin:=strtofloat(valor);

                  If Tipo='PAGE.WIDTH'//especifico do TquickRep
                  Then  TQUICKREP(FAlvo.FindComponent(NomeComponente) as QualquerCoisa).page.Width:=strtofloat(valor);
          Except
                Messagedlg('Erro na Tentativa de Configuração do Relatório! Componente='+ClasseComponente+' | Nome='+nomecomponente+' | Tipo='+Tipo+' |   Valor='+Valor,mterror,[mbok],0);
                exit;
          End;
          //*************************************************

     End;
result:=True;

end;

function TObjConfRelatorio.RecuperaTipoLinha(Linha: string): String;
var
cont:integer;
tipo:string;
begin
     tipo:='';
     for cont:=1 to length(linha) do
     Begin
          if (linha[cont]<>'=')
          Then tipo:=tipo+linha[cont]
          else break;
     End;
     result:=Uppercase(Tipo);
End;


function TObjConfRelatorio.RecuperaValorLinha(Linha: string): String;
var
cont:integer;
Valor:string;
comeca:boolean;
begin
     Valor:='';
     comeca:=False;

     for cont:=1 to length(linha) do
     Begin

          if (linha[cont]='=')
          Then comeca:=true
          else Begin
                If comeca=true
                Then Valor:=Valor+linha[cont];
               End;
         
     End;
     result:=Valor;
end;

end.


drop table TabCarga;

