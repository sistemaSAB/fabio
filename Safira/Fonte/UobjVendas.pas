unit UobjVendas;

interface

Uses forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal, UDataModulo,
Ufiltraimp,Upesquisa,SysUtils,Dialogs,Controls, IBCustomDataSet;

type
      Tobjvendas = class
            public
               state                                      :TDataSetState;
               SqlInicial                                  :String[200];

               constructor create;
               destructor free;
               Function    Salvar(ComCommit:Boolean)       :Boolean;
               Function    LocalizaCodigo(Parametro:string) :boolean;
               Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
               Function   TabelaparaObjeto:Boolean;
               Procedure   ZerarTabela;
               Procedure   Commit;
               procedure   Cancelar;
               Function  Get_NovoCodigo:string;
               Procedure Submit_Codigo(parametro: string);
               Function Get_Codigo: string;
               procedure Submit_Pedido(parametro:string);
               function Get_Pedido:string;

            Private
               query:Tibquery;
               ObjData:TIBDataSet;

               Codigo:string;
               Pedido:string;

               Procedure ObjetoparaTabela;

      end;




implementation

constructor Tobjvendas.create;
begin

      ObjData:=TIBDataSet.Create(nil);
      ObjData.Database:=FdataModulo.IBDatabase;
      query:=TIBQuery.Create(nil);
      Query.Database := FdataModulo.IBDatabase;

      with ObjData do
      begin
          SelectSQL.Clear;
          SelectSQL.Add('select codigo,pedido from tabvendas where codigo =0');

          InsertSQL.Clear;
          InsertSQL.Add('insert into tabvendas(codigo,pedido)');
          InsertSQL.Add('values(:codigo,:pedido)');

          ModifySQL.Clear;
          ModifySQL.Add('update tabvendas set codigo=:codigo,pedido=:pedido where codigo = 0') ;


          DeleteSQL.Clear;
          DeleteSQL.Add('delete from tabvendas where codigo= 0');

          RefreshSQL.Clear;
          RefreshSQL.Add('select codigo,pedido from tabvendas where codigo is null');

          Open;
          Self.state:= dsInactive;
      end;

end;

destructor Tobjvendas.free;
begin
        Freeandnil(Self.ObjData);
        Freeandnil(Self.Query);
end;

procedure Tobjvendas.ZerarTabela;
begin
    Self.Codigo:='';
    self.Pedido:='';
end;

procedure Tobjvendas.Commit;
begin
     FdataModulo.IBTransaction.CommitRetaining;
end;


procedure Tobjvendas.Cancelar;
begin
    FDataModulo.IBTransaction.RollbackRetaining;
end;

procedure Tobjvendas.Submit_Codigo(parametro:string);
begin
    self.Codigo:=parametro;
end;


procedure Tobjvendas.Submit_Pedido(parametro:string);
begin
    Self.Pedido:=parametro;
end;

procedure Tobjvendas.ObjetoparaTabela;
begin
   with ObjData do
   begin
       try
         FieldByName('codigo').asstring:=self.Codigo;
         FieldByName('pedido').AsString:=self.Pedido;
       except

       end;
   end;
end;

function Tobjvendas.Salvar(ComCommit : Boolean):Boolean;
begin
       if(LocalizaCodigo(codigo)=false)
       then
       begin
              if(State=dsEdit)
              then begin
                   //avisa que o registro n�o foi encotrado pra edi��o
                   result:=false;
                   exit;
              end;
       end
       else
       begin
              if(State=dsInsert)
              then begin
                   //avisa q registro esta duplicado
                   result:=false;
                   exit;

              end;
       end;

        if(State=dsInsert)
        then
        begin
             ObjData.Insert;
        end
        else
        begin
            if(State=dsedit)
            then begin
                ObjData.Edit;
            end
            else
            begin
                //n�o esta setado pra edi��o e nein pra inser��o
            end;
        end;
        ObjetoParaTabela;
        ObjData.Post;
        if(ComCommit =True)
        Then FdataModulo.IBTransaction.CommitRetaining;

        State:=dsInactive;
        result:=true;
end;

function Tobjvendas.LocalizaCodigo(parametro:string):Boolean;
begin
    with ObjData do
    begin
         Close;
         SelectSQL.Clear;
         SelectSQL.Add('select * from tabvendas where codigo='+parametro) ;
         Open;
         If (recordcount>0)
         Then Result:=True
         Else Result:=False;

    end;

end;

function Tobjvendas.Exclui(Pcodigo:string;ComCommit:boolean):Boolean;
begin
     with self do
     begin
        try
            result:=true;
            if(LocalizaCodigo(Pcodigo)=true)
            then begin
                  ObjData.Delete;
                   if(ComCommit=True)
                    then FdataModulo.IBTransaction.CommitRetaining;
            end
            else result:=false;
        except
            result:=false;
        end;
     end;

end;

function Tobjvendas.TabelaparaObjeto:Boolean;
begin
    with Self do
    begin
        with ObjData do
        begin
              codigo := fieldbyname('codigo').asstring;
              Pedido := fieldbyname('pedido').asstring;
        end;

    end;
end;

function Tobjvendas.Get_NovoCodigo:string;
var
  codigo:Integer;
begin
     with Self.query do
     begin
           Close;
           SQL.Clear;
           SQL.Add('select codigo from tabvendas');
           SQL.Add('order by codigo') ;

           Open;
           Last;
           Codigo:=fieldbyname('codigo').AsInteger;
           Codigo:=codigo+1;
           Result:=intToStr(codigo);
      end;
end;

function Tobjvendas.Get_Codigo:string;
begin
    Result:=Self.Codigo
end;

function Tobjvendas.Get_Pedido:string;
begin
    Result:=self.Pedido;
end;



end.
