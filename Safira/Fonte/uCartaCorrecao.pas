unit uCartaCorrecao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, Buttons, StdCtrls, ComCtrls,UobjCARTACORRECAO,DB,
  Mask, StdActns, ActnList, XPStyleActnCtrls, ActnMan, ImgList,
  ToolWin, OleCtrls, SHDocVw,ShellAPI, Menus;

type
  TfCartaCorrecao = class(TForm)
    StatusBar1: TStatusBar;
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    Btnovo: TBitBtn;
    btalterar: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btexcluir: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btOpcoes: TBitBtn;
    BtSair: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Image1: TImage;
    Label7: TLabel;
    Image3: TImage;
    Label9: TLabel;
    edtChavAcess: TEdit;
    edtLoteEnvio: TEdit;
    edtUFRecepcao: TEdit;
    panelTransmitida: TPanel;
    Image2: TImage;
    Label6: TLabel;
    panelNaoTransmitida: TPanel;
    Label5: TLabel;
    imgVazio: TImage;
    MemoCorrecao: TMemo;
    panelProgress: TPanel;
    Label8: TLabel;
    edtCNPJCPF: TEdit;
    Panel4: TPanel;
    Panel2: TPanel;
    TreeView1: TTreeView;
    Splitter1: TSplitter;
    Panel1: TPanel;
    MemoResp: TMemo;
    ImageList1: TImageList;
    ActionManager1: TActionManager;
    SearchFind1: TSearchFind;
    SearchFindNext1: TSearchFindNext;
    SearchReplace1: TSearchReplace;
    SearchFindFirst1: TSearchFindFirst;
    FileSaveAs1: TFileSaveAs;
    WBResposta: TWebBrowser;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    TabSheet4: TTabSheet;
    ToolBar2: TToolBar;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    memoRespWS: TMemo;
    Panel5: TPanel;
    TabSheet5: TTabSheet;
    Label10: TLabel;
    edtSequencialEvento: TEdit;
    Label11: TLabel;
    lbCCRestante: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    edtHoraTransmissao: TMaskEdit;
    edtDataTransmissao: TMaskEdit;
    Label14: TLabel;
    edtArquivoXML: TEdit;
    Image4: TImage;
    lbObsCorrecao: TLabel;
    painelFiltroTree: TPanel;
    lbDTINITree: TLabel;
    dataIniTree: TMaskEdit;
    lbDTFINTree: TLabel;
    dataFimTree: TMaskEdit;
    btCorrecaoErros: TSpeedButton;
    PopupMenu1: TPopupMenu;
    Criarcarta1: TMenuItem;
    Pesquisaritem1: TMenuItem;
    N1: TMenuItem;
    OpenDialog1: TOpenDialog;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure edtChavAcessExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure Image3Click(Sender: TObject);
    procedure Label9MouseLeave(Sender: TObject);
    procedure Label9MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Label9Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Image4Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure MemoCorrecaoChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure btCorrecaoErrosClick(Sender: TObject);
    procedure dataFimTreeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dataIniTreeEnter(Sender: TObject);
    procedure dataIniTreeExit(Sender: TObject);
    procedure dataFimTreeEnter(Sender: TObject);
    procedure dataFimTreeExit(Sender: TObject);
    procedure dataIniTreeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Criarcarta1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Pesquisaritem1Click(Sender: TObject);
    procedure TreeView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    objCC : TObjCARTACORRECAO;
    noLastFound : TTreeNode;
    strProcura :string;
    caminho_nfe_origem:string;
    function controlesParaObjeto:Boolean;
    function objetoParaControles:Boolean;
    function tabelaParaControles:Boolean;
    function consultaChave(mostraLabel:Boolean = true) : Boolean;
    function transmitir():Boolean;
    function criarNovaCarta(info : string):Boolean;
    function procurarItem(procura:string):Boolean;
  public
    { Public declarations }
  end;

var
  fCartaCorrecao: TfCartaCorrecao;

implementation

uses UescolheImagemBotao, UessencialGlobal, Upesquisa,
  UDataModulo, Uprincipal;

{$R *.dfm}

procedure TfCartaCorrecao.FormShow(Sender: TObject);
begin

  PageControl1.TabIndex := 0;
  painelFiltroTree.Align := alNone;
  
  desabilita_campos(self);
  
  objCC := TObjCARTACORRECAO.Create;

  FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
  panelTransmitida.Visible := False;
  panelNaoTransmitida.Visible := False;
  lbCCRestante.Caption := '';

  objCC.MemoResp   := self.MemoResp;
  objCC.memoRespWS := self.memoRespWS;
  objCC.WBResposta := self.WBResposta;

  SetWindowLong(TreeView1.Handle, GWL_STYLE,
  GetWindowLong(TreeView1.Handle,GWL_STYLE) or $80);

end;

procedure TfCartaCorrecao.FormClose(Sender: TObject;var Action: TCloseAction);
begin
  if Assigned(objCC) then
    objCC.Free;
end;

procedure TfCartaCorrecao.BtnovoClick(Sender: TObject);
begin

  limpaedit(Self);
  habilita_campos(Self);
  esconde_botoes(Self);


  Btgravar.Visible:=True;
  BtCancelar.Visible:=True;
  btpesquisar.Visible:=True;

  objCC.status:=dsInsert;
  edtLoteEnvio.Text := '1';
  edtChavAcess.setfocus;
  StatusBar1.Panels[1].Text := '';
  StatusBar1.Color := clBtnFace;
  objCC.zerarTabela;
  lbCodigo.Caption := '0';

  edtSequencialEvento.Enabled := False;

end;

procedure TfCartaCorrecao.btalterarClick(Sender: TObject);
begin

  if panelTransmitida.Visible then
  begin
    MensagemAviso('N�o � possivel alterar carta transmitida');
    Exit;
  end;

  if lbCodigo.Caption = '0' then Exit;
  if lbCodigo.Caption = '' then Exit;
  if objCC.Status <> dsinactive then Exit;

  habilita_campos(Self);
  objCC.Status:=dsEdit;
  edtChavAcess.setfocus;
  esconde_botoes(Self);
  Btgravar.Visible:=True;
  BtCancelar.Visible:=True;
  btpesquisar.Visible:=True;

  edtSequencialEvento.Enabled := False;
  
end;

procedure TfCartaCorrecao.btgravarClick(Sender: TObject);
begin

  If objCC.Status=dsInactive Then
    exit;

  If not ControlesParaObjeto Then
  Begin
    Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
    exit;
  End;

  If not (objCC.salvar(true)) Then
    exit;

  objCC.LocalizaCodigo(objCC.get_codigo ());
  objCC.TabelaparaObjeto;
  TabelaParaControles;

  mostra_botoes(Self);
  desabilita_campos(Self);

  StatusBar1.Panels[1].Text := '';
  StatusBar1.Color := clBtnFace;

end;

procedure TfCartaCorrecao.btcancelarClick(Sender: TObject);
begin

  objCC.cancelar;
  limpaedit(Self);
  desabilita_campos(Self);
  mostra_botoes(Self);

  panelTransmitida.Visible := False;
  panelNaoTransmitida.Visible := False;
  lbCCRestante.Caption := '';
  StatusBar1.Panels[1].Text := '';
  StatusBar1.Color := clBtnFace;

end;

procedure TfCartaCorrecao.btexcluirClick(Sender: TObject);
begin

  If (objCC.status<>dsinactive) or (lbCodigo.Caption='') Then exit;
  if objCC.Get_ENVIADA = 'S' then Exit;

  If not objCC.LocalizaCodigo(lbCodigo.Caption) Then
  Begin
    Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
    exit;
  End;

  If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno) Then
  exit;

  If not objCC.exclui(lbCodigo.Caption,true) Then
  Begin
    Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
    exit;
  End;

  limpaedit(Self);
  self.StatusBar1.Panels[1].Text := '';
  Self.StatusBar1.Color := clBtnFace;
  lbCCRestante.Caption := '';

end;

procedure TfCartaCorrecao.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

  Try
    Fpesquisalocal:=Tfpesquisa.create(Self);

    If (FpesquisaLocal.PreparaPesquisa(objCC.Get_pesquisa,objCC.Get_TituloPesquisa,Nil)) Then
    Begin

      Try
        If (FpesquisaLocal.showmodal=mrok) Then
        Begin

          If objCC.status<>dsinactive then
            exit;

          If not objCC.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring) Then
          Begin
            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
            exit;
          End;

          objCC.ZERARTABELA;

          If not (TabelaParaControles) Then
          Begin
            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
            limpaedit(Self);
            exit;
          End;

        End;

      Finally
        FpesquisaLocal.QueryPesq.close;
      End;

    End;

  Finally
    FreeandNil(FPesquisaLocal);
  End;

end;

procedure TfCartaCorrecao.BtSairClick(Sender: TObject);
begin
  self.Close;
end;

function TfCartaCorrecao.controlesParaObjeto: Boolean;
begin

  result := False;

  Try

    With objCC do
    Begin

      Submit_CODIGO(lbCodigo.Caption);
      Submit_CHAVEACESSO(edtChavAcess.Text);
      Submit_CNPJCPF(edtCNPJCPF.Text);
      Submit_LOTEENVIO(edtLoteEnvio.Text);
      Submit_UFRECEPCAO(edtUFRecepcao.Text);
      Submit_CORRECAO(MemoCorrecao.Text);
      submit_caminho_nfe_origem(caminho_nfe_origem);

    End;

  Except
    Exit;
  End;

  result := True;

end;

function TfCartaCorrecao.tabelaParaControles: Boolean;
begin

  If not (objCC.TabelaparaObjeto) Then
  Begin
    result:=False;
    exit;
  End;

  If not (ObjetoParaControles) Then
  Begin
    result:=False;
    exit;
  End;

  Result:=True;

end;

function TfCartaCorrecao.objetoParaControles: Boolean;
begin

  Try

    lbCodigo.Caption := objCC.Get_CODIGO;
    edtChavAcess.Text := objCC.Get_CHAVEACESSO;
    edtCNPJCPF.Text := objCC.Get_CNPJCPF;
    edtLoteEnvio.Text := objCC.Get_LOTEENVIO;
    edtUFRecepcao.Text := objCC.Get_UFRECEPCAO;
    MemoCorrecao.Text := objCC.Get_CORRECAO;
    edtSequencialEvento.Text := IntToStr(objCC.getnSeqEventoCampo);
    edtArquivoXML.Text := objCC.get_caminhoXML;
    edtDataTransmissao.Text := objCC.get_dataTransmissao;
    edtHoraTransmissao.Text := objCC.get_horatransmissao;
    caminho_nfe_origem := objCC.get_caminho_nfe_origem;

    if objCC.Get_ENVIADA = 'S' then
      panelTransmitida.Visible := true
    else
      panelTransmitida.Visible := False;

    panelNaoTransmitida.Visible := not (panelTransmitida.Visible);

    if FileExists(objCC.get_caminhoXML) then
      WBResposta.Navigate(objCC.get_caminhoXML);

    lbCCRestante.Caption := IntToStr(20 - objCC.getnSeqEventoCampo);

    result:=true;


  Except
    result:=False;
  End;

end;

procedure TfCartaCorrecao.Image1Click(Sender: TObject);
begin

  if not edtChavAcess.Enabled then
    Exit;

  if OpenDialog1.Execute then
  begin
    caminho_nfe_origem := OpenDialog1.FileName;
    edtChavAcess.SetFocus;
    edtChavAcess.Text := RetornaValorCampos(caminho_nfe_origem,'chNFe');
  end;

end;

function TfCartaCorrecao.consultaChave(mostraLabel:Boolean) : Boolean;
begin

  result := False;

  if Length(edtChavAcess.Text) = 44 then
  begin

    panelProgress.Visible := mostraLabel;
    Application.ProcessMessages;

    try

      if objcc.consultaChave(edtChavAcess.Text,edtUFRecepcao.Text) <> 100 then
      begin
        MensagemAviso('Chave de acesso n�o esta autorizada');
        StatusBar1.Panels[1].Text := 'Chave de acesso n�o esta autorizada';
        StatusBar1.Color := clRed;
        edtChavAcess.SetFocus;
        Exit;
      end;

    finally
      panelProgress.Visible := False;
    end;

  end;

  StatusBar1.Panels[1].Text := 'Chave de acesso autorizada';
  StatusBar1.Color := clMoneyGreen;
  result := True;

end;

procedure TfCartaCorrecao.edtChavAcessExit(Sender: TObject);
begin

  if Trim(edtChavAcess.Text) = '' then Exit;
  if not IsNumeric(edtChavAcess.Text) then Exit;

  consultaChave;
  edtUFRecepcao.Text := objCC.retornaUF(edtChavAcess.Text);
  edtCNPJCPF.Text := objCC.retornaCNPJ(edtChavAcess.Text);

  MemoCorrecao.SetFocus;

end;

function TfCartaCorrecao.transmitir: Boolean;
var
  msgRej : string;
begin

  result := false;

  if objCC.Status <> dsinactive then Exit;
  if (lbCodigo.Caption = '') or (lbCodigo.Caption = '0') then Exit;
  if (objcc.Get_ENVIADA = 'S') then Exit;

  StatusBar1.Panels[1].Text := '';
  StatusBar1.Color := clBtnFace;
  StatusBar1.Font.Style := [];

  if objCC.transmitir( lbCodigo.Caption, msgRej,caminho_nfe_origem ) then
  begin
    FDataModulo.IBTransaction.CommitRetaining;
    objCC.LocalizaCodigo(lbCodigo.Caption);
    self.tabelaParaControles;
    StatusBar1.Panels[1].Text := 'Transmitida com sucesso';
    StatusBar1.Color := clMoneyGreen;
  end
  else
  begin
    FDataModulo.IBTransaction.RollbackRetaining;
    StatusBar1.Color := clRed;
    StatusBar1.Panels[1].Text := msgRej;
  end;

  result := true;

end;

procedure TfCartaCorrecao.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if (key = #13) then
    Perform(Wm_NextDlgCtl,0,0);
end;

procedure TfCartaCorrecao.Image3Click(Sender: TObject);
begin
  self.transmitir;
end;

procedure TfCartaCorrecao.Label9MouseLeave(Sender: TObject);
begin
  (sender as TLabel).Font.Style:=[];
end;

procedure TfCartaCorrecao.Label9MouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  (sender as TLabel).Font.Style:=[fsBold,fsUnderline];
end;

procedure TfCartaCorrecao.Label9Click(Sender: TObject);
begin
  try
    Screen.Cursor := crHourGlass;
    self.transmitir;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfCartaCorrecao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if (Key = vk_f1) then
    Fprincipal.chamaPesquisaMenu();

end;

procedure TfCartaCorrecao.Image4Click(Sender: TObject);
begin
  if not (FileExists(edtArquivoXML.Text)) and Not (DirectoryExists(edtArquivoXML.Text)) then
    MensagemAviso('O arquivo ou diret�rio n�o existe')
  else
    ShellExecute(Application.Handle,PChar('open'),PChar('explorer.exe'),PChar(edtArquivoXML.Text),nil,SW_SHOWMAXIMIZED);
end;

procedure TfCartaCorrecao.PageControl1Change(Sender: TObject);
begin

  case PageControl1.TabIndex of

    0:;
    1:if (lbCodigo.Caption = '0') or (objCC.Get_ENVIADA = 'N') then PageControl1.TabIndex := 0;

  end

end;

procedure TfCartaCorrecao.MemoCorrecaoChange(Sender: TObject);
begin
  lbObsCorrecao.Visible := (Length(MemoCorrecao.Lines.Text) < 15);
end;

procedure TfCartaCorrecao.FormActivate(Sender: TObject);
var
  dataIni,dataFin : string;
begin
  if (dataIni = '') or (dataFin = '') then
    primeiroUltimoDia(FormatDateTime('mm',Date),FormatDateTime('yyyy',date),dataIni,dataFin);

  dataIniTree.Text := dataIni;
  dataFimTree.Text := dataFin;
  
  objCC.criarTreeView(TreeView1);
end;

procedure TfCartaCorrecao.btCorrecaoErrosClick(Sender: TObject);
begin

  if painelFiltroTree.Align = alNone then
  begin
    painelFiltroTree.Align := alTop;
    dataIniTree.Enabled := True;
    dataFimTree.Enabled := True;
    dataIniTree.SetFocus;
  end
  else
    painelFiltroTree.Align := alNone;

end;

procedure TfCartaCorrecao.dataFimTreeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  if Key = vk_return then
  begin

    if (RetornaSoNumeros(dataIniTree.Text) = '') or (RetornaSoNumeros(dataFimTree.Text) = '') then
      Exit;

    painelFiltroTree.Align := alNone;
    objCC.criarTreeView(TreeView1,dataIniTree.Text,dataFimTree.Text);

  end
  else
  if Key = vk_escape then
  begin

    dataIniTree.Text := '';
    dataFimTree.Text := '';
    painelFiltroTree.Align := alNone;

  end
  else if (Key = vk_space) then
    dataFimTree.Text := DateToStr(date)

end;

procedure TfCartaCorrecao.dataIniTreeEnter(Sender: TObject);
begin
  lbDTINITree.font.Style := [fsbold];
end;

procedure TfCartaCorrecao.dataIniTreeExit(Sender: TObject);
begin
  lbDTINITree.font.Style := [];
end;

procedure TfCartaCorrecao.dataFimTreeEnter(Sender: TObject);
begin
  lbDTFINTree.font.Style := [fsbold];
end;

procedure TfCartaCorrecao.dataFimTreeExit(Sender: TObject);
begin
  lbDTFINTree.font.Style := [];
end;

procedure TfCartaCorrecao.dataIniTreeKeyDown(Sender: TObject;var Key: Word; Shift: TShiftState);
begin

  if Key = vk_escape then
  begin
    dataIniTree.Text := '';
    dataFimTree.Text := '';
    painelFiltroTree.Align := alNone;
  end
  else if (Key = vk_space) then
    dataIniTree.Text := DateToStr(date);

end;

procedure TfCartaCorrecao.Criarcarta1Click(Sender: TObject);
begin

  if TreeView1.Selected <> nil then
    self.criarNovaCarta(TreeView1.Selected.Text);

end;

function TfCartaCorrecao.criarNovaCarta(info : string): Boolean;
var
  chAcess:string;
begin

  result := False;

  if info = '' then
    Exit;

  chAcess :=  copyStrFront('-',info);

  if chAcess = '' then
    Exit;

  caminho_nfe_origem := get_campoTabela('arquivo_xml','chave_acesso','tabnfe',chAcess);

  BtnovoClick(Btnovo);
  edtChavAcess.Text := chAcess;
  edtChavAcessExit(edtChavAcess);
  result := True;

end;

procedure TfCartaCorrecao.PopupMenu1Popup(Sender: TObject);
begin
  Criarcarta1.Enabled    := (Pos('-',TreeView1.Selected.Text) <> 0);
  Pesquisaritem1.Enabled := Criarcarta1.Enabled;
end;

procedure TfCartaCorrecao.Pesquisaritem1Click(Sender: TObject);
begin

  strProcura := InputBox('Pesquisar item','Procurar: ','');
  if not self.procurarItem(strProcura) then
  begin
    MensagemAviso('Item n�o encontrado');
    strProcura := '';
  end;

end;

function TfCartaCorrecao.procurarItem(procura: string): Boolean;
begin
  noLastFound := objCC.procuraItem(TreeView1,procura);
  result := (noLastFound <> nil);
end;

procedure TfCartaCorrecao.TreeView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key = vk_return then
  begin
    if (noLastFound <> nil) and (strProcura <> '') then
      noLastFound := objCC.procuraItem(TreeView1,strProcura,noLastFound.GetNext);

    if noLastFound <> nil then
      TreeView1.SetFocus
    else
      MensagemAviso('N�o h� mais itens');
  end;

end;

end.
