unit UUNIDADEMEDIDA;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjUNIDADEMEDIDA,
  jpeg;

type
  TFUNIDADEMEDIDA = class(TForm)
    panelbotes: TPanel;
    lbnomeformulario: TLabel;
    Btnovo: TBitBtn;
    btpesquisar: TBitBtn;
    btrelatorios: TBitBtn;
    btalterar: TBitBtn;
    btexcluir: TBitBtn;
    btgravar: TBitBtn;
    btcancelar: TBitBtn;
    btsair: TBitBtn;
    btopcoes: TBitBtn;
    LbCodigo: TLabel;
    EdtCodigo: TEdit;
    LbSIGLA: TLabel;
    EdtSIGLA: TEdit;
    LbDESCRICAO: TLabel;
    EdtDESCRICAO: TEdit;
    ImagemFundo: TImage;
    panelrodape: TPanel;
    ImagemRodape: TImage;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         ObjUNIDADEMEDIDA:TObjUNIDADEMEDIDA;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FUNIDADEMEDIDA: TFUNIDADEMEDIDA;


implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFUNIDADEMEDIDA.ControlesParaObjeto: Boolean;
Begin
  Try
    With Self.ObjUNIDADEMEDIDA do
    Begin
        Submit_Codigo(edtCodigo.text);
        Submit_SIGLA(edtSIGLA.text);
        Submit_DESCRICAO(edtDESCRICAO.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFUNIDADEMEDIDA.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjUNIDADEMEDIDA do
     Begin
        EdtCodigo.text:=Get_Codigo;
        EdtSIGLA.text:=Get_SIGLA;
        EdtDESCRICAO.text:=Get_DESCRICAO;
//CODIFICA GETS

      
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFUNIDADEMEDIDA.TabelaParaControles: Boolean;
begin
     If (Self.ObjUNIDADEMEDIDA.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
     End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
     End;
     Result:=True;
end;



//****************************************
procedure TFUNIDADEMEDIDA.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjUNIDADEMEDIDA=Nil)
     Then exit;

     If (Self.ObjUNIDADEMEDIDA.status<>dsinactive)
     Then Begin
               Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
               abort;
               exit;
     End;

     Self.ObjUNIDADEMEDIDA.free;
end;

procedure TFUNIDADEMEDIDA.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0)
      Else Begin
                if (Key=#27)
                then Self.Close;
      End;
end;

procedure TFUNIDADEMEDIDA.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     esconde_botoes(Self);

     edtcodigo.text:='0';
     edtcodigo.enabled:=False;

     Btgravar.visible:=True;
     BtCancelar.visible:=True;
     btpesquisar.visible:=True;

     Self.ObjUNIDADEMEDIDA.status:=dsInsert;
     edtSIGLA.setfocus;

end;


procedure TFUNIDADEMEDIDA.btalterarClick(Sender: TObject);
begin

    If (Self.ObjUNIDADEMEDIDA.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin

        habilita_campos(Self);
        EdtCodigo.enabled:=False;
        Self.ObjUNIDADEMEDIDA.Status:=dsEdit;
        esconde_botoes(Self);
        Btgravar.visible:=True;
        BtCancelar.visible:=True;
        btpesquisar.visible:=True;
        edtSIGLA.setfocus;
                
    End;


end;

procedure TFUNIDADEMEDIDA.btgravarClick(Sender: TObject);
begin

     If Self.ObjUNIDADEMEDIDA.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
     End;

     If (Self.ObjUNIDADEMEDIDA.salvar(true)=False)
     Then exit;

     edtCodigo.text:=Self.ObjUNIDADEMEDIDA.Get_codigo;
     Self.ObjUNIDADEMEDIDA.localizacodigo(edtcodigo.text);
     Self.tabelaparacontroles;
     
     mostra_botoes(Self);
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFUNIDADEMEDIDA.btexcluirClick(Sender: TObject);
var
Perro:String;
begin
     If (Self.ObjUNIDADEMEDIDA.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjUNIDADEMEDIDA.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
     End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (Self.ObjUNIDADEMEDIDA.exclui(edtcodigo.text,True,perro)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+Perro,mterror,[mbok],0);
               exit;
     End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);
end;

procedure TFUNIDADEMEDIDA.btcancelarClick(Sender: TObject);
begin
     Self.ObjUNIDADEMEDIDA.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     mostra_botoes(Self);

end;

procedure TFUNIDADEMEDIDA.btsairClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFUNIDADEMEDIDA.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjUNIDADEMEDIDA.Get_pesquisa,Self.ObjUNIDADEMEDIDA.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjUNIDADEMEDIDA.status<>dsinactive
                                  then exit;

                                  If (Self.ObjUNIDADEMEDIDA.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                  End;

                                  Self.ObjUNIDADEMEDIDA.ZERARTABELA;
                                  If (Self.TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                  End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;


procedure TFUNIDADEMEDIDA.LimpaLabels;
begin
//LIMPA LABELS
end;

procedure TFUNIDADEMEDIDA.FormShow(Sender: TObject);
begin
     PegaCorForm(Self);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);

     Try
        Self.ObjUNIDADEMEDIDA:=TObjUNIDADEMEDIDA.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           esconde_botoes(self);
           exit;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btgravar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImagemFundo,'FUNDO');
     FescolheImagemBotao.PegaFiguraImagem(ImagemRodape,'RODAPE');
     Self.Color:=Clwhite;
     retira_fundo_labels(self);


     if not (ObjUNIDADEMEDIDA.insereUnidade) then
       self.ObjUNIDADEMEDIDA.Status:=dsInactive
     else
       FDataModulo.IBTransaction.CommitRetaining;

end;


end.

