unit UescolheImagemBotao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, ComCtrls, jpeg;

type
  TFescolheImagemBotao = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Image6: TImage;
    Image7: TImage;
    Image8: TImage;
    Image9: TImage;
    Image10: TImage;
    Image11: TImage;
    Image12: TImage;
    Image13: TImage;
    Image14: TImage;
    Image15: TImage;
    Image16: TImage;
    Image17: TImage;
    Image18: TImage;
    TabSheet2: TTabSheet;
    imfundo: TImage;
    imrodape: TImage;
    imlogo: TImage;
    Image19: TImage;
    Image20: TImage;
    Image21: TImage;
    Image22: TImage;
    Image23: TImage;
    Image24: TImage;
    Image25: TImage;
    Image26: TImage;
    Image27: TImage;
    Image28: TImage;
    Image29: TImage;
    Image30: TImage;
    imgFoto: TImage;
    Image31: TImage;
    Image32: TImage;
    Image58: TImage;
    Image53: TImage;
    Image59: TImage;
    Image60: TImage;
    ts1: TTabSheet;
  private

    { Private declarations }
  public
    { Public declarations }
    Procedure PegaFiguraBotao(Parametro:TBitBtn;Pindice:integer);overload;
    Procedure PegaFiguraBotao(Parametro:TBitBtn;Pnome:String);overload;

    Procedure PegaFiguraBotaopequeno(Parametro:TBitBtn;Pnome:String);overload;

    Procedure PegaFiguraImagem(imlogo: TImage;Pnome:string);

    Procedure PegaFiguraBotoes(btnovo:TBitBtn;btalterar:TBitBtn;btcancelar:TBitBtn;btgravar:TBitBtn;btpesquisar:TBitBtn;btrelatorios:TBitBtn;btexcluir:TBitBtn;btsair:TBitBtn);
    procedure PegaFiguraBotoesPequeno(btnovo, btalterar, btcancelar,btgravar, btpesquisar, btrelatorios, btexcluir, btsair: TBitBtn);overload;
    procedure PegaFiguraBotoesPequeno(btnovo, btalterar, btcancelar,btgravar, btpesquisar, btrelatorios, btexcluir, btsair: TBitBtn;btopcoes: TBitBtn);overload;
    procedure PegaFiguraBotoesPequeno(btnovo, btalterar, btcancelar,btgravar, btpesquisar, btrelatorios, btexcluir, btsair: TBitBtn;btopcoes: TBitBtn;btconcluir: TBitBtn;btpesquisarpedidos: TBitBtn;btexcluiritemcarga: TBitBtn;btalterarquantidade: TBitBtn);overload;
    procedure PegaFiguraBotoesPequeno(btnovo, btalterar, btcancelar,btgravar, btpesquisar, btrelatorios, btexcluir, btsair: TBitBtn;btopcoes: TBitBtn;btgerafinanceiro: TBitBtn; btretornafinanceiro: TBitBtn; btduplicatas: TBitBtn);overload;
    procedure PegaFiguraBotaoSpeedButton(Parametro:TSpeedButton;Pnome: String);
    procedure PegaFiguraBotao2(Parametro:TSpeedButton;Pindice: integer);
  end;



const IndiceBtNovo=0;
const IndiceBtAlterar=1;
const IndiceBtCancelar=2;
const IndiceBtGravar=3;
const IndiceBtExcluir=4;
const IndiceBtRelatorios=5;
const IndiceBtSair=6;
const IndiceBtOpcoes=7;
const IndiceBtPesquisar=8;




const IndiceBtNovoPequeno=9;
const IndiceBtAlterarPEqueno=10;
const IndiceBtCancelarPequeno=11;
const IndiceBtGravarPequeno=12;
const IndiceBtExcluirPequeno=13;
const IndiceBtRelatoriosPequeno=14;
const IndiceBtSairPequeno=15;
const IndiceBtOpcoesPequeno=16;
const IndiceBtPesquisarPequeno=17;
const IndiceBtPesquisarCodigoPequeno=18;
const IndiceBtDescontoPequeno=19;
const IndiceBtInserirPequeno=20;
const IndiceBtAlterarProdutoPequeno=21;
const IndiceBtCancelarProdutoPequeno=22;
const IndiceBtRetirarPequeno=23;
const IndiceBtFinalizarPequeno=24;
const IndiceBtConcluirPequeno=25;
const IndiceBtGerafinanceiropequeno=29;
const IndiceBtRetornaFinanceiroPequeno=30;
const IndiceBtDuplicatasPequeno=31;
const IndiceBtBOTAOCANCELASEFAZ=32;
const IndiceBtConsultaSefaz=33;
const IndiceBtINUTILIZARFAIXA=34;
const IndiceBtIMPRIMIRDANFE=35;
const IndiceBtMENUNFE=36;
const IndiceBtReplicarPedido=37;


var

  FescolheImagemBotao: TFescolheImagemBotao;


implementation

{$R *.dfm}

{ TFescolheImagemBotao }

procedure TFescolheImagemBotao.PegaFiguraBotao(Parametro: TBitBtn;
  Pindice: integer);
var
  Cont:integer;
begin
     for cont:=0 to Self.ComponentCount -1
     do begin
          if (uppercase(Self.Components [cont].ClassName) = 'TIMAGE')
          then Begin
                    if (TImage(Self.Components [cont]).Tag=Pindice)
                    Then Parametro.Glyph:=TImage(Self.Components [cont]).Picture.Bitmap;
          End;
     End;
end;

procedure TFescolheImagemBotao.PegaFiguraBotao2(Parametro:TSpeedButton;
  Pindice: integer);
var
  Cont:integer;
begin
     for cont:=0 to Self.ComponentCount -1
     do begin
          if (uppercase(Self.Components [cont].ClassName) = 'TIMAGE')
          then Begin
                    if (TImage(Self.Components [cont]).Tag=Pindice)
                    Then Parametro.Glyph:=TImage(Self.Components [cont]).Picture.Bitmap;
          End;
     End;
end;

procedure TFescolheImagemBotao.PegaFiguraBotao(Parametro: TBitBtn;
  Pnome:STRING);
begin
     pnome:=uppercase(pnome);

     if (Pnome='BOTAONOVO.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtNovo);

     if (Pnome='BOTAOALTERAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtAlterar);

     if (Pnome='BOTAOCANCELAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtCancelar);

     if (Pnome='BOTAOGRAVAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtGravar);

     if (Pnome='BOTAOEXCLUIR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtExcluir);

     if (Pnome='BOTAOPESQUISAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtPesquisar);

     if (Pnome='BOTAORELATORIOS.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtRelatorios);

     if (Pnome='BOTAOSAIR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtSair);

End;


procedure TFescolheImagemBotao.PegaFiguraBotaopequeno(Parametro: TBitBtn;
  Pnome: String);
begin
     if (Pnome='BOTAONOVO.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtNovoPequeno);

     if (Pnome='BOTAOALTERAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtAlterarPEqueno);

     if (Pnome='BOTAOCANCELAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtCancelarPequeno);

     if (Pnome='BOTAOGRAVAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtGravarPequeno);

     if (Pnome='BOTAOEXCLUIR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtExcluirPequeno);

     if (Pnome='BOTAOPESQUISAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtPesquisarPequeno);

     if (Pnome='BOTAOPESQUISARCODIGO.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtPesquisarCODIGOPequeno);

     if (Pnome='BOTAODESCONTO.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtDescontoPequeno);

     if (Pnome='BOTAORELATORIOS.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtRelatoriosPequeno);

     if (Pnome='BOTAOSAIR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtSairPequeno);

     if (Pnome='BOTAOOPCOES.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtOpcoesPequeno);

     if (Pnome='BOTAOINSERIR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtInserirPequeno);

     if (Pnome='BOTAOALTERARPRODUTO.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtAlterarProdutoPequeno);

     if (Pnome='BOTAOCANCELARPRODUTO.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtCancelarProdutoPequeno);

     if (Pnome='BOTAORETIRAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtRetirarPequeno);

     if (Pnome='BOTAOFINALIZAR.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtFinalizarPequeno);

    { if(Pnome='BOTAOPROXIMO.BMP')
     then self.PegaFiguraBotao(Parametro,IndiceBtProximo);   }

     if (Pnome='BOTAOCANCELASEFAZ.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtBOTAOCANCELASEFAZ);

     if (Pnome='BOTAOCONSULTASEFAZ.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtConsultaSefaz);

     if (Pnome='BOTAOINUTILIZARFAIXA.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtINUTILIZARFAIXA);

     if (Pnome='BOTAOIMPRIMIRDANFE.BMP')
     Then Self.PegaFiguraBotao(parametro,IndiceBtIMPRIMIRDANFE);

     if(Pnome='BOTAOMENUNFE.BMP')
     then Self.PegaFiguraBotao(parametro,IndiceBtMENUNFE);




end;

procedure TFescolheImagemBotao.PegaFiguraBotoes(btnovo, btalterar,
  btcancelar, btgravar, btpesquisar, btrelatorios, btexcluir,
  btsair: TBitBtn);
begin
     if (btnovo<>nil)
     Then Self.PegaFiguraBotao(btnovo,IndiceBtNovo);

     if (btAlterar<>nil)
     Then Self.PegaFiguraBotao(btalterar,IndiceBtAlterar);

     if(btcancelar<>nil)
     Then Self.PegaFiguraBotao(btcancelar,IndiceBtCancelar);

     if (btgravar<>nil)
     Then Self.PegaFiguraBotao(btgravar,IndiceBtGravar);

     if (btpesquisar<>nil)
     Then Self.PegaFiguraBotao(btpesquisar,IndiceBtPesquisar);
     
     if (btrelatorios<>nil)
     then Self.PegaFiguraBotao(btrelatorios,IndiceBtRelatorios);
     
     if(btexcluir<>nil)
     Then Self.PegaFiguraBotao(btexcluir,IndiceBtExcluir);

     if (btsair<>nil)
     Then Self.PegaFiguraBotao(btsair,IndiceBtSair);
end;


procedure TFescolheImagemBotao.PegaFiguraBotoesPequeno(btnovo, btalterar,
  btcancelar, btgravar, btpesquisar, btrelatorios, btexcluir,
  btsair: TBitBtn);
begin
     Self.PegaFiguraBotoesPequeno(btnovo, btalterar,btcancelar, btgravar, btpesquisar, btrelatorios, btexcluir,btsair,nil);
end;

procedure TFescolheImagemBotao.PegaFiguraBotoesPequeno(btnovo, btalterar,
  btcancelar, btgravar, btpesquisar, btrelatorios, btexcluir, btsair,
  btopcoes: TBitBtn);
begin
     if (btnovo<>nil)
     Then Self.PegaFiguraBotao(btnovo,IndiceBtNovopequeno);

     if (btAlterar<>nil)
     Then Self.PegaFiguraBotao(btalterar,IndiceBtAlterarpequeno);

     if(btcancelar<>nil)
     Then Self.PegaFiguraBotao(btcancelar,IndiceBtCancelarpequeno);

     if (btgravar<>nil)
     Then Self.PegaFiguraBotao(btgravar,IndiceBtGravarpequeno);

     if (btpesquisar<>nil)
     Then Self.PegaFiguraBotao(btpesquisar,IndiceBtPesquisarpequeno);

     if (btrelatorios<>nil)
     then Self.PegaFiguraBotao(btrelatorios,IndiceBtRelatoriospequeno);

     if(btexcluir<>nil)
     Then Self.PegaFiguraBotao(btexcluir,IndiceBtExcluirpequeno);

     if (btsair<>nil)
     Then Self.PegaFiguraBotao(btsair,IndiceBtSairpequeno);

     if (btopcoes<>nil)
     Then Self.PegaFiguraBotao(btopcoes,IndiceBtOpcoesPequeno);
end;

procedure TFescolheImagemBotao.PegaFiguraBotoesPequeno(btnovo, btalterar,
  btcancelar, btgravar, btpesquisar, btrelatorios, btexcluir, btsair,
  btopcoes, btconcluir,btpesquisarpedidos,btexcluiritemcarga,btalterarquantidade: TBitBtn);
begin
     if (btnovo<>nil)
     Then Self.PegaFiguraBotao(btnovo,IndiceBtNovopequeno);

     if (btAlterar<>nil)
     Then Self.PegaFiguraBotao(btalterar,IndiceBtAlterarpequeno);

     if(btcancelar<>nil)
     Then Self.PegaFiguraBotao(btcancelar,IndiceBtCancelarpequeno);

     if (btgravar<>nil)
     Then Self.PegaFiguraBotao(btgravar,IndiceBtGravarpequeno);

     if (btpesquisar<>nil)
     Then Self.PegaFiguraBotao(btpesquisar,IndiceBtPesquisarpequeno);

     if (btrelatorios<>nil)
     then Self.PegaFiguraBotao(btrelatorios,IndiceBtRelatoriospequeno);

     if(btexcluir<>nil)
     Then Self.PegaFiguraBotao(btexcluir,IndiceBtExcluirpequeno);

     if (btsair<>nil)
     Then Self.PegaFiguraBotao(btsair,IndiceBtSairpequeno);

     if (btopcoes<>nil)
     Then Self.PegaFiguraBotao(btopcoes,IndiceBtOpcoesPequeno);

     if (btconcluir<>nil)
     Then Self.PegaFiguraBotao(btconcluir,IndiceBtConcluirPequeno);

     if(btpesquisarpedidos<>nil)
     Then Self.PegaFiguraBotao(btpesquisarpedidos,IndiceBtPesquisarPequeno);

     if(btexcluiritemcarga<>nil)
     Then Self.PegaFiguraBotao(btexcluiritemcarga,IndiceBtRetirarPequeno);

     if(btalterarquantidade<>nil)
     Then Self.PegaFiguraBotao(btalterarquantidade,IndiceBtalterarProdutoPequeno);
end;

procedure TFescolheImagemBotao.PegaFiguraBotoesPequeno(btnovo, btalterar,
  btcancelar, btgravar, btpesquisar, btrelatorios, btexcluir, btsair,
  btopcoes, btgerafinanceiro, btretornafinanceiro,
  btduplicatas: TBitBtn);
begin
     if (btnovo<>nil)
     Then Self.PegaFiguraBotao(btnovo,IndiceBtNovopequeno);

     if (btAlterar<>nil)
     Then Self.PegaFiguraBotao(btalterar,IndiceBtAlterarpequeno);

     if(btcancelar<>nil)
     Then Self.PegaFiguraBotao(btcancelar,IndiceBtCancelarpequeno);

     if (btgravar<>nil)
     Then Self.PegaFiguraBotao(btgravar,IndiceBtGravarpequeno);

     if (btpesquisar<>nil)
     Then Self.PegaFiguraBotao(btpesquisar,IndiceBtPesquisarpequeno);

     if (btrelatorios<>nil)
     then Self.PegaFiguraBotao(btrelatorios,IndiceBtRelatoriospequeno);

     if(btexcluir<>nil)
     Then Self.PegaFiguraBotao(btexcluir,IndiceBtExcluirpequeno);

     if (btsair<>nil)
     Then Self.PegaFiguraBotao(btsair,IndiceBtSairpequeno);

     if (btopcoes<>nil)
     Then Self.PegaFiguraBotao(btopcoes,IndiceBtOpcoesPequeno);

     if(btgerafinanceiro<>nil)
     Then Self.PegaFiguraBotao(btgerafinanceiro ,IndiceBtGerafinanceiropequeno);

     if(btretornafinanceiro<>nil)
     Then Self.PegaFiguraBotao(btretornafinanceiro,IndiceBtRetornaFinanceiroPequeno);

     if(btduplicatas<>nil)
     Then Self.PegaFiguraBotao(btduplicatas,IndiceBtDuplicatasPequeno);

end;

procedure TFescolheImagemBotao.PegaFiguraImagem(imlogo: TImage;
  Pnome: string);
var

  MyFormat : Word;
  AData:  THandle;
  APalette: HPALETTE;
begin
     pnome:=uppercase(pnome);

     if (pnome='FUNDO')
     Then Begin
              imlogo.Picture:=Self.imfundo.Picture;
     End;


     

     if (pnome='RODAPE')
     Then imlogo.Picture:=Self.imrodape.Picture;

     if(pnome='FOTO')
     then imlogo.Picture:=self.imgFoto.Picture;

end;

procedure TFescolheImagemBotao.PegaFiguraBotaoSpeedButton(parametro:TSpeedButton;Pnome:string);
begin
     if(Pnome='BOTAOREPLICARPEDIDO.BMP')
     then Self.PegaFiguraBotao2(Parametro,IndiceBtReplicarPedido);

end;

end.
