object FConfiguracaoSite: TFConfiguracaoSite
  Left = 272
  Top = 148
  Width = 827
  Height = 516
  Caption = 'Configura'#231#227'o Web'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object panelbotes: TPanel
    Left = 0
    Top = 0
    Width = 811
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    object lbnomeformulario: TLabel
      Left = 535
      Top = 0
      Width = 276
      Height = 54
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Configura'#231#227'o Web'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -27
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btalterar: TBitBtn
      Left = -1
      Top = -1
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btalterarClick
    end
    object btgravar: TBitBtn
      Left = 49
      Top = -1
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btgravarClick
    end
    object btcancelar: TBitBtn
      Left = 99
      Top = -1
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btcancelarClick
    end
    object btsair: TBitBtn
      Left = 149
      Top = -1
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btsairClick
    end
  end
  object panelrodape: TPanel
    Left = 0
    Top = 428
    Width = 811
    Height = 50
    Align = alBottom
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 1
    object imgRodape: TImage
      Left = 0
      Top = 0
      Width = 811
      Height = 50
      Align = alClient
      Stretch = True
    end
  end
  object panel2: TPanel
    Left = 0
    Top = 54
    Width = 811
    Height = 374
    Align = alClient
    TabOrder = 2
    DesignSize = (
      811
      374)
    object LbDATABASESITE: TLabel
      Left = 15
      Top = 23
      Width = 33
      Height = 14
      Caption = 'Banco'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbHOSTNAMESITE: TLabel
      Left = 15
      Top = 45
      Width = 56
      Height = 14
      Caption = 'Hostname'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbPASSWORDSITE: TLabel
      Left = 15
      Top = 67
      Width = 56
      Height = 14
      Caption = 'Password'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbPORTSITE: TLabel
      Left = 15
      Top = 89
      Width = 29
      Height = 14
      Caption = 'Porta'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbPROTOCOLSITE: TLabel
      Left = 15
      Top = 111
      Width = 53
      Height = 14
      Caption = 'Protocolo'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbUSERSITE: TLabel
      Left = 15
      Top = 133
      Width = 42
      Height = 14
      Caption = 'Usu'#225'rio'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb2: TLabel
      Left = 15
      Top = 173
      Width = 68
      Height = 16
      Alignment = taCenter
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Dados FTP'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -13
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object bt2: TSpeedButton
      Left = 90
      Top = 312
      Width = 263
      Height = 38
      Cursor = crHandPoint
      Anchors = [akLeft, akBottom]
      BiDiMode = bdLeftToRight
      Caption = 'Testar Conex'#227'o com Banco de Dados'
      Flat = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      Glyph.Data = {
        F6060000424DF606000000000000360000002800000018000000180000000100
        180000000000C006000000000000000000000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFA39B917D7469E2DED8FF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE9E4DF7D75699C94
        88FF00FFFF00FFFF00FFFF00FFFF00FFE8CDA8D19E55D39F55DAB176E7CBA4E7
        CBA4E7CBA4E7CBA4E7CBA4E7CBA4E7CBA4E7CBA4E7CBA4E7CBA4E7CBA4E7CBA4
        DBB47CD39F55D19D55E7CAA3FF00FFFF00FFFF00FFFF00FFD7AA6FD9AC72D9AC
        72D9AC72D9AC72D9AC72D9AC72D9AC72D9AC72D9AC72D9AC71D9AC71D9AC72D9
        AC72D9AC72D9AC72D9AC71D9AC71D9AC71D7A96FFF00FFFF00FFFF00FFFF00FF
        DEBB8DDFC097BEA88DB9A58AB9A58AB9A48AB9A48AB9A48ABAA58ABAA58ABAA5
        8ABBA58ABBA68ABBA68ABBA68ABBA68ABBA68BC1AB8DDFBF95DEBC8EF7EFE5FF
        00FFFF00FFFF00FFE5CEB0E6D7C4B2B5BA8DA6C38DA6C395A9C2AAB3BF8FA8C5
        8FA8C5A2AFC2A1B0C38FA9C691AAC7AFB7C397ADC691AAC892ABC7BDBEC1E5D6
        C2E5CFB1F6EEE1FF00FFFF00FFFF00FFEADCC7EFEDEBC7D4E359D6FE65E0FF67
        CAF8AAD6EE5EDBFF62DEFF8ACEF488D3F563DFFF5DDAFFB0D5F165D1FB65E1FF
        58D2FDDAE2EEEFEEEBEBDFCCF6EEE1FF00FFFF00FFFF00FFEBDAC2F6F6F6E6ED
        F697D5FA98D6FAA4D5F9D4E9FB98D6FA98D6FABDDEFABBDFFA98D6FA97D6FAD5
        E7FBA2D6FA98D6FA97D4FAEDF1F9F6F6F6ECDEC9F6EEE1FF00FFFF00FFFF00FF
        E4C89EFBF8F3E8E8E8E3E3E3E4E4E4E4E4E4E5E5E5E6E6E6E6E6E6E6E6E6E7E7
        E7E8E8E8E8E8E8E9E9E9EAEAEAEAEAEAEBEBEBF0F0F0F9F7F5E3C598FF00FFFF
        00FFFF00FFFF00FFEFDEC6F1E2CCFCFCFCFAFAFAFAFAFAFAFAFAFAFAFAF9F9F9
        F9F9F9F9F9F9F8F8F8F8F8F8F7F7F7F7F7F7F7F7F7F7F7F7F7F7F7F8F8F8F0E4
        D2EDD8BBFF00FFFF00FFFF00FFFF00FFFF00FFE8CDA7FBF9F7FBFAFAFAFAF9FA
        F9F8F9F8F7F4EDE5F0E4D8F8F7F6EBDAC8F7F5F3F6F3EFECDCCCF8F7F6EAD8C5
        F8F6F5F7F6F5E5CAA3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFF3E6D5ECDC
        C8F5F0ECF5F0ECF5F0EBF5F0EBF5F0EBF0E7DFF5F0EBF2E9E2F4EEE8F3EDE7F2
        EAE3F4EEE8F2E9E2F5F0EBEEE2D3EFDEC6FF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFE8D0AEF1E7DDF1E7DEF1E8DFF1E8DFF1E8DFEBD9C4F1E8DFEDDE
        CDEFE4D7EFE3D6EDDECEEFE4D8EDDDCDF2E9E0E7CEACFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFF6EDE0E2C6A2E8D1B6E1CCB3E1CCB4E8D3BA
        E7CEB2E5CAACE5CCAFE7CFB4E4C9ACE9D4BCE3C8AAEAD7C2E7D2B8F2E3CFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE7CCA7DDB787C5
        A77FA48E72B89D7CDDBA8DE0BC8FE0BD90E0BD91E0BE92E0BF93E1BF94E1C096
        E3C399FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFF1E1CCD2BFA878553D98745BC1AB94F0DFC8F0DFC8F0DFC8F0DFC8F0
        DFC8F0DFC8E6DDC1FF00FFD1F5DF24D76BFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFD8D8D8696969999999C0C0C0FF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FF28D96FCDF5DCFF00FF61E39445DE82FF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFDEDEDE7272729E9E9E
        C6C6C6FF00FFFF00FFFF00FF5AE18FB4F0CBFF00FFBBF2D032DA75FF00FFFF00
        FF16D662C5F3D7FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFE2
        E2E27070709C9C9CCBCBCBD6F6E242DD7FFF00FFFF00FF44DD81FF00FFFF00FF
        39DC7AD6F6E2FF00FF5AE19187E9AEFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FF6E6E6E9A9A9AD1D1D1FF00FF45DD81FF00FFFF00FF44
        DE81FF00FFFF00FF4ADE85C7F4D8FF00FF67E3987CE7A6FF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF707070979797D6D6D6CEF5DDB4F0
        CBFF00FFA3EEC070E59EFF00FFFF00FF20D869FF00FFFF00FF31D975A7EEC3FF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF727272909090
        DEDEDEFF00FFFF00FFFF00FF9FEDBDFF00FFFF00FF47DD8386E9ADFF00FFA8EE
        C31BD666FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFA5A5A5989898FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFA9EEC4
        FF00FFDBF7E615D561A7EEC3FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFB4F0CCFF00FFFF00FFFF00FF}
      ParentFont = False
      ParentBiDiMode = False
      OnClick = bt2Click
    end
    object Label1: TLabel
      Left = 15
      Top = 154
      Width = 76
      Height = 14
      Caption = 'ID Cliente Site'
      Font.Charset = ANSI_CHARSET
      Font.Color = 5855577
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object EdtDATABASESITE: TEdit
      Left = 100
      Top = 20
      Width = 509
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 250
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 0
    end
    object EdtHOSTNAMESITE: TEdit
      Left = 100
      Top = 42
      Width = 509
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 250
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 1
    end
    object EdtPASSWORDSITE: TEdit
      Left = 100
      Top = 64
      Width = 509
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 250
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 2
    end
    object EdtPORTSITE: TEdit
      Left = 100
      Top = 86
      Width = 509
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 250
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 3
    end
    object EdtPROTOCOLSITE: TEdit
      Left = 100
      Top = 108
      Width = 509
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 250
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 4
    end
    object EdtUSERSITE: TEdit
      Left = 100
      Top = 130
      Width = 509
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 250
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 5
    end
    object panelFTP1: TPanel
      Left = 87
      Top = 192
      Width = 680
      Height = 106
      Anchors = [akLeft, akTop, akRight]
      BevelInner = bvRaised
      Color = clActiveCaption
      TabOrder = 6
      object lbLbHOSTFTP: TLabel
        Left = 19
        Top = 21
        Width = 29
        Height = 13
        Caption = 'Host'
        Font.Charset = ANSI_CHARSET
        Font.Color = 5855577
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbLbUSERNAMEFTP: TLabel
        Left = 19
        Top = 44
        Width = 50
        Height = 13
        Caption = 'Usu'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = 5855577
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lbLbPASSWORDFTP: TLabel
        Left = 19
        Top = 67
        Width = 62
        Height = 13
        Caption = 'Password'
        Font.Charset = ANSI_CHARSET
        Font.Color = 5855577
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdtHOSTFTP: TEdit
        Left = 179
        Top = 18
        Width = 459
        Height = 19
        Ctl3D = False
        MaxLength = 250
        ParentCtl3D = False
        TabOrder = 0
      end
      object EdtUSERNAMEFTP: TEdit
        Left = 179
        Top = 41
        Width = 459
        Height = 19
        Ctl3D = False
        MaxLength = 250
        ParentCtl3D = False
        TabOrder = 1
      end
      object EdtPASSWORDFTP: TEdit
        Left = 179
        Top = 64
        Width = 459
        Height = 19
        Ctl3D = False
        MaxLength = 250
        ParentCtl3D = False
        TabOrder = 2
      end
    end
    object Edit1: TEdit
      Left = 100
      Top = 151
      Width = 77
      Height = 20
      Ctl3D = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Arial'
      Font.Style = []
      MaxLength = 250
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 7
    end
  end
end
