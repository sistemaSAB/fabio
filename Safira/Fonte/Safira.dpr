program Safira;

uses
  Forms,
  UDataModulo in 'UDataModulo.pas' {FDataModulo: TDataModule},
  ULogin in 'ULogin.pas' {Flogin},
  Useg in '..\..\Compartilhados\Useg.pas',
  Usobre in 'Usobre.pas' {Fsobre},
  UopcaoRel in 'UopcaoRel.pas' {FOpcaorel},
  UobjConfRelatorio in 'UobjConfRelatorio.pas',
  UBackup in 'Configuracoes\UBackup.pas' {Fbackup},
  UobjParametros in '..\..\Compartilhados\UobjParametros.pas',
  Uparametros in '..\..\Compartilhados\Uparametros.pas' {Fparametros},
  UobjPrinterMatrix in '..\..\Compartilhados\UobjPrinterMatrix.pas',
  UObjProcedimentosPesquisa in '..\..\Compartilhados\UObjProcedimentosPesquisa.pas',
  Uprincipal in 'Uprincipal.pas' {Fprincipal},
  UChequesPortador in '..\..\Financeiro\UChequesPortador.pas' {FchequesPortador},
  UBaixaCheque in '..\..\Financeiro\UBaixaCheque.pas' {FbaixaCheque},
  UConfValorParcelas in '..\..\Financeiro\UConfValorParcelas.pas' {FConfValorParcelas},
  Ucontager in '..\..\Financeiro\Ucontager.pas' {Fcontager},
  UCorrecaodeTitulos in '..\..\Financeiro\UCorrecaodeTitulos.pas' {FcorrecaodeTitulos},
  UCredorDevedor in '..\..\Financeiro\UCredorDevedor.pas' {Fcredordevedor},
  UGerador in '..\..\Financeiro\UGerador.pas' {Fgerador},
  Ugeralancamento in '..\..\Financeiro\Ugeralancamento.pas' {FGeraLancamento},
  UGeraTitulo in '..\..\Financeiro\UGeraTitulo.pas' {FGeraTitulo},
  UgeraTransferencia in '..\..\Financeiro\UgeraTransferencia.pas' {Fgeratransferencia},
  UHistorico in '..\..\Financeiro\UHistorico.pas' {Fhistorico},
  UHistoricoLancamentos in '..\..\Financeiro\UHistoricoLancamentos.pas' {FhistoricoLancamento},
  ULancamentoFR in '..\..\Financeiro\ULancamentoFR.pas' {FrLancamento: TFrame},
  ULanctoPortadorFR in '..\..\Financeiro\ULanctoPortadorFR.pas' {FrLanctoPortador: TFrame},
  UObjchequesPortador in '..\..\Financeiro\UObjchequesPortador.pas',
  UObjContaGer in '..\..\Financeiro\UObjContaGer.pas',
  UObjCredorDevedor in '..\..\Financeiro\UObjCredorDevedor.pas',
  UObjGerador in '..\..\Financeiro\UObjGerador.pas',
  UObjGeraLancamento in '..\..\Financeiro\UObjGeraLancamento.pas',
  UObjGeraTitulo in '..\..\Financeiro\UObjGeraTitulo.pas',
  UObjGeraTransferencia in '..\..\Financeiro\UObjGeraTransferencia.pas',
  UObjGrupo in '..\..\Financeiro\UObjGrupo.pas',
  UObjHistorico in '..\..\Financeiro\UObjHistorico.pas',
  UObjLancamento in '..\..\Financeiro\UObjLancamento.pas',
  UObjLanctoPortador in '..\..\Financeiro\UObjLanctoPortador.pas',
  UObjPendencia in '..\..\Financeiro\UObjPendencia.pas',
  UobjPortador in '..\..\Financeiro\UobjPortador.pas',
  UObjPrazoPagamento in '..\..\Financeiro\UObjPrazoPagamento.pas',
  UObjTalaodeCheques in '..\..\Financeiro\UObjTalaodeCheques.pas',
  UObjTipoLancto in '..\..\Financeiro\UObjTipoLancto.pas',
  UObjTitulo in '..\..\Financeiro\UObjTitulo.pas',
  UObjTransferenciaPortador in '..\..\Financeiro\UObjTransferenciaPortador.pas',
  UObjValores in '..\..\Financeiro\UObjValores.pas',
  UObjValoresTransferenciaPortador in '..\..\Financeiro\UObjValoresTransferenciaPortador.pas',
  UPendencia in '..\..\Financeiro\UPendencia.pas' {Fpendencia},
  Uportador in '..\..\Financeiro\Uportador.pas' {Fportador},
  UPrazoPagamento in '..\..\Financeiro\UPrazoPagamento.pas' {FprazoPagamento},
  UTalaodeCheques in '..\..\Financeiro\UTalaodeCheques.pas' {FtalaodeCheques},
  UTipoLancto in '..\..\Financeiro\UTipoLancto.pas' {FtipoLancto},
  UTransferenciaPortador in '..\..\Financeiro\UTransferenciaPortador.pas' {FtransferenciaPortador},
  UValores in '..\..\Financeiro\UValores.pas' {Fvalores},
  UpequenoDebito in '..\..\Financeiro\UpequenoDebito.pas' {FpequenoDebito},
  UObjTipoLanctoPortador in '..\..\Financeiro\UObjTipoLanctoPortador.pas',
  UTipoLanctoPortador in '..\..\Financeiro\UTipoLanctoPortador.pas' {Ftipolanctoportador},
  UMotivoDevolucao in '..\..\Financeiro\UMotivoDevolucao.pas' {FMotivoDevolucao},
  UDeposito in '..\..\Financeiro\UDeposito.pas' {Fdeposito},
  UobjPlanodeContas in '..\..\Financeiro\UobjPlanodeContas.pas',
  UPLanodeContas in '..\..\Financeiro\UPLanodeContas.pas' {FplanodeContas},
  UExportaContabilidade in '..\..\Financeiro\UExportaContabilidade.pas' {FexportaContabilidade},
  UobjExportaContabilidade in '..\..\Financeiro\UobjExportaContabilidade.pas',
  UObjHistoricoSimples in '..\..\Financeiro\UObjHistoricoSimples.pas',
  UComprovanteCheque in '..\..\Financeiro\UComprovanteCheque.pas' {FcomprovanteCheque},
  UobjComprovanteCheque in '..\..\Financeiro\UobjComprovanteCheque.pas',
  URelComprovanteCheque in '..\..\Financeiro\URelComprovanteCheque.pas' {FrelComprovanteCheque},
  URelCheque in '..\..\Financeiro\URelCheque.pas' {FrelCheque},
  UHistoricoSimples in '..\..\Financeiro\UHistoricoSimples.pas' {Fhistoricosimples},
  UfrBaixaCheque in '..\..\Financeiro\UfrBaixaCheque.pas' {FrBaixaCheque: TFrame},
  UComprovantePagamentoTMP in '..\..\Financeiro\UComprovantePagamentoTMP.pas' {FcomprovantepagamentoTMP},
  UConfiguraCheque in '..\..\Financeiro\UConfiguraCheque.pas' {FConfiguraCheque},
  UprevisaoFinanceira in '..\..\Financeiro\UprevisaoFinanceira.pas' {FprevisaoFinanceira},
  UObjPrevisaoFinanceira in '..\..\Financeiro\UObjPrevisaoFinanceira.pas',
  UTitulo in '..\..\Financeiro\UTitulo.pas' {Ftitulo},
  UFiltraImp in '..\..\Compartilhados\UFiltraImp.pas' {FfiltroImp},
  UinfoCheque in '..\..\Financeiro\UinfoCheque.pas' {Finfocheque},
  URelComprovanteChequeSRecibo in '..\..\Financeiro\URelComprovanteChequeSRecibo.pas' {FrelComprovanteChequeSRecibo},
  UUsuarios in '..\..\Compartilhados\UUsuarios.pas' {Fusuarios},
  UNiveis in '..\..\Compartilhados\UNiveis.pas' {FNiveis},
  UObjniveis in '..\..\Compartilhados\UObjniveis.pas',
  UObjPermissoesUso in '..\..\Compartilhados\UObjPermissoesUso.pas',
  UobjUsuarioIB in '..\..\Compartilhados\UobjUsuarioIB.pas',
  UObjUsuarios in '..\..\Compartilhados\UObjUsuarios.pas',
  UPermissoesUso in '..\..\Compartilhados\UPermissoesUso.pas' {FPermissoesUso},
  UobjBoletoBancario in '..\..\Financeiro\UobjBoletoBancario.pas',
  UBoletoBancario in '..\..\Financeiro\UBoletoBancario.pas' {FBoletoBancario},
  UEscolheBoletos in '..\..\Financeiro\UEscolheBoletos.pas' {FEscolheBoletos},
  Uobjduplicata in '..\..\Financeiro\Uobjduplicata.pas',
  UEscolhePendencias in '..\..\Financeiro\UEscolhePendencias.pas' {FescolhePendencias},
  URDPRINT in '..\..\Financeiro\URDPRINT.pas' {FRdPrint},
  UimpDuplicata in '..\..\Financeiro\UimpDuplicata.pas' {FImpDuplicata},
  UReltxtRDPRINT in '..\..\Compartilhados\UReltxtRDPRINT.pas' {FreltxtRDPRINT},
  UimpDuplicata02 in '..\..\Financeiro\UimpDuplicata02.pas' {FImpDuplicata02},
  UobjMenuFinanceiro in '..\..\Financeiro\UobjMenuFinanceiro.pas',
  UConfiguraChequeRDPRINT in '..\..\Financeiro\UConfiguraChequeRDPRINT.pas' {FConfiguraChequeRDPRINT},
  UObjConfiguraFolhaRdPrint in '..\..\Compartilhados\UObjConfiguraFolhaRdPrint.pas',
  uConfiguraBoleto in '..\..\Financeiro\uConfiguraBoleto.pas' {FConfiguraBoleto},
  URelAnotacoes in '..\..\Compartilhados\URelAnotacoes.pas' {Frelanotacoes},
  UAnotacoes in '..\..\Compartilhados\UAnotacoes.pas' {Fanotacoes},
  UObjAnotacoes in '..\..\Compartilhados\UObjAnotacoes.pas',
  Ucalendario in '..\..\Compartilhados\Ucalendario.pas' {Fcalendario},
  ULancaNovoLancamento in '..\..\Financeiro\ULancaNovoLancamento.pas' {FLancaNovoLancamento},
  UGrupo in '..\..\Financeiro\UGrupo.pas' {Fgrupo},
  UAlteraPendencias_Titulo in '..\..\Financeiro\UAlteraPendencias_Titulo.pas' {FAlteraPendencias_Titulo},
  UobjMENURELATORIOS in '..\..\Compartilhados\UobjMENURELATORIOS.pas',
  UMenuRelatorios in '..\..\Compartilhados\UMenuRelatorios.pas' {FMenuRelatorios},
  UobjABASMENURELATORIOS in '..\..\Compartilhados\UobjABASMENURELATORIOS.pas',
  UobjACERTOPENDENCIAS in '..\..\Financeiro\UobjACERTOPENDENCIAS.pas',
  UAcertoCreditoDebito in '..\..\Financeiro\UAcertoCreditoDebito.pas' {FAcertoCreditoDebito},
  UACERTOPENDENCIAS in '..\..\Financeiro\UACERTOPENDENCIAS.pas' {FACERTOPENDENCIAS},
  UFrPagamento in '..\..\Financeiro\UFrPagamento.pas' {FrPagamento: TFrame},
  UFrRecebimento in '..\..\Financeiro\UFrRecebimento.pas' {FrRecimento: TFrame},
  UobjVENCTOPEND in '..\..\Compartilhados\UobjVENCTOPEND.pas',
  UAcertaDataServidor in '..\..\Compartilhados\UAcertaDataServidor.pas' {FacertaDataServidor},
  UAcertaVenctoServidor in '..\..\Compartilhados\UAcertaVenctoServidor.pas' {FacertaVenctoServidor},
  UMostraBarraProgresso in '..\..\Compartilhados\UMostraBarraProgresso.pas' {FMostraBarraProgresso},
  Urecibo in '..\..\Financeiro\Urecibo.pas' {Frecibo},
  UimpReciboLancamento in '..\..\Financeiro\UimpReciboLancamento.pas' {FimpReciboLancamento},
  UobjBoletoSemRegistro in '..\..\Compartilhados\boleto\UobjBoletoSemRegistro.pas',
  UrelboletoSemRegistro in '..\..\Compartilhados\boleto\UrelboletoSemRegistro.pas' {FRelBoletoSemregistro: TQuickRep},
  UobjCONVENIOSBOLETO in '..\..\Financeiro\UobjCONVENIOSBOLETO.pas',
  UCONVENIOSBOLETO in '..\..\Financeiro\UCONVENIOSBOLETO.pas' {FCONVENIOSBOLETO},
  urelatorio in '..\..\Compartilhados\urelatorio.pas' {FRelatorio},
  UescolheConvenio in '..\..\Financeiro\UescolheConvenio.pas' {FescolheConvenio},
  UfrValorLanctoLote in '..\..\Financeiro\UfrValorLanctoLote.pas' {FrValorLanctoLote: TFrame},
  UValorLanctoLote_rec in '..\..\Financeiro\UValorLanctoLote_rec.pas' {FvalorLanctoLote_REC},
  Uformata_String_Grid in '..\..\Compartilhados\Uformata_String_Grid.pas',
  UObjRecibo in '..\..\Financeiro\UObjRecibo.pas',
  UDataN in '..\..\Compartilhados\Compatibilidade\UDataN.pas' {FdataN},
  UOrigemAnotacoes in '..\..\Compartilhados\UOrigemAnotacoes.pas' {FOrigemAnotacoes},
  UObjOrigemAnotacoes in '..\..\Compartilhados\UObjOrigemAnotacoes.pas',
  UobjGRUPOFERRAGEM in 'UobjGRUPOFERRAGEM.pas',
  UGRUPOFERRAGEM in 'UGRUPOFERRAGEM.pas' {FGRUPOFERRAGEM},
  UobjGRUPOSERVICO in 'UobjGRUPOSERVICO.pas',
  UGRUPOSERVICO in 'UGRUPOSERVICO.pas' {FGRUPOSERVICO},
  UobjGRUPOPERFILADO in 'UobjGRUPOPERFILADO.pas',
  UGRUPOPERFILADO in 'UGRUPOPERFILADO.pas' {FGRUPOPERFILADO},
  UobjGRUPOVIDRO in 'UobjGRUPOVIDRO.pas',
  UGRUPOVIDRO in 'UGRUPOVIDRO.pas' {FGRUPOVIDRO},
  UobjRAMOATIVIDADE in 'UobjRAMOATIVIDADE.pas',
  URAMOATIVIDADE in 'URAMOATIVIDADE.pas' {FRAMOATIVIDADE},
  UobjFORNECEDOR in 'UobjFORNECEDOR.pas',
  UFORNECEDOR in 'UFORNECEDOR.pas' {FFORNECEDOR},
  UobjCOR in 'UobjCOR.pas',
  UCOR in 'UCOR.pas' {FCOR},
  UobjFERRAGEM in 'UobjFERRAGEM.pas',
  UFERRAGEM in 'UFERRAGEM.pas' {FFERRAGEM},
  UlancaTroco in '..\..\Financeiro\UlancaTroco.pas' {FlancaTroco},
  URelPedidoRdPrint in '..\..\Compartilhados\URelPedidoRdPrint.pas' {FRelPedidoRdPrint},
  UTABELAB_ST in 'UTABELAB_ST.pas' {FTABELAB_ST},
  UobjTABELAA_ST in 'UobjTABELAA_ST.pas',
  UobjTABELAB_ST in 'UobjTABELAB_ST.pas',
  UTABELAA_ST in 'UTABELAA_ST.pas' {FTABELAA_ST},
  UAcertaBoletoaPagar in '..\..\Financeiro\UAcertaBoletoaPagar.pas' {FAcertaBoletoaPagar},
  UobjNotaFiscalObjetos in 'UobjNotaFiscalObjetos.pas',
  USERVICO in 'USERVICO.pas' {FSERVICO},
  UPERFILADO in 'UPERFILADO.pas' {FPERFILADO},
  UobjPERFILADO in 'UobjPERFILADO.pas',
  UVIDRO in 'UVIDRO.pas' {FVIDRO},
  UobjVIDRO in 'UobjVIDRO.pas',
  UKITBOX in 'UKITBOX.pas' {FKITBOX},
  UobjKITBOX in 'UobjKITBOX.pas',
  UobjCOMPONENTE in 'UobjCOMPONENTE.pas',
  UCOMPONENTE in 'UCOMPONENTE.pas' {FCOMPONENTE},
  UobjCLIENTE in 'UobjCLIENTE.pas',
  UCLIENTE in 'UCLIENTE.pas' {FCLIENTE},
  UobjFERRAGEMCOR in 'UobjFERRAGEMCOR.pas',
  UobjKITBOXCOR in 'UobjKITBOXCOR.pas',
  UobjPERFILADOCOR in 'UobjPERFILADOCOR.pas',
  UobjVIDROCOR in 'UobjVIDROCOR.pas',
  UobjPROJETO in 'UobjPROJETO.pas',
  UPROJETO in 'UPROJETO.pas' {FPROJETO},
  UobjCOMPONENTE_PROJ in 'UobjCOMPONENTE_PROJ.pas',
  UobjFERRAGEM_PROJ in 'UobjFERRAGEM_PROJ.pas',
  UobjKITBOX_PROJ in 'UobjKITBOX_PROJ.pas',
  UobjPERFILADO_PROJ in 'UobjPERFILADO_PROJ.pas',
  UobjSERVICO_PROJ in 'UobjSERVICO_PROJ.pas',
  UobjPEDIDO_PEDIDO in 'UobjPEDIDO_PEDIDO.pas',
  UPEDIDO in 'UPEDIDO.pas' {FPEDIDO},
  UpesquisaProjeto in 'UpesquisaProjeto.pas' {FpesquisaProjeto},
  UobjGRUPODIVERSO in 'UobjGRUPODIVERSO.pas',
  UGRUPODIVERSO in 'UGRUPODIVERSO.pas' {FGRUPODIVERSO},
  UobjDIVERSO in 'UobjDIVERSO.pas',
  UDIVERSO in 'UDIVERSO.pas' {FDIVERSO},
  UobjGRUPOPERSIANA in 'UobjGRUPOPERSIANA.pas',
  UGRUPOPERSIANA in 'UGRUPOPERSIANA.pas' {FGRUPOPERSIANA},
  UPERSIANA in 'UPERSIANA.pas' {FPERSIANA},
  UobjPERSIANA in 'UobjPERSIANA.pas',
  UDIAMETRO in 'UDIAMETRO.pas' {FDIAMETRO},
  UobjDIAMETRO in 'UobjDIAMETRO.pas',
  UobjPERSIANAGRUPODIAMETROCOR in 'UobjPERSIANAGRUPODIAMETROCOR.pas',
  UobjPEDIDO_PROJ_PEDIDO in 'UobjPEDIDO_PROJ_PEDIDO.pas',
  UobjFERRAGEM_PP_PEDIDO in 'UobjFERRAGEM_PP_PEDIDO.pas',
  UobjPERFILADO_PP_PEDIDO in 'UobjPERFILADO_PP_PEDIDO.pas',
  UobjVIDRO_PP_PEDIDO in 'UobjVIDRO_PP_PEDIDO.pas',
  UobjKITBOX_PP_Pedido in 'UobjKITBOX_PP_Pedido.pas',
  UobjSERVICO_PP_Pedido in 'UobjSERVICO_PP_Pedido.pas',
  UobjPERSIANA_PP_PEDIDO in 'UobjPERSIANA_PP_PEDIDO.pas',
  UobjDIVERSO_PP_PEDIDO in 'UobjDIVERSO_PP_PEDIDO.pas',
  UCalculaformula_Perfilado in 'UCalculaformula_Perfilado.pas' {Fcalculaformula_Perfilado},
  UmostraStringList in '..\..\Compartilhados\UmostraStringList.pas' {FmostraStringList},
  UObjMotivoDevolucao in '..\..\Financeiro\UObjMotivoDevolucao.pas',
  UessencialGlobal in '..\..\Compartilhados\UessencialGlobal.pas',
  UessencialLocal in 'UessencialLocal.pas',
  UobjSERVICO in 'UobjSERVICO.pas',
  UescolheVidro in 'UescolheVidro.pas' {FescolheVidro},
  UrelPedidoProjetoBranco in 'UrelPedidoProjetoBranco.pas' {QRpedidoProjetoBranco: TQuickRep},
  UCalculaformula_Vidro in 'UCalculaformula_Vidro.pas' {Fcalculaformula_Vidro},
  UMedidas_proj in 'UMedidas_proj.pas' {Fmedidas_proj},
  UobjDIVERSOCOR in 'UobjDIVERSOCOR.pas',
  UrelPedidoPersiana in 'UrelPedidoPersiana.pas' {QRpedidoPersiana: TQuickRep},
  UEscolheCor in 'UEscolheCor.pas' {FEscolheCor},
  UEscolheOpcoesPersiana in 'UEscolheOpcoesPersiana.pas' {FEscolheOpcoesPersiana},
  UobjBAIRRO in 'UobjBAIRRO.pas',
  UBAIRRO in 'UBAIRRO.pas' {FBAIRRO},
  UobjRUA in 'UobjRUA.pas',
  URUA in 'URUA.pas' {FRUA},
  UobjCIDADE in 'UobjCIDADE.pas',
  UCIDADE in 'UCIDADE.pas' {FCIDADE},
  UobjVidro_EP in 'UobjVidro_EP.pas',
  UENTRADAPRODUTOS in 'UENTRADAPRODUTOS.pas' {FENTRADAPRODUTOS},
  UobjDiverso_EP in 'UobjDiverso_EP.pas',
  UobjFERRAGEM_EP in 'UobjFERRAGEM_EP.pas',
  UobjKitBox_EP in 'UobjKitBox_EP.pas',
  UobjPerfilado_EP in 'UobjPerfilado_EP.pas',
  UobjPERSIANA_EP in 'UobjPERSIANA_EP.pas',
  UobjObjetosEntrada in 'UobjObjetosEntrada.pas',
  UobjENDERECOCOMPREDE in 'UobjENDERECOCOMPREDE.pas',
  UENDERECOCOMPREDE in 'UENDERECOCOMPREDE.pas' {FENDERECOCOMPREDE},
  UobjDIVERSO_PROJ in 'UobjDIVERSO_PROJ.pas',
  UobjLOTERETORNOCOBRANCA in '..\..\Compartilhados\boleto\UobjLOTERETORNOCOBRANCA.pas',
  ULOTERETORNOCOBRANCA in '..\..\Compartilhados\boleto\ULOTERETORNOCOBRANCA.pas' {FLOTERETORNOCOBRANCA},
  UobjTRANSPORTADORA in 'UobjTRANSPORTADORA.pas',
  UTRANSPORTADORA in 'UTRANSPORTADORA.pas' {FTRANSPORTADORA},
  UROMANEIO in 'UROMANEIO.pas' {FROMANEIO},
  UobjROMANEIO in 'UobjROMANEIO.pas',
  UPEDIDOPROJETOROMANEIO in 'UPEDIDOPROJETOROMANEIO.pas' {FPEDIDOPROJETOROMANEIO},
  UobjPEDIDOPROJETOROMANEIO in 'UobjPEDIDOPROJETOROMANEIO.pas',
  UrelPedidoControleEntrega in 'UrelPedidoControleEntrega.pas' {QRpedidoControleEntrega: TQuickRep},
  UrelPedidoCompacto in 'UrelPedidoCompacto.pas' {QRpedidoCompacto: TQuickRep},
  UrelPedido in 'UrelPedido.pas' {QRpedido: TQuickRep},
  UobjOCORRENCIARETORNOCOBRANCA in '..\..\Compartilhados\boleto\UobjOCORRENCIARETORNOCOBRANCA.pas',
  UobjREGISTRORETORNOCOBRANCA in '..\..\Compartilhados\boleto\UobjREGISTRORETORNOCOBRANCA.pas',
  UOCORRENCIARETORNOCOBRANCA in '..\..\Compartilhados\boleto\UOCORRENCIARETORNOCOBRANCA.pas' {FOCORRENCIARETORNOCOBRANCA},
  UREGISTRORETORNOCOBRANCA in '..\..\Compartilhados\boleto\UREGISTRORETORNOCOBRANCA.pas' {FREGISTRORETORNOCOBRANCA},
  UcargoFuncionario in 'UcargoFuncionario.pas' {FcargoFuncionario},
  UobjCmC7 in '..\..\Financeiro\UobjCmC7.pas',
  UacertaObservacaoPendencia in '..\..\Financeiro\UacertaObservacaoPendencia.pas' {FAcertaObservacaoPendencia},
  UobjVersaoSistema in '..\..\Compartilhados\UobjVersaoSistema.pas',
  UobjFUNCIONARIOFOLHAPAGAMENTO in 'Comissao_FolhaPagamento\UobjFUNCIONARIOFOLHAPAGAMENTO.pas',
  UADIANTAMENTOFUNCIONARIO in 'Comissao_FolhaPagamento\UADIANTAMENTOFUNCIONARIO.pas' {FADIANTAMENTOFUNCIONARIO},
  UCOLOCADOR in 'Comissao_FolhaPagamento\UCOLOCADOR.pas' {FCOLOCADOR},
  UCOMISSAOVENDEDORES in 'Comissao_FolhaPagamento\UCOMISSAOVENDEDORES.pas' {FCOMISSAOVENDEDORES},
  UFOLHAPAGAMENTO in 'Comissao_FolhaPagamento\UFOLHAPAGAMENTO.pas' {FFOLHAPAGAMENTO},
  UFUNCIONARIOFOLHAPAGAMENTO in 'Comissao_FolhaPagamento\UFUNCIONARIOFOLHAPAGAMENTO.pas' {FFUNCIONARIOFOLHAPAGAMENTO},
  UobjADIANTAMENTOFUNCIONARIO in 'Comissao_FolhaPagamento\UobjADIANTAMENTOFUNCIONARIO.pas',
  UobjCOLOCADOR in 'Comissao_FolhaPagamento\UobjCOLOCADOR.pas',
  UobjCOMISSAOVENDEDORES in 'Comissao_FolhaPagamento\UobjCOMISSAOVENDEDORES.pas',
  UobjFAIXADESCONTO_COMISSAO in 'Comissao_FolhaPagamento\UobjFAIXADESCONTO_COMISSAO.pas',
  UobjFOLHAPAGAMENTO in 'Comissao_FolhaPagamento\UobjFOLHAPAGAMENTO.pas',
  UFAIXADESCONTO in 'Comissao_FolhaPagamento\UFAIXADESCONTO.pas' {FFAIXADESCONTO},
  UFuncionarios in 'Comissao_FolhaPagamento\UFuncionarios.pas' {FFuncionarios},
  UVENDEDOR in 'Comissao_FolhaPagamento\UVENDEDOR.pas' {FVENDEDOR},
  UobjCargoFuncionario in 'Comissao_FolhaPagamento\UobjCargoFuncionario.pas',
  UobjFAIXADESCONTO in 'Comissao_FolhaPagamento\UobjFAIXADESCONTO.pas',
  UobjFuncionarios in 'Comissao_FolhaPagamento\UobjFuncionarios.pas',
  UobjVENDEDOR in 'Comissao_FolhaPagamento\UobjVENDEDOR.pas',
  UConsultaRapida in 'UConsultaRapida.pas' {FConsultaRapida},
  UobjfolhaPagamentoObjetos in 'Comissao_FolhaPagamento\UobjfolhaPagamentoObjetos.pas',
  UCOMISSAO_ADIANT_FUNCFOLHA in 'Comissao_FolhaPagamento\UCOMISSAO_ADIANT_FUNCFOLHA.pas' {FCOMISSAO_ADIANT_FUNCFOLHA},
  UCOMISSAOCOLOCADOR in 'Comissao_FolhaPagamento\UCOMISSAOCOLOCADOR.pas' {FCOMISSAOCOLOCADOR},
  UobjCOMISSAO_ADIANT_FUNCFOLHA in 'Comissao_FolhaPagamento\UobjCOMISSAO_ADIANT_FUNCFOLHA.pas',
  UobjCOMISSAOCOLOCADOR in 'Comissao_FolhaPagamento\UobjCOMISSAOCOLOCADOR.pas',
  UDescansoSemanalRemunerado in 'UDescansoSemanalRemunerado.pas' {FDescansoSemanalRemunerado},
  UobjDescontoContribuicaoSindical in 'UobjDescontoContribuicaoSindical.pas',
  UobjFAIXADESCONTOINSS in 'Comissao_FolhaPagamento\UobjFAIXADESCONTOINSS.pas',
  UFAIXADESCONTOINSS in 'Comissao_FolhaPagamento\UFAIXADESCONTOINSS.pas' {FFAIXADESCONTOINSS},
  UobjFAIXADESCONTOIRPF in 'Comissao_FolhaPagamento\UobjFAIXADESCONTOIRPF.pas',
  UFAIXADESCONTOIRPF in 'Comissao_FolhaPagamento\UFAIXADESCONTOIRPF.pas' {FFAIXADESCONTOIRPF},
  UData in 'UData.pas' {Fdata},
  UobjFERRAGEM_PP in 'UobjFERRAGEM_PP.pas',
  UobjPERFILADO_PP in 'UobjPERFILADO_PP.pas',
  UobjVIDRO_PP in 'UobjVIDRO_PP.pas',
  UobjKITBOX_PP in 'UobjKITBOX_PP.pas',
  UobjSERVICO_PP in 'UobjSERVICO_PP.pas',
  UobjPERSIANA_PP in 'UobjPERSIANA_PP.pas',
  UobjDIVERSO_PP in 'UobjDIVERSO_PP.pas',
  UobjMEDIDAS_PROJ_PEDIDO in 'UobjMEDIDAS_PROJ_PEDIDO.pas',
  UobjMEDIDAS_PROJ in 'UobjMEDIDAS_PROJ.pas',
  UobjPEDIDO_PROJ in 'UobjPEDIDO_PROJ.pas',
  UobjPEDIDO in 'UobjPEDIDO.pas',
  UobjSALARIOFAMILIA in 'Comissao_FolhaPagamento\UobjSALARIOFAMILIA.pas',
  USALARIOFAMILIA in 'Comissao_FolhaPagamento\USALARIOFAMILIA.pas' {FSALARIOFAMILIA},
  URecuperaBackup in 'URecuperaBackup.pas' {FrecuperaBackup},
  URelTXT in '..\..\Compartilhados\URelTXT.pas' {FrelTXT},
  UobjHORAEXTRA in 'UobjHORAEXTRA.pas',
  UHORAEXTRA in 'UHORAEXTRA.pas' {FHORAEXTRA},
  UobjARQUITETO in 'UobjARQUITETO.pas',
  UARQUITETO in 'UARQUITETO.pas' {FARQUITETO},
  UobjVERSAOEXECUTAVEL in '..\..\Compartilhados\UobjVERSAOEXECUTAVEL.pas',
  UObjNotaFiscal in 'UObjNotaFiscal.pas',
  UNotaFiscal in 'UNotaFiscal.pas' {FNotaFiscal},
  UCFOP in 'UCFOP.pas' {FCFOP},
  UObjCFOP in 'UObjCFOP.pas',
  UNOTAFISCALCFOP in 'UNOTAFISCALCFOP.pas' {FNOTAFISCALCFOP},
  UObjNOTAFISCALCFOP in 'UObjNOTAFISCALCFOP.pas',
  UobjFERRAGEM_NF in 'UobjFERRAGEM_NF.pas',
  UobjPERFILADO_NF in 'UobjPERFILADO_NF.pas',
  UobjKITBOX_NF in 'UobjKITBOX_NF.pas',
  UobjPERSIANA_NF in 'UobjPERSIANA_NF.pas',
  UobjDIVERSO_NF in 'UobjDIVERSO_NF.pas',
  UobjPedidoObjetos in 'UobjPedidoObjetos.pas',
  UrelNotaFiscalRdPrint in 'UrelNotaFiscalRdPrint.pas' {FrelNotaFiscalRdPrint},
  UFornecedor2 in 'UFornecedor2.pas' {Ffornecedor2},
  Ucliente2 in 'Ucliente2.pas' {Fcliente2},
  UtrocaMaterialpedido in 'UtrocaMaterialpedido.pas' {FtrocaMaterialpedido},
  UchequeDevolvido in '..\..\Financeiro\UchequeDevolvido.pas' {FCHEQUEDEVOLVIDO},
  UobjCHEQUEDEVOLVIDO in '..\..\Financeiro\UobjCHEQUEDEVOLVIDO.pas',
  UFrLancachequeDevolvido in '..\..\Financeiro\UFrLancachequeDevolvido.pas' {FrLancaChequeDevolvido: TFrame},
  UobjENTRADAPRODUTOS in 'UobjENTRADAPRODUTOS.pas',
  UEmiteCartaCobranca in '..\..\Financeiro\UEmiteCartaCobranca.pas' {FEmiteCartaCobranca},
  URefazContabilidadeEntradaeSaida in 'URefazContabilidadeEntradaeSaida.pas' {FRefazContabilidadeEntradaeSaida},
  UchequesDevolvidos in '..\..\Financeiro\UchequesDevolvidos.pas' {FchequesDevolvidos},
  UrelatoriosItensNF in 'UrelatoriosItensNF.pas' {FrelatoriosItensNF},
  UreajustaMateriais in 'UreajustaMateriais.pas' {FreajustaMateriais},
  UinterageExcel in '..\..\Compartilhados\UinterageExcel.pas' {FInterageExcel},
  UlancaBoletoVariasPendencias in '..\..\Financeiro\UlancaBoletoVariasPendencias.pas' {FlancaBoletoVariasPendencias},
  UobjDescansoSemanalRemunerado in 'UobjDescansoSemanalRemunerado.pas',
  UobjGratificacao in 'UobjGratificacao.pas',
  UDescontoContribuicaoSindical in 'UDescontoContribuicaoSindical.pas' {FDescontoContribuicaoSindical},
  UGratificacao in 'UGratificacao.pas' {Fgratificacao},
  UfrScriptSql in '..\..\Compartilhados\UfrScriptSql.pas' {FrScriptSql: TFrame},
  UvisualizaTabela in '..\..\Compartilhados\UvisualizaTabela.pas' {Fvisualizatabela},
  UrefazContabilidadeChequeDevolvido in 'UrefazContabilidadeChequeDevolvido.pas' {FrefazContabilidadeChequeDevolvido},
  UPagamento_e_geracao_emprestimo in '..\..\Financeiro\UPagamento_e_geracao_emprestimo.pas' {FPagamento_e_geracao_emprestimo},
  UrelPrevisaoFinanceira in '..\..\Financeiro\UrelPrevisaoFinanceira.pas' {FrelPrevisaoFinanceira},
  UescolheImagemBotao in 'UescolheImagemBotao.pas' {FescolheImagemBotao},
  UconfrontoContabilidade in '..\..\Financeiro\UconfrontoContabilidade.pas' {FconfrontoContabilidade},
  UobjSUBCONTAGERENCIAL in '..\..\Financeiro\UobjSUBCONTAGERENCIAL.pas',
  UEmpresa in 'UEmpresa.pas' {FEMPRESA},
  UObjEmpresa in 'UObjEmpresa.pas',
  USUBCONTAGERENCIAL in '..\..\Financeiro\USUBCONTAGERENCIAL.pas' {FSUBCONTAGERENCIAL},
  URelatorioEstoque in 'URelatorioEstoque.pas' {FrelatorioEstoque},
  UobjCREDITOVALORES in '..\..\Financeiro\UobjCREDITOVALORES.pas',
  UCREDITOVALORES in '..\..\Financeiro\UCREDITOVALORES.pas' {FCREDITOVALORES},
  UPEDIDOCOMPRA in 'UPEDIDOCOMPRA.pas' {FPEDIDOCOMPRA},
  UobjPEDIDOCOMPRA in 'UobjPEDIDOCOMPRA.pas',
  UobjPEDIDOPROJETOPEDIDOCOMPRA in 'UobjPEDIDOPROJETOPEDIDOCOMPRA.pas',
  ULOTECOMISSAO in 'ULOTECOMISSAO.pas' {FLOTECOMISSAO},
  UobjLOTECOMISSAO in 'UobjLOTECOMISSAO.pas',
  UCOMPONENTE_PP in 'UCOMPONENTE_PP.pas' {FCOMPONENTE_PP},
  UobjCOMPONENTE_PP_pedido in 'UobjCOMPONENTE_PP_pedido.pas',
  UobjCOMPONENTE_PP in 'UobjCOMPONENTE_PP.pas',
  Base64 in '..\..\Compartilhados\MD5\Base64.pas',
  DCPconst in '..\..\Compartilhados\MD5\DCPconst.pas',
  DCPcrypt in '..\..\Compartilhados\MD5\DCPcrypt.pas',
  IDEA in '..\..\Compartilhados\MD5\IDEA.pas',
  Md5 in '..\..\Compartilhados\MD5\Md5.pas',
  Sha1 in '..\..\Compartilhados\MD5\Sha1.pas',
  U_Cipher in '..\..\Compartilhados\MD5\U_Cipher.pas',
  UmostraMEnsagem in '..\..\Compartilhados\UmostraMEnsagem.pas' {Fmostramensagem},
  UobjREQUISICAOSENHA in '..\..\Compartilhados\UobjREQUISICAOSENHA.pas',
  UUtils in '..\..\Compartilhados\UUtils.pas',
  UimportaPedido in 'UimportaPedido.pas' {FimportaPedido},
  UobjPERMISSOESEXTRAS in '..\..\Compartilhados\UobjPERMISSOESEXTRAS.pas',
  UrelboletoSemRegistroModelo2 in '..\..\Compartilhados\boleto\UrelboletoSemRegistroModelo2.pas' {FRelBoletoSemregistroModelo2: TQuickRep},
  Uajustes in 'Uajustes.pas' {Fajustes},
  UFLUXOCAIXA in 'UFLUXOCAIXA.pas' {FFLUXOCAIXA},
  UobjCONTAGERENCIALFLUXOCAIXA in 'UobjCONTAGERENCIALFLUXOCAIXA.pas',
  UobjFLUXOCAIXA in 'UobjFLUXOCAIXA.pas',
  umensagem in '..\..\Compartilhados\umensagem.pas' {fmensagem},
  UAlteraPrecoPErsiana in 'UAlteraPrecoPErsiana.pas' {FAlteraPrecoPersiana},
  UalteraPrecoDiverso in 'UalteraPrecoDiverso.pas' {FalteraPrecoDiverso},
  UalteraPrecoFerragem in 'UalteraPrecoFerragem.pas' {FalteraPrecoFerragem},
  UAlteraPrecoKitbox in 'UAlteraPrecoKitbox.pas' {FAlteraPrecoKitbox},
  UAlteraPrecoPerfilado in 'UAlteraPrecoPerfilado.pas' {FAlteraPrecoPerfilado},
  UAlteraPrecoVidro in 'UAlteraPrecoVidro.pas' {FAlteraPrecoVidro},
  UCADASTRARELATORIOPERSONALIZADO in '..\..\Compartilhados\UCADASTRARELATORIOPERSONALIZADO.pas' {FCADASTRARELATORIOPERSONALIZADO},
  UobjFILTRORELPERSONALIZADO in '..\..\Compartilhados\UobjFILTRORELPERSONALIZADO.pas',
  UobjFILTRORELPERSONALIZADOextendido in '..\..\Compartilhados\UobjFILTRORELPERSONALIZADOextendido.pas',
  UobjORDERBYRELPERSONALIZADO in '..\..\Compartilhados\UobjORDERBYRELPERSONALIZADO.pas',
  UobjORDERBYRELPERSONALIZADOextendido in '..\..\Compartilhados\UobjORDERBYRELPERSONALIZADOextendido.pas',
  UObjrelatorioPersonalizado in '..\..\Compartilhados\UObjrelatorioPersonalizado.pas',
  UobjRelatorioPersonalizadoExtendido in '..\..\Compartilhados\UobjRelatorioPersonalizadoExtendido.pas',
  UobjSUBMENURELPERSONALIZADO in '..\..\Compartilhados\UobjSUBMENURELPERSONALIZADO.pas',
  USUBMENURELPERSONALIZADO in '..\..\Compartilhados\USUBMENURELPERSONALIZADO.pas' {FSUBMENURELPERSONALIZADO},
  UobjArquivoIni in '..\..\Compartilhados\UobjArquivoIni.pas',
  UescolheTipoCamporelatorio in '..\..\Compartilhados\UescolheTipoCamporelatorio.pas' {FescolheTipoCamporelatorio},
  UopcoesListCheckBox in '..\..\Compartilhados\UopcoesListCheckBox.pas' {FopcoesListCheckBox},
  UobjMENURELPERSONALIZADO in '..\..\Compartilhados\UobjMENURELPERSONALIZADO.pas',
  UMENURELPERSONALIZADO in '..\..\Compartilhados\UMENURELPERSONALIZADO.pas' {FMENURELPERSONALIZADO},
  UrelatorioPersonalizado in '..\..\Compartilhados\UrelatorioPersonalizado.pas',
  UConfiguracoesgeraisRelatorioPersonalizado in '..\..\Compartilhados\UConfiguracoesgeraisRelatorioPersonalizado.pas' {FConfiguracoesgeraisRelatorioPersonalizado},
  UvisualizaCamposRelatorioPersonalizado in '..\..\Compartilhados\UvisualizaCamposRelatorioPersonalizado.pas' {FvisualizaCamposRelatorioPersonalizado},
  UFILTRORELPERSONALIZADO in '..\..\Compartilhados\UFILTRORELPERSONALIZADO.pas' {Ffiltrorelpersonalizado},
  UImportaTabela in '..\..\Compartilhados\UImportaTabela.pas',
  UfrImportaFirebird in '..\..\Compartilhados\UfrImportaFirebird.pas' {FrImportaFirebird: TFrame},
  UfrImportaTXT in '..\..\Compartilhados\UfrImportaTXT.pas' {FrImportaTXT: TFrame},
  UobjImporta in 'UobjImporta.pas',
  UarquivoIni in '..\..\Compartilhados\UarquivoIni.pas' {FarquivoIni},
  Ucontafacil in '..\..\Financeiro\Ucontafacil.pas' {FcontaFacil},
  UFinalizaComissaoColocador in 'UFinalizaComissaoColocador.pas' {FFinalizaComissaoColocador},
  uobjencerrasistema in '..\..\Compartilhados\uobjencerrasistema.pas',
  UNOVIDADE in '..\..\Compartilhados\UNOVIDADE.pas' {FNOVIDADE},
  UobjNOVIDADE in '..\..\Compartilhados\UobjNOVIDADE.pas',
  UobjICONES in '..\..\Compartilhados\UobjICONES.pas',
  UICONES in '..\..\Compartilhados\UICONES.pas' {FICONES},
  UescolheIcones in '..\..\Compartilhados\UescolheIcones.pas' {FescolheIcones},
  UpopupIcone in '..\..\Compartilhados\UpopupIcone.pas' {FpopupIcone},
  UinputQuery in '..\..\Compartilhados\UinputQuery.pas' {FinputQuery},
  UobjNOVIDADEUSUARIO in '..\..\Compartilhados\UobjNOVIDADEUSUARIO.pas',
  UrelpersonalizadoReportBuilder in '..\..\Compartilhados\UrelpersonalizadoReportBuilder.pas' {FrelpersonalizadoReportBuilder},
  URELPERSREPORTBUILDER in '..\..\Compartilhados\URELPERSREPORTBUILDER.pas' {FRELPERSREPORTBUILDER},
  UobjRELPERSREPORTBUILDER in '..\..\Compartilhados\UobjRELPERSREPORTBUILDER.pas',
  UComponentesNfe in '..\..\Compartilhados\NFE\UComponentesNfe.pas' {FComponentesNfe},
  uobjgeranfe in 'uobjgeranfe.pas',
  UConfDuplicata in '..\..\Financeiro\UConfDuplicata.pas' {FConfDuplicata},
  UConfDuplicata02 in '..\..\Financeiro\UConfDuplicata02.pas' {FConfDuplicata02},
  UrelBoleto_PI in '..\..\Financeiro\UrelBoleto_PI.pas' {FrelBoleto_PI},
  UobjREQUISICAOSENHALOCAL in '..\..\Compartilhados\UobjREQUISICAOSENHALOCAL.pas',
  UIMPOSTO_ICMS in 'UIMPOSTO_ICMS.pas' {FIMPOSTO_ICMS},
  UobjIMPOSTO_ICMS in 'UobjIMPOSTO_ICMS.pas',
  UobjTIPOCLIENTE in 'UobjTIPOCLIENTE.pas',
  UTIPOCLIENTE in 'UTIPOCLIENTE.pas' {FTIPOCLIENTE},
  UobjVIDRO_ICMS in 'UobjVIDRO_ICMS.pas',
  UFRImposto_ICMS in 'UFRImposto_ICMS.pas' {FRImposto_ICMS: TFrame},
  UobjMaterial_ICMS in 'UobjMaterial_ICMS.pas',
  Uobjferragem_icms in 'Uobjferragem_icms.pas',
  UOBJPERFILADO_ICMS in 'UOBJPERFILADO_ICMS.pas',
  uobjdiverso_icms in 'uobjdiverso_icms.pas',
  UOBJKITBOX_ICMS in 'UOBJKITBOX_ICMS.pas',
  uobjpersiana_icms in 'uobjpersiana_icms.pas',
  UobjMaterial_NF in 'UobjMaterial_NF.pas',
  UobjVIDRO_NF in 'UobjVIDRO_NF.pas',
  USITUACAOTRIBUTARIA_COFINS in 'USITUACAOTRIBUTARIA_COFINS.pas' {FSITUACAOTRIBUTARIA_COFINS},
  UobjSITUACAOTRIBUTARIA_COFINS in 'UobjSITUACAOTRIBUTARIA_COFINS.pas',
  UIMPOSTO_IPI in 'UIMPOSTO_IPI.pas' {FIMPOSTO_IPI},
  UobjIMPOSTO_IPI in 'UobjIMPOSTO_IPI.pas',
  UobjSITUACAOTRIBUTARIA_PIS in 'UobjSITUACAOTRIBUTARIA_PIS.pas',
  UobjSITUACAOTRIBUTARIA_IPI in 'UobjSITUACAOTRIBUTARIA_IPI.pas',
  USITUACAOTRIBUTARIA_PIS in 'USITUACAOTRIBUTARIA_PIS.pas' {FSITUACAOTRIBUTARIA_PIS},
  USITUACAOTRIBUTARIA_IPI in 'USITUACAOTRIBUTARIA_IPI.pas' {FSITUACAOTRIBUTARIA_IPI},
  UobjIMPOSTO_COFINS in 'UobjIMPOSTO_COFINS.pas',
  UIMPOSTO_COFINS in 'UIMPOSTO_COFINS.pas' {FIMPOSTO_COFINS},
  UIMPOSTO_PIS in 'UIMPOSTO_PIS.pas' {FIMPOSTO_PIS},
  UobjIMPOSTO_PIS in 'UobjIMPOSTO_PIS.pas',
  UIE_ST_ESTADO in 'UIE_ST_ESTADO.pas' {FIE_ST_ESTADO},
  UobjIE_ST_ESTADO in 'UobjIE_ST_ESTADO.pas',
  ufrmStatus in '..\..\Compartilhados\NFE\ufrmStatus.pas' {frmStatus},
  UNFE in 'UNFE.pas' {FNFE},
  UOPERACAONF in 'UOPERACAONF.pas' {FOPERACAONF},
  UobjOPERACAONF in 'UobjOPERACAONF.pas',
  UMostraValorMoedas in '..\..\Financeiro\UMostraValorMoedas.pas' {FMostraValorMoedas},
  UobjCOTACAOMOEDA in '..\..\Financeiro\UobjCOTACAOMOEDA.pas',
  UobjMOEDA in '..\..\Financeiro\UobjMOEDA.pas',
  UMOEDA in '..\..\Financeiro\UMOEDA.pas' {FMOEDA},
  UFrMoedas in '..\..\Financeiro\UFrMoedas.pas' {FrMoedas: TFrame},
  UobjRequisicaoLara in '..\..\Compartilhados\RequisicaoLara\UobjRequisicaoLara.pas',
  UBOLETOINSTRUCOES in '..\..\Compartilhados\boleto\UBOLETOINSTRUCOES.pas' {FBOLETOINSTRUCOES},
  UGeraArquivoRemessa in '..\..\Compartilhados\boleto\UGeraArquivoRemessa.pas' {FGeraArquivoRemessa},
  UHistoricoBoleto in '..\..\Compartilhados\boleto\UHistoricoBoleto.pas' {FHistoricoBoleto},
  UINSTRUCAO in '..\..\Compartilhados\boleto\UINSTRUCAO.pas' {FINSTRUCAO},
  UobjBOLETOINSTRUCOES in '..\..\Compartilhados\boleto\UobjBOLETOINSTRUCOES.pas',
  UobjCobreBem in '..\..\Compartilhados\boleto\UobjCobreBem.pas',
  UobjINSTRUCAO in '..\..\Compartilhados\boleto\UobjINSTRUCAO.pas',
  UobjARQUIVOREMESSARETORNO in '..\..\Financeiro\UobjARQUIVOREMESSARETORNO.pas',
  UARQUIVOREMESSARETORNO in '..\..\Financeiro\UARQUIVOREMESSARETORNO.pas' {FARQUIVOREMESSARETORNO},
  UobjIntegracao in 'UobjIntegracao.pas',
  UEscolheMateriaisReplicar in 'UEscolheMateriaisReplicar.pas' {FEscolheMateriaisReplicar},
  UreprocessaChequeDevolvidoComissao in 'UreprocessaChequeDevolvidoComissao.pas' {FreprocessaChequeDevolvidoComissao},
  Upesquisa in '..\..\Compartilhados\Upesquisa.pas' {Fpesquisa},
  UobjCONFCAMPOSPESQUISA in '..\..\Compartilhados\UobjCONFCAMPOSPESQUISA.pas',
  UCONFCAMPOSPESQUISA in '..\..\Compartilhados\UCONFCAMPOSPESQUISA.pas' {FCONFCAMPOSPESQUISA},
  UConfordemcamposPesquisa in '..\..\Compartilhados\UConfordemcamposPesquisa.pas' {FConfordemcamposPesquisa},
  UobjESTOQUE in 'UobjESTOQUE.pas',
  UTitulo_novo in '..\..\Financeiro\UTitulo_novo.pas' {Ftitulo_novo},
  ULancamentoFR_novo in '..\..\Financeiro\ULancamentoFR_novo.pas' {FRLancamento_novo: TFrame},
  UobjBaixaemcheques in '..\..\Financeiro\UobjBaixaemcheques.pas' {FBaixaEmCheques},
  UescolheNF in 'UescolheNF.pas' {FescolheNF},
  UobjVendas in 'UobjVendas.pas',
  ObjMateriaisVenda in 'ObjMateriaisVenda.pas',
  UPesquisaProjNovo in 'UPesquisaProjNovo.pas' {FpesquisaProjNovo},
  Uobjestado in 'Uobjestado.pas',
  Uestado in 'Uestado.pas' {Festado},
  UModeloDeNF in 'UModeloDeNF.pas' {FModeloDeNF},
  UobjModeloNF in 'UobjModeloNF.pas',
  UpesquisaMenu in '..\..\Compartilhados\UpesquisaMenu.pas' {fPesquisaMenu},
  UnotaFiscalEletronica in 'UnotaFiscalEletronica.pas' {FnotaFiscalEletronica},
  UobjExtraiXML in '..\..\Compartilhados\UobjExtraiXML.pas',
  UobjNFe in 'UobjNFe.pas',
  UerrosNFE in '..\..\Compartilhados\UerrosNFE.pas' {FerrosNFE},
  UmenuNfe in 'UmenuNfe.pas' {FmenuNfe},
  UexecSql in '..\..\Compartilhados\UexecSql.pas' {fExecSql},
  UNfeDigitada in 'UNfeDigitada.pas' {FNfeDigitada},
  UobjPRODNFEDIGITADA in 'UobjPRODNFEDIGITADA.pas',
  UescolheTransportadora in 'UescolheTransportadora.pas' {FEscolheTransportadora},
  uReferenciaNFe in 'uReferenciaNFe.pas' {fReferenciaNFE},
  UOrdemServico in 'UOrdemServico.pas' {FOrdemServico},
  UobjORDEMSERVICO in 'UobjORDEMSERVICO.pas',
  UobjSERVICOSOS in 'UobjSERVICOSOS.pas',
  UobjMATERIAISOS in 'UobjMATERIAISOS.pas',
  UobjVIDROSERVICOOS in 'UobjVIDROSERVICOOS.pas',
  UobjMATERIAISTROCA in 'UobjMATERIAISTROCA.pas',
  UobjLANCAMENTOCREDITO in '..\..\Financeiro\UobjLANCAMENTOCREDITO.pas',
  UobjMATERIAISDEVOLUCAO in 'UobjMATERIAISDEVOLUCAO.pas',
  ULancamentosFC in '..\..\Compartilhados\ULancamentosFC.pas' {FLancamentosFrentedeCaixa},
  UobjLancamentosFCSafira in '..\..\Compartilhados\UobjLancamentosFCSafira.pas',
  UFORMASPAGAMENTO_FC in '..\..\Compartilhados\UFORMASPAGAMENTO_FC.pas' {FFORMASPAGAMENTO_FC},
  UobjLancamentosFC in '..\..\Compartilhados\UobjLancamentosFC.pas',
  UobjLancamentosFCAmanda in '..\..\Compartilhados\UobjLancamentosFCAmanda.pas',
  ULancCredito in 'ULancCredito.pas' {FlancCredito},
  UobjCREDITO in 'UobjCREDITO.pas',
  UCRT in '..\..\Compartilhados\UCRT.pas' {FCRT},
  UobjCRT in '..\..\Compartilhados\UobjCRT.pas',
  UCSOSN in '..\..\Compartilhados\UCSOSN.pas' {FCSOSN},
  UobjCSOSN in '..\..\Compartilhados\UobjCSOSN.pas',
  UrelPedidoCompraPadrao in 'UrelPedidoCompraPadrao.pas' {TQRPedidoCompraPadrao: TQuickRep},
  UobjPROJETO_ICMS in 'UobjPROJETO_ICMS.pas',
  CodigoFigura in 'CodigoFigura.pas',
  Ferragens in 'Ferragens.pas' {Form9},
  UMedidasFuros in 'UMedidasFuros.pas' {FmedidasFuros},
  UobjCORTECOMPONENTE_PROJ in 'UobjCORTECOMPONENTE_PROJ.pas',
  UobjPEDIDOCOMPRAMATERIAISAVULSO in 'UobjPEDIDOCOMPRAMATERIAISAVULSO.pas',
  UrelPedidoCompraMater in 'UrelPedidoCompraMater.pas' {TQRUrelPedidoCompraMater: TQuickRep},
  UAjuda in 'UAjuda.pas' {FAjuda},
  UOrdemCorte in 'UOrdemCorte.pas' {FOrdemCorte},
  UOrdemMedicao in 'UOrdemMedicao.pas' {FOrdemMedicao},
  UobjORDEMMEDICAO in 'UobjORDEMMEDICAO.pas',
  UErrosDesativarMateriais in 'UErrosDesativarMateriais.pas' {FErrosDesativarMaterial},
  UCobrador in 'UCobrador.pas' {FCobrador},
  UrelControleEntrega in 'UrelControleEntrega.pas' {QrControleEntrega: TQuickRep},
  UCadastroAjuda in '..\..\Compartilhados\UCadastroAjuda.pas' {FCadastroAjuda},
  UobjAJUDA in '..\..\Compartilhados\UobjAJUDA.pas',
  UmostraStringGrid in 'UmostraStringGrid.pas' {FmostraStringGrid},
  UordemInstalacao in 'UordemInstalacao.pas' {FordemInstalacao},
  UobjORDEMINSTALACAO in 'UobjORDEMINSTALACAO.pas',
  UobjROMANEIOSORDEMINSTALACAO in 'UobjROMANEIOSORDEMINSTALACAO.pas',
  UAgendaInstalacao in 'UAgendaInstalacao.pas' {FAgendaInstalacao},
  UVendaRapida in 'UVendaRapida.pas' {FVendaRapida},
  UobjREAGENDAMENTOINSTALACAO in 'UobjREAGENDAMENTOINSTALACAO.pas',
  UobjCANCELAMENTOINSTALACAO in 'UobjCANCELAMENTOINSTALACAO.pas',
  UMapa in 'UMapa.pas' {FMapa},
  UCONTADOR in 'UCONTADOR.pas' {rtotin},
  UobjCONTADOR in 'UobjCONTADOR.pas',
  UspedFiscal in 'UspedFiscal.pas' {fSpedFiscal},
  UCorrecaoCFOPEntradaProdutos in 'UCorrecaoCFOPEntradaProdutos.pas' {FCorrecaoCFOPEntradaProdutos},
  uCorrecaoCFOPSaida in 'uCorrecaoCFOPSaida.pas' {fCorrecaoCFOPSaida},
  UUNIDADEMEDIDA in 'UUNIDADEMEDIDA.pas' {FUNIDADEMEDIDA},
  UobjUNIDADEMEDIDA in 'UobjUNIDADEMEDIDA.pas',
  UComponentesSped in '..\..\Compartilhados\sped\UComponentesSped.pas' {FCOmponentesSped: TDataModule},
  UobjSpedFiscal in 'UobjSpedFiscal.pas',
  UobjMATERIAISENTRADA in 'UobjMATERIAISENTRADA.pas',
  UOBRIGACOESICMS in '..\..\Compartilhados\UOBRIGACOESICMS.pas' {FOBRIGACOESICMS},
  USintegra in 'USintegra.pas' {FSintegra},
  UAlteraCoresPProjeto in 'UAlteraCoresPProjeto.pas' {FAlterarCoresPProjetos},
  UAcertaUnidades in 'UAcertaUnidades.pas' {FAcertaUnidades},
  UobjRELACAOMEDIDAS in 'UobjRELACAOMEDIDAS.pas',
  UAcertoUnidadeEntrada in 'UAcertoUnidadeEntrada.pas' {FAcertoUnidadeEntrada},
  UVisulizarProjetos in 'UVisulizarProjetos.pas' {FVisulizarProjetos},
  UobjSpedFiscal_Novo in '..\..\Compartilhados\UobjSpedFiscal_Novo.pas',
  UProducao in 'UProducao.pas' {FProducao},
  UVisualizaStatusProducao in 'UVisualizaStatusProducao.pas' {FVisualizaStatusProducao},
  UvideosAjuda in 'UvideosAjuda.pas' {Fvideos},
  UMostraEstoque in 'UMostraEstoque.pas' {FMostraEstoque},
  UobjTransmiteNFE in '..\..\Compartilhados\NFE\UobjTransmiteNFE.pas',
  UobjCOBRDUPLICATA in '..\..\Compartilhados\UobjCOBRDUPLICATA.pas',
  UobjCOBRFATURA in '..\..\Compartilhados\UobjCOBRFATURA.pas',
  UItensCarrinho in 'UItensCarrinho.pas' {FItensCarrinhos},
  uCartaCorrecao in 'uCartaCorrecao.pas' {fCartaCorrecao},
  UobjCARTACORRECAO in 'UobjCARTACORRECAO.pas',
  UalteraFaturas in '..\..\Compartilhados\NFE\UalteraFaturas.pas' {fAlteraFaturas},
  UConfiguracaoSite in 'UConfiguracaoSite.pas' {FConfiguracaoSite},
  UobjConfiguracaosite in 'UobjConfiguracaosite.pas',
  UobjOBRIGACOESICMS in '..\..\Compartilhados\UobjOBRIGACOESICMS.pas',
  UobjCODIGORECEITAESTADUAL in '..\..\Compartilhados\UobjCODIGORECEITAESTADUAL.pas',
  UGeraRomaneioPedido in 'UGeraRomaneioPedido.pas' {FGeraRomaneioPedido},
  Uobjvencimento in '..\..\Compartilhados\Uobjvencimento.pas',
  UobjNCM in '..\..\Compartilhados\UobjNCM.pas',
  UNCM in '..\..\Compartilhados\UNCM.pas' {FNCM},
  UDevolucaoNFE in 'UDevolucaoNFE.pas' {fDevolucaoNFE},
  UobjDevolucaoNfe in 'UobjDevolucaoNfe.pas',
  UacertaImposto in '..\..\Compartilhados\NFE\UacertaImposto.pas' {fAcertaImposto},
  uAcertaImpostoSafira in 'uAcertaImpostoSafira.pas' {fAcertaImpostoSafira},
  mdGraphicText in '..\..\Compartilhados\mdGraphicText.pas',
  UPedidoonline in 'UPedidoonline.pas' {FPEDIDOONLINE},
  uRecuperaDuplicidade in '..\..\Compartilhados\NFE\uRecuperaDuplicidade.pas' {formRecuperaDuplicidade},
  uEnderecoEntrega in '..\..\Compartilhados\NFE\uEnderecoEntrega.pas' {FEnderecoEntrega},
  uTextoFormatado in '..\..\Compartilhados\uTextoFormatado.pas' {fTextoFormatado},
  UobjCEST in '..\..\Compartilhados\UobjCEST.pas',
  UCEST in '..\..\Compartilhados\UCEST.pas' {FCEST},
  UescolheStringGrid in '..\..\Compartilhados\UescolheStringGrid.pas' {FEscolheStringGrid},
  UobjFORMASPAGAMENTO_FC in '..\..\Compartilhados\UobjFORMASPAGAMENTO_FC.pas',
  GIFImage in '..\..\Compartilhados\GifImage.pas',
  UCODIGORECEITAESTADUAL in '..\..\Compartilhados\UCODIGORECEITAESTADUAL.pas' {FCODIGORECEITAESTADUAL},
  uEspera in '..\..\Compartilhados\uEspera.pas' {fEspera};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TFDataModulo, FDataModulo);
  Application.CreateForm(TFprincipal, Fprincipal);
  Application.CreateForm(TFsobre, Fsobre);
  Application.CreateForm(TFOpcaorel, FOpcaorel);
  Application.CreateForm(TFbackup, Fbackup);
  Application.CreateForm(TFfiltroImp, FfiltroImp);
  Application.CreateForm(TFparametros, Fparametros);
  Application.CreateForm(TFusuarios, Fusuarios);
  Application.CreateForm(TFNiveis, FNiveis);
  Application.CreateForm(TFPermissoesUso, FPermissoesUso);
  Application.CreateForm(TFcalendario, Fcalendario);
  Application.CreateForm(TFreltxtRDPRINT, FreltxtRDPRINT);
  Application.CreateForm(TFRelatorio, FRelatorio);
  Application.CreateForm(TFescolheConvenio, FescolheConvenio);
  Application.CreateForm(TFvalorLanctoLote_REC, FvalorLanctoLote_REC);
  Application.CreateForm(TFdataN, FdataN);
  Application.CreateForm(TFOrigemAnotacoes, FOrigemAnotacoes);
  Application.CreateForm(TFlancaTroco, FlancaTroco);
  Application.CreateForm(TFRelPedidoRdPrint, FRelPedidoRdPrint);
  Application.CreateForm(TFAcertaBoletoaPagar, FAcertaBoletoaPagar);
  Application.CreateForm(TFMenuRelatorios, FMenuRelatorios);
  Application.CreateForm(TFpesquisaProjeto, FpesquisaProjeto);
  Application.CreateForm(TFmostraStringList, FmostraStringList);
  Application.CreateForm(TFcalculaformula_Perfilado, Fcalculaformula_Perfilado);
  Application.CreateForm(TFescolheVidro, FescolheVidro);
  Application.CreateForm(TFcalculaformula_Vidro, Fcalculaformula_Vidro);
  Application.CreateForm(TFmedidas_proj, Fmedidas_proj);
  Application.CreateForm(TQRpedidoPersiana, QRpedidoPersiana);
  Application.CreateForm(TFEscolheCor, FEscolheCor);
  Application.CreateForm(TFEscolheOpcoesPersiana, FEscolheOpcoesPersiana);
  Application.CreateForm(TFLancaNovoLancamento, FLancaNovoLancamento);
  Application.CreateForm(TFConfiguraChequeRDPRINT, FConfiguraChequeRDPRINT);
  Application.CreateForm(TFrelComprovanteCheque, FrelComprovanteCheque);
  Application.CreateForm(TQRpedidoControleEntrega, QRpedidoControleEntrega);
  Application.CreateForm(TQRpedido, QRpedido);
  Application.CreateForm(TFAcertaObservacaoPendencia, FAcertaObservacaoPendencia);
  Application.CreateForm(TFEscolheBoletos, FEscolheBoletos);
  Application.CreateForm(TFescolhePendencias, FescolhePendencias);
  Application.CreateForm(TFMostraBarraProgresso, FMostraBarraProgresso);
  Application.CreateForm(TFdata, Fdata);
  Application.CreateForm(TFrecuperaBackup, FrecuperaBackup);
  Application.CreateForm(TFrelTXT, FrelTXT);
  Application.CreateForm(TFrelNotaFiscalRdPrint, FrelNotaFiscalRdPrint);
  Application.CreateForm(TFfornecedor2, Ffornecedor2);
  Application.CreateForm(TFFuncionarios, FFuncionarios);
  Application.CreateForm(TFcliente2, Fcliente2);
  Application.CreateForm(TFtrocaMaterialpedido, FtrocaMaterialpedido);
  Application.CreateForm(TFCHEQUEDEVOLVIDO, FCHEQUEDEVOLVIDO);
  Application.CreateForm(TFEmiteCartaCobranca, FEmiteCartaCobranca);
  Application.CreateForm(TFRefazContabilidadeEntradaeSaida, FRefazContabilidadeEntradaeSaida);
  Application.CreateForm(TFchequesDevolvidos, FchequesDevolvidos);
  Application.CreateForm(TFreajustaMateriais, FreajustaMateriais);
  Application.CreateForm(TFInterageExcel, FInterageExcel);
  Application.CreateForm(TFlancaBoletoVariasPendencias, FlancaBoletoVariasPendencias);
  Application.CreateForm(TFvisualizatabela, Fvisualizatabela);
  Application.CreateForm(TFrefazContabilidadeChequeDevolvido, FrefazContabilidadeChequeDevolvido);
  Application.CreateForm(TFPagamento_e_geracao_emprestimo, FPagamento_e_geracao_emprestimo);
  Application.CreateForm(TFrelPrevisaoFinanceira, FrelPrevisaoFinanceira);
  Application.CreateForm(TFescolheImagemBotao, FescolheImagemBotao);
  Application.CreateForm(TFconfrontoContabilidade, FconfrontoContabilidade);
  Application.CreateForm(TFEMPRESA, FEMPRESA);
  Application.CreateForm(TFSUBCONTAGERENCIAL, FSUBCONTAGERENCIAL);
  Application.CreateForm(TFrelatorioEstoque, FrelatorioEstoque);
  Application.CreateForm(TFCREDITOVALORES, FCREDITOVALORES);
  Application.CreateForm(TFRelBoletoSemregistro, FRelBoletoSemregistro);
  Application.CreateForm(TFRelBoletoSemregistroModelo2, FRelBoletoSemregistroModelo2);
  Application.CreateForm(TFajustes, Fajustes);
  Application.CreateForm(TFFLUXOCAIXA, FFLUXOCAIXA);
  Application.CreateForm(TFAlteraPrecoPersiana, FAlteraPrecoPersiana);
  Application.CreateForm(TFalteraPrecoDiverso, FalteraPrecoDiverso);
  Application.CreateForm(TFalteraPrecoFerragem, FalteraPrecoFerragem);
  Application.CreateForm(TFAlteraPrecoKitbox, FAlteraPrecoKitbox);
  Application.CreateForm(TFAlteraPrecoPerfilado, FAlteraPrecoPerfilado);
  Application.CreateForm(TFAlteraPrecoVidro, FAlteraPrecoVidro);
  Application.CreateForm(TFCADASTRARELATORIOPERSONALIZADO, FCADASTRARELATORIOPERSONALIZADO);
  Application.CreateForm(TFSUBMENURELPERSONALIZADO, FSUBMENURELPERSONALIZADO);
  Application.CreateForm(TFescolheTipoCamporelatorio, FescolheTipoCamporelatorio);
  Application.CreateForm(TFopcoesListCheckBox, FopcoesListCheckBox);
  Application.CreateForm(TFMENURELPERSONALIZADO, FMENURELPERSONALIZADO);
  Application.CreateForm(TFConfiguracoesgeraisRelatorioPersonalizado, FConfiguracoesgeraisRelatorioPersonalizado);
  Application.CreateForm(TFvisualizaCamposRelatorioPersonalizado, FvisualizaCamposRelatorioPersonalizado);
  Application.CreateForm(TFfiltrorelpersonalizado, Ffiltrorelpersonalizado);
  Application.CreateForm(TFRelatorioPersonalizado, FRelatorioPersonalizado);
  Application.CreateForm(TFarquivoIni, FarquivoIni);
  Application.CreateForm(TFcontaFacil, FcontaFacil);
  Application.CreateForm(TFFinalizaComissaoColocador, FFinalizaComissaoColocador);
  Application.CreateForm(TFmostramensagem, Fmostramensagem);
  Application.CreateForm(TFICONES, FICONES);
  Application.CreateForm(TFescolheIcones, FescolheIcones);
  Application.CreateForm(TFpopupIcone, FpopupIcone);
  Application.CreateForm(TFinputQuery, FinputQuery);
  Application.CreateForm(TfrmStatus, frmStatus);
  Application.CreateForm(TfrmStatus, frmStatus);
  Application.CreateForm(TFrelpersonalizadoReportBuilder, FrelpersonalizadoReportBuilder);
  Application.CreateForm(TFRELPERSREPORTBUILDER, FRELPERSREPORTBUILDER);
  Application.CreateForm(TformRecuperaDuplicidade, formRecuperaDuplicidade);
  Application.CreateForm(TFEscolheStringGrid, FEscolheStringGrid);
  Application.CreateForm(TFCODIGORECEITAESTADUAL, FCODIGORECEITAESTADUAL);
  Application.CreateForm(TfEspera, fEspera);
  Application.CreateForm(TFModeloDeNF, FModeloDeNF);
  //Application.CreateForm(TFComponentesNfe, FComponentesNfe);
  Application.CreateForm(TFRdPrint, FRdPrint);
  Application.CreateForm(TFImpDuplicata, FImpDuplicata);
  Application.CreateForm(TFImpDuplicata02, FImpDuplicata02);
  Application.CreateForm(TFConfDuplicata, FConfDuplicata);
  Application.CreateForm(TFConfDuplicata02, FConfDuplicata02);
  Application.CreateForm(TFrelBoleto_PI, FrelBoleto_PI);
  Application.CreateForm(TFConfiguraBoleto, FConfiguraBoleto);
  Application.CreateForm(TFBoletoBancario, FBoletoBancario);
  Application.CreateForm(TFIMPOSTO_ICMS, FIMPOSTO_ICMS);
  Application.CreateForm(TFTIPOCLIENTE, FTIPOCLIENTE);
  Application.CreateForm(TFSITUACAOTRIBUTARIA_COFINS, FSITUACAOTRIBUTARIA_COFINS);
  Application.CreateForm(TFIMPOSTO_IPI, FIMPOSTO_IPI);
  Application.CreateForm(TFSITUACAOTRIBUTARIA_PIS, FSITUACAOTRIBUTARIA_PIS);
  Application.CreateForm(TFSITUACAOTRIBUTARIA_IPI, FSITUACAOTRIBUTARIA_IPI);
  Application.CreateForm(TFIMPOSTO_COFINS, FIMPOSTO_COFINS);
  Application.CreateForm(TFIMPOSTO_PIS, FIMPOSTO_PIS);
  Application.CreateForm(TFIE_ST_ESTADO, FIE_ST_ESTADO);
  Application.CreateForm(TfrmStatus, frmStatus);
  Application.CreateForm(TFNFE, FNFE);
  Application.CreateForm(TFOPERACAONF, FOPERACAONF);
  Application.CreateForm(TFMostraValorMoedas, FMostraValorMoedas);
  Application.CreateForm(TFMOEDA, FMOEDA);
  Application.CreateForm(TFBOLETOINSTRUCOES, FBOLETOINSTRUCOES);
  Application.CreateForm(TFGeraArquivoRemessa, FGeraArquivoRemessa);
  Application.CreateForm(TFHistoricoBoleto, FHistoricoBoleto);
  Application.CreateForm(TFINSTRUCAO, FINSTRUCAO);
  Application.CreateForm(TFARQUIVOREMESSARETORNO, FARQUIVOREMESSARETORNO);
  Application.CreateForm(TFEscolheMateriaisReplicar, FEscolheMateriaisReplicar);
  Application.CreateForm(TFreprocessaChequeDevolvidoComissao, FreprocessaChequeDevolvidoComissao);
  Application.CreateForm(TFpesquisa, Fpesquisa);
  Application.CreateForm(TFCONFCAMPOSPESQUISA, FCONFCAMPOSPESQUISA);
  Application.CreateForm(TFConfordemcamposPesquisa, FConfordemcamposPesquisa);
  Application.CreateForm(TFtitulo_novo, Ftitulo_novo);
  Application.CreateForm(TFBaixaEmCheques, FBaixaEmCheques);
  Application.CreateForm(TFescolheNF, FescolheNF);
  Application.CreateForm(TFpesquisaProjNovo, FpesquisaProjNovo);
  Application.CreateForm(TFestado, Festado);
  Application.CreateForm(TFModeloDeNF, FModeloDeNF);
  Application.CreateForm(TFmenuNfe, FmenuNfe);
  Application.CreateForm(TFNfeDigitada, FNfeDigitada);
  Application.CreateForm(TfReferenciaNFE, fReferenciaNFE);
  Application.CreateForm(TFOrdemServico, FOrdemServico);
  Application.CreateForm(TFLancamentosFrentedeCaixa, FLancamentosFrentedeCaixa);
  Application.CreateForm(TFFORMASPAGAMENTO_FC, FFORMASPAGAMENTO_FC);
  Application.CreateForm(TFlancCredito, FlancCredito);
  Application.CreateForm(TTQRPedidoCompraPadrao, TQRPedidoCompraPadrao);
  Application.CreateForm(TForm9, Form9);
  Application.CreateForm(TFmedidasFuros, FmedidasFuros);
  Application.CreateForm(TTQRUrelPedidoCompraMater, TQRUrelPedidoCompraMater);
  Application.CreateForm(TFAjuda, FAjuda);
  Application.CreateForm(TFOrdemCorte, FOrdemCorte);
  Application.CreateForm(TFOrdemMedicao, FOrdemMedicao);
  Application.CreateForm(TFErrosDesativarMaterial, FErrosDesativarMaterial);
  Application.CreateForm(TFCobrador, FCobrador);
  Application.CreateForm(TQrControleEntrega, QrControleEntrega);
  Application.CreateForm(TFCadastroAjuda, FCadastroAjuda);
  Application.CreateForm(TFmostraStringGrid, FmostraStringGrid);
  Application.CreateForm(TFordemInstalacao, FordemInstalacao);
  Application.CreateForm(TFAgendaInstalacao, FAgendaInstalacao);
  Application.CreateForm(TFVendaRapida, FVendaRapida);
  Application.CreateForm(TFMapa, FMapa);
  Application.CreateForm(TFCONTADOR, FCONTADOR);
  Application.CreateForm(TfSpedFiscal, fSpedFiscal);
  Application.CreateForm(TFCorrecaoCFOPEntradaProdutos, FCorrecaoCFOPEntradaProdutos);
  Application.CreateForm(TfCorrecaoCFOPSaida, fCorrecaoCFOPSaida);
  Application.CreateForm(TFUNIDADEMEDIDA, FUNIDADEMEDIDA);
  Application.CreateForm(TFCOmponentesSped, FCOmponentesSped);
  Application.CreateForm(TFOBRIGACOESICMS, FOBRIGACOESICMS);
  Application.CreateForm(TFSintegra, FSintegra);
  Application.CreateForm(TFAlterarCoresPProjetos, FAlterarCoresPProjetos);
  Application.CreateForm(TFAcertaUnidades, FAcertaUnidades);
  Application.CreateForm(TFAcertoUnidadeEntrada, FAcertoUnidadeEntrada);
  Application.CreateForm(TFProducao, FProducao);
  Application.CreateForm(TFVisualizaStatusProducao, FVisualizaStatusProducao);
  Application.CreateForm(TFvideos, Fvideos);
  Application.CreateForm(TFMostraEstoque, FMostraEstoque);
  Application.CreateForm(TFItensCarrinhos, FItensCarrinhos);
  Application.CreateForm(TfAlteraFaturas, fAlteraFaturas);
  Application.CreateForm(TFConfiguracaoSite, FConfiguracaoSite);
  Application.CreateForm(TFGeraRomaneioPedido, FGeraRomaneioPedido);
  Application.CreateForm(TFCLIENTE, FCLIENTE);
  Application.CreateForm(TFFORNECEDOR, FFORNECEDOR);
  Application.CreateForm(TFNCM, FNCM);
  Application.CreateForm(TfDevolucaoNFE, fDevolucaoNFE);
  Application.CreateForm(TfAcertaImposto, fAcertaImposto);
  Application.CreateForm(TFPEDIDOONLINE, FPEDIDOONLINE);
  if (ObjIconesglobal<>nil)
  then ObjIconesglobal.CriaTodosIcones;

  Application.Run;
End.
