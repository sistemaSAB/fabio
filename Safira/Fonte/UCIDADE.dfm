object FCIDADE: TFCIDADE
  Left = 538
  Top = 317
  Width = 664
  Height = 271
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'CADASTRO DE CIDADE'
  Color = 10643006
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lb2: TLabel
    Left = 20
    Top = 73
    Width = 39
    Height = 14
    Caption = 'Codigo'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb3: TLabel
    Left = 20
    Top = 95
    Width = 89
    Height = 14
    Caption = 'Nome da Cidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb4: TLabel
    Left = 20
    Top = 119
    Width = 37
    Height = 14
    Caption = 'Estado'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbCodigoCidade: TLabel
    Left = 20
    Top = 143
    Width = 80
    Height = 14
    Caption = 'C'#243'digo Cidade'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object pnl1: TPanel
    Left = 0
    Top = 177
    Width = 648
    Height = 37
    Align = alBottom
    Color = clMedGray
    TabOrder = 0
    DesignSize = (
      648
      37)
    object imgrodape: TImage
      Left = 1
      Top = -8
      Width = 646
      Height = 44
      Align = alBottom
    end
    object lb14: TLabel
      Left = 365
      Top = 11
      Width = 252
      Height = 20
      Anchors = [akTop]
      Caption = 'Existem X cidades cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object edtCodigoCidade: TEdit
    Left = 125
    Top = 139
    Width = 121
    Height = 19
    TabOrder = 1
  end
  object edtestado: TEdit
    Left = 125
    Top = 116
    Width = 70
    Height = 19
    CharCase = ecUpperCase
    Color = 6073854
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 2
    ParentFont = False
    TabOrder = 2
  end
  object edtnome: TEdit
    Left = 125
    Top = 93
    Width = 287
    Height = 19
    CharCase = ecUpperCase
    MaxLength = 40
    TabOrder = 3
  end
  object edtCodigo: TEdit
    Left = 125
    Top = 70
    Width = 70
    Height = 19
    MaxLength = 9
    TabOrder = 4
  end
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 648
    Height = 50
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 5
    object lbnomeformulario: TLabel
      Left = 522
      Top = 0
      Width = 126
      Height = 50
      Align = alRight
      Alignment = taRightJustify
      Caption = 'Cidades'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -29
      Font.Name = 'Verdana'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object btNovo: TBitBtn
      Left = 2
      Top = -3
      Width = 50
      Height = 52
      Caption = '&N'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = btNovoClick
    end
    object btalterar: TBitBtn
      Left = 52
      Top = -3
      Width = 50
      Height = 52
      Caption = '&A'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btAlterarClick
    end
    object btgravar: TBitBtn
      Left = 102
      Top = -3
      Width = 50
      Height = 52
      Caption = '&G'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btSalvarClick
    end
    object btCancelar: TBitBtn
      Left = 152
      Top = -3
      Width = 50
      Height = 52
      Caption = '&C'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btCancelarClick
    end
    object btexcluir: TBitBtn
      Left = 202
      Top = -3
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btExcluirClick
    end
    object btpesquisar: TBitBtn
      Left = 252
      Top = -3
      Width = 50
      Height = 52
      Caption = '&P'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btPesquisarClick
    end
    object btrelatorios: TBitBtn
      Left = 302
      Top = -3
      Width = 50
      Height = 52
      Caption = '&R'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btRelatorioClick
    end
    object btOpcoes: TBitBtn
      Left = 352
      Top = -3
      Width = 50
      Height = 52
      Caption = '&O'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = btOpcoesClick
    end
    object btsair: TBitBtn
      Left = 402
      Top = -3
      Width = 50
      Height = 52
      Caption = '&S'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btSairClick
    end
  end
  object statusBar1: TStatusBar
    Left = 0
    Top = 214
    Width = 648
    Height = 19
    Panels = <
      item
        Width = 50
      end
      item
        Text = 'F2 - CANCELA IMPORTA'#199#195'O'
        Width = 50
      end>
  end
end
