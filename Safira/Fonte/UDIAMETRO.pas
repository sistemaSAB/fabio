unit UDIAMETRO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjDIAMETRO,
  UessencialGlobal, Tabs;

type
  TFDIAMETRO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    imgrodape: TImage;
    EdtCodigo: TEdit;
    lbLbCodigo: TLabel;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
   
  private
    ObjDIAMETRO:TObjDIAMETRO;
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FDIAMETRO: TFDIAMETRO;


implementation

uses Upesquisa, UessencialLocal, UDataModulo, UescolheImagemBotao,
  Uprincipal;

{$R *.dfm}


procedure TFDIAMETRO.FormCreate(Sender: TObject);
begin

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
       ColocaUpperCaseEdit(Self);

     Try
        Self.ObjDIAMETRO:=TObjDIAMETRO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');


     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE DIAMETRO')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
end;


procedure TFDIAMETRO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjDIAMETRO=Nil)
     Then exit;

     If (Self.ObjDIAMETRO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    Self.ObjDIAMETRO.free;
    Action := caFree;
    Self := nil;
end;

procedure TFDIAMETRO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     //edtcodigo.text:='0';
     //edtcodigo.text:=ObjDIAMETRO.Get_novocodigo;
     //edtcodigo.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjDIAMETRO.status:=dsInsert;

     EdtCodigo.setfocus;
      btalterar.visible:=false;
     btrelatorios.visible:=false;
     btsair.Visible:=false;
     btopcoes.visible:=false;
     btexcluir.Visible:=false;
     btnovo.Visible:=false;
     btopcoes.visible:=false;


end;

procedure TFDIAMETRO.btSalvarClick(Sender: TObject);
begin

     If ObjDIAMETRO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjDIAMETRO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjDIAMETRO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFDIAMETRO.btAlterarClick(Sender: TObject);
begin
    If (ObjDIAMETRO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ObjDIAMETRO.Status:=dsEdit;
                EdtCodigo.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                 btalterar.visible:=false;
               btrelatorios.visible:=false;
               btsair.Visible:=false;
               btopcoes.visible:=false;
               btexcluir.Visible:=false;
               btnovo.Visible:=false;
               btopcoes.visible:=false;

          End;

end;

procedure TFDIAMETRO.btCancelarClick(Sender: TObject);
begin
     ObjDIAMETRO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);
     btalterar.visible:=true;
     btrelatorios.visible:=true;
     btsair.Visible:=true;
     btopcoes.visible:=true;
     btexcluir.Visible:=true;
     btnovo.Visible:=true;
      btopcoes.visible:=true;

end;

procedure TFDIAMETRO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjDIAMETRO.Get_pesquisa,ObjDIAMETRO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjDIAMETRO.status<>dsinactive
                                  then exit;

                                  If (ObjDIAMETRO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjDIAMETRO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFDIAMETRO.btExcluirClick(Sender: TObject);
begin
     If (ObjDIAMETRO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjDIAMETRO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjDIAMETRO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFDIAMETRO.btRelatorioClick(Sender: TObject);
begin
    ObjDIAMETRO.Imprime(edtCodigo.text);
end;

procedure TFDIAMETRO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFDIAMETRO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFDIAMETRO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFDIAMETRO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;

end;

procedure TFDIAMETRO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFDIAMETRO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjDIAMETRO do
    Begin
        Submit_Codigo(edtCodigo.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFDIAMETRO.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFDIAMETRO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjDIAMETRO do
     Begin
        EdtCodigo.text:=Get_Codigo;
//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFDIAMETRO.TabelaParaControles: Boolean;
begin
     If (ObjDIAMETRO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;


end.
