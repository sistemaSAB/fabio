unit UDataModulo;

interface

uses
Uobjmenufinanceiro, UobjEMPRESA,Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IBDatabase, Db, IBServices,UObjUsuarios,Uobjpermissoesuso,UobjParametros,
  DBTables, RDprint,UObjConfRelatorio,UObjLanctoPortador,UobjCREDITOVALORES,
  IBCustomDataSet, IBQuery, uobjestoque,uobjicones,uobjintegracao,uobjconfcampospesquisa, StdCtrls;

type
  TFDataModulo = class(TDataModule)
    IBDatabase: TIBDatabase;
    IBTransaction: TIBTransaction;
    IbUser: TIBSecurityService;
    QueryImporta: TQuery;
    ComponenteRdPrint: TRDprint;
    IBDatabaseBackup: TIBDatabase;
    IBTransactionBackup: TIBTransaction;
    IBQueryPesquisa: TIBQuery;
    IBDataPEDIDOPROJ_ORDEMPRODUCAO: TIBDataSet;
    unicQuery: TIBQuery;
    unicTransaction: TIBTransaction;

    { Private declarations }
  public
    { Public declarations }
    LocalDesenhosGlobal:String;
    CodigoProjetoBrancoGlobal:String;
    DescontoMaximo_global:Currency;
    ValidaCPFCNPJ_venda_global:boolean;
    UtilizaArredondamentoGlobal5cm:boolean;
    ValidaCPFCNPJ_cadastramento_Cliente:boolean;

    Function ContaRegistros(PTABELA:String):integer;
    function Arredonda5Cm(Poriginal: currency; var PdiferencaMaior: Currency): boolean;
    procedure ExecutaMetodoExterno(Pnome, Pobjeto: String);

    //procedure edtNCMExit(Sender, pDestino: tobject);
    procedure edtNCMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSoNumerosKeyPress(Sender: TObject; var Key: Char);

  end;

var
  FDataModulo: TFDataModulo;
  ObjUsuarioGlobal:Tobjusuarios;
  ObjPermissoesUsoGlobal:TobjPermissoesUso;
  ObjParametroGlobal:Tobjparametros;
  ObjConfiguraRelGlobal:TObjConfRelatorio;
  ObjLanctoPortadorGlobal:TObjLanctoPortador;
  ObjEmpresaGlobal:tobjempresa;
  CorFormGlobal:TColor;
  SistemaMdiGlobal:Boolean;
  ModoCalculoSenhaGlobal:integer;
  SistemaemModoDemoGlobal:boolean;
  ObjCreditoValoresGlobal:tobjCreditoValores;
  DescontaComissaoArquitetoVendedor:boolean;
  ESTADOSISTEMAGLOBAL:STRING;
  UsaEnderecoEnderecoCobrancaGlobal,VerificaNumeroDocumentoTituloGlobal:boolean;
  VerificaDataMenorLancamentoFinanceiro_global:boolean;
  verificaDataMaiorLancamentoFinanceiro_global:boolean;
  PfechasistemaGlobal:Boolean;
  UtilizaNfe:Boolean;
  escolheTransportadora:Boolean;
  ObjMenuFinanceiro:tobjmenufinanceiro;
  ObjIconesglobal:tobjicones;
  PathLaraGlobal:string;
  ImprimeCepSeparadoPedidoCompacto_global:boolean;

  ObjIntegracaoglobal:Tobjintegracao=nil;
  ObjCamposPesquisa_Global:  TobjCONFCAMPOSPESQUISA;

  CORGRIDZEBRADOGLOBAL1:TColor;
  CORGRIDZEBRADOGLOBAL2:TColor;

  OBJESTOQUEGLOBAL:TOBJESTOQUE=nil;
  utilizaSpedFiscal:Boolean;
  indicePagmentoNfe:SmallInt;
  PRAZOPAGAMENTOPADRAO:string;

implementation

uses UobjNCM, UMostraBarraProgresso, UessencialGlobal;





{$R *.DFM}

{ TFDataModulo }

function TFDataModulo.ContaRegistros(PTABELA: String): integer;
begin
     result:=0;

     With Self.IBQueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select count(codigo) as conta from '+Ptabela);
          try
            open;
            result:=fieldbyname('conta').asinteger;
          Except
                
          End;
     End;


end;

Function TFDataModulo.Arredonda5Cm(Poriginal:currency;
  var PdiferencaMaior: Currency):boolean;
var
presultado,ValorCM:currency;
presto:integer;
begin
     result:=False;
     PdiferencaMaior:=0;

     if (FdataModulo.UtilizaArredondamentoGlobal5cm=False)
     Then Begin
                result:=True;
                exit;
     End;


     //O Valor enviado � em MM

     //portanto preciso dividir por 10 para obter o resultado em CM
     //de posse desse resultado / 5 e pego o resto, se o resto
     //for diferente de  zero o numero nao � multiplo de 5 cm
     //sendo assim incremento 1 cm no resultado e calculo
     //qual o valor de arrendodamento

     {
     Exemplo:

        1330 mm  / 10 = 133 cm/5 = 26,6 portando nao � multiplo

        Resultado inteiro = 26 + 1 = 27*5 cm = 135 cm portando

        135-133 = 2 cm * 10 = 20 mm de arredondamento

        entao a largura correta para calculo R$ seria
         1330+20 = 1350 mm que correspondem a 135 cm
         
     }

     ValorCM:=(poriginal/10);

     presultado:=(Valorcm/5);//sabendo se � multiplo de 5

     if ((int(presultado)*5)<Valorcm)//nao � multiplo
     Then presultado:=int(presultado)+1;

     presultado:=(presultado*5);
     pdiferencaMaior:=(Presultado*10)-poriginal;
    
     RESULT:=TRUE
end;

procedure TFDataModulo.ExecutaMetodoExterno(Pnome: string;Pobjeto:String);
begin
     if (Pobjeto='objmenufinanceiro')
     then ObjMenuFinanceiro.ExecutaMetodo(ObjMenuFinanceiro,pnome);
end;

{no sab nao existe preencher o edit, pois o imposto � carregado ao selecionar o imposto
procedure TFDataModulo.edtNCMExit(Sender: TObject; pDestino: tobject);
var
  objNCM:TObjNCM;
begin
  if Trim( TEdit(sender).Text ) <> '' then
  begin
    objNCM := TObjNCM.Create(SELF);
    try
      if objNCM.LocalizaporCampo( 'CODIGONCM',Trim( TEdit(sender).Text ) ) then
      begin
        objNCM.TabelaparaObjeto;
        TEdit(pdestino).Text := objNCM.get_percentualTributo;
      end;
    finally
      objNCM.Free;
    end;
  end;
end;
}

procedure TFDataModulo.edtNCMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  objNCM:TObjNCM;
begin

  if Key = vk_F9 then
  begin
    objNCM := TObjNCM.Create(SELF);
    TEdit(sender).Text := objNCM.pegaNCM;
    objNCM.Free;
  end;
end;

procedure TFDataModulo.edtSoNumerosKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Key in['0'..'9',Chr(8)]) then
      Key:= #0; 
end;

end.
