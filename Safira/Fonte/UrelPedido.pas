unit UrelPedido;

interface

uses dialogs,Windows, SysUtils, Messages, Classes, Graphics, Controls,
  ExtCtrls, Forms, QuickRpt, QRCtrls;

type
  TQRpedido = class(TQuickRep)
    SB: TQRStringsBand;
    Bandac: TQRBand;
    lbnomesuperior: TQRLabel;
    LbNumPag: TQRSysData;
    BandaDetail: TQRBand;
    QrImagemProjeto: TQRImage;
    LbReferenciaProjeto: TQRLabel;
    QRSysData1: TQRSysData;
    QRShape1: TQRShape;
    lbDescricao: TQRLabel;
    LbComplemento: TQRLabel;
    LbAltura: TQRLabel;
    LbLocal: TQRLabel;
    lbLargura: TQRLabel;
    QRShape4: TQRShape;
    QRShape3: TQRShape;
    lbITEMQTDEValor: TQRLabel;
    LBDADOS: TQRLabel;
    BANDATITULO: TQRBand;
    QRShape2: TQRShape;
    LbNome: TQRLabel;
    LBEndereco: TQRLabel;
    LbContato: TQRLabel;
    LbObra: TQRLabel;
    QrlinhaSuperiorProposta: TQRShape;
    QrLbProposta: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel18: TQRLabel;
    Label20: TQRLabel;
    LBTelefone: TQRLabel;
    lbCNPJCPF: TQRLabel;
    LbVendedor: TQRLabel;
    lbRGIE: TQRLabel;
    QRLabel21: TQRLabel;
    QRImageLogotipo: TQRImage;
    LBCabecalhoLinha1: TQRLabel;
    LBCabecalhoLinha2: TQRLabel;
    LBCabecalhoLinha3: TQRLabel;
    LBCabecalhoLinha4: TQRLabel;
    LBProposta: TQRLabel;
    QRBand1: TQRBand;
    LbValorTotal: TQRLabel;
    LBValorAcrescimo: TQRLabel;
    LBValorDesconto: TQRLabel;
    LBValorFinal: TQRLabel;
    lbTitulo: TQRLabel;
    QrMemoPendencias: TQRMemo;
    QRLabel1: TQRLabel;
    lB1: TQRLabel;
    lB2: TQRLabel;
    lBLado: TQRLabel;
    lBlado1: TQRLabel;
    lbObs: TQRLabel;
    procedure LBDADOSPrint(sender: TObject; var Value: String);
    procedure BANDATITULOBeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private

  public

    destructor destroy;Override;

  end;

var
  QRpedido: TQRpedido;

implementation



{$R *.DFM}


procedure TQRpedido.LBDADOSPrint(sender: TObject; var Value: String);
var
apoio:string;
posicao:integer;
begin
     lbdados.font.Color:=clBLACK;
     LBDADOS.Font.Style:=[];
     LBDADOS.Font.size:=9;

     case sb.item[1] of
     '?':Begin//negrito
              LBDADOS.Font.Style:=[fsbold];
              value:=copy(sb.Item,2,length(sb.item));
         end;
     '�':Begin//cor

              apoio:=copy(sb.Item,2,length(sb.item));
              posicao:=pos('�',apoio);
              If (posicao=0)//tem que ter o fecho e entre eles a cor
              Then value:=sb.item
              Else Begin//s� 3 cores, vermelho,verde e azul
                        apoio:=copy(apoio,1,posicao-1);
                        if UPPERCASE(apoio)='VERMELHO'
                        tHEN lbdados.font.Color:=CLRED
                        eLSE
                                IF UPPERCASE(APOIO)='VERDE'
                                THEN lbdados.font.Color:=clGreen
                                ELSE
                                        IF UPPERCASE(APOIO)='AZUL'
                                        THEN lbdados.font.Color:=clBlue
                                        ELSE lbdados.font.Color:=clBLACK;


                        //pegando os dados fora a cor
                        value:=copy(sb.Item,posicao+2,length(sb.item));
              End;
         End;
     Else Begin
                value:=Sb.item;
     End;

     end;


end;






procedure TQRpedido.BANDATITULOBeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

//ALTERADO POR RONNEI**********************************************************
{
testes feitos para posicionar a linha e a palavra proposta
e aumentar o tamanho da banda de acordo com o memo de pendencias

Banda titulo
244 para 1 linha
258 para 2 linhas
ou seja 14 por linha
----------------------------
linha proposta
207 para 1 linha
224 para 2 linhas
ou seja 17 por linha
----------------------------
palavra proposta
213 para 1 linha
230 para 2 linhas
ou seja 17 por linha
-----------------------------
}
BANDATITULO.Height:=224+(QrMemoPendencias.Lines.count*14);
QrlinhaSuperiorProposta.Top:=192+(QrMemoPendencias.Lines.count*17);
QrLbProposta.Top:=198+(QrMemoPendencias.Lines.count*17);

//*****************************************************************

end;

destructor TQRpedido.destroy;
begin

  inherited;
end;

end.
