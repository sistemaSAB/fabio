unit UobjMATERIAISDEVOLUCAO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc;

Type
   TObjMATERIAISDEVOLUCAO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_PEDIDO(parametro: string);
                Function Get_PEDIDO: string;
                Procedure Submit_FERRAGEM_PP(parametro: string);
                Function Get_FERRAGEM_PP: string;
                Procedure Submit_PERFILADO_PP(parametro: string);
                Function Get_PERFILADO_PP: string;
                Procedure Submit_DIVERSO_PP(parametro: string);
                Function Get_DIVERSO_PP: string;
                Procedure Submit_VIDRO_PP(parametro: string);
                Function Get_VIDRO_PP: string;
                Procedure Submit_KITBOX_PP(parametro: string);
                Function Get_KITBOX_PP: string;
                Procedure Submit_PERSIANA_PP(parametro: string);
                Function Get_PERSIANA_PP: string;
                Procedure Submit_FERRAGEM(parametro: string);
                Function Get_FERRAGEM: string;
                Procedure Submit_KITBOX(parametro: string);
                Function Get_KITBOX: string;
                Procedure Submit_PERFILADO(parametro: string);
                Function Get_PERFILADO: string;
                Procedure Submit_DIVERSO(parametro: string);
                Function Get_DIVERSO: string;
                Procedure Submit_VIDRO(parametro: string);
                Function Get_VIDRO: string;
                Procedure Submit_PERSIANA(parametro: string);
                Function Get_PERSIANA: string;
                Procedure Submit_DATADEVOLUCAO(parametro: string);
                Function Get_DATADEVOLUCAO: string;
                Procedure Submit_FERRAGEMCOR(parametro: string);
                Function Get_FERRAGEMCOR: string;
                Procedure Submit_PERFILADOCOR(parametro: string);
                Function Get_PERFILADOCOR: string;
                Procedure Submit_DIVERSOCOR(parametro: string);
                Function Get_DIVERSOCOR: string;
                Procedure Submit_VIDROCOR(parametro: string);
                Function Get_VIDROCOR: string;
                Procedure Submit_KITBOXCOR(parametro: string);
                Function Get_KITBOXCOR: string;
                Procedure Submit_QUANTIDADE(parametro: string);
                Function Get_QUANTIDADE: string;
                Procedure Submit_MATERIAL(parametro: string);
                Function Get_MATERIAL: string;
                Procedure Submit_DATAC(parametro: string);
                Function Get_DATAC: string;
                Procedure Submit_DATAM(parametro: string);
                Function Get_DATAM: string;
                Procedure Submit_USERM(parametro: string);
                Function Get_USERM: string;
                Procedure Submit_USERC(parametro: string);
                Function Get_USERC: string;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               PEDIDO:string;
               FERRAGEM_PP:string;
               PERFILADO_PP:string;
               DIVERSO_PP:string;
               VIDRO_PP:string;
               KITBOX_PP:string;
               PERSIANA_PP:string;
               FERRAGEM:string;
               KITBOX:string;
               PERFILADO:string;
               DIVERSO:string;
               VIDRO:string;
               PERSIANA:string;
               DATADEVOLUCAO:string;
               FERRAGEMCOR:string;
               PERFILADOCOR:string;
               DIVERSOCOR:string;
               VIDROCOR:string;
               KITBOXCOR:string;
               QUANTIDADE:string;
               MATERIAL:string;
               DATAC:string;
               DATAM:string;
               USERM:string;
               USERC:string;
//CODIFICA VARIAVEIS PRIVADAS




























               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios;


{ TTabTitulo }


Function  TObjMATERIAISDEVOLUCAO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.PEDIDO:=fieldbyname('PEDIDO').asstring;
        Self.FERRAGEM_PP:=fieldbyname('FERRAGEM_PP').asstring;
        Self.PERFILADO_PP:=fieldbyname('PERFILADO_PP').asstring;
        Self.DIVERSO_PP:=fieldbyname('DIVERSO_PP').asstring;
        Self.VIDRO_PP:=fieldbyname('VIDRO_PP').asstring;
        Self.KITBOX_PP:=fieldbyname('KITBOX_PP').asstring;
        Self.PERSIANA_PP:=fieldbyname('PERSIANA_PP').asstring;
        Self.FERRAGEM:=fieldbyname('FERRAGEM').asstring;
        Self.KITBOX:=fieldbyname('KITBOX').asstring;
        Self.PERFILADO:=fieldbyname('PERFILADO').asstring;
        Self.DIVERSO:=fieldbyname('DIVERSO').asstring;
        Self.VIDRO:=fieldbyname('VIDRO').asstring;
        Self.PERSIANA:=fieldbyname('PERSIANA').asstring;
        Self.DATADEVOLUCAO:=fieldbyname('DATADEVOLUCAO').asstring;
        Self.FERRAGEMCOR:=fieldbyname('FERRAGEMCOR').asstring;
        Self.PERFILADOCOR:=fieldbyname('PERFILADOCOR').asstring;
        Self.DIVERSOCOR:=fieldbyname('DIVERSOCOR').asstring;
        Self.VIDROCOR:=fieldbyname('VIDROCOR').asstring;
        Self.KITBOXCOR:=fieldbyname('KITBOXCOR').asstring;
        Self.QUANTIDADE:=fieldbyname('QUANTIDADE').asstring;
        Self.MATERIAL:=fieldbyname('MATERIAL').asstring;
        Self.DATAC:=fieldbyname('DATAC').asstring;
        Self.DATAM:=fieldbyname('DATAM').asstring;
        Self.USERM:=fieldbyname('USERM').asstring;
        Self.USERC:=fieldbyname('USERC').asstring;
//CODIFICA TABELAPARAOBJETO


























        result:=True;
     End;
end;


Procedure TObjMATERIAISDEVOLUCAO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('PEDIDO').asstring:=Self.PEDIDO;
        ParamByName('FERRAGEM_PP').asstring:=Self.FERRAGEM_PP;
        ParamByName('PERFILADO_PP').asstring:=Self.PERFILADO_PP;
        ParamByName('DIVERSO_PP').asstring:=Self.DIVERSO_PP;
        ParamByName('VIDRO_PP').asstring:=Self.VIDRO_PP;
        ParamByName('KITBOX_PP').asstring:=Self.KITBOX_PP;
        ParamByName('PERSIANA_PP').asstring:=Self.PERSIANA_PP;
        ParamByName('FERRAGEM').asstring:=Self.FERRAGEM;
        ParamByName('KITBOX').asstring:=Self.KITBOX;
        ParamByName('PERFILADO').asstring:=Self.PERFILADO;
        ParamByName('DIVERSO').asstring:=Self.DIVERSO;
        ParamByName('VIDRO').asstring:=Self.VIDRO;
        ParamByName('PERSIANA').asstring:=Self.PERSIANA;
        ParamByName('DATADEVOLUCAO').asstring:=Self.DATADEVOLUCAO;
        ParamByName('FERRAGEMCOR').asstring:=Self.FERRAGEMCOR;
        ParamByName('PERFILADOCOR').asstring:=Self.PERFILADOCOR;
        ParamByName('DIVERSOCOR').asstring:=Self.DIVERSOCOR;
        ParamByName('VIDROCOR').asstring:=Self.VIDROCOR;
        ParamByName('KITBOXCOR').asstring:=Self.KITBOXCOR;
        ParamByName('QUANTIDADE').asstring:=virgulaparaponto(Self.QUANTIDADE);
        ParamByName('MATERIAL').asstring:=Self.MATERIAL;
        ParamByName('DATAC').asstring:=Self.DATAC;
        ParamByName('DATAM').asstring:=Self.DATAM;
        ParamByName('USERM').asstring:=Self.USERM;
        ParamByName('USERC').asstring:=Self.USERC;
//CODIFICA OBJETOPARATABELA


























  End;
End;

//***********************************************************************

function TObjMATERIAISDEVOLUCAO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  {if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;      }

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjMATERIAISDEVOLUCAO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        PEDIDO:='';
        FERRAGEM_PP:='';
        PERFILADO_PP:='';
        DIVERSO_PP:='';
        VIDRO_PP:='';
        KITBOX_PP:='';
        PERSIANA_PP:='';
        FERRAGEM:='';
        KITBOX:='';
        PERFILADO:='';
        DIVERSO:='';
        VIDRO:='';
        PERSIANA:='';
        DATADEVOLUCAO:='';
        FERRAGEMCOR:='';
        PERFILADOCOR:='';
        DIVERSOCOR:='';
        VIDROCOR:='';
        KITBOXCOR:='';
        QUANTIDADE:='';
        MATERIAL:='';
        DATAC:='';
        DATAM:='';
        USERM:='';
        USERC:='';
//CODIFICA ZERARTABELA


























     End;
end;

Function TObjMATERIAISDEVOLUCAO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
            If (CODIGO='')
      Then Mensagem:=mensagem+'/CODIGO';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjMATERIAISDEVOLUCAO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjMATERIAISDEVOLUCAO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/CODIGO';
     End;
     try
        Strtoint(Self.PEDIDO);
     Except
           Mensagem:=mensagem+'/PEDIDO';
     End;
     try
        Strtoint(Self.FERRAGEM_PP);
     Except
           Mensagem:=mensagem+'/FERRAGEM_PP';
     End;
     try
        Strtoint(Self.PERFILADO_PP);
     Except
           Mensagem:=mensagem+'/PERFILADO_PP';
     End;
     try
        Strtoint(Self.DIVERSO_PP);
     Except
           Mensagem:=mensagem+'/DIVERSO_PP';
     End;
     try
        Strtoint(Self.VIDRO_PP);
     Except
           Mensagem:=mensagem+'/VIDRO_PP';
     End;
     try
        Strtoint(Self.KITBOX_PP);
     Except
           Mensagem:=mensagem+'/KITBOX_PP';
     End;
     try
        Strtoint(Self.PERSIANA_PP);
     Except
           Mensagem:=mensagem+'/PERSIANA_PP';
     End;
     try
        Strtoint(Self.FERRAGEM);
     Except
           Mensagem:=mensagem+'/FERRAGEM';
     End;
     try
        Strtoint(Self.KITBOX);
     Except
           Mensagem:=mensagem+'/KITBOX';
     End;
     try
        Strtoint(Self.PERFILADO);
     Except
           Mensagem:=mensagem+'/PERFILADO';
     End;
     try
        Strtoint(Self.DIVERSO);
     Except
           Mensagem:=mensagem+'/DIVERSO';
     End;
     try
        Strtoint(Self.VIDRO);
     Except
           Mensagem:=mensagem+'/VIDRO';
     End;
     try
        Strtoint(Self.PERSIANA);
     Except
           Mensagem:=mensagem+'/PERSIANA';
     End;
     try
        Strtoint(Self.FERRAGEMCOR);
     Except
           Mensagem:=mensagem+'/FERRAGEMCOR';
     End;
     try
        Strtoint(Self.PERFILADOCOR);
     Except
           Mensagem:=mensagem+'/PERFILADOCOR';
     End;
     try
        Strtoint(Self.DIVERSOCOR);
     Except
           Mensagem:=mensagem+'/DIVERSOCOR';
     End;
     try
        Strtoint(Self.VIDROCOR);
     Except
           Mensagem:=mensagem+'/VIDROCOR';
     End;
     try
        Strtoint(Self.KITBOXCOR);
     Except
           Mensagem:=mensagem+'/KITBOXCOR';
     End;
     try
        Strtofloat(Self.QUANTIDADE);
     Except
           Mensagem:=mensagem+'/QUANTIDADE';
     End;
     try
        Strtoint(Self.MATERIAL);
     Except
           Mensagem:=mensagem+'/MATERIAL';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjMATERIAISDEVOLUCAO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATADEVOLUCAO);
     Except
           Mensagem:=mensagem+'/DATADEVOLUCAO';
     End;
     try
        Strtodatetime(Self.DATAC);
     Except
           Mensagem:=mensagem+'/DATAC';
     End;
     try
        Strtodatetime(Self.DATAM);
     Except
           Mensagem:=mensagem+'/DATAM';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjMATERIAISDEVOLUCAO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjMATERIAISDEVOLUCAO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro MATERIAISDEVOLUCAO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,PEDIDO,FERRAGEM_PP,PERFILADO_PP,DIVERSO_PP,VIDRO_PP');
           SQL.ADD(' ,KITBOX_PP,PERSIANA_PP,FERRAGEM,KITBOX,PERFILADO,DIVERSO,VIDRO');
           SQL.ADD(' ,PERSIANA,DATADEVOLUCAO,FERRAGEMCOR,PERFILADOCOR,DIVERSOCOR,VIDROCOR');
           SQL.ADD(' ,KITBOXCOR,QUANTIDADE,MATERIAL,DATAC,DATAM,USERM,USERC');
           SQL.ADD(' from  TABMATERIAISDEVOLUCAO');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjMATERIAISDEVOLUCAO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjMATERIAISDEVOLUCAO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjMATERIAISDEVOLUCAO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABMATERIAISDEVOLUCAO(CODIGO,PEDIDO,FERRAGEM_PP');
                InsertSQL.add(' ,PERFILADO_PP,DIVERSO_PP,VIDRO_PP,KITBOX_PP,PERSIANA_PP');
                InsertSQL.add(' ,FERRAGEM,KITBOX,PERFILADO,DIVERSO,VIDRO,PERSIANA,DATADEVOLUCAO');
                InsertSQL.add(' ,FERRAGEMCOR,PERFILADOCOR,DIVERSOCOR,VIDROCOR,KITBOXCOR');
                InsertSQL.add(' ,QUANTIDADE,MATERIAL,DATAC,DATAM,USERM,USERC)');
                InsertSQL.add('values (:CODIGO,:PEDIDO,:FERRAGEM_PP,:PERFILADO_PP,:DIVERSO_PP');
                InsertSQL.add(' ,:VIDRO_PP,:KITBOX_PP,:PERSIANA_PP,:FERRAGEM,:KITBOX');
                InsertSQL.add(' ,:PERFILADO,:DIVERSO,:VIDRO,:PERSIANA,:DATADEVOLUCAO');
                InsertSQL.add(' ,:FERRAGEMCOR,:PERFILADOCOR,:DIVERSOCOR,:VIDROCOR,:KITBOXCOR');
                InsertSQL.add(' ,:QUANTIDADE,:MATERIAL,:DATAC,:DATAM,:USERM,:USERC');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABMATERIAISDEVOLUCAO set CODIGO=:CODIGO,PEDIDO=:PEDIDO');
                ModifySQL.add(',FERRAGEM_PP=:FERRAGEM_PP,PERFILADO_PP=:PERFILADO_PP');
                ModifySQL.add(',DIVERSO_PP=:DIVERSO_PP,VIDRO_PP=:VIDRO_PP,KITBOX_PP=:KITBOX_PP');
                ModifySQL.add(',PERSIANA_PP=:PERSIANA_PP,FERRAGEM=:FERRAGEM,KITBOX=:KITBOX');
                ModifySQL.add(',PERFILADO=:PERFILADO,DIVERSO=:DIVERSO,VIDRO=:VIDRO');
                ModifySQL.add(',PERSIANA=:PERSIANA,DATADEVOLUCAO=:DATADEVOLUCAO,FERRAGEMCOR=:FERRAGEMCOR');
                ModifySQL.add(',PERFILADOCOR=:PERFILADOCOR,DIVERSOCOR=:DIVERSOCOR,VIDROCOR=:VIDROCOR');
                ModifySQL.add(',KITBOXCOR=:KITBOXCOR,QUANTIDADE=:QUANTIDADE,MATERIAL=:MATERIAL');
                ModifySQL.add(',DATAC=:DATAC,DATAM=:DATAM,USERM=:USERM,USERC=:USERC');
                ModifySQL.add('');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABMATERIAISDEVOLUCAO where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjMATERIAISDEVOLUCAO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjMATERIAISDEVOLUCAO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabMATERIAISDEVOLUCAO');
     Result:=Self.ParametroPesquisa;
end;

function TObjMATERIAISDEVOLUCAO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de MATERIAISDEVOLUCAO ';
end;


function TObjMATERIAISDEVOLUCAO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENMATERIAISDEVOLUCAO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENMATERIAISDEVOLUCAO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjMATERIAISDEVOLUCAO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjMATERIAISDEVOLUCAO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjMATERIAISDEVOLUCAO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjMATERIAISDEVOLUCAO.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_PEDIDO(parametro: string);
begin
        Self.PEDIDO:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_PEDIDO: string;
begin
        Result:=Self.PEDIDO;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_FERRAGEM_PP(parametro: string);
begin
        Self.FERRAGEM_PP:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_FERRAGEM_PP: string;
begin
        Result:=Self.FERRAGEM_PP;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_PERFILADO_PP(parametro: string);
begin
        Self.PERFILADO_PP:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_PERFILADO_PP: string;
begin
        Result:=Self.PERFILADO_PP;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_DIVERSO_PP(parametro: string);
begin
        Self.DIVERSO_PP:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_DIVERSO_PP: string;
begin
        Result:=Self.DIVERSO_PP;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_VIDRO_PP(parametro: string);
begin
        Self.VIDRO_PP:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_VIDRO_PP: string;
begin
        Result:=Self.VIDRO_PP;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_KITBOX_PP(parametro: string);
begin
        Self.KITBOX_PP:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_KITBOX_PP: string;
begin
        Result:=Self.KITBOX_PP;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_PERSIANA_PP(parametro: string);
begin
        Self.PERSIANA_PP:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_PERSIANA_PP: string;
begin
        Result:=Self.PERSIANA_PP;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_FERRAGEM(parametro: string);
begin
        Self.FERRAGEM:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_FERRAGEM: string;
begin
        Result:=Self.FERRAGEM;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_KITBOX(parametro: string);
begin
        Self.KITBOX:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_KITBOX: string;
begin
        Result:=Self.KITBOX;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_PERFILADO(parametro: string);
begin
        Self.PERFILADO:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_PERFILADO: string;
begin
        Result:=Self.PERFILADO;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_DIVERSO(parametro: string);
begin
        Self.DIVERSO:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_DIVERSO: string;
begin
        Result:=Self.DIVERSO;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_VIDRO(parametro: string);
begin
        Self.VIDRO:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_VIDRO: string;
begin
        Result:=Self.VIDRO;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_PERSIANA(parametro: string);
begin
        Self.PERSIANA:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_PERSIANA: string;
begin
        Result:=Self.PERSIANA;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_DATADEVOLUCAO(parametro: string);
begin
        Self.DATADEVOLUCAO:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_DATADEVOLUCAO: string;
begin
        Result:=Self.DATADEVOLUCAO;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_FERRAGEMCOR(parametro: string);
begin
        Self.FERRAGEMCOR:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_FERRAGEMCOR: string;
begin
        Result:=Self.FERRAGEMCOR;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_PERFILADOCOR(parametro: string);
begin
        Self.PERFILADOCOR:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_PERFILADOCOR: string;
begin
        Result:=Self.PERFILADOCOR;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_DIVERSOCOR(parametro: string);
begin
        Self.DIVERSOCOR:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_DIVERSOCOR: string;
begin
        Result:=Self.DIVERSOCOR;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_VIDROCOR(parametro: string);
begin
        Self.VIDROCOR:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_VIDROCOR: string;
begin
        Result:=Self.VIDROCOR;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_KITBOXCOR(parametro: string);
begin
        Self.KITBOXCOR:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_KITBOXCOR: string;
begin
        Result:=Self.KITBOXCOR;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_QUANTIDADE(parametro: string);
begin
        Self.QUANTIDADE:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_QUANTIDADE: string;
begin
        Result:=Self.QUANTIDADE;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_MATERIAL(parametro: string);
begin
        Self.MATERIAL:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_MATERIAL: string;
begin
        Result:=Self.MATERIAL;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_DATAC(parametro: string);
begin
        Self.DATAC:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_DATAC: string;
begin
        Result:=Self.DATAC;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_DATAM(parametro: string);
begin
        Self.DATAM:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_DATAM: string;
begin
        Result:=Self.DATAM;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_USERM(parametro: string);
begin
        Self.USERM:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_USERM: string;
begin
        Result:=Self.USERM;
end;
procedure TObjMATERIAISDEVOLUCAO.Submit_USERC(parametro: string);
begin
        Self.USERC:=Parametro;
end;
function TObjMATERIAISDEVOLUCAO.Get_USERC: string;
begin
        Result:=Self.USERC;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjMATERIAISDEVOLUCAO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJMATERIAISDEVOLUCAO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          {If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of

          End; }
     end;

end;



end.



