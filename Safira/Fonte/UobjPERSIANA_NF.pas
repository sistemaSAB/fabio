unit UobjPersiana_NF;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UOBJPersianagrupodiametroCOR,uobjmaterial_nf;
//USES_INTERFACE



Type
   TObjPersiana_NF=class(TobjMaterial_NF)

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                persianagrupodiametrocor:TOBJpersianagrupodiametrocor ;

                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                procedure EdtpersianagrupodiametrocorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtpersianagrupodiametrocorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);



         Private
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls, UobjPERSIANA;





Function  TObjPersiana_NF.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        result:=False;
        
        result:=inherited TabelaparaObjeto;

        if (result=False)
        Then exit;

        If(FieldByName('persianagrupodiametrocor').asstring<>'')
        Then Begin
                 If (Self.persianagrupodiametrocor.LocalizaCodigo(FieldByName('persianagrupodiametrocor').asstring)=False)
                 Then Begin
                          Messagedlg('Persiana Cor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.persianagrupodiametrocor.TabelaparaObjeto;
        End;
      

        result:=True;
     End;
end;


Procedure TObjPersiana_NF.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        inherited ObjetoparaTabela;
        
        ParamByName('persianagrupodiametrocor').asstring:=Self.persianagrupodiametrocor.GET_CODIGO;
  End;
End;

//***********************************************************************

function TObjPersiana_NF.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.Get_CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Get_Codigo='0')
              Then Self.Submit_codigo(Self.Get_NovoCodigo);
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       on e:exception do
       Begin
         if (Self.Status=dsInsert)
         Then Messagedlg('Erro na  tentativa de Inserir '+#13+E.message,mterror,[mbok],0)
         Else Messagedlg('Erro na  tentativa de Editar'+#13+E.message,mterror,[mbok],0);
         exit;
       End;

 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjPersiana_NF.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        inherited ZerarTabela;

        persianagrupodiametrocor.ZerarTabela;
     End;
end;

Function TObjPersiana_NF.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
       Mensagem:=inherited VerificaBrancos;

       If (persianagrupodiametrocor.get_codigo='')
       Then Mensagem:=mensagem+'/Persiana Cor';
       
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjPersiana_NF.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
     mensagem:=inherited VerificaRelacionamentos;

     If (Self.persianagrupodiametrocor.LocalizaCodigo(Self.persianagrupodiametrocor.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Persiana Cor n�o Encontrado!';

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;
End;

function TObjPersiana_NF.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';

     Mensagem:=inherited VerificaNumericos;

     try
        If (Self.persianagrupodiametrocor.Get_Codigo<>'')
        Then Strtoint(Self.persianagrupodiametrocor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Persiana Cor';
     End;

     If (Mensagem<>'')
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjPersiana_NF.VerificaData: Boolean;
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';

     mensagem:=Inherited VerificaData;

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjPersiana_NF.VerificaFaixa: boolean;
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
        Mensagem:=Inherited VerificaFaixa;

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjPersiana_NF.LocalizaCodigo(parametro: string): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro c�digo do Persiana NF vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select '+Self.RetornaCamposSelectSQL+',persianagrupodiametrocor');
           SQL.ADD(' from  TABPersiana_NF');
           SQL.ADD(' WHERE codigo='+parametro);
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjPersiana_NF.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjPersiana_NF.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjPersiana_NF.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Owner := Owner;
        inherited create(Self.Owner);//Crio o Pai primeiro
        
        //Self.Objquery:=TIBQuery.create(nil);
        //Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.persianagrupodiametrocor:=TOBJpersianagrupodiametrocor .create;
        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABPersiana_NF('+Self.MaterialInsertSql.text+',persianagrupodiametrocor)');
                InsertSQL.add('values ('+Self.MaterialInsertSqlValues.text+',:persianagrupodiametrocor)');

                ModifySQL.clear;
                ModifySQL.add('Update TABPersiana_NF set '+Self.MaterialModifySQl.Text);
                ModifySQL.add(',persianagrupodiametrocor=:persianagrupodiametrocor where codigo=:codigo');
                
                DeleteSQL.clear;
                DeleteSql.add('Delete from TABPersiana_NF where codigo=:codigo ');
                
                Self.status          :=dsInactive;
        End;

end;
procedure TObjPersiana_NF.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjPersiana_NF.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabPersiana_NF');
     Result:=Self.ParametroPesquisa;
end;

function TObjPersiana_NF.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Persiana NF ';
end;


function TObjPersiana_NF.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENPersiana_NF,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjPersiana_NF.Free;
begin
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.persianagrupodiametrocor.FREE;
    inherited free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjPersiana_NF.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjPersiana_NF.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;


procedure TObjPersiana_NF.EdtpersianagrupodiametrocorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.persianagrupodiametrocor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.persianagrupodiametrocor.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.persianagrupodiametrocor.Persiana.Get_Nome+'-'+Self.persianagrupodiametrocor.Cor.Get_Descricao;
End;
procedure TObjPersiana_NF.EdtpersianagrupodiametrocorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.persianagrupodiametrocor.Get_Pesquisa,Self.persianagrupodiametrocor.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.persianagrupodiametrocor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.persianagrupodiametrocor.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.persianagrupodiametrocor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;

end.



