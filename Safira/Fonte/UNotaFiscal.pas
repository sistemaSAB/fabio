unit UNotaFiscal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db,
  UObjNotaFiscalCfop, Grids, UObjNotaFiscal,uobjnotafiscalobjetos,UpesquisaMenu;

type
  TFNotaFiscal = class(TForm)
    Guia: TTabbedNotebook;
    Label9: TLabel;
    LbNomeTransportadora: TLabel;
    EdtNomeTransportadora: TEdit;
    LbFreteporContaTransportadora: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdtFreteporContaTransportadora: TEdit;
    EdtPlacaVeiculoTransportadora: TEdit;
    LbPlacaVeiculoTransportadora: TLabel;
    LbUFVeiculoTransportadora: TLabel;
    EdtUFVeiculoTransportadora: TEdit;
    EdtCNPJTransportadora: TEdit;
    LbCNPJTransportadora: TLabel;
    LbIETransportadora: TLabel;
    EdtIETransportadora: TEdit;
    EdtUFTransportadora: TEdit;
    LbUFTransportadora: TLabel;
    EdtMunicipioTransportadora: TEdit;
    LbMunicipioTransportadora: TLabel;
    EdtEnderecoTransportadora: TEdit;
    LbEnderecoTransportadora: TLabel;
    LbQuantidade: TLabel;
    EdtQuantidade: TEdit;
    EdtEspecie: TEdit;
    LbMarca: TLabel;
    EdtMarca: TEdit;
    LbEspecie: TLabel;
    LbNumero: TLabel;
    edtnumerovolumes: TEdit;
    LbPesoBruto: TLabel;
    EdtPesoBruto: TEdit;
    LbPesoLiquido: TLabel;
    EdtPesoLiquido: TEdit;
    Bevel1: TBevel;
    Label13: TLabel;
    edttransportadora: TEdit;
    lbtransportadora: TLabel;
    PanelProdutos: TPanel;
    Label5: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    lbnomeproduto: TLabel;
    BtCancelar_nf: TBitBtn;
    btGravar_nf: TBitBtn;
    BtExcluir_nf: TBitBtn;
    edtvalor_nf: TEdit;
    edtquantidade_nf: TEdit;
    combotipoprodutos_nf: TComboBox;
    edtproduto_nf: TEdit;
    edtcodigo_nf: TEdit;
    StrgProdutos: TStringGrid;
    StatusBar1: TStatusBar;
    edtpesquisa_STRG_GRID: TMaskEdit;
    LbtipoCampoProduto: TListBox;
    combotributacao: TComboBox;
    EdtAliquota: TEdit;
    edtreducaobasecalculo: TEdit;
    edtaliquotacupom: TEdit;
    edtipi: TEdit;
    EdtSituacaoTributaria_TabelaA: TEdit;
    EdtSituacaoTributaria_TabelaB: TEdit;
    edtpercentualagregado: TEdit;
    edtvencimentofatura1: TMaskEdit;
    edtvencimentofatura2: TMaskEdit;
    edtvencimentofatura3: TMaskEdit;
    edtvencimentofatura4: TMaskEdit;
    edtvencimentofatura5: TMaskEdit;
    edtvalorfatura1: TMaskEdit;
    edtvalorfatura2: TMaskEdit;
    edtvalorfatura3: TMaskEdit;
    edtvalorfatura4: TMaskEdit;
    edtvalorfatura5: TMaskEdit;
    Label21: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    edtmargemvaloragregadoconsumidor: TEdit;
    edtvalorfrete_produto: TEdit;
    Label35: TLabel;
    edtvalorseguro_produto: TEdit;
    Label37: TLabel;
    panelimpostos: TPanel;
    Label17: TLabel;
    edticms_fornecedor: TEdit;
    Label22: TLabel;
    edtipi_fornecedor: TEdit;
    Label43: TLabel;
    edtpis_fornecedor: TEdit;
    Label44: TLabel;
    edtcofins_fornecedor: TEdit;
    panelcfop: TPanel;
    Label45: TLabel;
    edtcfop_fornecedor: TEdit;
    panelreferencia: TPanel;
    Label47: TLabel;
    edtproduto_ref: TEdit;
    edtnf_referencia: TEdit;
    Label46: TLabel;
    imgrodape: TImage;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lb22: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    lbLbDadosAdicionais: TLabel;
    mmoDadosAdicionais: TMemo;
    edtVALORPIS: TEdit;
    lb1: TLabel;
    edtVALORFRETE: TEdit;
    lbLbVALORFRETE: TLabel;
    edtBASECALCULOICMS: TEdit;
    lbLbBASECALCULOICMS: TLabel;
    edtcliente: TEdit;
    lb2: TLabel;
    lbnomecliente: TLabel;
    edtNATUREZAOPERACAO: TEdit;
    lbLbNATUREZAOPERACAO: TLabel;
    lb3: TLabel;
    edtCodigo: TEdit;
    edtnfe: TEdit;
    lb4: TLabel;
    edtoperacao: TEdit;
    lb5: TLabel;
    lbNomeoperacao: TLabel;
    edtNumero: TEdit;
    lb6: TLabel;
    edtnotafinal: TEdit;
    lb7: TLabel;
    cbbSituacao: TComboBox;
    lb8: TLabel;
    edtlote: TEdit;
    lb9: TLabel;
    edthorasaida: TMaskEdit;
    lb10: TLabel;
    edtdatasaida: TMaskEdit;
    lb11: TLabel;
    edtdataemissao: TMaskEdit;
    lb12: TLabel;
    cbbComboDevolucaoCliente: TComboBox;
    lb13: TLabel;
    edtfornecedor: TEdit;
    lb14: TLabel;
    lbnomefornecedor: TLabel;
    cbbcombotabeladepreco: TComboBox;
    lb15: TLabel;
    lb16: TLabel;
    edtVALORICMS_SUBST_recolhido: TEdit;
    edtBASECALCULOICMS_SUBST_recolhido: TEdit;
    lb17: TLabel;
    edtVALORICMS_SUBSTITUICAO: TEdit;
    lbLbVALORICMS_SUBSTITUICAO: TLabel;
    edtBASECALCULOICMS_SUBSTITUICAO: TEdit;
    lbLbBASECALCULOICMS_SUBSTITUICAO: TLabel;
    edtVALORICMS: TEdit;
    lbLbVALORICMS: TLabel;
    lbLbVALORSEGURO: TLabel;
    edtVALORSEGURO: TEdit;
    edtOUTRASDESPESAS: TEdit;
    lbLbOUTRASDESPESAS: TLabel;
    edtVALORTOTALIPI: TEdit;
    lbLbVALORTOTALIPI: TLabel;
    edtdesconto: TEdit;
    lb18: TLabel;
    edtVALORTOTAL: TEdit;
    lbLbVALORTOTAL: TLabel;
    edtVALORFINAL: TEdit;
    lbLbVALORFINAL: TLabel;
    rgRGTIPO: TRadioGroup;
    lstLbCfops: TListBox;
    edtVALORCOFINS_ST: TEdit;
    lb19: TLabel;
    edtVALORCOFINS: TEdit;
    lb20: TLabel;
    edtVALORPIS_ST: TEdit;
    lb21: TLabel;
    mmoOBSERVACOES: TMemo;
    lbLbOBSERVACOES: TLabel;
    bvl1: TBevel;
    bvl2: TBevel;
    bvl3: TBevel;
    edtModeloNF: TEdit;
    lb23: TLabel;
    bvl4: TBevel;
    bt1: TSpeedButton;
    Label1: TLabel;
    edtSerieNF: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure edtNumeroKeyPress(Sender: TObject; var Key: Char);
    procedure EdtCodigoVendaRasuradaKeyPress(Sender: TObject;
      var Key: Char);
    procedure EdtNumeroNovaNotaKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure edttransportadoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edttransportadoraExit(Sender: TObject);
    procedure edtclienteExit(Sender: TObject);
    procedure edtclienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure combotipoprodutos_nfChange(Sender: TObject);
    procedure edtproduto_nfExit(Sender: TObject);
    procedure edtproduto_nfKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtquantidade_nfKeyPress(Sender: TObject; var Key: Char);
    procedure edtvalor_nfKeyPress(Sender: TObject; var Key: Char);
    procedure btGravar_nfClick(Sender: TObject);
    procedure BtCancelar_nfClick(Sender: TObject);
    procedure BtExcluir_nfClick(Sender: TObject);
    procedure StrgProdutosDblClick(Sender: TObject);
    procedure StrgProdutosEnter(Sender: TObject);
    procedure StrgProdutosKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure StrgProdutosKeyPress(Sender: TObject; var Key: Char);
    procedure edtpesquisa_STRG_GRIDKeyPress(Sender: TObject;
      var Key: Char);
    procedure btrelatoriosClick(Sender: TObject);
    procedure btopcoesClick(Sender: TObject);
    procedure lstLbCfopsKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtoperacaoExit(Sender: TObject);
    procedure edtoperacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtfornecedorExit(Sender: TObject);
    procedure edtfornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edticms_fornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edticms_fornecedorExit(Sender: TObject);
    procedure edtipi_fornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtipi_fornecedorExit(Sender: TObject);
    procedure edtpis_fornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtpis_fornecedorExit(Sender: TObject);
    procedure edtcofins_fornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcofins_fornecedorExit(Sender: TObject);
    procedure edtcfop_fornecedorExit(Sender: TObject);
    procedure edtcfop_fornecedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtnf_referenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtnf_referenciaExit(Sender: TObject);
    procedure edtproduto_refKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtproduto_refExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure StrgProdutosDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure edtoperacaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtfornecedorKeyPress(Sender: TObject; var Key: Char);
    procedure edtclienteKeyPress(Sender: TObject; var Key: Char);
    procedure edtModeloNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure bt1Click(Sender: TObject);
    procedure edtoperacaoDblClick(Sender: TObject);
    procedure edtfornecedorDblClick(Sender: TObject);
    procedure edtModeloNFDblClick(Sender: TObject);
    procedure edtclienteDblClick(Sender: TObject);
    procedure edtproduto_nfDblClick(Sender: TObject);
    procedure edtcfop_fornecedorDblClick(Sender: TObject);
    procedure edticms_fornecedorDblClick(Sender: TObject);
    procedure edtipi_fornecedorDblClick(Sender: TObject);
    procedure edtpis_fornecedorDblClick(Sender: TObject);
    procedure edtcofins_fornecedorDblClick(Sender: TObject);
  private
         Usouf9napesquisa:Boolean;
         ObjNotaFiscalObjetos:TObjNotaFiscalObjetos;
         ppermiteexcluircfop,PvendasEstado:Boolean;
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         procedure AtualizaGrid;
         //Procedure ResgataCFOPS;

         procedure CalculaDiverso;
         procedure CalculaKitBox;
         procedure CalculaPerfilado;
         procedure CalculaPersianaGrupoDiametro;
         procedure CalculaVidro;
         Procedure CalculaFerragem;
         
    { Private declarations }
  public
    { Public declarations }
    permiteAlteracao:Boolean;
  end;

var
  FNotaFiscal: TFNotaFiscal;

implementation

uses UessencialGlobal, Upesquisa, UDataModulo, Uformata_String_Grid,
  UobjVIDRO_NF, UobjDIVERSO_NF, UobjDIVERSOCOR, UobjDIVERSO,
  UobjPERSIANA_NF, UobjCOR, UobjMaterial_NF, UobjOPERACAONF,
  UobjFORNECEDOR, UobjIMPOSTO_ICMS, UFiltraImp, UescolheImagemBotao,
  UMostraBarraProgresso, UmostraStringList, Uprincipal, UAjuda;

{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFNotaFiscal.ControlesParaObjeto: Boolean;
Begin
     Try
        With Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL do
        Begin
             Submit_CODIGO(edtCODIGO.text);
             Submit_Numero(edtNumero.text);

             If (CBBSituacao.itemindex=-1)
             Then submit_Situacao(' ')
             Else submit_Situacao(CBBsituacao.text[1]);

             If (RGRGTIPO.itemindex=0)
             Then submit_TIPO('E')
             Else submit_TIPO('S');


             Submit_NumeroFinal(edtnotafinal.text);
             Submit_DataEMISSAO(edtdataemissao.text);
             Submit_VALORTOTAL(edtVALORTOTAL.text);
             Submit_Desconto(edtdesconto.Text);
             Submit_NATUREZAOPERACAO(edtNATUREZAOPERACAO.text);
             Submit_BASECALCULOICMS(edtBASECALCULOICMS.text);
             Submit_VALORICMS(edtVALORICMS.text);
             Submit_BASECALCULOICMS_SUBSTITUICAO(edtBASECALCULOICMS_SUBSTITUICAO.text);
             Submit_VALORICMS_SUBSTITUICAO(edtVALORICMS_SUBSTITUICAO.text);
             Submit_VALORFRETE(edtVALORFRETE.text);
             Submit_VALORSEGURO(edtVALORSEGURO.text);
             Submit_OUTRASDESPESAS(edtOUTRASDESPESAS.text);
             Submit_VALORTOTALIPI(edtVALORTOTALIPI.text);
             Submit_NomeTransportadora(edtNomeTransportadora.text);
             Submit_FreteporContaTransportadora(edtFreteporContaTransportadora.text);
             Submit_PlacaVeiculoTransportadora(edtPlacaVeiculoTransportadora.text);
             Submit_UFVeiculoTransportadora(edtUFVeiculoTransportadora.text);
             Submit_CNPJTransportadora(edtCNPJTransportadora.text);
             Submit_EnderecoTransportadora(edtEnderecoTransportadora.text);
             Submit_MunicipioTransportadora(edtMunicipioTransportadora.text);
             Submit_UFTransportadora(edtUFTransportadora.text);
             Submit_IETransportadora(edtIETransportadora.text);
             Submit_Quantidade(edtQuantidade.text);
             Submit_Especie(edtEspecie.text);
             Submit_Marca(edtMarca.text);
             Submit_NumeroVolumes(edtNumeroVolumes.text);
             Submit_PesoBruto(edtPesoBruto.text);
             Submit_PesoLiquido(edtPesoLiquido.text);
             Submit_DadosAdicionais(mmoDadosAdicionais.text);
             Submit_OBSERVACOES(mmoOBSERVACOES.text);
             transportadora.Submit_CODIGO(edttransportadora.Text);
             Submit_datasaida(edtdatasaida.text);
             Submit_Horasaida(edthorasaida.Text);
             Cliente.Submit_CODIGO(edtcliente.Text);
             Fornecedor.Submit_CODIGO(edtFornecedor.Text);
             Submit_Lote(edtlote.text);
             Submit_serienf(edtSerieNF.Text);


             Submit_BASECALCULOICMS_SUBST_recolhido(edtBASECALCULOICMS_SUBST_recolhido.text);
             Submit_VALORICMS_SUBST_recolhido(edtVALORICMS_SUBST_recolhido.text);
             Submit_IcmsRecolhidoPelaEmpresa('N');//nao usa mais

             Submit_DevolucaoCliente(Submit_ComboBox(CBBcomboDevolucaoCliente));


             if (CBBcombotabeladepreco.ItemIndex=0)
             then Submit_TabeladePreco('INSTALADO')
             Else
                 if (CBBcombotabeladepreco.ItemIndex=1)
                 then Submit_TabeladePreco('FORNECIDO')
                 Else
                      if (CBBcombotabeladepreco.ItemIndex=2)
                      then Submit_TabeladePreco('RETIRADO')
                      Else Submit_TabeladePreco('');
                      
             Submit_vencimentofatura1(edtvencimentofatura1.text);
             Submit_vencimentofatura2(edtvencimentofatura2.text);
             Submit_vencimentofatura3(edtvencimentofatura3.text);
             Submit_vencimentofatura4(edtvencimentofatura4.text);
             Submit_vencimentofatura5(edtvencimentofatura5.text);

             Submit_valorfatura1(edtvalorfatura1.text);
             Submit_valorfatura2(edtvalorfatura2.text);
             Submit_valorfatura3(edtvalorfatura3.text);
             Submit_valorfatura4(edtvalorfatura4.text);
             Submit_valorfatura5(edtvalorfatura5.text);

             Nfe.Submit_codigo(edtnfe.Text);

             Submit_valorpis(edtVALORPIS.Text);
             Submit_valorpis_st(edtVALORPIS_st.Text);
             Submit_valorCOFINS(edtVALORCOFINS.Text);
             Submit_valorCOFINS_ST(edtVALORCOFINS_ST.Text);

             Operacao.Submit_CODIGO(edtoperacao.Text);
             Submit_ModeloNF(edtModeloNF.Text);
              
             result:=true;
        End;
        Except
          result:=False;
        End;
End;

function TFNotaFiscal.ObjetoParaControles: Boolean;
Begin
  Try
     With Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL do
     Begin

             edtCODIGO.text:=Get_CODIGO;
             edtNumero.text:=Get_Numero;


             If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_TIPO='E')
             Then RGRGTIPO.itemindex:=0//entrada
             Else RGRGTIPO.itemindex:=1;//sa�da


             CBBSituacao.Color:=clWindow;
             CBBSituacao.Font.color:=clNavy;
             
             If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_Situacao='I')
             Then CBBSituacao.itemindex:=0
             Else
                If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_Situacao='R')
                Then CBBSituacao.itemindex:=1
                Else
                   If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_Situacao='N')
                   Then CBBSituacao.itemindex:=2
                   Else
                      If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_Situacao='C')
                      Then Begin
                                  CBBSituacao.itemindex:=3;
                                  CBBSituacao.Color:=clRed;
                                  CBBSituacao.Font.color:=clWhite;
                                  CBBSituacao.Font.Style:=[fsBold];

                      End
                      Else
                          If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_Situacao='M')
                          Then CBBSituacao.itemindex:=4
                          Else
                              If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_Situacao='P')
                              Then CBBSituacao.itemindex:=5
                              Else
                                  If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.get_Situacao='T')
                                  Then CBBSituacao.itemindex:=6
                                  Else CBBSituacao.itemindex:=-1;

             edtdataemissao.text:=Get_DataEMISSAO;
             EdtVALORTOTAL.text:=Get_VALORTOTAL;
             EdtDesconto.text:=Get_desconto;
             EdtVALORFINAL.text:=Get_VALORFINAL;
             EdtNATUREZAOPERACAO.text:=Get_NATUREZAOPERACAO;
             EdtBASECALCULOICMS.text:=Get_BASECALCULOICMS;
             EdtVALORICMS.text:=Get_VALORICMS;
             EdtBASECALCULOICMS_SUBSTITUICAO.text:=Get_BASECALCULOICMS_SUBSTITUICAO;
             EdtVALORICMS_SUBSTITUICAO.text:=Get_VALORICMS_SUBSTITUICAO;
             EdtVALORFRETE.text:=Get_VALORFRETE;
             EdtVALORSEGURO.text:=Get_VALORSEGURO;
             EdtOUTRASDESPESAS.text:=Get_OUTRASDESPESAS;
             EdtVALORTOTALIPI.text:=Get_VALORTOTALIPI;
             EdtNomeTransportadora.text:=Get_NomeTransportadora;
             EdtFreteporContaTransportadora.text:=Get_FreteporContaTransportadora;
             EdtPlacaVeiculoTransportadora.text:=Get_PlacaVeiculoTransportadora;
             EdtUFVeiculoTransportadora.text:=Get_UFVeiculoTransportadora;
             EdtCNPJTransportadora.text:=Get_CNPJTransportadora;
             EdtEnderecoTransportadora.text:=Get_EnderecoTransportadora;
             EdtMunicipioTransportadora.text:=Get_MunicipioTransportadora;
             EdtUFTransportadora.text:=Get_UFTransportadora;
             EdtIETransportadora.text:=Get_IETransportadora;
             EdtQuantidade.text:=Get_Quantidade;
             EdtEspecie.text:=Get_Especie;
             EdtMarca.text:=Get_Marca;
             EdtNumeroVolumes.text:=Get_NumeroVolumes;
             EdtPesoBruto.text:=Get_PesoBruto;
             EdtPesoLiquido.text:=Get_PesoLiquido;
             mmoDadosAdicionais.text:=Get_DadosAdicionais;
             mmoOBSERVACOES.text:=Get_OBSERVACOES;
             edttransportadora.Text:=Transportadora.Get_CODIGO;
             lbtransportadora.Caption:=Transportadora.Get_NOME;
             edtlote.text:=Get_lote;
             edtSerieNF.Text:=get_serienf;

             edtdatasaida.text:=Get_datasaida;
             edthorasaida.Text:=get_Horasaida;
             edtcliente.Text:=Cliente.Get_CODIGO;
             lbnomecliente.caption:=Cliente.Get_nome;
             edtFornecedor.Text:=Fornecedor.Get_CODIGO;
             lbnomeFornecedor.caption:=Fornecedor.Get_RazaoSocial;


             edtnfe.text:=Nfe.Get_codigo;


             //Self.ResgataCfops;



             edtBASECALCULOICMS_SUBST_recolhido.text:=Get_BASECALCULOICMS_SUBST_recolhido;
             edtVALORICMS_SUBST_recolhido.text:=Get_VALORICMS_SUBST_recolhido;

             

             if (Get_DevolucaoCliente='S')
             then CBBcomboDevolucaoCliente.itemindex:=1
             Else CBBcomboDevolucaoCliente.itemindex:=0;


             if (get_TabeladePreco='INSTALADO')
             Then CBBcombotabeladepreco.ItemIndex:=0
             Else
                 if (get_TabeladePreco='FORNECIDO')
                 Then CBBcombotabeladepreco.ItemIndex:=1
                 Else
                      if (get_TabeladePreco='RETIRADO')
                      then CBBcombotabeladepreco.ItemIndex:=2
                      Else CBBcombotabeladepreco.ItemIndex:=-1;


             edtvencimentofatura1.Text:=Get_vencimentofatura1;
             edtvencimentofatura2.Text:=Get_vencimentofatura2;
             edtvencimentofatura3.Text:=Get_vencimentofatura3;
             edtvencimentofatura4.Text:=Get_vencimentofatura4;
             edtvencimentofatura5.Text:=Get_vencimentofatura5;


             edtvalorfatura1.Text:=Get_valorfatura1;
             edtvalorfatura2.Text:=Get_valorfatura2;
             edtvalorfatura3.Text:=Get_valorfatura3;
             edtvalorfatura4.Text:=Get_valorfatura4;
             edtvalorfatura5.Text:=Get_valorfatura5;

             edtVALORPIS.Text:=get_valorpis;
             edtVALORPIS_st.Text:=get_valorpis_st;
             edtVALORCOFINS.Text:=get_valorCOFINS;
             edtVALORCOFINS_ST.Text:=get_valorCOFINS_ST;

             Edtoperacao.Text:=Operacao.Get_CODIGO;
             Lbnomeoperacao.caption:=Operacao.Get_NOME;

             edtModeloNF.Text:=Get_ModeloNF;
             result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFNotaFiscal.TabelaParaControles: Boolean;
begin
     If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFNotaFiscal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Self.ObjNotaFiscalObjetos<>Nil)
     Then Begin
               If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.status<>dsinactive)
               Then Begin
                      Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
                      abort;
                      exit;
               End;
     End;

     Try
        Self.ObjNotaFiscalObjetos.free;
     Except
     End;

     Action := caFree;
     Self := nil;
end;

procedure TFNotaFiscal.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFNotaFiscal.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.LimpaLabels;
     habilita_campos(Self);
    
     rgrgTIPO.ItemIndex:=1;
     edtcodigo.text:='0';
     edtcodigo.enabled:=False;
     CBBSituacao.enabled:=False;
     CBBSituacao.itemindex:=2;
     edtvalorfinal.Text:='0';
     edtdesconto.text:='0';
     EdtVALORFRETE.text:='0';
     EdtVALORSEGURO.text:='0';
     EdtOUTRASDESPESAS.text:='0';
     EdtVALORTOTALIPI.text:='0';
     EdtBASECALCULOICMS.text:='0';
     EdtVALORICMS.text:='0';
     EdtBASECALCULOICMS_SUBSTITUICAO.text:='0';
     EdtVALORICMS_SUBSTITUICAO.text:='0';


     edtnfe.enabled:=False;
     EdtVALORFINAL.enabled:=False;
     EdtVALORTOTAL.enabled:=False;
     EdtBASECALCULOICMS.enabled:=False;
     EdtBASECALCULOICMS_SUBSTITUICAO.enabled:=False;
     EdtVALORICMS.enabled:=False;
     EdtVALORICMS_SUBSTITUICAO.enabled:=False;
     edtBASECALCULOICMS_SUBST_recolhido.enabled:=False;
     edtVALORICMS_SUBST_recolhido.enabled:=False;


     edtBASECALCULOICMS_SUBST_recolhido.enabled:=False;
     edtVALORICMS_SUBST_recolhido.enabled:=False;
     CBBcomboDevolucaoCliente.enabled:=False;

     EdtFreteporContaTransportadora.text:='9';

     EdtVALORTOTALIPI.enabled:=False;
     EdtVALORFRETE.enabled:=False;
     EdtVALORSEGURO.enabled:=False;

     edtVALORPIS.text:='0';
     edtVALORPIS.Enabled:=False;
     edtVALORPIS_ST.text:='0';
     edtVALORPIS_ST.Enabled:=False;

     edtVALORCOFINS.text:='0';
     edtVALORCOFINS.Enabled:=False;
     edtVALORCOFINS_ST.text:='0';
     edtVALORCOFINS_ST.Enabled:=False;



     btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorioS.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;


     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.status:=dsInsert;
     Guia.pageindex:=0;
     EdtNumero.setfocus;

     edtlote.Text:='0';

end;


procedure TFNotaFiscal.btalterarClick(Sender: TObject);
begin
    


    if (ObjPermissoesUsoGlobal.ValidaPermissao('ALTERAR SITUA��O DA NF MANUALMENTE')=False)
    Then exit;

    self.permiteAlteracao:=True;

    if not self.permiteAlteracao then
    begin

      MensagemAviso ('N�o � possivel alterar a nota fiscal manualmente');
      Exit;

    end;
    If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
              if (edtnfe.Text<>'')
              Then Begin
                        MensagemErro('Foi gerado uma NFE, n�o � poss�vel alterar a NF');
                        exit;
              End;

              if(ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_Situacao <> 'N')
              then Exit;

             Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Status:=dsEdit;
             guia.pageindex:=0;
             CBBSituacao.Enabled:=True;

             habilita_campos(self);


             btnovo.Visible :=false;
             btalterar.Visible:=false;
             btpesquisar.Visible:=false;
             btrelatorioS.Visible:=false;
             btexcluir.Visible:=false;
             btsair.Visible:=false;
             btopcoes.Visible :=false;
             edtvalorfinal.Text:='0';

             edtnfe.enabled:=False;
             EdtVALORFINAL.enabled:=False;
             EdtVALORTOTAL.enabled:=False;
             EdtBASECALCULOICMS.enabled:=False;
             EdtBASECALCULOICMS_SUBSTITUICAO.enabled:=False;
             EdtVALORICMS.enabled:=False;
             EdtVALORICMS_SUBSTITUICAO.enabled:=False;
                

             EdtVALORTOTALIPI.enabled:=False;
             EdtVALORFRETE.enabled:=False;
             EdtVALORSEGURO.enabled:=False;

             edtVALORPIS.Enabled:=False;
             edtVALORPIS_ST.Enabled:=False;
             edtVALORCOFINS.Enabled:=False;
             edtVALORCOFINS_ST.Enabled:=False;
             Edtoperacao.setfocus;
    End;
end;

procedure TFNotaFiscal.btgravarClick(Sender: TObject);
var
  cont,notaInicial,notaFinal:Integer;
begin

     {jonas 12/04/2011 09:28}

    if (edtModeloNF.Text = '2') then
    begin

        if (EdtNumero.Text = '') then
        begin

          MensagemAviso ('Requer n�mero inicial da nota');
          Exit;

        end;

        if (edtnotafinal.Text = '') then
        begin

          MensagemAviso ('Requer n�mero final da nota');
          Exit;

        end;

        try
          notaInicial:=StrToInt (EdtNumero.Text);
        except
          MensagemErro ('N�mero inicial invalido');
          Exit;
        end;

        try
          notaFinal:=StrToInt(edtnotafinal.Text);
        except
          MensagemErro ('N�mero final invalido');
          Exit;
        end;


        FMostraBarraProgresso.ConfiguracoesIniciais(notaFinal-notaInicial,0);
        FMostraBarraProgresso.Lbmensagem.caption:='Cadastrando Novas NFe';

        FmostraStringList.Memo.Lines.clear;

       try

         for cont:=notaInicial to notaFinal do
         begin

              FMostraBarraProgresso.IncrementaBarra1(1);
              FMostraBarraProgresso.show;

              if (ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.LocalizaCodigo (inttostr(cont)) = True) then
                FmostraStringList.Memo.Lines.add('NFe C�digo '+inttostr(cont)+' j� se encontra cadastrada')
              else
              Begin
              
                ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.ZerarTabela;
                ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Status:=dsInsert;
                ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_Codigo(inttostr(cont));
                ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Submit_StatusNota('A');


                if not (ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.Salvar(false)) then
                  FmostraStringList.Memo.Lines.add('Erro na tentativa de Salvar a NFe C�digo '+inttostr(cont));

              End;
         end;

        if (FmostraStringList.Memo.Lines.text <> '') then FmostraStringList.showmodal;

       Finally
        FMostraBarraProgresso.Close;
       End;

    end;

     {-}


     If Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Status=dsInactive Then
     exit;

     if not ControlesParaObjeto Then
     begin
      Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
      exit;
     end;
     
     if not (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.salvar(true)) then
     begin
       FDataModulo.IBTransaction.RollbackRetaining;
       exit;
     end;


     habilita_botoes(Self);
     desabilita_campos(Self);
     if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(EdtCodigo.Text)=True)
     Then Begin
               limpaedit(Self);
               Self.LimpaLabels;
               Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
               Self.TabelaParaControles;
     End
     Else Begin
               limpaedit(Self);
               Self.LimpaLabels;
     end;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true ;
     btopcoes.Visible :=true;

end;

procedure TFNotaFiscal.btexcluirClick(Sender: TObject);
begin

       if (edtnfe.Text<>'')
       Then Begin
                 MensagemErro('Foi gerado uma NFE, n�o � poss�vel excluir a NF');
                 exit;
       End;

     If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     
     
     If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.exclui(edtcodigo.text,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.LimpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFNotaFiscal.btcancelarClick(Sender: TObject);
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.cancelar;
      btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorios.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true ;
     btopcoes.Visible :=true;
     limpaedit(Self);
     Self.LimpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFNotaFiscal.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFNotaFiscal.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_pesquisa,Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.status<>dsinactive
                                  then exit;

                                  If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.LimpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFNotaFiscal.edtNumeroKeyPress(Sender: TObject; var Key: Char);
begin
        If not (key in ['0'..'9',#8])
        then key:=#0
end;

procedure TFNotaFiscal.EdtCodigoVendaRasuradaKeyPress(Sender: TObject;
  var Key: Char);
begin
        If not (key in ['0'..'9',',',#8])
        then key:=#0
        Else
        If Key='.'
        Then key:=',';
end;

procedure TFNotaFiscal.EdtNumeroNovaNotaKeyPress(Sender: TObject;
  var Key: Char);
begin
        If not (key in ['0'..'9',',',#8])
        then key:=#0
        Else
        If Key='.'
        Then key:=',';
end;

procedure TFNotaFiscal.FormShow(Sender: TObject);
begin

    {UessencialGlobal.PegaCorForm(Self);}

    if (self.Tag <> 0) then
    begin

      if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo (IntToStr (Self.Tag))) then
      begin

        Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
        Self.ObjetoParaControles;
        
      end;

    end;

end;

procedure TFNotaFiscal.edttransportadoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.EdtTransportadoraKeyDown(sender,key,shift,lbtransportadora);
end;

procedure TFNotaFiscal.edttransportadoraExit(Sender: TObject);
begin
    Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.edttransportadoraexit(sender,lbtransportadora);
end;
procedure TFNotaFiscal.edtclienteExit(Sender: TObject);
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.EdtClienteExit(sender,lbnomecliente);
end;

procedure TFNotaFiscal.edtclienteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.EdtClienteKeyDown(sender,key,shift,lbnomecliente);
end;

procedure TFNotaFiscal.LimpaLabels;
begin
     lbtransportadora.Caption:='';
     lbnomecliente.Caption:='';
     lbnomefornecedor.caption:='';
     lstLbCfops.Items.clear;
     Lbnomeoperacao.caption:='';
end;

procedure TFNotaFiscal.FormCreate(Sender: TObject);
begin
     limpaedit(Self);
     Self.LimpaLabels;
     desabilita_campos(Self);
     Guia.PageIndex:=0;

     Try
          Self.ObjNotaFiscalObjetos:=TObjNotaFiscalObjetos.create(self);
     Except
           Messagedlg('Erro na inicializa��o do Objeto de NotaFiscal X CFOP ',mterror,[mbok],0);
           Self.close;
     End;

     If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.VerificaPermissao=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     FescolheImagemBotao.PegaFiguraBotaopequeno(btGravar_nf,'BOTAOINSERIR.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(BtCancelar_nf,'BOTAOCANCELARPRODUTO.BMP');
     FescolheImagemBotao.PegaFiguraBotaopequeno(BtExcluir_nf,'BOTAORETIRAR.BMP');


     ppermiteexcluircfop:=false;

     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('EXCLUIR CFOP NA NOTA FISCAL')=True)
     Then ppermiteexcluircfop:=True;

     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE NOTA FISCAL')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);




end;

procedure TFNotaFiscal.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (NewTab=1)
     Then Begin
               if (EdtCODIGO.Text='') or (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Status=dsinsert)
               Then Begin
                          AllowChange:=False;
                          exit;
               end;



               if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Nfe.get_codigo<>'')
               Then Begin
                         desab_botoes(PanelProdutos);
                         desabilita_campos(PanelProdutos);
                         MensagemAviso('Foi gerado uma NFe n�o � poss�vel alterar a NF');
               End
               else Begin

                       if ((Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.get_codigo='')
                       and (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Fornecedor.get_codigo=''))
                       then Begin
                                 Mensagemerro('Escolha um cliente ou Fornecedor para a Nota Fiscal');
                                 AllowChange:=False;
                                 exit;
                       End;

                       if ((Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.get_codigo<>'')
                       and (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Fornecedor.get_codigo<>''))
                       then Begin
                                 Mensagemerro('Escolha somente um cliente ou um Fornecedor para a Nota Fiscal');
                                 AllowChange:=False;
                                 exit;
                       End;


                       if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Fornecedor.get_codigo<>'')
                       and (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_DevolucaoCliente='S')
                       then Begin
                                 Mensagemerro('N�o � poss�vel emitir uma NF de Devolu��o de CLiente para um fornecedor');
                                 AllowChange:=False;
                                 exit;
                       End;


                       if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Operacao.get_codigo='')
                       then Begin
                                 Mensagemerro('Escolha a opera��o da Nota Fiscal');
                                 AllowChange:=False;
                                 exit;
                       End;

                        habilita_botoes(PanelProdutos);
                        habilita_campos(PanelProdutos);
                        habilita_campos(panelimpostos);
                        habilita_campos(panelcfop);
                        habilita_campos(panelreferencia);

                        panelimpostos.Visible:=False;
                        panelcfop.Visible:=False;
                        panelreferencia.Visible:=False;



                        PvendasEstado:=True;
                        if  (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Cliente.Get_Estado<>ESTADOSISTEMAGLOBAL)
                        Then PvendasEstado:=False;

                        if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Fornecedor.Get_Codigo<>'')
                        Then BEgin
                                  panelimpostos.Visible:=true;
                                  panelcfop.Visible:=true;
                        End;

                        if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_DevolucaoCliente='S')
                        then Begin
                                  panelcfop.Visible:=true;
                                  panelreferencia.Visible:=true;
                                  edtproduto_nf.Enabled:=False;
                                  edtvalor_nf.Enabled:=False;
                                  edtvalorfrete_produto.Enabled:=False;
                                  edtvalorseguro_produto.Enabled:=False;
                        End;


                        combotipoprodutos_nf.ItemIndex:=0;
                        combotipoprodutos_nfChange(sender);


                        edtvalorfrete_produto.Text:='0';
                        edtvalorseguro_produto.Text:='0';
               End;
               limpaedit(panelprodutos);
               lbnomeproduto.caption:='';
               edtpercentualagregado.text:='0';
               Usouf9napesquisa:=False;
               Self.Atualizagrid;
               exit;
     End
     Else Begin
               if (NewTab=0)
               Then Begin
                         if ((EdtCodigo.Text<>'')
                         and (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Status=dsinactive))
                         Then Begin
                                   Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(EdtCodigo.text);
                                   Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
                                   Self.TabelaParaControles;
                         End;
               End;
     End;
end;

procedure TFNotaFiscal.AtualizaGrid;
var
temp1,temp2:String;
begin
   Self.ObjNotaFiscalObjetos.Atualizagrid(StrgProdutos,edtcodigo.text,lbtipocampoproduto.items);
   StrgProdutos.Row:=StrgProdutos.RowCount-1;
   edtpesquisa_STRG_GRID.enabled:=True;
   edtpesquisa_STRG_GRID.Visible:=False;
   StrgProdutos.Col:=0;
   Ordena_StringGrid(StrgProdutos,LbtipoCampoProduto.Items);
   StrgProdutos.Col:=0;
end;


procedure TFNotaFiscal.combotipoprodutos_nfChange(Sender: TObject);
begin
     edtproduto_nf.text:='';
end;

procedure TFNotaFiscal.edtproduto_nfExit(Sender: TObject);
begin
     lbnomeproduto.caption:='';

     if (edtproduto_nf.Text='')
     Then exit;

     Case combotipoprodutos_nf.ItemIndex of
     0:Begin//diversos
            if (Self.ObjNotaFiscalObjetos.ObjDiverso_NF.DiversoCor.LocalizaCodigo(edtproduto_nf.Text)=False)
            Then Begin
                      edtproduto_nf.Text:='';
                      exit;
            End
            Else Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DiversoCor.TabelaparaObjeto;
            lbnomeproduto.caption:=CompletaPalavra(Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DiversoCor.Diverso.Get_Descricao,50,' ')+' Cor '+Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DiversoCor.Cor.Get_Descricao;

            if (edtvalor_nf.Text<>'')
            Then Begin
                      if (MensagemPergunta('Alguns dados j� est�o preenchidos, deseja alter�-los com o valor do produto escolhido?')=mrno)
                      Then exit;
            End;

            //*******************************************************************
            
            EdtSituacaoTributaria_TabelaA.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.SituacaoTributaria_TabelaA.get_codigo;
            EdtSituacaoTributaria_TabelaB.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.SituacaoTributaria_TabelaB.get_codigo;


            edtpercentualagregado.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_percentualagregado;
            edtipi.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_ipi;
            Self.CalculaDiverso;//calcula preco

            if (PvendasEstado=True)
            Then Begin
                      combotributacao.itemindex:=2;

                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_Aliquota_ICMS_Estado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_Reducao_BC_ICMS_estado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_Aliquota_ICMS_Cupom_Estado;


                      //edtvalor_nf.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.get_

                      if (Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_IsentoICMS_Estado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_SubstituicaoICMS_Estado='S')
                      Then combotributacao.itemindex:=1;





            End
            Else Begin
                      combotributacao.itemindex:=2;

                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_Aliquota_ICMS_foraestado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_Reducao_BC_ICMS_foraestado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_Aliquota_ICMS_Cupom_foraestado;

                      if (Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_IsentoICMS_foraestado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.Diverso.Get_SubstituicaoICMS_foraestado='S')
                      Then combotributacao.itemindex:=1;
            End;
            //******************************************************************

            
     End;
     1:Begin//ferragem
            if (Self.ObjNotaFiscalObjetos.Objferragem_NF.ferragemCor.LocalizaCodigo(edtproduto_nf.Text)=False)
            Then Begin
                      edtproduto_nf.Text:='';
                      exit;
            End
            Else Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCor.TabelaparaObjeto;

            lbnomeproduto.caption:=CompletaPalavra(Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCor.ferragem.Get_Descricao,50,' ')+' Cor '+Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCor.Cor.Get_Descricao;


            //*******************************************************************
            EdtSituacaoTributaria_TabelaA.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.SituacaoTributaria_TabelaA.get_codigo;
            EdtSituacaoTributaria_TabelaB.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.SituacaoTributaria_TabelaB.get_codigo;

            edtpercentualagregado.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_percentualagregado;
            edtipi.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_ipi;
            Self.Calculaferragem;//calcula preco

            if (PvendasEstado=True)
            Then Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_Aliquota_ICMS_Estado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_Reducao_BC_ICMS_estado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_Aliquota_ICMS_Cupom_Estado;

                      if (Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_IsentoICMS_Estado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_SubstituicaoICMS_Estado='S')
                      Then combotributacao.itemindex:=1;

            End
            Else Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_Aliquota_ICMS_foraestado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_Reducao_BC_ICMS_foraestado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_Aliquota_ICMS_Cupom_foraestado;

                      if (Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_IsentoICMS_foraestado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objferragem_nf.ferragemCOR.ferragem.Get_SubstituicaoICMS_foraestado='S')
                      Then combotributacao.itemindex:=1;
            End;
            //******************************************************************
     End;
     2:Begin//KitBox
            if (Self.ObjNotaFiscalObjetos.Objkitbox_NF.kitboxCor.LocalizaCodigo(edtproduto_nf.Text)=False)
            Then Begin
                      edtproduto_nf.Text:='';
                      exit;
            End
            Else Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCor.TabelaparaObjeto;
            lbnomeproduto.caption:=CompletaPalavra(Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCor.kitbox.Get_Descricao,50,' ')+' Cor '+Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCor.Cor.Get_Descricao;



            //*******************************************************************
            EdtSituacaoTributaria_TabelaA.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.SituacaoTributaria_TabelaA.get_codigo;
            EdtSituacaoTributaria_TabelaB.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.SituacaoTributaria_TabelaB.get_codigo;
            edtpercentualagregado.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_percentualagregado;
            edtipi.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_ipi;
            Self.Calculakitbox;//calcula preco

            if (PvendasEstado=True)
            Then Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_Aliquota_ICMS_Estado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_Reducao_BC_ICMS_estado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_Aliquota_ICMS_Cupom_Estado;

                      if (Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_IsentoICMS_Estado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_SubstituicaoICMS_Estado='S')
                      Then combotributacao.itemindex:=1;

            End
            Else Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_Aliquota_ICMS_foraestado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_Reducao_BC_ICMS_foraestado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_Aliquota_ICMS_Cupom_foraestado;

                      if (Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_IsentoICMS_foraestado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objkitbox_nf.kitboxCOR.kitbox.Get_SubstituicaoICMS_foraestado='S')
                      Then combotributacao.itemindex:=1;
            End;
            //******************************************************************

     End;
     3:Begin//Perfilado
            if (Self.ObjNotaFiscalObjetos.Objperfilado_NF.perfiladoCor.LocalizaCodigo(edtproduto_nf.Text)=False)
            Then Begin
                      edtproduto_nf.Text:='';
                      exit;
            End
            Else Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCor.TabelaparaObjeto;
            lbnomeproduto.caption:=CompletaPalavra(Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCor.perfilado.Get_Descricao,50,' ')+' Cor '+Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCor.Cor.Get_Descricao;

            //*******************************************************************
            EdtSituacaoTributaria_TabelaA.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.SituacaoTributaria_TabelaA.get_codigo;
            EdtSituacaoTributaria_TabelaB.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.SituacaoTributaria_TabelaB.get_codigo;
            edtpercentualagregado.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_percentualagregado;
            edtipi.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_ipi;
            Self.Calculaperfilado;//calcula preco

            if (PvendasEstado=True)
            Then Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_Aliquota_ICMS_Estado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_Reducao_BC_ICMS_estado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_Aliquota_ICMS_Cupom_Estado;

                      if (Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_IsentoICMS_Estado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_SubstituicaoICMS_Estado='S')
                      Then combotributacao.itemindex:=1;

            End
            Else Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_Aliquota_ICMS_foraestado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_Reducao_BC_ICMS_foraestado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_Aliquota_ICMS_Cupom_foraestado;

                      if (Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_IsentoICMS_foraestado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objperfilado_nf.perfiladoCOR.perfilado.Get_SubstituicaoICMS_foraestado='S')
                      Then combotributacao.itemindex:=1;
            End;
            //******************************************************************

     End;
     4:Begin//Persiana
           if (Self.ObjNotaFiscalObjetos.ObjPersiana_NF.persianagrupodiametroCor.LocalizaCodigo(edtproduto_nf.Text)=False)
            Then Begin
                      edtproduto_nf.Text:='';
                      exit;
            End
            Else Self.ObjNotaFiscalObjetos.ObjPersiana_NF.persianagrupodiametroCor.TabelaparaObjeto;
            lbnomeproduto.caption:=CompletaPalavra(Self.ObjNotaFiscalObjetos.ObjPersiana_NF.persianagrupodiametroCor.Persiana.Get_Nome,50,' ')+' Cor '+Self.ObjNotaFiscalObjetos.ObjPersiana_NF.PERSIANAGRUPODIAMETROCOR.Cor.Get_Descricao;


            //*******************************************************************
            EdtSituacaoTributaria_TabelaA.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.SituacaoTributaria_TabelaA.get_codigo;
            EdtSituacaoTributaria_TabelaB.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.SituacaoTributaria_TabelaB.get_codigo;
            edtpercentualagregado.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_percentualagregado;
            edtipi.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_ipi;
            Self.CalculaPersianaGrupoDiametro;//calcula preco

            if (PvendasEstado=True)
            Then Begin
                      combotributacao.itemindex:=2;

                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_Aliquota_ICMS_Estado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_Reducao_BC_ICMS_estado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_Aliquota_ICMS_Cupom_Estado;

                      if (Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_IsentoICMS_Estado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_SubstituicaoICMS_Estado='S')
                      Then combotributacao.itemindex:=1;

            End
            Else Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_Aliquota_ICMS_foraestado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_Reducao_BC_ICMS_foraestado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_Aliquota_ICMS_Cupom_foraestado;

                      if (Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_IsentoICMS_foraestado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objpersiana_nf.PERSIANAGRUPODIAMETROCOR.persiana.Get_SubstituicaoICMS_foraestado='S')
                      Then combotributacao.itemindex:=1;
            End;
            //******************************************************************
     End;
     5:Begin//Vidro
            if (Self.ObjNotaFiscalObjetos.Objvidro_NF.vidroCor.LocalizaCodigo(edtproduto_nf.Text)=False)
            Then Begin
                      edtproduto_nf.Text:='';
                      exit;
            End
            Else Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCor.TabelaparaObjeto;
            lbnomeproduto.caption:=CompletaPalavra(Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCor.vidro.Get_Descricao,50,' ')+' Cor '+Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCor.Cor.Get_Descricao;



            //*******************************************************************
            EdtSituacaoTributaria_TabelaA.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.SituacaoTributaria_TabelaA.get_codigo;
            EdtSituacaoTributaria_TabelaB.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.SituacaoTributaria_TabelaB.get_codigo;
            edtpercentualagregado.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_percentualagregado;
            edtipi.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_ipi;
            Self.Calculavidro;//calcula preco

            if (PvendasEstado=True)
            Then Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_Aliquota_ICMS_Estado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_Reducao_BC_ICMS_estado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_Aliquota_ICMS_Cupom_Estado;

                      if (Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_IsentoICMS_Estado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_SubstituicaoICMS_Estado='S')
                      Then combotributacao.itemindex:=1;

            End
            Else Begin
                      combotributacao.itemindex:=2;
                      
                      EdtAliquota.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_Aliquota_ICMS_foraestado;
                      edtreducaobasecalculo.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_Reducao_BC_ICMS_foraestado;
                      edtaliquotacupom.text:=Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_Aliquota_ICMS_Cupom_foraestado;

                      if (Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_IsentoICMS_foraestado='S')
                      Then combotributacao.itemindex:=0;

                      if (Self.ObjNotaFiscalObjetos.Objvidro_nf.vidroCOR.vidro.Get_SubstituicaoICMS_foraestado='S')
                      Then combotributacao.itemindex:=1;
            End;
            //******************************************************************

     End;
  End;
  Usouf9napesquisa:=False;


end;

procedure TFNotaFiscal.edtproduto_nfKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  if Key<>Vk_f9
  Then exit;

  edtproduto_nf.Text:='';

Try

  Case combotipoprodutos_nf.ItemIndex of

     0:Begin//diversos
            Self.objNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR.EdtDiversoCorKeyDown(sender,key,shift,lbnomeproduto);
     End;
     1:Begin//ferragem
            Self.objNotaFiscalObjetos.Objferragem_nf.FerragemCor.EdtFerragemCorKeyDown(sender,key,shift,lbnomeproduto);
     End;
     2:Begin//KitBox
            Self.objNotaFiscalObjetos.Objkitbox_nf.KitBoxCor.EdtkitboxCorKeyDown(sender,key,shift,lbnomeproduto);
     End;
     3:Begin//Perfilado
            Self.objNotaFiscalObjetos.Objperfilado_nf.PerfiladoCor.EdtPerfiladoCorKeyDown(sender,key,shift,lbnomeproduto);
     End;
     4:Begin//Persiana
            Self.objNotaFiscalObjetos.ObjPersiana_nf.PersianaGrupoDiametroCor.EdtPersianagrupodiametrocorKeyDown(sender,key,shift,lbnomeproduto);
     End;
     5:Begin//Vidro
            Self.objNotaFiscalObjetos.Objvidro_nf.VidroCor.EdtVidroCorKeyDown(sender,key,shift,lbnomeproduto);
     End;

  End;

Finally
       if (edtproduto_nf.Text<>'')
       Then Usouf9napesquisa:=True;//significa que ele retorna o codigo
End;


end;




procedure TFNotaFiscal.edtquantidade_nfKeyPress(Sender: TObject;
  var Key: Char);
begin
    if not (key in['0'..'9',',',#8])
    Then
       if (key='.')
       Then Key:=','
       Else Key:=#0;

end;

procedure TFNotaFiscal.edtvalor_nfKeyPress(Sender: TObject; var Key: Char);
begin
    if not (key in['0'..'9',',',#8])
    Then
       if (key='.')
       Then Key:=','
       Else Key:=#0;

end;

procedure TFNotaFiscal.btGravar_nfClick(Sender: TObject);
var
PcodigoCombo:Integer;
begin
  PcodigoCombo:=combotipoprodutos_nf.ItemIndex;

  if (edtproduto_nf.Text='')
  then exit;

  if(ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.Get_Situacao<>'N')
  then Exit;

try

  Case combotipoprodutos_nf.ItemIndex of
     0:Begin//diversos

            With Self.objNotaFiscalObjetos.ObjDiverso_nf do
            Begin
                 ZerarTabela;
                 if (panelreferencia.Visible=True)
                 Then Begin
                         //Se tiver produto referenciado � uma dev. de cliente
                         //n�o posso mudar nada
                         //uso os mesmos impostos, troco apenas o valor, qtde e CFOP
                         if (LocalizaCodigo(edtproduto_ref.Text)=False)
                         Then Begin
                                  MensagemErro('N�o foi encontrado o Diverso ref. na NF');
                                  exit;
                         End;
                        TabelaparaObjeto;
                        Submit_Codigo('0');
                        Submit_QUANTIDADE(edtquantidade_nf.Text);
                        NOTAFISCAL.Submit_CODIGO(EdtCodigo.Text);
                        Submit_valorseguro('0');
                        Submit_valorfrete('0');
                        Submit_referencia(edtproduto_ref.text);
                        CFOP.Submit_CODIGO(edtcfop_fornecedor.text);
                        Status:=dsinsert;
                 End
                 Else Begin
                           ZerarTabela;
                           if (DiversoCor.LocalizaCodigo(edtproduto_nf.Text)=False)
                           Then Begin
                                   Messagedlg('N�o foi Encontrado esse Diverso nessa cor!',mterror,[mbok],0);
                                   exit;
                           End;
                           DiversoCor.TabelaparaObjeto;

                           if (notafiscal.LocalizaCodigo(EdtCodigo.Text)=False)
                           Then Begin
                                     mensagemerro('Nota Fiscal n�o encontrada');
                                     exit;
                           End;
                           notafiscal.TabelaparaObjeto;

                           status:=dsInactive;
                           if (edtcodigo_nf.text='')
                           or (edtcodigo_nf.text='0')
                           then begin
                                    Status:=dsinsert;
                                    Submit_Codigo('0');
                           End
                           Else Begin
                                     status:=dsedit;
                                     Submit_codigo(edtcodigo_nf.text);
                           End;
                           
                           Submit_Quantidade(edtquantidade_nf.Text);
                           Submit_Valor(edtvalor_nf.text);
                           
                           
                           
                           if (Notafiscal.cliente.get_codigo<>'')//tem cliente
                           Then Begin
                                   //Procurando o Imposto do Estado de origem

                                   if (Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Diversocor.Diverso.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Origem para este Diverso, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Diversocor.Diverso.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.TabelaparaObjeto;

                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_IPI.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.imposto_ipi.Get_CODIGO);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);

                                   if( NOTAFISCAL.Cliente.Get_Estado=ESTADOSISTEMAGLOBAL)
                                   then   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                                   else   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                                   //Procurando o estado de destino
                                   if (Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Diversocor.Diverso.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Destino para este Diverso, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Diversocor.Diverso.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.TabelaparaObjeto;
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjDiverso_ICMS.imposto_cofins.Get_CODIGO);
                           
                                   Submit_BC_IPI('0');
                                   Submit_VALOR_IPI('0');

                                   Submit_BC_PIS('0');
                                   Submit_VALOR_PIS('0');
                                   Submit_BC_PIS_ST('0');
                                   Submit_VALOR_PIS_ST('0');
                           
                                   Submit_BC_COFINS('0');
                                   Submit_VALOR_COFINS('0');
                                   Submit_BC_COFINS_ST('0');
                                   Submit_VALOR_COFINS_ST('0');
                           End
                           Else Begin
                                     //n�o tem cliente
                           
                                     if (NOTAFISCAL.Fornecedor.Get_Codigo='')
                                     Then Begin
                                               Mensagemerro('� necess�rio possuir um cliente ou fornecedor na NF');
                                               exit;
                                     End;
                           
                                     //Uso o Fornecedor
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_IPI.Submit_codigo(edtipi_fornecedor.Text);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(edtcofins_fornecedor.Text);
                                   CFOP.Submit_CODIGO(edtcfop_fornecedor.Text);
                           
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(edtcofins_fornecedor.Text);
                           
                           End;
                           
                           Submit_BC_IPI('0');
                           Submit_VALOR_IPI('0');
                           
                           Submit_BC_PIS('0');
                           Submit_VALOR_PIS('0');
                           Submit_BC_PIS_ST('0');
                           Submit_VALOR_PIS_ST('0');
                           
                           Submit_BC_COFINS('0');
                           Submit_VALOR_COFINS('0');
                           Submit_BC_COFINS_ST('0');
                           Submit_VALOR_COFINS_ST('0');
                           
                           
                           //******nao usa mais******
                           Submit_valorpauta('0');
                           Submit_ISENTO('N');
                           Submit_SUBSTITUICAOTRIBUTARIA('N');
                           Submit_ALIQUOTA('0');
                           Submit_REDUCAOBASECALCULO('0');
                           Submit_ALIQUOTACUPOM('0');
                           Submit_IPI('0');
                           Submit_percentualagregado('0');
                           Submit_Margemvaloragregadoconsumidor('0');
                           SituacaoTributaria_TabelaA.Submit_CODIGO('');
                           SituacaoTributaria_TabelaB.Submit_CODIGO('');
                           //*********************************
                           
                           
                           Submit_valorfrete(EdtVALORFRETE_produto.Text);
                           Submit_valorseguro(EdtVALORSEGURO_produto.Text);
                           
                           Submit_referencia('');
                 End;

                 if (Salvar(True)=False)
                 Then Begin
                           edtproduto_nf.SetFocus;
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Self.Atualizagrid;
                 BtCancelar_nfClick(sender);

            End;
     End;
     1:Begin//Ferragem
            With Self.objNotaFiscalObjetos.ObjFerragem_nf do
            Begin
                 ZerarTabela;
                 if (panelreferencia.Visible=True)
                 Then Begin
                         //Se tiver produto referenciado � uma dev. de cliente
                         //n�o posso mudar nada
                         //uso os mesmos impostos, troco apenas o valor
                         if (LocalizaCodigo(edtproduto_ref.Text)=False)
                         Then Begin
                                  MensagemErro('N�o foi encontrado o Ferragem ref. na NF');
                                  exit;
                         End;
                        TabelaparaObjeto;
                        Submit_Codigo('0');
                        Submit_QUANTIDADE(edtquantidade_nf.Text);
                        NOTAFISCAL.Submit_CODIGO(EdtCodigo.Text);
                        Submit_valorseguro('0');
                        Submit_valorfrete('0');
                        Submit_referencia(edtproduto_ref.text);
                        CFOP.Submit_CODIGO(edtcfop_fornecedor.text);
                        Status:=dsinsert;
                 End
                 Else Begin
                           ZerarTabela;
                           if (FerragemCor.LocalizaCodigo(edtproduto_nf.Text)=False)
                           Then Begin
                                   Messagedlg('N�o foi Encontrado esse Ferragem nessa cor!',mterror,[mbok],0);
                                   exit;
                           End;
                           FerragemCor.TabelaparaObjeto;

                           if (notafiscal.LocalizaCodigo(EdtCodigo.Text)=False)
                           Then Begin
                                     mensagemerro('Nota Fiscal n�o encontrada');
                                     exit;
                           End;
                           notafiscal.TabelaparaObjeto;

                           status:=dsInactive;
                           if (edtcodigo_nf.text='')
                           or (edtcodigo_nf.text='0')
                           then begin
                                    Status:=dsinsert;
                                    Submit_Codigo('0');
                           End
                           Else Begin
                                     status:=dsedit;
                                     Submit_codigo(edtcodigo_nf.text);
                           End;
                           
                           Submit_Quantidade(edtquantidade_nf.Text);
                           Submit_Valor(edtvalor_nf.text);
                           
                           
                           
                           if (Notafiscal.cliente.get_codigo<>'')//tem cliente
                           Then Begin
                                   //Procurando o Imposto do Estado de origem
                                  if (Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Ferragemcor.Ferragem.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Origem para este Ferragem, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Ferragemcor.Ferragem.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.TabelaparaObjeto;
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_IPI.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.imposto_ipi.Get_CODIGO);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);
                                   //CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.IMPOSTO.CFOP.Get_CODIGO);
                                   if( NOTAFISCAL.Cliente.Get_Estado=ESTADOSISTEMAGLOBAL)
                                   then   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.IMPOSTO.CFOP.Get_CODIGO)
                                   else   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.IMPOSTO.Get_CFOP_FORAESTADO);

                                   //Procurando o estado de destino
                                   if (Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Ferragemcor.Ferragem.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Destino para este Ferragem, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Ferragemcor.Ferragem.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.TabelaparaObjeto;
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjFerragem_ICMS.imposto_cofins.Get_CODIGO);
                           
                                   Submit_BC_IPI('0');
                                   Submit_VALOR_IPI('0');

                                   Submit_BC_PIS('0');
                                   Submit_VALOR_PIS('0');
                                   Submit_BC_PIS_ST('0');
                                   Submit_VALOR_PIS_ST('0');
                           
                                   Submit_BC_COFINS('0');
                                   Submit_VALOR_COFINS('0');
                                   Submit_BC_COFINS_ST('0');
                                   Submit_VALOR_COFINS_ST('0');
                           End
                           Else Begin
                                     //n�o tem cliente
                           
                                     if (NOTAFISCAL.Fornecedor.Get_Codigo='')
                                     Then Begin
                                               Mensagemerro('� necess�rio possuir um cliente ou fornecedor na NF');
                                               exit;
                                     End;
                           
                                     //Uso o Fornecedor
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_IPI.Submit_codigo(edtipi_fornecedor.Text);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(edtcofins_fornecedor.Text);
                                   CFOP.Submit_CODIGO(edtcfop_fornecedor.Text);
                           
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(edtcofins_fornecedor.Text);
                           
                           End;
                           
                           Submit_BC_IPI('0');
                           Submit_VALOR_IPI('0');
                           
                           Submit_BC_PIS('0');
                           Submit_VALOR_PIS('0');
                           Submit_BC_PIS_ST('0');
                           Submit_VALOR_PIS_ST('0');
                           
                           Submit_BC_COFINS('0');
                           Submit_VALOR_COFINS('0');
                           Submit_BC_COFINS_ST('0');
                           Submit_VALOR_COFINS_ST('0');
                           
                           
                           //******nao usa mais******
                           Submit_valorpauta('0');
                           Submit_ISENTO('N');
                           Submit_SUBSTITUICAOTRIBUTARIA('N');
                           Submit_ALIQUOTA('0');
                           Submit_REDUCAOBASECALCULO('0');
                           Submit_ALIQUOTACUPOM('0');
                           Submit_IPI('0');
                           Submit_percentualagregado('0');
                           Submit_Margemvaloragregadoconsumidor('0');
                           SituacaoTributaria_TabelaA.Submit_CODIGO('');
                           SituacaoTributaria_TabelaB.Submit_CODIGO('');
                           //*********************************
                           
                           
                           Submit_valorfrete(EdtVALORFRETE_produto.Text);
                           Submit_valorseguro(EdtVALORSEGURO_produto.Text);
                           
                           Submit_referencia('');
                 End;

                 if (Salvar(True)=False)
                 Then Begin
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Self.Atualizagrid;
                 BtCancelar_nfClick(sender);
            End;
     End;
     2:Begin//KitBox
            With Self.objNotaFiscalObjetos.ObjKitbox_nf do
            Begin
                 ZerarTabela;
                 if (panelreferencia.Visible=True)
                 Then Begin
                         //Se tiver produto referenciado � uma dev. de cliente
                         //n�o posso mudar nada
                         //uso os mesmos impostos, troco apenas o valor
                         if (LocalizaCodigo(edtproduto_ref.Text)=False)
                         Then Begin
                                  MensagemErro('N�o foi encontrado o Kitbox ref. na NF');
                                  exit;
                         End;
                        TabelaparaObjeto;
                        Submit_Codigo('0');
                        Submit_QUANTIDADE(edtquantidade_nf.Text);
                        NOTAFISCAL.Submit_CODIGO(EdtCodigo.Text);
                        Submit_valorseguro('0');
                        Submit_valorfrete('0');
                        Submit_referencia(edtproduto_ref.text);
                        CFOP.Submit_CODIGO(edtcfop_fornecedor.text);
                        Status:=dsinsert;
                 End
                 Else Begin
                           ZerarTabela;
                           if (KitboxCor.LocalizaCodigo(edtproduto_nf.Text)=False)
                           Then Begin
                                   Messagedlg('N�o foi Encontrado esse Kitbox nessa cor!',mterror,[mbok],0);
                                   exit;
                           End;
                           KitboxCor.TabelaparaObjeto;

                           if (notafiscal.LocalizaCodigo(EdtCodigo.Text)=False)
                           Then Begin
                                     mensagemerro('Nota Fiscal n�o encontrada');
                                     exit;
                           End;
                           notafiscal.TabelaparaObjeto;

                           status:=dsInactive;
                           if (edtcodigo_nf.text='')
                           or (edtcodigo_nf.text='0')
                           then begin
                                    Status:=dsinsert;
                                    Submit_Codigo('0');
                           End
                           Else Begin
                                     status:=dsedit;
                                     Submit_codigo(edtcodigo_nf.text);
                           End;
                           
                           Submit_Quantidade(edtquantidade_nf.Text);
                           Submit_Valor(edtvalor_nf.text);
                           
                           
                           
                           if (Notafiscal.cliente.get_codigo<>'')//tem cliente
                           Then Begin
                                   //Procurando o Imposto do Estado de origem
                                   if (Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Kitboxcor.Kitbox.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Origem para este Kitbox, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Kitboxcor.Kitbox.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.TabelaparaObjeto;
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_IPI.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.imposto_ipi.Get_CODIGO);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);
                                   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.IMPOSTO.CFOP.Get_CODIGO);
                           
                                   //Procurando o estado de destino
                                   if (Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Kitboxcor.Kitbox.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Destino para este Kitbox, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Kitboxcor.Kitbox.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.TabelaparaObjeto;
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjKitbox_ICMS.imposto_cofins.Get_CODIGO);
                           
                                   Submit_BC_IPI('0');
                                   Submit_VALOR_IPI('0');

                                   Submit_BC_PIS('0');
                                   Submit_VALOR_PIS('0');
                                   Submit_BC_PIS_ST('0');
                                   Submit_VALOR_PIS_ST('0');
                           
                                   Submit_BC_COFINS('0');
                                   Submit_VALOR_COFINS('0');
                                   Submit_BC_COFINS_ST('0');
                                   Submit_VALOR_COFINS_ST('0');
                           End
                           Else Begin
                                     //n�o tem cliente
                           
                                     if (NOTAFISCAL.Fornecedor.Get_Codigo='')
                                     Then Begin
                                               Mensagemerro('� necess�rio possuir um cliente ou fornecedor na NF');
                                               exit;
                                     End;
                           
                                     //Uso o Fornecedor
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_IPI.Submit_codigo(edtipi_fornecedor.Text);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(edtcofins_fornecedor.Text);
                                   CFOP.Submit_CODIGO(edtcfop_fornecedor.Text);
                           
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(edtcofins_fornecedor.Text);
                           
                           End;
                           
                           Submit_BC_IPI('0');
                           Submit_VALOR_IPI('0');
                           
                           Submit_BC_PIS('0');
                           Submit_VALOR_PIS('0');
                           Submit_BC_PIS_ST('0');
                           Submit_VALOR_PIS_ST('0');
                           
                           Submit_BC_COFINS('0');
                           Submit_VALOR_COFINS('0');
                           Submit_BC_COFINS_ST('0');
                           Submit_VALOR_COFINS_ST('0');
                           
                           
                           //******nao usa mais******
                           Submit_valorpauta('0');
                           Submit_ISENTO('N');
                           Submit_SUBSTITUICAOTRIBUTARIA('N');
                           Submit_ALIQUOTA('0');
                           Submit_REDUCAOBASECALCULO('0');
                           Submit_ALIQUOTACUPOM('0');
                           Submit_IPI('0');
                           Submit_percentualagregado('0');
                           Submit_Margemvaloragregadoconsumidor('0');
                           SituacaoTributaria_TabelaA.Submit_CODIGO('');
                           SituacaoTributaria_TabelaB.Submit_CODIGO('');
                           //*********************************
                           
                           
                           Submit_valorfrete(EdtVALORFRETE_produto.Text);
                           Submit_valorseguro(EdtVALORSEGURO_produto.Text);
                           
                           Submit_referencia('');
                 End;

                 if (Salvar(True)=False)
                 Then Begin
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Self.Atualizagrid;
                 BtCancelar_nfClick(sender);
            End;
     End;
     3:Begin//Perfilado
            With Self.objNotaFiscalObjetos.ObjPerfilado_nf do
            Begin
                 ZerarTabela;
                 if (panelreferencia.Visible=True)
                 Then Begin
                         //Se tiver produto referenciado � uma dev. de cliente
                         //n�o posso mudar nada
                         //uso os mesmos impostos, troco apenas o valor
                         if (LocalizaCodigo(edtproduto_ref.Text)=False)
                         Then Begin
                                  MensagemErro('N�o foi encontrado o Perfilado ref. na NF');
                                  exit;
                         End;
                        TabelaparaObjeto;
                        Submit_Codigo('0');
                        Submit_QUANTIDADE(edtquantidade_nf.Text);
                        NOTAFISCAL.Submit_CODIGO(EdtCodigo.Text);
                        Submit_valorseguro('0');
                        Submit_valorfrete('0');
                        Submit_referencia(edtproduto_ref.text);
                        CFOP.Submit_CODIGO(edtcfop_fornecedor.text);
                        Status:=dsinsert;
                 End
                 Else Begin
                           ZerarTabela;
                           if (PerfiladoCor.LocalizaCodigo(edtproduto_nf.Text)=False)
                           Then Begin
                                   Messagedlg('N�o foi Encontrado esse Perfilado nessa cor!',mterror,[mbok],0);
                                   exit;
                           End;
                           PerfiladoCor.TabelaparaObjeto;

                           if (notafiscal.LocalizaCodigo(EdtCodigo.Text)=False)
                           Then Begin
                                     mensagemerro('Nota Fiscal n�o encontrada');
                                     exit;
                           End;
                           notafiscal.TabelaparaObjeto;

                           status:=dsInactive;
                           if (edtcodigo_nf.text='')
                           or (edtcodigo_nf.text='0')
                           then begin
                                    Status:=dsinsert;
                                    Submit_Codigo('0');
                           End
                           Else Begin
                                     status:=dsedit;
                                     Submit_codigo(edtcodigo_nf.text);
                           End;
                           
                           Submit_Quantidade(edtquantidade_nf.Text);
                           Submit_Valor(edtvalor_nf.text);
                           
                           
                           
                           if (Notafiscal.cliente.get_codigo<>'')//tem cliente
                           Then Begin
                                   //Procurando o Imposto do Estado de origem
                                   if (Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Perfiladocor.Perfilado.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Origem para este Perfilado, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Perfiladocor.Perfilado.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.TabelaparaObjeto;
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_IPI.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.imposto_ipi.Get_CODIGO);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);
                                   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.IMPOSTO.CFOP.Get_CODIGO);
                           
                                   //Procurando o estado de destino
                                   if (Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Perfiladocor.Perfilado.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Destino para este Perfilado, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Perfiladocor.Perfilado.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.TabelaparaObjeto;
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPerfilado_ICMS.imposto_cofins.Get_CODIGO);
                           
                                   Submit_BC_IPI('0');
                                   Submit_VALOR_IPI('0');

                                   Submit_BC_PIS('0');
                                   Submit_VALOR_PIS('0');
                                   Submit_BC_PIS_ST('0');
                                   Submit_VALOR_PIS_ST('0');
                           
                                   Submit_BC_COFINS('0');
                                   Submit_VALOR_COFINS('0');
                                   Submit_BC_COFINS_ST('0');
                                   Submit_VALOR_COFINS_ST('0');
                           End
                           Else Begin
                                     //n�o tem cliente
                           
                                     if (NOTAFISCAL.Fornecedor.Get_Codigo='')
                                     Then Begin
                                               Mensagemerro('� necess�rio possuir um cliente ou fornecedor na NF');
                                               exit;
                                     End;
                           
                                     //Uso o Fornecedor
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_IPI.Submit_codigo(edtipi_fornecedor.Text);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(edtcofins_fornecedor.Text);
                                   CFOP.Submit_CODIGO(edtcfop_fornecedor.Text);
                           
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(edtcofins_fornecedor.Text);
                           
                           End;
                           
                           Submit_BC_IPI('0');
                           Submit_VALOR_IPI('0');
                           
                           Submit_BC_PIS('0');
                           Submit_VALOR_PIS('0');
                           Submit_BC_PIS_ST('0');
                           Submit_VALOR_PIS_ST('0');
                           
                           Submit_BC_COFINS('0');
                           Submit_VALOR_COFINS('0');
                           Submit_BC_COFINS_ST('0');
                           Submit_VALOR_COFINS_ST('0');
                           
                           
                           //******nao usa mais******
                           Submit_valorpauta('0');
                           Submit_ISENTO('N');
                           Submit_SUBSTITUICAOTRIBUTARIA('N');
                           Submit_ALIQUOTA('0');
                           Submit_REDUCAOBASECALCULO('0');
                           Submit_ALIQUOTACUPOM('0');
                           Submit_IPI('0');
                           Submit_percentualagregado('0');
                           Submit_Margemvaloragregadoconsumidor('0');
                           SituacaoTributaria_TabelaA.Submit_CODIGO('');
                           SituacaoTributaria_TabelaB.Submit_CODIGO('');
                           //*********************************
                           
                           
                           Submit_valorfrete(EdtVALORFRETE_produto.Text);
                           Submit_valorseguro(EdtVALORSEGURO_produto.Text);
                           
                           Submit_referencia('');
                 End;

                 if (Salvar(True)=False)
                 Then Begin
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Self.Atualizagrid;
                 BtCancelar_nfClick(sender);
            End;
     End;

     4:Begin//Persiana
            With Self.objNotaFiscalObjetos.ObjPersiana_nf do
            Begin
                 ZerarTabela;
                 if (panelreferencia.Visible=True)
                 Then Begin
                         //Se tiver produto referenciado � uma dev. de cliente
                         //n�o posso mudar nada
                         //uso os mesmos impostos, troco apenas o valor
                         if (LocalizaCodigo(edtproduto_ref.Text)=False)
                         Then Begin
                                  MensagemErro('N�o foi encontrado o Persiana ref. na NF');
                                  exit;
                         End;
                        TabelaparaObjeto;
                        Submit_Codigo('0');
                        Submit_QUANTIDADE(edtquantidade_nf.Text);
                        NOTAFISCAL.Submit_CODIGO(EdtCodigo.Text);
                        Submit_valorseguro('0');
                        Submit_valorfrete('0');
                        Submit_referencia(edtproduto_ref.text);
                        CFOP.Submit_CODIGO(edtcfop_fornecedor.text);
                        Status:=dsinsert;
                 End
                 Else Begin
                           ZerarTabela;
                           if (persianagrupodiametrocor.LocalizaCodigo(edtproduto_nf.Text)=False)
                           Then Begin
                                   Messagedlg('N�o foi Encontrado esse Persiana nessa cor!',mterror,[mbok],0);
                                   exit;
                           End;
                           persianagrupodiametrocor.TabelaparaObjeto;

                           if (notafiscal.LocalizaCodigo(EdtCodigo.Text)=False)
                           Then Begin
                                     mensagemerro('Nota Fiscal n�o encontrada');
                                     exit;
                           End;
                           notafiscal.TabelaparaObjeto;

                           status:=dsInactive;
                           if (edtcodigo_nf.text='')
                           or (edtcodigo_nf.text='0')
                           then begin
                                    Status:=dsinsert;
                                    Submit_Codigo('0');
                           End
                           Else Begin
                                     status:=dsedit;
                                     Submit_codigo(edtcodigo_nf.text);
                           End;
                           
                           Submit_Quantidade(edtquantidade_nf.Text);
                           Submit_Valor(edtvalor_nf.text);
                           
                           
                           
                           if (Notafiscal.cliente.get_codigo<>'')//tem cliente
                           Then Begin
                                   //Procurando o Imposto do Estado de origem
                                   if (Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,persianagrupodiametrocor.Persiana.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Origem para este Persiana, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+persianagrupodiametrocor.Persiana.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.TabelaparaObjeto;
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_IPI.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.imposto_ipi.Get_CODIGO);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);
                                   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.IMPOSTO.CFOP.Get_CODIGO);
                           
                                   //Procurando o estado de destino
                                   if (Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,persianagrupodiametrocor.Persiana.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Destino para este Persiana, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+persianagrupodiametrocor.Persiana.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.TabelaparaObjeto;
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjPersiana_ICMS.imposto_cofins.Get_CODIGO);
                           
                                   Submit_BC_IPI('0');
                                   Submit_VALOR_IPI('0');

                                   Submit_BC_PIS('0');
                                   Submit_VALOR_PIS('0');
                                   Submit_BC_PIS_ST('0');
                                   Submit_VALOR_PIS_ST('0');
                           
                                   Submit_BC_COFINS('0');
                                   Submit_VALOR_COFINS('0');
                                   Submit_BC_COFINS_ST('0');
                                   Submit_VALOR_COFINS_ST('0');
                           End
                           Else Begin
                                     //n�o tem cliente
                           
                                     if (NOTAFISCAL.Fornecedor.Get_Codigo='')
                                     Then Begin
                                               Mensagemerro('� necess�rio possuir um cliente ou fornecedor na NF');
                                               exit;
                                     End;
                           
                                     //Uso o Fornecedor
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_IPI.Submit_codigo(edtipi_fornecedor.Text);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(edtcofins_fornecedor.Text);
                                   CFOP.Submit_CODIGO(edtcfop_fornecedor.Text);
                           
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(edtcofins_fornecedor.Text);
                           
                           End;
                           
                           Submit_BC_IPI('0');
                           Submit_VALOR_IPI('0');
                           
                           Submit_BC_PIS('0');
                           Submit_VALOR_PIS('0');
                           Submit_BC_PIS_ST('0');
                           Submit_VALOR_PIS_ST('0');
                           
                           Submit_BC_COFINS('0');
                           Submit_VALOR_COFINS('0');
                           Submit_BC_COFINS_ST('0');
                           Submit_VALOR_COFINS_ST('0');
                           
                           
                           //******nao usa mais******
                           Submit_valorpauta('0');
                           Submit_ISENTO('N');
                           Submit_SUBSTITUICAOTRIBUTARIA('N');
                           Submit_ALIQUOTA('0');
                           Submit_REDUCAOBASECALCULO('0');
                           Submit_ALIQUOTACUPOM('0');
                           Submit_IPI('0');
                           Submit_percentualagregado('0');
                           Submit_Margemvaloragregadoconsumidor('0');
                           SituacaoTributaria_TabelaA.Submit_CODIGO('');
                           SituacaoTributaria_TabelaB.Submit_CODIGO('');
                           //*********************************
                           
                           
                           Submit_valorfrete(EdtVALORFRETE_produto.Text);
                           Submit_valorseguro(EdtVALORSEGURO_produto.Text);
                           
                           Submit_referencia('');
                 End;

                 if (Salvar(True)=False)
                 Then Begin
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Self.Atualizagrid;
                 BtCancelar_nfClick(sender);
            End;
     End;

     5:Begin//Vidro
            With Self.objNotaFiscalObjetos.ObjVidro_nf do
            Begin
                 ZerarTabela;
                 if (panelreferencia.Visible=True)
                 Then Begin
                         //Se tiver produto referenciado � uma dev. de cliente
                         //n�o posso mudar nada
                         //uso os mesmos impostos, troco apenas o valor
                         if (LocalizaCodigo(edtproduto_ref.Text)=False)
                         Then Begin
                                  MensagemErro('N�o foi encontrado o Vidro ref. na NF');
                                  exit;
                         End;
                        TabelaparaObjeto;
                        Submit_Codigo('0');
                        Submit_QUANTIDADE(edtquantidade_nf.Text);
                        NOTAFISCAL.Submit_CODIGO(EdtCodigo.Text);
                        Submit_valorseguro('0');
                        Submit_valorfrete('0');
                        Submit_referencia(edtproduto_ref.text);
                        CFOP.Submit_CODIGO(edtcfop_fornecedor.text);
                        Status:=dsinsert;
                 End
                 Else Begin
                           ZerarTabela;
                           if (VidroCor.LocalizaCodigo(edtproduto_nf.Text)=False)
                           Then Begin
                                   Messagedlg('N�o foi Encontrado esse Vidro nessa cor!',mterror,[mbok],0);
                                   exit;
                           End;
                           VidroCor.TabelaparaObjeto;

                           if (notafiscal.LocalizaCodigo(EdtCodigo.Text)=False)
                           Then Begin
                                     mensagemerro('Nota Fiscal n�o encontrada');
                                     exit;
                           End;
                           notafiscal.TabelaparaObjeto;

                           status:=dsInactive;
                           if (edtcodigo_nf.text='')
                           or (edtcodigo_nf.text='0')
                           then begin
                                    Status:=dsinsert;
                                    Submit_Codigo('0');
                           End
                           Else Begin
                                     status:=dsedit;
                                     Submit_codigo(edtcodigo_nf.text);
                           End;
                           
                           Submit_Quantidade(edtquantidade_nf.Text);
                           Submit_Valor(edtvalor_nf.text);
                           
                           
                           
                           if (Notafiscal.cliente.get_codigo<>'')//tem cliente
                           Then Begin
                                   //Procurando o Imposto do Estado de origem
                                   if (Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Localiza(ESTADOSISTEMAGLOBAL,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Vidrocor.Vidro.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Origem para este Vidro, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+ESTADOSISTEMAGLOBAL+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Vidrocor.Vidro.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.TabelaparaObjeto;
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_IPI.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.imposto_ipi.Get_CODIGO);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.imposto_cofins.Get_CODIGO);
                                   CFOP.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.IMPOSTO.CFOP.Get_CODIGO);
                           
                                   //Procurando o estado de destino
                                   if (Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Localiza(NOTAFISCAL.Cliente.Get_Estado,NOTAFISCAL.Cliente.tipocliente.Get_CODIGO,Vidrocor.Vidro.Get_Codigo,NOTAFISCAL.Operacao.Get_CODIGO)=False)
                                   Then Begin
                                             MensagemErro('N�o foi encontrado o imposto de Destino para este Vidro, tipo de cliente e opera��o'+#13+
                                                          'Estado: '+NOTAFISCAL.Cliente.Get_Estado+' Tipo de Cliente '+NOTAFISCAL.Cliente.tipocliente.Get_CODIGO+' '+'C�digo do material '+Vidrocor.Vidro.Get_Codigo);
                                             exit;
                                   End;
                                   Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.TabelaparaObjeto;
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.IMPOSTO.Get_CODIGO);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.imposto_pis.Get_CODIGO);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.imposto_cofins.Get_CODIGO);
                           
                                   Submit_BC_IPI('0');
                                   Submit_VALOR_IPI('0');

                                   Submit_BC_PIS('0');
                                   Submit_VALOR_PIS('0');
                                   Submit_BC_PIS_ST('0');
                                   Submit_VALOR_PIS_ST('0');
                           
                                   Submit_BC_COFINS('0');
                                   Submit_VALOR_COFINS('0');
                                   Submit_BC_COFINS_ST('0');
                                   Submit_VALOR_COFINS_ST('0');
                           End
                           Else Begin
                                     //n�o tem cliente
                           
                                     if (NOTAFISCAL.Fornecedor.Get_Codigo='')
                                     Then Begin
                                               Mensagemerro('� necess�rio possuir um cliente ou fornecedor na NF');
                                               exit;
                                     End;
                           
                                     //Uso o Fornecedor
                           
                                   IMPOSTO_ICMS_ORIGEM.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_IPI.Submit_codigo(edtipi_fornecedor.Text);
                                   IMPOSTO_PIS_ORIGEM.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_ORIGEM.Submit_codigo(edtcofins_fornecedor.Text);
                                   CFOP.Submit_CODIGO(edtcfop_fornecedor.Text);
                           
                                   IMPOSTO_ICMS_DESTINO.Submit_CODIGO(edticms_fornecedor.Text);
                                   IMPOSTO_PIS_DESTINO.Submit_codigo(edtpis_fornecedor.Text);
                                   IMPOSTO_COFINS_DESTINO.Submit_codigo(edtcofins_fornecedor.Text);
                           
                           End;
                           
                           Submit_BC_IPI('0');
                           Submit_VALOR_IPI('0');
                           
                           Submit_BC_PIS('0');
                           Submit_VALOR_PIS('0');
                           Submit_BC_PIS_ST('0');
                           Submit_VALOR_PIS_ST('0');
                           
                           Submit_BC_COFINS('0');
                           Submit_VALOR_COFINS('0');
                           Submit_BC_COFINS_ST('0');
                           Submit_VALOR_COFINS_ST('0');
                           
                           
                           //******nao usa mais******
                           Submit_valorpauta('0');
                           Submit_ISENTO('N');
                           Submit_SUBSTITUICAOTRIBUTARIA('N');
                           Submit_ALIQUOTA('0');
                           Submit_REDUCAOBASECALCULO('0');
                           Submit_ALIQUOTACUPOM('0');
                           Submit_IPI('0');
                           Submit_percentualagregado('0');
                           Submit_Margemvaloragregadoconsumidor('0');
                           SituacaoTributaria_TabelaA.Submit_CODIGO('');
                           SituacaoTributaria_TabelaB.Submit_CODIGO('');
                           //*********************************
                           
                           
                           Submit_valorfrete(EdtVALORFRETE_produto.Text);
                           Submit_valorseguro(EdtVALORSEGURO_produto.Text);
                           
                           Submit_referencia('');
                 End;

                 if (Salvar(True)=False)
                 Then Begin
                           messagedlg('N�o foi poss�vel Gravar',mterror,[mbok],0);
                           exit;
                 End;
                 Self.Atualizagrid;
                 BtCancelar_nfClick(sender);
            End;
     End;

  End;//case

Finally
       combotipoprodutos_nf.ItemIndex:=PcodigoCombo;
       combotipoprodutos_nfChange(sender);
End;


end;

procedure TFNotaFiscal.BtCancelar_nfClick(Sender: TObject);
var
pcodigocombo:integer;
pMargemvaloragregadoconsumidor,ppercentualagregado:string;
begin

     PcodigoCombo:=combotipoprodutos_nf.ItemIndex;
     limpaedit(PanelProdutos);
     limpaedit(panelimpostos);

     lbnomeproduto.caption:='';

     combotipoprodutos_nf.ItemIndex:=PcodigoCombo;

     edtpercentualagregado.text:='0';
     edtMargemvaloragregadoconsumidor.text:='0';

     if (edtproduto_nf.enabled=true)
     Then edtproduto_nf.setfocus;

     if (panelreferencia.visible=true) and (edtproduto_ref.enabled=true)
     Then edtproduto_ref.SetFocus;

     combotipoprodutos_nfChange(sender);
end;

procedure TFNotaFiscal.BtExcluir_nfClick(Sender: TObject);
begin

     if (StrgProdutos.Cells[6,StrgProdutos.row]='')
     Then exit;

     If (Messagedlg('Tem certeza que deseja Excluir o(a) '+StrgProdutos.Cells[0,StrgProdutos.row]+' C�digo '+StrgProdutos.Cells[6,StrgProdutos.row]+' ?',mtconfirmation,[mbyes,mbno],0)=mrno)
     Then exit;

     if (StrgProdutos.Cells[0,StrgProdutos.row]='DIVERSO')
     Then Begin
               if (ObjNotaFiscalObjetos.ObjDiverso_nf.Exclui(StrgProdutos.Cells[6,StrgProdutos.row],True)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o DIVERSO c�digo '+StrgProdutos.Cells[6,StrgProdutos.row],mterror,[mbok],0);
                         exit;
               End;
               Self.AtualizaGrid;
               exit;
     End;

     if (StrgProdutos.Cells[0,StrgProdutos.row]='FERRAGEM')
     Then Begin
               if (ObjNotaFiscalObjetos.ObjFERRAGEM_nf.Exclui(StrgProdutos.Cells[6,StrgProdutos.row],True)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o FERRAGEM c�digo '+StrgProdutos.Cells[6,StrgProdutos.row],mterror,[mbok],0);
                         exit;
               End;
               Self.AtualizaGrid;
               exit;
     End;

     if (StrgProdutos.Cells[0,StrgProdutos.row]='KITBOX')
     Then Begin
               if (ObjNotaFiscalObjetos.ObjKITBOX_nf.Exclui(StrgProdutos.Cells[6,StrgProdutos.row],True)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o KITBOX c�digo '+StrgProdutos.Cells[6,StrgProdutos.row],mterror,[mbok],0);
                         exit;
               End;
               Self.AtualizaGrid;
               exit;
     End;

     if (StrgProdutos.Cells[0,StrgProdutos.row]='PERFILADO')
     Then Begin
               if (ObjNotaFiscalObjetos.ObjPERFILADO_nf.Exclui(StrgProdutos.Cells[6,StrgProdutos.row],True)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o PERFILADO c�digo '+StrgProdutos.Cells[6,StrgProdutos.row],mterror,[mbok],0);
                         exit;
               End;
               Self.AtualizaGrid;
               exit;
     End;

     if (StrgProdutos.Cells[0,StrgProdutos.row]='PERSIANA')
     Then Begin
               if (ObjNotaFiscalObjetos.ObjPERSIANA_nf.Exclui(StrgProdutos.Cells[6,StrgProdutos.row],True)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir a PERSIANA c�digo '+StrgProdutos.Cells[6,StrgProdutos.row],mterror,[mbok],0);
                         exit;
               End;
               Self.AtualizaGrid;
               exit;
     End;

     if (StrgProdutos.Cells[0,StrgProdutos.row]='VIDRO')
     Then Begin
               if (ObjNotaFiscalObjetos.ObjVIDRO_nf.Exclui(StrgProdutos.Cells[6,StrgProdutos.row],True)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o VIDRO c�digo '+StrgProdutos.Cells[6,StrgProdutos.row],mterror,[mbok],0);
                         exit;
               End;
               Self.AtualizaGrid;
               exit;
     End;
end;

procedure TFNotaFiscal.StrgProdutosDblClick(Sender: TObject);
begin
     //alterar
      
     if (btgravar_nf.Enabled=False)
     Then exit;

     if (StrgProdutos.Cells[6,StrgProdutos.row]='')//codigo vazio
     Then exit;

     with ObjNotaFiscalObjetos do
     begin
          if (StrgProdutos.Cells[0,StrgProdutos.row]='DIVERSO')
          Then Begin

                    if (ObjDiverso_nf.LocalizaCodigo(StrgProdutos.Cells[6,StrgProdutos.row])=False)
                    Then exit;
                    
                    ObjDiverso_nf.TabelaparaObjeto;

                    if (ObjDiverso_NF.Get_referencia<>'')
                    Then Begin
                              BtCancelar_nfClick(sender);
                              mensagemerro('Exclua e lance novamente, pois este registro possui uma refer�ncia');
                              exit;
                    End;



                    limpaedit(PanelProdutos);

                    combotipoprodutos_nf.ItemIndex:=0;
                    combotipoprodutos_nfChange(sender);

                    edtcodigo_nf.Text       :=ObjDiverso_nf.Get_CODIGO;
                    edtproduto_nf.text      :=ObjDiverso_nf.DiversoCor.Get_codigo;
                    edtquantidade_nf.text   :=ObjDiverso_nf.get_quantidade;
                    edtvalor_nf.text        :=ObjDiverso_nf.Get_Valor;

                    EdtSituacaoTributaria_TabelaA.text:=ObjDiverso_NF.SituacaoTributaria_TabelaA. get_codigo;
                    EdtSituacaoTributaria_TabelaB.text:=ObjDiverso_NF.SituacaoTributaria_TabelaB. get_codigo;
                    edtipi.text                       :=ObjDiverso_NF.Get_IPI;
                    edtpercentualagregado.text        :=ObjDiverso_NF.Get_percentualagregado;
                    edtMargemvaloragregadoconsumidor.text        :=ObjDiverso_NF.Get_Margemvaloragregadoconsumidor;
                    EdtAliquota.text                  :=ObjDiverso_NF.Get_ALIQUOTA;
                    edtreducaobasecalculo.text        :=ObjDiverso_NF.Get_REDUCAOBASECALCULO;
                    edtaliquotacupom.text             :=ObjDiverso_NF.Get_ALIQUOTACUPOM;
                    edtvalorfrete_produto.Text                :=ObjDiverso_NF.Get_valorfrete;
                    edtvalorseguro_produto.text               :=ObjDiverso_NF.Get_valorseguro;


                    combotributacao.ItemIndex:=2;

                    if (ObjDiverso_NF.Get_ISENTO='S')
                    then combotributacao.ItemIndex:=0;

                    if (ObjDiverso_NF.Get_SUBSTITUICAOTRIBUTARIA='S')
                    then combotributacao.ItemIndex:=1;

                    if (panelimpostos.Visible=True)
                    Then Begin
                            edtcfop_fornecedor.Text:=ObjDiverso_NF.CFOP.Get_CODIGO;
                            edticms_fornecedor.Text:=ObjDiverso_NF.IMPOSTO_ICMS_ORIGEM.Get_CODIGO;
                            edtipi_fornecedor.Text:=ObjDiverso_NF.IMPOSTO_IPI.Get_CODIGO;
                            edtpis_fornecedor.text:=ObjDiverso_NF.IMPOSTO_PIS_ORIGEM.get_codigo;
                            edtcofins_fornecedor.Text:=ObjDiverso_NF.IMPOSTO_COFINS_ORIGEM.Get_CODIGO
                    End;

                    

          End;

          if (StrgProdutos.Cells[0,StrgProdutos.row]='FERRAGEM')
          Then Begin
                    if (ObjFERRAGEM_nf.LocalizaCodigo(StrgProdutos.Cells[6,StrgProdutos.row])=False)
                    Then exit;
                    ObjFERRAGEM_nf.TabelaparaObjeto;

                    if (ObjFERRAGEM_nf.Get_referencia<>'')
                    Then Begin
                              BtCancelar_nfClick(sender);
                              mensagemerro('Exclua e lance novamente, pois este registro possui uma refer�ncia');
                              exit;
                    End;

                    limpaedit(PanelProdutos);

                    combotipoprodutos_nf.ItemIndex:=1;
                    combotipoprodutos_nfChange(sender);

                    edtcodigo_nf.Text       :=ObjFERRAGEM_nf.Get_CODIGO;
                    edtproduto_nf.text      :=ObjFERRAGEM_nf.FERRAGEMCor.get_codigo;
                    edtquantidade_nf.text   :=ObjFERRAGEM_nf.get_quantidade;
                    edtvalor_nf.text        :=ObjFERRAGEM_nf.Get_Valor;

                    EdtSituacaoTributaria_TabelaA.text:=Objferragem_nf.SituacaoTributaria_TabelaA. get_codigo;
                    EdtSituacaoTributaria_TabelaB.text:=Objferragem_nf.SituacaoTributaria_TabelaB. get_codigo;
                    edtipi.text                       :=Objferragem_nf.Get_IPI;
                    edtpercentualagregado.text        :=Objferragem_nf.Get_percentualagregado;
                    edtMargemvaloragregadoconsumidor.text        :=Objferragem_nf.Get_Margemvaloragregadoconsumidor;
                    EdtAliquota.text                  :=Objferragem_nf.Get_ALIQUOTA;
                    edtreducaobasecalculo.text        :=Objferragem_nf.Get_REDUCAOBASECALCULO;
                    edtaliquotacupom.text             :=Objferragem_nf.Get_ALIQUOTACUPOM;
                    edtvalorfrete_produto.Text                :=Objferragem_nf.Get_valorfrete;
                    edtvalorseguro_produto.text               :=Objferragem_nf.Get_valorseguro;

                    combotributacao.ItemIndex:=2;

                    if (Objferragem_nf.Get_ISENTO='S')
                    then combotributacao.ItemIndex:=0;

                    if (Objferragem_nf.Get_SUBSTITUICAOTRIBUTARIA='S')
                    then combotributacao.ItemIndex:=1;

                    if (panelimpostos.Visible=True)
                    Then Begin
                            edtcfop_fornecedor.Text:=Objferragem_NF.CFOP.Get_CODIGO;
                            edticms_fornecedor.Text:=Objferragem_NF.IMPOSTO_ICMS_ORIGEM.Get_CODIGO;
                            edtipi_fornecedor.Text:=Objferragem_NF.IMPOSTO_IPI.Get_CODIGO;
                            edtpis_fornecedor.text:=Objferragem_NF.IMPOSTO_PIS_ORIGEM.get_codigo;
                            edtcofins_fornecedor.Text:=Objferragem_NF.IMPOSTO_COFINS_ORIGEM.Get_CODIGO
                    End;


          End;

          if (StrgProdutos.Cells[0,StrgProdutos.row]='KITBOX')
          Then Begin
                    if (Objkitbox_nf.LocalizaCodigo(StrgProdutos.Cells[6,StrgProdutos.row])=False)
                    Then exit;
                    Objkitbox_nf.TabelaparaObjeto;

                    if (Objkitbox_nf.Get_referencia<>'')
                    Then Begin
                              BtCancelar_nfClick(sender);
                              mensagemerro('Exclua e lance novamente, pois este registro possui uma refer�ncia');
                              exit;
                    End;

                    limpaedit(PanelProdutos);

                    combotipoprodutos_nf.ItemIndex:=2;
                    combotipoprodutos_nfChange(sender);

                    edtcodigo_nf.Text       :=Objkitbox_nf.Get_CODIGO;
                    edtproduto_nf.text      :=Objkitbox_nf.kitboxCor.get_codigo;
                    edtquantidade_nf.text   :=Objkitbox_nf.get_quantidade;
                    edtvalor_nf.text        :=Objkitbox_nf.Get_Valor;


                    EdtSituacaoTributaria_TabelaA.text:=Objkitbox_nf.SituacaoTributaria_TabelaA. get_codigo;
                    EdtSituacaoTributaria_TabelaB.text:=Objkitbox_nf.SituacaoTributaria_TabelaB. get_codigo;
                    edtipi.text                       :=Objkitbox_nf.Get_IPI;
                    edtpercentualagregado.text        :=Objkitbox_nf.Get_percentualagregado;
                    edtMargemvaloragregadoconsumidor.text        :=Objkitbox_nf.Get_Margemvaloragregadoconsumidor;
                    EdtAliquota.text                  :=Objkitbox_nf.Get_ALIQUOTA;
                    edtreducaobasecalculo.text        :=Objkitbox_nf.Get_REDUCAOBASECALCULO;
                    edtaliquotacupom.text             :=Objkitbox_nf.Get_ALIQUOTACUPOM;
                    edtvalorfrete_produto.Text                :=Objkitbox_nf.Get_valorfrete;
                    edtvalorseguro_produto.text               :=Objkitbox_nf.Get_valorseguro;

                    combotributacao.ItemIndex:=2;
                    
                    if (Objkitbox_nf.Get_ISENTO='S')
                    then combotributacao.ItemIndex:=0;

                    if (Objkitbox_nf.Get_SUBSTITUICAOTRIBUTARIA='S')
                    then combotributacao.ItemIndex:=1;

                    if (panelimpostos.Visible=True)
                    Then Begin
                            edtcfop_fornecedor.Text:=Objkitbox_NF.CFOP.Get_CODIGO;
                            edticms_fornecedor.Text:=Objkitbox_NF.IMPOSTO_ICMS_ORIGEM.Get_CODIGO;
                            edtipi_fornecedor.Text:=Objkitbox_NF.IMPOSTO_IPI.Get_CODIGO;
                            edtpis_fornecedor.text:=Objkitbox_NF.IMPOSTO_PIS_ORIGEM.get_codigo;
                            edtcofins_fornecedor.Text:=Objkitbox_NF.IMPOSTO_COFINS_ORIGEM.Get_CODIGO
                    End;


          End;

          if (StrgProdutos.Cells[0,StrgProdutos.row]='PERFILADO')
          Then Begin
                    if (ObjPERFILADO_nf.LocalizaCodigo(StrgProdutos.Cells[6,StrgProdutos.row])=False)
                    Then exit;
                    ObjPERFILADO_nf.TabelaparaObjeto;

                    if (ObjPERFILADO_nf.Get_referencia<>'')
                    Then Begin
                              BtCancelar_nfClick(sender);
                              mensagemerro('Exclua e lance novamente, pois este registro possui uma refer�ncia');
                              exit;
                    End;

                    limpaedit(PanelProdutos);

                    combotipoprodutos_nf.ItemIndex:=3;
                    combotipoprodutos_nfChange(sender);

                    edtcodigo_nf.Text       :=ObjPERFILADO_nf.Get_CODIGO;
                    edtproduto_nf.text      :=ObjPERFILADO_nf.PERFILADOCor.get_codigo;
                    edtquantidade_nf.text   :=ObjPERFILADO_nf.get_quantidade;
                    edtvalor_nf.text        :=ObjPERFILADO_nf.Get_Valor;

                    EdtSituacaoTributaria_TabelaA.text:=Objperfilado_nf.SituacaoTributaria_TabelaA. get_codigo;
                    EdtSituacaoTributaria_TabelaB.text:=Objperfilado_nf.SituacaoTributaria_TabelaB. get_codigo;
                    edtipi.text                       :=Objperfilado_nf.Get_IPI;
                    edtpercentualagregado.text        :=Objperfilado_nf.Get_percentualagregado;
                    edtMargemvaloragregadoconsumidor.text        :=Objperfilado_nf.Get_Margemvaloragregadoconsumidor;
                    EdtAliquota.text                  :=Objperfilado_nf.Get_ALIQUOTA;
                    edtreducaobasecalculo.text        :=Objperfilado_nf.Get_REDUCAOBASECALCULO;
                    edtaliquotacupom.text             :=Objperfilado_nf.Get_ALIQUOTACUPOM;
                    edtvalorfrete_produto.Text                :=Objperfilado_nf.Get_valorfrete;
                    edtvalorseguro_produto.text               :=Objperfilado_nf.Get_valorseguro;

                    combotributacao.ItemIndex:=2;

                    if (Objperfilado_nf.Get_ISENTO='S')
                    then combotributacao.ItemIndex:=0;

                    if (Objperfilado_nf.Get_SUBSTITUICAOTRIBUTARIA='S')
                    then combotributacao.ItemIndex:=1;

                    if (panelimpostos.Visible=True)
                    Then Begin
                            edtcfop_fornecedor.Text:=Objperfilado_NF.CFOP.Get_CODIGO;
                            edticms_fornecedor.Text:=Objperfilado_NF.IMPOSTO_ICMS_ORIGEM.Get_CODIGO;
                            edtipi_fornecedor.Text:=Objperfilado_NF.IMPOSTO_IPI.Get_CODIGO;
                            edtpis_fornecedor.text:=Objperfilado_NF.IMPOSTO_PIS_ORIGEM.get_codigo;
                            edtcofins_fornecedor.Text:=Objperfilado_NF.IMPOSTO_COFINS_ORIGEM.Get_CODIGO
                    End;


          End;

          if (StrgProdutos.Cells[0,StrgProdutos.row]='PERSIANA')
          Then Begin
                    if (ObjPERSIANA_nf.LocalizaCodigo(StrgProdutos.Cells[6,StrgProdutos.row])=False)
                    Then exit;
                    ObjPERSIANA_nf.TabelaparaObjeto;

                    if (ObjPERSIANA_nf.Get_referencia<>'')
                    Then Begin
                              BtCancelar_nfClick(sender);
                              mensagemerro('Exclua e lance novamente, pois este registro possui uma refer�ncia');
                              exit;
                    End;

                    limpaedit(PanelProdutos);

                    combotipoprodutos_nf.ItemIndex:=4;
                    combotipoprodutos_nfChange(sender);

                    edtcodigo_nf.Text       :=ObjPERSIANA_nf.Get_CODIGO;
                    edtproduto_nf.text      :=ObjPERSIANA_nf.PersianaGrupoDiametroCor.get_codigo;
                    edtquantidade_nf.text   :=ObjPERSIANA_nf.get_quantidade;
                    edtvalor_nf.text        :=ObjPERSIANA_nf.Get_Valor;


                    EdtSituacaoTributaria_TabelaA.text:=Objpersiana_nf.SituacaoTributaria_TabelaA. get_codigo;
                    EdtSituacaoTributaria_TabelaB.text:=Objpersiana_nf.SituacaoTributaria_TabelaB. get_codigo;
                    edtipi.text                       :=Objpersiana_nf.Get_IPI;
                    edtpercentualagregado.text        :=Objpersiana_nf.Get_percentualagregado;
                    edtMargemvaloragregadoconsumidor.text        :=Objpersiana_nf.Get_Margemvaloragregadoconsumidor;
                    EdtAliquota.text                  :=Objpersiana_nf.Get_ALIQUOTA;
                    edtreducaobasecalculo.text        :=Objpersiana_nf.Get_REDUCAOBASECALCULO;
                    edtaliquotacupom.text             :=Objpersiana_nf.Get_ALIQUOTACUPOM;
                    edtvalorfrete_produto.Text                :=Objpersiana_nf.Get_valorfrete;
                    edtvalorseguro_produto.text               :=Objpersiana_nf.Get_valorseguro;

                    combotributacao.ItemIndex:=2;


                    if (Objpersiana_nf.Get_ISENTO='S')
                    then combotributacao.ItemIndex:=0;

                    if (Objpersiana_nf.Get_SUBSTITUICAOTRIBUTARIA='S')
                    then combotributacao.ItemIndex:=1;

                    if (panelimpostos.Visible=True)
                    Then Begin
                            edtcfop_fornecedor.Text:=Objpersiana_NF.CFOP.Get_CODIGO;
                            edticms_fornecedor.Text:=Objpersiana_NF.IMPOSTO_ICMS_ORIGEM.Get_CODIGO;
                            edtipi_fornecedor.Text:=Objpersiana_NF.IMPOSTO_IPI.Get_CODIGO;
                            edtpis_fornecedor.text:=Objpersiana_NF.IMPOSTO_PIS_ORIGEM.get_codigo;
                            edtcofins_fornecedor.Text:=Objpersiana_NF.IMPOSTO_COFINS_ORIGEM.Get_CODIGO
                    End;



          End;

          if (StrgProdutos.Cells[0,StrgProdutos.row]='VIDRO')
          Then Begin
                    if (ObjVIDRO_nf.LocalizaCodigo(StrgProdutos.Cells[6,StrgProdutos.row])=False)
                    Then exit;
                    ObjVIDRO_nf.TabelaparaObjeto;

                    if (ObjVIDRO_nf.Get_referencia<>'')
                    Then Begin
                              BtCancelar_nfClick(sender);
                              mensagemerro('Exclua e lance novamente, pois este registro possui uma refer�ncia');
                              exit;
                    End;

                    limpaedit(PanelProdutos);

                    combotipoprodutos_nf.ItemIndex:=5;
                    combotipoprodutos_nfChange(sender);

                    edtcodigo_nf.Text       :=ObjVIDRO_nf.Get_CODIGO;
                    edtproduto_nf.text      :=ObjVIDRO_nf.VIDROCor.VIDRO.get_codigo;
                    edtquantidade_nf.text   :=ObjVIDRO_nf.get_quantidade;
                    edtvalor_nf.text        :=ObjVIDRO_nf.Get_Valor;

                    EdtSituacaoTributaria_TabelaA.text:=Objvidro_nf.SituacaoTributaria_TabelaA. get_codigo;
                    EdtSituacaoTributaria_TabelaB.text:=Objvidro_nf.SituacaoTributaria_TabelaB. get_codigo;
                    edtipi.text                       :=Objvidro_nf.Get_IPI;
                    edtpercentualagregado.text        :=Objvidro_nf.Get_percentualagregado;
                    edtMargemvaloragregadoconsumidor.text        :=Objvidro_nf.Get_Margemvaloragregadoconsumidor;
                    EdtAliquota.text                  :=Objvidro_nf.Get_ALIQUOTA;
                    edtreducaobasecalculo.text        :=Objvidro_nf.Get_REDUCAOBASECALCULO;
                    edtaliquotacupom.text             :=Objvidro_nf.Get_ALIQUOTACUPOM;
                    edtvalorfrete_produto.Text                :=ObjVidro_nf.Get_valorfrete;
                    edtvalorseguro_produto.text               :=ObjVidro_nf.Get_valorseguro;

                    combotributacao.ItemIndex:=2;

                    if (Objvidro_nf.Get_ISENTO='S')
                    then combotributacao.ItemIndex:=0;

                    if (Objvidro_nf.Get_SUBSTITUICAOTRIBUTARIA='S')
                    then combotributacao.ItemIndex:=1;

                    if (panelimpostos.Visible=True)
                    Then Begin
                            edtcfop_fornecedor.Text:=Objvidro_NF.CFOP.Get_CODIGO;
                            edticms_fornecedor.Text:=Objvidro_NF.IMPOSTO_ICMS_ORIGEM.Get_CODIGO;
                            edtipi_fornecedor.Text:=Objvidro_NF.IMPOSTO_IPI.Get_CODIGO;
                            edtpis_fornecedor.text:=Objvidro_NF.IMPOSTO_PIS_ORIGEM.get_codigo;
                            edtcofins_fornecedor.Text:=Objvidro_NF.IMPOSTO_COFINS_ORIGEM.Get_CODIGO
                    End;

          End;

          lbnomeproduto.caption:='';
          edtproduto_nf.SetFocus;

     End;

end;

procedure TFNotaFiscal.StrgProdutosEnter(Sender: TObject);
begin
edtpesquisa_STRG_GRID.visible:=False;
end;

procedure TFNotaFiscal.StrgProdutosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

     if (StrgProdutos.cells[0,1]='')
     Then exit;

     if (key=vk_f12)
     Then Ordena_StringGrid(StrgProdutos,LbtipoCampoProduto.Items);

end;

procedure TFNotaFiscal.StrgProdutosKeyPress(Sender: TObject;
  var Key: Char);
begin
 If (Key=#13)
     Then StrgProdutos.OnDblClick(nil);

     if (key=#32)
     Then Begin
               if (StrgProdutos.cells[0,1]='')
               Then exit;
               PreparaPesquisa_StringGrid(StrgProdutos,edtpesquisa_STRG_GRID,LbtipoCampoProduto.Items);
     End;


end;

procedure TFNotaFiscal.edtpesquisa_STRG_GRIDKeyPress(Sender: TObject;
  var Key: Char);
begin
   if key =#27//ESC
   then Begin
           edtpesquisa_STRG_GRID.Visible := false;
           StrgProdutos.SetFocus;
           exit;
   End;

   if key=#13
   Then Begin
            Pesquisa_StringGrid(StrgProdutos,edtpesquisa_STRG_GRID,LbtipoCampoproduto.Items);
            StrgProdutos.SetFocus;
   End;

end;

procedure TFNotaFiscal.btrelatoriosClick(Sender: TObject);
begin
     ObjNotaFiscalObjetos.ImprimeNFRDPRINT(EdtCodigo.Text);
     if(edtCodigo.Text='')
     then exit;
     ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(edtcodigo.text);
     ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
     Self.ObjetoParaControles;
end;

procedure TFNotaFiscal.btopcoesClick(Sender: TObject);
begin
     ObjNotaFiscalObjetos.Opcoes(EdtCodigo.text);

     if (EdtCodigo.Text='')
     then exit;

     if (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(EdtCodigo.Text)=True)
     Then Begin
               limpaedit(Self);
               Self.LimpaLabels;
               Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.TabelaparaObjeto;
               Self.TabelaParaControles;
     End
     Else Begin
               limpaedit(Self);
               Self.LimpaLabels;
     end;
end;

procedure TFNotaFiscal.lstLbCfopsKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

     (*if key=vk_delete
     Then Begin
               if (ppermiteexcluircfop=False)
               Then Begin
                         MensagemErro('O usu�rio atual n�o tem permiss�o para excluir CFOPs da NF');
                         exit;
               End;

               Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.exclui_Cfop(EdtCodigo.text,LbCfops.Items[LbCfops.itemindex]);
               Self.ResgataCFOPS;
     End;*)
end;

(*procedure TFNotaFiscal.ResgataCFOPS;
var
ptemp,ptemp1,ptemp2:string;
cont:integer;
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.RetornaCFOP_por_Nota(EdtCodigo.Text,ptemp1,ptemp2);

     ptemp:='';
     LbCfops.Items.Clear;
     for cont:=1 to length(ptemp1) do
     Begin
          if ptemp1[cont]<>'/'
          Then ptemp:=ptemp+ptemp1[cont]
          Else begin
                    LbCfops.Items.add(ptemp);
                    ptemp:='';
          End;
     End;

     if (ptemp<>'')
     Then LbCfops.Items.add(ptemp);

     EdtNATUREZAOPERACAO.Text:=ptemp2;
end;*)




procedure TFNotaFiscal.CalculaVidro;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjNotaFiscalObjetos.ObjVidro_NF.VIDROCOR do
      Begin
           PValor:=0;


           if (cbbcombotabeladepreco.itemindex=0)then
           Begin
              PValor := StrToCurr(Vidro.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=1)then
           Begin
              PValor := StrToCurr(Vidro.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=2)then
           Begin
              PValor := StrToCurr(Vidro.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

      end;
end;


procedure TFNotaFiscal.CalculaPerfilado;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjNotaFiscalObjetos.ObjPerfilado_NF.PERFILADOCOR do
      Begin                                            

           PValor:=0;


           if (cbbcombotabeladepreco.itemindex=0)then
           Begin
              PValor := StrToCurr(Perfilado.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=1)then
           Begin
              PValor := StrToCurr(Perfilado.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=2)then
           Begin
              PValor := StrToCurr(Perfilado.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

      end;
end;

procedure TFNotaFiscal.CalculaKitBox;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjNotaFiscalObjetos.ObjKitBox_NF.KITBOXCOR do
      Begin

           PValor:=0;

           if (cbbcombotabeladepreco.itemindex=0)then
           Begin
              PValor := StrToCurr(KitBox.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=1)then
           Begin
              PValor := StrToCurr(KitBox.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=2)then
           Begin
              PValor := StrToCurr(KitBox.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

      end;
end;

procedure TFNotaFiscal.CalculaDiverso;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjNotaFiscalObjetos.ObjDiverso_nf.DIVERSOCOR do
      Begin


           if (cbbcombotabeladepreco.ItemIndex=0) //INSTALADO
           then Begin
              PValor := StrToCurr(Diverso.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.Text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=1)then//FORNECIDO
           Begin
              PValor := StrToCurr(Diverso.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.itemindex=2)then //RETIRADO
           Begin
              PValor := StrToCurr(Diverso.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

      end;
end;

procedure TFNotaFiscal.CalculaPersianaGrupoDiametro;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjNotaFiscalObjetos.ObjPersiana_NF.PERSIANAGRUPODIAMETROCOR do
      Begin

           PValor:=0;

           if (cbbcombotabeladepreco.itemindex=0)then
           Begin
              edtvalor_nf.text:=Get_PrecoVendaInstalado;
           end;

           if (cbbcombotabeladepreco.itemindex=1)then
           Begin
              edtvalor_nf.text:=Get_PrecoVendaFornecido;
           end;

           if (cbbcombotabeladepreco.itemindex=2)then
           Begin
              edtvalor_nf.text:=Get_PrecoVendaRetirado;
           end;

      end;
end;

procedure TFNotaFiscal.CalculaFerragem;
Var PValor, PValorVenda:Currency;
begin
      With Self.ObjNotaFiscalObjetos.ObjFerragem_NF.FERRAGEMCOR do
      Begin
           PValor:=0;


           if (cbbcombotabeladepreco.ItemIndex=0)
           then Begin
              PValor := StrToCurr(Ferragem.Get_PrecoVendaInstalado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.ItemIndex=1)
           then Begin
              PValor := StrToCurr(Ferragem.Get_PrecoVendaFornecido);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;

           if (cbbcombotabeladepreco.ItemIndex=2)
           then Begin
              PValor := StrToCurr(Ferragem.Get_PrecoVendaRetirado);
              PValorVenda:=PValor+(PValor*(StrToCurr(Get_PorcentagemAcrescimoFinal)/100));
              edtvalor_nf.text:=currtostr(PValorVenda);
           end;
      end;

end;

procedure TFNotaFiscal.edtoperacaoExit(Sender: TObject);
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.EdtoperacaoExit(sender,Lbnomeoperacao);
end;

procedure TFNotaFiscal.edtoperacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.EdtoperacaoKeyDown(sender,key,shift,Lbnomeoperacao);
end;

procedure TFNotaFiscal.edtfornecedorExit(Sender: TObject);
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.EdtFornecedorExit(sender,lbnomecliente);
end;

procedure TFNotaFiscal.edtfornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.EdtFornecedorKeyDown(sender,key,shift,lbnomecliente);
end;

procedure TFNotaFiscal.edticms_fornecedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.EdtIMPOSTOKeyDown(sender,key,shift,nil);
end;

procedure TFNotaFiscal.edticms_fornecedorExit(Sender: TObject);
begin
    Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.EdtIMPOSTOExit(sender,nil);
end;

procedure TFNotaFiscal.edtipi_fornecedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Edtimposto_ipiKeyDown(sender,key,shift,nil);
end;

procedure TFNotaFiscal.edtipi_fornecedorExit(Sender: TObject);
begin
     Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Edtimposto_ipiExit(sender,nil);
end;

procedure TFNotaFiscal.edtpis_fornecedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Edtimposto_pisKeyDown(sender,key,shift,nil);
end;

procedure TFNotaFiscal.edtpis_fornecedorExit(Sender: TObject);
begin
     Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Edtimposto_pisExit(sender,nil);
end;

procedure TFNotaFiscal.edtcofins_fornecedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Edtimposto_cofinsKeyDown(sender,key,shift,nil);
end;

procedure TFNotaFiscal.edtcofins_fornecedorExit(Sender: TObject);
begin
     Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.Edtimposto_cofinsExit(sender,nil);
end;

procedure TFNotaFiscal.edtcfop_fornecedorExit(Sender: TObject);
begin
    Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.IMPOSTO.EdtCFOPExit(sender,nil);
end;

procedure TFNotaFiscal.edtcfop_fornecedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
      Self.ObjNotaFiscalObjetos.ObjVidro_ICMS.IMPOSTO.EdtCFOPKeyDown(sender,key,shift,nil);
end;

procedure TFNotaFiscal.edtnf_referenciaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.EdtNOTAFISCALKeyDown(sender,key,shift,nil);
end;

procedure TFNotaFiscal.edtnf_referenciaExit(Sender: TObject);
begin
   Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.EdtNOTAFISCALExit(sender,nil);
end;

procedure TFNotaFiscal.edtproduto_refKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  ptemp,ptipo:string;
  posicao:integer;
begin
     if key<>vk_f9
     Then exit;

     case combotipoprodutos_nf.ItemIndex of
       0:ptipo:='DIVERSO';
       1:ptipo:='FERRAGEM';
       2:ptipo:='KITBOX';
       3:ptipo:='PERFILADO';
       4:ptipo:='PERSIANA';
       5:ptipo:='VIDRO';
     End;

     Self.ObjNotaFiscalObjetos.EdtLocalizaProdutoReferenciaKeyDown(sender,key,shift,ptipo,edtnf_referencia.Text);

end;

procedure TFNotaFiscal.edtproduto_refExit(Sender: TObject);
var
  ptemp,ptipo:string;
  posicao:integer;
begin

     edtproduto_nf.Text:='';
     edtvalor_nf.text:='';

     if (edtproduto_ref.Text='')
     Then exit;

     With Self.ObjNotaFiscalObjetos do
     Begin
         case combotipoprodutos_nf.ItemIndex of
           //0: ptipo:='DIVERSO';
           0:Begin
                  if (ObjDiverso_NF.LocalizaCodigo(edtproduto_ref.Text)=False)
                  Then Begin
                            edtproduto_ref.Text:='';
                            MensagemErro('N�o foi encontrado o diverso na NF');
                            exit;
                  End;
                  ObjDiverso_NF.TabelaparaObjeto;
                  edtproduto_nf.Text:=ObjDiverso_NF.diversoCOR.Get_Codigo;
                  edtvalor_nf.text:=ObjDiverso_NF.Get_VALOR;
           End;
           //1: ptipo:='FERRAGEM';
           1:Begin

                  if (ObjFerragem_NF.LocalizaCodigo(edtproduto_ref.Text)=False)
                  Then Begin
                            edtproduto_ref.Text:='';
                            MensagemErro('N�o foi encontrado a Ferragem na NF');
                            exit;
                  End;
                  ObjFerragem_NF.TabelaparaObjeto;
                  edtproduto_nf.Text:=ObjFerragem_NF.FerragemCOR.Get_Codigo;
                  edtvalor_nf.text:=ObjFerragem_NF.Get_VALOR;
           End;
           //2:ptipo:='KITBOX';
           2:Begin

                  if (ObjKitbox_NF.LocalizaCodigo(edtproduto_ref.Text)=False)
                  Then Begin
                            edtproduto_ref.Text:='';
                            MensagemErro('N�o foi encontrado o Kitbox na NF');
                            exit;
                  End;
                  ObjKitbox_NF.TabelaparaObjeto;
                  edtproduto_nf.Text:=ObjKitbox_NF.KitboxCOR.Get_Codigo;
                  edtvalor_nf.text:=ObjKitbox_NF.Get_VALOR;
           End;
           //3:ptipo:='PERFILADO';
           3:Begin
                  if (ObjPerfilado_NF.LocalizaCodigo(edtproduto_ref.Text)=False)
                  Then Begin
                            edtproduto_ref.Text:='';
                            MensagemErro('N�o foi encontrado o Perfilado na NF');
                            exit;
                  End;
                  ObjPerfilado_NF.TabelaparaObjeto;
                  edtproduto_nf.Text:=ObjPerfilado_NF.PerfiladoCOR.Get_Codigo;
                  edtvalor_nf.text:=ObjPerfilado_NF.Get_VALOR;
           End;
           //4:ptipo:='PERSIANA';
           4:Begin
                  if (ObjPersiana_NF.LocalizaCodigo(edtproduto_ref.Text)=False)
                  Then Begin
                            edtproduto_ref.Text:='';
                            MensagemErro('N�o foi encontrado a Persiana na NF');
                            exit;
                  End;
                  ObjPersiana_NF.TabelaparaObjeto;
                  edtproduto_nf.Text:=ObjPersiana_NF.persianagrupodiametrocor.Get_Codigo;
                  edtvalor_nf.text:=ObjPersiana_NF.Get_VALOR;
           End;
           //ptipo:='VIDRO';
           5:Begin

                  if (ObjVidro_NF.LocalizaCodigo(edtproduto_ref.Text)=False)
                  Then Begin
                            edtproduto_ref.Text:='';
                            MensagemErro('N�o foi encontrado o Vidro na NF');
                            exit;
                  End;
                  ObjVidro_NF.TabelaparaObjeto;
                  edtproduto_nf.Text:=ObjVidro_NF.VidroCOR.Get_Codigo;
                  edtvalor_nf.text:=ObjVidro_NF.Get_VALOR;
           End;
           
         End;
     End;

     edtquantidade_nf.setfocus;//para evitar voltar no produto e alterar o valor

end;

procedure TFNotaFiscal.BitBtn1Click(Sender: TObject);
var
ptemp:string;
begin
     if (inputquery('Pesquisa de NF','Digite o C�digo',ptemp)=False)
     then exit;

     
     If (Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.LocalizaCodigo(ptemp)=False)
     Then Begin
               Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
               exit;
          End;

     Self.ObjNotaFiscalObjetos.ObjNotaFiscalCfop.NOTAFISCAL.ZERARTABELA;

     If (TabelaParaControles=False)
     Then Begin
               Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
               limpaedit(Self);
               Self.LimpaLabels;
               exit;
     End;
end;

procedure TFNotaFiscal.StrgProdutosDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}

begin
          //ZEBRANDO A STRIGRID
          if ((ARow mod 2)=0) then
          begin
               StrgProdutos.Canvas.Brush.Color := CORGRIDZEBRADOGLOBAL2;
               StrgProdutos.Canvas.Font.Color := clBlack;
          end
          else
          begin
             StrgProdutos.Canvas.Brush.Color :=clWhite;
             StrgProdutos.Canvas.Font.Color := clBlack;
          end;
         StrgProdutos.Canvas.TextRect(Rect, Rect.Left + LM, Rect.Top + TM, StrgProdutos.Cells[Acol,Arow]);



end;

procedure TFNotaFiscal.edtoperacaoKeyPress(Sender: TObject; var Key: Char);
begin
    if not (Key in['0'..'9',Chr(8)]) then
    begin
            Key:= #0;
    end;

end;

procedure TFNotaFiscal.edtfornecedorKeyPress(Sender: TObject;
  var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8)]) then
    begin
            Key:= #0;
    end;
end;

procedure TFNotaFiscal.edtclienteKeyPress(Sender: TObject; var Key: Char);
begin
     if not (Key in['0'..'9',Chr(8)]) then
    begin
            Key:= #0;
    end;
end;

procedure TFNotaFiscal.edtModeloNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabmodelonf','',nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  edtModeloNF.Text:=FpesquisaLocal.querypesq.fieldbyname('codigo').AsString;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;


end;

procedure TFNotaFiscal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  formPesquisaMenu:TfPesquisaMenu;
begin
    If Key=VK_Escape
    Then Self.Close;
     if (Key = VK_F1) then
    begin

      try

        formPesquisaMenu:=TfPesquisaMenu.Create(nil);
        formPesquisaMenu.submit_formulario(Fprincipal);
        formPesquisaMenu.ShowModal;

      finally
        FreeAndNil(formPesquisaMenu);
      end;

    end;

    if (Key = VK_F2) then
    begin

         FAjuda.PassaAjuda('NOTA FISCAL');
         FAjuda.ShowModal;
    end;


end;

procedure TFNotaFiscal.bt1Click(Sender: TObject);
begin
      FAjuda.PassaAjuda('NOTA FISCAL');
      FAjuda.ShowModal
end;

{r4mr}
procedure TFNotaFiscal.edtoperacaoDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtoperacaoKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtfornecedorDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtfornecedorKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtModeloNFDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtModeloNFKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtclienteDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtclienteKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtproduto_nfDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtproduto_nfKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtcfop_fornecedorDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtcfop_fornecedorKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edticms_fornecedorDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edticms_fornecedorKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtipi_fornecedorDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtipi_fornecedorKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtpis_fornecedorDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtpis_fornecedorKeyDown(Sender, Key, Shift);
end;

{r4mr}
procedure TFNotaFiscal.edtcofins_fornecedorDblClick(Sender: TObject);
var
  Key : Word;
  Shift : TShiftState;
begin
  Key := VK_F9;
  edtcofins_fornecedorKeyDown(Sender, Key, Shift);
end;

end.



