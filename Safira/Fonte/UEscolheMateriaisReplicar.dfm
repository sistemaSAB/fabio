object FEscolheMateriaisReplicar: TFEscolheMateriaisReplicar
  Left = 646
  Top = 267
  Width = 935
  Height = 556
  BorderIcons = [biSystemMenu]
  Caption = 'Replica'#231#227'o de Impostos'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel: TPanel
    Left = 0
    Top = 0
    Width = 927
    Height = 72
    Align = alTop
    BevelOuter = bvNone
    Color = clWhite
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 0
    object lb2: TLabel
      Left = 322
      Top = 1
      Width = 310
      Height = 37
      Caption = 'R'#233'plica de Impostos'
      Font.Charset = ANSI_CHARSET
      Font.Color = clGray
      Font.Height = -32
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbselecionartodos: TLabel
      Left = 2
      Top = 17
      Width = 141
      Height = 17
      Cursor = crHandPoint
      Hint = 'Nome do Colocador'
      Align = alCustom
      Caption = 'F5 - Selecionar Todos'
      Font.Charset = ANSI_CHARSET
      Font.Color = 33023
      Font.Height = -12
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Button1Click
      OnMouseMove = lbselecionartodosMouseMove
      OnMouseLeave = lbselecionartodosMouseLeave
    end
    object lb1: TLabel
      Left = 2
      Top = 34
      Width = 192
      Height = 17
      Cursor = crHandPoint
      Hint = 'Nome do Colocador'
      Align = alCustom
      Caption = 'F6 - Retirar Sele'#231#227'o de Todos'
      Font.Charset = ANSI_CHARSET
      Font.Color = 33023
      Font.Height = -12
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Button2Click
      OnMouseMove = lbselecionartodosMouseMove
      OnMouseLeave = lbselecionartodosMouseLeave
    end
    object lb3: TLabel
      Left = 2
      Top = 52
      Width = 240
      Height = 17
      Cursor = crHandPoint
      Hint = 'Nome do Colocador'
      Align = alCustom
      Caption = 'F7 - Selecionar do ponto atual acima'
      Font.Charset = ANSI_CHARSET
      Font.Color = 33023
      Font.Height = -12
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Button3Click
      OnMouseMove = lbselecionartodosMouseMove
      OnMouseLeave = lbselecionartodosMouseLeave
    end
  end
  object GRID: TStringGrid
    Left = 0
    Top = 96
    Width = 927
    Height = 370
    Align = alClient
    BorderStyle = bsNone
    ColCount = 7
    Ctl3D = False
    FixedCols = 0
    Options = [goVertLine, goHorzLine]
    ParentCtl3D = False
    TabOrder = 1
    OnDblClick = GRIDDblClick
    OnKeyPress = GRIDKeyPress
  end
  object pnl1: TPanel
    Left = 0
    Top = 466
    Width = 927
    Height = 59
    Align = alBottom
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 4
      Width = 925
      Height = 54
      Align = alBottom
    end
  end
  object pnl6: TPanel
    Left = 0
    Top = 72
    Width = 927
    Height = 24
    Align = alTop
    BevelOuter = bvNone
    Color = 10643006
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
  end
end
