unit UItensCarrinho;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, ExtCtrls, ImgList;

type
  TFItensCarrinhos = class(TForm)
    STRGItensPedido: TStringGrid;
    panelpnl1: TPanel;
    lbnomeformulario: TLabel;
    panelpnl2: TPanel;
    img1: TImage;
    ilProdutos: TImageList;
    procedure FormShow(Sender: TObject);
    procedure STRGItensPedidoDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure STRGItensPedidoClick(Sender: TObject);
    procedure STRGItensPedidoDblClick(Sender: TObject);

  private
    procedure __Delete;
  public
    procedure __MontaStringGrid;
    procedure __LimpaStringGrid;
    function ___LocalizaItemCarrinho(pedido,pedidoprojeto,itemcor,item:string):boolean;
  end;

var
  FItensCarrinhos: TFItensCarrinhos;

implementation

uses UescolheImagemBotao;

{$R *.dfm}

procedure TFItensCarrinhos.FormShow(Sender: TObject);
begin
   FescolheImagemBotao.PegaFiguraImagem(Img1,'RODAPE');
end;

procedure TFItensCarrinhos.__MontaStringGrid;
begin
  STRGItensPedido.ColCount:=8;
  STRGItensPedido.RowCount:=2;
  STRGItensPedido.FixedRows:=1;
  STRGItensPedido.ColWidths[0]:=50;
  STRGItensPedido.ColWidths[1]:=0;
  STRGItensPedido.ColWidths[2]:=100;
  STRGItensPedido.ColWidths[3]:=500;
  STRGItensPedido.ColWidths[4]:=0;
  STRGItensPedido.ColWidths[5]:=120;
  STRGItensPedido.ColWidths[6]:=120;
  STRGItensPedido.ColWidths[7]:=0;

  STRGItensPedido.Cells[0,0]:='';
  STRGItensPedido.Cells[1,0]:='Pedido';
  STRGItensPedido.Cells[2,0]:='TIPO';
  STRGItensPedido.Cells[3,0]:='�TEM';
  STRGItensPedido.Cells[4,0]:='Ped/Projeto';
  STRGItensPedido.Cells[5,0]:='Quantidade Vendida';
  STRGItensPedido.Cells[6,0]:='Quantidade Estoque';
  STRGItensPedido.Cells[7,0]:='ItemCor';

end;

procedure TFItensCarrinhos.__LimpaStringGrid;
var
  i:Integer;
begin
  for i:=0 to STRGItensPedido.RowCount do
  begin
    STRGItensPedido.Cells[0,i]:='';
    STRGItensPedido.Cells[1,i]:='';
    STRGItensPedido.Cells[2,i]:='';
    STRGItensPedido.Cells[3,i]:='';
    STRGItensPedido.Cells[4,i]:='';
    STRGItensPedido.Cells[5,i]:='';
    STRGItensPedido.Cells[6,i]:='';
    STRGItensPedido.Cells[7,i]:='';
  end;
end;


procedure TFItensCarrinhos.STRGItensPedidoDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
const
LM = 10; {margem esquerda de cada c�lula}
TM = 5; {margem superior de cada c�lula}
begin
  if((ARow <> 0) and (ACol = 0))then
  begin
    Ilprodutos.Draw(STRGItensPedido.Canvas, LM*3 + Rect.Left+1 , TM + Rect.Top+1, 4); //check box sem marcar    5
  end;
end;


function TFItensCarrinhos.___LocalizaItemCarrinho(pedido,pedidoprojeto,itemcor,item:string):boolean;
var
  i:Integer;
begin
  result:=False;
  for i:=0 to STRGItensPedido.RowCount -1 do
  begin
    if(STRGItensPedido.Cells[1,i]=pedido)
    and (STRGItensPedido.Cells[2,i]=item)
    //and (STRGItensPedido.Cells[4,i]=pedidoprojeto)
    and (STRGItensPedido.Cells[7,i]=itemcor)
    then
    begin
      Result:=True;
    end;
  end;
end;

procedure TFItensCarrinhos.STRGItensPedidoClick(Sender: TObject);
begin
//
end;

procedure TFItensCarrinhos.__Delete;
var
    nlinha: integer;
begin
  if(STRGItensPedido.Row=0)then
  begin
         exit;
  end;


  if STRGItensPedido.RowCount = 1 then
  begin
    STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;
  end;
  if STRGItensPedido.RowCount = 2 then
  begin
    STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;
    Exit;
  end;
  if STRGItensPedido.RowCount-1 = STRGItensPedido.Row then
  begin
    STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;

  end;

  if (STRGItensPedido.RowCount > 1) and (STRGItensPedido.RowCount <> STRGItensPedido.Row) then
  begin

    {Se o numero de linha for igual ao numero de ao numero da linha ent�o ele simplesmente limpa as c�lulas, ou entao ele
    llimpa as celulas da linhas selecionada e copia o conteudo da proxima, e assim por diante ate chegar na ultima, que ser� excluida.}
    STRGItensPedido.Cells[0,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[1,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[2,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[3,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[4,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[5,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[6,STRGItensPedido.Row] := '';
    STRGItensPedido.Cells[7,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[8,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[9,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[10,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[11,STRGItensPedido.Row] := '';
    //STRGItensPedido.Cells[12,STRGItensPedido.Row] :='' ;

    nlinha := STRGItensPedido.Row;

    while STRGItensPedido.Row <> STRGItensPedido.RowCount do
    begin
      if STRGItensPedido.Cells[1,(nlinha + 1)] <> '' then
      begin
        STRGItensPedido.Cells[0,STRGItensPedido.Row] := STRGItensPedido.Cells[0,(nlinha +1)];
        STRGItensPedido.Cells[1,STRGItensPedido.Row] := STRGItensPedido.Cells[1,(nlinha +1)];
        STRGItensPedido.Cells[2,STRGItensPedido.Row] := STRGItensPedido.Cells[2,(nlinha +1)];
        STRGItensPedido.Cells[3,STRGItensPedido.Row] := STRGItensPedido.Cells[3,(nlinha +1)];
        STRGItensPedido.Cells[4,STRGItensPedido.Row] := STRGItensPedido.Cells[4,(nlinha +1)];
        STRGItensPedido.Cells[5,STRGItensPedido.Row] := STRGItensPedido.Cells[5,(nlinha +1)];
        STRGItensPedido.Cells[6,STRGItensPedido.Row] := STRGItensPedido.Cells[6,(nlinha +1)];
        STRGItensPedido.Cells[7,STRGItensPedido.Row] := STRGItensPedido.Cells[7,(nlinha +1)];
        //STRGItensPedido.Cells[8,STRGItensPedido.Row] := STRGItensPedido.Cells[8,(nlinha +1)];
        //STRGItensPedido.Cells[9,STRGItensPedido.Row] := STRGItensPedido.Cells[9,(nlinha +1)];
        //STRGItensPedido.Cells[10,STRGItensPedido.Row] := STRGItensPedido.Cells[10,(nlinha +1)];
        //STRGItensPedido.Cells[11,STRGItensPedido.Row] := STRGItensPedido.Cells[11,(nlinha +1)];
        //STRGItensPedido.Cells[12,STRGItensPedido.Row] := STRGItensPedido.Cells[12,(nlinha +1)];
      end
      else
      begin
        STRGItensPedido.RowCount := STRGItensPedido.RowCount - 1;
        STRGItensPedido.Cells[0,nlinha] := '';
        STRGItensPedido.Cells[1,nlinha] := '';
        STRGItensPedido.Cells[2,nlinha] := '';
        STRGItensPedido.Cells[3,nlinha] := '';
        STRGItensPedido.Cells[4,nlinha] := '';
        STRGItensPedido.Cells[5,nlinha] := '';
        STRGItensPedido.Cells[6,nlinha] := '';
        STRGItensPedido.Cells[7,nlinha] := '';
        //STRGItensPedido.Cells[8,nlinha] := '';
        //STRGItensPedido.Cells[9,nlinha] := '';
        //STRGItensPedido.Cells[10,nlinha] := '';
        //STRGItensPedido.Cells[11,nlinha] := '';
        //STRGItensPedido.Cells[12,nlinha] := '';
        exit;

      end;
      STRGItensPedido.Row := STRGItensPedido.Row + 1;
      nlinha := STRGItensPedido.Row;
    end;
  end;
end;



procedure TFItensCarrinhos.STRGItensPedidoDblClick(Sender: TObject);
begin
   if(STRGItensPedido.Col=0)
   then __Delete;
end;

end.
