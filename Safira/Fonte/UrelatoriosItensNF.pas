unit UrelatoriosItensNF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons,UobjNotaFiscalObjetos;

type
  TFrelatoriosItensNF = class(TForm)
    Label3: TLabel;
    BtImprimir: TBitBtn;
    ComboMaterial: TComboBox;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtImprimirClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    ObjNotaFiscalObjetos:TobjNotaFiscalObjetos;
  public
    { Public declarations }
  end;

var
  FrelatoriosItensNF: TFrelatoriosItensNF;

implementation

uses UessencialGlobal;

{$R *.dfm}

procedure TFrelatoriosItensNF.FormShow(Sender: TObject);
begin
     combomaterial.text:='';
     limpaedit(self);
     BtImprimir.Enabled:=False;
     desabilita_campos(self);
     PegaFiguraBotao(BtImprimir,'Botaorelatorios.Bmp'); 
     Try
        objNotaFiscalObjetos:=TobjNotaFiscalObjetos.create(self);
        BtImprimir.Enabled:=true;
        habilita_campos(self);
        ComboMaterial.setfocus;
     except
           mensagemerro('Erro na tentativa de Criar os Objetos');
           exit;
     end;

end;

procedure TFrelatoriosItensNF.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
     if (objNotaFiscalObjetos<>nil)
     Then objNotaFiscalObjetos.free;
end;

procedure TFrelatoriosItensNF.BtImprimirClick(Sender: TObject);
begin
     case ComboMaterial.itemindex of
       0:objNotaFiscalObjetos.ImprimeItensNF('DIVERSO');
       1:objNotaFiscalObjetos.ImprimeItensNF('FERRAGEM');
       2:objNotaFiscalObjetos.ImprimeItensNF('KITBOX');
       3:objNotaFiscalObjetos.ImprimeItensNF('PERFILADO');
       4:objNotaFiscalObjetos.ImprimeItensNF('PERSIANA');
       5:objNotaFiscalObjetos.ImprimeItensNF('VIDRO');
       Else Begin
                 MensagemAviso('Escolha um material');
       End;
     End;
end;

procedure TFrelatoriosItensNF.FormKeyPress(Sender: TObject; var Key: Char);
begin
     if key=#13
     Then Perform(Wm_NextDlgCtl,0,0);
end;

end.
