unit UobjNFEDIGITADA;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,IBStoredProc
,UOBJCLIENTE        
,UOBJFORNECEDOR     
,UOBJNOTAFISCAL     
,UOBJTRANSPORTADORA 
;
//USES_INTERFACE






Type
   TObjNFEDIGITADA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
               CLIENTE:TOBJCLIENTE        ;
               FORNECEDOR:TOBJFORNECEDOR     ;
               NOTA:TOBJNOTAFISCAL     ;
               TRANSPORTADORA:TOBJTRANSPORTADORA ;


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean):Boolean;overload;
                Function    Exclui(Pcodigo:string;ComCommit:boolean;var Perro:String):Boolean;overload;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_VALORDESCONTO(parametro: string);
                Function Get_VALORDESCONTO: string;
                Procedure Submit_VALORNF(parametro: string);
                Function Get_VALORNF: string;
                Procedure Submit_VALORPIS(parametro: string);
                Function Get_VALORPIS: string;
                Procedure Submit_VALORCOFINS(parametro: string);
                Function Get_VALORCOFINS: string;
                Procedure Submit_VALORBASECALCULO(parametro: string);
                Function Get_VALORBASECALCULO: string;
                Procedure Submit_VALORICMS(parametro: string);
                Function Get_VALORICMS: string;
                Procedure Submit_VALORPRODUTO(parametro: string);
                Function Get_VALORPRODUTO: string;
                Procedure Submit_VALORFRETE(parametro: string);
                Function Get_VALORFRETE: string;
                Procedure Submit_VALORSEGURO(parametro: string);
                Function Get_VALORSEGURO: string;
                Procedure Submit_VALOROUTROS(parametro: string);
                procedure submit_valoripi(parametro:string);
                procedure submit_valoricms_st(parametro:string);
                Function Get_VALOROUTROS: string;
                function get_valoripi():string;
                function get_valoricms_st:string;
                Procedure Submit_DATAEMISSAO(parametro: string);
                Function Get_DATAEMISSAO: string;
                Procedure Submit_DATASAIDA(parametro: string);
                Function Get_DATASAIDA: string;
                Procedure Submit_HORASAIDA(parametro: string);
                procedure submit_HORAEMISSAO(parametro:string);
                Function Get_HORASAIDA: string;
                function get_HORAEMISSAO:string;
                Procedure Submit_INFORMACOES(parametro: string);
                procedure submit_IMPOSTOAUTOMATICO(p:string);
                function get_IMPOSTOAUTOMATICO:string;
                Function Get_INFORMACOES: string;
                {Procedure Submit_DataC(parametro: string);
                Function Get_DataC: string;
                Procedure Submit_UserC(parametro: string);
                Function Get_UserC: string;
                Procedure Submit_DataM(parametro: string);
                Function Get_DataM: string;
                Procedure Submit_UserM(parametro: string);
                Function Get_UserM: string;    }
                procedure EdtCLIENTEExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCLIENTEKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtFORNECEDORExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFORNECEDORKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtNOTAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtNOTAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtTRANSPORTADORAExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTRANSPORTADORAKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);

                function get_indpag:string;
                procedure submit_indpag(parametro:string);

                procedure Submit_Operacao(parametro:string);
                function Get_Operacao:string;
                procedure Submit_BaixaEstoque(parametro:string);
                Function Get_BaixaEstoque:string;
                procedure Submit_ADDFATURAS(parametro:string);
                Function Get_AddFaturas:String;


                procedure updateDesconto           (valor,pCodigo:string);
                procedure updateValorProdutos      (valor,pCodigo:string);
                procedure updateValorNF            (valor,pCodigo:string);
                procedure updateValorFrete         (valor,pCodigo:string);
                procedure updateValorIPI           (valor,pCodigo:string);
                procedure updateValorICMSST        (valor,pCodigo:string);
                Function BaixarEstoque              (Pnfedigitada,Pnota:string):Boolean;



         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               VALORDESCONTO:string;
               VALORNF:string;
               VALORPIS:string;
               VALORCOFINS:string;
               VALORBASECALCULO:string;
               VALORICMS:string;
               VALORPRODUTO:string;
               VALORFRETE:string;
               VALORSEGURO:string;
               VALOROUTROS:string;
               VALORIPI:string;
               VALORICMS_ST:string;
               DATAEMISSAO:string;
               DATASAIDA:string;
               HORASAIDA:string;
               HORAEMISSAO:string;
               INFORMACOES:string;
               IMPOSTOAUTOMATICO:string;
               operacao:string;
               baixaestoque:string;
               ADDFATURAS:string;
               indpag:string;
//CODIFICA VARIAVEIS PRIVADAS


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
UmenuRelatorios, UCLIENTE, UFORNECEDOR, UNotaFiscal, UTRANSPORTADORA,
  UobjFERRAGEMCOR, UobjPERFILADO, UobjVIDROCOR, UobjDIVERSOCOR,
  UobjKITBOXCOR, UobjPERSIANAGRUPODIAMETROCOR, UobjPERFILADOCOR;





Function  TObjNFEDIGITADA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('CLIENTE').asstring<>'')
        Then Begin
                 If (Self.CLIENTE.LocalizaCodigo(FieldByName('CLIENTE').asstring)=False)
                 Then Begin
                          Messagedlg('Cliente N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.CLIENTE.TabelaparaObjeto;
        End;
        If(FieldByName('FORNECEDOR').asstring<>'')
        Then Begin
                 If (Self.FORNECEDOR.LocalizaCodigo(FieldByName('FORNECEDOR').asstring)=False)
                 Then Begin
                          Messagedlg('Fornecedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.FORNECEDOR.TabelaparaObjeto;
        End;
        If(FieldByName('NOTA').asstring<>'')
        Then Begin
                 If (Self.NOTA.LocalizaCodigo(FieldByName('NOTA').asstring)=False)
                 Then Begin
                          Messagedlg('Nota N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.NOTA.TabelaparaObjeto;
        End;
        If(FieldByName('TRANSPORTADORA').asstring<>'')
        Then Begin
                 If (Self.TRANSPORTADORA.LocalizaCodigo(FieldByName('TRANSPORTADORA').asstring)=False)
                 Then Begin
                          Messagedlg('Transportadora N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.TRANSPORTADORA.TabelaparaObjeto;
        End;
        Self.VALORDESCONTO:=fieldbyname('VALORDESCONTO').asstring;
        Self.VALORNF:=fieldbyname('VALORNF').asstring;
        Self.VALORPIS:=fieldbyname('VALORPIS').asstring;
        Self.VALORCOFINS:=fieldbyname('VALORCOFINS').asstring;
        Self.VALORBASECALCULO:=fieldbyname('VALORBASECALCULO').asstring;
        Self.VALORICMS:=fieldbyname('VALORICMS').asstring;
        Self.VALORPRODUTO:=fieldbyname('VALORPRODUTO').asstring;
        Self.VALORFRETE:=fieldbyname('VALORFRETE').asstring;
        Self.VALORSEGURO:=fieldbyname('VALORSEGURO').asstring;
        Self.VALOROUTROS:=fieldbyname('VALOROUTROS').asstring;
        self.VALORIPI   := fieldbyname('valoripi').AsString;
        self.VALORICMS_ST := fieldbyname('valoricms_st').AsString;
        Self.DATAEMISSAO:=fieldbyname('DATAEMISSAO').asstring;
        Self.DATASAIDA:=fieldbyname('DATASAIDA').asstring;
        Self.HORASAIDA:=fieldbyname('HORASAIDA').asstring;
        self.HORAEMISSAO:=fieldbyname('HORAEMISSAO').AsString;
        Self.INFORMACOES:=fieldbyname('INFORMACOES').asstring;
        self.IMPOSTOAUTOMATICO:=fieldbyname('IMPOSTOAUTOMATICO').AsString;
       { Self.DataC:=fieldbyname('DataC').asstring;
        Self.UserC:=fieldbyname('UserC').asstring;
        Self.DataM:=fieldbyname('DataM').asstring;
        Self.UserM:=fieldbyname('UserM').asstring; }
        self.operacao:=fieldbyname('operacao').asstring;
        self.baixaestoque:=fieldbyname('baixaestoque').AsString;
        self.ADDFATURAS:=fieldbyname('ADDFATURAS').AsString;
        indpag:= fieldbyname('indpag').AsString;
        result:=True;
     End;
end;


Procedure TObjNFEDIGITADA.ObjetoparaTabela;
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('CLIENTE').asstring:=Self.CLIENTE.GET_CODIGO;
        ParamByName('FORNECEDOR').asstring:=Self.FORNECEDOR.GET_CODIGO;
        ParamByName('NOTA').asstring:=Self.NOTA.GET_CODIGO;
        ParamByName('TRANSPORTADORA').asstring:=Self.TRANSPORTADORA.GET_CODIGO;
        ParamByName('VALORDESCONTO').asstring:=virgulaparaponto(Self.VALORDESCONTO);
        ParamByName('VALORNF').asstring:=virgulaparaponto(Self.VALORNF);
        ParamByName('VALORPIS').asstring:=virgulaparaponto(Self.VALORPIS);
        ParamByName('VALORCOFINS').asstring:=virgulaparaponto(Self.VALORCOFINS);
        ParamByName('VALORBASECALCULO').asstring:=virgulaparaponto(Self.VALORBASECALCULO);
        ParamByName('VALORICMS').asstring:=virgulaparaponto(Self.VALORICMS);
        ParamByName('VALORPRODUTO').asstring:=virgulaparaponto(Self.VALORPRODUTO);
        ParamByName('VALORFRETE').asstring:=virgulaparaponto(Self.VALORFRETE);
        ParamByName('VALORSEGURO').asstring:=virgulaparaponto(Self.VALORSEGURO);
        ParamByName('VALOROUTROS').asstring:=virgulaparaponto(Self.VALOROUTROS);
        ParamByName('valoripi').AsString := virgulaparaponto(Self.VALORIPI);
        ParamByName('valoricms_st').AsString := virgulaparaponto(self.valoricms_st);
        ParamByName('DATAEMISSAO').asstring:=Self.DATAEMISSAO;
        ParamByName('DATASAIDA').asstring:=Self.DATASAIDA;
        ParamByName('HORASAIDA').asstring:=Self.HORASAIDA;
        ParamByName('HORAEMISSAO').asstring:=Self.HORAEMISSAO;
        ParamByName('INFORMACOES').asstring:=Self.INFORMACOES;
        ParamByName('IMPOSTOAUTOMATICO').AsString := self.IMPOSTOAUTOMATICO;
        {ParamByName('DataC').asstring:=Self.DataC;
        ParamByName('UserC').asstring:=Self.UserC;
        ParamByName('DataM').asstring:=Self.DataM;
        ParamByName('UserM').asstring:=Self.UserM;  }
        ParamByName('operacao').asstring:=Self.operacao;
        ParamByName('baixaestoque').AsString:=Self.baixaestoque;
        ParamByName('ADDFATURAS').AsString:=Self.ADDFATURAS;
        ParamByName('indpag').AsString := self.indpag;
  End;
End;

//***********************************************************************

function TObjNFEDIGITADA.Salvar(ComCommit:Boolean): Boolean;
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjNFEDIGITADA.ZerarTabela;
Begin
     With Self do
     Begin
        CODIGO:='';
        CLIENTE.ZerarTabela;
        FORNECEDOR.ZerarTabela;
        NOTA.ZerarTabela;
        TRANSPORTADORA.ZerarTabela;
        VALORDESCONTO:='';
        VALORNF:='';
        VALORPIS:='';
        VALORCOFINS:='';
        VALORBASECALCULO:='';
        VALORICMS:='';
        VALORPRODUTO:='';
        VALORFRETE:='';
        VALORSEGURO:='';
        VALOROUTROS:='';
        VALORIPI := '';
        valoricms_st:='';
        DATAEMISSAO:='';
        DATASAIDA:='';
        HORASAIDA:='';
        HORAEMISSAO:='';
        INFORMACOES:='';
        self.IMPOSTOAUTOMATICO := '';
        operacao:='';
        baixaestoque:='';
        ADDFATURAS:='';
     End;
end;

Function TObjNFEDIGITADA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
  
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      if VALORIPI = '' then
        VALORIPI := '0';

      if VALORICMS_ST = '' then
        VALORICMS_ST := '0';

      if Trim(IMPOSTOAUTOMATICO) = '' then
        IMPOSTOAUTOMATICO := 'N';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjNFEDIGITADA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     if (self.CLIENTE.Get_Codigo <> '') then
     begin
      If (Self.CLIENTE.LocalizaCodigo(Self.CLIENTE.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Cliente n�o Encontrado!';
     end;


     if (self.FORNECEDOR.Get_Codigo <> '') then
     begin
       If (Self.FORNECEDOR.LocalizaCodigo(Self.FORNECEDOR.Get_CODIGO)=False)
       Then Mensagem:=mensagem+'/ Fornecedor n�o Encontrado!';
     end;

     //If (Self.NOTA.LocalizaCodigo(Self.NOTA.Get_CODIGO)=False)
     //Then Mensagem:=mensagem+'/ Nota n�o Encontrado!';


     if (self.TRANSPORTADORA.Get_CODIGO <> '') then
     begin

      If (Self.TRANSPORTADORA.LocalizaCodigo(Self.TRANSPORTADORA.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Transportadora n�o Encontrado!';

     end;



     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjNFEDIGITADA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.CLIENTE.Get_Codigo<>'')
        Then Strtoint(Self.CLIENTE.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cliente';
     End;
     try
        If (Self.FORNECEDOR.Get_Codigo<>'')
        Then Strtoint(Self.FORNECEDOR.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Fornecedor';
     End;
     try
        If (Self.NOTA.Get_Codigo<>'')
        Then Strtoint(Self.NOTA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Nota';
     End;
     try
        If (Self.TRANSPORTADORA.Get_Codigo<>'')
        Then Strtoint(Self.TRANSPORTADORA.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Transportadora';
     End;
     try
        //Strtofloat(virgulaparaponto(Self.VALORDESCONTO));
        Strtofloat((Self.VALORDESCONTO));
     Except
           Mensagem:=mensagem+'/VALORDESCONTO';
     End;

     try
         Strtofloat((tira_ponto(Self.VALORNF)));
         //Strtofloat((Self.VALORNF));
     Except
           Mensagem:=mensagem+'/VALORNF';
     End;

     {try
        Strtofloat(Self.VALORPIS);
     Except
           Mensagem:=mensagem+'/VALORPIS';
     End;
     try
        Strtofloat(Self.VALORCOFINS);
     Except
           Mensagem:=mensagem+'/VALORCOFINS';
     End;
     try
        Strtofloat(Self.VALORBASECALCULO);
     Except
           Mensagem:=mensagem+'/VALORBASECALCULO';
     End;
     try
        Strtofloat(Self.VALORICMS);
     Except
           Mensagem:=mensagem+'/VALORICMS';
     End;}

    { try
        Strtofloat(Self.VALORPRODUTO);
     Except
           Mensagem:=mensagem+'/VALORPRODUTO';
     End;

     try
        Strtofloat(Self.VALORFRETE);
     Except
           Mensagem:=mensagem+'/VALORFRETE';
     End;    

     {try
        Strtofloat(Self.VALORSEGURO);
     Except
           Mensagem:=mensagem+'/VALORSEGURO';
     End;}

     {try
        Strtofloat(Self.VALOROUTROS);
     Except
           Mensagem:=mensagem+'/VALOROUTROS';
     End;}


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjNFEDIGITADA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.DATAEMISSAO);
     Except
           Mensagem:=mensagem+'/DATAEMISSAO';
     End;
     try
        Strtodate(Self.DATASAIDA);
     Except
           Mensagem:=mensagem+'/DATASAIDA';
     End;
     try
        Strtotime(Self.HORASAIDA);
     Except
           Mensagem:=mensagem+'/HORASAIDA';
     End;

     try
        Strtotime(Self.HORAEMISSAO);
     Except
           Mensagem:=mensagem+'/HORAEMISSAO';
     End;
 


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjNFEDIGITADA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjNFEDIGITADA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro NFEDIGITADA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,CLIENTE,FORNECEDOR,NOTA,TRANSPORTADORA,VALORDESCONTO');
           SQL.ADD(' ,VALORNF,VALORPIS,VALORCOFINS,VALORBASECALCULO,VALORICMS,VALORPRODUTO');
           SQL.ADD(' ,VALORFRETE,VALORSEGURO,VALOROUTROS,valoripi,valoricms_st,DATAEMISSAO,DATASAIDA,HORASAIDA,HORAEMISSAO');
           SQL.ADD(' ,INFORMACOES,DataC,UserC,DataM,UserM,operacao,baixaestoque,ADDFATURAS,indpag,IMPOSTOAUTOMATICO');
           SQL.ADD(' from  TABNFEDIGITADA');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjNFEDIGITADA.Cancelar;
begin
     Self.status:=dsInactive;
end;


function TObjNFEDIGITADA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
var
ptemp:string;
Begin
     result:=Self.exclui(Pcodigo,comcommit,ptemp);
End;

function TObjNFEDIGITADA.Exclui(Pcodigo: string;ComCommit:Boolean;var Perro:String): Boolean;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End
        Else Begin
                  result:=false;
                  Perro:='C�digo '+Pcodigo+' n�o localizado';
        End;
     Except
           on e:exception do
           Begin
                 perro:=E.message;
                 result:=false;
           End;
     End;
end;


constructor TObjNFEDIGITADA.create;

begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.CLIENTE:=TOBJCLIENTE.create;
        Self.FORNECEDOR:=TOBJFORNECEDOR.create;
        Self.NOTA:=TOBJNOTAFISCAL.create;
        Self.TRANSPORTADORA:=TOBJTRANSPORTADORA .create;


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABNFEDIGITADA(CODIGO,CLIENTE,FORNECEDOR');
                InsertSQL.add(' ,NOTA,TRANSPORTADORA,VALORDESCONTO,VALORNF,VALORPIS');
                InsertSQL.add(' ,VALORCOFINS,VALORBASECALCULO,VALORICMS,VALORPRODUTO');
                InsertSQL.add(' ,VALORFRETE,VALORSEGURO,VALOROUTROS,DATAEMISSAO,DATASAIDA');
                InsertSQL.add(' ,HORASAIDA,HORAEMISSAO,INFORMACOES,operacao,baixaestoque,ADDFATURAS,indpag,valoripi,valoricms_st,IMPOSTOAUTOMATICO)');
                InsertSQL.add('values (:CODIGO,:CLIENTE,:FORNECEDOR,:NOTA,:TRANSPORTADORA');
                InsertSQL.add(' ,:VALORDESCONTO,:VALORNF,:VALORPIS,:VALORCOFINS,:VALORBASECALCULO');
                InsertSQL.add(' ,:VALORICMS,:VALORPRODUTO,:VALORFRETE,:VALORSEGURO');
                InsertSQL.add(' ,:VALOROUTROS,:DATAEMISSAO,:DATASAIDA,:HORASAIDA,:HORAEMISSAO,:INFORMACOES');
                InsertSQL.add(' ,:operacao,:baixaestoque,:ADDFATURAS,:indpag,:valoripi,:valoricms_st,:IMPOSTOAUTOMATICO)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABNFEDIGITADA set CODIGO=:CODIGO,CLIENTE=:CLIENTE');
                ModifySQL.add(',FORNECEDOR=:FORNECEDOR,NOTA=:NOTA,TRANSPORTADORA=:TRANSPORTADORA');
                ModifySQL.add(',VALORDESCONTO=:VALORDESCONTO,VALORNF=:VALORNF,VALORPIS=:VALORPIS');
                ModifySQL.add(',VALORCOFINS=:VALORCOFINS,VALORBASECALCULO=:VALORBASECALCULO');
                ModifySQL.add(',VALORICMS=:VALORICMS,VALORPRODUTO=:VALORPRODUTO,VALORFRETE=:VALORFRETE');
                ModifySQL.add(',VALORSEGURO=:VALORSEGURO,VALOROUTROS=:VALOROUTROS,DATAEMISSAO=:DATAEMISSAO');
                ModifySQL.add(',DATASAIDA=:DATASAIDA,HORASAIDA=:HORASAIDA,HORAEMISSAO=:HORAEMISSAO,INFORMACOES=:INFORMACOES');
                ModifySQL.add(',operacao=:operacao,baixaestoque=:baixaestoque,ADDFATURAS=:ADDFATURAS,indpag=:indpag');
                ModifySQL.add(',valoripi=:valoripi,valoricms_st=:valoricms_st,IMPOSTOAUTOMATICO=:IMPOSTOAUTOMATICO ');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABNFEDIGITADA where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjNFEDIGITADA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjNFEDIGITADA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select *');
     Self.ParametroPesquisa.add('from VIEWNFEDIGITADA');

     Result:=Self.ParametroPesquisa;
end;

function TObjNFEDIGITADA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de NFEDIGITADA ';
end;


function TObjNFEDIGITADA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENNFEDIGITADA,1) CODIGO FROM RDB$DATABASE');

           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjNFEDIGITADA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.CLIENTE.FREE;
    Self.FORNECEDOR.FREE;
    Self.NOTA.FREE;
    Self.TRANSPORTADORA.FREE;

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjNFEDIGITADA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjNFEDIGITADA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjNFEDIGITADA.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjNFEDIGITADA.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjNFEDIGITADA.Submit_VALORDESCONTO(parametro: string);
begin
        Self.VALORDESCONTO:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORDESCONTO: string;
begin
        Result:=Self.VALORDESCONTO;
end;
procedure TObjNFEDIGITADA.Submit_VALORNF(parametro: string);
begin
        Self.VALORNF:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORNF: string;
begin
        Result:=Self.VALORNF;
end;
procedure TObjNFEDIGITADA.Submit_VALORPIS(parametro: string);
begin
        Self.VALORPIS:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORPIS: string;
begin
        Result:=Self.VALORPIS;
end;
procedure TObjNFEDIGITADA.Submit_VALORCOFINS(parametro: string);
begin
        Self.VALORCOFINS:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORCOFINS: string;
begin
        Result:=Self.VALORCOFINS;
end;
procedure TObjNFEDIGITADA.Submit_VALORBASECALCULO(parametro: string);
begin
        Self.VALORBASECALCULO:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORBASECALCULO: string;
begin
        Result:=Self.VALORBASECALCULO;
end;
procedure TObjNFEDIGITADA.Submit_VALORICMS(parametro: string);
begin
        Self.VALORICMS:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORICMS: string;
begin
        Result:=Self.VALORICMS;
end;
procedure TObjNFEDIGITADA.Submit_VALORPRODUTO(parametro: string);
begin
        Self.VALORPRODUTO:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORPRODUTO: string;
begin
        Result:=Self.VALORPRODUTO;
end;
procedure TObjNFEDIGITADA.Submit_VALORFRETE(parametro: string);
begin
        Self.VALORFRETE:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORFRETE: string;
begin
        Result:=Self.VALORFRETE;
end;
procedure TObjNFEDIGITADA.Submit_VALORSEGURO(parametro: string);
begin
        Self.VALORSEGURO:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALORSEGURO: string;
begin
        Result:=Self.VALORSEGURO;
end;
procedure TObjNFEDIGITADA.Submit_VALOROUTROS(parametro: string);
begin
        Self.VALOROUTROS:=Parametro;
end;
function TObjNFEDIGITADA.Get_VALOROUTROS: string;
begin
        Result:=Self.VALOROUTROS;
end;
procedure TObjNFEDIGITADA.Submit_DATAEMISSAO(parametro: string);
begin
        Self.DATAEMISSAO:=Parametro;
end;
function TObjNFEDIGITADA.Get_DATAEMISSAO: string;
begin
        Result:=Self.DATAEMISSAO;
end;
procedure TObjNFEDIGITADA.Submit_DATASAIDA(parametro: string);
begin
        Self.DATASAIDA:=Parametro;
end;
function TObjNFEDIGITADA.Get_DATASAIDA: string;
begin
        Result:=Self.DATASAIDA;
end;
procedure TObjNFEDIGITADA.Submit_HORASAIDA(parametro: string);
begin
        Self.HORASAIDA:=Parametro;
end;
function TObjNFEDIGITADA.Get_HORASAIDA: string;
begin
        Result:=Self.HORASAIDA;
end;
procedure TObjNFEDIGITADA.Submit_INFORMACOES(parametro: string);
begin
        Self.INFORMACOES:=Parametro;
end;
function TObjNFEDIGITADA.Get_INFORMACOES: string;
begin
        Result:=Self.INFORMACOES;
end;
{procedure TObjNFEDIGITADA.Submit_DataC(parametro: string);
begin
        Self.DataC:=Parametro;
end;
function TObjNFEDIGITADA.Get_DataC: string;
begin
        Result:=Self.DataC;
end;
procedure TObjNFEDIGITADA.Submit_UserC(parametro: string);
begin
        Self.UserC:=Parametro;
end;
function TObjNFEDIGITADA.Get_UserC: string;
begin
        Result:=Self.UserC;
end;
procedure TObjNFEDIGITADA.Submit_DataM(parametro: string);
begin
        Self.DataM:=Parametro;
end;
function TObjNFEDIGITADA.Get_DataM: string;
begin
        Result:=Self.DataM;
end;
procedure TObjNFEDIGITADA.Submit_UserM(parametro: string);
begin
        Self.UserM:=Parametro;
end;
function TObjNFEDIGITADA.Get_UserM: string;
begin
        Result:=Self.UserM;
end;  }
//CODIFICA GETSESUBMITS


procedure TObjNFEDIGITADA.EdtCLIENTEExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.CLIENTE.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.CLIENTE.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.CLIENTE.GET_NOME;
End;
procedure TObjNFEDIGITADA.EdtCLIENTEKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCLIENTE        :TFCLIENTE;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCLIENTE        :=TFCLIENTE        .create(nil);

            If (FpesquisaLocal.PreparaPesquisa('select * from tabcliente where ativo=''S'' ',Self.CLIENTE.Get_TituloPesquisa,FCLIENTE)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CLIENTE.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.CLIENTE.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.CLIENTE.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCLIENTE        );
     End;
end;

procedure TObjNFEDIGITADA.EdtFORNECEDORExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FORNECEDOR.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.FORNECEDOR.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.FORNECEDOR.Get_Fantasia;
End;

procedure TObjNFEDIGITADA.EdtFORNECEDORKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFORNECEDOR     :TFFORNECEDOR;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFORNECEDOR     :=TFFORNECEDOR     .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.FORNECEDOR.Get_Pesquisa,Self.FORNECEDOR.Get_TituloPesquisa,FFORNECEDOR)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FORNECEDOR.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.FORNECEDOR.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FORNECEDOR.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFORNECEDOR     );
     End;
end;
procedure TObjNFEDIGITADA.EdtNOTAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.NOTA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.NOTA.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.NOTA.GET_NOME;
End;
procedure TObjNFEDIGITADA.EdtNOTAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FNOTAFISCAL     :TFNOTAFISCAL;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FNOTAFISCAL     :=TFNOTAFISCAL     .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.NOTA.Get_Pesquisa,Self.NOTA.Get_TituloPesquisa,FNOTAFISCAL)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NOTA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.NOTA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.NOTA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FNOTAFISCAL     );
     End;
end;
procedure TObjNFEDIGITADA.EdtTRANSPORTADORAExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.TRANSPORTADORA.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.TRANSPORTADORA.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.TRANSPORTADORA.GET_NOME;
End;
procedure TObjNFEDIGITADA.EdtTRANSPORTADORAKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FTRANSPORTADORA :TFTRANSPORTADORA;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FTRANSPORTADORA :=TFTRANSPORTADORA .create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.TRANSPORTADORA.Get_Pesquisa,Self.TRANSPORTADORA.Get_TituloPesquisa,FTRANSPORTADORA)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TRANSPORTADORA.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.TRANSPORTADORA.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.TRANSPORTADORA.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FTRANSPORTADORA );
     End;
end;
//CODIFICA EXITONKEYDOWN




procedure TObjNFEDIGITADA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJNFEDIGITADA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:begin
            End;
          End;
     end;

end;



procedure TObjNFEDIGITADA.updateDesconto(valor, pCodigo: string);
begin

  valor:=tira_ponto (valor);
  valor:=troca(valor,',','.');

  with self.ObjQuery do
  begin

    Close;
    SQL.Clear;
    SQL.Add('update TABNFEDIGITADA set VALORDESCONTO = '+valor+' where CODIGO = '+pCodigo);

    try
      ExecSQL;
    except
      MensagemAviso ('N�o foi possivel gravar valor desconto')
    end;

  end;

end;


procedure TObjNFEDIGITADA.updateValorFrete(valor, pCodigo: string);
begin

  valor:=tira_ponto (valor);
  valor:=troca(valor,',','.');

  with self.ObjQuery do
  begin

    Close;
    SQL.Clear;
    SQL.Add('update TABNFEDIGITADA set VALORFRETE = '+valor+' where CODIGO = '+pCodigo);

    try
      ExecSQL;
    except
      MensagemAviso ('N�o foi possivel gravar valor do frete')
    end;

  end;

end;

procedure TObjNFEDIGITADA.updateValorICMSST(valor, pCodigo: string);
begin
  valor:=tira_ponto (valor);
  valor:=troca(valor,',','.');

  with self.ObjQuery do
  begin

    Close;
    SQL.Clear;
    SQL.Add('update TABNFEDIGITADA set VALORICMS_ST = '+valor+' where CODIGO = '+pCodigo);

    try
      ExecSQL;
    except
      MensagemAviso ('N�o foi possivel gravar valor do ICMSST')
    end;

  end;

end;

procedure TObjNFEDIGITADA.updateValorIPI(valor, pCodigo: string);
begin
  valor:=tira_ponto (valor);
  valor:=troca(valor,',','.');

  with self.ObjQuery do
  begin

    Close;
    SQL.Clear;
    SQL.Add('update TABNFEDIGITADA set VALORIPI = '+valor+' where CODIGO = '+pCodigo);

    try
      ExecSQL;
    except
      MensagemAviso ('N�o foi possivel gravar valor do IPI')
    end;

  end;
end;

procedure TObjNFEDIGITADA.updateValorNF(valor, pCodigo: string);
begin

  valor:=tira_ponto (valor);
  valor:=troca(valor,',','.');

  with self.ObjQuery do
  begin

    Close;
    SQL.Clear;
    SQL.Add('update TABNFEDIGITADA set VALORNF = '+valor+' where CODIGO = '+pCodigo);

    try
      ExecSQL;
    except
      MensagemAviso ('N�o foi possivel gravar valorNF')
    end;

  end;

end;

procedure TObjNFEDIGITADA.updateValorProdutos(valor, pCodigo: string);
begin

  valor:=tira_ponto (valor);
  valor:=troca(valor,',','.');

  with self.ObjQuery do
  begin

    Close;
    SQL.Clear;
    SQL.Add('update TABNFEDIGITADA set VALORPRODUTO = '+valor+' where CODIGO = '+pCodigo);

    try
      ExecSQL;
    except
      MensagemAviso ('N�o foi possivel gravar valor dos produtos')
    end;

  end;

end;

procedure TObjNFEDIGITADA.Submit_Operacao(parametro:string);
begin
  Self.operacao:=parametro;
end;

function TObjNFEDIGITADA.Get_Operacao:string;
begin
  Result:=operacao;
end;

procedure TObjNFEDIGITADA.Submit_BaixaEstoque(parametro:string);
begin
  Self.baixaestoque:=parametro;
end;

function TObjNFEDIGITADA.Get_BaixaEstoque:string;
begin
  Result:=baixaestoque;
end;

function TObjNFEDIGITADA.BaixarEstoque(PnfeDigitada,Pnota:String):Boolean;
var
  Qry_BuscaMateriais_QUERY:TIBQuery;
  QryPesquisaItens_QUERYaux:TIBQuery;
  PestoqueAtual,PnovoEstoque:Currency;
  Ferragemcor:tobjferragemcor;
  PerfiladoCor:tobjperfiladocor;
  VidroCor:tobjvidrocor;
  diversocor:tobjdiversocor;
  kitboxcor:tobjkitboxcor;
  persianagrupodiametrocor:tobjpersianagrupodiametrocor;
  PControlaNegativo:Boolean;
begin

  Result:=False;
  if (ObjParametroGlobal.ValidaParametro('TRAVAR PARA ESTOQUE N�O FICAR NEGATIVO?')=False)
  then exit;
  if (ObjParametroGlobal.Get_Valor='SIM')
  Then PControlaNegativo:=True;
  
  try
    Qry_BuscaMateriais_QUERY:=TIBQuery.Create(nil);
    Qry_BuscaMateriais_QUERY.Database:=FDataModulo.IBDatabase;
    QryPesquisaItens_QUERYaux:=TIBQuery.Create(nil);
    QryPesquisaItens_QUERYaux.Database:=FDataModulo.IBDatabase;

    Ferragemcor:=tobjferragemcor.Create;
    PerfiladoCor:=tobjperfiladocor.Create;
    VidroCor:=tobjvidrocor.Create;
    diversocor:=tobjdiversocor.Create;
    kitboxcor:=tobjkitboxcor.Create;
    persianagrupodiametrocor:=tobjpersianagrupodiametrocor.Create;
  except
    Exit;
  end;

  try
    Qry_BuscaMateriais_QUERY.Close;
    Qry_BuscaMateriais_QUERY.SQL.Clear;
    Qry_BuscaMateriais_QUERY.SQL.Text:=
    'SELECT * FROM '+
    'TABPRODNFEDIGITADA '+
    'WHERE NFEDIGITADA=:PNfeDigitada ';
    //'AND NOTAFISCAL=:pNota';
    Qry_BuscaMateriais_QUERY.Params[0].AsString:=PNfeDigitada;
    //Qry_BuscaMateriais_QUERY.Params[1].AsString:=Pnota;
    Qry_BuscaMateriais_QUERY.Open;
    while not Qry_BuscaMateriais_QUERY.Eof do
    begin

       if(Qry_BuscaMateriais_QUERY.FieldByName('material').AsString='1')then
       begin

          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabferragemcor where cor='+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' '+
          'and ferragem='+Qry_BuscaMateriais_QUERY.fieldbyname('ferragem').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if not (FerragemCor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fieldS[0].asstring)) then
          begin
            MensagemErro('Ferragem: '+Qry_BuscaMateriais_QUERY.fieldbyname('ferragem').AsString + ' Cor: '+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' n�o foi localizada');
            Exit;
          end;

          FerragemCor.TabelaparaObjeto;
          OBJESTOQUEGLOBAL.ZerarTabela;
          OBJESTOQUEGLOBAL.Submit_CODIGO('0');
          OBJESTOQUEGLOBAL.Status:=dsinsert;
          OBJESTOQUEGLOBAL.Submit_QUANTIDADE(CurrToStr(Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').AsCurrency*(-1)));
          OBJESTOQUEGLOBAL.Submit_DATA(DateToStr(Now));
          OBJESTOQUEGLOBAL.Submit_FERRAGEMCOR(QryPesquisaItens_QUERYaux.fields[0].asstring);
          OBJESTOQUEGLOBAL.Submit_FERRAGEM_PP('');
          OBJESTOQUEGLOBAL.Submit_OBSERVACAO('NFE DIGITADA');
          OBJESTOQUEGLOBAL.Submit_NotaFiscal(Pnota);
          //Preciso ver se essa ferragem tem um estoque valido
          try
            PestoqueAtual:=strtofloat(ferragemCor.RetornaEstoque);
          Except
            Messagedlg('Erro no Estoque Atual da ferragem '+FerragemCor.Ferragem.Get_Descricao+' da cor '+FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Calculo quanto ser� o novo toque
          Try
           PnovoEstoque:=PestoqueAtual-Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').asfloat;
          Except
            Messagedlg('Erro na gera��o do novo Estoque da ferragem '+FerragemCor.Ferragem.Get_Descricao+' da cor '+FerragemCor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
          //ent�o avisa e n�o deixa gerar nota fiscal
          if (Pnovoestoque<0)
          then Begin
            if (PControlaNegativo=True)
            Then begin
              Messagedlg('A ferragem '+FerragemCor.Ferragem.Get_Descricao+' da cor '+FerragemCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
              exit;
            End;
          End;
          if (OBJESTOQUEGLOBAL.Salvar(False)=False)
          then exit;
       end
       else
       //Perfilados
       if(Qry_BuscaMateriais_QUERY.FieldByName('material').AsString='2')then
       begin
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabPerfiladocor where cor='+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' '+
          'and Perfilado='+Qry_BuscaMateriais_QUERY.fieldbyname('Perfilado').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if not (PerfiladoCor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fields[0].asstring)) then
          begin
            MensagemErro('Perfilado: '+Qry_BuscaMateriais_QUERY.fieldbyname('Perfilado').AsString + ' Cor: '+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' n�o foi localizada');
            Exit;
          end;

          PerfiladoCor.TabelaparaObjeto;
          OBJESTOQUEGLOBAL.ZerarTabela;
          OBJESTOQUEGLOBAL.Submit_CODIGO('0');
          OBJESTOQUEGLOBAL.Status:=dsinsert;
          OBJESTOQUEGLOBAL.Submit_QUANTIDADE(CurrToStr(Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').AsCurrency*(-1)));
          OBJESTOQUEGLOBAL.Submit_DATA(DateToStr(Now));
          OBJESTOQUEGLOBAL.Submit_PERFILADOCOR(QryPesquisaItens_QUERYaux.fields[0].asstring);
          OBJESTOQUEGLOBAL.Submit_OBSERVACAO('NFE DIGITADA');
          OBJESTOQUEGLOBAL.Submit_NotaFiscal(Pnota);
          try
            PestoqueAtual:=0;
            PestoqueAtual:=strtofloat(perfiladoCor.RetornaEstoque);
          Except
            Messagedlg('Erro no Estoque Atual do perfilado '+perfiladoCor.perfilado.Get_Descricao+' da cor '+perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Calculo quanto ser� o novo estoque
          Try
           PnovoEstoque:=PestoqueAtual-Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').asfloat;
          Except
            Messagedlg('Erro na gera��o do novo Estoque do perfilado '+perfiladoCor.perfilado.Get_Descricao+' da cor '+perfiladoCor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Caso o estoque fique negativo e se n�o pode vender com estoque negativo,
          //ent�o avisa e n�o deixa gerar nota fiscal
          if (Pnovoestoque<0)
          then Begin
            if (PControlaNegativo=True)
            Then begin
              Messagedlg('O perfilado '+perfiladoCor.perfilado.Get_Descricao+' da cor '+perfiladoCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
              exit;
            End;
          End;
          if (OBJESTOQUEGLOBAL.Salvar(False)=False)
          then exit;
       end
       else
       //vidro
       if(Qry_BuscaMateriais_QUERY.FieldByName('material').AsString='3')then
       begin
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabvidrocor where cor='+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' '+
          'and vidro='+Qry_BuscaMateriais_QUERY.fieldbyname('vidro').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if not (VidroCor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fieldS[0].asstring)) then
          begin
            MensagemErro('Vidro: '+Qry_BuscaMateriais_QUERY.fieldbyname('vidro').AsString + ' Cor: '+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' n�o foi localizada');
            Exit;
          end;

          VidroCor.TabelaparaObjeto;
          OBJESTOQUEGLOBAL.ZerarTabela;
          OBJESTOQUEGLOBAL.Submit_CODIGO('0');
          OBJESTOQUEGLOBAL.Status:=dsinsert;
          OBJESTOQUEGLOBAL.Submit_QUANTIDADE(CurrToStr(Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').AsCurrency*(-1)));
          OBJESTOQUEGLOBAL.Submit_DATA(DateToStr(Now));
          OBJESTOQUEGLOBAL.Submit_VIDROCOR(QryPesquisaItens_QUERYaux.fields[0].asstring);
          OBJESTOQUEGLOBAL.Submit_OBSERVACAO('NFE DIGITADA');
          OBJESTOQUEGLOBAL.Submit_NotaFiscal(Pnota);
          try
            PestoqueAtual:=0;
            PestoqueAtual:=strtofloat(VidroCor.RetornaEstoque);
          Except
            Messagedlg('Erro no Estoque Atual do vidro '+vidroCor.vidro.Get_Descricao+' da cor '+vidroCor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Calculo quanto ser� o novo estoque
          Try
           PnovoEstoque:=PestoqueAtual-Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').asfloat;
          Except
            Messagedlg('Erro na gera��o do novo Estoque do vidro '+vidroCor.vidro.Get_Descricao+' da cor '+vidrocor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;

          if (Pnovoestoque<0)
          then Begin
            if (PControlaNegativo=True)
            Then begin
              Messagedlg('A vidro '+vidroCor.vidro.Get_Descricao+' da cor '+vidroCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
              exit;
            End;
          End;
          if (OBJESTOQUEGLOBAL.Salvar(False)=False)
          then exit;
       end
       else
       //kitbox
       if(Qry_BuscaMateriais_QUERY.FieldByName('material').AsString='4')then
       begin
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabkitboxcor where cor='+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' '+
          'and kitbox='+Qry_BuscaMateriais_QUERY.fieldbyname('kitbox').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if not (kitboxcor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fieldS[0].asstring)) then
          begin
            MensagemErro('KitBox: '+Qry_BuscaMateriais_QUERY.fieldbyname('kitbox').AsString + ' Cor: '+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' n�o foi localizada');
            Exit;
          end;

          kitboxcor.TabelaparaObjeto;
          OBJESTOQUEGLOBAL.ZerarTabela;
          OBJESTOQUEGLOBAL.Submit_CODIGO('0');
          OBJESTOQUEGLOBAL.Status:=dsinsert;
          OBJESTOQUEGLOBAL.Submit_QUANTIDADE(CurrToStr(Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').AsCurrency*(-1)));
          OBJESTOQUEGLOBAL.Submit_DATA(DateToStr(Now));
          OBJESTOQUEGLOBAL.Submit_KITBOXCOR(QryPesquisaItens_QUERYaux.fields[0].asstring);
          OBJESTOQUEGLOBAL.Submit_OBSERVACAO('NFE DIGITADA');
          OBJESTOQUEGLOBAL.Submit_NotaFiscal(Pnota);
          try
            PestoqueAtual:=0;
            PestoqueAtual:=strtofloat(kitboxCor.RetornaEstoque);
          Except
            Messagedlg('Erro no Estoque Atual do vidro '+kitboxCor.kitbox.Get_Descricao+' da cor '+kitboxCor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Calculo quanto ser� o novo estoque
          Try
           PnovoEstoque:=PestoqueAtual-Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').asfloat;
          Except
            Messagedlg('Erro na gera��o do novo Estoque do vidro '+kitboxCor.kitbox.Get_Descricao+' da cor '+kitboxcor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;

          if (Pnovoestoque<0)
          then Begin
            if (PControlaNegativo=True)
            Then begin
              Messagedlg('A vidro '+kitboxCor.kitbox.Get_Descricao+' da cor '+kitboxCor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
              exit;
            End;
          End;
          if (OBJESTOQUEGLOBAL.Salvar(False)=False)
          then exit;
       end
       else
       //persiana
       if(Qry_BuscaMateriais_QUERY.FieldByName('material').AsString='5')then
       begin
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabpersianagrupodiametrocor where cor='+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' '+
          'and persiana='+Qry_BuscaMateriais_QUERY.fieldbyname('persiana').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if not (persianagrupodiametrocor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fieldS[0].asstring)) then
          begin
            MensagemErro('Persiana: '+Qry_BuscaMateriais_QUERY.fieldbyname('persiana').AsString + ' Cor: '+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' n�o foi localizada');
            Exit;
          end;

          persianagrupodiametrocor.TabelaparaObjeto;
          OBJESTOQUEGLOBAL.ZerarTabela;
          OBJESTOQUEGLOBAL.Submit_CODIGO('0');
          OBJESTOQUEGLOBAL.Status:=dsinsert;
          OBJESTOQUEGLOBAL.Submit_QUANTIDADE(CurrToStr(Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').AsCurrency*(-1)));
          OBJESTOQUEGLOBAL.Submit_DATA(DateToStr(Now));
          OBJESTOQUEGLOBAL.Submit_PERSIANAGRUPODIAMETROCOR(QryPesquisaItens_QUERYaux.fields[0].asstring);
          OBJESTOQUEGLOBAL.Submit_OBSERVACAO('NFE DIGITADA');
          OBJESTOQUEGLOBAL.Submit_NotaFiscal(Pnota);
          try
            PestoqueAtual:=0;
            PestoqueAtual:=strtofloat(persianagrupodiametrocor.RetornaEstoque);
          Except
            Messagedlg('Erro no Estoque Atual do vidro '+persianagrupodiametrocor.persiana.Get_Nome+' da cor '+persianagrupodiametrocor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Calculo quanto ser� o novo estoque
          Try
           PnovoEstoque:=PestoqueAtual-Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').asfloat;
          Except
            Messagedlg('Erro na gera��o do novo Estoque do vidro '+persianagrupodiametrocor.persiana.Get_Nome+' da cor '+persianagrupodiametrocor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;

          if (Pnovoestoque<0)
          then Begin
            if (PControlaNegativo=True)
            Then begin
              Messagedlg('A vidro '+persianagrupodiametrocor.Persiana.Get_Nome+' da cor '+persianagrupodiametrocor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
              exit;
            End;
          End;
          if (OBJESTOQUEGLOBAL.Salvar(False)=False)
          then exit;
       end
       else
       //diverso
       if(Qry_BuscaMateriais_QUERY.FieldByName('material').AsString='6')then
       begin
          QryPesquisaItens_QUERYaux.Close;
          QryPesquisaItens_QUERYaux.SQL.Clear;
          QryPesquisaItens_QUERYaux.SQL.Text:=
          'select codigo from tabdiversocor where cor='+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' '+
          'and diverso='+Qry_BuscaMateriais_QUERY.fieldbyname('diverso').AsString;
          QryPesquisaItens_QUERYaux.Open;

          if not (diversocor.LocalizaCodigo(QryPesquisaItens_QUERYaux.fieldS[0].asstring)) then
          begin
            MensagemErro('Diverso: '+Qry_BuscaMateriais_QUERY.fieldbyname('diverso').AsString + ' Cor: '+Qry_BuscaMateriais_QUERY.fieldbyname('cor').AsString+' n�o foi localizada');
            Exit;
          end;

          diversocor.TabelaparaObjeto;
          OBJESTOQUEGLOBAL.ZerarTabela;
          OBJESTOQUEGLOBAL.Submit_CODIGO('0');
          OBJESTOQUEGLOBAL.Status:=dsinsert;
          OBJESTOQUEGLOBAL.Submit_QUANTIDADE(CurrToStr(Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').AsCurrency*(-1)));
          OBJESTOQUEGLOBAL.Submit_DATA(DateToStr(Now));
          OBJESTOQUEGLOBAL.Submit_DIVERSOCOR(QryPesquisaItens_QUERYaux.fields[0].asstring);
          OBJESTOQUEGLOBAL.Submit_FERRAGEM_PP('');
          OBJESTOQUEGLOBAL.Submit_OBSERVACAO('NFE DIGITADA');
          OBJESTOQUEGLOBAL.Submit_NotaFiscal(Pnota);
          try
            PestoqueAtual:=0;
            PestoqueAtual:=strtofloat(diversocor.RetornaEstoque);
          Except
            Messagedlg('Erro no Estoque Atual do vidro '+diversocor.diverso.Get_Descricao+' da cor '+diversocor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;
          //Calculo quanto ser� o novo estoque
          Try
           PnovoEstoque:=PestoqueAtual-Qry_BuscaMateriais_QUERY.fieldbyname('quantidade').asfloat;
          Except
            Messagedlg('Erro na gera��o do novo Estoque do vidro '+diversocor.diverso.Get_Descricao+' da cor '+diversocor.Cor.Get_Descricao,mterror,[mbok],0);
            exit;
          End;

          if (Pnovoestoque<0)
          then Begin
            if (PControlaNegativo=True)
            Then begin
              Messagedlg('A vidro '+diversocor.diverso.Get_Descricao+' da cor '+diversocor.Cor.Get_Descricao+' n�o poder� ficar com o estoque negativo',mterror,[mbok],0);
              exit;
            End;
          End;
          if (OBJESTOQUEGLOBAL.Salvar(False)=False)
          then exit;
       end;

       Qry_BuscaMateriais_QUERY.Next;
    end;
    Result:=True;
  finally
    FreeAndNil(Qry_BuscaMateriais_QUERY);
    FreeAndNil(QryPesquisaItens_QUERYaux);
    Ferragemcor.free;
    PerfiladoCor.free;
    VidroCor.free;
    diversocor.free;
    kitboxcor.free;
    persianagrupodiametrocor.free;
  end;


end;

procedure TObjNFEDIGITADA.Submit_ADDFATURAS(parametro:string);
begin
  Self.ADDFATURAS:=parametro;
end;

function TObjNFEDIGITADA.Get_AddFaturas:string;
begin
 result:=ADDFATURAS;
end;


function TObjNFEDIGITADA.get_indpag:string;
begin
    if self.indpag = '' then
    self.indpag := '2';
  Result := self.indpag;
end;

procedure TObjNFEDIGITADA.submit_indpag(parametro:string);
begin
  Self.indpag:=parametro;
end;

function TObjNFEDIGITADA.get_valoricms_st: string;
begin
  Result := self.VALORICMS_ST;
  if Trim(Result) = '' then
    Result := '0';
end;

function TObjNFEDIGITADA.get_valoripi: string;
begin
  Result := self.VALORIPI;
  if Trim(Result) = '' then
    Result := '0';
end;

procedure TObjNFEDIGITADA.submit_valoricms_st(parametro: string);
begin
  if Trim(parametro) = '' then
    parametro := '0';
  self.VALORICMS_ST := parametro;
end;

procedure TObjNFEDIGITADA.submit_valoripi(parametro: string);
begin
  if Trim(parametro) = '' then
    parametro := '0';
  self.VALORIPI := parametro;
end;



function TObjNFEDIGITADA.get_IMPOSTOAUTOMATICO: string;
begin
  result := UpperCase(self.IMPOSTOAUTOMATICO);
end;

procedure TObjNFEDIGITADA.submit_IMPOSTOAUTOMATICO(p: string);
begin
  Self.IMPOSTOAUTOMATICO := UpperCase(p);
end;

function TObjNFEDIGITADA.get_HORAEMISSAO: string;
begin
  Result := self.HORAEMISSAO;
end;

procedure TObjNFEDIGITADA.submit_HORAEMISSAO(parametro: string);
begin
  self.HORAEMISSAO := parametro;
end;

end.



