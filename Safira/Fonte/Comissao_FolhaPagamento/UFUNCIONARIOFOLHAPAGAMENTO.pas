unit UFUNCIONARIOFOLHAPAGAMENTO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjFUNCIONARIOFOLHAPAGAMENTO,
  UessencialGlobal,Uessenciallocal, Tabs;

type
  TFFUNCIONARIOFOLHAPAGAMENTO = class(TForm)
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    LbCODIGO: TLabel;
    LbFolhaPagamento: TLabel;
    LbNomeFolhaPagamento: TLabel;
    LbFuncionario: TLabel;
    LbNomeFuncionario: TLabel;
    LbTitulo: TLabel;
    LbNomeTitulo: TLabel;
    EdtCODIGO: TEdit;
    EdtFolhaPagamento: TEdit;
    EdtFuncionario: TEdit;
    EdtTitulo: TEdit;
    Bevel: TBevel;
    Label1: TLabel;
    edtsalario: TEdit;
    Label2: TLabel;
    edtcomissaovenda: TEdit;
    Label3: TLabel;
    edtcomissaocolocador: TEdit;
    Label4: TLabel;
    edttotaladiantamento: TEdit;
    Label6: TLabel;
    edtsalariofinal: TEdit;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    btNovo: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    Btopcoes: TSpeedButton;
    Label5: TLabel;
    edtacrescimosalariofamilia: TEdit;
    Label7: TLabel;
    edtacrescimohoraextra: TEdit;
    Label8: TLabel;
    edtdescontoinss: TEdit;
    edtdescontoIRPF: TEdit;
    Label9: TLabel;
    Label10: TLabel;
    edtvalorfgts: TEdit;
    EdtAcrescimoGratificacao: TEdit;
    Label11: TLabel;
    Label12: TLabel;
    edtacrescimodsr: TEdit;
    Label13: TLabel;
    edtdescontoContSindical: TEdit;
    Label14: TLabel;
    edtsalariobruto: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    edtSalarioFeriasProporcional: TEdit;
    edtFGTSFeriasProporcional: TEdit;
    edtINSSFeriasProporcional: TEdit;
    edtSalario13Proporcional: TEdit;
    edtFGTS13Proporcional: TEdit;
    edtINSS13Proporcional: TEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure FormShow(Sender: TObject);
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtFolhaPagamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtFolhaPagamentoExit(Sender: TObject);
    procedure EdtFuncionarioExit(Sender: TObject);
    procedure EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtTituloExit(Sender: TObject);
    procedure EdtTituloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFUNCIONARIOFOLHAPAGAMENTO: TFFUNCIONARIOFOLHAPAGAMENTO;
  ObjFUNCIONARIOFOLHAPAGAMENTO:TObjFUNCIONARIOFOLHAPAGAMENTO;

implementation

uses Upesquisa, UobjFOLHAPAGAMENTO;

{$R *.dfm}


procedure TFFUNCIONARIOFOLHAPAGAMENTO.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);

SetWindowRgn(Self.Handle, regiao1, False);
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;

     Try
        ObjFUNCIONARIOFOLHAPAGAMENTO:=TObjFUNCIONARIOFOLHAPAGAMENTO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.FormShow(Sender: TObject);
begin
    PegaCorForm(Self);
    Self.PosicionaPaineis;
    Self.PosicionaForm;
    Self.PegaFiguras;
    Self.ColocaAtalhoBotoes;

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjFUNCIONARIOFOLHAPAGAMENTO=Nil)
     Then exit;

     If (ObjFUNCIONARIOFOLHAPAGAMENTO.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjFUNCIONARIOFOLHAPAGAMENTO.free;
    Action := caFree;
    Self := nil;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjFUNCIONARIOFOLHAPAGAMENTO.Get_novocodigo;
     edtcodigo.enabled:=False;
     edtsalariofinal.Enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjFUNCIONARIOFOLHAPAGAMENTO.status:=dsInsert;
     Guia.TabIndex:=0;
     EdtFolhaPagamento.setfocus;

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btSalvarClick(Sender: TObject);
begin

     If ObjFUNCIONARIOFOLHAPAGAMENTO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjFUNCIONARIOFOLHAPAGAMENTO.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjFUNCIONARIOFOLHAPAGAMENTO.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btAlterarClick(Sender: TObject);
begin
    If (ObjFUNCIONARIOFOLHAPAGAMENTO.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                edtsalariofinal.Enabled:=False;
                
                ObjFUNCIONARIOFOLHAPAGAMENTO.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtFolhaPagamento.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btCancelarClick(Sender: TObject);
begin
     ObjFUNCIONARIOFOLHAPAGAMENTO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjFUNCIONARIOFOLHAPAGAMENTO.Get_pesquisa,ObjFUNCIONARIOFOLHAPAGAMENTO.Get_TituloPesquisa,Nil)
=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjFUNCIONARIOFOLHAPAGAMENTO.status<>dsinactive
                                  then exit;

                                  If (ObjFUNCIONARIOFOLHAPAGAMENTO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjFUNCIONARIOFOLHAPAGAMENTO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btExcluirClick(Sender: TObject);
begin
     If (ObjFUNCIONARIOFOLHAPAGAMENTO.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjFUNCIONARIOFOLHAPAGAMENTO.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjFUNCIONARIOFOLHAPAGAMENTO.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btRelatorioClick(Sender: TObject);
begin
    ObjFUNCIONARIOFOLHAPAGAMENTO.Imprime(edtcodigo.text);
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFFUNCIONARIOFOLHAPAGAMENTO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if key=vk_f5
    Then Begin
              if (Self.PainelPrincipal.Visible=true)
              Then Begin
                        ImageGuiaExtraClick(sender);
              End
              Else Begin
                        ImageGuiaPrincipalClick(sender);
              End;
    End;
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFFUNCIONARIOFOLHAPAGAMENTO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjFUNCIONARIOFOLHAPAGAMENTO do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        FolhaPagamento.Submit_codigo(edtFolhaPagamento.text);
        Funcionario.Submit_codigo(edtFuncionario.text);
        Titulo.Submit_codigo(edtTitulo.text);
        Submit_Salario(edtsalario.Text);
        Submit_ComissaoVenda(edtcomissaovenda.text);
        Submit_Comissaocolocador(edtcomissaocolocador.text);
        Submit_TotalAdiantamento(edttotaladiantamento.text);

        Submit_DescontoInss(edtdescontoinss.text);
        Submit_DescontoIRPF(edtdescontoIRPF.text);
        Submit_AcrescimoSalarioFamilia(edtacrescimosalariofamilia.text);
        Submit_AcrescimoHoraExtra(edtacrescimohoraextra.text);
        Submit_Acrescimogratificacao(EdtAcrescimoGratificacao.text);
        Submit_ValorFgts(edtvalorfgts.text);
        Submit_AcrescimoDSR(edtacrescimodsr.text);
        Submit_DescontoContSindical(edtdescontoContSindical.text);
        
        Submit_SalarioFeriasProporcional (edtSalarioFeriasProporcional  .text);
        Submit_Salario13Proporcional     (edtSalario13Proporcional      .text);
        Submit_FGTSFeriasProporcional    (edtFGTSFeriasProporcional     .text);
        Submit_FGTS13Proporcional        (edtFGTS13Proporcional         .text);
        Submit_INSSFeriasProporcional    (edtINSSFeriasProporcional     .text);
        Submit_INSS13Proporcional        (edtINSS13Proporcional         .text);

//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.LimpaLabels;
begin
     LbNomeFolhaPagamento.caption:='';
     LbNomeFuncionario.caption:='';
     LbNomeTitulo.caption:='';
end;

function TFFUNCIONARIOFOLHAPAGAMENTO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjFUNCIONARIOFOLHAPAGAMENTO do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtFolhaPagamento.text:=FolhaPagamento.Get_codigo;
        LbNomeFolhaPagamento.caption:=FolhaPagamento.Get_Historico;
        EdtFuncionario.text:=Funcionario.Get_codigo;
        LbNomeFuncionario.caption:=Funcionario.Get_Nome;
        EdtTitulo.text:=Titulo.Get_codigo;
        LbNomeTitulo.caption:=Titulo.Get_HISTORICO;
        edtsalario.Text:=Get_Salario;
        edtcomissaovenda.Text:=Get_ComissaoVenda;
        edtcomissaocolocador.text:=Get_Comissaocolocador;
        edttotaladiantamento.text:=Get_TotalAdiantamento;
        edtsalariofinal.Text:=Get_SalarioFinal;
        edtdescontoinss.text:=Get_DescontoInss;
        edtdescontoIRPF.text:=Get_DescontoIRPF;
        edtacrescimosalariofamilia.text:=Get_AcrescimoSalarioFamilia;
        edtacrescimohoraextra.text:=Get_AcrescimoHoraExtra;
        edtvalorfgts.text:=Get_valorFGTS;
        EdtAcrescimoGratificacao.text:=Get_AcrescimoGratificacao;
        edtacrescimodsr.text:=Get_AcrescimoDSR;
        edtdescontoContSindical.text:=Get_DescontoContSindical;
        edtSalarioBruto               .text:=Get_SalarioBruto             ;
        edtSalarioFeriasProporcional  .text:=Get_SalarioFeriasProporcional;
        edtSalario13Proporcional      .text:=Get_Salario13Proporcional    ;
        edtFGTSFeriasProporcional     .text:=Get_FGTSFeriasProporcional   ;
        edtFGTS13Proporcional         .text:=Get_FGTS13Proporcional       ;
        edtINSSFeriasProporcional     .text:=Get_INSSFeriasProporcional   ;
        edtINSS13Proporcional         .text:=Get_INSS13Proporcional       ;




//CODIFICA GETS
        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFFUNCIONARIOFOLHAPAGAMENTO.TabelaParaControles: Boolean;
begin
     If (ObjFUNCIONARIOFOLHAPAGAMENTO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.EdtFolhaPagamentoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjFUNCIONARIOFOLHAPAGAMENTO.EdtFolhaPagamentoKeyDown(sender,key,shift,LbNomeFolhaPagamento);
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.EdtFolhaPagamentoExit(
  Sender: TObject);
begin
     ObjFUNCIONARIOFOLHAPAGAMENTO.EdtFolhaPagamentoExit(sender,LbNomeFolhaPagamento);
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.EdtFuncionarioExit(Sender: TObject);
begin
     ObjFUNCIONARIOFOLHAPAGAMENTO.EdtFuncionarioExit(sender,LbNomeFuncionario);
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.EdtFuncionarioKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
ObjFUNCIONARIOFOLHAPAGAMENTO.EdtFuncionarioKeyDown(sender,key,shift,LbNomeFuncionario);
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.EdtTituloExit(Sender: TObject);
begin
     ObjFUNCIONARIOFOLHAPAGAMENTO.EdtTituloExit(sender,LbNomeTitulo);
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.EdtTituloKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
ObjFUNCIONARIOFOLHAPAGAMENTO.EdtTituloKeyDown(sender,key,shift,LbNomeTitulo);
end;

procedure TFFUNCIONARIOFOLHAPAGAMENTO.GuiaChange(Sender: TObject;
  NewTab: Integer; var AllowChange: Boolean);
begin
    Notebook.PageIndex:=NewTab;
end;

end.
