unit UObjADIANTAMENTOFUNCIONARIO;
Interface
Uses windows,stdctrls,Classes,Db,UessencialGlobal,Ibcustomdataset
,UOBJFUNCIONARIOS,uobjlancamento;

Type
   TObjADIANTAMENTOFUNCIONARIO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Funcionario:TOBJFUNCIONARIOS;
                //CodigoLancamentoPortador:Tobjlanctoportador;
                LancamentoPendencia:tobjLancamento;

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_Codigo(parametro: string);
                Function Get_Codigo: string;
                Procedure Submit_Valor(parametro: string);
                Function Get_Valor: string;
                Procedure Submit_DataSalario(parametro: string);
                Function Get_DataSalario: string;
                Procedure Submit_Data(parametro: string);
                Function Get_Data: string;

                Function get_Historico:String;
                Procedure Submit_Historico(parametro:String);

                procedure EdtFuncionarioExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                Procedure Opcoes;
                Procedure Imprime;

         Private
               ObjDataset:Tibdataset;
               Codigo:string;
               Valor:string;
               DataSalario:string;
               Data:string;
               Historico:String;


               ParametroPesquisa:TStringList;

               Function   VerificaBrancos:Boolean;
               Function   VerificaRelacionamentos:Boolean;
               Function   VerificaNumericos:Boolean;
               Function   VerificaData:Boolean;
               Function   VerificaFaixa:boolean;
               Procedure  ObjetoparaTabela;
               Procedure  Lancamentos;
               Procedure  LancamentosSintetico;
               function   VerificaPorcentagem:boolean;
               function   GerarTituloAdiantamento(pcodigo:string):boolean;

   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Ibquery,Controls
,uFUNCIONARIOS, UObjGeraTitulo, UObjTitulo,
  UReltxtRDPRINT,rdprint, UMenuRelatorios, UObjPendencia;


{ TTabTitulo }


Function  TObjADIANTAMENTOFUNCIONARIO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.Codigo:=fieldbyname('Codigo').asstring;
        If (Self.Funcionario.LocalizaCodigo(FieldByName('Funcionario').asstring)=False)
        Then Begin
                          Messagedlg('Funcion�rio N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
        End
        Else Self.Funcionario.TabelaparaObjeto;

        Self.Valor:=fieldbyname('Valor').asstring;
        Self.DataSalario:=fieldbyname('DataSalario').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.Historico:=fieldbyname('Historico').asstring;


        if (FieldByName('LancamentoPendencia').asstring<>'')
        Then Begin
                If (Self.LancamentoPendencia.LocalizaCodigo(FieldByName('LancamentoPendencia').asstring)=False)
                Then Begin
                          Messagedlg('Codigo de Lancamento na Pend�ncia N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                End
                Else Self.LancamentoPendencia.TabelaparaObjeto;
        End;

        result:=True;
     End;
end;


Procedure TObjADIANTAMENTOFUNCIONARIO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('Codigo').asstring:=Self.Codigo;
        fieldbyname('Funcionario').asstring:=Self.Funcionario.GET_CODIGO;
        fieldbyname('Valor').asstring:=Self.Valor;
        fieldbyname('DataSalario').asstring:=Self.DataSalario;
        fieldbyname('Data').asstring:=Self.Data;
        fieldbyname('LancamentoPendencia').asstring:=Self.LancamentoPendencia.get_codigo;
        fieldbyname('Historico').asstring:=Self.Historico;
  End;
End;

//***********************************************************************

function TObjADIANTAMENTOFUNCIONARIO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
var
PcodigoLancamento:string;
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then Exit;

  if (Self.VerificaNumericos=False)
  Then Exit;


  if (Self.VerificaData=False)
  Then result:=false;

  if (Self.VerificaFaixa=False)
  Then result:=false;

  if (Self.VerificaRelacionamentos=False)
  Then result:=false;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
                  End;
   End;

 if (Self.status=dsinsert)
 Then Begin
           if (self.VerificaPorcentagem=False)
           Then exit;
           Self.LocalizaCodigo(Self.CODIGO);
 End;


if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 if (Self.status=dsinsert)
 Then Begin
           if (Self.GerarTituloAdiantamento(self.Codigo)=False)
           Then Begin
                    If ComCommit=True
                    Then FDataModulo.IBTransaction.RollbackRetaining;
                    exit;
           End;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjADIANTAMENTOFUNCIONARIO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        Codigo:='';
        Funcionario.ZerarTabela;
        Valor:='';
        DataSalario:='';
        Data:='';
        LancamentoPendencia.ZerarTabela;
        Historico:='';
     End;
end;

Function TObjADIANTAMENTOFUNCIONARIO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (Codigo='')
      Then Mensagem:=mensagem+'/C�digo';

      If (Funcionario.get_codigo='')
      Then Mensagem:=mensagem+'/Funcion�rio';

      If (Valor='')
      Then Mensagem:=mensagem+'/Valor';

      If (DataSalario='')
      Then Mensagem:=mensagem+'/Data Sal�rio';

      If (Data='')
      Then Mensagem:=mensagem+'/Data';


  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjADIANTAMENTOFUNCIONARIO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';

      If (Self.Funcionario.LocalizaCodigo(Self.Funcionario.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Funcion�rio n�o Encontrado!'
      Else Self.Funcionario.TabelaparaObjeto;

      If (mensagem<>'')
      Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
      End;
     result:=true;
End;

function TObjADIANTAMENTOFUNCIONARIO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.Codigo);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Funcionario.Get_Codigo<>'')
        Then Strtoint(Self.Funcionario.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Funcion�rio';
     End;

     try
        Strtofloat(Self.Valor);
     Except
           Mensagem:=mensagem+'/Valor';
     End;
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjADIANTAMENTOFUNCIONARIO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
     try
        Strtodate(Self.DataSalario);
     Except
           Mensagem:=mensagem+'/Data Sal�rio';
     End;

     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;


     //CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjADIANTAMENTOFUNCIONARIO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';
//CODIFICA VERIFICAFAIXA

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjADIANTAMENTOFUNCIONARIO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin
           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select Codigo,Funcionario,Valor,Data,DataSalario,LancamentoPendencia,historico');
           SelectSQL.ADD(' from  TabAdiantamentoFuncionario');
           SelectSQL.ADD(' WHERE codigo='+parametro);

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjADIANTAMENTOFUNCIONARIO.Cancelar;
begin
     Self.status:=dsInactive;
     Self.ZerarTabela;
end;

function TObjADIANTAMENTOFUNCIONARIO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
var
ptitulo,PcodigoLancamento:string;
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.TabelaparaObjeto;
                 ptitulo:=Self.LancamentoPendencia.Pendencia.Titulo.Get_CODIGO;
                 PcodigoLancamento:=Self.LancamentoPendencia.get_codigo;

                 if (PcodigoLancamento<>'')
                 Then Begin
                           Self.LancamentoPendencia.ZerarTabela;
                           Self.Status:=dsedit;
                           if (Self.Salvar(False)=False)
                           Then begin
                                     Messagedlg('Erro na tentativa de retirar o lan�amento do adiantamento',mterror,[mbok],0);
                                     If (ComCommit=True)
                                     Then FDataModulo.IBTransaction.CommitRetaining;
                                     exit;
                           end;
                           
                           if (Self.LancamentoPendencia.ApagaLancamento(pcodigolancamento,False)=False)
                           Then Begin
                                     Messagedlg('Erro na tentativa de Excluir o Lan�amento',mterror,[mbok],0);
                                     If (ComCommit=True)
                                     Then FDataModulo.IBTransaction.CommitRetaining;
                                     exit;
                           End;


                           
                           if (Self.LancamentoPendencia.Pendencia.VerificaLancamentos(Ptitulo)>0)
                           then Begin
                                     MensagemErro('Existem outros lan�amentos al�m do adiantamento para o T�tulo '+Ptitulo+#13+
                                     'Para excluir esse t�tulo fa�a manualmente o extorno dos lan�amentos');
                           End
                           Else Begin
                                     if (Self.LancamentoPendencia.ExcluiTitulo(ptitulo,false)=false)
                                     then Begin
                                               Mensagemerro('Erro na tentativa de excluir o t�tulo '+ptitulo);
                                               
                                               If (ComCommit=True)
                                               Then FDataModulo.IBTransaction.RollbackRetaining;
                                     End;
                           End;
                 End
                 Else Begin
                           if (MensagemPergunta('N�o foi encontrado um lan�amento financeiro para o Adiantamento atual. Certeza que deseja excluir somente o adiantamento?')=mrno)
                           Then exit; 
                 End;


                 Self.ObjDataset.delete;

                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;

           If (ComCommit=True)
           Then FDataModulo.IBTransaction.RollbackRetaining;
     End;
end;


constructor TObjADIANTAMENTOFUNCIONARIO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Funcionario:=TOBJFUNCIONARIOS.create;
        //Self.Lanctoportador:=Tobjlanctoportador.create;
        Self.LancamentoPendencia:=tobjLancamento.create;
        Self.ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select Codigo,Funcionario,Valor,Data,DataSalario,LancamentoPendencia,historico');
                SelectSQL.ADD(' from  TabAdiantamentoFuncionario');
                SelectSQL.ADD(' WHERE codigo=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TabAdiantamentoFuncionario(Codigo,Funcionario');
                InsertSQL.add(' ,Valor,Data,DataSalario,LancamentoPendencia,historico)');
                InsertSQL.add('values (:Codigo,:Funcionario,:Valor,:Data,:DataSalario,:LancamentoPendencia,:historico)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabAdiantamentoFuncionario set Codigo=:Codigo');
                ModifySQL.add(',Funcionario=:Funcionario,Valor=:Valor,Data=:Data,DataSalario=:DataSalario,LancamentoPendencia=:LancamentoPendencia');
                ModifySQL.add(',historico=:historico');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabAdiantamentoFuncionario where codigo=:codigo ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select Codigo,Funcionario,Valor,Data,DataSalario,LancamentoPendencia,historico');
                RefreshSQL.ADD(' from  TabAdiantamentoFuncionario');
                RefreshSQL.ADD(' WHERE codigo=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjADIANTAMENTOFUNCIONARIO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjADIANTAMENTOFUNCIONARIO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabADIANTAMENTOFUNCIONARIO');
     Result:=Self.ParametroPesquisa;
end;

function TObjADIANTAMENTOFUNCIONARIO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Adiantamento de Funcion�rio ';
end;


function TObjADIANTAMENTOFUNCIONARIO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENADIANTAMENTOFUNCIONARIO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjADIANTAMENTOFUNCIONARIO.Free;
begin
    Freeandnil(Self.ObjDataset);
    Freeandnil(Self.ParametroPesquisa);
    Self.Funcionario.FREE;
    //Self.CodigoLancamentoPortador.free;
    Self.LancamentoPendencia.free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjADIANTAMENTOFUNCIONARIO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjADIANTAMENTOFUNCIONARIO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjAdiantamentoFuncionario.Submit_Codigo(parametro: string);
begin
        Self.Codigo:=Parametro;
end;
function TObjAdiantamentoFuncionario.Get_Codigo: string;
begin
        Result:=Self.Codigo;
end;
procedure TObjAdiantamentoFuncionario.Submit_Valor(parametro: string);
begin
        Self.Valor:=Parametro;
end;
function TObjAdiantamentoFuncionario.Get_Valor: string;
begin
        Result:=Self.Valor;
end;
procedure TObjAdiantamentoFuncionario.Submit_DataSalario(parametro: string);
begin
        Self.DataSalario:=Parametro;
end;
function TObjAdiantamentoFuncionario.Get_DataSalario: string;
begin
        Result:=Self.DataSalario;
end;
//CODIFICA GETSESUBMITS

procedure TObjADIANTAMENTOFUNCIONARIO.EdtFuncionarioExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Funcionario.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Funcionario.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Funcionario.GET_NOME;
End;
procedure TObjADIANTAMENTOFUNCIONARIO.EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FFUNCIONARIOS:TFFUNCIONARIOS;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FFUNCIONARIOS:=TFFUNCIONARIOS.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Funcionario.Get_Pesquisa,Self.Funcionario.Get_TituloPesquisa,FFuncionarios)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Funcionario.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Funcionario.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Funcionario.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FFUNCIONARIOS);
     End;
end;
//*******|

procedure TObjADIANTAMENTOFUNCIONARIO.EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Funcionario.Get_Pesquisa,Self.Funcionario.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Funcionario.RETORNACAMPOCODIGO).asstring;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;


function TObjADIANTAMENTOFUNCIONARIO.Get_Data: string;
begin
     Result:=Self.data;
end;

procedure TObjADIANTAMENTOFUNCIONARIO.Submit_Data(parametro: string);
begin
     Self.Data:=Parametro;
end;


procedure TObjADIANTAMENTOFUNCIONARIO.Opcoes;
begin
     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          //RgOpcoes.Items.Add('Gerar T�tulo de Sal�rio');
          showmodal;
          if tag=0
          Then exit;

          //if RgOpcoes.ItemIndex=0
          //Then Self.GerarTituloSalario;

     End;
end;


Function TObjADIANTAMENTOFUNCIONARIO.GerarTituloAdiantamento(pcodigo:string):Boolean;
var
ObjGeraTitulo:tobjgeratitulo;
PdataSalario:Tdate;
Ppendencia,PCodigoLancamento,PtituloFinanceiro,MesAnoSalario:string;
pano,pmes,pdia:word;
Phistorico:String;
pdata:string;
begin
     result:=False;
     
     if (Self.LocalizaCodigo(pcodigo)=False)
     Then Begin
               Messagedlg('Adiantamento Num '+pcodigo+' n�o encontrado',mterror,[mbok],0);
               exit;
     End;
     Self.TabelaparaObjeto;
     pdata:=Self.Get_Data;


     Try
        ObjGeraTitulo:=TObjGeraTitulo.Create;
     Except
           Messagedlg('Erro na tentativa de Gerar o Objeto Gerador de T�tulo',mterror,[mbok],0);
           exit;
     End;

Try

     if (ObjGeraTitulo.LocalizaHistorico('T�TULO DE ADIANTAMENTO DE SAL�RIO')=False)
     Then Begin
               Messagedlg('O gerador de T�tulo com hist�rico "T�TULO DE ADIANTAMENTO DE SAL�RIO" n�o foi encontrado!',mterror,[mbok],0);
               exit;
     End;
     ObjGeraTitulo.TabelaparaObjeto;

     //usado no historico que vai pra contabilidade
     //exemplo  "Adiantamento Ref. 01/2005"
     PdataSalario:=Strtodate(Self.DataSalario);
     DecodeDate(PdataSalario,pano,pmes,pdia);
     MesAnoSalario:=inttostr(pmes)+'/'+inttostr(pano);
     //****************************
     //Titulo financeiro que sera gerado, guardo o codigo antes pra por na contabilidade
     PtituloFinanceiro:=Self.LancamentoPendencia.Pendencia.Titulo.Get_NovoCodigo;
     //*********************************

     //gerando o titulo financeiro
     With Self.LancamentoPendencia.Pendencia.Titulo do
     Begin
          ZerarTabela;
          status:=dsinsert;
          Submit_CODIGO(PtituloFinanceiro);
          Submit_HISTORICO('ADIANTAMENTO SAL. '+MesAnoSalario+'-'+Self.Funcionario.Get_Nome);
          Submit_GERADOR(ObjGeraTitulo.Get_Gerador);
          Submit_CODIGOGERADOR(ObjGeraTitulo.Get_CodigoGerador);
          Submit_CREDORDEVEDOR(ObjGeraTitulo.Get_CredorDevedor);
          Submit_CODIGOCREDORDEVEDOR(Self.Funcionario.Get_CODIGO);
          Submit_EMISSAO(pdata);
          Submit_PRAZO(ObjGeraTitulo.Get_Prazo);
          Submit_PORTADOR(objGeraTitulo.Get_Portador);
          Submit_VALOR(Self.Valor);
          Submit_CONTAGERENCIAL(objGeraTitulo.Get_ContaGerencial);
          Submit_NUMDCTO('');
          Submit_NotaFiscal('');
          Submit_GeradoPeloSistema('N');
          Submit_ParcelasIguais(False);

          if (Salvar(False,False)=False)
          then Begin
                    Messagedlg('N�o foi poss�vel gerar o T�tulo a Pagar!',mterror,[mbok],0);
                    exit;
          End;
          if (Self.LancamentoPendencia.Pendencia.Localizaportitulo(PtituloFinanceiro)=False)
          Then Begin
                    Messagedlg('Pend�ncia n�o localizada',mterror,[mbok],0);
                    exit;
          end;
          Self.LancamentoPendencia.Pendencia.TabelaparaObjeto;
          Ppendencia:=Self.LancamentoPendencia.Pendencia.get_codigo;

          //Gerando a quita��o
          If (Self.LancamentoPendencia.ObjGeraLancamento.LocalizaCodigo('OBJLANCAMENTO','QUITA��O')=fALSE)
          Then Begin
                       Messagedlg('O Gerador de lan�amento com OBJGERADOR=OBJLANCAMENTO e HISTORICOGERADOR="QUITA��O", n�o foi encontrado!',mterror,[mbok],0);
                       exit;
          End;
          Self.LancamentoPendencia.ObjGeraLancamento.TabelaparaObjeto;
          //*******************************
           PCodigoLancamento:=Self.LancamentoPendencia.Get_NovoCodigo;
           Phistorico:=Self.LancamentoPendencia.Pendencia.Titulo.Get_HISTORICO;
           Self.LancamentoPendencia.ZerarTabela;
          //***********************************************************************
          //CHAMANDO A TELA DE QUITACAO
          if (Self.LancamentoPendencia.NovoLancamento(Ppendencia,'P',self.Valor,PCodigoLancamento,phistorico,False,False,True,ObjGeraTitulo.Get_CredorDevedor,Self.Funcionario.Get_CODIGO,Pdata)=False)
          Then Begin
                    FDataModulo.IBTransaction.RollbackRetaining;
                    Messagedlg('Erro na tentativa de Salvar a quita��o do adiantamento!',mterror,[mbok],0);
                    exit;
          End;
          //****************************************************************
          //Guardando o codigo do lancamento
          Self.ZerarTabela;
          Self.LocalizaCodigo(pcodigo);
          Self.TabelaparaObjeto;
          Self.LancamentoPendencia.Submit_CODIGO(PCodigoLancamento);
          Self.Status:=dsedit;
          if (Self.Salvar(False)=False)
          Then Begin
                    Messagedlg('Erro na tentativa de gravar o codigo do lan�amento',mterror,[mbok],0);
                    exit;
          End;

          Self.LancamentoPendencia.LocalizaCodigo(PCodigoLancamento);
          Self.LancamentoPendencia.TabelaparaObjeto;
          if (Self.LancamentoPendencia.ExportaContabilidade_Pagamento=False)
          then begin
                    Messagedlg('Erro na tentativa de Exportar a contabilidade do pagamento',mterror,[mbok],0);
                    exit;
          end;

          result:=true;
     End;
Finally
       ObjGeraTitulo.Free;
End;

end;


procedure TObjADIANTAMENTOFUNCIONARIO.Imprime;
begin
     With FMenuRelatorios  do
     Begin
          nomeobjeto:='UOBJADIANTAMENTOFUNCIONARIO';
          RgOpcoes.Items.clear;
          RgOpcoes.Items.Add('Lan�amentos Anal�tico');
          RgOpcoes.Items.Add('Lan�amentos Sint�tico');
          //RgOpcoes.Items.Add('Folha de Pagamento');
         
          showmodal;
          if tag=0
          Then exit;

          case RgOpcoes.ItemIndex of
            0:Self.Lancamentos;
            1:Self.LancamentosSintetico;
            //2:Self.ImprimeFolhadePagamento;
          End;

     End;
end;

procedure TObjADIANTAMENTOFUNCIONARIO.Lancamentos;
var
Pdatasalario,pdata1,pdata2:string;
linha:integer;
Soma,SomaFuncionario:currency;
Pfuncionario:string;
begin
     //filtrando por data e data de salario
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          Grupo04.Enabled:=true;

          edtGrupo01.EditMask:='!99/99/9999;';
          edtGrupo02.EditMask:='!99/99/9999;';
          edtGrupo03.EditMask:='!99/99/9999;';
          edtGrupo04.EditMask:='';
          
          LbGrupo01.caption:='Lan�amento Inicial';
          LbGrupo02.caption:='Lan�amento Inicial';
          LbGrupo03.caption:='Data Sal�rio';
          LbGrupo04.caption:='Funcion�rio';

          edtgrupo04.OnKeyDown:=Self.EdtFuncionarioKeyDown;
          
          showmodal;

          if (tag=0)
          Then exit;

          Try
             strtodate(edtgrupo01.text);
             strtodate(edtgrupo02.text);
             pdata1:=edtgrupo01.text;
             pdata2:=edtgrupo02.text;
          Except
                pdata1:='';
                pdata2:='';
          End;
          Try
             strtodate(edtgrupo03.text);
             Pdatasalario:=edtgrupo03.text;
          Except
                Pdatasalario:='';
          End;

          Try
             strtoint(edtgrupo04.text);
             Pfuncionario:=edtgrupo04.text;
          Except
                Pfuncionario:='';
          End;

     End;
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select tabadiantamentofuncionario.funcionario,');
          SelectSQL.add('tabfuncionarios.nome,');
          SelectSQL.add('tabadiantamentofuncionario.Data,');
          SelectSQL.add('tabadiantamentofuncionario.valor,');
          SelectSQL.add('tabadiantamentofuncionario.datasalario,');
          SelectSQL.add('tabfuncionarios.salario,tabadiantamentofuncionario.historico');
          SelectSQL.add('from tabadiantamentofuncionario');
          SelectSQL.add('join tabfuncionarios on tabadiantamentofuncionario.funcionario=tabfuncionarios.codigo');
          SelectSQL.add('where tabadiantamentofuncionario.codigo<>-100');//so para ter where
          
          if (pdata1<>'')
          Then Begin
                    SelectSQL.add('and (TabAdiantamentoFuncionario.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                    SelectSQL.add('and TabAdiantamentoFuncionario.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
          End;

          if (Pdatasalario<>'')
          Then SelectSQL.add('and TabAdiantamentoFuncionario.DataSalario='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatasalario))+#39);

          if (Pfuncionario<>'')
          Then SelectSQL.add('and TabAdiantamentoFuncionario.Funcionario='+Pfuncionario);

          SelectSQL.add('order by TabAdiantamentoFuncionario.funcionario,TabAdiantamentoFuncionario.Data,TabAdiantamentoFuncionario.DataSalario');

          open;

          if (recordcount=0)
          Then begin
                    Messagedlg('Nenhum Lan�amento foi encontrado nestas datas!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.RDprint.Impc(linha,45,'LAN�AMENTOS DE ADIANTAMENTO SAL�RIO',[negrito]);
          inc(linha,2);
          if (pdata1<>'')
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,01,'INTERVALO: '+pdata1+' a '+pdata2,[negrito]);
                    inc(linha,1);
          end;

          if (Pdatasalario<>'')
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,01,'DATA DO SAL�RIO: '+Pdatasalario,[negrito]);
                    inc(linha,1);
          end;

          if (Pfuncionario<>'')
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,01,'FUNCION�RIO: '+fieldbyname('nome').asstring,[negrito]);
                    inc(linha,1);
          end;


          //colunas
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('DATA',10,' ')+'  '+
                                               CompletaPalavra('DATA SALARIO',10,' ')+'  '+
                                               CompletaPalavra('FUNCION�RIO',20,' ')+'  '+
                                               CompletaPalavra_a_Esquerda('VALOR',12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda('SAL�RIO',12,' ')+' '+
                                               CompletaPalavra('HIST�RICO',20,' '),[negrito]);
          inc(linha,2);

          
          soma:=0;
          SomaFuncionario:=0;
          Pfuncionario:=fieldbyname('FUNCIONaRIO').asstring;
          While not(eof) do
          Begin
               if (Pfuncionario<>fieldbyname('FUNCIONaRIO').asstring)
               Then begin
                         FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
                         FreltxtRDPRINT.RDprint.Impf(linha,01,'SOMA DO FUNCION�RIO '+formata_valor(SomaFuncionario),[negrito]);
                         inc(linha,2);
                         SomaFuncionario:=0;
                         Pfuncionario:=fieldbyname('FUNCIONaRIO').asstring;
               end;


               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra(fieldbyname('DATA').asstring,10,' ')+'  '+
                                               CompletaPalavra(fieldbyname('DATASALARIO').asstring,10,' ')+'  '+
                                               CompletaPalavra(fieldbyname('FUNCIONaRIO').asstring+'-'+fieldbyname('nome').asstring,20,' ')+'  '+
                                               CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('salario').asstring),12,' ')+' '+
                                               CompletaPalavra(fieldbyname('historico').asstring,20,' ')
                                               );
               inc(linha,1);
               Soma:=Soma+fieldbyname('valor').asfloat;
               SomaFuncionario:=SomaFuncionario+fieldbyname('valor').asfloat;
               next;
          End;//while
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,01,'SOMA DO FUNCION�RIO '+formata_valor(SomaFuncionario),[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,01,'SOMA '+FORMATA_VALOR(FLOATTOSTR(SOMA)),[negrito]);
          FreltxtRDPRINT.RDprint.Fechar;
     End;
end;

procedure TObjADIANTAMENTOFUNCIONARIO.LancamentosSintetico;
var
Pdatasalario,pdata1,pdata2:string;
linha:integer;
Soma:currency;
Pfuncionario:string;
begin
     //filtrando por data e data de salario
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          Grupo02.Enabled:=true;
          Grupo03.Enabled:=true;
          Grupo04.Enabled:=true;

          edtGrupo01.EditMask:='!99/99/9999;';
          edtGrupo02.EditMask:='!99/99/9999;';
          edtGrupo03.EditMask:='!99/99/9999;';
          edtGrupo04.EditMask:='';
          
          LbGrupo01.caption:='Lan�amento Inicial';
          LbGrupo02.caption:='Lan�amento Inicial';
          LbGrupo03.caption:='Data Sal�rio';
          LbGrupo04.caption:='Funcion�rio';

          edtgrupo04.OnKeyDown:=Self.EdtFuncionarioKeyDown;
          
          showmodal;

          if (tag=0)
          Then exit;

          Try
             strtodate(edtgrupo01.text);
             strtodate(edtgrupo02.text);
             pdata1:=edtgrupo01.text;
             pdata2:=edtgrupo02.text;
          Except
                pdata1:='';
                pdata2:='';
          End;
          Try
             strtodate(edtgrupo03.text);
             Pdatasalario:=edtgrupo03.text;
          Except
                Pdatasalario:='';
          End;

          Try
             strtoint(edtgrupo04.text);
             Pfuncionario:=edtgrupo04.text;
          Except
                Pfuncionario:='';
          End;

     End;
     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select tabadiantamentofuncionario.funcionario,');
          SelectSQL.add('tabfuncionarios.nome,');
          SelectSQL.add('tabfuncionarios.salario,');
          SelectSQL.add('sum(tabadiantamentofuncionario.valor) as VALOR');
          SelectSQL.add('from tabadiantamentofuncionario');
          SelectSQL.add('join tabfuncionarios on tabadiantamentofuncionario.funcionario=tabfuncionarios.codigo');
          SelectSQL.add('where tabadiantamentofuncionario.codigo<>-100');//so para ter where
          if (pdata1<>'')
          Then Begin
                    SelectSQL.add('and (TabAdiantamentoFuncionario.Data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39);
                    SelectSQL.add('and TabAdiantamentoFuncionario.Data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39+')');
          End;

          if (Pdatasalario<>'')
          Then SelectSQL.add('and TabAdiantamentoFuncionario.DataSalario='+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatasalario))+#39);

          if (Pfuncionario<>'')
          Then SelectSQL.add('and TabAdiantamentoFuncionario.Funcionario='+Pfuncionario);

          SelectSQL.add('group by tabadiantamentofuncionario.funcionario,');
          SelectSQL.add('tabfuncionarios.nome,');
          SelectSQL.add('tabfuncionarios.salario');
          
          SelectSQL.add('order by tabfuncionarios.nome');

          open;

          if (recordcount=0)
          Then begin
                    Messagedlg('Nenhum Lan�amento foi encontrado nestas datas!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;
          FreltxtRDPRINT.RDprint.Impc(linha,45,'LAN�AMENTOS DE ADIANTAMENTO SAL�RIO SINT�TICO',[negrito]);
          inc(linha,2);
          if (pdata1<>'')
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,01,'INTERVALO: '+pdata1+' a '+pdata2,[negrito]);
                    inc(linha,1);
          end;

          if (Pdatasalario<>'')
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,01,'DATA DO SAL�RIO: '+Pdatasalario,[negrito]);
                    inc(linha,1);
          end;

          if (Pfuncionario<>'')
          Then Begin
                    FreltxtRDPRINT.RDprint.Impf(linha,01,'FUNCION�RIO: '+fieldbyname('nome').asstring,[negrito]);
                    inc(linha,1);
          end;


          //colunas
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('FUNCION�RIO',40,' ')+'  '+
                                               CompletaPalavra_a_Esquerda('VALOR',12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda('SAL�RIO',12,' '),[negrito]);
          inc(linha,2);

          
          soma:=0;
          While not(eof) do
          Begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra(fieldbyname('FUNCIONaRIO').asstring+'-'+fieldbyname('nome').asstring,40,' ')+'  '+
                                               CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').asstring),12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('salario').asstring),12,' '));
               inc(linha,1);
               Soma:=Soma+fieldbyname('valor').asfloat;
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,01,'SOMA '+FORMATA_VALOR(FLOATTOSTR(SOMA)),[negrito]);
          FreltxtRDPRINT.RDprint.Fechar;
     End;
end;

function TObjADIANTAMENTOFUNCIONARIO.VerificaPorcentagem: boolean;
var
PMaximo,Pusado,Psalario:Currency;
begin
     result:=False;

     if (ObjParametroGlobal.ValidaParametro('M�XIMO  EM % DE ADIANTAMENTO POR M�S')=False)
     Then Begin
               Messagedlg('O par�metro "M�XIMO  EM % DE ADIANTAMENTO POR M�S" n�o foi encontrado!',mterror,[mbok],0);
               exit;
     End;
     try
        pmaximo:=strtofloat(ObjParametroGlobal.get_valor);
     Except
           Messagedlg('Valor inv�lido no par�metro "M�XIMO  EM % DE ADIANTAMENTO POR M�S"!',mterror,[mbok],0);
           exit;
     End;

     try
        Self.Funcionario.LocalizaCodigo(Self.Funcionario.get_codigo);
        Self.Funcionario.TabelaparaObjeto;
        Psalario:=strtofloat(Self.Funcionario.Get_Salario);
     Except
           Psalario:=0;
     End;

     try
        Pmaximo:=(Psalario*PMaximo)/100;
     Except
           Pmaximo:=0;
     End;

     //somando quantos % ja foi pego

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('Select sum(valor) as SOMA from TabAdiantamentoFuncionario');
          SelectSQL.add('where funcionario='+Self.Funcionario.Get_CODIGO);
          SelectSQL.add('and datasalario='+#39+FormatDateTime('mm/dd/yyyy',strtodate(Self.DataSalario))+#39);
          open;
          if ((fieldbyname('SOMA').asfloat+strtofloat(Self.valor))>PMaximo)
          Then Begin
                    if (Self.Funcionario.Get_PermiteUltrapassarAdiantamento='N')
                    Then Begin
                              Messagedlg('Este funcion�rio n�o pode ultrapassar a cota de Adiantamento'+#13+
                               'Valor M�ximo: '+formata_valor(floattostr(pmaximo))+#13+
                               'Soma de Adiantamentos: '+formata_valor(fieldbyname('soma').asstring),mterror,[mbok],0);
                               exit;
                    End
                    Else Begin
                              Messagedlg('Aten��o este funcion�rio j� ultrapassou o Limite permitido de adiantamentos',mtInformation,[mbok],0);
                              result:=True;
                              exit;
                    End;
          End;
          result:=True;
          close;
     End;
end;

{
procedure TObjADIANTAMENTOFUNCIONARIO.ImprimeFolhadePagamento;
var
Pdatasalario:string;
linha:integer;
SomaAdiantamento,SomaSalario:currency;
begin

{vou precisar de um relatorio no modulo de adiantamentos , assim:
quero q mostre todos os funcionarios ativos , valor do salario , valor dos adiantamentos (mesmo q algum funcinario nao tenha) e saldo (VL-VA)
e soma no final de cada coluna
isso por data de salario
ok}
 {
     //filtrando por data e data de salario
     limpaedit(FfiltroImp);
     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo03.Enabled:=true;
          edtGrupo03.EditMask:='!99/99/9999;';
          LbGrupo03.caption:='Data Sal�rio';
          showmodal;

          if (tag=0)
          Then exit;

          Try
             strtodate(edtgrupo03.text);
             Pdatasalario:=edtgrupo03.text;
          Except
                Messagedlg('Digite a data do Sal�rio!',mterror,[mbok],0);
                exit;
          End;

     End;


     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select * from PROCFOLHADEPAGAMENTO ('+#39+formatdatetime('mm/dd/yyyy',strtodate(Pdatasalario))+#39+')');
          open;

          if (recordcount=0)
          Then begin
                    Messagedlg('Nenhum Lan�amento foi encontrado nestas datas!',mtinformation,[mbok],0);
                    exit;
          End;
          first;
          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;

          FreltxtRDPRINT.RDprint.Impc(linha,45,'FOLHA DE PAGAMENTO',[negrito]);
          inc(linha,2);

          FreltxtRDPRINT.RDprint.Impf(linha,1,'DATA DO SAL�RIO:'+Pdatasalario,[negrito]);
          inc(linha,2);

          //colunas
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Impf(linha,01,CompletaPalavra('FUNCION�RIO',40,' ')+'  '+
                                               CompletaPalavra_a_Esquerda('SAL�RIO',12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda('ADIANTAMENTO',12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda('SALDO',12,' '),[negrito]);

          inc(linha,2);

          
          SomaAdiantamento:=0;
          SomaSalario:=0;
          While not(eof) do
          Begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra(fieldbyname('PFUNCIONaRIO').asstring,40,' ')+'  '+
                                                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Psalario').asstring),12,' ')+'  '+
                                                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Padiantamento').asstring),12,' ')+'  '+
                                                   CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('Psaldo').asstring),12,' '));
               inc(linha,1);

               SomaAdiantamento:=SomaAdiantamento+fieldbyname('Padiantamento').asfloat;
               SomaSalario:=SomaSalario+fieldbyname('Psalario').asfloat;
               next;
          End;
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Imp(linha,01,CompletaPalavra('_',90,'_'));
          inc(linha,1);
          FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
          FreltxtRDPRINT.RDprint.Impf(linha,42,CompletaPalavra_a_Esquerda(formata_valor(floattostr(somasalario)),12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda(formata_valor(floattostr(somaadiantamento)),12,' ')+'  '+
                                               CompletaPalavra_a_Esquerda(formata_valor(floattostr(somasalario-somaadiantamento)),12,' '),[negrito]);
          FreltxtRDPRINT.RDprint.Fechar;
     End;
end;
}


function TObjADIANTAMENTOFUNCIONARIO.get_Historico: String;
begin
     result:=self.Historico;
end;

procedure TObjADIANTAMENTOFUNCIONARIO.Submit_Historico(parametro: String);
begin
     Self.Historico:=parametro;
end;


end.


