unit UobjFUNCIONARIOFOLHAPAGAMENTO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,Uessencialglobal,
uOBJFOLHAPAGAMENTO,uOBJFUNCIONARIOS,uOBJTITULO;

Type
   TObjFUNCIONARIOFOLHAPAGAMENTO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                FolhaPagamento:TOBJFOLHAPAGAMENTO;
                Funcionario:TOBJFUNCIONARIOS;
                Titulo:TOBJTITULO;
//CODIFICA VARIAVEIS PUBLICAS




                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Salario(parametro: string);
                Function Get_Salario: string;
                Procedure Submit_ComissaoVenda(parametro: string);
                Function Get_ComissaoVenda: string;
                Procedure Submit_Comissaocolocador(parametro: string);
                Function Get_Comissaocolocador: string;
                Procedure Submit_TotalAdiantamento(parametro: string);
                Function Get_TotalAdiantamento: string;
                Function Get_SalarioFinal: string;
                Function Get_DescontoInss:string;
                Function Get_DescontoIRPF:string;
                Function Get_AcrescimoSalarioFamilia:string;
                Function Get_AcrescimoHoraExtra:string;
                Function Get_AcrescimoGratificacao:string;
                Function Get_valorFGTS:string;
                Function Get_AcrescimoDSR:string;
                Function Get_DescontoContSindical:string;
                Function Get_SalarioBruto:string;//Computed by (Salario+ComissaoVenda+ComissaoColocador+AcrescimoHoraExtra+AcrescimoDSR);
                Function Get_SalarioFeriasProporcional:string;
                Function Get_Salario13Proporcional:string;
                Function Get_FGTSFeriasProporcional:string;
                Function Get_FGTS13Proporcional:string;
                Function Get_INSSFeriasProporcional:string;
                Function Get_INSS13Proporcional:string;



                Procedure Submit_DescontoInss(parametro:string);
                Procedure Submit_DescontoIRPF(parametro:string);
                Procedure Submit_AcrescimoSalarioFamilia(parametro:string);
                Procedure Submit_AcrescimoHoraExtra(parametro:string);
                Procedure Submit_Acrescimogratificacao(parametro:string);
                Procedure Submit_ValorFgts(parametro:string);
                Procedure Submit_AcrescimoDSR(parametro:string);
                Procedure Submit_DescontoContSindical(parametro:string);

                Procedure Submit_SalarioFeriasProporcional(parametro:string);
                Procedure Submit_Salario13Proporcional(parametro:string);
                Procedure Submit_FGTSFeriasProporcional(parametro:string);
                Procedure Submit_FGTS13Proporcional(parametro:string);
                Procedure Submit_INSSFeriasProporcional(parametro:string);
                Procedure Submit_INSS13Proporcional(parametro:string);




                procedure EdtFolhaPagamentoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFolhaPagamentoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtFuncionarioExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);overload;
                procedure EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;
                procedure EdtTituloExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtTituloKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Function  Cria_registros_Folha(Pfolha:string):boolean;
                Function  Gerar_titulos(Pfolha:string):boolean;
                Function  Exclui_por_Folha(pfolha:string):Boolean;
                Function  Gerar_contabilidade_Folha(Pfolha:string):Boolean;
                Function  Retornar_contabilidade_Folha(Pfolha:string):Boolean;
                //CODIFICA DECLARA GETSESUBMITS




         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;

               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Salario:string;
               ComissaoVenda:string;
               Comissaocolocador:string;
               TotalAdiantamento:string;
               DescontoInss:string;
               DescontoIRPF:string;
               AcrescimoSalarioFamilia:string;
               AcrescimoHoraExtra:string;
               Acrescimogratificacao:string;
               ValorFGTS:string;
               AcrescimoDSR:string;
               DescontoContSindical:string;

               SalarioBruto:string;//Computed by (Salario+ComissaoVenda+ComissaoColocador+AcrescimoHoraExtra+AcrescimoDSR);
               SalarioFeriasProporcional:string;
               Salario13Proporcional:string;
               FGTSFeriasProporcional:string;
               FGTS13Proporcional:string;
               INSSFeriasProporcional:string;
               INSS13Proporcional:string;




               SalarioFinal:string;
//CODIFICA VARIAVEIS PRIVADAS

               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;


   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls



//USES IMPLEMENTATION



, UMenuRelatorios, UObjGeraTitulo, UObjLancamento, UMostraBarraProgresso,
  UobjExportaContabilidade;


{ TTabTitulo }


Function  TObjFUNCIONARIOFOLHAPAGAMENTO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('FolhaPagamento').asstring<>'')
        Then Begin
                 If (Self.FolhaPagamento.LocalizaCodigo(FieldByName('FolhaPagamento').asstring)=False)
                 Then Begin
                          Messagedlg('Folha de Pagamento N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.FolhaPagamento.TabelaparaObjeto;
        End;
        If(FieldByName('Funcionario').asstring<>'')
        Then Begin
                 If (Self.Funcionario.LocalizaCodigo(FieldByName('Funcionario').asstring)=False)
                 Then Begin
                          Messagedlg('Funcion�rio N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Funcionario.TabelaparaObjeto;
        End;
        If(FieldByName('Titulo').asstring<>'')
        Then Begin
                 If (Self.Titulo.LocalizaCodigo(FieldByName('Titulo').asstring)=False)
                 Then Begin
                          Messagedlg('T�tulo N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Titulo.TabelaparaObjeto;
        End;
        Self.Salario:=fieldbyname('Salario').asstring;
        Self.ComissaoVenda:=fieldbyname('ComissaoVenda').asstring;
        Self.Comissaocolocador:=fieldbyname('Comissaocolocador').asstring;
        Self.TotalAdiantamento:=fieldbyname('TotalAdiantamento').asstring;
        Self.SalarioFinal:=fieldbyname('SalarioFinal').asstring;
        Self.DescontoInss             :=fieldbyname('DescontoInss').asstring;
        Self.DescontoIRPF             :=fieldbyname('DescontoIRPF').asstring; 
        Self.AcrescimoSalarioFamilia  :=fieldbyname('AcrescimoSalarioFamilia').asstring;
        Self.AcrescimoHoraExtra       :=fieldbyname('AcrescimoHoraExtra').asstring;
        Self.Acrescimogratificacao       :=fieldbyname('Acrescimogratificacao').asstring;
        Self.ValorFGTS:=Fieldbyname('ValorFGTS').asstring;
        Self.AcrescimoDSR:=Fieldbyname('AcrescimoDSR').asstring;
        Self.DescontoContSindical:=Fieldbyname('DescontoContSindical').asstring;
        Self.SalarioBruto               :=fieldbyname('SalarioBruto').asstring;
        Self.SalarioFeriasProporcional  :=fieldbyname('SalarioFeriasProporcional').asstring;
        Self.Salario13Proporcional      :=fieldbyname('Salario13Proporcional').asstring;
        Self.FGTSFeriasProporcional     :=fieldbyname('FGTSFeriasProporcional').asstring;
        Self.FGTS13Proporcional         :=fieldbyname('FGTS13Proporcional').asstring;
        Self.INSSFeriasProporcional     :=fieldbyname('INSSFeriasProporcional').asstring;
        Self.INSS13Proporcional         :=fieldbyname('INSS13Proporcional').asstring;

//CODIFICA TABELAPARAOBJETO

        result:=True;
     End;
end;


Procedure TObjFUNCIONARIOFOLHAPAGAMENTO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('FolhaPagamento').asstring:=Self.FolhaPagamento.GET_CODIGO;
        ParamByName('Funcionario').asstring:=Self.Funcionario.GET_CODIGO;
        ParamByName('Titulo').asstring:=Self.Titulo.GET_CODIGO;
        ParamByName('Salario').asstring:=virgulaparaponto(Self.Salario);
        ParamByName('ComissaoVenda').asstring:=virgulaparaponto(Self.ComissaoVenda);
        ParamByName('Comissaocolocador').asstring:=virgulaparaponto(Self.Comissaocolocador);
        ParamByName('TotalAdiantamento').asstring:=virgulaparaponto(Self.TotalAdiantamento);
        ParamByName('DescontoInss').asstring             :=virgulaparaponto(Self.DescontoInss)             ;
        ParamByName('DescontoIRPF').asstring             :=virgulaparaponto(Self.DescontoIRPF)             ;
        ParamByName('AcrescimoSalarioFamilia').asstring  :=virgulaparaponto(Self.AcrescimoSalarioFamilia)  ;
        ParamByName('AcrescimoHoraExtra').asstring       :=virgulaparaponto(Self.AcrescimoHoraExtra)       ;
        ParamByName('Acrescimogratificacao').asstring    :=virgulaparaponto(Self.Acrescimogratificacao)       ;
        Parambyname('AcrescimoDSR').asstring             :=Virgulaparaponto(Self.AcrescimoDSR);
        Parambyname('DescontoContSindical').asstring     :=VirgulaParaPonto(Self.DescontoContSindical);

        Parambyname('SalarioFeriasProporcional').asstring  :=VirgulaParaPonto(Self.SalarioFeriasProporcional);
        Parambyname('Salario13Proporcional').asstring      :=VirgulaParaPonto(Self.Salario13Proporcional);
        Parambyname('FGTSFeriasProporcional').asstring     :=VirgulaParaPonto(Self.FGTSFeriasProporcional);
        Parambyname('FGTS13Proporcional').asstring         :=VirgulaParaPonto(Self.FGTS13Proporcional);
        Parambyname('INSSFeriasProporcional').asstring     :=VirgulaParaPonto(Self.INSSFeriasProporcional);
        Parambyname('INSS13Proporcional').asstring         :=VirgulaParaPonto(Self.INSS13Proporcional);

        if (self.ValorFGTS='')
        Then self.ValorFGTS:='0';
        
        ParamByName('valorfgts').asstring:=virgulaparaponto(Self.ValorFGTS);
  End;
End;

//***********************************************************************

function TObjFUNCIONARIOFOLHAPAGAMENTO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        FolhaPagamento.ZerarTabela;
        Funcionario.ZerarTabela;
        Titulo.ZerarTabela;
        Salario:='';
        ComissaoVenda:='';
        Comissaocolocador:='';
        TotalAdiantamento:='';
        DescontoInss:='';
        DescontoIRPF:='';
        AcrescimoSalarioFamilia:='';
        AcrescimoHoraExtra:='';
        Acrescimogratificacao:='';
        ValorFGTS:='';
        SalarioFinal:='';
        AcrescimoDSR:='';
        DescontoContSindical:='';

        SalarioBruto                :='';
        SalarioFeriasProporcional   :='';
        Salario13Proporcional       :='';
        FGTSFeriasProporcional      :='';
        FGTS13Proporcional          :='';
        INSSFeriasProporcional      :='';
        INSS13Proporcional          :='';
     End;
end;

Function TObjFUNCIONARIOFOLHAPAGAMENTO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (FolhaPagamento.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Folha de Pagamento';
      If (Funcionario.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Funcion�rio';

      //If (Titulo.Get_CODIGO='')
      //Then Mensagem:=mensagem+'/T�tulo';

      If (Salario='')
      Then Mensagem:=mensagem+'/Sal�rio';

      If (ComissaoVenda='')
      Then Self.ComissaoVenda:='0';

      If (Comissaocolocador='')
      Then Self.Comissaocolocador:='0';

      If (TotalAdiantamento='')
      Then Self.TotalAdiantamento:='0';

      if (Self.DescontoInss='')
      Then Self.DescontoInss:='0';;

      if (Self.DescontoIRPF='')
      Then Self.DescontoIRPF:='0';

      if (Self.AcrescimoSalarioFamilia='')
      Then Self.AcrescimoSalarioFamilia:='0';
      
      if (Self.AcrescimoHoraExtra='')
      Then Self.AcrescimoHoraExtra:='0';

      if (Self.Acrescimogratificacao='')
      Then Self.Acrescimogratificacao:='0';

      if (Self.AcrescimoDSR='')
      Then Self.AcrescimoDSR:='0';

      if (Self.DescontoContSindical='')
      Then Self.DescontoContSindical:='0';

      if (Self.ValorFGTS='')
      Then Self.ValorFGTS:='0';

      if (Self.SalarioBruto='')
      Then Self.SalarioBruto:='0';

      if (Self.SalarioFeriasProporcional='')
      then Self.SalarioFeriasProporcional:='0';

      if (Self.Salario13Proporcional='')
      Then Self.Salario13Proporcional:='0';

      if (Self.FGTSFeriasProporcional='')
      Then Self.FGTSFeriasProporcional:='0';

      if (Self.FGTS13Proporcional='')
      then Self.FGTS13Proporcional:='0';

      if (self.INSSFeriasProporcional='')
      Then Self.INSSFeriasProporcional:='0';

      if (Self.INSS13Proporcional='')
      Then Self.INSS13Proporcional:='0';
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFUNCIONARIOFOLHAPAGAMENTO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.FolhaPagamento.LocalizaCodigo(Self.FolhaPagamento.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Folha de Pagamento n�o Encontrado!';

      If (Self.Funcionario.LocalizaCodigo(Self.Funcionario.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Funcion�rio n�o Encontrado!';

      if (Self.Titulo.get_codigo<>'')
      then begin
              If (Self.Titulo.LocalizaCodigo(Self.Titulo.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'/ T�tulo n�o Encontrado!';
      End;
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFUNCIONARIOFOLHAPAGAMENTO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.FolhaPagamento.Get_Codigo<>'')
        Then Strtoint(Self.FolhaPagamento.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Folha de Pagamento';
     End;
     try
        If (Self.Funcionario.Get_Codigo<>'')
        Then Strtoint(Self.Funcionario.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Funcion�rio';
     End;
     try
        If (Self.Titulo.Get_Codigo<>'')
        Then Strtoint(Self.Titulo.Get_Codigo);
     Except
           Mensagem:=mensagem+'/T�tulo';
     End;
     try
        Strtofloat(Self.Salario);
     Except
           Mensagem:=mensagem+'/Sal�rio';
     End;
     try
        Strtofloat(Self.ComissaoVenda);
     Except
           Mensagem:=mensagem+'/Comiss�o de Venda';
     End;
     try
        Strtofloat(Self.Comissaocolocador);
     Except
           Mensagem:=mensagem+'/Comiss�o de Colocador';
     End;
     try
        Strtofloat(Self.TotalAdiantamento);
     Except
           Mensagem:=mensagem+'/Total de Adiantamento';
     End;

     try
        strtofloat(Self.DescontoInss);
     Except
           Mensagem:=Mensagem+'/Desconto de Inss';
     End;

     try
          strtofloat(Self.DescontoIRPF);
     Except
           Mensagem:=Mensagem+'/Desconto de IRPF';
     end;

     try
        strtofloat(Self.AcrescimoSalarioFamilia);
     Except
           Mensagem:=Mensagem+'/Acr�scimo de Sal�rio Fam�lia';
     End;


     try
        strtofloat(Self.AcrescimoDSR);
     Except
           Mensagem:=Mensagem+'/Acr�scimo de DSR';
     End;

     try
        strtofloat(Self.DescontoContSindical);
     Except
           Mensagem:=Mensagem+'/Desconto de Contribui��o Sindical';
     End;


     Try
        strtofloat(Self.AcrescimoHoraExtra);
     Except
        Mensagem:=Mensagem+'/Acr�scimo de Hora Extra';
     End;

     Try
        strtofloat(Self.Acrescimogratificacao);
     Except
        Mensagem:=Mensagem+'/Acr�scimo de Gratifica��o';
     End;

     Try
        strtofloat(Self.ValorFGTS);
     Except
        Mensagem:=Mensagem+'/Valor do FGTS';
     End;



     Try
        strtofloat(SalarioFeriasProporcional);
     Except
        Mensagem:=Mensagem+'/Valor das F�rias Proporcional';
     End;
     Try
        strtofloat(Salario13Proporcional);
     Except
        Mensagem:=Mensagem+'/Valor do 13 Proporcional';
     End;
     Try
        strtofloat(FGTSFeriasProporcional);
     Except
        Mensagem:=Mensagem+'/Valor do FGTS das F�rias Proporcional';
     End;
     Try
        strtofloat(FGTS13Proporcional);
     Except
        Mensagem:=Mensagem+'/Valor do FGTS do 13 Proporcional';
     End;

     Try
        strtofloat(INSSFeriasProporcional);
     Except
        Mensagem:=Mensagem+'/Valor do Inss das F�rias Proporcional';
     End;
     
     Try
        strtofloat(INSS13Proporcional);
     Except
        Mensagem:=Mensagem+'/Valor do Inss do 13 Proporcional';
     End;


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;



function TObjFUNCIONARIOFOLHAPAGAMENTO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FUNCIONARIOFOLHAPAGAMENTO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,FolhaPagamento,Funcionario,Titulo,Salario,ComissaoVenda');
           SQL.ADD(' ,Comissaocolocador,TotalAdiantamento,DescontoInss,DescontoIRPF,AcrescimoSalarioFamilia,AcrescimoHoraExtra,Acrescimogratificacao,SalarioFinal,valorfgts,AcrescimoDSR,DescontoContSindical');
           SQL.ADD(',SalarioBruto,SalarioFeriasProporcional,Salario13Proporcional,FGTSFeriasProporcional,FGTS13Proporcional,INSSFeriasProporcional,INSS13Proporcional from  TabFuncionarioFolhaPagamento');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFUNCIONARIOFOLHAPAGAMENTO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;

        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.FolhaPagamento:=TOBJFOLHAPAGAMENTO.create;
        Self.Funcionario:=TOBJFUNCIONARIOS.create;
        Self.Titulo:=TOBJTITULO.create;
//CODIFICA CRIACAO DE OBJETOS




        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabFuncionarioFolhaPagamento(CODIGO,FolhaPagamento');
                InsertSQL.add(' ,Funcionario,Titulo,Salario,ComissaoVenda,Comissaocolocador');
                InsertSQL.add(' ,TotalAdiantamento,DescontoInss,DescontoIRPF,AcrescimoSalarioFamilia,AcrescimoHoraExtra,Acrescimogratificacao,valorfgts,AcrescimoDSR,DescontoContSindical');
                InsertSQL.add(',SalarioFeriasProporcional,Salario13Proporcional,FGTSFeriasProporcional,FGTS13Proporcional,INSSFeriasProporcional,INSS13Proporcional) values (:CODIGO,:FolhaPagamento,:Funcionario,:Titulo');
                InsertSQL.add(' ,:Salario,:ComissaoVenda,:Comissaocolocador,:TotalAdiantamento,:DescontoInss,:DescontoIRPF,:AcrescimoSalarioFamilia,:AcrescimoHoraExtra,:Acrescimogratificacao,:valorfgts,:AcrescimoDSR,:DescontoContSindical');
                InsertSQL.add(',:SalarioFeriasProporcional,:Salario13Proporcional,:FGTSFeriasProporcional,:FGTS13Proporcional,:INSSFeriasProporcional,:INSS13Proporcional)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabFuncionarioFolhaPagamento set CODIGO=:CODIGO');
                ModifySQL.add(',FolhaPagamento=:FolhaPagamento,Funcionario=:Funcionario');
                ModifySQL.add(',Titulo=:Titulo,Salario=:Salario,ComissaoVenda=:ComissaoVenda');
                ModifySQL.add(',Comissaocolocador=:Comissaocolocador,TotalAdiantamento=:TotalAdiantamento');
                ModifySQL.add(',DescontoInss=:DescontoInss,DescontoIRPF=:DescontoIRPF,AcrescimoSalarioFamilia=:AcrescimoSalarioFamilia,');
                ModifySQL.add('AcrescimoHoraExtra=:AcrescimoHoraExtra,Acrescimogratificacao=:Acrescimogratificacao,valorfgts=:valorfgts,AcrescimoDSR=:AcrescimoDSR,DescontoContSindical=:DescontoContSindical');
                ModifySQl.add(',SalarioFeriasProporcional=:SalarioFeriasProporcional,');
                ModifySQl.add('Salario13Proporcional=:Salario13Proporcional,FGTSFeriasProporcional=:FGTSFeriasProporcional,FGTS13Proporcional=:FGTS13Proporcional,INSSFeriasProporcional=:INSSFeriasProporcional,INSS13Proporcional=:INSS13Proporcional');
                ModifySQl.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabFuncionarioFolhaPagamento where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFUNCIONARIOFOLHAPAGAMENTO');
     Result:=Self.ParametroPesquisa;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Funcion�rios por Folha de Pagamento ';
end;


function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENFUNCIONARIOFOLHAPAGAMENTO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFUNCIONARIOFOLHAPAGAMENTO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.FolhaPagamento.FREE;
    Self.Funcionario.FREE;
    Self.Titulo.FREE;
    //CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFUNCIONARIOFOLHAPAGAMENTO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFUNCIONARIOFOLHAPAGAMENTO.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFuncionarioFolhaPagamento.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjFuncionarioFolhaPagamento.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjFuncionarioFolhaPagamento.Submit_Salario(parametro: string);
begin
        Self.Salario:=Parametro;
end;
function TObjFuncionarioFolhaPagamento.Get_Salario: string;
begin
        Result:=Self.Salario;
end;
procedure TObjFuncionarioFolhaPagamento.Submit_ComissaoVenda(parametro: string);
begin
        Self.ComissaoVenda:=Parametro;
end;
function TObjFuncionarioFolhaPagamento.Get_ComissaoVenda: string;
begin
        Result:=Self.ComissaoVenda;
end;
procedure TObjFuncionarioFolhaPagamento.Submit_Comissaocolocador(parametro: string);
begin
        Self.Comissaocolocador:=Parametro;
end;
function TObjFuncionarioFolhaPagamento.Get_Comissaocolocador: string;
begin
        Result:=Self.Comissaocolocador;
end;
procedure TObjFuncionarioFolhaPagamento.Submit_TotalAdiantamento(parametro: string);
begin
        Self.TotalAdiantamento:=Parametro;
end;
function TObjFuncionarioFolhaPagamento.Get_TotalAdiantamento: string;
begin
        Result:=Self.TotalAdiantamento;
end;
function TObjFuncionarioFolhaPagamento.Get_SalarioFinal: string;
begin
        Result:=Self.SalarioFinal;
end;
//CODIFICA GETSESUBMITS


procedure TObjFUNCIONARIOFOLHAPAGAMENTO.EdtFolhaPagamentoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FolhaPagamento.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.FolhaPagamento.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.FolhaPagamento.Get_Historico;
End;
procedure TObjFUNCIONARIOFOLHAPAGAMENTO.EdtFolhaPagamentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.FolhaPagamento.Get_Pesquisa,Self.FolhaPagamento.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FolhaPagamento.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.FolhaPagamento.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FolhaPagamento.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjFUNCIONARIOFOLHAPAGAMENTO.EdtFuncionarioExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Funcionario.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Funcionario.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Funcionario.GET_NOME;
End;


procedure TObjFUNCIONARIOFOLHAPAGAMENTO.EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Self.edtfuncionariokeydown(sender,key,shift,nil);
End;



procedure TObjFUNCIONARIOFOLHAPAGAMENTO.EdtFuncionarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Funcionario.Get_Pesquisa,Self.Funcionario.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Funcionario.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Funcionario.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Funcionario.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjFUNCIONARIOFOLHAPAGAMENTO.EdtTituloExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Titulo.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Titulo.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Titulo.GET_historico;
End;
procedure TObjFUNCIONARIOFOLHAPAGAMENTO.EdtTituloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Titulo.Get_Pesquisa,Self.Titulo.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Titulo.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Titulo.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Titulo.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN



procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFUNCIONARIOFOLHAPAGAMENTO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:begin
          End;

          End;
     end;

end;
function TObjFUNCIONARIOFOLHAPAGAMENTO.Cria_registros_Folha(
  Pfolha: string): boolean;
begin
     Result:=False;
     //esse procedimento faz parte de uma serie de procedimentos na geracao da folha
     //Esse � o primeiro deles, seleciona-se todos os funcionarios que estaum ativos
     //e cria um registro para cada um na FuncFolha
     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo,salario from tabFuncionarios where Ativo=''S''  ');
          open;
          last;
          FMostraBarraProgresso.BarradeProgresso.MaxValue:=RecordCount;
          FMostraBarraProgresso.BarradeProgresso.progress:=0;
          FMostraBarraProgresso.Show;
          if recordcount=0
          Then Begin
                    Messagedlg('Nenhum Funcion�rio foi encontrado cadastrado',mtinformation,[mbok],0);
                    exit;
          End;
          first;



          While not(eof) do
          Begin
               FMostraBarraProgresso.BarradeProgresso.progress:=FMostraBarraProgresso.BarradeProgresso.progress+1;
               FMostraBarraProgresso.Repaint;


               Self.ZerarTabela;
               Self.Status:=dsinsert;
               Self.Submit_CODIGO('0');
               Self.Submit_Salario(fieldbyname('salario').asstring);
               Self.Submit_ComissaoVenda('0');
               Self.Submit_Comissaocolocador('0');
               Self.Submit_TotalAdiantamento('0');
               Self.FolhaPagamento.Submit_CODIGO(Pfolha);
               Self.Funcionario.Submit_CODIGO(fieldbyname('codigo').AsString);
               Self.Submit_AcrescimoSalarioFamilia('0');
               Self.Submit_AcrescimoDSR('0');
               Self.Submit_DescontoContSindical('0');
               Self.Submit_SalarioFeriasProporcional('0');
               Self.Submit_Salario13Proporcional('0');
               Self.Submit_FGTSFeriasProporcional('0');
               Self.Submit_FGTS13Proporcional('0');
               Self.Submit_INSSFeriasProporcional('0');
               Self.Submit_INSS13Proporcional('0');

               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('N�o foi poss�vel salvar o funcion�rio na Folha de Pagamento',mterror,[mbok],0);
                         exit;
               End;
               next;
          End;
     End;
     Result:=true;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Gerar_titulos(
  Pfolha: string): boolean;
var
ObjGeraTitulo:tobjgeraTitulo;
ppercentualinssempregador,pfgts,pinss,psalariobruto:Currency;

begin
     Result:=False;

     try
        ObjGeraTitulo:=tobjgeraTitulo.create;
     Except
           Messagedlg('Erro na Tentativa de criar o ObjgeraTitulo',mterror,[mbok],0);
           exit; 
     End;

try     

     //localizando o gerador de titulo
     if (ObjGeraTitulo.LocalizaHistorico('T�TULO DE SALARIOS A PAGAR')=False)
     Then Begin
               Messagedlg('O gerador de T�tulo com hist�rico "T�TULO DE SALARIOS A PAGAR" n�o foi encontrado!',mterror,[mbok],0);
               exit;
     End;
     ObjGeraTitulo.TabelaparaObjeto;


     With Self.ObjqueryPesquisa do
     Begin
          //selecionando todos os funcionarios dessa folha
          //eles ja estaum com o valor final a ser pago de salario
          //ver depois com a Camila  a contabilidade desses titulos 
          close;
          sql.clear;
          sql.add('Select codigo from TabFuncionarioFolhaPagamento where FolhaPagamento='+Pfolha);
          open;
          last;
          FMostraBarraProgresso.Lbmensagem.Caption:='GERANDO T�TULOS NO FINANCEIRO';
          FMostraBarraProgresso. BarradeProgresso.MaxValue:=RecordCount;
          FMostraBarraProgresso. BarradeProgresso.Progress:=0;
          first;

          if (recordcount=0)
          Then begin
                    Messagedlg('N�o foi encontrado nenhum funcion�rio nessa folha',mterror,[mbok],0);
                    exit;
          end;

          While not(eof) do
          Begin
               FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso. BarradeProgresso.Progress+1;
               FMostraBarraProgresso.show;
               FMostraBarraProgresso.repaint;

               
               if (Self.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
               Then Begin
                         Messagedlg('Func. da Folha de C�digo '+fieldbyname('codigo').asstring+' n�o foi encontrado',mterror,[mbok],0);
                         exit;
               end;
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               //*****************************
               Self.Titulo.ZerarTabela;
               Self.Titulo.status:=dsinsert;
               Self.Titulo.Submit_CODIGO(Self.Titulo.Get_NovoCodigo);
               Self.Titulo.Submit_HISTORICO('PAGAMENTO DE SAL�RIOS');
               Self.Titulo.Submit_GERADOR(ObjGeraTitulo.Get_Gerador);
               Self.Titulo.Submit_CODIGOGERADOR(ObjGeraTitulo.Get_CodigoGerador);
               Self.Titulo.Submit_CREDORDEVEDOR(ObjGeraTitulo.Get_CredorDevedor);
               Self.Titulo.Submit_CODIGOCREDORDEVEDOR(Self.Funcionario.Get_CODIGO);
               Self.Titulo.Submit_EMISSAO(Self.FolhaPagamento.Get_Data);
               Self.Titulo.Submit_PRAZO(ObjGeraTitulo.Get_Prazo);
               Self.Titulo.Submit_PORTADOR(objGeraTitulo.Get_Portador);
               Self.Titulo.Submit_VALOR(Self.SalarioFinal);
               Self.Titulo.Submit_CONTAGERENCIAL(objGeraTitulo.Get_ContaGerencial);
               Self.Titulo.Submit_NUMDCTO('');
               Self.Titulo.Submit_NotaFiscal('');
               Self.Titulo.Submit_GeradoPeloSistema('N');
               Self.Titulo.Submit_ParcelasIguais(True);
               Self.Titulo.Submit_ExportaLanctoContaGer('S');

               if (Self.Titulo.Salvar(False,False)=False)
               then Begin
                         Messagedlg('N�o foi poss�vel gerar o T�tulo a Pagar do funcion�rio '+Self.Funcionario.get_codigo,mterror,[mbok],0);
                         exit;
               End;
               //****************************
               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('N�o foi poss�vel guardar o c�digo do t�tulo no Funcion�rio '+Self.Funcionario.get_codigo,mterror,[mbok],0);
                         exit;
               End;
               next;
          End;//while dos funcfolha
         {
         Criar 3 campos para T�tulos Financeiro no cadastro de Folha de Pagamento

         Ao Gerar a Folha gerar os seguintes t�tulos
         
         FGTS
         	  Somar o FGTS de todos os funcion�rios e Criar um t�tulo (exporta a contabilidade)
         INSS DESCONTADO DOS FUNCION�RIOS
         	  Somar o INSS dos Funcion�rios e gerar um t�tulo (n�o exportar a contabilidade, apenas a quita��o)
         INSS DA PARTE DO EMPREGADOR
         	   Somar todos os sal�rios brutos * 28,8%
         	   Gerar Um t�tulo com esse valor. Exportar a Contabilidade
         }


          close;
          sql.clear;
          sql.add('Select Sum(valorfgts) as FGTS,sum(descontoinss) as INSS,sum(salariobruto) as SALARIOBRUTO from TabFuncionarioFolhaPagamento where FolhaPagamento='+Pfolha);
          open;

          Pfgts:=Fieldbyname('fgts').asfloat;
          Pinss:=Fieldbyname('inss').asfloat;
          Psalariobruto:=Fieldbyname('salariobruto').asfloat;

          if (self.FolhaPagamento.LocalizaCodigo(Pfolha)=False)
          then Begin
                    mensagemerro('Folha n�o encontrada');
                    exit;
          End;
          Self.FolhaPagamento.TabelaparaObjeto;


          //localizando o gerador de titulo
          if (ObjGeraTitulo.LocalizaHistorico('T�TULO DE FGTS')=False)
          Then Begin
                    Messagedlg('O gerador de T�tulo com hist�rico "T�TULO DE FGTS" n�o foi encontrado!',mterror,[mbok],0);
                    exit;
          End;
          ObjGeraTitulo.TabelaparaObjeto;

          Self.FolhaPagamento.TituloTemp.ZerarTabela;



          Self.FolhaPagamento.TituloTemp.status:=dsinsert;
          Self.FolhaPagamento.Submit_TITULOFGTS(Self.FolhaPagamento.TituloTemp.Get_NovoCodigo);
          Self.FolhaPagamento.TituloTemp.Submit_codigo(Self.FolhaPagamento.get_titulofgts);
          Self.FolhaPagamento.TituloTemp.Submit_HISTORICO('FGTS FOLHA DE PAGAMENTO '+Pfolha);
          Self.FolhaPagamento.TituloTemp.Submit_GERADOR(ObjGeraTitulo.Get_Gerador);
          Self.FolhaPagamento.TituloTemp.Submit_CODIGOGERADOR(ObjGeraTitulo.Get_CodigoGerador);
          Self.FolhaPagamento.TituloTemp.Submit_CREDORDEVEDOR(ObjGeraTitulo.Get_CredorDevedor);
          Self.FolhaPagamento.TituloTemp.Submit_CODIGOCREDORDEVEDOR(Self.Funcionario.Get_CODIGO);
          Self.FolhaPagamento.TituloTemp.Submit_EMISSAO(Self.FolhaPagamento.Get_Data);
          Self.FolhaPagamento.TituloTemp.Submit_PRAZO(ObjGeraTitulo.Get_Prazo);
          Self.FolhaPagamento.TituloTemp.Submit_PORTADOR(objGeraTitulo.Get_Portador);
          Self.FolhaPagamento.TituloTemp.Submit_VALOR(FloatToStr(pfgts));
          Self.FolhaPagamento.TituloTemp.Submit_CONTAGERENCIAL(objGeraTitulo.Get_ContaGerencial);
          Self.FolhaPagamento.TituloTemp.Submit_NUMDCTO('');
          Self.FolhaPagamento.TituloTemp.Submit_NotaFiscal('');
          Self.FolhaPagamento.TituloTemp.Submit_GeradoPeloSistema('N');
          Self.FolhaPagamento.TituloTemp.Submit_ParcelasIguais(True);
          if (Self.FolhaPagamento.TituloTemp.Salvar(False,True)=False)
          then Begin
                    Messagedlg('N�o foi poss�vel gerar o T�tulo a Pagar do funcion�rio '+Self.Funcionario.get_codigo,mterror,[mbok],0);
                    exit;
          End;
          
          if (ObjGeraTitulo.LocalizaHistorico('T�TULO DE INSS DOS FUNCION�RIOS')=False)
          Then Begin
                  Messagedlg('O gerador de T�tulo com hist�rico "T�TULO DE INSS DOS FUNCION�RIOS" n�o foi encontrado!',mterror,[mbok],0);
                  exit;
          End;
          ObjGeraTitulo.TabelaparaObjeto;

          Self.FolhaPagamento.TituloTemp.ZerarTabela;

          Self.FolhaPagamento.TituloTemp.status:=dsinsert;
          Self.FolhaPagamento.Submit_TITULOINSS_FUNCIONARIOS(Self.FolhaPagamento.TituloTemp.Get_NovoCodigo);
          Self.FolhaPagamento.TituloTemp.Submit_codigo(Self.FolhaPagamento.Get_TITULOINSS_FUNCIONARIOS);
          Self.FolhaPagamento.TituloTemp.Submit_HISTORICO('INSS FUNCION�RIOS FOLHA DE PAGAMENTO '+Pfolha);
          Self.FolhaPagamento.TituloTemp.Submit_GERADOR(ObjGeraTitulo.Get_Gerador);
          Self.FolhaPagamento.TituloTemp.Submit_CODIGOGERADOR(ObjGeraTitulo.Get_CodigoGerador);
          Self.FolhaPagamento.TituloTemp.Submit_CREDORDEVEDOR(ObjGeraTitulo.Get_CredorDevedor);
          Self.FolhaPagamento.TituloTemp.Submit_CODIGOCREDORDEVEDOR(Self.Funcionario.Get_CODIGO);
          Self.FolhaPagamento.TituloTemp.Submit_EMISSAO(Self.FolhaPagamento.Get_Data);
          Self.FolhaPagamento.TituloTemp.Submit_PRAZO(ObjGeraTitulo.Get_Prazo);
          Self.FolhaPagamento.TituloTemp.Submit_PORTADOR(objGeraTitulo.Get_Portador);
          Self.FolhaPagamento.TituloTemp.Submit_VALOR(FloatToStr(pinss));
          Self.FolhaPagamento.TituloTemp.Submit_CONTAGERENCIAL(objGeraTitulo.Get_ContaGerencial);
          Self.FolhaPagamento.TituloTemp.Submit_NUMDCTO('');
          Self.FolhaPagamento.TituloTemp.Submit_NotaFiscal('');
          Self.FolhaPagamento.TituloTemp.Submit_GeradoPeloSistema('N');
          Self.FolhaPagamento.TituloTemp.Submit_ParcelasIguais(True);
          if (Self.FolhaPagamento.TituloTemp.Salvar(False,False)=False)
          then Begin
                    Messagedlg('N�o foi poss�vel gerar o T�tulo a Pagar do funcion�rio '+Self.Funcionario.get_codigo,mterror,[mbok],0);
                    exit;
          End;

          if (ObjGeraTitulo.LocalizaHistorico('T�TULO DE INSS DO EMPREGADOR')=False)
          Then Begin
                  Messagedlg('O gerador de T�tulo com hist�rico "T�TULO DE INSS DO EMPREGADOR" n�o foi encontrado!',mterror,[mbok],0);
                  exit;
          End;
          ObjGeraTitulo.TabelaparaObjeto;

          if (ObjParametroGlobal.ValidaParametro('PERCENTUAL INSS EMPREGADOR')=False)
          Then exit;

          Try
             Ppercentualinssempregador:=Strtofloat(ObjParametroGlobal.Get_Valor);
          Except
             mensagemerro('Valor Inv�lido no par�metro "PERCENTUAL INSS EMPREGADOR"');
             EXIT;
          End;
          


          Pinss:=0;
          pinss:=(psalariobruto*PpercentualInssEmpregador)/100;

          Self.FolhaPagamento.TituloTemp.ZerarTabela;
          Self.FolhaPagamento.TituloTemp.status:=dsinsert;
          Self.FolhaPagamento.Submit_TITULOINSS_EMPREGADOR(Self.FolhaPagamento.TituloTemp.Get_NovoCodigo);
          Self.FolhaPagamento.TituloTemp.Submit_codigo(Self.FolhaPagamento.Get_TITULOINSS_EMPREGADOR);
          Self.FolhaPagamento.TituloTemp.Submit_HISTORICO('INSS EMPREGADOR FOLHA DE PAGAMENTO '+Pfolha);
          Self.FolhaPagamento.TituloTemp.Submit_GERADOR(ObjGeraTitulo.Get_Gerador);
          Self.FolhaPagamento.TituloTemp.Submit_CODIGOGERADOR(ObjGeraTitulo.Get_CodigoGerador);
          Self.FolhaPagamento.TituloTemp.Submit_CREDORDEVEDOR(ObjGeraTitulo.Get_CredorDevedor);
          Self.FolhaPagamento.TituloTemp.Submit_CODIGOCREDORDEVEDOR(Self.Funcionario.Get_CODIGO);
          Self.FolhaPagamento.TituloTemp.Submit_EMISSAO(Self.FolhaPagamento.Get_Data);
          Self.FolhaPagamento.TituloTemp.Submit_PRAZO(ObjGeraTitulo.Get_Prazo);
          Self.FolhaPagamento.TituloTemp.Submit_PORTADOR(objGeraTitulo.Get_Portador);
          Self.FolhaPagamento.TituloTemp.Submit_VALOR(FloatToStr(pinss));
          Self.FolhaPagamento.TituloTemp.Submit_CONTAGERENCIAL(objGeraTitulo.Get_ContaGerencial);
          Self.FolhaPagamento.TituloTemp.Submit_NUMDCTO('');
          Self.FolhaPagamento.TituloTemp.Submit_NotaFiscal('');
          Self.FolhaPagamento.TituloTemp.Submit_GeradoPeloSistema('N');
          Self.FolhaPagamento.TituloTemp.Submit_ParcelasIguais(True);
          if (Self.FolhaPagamento.TituloTemp.Salvar(False,True)=False)
          then Begin
                    Messagedlg('N�o foi poss�vel gerar o T�tulo a Pagar do funcion�rio '+Self.Funcionario.get_codigo,mterror,[mbok],0);
                    exit;
          End;
     end;

     Self.FolhaPagamento.Status:=dsedit;
     if (self.FolhaPagamento.Salvar(False)=False)
     then Begin
               mensagemerro('Erro na tentativa de Salvar os t�tulos de FGTS e INSS na folha de pagamento');
               exit;
     End;

     result:=True;
finally
       ObjGeraTitulo.Free;
end;

End;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Exclui_por_Folha(
  pfolha: string): Boolean;
var
ptitulo:string;
PtituloFGTS:string;
PtituloInssFunc:string;
PtituloInssempregador:string;
TempObjlancamento:TobjLancamento;
begin
     Result:=False;

     try
        TempObjlancamento:=TobjLancamento.create;
     Except
           Messagedlg('Erro na tentativa de Criar o ObjLancamento',mterror,[mbok],0);
           exit;
     End;

Try
     //primeiro seleciono todos os func da folha
     //tiro o titulo, salvo e  excluo o titulo
     With Self.ObjqueryPesquisa do
     Begin

          close;
          sql.clear;
          sql.add('Select codigo from tabfuncionariofolhapagamento where FolhaPagamento='+PFolha);
          open;
          last;
          FMostraBarraProgresso.BarradeProgresso.MaxValue:=recordcount;
          FMostraBarraProgresso.BarradeProgresso.progress:=0;
          first;


          While not (eof) do
          Begin
               FMostraBarraProgresso.BarradeProgresso.progress:=FMostraBarraProgresso.BarradeProgresso.progress+1;
               FMostraBarraProgresso.Lbmensagem.caption:='EXCLUINDO T�TULOS A PAGAR';
               FMostraBarraProgresso.show;
               FMostraBarraProgresso.repaint;

               Self.LocalizaCodigo(fieldbyname('codigo').asstring);
               Self.TabelaparaObjeto;
               Self.Status:=dsedit;
               Ptitulo:=Self.Titulo.get_codigo;
               Self.Titulo.ZerarTabela;

               if (Self.Salvar(False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Gravar sem o t�tulo o Funcionario na Folha',mterror,[mbok],0);
                         exit;
               End;

               if (Ptitulo<>'')
               Then Begin
                       if (TempObjlancamento.ExcluiTitulo(ptitulo,False)=False)
                       Then Begin
                                 Messagedlg('Erro na tentativa de Excluir o T�tulo '+Ptitulo,mterror,[mbok],0);
                                 exit;
                       End;
               End;

               if (Self.Exclui(Fieldbyname('codigo').asstring,False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o Func. da Folha de Pagamento',mterror,[mbok],0);
                         exit;
               End;

               next;//proximo funcionario da folha
          End;//while
     End;//with

     //Excluindo os T�tulos de INSS e FGTS
     if (Self.FolhaPagamento.LocalizaCodigo(pfolha)=False)
     Then Begin
               mensagemerro('Folha n�o encontrada');
               exit;
     End;
     Self.FolhaPagamento.TabelaparaObjeto;

     PtituloFGTS:=Self.FolhaPagamento.Get_TITULOFGTS;
     PtituloInssFunc:=Self.FolhaPagamento.get_tituloinss_funcionarios;
     PtituloInssempregador:=Self.FolhaPagamento.Get_tituloinss_empregador;

     Self.FolhaPagamento.Submit_titulofgts('');
     Self.FolhaPagamento.Submit_TITULOINSS_FUNCIONARIOS('');
     Self.FolhaPagamento.Submit_TITULOINSS_EMPREGADOR('');
     Self.FolhaPagamento.Status:=dsedit;

     if (Self.FolhaPagamento.Salvar(False)=False)
     then Begin
               mensagemerro('Erro na tentativa de limpar os campos de t�tulos de fgts e inss da folha');
               exit;
     End;

     if (PtituloFGTS<>'')
     Then Begin
               if (TempObjlancamento.ExcluiTitulo(PtituloFGTS,False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o T�tulo de FGTS N�mero '+PtituloFgts,mterror,[mbok],0);
                         exit;
               End;
     End;

     if (PtituloInssFunc<>'')
     Then Begin
               if (TempObjlancamento.ExcluiTitulo(PtituloInssFunc,False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o T�tulo de INSS de Funcion�rios N�mero '+PtituloInssFunc,mterror,[mbok],0);
                         exit;
               End;
     End;

     if (PtituloInssempregador<>'')
     Then Begin     
               if (TempObjlancamento.ExcluiTitulo(PtituloInssempregador,False)=False)
               Then Begin
                         Messagedlg('Erro na tentativa de Excluir o T�tulo de INSS de Empregador N�mero '+PtituloInssempregador,mterror,[mbok],0);
                         exit;
               End;
     End;
          
     Result:=True;
Finally
       TempObjlancamento.free;
       FMostraBarraProgresso.close;
End;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_AcrescimoSalarioFamilia: string;
begin
     Result:=Self.AcrescimoSalarioFamilia;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_DescontoInss: string;
begin
     Result:=Self.DescontoInss;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_DescontoIRPF: string;
begin
     Result:=Self.DescontoIRPF;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_AcrescimoSalarioFamilia(
  parametro: string);
begin
     Self.AcrescimoSalarioFamilia:=parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_DescontoInss(
  parametro: string);
begin
     self.DescontoInss:=parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_DescontoIRPF(
  parametro: string);
begin
     Self.DescontoIRPF:=parametro;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_AcrescimoHoraExtra: string;
begin
     Result:=Self.AcrescimoHoraExtra;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_AcrescimoHoraExtra(
  parametro: string);
begin
     Self.AcrescimoHoraExtra:=parametro;
end;
//************
function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_Acrescimogratificacao: string;
begin
     Result:=Self.Acrescimogratificacao;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_Acrescimogratificacao(
  parametro: string);
begin
     Self.Acrescimogratificacao:=parametro;
end;



function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_valorFGTS: string;
begin
     Result:=Self.ValorFGTS;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_ValorFgts(parametro: string);
begin
     Self.ValorFGTS:=parametro;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_AcrescimoDSR: string;
begin
     Result:=Self.AcrescimoDSR;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_DescontoContSindical: string;
begin
     Result:=Self.DescontoContSindical;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_AcrescimoDSR(
  parametro: string);
begin
     Self.AcrescimoDSR:=Parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_DescontoContSindical(
  parametro: string);
begin
     Self.DescontoContSindical:=Parametro;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_FGTS13Proporcional: string;
begin
     Result:=Self.FGTS13Proporcional;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_FGTSFeriasProporcional: string;
begin
     Result:=Self.FGTSFeriasProporcional;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_INSS13Proporcional: string;
begin
     Result:=Self.INSS13Proporcional;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_INSSFeriasProporcional: string;
begin
     Result:=Self.INSSFeriasProporcional;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_Salario13Proporcional: string;
begin
     Result:=Self.Salario13Proporcional;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_SalarioFeriasProporcional: string;
begin
     Result:=Self.SalarioFeriasProporcional;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_FGTS13Proporcional(
  parametro: string);
begin
     Self.FGTS13Proporcional:=parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_FGTSFeriasProporcional(
  parametro: string);
begin
     Self.FGTSFeriasProporcional:=parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_INSS13Proporcional(
  parametro: string);
begin
     Self.INSS13Proporcional:=parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_INSSFeriasProporcional(
  parametro: string);
begin
     Self.INSSFeriasProporcional:=parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_Salario13Proporcional(
  parametro: string);
begin
     Self.Salario13Proporcional:=parametro;
end;

procedure TObjFUNCIONARIOFOLHAPAGAMENTO.Submit_SalarioFeriasProporcional(
  parametro: string);
begin
     Self.SalarioFeriasProporcional:=parametro;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Get_SalarioBruto: string;
begin
     Result:=Self.SalarioBruto;
end;


Function TObjFUNCIONARIOFOLHAPAGAMENTO.Gerar_contabilidade_Folha(Pfolha: string): Boolean;
var
  Psalario,PComissao,PHoraExtra,PGratificacao,PDSR,PSalarioFamilia,PINSS,PIRPF,Padiantamento,PsalarioPagar:Currency;
  PcontaSalario,PcontaSalarioPagar,PContaComissao,PContaHoraExtra,PContaGratificacao,PContaDSR,PContaSalarioFamilia,
  PContaINSS,PContaIRPF,PContaadiantamento:string;
  Temp:string;
  PdataRefSalario:string;
begin
     Result:=False;
     
     If (Self.FolhaPagamento.LocalizaCodigo(pfolha)=False)
     Then begin
               mensagemerro('Folha n�o localizada');
               exit;
     end;
     Self.FolhaPagamento.TabelaparaObjeto;
     PDatarefSalario:=formatdatetime('mm/yyyy',strtodate(Self.FolhaPagamento.Get_Data));


{
Debitar	(uma conta para cada lan�amento)
  Soma de Sal�rio?????????????
	Soma de Comiss�o
	Soma de Hora Extra
	Soma de Gratifica��o
	Soma de DSR
	Soma do Sal�rio Fam�lia
  
Creditar	(uma conta para cada lan�amento)

	Soma do INSS (dos funcion�rios)
  Soma  do IRPF
  Soma de adiantamentos
  Conta Sal�rios a Pagar a Diferen�a de (D�bitos - Cr�ditos)??????????
**Debito e Cr�dito em Contas com o valor no cadastro Funcion�rio Folha de Pagamento**
}


//Para cada lan�amento tem uma conta cont�bil diferente
//procurar nos par�metros

//*****************************************************************************
    temp:='CONTA CONT�BIL D�BITO DE SALARIO DO CADASTRO';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;

    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PcontaSalario:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL D�BITO DE COMISS�O';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaComissao:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL D�BITO DE HORA EXTRA';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaHoraExtra:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL D�BITO DE GRATIFICA��ES';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaGratificacao:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL D�BITO DE DSR';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaDSR:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL D�BITO DE SALARIO FAMILIA';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaSalarioFamilia:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL CR�DITO DE INSS';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaINSS:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL CR�DITO DE IRPF';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaIRPF:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL CR�DITO DE ADIANTAMENTO';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PContaadiantamento:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;

    temp:='CONTA CONT�BIL CR�DITO DE SALARIOS A PAGAR';
    if (ObjParametroGlobal.ValidaParametro(temp)=False)
    then exit;
    Try
       Strtoint(ObjParametroGlobal.Get_Valor);
       PcontaSalarioPagar:=ObjParametroGlobal.Get_Valor;
    Except
          mensagemerro('Valor inv�lido no par�metro "'+temp+'"');
          exit;
    End;
    //*************************************************************************
    With Self.ObjqueryPesquisa do
    Begin
          close;
          sql.clear;
          sql.add('Select Sum(Salario) as Salario,');
          sql.add('sum(ComissaoVenda+ComissaoColocador) as COMISSAO,');
          sql.add('sum(AcrescimoHoraextra) as HORAEXTRA,');
          sql.add('sum(AcrescimoGratificacao)as Gratificacao,');
          sql.add('sum(AcrescimoDSR) as DSR,');
          sql.add('sum(AcrescimoSalarioFamilia) as SALARIOFAMILIA,');
          sql.add('sum(DescontoInss) as INSS,');
          sql.add('sum(DescontoIRPF) as IRPF,');
          sql.add('sum(TotalAdiantamento) as ADIANTAMENTO');
          sql.add('from tabFuncionarioFolhaPagamento');
          sql.add('where FolhaPagamento='+Pfolha);
          open;

          //debitos
          PSalario:=Fieldbyname('salario').asfloat;
          PComissao:=fieldbyname('comissao').asfloat;
          PHoraExtra:=fieldbyname('horaextra').asfloat;
          PGratificacao:=fieldbyname('gratificacao').asfloat;
          PDSR:=fieldbyname('dsr').asfloat;
          PSalarioFamilia:=fieldbyname('SalarioFamilia').asfloat;
          //creditos
          PINSS:=fieldbyname('inss').asfloat;
          PIRPF:=fieldbyname('irpf').asfloat;
          Padiantamento:=fieldbyname('adiantamento').asfloat;
          PsalarioPagar:=Psalario-PINSS-PIRPF-Padiantamento;
     End;


     //FMostraBarraProgresso.ConfiguracoesIniciais();
     
     With Self.Titulo.ObjExportaContabilidade do
     Begin
          //SOMA DE SALARIO BRUTOS

          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaDebite(PContaSalario);
          Submit_Valor(floattostr(pSalario));
          Submit_Historico('SAL�RIO FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  do Sal�rio Bruto');
                    exit;
          End;

          //COMISSAO
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaDebite(PContaComissao);
          Submit_Valor(floattostr(pcomissao));
          Submit_Historico('COMISS�O FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de comiss�o');
                    exit;
          End;

          //HORA EXTRA
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaDebite(PContahoraExtra);
          Submit_Valor(floattostr(phoraextra));
          Submit_Historico('HORA EXTRA FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de Hora Extra');
                    exit;
          End;

          //GRATIFICACOES
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaDebite(PContaGratificacao);
          Submit_Valor(floattostr(pgratificacao));
          Submit_Historico('GRATIFICA��ES FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de gratifica��es');
                    exit;
          End;
          //DSR
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaDebite(PContaDSR);
          Submit_Valor(floattostr(pdsr));
          Submit_Historico('DSR FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de DSR');
                    exit;
          End;

          //SALARIO FAMILIA
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaDebite(PContaSalarioFamilia);
          Submit_Valor(floattostr(psalariofamilia));
          Submit_Historico('SAL�RIO FAMILIA FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de Sal�rio Familia');
                    exit;
          End;

          //*******************************************************************
          //CR�DITOS

          //INSS
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaCredite(PContaINSS);
          Submit_Valor(floattostr(pINSS));
          Submit_Historico('INSS FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de INSS');
                    exit;
          End;

          //IRPF
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaCredite(PContaIRPF);
          Submit_Valor(floattostr(pIRPF));
          Submit_Historico('IRPF FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de IRPF');
                    exit;
          End;

          //ADIANTAMENTOS
          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaCredite(PContaAdiantamento);
          Submit_Valor(floattostr(padiantamento));
          Submit_Historico('ADIANTAMENTO FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de Adiantamentos');
                    exit;
          End;

          //conta salarios a pagar

          ZerarTabela;
          Status:=dsinsert;
          Submit_CODIGO('0');
          Submit_Data(Self.FolhaPagamento.Get_Data);
          Submit_ContaCredite(PContaSalarioPagar);
          Submit_Valor(floattostr(psalariopagar));
          Submit_Historico('SAL�RIOS A PAGAR FOLHA PAGTO '+PdataRefSalario);
          Submit_ObjGerador('OBJFOLHAPAGAMENTO');
          Submit_CodGerador(pfolha);
          Submit_Exportado('N');
          Submit_exporta('S');
          if (Salvar(False,true)=False)
          then Begin
                    mensagemerro('Erro na tentativa de Salvar o Lan�amento cont�bil  de Sal�rios a Pagar');
                    exit;
          End;
     End;

     result:=True;
end;

function TObjFUNCIONARIOFOLHAPAGAMENTO.Retornar_contabilidade_Folha(
  Pfolha: string): Boolean;
begin
     Result:=False;
     With Self.ObjqueryPesquisa do
     Begin
         //verificando se a contabilidade foi exportada
         close;
         sql.clear;
         sql.add('Select count(codigo) as CONTA from tabExportaContabilidade');
         sql.add('where Objgerador='+#39+'OBJFOLHAPAGAMENTO'+#39);
         sql.add('and Codgerador='+Pfolha);
         sql.add('and Exportado=''S'' ');

         Try
            open;
            if (fieldbyname('conta').asinteger>0)
            then begin
                      if (Self.Titulo.ObjExportaContabilidade.LancaInverso('OBJFOLHAPAGAMENTO',PFolha,datetostr(now))=False)
                      then begin
                                mensagemerro('N�o foi poss�vel lan�ar o inverso na contabilidade');
                                exit;
                      End;
            End
            Else Begin
                      //apagando a contabilidade do estoque e do financeiro
                      close;
                      sql.clear;
                      sql.add('Delete from tabExportaContabilidade');
                      sql.add('where Objgerador='+#39+'OBJFOLHAPAGAMENTO'+#39);
                      sql.add('and Codgerador='+Pfolha);
                      execsql;
            End;
         except
            MensagemErro('N�o foi poss�vel excluir a contabilidade da Folha');
            exit;
         End;
    End;
    Result:=True;
end;

End.



