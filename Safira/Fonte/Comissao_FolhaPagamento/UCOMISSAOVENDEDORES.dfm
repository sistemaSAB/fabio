object FCOMISSAOVENDEDORES: TFCOMISSAOVENDEDORES
  Left = 126
  Top = 240
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'Comiss'#227'o de Vendedores'
  ClientHeight = 409
  ClientWidth = 792
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ImageGuiaPrincipal: TImage
    Left = 25
    Top = -1
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaPrincipalClick
  end
  object ImageGuiaExtra: TImage
    Left = 25
    Top = 76
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaExtraClick
  end
  object btFechar: TSpeedButton
    Left = 754
    Top = 6
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btFecharClick
  end
  object btMinimizar: TSpeedButton
    Left = 754
    Top = 32
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btMinimizarClick
  end
  object lbAjuda: TLabel
    Left = 649
    Top = 351
    Width = 50
    Height = 13
    Caption = 'Ajuda...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btAjuda: TSpeedButton
    Left = 719
    Top = 336
    Width = 28
    Height = 38
    Flat = True
  end
  object Notebook: TNotebook
    Left = 137
    Top = 1
    Width = 616
    Height = 334
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    object TPage
      Left = 0
      Top = 0
      Caption = 'Ferragem'
      object Bevel: TBevel
        Left = 0
        Top = 0
        Width = 616
        Height = 334
        Align = alClient
        Shape = bsFrame
      end
      object LbCODIGO: TLabel
        Left = 3
        Top = 23
        Width = 40
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbVENDEDOR: TLabel
        Left = 3
        Top = 71
        Width = 55
        Height = 13
        Caption = 'Vendedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeVENDEDOR: TLabel
        Left = 80
        Top = 87
        Width = 55
        Height = 13
        Caption = 'Vendedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPEDIDO: TLabel
        Left = 3
        Top = 126
        Width = 38
        Height = 13
        Caption = 'Pedido'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomePEDIDO: TLabel
        Left = 80
        Top = 143
        Width = 38
        Height = 13
        Caption = 'Pedido'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbPENDENCIA: TLabel
        Left = 3
        Top = 181
        Width = 58
        Height = 13
        Caption = 'Pend'#234'ncia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomePENDENCIA: TLabel
        Left = 80
        Top = 198
        Width = 58
        Height = 13
        Caption = 'Pend'#234'ncia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbComissao: TLabel
        Left = 3
        Top = 236
        Width = 72
        Height = 13
        Caption = '% Comiss'#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbValor: TLabel
        Left = 155
        Top = 236
        Width = 30
        Height = 13
        Caption = 'Valor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbVALORCOMISSAO: TLabel
        Left = 308
        Top = 236
        Width = 90
        Height = 13
        Caption = 'Valor Comiss'#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 251
        Top = 181
        Width = 77
        Height = 13
        Caption = 'Ch.Devolvido'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 3
        Top = 285
        Width = 68
        Height = 13
        Caption = 'Observa'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 3
        Top = 36
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtVENDEDOR: TEdit
        Left = 3
        Top = 84
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        OnExit = EdtVENDEDORExit
        OnKeyDown = edtVENDEDORKeyDown
      end
      object EdtPEDIDO: TEdit
        Left = 3
        Top = 140
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 2
        OnExit = EdtPEDIDOExit
        OnKeyDown = EdtPEDIDOKeyDown
      end
      object EdtPENDENCIA: TEdit
        Left = 3
        Top = 195
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 3
        OnExit = EdtPENDENCIAExit
        OnKeyDown = EdtPENDENCIAKeyDown
      end
      object EdtComissao: TEdit
        Left = 3
        Top = 251
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 5
      end
      object EdtValor: TEdit
        Left = 155
        Top = 251
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 6
      end
      object EdtVALORCOMISSAO: TEdit
        Left = 308
        Top = 251
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 7
      end
      object edtchequedevolvido: TEdit
        Left = 251
        Top = 195
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 4
        OnExit = edtchequedevolvidoExit
        OnKeyDown = edtchequedevolvidoKeyDown
      end
      object edtobservacao: TEdit
        Left = 3
        Top = 304
        Width = 585
        Height = 19
        TabOrder = 8
        Text = 'edtobservacao'
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Perfilado'
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Componente'
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'KitBox'
    end
  end
  object PainelExtra: TPanel
    Left = 40
    Top = 27
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 0
    object ImagePainelExtra: TImage
      Left = 0
      Top = 14
      Width = 98
      Height = 352
    end
  end
  object PainelPrincipal: TPanel
    Left = 40
    Top = -13
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 1
    object ImagePainelPrincipal: TImage
      Left = 1
      Top = 14
      Width = 98
      Height = 352
    end
    object btNovo: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btNovoClick
    end
    object btSalvar: TSpeedButton
      Left = 15
      Top = 66
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSalvarClick
    end
    object btAlterar: TSpeedButton
      Left = 15
      Top = 107
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btAlterarClick
    end
    object btCancelar: TSpeedButton
      Left = 15
      Top = 148
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btCancelarClick
    end
    object btSair: TSpeedButton
      Left = 15
      Top = 311
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSairClick
    end
    object btRelatorio: TSpeedButton
      Left = 15
      Top = 270
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btRelatorioClick
    end
    object btExcluir: TSpeedButton
      Left = 15
      Top = 229
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btExcluirClick
    end
    object btPesquisar: TSpeedButton
      Left = 15
      Top = 188
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      Layout = blGlyphRight
      OnClick = btPesquisarClick
    end
  end
  object Guia: TTabSet
    Left = 138
    Top = -1
    Width = 614
    Height = 22
    BackgroundColor = 6710886
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      'Principal')
    TabIndex = 0
    UnselectedColor = 13421772
  end
end
