object FFUNCIONARIOFOLHAPAGAMENTO: TFFUNCIONARIOFOLHAPAGAMENTO
  Left = 256
  Top = 153
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'Funcion'#225'rios por Folha de Pagamento'
  ClientHeight = 409
  ClientWidth = 792
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ImageGuiaPrincipal: TImage
    Left = 25
    Top = -1
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaPrincipalClick
  end
  object ImageGuiaExtra: TImage
    Left = 25
    Top = 76
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaExtraClick
  end
  object btFechar: TSpeedButton
    Left = 754
    Top = 6
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btFecharClick
  end
  object btMinimizar: TSpeedButton
    Left = 754
    Top = 32
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btMinimizarClick
  end
  object lbAjuda: TLabel
    Left = 649
    Top = 351
    Width = 50
    Height = 13
    Caption = 'Ajuda...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btAjuda: TSpeedButton
    Left = 719
    Top = 336
    Width = 28
    Height = 38
    Flat = True
  end
  object PainelExtra: TPanel
    Left = 41
    Top = -12
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 0
    object ImagePainelExtra: TImage
      Left = 0
      Top = 14
      Width = 98
      Height = 352
    end
    object Btopcoes: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Caption = 'Op'#231#245'es'
      Flat = True
    end
  end
  object Notebook: TNotebook
    Left = 139
    Top = 16
    Width = 616
    Height = 334
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 2
    object TPage
      Left = 0
      Top = 0
      Caption = 'Principal'
      object Bevel: TBevel
        Left = 0
        Top = 0
        Width = 616
        Height = 334
        Align = alClient
        Shape = bsFrame
      end
      object LbCODIGO: TLabel
        Left = 11
        Top = 24
        Width = 40
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFolhaPagamento: TLabel
        Left = 11
        Top = 70
        Width = 116
        Height = 13
        Caption = 'Folha de Pagamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeFolhaPagamento: TLabel
        Left = 89
        Top = 89
        Width = 116
        Height = 13
        Caption = 'Folha de Pagamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFuncionario: TLabel
        Left = 11
        Top = 116
        Width = 65
        Height = 13
        Caption = 'Funcion'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeFuncionario: TLabel
        Left = 89
        Top = 135
        Width = 65
        Height = 13
        Caption = 'Funcion'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbTitulo: TLabel
        Left = 11
        Top = 163
        Width = 31
        Height = 13
        Caption = 'T'#237'tulo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbNomeTitulo: TLabel
        Left = 89
        Top = 182
        Width = 31
        Height = 13
        Caption = 'T'#237'tulo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 11
        Top = 209
        Width = 40
        Height = 13
        Caption = 'Sal'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 90
        Top = 212
        Width = 96
        Height = 13
        Caption = 'Comiss'#227'o Venda'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 197
        Top = 212
        Width = 118
        Height = 13
        Caption = 'Comiss'#227'o Colocador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 320
        Top = 256
        Width = 111
        Height = 13
        Caption = 'Total Adiantamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 531
        Top = 296
        Width = 70
        Height = 13
        Caption = 'Sal'#225'rio Final'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label5: TLabel
        Left = 320
        Top = 212
        Width = 84
        Height = 13
        Caption = 'Sal'#225'rio Fam'#237'lia'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 436
        Top = 212
        Width = 61
        Height = 13
        Caption = 'Hora Extra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 90
        Top = 256
        Width = 86
        Height = 13
        Caption = 'Desconto INSS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 197
        Top = 256
        Width = 83
        Height = 13
        Caption = 'Desconto IRPF'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label10: TLabel
        Left = 531
        Top = 256
        Width = 82
        Height = 13
        Caption = 'Valor do FGTS'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 531
        Top = 212
        Width = 68
        Height = 13
        Caption = 'Gratifica'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 11
        Top = 256
        Width = 20
        Height = 13
        Caption = 'Dsr'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label13: TLabel
        Left = 436
        Top = 256
        Width = 79
        Height = 13
        Caption = 'Cont. Sindical'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 435
        Top = 294
        Width = 84
        Height = 13
        Caption = 'Sal'#225'rio BRUTO'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 11
        Top = 40
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 0
      end
      object EdtFolhaPagamento: TEdit
        Left = 11
        Top = 86
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        OnExit = edtFolhaPagamentoExit
        OnKeyDown = edtFolhaPagamentoKeyDown
      end
      object EdtFuncionario: TEdit
        Left = 11
        Top = 132
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 2
        OnExit = edtFuncionarioExit
        OnKeyDown = edtFuncionarioKeyDown
      end
      object EdtTitulo: TEdit
        Left = 11
        Top = 179
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 3
        OnExit = edtTituloExit
        OnKeyDown = edtTituloKeyDown
      end
      object edtsalario: TEdit
        Left = 11
        Top = 225
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 4
      end
      object edtcomissaovenda: TEdit
        Left = 90
        Top = 228
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 5
      end
      object edtcomissaocolocador: TEdit
        Left = 197
        Top = 228
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 6
      end
      object edttotaladiantamento: TEdit
        Left = 320
        Top = 272
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 13
      end
      object edtsalariofinal: TEdit
        Left = 531
        Top = 312
        Width = 72
        Height = 19
        Color = clMoneyGreen
        MaxLength = 9
        TabOrder = 17
      end
      object edtacrescimosalariofamilia: TEdit
        Left = 320
        Top = 228
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 7
      end
      object edtacrescimohoraextra: TEdit
        Left = 436
        Top = 228
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 8
      end
      object edtdescontoinss: TEdit
        Left = 90
        Top = 272
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 11
      end
      object edtdescontoIRPF: TEdit
        Left = 197
        Top = 272
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 12
      end
      object edtvalorfgts: TEdit
        Left = 531
        Top = 272
        Width = 72
        Height = 19
        Color = clCream
        MaxLength = 9
        TabOrder = 15
      end
      object EdtAcrescimoGratificacao: TEdit
        Left = 531
        Top = 228
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 9
      end
      object edtacrescimodsr: TEdit
        Left = 11
        Top = 272
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 10
      end
      object edtdescontoContSindical: TEdit
        Left = 436
        Top = 272
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 14
      end
      object edtsalariobruto: TEdit
        Left = 435
        Top = 310
        Width = 72
        Height = 19
        Ctl3D = False
        MaxLength = 9
        ParentCtl3D = False
        TabOrder = 16
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Proporcionais'
      object Label15: TLabel
        Left = 8
        Top = 32
        Width = 153
        Height = 13
        Caption = 'Sal'#225'rio F'#233'rias Proporcional'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 8
        Top = 80
        Width = 143
        Height = 13
        Caption = 'FGTS F'#233'rias Proporcional'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label17: TLabel
        Left = 8
        Top = 128
        Width = 142
        Height = 13
        Caption = 'INSS F'#233'rias Proporcional'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label18: TLabel
        Left = 8
        Top = 176
        Width = 133
        Height = 13
        Caption = 'Sal'#225'rio 13 Proporcional'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 8
        Top = 224
        Width = 123
        Height = 13
        Caption = 'FGTS 13 Proporcional'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label20: TLabel
        Left = 8
        Top = 272
        Width = 122
        Height = 13
        Caption = 'INSS 13 Proporcional'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object edtSalarioFeriasProporcional: TEdit
        Left = 8
        Top = 48
        Width = 121
        Height = 19
        TabOrder = 0
        Text = 'edtSalarioFeriasProporcional'
      end
      object edtFGTSFeriasProporcional: TEdit
        Left = 8
        Top = 96
        Width = 121
        Height = 19
        TabOrder = 1
        Text = 'edtFGTSFeriasProporcional'
      end
      object edtINSSFeriasProporcional: TEdit
        Left = 8
        Top = 144
        Width = 121
        Height = 19
        TabOrder = 2
        Text = 'edtINSSFeriasProporcional'
      end
      object edtSalario13Proporcional: TEdit
        Left = 8
        Top = 192
        Width = 121
        Height = 19
        TabOrder = 3
        Text = 'edtSalario13Proporcional'
      end
      object edtFGTS13Proporcional: TEdit
        Left = 8
        Top = 240
        Width = 121
        Height = 19
        TabOrder = 4
        Text = 'edtFGTS13Proporcional'
      end
      object edtINSS13Proporcional: TEdit
        Left = 8
        Top = 288
        Width = 121
        Height = 19
        TabOrder = 5
        Text = 'edtINSS13Proporcional'
      end
    end
  end
  object Guia: TTabSet
    Left = 138
    Top = -1
    Width = 614
    Height = 17
    BackgroundColor = 6710886
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      '&1-Principal'
      '&2-Proporcionais')
    TabIndex = 0
    UnselectedColor = 13421772
    OnChange = GuiaChange
  end
  object PainelPrincipal: TPanel
    Left = 40
    Top = -13
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 3
    object ImagePainelPrincipal: TImage
      Left = 1
      Top = 14
      Width = 98
      Height = 352
    end
    object btNovo: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btNovoClick
    end
    object btSalvar: TSpeedButton
      Left = 15
      Top = 66
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSalvarClick
    end
    object btAlterar: TSpeedButton
      Left = 15
      Top = 107
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btAlterarClick
    end
    object btCancelar: TSpeedButton
      Left = 15
      Top = 148
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btCancelarClick
    end
    object btSair: TSpeedButton
      Left = 15
      Top = 311
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSairClick
    end
    object btRelatorio: TSpeedButton
      Left = 15
      Top = 270
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btRelatorioClick
    end
    object btExcluir: TSpeedButton
      Left = 15
      Top = 229
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btExcluirClick
    end
    object btPesquisar: TSpeedButton
      Left = 15
      Top = 188
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      Layout = blGlyphRight
      OnClick = btPesquisarClick
    end
  end
end
