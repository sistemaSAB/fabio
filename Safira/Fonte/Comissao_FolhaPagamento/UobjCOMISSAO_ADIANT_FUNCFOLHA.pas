unit UobjCOMISSAO_ADIANT_FUNCFOLHA;
Interface
Uses forms,Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,
UOBJCOMISSAOVENDEDORES,UOBJCOMISSAOCOLOCADOR,UOBJADIANTAMENTOFUNCIONARIO,UOBJFUNCIONARIOFOLHAPAGAMENTO,UobjHoraExtra,rdprint,uobjgratificacao,
UobjDescansoSemanalRemunerado,UobjDescontoContribuicaoSindical;
//CODIFICA VARIAVEIS PUBLICAS

Type
   TObjCOMISSAO_ADIANT_FUNCFOLHA=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                ComissaoVendedor        :TOBJCOMISSAOVENDEDORES;
                ComissaoColocador:TOBJCOMISSAOCOLOCADOR;
                AdiantamentoFuncionario:TOBJADIANTAMENTOFUNCIONARIO;
                FuncionarioFolhaPagamento:TOBJFUNCIONARIOFOLHAPAGAMENTO;
                HoraExtra:TobjHoraExtra;
                Gratificacao:TobjGratificacao;

                AcrescimoDSR:TObjDescansoSemanalRemunerado;
                DescontoContSindical:TObjDescontoContribuicaoSindical;
                


//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create(Owner:TComponent);
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                procedure EdtComissaoVendedorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtComissaoVendedorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtComissaoColocadorExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtComissaoColocadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtAdiantamentoFuncionarioExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtAdiantamentoFuncionarioKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtHoraExtraExit(Sender: TObject; LABELNOME: TLABEL);
                procedure EdtHoraExtraKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
                procedure EdtGratificacaoExit(Sender: TObject; LABELNOME: TLABEL);
                procedure EdtGratificacaoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);


                procedure EdtFuncionarioFolhaPagamentoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFuncionarioFolhaPagamentoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                function  ExcluiComissaoColocadorporRomaneio(Promaneio: string): Boolean;
                Function  Cria_Registros_Folha(PFolha:string):boolean;
                Function  Exclui_por_folha(Pfolha:string):boolean;
                Procedure ImprimeComissaovendedor;
                Procedure RelatoriosComissao;
                Function  AcertaInss_IRPF_FGTS_SalarioFamilia(Pfolha:string):boolean;
         Private
               Objquery:Tibquery;
               ObjqueryPesquisa:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;
               Owner:TComponent;
               
               CODIGO:string;
//CODIFICA VARIAVEIS PRIVADAS



               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ImprimePrevisaoComissao;
                procedure ImprimeComissaovendedor_LoteComissao;
                procedure ImprimePrevisaoComissao_LOTECOMISSAO;





   End;


implementation
uses UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls,
  UMenuRelatorios, UobjFOLHAPAGAMENTO, UMostraBarraProgresso,
  UReltxtRDPRINT, UobjFAIXADESCONTOINSS, UobjFAIXADESCONTOIRPF,
  UobjSALARIOFAMILIA, UmostraStringGrid;


{ TTabTitulo }


Function  TObjCOMISSAO_ADIANT_FUNCFOLHA.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('ComissaoVendedor').asstring<>'')
        Then Begin
                 If (Self.ComissaoVendedor.LocalizaCodigo(FieldByName('ComissaoVendedor').asstring)=False)
                 Then Begin
                          Messagedlg('Comiss�o de Vendedor N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ComissaoVendedor.TabelaparaObjeto;
        End;
        If(FieldByName('ComissaoColocador').asstring<>'')
        Then Begin
                 If (Self.ComissaoColocador.LocalizaCodigo(FieldByName('ComissaoColocador').asstring)=False)
                 Then Begin
                          Messagedlg('Comiss�o de Colocador N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.ComissaoColocador.TabelaparaObjeto;
        End;
        If(FieldByName('AdiantamentoFuncionario').asstring<>'')
        Then Begin
                 If (Self.AdiantamentoFuncionario.LocalizaCodigo(FieldByName('AdiantamentoFuncionario').asstring)=False)
                 Then Begin
                          Messagedlg('Adiantamento de Sal�rio N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.AdiantamentoFuncionario.TabelaparaObjeto;
        End;
        If(FieldByName('FuncionarioFolhaPagamento').asstring<>'')
        Then Begin
                 If (Self.FuncionarioFolhaPagamento.LocalizaCodigo(FieldByName('FuncionarioFolhaPagamento').asstring)=False)
                 Then Begin
                          Messagedlg('Funcion�rio da Folha de Pagamento N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.FuncionarioFolhaPagamento.TabelaparaObjeto;
        End;
//CODIFICA TABELAPARAOBJETO
        If(FieldByName('HoraExtra').asstring<>'')
        Then Begin
                 If (Self.HoraExtra.LocalizaCodigo(FieldByName('HoraExtra').asstring)=False)
                 Then Begin
                          Messagedlg('Hora Extra N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.HoraExtra.TabelaparaObjeto;
        End;

        If(FieldByName('gratificacao').asstring<>'')
        Then Begin
                 If (Self.gratificacao.LocalizaCodigo(FieldByName('gratificacao').asstring)=False)
                 Then Begin
                          Messagedlg('Gratifica��o N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Gratificacao.TabelaparaObjeto;
        End;

        If(FieldByName('AcrescimoDSR').asstring<>'')
        Then Begin
                 If (Self.AcrescimoDSR.LocalizaCodigo(FieldByName('AcrescimoDSR').asstring)=False)
                 Then Begin
                          Messagedlg('Descanso Semanal remunerado N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.AcrescimoDSR.TabelaparaObjeto;
        End;

        If(FieldByName('DescontoContSindical').asstring<>'')
        Then Begin
                 If (Self.DescontoContSindical.LocalizaCodigo(FieldByName('DescontoContSindical').asstring)=False)
                 Then Begin
                          Messagedlg('Desconto Contribui��o Sindical N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.DescontoContSindical.TabelaparaObjeto;
        End;





        result:=True;
     End;
end;


Procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('ComissaoVendedor').asstring:=Self.ComissaoVendedor.GET_CODIGO;
        ParamByName('ComissaoColocador').asstring:=Self.ComissaoColocador.GET_CODIGO;
        ParamByName('AdiantamentoFuncionario').asstring:=Self.AdiantamentoFuncionario.GET_CODIGO;
        ParamByName('HoraExtra').asstring:=Self.HoraExtra.GET_CODIGO;
        ParamByName('Gratificacao').asstring:=Self.gratificacao.GET_CODIGO;
        ParamByName('FuncionarioFolhaPagamento').asstring:=Self.FuncionarioFolhaPagamento.GET_CODIGO;

        ParamByName('AcrescimoDSR').asstring:=Self.AcrescimoDSR.GET_CODIGO;
        ParamByName('DescontoContSindical').asstring:=Self.DescontoContSindical.GET_CODIGO;

//CODIFICA OBJETOPARATABELA


  End;
End;

//***********************************************************************

function TObjCOMISSAO_ADIANT_FUNCFOLHA.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        ComissaoVendedor.ZerarTabela;
        ComissaoColocador.ZerarTabela;
        AdiantamentoFuncionario.ZerarTabela;
        FuncionarioFolhaPagamento.ZerarTabela;
        HoraExtra.ZerarTabela;
        gratificacao.ZerarTabela;
        AcrescimoDSR.ZerarTabela;
        DescontoContSindical.ZerarTabela;

//CODIFICA ZERARTABELA





     End;
end;

Function TObjCOMISSAO_ADIANT_FUNCFOLHA.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
       If (CODIGO='')
       Then Mensagem:=mensagem+'/C�digo';

       If (FuncionarioFolhaPagamento.get_codigo='')
       Then Mensagem:=mensagem+'/Funcion�rio da Folha de Pagamento';


       if ((ComissaoVendedor.get_codigo='')
       and (ComissaoColocador.get_codigo='')
       and (AdiantamentoFuncionario.Get_Codigo='')
       and (Horaextra.get_codigo='')
       and (Gratificacao.Get_CODIGO='')
       and (AcrescimoDSR.get_codigo='')
       and (DescontoContSindical.get_codigo=''))
       Then Begin
                 Messagedlg('Uma das Comiss�es, Adiantamento, Hora Extra ou Gratifica��o deve ser preenchido',mterror,[mbok],0);
                 exit; 
       End;

      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjCOMISSAO_ADIANT_FUNCFOLHA.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     If (Self.ComissaoVendedor.get_codigo<>'')
     Then Begin
              If (Self.ComissaoVendedor.LocalizaCodigo(Self.ComissaoVendedor.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'/ Comiss�o de Vendedor n�o Encontrado!';
     end;

     if (Self.ComissaoColocador.get_codigo<>'')
     Then Begin
              If (Self.ComissaoColocador.LocalizaCodigo(Self.ComissaoColocador.Get_CODIGO)=False)
              Then Mensagem:=mensagem+'/ Comiss�o de Colocador n�o Encontrado!';
     End;

     if (Self.AdiantamentoFuncionario.Get_codigo<>'')
     Then Begin
               If (Self.AdiantamentoFuncionario.LocalizaCodigo(Self.AdiantamentoFuncionario.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Adiantamento de Sal�rio n�o Encontrado!';
     End;

     if (Self.HoraExtra.Get_codigo<>'')
     Then Begin
               If (Self.HoraExtra.LocalizaCodigo(Self.HoraExtra.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Hora Extra n�o Encontrada!';
     End;

     if (Self.Gratificacao.Get_codigo<>'')
     Then Begin
               If (Self.gratificacao.LocalizaCodigo(Self.gratificacao.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Gratifica��o n�o Encontrada!';
     End;

     if (Self.AcrescimoDSR.Get_codigo<>'')
     Then Begin
               If (Self.AcrescimoDSR.LocalizaCodigo(Self.AcrescimoDSR.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Descanso Semanal Remunerado n�o Encontrado!';
     End;

     if (Self.DescontoContSindical.Get_codigo<>'')
     Then Begin
               If (Self.DescontoContSindical.LocalizaCodigo(Self.DescontoContSindical.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Desconto Contribui��o Sindical n�o Encontrado!';
     End;

     If (Self.FuncionarioFolhaPagamento.LocalizaCodigo(Self.FuncionarioFolhaPagamento.Get_CODIGO)=False)
     Then Mensagem:=mensagem+'/ Funcion�rio da Folha de Pagamento n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.ComissaoVendedor.Get_Codigo<>'')
        Then Strtoint(Self.ComissaoVendedor.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Comiss�o de Vendedor';
     End;
     try
        If (Self.ComissaoColocador.Get_Codigo<>'')
        Then Strtoint(Self.ComissaoColocador.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Comiss�o de Colocador';
     End;
     try
        If (Self.AdiantamentoFuncionario.Get_Codigo<>'')
        Then Strtoint(Self.AdiantamentoFuncionario.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Adiantamento de Sal�rio';
     End;

     try
        If (Self.HoraExtra.Get_Codigo<>'')
        Then Strtoint(Self.HoraExtra.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Hora Extra';
     End;

     try
        If (Self.gratificacao.Get_Codigo<>'')
        Then Strtoint(Self.gratificacao.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Gratifica��o';
     End;

     try
        If (Self.AcrescimoDSR.Get_Codigo<>'')
        Then Strtoint(Self.AcrescimoDSR.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Descanso Semanal Remunerado';
     End;

     try
        If (Self.DescontoContSindical.Get_Codigo<>'')
        Then Strtoint(Self.DescontoContSindical.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Desconto de Contribui��o Sindical';
     End;

     try
        If (Self.FuncionarioFolhaPagamento.Get_Codigo<>'')
        Then Strtoint(Self.FuncionarioFolhaPagamento.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Funcion�rio da Folha de Pagamento';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro COMISSAO_ADIANT_FUNCFOLHA vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ComissaoVendedor,ComissaoColocador,AdiantamentoFuncionario,HoraExtra,gratificacao');
           SQL.ADD(' ,FuncionarioFolhaPagamento,AcrescimoDSR,DescontoContSindical');
           SQL.ADD(' from  TabComissao_Adiant_FuncFolha');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjCOMISSAO_ADIANT_FUNCFOLHA.create(Owner:TComponent);
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin

        Self.Owner := Owner;
        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.ComissaoVendedor:=TOBJCOMISSAOVENDEDORES.create;
        Self.ComissaoColocador:=TOBJCOMISSAOCOLOCADOR.create(Self.Owner);
        Self.AdiantamentoFuncionario:=TOBJADIANTAMENTOFUNCIONARIO.create;
        Self.FuncionarioFolhaPagamento:=TOBJFUNCIONARIOFOLHAPAGAMENTO.create;
        Self.HoraExtra:=TobjHoraExtra.Create;
        Self.Gratificacao:=TObjGratificacao.Create;
        Self.AcrescimoDSR:=TObjDescansoSemanalRemunerado.create;
        Self.DescontoContSindical:=TObjDescontoContribuicaoSindical.create;

//CODIFICA CRIACAO DE OBJETOS





        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabComissao_Adiant_FuncFolha(CODIGO,ComissaoVendedor');
                InsertSQL.add(' ,ComissaoColocador,AdiantamentoFuncionario,HoraExtra,gratificacao,FuncionarioFolhaPagamento,AcrescimoDSR,DescontoContSindical');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:ComissaoVendedor,:ComissaoColocador');
                InsertSQL.add(' ,:AdiantamentoFuncionario,:HoraExtra,:gratificacao,:FuncionarioFolhaPagamento,:AcrescimoDSR,:DescontoContSindical');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabComissao_Adiant_FuncFolha set CODIGO=:CODIGO');
                ModifySQL.add(',ComissaoVendedor=:ComissaoVendedor,ComissaoColocador=:ComissaoColocador');
                ModifySQL.add(',AdiantamentoFuncionario=:AdiantamentoFuncionario,HoraExtra=:HoraExtra,gratificacao=:gratificacao,FuncionarioFolhaPagamento=:FuncionarioFolhaPagamento');
                ModifySQL.add(',AcrescimoDSR=:AcrescimoDSR,DescontoContSindical=:DescontoContSindical');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabComissao_Adiant_FuncFolha where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabCOMISSAO_ADIANT_FUNCFOLHA');
     Result:=Self.ParametroPesquisa;
end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de COMISSAO_ADIANT_FUNCFOLHA ';
end;


function TObjCOMISSAO_ADIANT_FUNCFOLHA.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENCOMISSAO_ADIANT_FUNCFOLHA,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENCOMISSAO_ADIANT_FUNCFOLHA,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjCOMISSAO_ADIANT_FUNCFOLHA.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.ComissaoVendedor.FREE;
    Self.ComissaoColocador.FREE;
    Self.AdiantamentoFuncionario.FREE;
    Self.FuncionarioFolhaPagamento.FREE;
    Self.HoraExtra.Free;
    Self.Gratificacao.Free;
    Self.AcrescimoDSR.free;
    Self.DescontoContSindical.free;
    //CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjCOMISSAO_ADIANT_FUNCFOLHA.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjCOMISSAO_ADIANT_FUNCFOLHA.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjComissao_Adiant_FuncFolha.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjComissao_Adiant_FuncFolha.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
//CODIFICA GETSESUBMITS


procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoVendedorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ComissaoVendedor.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ComissaoVendedor.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.ComissaoVendedor.GET_NOME;
End;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoVendedorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ComissaoVendedor.Get_Pesquisa,Self.ComissaoVendedor.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ComissaoVendedor.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.ComissaoVendedor.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ComissaoVendedor.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoColocadorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.ComissaoColocador.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.ComissaoColocador.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.ComissaoColocador.GET_NOME;
End;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtComissaoColocadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.ComissaoColocador.Get_Pesquisa,Self.ComissaoColocador.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ComissaoColocador.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.ComissaoColocador.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.ComissaoColocador.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtAdiantamentoFuncionarioExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.AdiantamentoFuncionario.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.AdiantamentoFuncionario.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.AdiantamentoFuncionario.GET_NOME;
End;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtAdiantamentoFuncionarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   //FADIANTAMENTOFUNCIONARIO:TFADIANTAMENTOFUNCIONARIO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            //FADIANTAMENTOFUNCIONARIO:=TFADIANTAMENTOFUNCIONARIO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa
(Self.AdiantamentoFuncionario.Get_Pesquisa,Self.AdiantamentoFuncionario.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.AdiantamentoFuncionario.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.AdiantamentoFuncionario.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.AdiantamentoFuncionario.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           //Freeandnil(FADIANTAMENTOFUNCIONARIO);
     End;
end;
//*****************
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtHoraExtraExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.HoraExtra.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.HoraExtra.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.HoraExtra.GET_NOME;
End;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtHoraExtraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   //FHoraExtra:TFHoraExtra;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            //FHoraExtra:=TFHoraExtra.create(nil);

            If (FpesquisaLocal.PreparaPesquisa
(Self.HoraExtra.Get_Pesquisa,Self.HoraExtra.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.HoraExtra.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.HoraExtra.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.HoraExtra.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           //Freeandnil(FHoraExtra);
     End;
end;

procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtGratificacaoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Gratificacao.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Gratificacao.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.Gratificacao.GET_NOME;
End;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtGratificacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   //FGratificacao:TFGratificacao;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            //FGratificacao:=TFGratificacao.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Gratificacao.Get_Pesquisa,Self.Gratificacao.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Gratificacao.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl))
                                 Then Begin
                                        If Self.Gratificacao.RETORNACAMPONOME<>''
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Gratificacao.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           //Freeandnil(FGratificacao);
     End;
end;



procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtFuncionarioFolhaPagamentoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.FuncionarioFolhaPagamento.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.FuncionarioFolhaPagamento.tabelaparaobjeto;
     //LABELNOME.CAPTION:=Self.FuncionarioFolhaPagamento.GET_NOME;
End;
procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.EdtFuncionarioFolhaPagamentoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   //FFUNCIONARIOFOLHAPAGAMENTO:TFFUNCIONARIOFOLHAPAGAMENTO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            //FFUNCIONARIOFOLHAPAGAMENTO:=TFFUNCIONARIOFOLHAPAGAMENTO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa
(Self.FuncionarioFolhaPagamento.Get_Pesquisa,Self.FuncionarioFolhaPagamento.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FuncionarioFolhaPagamento.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.FuncionarioFolhaPagamento.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.FuncionarioFolhaPagamento.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           //Freeandnil(FFUNCIONARIOFOLHAPAGAMENTO);
     End;
end;
//CODIFICA EXITONKEYDOWN




procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJCOMISSAO_ADIANT_FUNCFOLHA';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Begin

            End;
          End;
     end;
end;


function TObjCOMISSAO_ADIANT_FUNCFOLHA.ExcluiComissaoColocadorporRomaneio(Promaneio: string): Boolean;
var
    PcomissaoColocador:string;
begin
     //este procedimento � chamado no Retornar do Romaneio
     //Preciso Tirar o Valor do Percentual de COmissao e o codigo do pedidoprojetoromaneio
     result:=False;

     With Self.ObjqueryPesquisa do
     Begin
          //Verificando se tem alguma comissao de colocador ligada ao romaneio
          //que ja esteja nesta tabela, se sim � porque ja foi gerada
          //a folha de pagamento para estas comissoes
          close;
          sql.clear;
          sql.add('Select TabFuncionarioFolhaPagamento.FolhaPagamento,');
          sql.add('TabpedidoProjetoRomaneio.PedidoProjeto');
          sql.add('from TabComissao_Adiant_FuncFolha TCAF');
          sql.add('join TabComissaoColocador on TCAF.comissaocolocador=tabcomissaocolocador.codigo');
          sql.add('join TABFUNCIONARIOFOLHAPAGAMENTO on TCAF.FUNCIONARIOFOLHAPAGAMENTO=TABFUNCIONARIOFOLHAPAGAMENTO.codigo');
          sql.add('join tabpedidoprojetoromaneio on tabcomissaocolocador.pedidoprojetoromaneio=tabpedidoprojetoromaneio.codigo');
          sql.add('join tabromaneio on tabpedidoprojetoromaneio.romaneio=tabromaneio.codigo');
          sql.add('where TabPedidoProjetoRomaneio.Romaneio='+Promaneio);
          open;
          last;
          if (recordcount>0)
          Then Begin
                    if (Messagedlg('Algumas comiss�es de colocador ligadas a este romaneio ja foram pagas. Deseja visualiz�-las?',mtinformation,[mbyes,mbno],0)=mrno)
                    Then exit;

                    FmostraStringGrid.StringGrid.FixedCols:=0;
                    FmostraStringGrid.StringGrid.FixedRows:=0;
                    FmostraStringGrid.StringGrid.ColCount:=2;
                    FmostraStringGrid.StringGrid.RowCount:=1;
                    FmostraStringGrid.StringGrid.Cells[0,0]:='Folha';
                    FmostraStringGrid.StringGrid.Cells[1,0]:='Ped.Proj.';
                    first;
                    while not(eof) do
                    Begin
                         FmostraStringGrid.StringGrid.RowCount:=FmostraStringGrid.StringGrid.RowCount+1;
                         FmostraStringGrid.StringGrid.Cells[0,FmostraStringGrid.StringGrid.RowCount-1]:=fieldbyname('folhapagamento').asstring;
                         FmostraStringGrid.StringGrid.Cells[1,FmostraStringGrid.StringGrid.RowCount-1]:=fieldbyname('pedidoprojeto').asstring;
                         next;
                    End;

                    FmostraStringGrid.StringGrid.FixedRows:=1;
                    AjustaLArguraColunaGrid(FmostraStringGrid.StringGrid);

                    FmostraStringGrid.LbMensagem.caption:='Pedidos Projetos com Folha de Pagamento';
                    FmostraStringGrid.ShowModal;
                    exit;
          End;

          Close;
          SQL.clear;
          close;
          sql.add('Update TabComissaoColocador set PercentualComissaoColocador=NULL');
          sql.add(',PedidoProjetoRomaneio=NULL');
          sql.add('where Codigo in (');
          sql.add('Select TabComissaoColocador.codigo from TabComissaoColocador');
          sql.add('join tabpedidoprojetoromaneio on TabComissaoColocador.PedidoProjetoRomaneio=TabPedidoProjetoRomaneio.codigo');
          sql.add('where TabPedidoProjetoRomaneio.Romaneio='+Promaneio+')');
          Try
            Execsql;
          Except
                Messagedlg('Erro na Tentativa de Alterar os Registros da Comiss�o do Colocador para o Romaneio',mterror,[mbok],0);
                exit;
          End;
          Result:=True;
     End;
End;


function TObjCOMISSAO_ADIANT_FUNCFOLHA.Cria_Registros_Folha(PFolha:string): boolean;
var
Pquerytemp:Tibquery;
PdataQuitacao,PdataSalario,PDataComissaoColocador:string;
PsomaDescontoContSindical,PsomaDSR,PSomaGratificacao,PSomaHoraExtra,PsomaAdiantamento,PSomaComissaoVendedor,PSomaComissaoColocador:Currency;
PgeraComissaoColocador:boolean;
begin
     result:=False;

     PgeraComissaoColocador:=False;
     If (ObjParametroGlobal.ValidaParametro('INCLUI COMISSAO DO COLOCADOR NA FOLHA DE PAGAMENTO?')=True)
     Then
        if (ObjParametroGlobal.Get_Valor='SIM')
        then PgeraComissaoColocador:=True;



     if (PFolha='')
     Then Begin
               Messagedlg('� necess�rio escolher uma Folha',mterror,[mbok],0);
               exit;
     End;

     if (Self.FuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(pfolha)=False)
     Then Begin
               Messagedlg('Folha de Pagamento n�o encontrada',mterror,[mbok],0);
               exit;
     End;
     Self.FuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto;
     PdataSalario:=Self.FuncionarioFolhaPagamento.FolhaPagamento.Get_DataSalarioAdiantamento;
     //PdataComissaoVendedor:=Self.FuncionarioFolhaPagamento.FolhaPagamento.Get_DataComissaoVendedor;
     PDataComissaoColocador:=Self.FuncionarioFolhaPagamento.FolhaPagamento.Get_DataComissaocolocador;
     PdataQuitacao:=Self.FuncionarioFolhaPagamento.FolhaPagamento.get_DataQuitacaoComissaoVendedor;
     //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

     Try
        Pquerytemp:=Tibquery.create(nil);
        Pquerytemp.database:=Fdatamodulo.IBDatabase;
     Except
           Messagedlg('Erro na tentativa de Criar a PqueryTemp',mterror,[mbok],0);
           exit;
     End;

Try
     //Primeiro Seleciono todos os funcionarios da Folha de Pagamento escolhida

     With Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo,Funcionario from TabFuncionarioFolhaPagamento where FolhaPagamento='+Pfolha);
          open;
          last;
          FMostraBarraProgresso.Lbmensagem.caption:='SOMANDO COMISS�ES E ADIANTAMENTOS (FUNCION�RIOS)';
          FMostraBarraProgresso.BarradeProgresso.MaxValue:=RECORDCOUNT;
          FMostraBarraProgresso.BarradeProgresso.Progress:=0;
          FMostraBarraProgresso.Show;
          first;
          
          if (Recordcount=0)
          Then exit;

          While not(eof) do//esse while percorre os funcionarios da folha
          Begin
               FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
               Application.ProcessMessages;


               PsomaAdiantamento:=0;
               PSomaComissaoVendedor:=0;
               PSomaComissaoColocador:=0;
               PsomaHoraExtra:=0;
               PsomaGratificacao:=0;
               PsomaDSR:=0;
               PsomaDescontoContSindical:=0;

               //com o funcionario em maos preciso pesquisar nas 4 tabelas para inserir nessa

               //Iniciando com os Adiantamentos
               Pquerytemp.Close;
               Pquerytemp.sql.clear;
               Pquerytemp.sql.add('Select TabAdiantamentoFuncionario.* from TabAdiantamentoFuncionario');
               Pquerytemp.sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.AdiantamentoFuncionario=TabAdiantamentoFuncionario.codigo');
               Pquerytemp.sql.add('where TCAFF.codigo is null');
               Pquerytemp.sql.add('and DataSalario='+#39+formatdatetime('mm/dd/yyyy',strtodate(PdataSalario))+#39);
               Pquerytemp.sql.add('and Funcionario='+Fieldbyname('funcionario').asstring);
               Pquerytemp.open;
               Pquerytemp.last;
               FMostraBarraProgresso.Lbmensagem2.Visible:=true;
               FMostraBarraProgresso.Lbmensagem2.Caption:='ADIANTAMENTO DO FUNCIONARIO ATUAL';
               FMostraBarraProgresso.BarradeProgresso2.Visible:=true;
               FMostraBarraProgresso.BarradeProgresso2.MaxValue:=pquerytemp.recordcount;
               FMostraBarraProgresso.BarradeProgresso2.progress:=0;


               Pquerytemp.first;

               While Not(PqueryTemp.eof) do
               Begin
                   FMostraBarraProgresso.BarradeProgresso2.progress:=FMostraBarraProgresso.BarradeProgresso2.progress+1;
                   Application.ProcessMessages;

                   //esse while percorre os adiantamentos desse funcionario
                   //para cada adiant. gravo um registro na Tabcomissao_aditn....
                   //e guardo o valor do mesmo para no final ter o total
                   //showmessage(fieldbyname('funcionario').asstring);
                   Self.ZerarTabela;
                   Self.Status:=dsinsert;
                   Self.Submit_CODIGO('0');
                   Self.AdiantamentoFuncionario.Submit_Codigo(Pquerytemp.fieldbyname('codigo').asstring);
                   Self.FuncionarioFolhaPagamento.Submit_CODIGO(Fieldbyname('codigo').asstring);
                   if (Self.Salvar(False)=False)
                   then Begin
                             Messagedlg('N�o foi poss�vel salvar o relac. entre o funcion�rio da folha e o adiantamento '+#13+
                                        'Adiant.'+pquerytemp.Fieldbyname('codigo').asstring,mterror,[mbok],0);
                             exit;
                   End;
                   //somando os adiantamentos desse funcionario
                   PsomaAdiantamento:=PsomaAdiantamento+Pquerytemp.Fieldbyname('valor').asfloat;
                   Pquerytemp.Next;
               End;

               //Comissao de vendedores
               //Pega Todos os Registros da tabela de Comissao
               //que nao tenham relacionamento com a tabela TabComissao_Adiant_FuncFolha
               //que estejam com o venmcimento da pendencia <= a data escolhida
               //seja o funcionario atual da FuncFolha
               //possua o saldo=0 na pendencia
               //e que tenha tido qualquer valor de quitacao
               
               Pquerytemp.Close;
               Pquerytemp.sql.clear;
               Pquerytemp.sql.add('Select TabComissaoVendedores.* from TabComissaoVendedores');
               Pquerytemp.sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.Comissaovendedor=TabComissaoVendedores.codigo');
               Pquerytemp.sql.add('join TabVendedor on TabComissaoVendedores.Vendedor=tabvendedor.codigo');
               Pquerytemp.sql.add('join TabFuncionarios on Tabvendedor.codigofuncionario=tabfuncionarios.codigo');
               Pquerytemp.sql.add('join ProcLancamentoPendencia on TabComissaovendedores.pendencia=ProcLancamentoPendencia.ppendencia');
               Pquerytemp.sql.add('where TCAFF.codigo is null');
               Pquerytemp.sql.add('and TabFuncionarios.codigo='+Fieldbyname('funcionario').asstring);
               Pquerytemp.sql.add('and ProcLancamentoPendencia.pSaldo=0 and ProcLancamentoPendencia.PsomaQuitacao>0');
               Pquerytemp.sql.add('and ProcLancamentoPendencia.Pultimaquitacao<='+#39+Formatdatetime('mm/dd/yyyy',strtodate(PdataQuitacao))+#39);
               Pquerytemp.sql.add('order by ProcLancamentoPendencia.pVencimento');
               Pquerytemp.open;
               Pquerytemp.last;
               FMostraBarraProgresso.Lbmensagem2.Visible:=true;
               FMostraBarraProgresso.Lbmensagem2.Caption:='COMISS�O DE VENDA DO FUNCIONARIO ATUAL';
               FMostraBarraProgresso.BarradeProgresso2.Visible:=true;
               FMostraBarraProgresso.BarradeProgresso2.MaxValue:=pquerytemp.recordcount;
               FMostraBarraProgresso.BarradeProgresso2.progress:=0;
               Pquerytemp.first;

               While Not(PqueryTemp.eof) do
               Begin
                   FMostraBarraProgresso.BarradeProgresso2.progress:=FMostraBarraProgresso.BarradeProgresso2.progress+1;
                   Application.ProcessMessages;
                   
                   Self.ZerarTabela;
                   Self.Status:=dsinsert;
                   Self.Submit_CODIGO('0');
                   Self.ComissaoVendedor.Submit_Codigo(Pquerytemp.fieldbyname('codigo').asstring);
                   Self.FuncionarioFolhaPagamento.Submit_CODIGO(Fieldbyname('codigo').asstring);
                   if (Self.Salvar(False)=False)
                   then Begin
                             Messagedlg('N�o foi poss�vel salvar o relac. entre o funcion�rio da folha e a Comiss�o de vendedor de c�digo '+#13+pquerytemp.Fieldbyname('codigo').asstring,mterror,[mbok],0);
                             exit;
                   End;
                   //somando os adiantamentos desse funcionario
                   PSomaComissaoVendedor:=PSomaComissaoVendedor+Pquerytemp.Fieldbyname('valorcomissao').asfloat;
                   Pquerytemp.Next;
               End;


               if (PGeraComissaoColocador)
               then Begin
                         //Comissao de Colocador
                         Pquerytemp.Close;
                         Pquerytemp.sql.clear;
                         Pquerytemp.sql.add('Select TabcomissaoColocador.* from TabcomissaoColocador');
                         Pquerytemp.sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.ComissaoColocador=TAbComissaoColocador.codigo');
                         Pquerytemp.sql.add('join tabpedidoprojetoromaneio on Tabpedidoprojetoromaneio.codigo=tabcomissaocolocador.pedidoprojetoromaneio');
                         Pquerytemp.sql.add('join TabRomaneio on TabPedidoProjetoRomaneio.Romaneio=tabromaneio.codigo');
                         Pquerytemp.sql.add('join TabColocador on TabRomaneio.Colocador=TabColocador.codigo');
                         Pquerytemp.sql.add('join tabfuncionarios on Tabcolocador.funcionario=tabfuncionarios.codigo');
                         Pquerytemp.sql.add('Where TCAFF.Codigo is null');
                         Pquerytemp.sql.add('and TabFuncionarios.codigo='+Fieldbyname('funcionario').asstring);
                         Pquerytemp.sql.add('and TabRomaneio.DataEntrega<='#39+formatdatetime('mm/dd/yyyy',strtodate(PDataComissaoColocador))+#39);
                         Pquerytemp.open;
                         Pquerytemp.last;
                         FMostraBarraProgresso.Lbmensagem2.Visible:=true;
                         FMostraBarraProgresso.Lbmensagem2.Caption:='COMISS�O DO COLOCADOR DO FUNCIONARIO ATUAL';
                         FMostraBarraProgresso.BarradeProgresso2.Visible:=true;
                         FMostraBarraProgresso.BarradeProgresso2.MaxValue:=pquerytemp.recordcount;
                         FMostraBarraProgresso.BarradeProgresso2.progress:=0;
                         Pquerytemp.first;

                         While Not(PqueryTemp.eof) do
                         Begin
                             FMostraBarraProgresso.BarradeProgresso2.progress:=FMostraBarraProgresso.BarradeProgresso2.progress+1;
                             Application.ProcessMessages;

                             Self.ZerarTabela;
                             Self.Status:=dsinsert;
                             Self.Submit_CODIGO('0');
                             Self.ComissaoColocador.Submit_Codigo(Pquerytemp.fieldbyname('codigo').asstring);
                             Self.FuncionarioFolhaPagamento.Submit_CODIGO(Fieldbyname('codigo').asstring);
                             if (Self.Salvar(False)=False)
                             then Begin
                                       Messagedlg('N�o foi poss�vel salvar o relac. entre o funcion�rio da folha e a Comiss�o de Colocador de c�digo '+#13+pquerytemp.Fieldbyname('codigo').asstring,mterror,[mbok],0);
                                       exit;
                             End;
                             //somando os adiantamentos desse funcionario
                             PSomaComissaoColocador:=PSomaComissaoColocador+Pquerytemp.Fieldbyname('valorcomissao').asfloat;
                             Pquerytemp.Next;
                         End;
               End;

               //HORA EXTRA
               Pquerytemp.Close;
               Pquerytemp.sql.clear;
               Pquerytemp.sql.add('Select TabHoraExtra.*,Tcaff.codigo as CODTCAFF from TabHoraExtra');
               Pquerytemp.sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.HoraExtra=TabHoraExtra.codigo');
               Pquerytemp.sql.add('where TCAFF.codigo is null and');
               Pquerytemp.sql.add('TabHoraExtra.DataSalario='#39+formatdatetime('mm/dd/yyyy',strtodate(PdataSalario))+#39);
               Pquerytemp.sql.add('and TabHoraExtra.Funcionario='+Fieldbyname('funcionario').asstring);
               Pquerytemp.open;
               Pquerytemp.last;
               FMostraBarraProgresso.Lbmensagem2.Visible:=true;
               FMostraBarraProgresso.Lbmensagem2.Caption:='HORA EXTRA DO FUNCIONARIO ATUAL';
               FMostraBarraProgresso.BarradeProgresso2.Visible:=true;
               FMostraBarraProgresso.BarradeProgresso2.MaxValue:=pquerytemp.recordcount;
               FMostraBarraProgresso.BarradeProgresso2.progress:=0;
               Pquerytemp.first;

               While Not(PqueryTemp.eof) do
               Begin
                   FMostraBarraProgresso.BarradeProgresso2.progress:=FMostraBarraProgresso.BarradeProgresso2.progress+1;
                   Application.ProcessMessages;

                   Self.ZerarTabela;
                   Self.Status:=dsinsert;
                   Self.Submit_CODIGO('0');
                   Self.HoraExtra.Submit_Codigo(Pquerytemp.fieldbyname('codigo').asstring);
                   Self.FuncionarioFolhaPagamento.Submit_CODIGO(Fieldbyname('codigo').asstring);
                   if (Self.Salvar(False)=False)
                   then Begin
                             Messagedlg('N�o foi poss�vel salvar o relac. entre o funcion�rio da folha e a Hora Extra de c�digo '+#13+pquerytemp.Fieldbyname('codigo').asstring,mterror,[mbok],0);
                             exit;
                   End;
                   //somando os adiantamentos desse funcionario
                   PSomaHoraExtra:=PSomaHoraExtra+Pquerytemp.Fieldbyname('valor').asfloat;
                   Pquerytemp.Next;
               End;

               //*****************

               //Gratifica��es
               Pquerytemp.Close;
               Pquerytemp.sql.clear;
               Pquerytemp.sql.add('Select TabGratificacao.*,Tcaff.codigo as CODTCAFF from TabGratificacao');
               Pquerytemp.sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.Gratificacao=TabGratificacao.codigo');
               Pquerytemp.sql.add('where TCAFF.codigo is null and');
               Pquerytemp.sql.add('TabGratificacao.DataSalario='#39+formatdatetime('mm/dd/yyyy',strtodate(PdataSalario))+#39);
               Pquerytemp.sql.add('and TabGratificacao.Funcionario='+Fieldbyname('funcionario').asstring);
               Pquerytemp.open;
               Pquerytemp.last;
               FMostraBarraProgresso.Lbmensagem2.Visible:=true;
               FMostraBarraProgresso.Lbmensagem2.Caption:='GRATIFICA��ES DO FUNCIONARIO ATUAL';
               FMostraBarraProgresso.BarradeProgresso2.Visible:=true;
               FMostraBarraProgresso.BarradeProgresso2.MaxValue:=pquerytemp.recordcount;
               FMostraBarraProgresso.BarradeProgresso2.progress:=0;
               Pquerytemp.first;

               While Not(PqueryTemp.eof) do
               Begin
                   FMostraBarraProgresso.BarradeProgresso2.progress:=FMostraBarraProgresso.BarradeProgresso2.progress+1;
                   Application.ProcessMessages;

                   Self.ZerarTabela;
                   Self.Status:=dsinsert;
                   Self.Submit_CODIGO('0');
                   Self.Gratificacao.Submit_Codigo(Pquerytemp.fieldbyname('codigo').asstring);
                   Self.FuncionarioFolhaPagamento.Submit_CODIGO(Fieldbyname('codigo').asstring);
                   if (Self.Salvar(False)=False)
                   then Begin
                             Messagedlg('N�o foi poss�vel salvar o relac. entre o funcion�rio da folha e a Gratifica��o de c�digo '+#13+pquerytemp.Fieldbyname('codigo').asstring,mterror,[mbok],0);
                             exit;
                   End;
                   //somando os adiantamentos desse funcionario
                   PSomaGratificacao:=PSomaGratificacao+Pquerytemp.Fieldbyname('valor').asfloat;
                   Pquerytemp.Next;
               End;

               //*****************

               //Descanso Semanal Remunerado
               Pquerytemp.Close;
               Pquerytemp.sql.clear;
               Pquerytemp.sql.add('Select TDSR.*,Tcaff.codigo as CODTCAFF from TabDescansoSemanalRemunerado TDSR');
               Pquerytemp.sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.AcrescimoDSR=TDSR.codigo');
               Pquerytemp.sql.add('where TCAFF.codigo is null and');
               Pquerytemp.sql.add('TDSR.DataSalario='#39+formatdatetime('mm/dd/yyyy',strtodate(PdataSalario))+#39);
               Pquerytemp.sql.add('and TDSR.Funcionario='+Fieldbyname('funcionario').asstring);
               Pquerytemp.open;
               Pquerytemp.last;
               FMostraBarraProgresso.Lbmensagem2.Visible:=true;
               FMostraBarraProgresso.Lbmensagem2.Caption:='DSR DO FUNCIONARIO ATUAL';
               FMostraBarraProgresso.BarradeProgresso2.Visible:=true;
               FMostraBarraProgresso.BarradeProgresso2.MaxValue:=pquerytemp.recordcount;
               FMostraBarraProgresso.BarradeProgresso2.progress:=0;
               Pquerytemp.first;

               While Not(PqueryTemp.eof) do
               Begin
                   FMostraBarraProgresso.BarradeProgresso2.progress:=FMostraBarraProgresso.BarradeProgresso2.progress+1;
                   Application.ProcessMessages;

                   Self.ZerarTabela;
                   Self.Status:=dsinsert;
                   Self.Submit_CODIGO('0');
                   Self.AcrescimoDSR.Submit_Codigo(Pquerytemp.fieldbyname('codigo').asstring);
                   Self.FuncionarioFolhaPagamento.Submit_CODIGO(Fieldbyname('codigo').asstring);
                   if (Self.Salvar(False)=False)
                   then Begin
                             Messagedlg('N�o foi poss�vel salvar o relac. entre o funcion�rio da folha e o DSR de c�digo '+#13+pquerytemp.Fieldbyname('codigo').asstring,mterror,[mbok],0);
                             exit;
                   End;
                   //somando os adiantamentos desse funcionario
                   PSomaDSR:=PSomaDSR+Pquerytemp.Fieldbyname('valor').asfloat;
                   Pquerytemp.Next;
               End;

               //Desconto Sindical remunerado
               Pquerytemp.Close;
               Pquerytemp.sql.clear;                                                                        
               Pquerytemp.sql.add('Select TDCS.*,Tcaff.codigo as CODTCAFF from TabDescontoContribuicaoSindical TDCS');
               Pquerytemp.sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.DescontoContSindical=TDCS.codigo');
               Pquerytemp.sql.add('where TCAFF.codigo is null and');
               Pquerytemp.sql.add('TDCS.DataSalario='#39+formatdatetime('mm/dd/yyyy',strtodate(PdataSalario))+#39);
               Pquerytemp.sql.add('and TDCS.Funcionario='+Fieldbyname('funcionario').asstring);
               Pquerytemp.open;
               Pquerytemp.last;
               FMostraBarraProgresso.Lbmensagem2.Visible:=true;
               FMostraBarraProgresso.Lbmensagem2.Caption:='DESCONTO CONT. SINDICAL DO FUNCIONARIO ATUAL';
               FMostraBarraProgresso.BarradeProgresso2.Visible:=true;
               FMostraBarraProgresso.BarradeProgresso2.MaxValue:=pquerytemp.recordcount;
               FMostraBarraProgresso.BarradeProgresso2.progress:=0;
               Pquerytemp.first;

               While Not(PqueryTemp.eof) do
               Begin
                   FMostraBarraProgresso.BarradeProgresso2.progress:=FMostraBarraProgresso.BarradeProgresso2.progress+1;
                   Application.ProcessMessages;

                   Self.ZerarTabela;
                   Self.Status:=dsinsert;
                   Self.Submit_CODIGO('0');
                   Self.DescontoContSindical.Submit_Codigo(Pquerytemp.fieldbyname('codigo').asstring);
                   Self.FuncionarioFolhaPagamento.Submit_CODIGO(Fieldbyname('codigo').asstring);
                   if (Self.Salvar(False)=False)
                   then Begin
                             Messagedlg('N�o foi poss�vel salvar o relac. entre o funcion�rio da folha e o Desc. Cont. Sindical de c�digo '+#13+pquerytemp.Fieldbyname('codigo').asstring,mterror,[mbok],0);
                             exit;
                   End;
                   //somando os adiantamentos desse funcionario
                   PsomaDescontoContSindical:=PsomaDescontoContSindical+Pquerytemp.Fieldbyname('valor').asfloat;
                   Pquerytemp.Next;
               End;

               FMostraBarraProgresso.Lbmensagem2.Caption:='';
               
               //*****************
               //no Final Guardando os totais na TabFuncionariofolhaPagamento
               if (Self.FuncionarioFolhaPagamento.LocalizaCodigo(fieldbyname('codigo').asstring)=False)
               Then begin
                         Messagedlg('O Funcion�rio da Folha n�o encontrado',mterror,[mbok],0);
                         exit;
               End;
               
               Self.FuncionarioFolhaPagamento.TabelaparaObjeto;
               Self.FuncionarioFolhaPagamento.Status:=dsedit;
               Self.FuncionarioFolhaPagamento.Submit_TotalAdiantamento(floattostr(PsomaAdiantamento));
               Self.FuncionarioFolhaPagamento.Submit_ComissaoVenda(floattostr(PSomaComissaoVendedor));
               Self.FuncionarioFolhaPagamento.Submit_Comissaocolocador(floattostr(PSomaComissaoColocador));
               Self.FuncionarioFolhaPagamento.Submit_AcrescimoHoraExtra(floattostr(PSomaHoraExtra));
               Self.FuncionarioFolhaPagamento.Submit_AcrescimoGratificacao(floattostr(PSomaGratificacao));
               Self.FuncionarioFolhaPagamento.Submit_AcrescimoDSR(floattostr(pSomaDSR));
               Self.FuncionarioFolhaPagamento.Submit_DescontoContSindical(floattostr(PsomaDescontoContSindical));

               if (Self.FuncionarioFolhaPagamento.Salvar(False)=False)
               Then begin
                         Messagedlg('N�o foi poss�vel atualizar os Valores das Comiss�es,Adiantamentos, descontos e acr�scimos',mterror,[mbok],0);
                         exit;
               End;

               Next;//pegando o prox. funcfolha
          End;//while do funcionario da folha
               
     End;//with
     Result:=True;
Finally
       Freeandnil(PqueryTemp);
End;

end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.Exclui_por_folha(
  Pfolha: string): boolean;
begin
     result:=False;
     With Self.ObjqueryPesquisa do
     Begin
          //apaga todos os registros que estaum na folha escolhida
          close;
          sql.clear;
          sql.add('Delete from tabComissao_Adiant_FuncFolha where codigo in');
          sql.add('(Select TCAF.codigo from TabComissao_Adiant_FuncFolha TCAF ');
          sql.add('join TabFuncionarioFolhaPagamento on TCAF.FuncionarioFolhaPagamento=TabFuncionarioFolhaPagamento.codigo');
          sql.add('where TabFuncionarioFolhaPagamento.FolhaPagamento='+Pfolha+')');
          Try
              execsql;
              result:=true;
          Except
                on e:exception do
                Begin
                     Messagedlg(E.message,mterror,[mbok],0);
                     exit;
                End;
          End;
     End;
end;

procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.ImprimeComissaovendedor;
var
  psomenteaberto,ppedido,Pvendedor:string;
  Pdata1,pdata2:string;
  SomaComissaoTotal,
  SomaVendasTotal,
  somacomissaovendedor,
  somavendasvendedor:currency;
  temp:string;
  incluidevolvidos:Boolean;
begin

     pvendedor:='';
     psomenteaberto:='';
     
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('RELAT�RIO DE COMISS�O POR VENDEDOR')=False)
     then Begin
               //nao tem permissao, entao pe�o senha para escolher o vendedor
               //com senha

               if (Self.ComissaoVendedor.VENDEDOR.PegaVendedor(Pvendedor)=False)
               Then exit;
     End;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Venda Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.caption:='Venda Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo06.Enabled:=True;
          LbGrupo06.caption:='Somente Abertos';
          ComboGrupo06.Items.clear;
          ComboGrupo06.Items.add('N�O');
          ComboGrupo06.Items.add('SIM');
          ComboGrupo06.ItemIndex:=0;

          Grupo07.Enabled:=True;
          LbGrupo07.caption:='Mostra Ocorr.Ch.Dev.';
          ComboGrupo07.Items.Clear;
          ComboGrupo07.Items.add('N�O');
          ComboGrupo07.Items.add('SIM');
          ComboGrupo07.ItemIndex:=0;
          edtgrupo07.Visible:=False;
         
          if (Pvendedor='')
          Then Begin
                    Grupo03.Enabled:=true;
                    LbGrupo03.caption:='Vendedor';
                    edtgrupo03.OnKeyDown:=Self.ComissaoVendedor.EdtVENDEDORKeyDown;
                    edtgrupo03.Color:=$005CADFE
          End
          Else edtgrupo03.text:=pvendedor;

          showmodal;

          edtgrupo07.Visible:=True;

          if (tag=0)
          Then exit;

          if (ComboGrupo06.ItemIndex=1)
          Then psomenteaberto:='S'
          Else psomenteaberto:='N';

          if (ComboGrupo07.ItemIndex=1)
          Then IncluiDevolvidos:=True
          Else IncluiDevolvidos:=False;



          Pdata1:='';
          Pdata2:='';
          Pvendedor:='';

          Try
             if (trim(comebarra(edtgrupo01.text))<>'')
             and (trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                       strtodate(edtgrupo01.Text);
                       Pdata1:=edtgrupo01.Text;
                       strtodate(edtgrupo02.Text);
                       Pdata2:=edtgrupo02.Text;
             end;
          Except
                Messagedlg('Datas inv�lida',mterror,[mbok],0);
                exit;
          End;


          Try
             if (trim(comebarra(edtgrupo03.text))<>'')
             Then Begin
                       strtoint(edtgrupo03.Text);
                       Pvendedor:=edtgrupo03.text;
                       if (Self.ComissaoVendedor.VENDEDOR.LocalizaCodigo(Pvendedor)=False)
                       Then Begin
                                 Messagedlg('Vendedor n�o localizado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ComissaoVendedor.VENDEDOR.TabelaparaObjeto;
             End;
          Except
                Messagedlg('Funcion�rio inv�lido',mterror,[mbok],0);
                exit;
          End;



     End;


     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select TabPedido.vendedor,TabPedido.data,tabvendedor.nome as NOMEVENDEDOR,TabPedido.Codigo as PEDIDO,');
          sql.add('TabPedido.cliente,TabCliente.Nome as NOMECLIENTE,');
          sql.add('tabpedido.valortotal,');
          sql.add('tabpedido.valordesconto,tabpedido.valoracrescimo,tabpedido.valorfinal,');
          sql.add('TabComissaovendedores.Pendencia,');
          sql.add('TabComissaovendedores.Comissao,');
          sql.add('TabComissaovendedores.Valor,');
          sql.add('TabComissaovendedores.ValorComissao,');
          sql.add('TabComissaovendedores.Codigo as CODIGOCOMISSAO,');
          sql.add('TabComissao_adiant_funcfolha.codigo as COMISSAOADIANT,');
          sql.add('TabFuncionariofolhaPagamento.codigo as FUNCFOLHA,');
          sql.add('TabFuncionariofolhaPagamento.FolhaPagamento,TabComissaovendedores.ChequeDevolvido');
          sql.add('from TabComissaoVendedores');
          sql.add('join tabpedido on tabcomissaovendedores.pedido=tabpedido.codigo');
          sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
          sql.add('join tabvendedor on tabpedido.vendedor=tabvendedor.codigo');
          sql.add('left join TabComissao_adiant_funcfolha on tabcomissaovendedores.codigo=TabComissao_adiant_funcfolha.comissaovendedor');
          sql.add('left join TabFuncionariofolhaPagamento on TabComissao_adiant_funcfolha.funcionariofolhapagamento=tabfuncionariofolhapagamento.codigo');
          sql.add('where TabComissaoVendedores.codigo<>-500');

          if (incluidevolvidos=False)
          Then sql.add('and TabComissaoVendedores.ChequeDevolvido is null');//nao pode entrar as comissoes do Cheque Devolvido a pedido da Genice


          if (Pdata1<>'')
          Then Sql.add('and Tabpedido.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39+' and Tabpedido.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39);

          if (Pvendedor<>'')
          Then sql.add('and TabPedido.Vendedor='+Pvendedor);


          if (PsomenteAberto='S')
          Then sql.add('and TabComissao_adiant_funcfolha.funcionariofolhapagamento is null');

          sql.add('order by tabpedido.vendedor,tabpedido.codigo');

          //InputBox('','',SQL.Text);

          open;
          If (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma Informa��o foi Selecionada',mtinformation,[mbok],0);
                    exit;
          end;

          
          with FreltxtRDPRINT do
          Begin
              ConfiguraImpressao;
              LinhaLocal:=3;
              RDprint.FonteTamanhoPadrao:=s17cpp;
              RDprint.TamanhoQteColunas:=130;
              RDprint.Abrir;
              if (RDprint.Setup=False)
              Then begin
                        RDprint.Fechar;
                        exit;
              end;
              RDprint.ImpC(LinhaLocal,65,'RELAT�RIO DE COMISS�O DE VENDEDOR',[negrito]);
              IncrementaLinha(2);

              if (Pdata1<>'')
              Then Begin
                        RDprint.ImpF(linhalocal,1,'DATAS '+Pdata1+' a '+Pdata2,[negrito]);
                        IncrementaLinha(1);
              end;

              if (Pvendedor<>'')
              Then Begin
                        RDprint.ImpF(linhalocal,1,'VENDEDOR '+PVENDEDOR+'-'+Self.ComissaoVendedor.VENDEDOR.Get_Nome,[negrito]);
                        IncrementaLinha(1);
              End;

              IncrementaLinha(1);

              temp:=CompletaPalavra('PEDIDO',6,' ')+' '+
                                   CompletaPalavra('DATA',10,' ')+' '+
                                   CompletaPalavra('CLIENTE',30,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VALOR',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('DESCONTO',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('ACRESCIMO',05,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VAL. FINAL',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('PEND.',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VL.COMISSAO',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('FOLHA PAG.',10,' ');

              if (incluidevolvidos)
              Then temp:=temp+'CHDEV';


              RDprint.ImpF(LinhaLocal,1,temp,[negrito]);



              IncrementaLinha(1);

              DesenhaLinha;
              IncrementaLinha(1);



              //Pedido		Cliente		Valor 		Desconto	Valor Final	Pend�ncia	Valor Comiss�o
              Pvendedor:=Fieldbyname('vendedor').asstring;
              SomaComissaovendedor:=0;
              Somavendasvendedor:=0;
              SomaComissaoTotal:=0;
              SomaVendasTotal:=0;


              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
              IncrementaLinha(1);

              Ppedido:='';
              
              While not(Self.Objquery.eof) do
              Begin
                   if (Pvendedor<>Fieldbyname('vendedor').asstring)
                   Then Begin
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,'SOMA DAS VENDAS              '+CompletaPalavra_a_Esquerda(formata_Valor(somavendasvendedor),12,' '),[negrito]);
                             IncrementaLinha(1);
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
                             IncrementaLinha(2);

                             Pvendedor:=Fieldbyname('vendedor').asstring;
                             SomaComissaovendedor:=0;
                             SomaVendasVendedor:=0;

                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
                             IncrementaLinha(1);
                   End;

                   if (Ppedido<>Fieldbyname('pedido').asstring)
                   Then Begin
                             Ppedido:=Fieldbyname('pedido').asstring;
                             somavendasvendedor:=Somavendasvendedor+fieldbyname('valorfinal').asfloat;
                             SomaVendasTotal:=SomaVendasTotal+fieldbyname('valorfinal').asfloat;

                             VerificaLinha;
                             RDprint.Imp(LinhaLocal,1,CompletaPalavra(fieldbyname('PEDIDO').AsString,6,' ')+' '+
                                                      CompletaPalavra(fieldbyname('data').AsString,10,' ')+' '+
                                                      CompletaPalavra(fieldbyname('CLIENTE').AsString+'-'+fieldbyname('nomeCLIENTE').AsString,30,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORtotal').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorDESCONTO').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valoracrescimo').AsString),5,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORFINAL').AsString),10,' ')+' '+
                                                      //CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALOR').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('PENDencia').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorCOMISSAO').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('folhapagamento').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('chequedevolvido').AsString,5,' '));
                             IncrementaLinha(1);
                   End
                   Else Begin
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,CompletaPalavra(fieldbyname('PEDIDO').AsString,6,' ')+' '+
                                                      CompletaPalavra(fieldbyname('data').AsString,10,' ')+' '+
                                                      CompletaPalavra(fieldbyname('CLIENTE').AsString+'-'+fieldbyname('nomeCLIENTE').AsString,30,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',5,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('PENDencia').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorCOMISSAO').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('folhapagamento').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('chequedevolvido').AsString,5,' '),[italico]);
                             IncrementaLinha(1);

                   End;
                   somacomissaovendedor:=Somacomissaovendedor+fieldbyname('valorcomissao').asfloat;
                   SomaComissaototal:=SomaComissaoTotal+fieldbyname('valorcomissao').asfloat;
                   Self.Objquery.Next;
              End;
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DAS VENDAS              '+CompletaPalavra_a_Esquerda(formata_Valor(somavendasvendedor),12,' '),[negrito]);
              IncrementaLinha(1);
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
              IncrementaLinha(1);

              VerificaLinha;
              RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',130,'_'));
              IncrementaLinha(1);

              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DAS COMISS�ES GERAL '+CompletaPalavra_a_Esquerda(formata_Valor(SomaComissaoTotal),12,' '),[negrito]);
              IncrementaLinha(1);
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DAS VENDAS    GERAL '+CompletaPalavra_a_Esquerda(formata_Valor(SomaVendasTotal),12,' '),[negrito]);
              IncrementaLinha(1);
              RDprint.Fechar;
          End;//with Freltxt
     end;//objquery
end;


procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.ImprimeComissaovendedor_LoteComissao;
var
  psomenteaberto,ppedido,Pvendedor:string;
  Pdata1,pdata2:string;
  SomaComissaoTotal,
  SomaVendasTotal,
  somacomissaovendedor,
  somavendasvendedor:currency;
  temp:string;
  incluidevolvidos:Boolean;
begin

     pvendedor:='';
     psomenteaberto:='';

     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('RELAT�RIO DE COMISS�O POR VENDEDOR')=False)
     then Begin
               //nao tem permissao, entao pe�o senha para escolher o vendedor
               //com senha

               if (Self.ComissaoVendedor.VENDEDOR.PegaVendedor(Pvendedor)=False)
               Then exit;
     End;

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo01.Enabled:=true;
          LbGrupo01.caption:='Venda Inicial';
          edtgrupo01.EditMask:=MascaraData;

          Grupo02.Enabled:=true;
          LbGrupo02.caption:='Venda Final';
          edtgrupo02.EditMask:=MascaraData;

          Grupo06.Enabled:=True;
          LbGrupo06.caption:='Somente Abertos';
          ComboGrupo06.Items.clear;
          ComboGrupo06.Items.add('N�O');
          ComboGrupo06.Items.add('SIM');
          ComboGrupo06.ItemIndex:=0;

          Grupo07.Enabled:=True;
          LbGrupo07.caption:='Mostra Ocorr.Ch.Dev.';
          ComboGrupo07.Items.Clear;
          ComboGrupo07.Items.add('N�O');
          ComboGrupo07.Items.add('SIM');
          ComboGrupo07.ItemIndex:=0;
          edtgrupo07.Visible:=False;

         
          if (Pvendedor='')
          Then Begin
                    Grupo03.Enabled:=true;
                    LbGrupo03.caption:='Vendedor';
                    edtgrupo03.OnKeyDown:=Self.ComissaoVendedor.EdtVENDEDORKeyDown;
                    edtgrupo03.Color:=$005CADFE;
          End
          Else edtgrupo03.text:=pvendedor;

          showmodal;
          
          edtgrupo07.Visible:=True;

          if (tag=0)
          Then exit;

          if (ComboGrupo06.ItemIndex=1)
          Then psomenteaberto:='S'
          Else psomenteaberto:='N';

          if (ComboGrupo07.ItemIndex=1)
          Then IncluiDevolvidos:=True
          Else IncluiDevolvidos:=False;



          Pdata1:='';
          Pdata2:='';
          Pvendedor:='';

          Try
             if (trim(comebarra(edtgrupo01.text))<>'')
             and (trim(comebarra(edtgrupo02.text))<>'')
             Then Begin
                       strtodate(edtgrupo01.Text);
                       Pdata1:=edtgrupo01.Text;
                       strtodate(edtgrupo02.Text);
                       Pdata2:=edtgrupo02.Text;
             end;
          Except
                Messagedlg('Datas inv�lida',mterror,[mbok],0);
                exit;
          End;


          Try
             if (trim(comebarra(edtgrupo03.text))<>'')
             Then Begin
                       strtoint(edtgrupo03.Text);
                       Pvendedor:=edtgrupo03.text;
                       if (Self.ComissaoVendedor.VENDEDOR.LocalizaCodigo(Pvendedor)=False)
                       Then Begin
                                 Messagedlg('Vendedor n�o localizado',mterror,[mbok],0);
                                 exit;
                       End;
                       Self.ComissaoVendedor.VENDEDOR.TabelaparaObjeto;
             End;
          Except
                Messagedlg('Funcion�rio inv�lido',mterror,[mbok],0);
                exit;
          End;



     End;


     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select TabPedido.vendedor,TabPedido.data,tabvendedor.nome as NOMEVENDEDOR,');
          sql.add('TabPedido.Codigo as PEDIDO,');
          sql.add('TabPedido.cliente,TabCliente.Nome as NOMECLIENTE,');
          sql.add('tabpedido.valortotal,');
          sql.add('tabpedido.valordesconto,tabpedido.valoracrescimo,tabpedido.valorfinal,');
          sql.add('TabComissaovendedores.Pendencia,');
          sql.add('TabComissaovendedores.Comissao,');
          sql.add('TabComissaovendedores.Valor,');
          sql.add('TabComissaovendedores.ValorComissao,');
          sql.add('TabComissaovendedores.Codigo as CODIGOCOMISSAO,TabComissaovendedores.LotePagamento');
          //sql.add('TabComissao_adiant_funcfolha.codigo as COMISSAOADIANT,');
          //sql.add('TabFuncionariofolhaPagamento.codigo as FUNCFOLHA,');
          //sql.add('TabFuncionariofolhaPagamento.FolhaPagamento');
          sql.add(',TabComissaovendedores.ChequeDevolvido from TabComissaoVendedores');
          sql.add('join tabpedido on tabcomissaovendedores.pedido=tabpedido.codigo');
          sql.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
          sql.add('join tabvendedor on tabpedido.vendedor=tabvendedor.codigo');
          //sql.add('left join TabComissao_adiant_funcfolha on tabcomissaovendedores.codigo=TabComissao_adiant_funcfolha.comissaovendedor');
          //sql.add('left join TabFuncionariofolhaPagamento on TabComissao_adiant_funcfolha.funcionariofolhapagamento=tabfuncionariofolhapagamento.codigo');
          sql.add('where TabComissaoVendedores.codigo<>-500');

          if (incluidevolvidos=False)
          Then sql.add('and TabComissaoVendedores.ChequeDevolvido is null');//nao pode entrar as comissoes do Cheque Devolvido a pedido da Genice


          if (Pdata1<>'')
          Then Sql.add('and Tabpedido.data>='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata1))+#39+' and Tabpedido.data<='+#39+formatdatetime('mm/dd/yyyy',strtodate(pdata2))+#39);

          if (Pvendedor<>'')
          Then sql.add('and TabPedido.Vendedor='+Pvendedor);


          if (PsomenteAberto='S')
          Then sql.add('and TabComissaoVendedores.LotePagamento is null');//nova forma de marcar como pago

          //sql.add('and TabComissao_adiant_funcfolha.funcionariofolhapagamento is null');

          sql.add('order by tabpedido.vendedor,tabpedido.codigo');
          

          open;
          If (Recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma Informa��o foi Selecionada',mtinformation,[mbok],0);
                    exit;
          end;

          
          with FreltxtRDPRINT do
          Begin
              ConfiguraImpressao;
              LinhaLocal:=3;
              RDprint.FonteTamanhoPadrao:=s17cpp;
              RDprint.TamanhoQteColunas:=130;
              RDprint.Abrir;
              if (RDprint.Setup=False)
              Then begin
                        RDprint.Fechar;
                        exit;
              end;
              RDprint.ImpC(LinhaLocal,65,'RELAT�RIO DE COMISS�O DE VENDEDOR',[negrito]);
              IncrementaLinha(2);

              if (Pdata1<>'')
              Then Begin
                        RDprint.ImpF(linhalocal,1,'DATAS '+Pdata1+' a '+Pdata2,[negrito]);
                        IncrementaLinha(1);
              end;

              if (Pvendedor<>'')
              Then Begin
                        RDprint.ImpF(linhalocal,1,'VENDEDOR '+PVENDEDOR+'-'+Self.ComissaoVendedor.VENDEDOR.Get_Nome,[negrito]);
                        IncrementaLinha(1);
              End;

              IncrementaLinha(1);

              temp:=CompletaPalavra('PEDIDO',6,' ')+' '+
                                   CompletaPalavra('DATA',10,' ')+' '+
                                   CompletaPalavra('CLIENTE',30,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VALOR',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('DESCONTO',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('ACRESCIMO',05,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VAL. FINAL',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('PEND.',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('VL.COMISSAO',10,' ')+' '+
                                   CompletaPalavra_a_Esquerda('LOTE PG',10,' ');

              if (incluidevolvidos)
              Then temp:=temp+'CHDEV';


              RDprint.ImpF(LinhaLocal,1,temp,[negrito]);



              IncrementaLinha(1);

              DesenhaLinha;
              IncrementaLinha(1);



              //Pedido		Cliente		Valor 		Desconto	Valor Final	Pend�ncia	Valor Comiss�o
              Pvendedor:=Fieldbyname('vendedor').asstring;
              SomaComissaovendedor:=0;
              Somavendasvendedor:=0;
              SomaComissaoTotal:=0;
              SomaVendasTotal:=0;


              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
              IncrementaLinha(1);

              Ppedido:='';
              
              While not(Self.Objquery.eof) do
              Begin
                   if (Pvendedor<>Fieldbyname('vendedor').asstring)
                   Then Begin
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,'SOMA DAS VENDAS              '+CompletaPalavra_a_Esquerda(formata_Valor(somavendasvendedor),12,' '),[negrito]);
                             IncrementaLinha(1);
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
                             IncrementaLinha(2);

                             Pvendedor:=Fieldbyname('vendedor').asstring;
                             SomaComissaovendedor:=0;
                             SomaVendasVendedor:=0;

                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
                             IncrementaLinha(1);
                   End;

                   if (Ppedido<>Fieldbyname('pedido').asstring)
                   Then Begin
                             Ppedido:=Fieldbyname('pedido').asstring;
                             somavendasvendedor:=Somavendasvendedor+fieldbyname('valorfinal').asfloat;
                             SomaVendasTotal:=SomaVendasTotal+fieldbyname('valorfinal').asfloat;

                             VerificaLinha;
                             RDprint.Imp(LinhaLocal,1,CompletaPalavra(fieldbyname('PEDIDO').AsString,6,' ')+' '+
                                                      CompletaPalavra(fieldbyname('data').AsString,10,' ')+' '+
                                                      CompletaPalavra(fieldbyname('CLIENTE').AsString+'-'+fieldbyname('nomeCLIENTE').AsString,30,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORtotal').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorDESCONTO').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valoracrescimo').AsString),5,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORFINAL').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('PENDencia').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorCOMISSAO').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('lotepagamento').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('chequedevolvido').AsString,5,' '));
                             IncrementaLinha(1);
                   End
                   Else Begin
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,1,CompletaPalavra(fieldbyname('PEDIDO').AsString,6,' ')+' '+
                                                      CompletaPalavra(fieldbyname('data').AsString,10,' ')+' '+
                                                      CompletaPalavra(fieldbyname('CLIENTE').AsString+'-'+fieldbyname('nomeCLIENTE').AsString,30,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',5,' ')+' '+
                                                      CompletaPalavra_a_Esquerda('',10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('PENDencia').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorCOMISSAO').AsString),10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('lotepagamento').AsString,10,' ')+' '+
                                                      CompletaPalavra_a_Esquerda(fieldbyname('chequedevolvido').AsString,5,' '),[italico]);
                             IncrementaLinha(1);

                   End;
                   somacomissaovendedor:=Somacomissaovendedor+fieldbyname('valorcomissao').asfloat;
                   SomaComissaototal:=SomaComissaoTotal+fieldbyname('valorcomissao').asfloat;
                   Self.Objquery.Next;
              End;
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DAS VENDAS              '+CompletaPalavra_a_Esquerda(formata_Valor(somavendasvendedor),12,' '),[negrito]);
              IncrementaLinha(1);
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
              IncrementaLinha(1);

              VerificaLinha;
              RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',130,'_'));
              IncrementaLinha(1);

              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DAS COMISS�ES GERAL '+CompletaPalavra_a_Esquerda(formata_Valor(SomaComissaoTotal),12,' '),[negrito]);
              IncrementaLinha(1);
              VerificaLinha;
              RDprint.Impf(LinhaLocal,1,'SOMA DAS VENDAS    GERAL '+CompletaPalavra_a_Esquerda(formata_Valor(SomaVendasTotal),12,' '),[negrito]);
              IncrementaLinha(1);
              RDprint.Fechar;
          End;//with Freltxt
     end;//objquery
end;

function TObjCOMISSAO_ADIANT_FUNCFOLHA.AcertaInss_IRPF_FGTS_SalarioFamilia(Pfolha: string): boolean;
var
PSalarioFerias,PvalorFGTS_Ferias,PValorINSS_ferias,PporcentoFGTS,PvalorFGTS,PpercentualIRPF,PDeducaoDependente,PvalorSalarioFamilia,PhoraExtra,
PvalorDeducao,Psalario,Psalariobruto,PvalorInss,PvalorIRPF,PComissaoVenda,PComissaoColocador,ptemp,PvalorMaximonoInss,
PaliquotaValorMaximoInss:Currency;

PNumeroDependentesSalFamilia,PNumeroDependentesIRPF:INteger;
ObjFaixaDescontoINSS:tobjFaixaDescontoINSS;
ObjFaixaDescontoIRPF:tobjFaixaDescontoIRPF;
ObjSalarioFamilia:TobjSalarioFamilia;

temp:string;

begin
     result:=False;

     //************************************************************************
     temp:='VALOR E PORCENTAGEM DO INSS M�XIMO';
     
     if (ObjParametroGlobal.ValidaParametro(temp)=False)
     Then exit;

      If (ExplodeStr(ObjParametroGlobal.get_valor,Self.parametropesquisa,'/','FLOAT')=False)
      Then exit;

      if (Self.ParametroPesquisa.Count<2)
      Then Begin
                Messagedlg('O par�metro "'+temp+'" deve conter o valor m�ximo ; a porcentagem atribu�da',mterror,[mbok],0);
                exit;
      End;

      Try
         Strtofloat(Self.ParametroPesquisa[0]);//valor
      Except
            Messagedlg('Erro no valor do par�metro "'+temp+'". Exemplo de como deveria ser : 2800,56/11 isso indicaria um sal�rio superior a R$ 2800,56 com 11% de Aliquota',mterror,[mbok],0);
            exit;
      End;

      Try
         Strtofloat(Self.ParametroPesquisa[1]);//porcentagem
      Except
            Messagedlg('Erro na porcentagem no valor do par�metro "'+temp+'". Exemplo de como deveria ser : 2800,56/11 isso indicaria um sal�rio superior a R$ 2800,56 com 11% de Aliquota',mterror,[mbok],0);
            exit;
      End;

      PvalorMaximonoInss:=Strtofloat(Self.ParametroPesquisa[0]);
      PaliquotaValorMaximoInss:=Strtofloat(Self.ParametroPesquisa[1]);

      if (PaliquotaValorMaximoInss>=100)
      Then Begin
                Messagedlg('A porcentagem do INSS no valor m�ximo n�o pode ser superior a 100%. Par�metro "'+temp+'"',mterror,[mbok],0);
                exit;
      End;                              
      
      //***********************************************************************
      temp:='PORCENTAGEM DO FGTS';
      if (ObjParametroGlobal.ValidaParametro(temp)=False)
      Then exit;

      Try
         PporcentoFGTS:=Strtofloat(ObjParametroGlobal.get_valor);
         if (PporcentoFGTS>=100)
         then BEgin
                   Messagedlg('A porcentagem do FGTS n�o pode ser superior a 100%. Par�metro "'+temp+'"',mterror,[mbok],0);
                   exit;
         End;
      Except
            Messagedlg('Valor inv�lido par�metro "'+temp+'"',mterror,[mbok],0);
            exit;
      End;
     //*************************************************************************
     temp:='VALOR DEDU��O POR DEPENDENTE NO IRPF';
     if (ObjParametroGlobal.ValidaParametro(temp)=False)
     Then exit;

      Try
         PDeducaoDependente:=Strtofloat(ObjParametroGlobal.get_valor);
      Except
            Messagedlg('Valor inv�lido par�metro "'+temp+'"',mterror,[mbok],0);
            exit;
      End;
     //*************************************************************************

     Try
         ObjFaixaDescontoINSS:=tobjFaixaDescontoINSS.create;
         ObjFaixaDescontoIRPF:=tobjFaixaDescontoIRPF.create;
         ObjSalarioFamilia:=TobjSalarioFamilia.create;
     Except
           Messagedlg('Erro na tentativa de Criar os Objetos de Desconto de INSS e IRPF',mterror,[mbok],0);
           exit;
     End;

Try

     with Self.ObjqueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select codigo,Funcionario from TabFuncionarioFolhaPagamento where FolhaPagamento='+Pfolha);
          open;
          last;
          FMostraBarraProgresso.Lbmensagem.caption:='CALCULANDO INSS E IRPF (FUNCION�RIOS)';
          FMostraBarraProgresso.BarradeProgresso.MaxValue:=RECORDCOUNT;
          FMostraBarraProgresso.BarradeProgresso.Progress:=0;
          FMostraBarraProgresso.Show;
          first;
          
          if (Recordcount=0)
          Then exit;

          While not(eof) do
          Begin//esse while percorre os funcionarios da folha
          
              application.ProcessMessages;//atualiza a tela
              
              FMostraBarraProgresso.BarradeProgresso.Progress:=FMostraBarraProgresso.BarradeProgresso.Progress+1;
              FMostraBarraProgresso.show;
              FMostraBarraProgresso.repaint;

              Self.FuncionarioFolhaPagamento.ZerarTabela;
              Self.FuncionarioFolhaPagamento.LocalizaCodigo(fieldbyname('codigo').AsString);
              Self.FuncionarioFolhaPagamento.TabelaparaObjeto;

              Try
                  Psalariobruto:=Strtofloat(Self.FuncionarioFolhaPagamento.Get_SalarioBruto);
              Except
                  Psalariobruto:=0;
              End;

              Try
                PNumeroDependentesSalFamilia:=Strtoint(Self.FuncionarioFolhaPagamento.Funcionario.Get_NumeroDependentesSalFamilia);
              Except
                    PNumeroDependentesSalFamilia:=0;
              End;

              Try
                PNumeroDependentesIRPF:=Strtoint(Self.FuncionarioFolhaPagamento.Funcionario.Get_NumeroDependentesIRPF);
              Except
                    PNumeroDependentesIRPF:=0;
              End;


              Try
                 PComissaoVenda:=Strtofloat(Self.FuncionarioFolhaPagamento.Get_ComissaoVenda);
              Except
                 PComissaoVenda:=0;
              End;

              Try
                  Pcomissaocolocador:=Strtofloat(Self.FuncionarioFolhaPagamento.Get_Comissaocolocador);
              Except
                    PComissaoColocador:=0;
              End;

              Try
                 PHoraExtra:=strtofloat(Self.FuncionarioFolhaPagamento.Get_AcrescimoHoraExtra);
              Except
                 PHoraExtra:=0;
              End;

              Try
                 Psalario:=strtofloat(Self.FuncionarioFolhaPagamento.Get_Salario);
              Except
                 Psalario:=0;
              End;

              {O salario bruto � a Soma de Tudo que � Cr�dito
              Salario+Comissao+HoraExtra+Descanso Semanal}
              //Psalariobruto:=Psalario+PComissaoVenda+PComissaoColocador+PhoraExtra;
              //O Salario Bruto � um Campo Calculado na TabFuncionarioFolhaPagamento

              //CAMPOS DE 13o e FERIAS PROPORCIONAL
              Psalarioferias:=(PSalarioBruto+(PSalariobruto/3));

              //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

              PvalorInss:=0;
              PvalorIRPF:=0;
              PValorINSS_ferias:=0;
              PvalorFGTS_Ferias:=0;

              //* * * * *calculando o valor do inss do Salario Normal* * * * * * * * * * * * * *

              if (Psalariobruto<PvalorMaximonoInss)
              Then Begin
                      if (ObjFaixaDescontoINSS.LocalizaValor(floattostr(Psalariobruto))=True)
                      Then Begin
                                ObjFaixaDescontoINSS.TabelaparaObjeto;
                                Try
                                  PvalorInss:=((strtofloat(ObjFaixaDescontoINSS.Get_Percentual)*Psalariobruto)/100);
                                Except
                                  PvalorInss:=0;
                                End;
                      End;
              End
              Else Begin
                        //Acima de um determinado valor o inss � calculado sobre um teto
                        //conforme a camila
                        //o teto � R$ 2801,57 e aliquota 11% .
                        //Assim o valor do inss seria R$ 308,1727
                        PvalorInss:=(PvalorMaximonoInss*PaliquotaValorMaximoInss)/100;
              End;

              //* * * * *calculando o valor do inss do Salario de Ferias* * * * * * * * * * * * * *
              if (PSalarioFerias<PvalorMaximonoInss)
              Then Begin
                      if (ObjFaixaDescontoINSS.LocalizaValor(floattostr(PSalarioFerias))=True)
                      Then Begin
                                ObjFaixaDescontoINSS.TabelaparaObjeto;
                                Try
                                  PvalorInss_ferias:=((strtofloat(ObjFaixaDescontoINSS.Get_Percentual)*PSalarioFerias)/100);
                                Except
                                  PvalorInss_ferias:=0;
                                End;
                      End;
              End
              Else Begin
                        //Acima de um determinado valor o inss � calculado sobre um teto
                        //conforme a camila
                        //o teto � R$ 2801,57 e aliquota 11% .
                        //Assim o valor do inss seria R$ 308,1727
                        PvalorInss_ferias:=(PvalorMaximonoInss*PaliquotaValorMaximoInss)/100;
              End;
              //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  *

              //CALCULANDO O FGTS
              PvalorFGTS:=(Psalariobruto*PporcentoFGTS)/100;
              //Calculando o FGTS das Ferias
              PvalorFGTS_ferias:=(PSalarioFerias*PporcentoFGTS)/100;





              
              //* * * * *calculando o valor do Salario Familia * * * * * * * * *
              PvalorSalarioFamilia:=0;
              if (ObjSalarioFamilia.LocalizaValor(Floattostr(PSalarioBruto))=True)
              Then Begin
                        ObjSalarioFamilia.TabelaparaObjeto;
                        Try
                            PvalorSalarioFamilia:=Strtofloat(ObjSalarioFamilia.Get_ValorporDependente)*PNumeroDependentesSalFamilia;
                        Except
                            PvalorSalarioFamilia:=0;
                        End;
              End;
              //* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *  *
              {
               Faixa do Imposto de renda tem uma deducao em Reais de acordo com a faixa
               Exemplo
               Faixa de Salario	Aliq		Valordeducao em R$
               0        1257    0,00%    R$ X
               1257     2512   15,00%    R$ X
               Acima    2512   27,50% 	 R$ Y             

               A deducao por dependete   =(R$ VALORDEDUCAOpordepenten * NumeroDependentes)
               Valor do imposto de renda= ((Salario - INSS -DeducaoDependentes)*aliquota)-ValorDeducao

               A deducao por dependente � fixa
              }

              PvalorIRPF:=0;


              //Para Localizar a faixa do IRPF uso o seguinte valor
              //(Psalariobruto-PvalorInss-(PDeducaoDependente*PNumeroDependentes))

              Ptemp:=Psalariobruto-PvalorInss-(PDeducaoDependente*PNumeroDependentesIRPF);
                   
              if (ObjFaixaDescontoIRPF.LocalizaValor(floattostr(ptemp))=True)
              Then Begin
                        ObjFaixaDescontoIRPF.TabelaparaObjeto;

                        PvalorIRPF:=0;

                        Try
                           PpercentualIRPF:=Strtofloat(ObjFaixaDescontoIRPF.Get_Percentual);
                        Except
                           PpercentualIRPF:=0;
                        End;

                        if (PpercentualIRPF>0)
                        Then Begin
                                Try
                                   PvalorDeducao:=strtofloat(ObjFaixaDescontoIRPF.Get_ValorDeducao);
                                Except
                                      PvalorDeducao:=0;
                                End;

                                Try
                                  PvalorIRPF:=(((Psalariobruto-PvalorInss-(PDeducaoDependente*PNumeroDependentesIRPF))*PpercentualIRPF)/100)-PvalorDeducao;
                                Except
                                  PvalorIRPF:=0;
                                End;

                                if (PvalorIRPF<0)
                                Then Begin
                                          application.ProcessMessages;//atualiza a tela

                                          Messagedlg('O valor do imposto de renda est� negativo! Func: '+Self.FuncionarioFolhaPagamento.Funcionario.Get_CODIGO+' Salario Bruto: '+Self.FuncionarioFolhaPagamento.Get_Salario+
                                          ' Comiss�o :'+formata_valor(PComissaoVenda+PComissaoColocador)+' Adiantamento:'+Self.FuncionarioFolhaPagamento.Get_TotalAdiantamento+#13+
                                          ' Inss:'+floattostr(PvalorInss)+' Sal Familia:'+floattostr(PvalorSalarioFamilia)+' Hora Extra'+floattostr(PhoraExtra)+' %IRPF:'+floattostr(ppercentualirpf),mterror,[mbok],0);
                                          exit;
                                End;
                        End;

              End;

              Self.FuncionarioFolhaPagamento.Status:=dsedit;

              if (FuncionarioFolhaPagamento.Funcionario.Get_CarteiraTrabalho<>'')
              Then Begin
                        {
                         Somente funcion�rios que possuem carteira de trabalho preenchidas
                         que calcula-se os impostos
                        }
                        Self.FuncionarioFolhaPagamento.Submit_DescontoInss(floattostr(PvalorInss));
                        Self.FuncionarioFolhaPagamento.Submit_AcrescimoSalarioFamilia(floattostr(PvalorSalarioFamilia));
                        Self.FuncionarioFolhaPagamento.Submit_DescontoIRPF(floattostr(PvalorIrpf));
                        Self.FuncionarioFolhaPagamento.Submit_ValorFgts(floattostr(pvalorfgts));
                        //******************************************************
                        Self.FuncionarioFolhaPagamento.Submit_SalarioFeriasProporcional(FloatToStr(PSalarioFerias/12));
                        Self.FuncionarioFolhaPagamento.Submit_Salario13Proporcional(floattostr(Psalariobruto/12));
                        //O Inss e FGTS � o mesmo do mensal, s� que dividido por 12
                        Self.FuncionarioFolhaPagamento.Submit_FGTS13Proporcional(floattostr(pvalorfgts/12));
                        Self.FuncionarioFolhaPagamento.Submit_INSS13Proporcional(floattostr(PvalorInss/12));
                        //O Inss e o FGTS das F�rias � diferente pois o Sal.Bruto acresce 50%
                        Self.FuncionarioFolhaPagamento.Submit_FGTSFeriasProporcional(floattostr(PvalorFGTS_Ferias/12));
                        Self.FuncionarioFolhaPagamento.Submit_INSSFeriasProporcional(floattostr(PValorINSS_ferias/12));

              End
              Else Begin
                        Self.FuncionarioFolhaPagamento.Submit_DescontoInss('0');
                        Self.FuncionarioFolhaPagamento.Submit_AcrescimoSalarioFamilia('0');
                        Self.FuncionarioFolhaPagamento.Submit_DescontoIRPF('0');
                        Self.FuncionarioFolhaPagamento.Submit_ValorFgts('0');
                        //******************************************************
                        Self.FuncionarioFolhaPagamento.Submit_SalarioFeriasProporcional(FloatToStr(PSalarioFerias/12));
                        Self.FuncionarioFolhaPagamento.Submit_Salario13Proporcional(floattostr(Psalariobruto/12));
                        //O Inss e FGTS � o mesmo do mensal, s� que dividido por 12
                        Self.FuncionarioFolhaPagamento.Submit_FGTS13Proporcional('0');
                        Self.FuncionarioFolhaPagamento.Submit_INSS13Proporcional('0');
                        //O Inss e o FGTS das F�rias � diferente pois o Sal.Bruto acresce 50%
                        Self.FuncionarioFolhaPagamento.Submit_FGTSFeriasProporcional('0');
                        Self.FuncionarioFolhaPagamento.Submit_INSSFeriasProporcional('0');
              End;

              if (Self.FuncionarioFolhaPagamento.Salvar(False)=False)
              Then Begin
                        Messagedlg('Erro na tentativa de salvar o INSS e IRPF do Funcion�rio '+Self.FuncionarioFolhaPagamento.Funcionario.Get_Nome,mterror,[mbok],0);
                        exit;
              End;

              next;
          End;//while
          result:=true;
     End;//with

Finally
       ObjFaixaDescontoINSS.Free;
       ObjFaixaDescontoIRPF.Free;
       ObjSalarioFamilia.Free;
End;

end;

procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.RelatoriosComissao;
begin
     With FMenuRelatorios do
     Begin
           //era no titulo antes
          NomeObjeto:='UCOMISSAOADIANT_RELCOMISSAO';
          
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Comiss�o Gerada por Pedidos - Ligado a Folha de Pagamento');
          RgOpcoes.Items.add('Previs�o de Comiss�o do Per�odo - Ligado a Folha de Pagamento');

          RgOpcoes.Items.add('Comiss�o Gerada por Pedidos - Ligado a Lote de Comiss�o');
          RgOpcoes.Items.add('Previs�o de Comiss�o do Per�odo - Ligado a Lote de Comiss�o');



          showmodal;

          if (Tag=0)
          Then exit;

          case RgOpcoes.ItemIndex of
            0:Self.ImprimeComissaovendedor;
            1:Self.ImprimePrevisaoComissao;
            2:Self.ImprimeComissaovendedor_LoteComissao;
            3:Self.ImprimePrevisaoComissao_LOTECOMISSAO;
          End;


     End;

end;

procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.ImprimePrevisaoComissao;
var
Pvendedor:string;
SomaComissaovendedor,SomaComissaoTotal:Currency;
begin

     pvendedor:='';
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('RELAT�RIO DE PREVIS�O DE COMISS�O')=False)
     then Begin
               //nao tem permissao, entao pe�o senha para escolher o vendedor
               //com senha

               if (Self.ComissaoVendedor.VENDEDOR.PegaVendedor(Pvendedor)=False)
               Then exit;
     End;




     With FfiltroImp do
     begin
          DesativaGrupos;
          if (Pvendedor='')
          Then Begin
                    Grupo01.Enabled:=True;
                    LbGrupo01.caption:='Vendedor';
                    edtgrupo01.OnKeyDown:=Self.ComissaoVendedor.EdtVENDEDORKeyDown;
                    edtgrupo01.Color:=$005CADFE;

                    Showmodal;

                    if (tag=0)
                    then exit;
          End
          Else edtgrupo01.text:=pvendedor;

          Pvendedor:='';
          Try
             if (edtgrupo01.text<>'')
             Then Begin
                       Strtoint(edtgrupo01.text);
                       Pvendedor:=edtgrupo01.text;
             End;
          Except
                Messagedlg('Vendedor Inv�lido',mterror,[mbok],0);
                exit;
          End;
     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('tabvendedor.codigo as VENDEDOR,');
          sql.add('tabvendedor.nome as NOMEVENDEDOR,');
          sql.add('TabComissaoVendedores.pedido,');
          sql.add('tabpedido.Data as DATAPedido,');
          sql.add('tabpedido.cliente,');
          sql.add('tabCliente.Nome as NomeCliente,');
          sql.add('tabpedido.valortotal,');
          sql.add('tabpedido.valordesconto,tabpedido.valoracrescimo,tabpedido.valorfinal,');
          sql.add('TabComissaovendedores.Pendencia,');
          sql.add('ProcLancamentoPendencia.Pvencimento as VENCIMENTOPENDENCIA,');
          sql.add('TabComissaovendedores.Comissao,');
          sql.add('TabComissaovendedores.Valor,');
          sql.add('TabComissaovendedores.ValorComissao,');
          sql.add('TabComissaovendedores.Codigo as CODIGOCOMISSAO,TabComissaovendedores.CHEQUEDEVOLVIDO');
          sql.add('from TabComissaoVendedores');
          sql.add('left join tabComissao_Adiant_funcFolha TCAFF on TCAFF.Comissaovendedor=TabComissaoVendedores.codigo');
          sql.add('join TabVendedor on TabComissaoVendedores.Vendedor=tabvendedor.codigo');
          sql.add('join TabFuncionarios on Tabvendedor.codigofuncionario=tabfuncionarios.codigo');
          sql.add('join ProcLancamentoPendencia on  TabComissaovendedores.pendencia=ProcLancamentoPendencia.ppendencia');
          sql.add('join tabpedido on TabComissaoVendedores.Pedido=Tabpedido.codigo');
          sql.add('join tabcliente on Tabpedido.cliente=tabcliente.codigo');
          sql.add('where TCAFF.codigo is null');

          //sql.savetofile('02.sql');
          
          //sql.add('and ProcLancamentoPendencia.pvencimento<='+#39+FormatDateTime('mm/dd/yyyy',pdatalimite)+#39);

          if (Pvendedor<>'')
          Then sql.add('and TabVendedor.codigo='+pvendedor);
          
          sql.add('and ProcLancamentoPendencia.pSaldo=0 and ProcLancamentoPendencia.PsomaQuitacao>0');
          sql.add('order by ProcLancamentoPendencia.pVencimento');
          open;

          if (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mtinformation,[mbok],0);
                    exit;
          End;
          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=s17cpp;
               RDprint.TamanhoQteColunas:=130;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.Fechar;
                         exit;
               End;

               Try//do rdprint.fechar
                  linhalocal:=3;
                  RDprint.ImpC(linhalocal,65,'PREVIS�O DE COMISS�O A SER PAGA',[negrito]);
                  IncrementaLinha(2);
                  RDprint.ImpF(LinhaLocal,1,CompletaPalavra('PEDIDO',6,' ')+' '+
                                       CompletaPalavra('DATA',10,' ')+' '+
                                       CompletaPalavra('CLIENTE',30,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VALOR',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('DESCONTO',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('ACRESCIMO',8,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VAL. FINAL',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('PEND�NCIA',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VCTO.PEND',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VL.COMISSAO',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('CHDVL',5,' '),[negrito]);
                  IncrementaLinha(1);
                  DesenhaLinha;
                  IncrementaLinha(1);
                  Pvendedor:=Fieldbyname('vendedor').asstring;
                  SomaComissaovendedor:=0;
                  SomaComissaoTotal:=0;
                  VerificaLinha;
                  RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
                  IncrementaLinha(1);
                  While not(Self.Objquery.eof) do
                  Begin
                       if (Pvendedor<>Fieldbyname('vendedor').asstring)
                       Then Begin
                                 VerificaLinha;
                                 RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
                                 IncrementaLinha(2);
                  
                                 Pvendedor:=Fieldbyname('vendedor').asstring;
                                 SomaComissaovendedor:=0;
                  
                                 VerificaLinha;
                                 RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
                                 IncrementaLinha(1);
                       End;

                       VerificaLinha;
                       RDprint.Imp(LinhaLocal,1,CompletaPalavra(fieldbyname('PEDIDO').AsString,6,' ')+' '+
                                                CompletaPalavra(fieldbyname('datapedido').AsString,10,' ')+' '+
                                                CompletaPalavra(fieldbyname('CLIENTE').AsString+'-'+fieldbyname('nomeCLIENTE').AsString,30,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORtotal').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorDESCONTO').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valoracrescimo').AsString),8,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORFINAL').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(fieldbyname('PENDencia').AsString,10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(fieldbyname('vencimentoPENDencia').AsString,10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorCOMISSAO').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(fieldbyname('chequedevolvido').AsString,5,' '));
                       IncrementaLinha(1);
                       somacomissaovendedor:=Somacomissaovendedor+fieldbyname('valorcomissao').asfloat;
                       SomaComissaototal:=SomaComissaoTotal+fieldbyname('valorcomissao').asfloat;
                       Self.Objquery.Next;
                  End;
                  VerificaLinha;
                  RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
                  IncrementaLinha(1);
                  
                  VerificaLinha;
                  RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',130,'_'));
                  IncrementaLinha(1);
                  
                  VerificaLinha;
                  RDprint.Impf(LinhaLocal,1,'SOMA DAS COMISS�ES GERAL '+CompletaPalavra_a_Esquerda(formata_Valor(SomaComissaoTotal),12,' '),[negrito]);
                  IncrementaLinha(1);
               Finally
                  Rdprint.Fechar;
               End;
          End;
     End;
end;


procedure TObjCOMISSAO_ADIANT_FUNCFOLHA.ImprimePrevisaoComissao_LOTECOMISSAO;
var
Pvendedor:string;
SomaComissaovendedor,SomaComissaoTotal:Currency;
begin

     pvendedor:='';
     if (ObjPermissoesUsoGlobal.ValidaPermissao_Silenciosa('RELAT�RIO DE PREVIS�O DE COMISS�O')=False)
     then Begin
               //nao tem permissao, entao pe�o senha para escolher o vendedor
               //com senha

               if (Self.ComissaoVendedor.VENDEDOR.PegaVendedor(Pvendedor)=False)
               Then exit;
     End;                      


     With FfiltroImp do
     begin
          DesativaGrupos;
          if (Pvendedor='')
          Then Begin
                    Grupo01.Enabled:=True;
                    LbGrupo01.caption:='Vendedor';
                    edtgrupo01.OnKeyDown:=Self.ComissaoVendedor.EdtVENDEDORKeyDown;
                    edtgrupo01.Color:=$005CADFE;

                    Showmodal;

                    if (tag=0)
                    then exit;
          End
          Else edtgrupo01.text:=pvendedor;

          Pvendedor:='';
          Try
             if (edtgrupo01.text<>'')
             Then Begin
                       Strtoint(edtgrupo01.text);
                       Pvendedor:=edtgrupo01.text;
             End;
          Except
                Messagedlg('Vendedor Inv�lido',mterror,[mbok],0);
                exit;
          End;
     End;

     With Self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select');
          sql.add('tabvendedor.codigo as VENDEDOR,');
          sql.add('tabvendedor.nome as NOMEVENDEDOR,');
          sql.add('TabComissaoVendedores.pedido,');
          sql.add('tabpedido.Data as DATAPedido,');
          sql.add('tabpedido.cliente,');
          sql.add('tabCliente.Nome as NomeCliente,');
          sql.add('tabpedido.valortotal,');
          sql.add('tabpedido.valordesconto,tabpedido.valoracrescimo,tabpedido.valorfinal,');
          sql.add('TabComissaovendedores.Pendencia,');
          sql.add('ProcLancamentoPendencia.Pvencimento as VENCIMENTOPENDENCIA,');
          sql.add('TabComissaovendedores.Comissao,');
          sql.add('TabComissaovendedores.Valor,');
          sql.add('TabComissaovendedores.ValorComissao,');
          sql.add('TabComissaovendedores.Codigo as CODIGOCOMISSAO,TabComissaovendedores.CHEQUEDEVOLVIDO');
          sql.add('from TabComissaoVendedores');
          sql.add('join TabVendedor on TabComissaoVendedores.Vendedor=tabvendedor.codigo');
          sql.add('join TabFuncionarios on Tabvendedor.codigofuncionario=tabfuncionarios.codigo');
          sql.add('join ProcLancamentoPendencia on  TabComissaovendedores.pendencia=ProcLancamentoPendencia.ppendencia');
          sql.add('join tabpedido on TabComissaoVendedores.Pedido=Tabpedido.codigo');
          sql.add('join tabcliente on Tabpedido.cliente=tabcliente.codigo');
          sql.add('where TabComissaoVendedores.LotePagamento is null');

          if (Pvendedor<>'')
          Then sql.add('and TabVendedor.codigo='+pvendedor);
          
          sql.add('and ProcLancamentoPendencia.pSaldo=0 and ProcLancamentoPendencia.PsomaQuitacao>0');
          sql.add('order by ProcLancamentoPendencia.pVencimento');
          open;

          if (recordcount=0)
          Then Begin
                    Messagedlg('Nenhuma informa��o foi selecionada',mtinformation,[mbok],0);
                    exit;
          End;
          With FreltxtRDPRINT do
          Begin
               ConfiguraImpressao;
               RDprint.FonteTamanhoPadrao:=s17cpp;
               RDprint.TamanhoQteColunas:=130;
               RDprint.Abrir;
               if (RDprint.Setup=False)
               Then Begin
                         RDprint.Fechar;
                         exit;
               End;

               Try//do rdprint.fechar
                  linhalocal:=3;
                  RDprint.ImpC(linhalocal,65,'PREVIS�O DE COMISS�O A SER PAGA',[negrito]);
                  IncrementaLinha(2);
                  RDprint.ImpF(LinhaLocal,1,CompletaPalavra('PEDIDO',6,' ')+' '+
                                       CompletaPalavra('DATA',10,' ')+' '+
                                       CompletaPalavra('CLIENTE',30,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VALOR',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('DESCONTO',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('ACRESCIMO',8,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VAL. FINAL',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('PEND�NCIA',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VCTO.PEND',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VL.COMISSAO',10,' ')+' '+
                                       CompletaPalavra_a_Esquerda('CHDVL',5,' '),[negrito]);
                  IncrementaLinha(1);
                  DesenhaLinha;
                  IncrementaLinha(1);
                  Pvendedor:=Fieldbyname('vendedor').asstring;
                  SomaComissaovendedor:=0;
                  SomaComissaoTotal:=0;
                  VerificaLinha;
                  RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
                  IncrementaLinha(1);
                  While not(Self.Objquery.eof) do
                  Begin
                       if (Pvendedor<>Fieldbyname('vendedor').asstring)
                       Then Begin
                                 VerificaLinha;
                                 RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
                                 IncrementaLinha(2);
                  
                                 Pvendedor:=Fieldbyname('vendedor').asstring;
                                 SomaComissaovendedor:=0;
                  
                                 VerificaLinha;
                                 RDprint.Impf(LinhaLocal,1,'VENDEDOR: '+Fieldbyname('vendedor').asstring+'-'+fieldbyname('nomevendedor').asstring,[negrito]);
                                 IncrementaLinha(1);
                       End;

                       VerificaLinha;
                       RDprint.Imp(LinhaLocal,1,CompletaPalavra(fieldbyname('PEDIDO').AsString,6,' ')+' '+
                                                CompletaPalavra(fieldbyname('datapedido').AsString,10,' ')+' '+
                                                CompletaPalavra(fieldbyname('CLIENTE').AsString+'-'+fieldbyname('nomeCLIENTE').AsString,30,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORtotal').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valorDESCONTO').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('valoracrescimo').AsString),8,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('VALORFINAL').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(fieldbyname('PENDencia').AsString,10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(fieldbyname('vencimentoPENDencia').AsString,10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('ValorCOMISSAO').AsString),10,' ')+' '+
                                                CompletaPalavra_a_Esquerda(fieldbyname('chequedevolvido').AsString,5,' '));
                       IncrementaLinha(1);
                       somacomissaovendedor:=Somacomissaovendedor+fieldbyname('valorcomissao').asfloat;
                       SomaComissaototal:=SomaComissaoTotal+fieldbyname('valorcomissao').asfloat;
                       Self.Objquery.Next;
                  End;
                  VerificaLinha;
                  RDprint.Impf(LinhaLocal,1,'SOMA DA COMISS�O DO VENDEDOR '+CompletaPalavra_a_Esquerda(formata_Valor(somacomissaovendedor),12,' '),[negrito]);
                  IncrementaLinha(1);
                  
                  VerificaLinha;
                  RDprint.Imp(LinhaLocal,1,CompletaPalavra('_',130,'_'));
                  IncrementaLinha(1);
                  
                  VerificaLinha;
                  RDprint.Impf(LinhaLocal,1,'SOMA DAS COMISS�ES GERAL '+CompletaPalavra_a_Esquerda(formata_Valor(SomaComissaoTotal),12,' '),[negrito]);
                  IncrementaLinha(1);
               Finally
                  Rdprint.Fechar;
               End;
          End;
     End;
end;

end.



