unit UobjFOLHAPAGAMENTO;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal,uobjtitulo;

Type
   TObjFOLHAPAGAMENTO=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                TituloTemp:tobjTitulo;
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;

                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Historico(parametro: string);
                Function Get_Historico: string;
                Procedure Submit_Data(parametro: string);
                Function Get_Data: string;
                Procedure Submit_DataComissaocolocador(parametro: string);
                Function Get_DataComissaocolocador: string;
                Procedure Submit_DataSalarioAdiantamento(parametro: string);
                Function Get_DataSalarioAdiantamento: string;
                Function Get_Processado:string;
                Procedure Submit_Processado(parametro:string);
                Function get_DataQuitacaoComissaoVendedor:string;
                Procedure Submit_DataQuitacaoComissaoVendedor(parametro:string);

                Function Get_TITULOFGTS:string;
                Procedure Submit_TITULOFGTS(parametro:string);
                Function Get_TITULOINSS_FUNCIONARIOS:string;
                Procedure Submit_TITULOINSS_FUNCIONARIOS(parametro:string);
                Function Get_TITULOINSS_EMPREGADOR:string;
                Procedure Submit_TITULOINSS_EMPREGADOR(parametro:string);

                Function Get_ObjetoTituloFgts:Boolean;
                Function Get_ObjetoTituloInss_Funcionarios:Boolean;
                Function Get_ObjetoTituloInss_Empregador:Boolean;


                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               Historico:string;
               Data:string;
               DataComissaocolocador:string;
               DataSalarioAdiantamento:string;
               DataQuitacaoComissaoVendedor:string;
               Processado:string;

               //Esse 3 campos s�o chav. estrangeira para a tabtitulo
               //porem para evitar ficar muito pesado criar os 3
               //objetos eu crio apenas um para quando precisar

               TITULOFGTS:string;
               TITULOINSS_FUNCIONARIOS:string;
               TITULOINSS_EMPREGADOR:string;


               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios;


{ TTabTitulo }


Function  TObjFOLHAPAGAMENTO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Historico:=fieldbyname('Historico').asstring;
        Self.Data:=fieldbyname('Data').asstring;
        Self.DataComissaocolocador:=fieldbyname('DataComissaocolocador').asstring;
        Self.DataSalarioAdiantamento:=fieldbyname('DataSalarioAdiantamento').asstring;
        Self.Processado:=FieldByname('processado').asstring;
        Self.DataQuitacaoComissaoVendedor:=FieldByname('DataQuitacaoComissaoVendedor').asstring;

        Self.TITULOFGTS               :=Fieldbyname('TITULOFGTS').asstring;
        Self.TITULOINSS_FUNCIONARIOS  :=Fieldbyname('TITULOINSS_FUNCIONARIOS').asstring;
        Self.TITULOINSS_EMPREGADOR    :=Fieldbyname('TITULOINSS_EMPREGADOR').asstring;

        result:=True;
     End;
end;


Procedure TObjFOLHAPAGAMENTO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Historico').asstring:=Self.Historico;
        ParamByName('Data').asstring:=Self.Data;
        ParamByName('DataComissaocolocador').asstring:=Self.DataComissaocolocador;
        ParamByName('DataSalarioAdiantamento').asstring:=Self.DataSalarioAdiantamento;
        ParamByName('processado').asstring:=Self.Processado;
        ParamByName('DataQuitacaoComissaoVendedor').asstring:=Self.DataQuitacaoComissaoVendedor;

        Parambyname('TITULOFGTS').asstring:=Self.TITULOFGTS;
        Parambyname('TITULOINSS_FUNCIONARIOS').asstring:=Self.TITULOINSS_FUNCIONARIOS;
        Parambyname('TITULOINSS_EMPREGADOR').asstring:=Self.TITULOINSS_EMPREGADOR;




//CODIFICA OBJETOPARATABELA
  End;
End;

//***********************************************************************

function TObjFOLHAPAGAMENTO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFOLHAPAGAMENTO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Historico:='';
        Data:='';
        DataComissaocolocador:='';
        DataSalarioAdiantamento:='';
        Processado:='';
        DataQuitacaoComissaoVendedor:='';
        TITULOFGTS:='';
        TITULOINSS_FUNCIONARIOS:='';
        TITULOINSS_EMPREGADOR:='';
     End;
end;

Function TObjFOLHAPAGAMENTO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

      If (Processado='')
      Then Mensagem:=mensagem+'/Processado';

      If (Data='')
      Then Mensagem:=mensagem+'/Data';

      If (DataComissaocolocador='')
      Then Mensagem:=mensagem+'/Data de Comiss�o do Colocador';

      If (DataSalarioAdiantamento='')
      Then Mensagem:=mensagem+'/Data do Sal�rio de Adiantamento';

      If (DataQuitacaoComissaoVendedor='')
      Then Mensagem:=mensagem+'/Data de Quita��o da Pend�ncia';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFOLHAPAGAMENTO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';

     if (Self.TITULOFGTS<>'')
     Then Begin
               if (Self.TituloTemp.LocalizaCodigo(Self.TITULOFGTS)=False)
               then Mensagem:=Mensagem+'\T�tulo de FGTS n�o encontrado na tabela de t�tulos';
     End;


     if (Self.TITULOINSS_FUNCIONARIOS<>'')
     Then Begin
               if (Self.TituloTemp.LocalizaCodigo(Self.TITULOINSS_FUNCIONARIOS)=False)
               then Mensagem:=Mensagem+'\T�tulo de Inss de Funcion�rios n�o encontrado na tabela de t�tulos';
     End;              


     if (Self.TITULOINSS_EMPREGADOR<>'')
     Then Begin
               if (Self.TituloTemp.LocalizaCodigo(Self.TITULOINSS_EMPREGADOR)=False)
               then Mensagem:=Mensagem+'\T�tulo de Inss do Empregador n�o encontrado na tabela de t�tulos';
     End;

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFOLHAPAGAMENTO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;

     if (Self.TITULOFGTS<>'')
     Then Begin
               Try
                  Strtoint(Self.titulofgts);
               Except
                  Mensagem:=Mensagem+'\T�tulo de FGTS';
               End;
     End;

     if (Self.TITULOINSS_FUNCIONARIOS<>'')
     Then Begin
               Try
                  Strtoint(Self.TITULOINSS_FUNCIONARIOS);
               Except
                  Mensagem:=Mensagem+'\T�tulo de Inss de Funcion�rios';
               End;
     End;


     if (Self.TITULOINSS_EMPREGADOR<>'')
     Then Begin
               Try
                  Strtoint(Self.TITULOINSS_EMPREGADOR);
               Except
                  Mensagem:=Mensagem+'\T�tulo de Inss de Empregador';
               End;
     End;

//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFOLHAPAGAMENTO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
     try
        Strtodate(Self.Data);
     Except
           Mensagem:=mensagem+'/Data';
     End;


     try
        Strtodate(Self.DataComissaocolocador);
     Except
           Mensagem:=mensagem+'/Data de Comiss�o do Colocador';
     End;

     try
        Strtodate(Self.DataSalarioAdiantamento);
     Except
           Mensagem:=mensagem+'/Data do Sal�rio de Adiantamento';
     End;


     try
        Strtodate(Self.DataQuitacaoComissaoVendedor);
     Except
           Mensagem:=mensagem+'/Data de Quita��o da Pend�ncia';
     End;
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFOLHAPAGAMENTO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';

        if (Processado<>'S') and (Processado<>'N')
        Then Mensagem:=Mensagem+'/O Campo Processado cont�m um valor inv�lido';

//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjFOLHAPAGAMENTO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FOLHAPAGAMENTO vazio',mterror,[mbok],0);
                 exit;
       End;




       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Historico,Data,DataComissaocolocador');
           SQL.ADD(' ,DataSalarioAdiantamento,DataQuitacaoComissaoVendedor,processado,TITULOFGTS,TITULOINSS_FUNCIONARIOS,TITULOINSS_EMPREGADOR');
           SQL.ADD(' from  TabFolhaPagamento');
           SQL.ADD(' WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFOLHAPAGAMENTO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFOLHAPAGAMENTO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFOLHAPAGAMENTO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.TituloTemp:=tobjTitulo.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabFolhaPagamento(CODIGO,Historico,Data');
                InsertSQL.add(' ,DataComissaocolocador,DataSalarioAdiantamento,DataQuitacaoComissaoVendedor,processado,TITULOFGTS,TITULOINSS_FUNCIONARIOS,TITULOINSS_EMPREGADOR');
                InsertSQL.add(' )');
                InsertSQL.add('values (:CODIGO,:Historico,:Data');
                InsertSQL.add(' ,:DataComissaocolocador,:DataSalarioAdiantamento,:DataQuitacaoComissaoVendedor,:processado,:TITULOFGTS,:TITULOINSS_FUNCIONARIOS,:TITULOINSS_EMPREGADOR)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabFolhaPagamento set CODIGO=:CODIGO,Historico=:Historico');
                ModifySQL.add(',Data=:Data');
                ModifySQL.add(',DataComissaocolocador=:DataComissaocolocador,DataSalarioAdiantamento=:DataSalarioAdiantamento,DataQuitacaoComissaoVendedor=:DataQuitacaoComissaoVendedor,');
                ModifySQL.add('processado=:processado,TITULOFGTS=:TITULOFGTS,TITULOINSS_FUNCIONARIOS=:TITULOINSS_FUNCIONARIOS,TITULOINSS_EMPREGADOR=:TITULOINSS_EMPREGADOR');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabFolhaPagamento where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFOLHAPAGAMENTO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFOLHAPAGAMENTO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFOLHAPAGAMENTO');
     Result:=Self.ParametroPesquisa;
end;

function TObjFOLHAPAGAMENTO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Folha de Pagamento ';
end;


function TObjFOLHAPAGAMENTO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENFOLHAPAGAMENTO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFOLHAPAGAMENTO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.titulotemp.free;
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFOLHAPAGAMENTO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFOLHAPAGAMENTO.RetornaCampoNome: string;
begin
      result:='Historico';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFolhaPagamento.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjFolhaPagamento.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjFolhaPagamento.Submit_Historico(parametro: string);
begin
        Self.Historico:=Parametro;
end;
function TObjFolhaPagamento.Get_Historico: string;
begin
        Result:=Self.Historico;
end;
procedure TObjFolhaPagamento.Submit_Data(parametro: string);
begin
        Self.Data:=Parametro;
end;
function TObjFolhaPagamento.Get_Data: string;
begin
        Result:=Self.Data;
end;

procedure TObjFolhaPagamento.Submit_DataComissaocolocador(parametro: string);
begin
        Self.DataComissaocolocador:=Parametro;
end;
function TObjFolhaPagamento.Get_DataComissaocolocador: string;
begin
        Result:=Self.DataComissaocolocador;
end;
procedure TObjFolhaPagamento.Submit_DataSalarioAdiantamento(parametro: string);
begin
        Self.DataSalarioAdiantamento:=Parametro;
end;
function TObjFolhaPagamento.Get_DataSalarioAdiantamento: string;
begin
        Result:=Self.DataSalarioAdiantamento;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjFOLHAPAGAMENTO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFOLHAPAGAMENTO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:begin
          End;
          End;
     end;

end;



function TObjFOLHAPAGAMENTO.Get_Processado: string;
begin
     Result:=Self.Processado;
end;

procedure TObjFOLHAPAGAMENTO.Submit_Processado(parametro: string);
begin
     Self.Processado:=parametro;
end;

function TObjFOLHAPAGAMENTO.get_DataQuitacaoComissaoVendedor: string;
begin
     Result:=Self.DataQuitacaoComissaoVendedor;
end;

procedure TObjFOLHAPAGAMENTO.Submit_DataQuitacaoComissaoVendedor(
  parametro: string);
begin
     Self.DataQuitacaoComissaoVendedor:=parametro;
end;

function TObjFOLHAPAGAMENTO.Get_TITULOFGTS: string;
begin
      Result:=Self.TITULOFGTS;
end;

function TObjFOLHAPAGAMENTO.Get_TITULOINSS_EMPREGADOR: string;
begin
     Result:=Self.TITULOINSS_EMPREGADOR;
end;

function TObjFOLHAPAGAMENTO.Get_TITULOINSS_FUNCIONARIOS: string;
begin
     Result:=Self.TITULOINSS_FUNCIONARIOS;
end;

procedure TObjFOLHAPAGAMENTO.Submit_TITULOFGTS(parametro: string);
begin
     Self.TITULOFGTS:=parametro;
end;

procedure TObjFOLHAPAGAMENTO.Submit_TITULOINSS_EMPREGADOR(
  parametro: string);
begin
     Self.TITULOINSS_EMPREGADOR:=parametro;
end;

procedure TObjFOLHAPAGAMENTO.Submit_TITULOINSS_FUNCIONARIOS(
  parametro: string);
begin
     Self.TITULOINSS_FUNCIONARIOS:=parametro;
end;

function TObjFOLHAPAGAMENTO.Get_ObjetoTituloFgts: Boolean;
begin
     Result:=False;

     if (Self.TITULOFGTS='')
     then exit;

     if (Self.TituloTemp.LocalizaCodigo(Self.TITULOFGTS)=False)
     then exit;

     Self.TituloTemp.TabelaparaObjeto;

     result:=True;
end;

function TObjFOLHAPAGAMENTO.Get_ObjetoTituloInss_Empregador: Boolean;
begin
     Result:=False;

     if (Self.TITULOINSS_EMPREGADOR='')
     then exit;

     if (Self.TituloTemp.LocalizaCodigo(Self.TITULOINSS_EMPREGADOR)=False)
     then exit;

     Self.TituloTemp.TabelaparaObjeto;

     result:=True;
end;

function TObjFOLHAPAGAMENTO.Get_ObjetoTituloInss_Funcionarios: Boolean;
begin
     Result:=False;

     if (Self.TITULOINSS_FUNCIONARIOS='')
     then exit;

     if (Self.TituloTemp.LocalizaCodigo(Self.TITULOINSS_FUNCIONARIOS)=False)
     then exit;

     Self.TituloTemp.TabelaparaObjeto;

     result:=True;
end;

end.



