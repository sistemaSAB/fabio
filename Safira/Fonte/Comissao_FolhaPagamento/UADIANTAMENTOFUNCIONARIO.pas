unit UADIANTAMENTOFUNCIONARIO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjADIANTAMENTOFUNCIONARIO,IBQuery;

type
  TFADIANTAMENTOFUNCIONARIO = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbcodigo: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb2: TLabel;
    mmohistorico: TMemo;
    lb4: TLabel;
    edtValor: TEdit;
    lbLbValor: TLabel;
    edtdata: TMaskEdit;
    lb5: TLabel;
    edtDataSalario: TMaskEdit;
    lbLbDataSalario: TLabel;
    lbnomefuncionario: TLabel;
    edtFuncionario: TEdit;
    lbLbFuncionario: TLabel;
//DECLARA COMPONENTES

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure edtFuncionarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFuncionarioExit(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure btrelatoriosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         Procedure LimpaLabels;
         procedure MostraQuantidadeCadastrada;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FADIANTAMENTOFUNCIONARIO: TFADIANTAMENTOFUNCIONARIO;
  ObjADIANTAMENTOFUNCIONARIO:TObjADIANTAMENTOFUNCIONARIO;

implementation

uses UessencialGlobal, Upesquisa, UescolheImagemBotao, UDataModulo;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFADIANTAMENTOFUNCIONARIO.ControlesParaObjeto: Boolean;
Begin
  Try
    With ObjADIANTAMENTOFUNCIONARIO do
    Begin
        Submit_Codigo(lbcodigo.Caption);
        Funcionario.submit_codigo(edtFuncionario.text);
        Submit_Valor(edtValor.text);
        Submit_DataSalario(edtDataSalario.text);
        Submit_data(Edtdata.text);
        Submit_Historico(mmohistorico.Text);
        result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFADIANTAMENTOFUNCIONARIO.ObjetoParaControles: Boolean;
Begin
  Try
     With ObjADIANTAMENTOFUNCIONARIO do
     Begin
        lbcodigo.Caption:=Get_Codigo;
        EdtFuncionario.text:=Funcionario.get_codigo;
        EdtValor.text:=Get_Valor;
        EdtDataSalario.text:=Get_DataSalario;
        Edtdata.text:=get_data;
        mmohistorico.Text:=get_Historico;
        result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFADIANTAMENTOFUNCIONARIO.TabelaParaControles: Boolean;
begin
     If (ObjADIANTAMENTOFUNCIONARIO.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;



//****************************************
procedure TFADIANTAMENTOFUNCIONARIO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjADIANTAMENTOFUNCIONARIO=Nil)
     Then exit;

If (ObjADIANTAMENTOFUNCIONARIO.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

ObjADIANTAMENTOFUNCIONARIO.free;
end;

procedure TFADIANTAMENTOFUNCIONARIO.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFADIANTAMENTOFUNCIONARIO.BtnovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     //edtcodigo.text:='0';
     lbcodigo.Caption:=ObjADIANTAMENTOFUNCIONARIO.Get_novocodigo;
     edtdata.text:=datetostr(now);

     
     btsalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjADIANTAMENTOFUNCIONARIO.status:=dsInsert;

     EdtFuncionario.setfocus;

end;


procedure TFADIANTAMENTOFUNCIONARIO.btgravarClick(Sender: TObject);
begin

     If ObjADIANTAMENTOFUNCIONARIO.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjADIANTAMENTOFUNCIONARIO.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     MostraQuantidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFADIANTAMENTOFUNCIONARIO.btexcluirClick(Sender: TObject);
begin
     If (ObjADIANTAMENTOFUNCIONARIO.status<>dsinactive) or (lbcodigo.Caption='')
     Then exit;

     If (ObjADIANTAMENTOFUNCIONARIO.LocalizaCodigo(lbcodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     

     If (ObjADIANTAMENTOFUNCIONARIO.exclui(lbcodigo.Caption,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFADIANTAMENTOFUNCIONARIO.btcancelarClick(Sender: TObject);
begin
     ObjADIANTAMENTOFUNCIONARIO.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFADIANTAMENTOFUNCIONARIO.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFADIANTAMENTOFUNCIONARIO.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjADIANTAMENTOFUNCIONARIO.Get_pesquisa,ObjADIANTAMENTOFUNCIONARIO.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjADIANTAMENTOFUNCIONARIO.status<>dsinactive
                                  then exit;

                                  If (ObjADIANTAMENTOFUNCIONARIO.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjADIANTAMENTOFUNCIONARIO.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFADIANTAMENTOFUNCIONARIO.LimpaLabels;
begin
     lbnomefuncionario.caption:='';
end;

procedure TFADIANTAMENTOFUNCIONARIO.edtFuncionarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjADIANTAMENTOFUNCIONARIO.EdtFuncionarioKeyDown(sender,key,shift,lbnomefuncionario);
end;

procedure TFADIANTAMENTOFUNCIONARIO.edtFuncionarioExit(Sender: TObject);
begin
ObjADIANTAMENTOFUNCIONARIO.EdtFuncionarioExit(sender,lbnomefuncionario);
end;

procedure TFADIANTAMENTOFUNCIONARIO.BitBtn1Click(Sender: TObject);
begin
     ObjADIANTAMENTOFUNCIONARIO.opcoes;
end;

procedure TFADIANTAMENTOFUNCIONARIO.btrelatoriosClick(Sender: TObject);
begin
     ObjADIANTAMENTOFUNCIONARIO.imprime;
end;

procedure TFADIANTAMENTOFUNCIONARIO.FormShow(Sender: TObject);
begin
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
     lbCodigo.caption:='';
     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);


     Try
        ObjADIANTAMENTOFUNCIONARIO:=TObjADIANTAMENTOFUNCIONARIO.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;

     MostraQuantidadeCadastrada;
end;

procedure TFADIANTAMENTOFUNCIONARIO.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabcidade');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb2.Caption:='Existem '+IntToStr(contador)+' adiantamentos cadastrados'
       else
       begin
            lb2.Caption:='Existe '+IntToStr(contador)+' adiantamento cadastrado';
       end;

    finally

    end;
end;




end.

