unit UobjfolhaPagamentoObjetos;

interface

Uses sysutils,classes,ibquery,Controls,db,UessencialGlobal,UobjFuncionarioFolhaPagamento,UObjComissao_Adiant_FuncFolha;

Type
      TobjFolhaPagamentoObjetos=class
      Public
            ObjFuncionarioFolhaPagamento:TobjFuncionarioFolhaPagamento;
            ObjComissao_Adiant_FuncFolha:TObjComissao_Adiant_FuncFolha;
            Procedure Opcoes(Pfolha:string);
            Procedure Imprime(Pcodigo:string);
            Procedure ImprimeFolha(pcodigo:string);
            Constructor Create(Owner:TComponent);
            Destructor  Free;

      Private
             Objquery:Tibquery;
             Owner:TComponent;
             
             Procedure GerarFolhaPagamento(PFolha:string);
             Procedure RetornarFolhaPagamento(Pfolha:string);
             procedure ImprimeFolhaAnalitica(pcodigo: string);
      End;

implementation

uses UopcaoRel,dialogs, UDataModulo, UMostraBarraProgresso,
  UMenuRelatorios, UReltxtRDPRINT,rdprint, UFiltraImp;

{ TobjFolhaPagamentoObjetos }

constructor TobjFolhaPagamentoObjetos.Create(Owner:TComponent);
begin
     With Self do
     Begin
          Self.Owner := Owner;
          ObjFuncionarioFolhaPagamento:=TobjFuncionarioFolhaPagamento.Create;
          ObjComissao_Adiant_FuncFolha:=TObjComissao_Adiant_FuncFolha.Create(Self.Owner);
          Objquery:=Tibquery.create(nil);
          Objquery.database:=FdataModulo.ibdatabase;
     End;
end;

destructor TobjFolhaPagamentoObjetos.Free;
begin
     Self.ObjFuncionarioFolhaPagamento.Free;
     Self.ObjComissao_Adiant_FuncFolha.free;
     Self.Objquery.free;
end;


procedure TobjFolhaPagamentoObjetos.GerarFolhaPagamento(PFolha: string);
begin
     if (PFolha='')
     Then Begin
               Messagedlg('Escolha a Folha que deseja gerar os t�tulos',mterror,[mbok],0);
               exit;
     End;

     if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(PFolha)=False)
     Then Begin
               Messagedlg('Folha N�o encontrada',mterror,[mbok],0);
               exit;
     End;

     Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto;
     if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_Processado='S')
     Then begin
               Messagedlg('Esta folha j� foi processada',mtinformation,[mbok],0);
               exit;
     End;


Try
     FMostraBarraProgresso.Lbmensagem.caption:='RESGATANDO OS FUNCION�RIOS ATIVOS E SEUS SAL�RIOS';
     FMostraBarraProgresso.BarradeProgresso.MaxValue:=0;
     FMostraBarraProgresso.BarradeProgresso.Progress:=0;

     //Selecionar todos os funcionarios que nao estao ativos
     //e gravar na TabFuncionarioFolhaPagamento
     if (Self.ObjFuncionarioFolhaPagamento.Cria_registros_Folha(PFolha)=False)
     Then Begin
               Messagedlg('N�o foi poss�vel gravar os Funcion�rios na Folha',mterror,[mbok],0);
               exit;
     End;

     //Agora na Comissao_Adi.. Selecionar todos os FuncionariosFolhaPagmento da Folha Atual
     //Para cada um fazer um select em cada item (comissaovende,comissaocol,adiant)
     //e ir gravando na Comissao_adi... no final de tudo atualizado o valor das variaveeis da FuncFolha e gero os titulos
     if (Self.ObjComissao_Adiant_FuncFolha.Cria_Registros_Folha(Pfolha)=False)
     Then Begin
                Messagedlg('N�o foi poss�vel gravar os registros de comiss�es e adiantamento da Folha',mterror,[mbok],0);
                exit;
     End;


     if (Self.ObjComissao_Adiant_FuncFolha.AcertaInss_IRPF_FGTS_SalarioFamilia(Pfolha)=False)
     Then Begin
                Messagedlg('N�o foi poss�vel gravar os valores de INSS, IRPF e Sal�rio Fam�lia dos funcion�rios da Folha',mterror,[mbok],0);
                exit;
     End;

     //gerando os Titulos

       if (ObjParametroGlobal.ValidaParametro('GERAR TITULOS A PAGAR DA FOLHA DE PAGAMENTO?')=True)
     then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               Then Begin
                         //A pedido da Camila no dia 25/10/07 foi adicionado um parametro
                         //para gerar ou nao os titulos, devido a mudanca do simples para super simples
                         //ocorrerao algumas mudancas nos calculos de inss e outros, sendo assim
                         //ate nova decisao nao se gera titulos
                         if (Self.ObjFuncionarioFolhaPagamento.Gerar_titulos(Pfolha)=False)
                         Then Begin
                                Messagedlg('N�o foi poss�vel criar os t�tulos apagar para os funcion�rios',mterror,[mbok],0);
                                exit;
                         End;
               End
     End;

     //gerando os Titulos
     If (ObjParametroGlobal.ValidaParametro('GERAR CONTABILIDADE DA FOLHA DE PAGAMENTO?')=True)
     Then Begin
               if (ObjParametroGlobal.Get_Valor='SIM')
               then Begin
                     if (Self.ObjFuncionarioFolhaPagamento.Gerar_contabilidade_Folha(Pfolha)=False)
                     Then Begin
                               Messagedlg('N�o foi poss�vel criar os registros cont�beis da Folha',mterror,[mbok],0);
                               exit;
                     End;
               End;
     End;

     //passando para processado=S
     if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(PFolha)=False)
     Then Begin
               Messagedlg('Folha N�o encontrada',mterror,[mbok],0);
               exit;
     End;
     Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto;
     Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Status:=dsedit;
     Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Submit_Processado('S');
     if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Salvar(False)=False)
     Then begin
               Messagedlg('Erro na Tentativa de Alterar o Campo Processado para "S" na Folha de Pagamento',mterror,[mbok],0);
               exit;
     End;

     FDataModulo.IBTransaction.CommitRetaining;
     Messagedlg('Folha gerada com Sucesso',mtinformation,[mbok],0);

Finally
     FdataModulo.ibtransaction.rollbackretaining;
     FMostraBarraProgresso.Close;
end;
     
end;

procedure TobjFolhaPagamentoObjetos.Imprime(Pcodigo: string);
Begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFOLHAPAGAMENTO';

          With RgOpcoes do
          Begin
                items.clear;
                items.add('Folha de Pagamento');
                items.add('Folha de Pagamento Anal�tica');
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
              0:Self.ImprimeFolha(pcodigo);
              1:Self.ImprimeFolhaAnalitica(pcodigo);
          End;
     end;

end;

procedure TobjFolhaPagamentoObjetos.ImprimeFolhaAnalitica(pcodigo: string);
var
cont:integer;
PqueryTemp:Tibquery;
psoma:currency;
pfuncionario:String;
begin
{
    Funcionario Joao da Silva         Salario R$ 200,00
      Adiantamentos
        02/03               R$ 30,00
        05/03               R$ 50,00
      Total                 R$ 80,00

      Comiss�o de Venda     R$ 50,00
      Comiss�o de Colocador R$ 80,00

      Salario Final         R$ 250,00

      Funcionario Maria de Souza
      ....
      ....
      ...
}

    if (Pcodigo='')
    then Begin
              Messagedlg('Escolha uma Folha de Pagamento',mtinformation,[mbok],0);
              exit;
    end;

    if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(pcodigo)=False)
    Then begin
              Messagedlg('Folha n�o encontrada',mterror,[mbok],0);
              exit;
    End;
    Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto;

    if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_Processado='N')
    Then Begin
              Messagedlg('Folha n�o processada',mtinformation,[mbok],0);
              exit;
    End;

    With FfiltroImp do
    Begin
         DesativaGrupos;
         Grupo01.Enabled:=True;
         edtgrupo01.OnKeyDown:=Self.ObjFuncionarioFolhaPagamento.EdtFuncionarioKeyDown;
         LbGrupo01.Caption:='Funcion�rio (opcional)';
         showmodal;

         if (tag=0)
         then exit;

         PFuncionario:='';
         if (edtgrupo01.Text<>'')
         Then Begin
                   Try
                      strtoint(edtgrupo01.Text);
                      pfuncionario:=edtgrupo01.Text;
                   Except
                         MensagemErro('C�digo Inv�lido do Funcion�rio');
                         exit;
                   End;
         End;



    End;


    Try
      PqueryTemp:=Tibquery.Create(nil);
      PqueryTemp.Database:=Self.Objquery.Database;
    Except
          Messagedlg('Erro na tentativa de Criar a QueryTemp',mterror,[mbok],0);
          exit;
    End;
Try    

    //************************************************************************
    With Self.Objquery do
    Begin
         close;
         SQL.clear;
         sql.add('Select Tabfuncionarios.nome,TFP.* from Tabfuncionariofolhapagamento TFP');
         sql.add('join TabFuncionarios on TFP.funcionario=tabfuncionarios.codigo');
         sql.add('where TFP.FolhaPagamento='+pcodigo);

         if (Pfuncionario<>'')
         then sql.add('and TFP.funcionario='+Pfuncionario);

         open;
         if (recordcount=0)
         Then Begin
                   Messagedlg('Nenhuma informa��o foi selecionada!',mtinformation,[mbok],0);
                   exit;
         End;
         FreltxtRDPRINT.ConfiguraImpressao;
         FreltxtRDPRINT.RDprint.TamanhoQteColunas:=130;
         FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S17cpp;
         FreltxtRDPRINT.RDprint.abrir;

         if (FreltxtRDPRINT.RDprint.Setup=False)
         Then Begin
                   FreltxtRDPRINT.RDprint.Fechar;
                   exit;
         End;

         With FreltxtRDPRINT do
         Begin

              LinhaLocal:=3;
              RDprint.ImpC(linhalocal,45,'FOLHA DE PAGAMENTO ANAL�TICA',[negrito]);
              inc(linhalocal,2);

              RDprint.Impf(linhalocal,01,'FOLHA N�: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_CODIGO,[negrito],0);
              inc(linhalocal,1);
              RDprint.Impf(linhalocal,01,'DATA: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_Data,[negrito],0);
              inc(linhalocal,1);
              RDprint.Impf(linhalocal,01,'DATA SAL�RIO: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_DataSalarioAdiantamento,[negrito],0);
              inc(linhalocal,1);
              RDprint.Impf(linhalocal,01,'DATA COMISS�O COLOCADOR: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_DataComissaocolocador,[negrito],0);
              inc(linhalocal,1);

              if (pfuncionario<>'')
              Then Begin
                        RDprint.Impf(linhalocal,1,'FUNCION�RIO (FILTRO): '+completapalavra(fieldbyname('funcionario').asstring+'-'+fieldbyname('nome').asstring,63,' '),[negrito]);
                        inc(linhalocal,1);
              End;

              RDprint.Imp(linhalocal,01,Completapalavra('_',90,'_'));
              inc(linhalocal,1);

              RDprint.Impf(linhalocal,1,completapalavra('FUNCION�RIO',23,' ')+' '+
                                        CompletaPalavra_a_Esquerda('SAL�RIO',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('SAL.FAMILIA',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('INSS',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('IRPF',12,' ')+' '+
                                        CompletaPalavra_a_Esquerda('SAL.FINAL',12,' '),[negrito]);
              IncrementaLinha(1);

              While not(Self.Objquery.eof) do
              Begin
                   inc(linhalocal,1);
                   VerificaLinha;
                   {RDprint.Imp(linhalocal,1,completapalavra('Nome: '+fieldbyname('funcionario').asstring+'-'+fieldbyname('nome').asstring,40,' ')+' '+
                                            'Sal�rio: '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('salario').asstring),10,' ')+' '+
                                            'Sal�rio a Pagar: '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('salariofinal').asstring),10,' '));
                   inc(linhalocal,1);}

                   RDprint.Imp(linhalocal,1,completapalavra(fieldbyname('funcionario').asstring+'-'+fieldbyname('nome').asstring,23,' ')+' '+
                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('salario').asstring),12,' ')+' '+
                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('acrescimosalariofamilia').asstring),12,' ')+' '+
                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('descontoinss').asstring),12,' ')+' '+
                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('descontoirpf').asstring),12,' ')+' '+
                                       CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('salariofinal').asstring),12,' '));
                   IncrementaLinha(1);

                   PSoma:=0;





                   //Adiantamento
                   PqueryTemp.close;
                   PqueryTemp.sql.clear;
                   PqueryTemp.SQL.add('Select TADF.* from TabComissao_adiant_funcFolha TCAFF');
                   PqueryTemp.SQL.add('join TabAdiantamentofuncionario TADF on TCAFF.adiantamentofuncionario=TADF.codigo');
                   PqueryTemp.SQL.add('where TCAFF.FuncionarioFolhaPagamento='+Fieldbyname('codigo').asstring);
                   PqueryTemp.open;
                   if (PqueryTemp.recordcount>0)
                   Then Begin
                             inc(LinhaLocal,1);
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,3,'ADIANTAMENTOS',[negrito]);
                             inc(LinhaLocal,1);
                   End;

                   while Not(PqueryTemp.eof) do
                   Begin
                        VerificaLinha;
                         RDprint.Imp(LinhaLocal,3,CompletaPalavra(PqueryTemp.fieldbyname('data').asstring,10,' ')+
                                                  CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.fieldbyname('valor').asstring),76,' '));

                        inc(LinhaLocal,1);
                        PqueryTemp.next;
                   End;

                   if (PqueryTemp.recordcount>0)
                   Then Begin
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,3,'TOTAL EM ADIANTAMENTOS: '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('totaladiantamento').asstring),62,' '),[negrito]);
                             inc(LinhaLocal,1);;
                   End;

                   PSoma:=0;

                   //Hora Extra
                   PqueryTemp.close;
                   PqueryTemp.sql.clear;
                   PqueryTemp.SQL.add('Select THE.Data,THE.Valor');
                   PqueryTemp.SQL.add('from TabComissao_adiant_funcFolha TCAFF');
                   PqueryTemp.SQL.add('join TabHoraExtra THE on TCAFF.HoraExtra=THE.codigo');
                   PqueryTemp.SQL.add('where TCAFF.FuncionarioFolhaPagamento='+Fieldbyname('codigo').asstring);
                   PqueryTemp.open;
                   if (PqueryTemp.recordcount>0)
                   Then Begin
                             inc(LinhaLocal,1);
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,3,'HORA EXTRA',[negrito]);
                             inc(LinhaLocal,1);
                   End;

                   while Not(PqueryTemp.eof) do
                   Begin
                        VerificaLinha;
                         RDprint.Imp(LinhaLocal,3,CompletaPalavra(PqueryTemp.fieldbyname('data').asstring,10,' ')+
                                                  CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.fieldbyname('valor').asstring),76,' '));

                        inc(LinhaLocal,1);
                        PqueryTemp.next;
                   End;

                   if (PqueryTemp.recordcount>0)
                   Then Begin
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,3,'TOTAL EM HORA EXTRA:    '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('acrescimohoraextra').asstring),62,' '),[negrito]);
                             inc(LinhaLocal,1);;
                   End;

                   //Gratificacao
                   PqueryTemp.close;
                   PqueryTemp.sql.clear;
                   PqueryTemp.SQL.add('Select TGT.Data,TGT.Valor');
                   PqueryTemp.SQL.add('from TabComissao_adiant_funcFolha TCAFF');
                   PqueryTemp.SQL.add('join TabGratificacao TGT on TCAFF.Gratificacao=TGT.codigo');
                   PqueryTemp.SQL.add('where TCAFF.FuncionarioFolhaPagamento='+Fieldbyname('codigo').asstring);
                   PqueryTemp.open;
                   if (PqueryTemp.recordcount>0)
                   Then Begin
                             inc(LinhaLocal,1);
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,3,'GRATIFICA��O',[negrito]);
                             inc(LinhaLocal,1);
                   End;

                   while Not(PqueryTemp.eof) do
                   Begin
                        VerificaLinha;
                         RDprint.Imp(LinhaLocal,3,CompletaPalavra(PqueryTemp.fieldbyname('data').asstring,10,' ')+
                                                  CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.fieldbyname('valor').asstring),76,' '));

                        inc(LinhaLocal,1);
                        PqueryTemp.next;
                   End;

                   if (PqueryTemp.recordcount>0)
                   Then Begin
                             VerificaLinha;
                             RDprint.Impf(LinhaLocal,3,'TOTAL EM GRATIFICA��O:  '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('acrescimogratificacao').asstring),62,' '),[negrito]);
                             inc(LinhaLocal,1);;
                   End;





                   //Comissao de Venda
                   PqueryTemp.close;
                   PqueryTemp.sql.clear;
                   PqueryTemp.SQL.add('Select TCV.Pedido,');
                   PqueryTemp.SQL.add('TabCliente.nome,');
                   PqueryTemp.SQL.add('TCV.Pendencia,TCV.Comissao,TCV.Valor,TCV.ValorComissao');
                   PqueryTemp.SQL.add('from TabComissao_adiant_funcFolha TCAFF');
                   PqueryTemp.SQL.add('join TabComissaoVendedores TCV on TCAFF.ComissaoVendedor=TCV.codigo');
                   PqueryTemp.SQL.add('join TabPedido on TCV.Pedido=tabPedido.codigo');
                   PqueryTemp.SQL.add('join TabCliente on TabPedido.Cliente=TabCliente.codigo');
                   PqueryTemp.SQL.add('where TCAFF.FuncionarioFolhaPagamento='+Fieldbyname('codigo').asstring);
                   PqueryTemp.open;

                   if (PqueryTemp.RecordCount>0)
                   Then Begin
                            inc(LinhaLocal,1);

                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,3,'COMISS�O DE VENDA',[negrito]);
                            inc(LinhaLocal,1);

                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,3,CompletaPalavra('PEDIDO',6,' ')+' '+
                                                     CompletaPalavra('CLIENTE',33,' ')+' '+
                                                     CompletaPalavra('PEND.',6,' ')+' '+
                                                     CompletaPalavra_a_Esquerda('VALOR PEND',12,' ')+' '+
                                                     CompletaPalavra_a_Esquerda('COMISS�O',12,' ')+' '+
                                                     CompletaPalavra_a_Esquerda('VL.COMISS�O',12,' '),[negrito]);
                            inc(LinhaLocal,1);;
                   End;

                   While not(PqueryTemp.eof) do
                   Begin
                        VerificaLinha;
                        RDprint.Imp(LinhaLocal,3,CompletaPalavra(PqueryTemp.Fieldbyname('pedido').asstring,6,' ')+' '+
                                                 CompletaPalavra(PqueryTemp.Fieldbyname('nome').asstring,33,' ')+' '+
                                                 CompletaPalavra(PqueryTemp.Fieldbyname('pendencia').asstring,6,' ')+' '+
                                                 CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.Fieldbyname('valor').asstring),12,' ')+' '+
                                                 CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.Fieldbyname('comissao').asstring),12,' ')+' '+
                                                 CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.Fieldbyname('valorcomissao').asstring),12,' '));
                        inc(LinhaLocal,1);;
                        PqueryTemp.Next;
                   End;

                   if (PqueryTemp.RecordCount>0)
                   Then Begin
                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,3,'TOTAL EM COMISS�O DE VENDA: '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('comissaovenda').AsString),58,' '),[negrito]);
                            inc(LinhaLocal,1);
                   End;


                   //Comissao de colocador
                   PqueryTemp.close;
                   PqueryTemp.sql.clear;
                   PqueryTemp.SQL.add('Select');
                   PqueryTemp.SQL.add('TabPedido.codigo as PEDIDO,');
                   PqueryTemp.SQL.add('TCC.PedidoProjeto,');
                   PqueryTemp.SQL.add('TabCliente.Nome,');
                   PqueryTemp.SQL.add('Tcc.BasedeCalculo,');
                   PqueryTemp.SQL.add('Tcc.PercentualComissaoColocador,');
                   PqueryTemp.SQL.add('Tcc.ValorComissao,TabPedidoprojetoRomaneio.romaneio');
                   PqueryTemp.SQL.add('from TabComissao_adiant_funcFolha TCAFF');
                   PqueryTemp.SQL.add('join TabComissaoColocador TCC on TCAFF.ComissaoColocador=TCC.codigo');
                   PqueryTemp.SQL.add('join TabPedidoProjetoRomaneio on TCC.PedidoProjetoRomaneio=TabPedidoProjetoRomaneio.codigo');
                   PqueryTemp.SQL.add('join tabPedido_proj on TCC.pedidoprojeto=Tabpedido_proj.codigo');
                   PqueryTemp.SQL.add('join tabpedido on tabPedido_proj.pedido=tabpedido.codigo');
                   PqueryTemp.SQL.add('join tabcliente on tabpedido.cliente=tabcliente.codigo');
                   PqueryTemp.SQL.add('where TCAFF.FuncionarioFolhaPagamento='+Fieldbyname('codigo').asstring);
                   PqueryTemp.open;

                   if (PqueryTemp.RecordCount>0)
                   Then Begin
                            inc(LinhaLocal,1);

                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,3,'COMISS�O DE COLOCADOR',[negrito]);
                            inc(LinhaLocal,1);

                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,3,
                                                     CompletaPalavra('ROMANEIO',6,' ')+' '+
                                                     CompletaPalavra('PEDIDO',6,' ')+' '+
                                                     CompletaPalavra('CLIENTE',26,' ')+' '+
                                                     CompletaPalavra('PEDPROJ',6,' ')+' '+
                                                     CompletaPalavra_a_Esquerda('VALOR',12,' ')+' '+
                                                     CompletaPalavra_a_Esquerda('COMISS�O',12,' ')+' '+
                                                     CompletaPalavra_a_Esquerda('VL.COMISS�O',12,' '),[negrito]);
                            inc(LinhaLocal,1);;
                   End;

                   While not(PqueryTemp.eof) do
                   Begin
                        VerificaLinha;
                        RDprint.Imp(LinhaLocal,3,CompletaPalavra(PqueryTemp.Fieldbyname('romaneio').asstring,6,' ')+' '+
                                                 CompletaPalavra(PqueryTemp.Fieldbyname('pedido').asstring,6,' ')+' '+
                                                 CompletaPalavra(PqueryTemp.Fieldbyname('nome').asstring,26,' ')+' '+
                                                 CompletaPalavra(PqueryTemp.Fieldbyname('PEDIDOPROJETO').asstring,6,' ')+' '+
                                                 CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.Fieldbyname('basedecalculo').asstring),12,' ')+' '+
                                                 CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.Fieldbyname('percentualcomissaocolocador').asstring)+'%',12,' ')+' '+
                                                 CompletaPalavra_a_Esquerda(formata_valor(PqueryTemp.Fieldbyname('valorcomissao').asstring),12,' '));
                        inc(LinhaLocal,1);
                        PqueryTemp.Next;
                   End;

                   if (PqueryTemp.RecordCount>0)
                   Then Begin
                            VerificaLinha;
                            RDprint.Impf(LinhaLocal,3,'TOTAL EM COMISS�O DE COLOCADOR: '+CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('comissaocolocador').AsString),54,' '),[negrito]);
                            inc(LinhaLocal,1);
                   End;

                   Self.Objquery.next;
              End;//While FuncionarioFolhaPagamento

              verificalinha;
              RDprint.Imp(linhalocal,01,Completapalavra('_',90,'_'));
              inc(linhalocal,1);
              Rdprint.Fechar;
         End;//Freltxtrdprint
    End;//with

Finally
       Freeandnil(PqueryTemp);
end;

end;

procedure TobjFolhaPagamentoObjetos.ImprimeFolha(pcodigo: string);
var
cont:integer;
PqueryTemp:Tibquery;
psoma:currency;
begin

    if (Pcodigo='')
    then Begin
              Messagedlg('Escolha uma Folha de Pagamento',mtinformation,[mbok],0);
              exit;
    end;

    if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(pcodigo)=False)
    Then begin
              Messagedlg('Folha n�o encontrada',mterror,[mbok],0);
              exit;
    End;
    Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto;

    if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_Processado='N')
    Then Begin
              Messagedlg('Folha n�o processada',mtinformation,[mbok],0);
              exit;
    End;

    Try
      PqueryTemp:=Tibquery.Create(nil);
      PqueryTemp.Database:=Self.Objquery.Database;
    Except
          Messagedlg('Erro na tentativa de Criar a QueryTemp',mterror,[mbok],0);
          exit;
    End;
Try

    //************************************************************************
    With Self.Objquery do
    Begin

         close;
         SQL.clear;
         sql.add('Select TFFP.Funcionario,TF.nome,TFFP.Salario,(TFFP.ComissaoVenda+TFFP.Comissaocolocador) as COMISSAO,');
         sql.add('TFFP.AcrescimoSalarioFamilia as SalarioFamilia,TFFP.DescontoInss as INSS,TFFp.DescontoIRPF as IRPF,');
         sql.add('TFFP.AcrescimoHoraExtra as Horaextra,TFFP.TotalAdiantamento as VALES,TFFP.SalarioFinal,TFFP.FolhaPagamento,');
         sql.add('TFFP.AcrescimoGratificacao as Gratificacao');
         sql.add('from TabFuncionariofolhaPagamento TFFP');
         sql.add('join tabfuncionarios TF on TFFP.funcionario=Tabfuncionarios.codigo');
         sql.add('where TFFP.FolhaPagamento='+pcodigo);
         open;
         if (recordcount=0)
         Then Begin
                   Messagedlg('N�o existe nenhum funcion�rio ligado a esta folha',mtinformation,[mbok],0);
                   exit;
         End;



         FreltxtRDPRINT.ConfiguraImpressao;
         FreltxtRDPRINT.RDprint.FonteTamanhoPadrao:=S20cpp;
         FreltxtRDPRINT.RDprint.TamanhoQteColunas:=160;

         FreltxtRDPRINT.RDprint.abrir;
         if (FreltxtRDPRINT.RDprint.Setup=False)
         Then Begin
                   FreltxtRDPRINT.RDprint.Fechar;
                   exit;
         End;

         With FreltxtRDPRINT do
         Begin


              LinhaLocal:=3;
              RDprint.ImpC(linhalocal,80,'FOLHA DE PAGAMENTO ',[negrito]);
              IncrementaLinha(2);

              RDprint.Impf(linhalocal,01,'FOLHA N�: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_CODIGO,[negrito],0);
              IncrementaLinha(1);
              RDprint.Impf(linhalocal,01,'DATA: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_Data,[negrito],0);
              IncrementaLinha(1);
              RDprint.Impf(linhalocal,01,'DATA SAL�RIO: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_DataSalarioAdiantamento,[negrito],0);
              IncrementaLinha(1);
              //RDprint.Impf(linhalocal,01,'DATA COMISS�O VENDEDOR: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_DataComissaoVendedor,[negrito],0);
              //IncrementaLinha(1);
              RDprint.Impf(linhalocal,01,'DATA COMISS�O COLOCADOR: '+Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_DataComissaocolocador,[negrito],0);
              IncrementaLinha(1);
              RDprint.Impf(linhalocal,1,completapalavra('FUNCION�RIO',40,' ')+' '+
                                       CompletaPalavra_a_Esquerda('SAL�RIO',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('COMISS�O',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('SAL.FAMILIA',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('INSS',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('IRPF',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('HORA EXTRA',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('VALES',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('GRATIFICA��O',12,' ')+' '+
                                       CompletaPalavra_a_Esquerda('TOTAL',12,' '),[negrito]);
              IncrementaLinha(1);
              RDprint.Imp(linhalocal,1,completapalavra('_',160,'_'));
              IncrementaLinha(1);
              Psoma:=0;

              While not(Self.Objquery.eof) do
              Begin
                    VerificaLinha;
                    RDprint.Imp(linhalocal,1,completapalavra(fieldbyname('FUNCIONARIO').asstring+'-'+fieldbyname('nome').asstring,40,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALaRIO').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('COMISSaO').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('SALarioFAMILIA').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('INSS').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('IRPF').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('HORAEXTRA').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('vales').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('gratificacao').asstring),12,' ')+' '+
                                              CompletaPalavra_a_Esquerda(formata_valor(fieldbyname('salariofinal').asstring),12,' '));
                   IncrementaLinha(1);
                   PSoma:=Psoma+fieldbyname('salariofinal').asfloat;
                   Self.Objquery.next;
              End;
              verificalinha;
              RDprint.Imp(linhalocal,1,completapalavra('_',160,'_'));
              inc(linhalocal,1);
              verificalinha;
              RDprint.Impf(linhalocal,1,'SOMA GERAL '+formata_valor(psoma),[negrito]);
              inc(linhalocal,1);


              Rdprint.Fechar;
         End;
    End;

Finally
       Freeandnil(PqueryTemp);
end;
end;

procedure TobjFolhaPagamentoObjetos.Opcoes(Pfolha: string);
begin
     With FOpcaorel do
     Begin
          With RgOpcoes do
          Begin
                items.clear;
                items.add('Gerar T�tulos da Folha Atual');
                items.add('Retornar T�tulos da Folha Atual');

          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
              0:Self.GerarFolhaPagamento(pfolha);
              1:Self.RetornarFolhaPagamento(Pfolha);
          End;
     end;


end;

procedure TobjFolhaPagamentoObjetos.RetornarFolhaPagamento(Pfolha: string);
begin

     if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(Pfolha)=False)
     Then Begin
               Messagedlg('Folha de Pagamento n�o encontrada!',mterror,[mbok],0);
               exit;
     End;
     Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto;

     if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_Processado='N')
     Then Begin
               Messagedlg('Essa folha n�o foi processada',mtinformation,[mbok],0);
               exit;
     End;

     if (Messagedlg('Tem Certeza que deseja retornar os t�tulos da Folha Atual?',mtconfirmation,[mbyes,mbno],0)=mrno)
     then exit;

Try


     Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Status:=dsedit;
     Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Submit_Processado('N');

     if (Self.ObjFuncionarioFolhaPagamento.FolhaPagamento.Salvar(False)=False)
     then begin
               Messagedlg('Erro na tentativa de alterar o Campo Processado para "N" na Folha',mterror,[mbok],0);
               exit;
     End;

     //primeiro excluo os relacionamentos na Comissao_Adiant
     if (Self.ObjComissao_Adiant_FuncFolha.Exclui_por_folha(Pfolha)=False)
     Then Begin
               Messagedlg('N�o foi poss�vel excluir os relacionamentos das comiss�es, adiantamentos e os Funcion�rios da Folha',mterror,[mbok],0);
               exit;
     end;

     if (Self.ObjFuncionarioFolhaPagamento.Exclui_por_Folha(pfolha)=False)
     Then Begin
              Messagedlg('N�o foi poss�vel excluir os funcion�rios da folha de pagamento',mterror,[mbok],0);
              exit;
     End;

     if (Self.ObjFuncionarioFolhaPagamento.Retornar_contabilidade_Folha(pfolha)=False)
     then Begin
               mensagemerro('Erro na tentativa de Retornar a Contabilidade da Folha');
               exit;
     End;

     FDataModulo.IBTransaction.CommitRetaining;
     Messagedlg('Folha retornada com Sucesso',mtinformation,[mbok],0);
     exit;
Finally
       FDataModulo.ibtransaction.rollbackretaining;
End;

end;

end.

{
Boa Tarde Ronnei!

Seguem as alteracoes conforme combinado.
1-Relatorio analitico para confer�ncia das comissoes que foram jogadas para a folha de pagamento.
//OK-> 2-Alguns funcionarios nao sao registrados, logo nao pagam impostos, poderiamos colocar um campo para registrado ou nao.... se registrado calcula os impostos normalmente , senao ....
//ok-> 3-o INSS e pago ate 2801,57 , para salarios maiores q isso  a aliqota e a mesma...exemplo se eu tenho um salario de 5000,00 eu pago INSS referente a 2801,57*11% .
//OK->4-A hora extra vai ser mandada para a contabilidade , logo ela deve ser usada como base de calculo para INSS E IRPF entra na soma do salario bruto.... (antes eles nao mandavam )
//ok->5-Colocar um campo para gratificacoes que nao entra na base de calculo para INSS E IRPF
//OK->6-No modulo de adiantamento o sistema esta jogando a data de salario para todo dia 01 automaticamente , tem q ficar a data q o usuario informa.
//OK->7-no modulo de adiantamento e transferencia colocar a opcao de selecionar cheques do talao
//OK->8-No final do relatorio colocar campo para calculo de FGTS que e 8% sobre o salario base.

Acho que e isso!!!


Para Contrucao do Modulo de Nf


Na Nota Sai item por item, vidro, ferragens, e perfis separados,
pode acontecer de colocar mais de 1 pedido na mesma Nota Fiscal,
Existem acertos de valores e quantidade de mercadoria na hora de tirar a NF

Imprime em Varias Linhas???
Qual o vinculo que devo manter com o Pedido??
De um pedido posso gerar mais de uma NF??
Cada Nf necessariamente gera um cupom fiscal??





Camila.
}
