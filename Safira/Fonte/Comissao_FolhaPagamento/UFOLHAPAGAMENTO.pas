unit UFOLHAPAGAMENTO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, Tabs,UobjFolhaPagamentoObjetos,
  Uessencialglobal;

type
  TFFOLHAPAGAMENTO = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    LbCODIGO: TLabel;
    LbHistorico: TLabel;
    LbData: TLabel;
    LbDataComissaocolocador: TLabel;
    LbDataSalarioAdiantamento: TLabel;
    Bevel: TBevel;
    EdtCODIGO: TEdit;
    EdtHistorico: TEdit;
    EdtData: TMaskEdit;
    EdtDataComissaocolocador: TMaskEdit;
    EdtDataSalarioAdiantamento: TMaskEdit;
    Btopcoes: TSpeedButton;
    ComboProcessado: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    edtDataQuitacaoComissaoVendedor: TMaskEdit;
    Label3: TLabel;
    edtTITULOFGTS: TEdit;
    Label4: TLabel;
    edtTITULOINSS_FUNCIONARIOS: TEdit;
    Label5: TLabel;
    edtTITULOINSS_EMPREGADOR: TEdit;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure FormShow(Sender: TObject);
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GuiaChange(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure BtopcoesClick(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FFOLHAPAGAMENTO: TFFOLHAPAGAMENTO;
  ObjFolhaPagamentoObjetos:TObjFolhaPagamentoObjetos;

implementation

uses Upesquisa, UessencialLocal, UobjFOLHAPAGAMENTO;

{$R *.dfm}


procedure TFFOLHAPAGAMENTO.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     Guia.TabIndex:=0;
     Notebook.PageIndex:=0;

     Try
        ObjFolhaPagamentoObjetos:=TObjFolhaPagamentoObjetos.create(self);
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;


end;

procedure TFFOLHAPAGAMENTO.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFFOLHAPAGAMENTO.FormShow(Sender: TObject);
begin
    PegaCorForm(Self);
    Self.PosicionaPaineis;
    Self.PosicionaForm;
    Self.PegaFiguras;
    Self.ColocaAtalhoBotoes;

end;

procedure TFFOLHAPAGAMENTO.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFFOLHAPAGAMENTO.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFFOLHAPAGAMENTO.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjFolhaPagamentoObjetos=Nil)
     Then exit;

     If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjFolhaPagamentoObjetos.free;
    Action := caFree;
    Self := nil;
end;

procedure TFFOLHAPAGAMENTO.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFFOLHAPAGAMENTO.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFFOLHAPAGAMENTO.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjFOLHAPAGAMENTO.Get_novocodigo;
     edtcodigo.enabled:=False;
     ComboProcessado.enabled:=False;
     ComboProcessado.ItemIndex:=0;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.status:=dsInsert;
     Guia.TabIndex:=0;
     Edthistorico.setfocus;

end;

procedure TFFOLHAPAGAMENTO.btSalvarClick(Sender: TObject);
begin

     If ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFOLHAPAGAMENTO.btAlterarClick(Sender: TObject);
begin
    If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                if (ComboProcessado.itemindex=1)//processada='S'
                Then exit;
                
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                ComboProcessado.enabled:=False;
                ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.Status:=dsEdit;
                guia.TabIndex:=0;
                edthistorico.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFFOLHAPAGAMENTO.btCancelarClick(Sender: TObject);
begin
     ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFFOLHAPAGAMENTO.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_pesquisa,ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.status<>dsinactive
                                  then exit;

                                  If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFFOLHAPAGAMENTO.btExcluirClick(Sender: TObject);
begin
     If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFFOLHAPAGAMENTO.btRelatorioClick(Sender: TObject);
begin
    ObjFolhaPagamentoObjetos.Imprime(edtcodigo.Text);
end;

procedure TFFOLHAPAGAMENTO.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFFOLHAPAGAMENTO.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFFOLHAPAGAMENTO.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFFOLHAPAGAMENTO.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFFOLHAPAGAMENTO.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if key=vk_f5
    Then Begin
              if (Self.PainelPrincipal.Visible=true)
              Then Begin
                        Self.PainelPrincipal.Visible:=False;
                        Self.PainelExtra.Visible:=True;
              End
              Else Begin
                        Self.PainelPrincipal.Visible:=True;
                        Self.PainelExtra.Visible:=False;
              End;
    End;
end;

procedure TFFOLHAPAGAMENTO.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFFOLHAPAGAMENTO.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        Submit_Historico(edtHistorico.text);
        Submit_Data(edtData.text);
        //Submit_DataComissaoVendedor(edtDataComissaoVendedor.text);
        Submit_DataComissaocolocador(edtDataComissaocolocador.text);
        Submit_DataSalarioAdiantamento(edtDataSalarioAdiantamento.text);
        Submit_Processado(Submit_ComboBox(ComboProcessado));
        Submit_DataQuitacaoComissaoVendedor(edtDataQuitacaoComissaoVendedor.Text);
        Submit_TITULOFGTS(edtTITULOFGTS.text);
        Submit_TITULOINSS_FUNCIONARIOS(edtTITULOINSS_FUNCIONARIOS.Text);
        Submit_TITULOINSS_EMPREGADOR(edtTITULOINSS_EMPREGADOR.Text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFFOLHAPAGAMENTO.LimpaLabels;
begin
//LIMPA LABELS
end;

function TFFOLHAPAGAMENTO.ObjetoParaControles: Boolean;
begin
  Try
     With ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtHistorico.text:=Get_Historico;
        EdtData.text:=Get_Data;
        //EdtDataComissaoVendedor.text:=Get_DataComissaoVendedor;
        EdtDataComissaocolocador.text:=Get_DataComissaocolocador;
        EdtDataSalarioAdiantamento.text:=Get_DataSalarioAdiantamento;
        EdtDataQuitacaoComissaoVendedor.text:=get_DataQuitacaoComissaoVendedor;

        if (Get_Processado='S')
        Then ComboProcessado.ItemIndex:=1
        Else ComboProcessado.ItemIndex:=0;
        edtTITULOFGTS.Text:=Get_TITULOFGTS;
        edtTITULOINSS_FUNCIONARIOS.text:=Get_TITULOINSS_FUNCIONARIOS;
        edtTITULOINSS_EMPREGADOR.Text:=Get_TITULOINSS_EMPREGADOR;

        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFFOLHAPAGAMENTO.TabelaParaControles: Boolean;
begin
     If (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFFOLHAPAGAMENTO.GuiaChange(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
     if (newtab=1)//Funcionarios
     Then Begin
               if(edtcodigo.text='')
               Then Begin
                         AllowChange:=False;
                         exit;
               End;
     End;
     Notebook.PageIndex:=NewTab;
end;

procedure TFFOLHAPAGAMENTO.BtopcoesClick(Sender: TObject);
begin
     ObjFolhaPagamentoObjetos.Opcoes(EdtCODIGO.Text);
     
     if (edtcodigo.text='')
     then exit;

     if (ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.LocalizaCodigo(EdtCODIGO.text)=true)
     Then Begin
               ObjFolhaPagamentoObjetos.ObjFuncionarioFolhaPagamento.FolhaPagamento.TabelaparaObjeto;
               Self.TabelaParaControles;
     end;
end;

end.
