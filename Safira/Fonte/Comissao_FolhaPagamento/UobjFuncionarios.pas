unit UObjFUNCIONARIOS;
Interface
Uses windows,stdctrls,Classes,Db,UessencialGlobal,Ibcustomdataset,UOBJCARGOFUNCIONARIO,UObjportador;

Type
   TObjFUNCIONARIOS=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Cargo                                       :TOBJCARGOFUNCIONARIO;
                Portador                                    :TobjPortador;

                
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_Nome(parametro: string);
                Function Get_Nome: string;
                Procedure Submit_Salario(parametro: string);
                Function Get_Salario: string;
                Procedure Submit_DataAdmissao(parametro: string);
                Function Get_DataAdmissao: string;
                Procedure Submit_DataNascimento(parametro: string);
                Function Get_DataNascimento: string;
                Procedure Submit_Cpf(parametro: string);
                Function Get_Cpf: string;
                Procedure Submit_RG(parametro: string);
                Function Get_RG: string;
                Procedure Submit_Fone(parametro: string);
                Function Get_Fone: string;
                Procedure Submit_Contato(parametro: string);
                Function Get_Contato: string;
                Procedure Submit_Celular(parametro: string);
                Function Get_Celular: string;
                Procedure Submit_CarteiraTrabalho(parametro: string);
                Function Get_CarteiraTrabalho: string;
                Procedure Submit_CNH(parametro: string);
                Function Get_CNH: string;
                Procedure Submit_EstadoCivil(parametro: string);
                Function Get_EstadoCivil: string;
                Procedure Submit_Endereco(parametro: string);
                Function Get_Endereco: string;
                Procedure Submit_Complemento(parametro: string);
                Function Get_Complemento: string;
                Procedure Submit_Bairro(parametro: string);
                Function Get_Bairro: string;
                Procedure Submit_Cidade(parametro: string);
                Function Get_Cidade: string;
                Procedure Submit_Estado(parametro: string);
                Function Get_Estado: string;
                Procedure Submit_Cep(parametro: string);
                Function Get_Cep: string;
                Procedure Submit_Banco(parametro: string);
                Function Get_Banco: string;
                Procedure Submit_Agencia(parametro: string);
                Function Get_Agencia: string;
                Procedure Submit_ContaCorrente(parametro: string);
                Function Get_ContaCorrente: string;
                Procedure Submit_CodigoPlanodeCOntas(parametro:string);
                Function  Get_CodigoPlanodeCOntas:string;

                Procedure Submit_senha(parametro:string);
                Function  Get_senha:string; 

                Procedure Submit_Ativo(parametro:string);
                Function  get_Ativo:string;
                Procedure Submit_PermiteUltrapassarAdiantamento(parametro:string);
                Function  Get_PermiteUltrapassarAdiantamento:string;
                Function  Get_NumeroDependentesSalFamilia:string;
                PRocedure Submit_NumeroDependentesSalFamilia(Parametro:string);

                Function  Get_NumeroDependentesIRPF:string;
                PRocedure Submit_NumeroDependentesIRPF(Parametro:string);


                procedure EdtCargoExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtCargoKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                procedure EdtportadorExit(Sender: TObject; LABELNOME: TLABEL);
                procedure EdtportadorKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState; LABELNOME: Tlabel);
                procedure EdtFuncionariosKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;EDIT:TEdit;LABELNOME:Tlabel);overload;
                procedure EdtFuncionariosKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState);overload;

                Procedure Imprime;
                //CODIFICA DECLARA GETSESUBMITS



         Private
               ObjDataset:Tibdataset;
               CODIGO:string;
               Nome:string;
               Salario:string;
               DataAdmissao:string;
               DataNascimento:string;
               Cpf:string;
               RG:string;
               Fone:string;
               Contato:string;
               Celular:string;
               CarteiraTrabalho:string;
               CNH:string;
               EstadoCivil:string;
               Endereco:string;
               Complemento:string;
               Bairro:string;
               Cidade:string;
               Estado:string;
               Cep:string;
               Banco:string;
               Agencia:string;
               ContaCorrente:string;
               CodigoPlanodeCOntas:string;

               ParametroPesquisa:TStringList;
               Ativo:string;
               PermiteUltrapassarAdiantamento:string;
               NumeroDependentesSalFamilia:string;
               NumeroDependentesIRPF:string;

               Senha:string;


                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
                Procedure ImprimeAniversariantes;

   End;


implementation
uses rdprint,UopcaoRel,Ufiltraimp,Upesquisa,SysUtils,Dialogs,UDatamodulo,Ibquery,Controls
,uCARGOFUNCIONARIO
//USES IMPLEMENTATION

, UReltxtRDPRINT, Uportador, Useg, Ufuncionarios;


{ TTabTitulo }


Function  TObjFUNCIONARIOS.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With ObjDataset do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.Nome:=fieldbyname('Nome').asstring;
        Self.Salario:=fieldbyname('Salario').asstring;
        Self.DataAdmissao:=fieldbyname('DataAdmissao').asstring;
        Self.DataNascimento:=fieldbyname('DataNascimento').asstring;
        Self.Cpf:=fieldbyname('Cpf').asstring;
        Self.RG:=fieldbyname('RG').asstring;
        Self.Fone:=fieldbyname('Fone').asstring;
        Self.Contato:=fieldbyname('Contato').asstring;
        Self.Celular:=fieldbyname('Celular').asstring;
        Self.CarteiraTrabalho:=fieldbyname('CarteiraTrabalho').asstring;
        Self.CNH:=fieldbyname('CNH').asstring;
        Self.EstadoCivil:=fieldbyname('EstadoCivil').asstring;
        Self.Endereco:=fieldbyname('Endereco').asstring;
        Self.Complemento:=fieldbyname('Complemento').asstring;
        Self.Bairro:=fieldbyname('Bairro').asstring;
        Self.Cidade:=fieldbyname('Cidade').asstring;
        Self.Estado:=fieldbyname('Estado').asstring;
        Self.Cep:=fieldbyname('Cep').asstring;
        Self.Ativo:=Fieldbyname('ativo').asstring;
        Self.PermiteUltrapassarAdiantamento:=Fieldbyname('PermiteUltrapassarAdiantamento').asstring;
        Self.Senha:=useg.DesincriptaSenha(Fieldbyname('senha').asstring);


        If(FieldByName('Cargo').asstring<>'')
        Then Begin
                 If (Self.Cargo.LocalizaCodigo(FieldByName('Cargo').asstring)=False)
                 Then Begin
                          Messagedlg('Cargo N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Cargo.TabelaparaObjeto;
        End;

        If(FieldByName('portador').asstring<>'')
        Then Begin
                 If (Self.portador.LocalizaCodigo(FieldByName('portador').asstring)=False)
                 Then Begin
                          Messagedlg('Portador N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.portador.TabelaparaObjeto;
        End;

        Self.Banco:=fieldbyname('Banco').asstring;
        Self.Agencia:=fieldbyname('Agencia').asstring;
        Self.ContaCorrente:=fieldbyname('ContaCorrente').asstring;
        Self.CodigoPlanodeCOntas:=fieldbyname('codigoplanodecontas').asstring;

        Self.NumeroDependentesSalFamilia:=fieldbyname('NumeroDependentesSalFamilia').asstring;
        Self.NumeroDependentesIRPF:=fieldbyname('NumeroDependentesIRPF').asstring;
        result:=True;
     End;
end;


Procedure TObjFUNCIONARIOS.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With ObjDataset do
  Begin
        fieldbyname('CODIGO').asstring:=Self.CODIGO;
        fieldbyname('Nome').asstring:=Self.Nome;
        fieldbyname('Salario').asstring:=Self.Salario;
        fieldbyname('DataAdmissao').asstring:=Self.DataAdmissao;
        fieldbyname('DataNascimento').asstring:=Self.DataNascimento;
        fieldbyname('Cpf').asstring:=Self.Cpf;
        fieldbyname('RG').asstring:=Self.RG;
        fieldbyname('Fone').asstring:=Self.Fone;
        fieldbyname('Contato').asstring:=Self.Contato;
        fieldbyname('Celular').asstring:=Self.Celular;
        fieldbyname('CarteiraTrabalho').asstring:=Self.CarteiraTrabalho;
        fieldbyname('CNH').asstring:=Self.CNH;
        fieldbyname('EstadoCivil').asstring:=Self.EstadoCivil;
        fieldbyname('Endereco').asstring:=Self.Endereco;
        fieldbyname('Complemento').asstring:=Self.Complemento;
        fieldbyname('Bairro').asstring:=Self.Bairro;
        fieldbyname('Cidade').asstring:=Self.Cidade;
        fieldbyname('Estado').asstring:=Self.Estado;
        fieldbyname('Cep').asstring:=Self.Cep;   
        fieldbyname('Cargo').asstring:=Self.Cargo.GET_CODIGO;
        fieldbyname('portador').asstring:=Self.portador.GET_CODIGO;
        fieldbyname('Banco').asstring:=Self.Banco;
        fieldbyname('Agencia').asstring:=Self.Agencia;
        fieldbyname('ContaCorrente').asstring:=Self.ContaCorrente;
        fieldbyname('codigoplanodecontas').asstring:=Self.CodigoPlanodeCOntas;
        Fieldbyname('ativo').asstring:=Self.Ativo;
        Fieldbyname('PermiteUltrapassarAdiantamento').asstring:=Self.PermiteUltrapassarAdiantamento;
        fieldbyname('NumeroDependentesSalFamilia').asstring:=Self.NumeroDependentesSalFamilia;
        fieldbyname('NumeroDependentesIRPF').asstring:=Self.NumeroDependentesIRPF;
        Fieldbyname('senha').asstring:=Useg.EncriptaSenha(Self.Senha);
  End;
End;

//***********************************************************************

function TObjFUNCIONARIOS.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin

  if (Self.VerificaBrancos=True)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaNumericos=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaData=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaFaixa=False)
  Then Begin
           result:=false;
           Exit;
       End;

  if (Self.VerificaRelacionamentos=False)
  Then Begin
           result:=false;
           Exit;
       End;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       result:=False;
                       exit;
                  End;
        End;




if Self.status=dsinsert
    then Self.ObjDataset.Insert//libera para insercao
    Else
       if (Self.Status=dsedit)
       Then Self.ObjDataset.edit//se for edicao libera para tal
       else Begin
                  mensagemerro('Status Inv�lido na Grava��o');
                  exit;
       End;

 Self.ObjetoParaTabela;
 Self.ObjDataset.Post;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;

end;

procedure TObjFUNCIONARIOS.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Nome:='';
        Salario:='';
        DataAdmissao:='';
        DataNascimento:='';
        Cpf:='';
        RG:='';
        Fone:='';
        Contato:='';
        Celular:='';
        CarteiraTrabalho:='';
        CNH:='';
        EstadoCivil:='';
        Endereco:='';
        Complemento:='';
        Bairro:='';
        Cidade:='';
        Estado:='';
        Cep:='';
        Cargo.ZerarTabela;
        portador.ZerarTabela;
        Banco:='';
        Agencia:='';
        ContaCorrente:='';
        Self.CodigoPlanodeCOntas:='';

        Self.Ativo:='';
        Self.PermiteUltrapassarAdiantamento:='';
        Self.NumeroDependentesSalFamilia:='';
        Self.NumeroDependentesIRPF:='';
        Senha:='';

     End;
end;

Function TObjFUNCIONARIOS.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';

     { If (Cargo.Get_codigo='')
      Then Mensagem:=mensagem+'/Cargo';

     { if (Self.PermiteUltrapassarAdiantamento='')
      Then Mensagem:=Mensagem+'/Permite Ultrapassar Adiantamento?';

      if (Self.NumeroDependentesSalFamilia='')
      Then Mensagem:=Mensagem+'/N�mero de Dependentes do Sal�rio Fam�lia';

      if (Self.NumeroDependentesIRPF='')
      Then Mensagem:=Mensagem+'/N�mero de Dependentes IRPF';   }

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            result:=true;
            exit;
       End;
   result:=false;
end;


function TObjFUNCIONARIOS.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     mensagem:='';

     if(Self.portador.Get_CODIGO<>'')then
     begin
        If (Self.Cargo.LocalizaCodigo(Self.Cargo.Get_CODIGO)=False)
        Then Mensagem:=mensagem+'/ Cargo n�o Encontrado!';
     end;

     if (Self.portador.get_codigo<>'')
     Then Begin
               If (Self.portador.LocalizaCodigo(Self.portador.Get_CODIGO)=False)
               Then Mensagem:=mensagem+'/ Portador n�o Encontrado!';
     End;


//CODIFICA VERIFICARELACIONAMENTOS

     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               result:=False;
               exit;
          End;
     result:=true;
End;

function TObjFUNCIONARIOS.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     {try
        Strtofloat(Self.Salario);
     Except
           Mensagem:=mensagem+'/Sal�rio';
     End;

     try
        If (Self.Cargo.Get_Codigo<>'')
        Then Strtoint(Self.Cargo.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Cargo';
     End;

     try
        If (Self.portador.Get_Codigo<>'')
        Then Strtoint(Self.portador.Get_Codigo);
     Except
           Mensagem:=mensagem+'/portador';
     End;

     try
          Strtoint(Self.NumeroDependentesSalFamilia);
     Except
           Mensagem:=mensagem+'/N�mero de Dependentes do Sal�rio Fam�lia';
     End;

     try
          Strtoint(Self.NumeroDependentesIRPF);
     Except
           Mensagem:=mensagem+'/N�mero de Dependentes IRPF';
     End; }
//CODIFICA VERIFICANUMERICOS

     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               result:=false;;
               exit;
          End;
     result:=true;

end;

function TObjFUNCIONARIOS.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     mensagem:='';
     try
        Strtodate(Self.DataAdmissao);
     Except
           Mensagem:=mensagem+'/Data de Admiss�o';
     End;
     try
        Strtodate(Self.DataNascimento);
     Except
           Mensagem:=mensagem+'/Data de Nascimento';
     End;
//CODIFICA VERIFICADATA

     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           result:=false;
           exit;
     End;
     result:=true;

end;

function TObjFUNCIONARIOS.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin

Try

     Mensagem:='';

     if (Self.Ativo<>'S') and (Self.Ativo<>'N')
     Then Mensagem:=mensagem+'Valor Inv�lido para o Campo Ativo';
      If not( (ESTADOCIVIL='C') OR (ESTADOCIVIL='S') OR (ESTADOCIVIL='O'))
      Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Estado Civil';

      if ((Self.PermiteUltrapassarAdiantamento<>'S') and (Self.PermiteUltrapassarAdiantamento<>'N'))  
      Then Mensagem:=mensagem+'/Valor Inv�lido para o campo Permite Ultrapassar Limite de Adiantamento';

     If mensagem<>''
     Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
               result:=false;
               exit;
          End;

     result:=true;

Finally

end;

end;

function TObjFUNCIONARIOS.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       With Self.ObjDataset do
       Begin

           close;
           SelectSql.Clear;
           SelectSQL.ADD('Select CODIGO,Nome,Salario,DataAdmissao,DataNascimento,Cpf');
           SelectSQL.ADD(' ,RG,Fone,Contato,Celular,CarteiraTrabalho,CNH,EstadoCivil');
           SelectSQL.ADD(' ,Endereco,Complemento,Bairro,Cidade,Estado,Cep,Cargo,Banco');
           SelectSQL.ADD(' ,Agencia,ContaCorrente,CodigoPlanodeCOntas,Ativo,PermiteUltrapassarAdiantamento,NumeroDependentesSalFamilia,NumeroDependentesIRPF,portador,senha');
           SelectSQL.ADD(' from  TABFUNCIONARIOS');
           SelectSQL.ADD(' WHERE CODIGO='+parametro);

//CODIFICA LOCALIZACODIGO

           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFUNCIONARIOS.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFUNCIONARIOS.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.ObjDataset.delete;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFUNCIONARIOS.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.ObjDataset:=Tibdataset.create(nil);
        Self.ObjDataset.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;
        Self.Portador:=TobjPortador.create;

        Self.Cargo:=TOBJCARGOFUNCIONARIO.create;
//CODIFICA CRIACAO DE OBJETOS


        ZerarTabela;

        With Self.ObjDataset do
        Begin

                SelectSQL.clear;
                SelectSQL.ADD('Select CODIGO,Nome,Salario,DataAdmissao,DataNascimento');
                SelectSQL.ADD(' ,Cpf,RG,Fone,Contato,Celular,CarteiraTrabalho,CNH,EstadoCivil');
                SelectSQL.ADD(' ,Endereco,Complemento,Bairro,Cidade,Estado,Cep,Cargo');
                SelectSQL.ADD(' ,Banco,Agencia,ContaCorrente,CodigoPlanodeCOntas,Ativo,PermiteUltrapassarAdiantamento,NumeroDependentesSalFamilia,NumeroDependentesIRPF');
                SelectSQL.ADD(' ,portador,senha from  TABFUNCIONARIOS');
                SelectSQL.ADD(' WHERE CODIGO=0');
//CODIFICA SELECTSQL

                Self.SqlInicial:=SelectSQL.text;
                InsertSQL.clear;
                InsertSQL.add('Insert Into TABFUNCIONARIOS(CODIGO,Nome,Salario,DataAdmissao');
                InsertSQL.add(' ,DataNascimento,Cpf,RG,Fone,Contato,Celular,CarteiraTrabalho');
                InsertSQL.add(' ,CNH,EstadoCivil,Endereco,Complemento,Bairro,Cidade');
                InsertSQL.add(' ,Estado,Cep,Cargo,Banco,Agencia,ContaCorrente,CodigoPlanodeCOntas,Ativo,PermiteUltrapassarAdiantamento,NumeroDependentesSalFamilia,NumeroDependentesIRPF,portador,senha )');
                InsertSQL.add('values (:CODIGO,:Nome,:Salario,:DataAdmissao,:DataNascimento');
                InsertSQL.add(' ,:Cpf,:RG,:Fone,:Contato,:Celular,:CarteiraTrabalho');
                InsertSQL.add(' ,:CNH,:EstadoCivil,:Endereco,:Complemento,:Bairro,:Cidade');
                InsertSQL.add(' ,:Estado,:Cep,:Cargo,:Banco,:Agencia,:ContaCorrente,:CodigoPlanodeCOntas,:Ativo,:PermiteUltrapassarAdiantamento,:NumeroDependentesSalFamilia,:NumeroDependentesIRPF,:portador,:senha');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABFUNCIONARIOS set CODIGO=:CODIGO,Nome=:Nome');
                ModifySQL.add(',Salario=:Salario,DataAdmissao=:DataAdmissao,DataNascimento=:DataNascimento');
                ModifySQL.add(',Cpf=:Cpf,RG=:RG,Fone=:Fone,Contato=:Contato,Celular=:Celular');
                ModifySQL.add(',CarteiraTrabalho=:CarteiraTrabalho,CNH=:CNH,EstadoCivil=:EstadoCivil');
                ModifySQL.add(',Endereco=:Endereco,Complemento=:Complemento,Bairro=:Bairro');
                ModifySQL.add(',Cidade=:Cidade,Estado=:Estado,Cep=:Cep,Cargo=:Cargo');
                ModifySQL.add(',Banco=:Banco,Agencia=:Agencia,ContaCorrente=:ContaCorrente,CodigoPlanodeCOntas=:CodigoPlanodeCOntas');
                ModifySQL.add(',Ativo=:Ativo,PermiteUltrapassarAdiantamento=:PermiteUltrapassarAdiantamento,NumeroDependentesSalFamilia=:NumeroDependentesSalFamilia,NumeroDependentesIRPF=:NumeroDependentesIRPF,portador=:portador ');
                ModifySQL.add(',senha=:senha where CODIGO=:CODIGO');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABFUNCIONARIOS where CODIGO=:CODIGO ');
//CODIFICA DELETESQL

                RefreshSQL.clear;
                RefreshSQL.ADD('Select CODIGO,Nome,Salario,DataAdmissao,DataNascimento');
                RefreshSQL.ADD(',Cpf,RG,Fone,Contato,Celular,CarteiraTrabalho,CNH');
                RefreshSQL.ADD(',EstadoCivil,Endereco,Complemento,Bairro,Cidade,Estado');
                RefreshSQL.ADD(',Cep,Cargo,Banco,Agencia,ContaCorrente,CodigoPlanodeCOntas,Ativo,PermiteUltrapassarAdiantamento,NumeroDependentesSalFamilia,NumeroDependentesIRPF,portador');
                RefreshSQL.ADD('from  TABFUNCIONARIOS');
                RefreshSQL.ADD('WHERE CODIGO=0');
//CODIFICA REFRESHSQL

                open;
                Self.ObjDataset.First ;
                Self.status          :=dsInactive;
        End;

end;
procedure TObjFUNCIONARIOS.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFUNCIONARIOS.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from ViewFUNCIONARIOS');
     Result:=Self.ParametroPesquisa;
end;

function TObjFUNCIONARIOS.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de Funcion�rios ';
end;


function TObjFUNCIONARIOS.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
           IbqueryGen.sql.add('SELECT GEN_ID(GENFUNCIONARIOS,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFUNCIONARIOS.Free;
begin
      Freeandnil(Self.ObjDataset);
      Freeandnil(Self.ParametroPesquisa);
      Self.Cargo.FREE;
      Self.portador.Free;
      //CODIFICA DESTRUICAO DE OBJETOS
end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFUNCIONARIOS.RetornaCampoCodigo: string;
begin
    result:='CODIGO';
    //CODIFICA RETORNACAMPOCODIGO
end;
//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFUNCIONARIOS.RetornaCampoNome: string;
begin
      result:='NOME';
      //CODIFICA RETORNACAMPONOME
end;

procedure TOBJFUNCIONARIOS.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TOBJFUNCIONARIOS.Submit_Nome(parametro: string);
begin
        Self.Nome:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Nome: string;
begin
        Result:=Self.Nome;
end;
procedure TOBJFUNCIONARIOS.Submit_Salario(parametro: string);
begin
        Self.Salario:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Salario: string;
begin
        Result:=Self.Salario;
end;
procedure TOBJFUNCIONARIOS.Submit_DataAdmissao(parametro: string);
begin
        Self.DataAdmissao:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_DataAdmissao: string;
begin
        Result:=Self.DataAdmissao;
end;
procedure TOBJFUNCIONARIOS.Submit_DataNascimento(parametro: string);
begin
        Self.DataNascimento:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_DataNascimento: string;
begin
        Result:=Self.DataNascimento;
end;
procedure TOBJFUNCIONARIOS.Submit_Cpf(parametro: string);
begin
        Self.Cpf:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Cpf: string;
begin
        Result:=Self.Cpf;
end;
procedure TOBJFUNCIONARIOS.Submit_RG(parametro: string);
begin
        Self.RG:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_RG: string;
begin
        Result:=Self.RG;
end;
procedure TOBJFUNCIONARIOS.Submit_Fone(parametro: string);
begin
        Self.Fone:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Fone: string;
begin
        Result:=Self.Fone;
end;
procedure TOBJFUNCIONARIOS.Submit_Contato(parametro: string);
begin
        Self.Contato:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Contato: string;
begin
        Result:=Self.Contato;
end;
procedure TOBJFUNCIONARIOS.Submit_Celular(parametro: string);
begin
        Self.Celular:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Celular: string;
begin
        Result:=Self.Celular;
end;
procedure TOBJFUNCIONARIOS.Submit_CarteiraTrabalho(parametro: string);
begin
        Self.CarteiraTrabalho:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_CarteiraTrabalho: string;
begin
        Result:=Self.CarteiraTrabalho;
end;
procedure TOBJFUNCIONARIOS.Submit_CNH(parametro: string);
begin
        Self.CNH:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_CNH: string;
begin
        Result:=Self.CNH;
end;
procedure TOBJFUNCIONARIOS.Submit_EstadoCivil(parametro: string);
begin
        Self.EstadoCivil:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_EstadoCivil: string;
begin
        Result:=Self.EstadoCivil;
end;
procedure TOBJFUNCIONARIOS.Submit_Endereco(parametro: string);
begin
        Self.Endereco:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Endereco: string;
begin
        Result:=Self.Endereco;
end;
procedure TOBJFUNCIONARIOS.Submit_Complemento(parametro: string);
begin
        Self.Complemento:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Complemento: string;
begin
        Result:=Self.Complemento;
end;
procedure TOBJFUNCIONARIOS.Submit_Bairro(parametro: string);
begin
        Self.Bairro:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Bairro: string;
begin
        Result:=Self.Bairro;
end;
procedure TOBJFUNCIONARIOS.Submit_Cidade(parametro: string);
begin
        Self.Cidade:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Cidade: string;
begin
        Result:=Self.Cidade;
end;
procedure TOBJFUNCIONARIOS.Submit_Estado(parametro: string);
begin
        Self.Estado:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Estado: string;
begin
        Result:=Self.Estado;
end;
procedure TOBJFUNCIONARIOS.Submit_Cep(parametro: string);
begin
        Self.Cep:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Cep: string;
begin
        Result:=Self.Cep;
end;
procedure TOBJFUNCIONARIOS.Submit_Banco(parametro: string);
begin
        Self.Banco:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Banco: string;
begin
        Result:=Self.Banco;
end;
procedure TOBJFUNCIONARIOS.Submit_Agencia(parametro: string);
begin
        Self.Agencia:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_Agencia: string;
begin
        Result:=Self.Agencia;
end;
procedure TOBJFUNCIONARIOS.Submit_ContaCorrente(parametro: string);
begin
        Self.ContaCorrente:=Parametro;
end;
function TOBJFUNCIONARIOS.Get_ContaCorrente: string;
begin
        Result:=Self.ContaCorrente;
end;
//CODIFICA GETSESUBMITS

procedure TObjFUNCIONARIOS.EdtCargoExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Cargo.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Cargo.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Cargo.GET_NOME;
End;
procedure TObjFUNCIONARIOS.EdtCargoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   FCARGOFUNCIONARIO:TFCARGOFUNCIONARIO;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            FCARGOFUNCIONARIO:=TFCARGOFUNCIONARIO.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Cargo.Get_Pesquisa,Self.Cargo.Get_TituloPesquisa,FcargoFuncionario)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cargo.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Cargo.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Cargo.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FCARGOFUNCIONARIO);
     End;
end;
//****************************************************************************
procedure TObjFUNCIONARIOS.EdtportadorExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.portador.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.portador.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.portador.GET_NOME;
End;

procedure TObjFUNCIONARIOS.EdtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   Fportador:TFportador;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);
            Fportador:=TFportador.create(nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.portador.Get_Pesquisa,Self.portador.Get_TituloPesquisa,Fportador)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                 LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(Fportador);
     End;
end;

function TObjFUNCIONARIOS.Get_CodigoPlanodeCOntas: string;
begin
     Result:=Self.CodigoPlanodeCOntas;
end;

procedure TObjFUNCIONARIOS.Submit_CodigoPlanodeCOntas(parametro: string);
begin
     Self.CodigoPlanodeCOntas:=parametro;
end;

function TObjFUNCIONARIOS.get_Ativo: string;
begin
     result:=Self.Ativo;
end;

procedure TObjFUNCIONARIOS.Submit_Ativo(parametro: string);
begin
     Self.Ativo:=parametro;
end;

function TObjFUNCIONARIOS.Get_PermiteUltrapassarAdiantamento: string;
begin
     Result:=Self.PermiteUltrapassarAdiantamento;
end;

procedure TObjFUNCIONARIOS.Submit_PermiteUltrapassarAdiantamento(
  parametro: string);
begin
     Self.PermiteUltrapassarAdiantamento:=parametro;
end;

procedure TObjFUNCIONARIOS.Imprime;
begin

     With FOpcaorel do
     Begin
          RgOpcoes.Items.clear;
          RgOpcoes.Items.add('Relat�rio de Aniversariantes');
          showmodal;
          if (tag=0)
          Then exit;

          case RgOpcoes.ItemIndex of
          0:Self.ImprimeAniversariantes;
          End;

     End;
end;

procedure TObjFUNCIONARIOS.ImprimeAniversariantes;
var
TMes:Integer;
VMes:array[1..12] of string;
linha:integer;
begin
     Vmes[1]:='JANEIRO';Vmes[2]:='FEVEREIRO';Vmes[3]:='MAR�O';
     Vmes[4]:='ABRIL';Vmes[5]:='MAIO';Vmes[6]:='JUNHO';
     Vmes[7]:='JULHO';Vmes[8]:='AGOSTO';Vmes[9]:='SETEMBRO';
     Vmes[10]:='OUTUBRO';Vmes[11]:='NOVEMBRO';Vmes[12]:='DEZEMBRO';

     With FfiltroImp do
     Begin
          DesativaGrupos;
          Grupo05.Enabled:=True;
          edtgrupo05.text:='';
          LbGrupo05.caption:='M�S';
          edtgrupo05.OnKeyDown:=nil;

          ShowModal;

          If tag=0
          Then exit;

          Try
             TMes:=StrToint(edtgrupo05.text);
             If not(TMes in[1..12])
             Then Begin
                       Messagedlg('M�s Inv�lido!',mterror,[mbok],0);
                       exit;
                  End;
          Except
                Messagedlg('M�s Inv�lido!',mterror,[mbok],0);
                exit;

          End;
          
     End;

     With Self.ObjDataset do
     Begin
          close;
          SelectSQL.clear;
          SelectSQL.add('select  TabFuncionarios.Codigo,TabFuncionarios.nome,TabFuncionarios.DataNascimento,EXTRACT(MONTH FROM DATANASCIMENTO) from TabFuncionarios');
          SelectSQL.add('where EXTRACT(MONTH FROM DATANASCIMENTO) = '+Inttostr(Tmes));
          SelectSQL.add('order by 3');//ordenando pelo dia
          Open;

          If (RecordCount=0)
          Then Begin
                    Messagedlg('N�o h� dados para serem impressos!',mtinformation,[mbok],0);
                    exit;
          End;

          FreltxtRDPRINT.ConfiguraImpressao;
          FreltxtRDPRINT.RDprint.Abrir;
          if (FreltxtRDPRINT.RDprint.Setup=False)
          Then Begin
                    FreltxtRDPRINT.RDprint.Fechar;
                    exit;
          End;
          linha:=3;

          FreltxtRDPRINT.RDprint.Impc(linha,45,'ANIVERSARIANTES DO M�S '+vmes[tmes],[negrito]);
          inc(linha,2);
          FreltxtRDPRINT.RDprint.Impf(linha,1,CompletaPalavra('C�DIGO',9,' ')+' '+
                                               CompletaPalavra('NOME',50,' ')+' '+
                                               'DATA NASCIMENTO',[negrito]);
          inc(linha,1);
          FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra('_',90,'_'));
          inc(linha,1);

          While not(eof) do
          Begin
               FreltxtRDPRINT.VerificaLinha(FreltxtRDPRINT.RDprint,linha);
               FreltxtRDPRINT.RDprint.Imp(linha,1,CompletaPalavra(fieldbyname('codigo').asstring,9,' ')+' '+
                                                  CompletaPalavra(fieldbyname('nome').asstring,50,' ')+' '+
                                                  fieldbyname('datanascimento').asstring);
               inc(linha,1);
               next;
          End;
          FreltxtRDPRINT.RDprint.Fechar;
     End;
end;

function TObjFUNCIONARIOS.Get_NumeroDependentesSalFamilia: string;
begin
     Result:=Self.NumeroDependentesSalFamilia;
end;

procedure TObjFUNCIONARIOS.Submit_NumeroDependentesSalFamilia(Parametro: string);
begin
     Self.NumeroDependentesSalFamilia:=parametro;
end;

function TObjFUNCIONARIOS.Get_NumeroDependentesIRPF: string;
begin
     Result:=Self.NumeroDependentesIRPF;
end;

procedure TObjFUNCIONARIOS.Submit_NumeroDependentesIRPF(Parametro: string);
begin
     Self.NumeroDependentesIRPF:=Parametro;
end;

function TObjFUNCIONARIOS.Get_senha: string;
begin
     Result:=Self.Senha;
end;

procedure TObjFUNCIONARIOS.Submit_senha(parametro: string);
begin
     Self.Senha:=Parametro;
end;

procedure TObjFUNCIONARIOS.EdtFuncionariosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;EDIT:TEdit;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
   ffuncionario:TFfuncionarios;
begin

  if (key <>vk_f9) then
    exit;

  try
    Fpesquisalocal:=Tfpesquisa.create(Nil);
    ffuncionario:=TFfuncionarios.Create(nil);

    if (FpesquisaLocal.PreparaPesquisa(self.Get_Pesquisa,Self.Get_TituloPesquisa,ffuncionario)=True) then
    begin
      try
        if (FpesquisaLocal.showmodal=mrok) then
        begin
          tedit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
          laBELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
        end;
      finally
        fpesquisaLocal.QueryPesq.close;
      end;
    end;

  finally
    FreeandNil(FPesquisaLocal);
    FreeandNil(ffuncionario);
  end;
end;

procedure TObjFUNCIONARIOS.EdtFuncionariosKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
  // Ffuncionarios:T
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(self.Get_Pesquisa,Self.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                // LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);

     End;
end;

end.



