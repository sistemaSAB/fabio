unit UobjFAIXADESCONTO_COMISSAO;
Interface
Uses Ibquery,windows,stdctrls,Classes,Db,UessencialGlobal,UobjFaixaDesconto;

Type
   TObjFAIXADESCONTO_COMISSAO=class

          Public
                ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
                Faixa:TOBJFAIXADESCONTO;
//CODIFICA VARIAVEIS PUBLICAS


                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                function    Localiza_desconto(PFaixa, Pdesconto: string): boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_DescontoInicial(parametro: string);
                Function Get_DescontoInicial: string;
                Procedure Submit_DescontoFinal(parametro: string);
                Function Get_DescontoFinal: string;
                Procedure Submit_Comissao(parametro: string);
                Function Get_Comissao: string;
                procedure EdtFaixaExit(Sender: TObject;LABELNOME:TLABEL);
                procedure EdtFaixaKeyDown(Sender: TObject; var Key: Word;Shift: TShiftState;LABELNOME:Tlabel);
                Procedure ResgataFaixaComissao(PFaixa:string);

//CODIFICA DECLARA GETSESUBMITS


         Private
               Objquery:Tibquery;
               ObjQueryPesquisa:TIBQuery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               DescontoInicial:string;
               DescontoFinal:string;
               Comissao:string;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;

   End;


implementation
uses Upesquisa,SysUtils,Dialogs,UDatamodulo,Controls

//USES IMPLEMENTATION

, UMenuRelatorios;


{ TTabTitulo }


Function  TObjFAIXADESCONTO_COMISSAO.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        If(FieldByName('Faixa').asstring<>'')
        Then Begin
                 If (Self.Faixa.LocalizaCodigo(FieldByName('Faixa').asstring)=False)
                 Then Begin
                          Messagedlg('Faixa N�o encontrado(a)!',mterror,[mbok],0);
                          Self.ZerarTabela;
                          result:=False;
                          exit;
                 End
                 Else Self.Faixa.TabelaparaObjeto;
        End;
        Self.DescontoInicial:=fieldbyname('DescontoInicial').asstring;
        Self.DescontoFinal:=fieldbyname('DescontoFinal').asstring;
        Self.Comissao:=fieldbyname('Comissao').asstring;
//CODIFICA TABELAPARAOBJETO





        result:=True;
     End;
end;


Procedure TObjFAIXADESCONTO_COMISSAO.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring:=Self.CODIGO;
        ParamByName('Faixa').asstring:=Self.Faixa.GET_CODIGO;
        ParamByName('DescontoInicial').asstring:=virgulaparaponto(Self.DescontoInicial);
        ParamByName('DescontoFinal').asstring:=virgulaparaponto(Self.DescontoFinal);
        ParamByName('Comissao').asstring:=virgulaparaponto(Self.Comissao);
//CODIFICA OBJETOPARATABELA





  End;
End;

//***********************************************************************

function TObjFAIXADESCONTO_COMISSAO.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFAIXADESCONTO_COMISSAO.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        Faixa.ZerarTabela;
        DescontoInicial:='';
        DescontoFinal:='';
        Comissao:='';
//CODIFICA ZERARTABELA





     End;
end;

Function TObjFAIXADESCONTO_COMISSAO.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (Faixa.Get_CODIGO='')
      Then Mensagem:=mensagem+'/Faixa';
      If (DescontoInicial='')
      Then Mensagem:=mensagem+'/Desconto Inicial';
      If (DescontoFinal='')
      Then Mensagem:=mensagem+'/Desconto Final';
      If (Comissao='')
      Then Mensagem:=mensagem+'/Comiss�o';
      //CODIFICA VERIFICABRANCOS

  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFAIXADESCONTO_COMISSAO.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
      If (Self.Faixa.LocalizaCodigo(Self.Faixa.Get_CODIGO)=False)
      Then Mensagem:=mensagem+'/ Faixa n�o Encontrado!';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFAIXADESCONTO_COMISSAO.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        If (Self.Faixa.Get_Codigo<>'')
        Then Strtoint(Self.Faixa.Get_Codigo);
     Except
           Mensagem:=mensagem+'/Faixa';
     End;
     try
        Strtofloat(Self.DescontoInicial);
     Except
           Mensagem:=mensagem+'/Desconto Inicial';
     End;
     try
        Strtofloat(Self.DescontoFinal);
     Except
           Mensagem:=mensagem+'/Desconto Final';
     End;
     try
        Strtofloat(Self.Comissao);
     Except
           Mensagem:=mensagem+'/Comiss�o';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFAIXADESCONTO_COMISSAO.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFAIXADESCONTO_COMISSAO.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;
function TObjFAIXADESCONTO_COMISSAO.Localiza_desconto(PFaixa,
  Pdesconto: string): boolean;
begin
     result:=false;
     if(PFaixa='')
     then PFaixa:='1';

     if(pdesconto='')
     then Pdesconto:='0';

     with self.Objquery do
     Begin
          close;
          sql.clear;
          sql.add('Select CODIGO,Faixa,DescontoInicial,DescontoFinal,Comissao');
          SQL.ADD('from  TABFAIXADESCONTO_COMISSAO');
          sql.add('where Faixa='+Pfaixa+' and DescontoInicial<='+virgulaparaponto(formata_valor_4_casas(Pdesconto))+' and DescontoFinal>='+virgulaparaponto(formata_valor_4_casas(pdesconto)));
          //InputBox('','',sql.Text);
          open;
          if (Recordcount=0)
          Then exit
          Else Result:=True;      

     End;

end;

function TObjFAIXADESCONTO_COMISSAO.LocalizaCodigo(parametro: string): boolean;//ok
//usado para localizar dados pela chave primaria (CODIGO)
//aqui vaum todos os campos que se deseja recuperar
//usado tmbm no salvar para verificar se os dados ja existem
//ou naum
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FAIXADESCONTO_COMISSAO vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,Faixa,DescontoInicial,DescontoFinal,Comissao');
           SQL.ADD(' from  TABFAIXADESCONTO_Comissao');
           SQL.ADD(' WHERE codigo='+parametro);
           
//CODIFICA LOCALIZACODIGO


           Open;

           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFAIXADESCONTO_COMISSAO.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFAIXADESCONTO_COMISSAO.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFAIXADESCONTO_COMISSAO.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ObjqueryPesquisa:=TIBQuery.create(nil);
        Self.ObjqueryPesquisa.Database:=FDataModulo.IbDatabase;
        Self.ObjDatasource:=TDataSource.Create(nil);
        Self.ObjDatasource.DataSet:=Self.ObjQueryPesquisa;
        

        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
        Self.Faixa:=TOBJFAIXADESCONTO.create;
//CODIFICA CRIACAO DE OBJETOS


        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TABFAIXADESCONTO_Comissao(CODIGO,Faixa,DescontoInicial');
                InsertSQL.add(' ,DescontoFinal,Comissao)');
                InsertSQL.add('values (:CODIGO,:Faixa,:DescontoInicial,:DescontoFinal');
                InsertSQL.add(' ,:Comissao)');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TABFAIXADESCONTO_Comissao set CODIGO=:CODIGO');
                ModifySQL.add(',Faixa=:Faixa,DescontoInicial=:DescontoInicial,DescontoFinal=:DescontoFinal');
                ModifySQL.add(',Comissao=:Comissao');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TABFAIXADESCONTO_Comissao where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFAIXADESCONTO_COMISSAO.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFAIXADESCONTO_COMISSAO.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFAIXADESCONTO_COMISSAO');
     Result:=Self.ParametroPesquisa;
end;

function TObjFAIXADESCONTO_COMISSAO.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FAIXADESCONTO_COMISSAO ';
end;


function TObjFAIXADESCONTO_COMISSAO.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFAIXADESCONTO_COMISSAO,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFAIXADESCONTO_COMISSAO,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFAIXADESCONTO_COMISSAO.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ObjqueryPesquisa);
    Freeandnil(Self.objdatasource);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    Self.Faixa.FREE;
//CODIFICA DESTRUICAO DE OBJETOS


end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFAIXADESCONTO_COMISSAO.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFAIXADESCONTO_COMISSAO.RetornaCampoNome: string;
begin
      result:='Nome';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFAIXADESCONTO_Comissao.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjFAIXADESCONTO_Comissao.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjFAIXADESCONTO_Comissao.Submit_DescontoInicial(parametro: string);
begin
        Self.DescontoInicial:=Parametro;
end;
function TObjFAIXADESCONTO_Comissao.Get_DescontoInicial: string;
begin
        Result:=Self.DescontoInicial;
end;
procedure TObjFAIXADESCONTO_Comissao.Submit_DescontoFinal(parametro: string);
begin
        Self.DescontoFinal:=Parametro;
end;
function TObjFAIXADESCONTO_Comissao.Get_DescontoFinal: string;
begin
        Result:=Self.DescontoFinal;
end;
procedure TObjFAIXADESCONTO_Comissao.Submit_Comissao(parametro: string);
begin
        Self.Comissao:=Parametro;
end;
function TObjFAIXADESCONTO_Comissao.Get_Comissao: string;
begin
        Result:=Self.Comissao;
end;
//CODIFICA GETSESUBMITS


procedure TObjFAIXADESCONTO_COMISSAO.EdtFaixaExit(Sender: TObject;LABELNOME:TLABEL);
Begin
     labelnome.caption:='';
     If (Tedit(Sender).text='')
     Then exit;

     If (Self.Faixa.localizacodigo(TEdit(Sender).text)=False)
     Then Begin
               TEdit(Sender).text:='';
               exit;
     End;
     Self.Faixa.tabelaparaobjeto;
     LABELNOME.CAPTION:=Self.Faixa.GET_NOME;
End;
procedure TObjFAIXADESCONTO_COMISSAO.EdtFaixaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState;LABELNOME:Tlabel);
var
   FpesquisaLocal:Tfpesquisa;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Nil);

            If (FpesquisaLocal.PreparaPesquisa(Self.Faixa.Get_Pesquisa,Self.Faixa.Get_TituloPesquisa,nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                 TEdit(Sender).text:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Faixa.RETORNACAMPOCODIGO).asstring;
                                 If ((TEdit(Sender).text<>'') and (LabelNome<>NIl)) 
                                 Then Begin
                                        If Self.Faixa.RETORNACAMPONOME<>'' 
                                        Then LABELNOME.caption:=FpesquisaLocal.QueryPesq.fieldbyname(Self.Faixa.RETORNACAMPONOME).asstring
                                        Else LABELNOME.caption:='';
                                 End;
                             End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
     End;
end;
//CODIFICA EXITONKEYDOWN

procedure TObjFAIXADESCONTO_COMISSAO.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFAIXADESCONTO_COMISSAO';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
            0:Begin

            end;
          End;
     end;

end;



procedure TObjFAIXADESCONTO_COMISSAO.ResgataFaixaComissao(PFaixa: string);
begin
     With Self.ObjQueryPesquisa do
     Begin
          close;
          sql.clear;
          sql.add('Select * from TabFaixaDesconto_Comissao where Faixa='+pfaixa);
          sql.add('ORDER BY descontoinicial');

          if (Pfaixa='')
          Then exit;
          open;
     End;
end;


end.



