unit Ufuncionarios;

interface

uses
Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Tabnotbk, Mask, StdCtrls, Buttons, ExtCtrls,db, UObjfuncionarios,IBQuery,UcargoFuncionario;

type
  TFfuncionarios = class(TForm)
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorios: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb2: TLabel;
    pgcGuia: TPageControl;
    tsPrincipal: TTabSheet;
    panel2: TPanel;
    lb3: TLabel;
    lb5: TLabel;
    lb6: TLabel;
    lb7: TLabel;
    lb8: TLabel;
    lb9: TLabel;
    lb10: TLabel;
    lb11: TLabel;
    lb12: TLabel;
    lb13: TLabel;
    lb14: TLabel;
    lb15: TLabel;
    lb16: TLabel;
    lb17: TLabel;
    lb22: TLabel;
    lb30: TLabel;
    cbbcomboativo: TComboBox;
    edtnome: TEdit;
    edtrg: TEdit;
    edtcpf: TEdit;
    cbbcomboestadocivil: TComboBox;
    edtdatanascimento: TMaskEdit;
    edtcidade: TEdit;
    edtbairro: TEdit;
    edtendereco: TEdit;
    edtcep: TEdit;
    edtcomplemento: TEdit;
    edtfone: TEdit;
    edtcelular: TEdit;
    edtcontato: TEdit;
    edtcnh: TEdit;
    edtestado: TEdit;
    tsDP: TTabSheet;
    panel3: TPanel;
    lb31: TLabel;
    lb29: TLabel;
    lb24: TLabel;
    lb28: TLabel;
    edtportador: TEdit;
    edtcodigoplanodecontas: TEdit;
    cbbcombopermiteultrapassarlimiteadiantamento: TComboBox;
    edtNumeroDependentesSalFamilia: TEdit;
    lbNomePlanodeContas_Adiantamento: TLabel;
    lbnomeportador: TLabel;
    lb27: TLabel;
    edtNumeroDependentesIRPF: TEdit;
    lb18: TLabel;
    lb21: TLabel;
    edtcarteiratrabalho: TEdit;
    edtcontacorrente: TEdit;
    edtbanco: TEdit;
    lb20: TLabel;
    lb23: TLabel;
    edtsalario: TEdit;
    lb25: TLabel;
    lb19: TLabel;
    edtagencia: TEdit;
    edtdataadmissao: TMaskEdit;
    lb26: TLabel;
    edtcargo: TEdit;
    lbcargo: TLabel;
    lb4: TLabel;
    edtsenha: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure BtnovoClick(Sender: TObject);
    procedure btalterarClick(Sender: TObject);
    procedure btgravarClick(Sender: TObject);
    procedure btexcluirClick(Sender: TObject);
    procedure btcancelarClick(Sender: TObject);
    procedure btsairClick(Sender: TObject);
    procedure btpesquisarClick(Sender: TObject);
    procedure edtcargoKeyPress(Sender: TObject; var Key: Char);
    procedure edtcargoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtcargoExit(Sender: TObject);
    procedure edtcodigoplanodecontasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btrelatoriosClick(Sender: TObject);
    procedure edtportadorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtportadorExit(Sender: TObject);
    procedure edtcodigoplanodecontasExit(Sender: TObject);
    procedure edtcodigoplanodecontasKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtportadorKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure lbcargoMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lbcargoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
         Function  ControlesParaObjeto:Boolean;
         Function  ObjetoParaControles:Boolean;
         Function  TabelaParaControles:Boolean;
         procedure MostraQuantidadeCadastrada;
         procedure Limpaedit;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Ffuncionarios: TFfuncionarios;
  Objfuncionarios:TObjfuncionarios;

implementation

uses UessencialGlobal, Upesquisa, UPLanodeContas, UDataModulo,
  UescolheImagemBotao, Uprincipal;



{$R *.DFM}
//****************************************
//*************MANIPULACAO DO OBJETO******
//****************************************
function TFfuncionarios.ControlesParaObjeto: Boolean;
Begin
  Try
    With Objfuncionarios do
    Begin
         Submit_CODIGO          ( lbCodigo.Caption);
         Submit_Nome(edtnome.text);
         Submit_Salario(edtsalario.text);
         Submit_DataAdmissao(edtdataadmissao.text);
         Submit_DataNascimento(edtdatanascimento.text);
         Submit_Cpf(edtcpf.text);
         Submit_RG(edtrg.text);
         Submit_Fone(edtfone.text);
         Submit_Contato(edtcontato.text);
         Submit_Celular(edtcelular.text);
         Submit_CarteiraTrabalho(edtcarteiratrabalho.text);
         Submit_CNH(edtcnh.text);
         Submit_EstadoCivil(cbbcomboestadocivil.text[1]);
         Submit_Endereco(edtendereco.text);
         Submit_Complemento(edtcomplemento.text);
         Submit_Bairro(edtbairro.text);
         Submit_Cidade(edtcidade.text);
         Submit_Estado(edtestado.text);
         Submit_Cep(edtcep.text);
         Submit_Banco(edtbanco.text);
         Submit_Agencia(edtagencia.text);
         Submit_ContaCorrente(edtcontacorrente.text);
         Cargo.Submit_CODIGO(EdtCargo.text);
         Submit_CodigoPlanodeCOntas(edtcodigoplanodecontas.text);

         Submit_Ativo(cbbcomboativo.text[1]);
         Submit_PermiteUltrapassarAdiantamento(cbbcombopermiteultrapassarlimiteadiantamento.text[1]);
         Submit_NumeroDependentesSalFamilia(edtNumeroDependentesSalFamilia.Text);
         Submit_NumeroDependentesIRPF(edtNumeroDependentesIRPF.Text);
         Portador.Submit_codigo(edtportador.text);
         Submit_senha(edtsenha.text);

         result:=true;
    End;
  Except
        result:=False;
  End;
End;

function TFfuncionarios.ObjetoParaControles: Boolean;
Begin
  Try
     With Objfuncionarios do
     Begin
        lbCodigo.Caption         :=Get_CODIGO            ;
         edtNome               .text          :=Get_Nome              ;
         edtSalario            .text          :=Get_Salario           ;
         edtDataAdmissao       .text          :=Get_DataAdmissao      ;
         edtDataNascimento     .text          :=Get_DataNascimento    ;
         edtCpf                .text          :=Get_Cpf               ;
         edtRG                 .text          :=Get_RG                ;
         edtFone               .text          :=Get_Fone              ;
         edtContato            .text          :=Get_Contato           ;
         edtCelular            .text          :=Get_Celular           ;
         edtCarteiraTrabalho   .text          :=Get_CarteiraTrabalho  ;
         edtCNH                .text          :=Get_CNH               ;
         If (Get_EstadoCivil='S')
         Then cbbcomboestadocivil.itemindex:=0
         Else Begin
                If (get_estadocivil='C')
                Then cbbcomboestadocivil.itemindex:=1
                Else cbbcomboestadocivil.itemindex:=2;
         End;


         edtEndereco           .text          :=Get_Endereco          ;
         edtComplemento        .text          :=Get_Complemento       ;
         edtBairro             .text          :=Get_Bairro            ;
         edtCidade             .text          :=Get_Cidade            ;
         edtEstado             .text          :=Get_Estado            ;
         edtCep                .text          :=Get_Cep               ;
         edtBanco              .text          :=Get_Banco             ;
         edtAgencia            .text          :=Get_Agencia           ;
         edtContaCorrente      .text          :=Get_ContaCorrente     ;
         edtcargo.text                        :=Cargo.get_codigo;
         lbcargo.Caption:=Cargo.Get_Nome;
         edtcodigoplanodecontas.text          :=Get_CodigoPlanodeCOntas;

         if (get_ativo='S')
         Then cbbcomboativo.ItemIndex:=1
         Else cbbcomboativo.ItemIndex:=0;

         if (Get_PermiteUltrapassarAdiantamento='S')
         Then cbbcombopermiteultrapassarlimiteadiantamento.ItemIndex:=1
         Else cbbcombopermiteultrapassarlimiteadiantamento.ItemIndex:=0;

         edtNumeroDependentesSalFamilia.Text:=Get_NumeroDependentesSalFamilia;
         edtNumeroDependentesIRPF.Text:=Get_NumeroDependentesIRPF;
         edtportador.text:=Portador.Get_Codigo;
         edtsenha.text:=Get_senha;

         result:=True;
     End;
  Except
        Result:=False;
  End;
End;

function TFfuncionarios.TabelaParaControles: Boolean;
begin
     If (Objfuncionarios.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;
end;





procedure TFfuncionarios.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (Objfuncionarios=Nil)
     Then exit;

If (Objfuncionarios.status<>dsinactive)
Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

Objfuncionarios.free;
end;

procedure TFfuncionarios.FormKeyPress(Sender: TObject; var Key: Char);
begin
      if key=#13
      Then Perform(Wm_NextDlgCtl,0,0);
end;



procedure TFfuncionarios.BtnovoClick(Sender: TObject);
begin
     pgcGuia.TabIndex:=0;
     limpaedit;
     lbcargo.Caption:='';
     lbnomeportador.caption:='';
     LbNomePlanodeContas_Adiantamento.caption:='';
     habilita_campos(Self);
     //desab_botoes(Self);

     //edtcodigo.text:='0';
     lbCodigo.Caption:=Objfuncionarios.Get_novocodigo;



     Btsalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     Objfuncionarios.status:=dsInsert;

     cbbcomboativo.setfocus;

     btnovo.Visible:=false;
     btalterar.Visible:=False;
     btexcluir.Visible:=false;
     btrelatorios.Visible:=false;
     btopcoes.Visible:=false;
     btsair.Visible:=False;
     btpesquisar.visible:=False;
     edtdataadmissao.Text:=DateToStr(Now);

end;


procedure TFfuncionarios.btalterarClick(Sender: TObject);
begin
    If (Objfuncionarios.Status=dsinactive) and (lbCodigo.Caption<>'')
    Then Begin
                pgcGuia.TabIndex:=0;
                habilita_campos(Self);

                Objfuncionarios.Status:=dsEdit;

                cbbcomboativo.setfocus;
                //desab_botoes(Self);
                Btsalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;

                  btnovo.Visible:=false;
                 btalterar.Visible:=False;
                 btexcluir.Visible:=false;
                 btrelatorios.Visible:=false;
                 btopcoes.Visible:=false;
                 btsair.Visible:=False;
                 btpesquisar.visible:=False;
          End;


end;

procedure TFfuncionarios.btgravarClick(Sender: TObject);
begin

     If Objfuncionarios.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (Objfuncionarios.salvar(true)=False)
     Then exit;

     habilita_botoes(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     desabilita_campos(Self);
     MostraQuantidadeCadastrada;
end;

procedure TFfuncionarios.btexcluirClick(Sender: TObject);
begin
     If (Objfuncionarios.status<>dsinactive) or (lbCodigo.Caption='')
     Then exit;

     If (Objfuncionarios.LocalizaCodigo(lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;     

     If (Objfuncionarios.exclui(lbCodigo.Caption,true)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit;
     lbnomeportador.caption:='';
     lbcargo.Caption:='';
     LbNomePlanodeContas_Adiantamento.caption:='';
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);


end;

procedure TFfuncionarios.btcancelarClick(Sender: TObject);
begin
     Objfuncionarios.cancelar;

     limpaedit;
     lbcargo.Caption:='';
     lbnomeportador.caption:='';
     LbNomePlanodeContas_Adiantamento.caption:='';
     desabilita_campos(Self);
     habilita_botoes(Self);
     btnovo.Visible:=true;
     btalterar.Visible:=true;
     btexcluir.Visible:=true;
     btrelatorios.Visible:=true;
     btopcoes.Visible:=true;
     btsair.Visible:=true;
     btpesquisar.visible:=true;
     lbcodigo.Caption:='';

end;

procedure TFfuncionarios.btsairClick(Sender: TObject);
begin
    Close;
end;

procedure TFfuncionarios.btpesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(Objfuncionarios.Get_pesquisa,Objfuncionarios.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If Objfuncionarios.status<>dsinactive
                                  then exit;

                                  If (Objfuncionarios.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  Objfuncionarios.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit;
                                            lbnomeportador.caption:='';
                                            lbcargo.Caption:='';
                                            LbNomePlanodeContas_Adiantamento.caption:='';
                                            exit;
                                       End; 

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;


end;


procedure TFfuncionarios.edtcargoKeyPress(Sender: TObject; var Key: Char);
begin
     if not (key in ['0'..'9',#8])
     Then key:=#0;
end;

procedure TFfuncionarios.edtcargoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     Objfuncionarios.EdtCargoKeyDown(sender,key,shift,lbcargo);
end;

procedure TFfuncionarios.edtcargoExit(Sender: TObject);
begin
     Objfuncionarios.EdtCargoExit(sender,lbcargo);
end;

procedure TFfuncionarios.edtcodigoplanodecontasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   FpesquisaLocal:Tfpesquisa;
   FplanodeCOntas:TFplanodeCOntas;
begin

     If (key <>vk_f9)
     Then exit;

     Try
            Fpesquisalocal:=Tfpesquisa.create(Self);
            FplanodeCOntas:=TFplanodeCOntas.create(NIl);
            If (FpesquisaLocal.PreparaPesquisa(ObjLanctoPortadorGlobal.Portador.PlanodeContas.Get_Pesquisa,ObjLanctoPortadorGlobal.Portador.PlanodeContas.Get_TituloPesquisa,FplanodeCOntas)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  edtCodigoPlanodeContas.text:=FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring;
                                  LbNomePlanodeContas_Adiantamento.caption:=FpesquisaLocal.QueryPesq.fieldbyname('nome').asstring;
                        End;
                      Finally
                             FpesquisaLocal.QueryPesq.close;
                      End;
                  End;

     Finally
           FreeandNil(FPesquisaLocal);
           Freeandnil(FplanodeCOntas);
     End;


end;

procedure TFfuncionarios.btrelatoriosClick(Sender: TObject);
begin
     ObjFuncionarios.Imprime;
end;

procedure TFfuncionarios.edtportadorKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjFuncionarios.EdtportadorKeyDown(sender,key,shift,Lbnomeportador);
end;

procedure TFfuncionarios.edtportadorExit(Sender: TObject);
begin
     ObjFuncionarios.EdtportadorExit(sender,Lbnomeportador);
end;

procedure TFfuncionarios.edtcodigoplanodecontasExit(Sender: TObject);
begin
     LbNomePlanodeContas_Adiantamento.caption:='';
end;

procedure TFfuncionarios.edtcodigoplanodecontasKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;

end;

procedure TFfuncionarios.edtportadorKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;

end;

procedure TFfuncionarios.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabfuncionarios');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb2.Caption:='Existem '+IntToStr(contador)+' funcion�rios cadastrados'
       else
       begin
            lb2.Caption:='Existe '+IntToStr(contador)+' funcion�rio cadastrado';
       end;

    finally

    end;



end;

procedure TFfuncionarios.FormShow(Sender: TObject);
begin
      limpaedit;
     lbcargo.Caption:='';
     lbnomeportador.caption:='';
     LbNomePlanodeContas_Adiantamento.caption:='';
     //desabilita_campos(Self);

     pgcGuia.TabIndex:=0;
     
     Try
        Objfuncionarios:=TObjfuncionarios.create;
     Except
           Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
           Self.close;
     End;
     FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorios,btexcluir,btsair,btopcoes);
     FescolheImagemBotao.PegaFiguraImagem(imgrodape,'RODAPE');
     if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE FUNCIONARIOS')=False)
     Then desab_botoes(Self)
     Else habilita_botoes(Self);
     lbCodigo.Caption:='';
     desabilita_campos(Self);

     MostraQuantidadeCadastrada;

     if(Tag<>0)
     then begin
           if(Objfuncionarios.LocalizaCodigo(IntToStr(Tag))=True)then
           begin
                Objfuncionarios.TabelaparaObjeto;
                self.ObjetoParaControles;
           end;

     end;

      Self.Color:=clBtnFace;
end;

procedure TFfuncionarios.lbcargoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
       TEdit(sender).Font.Style:=[fsBold,fsUnderline];
end;

procedure TFfuncionarios.lbcargoClick(Sender: TObject);
var
  Cargo: TFcargoFuncionario;
begin
     try
        Cargo:=TFcargoFuncionario.Create(nil);
     except

     end;

     try
           if(edtcargo.Text='')
           then Exit;
           Cargo.Tag:=StrToInt(edtcargo.text);
           Cargo.ShowModal;
     finally
           FreeAndNil(Cargo);
     end;
end;

procedure TFfuncionarios.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     if (Key = VK_F1) then
      Fprincipal.chamaPesquisaMenu();
end;

procedure TFfuncionarios.Limpaedit;
begin
   edtdatanascimento.Text:='';
   edtnome.Text:='';
   edtrg.Text:='';
   edtcpf.Text:='';
   edtcnh.Text:='';
   edtendereco.Text:='';
   edtcomplemento.Text:='';
   edtbairro.Text:='';
   edtcidade.Text:='';
   edtestado.Text:='';
   edtcep.Text:='';
   edtfone.Text:='';
   edtcelular.Text:='';
   edtcontato.Text:='';
   edtcarteiratrabalho.Text:='';
   edtcontacorrente.Text:='';
   edtbanco.Text:='';
   edtsalario.Text:='';
   edtagencia.Text:='';
   edtNumeroDependentesSalFamilia.Text:='';
   edtcodigoplanodecontas.Text:='';
   edtportador.Text:='';
   edtNumeroDependentesIRPF.Text:='';
   edtcargo.Text:='';
   edtsenha.Text:='';
end;

end.

