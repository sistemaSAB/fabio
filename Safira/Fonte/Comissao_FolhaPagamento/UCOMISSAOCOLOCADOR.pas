unit UCOMISSAOCOLOCADOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mask, StdCtrls, Buttons, ExtCtrls,db, UObjCOMISSAOCOLOCADOR,
  Uessencialglobal,UessencialLocal, Tabs;

type
  TFCOMISSAOCOLOCADOR = class(TForm)
    btNovo: TSpeedButton;
    btFechar: TSpeedButton;
    btMinimizar: TSpeedButton;
    btSalvar: TSpeedButton;
    btAlterar: TSpeedButton;
    btCancelar: TSpeedButton;
    btSair: TSpeedButton;
    btRelatorio: TSpeedButton;
    btExcluir: TSpeedButton;
    btPesquisar: TSpeedButton;
    lbAjuda: TLabel;
    btAjuda: TSpeedButton;
    PainelExtra: TPanel;
    ImageGuiaPrincipal: TImage;
    ImageGuiaExtra: TImage;
    PainelPrincipal: TPanel;
    ImagePainelPrincipal: TImage;
    ImagePainelExtra: TImage;
    Guia: TTabSet;
    Notebook: TNotebook;
    Bevel: TBevel;
    LbCODIGO: TLabel;
    LbPedidoProjeto: TLabel;
    LbBasedeCalculo: TLabel;
    LbPorcentagemPP: TLabel;
    LbPERCENTUALCOMISSAO: TLabel;
    LbVALORCOMISSAO: TLabel;
    LbPEDIDOPROJETOROMANEIO: TLabel;
    EdtCODIGO: TEdit;
    EdtPedidoProjeto: TEdit;
    EdtBasedeCalculo: TEdit;
    EdtPorcentagemPP: TEdit;
    EdtPERCENTUALCOMISSAOColocador: TEdit;
    EdtVALORCOMISSAO: TEdit;
    EdtPEDIDOPROJETOROMANEIO: TEdit;
    combofinalizado: TComboBox;
    edtdatafinalizacao: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
//DECLARA COMPONENTES

    procedure FormCreate(Sender: TObject);
    Procedure PosicionaPaineis;
    Procedure PegaFiguras;
    Procedure ColocaAtalhoBotoes;
    Procedure PosicionaForm;
    procedure FormShow(Sender: TObject);
    procedure ImageGuiaPrincipalClick(Sender: TObject);
    procedure ImageGuiaExtraClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure EdtPedidoProjetoExit(Sender: TObject);
    procedure EdtPEDIDOPROJETOROMANEIOExit(Sender: TObject);
    procedure EdtPedidoProjetoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdtPEDIDOPROJETOROMANEIOKeyDown(Sender: TObject;
      var Key: Word; Shift: TShiftState);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FCOMISSAOCOLOCADOR: TFCOMISSAOCOLOCADOR;
  ObjCOMISSAOCOLOCADOR:TObjCOMISSAOCOLOCADOR;

implementation

uses Upesquisa;

{$R *.dfm}


procedure TFCOMISSAOCOLOCADOR.FormCreate(Sender: TObject);
var
  Points: array [0..15] of TPoint;
  Regiao1:HRgn;
begin

Points[0].X :=305;      Points[0].Y := 22;
Points[1].X :=286;      Points[1].Y := 3;
Points[2].X :=41;       Points[2].Y := 3;
Points[3].X :=41;       Points[3].Y := 22;
Points[4].X :=28;       Points[4].Y := 35;
Points[5].X :=28;       Points[5].Y := 164;
Points[6].X :=41;       Points[6].Y := 177;
Points[7].X :=41;       Points[7].Y := 360;
Points[8].X :=62;       Points[8].Y := 381;
Points[9].X :=632;      Points[9].Y := 381;
Points[10].X :=647;     Points[10].Y := 396;
Points[11].X :=764;     Points[11].Y := 396;
Points[12].X :=764;     Points[12].Y := 95;
Points[13].X :=780;     Points[13].Y := 79;
Points[14].X :=780;     Points[14].Y := 22;
Points[15].X :=305;     Points[15].Y := 22;

Regiao1:=CreatePolygonRgn(Points, 16, WINDING);
SetWindowRgn(Self.Handle, regiao1, False);


limpaedit(Self);
Self.limpaLabels;
desabilita_campos(Self);
Guia.TabIndex:=0;

Try
   ObjCOMISSAOCOLOCADOR:=TObjCOMISSAOCOLOCADOR.create(Self);
Except
      Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
      Self.close;
End;

end;

procedure TFCOMISSAOCOLOCADOR.PosicionaPaineis;
begin
  With Self.PainelPrincipal do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelPrincipal do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  With Self.PainelExtra  do
  Begin
    Top := -13;
    Width := 98;
    Height := 367;
    BevelOuter := bvNone;
    TabOrder := 1;
  end;

  With Self.ImagePainelExtra  do
  Begin
    Left := 1;
    Top := 14;
    Width := 98;
    Height := 352;
  end;

  Self.PainelPrincipal.Enabled:=true;
  Self.PainelPrincipal.Visible:=true;
  Self.PainelExtra.Enabled:=false;
  Self.PainelExtra.Visible:=false;
end;

procedure TFCOMISSAOCOLOCADOR.FormShow(Sender: TObject);
begin
    PegaCorForm(Self);
    Self.PosicionaPaineis;
    Self.PosicionaForm;
    Self.PegaFiguras;
    Self.ColocaAtalhoBotoes;

end;

procedure TFCOMISSAOCOLOCADOR.ImageGuiaPrincipalClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=true;
PainelPrincipal.Visible:=true;
PainelExtra.Enabled:=false;
PainelExtra.Visible:=false;
end;

procedure TFCOMISSAOCOLOCADOR.ImageGuiaExtraClick(Sender: TObject);
begin
PainelPrincipal.Enabled:=false;
PainelPrincipal.Visible:=false;
PainelExtra.Enabled:=true;
PainelExtra.Visible:=true;

end;

procedure TFCOMISSAOCOLOCADOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCOMISSAOCOLOCADOR=Nil)
     Then exit;

     If (ObjCOMISSAOCOLOCADOR.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjCOMISSAOCOLOCADOR.free;
    Action := caFree;
    Self := nil;
end;

procedure TFCOMISSAOCOLOCADOR.PegaFiguras;
begin
    PegaFigura(ImageGuiaPrincipal,'guia_principal.jpg');
    PegaFigura(ImageGuiaExtra,'guia_extra.jpg');
    PegaFigura(ImagePainelPrincipal,'fundo_menu.jpg');
    PegaFigura(ImagePainelExtra,'fundo_menu.jpg');
    PegaFiguraBotao(btNovo,'botao_novo.bmp');
    PegaFiguraBotao(btSalvar,'botao_salvar.bmp');
    PegaFiguraBotao(btAlterar,'botao_alterar.bmp');
    PegaFiguraBotao(btCancelar,'botao_Cancelar.bmp');
    PegaFiguraBotao(btPesquisar,'botao_Pesquisar.bmp');
    PegaFiguraBotao(btExcluir,'botao_excluir.bmp');
    PegaFiguraBotao(btRelatorio,'botao_relatorios.bmp');
    PegaFiguraBotao(btSair,'botao_sair.bmp');
    PegaFiguraBotao(btFechar,'botao_close.bmp');
    PegaFiguraBotao(btMinimizar,'botao_minimizar.bmp');
    PegaFiguraBotao(btAjuda,'lampada.bmp');

end;

procedure TFCOMISSAOCOLOCADOR.ColocaAtalhoBotoes;
begin
   Coloca_Atalho_Botoes(BtNovo, 'N');
   Coloca_Atalho_Botoes(BtSalvar, 'S');
   Coloca_Atalho_Botoes(BtAlterar, 'A');
   Coloca_Atalho_Botoes(BtCancelar, 'C');
   Coloca_Atalho_Botoes(BtExcluir, 'E');
   Coloca_Atalho_Botoes(BtPesquisar, 'P');
   Coloca_Atalho_Botoes(BtRelatorio, 'R');
   Coloca_Atalho_Botoes(BtSair, 'I');
end;

procedure TFCOMISSAOCOLOCADOR.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

     edtcodigo.text:='0';
     //edtcodigo.text:=ObjCOMISSAOCOLOCADOR.Get_novocodigo;
     edtcodigo.enabled:=False;
     EdtVALORCOMISSAO.enabled:=False;


     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjCOMISSAOCOLOCADOR.status:=dsInsert;
     Guia.TabIndex:=0;
     EdtPedidoProjeto.setfocus;

end;

procedure TFCOMISSAOCOLOCADOR.btSalvarClick(Sender: TObject);
begin

     If ObjCOMISSAOCOLOCADOR.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCOMISSAOCOLOCADOR.salvar(true)=False)
     Then exit;

     edtCodigo.text:=ObjCOMISSAOCOLOCADOR.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMISSAOCOLOCADOR.btAlterarClick(Sender: TObject);
begin
    If (ObjCOMISSAOCOLOCADOR.Status=dsinactive) and (EdtCodigo.text<>'')
    Then Begin
                habilita_campos(Self);
                EdtCodigo.enabled:=False;
                EdtVALORCOMISSAO.enabled:=False;
                ObjCOMISSAOCOLOCADOR.Status:=dsEdit;
                guia.TabIndex:=0;
                EdtPedidoProjeto.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
          End;

end;

procedure TFCOMISSAOCOLOCADOR.btCancelarClick(Sender: TObject);
begin
     ObjCOMISSAOCOLOCADOR.cancelar;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCOMISSAOCOLOCADOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCOMISSAOCOLOCADOR.Get_pesquisa,ObjCOMISSAOCOLOCADOR.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCOMISSAOCOLOCADOR.status<>dsinactive
                                  then exit;

                                  If (ObjCOMISSAOCOLOCADOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCOMISSAOCOLOCADOR.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFCOMISSAOCOLOCADOR.btExcluirClick(Sender: TObject);
begin
     If (ObjCOMISSAOCOLOCADOR.status<>dsinactive) or (Edtcodigo.text='')
     Then exit;

     If (ObjCOMISSAOCOLOCADOR.LocalizaCodigo(edtcodigo.text)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCOMISSAOCOLOCADOR.exclui(edtcodigo.text,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOMISSAOCOLOCADOR.btRelatorioClick(Sender: TObject);
begin
    ObjCOMISSAOCOLOCADOR.Imprime(edtcodigo.text);
end;

procedure TFCOMISSAOCOLOCADOR.btSairClick(Sender: TObject);
begin
    Self.close;
end;

procedure TFCOMISSAOCOLOCADOR.PosicionaForm;
begin
    Self.Caption:='      '+Self.Caption;
    Self.Left:=-18;
    Self.Top:=0;
end;
procedure TFCOMISSAOCOLOCADOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCOMISSAOCOLOCADOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCOMISSAOCOLOCADOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;

    if key=vk_f5
    Then Begin
              if (Self.PainelPrincipal.Visible=true)
              Then Begin
                        Self.PainelPrincipal.Visible:=False;
                        Self.PainelExtra.Visible:=True;
              End
              Else Begin
                        Self.PainelPrincipal.Visible:=True;
                        Self.PainelExtra.Visible:=False;
              End;
    End;
end;

procedure TFCOMISSAOCOLOCADOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCOMISSAOCOLOCADOR.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjCOMISSAOCOLOCADOR do
    Begin
        Submit_CODIGO(edtCODIGO.text);
        PedidoProjeto.Submit_codigo(edtPedidoProjeto.text);
        Submit_BasedeCalculo(edtBasedeCalculo.text);
        Submit_PorcentagemPP(edtPorcentagemPP.text);
        Submit_PercentualComissaoColocador(edtPercentualComissaoColocador.text);
        PEDIDOPROJETOROMANEIO.Submit_codigo(edtPEDIDOPROJETOROMANEIO.text);
        Submit_Finalizado(Submit_ComboBox(combofinalizado));
        Submit_DataFinalizacao(edtdatafinalizacao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCOMISSAOCOLOCADOR.LimpaLabels;
begin
//LIMPA LABELS

end;

function TFCOMISSAOCOLOCADOR.ObjetoParaControles: Boolean;
begin
  Try
     With ObjCOMISSAOCOLOCADOR do
     Begin
        EdtCODIGO.text:=Get_CODIGO;
        EdtPedidoProjeto.text:=PedidoProjeto.Get_codigo;
        EdtBasedeCalculo.text:=Get_BasedeCalculo;
        EdtPorcentagemPP.text:=Get_PorcentagemPP;
        EdtPercentualComissaoColocador.text:=Get_PercentualComissaoColocador;
        EdtVALORCOMISSAO.text:=Get_VALORCOMISSAO;
        EdtPEDIDOPROJETOROMANEIO.text:=PEDIDOPROJETOROMANEIO.Get_codigo;
        edtdatafinalizacao.Text:=Get_DataFinalizacao;

        if (Get_Finalizado='S')
        then combofinalizado.ItemIndex:=1
        Else combofinalizado.ItemIndex:=0;

//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCOMISSAOCOLOCADOR.TabelaParaControles: Boolean;
begin
     If (ObjCOMISSAOCOLOCADOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFCOMISSAOCOLOCADOR.EdtPedidoProjetoExit(Sender: TObject);
begin
     ObjCOMISSAOCOLOCADOR.EdtPedidoProjetoExit(sender,nil);
end;

procedure TFCOMISSAOCOLOCADOR.EdtPEDIDOPROJETOROMANEIOExit(
  Sender: TObject);
begin
     ObjCOMISSAOCOLOCADOR.EdtPEDIDOPROJETOROMANEIOExit(sender,nil);
end;

procedure TFCOMISSAOCOLOCADOR.EdtPedidoProjetoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAOCOLOCADOR.EdtPedidoProjetoKeyDown(sender,key,shift,nil);
end;

procedure TFCOMISSAOCOLOCADOR.EdtPEDIDOPROJETOROMANEIOKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
     ObjCOMISSAOCOLOCADOR. EdtPEDIDOPROJETOROMANEIOKeyDown(sender,key,shift,nil);
end;

end.
