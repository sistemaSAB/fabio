object FCOMISSAO_ADIANT_FUNCFOLHA: TFCOMISSAO_ADIANT_FUNCFOLHA
  Left = 141
  Top = 169
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsNone
  Caption = 'Relac. Folha de Pagamento e Comiss'#245'es'
  ClientHeight = 409
  ClientWidth = 792
  Color = 13421772
  Ctl3D = False
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object ImageGuiaPrincipal: TImage
    Left = 25
    Top = -1
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaPrincipalClick
  end
  object ImageGuiaExtra: TImage
    Left = 25
    Top = 76
    Width = 13
    Height = 76
    Cursor = crHandPoint
    OnClick = ImageGuiaExtraClick
  end
  object btFechar: TSpeedButton
    Left = 754
    Top = 6
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btFecharClick
  end
  object btMinimizar: TSpeedButton
    Left = 754
    Top = 32
    Width = 24
    Height = 24
    Cursor = crHandPoint
    Flat = True
    OnClick = btMinimizarClick
  end
  object lbAjuda: TLabel
    Left = 649
    Top = 351
    Width = 50
    Height = 13
    Caption = 'Ajuda...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object btAjuda: TSpeedButton
    Left = 719
    Top = 336
    Width = 28
    Height = 38
    Flat = True
  end
  object PainelExtra: TPanel
    Left = 40
    Top = 27
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 0
    object ImagePainelExtra: TImage
      Left = 0
      Top = 14
      Width = 98
      Height = 352
    end
  end
  object PainelPrincipal: TPanel
    Left = 40
    Top = -13
    Width = 98
    Height = 367
    BevelOuter = bvNone
    TabOrder = 1
    object ImagePainelPrincipal: TImage
      Left = 1
      Top = 14
      Width = 98
      Height = 352
    end
    object btNovo: TSpeedButton
      Left = 15
      Top = 25
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btNovoClick
    end
    object btSalvar: TSpeedButton
      Left = 15
      Top = 66
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSalvarClick
    end
    object btAlterar: TSpeedButton
      Left = 15
      Top = 107
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btAlterarClick
    end
    object btCancelar: TSpeedButton
      Left = 15
      Top = 148
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btCancelarClick
    end
    object btSair: TSpeedButton
      Left = 15
      Top = 311
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btSairClick
    end
    object btRelatorio: TSpeedButton
      Left = 15
      Top = 270
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btRelatorioClick
    end
    object btExcluir: TSpeedButton
      Left = 15
      Top = 229
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      OnClick = btExcluirClick
    end
    object btPesquisar: TSpeedButton
      Left = 15
      Top = 188
      Width = 67
      Height = 39
      Cursor = crHandPoint
      Flat = True
      Layout = blGlyphRight
      OnClick = btPesquisarClick
    end
  end
  object Notebook: TNotebook
    Left = 137
    Top = 1
    Width = 616
    Height = 334
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 3
    object TPage
      Left = 0
      Top = 0
      Caption = 'Ferragem'
      object Bevel: TBevel
        Left = 0
        Top = 0
        Width = 616
        Height = 334
        Align = alClient
        Shape = bsFrame
      end
      object LbCODIGO: TLabel
        Left = 5
        Top = 30
        Width = 40
        Height = 13
        Caption = 'C'#243'digo'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbComissaoVendedor: TLabel
        Left = 5
        Top = 82
        Width = 133
        Height = 13
        Caption = 'Comiss'#227'o de Vendedor'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbComissaoColocador: TLabel
        Left = 5
        Top = 134
        Width = 136
        Height = 13
        Caption = 'Comiss'#227'o de Colocador'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbAdiantamentoFuncionario: TLabel
        Left = 5
        Top = 187
        Width = 141
        Height = 13
        Caption = 'Adiantamento de Sal'#225'rio'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object LbFuncionarioFolhaPagamento: TLabel
        Left = 5
        Top = 239
        Width = 203
        Height = 13
        Caption = 'Funcion'#225'rio da Folha de Pagamento'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 5
        Top = 292
        Width = 61
        Height = 13
        Caption = 'Hora Extra'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 125
        Top = 293
        Width = 68
        Height = 13
        Caption = 'Gratifica'#231#227'o'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 261
        Top = 293
        Width = 183
        Height = 13
        Caption = 'Descanso Semanal remunerado'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 469
        Top = 293
        Width = 136
        Height = 13
        Caption = 'Desconto Cont. Sindical'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Verdana'
        Font.Style = []
        ParentFont = False
      end
      object EdtCODIGO: TEdit
        Left = 5
        Top = 46
        Width = 72
        Height = 19
        MaxLength = 9
        TabOrder = 0
      end
      object EdtComissaoVendedor: TEdit
        Left = 5
        Top = 98
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 1
        OnExit = edtComissaoVendedorExit
        OnKeyDown = edtComissaoVendedorKeyDown
      end
      object EdtComissaoColocador: TEdit
        Left = 5
        Top = 150
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 2
        OnExit = edtComissaoColocadorExit
        OnKeyDown = edtComissaoColocadorKeyDown
      end
      object EdtAdiantamentoFuncionario: TEdit
        Left = 5
        Top = 203
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 3
        OnExit = edtAdiantamentoFuncionarioExit
        OnKeyDown = edtAdiantamentoFuncionarioKeyDown
      end
      object EdtFuncionarioFolhaPagamento: TEdit
        Left = 5
        Top = 255
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 4
        OnExit = edtFuncionarioFolhaPagamentoExit
        OnKeyDown = edtFuncionarioFolhaPagamentoKeyDown
      end
      object edthoraextra: TEdit
        Left = 5
        Top = 308
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 5
        OnExit = edthoraextraExit
        OnKeyDown = edthoraextraKeyDown
      end
      object edtgratificacao: TEdit
        Left = 125
        Top = 308
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 6
        OnExit = edtgratificacaoExit
        OnKeyDown = edtgratificacaoKeyDown
      end
      object edtAcrescimoDSR: TEdit
        Left = 261
        Top = 307
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 7
      end
      object edtdescontocontsindical: TEdit
        Left = 469
        Top = 307
        Width = 72
        Height = 19
        Color = clSkyBlue
        MaxLength = 9
        TabOrder = 8
      end
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Perfilado'
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'Componente'
    end
    object TPage
      Left = 0
      Top = 0
      Caption = 'KitBox'
    end
  end
  object Guia: TTabSet
    Left = 138
    Top = -1
    Width = 614
    Height = 22
    BackgroundColor = 6710886
    DitherBackground = False
    EndMargin = 0
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Verdana'
    Font.Style = []
    ParentBackground = True
    StartMargin = -3
    SelectedColor = 15921906
    Tabs.Strings = (
      'Principal')
    TabIndex = 0
    UnselectedColor = 13421772
  end
end
