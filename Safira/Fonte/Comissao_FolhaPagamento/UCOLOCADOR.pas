unit UCOLOCADOR;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls,db, UObjCOLOCADOR,
  Uessencialglobal,UessencialLocal, Tabs,IBQuery;

type
  TFCOLOCADOR = class(TForm)
    edtFuncionario: TEdit;
    lbFuncionario: TLabel;
    lbNomeFuncionario: TLabel;
    edtComissao: TEdit;
    lbComissao: TLabel;
    pnlbotes: TPanel;
    lbnomeformulario: TLabel;
    lbCodigo: TLabel;
    lb1: TLabel;
    btrelatorio: TBitBtn;
    btopcoes: TBitBtn;
    btpesquisar: TBitBtn;
    btexcluir: TBitBtn;
    btcancelar: TBitBtn;
    btsalvar: TBitBtn;
    btalterar: TBitBtn;
    btnovo: TBitBtn;
    btsair: TBitBtn;
    pnl1: TPanel;
    imgrodape: TImage;
    lb9: TLabel;

    //DECLARA COMPONENTES
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btNovoClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btAlterarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btPesquisarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btMinimizarClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure edtFuncionarioExit(Sender: TObject);
    procedure edtFuncionarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtFuncionarioKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    Function  ControlesParaObjeto:Boolean;
    Function  ObjetoParaControles:Boolean;
    Function  TabelaParaControles:Boolean;
    Procedure LimpaLabels;
    procedure MostraQuantidadeCadastrada;

  public
    { Public declarations }
  end;

var
  FCOLOCADOR: TFCOLOCADOR;
  ObjCOLOCADOR:TObjCOLOCADOR;

implementation

uses Upesquisa, UDataModulo, UescolheImagemBotao, Uprincipal;

{$R *.dfm}


procedure TFCOLOCADOR.FormClose(Sender: TObject; var Action: TCloseAction);
begin
     If (ObjCOLOCADOR=Nil)
     Then exit;

     If (ObjCOLOCADOR.status<>dsinactive)
     Then Begin
          Messagedlg('N�o � poss�vel Finalizar antes de Cancelar ou Salvar as Altera��es!',mterror,[mbok],0);
          abort;
          exit;
     End;

    ObjCOLOCADOR.free;
    //Action := caFree;
    //Self := nil;
end;


procedure TFCOLOCADOR.btNovoClick(Sender: TObject);
begin
     limpaedit(Self);
     Self.limpaLabels;
     habilita_campos(Self);
     desab_botoes(Self);

      lbCodigo.Caption:='0';
     //edtcodigo.text:=ObjCOLOCADOR.Get_novocodigo;

      btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;

     btSalvar.enabled:=True;
     BtCancelar.enabled:=True;
     btpesquisar.enabled:=True;

     ObjCOLOCADOR.status:=dsInsert;
     Edtfuncionario.setfocus;

end;

procedure TFCOLOCADOR.btSalvarClick(Sender: TObject);
begin

     If ObjCOLOCADOR.Status=dsInactive
     Then exit;

     If ControlesParaObjeto=False
     Then Begin
               Messagedlg('Erro na tentativa de Transferir os Dados dos Edits para o Objeto!',mterror,[mbok],0);
               exit;
          End;

     If (ObjCOLOCADOR.salvar(true)=False)
     Then exit;

      lbCodigo.Caption:=ObjCOLOCADOR.Get_codigo;
     habilita_botoes(Self);
     //limpaedit(Self);
     //Self.limpaLabels;
     desabilita_campos(Self);
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;
     MostraQuantidadeCadastrada;
     Messagedlg('Os Dados foram Salvos com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOLOCADOR.btAlterarClick(Sender: TObject);
begin
    If (ObjCOLOCADOR.Status=dsinactive) and ( lbCodigo.Caption<>'')
    Then Begin
                habilita_campos(Self);

                ObjCOLOCADOR.Status:=dsEdit;

                edtfuncionario.setfocus;
                desab_botoes(Self);
                btSalvar.enabled:=True;
                BtCancelar.enabled:=True;
                btpesquisar.enabled:=True;
                btnovo.Visible :=false;
     btalterar.Visible:=false;
     btpesquisar.Visible:=false;
     btrelatorio.Visible:=false;
     btexcluir.Visible:=false;
     btsair.Visible:=false;
     btopcoes.Visible :=false;
          End;

end;

procedure TFCOLOCADOR.btCancelarClick(Sender: TObject);
begin
     ObjCOLOCADOR.cancelar;
     btnovo.Visible :=true;
     btalterar.Visible:=true;
     btpesquisar.Visible:=true;
     btrelatorio.Visible:=true;
     btexcluir.Visible:=true;
     btsair.Visible:=true;
     btopcoes.Visible :=true;

     limpaedit(Self);
     Self.limpaLabels;
     desabilita_campos(Self);
     habilita_botoes(Self);

end;

procedure TFCOLOCADOR.btPesquisarClick(Sender: TObject);
var
   FpesquisaLocal:TFpesquisa;
begin

        Try
           Fpesquisalocal:=Tfpesquisa.create(Self);

            If (FpesquisaLocal.PreparaPesquisa(ObjCOLOCADOR.Get_pesquisa,ObjCOLOCADOR.Get_TituloPesquisa,Nil)=True)
            Then Begin
                      Try
                        If (FpesquisaLocal.showmodal=mrok)
                        Then Begin
                                  If ObjCOLOCADOR.status<>dsinactive
                                  then exit;

                                  If (ObjCOLOCADOR.LocalizaCodigo(FpesquisaLocal.QueryPesq.fieldbyname('codigo').asstring)=False)
                                  Then Begin
                                            Messagedlg('Dados n�o encontrados!',mterror,[mbok],0);
                                            exit;
                                       End;
                                  ObjCOLOCADOR.ZERARTABELA;
                                  If (TabelaParaControles=False)
                                  Then Begin
                                            Messagedlg('Erro na Transfer�ncia dos Dados!',mterror,[mbok],0);
                                            limpaedit(Self);
                                            Self.limpaLabels;
                                            exit;
                                       End;

                             End;
                      Finally
                        FpesquisaLocal.QueryPesq.close;
                      End;
                 End;

        Finally
           FreeandNil(FPesquisaLocal);
        End;
end;

procedure TFCOLOCADOR.btExcluirClick(Sender: TObject);
begin
     If (ObjCOLOCADOR.status<>dsinactive) or ( lbCodigo.Caption='')
     Then exit;

     If (ObjCOLOCADOR.LocalizaCodigo( lbCodigo.Caption)=False)
     Then Begin
               Messagedlg('Registro n�o localizado para ser exclu�do!',mterror,[mbok],0);
               exit;
          End;

     If (Messagedlg('Certeza que deseja Excluir?',mtconfirmation,[mbyes,mbno],0)=Mrno)
     Then exit;

     If (ObjCOLOCADOR.exclui( lbCodigo.Caption,True)=False)
     Then Begin
               Messagedlg('Erro Durante a Exclus�o!!'+#13+'Poss�veis Causas: Registro bloqueado por outro usu�rio!',mterror,[mbok],0);
               exit;
          End;
     limpaedit(Self);
     Self.limpaLabels;
     Messagedlg('Exclus�o Conclu�da com Sucesso!',mtinformation,[mbok],0);

end;

procedure TFCOLOCADOR.btRelatorioClick(Sender: TObject);
begin
    ObjCOLOCADOR.Imprime( lbCodigo.Caption);
end;

procedure TFCOLOCADOR.btSairClick(Sender: TObject);
begin
    Self.close;
end;


procedure TFCOLOCADOR.btFecharClick(Sender: TObject);
begin
Self.Close;
end;

procedure TFCOLOCADOR.btMinimizarClick(Sender: TObject);
begin
      Self.WindowState:=wsMinimized;
end;

procedure TFCOLOCADOR.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    If Key=VK_Escape
    Then Self.Close;
    if(key=vk_f1) then
    begin
        Fprincipal.chamaPesquisaMenu;
    end;
end;

procedure TFCOLOCADOR.FormKeyPress(Sender: TObject; var Key: Char);
begin
   if key=#13
   Then Perform(Wm_NextDlgCtl,0,0);
end;

function TFCOLOCADOR.ControlesParaObjeto: Boolean;
begin
  Try
    With ObjCOLOCADOR do
    Begin
        Submit_CODIGO( lbCodigo.Caption);
        Funcionario.Submit_codigo(edtFuncionario.text);
        Submit_Comissao(edtComissao.text);
//CODIFICA SUBMITS


         result:=true;
    End;
  Except
        result:=False;
  End;

end;

procedure TFCOLOCADOR.LimpaLabels;
begin
     LbNomeFuncionario.CAPTION:='';
     lbCodigo.Caption:='';
end;

function TFCOLOCADOR.ObjetoParaControles: Boolean;
begin
  Try
     With ObjCOLOCADOR do
     Begin
         lbCodigo.Caption:=Get_CODIGO;
        EdtFuncionario.text:=Funcionario.Get_codigo;
        LbNomeFuncionario.caption:=funcionario.Get_Nome;
        EdtComissao.text:=Get_Comissao;

//CODIFICA GETS


        result:=True;
     End;
  Except
        Result:=False;
  End;
end;

function TFCOLOCADOR.TabelaParaControles: Boolean;
begin
     If (ObjCOLOCADOR.TabelaparaObjeto=False)
     Then Begin
                result:=False;
                exit;
          End;
     If (ObjetoParaControles=False)
     Then Begin
                result:=False;
                exit;
          End;
     Result:=True;

end;

procedure TFCOLOCADOR.edtFuncionarioExit(Sender: TObject);
begin
     ObjCOLOCADOR.EdtFuncionarioExit(sender,LbNomeFuncionario);
end;

procedure TFCOLOCADOR.edtFuncionarioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     ObjCOLOCADOR.EdtFuncionarioKeyDown(sender,key,shift,LbNomeFuncionario);
end;

procedure TFCOLOCADOR.edtFuncionarioKeyPress(Sender: TObject;
  var Key: Char);
begin
       if not (Key in['0'..'9',Chr(8)]) then
       begin
              Key:= #0;
       end;
end;

procedure TFCOLOCADOR.MostraQuantidadeCadastrada;
var
  Query: TIBQuery;
  contador:Integer;
begin
    contador:=0;
    try
      query:=TIBQuery.Create(nil);
      Query.Database:=FDataModulo.IBDatabase;
    except

    end;

    try
      with Query do
       begin
         close;
         sql.Clear;
         sql.Add('select * from tabcolocador');
         open;
         while not Eof do
         begin
           inc(contador,1);
           Next;
         end;
       end;
       if(contador>1) then
            lb9.Caption:='Existem '+IntToStr(contador)+' colocadores cadastrados'
       else
       begin
            lb9.Caption:='Existe '+IntToStr(contador)+' colocadore cadastrado';
       end;

    finally

    end;


end;


procedure TFCOLOCADOR.FormShow(Sender: TObject);
begin
          FescolheImagemBotao.PegaFiguraBotoespequeno(btnovo,btalterar,btcancelar,btsalvar,btpesquisar,btrelatorio,btexcluir,btsair,btopcoes);
          FescolheImagemBotao.PegaFiguraImagem(ImgRodape,'RODAPE');
          limpaedit(Self);
          Self.limpaLabels;
          desabilita_campos(Self);


          Try
             ObjCOLOCADOR:=TObjCOLOCADOR.create;
          Except
                Messagedlg('Erro na Inicializa��o do Objeto !',mterror,[mbok],0);
                Self.close;
          End;

          if (ObjPermissoesUsoGlobal.ValidaPermissao('ACESSO AO CADASTRO DE COLOCADOR')=False)
          Then desab_botoes(Self)
          Else habilita_botoes(Self);

          MostraQuantidadeCadastrada;

end;


end.
