unit UobjFAIXADESCONTOIRPF;
Interface
Uses Ibquery,windows,Classes,Db,UessencialGlobal;

Type
   TObjFAIXADESCONTOIRPF=class

          Public
                //ObjDatasource                               :TDataSource;
                Status                                      :TDataSetState;
                SqlInicial                                  :String[200];
//CODIFICA VARIAVEIS PUBLICAS

                Constructor Create;
                Destructor  Free;
                Function    Salvar(ComCommit:Boolean)       :Boolean;
                Function    LocalizaCodigo(Parametro:string) :boolean;
                Function    LocalizaValor(Pvalor:string):Boolean;

                Function    Exclui(Pcodigo:string;ComCommit:boolean)            :Boolean;
                Function    Get_Pesquisa                    :TStringList;
                Function    Get_TituloPesquisa              :string;

                Function   TabelaparaObjeto:Boolean;
                Procedure   ZerarTabela;
                Procedure   Cancelar;
                Procedure   Commit;

                Function  Get_NovoCodigo:string;
                Function  RetornaCampoCodigo:string;
                Function  RetornaCampoNome:string;
                Procedure Imprime(Pcodigo:string);

                Procedure Submit_CODIGO(parametro: string);
                Function Get_CODIGO: string;
                Procedure Submit_ValorInicial(parametro: string);
                Function Get_ValorInicial: string;
                Procedure Submit_ValorFinal(parametro: string);
                Function Get_ValorFinal: string;
                Procedure Submit_Percentual(parametro: string);
                Function Get_Percentual: string;

                Procedure Submit_ValorDeducao(parametro:string);
                Function  Get_ValorDeducao:string;
                //CODIFICA DECLARA GETSESUBMITS

         Private
               Objquery:Tibquery;
               InsertSql,DeleteSql,ModifySQl:TStringList;

               CODIGO:string;
               ValorInicial:string;
               ValorFinal:string;
               Percentual:string;
               ValorDeducao:string;
//CODIFICA VARIAVEIS PRIVADAS






               ParametroPesquisa:TStringList;

                Function  VerificaBrancos:Boolean;
                Function  VerificaRelacionamentos:Boolean;
                Function  VerificaNumericos:Boolean;
                Function  VerificaData:Boolean;
                Function  VerificaFaixa:boolean;
                Procedure ObjetoparaTabela;
   End;


implementation
uses SysUtils,Dialogs,UDatamodulo,Controls
//USES IMPLEMENTATION
, UMenuRelatorios;


{ TTabTitulo }


Function  TObjFAIXADESCONTOIRPF.TabelaparaObjeto:Boolean;//ok
//procedimento que transfere os dados do DATASET para o Objeto
begin
     With Objquery do
     Begin
        Self.ZerarTabela;
        Self.CODIGO:=fieldbyname('CODIGO').asstring;
        Self.ValorInicial:=fieldbyname('ValorInicial').asstring;
        Self.ValorFinal:=fieldbyname('ValorFinal').asstring;
        Self.Percentual:=fieldbyname('Percentual').asstring;
        Self.ValorDeducao:=Fieldbyname('Valordeducao').asstring;

        //CODIFICA TABELAPARAOBJETO
        result:=True;
     End;
end;


Procedure TObjFAIXADESCONTOIRPF.ObjetoparaTabela;//ok
//procedimento que transfere os dados do Objeto
//para a Tabela para ser salvo pelo BD atrav�s do DATASET
begin
  With Objquery do
  Begin
        ParamByName('CODIGO').asstring              :=Self.CODIGO;
        ParamByName('ValorInicial').asstring        :=virgulaparaponto(Self.ValorInicial);
        ParamByName('ValorFinal').asstring          :=virgulaparaponto(Self.ValorFinal);
        ParamByName('Percentual').asstring          :=virgulaparaponto(Self.Percentual);
        ParamByname('valordeducao').asstring:=virgulaparaponto(Self.ValorDeducao);
        //CODIFICA OBJETOPARATABELA
  End;
End;

//***********************************************************************

function TObjFAIXADESCONTOIRPF.Salvar(ComCommit:Boolean): Boolean;//Ok
//procedimento para salvar os dados do objeto na tabela
//se � insercao ou edicao depende da variavel STATUS
//� testado todas as inconsistencias antes de SALVAR
begin
  result:=False;

  if (Self.VerificaBrancos=True)
  Then exit;

  if (Self.VerificaNumericos=False)
  Then Exit;

  if (Self.VerificaData=False)
  Then Exit;

  if (Self.VerificaFaixa=False)
  Then Exit;

  if (Self.VerificaRelacionamentos=False)
  Then Exit;


   If Self.LocalizaCodigo(Self.CODIGO)=False
   Then Begin
             if(Self.Status=dsedit)
             Then Begin
                       Messagedlg('O registro n�o foi encontrado para Edi��o!',mterror,[mbok],0);
                       exit;
             End;
   End
   Else Begin
             if(Self.Status=dsinsert)
             Then Begin
                       Messagedlg('J� existe um registro com estes dados!',mterror,[mbok],0);
                       exit;
             End;
   End;

    if Self.status=dsinsert
    Then Begin
              Self.Objquery.SQL.Clear;
              Self.Objquery.SQL.text:=Self.InsertSql.Text;
              if (Self.Codigo='0')
              Then Self.codigo:=Self.Get_NovoCodigo;
    End
    Else Begin
              if (Self.Status=dsedit)
              Then Begin
                        Self.Objquery.SQL.Clear;
                        Self.Objquery.SQL.text:=Self.ModifySQl.Text;
              End
              Else Begin
                        Messagedlg('O status n�o esta nem como inser��o nem como edi��o!',mterror,[mbok],0);
                        exit;
              End;
    End;
 Self.ObjetoParaTabela;
 Try
    Self.Objquery.ExecSQL;
 Except
       if (Self.Status=dsInsert)
       Then Messagedlg('Erro na  tentativa de Inserir',mterror,[mbok],0)
       Else Messagedlg('Erro na  tentativa de Editar',mterror,[mbok],0); 
       exit;
 End;

 If ComCommit=True
 Then FDataModulo.IBTransaction.CommitRetaining;

 Self.status          :=dsInactive;
 result:=True;
end;

procedure TObjFAIXADESCONTOIRPF.ZerarTabela;//Ok
//limpa os campos do objeto sejam eles
//outros objetos ou naum
//usado para deixar o objeto pronto
//para receber outros dados
Begin
     With Self do
     Begin
        CODIGO:='';
        ValorInicial:='';
        ValorFinal:='';
        Percentual:='';
        ValorDeducao:='';
//CODIFICA ZERARTABELA
     End;
end;

Function TObjFAIXADESCONTOIRPF.VerificaBrancos:boolean;
//procedimento usado para verificar antes de salvar
//se tem algum campo que naum poderia estar em branco
var
   Mensagem:string;
begin
  Result:=True;
  mensagem:='';

  With Self do
  Begin
      If (CODIGO='')
      Then Mensagem:=mensagem+'/C�digo';
      If (ValorInicial='')
      Then Mensagem:=mensagem+'/Valor Inicial';
      If (ValorFinal='')
      Then Mensagem:=mensagem+'/Valor Final';
      If (Percentual='')
      Then Mensagem:=mensagem+'/Percentual';

      If (ValorDeducao='')
      Then ValorDeducao:='0';
      //CODIFICA VERIFICABRANCOS
  End;

  if mensagem<>''
  Then Begin//mostra mensagem de erro caso existam cpos requeridos em branco
            messagedlg('Os Seguintes Campos n�o podem estar vazios: '+mensagem,mterror,[mbok],0);
            exit;
  End;
   result:=false;
end;


function TObjFAIXADESCONTOIRPF.VerificaRelacionamentos: Boolean;
//usado apenas por chaves estrangeiras
//ou seja, um campo chave estrangeira
//tem que NULL (depende da situacao)
//ou conter valor valido que indique o campo
//primeiro de outra tabela, neste procedimento
//� localizado este dado na outra tabela
var
mensagem:string;
Begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICARELACIONAMENTOS


     If (mensagem<>'')
     Then Begin
               Messagedlg('Os Seguintes erros foram encontrados:'+#13+Mensagem,mterror,[mbok],0);
               exit;
          End;
     result:=true;
End;

function TObjFAIXADESCONTOIRPF.VerificaNumericos: Boolean;
//procedimento usado para verificar
//se tem algum campo numerico com valor invalido
//ou seja em ALGUMAS situacoes campos numericos
//podem ser NULOS porem em outras somente
//valores numericos COMO o (0) por exemplo
//testo passando de string para o tipo dele
//ou inteiro ou real
var
   Mensagem:string;
begin
     Result:=False;
     Mensagem:='';
     try
        Strtoint(Self.CODIGO);
     Except
           Mensagem:=mensagem+'/C�digo';
     End;
     try
        Strtofloat(Self.ValorInicial);
     Except
           Mensagem:=mensagem+'/Valor Inicial';
     End;
     try
        Strtofloat(Self.ValorFinal);
     Except
           Mensagem:=mensagem+'/Valor Final';
     End;
     try
        Strtofloat(Self.Percentual);
     Except
           Mensagem:=mensagem+'/Percentual';
     End;

     try
        Strtofloat(Self.ValorDeducao);
     Except
           Mensagem:=mensagem+'/Valor de Dedu��o';
     End;
//CODIFICA VERIFICANUMERICOS


     If Mensagem<>''
     Then Begin
               Messagedlg('Os seguintes campos cont�m valores inv�lidos: '+mensagem,mterror,[mbok],0);
               exit;
     End;
     result:=true;

end;

function TObjFAIXADESCONTOIRPF.VerificaData: Boolean;
//campo usado para verificar se te alguma data em branco
//que naum deveria estar
//mesmo caso do numerico, em ALGUNS casos elas podem ser nulas
//em outros NAUM
var
Mensagem:string;
begin
     Result:=False;
     mensagem:='';
//CODIFICA VERIFICADATA


     If Mensagem<>''
     Then Begin
           Messagedlg('Os Seguintes campos cont�m Datas ou Horas inv�lidas:'+Mensagem,mterror,[mbok],0);
           exit;
     End;
     result:=true;

end;

function TObjFAIXADESCONTOIRPF.VerificaFaixa: boolean;
//USADO EM CASOS DE CAMPOS QUE TENHAM UMA FAIXA DE VALORES
//COMO POR EXEMPLO de 1 a 10, ou "S" ou "N" ,
//ou "M" ou "F"
var
   Mensagem:string;
begin
     Result:=False;
Try
   With Self do
   Begin
        Mensagem:='';
//CODIFICA VERIFICAFAIXA

        If mensagem<>''
        Then Begin
               Messagedlg('Os seguintes Erros foram encontrados!'+#13+mensagem,mterror,[mbok],0);
                    exit;
        End;
        result:=true;
  End;
Finally

end;

end;

function TObjFAIXADESCONTOIRPF.LocalizaValor(Pvalor: string): Boolean;
begin
       result:=False;
       if (Pvalor='')
       Then Begin
                 Messagedlg('Par�metro FAIXADESCONTOIRPF vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ValorInicial,ValorFinal,Percentual,valordeducao');
           SQL.ADD(' from  TabFaixaDescontoIRPF');
           SQL.ADD(' WHERE valorinicial<='+virgulaparaponto(Pvalor)+' and valorfinal>'+virgulaparaponto(Pvalor));
           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

function TObjFAIXADESCONTOIRPF.LocalizaCodigo(parametro: string): boolean;//ok
begin
       if (Parametro='')
       Then Begin
                 Messagedlg('Par�metro FAIXADESCONTOIRPF vazio',mterror,[mbok],0);
                 exit;
       End;

       With Self.Objquery do
       Begin
           close;
           Sql.Clear;
           SQL.ADD('Select CODIGO,ValorInicial,ValorFinal,Percentual,valordeducao');
           SQL.ADD('from  TabFaixaDescontoIRPF');
           SQL.ADD('WHERE codigo='+parametro);
//CODIFICA LOCALIZACODIGO


           Open;
           If (recordcount>0)
           Then Result:=True
           Else Result:=False;
       End;
end;

procedure TObjFAIXADESCONTOIRPF.Cancelar;
begin
     Self.status:=dsInactive;
end;

function TObjFAIXADESCONTOIRPF.Exclui(Pcodigo: string;ComCommit:Boolean): Boolean;
//Localiza o registro e tenta exclusao
//o parametro COMCOMMIT indica se os dados
//poderao ser COMMITADOS ou Naum
begin
     Try
        result:=true;
        If (Self.LocalizaCodigo(Pcodigo)=True)
        Then Begin
                 Self.Objquery.close;
                 Self.Objquery.SQL.clear;
                 Self.Objquery.SQL.Text:=Self.DeleteSql.Text;
                 Self.Objquery.ParamByName('codigo').asstring:=Pcodigo;
                 Self.Objquery.ExecSQL;
                 If (ComCommit=True)
                 Then FDataModulo.IBTransaction.CommitRetaining;
             End

        Else result:=false;
     Except
           result:=false;
     End;
end;


constructor TObjFAIXADESCONTOIRPF.create;
//procedimento que cria a instancia do objeto
//e os seus objetos internos
//alem de preencher as SQLS do DATASET
begin


        Self.Objquery:=TIBQuery.create(nil);
        Self.Objquery.Database:=FDataModulo.IbDatabase;
        Self.ParametroPesquisa:=TStringList.create;

        InsertSql:=TStringList.create;
        DeleteSql:=TStringList.create;
        ModifySQl:=TStringList.create;
//CODIFICA CRIACAO DE OBJETOS

        Self.ZerarTabela;

        With Self do
        Begin

                InsertSQL.clear;
                InsertSQL.add('Insert Into TabFaixaDescontoIRPF(CODIGO,ValorInicial');
                InsertSQL.add(' ,ValorFinal,Percentual,valordeducao)');
                InsertSQL.add('values (:CODIGO,:ValorInicial,:ValorFinal,:Percentual,:valordeducao');
                InsertSQL.add(' )');
//CODIFICA INSERTSQL

                ModifySQL.clear;
                ModifySQL.add('Update TabFaixaDescontoIRPF set CODIGO=:CODIGO,ValorInicial=:ValorInicial');
                ModifySQL.add(',ValorFinal=:ValorFinal,Percentual=:Percentual,valordeducao=:valordeducao');
                ModifySQL.add('where codigo=:codigo');
//CODIFICA MODIFYSQL

                DeleteSQL.clear;
                DeleteSql.add('Delete from TabFaixaDescontoIRPF where codigo=:codigo ');
//CODIFICA DELETESQL

                Self.status          :=dsInactive;
        End;

end;
procedure TObjFAIXADESCONTOIRPF.Commit;
begin
     FDataModulo.IBTransaction.CommitRetaining;
end;

function TObjFAIXADESCONTOIRPF.Get_Pesquisa: TStringList;
begin
     Self.ParametroPesquisa.clear;
     Self.ParametroPesquisa.add('Select * from TabFAIXADESCONTOIRPF');
     Result:=Self.ParametroPesquisa;
end;

function TObjFAIXADESCONTOIRPF.Get_TituloPesquisa: string;
begin
     Result:=' Pesquisa de FAIXADESCONTOIRPF ';
end;


function TObjFAIXADESCONTOIRPF.Get_NovoCodigo: string;
var
IbQueryGen:TIBQuery;
begin
     Try
        Try
           IbqueryGen:=TIBquery.create(nil);
           IbqueryGen.database:=FdataModulo.IBDatabase;
           IbqueryGen.close;
           IbqueryGen.sql.clear;
      IbqueryGen.sql.add('SELECT GEN_ID(GENFAIXADESCONTOIRPF,1) CODIGO FROM RDB$DATABASE');
//CODIFICA NOMEPROCEDIMENTO NOVOCODIGO

//IbqueryGen.sql.add('SELECT GEN_ID(GENFAIXADESCONTOIRPF,1) CODIGO FROM RDB$DATABASE');
           IbqueryGen.open;
           Result:=IbqueryGen.fieldbyname('CODIGO').asstring;
        Except
           Messagedlg('Erro durante a Cria��o de Um novo C�digo para o GRUPO',mterror,[mbok],0);
           result:='0';
           exit;
        End;
     Finally
            FreeandNil(IbqueryGen);
     End;
End;


destructor TObjFAIXADESCONTOIRPF.Free;
begin
    Freeandnil(Self.Objquery);
    Freeandnil(Self.ParametroPesquisa);
    Freeandnil(InsertSql);
    Freeandnil(DeleteSql);
    Freeandnil(ModifySQl);
    //CODIFICA DESTRUICAO DE OBJETOS

end;

//Usado em pesquisas de form antigos
//para retornar os dados do objeto
function TObjFAIXADESCONTOIRPF.RetornaCampoCodigo: string;
begin
      result:='codigo';
//CODIFICA RETORNACAMPOCODIGO

end;

//USado em forms antigos para retornar nomes
//caso existam na tabela para labels.
function TObjFAIXADESCONTOIRPF.RetornaCampoNome: string;
begin
      result:='';
//CODIFICA RETORNACAMPONOME

end;

procedure TObjFaixaDescontoIRPF.Submit_CODIGO(parametro: string);
begin
        Self.CODIGO:=Parametro;
end;
function TObjFaixaDescontoIRPF.Get_CODIGO: string;
begin
        Result:=Self.CODIGO;
end;
procedure TObjFaixaDescontoIRPF.Submit_ValorInicial(parametro: string);
begin
        Self.ValorInicial:=Parametro;
end;
function TObjFaixaDescontoIRPF.Get_ValorInicial: string;
begin
        Result:=Self.ValorInicial;
end;
procedure TObjFaixaDescontoIRPF.Submit_ValorFinal(parametro: string);
begin
        Self.ValorFinal:=Parametro;
end;
function TObjFaixaDescontoIRPF.Get_ValorFinal: string;
begin
        Result:=Self.ValorFinal;
end;
procedure TObjFaixaDescontoIRPF.Submit_Percentual(parametro: string);
begin
        Self.Percentual:=Parametro;
end;
function TObjFaixaDescontoIRPF.Get_Percentual: string;
begin
        Result:=Self.Percentual;
end;
//CODIFICA GETSESUBMITS


//CODIFICA EXITONKEYDOWN
procedure TObjFAIXADESCONTOIRPF.Imprime(Pcodigo: string);
begin
     With FmenuRelatorios do
     Begin
          NomeObjeto:='UOBJFAIXADESCONTOIRPF';

          With RgOpcoes do
          Begin
                items.clear;
          End;

          showmodal;

          If (Tag=0)//indica botao cancel ou fechar
          Then exit;

          Case RgOpcoes.ItemIndex of
          0:begin
          end;

          End;
     end;

end;

function TObjFAIXADESCONTOIRPF.Get_ValorDeducao: string;
begin
     Result:=Self.ValorDeducao;
end;

procedure TObjFAIXADESCONTOIRPF.Submit_ValorDeducao(
  parametro: string);
begin
     if (Parametro='')
     Then Parametro:='0';

     Self.ValorDeducao:=Parametro;
end;

end.



