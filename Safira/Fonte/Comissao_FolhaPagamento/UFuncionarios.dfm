object FFuncionarios: TFFuncionarios
  Left = 707
  Top = 148
  Width = 905
  Height = 533
  Caption = 'Cadastro de Funcion'#225'rios - EXCLAIM TECNOLOGIA'
  Color = clWindow
  Ctl3D = False
  Font.Charset = ANSI_CHARSET
  Font.Color = clWhite
  Font.Height = -11
  Font.Name = 'Arial'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object pnlbotes: TPanel
    Left = 0
    Top = 0
    Width = 889
    Height = 54
    Align = alTop
    BevelOuter = bvNone
    Color = clWindow
    TabOrder = 0
    DesignSize = (
      889
      54)
    object lbnomeformulario: TLabel
      Left = 579
      Top = 3
      Width = 104
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Cadastro de'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object lbCodigo: TLabel
      Left = 718
      Top = 9
      Width = 99
      Height = 32
      Alignment = taRightJustify
      Anchors = [akTop, akRight]
      Caption = ' 10906  '
      Font.Charset = ANSI_CHARSET
      Font.Color = clGreen
      Font.Height = -27
      Font.Name = 'Arial'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lb1: TLabel
      Left = 579
      Top = 25
      Width = 108
      Height = 22
      Align = alCustom
      Anchors = [akTop, akRight]
      Caption = 'Funcion'#225'rios'
      Font.Charset = ANSI_CHARSET
      Font.Color = 6710886
      Font.Height = -19
      Font.Name = 'Arial'
      Font.Style = []
      ParentFont = False
    end
    object btrelatorios: TBitBtn
      Left = 301
      Top = 0
      Width = 50
      Height = 52
      Caption = '&r'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = btrelatoriosClick
      Spacing = 0
    end
    object btopcoes: TBitBtn
      Left = 352
      Top = 0
      Width = 50
      Height = 52
      Caption = '&o'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      Spacing = 0
    end
    object btpesquisar: TBitBtn
      Left = 251
      Top = 0
      Width = 50
      Height = 52
      Caption = '&p'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = btpesquisarClick
      Spacing = 0
    end
    object btexcluir: TBitBtn
      Left = 201
      Top = 0
      Width = 50
      Height = 52
      Caption = '&e'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = btexcluirClick
      Spacing = 0
    end
    object btcancelar: TBitBtn
      Left = 151
      Top = 0
      Width = 50
      Height = 52
      Caption = '&c'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = btcancelarClick
      Spacing = 0
    end
    object btsalvar: TBitBtn
      Left = 101
      Top = 0
      Width = 50
      Height = 52
      Caption = '&g'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = btgravarClick
      Spacing = 0
    end
    object btalterar: TBitBtn
      Left = 51
      Top = 0
      Width = 50
      Height = 52
      Caption = '&a'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = btalterarClick
      Spacing = 0
    end
    object btnovo: TBitBtn
      Left = 1
      Top = 0
      Width = 50
      Height = 52
      Caption = '&n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = BtnovoClick
      Spacing = 0
    end
    object btsair: TBitBtn
      Left = 402
      Top = 0
      Width = 50
      Height = 52
      Caption = '&s'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWhite
      Font.Height = -1
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = btsairClick
      Spacing = 0
    end
  end
  object pnl1: TPanel
    Left = 0
    Top = 450
    Width = 889
    Height = 45
    Align = alBottom
    Color = clMedGray
    TabOrder = 1
    DesignSize = (
      889
      45)
    object imgrodape: TImage
      Left = 1
      Top = 0
      Width = 887
      Height = 44
      Align = alBottom
    end
    object lb2: TLabel
      Left = 548
      Top = 11
      Width = 288
      Height = 20
      Anchors = [akTop]
      Caption = 'Existem X funcion'#225'rios cadastrados'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -16
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
  end
  object pgcGuia: TPageControl
    Left = 0
    Top = 54
    Width = 889
    Height = 396
    ActivePage = tsDP
    Align = alClient
    Style = tsFlatButtons
    TabOrder = 2
    object tsPrincipal: TTabSheet
      Caption = '&1 - Principal'
      object panel2: TPanel
        Left = 0
        Top = 0
        Width = 881
        Height = 364
        Align = alClient
        BevelOuter = bvNone
        Color = 10643006
        TabOrder = 0
        object lb3: TLabel
          Left = 4
          Top = 8
          Width = 25
          Height = 14
          Caption = 'Ativo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb5: TLabel
          Left = 4
          Top = 60
          Width = 27
          Height = 14
          Caption = 'Nome'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb6: TLabel
          Left = 4
          Top = 82
          Width = 15
          Height = 14
          Caption = 'RG'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb7: TLabel
          Left = 364
          Top = 77
          Width = 19
          Height = 14
          Caption = 'CPF'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb8: TLabel
          Left = 332
          Top = 100
          Width = 55
          Height = 14
          Caption = 'Estado Civil'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb9: TLabel
          Left = 4
          Top = 31
          Width = 96
          Height = 14
          Caption = 'Data de Nascimento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb10: TLabel
          Left = 4
          Top = 193
          Width = 33
          Height = 14
          Caption = 'Cidade'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb11: TLabel
          Left = 4
          Top = 169
          Width = 29
          Height = 14
          Caption = 'Bairro'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb12: TLabel
          Left = 4
          Top = 121
          Width = 46
          Height = 14
          Caption = 'Endereco'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb13: TLabel
          Left = 4
          Top = 241
          Width = 19
          Height = 14
          Caption = 'Cep'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb14: TLabel
          Left = 4
          Top = 145
          Width = 64
          Height = 14
          Caption = 'Complemento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb15: TLabel
          Left = 4
          Top = 265
          Width = 41
          Height = 14
          Caption = 'Telefone'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb16: TLabel
          Left = 354
          Top = 265
          Width = 33
          Height = 14
          Caption = 'Celular'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb17: TLabel
          Left = 4
          Top = 287
          Width = 37
          Height = 14
          Caption = 'Contato'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb22: TLabel
          Left = 4
          Top = 102
          Width = 21
          Height = 14
          Caption = 'CNH'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb30: TLabel
          Left = 4
          Top = 217
          Width = 33
          Height = 14
          Caption = 'Estado'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object cbbcomboativo: TComboBox
          Left = 106
          Top = 5
          Width = 150
          Height = 22
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ItemHeight = 14
          ItemIndex = 1
          ParentFont = False
          TabOrder = 0
          Text = 'Sim'
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object edtnome: TEdit
          Left = 105
          Top = 53
          Width = 440
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          TabOrder = 2
        end
        object edtrg: TEdit
          Left = 105
          Top = 75
          Width = 150
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 15
          ParentFont = False
          TabOrder = 3
        end
        object edtcpf: TEdit
          Left = 397
          Top = 74
          Width = 148
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 15
          ParentFont = False
          TabOrder = 4
        end
        object cbbcomboestadocivil: TComboBox
          Left = 397
          Top = 95
          Width = 148
          Height = 22
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ItemHeight = 14
          ItemIndex = 0
          ParentFont = False
          TabOrder = 6
          Text = 'Solteito(a)'
          Items.Strings = (
            'Solteito(a)'
            'Casado(a)'
            'Outro')
        end
        object edtdatanascimento: TMaskEdit
          Left = 105
          Top = 31
          Width = 78
          Height = 20
          EditMask = '!99/99/9999;1;_'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 1
          Text = '  /  /    '
        end
        object edtcidade: TEdit
          Left = 105
          Top = 189
          Width = 440
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 10
        end
        object edtbairro: TEdit
          Left = 105
          Top = 165
          Width = 440
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 9
        end
        object edtendereco: TEdit
          Left = 105
          Top = 119
          Width = 440
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          TabOrder = 7
        end
        object edtcep: TEdit
          Left = 105
          Top = 237
          Width = 150
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 12
        end
        object edtcomplemento: TEdit
          Left = 105
          Top = 141
          Width = 440
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 8
        end
        object edtfone: TEdit
          Left = 105
          Top = 260
          Width = 150
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 13
        end
        object edtcelular: TEdit
          Left = 397
          Top = 260
          Width = 148
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 14
        end
        object edtcontato: TEdit
          Left = 105
          Top = 281
          Width = 150
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 15
        end
        object edtcnh: TEdit
          Left = 105
          Top = 97
          Width = 150
          Height = 20
          BevelInner = bvNone
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 5
        end
        object edtestado: TEdit
          Left = 105
          Top = 213
          Width = 70
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 2
          ParentFont = False
          TabOrder = 11
        end
      end
    end
    object tsDP: TTabSheet
      Caption = '&2 - Departamento Pessoal'
      ImageIndex = 1
      object panel3: TPanel
        Left = 0
        Top = 0
        Width = 881
        Height = 364
        Align = alClient
        Color = 10643006
        TabOrder = 0
        object lb31: TLabel
          Left = 7
          Top = 240
          Width = 41
          Height = 14
          Caption = 'Portador'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb29: TLabel
          Left = 7
          Top = 216
          Width = 237
          Height = 14
          Caption = 'C'#243'digo Plano de Contas (cont'#225'bil) - Adiantamento'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb24: TLabel
          Left = 7
          Top = 192
          Width = 178
          Height = 14
          Caption = 'Ultrapassar Limite  de Adiantamento?'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb28: TLabel
          Left = 7
          Top = 168
          Width = 141
          Height = 14
          Caption = 'N. Dependentes (Sal. Familia)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lbNomePlanodeContas_Adiantamento: TLabel
          Left = 349
          Top = 216
          Width = 48
          Height = 14
          Caption = 'Portador'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbnomeportador: TLabel
          Left = 349
          Top = 240
          Width = 48
          Height = 14
          Caption = 'Portador'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lb27: TLabel
          Left = 7
          Top = 264
          Width = 151
          Height = 14
          Caption = 'N'#250'mero de Dependentes (IRPF)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb18: TLabel
          Left = 7
          Top = 48
          Width = 73
          Height = 14
          Caption = 'Conta Corrente'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb21: TLabel
          Left = 7
          Top = 24
          Width = 98
          Height = 14
          Caption = 'Carteira de Trabalho'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb20: TLabel
          Left = 7
          Top = 72
          Width = 31
          Height = 14
          Caption = 'Banco'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb23: TLabel
          Left = 7
          Top = 96
          Width = 33
          Height = 14
          Caption = 'Sal'#225'rio'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb25: TLabel
          Left = 7
          Top = 144
          Width = 87
          Height = 14
          Caption = 'Data de Admiss'#227'o'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb19: TLabel
          Left = 7
          Top = 120
          Width = 40
          Height = 14
          Caption = 'Ag'#234'ncia'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lb26: TLabel
          Left = 7
          Top = 288
          Width = 29
          Height = 14
          Caption = 'Cargo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object lbcargo: TLabel
          Left = 349
          Top = 290
          Width = 278
          Height = 14
          Cursor = crHandPoint
          AutoSize = False
          Caption = 'Cargo'
          Font.Charset = ANSI_CHARSET
          Font.Color = clAqua
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          OnClick = lbcargoClick
          OnMouseMove = lbcargoMouseMove
        end
        object lb4: TLabel
          Left = 7
          Top = 309
          Width = 139
          Height = 14
          Caption = 'Senha (Fun'#231#245'es do sistema)'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object edtportador: TEdit
          Left = 254
          Top = 238
          Width = 90
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 9
          OnExit = edtportadorExit
          OnKeyDown = edtportadorKeyDown
          OnKeyPress = edtportadorKeyPress
        end
        object edtcodigoplanodecontas: TEdit
          Left = 254
          Top = 214
          Width = 90
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 20
          ParentFont = False
          TabOrder = 8
          OnExit = edtcodigoplanodecontasExit
          OnKeyDown = edtcodigoplanodecontasKeyDown
          OnKeyPress = edtcodigoplanodecontasKeyPress
        end
        object cbbcombopermiteultrapassarlimiteadiantamento: TComboBox
          Left = 254
          Top = 187
          Width = 90
          Height = 22
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ItemHeight = 14
          ItemIndex = 0
          ParentFont = False
          TabOrder = 7
          Text = 'N'#227'o'
          Items.Strings = (
            'N'#227'o'
            'Sim')
        end
        object edtNumeroDependentesSalFamilia: TEdit
          Left = 254
          Top = 163
          Width = 90
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 15
          ParentFont = False
          TabOrder = 6
        end
        object edtNumeroDependentesIRPF: TEdit
          Left = 254
          Top = 262
          Width = 90
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 15
          ParentFont = False
          TabOrder = 10
        end
        object edtcarteiratrabalho: TEdit
          Left = 254
          Top = 18
          Width = 170
          Height = 20
          BevelInner = bvNone
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 0
        end
        object edtcontacorrente: TEdit
          Left = 254
          Top = 42
          Width = 170
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 15
          ParentFont = False
          TabOrder = 1
        end
        object edtbanco: TEdit
          Left = 254
          Top = 66
          Width = 170
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 30
          ParentFont = False
          TabOrder = 2
        end
        object edtsalario: TEdit
          Left = 254
          Top = 91
          Width = 170
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 3
        end
        object edtagencia: TEdit
          Left = 254
          Top = 115
          Width = 170
          Height = 20
          CharCase = ecUpperCase
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 4
        end
        object edtdataadmissao: TMaskEdit
          Left = 254
          Top = 139
          Width = 90
          Height = 20
          EditMask = '!99/99/9999;1;_'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 10
          ParentFont = False
          TabOrder = 5
          Text = '  /  /    '
        end
        object edtcargo: TEdit
          Left = 254
          Top = 284
          Width = 90
          Height = 20
          Color = 6073854
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 9
          ParentFont = False
          TabOrder = 11
          OnExit = edtcargoExit
          OnKeyDown = edtcargoKeyDown
          OnKeyPress = edtcargoKeyPress
        end
        object edtsenha: TEdit
          Left = 254
          Top = 306
          Width = 90
          Height = 20
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          MaxLength = 50
          ParentFont = False
          PasswordChar = '#'
          TabOrder = 12
        end
      end
    end
  end
end
